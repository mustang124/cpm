﻿/***********************************************************************************
Description:      
Find Functions using a specified parameter

Author: Rodrick Blanton
Date: 31 Jul 2017
Change History: 
example 01/09/2014:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/
 
SELECT
    SCHEMA_NAME(SCHEMA_ID) AS [Schema], 
    so.name AS [ObjectName],
    so.Type_Desc AS [ObjectType (UDF/SP)],
    p.parameter_id AS [ParameterID],
    p.name AS [ParameterName],
    TYPE_NAME(p.user_type_id) AS [ParameterDataType],
    p.max_length AS [ParameterMaxBytes],
    p.is_output AS [IsOutPutParameter]
FROM sys.objects AS so
INNER JOIN sys.parameters AS p ON so.OBJECT_ID = p.OBJECT_ID
WHERE so.OBJECT_ID IN
( 
    SELECT OBJECT_ID 
    FROM sys.objects
    --WHERE TYPE IN ('P','FN') -- Procedures and Functions
    WHERE TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT') -- Functions
)
AND p.name = '@[PARAMETER NAME]'
--AND so.name = '[ADD FUNCTION NAME]'
ORDER BY [Schema], so.name, p.parameter_id
GO