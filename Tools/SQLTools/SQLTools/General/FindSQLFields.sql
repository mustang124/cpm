﻿/***********************************************************************************
Description:      
Specify a column and find the tables or views it belongs to.

Author: Rodrick Blanton
Date: 05 Dec 2017
Change History: 
example 01/09/2014:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/



SELECT      COLUMN_NAME AS 'ColumnName'
            ,TABLE_NAME AS  'TableName'
FROM        INFORMATION_SCHEMA.COLUMNS
WHERE       COLUMN_NAME LIKE '%FieldName%'
ORDER BY    TableName
            ,ColumnName;