﻿/***********************************************************************************
Description:      
Specify a column and find the tables it belongs to.

Author: Rodrick Blanton
Date: 31 Jul 2017
Change History: 
example 01/09/2014:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/


DECLARE @searchTerm VARCHAR(50) = '%[COLUMN NAME]'

SELECT tb.name, c.name, ty.name
FROM Sys.columns c
INNER JOIN Sys.tables tb ON c.object_id = tb.object_id
INNER JOIN Sys.types ty ON c.user_type_id =  ty.user_type_id
WHERE c.name like @searchTerm