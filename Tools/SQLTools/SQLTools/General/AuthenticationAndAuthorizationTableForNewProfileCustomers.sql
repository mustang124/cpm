/***********************************************************************************
Description:      
Used to for authentication and authorization of new Profile and ProfileLite customers.

Author: Stuart Bard
Date: 2017-08-08

-----------------------------------------------------------------------------------
Notes:
Replace XXPAC with the RefineryID you need.
The ChartTitle ties to dbo.Chart_LU and shows the KPIs you want.
***********************************************************************************/
USE [ProfileFuels12]
INSERT INTO [ProfileFuels12].[dbo].[AuthenticationAndAuthorization] ([RefineryGroupId],[RefineryId],[Active],[CertificateRequired],[ClientKeyRequired],[ClientKey]) VALUES ('TEST','XXPAC ',1,0,1,'SUKEohFspIafM1KDHRhp3UuTcQAKR93l8+0iWGifV00=');
