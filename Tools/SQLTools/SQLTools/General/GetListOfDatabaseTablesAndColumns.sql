﻿Select t.name [table_name]
, t.object_id
, c.name [column_name]
, c.column_id
, ty.name [data_type]
, c.max_length
, c.precision
, c.scale
, c.is_nullable
From sys.tables t
Inner Join sys.columns c on t.object_id = c.object_id
Inner Join sys.types ty on c.user_type_id = ty.user_type_id

UNION ALL

Select v.name [table_name]
, v.object_id
, c.name [column_name]
, c.column_id
, ty.name [data_type]
, c.max_length
, c.precision
, c.scale
, c.is_nullable
From sys.views v
Inner Join sys.columns c on v.object_id = c.object_id
Inner Join sys.types ty on c.user_type_id = ty.user_type_id

Order by [table_name]
, c.column_id