/***********************************************************************************
Description:      
Used to show the RefineryTrends report for ProfileLite customers.

Author: Stuart Bard
Date: 2017-08-07

-----------------------------------------------------------------------------------
Notes:
Replace XXPAC with the RefineryID you need.
The ChartTitle ties to dbo.Chart_LU and shows the KPIs you want.
***********************************************************************************/
USE [ProfileFuels12]
DECLARE @RefineryId nvarchar(16) = '58NSA';
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Average Energy Cost',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Average Produced Fuel Cost',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Average Purchased Energy Cost',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Cash Operating Expenses UEDC Basis',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'EDC',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Energy Intensity Index',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Maintenance Efficiency Index',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Maintenance Index',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Maintenance Personnel Efficiency Index',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Mechanical Availability',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Mechanical Unavailability due to T/A',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Net Raw Material Input',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Non-Energy Operating Expenses EDC Basis',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Non-Turnaround Index',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Non-Turnaround MEI',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Operational Availability',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Process Unit Utilization',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Turnaround Index',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'Turnaround MEI',NULL);
INSERT INTO dbo.ProfileLiteScorecardItems(RefineryID,ChartTitle,SortKey) VALUES (@RefineryId,'UEDC',NULL);
