﻿USE [Power]
GO
/****** Object:  UserDefinedFunction [dbo].[GetRollOvers]    Script Date: 11/21/2017 12:59:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************
Description:      
To be used in conjunction with other queries.  This function returns all rollovers ([YY]P)
and test accounts (X).

Author: Rodrick Blanton
Date: 21 NOV 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		Use in a where clause such as the following to exclude all rollovers:
		WHERE RefNum NOT IN (SELECT dbo.GetRollOvers(0))

		Use in Select statement such as the following to return rollovers by year:
		SELECT RefNum from dbo.GetRollOvers(2016)

***********************************************************************************/
ALTER FUNCTION [dbo].[GetRollOvers] 
(	
	@studyYear INT = 0
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT DISTINCT Rtrim(refnum) + 'P' AS RefNum
	FROM TSort
	WHERE refnum IN   
	(
		SELECT Substring(refnum,1, Len(Rtrim(refnum))-1) 
		FROM TSORT 
		WHERE refnum like '%16P'
		AND StudyYear = CASE @studyYear WHEN 0 THEN StudyYear ELSE @studyYear END
	)
	UNION All 
	SELECT DISTINCT Rtrim(refnum) + 'X' AS RefNum
	FROM TSort 
	WHERE refnum IN
	(
		SELECT Substring(refnum,1, Len(Rtrim(refnum))-1) 
		FROM TSort 
		WHERE StudyYear = CASE @studyYear WHEN 0 THEN StudyYear ELSE @studyYear END
	)
)
