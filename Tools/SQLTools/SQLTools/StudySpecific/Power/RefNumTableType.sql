﻿/***********************************************************************************
Description:      
Table Type to store RefNums and pass to Procs and Functions

Author: Rodrick Blanton
Date: 21 NOV 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/



CREATE TYPE RefNumTable AS TABLE 
( 
	RefNum VARCHAR(100)
)
GO