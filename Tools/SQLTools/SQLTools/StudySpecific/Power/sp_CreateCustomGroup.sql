﻿-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************
Description:      
Create custom peer groups

Author: Rodrick Blanton
Date: 21 NOV 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/
ALTER PROCEDURE sp_CreateCustomGroup
	@ListName AS VARCHAR(30)
	, @ReportTitle AS VARCHAR(100)
	, @Batch AS VARCHAR(10)
	, @RefNums AS RefNumTable READONLY
	, @Description AS VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
		@refListNo AS INT
		, @rlUniqueID AS VARCHAR(25)
		, @cleanListName AS VARCHAR(100)



	BEGIN TRY

		-- SET Unique ID as DATETIME VARCHAR
		SELECT @rlUniqueID = (
								SELECT REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(23), GETDATE(), 126),'-',''),':',''),'.','')
							 )

		-- Check Existence of RefList
		SELECT @refListNo = (
								SELECT ISNULL(RefListNo, 0)
								FROM RefList_Lu
								WHERE ListName = @ListName
							)


		-- CREATE RefList Header
		IF @refListNo > 0 
		BEGIN

			-- If Listname already belongs to a list - make list unique by adding datetime string

			SELECT @ListName = @ListName + '_' + @rlUniqueID

			INSERT RefList_LU (ListName, CreateDate, [Owner], [Description])
			VALUES (@ListName, GETDATE(), 'ADMIN', @Description)

			SELECT @refListNo = (
									SELECT RefListNo 
									FROM RefList_Lu
									WHERE ListName = @ListName
								)

		END
		ELSE
		BEGIN
	
			-- If Listname does not belong to list, create new list header
			INSERT RefList_LU (ListName, CreateDate, [Owner], [Description])
			VALUES (@ListName, GETDATE(), 'ADMIN', @Description)

			SELECT @refListNo = (
									SELECT RefListNo 
									FROM RefList_Lu
									WHERE ListName = @ListName
								)

		END


		-- Retrieve new list number
		SELECT @refListNo = (
								SELECT ISNULL(RefListNo, 0)
								FROM RefList_Lu
								WHERE ListName = @ListName
							)


		-- Add Refnums to RefList
		INSERT RefList(RefListNo, Refnum)
		SELECT @refListNo, RefNum
		FROM @RefNums


		SELECT @cleanListName = REPLACE(LTRIM(RTRIM(@ListName)),' ','')


		INSERT Report.ReportGroups (Refnum, ListName,  ReportTitle,PostProcess, Batch)
		VALUES(@cleanListName, @ListName, @ReportTitle, 'Y', @Batch)


		EXEC report.UpdateReportGroup @cleanListName

		
		SELECT * FROM RefList_LU WHERE RefListNo = @refListNo
		SELECT * FROM RefList WHERE RefListNo = @refListNo
		SELECT * FROM Report.ReportGroups WHERE Refnum = @cleanListName AND ListName = @ListName AND ReportTitle = @ReportTitle AND Batch = @Batch

	END TRY
	BEGIN CATCH

		SELECT  
		ERROR_NUMBER() AS ErrorNumber  
		,ERROR_SEVERITY() AS ErrorSeverity  
		,ERROR_STATE() AS ErrorState  
		,ERROR_PROCEDURE() AS ErrorProcedure  
		,ERROR_LINE() AS ErrorLine  
		,ERROR_MESSAGE() AS ErrorMessage;  

	END CATCH	

END
GO
