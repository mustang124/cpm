﻿/***********************************************************************************
Description:      
2016 EGC Coefficient table created by Tony and Rodrick.  This table holds all the 
Multipliers/Coefficients and Powers/Exponents to support the model.

Author: Rodrick Blanton
Date: 02 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		none
***********************************************************************************/



USE [PowerGlobal]
GO

/****** Object:  Table [dbo].[EGCCoefficients2013]    Script Date: 8/2/2017 5:29:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[EGCCoefficients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] DEFAULT(1900) NOT NULL,
	[egcTechGroupID] VARCHAR(50) NOT NULL,
	[ExceptionName] VARCHAR(50) DEFAULT('') NOT NULL,
	[Pwr_Scrubber] [real] DEFAULT(0) NOT NULL,
	[Mult_Scrubber] [real] DEFAULT(0) NOT NULL,
	[Pwr_PrecBag] [real]  DEFAULT(0) NOT NULL,
	[Mult_PrecBag] [real]  DEFAULT(0) NOT NULL,
	[Pwr_SCR] [real]  DEFAULT(0) NOT NULL,
	[Mult_SCR] [real]  DEFAULT(0) NOT NULL,
	[Pwr_Freq50] [real]  DEFAULT(0) NOT NULL,
	[Mult_Freq50] [real]  DEFAULT(0) NOT NULL,
	[Pwr_Freq60] [real]  DEFAULT(0) NOT NULL,
	[Mult_Freq60] [real]  DEFAULT(0) NOT NULL,
	[Pwr_Starts] [real]  DEFAULT(0) NOT NULL,
	[Mult_Starts] [real]  DEFAULT(0) NOT NULL,
	[Pwr_SiteEffect] [real]  DEFAULT(0) NOT NULL,
	[Mult_SiteEffect] [real]  DEFAULT(0) NOT NULL,
	[Pwr_OtherSiteCap] [real]  DEFAULT(0) NOT NULL,
	[Mult_OtherSiteCap] [real]  DEFAULT(0) NOT NULL,
	[Pwr_SteamSalesMBTU] [real]  DEFAULT(0) NOT NULL,
	[Mult_SteamSalesMBTU] [real]  DEFAULT(0) NOT NULL,
	[Pwr_SolidFuel] [real]  DEFAULT(0) NOT NULL,
	[Mult_SolidFuel] [real]  DEFAULT(0) NOT NULL,
	[Pwr_NatGas] [real]  DEFAULT(0) NOT NULL,
	[Mult_NatGas] [real]  DEFAULT(0) NOT NULL,
	[Pwr_OffGas] [real]  DEFAULT(0) NOT NULL,
	[Mult_OffGas] [real]  DEFAULT(0) NOT NULL,
	[Pwr_DieselJet] [real]  DEFAULT(0) NOT NULL,
	[Mult_DieselJet] [real]  DEFAULT(0) NOT NULL,
	[Pwr_HeavyFuel] [real]  DEFAULT(0) NOT NULL,
	[Mult_HeavyFuel] [real]  DEFAULT(0) NOT NULL,
	[Pwr_Age] [real]  DEFAULT(0) NOT NULL,
	[Mult_Age] [real]  DEFAULT(0) NOT NULL,
	[Pwr_EquivBoilers] [real]  DEFAULT(0) NOT NULL,
	[Mult_EquivBoilers] [real]  DEFAULT(0) NOT NULL,
	[Pwr_Supercritical] [real]  DEFAULT(0) NOT NULL,
	[Mult_Supercritical] [real]  DEFAULT(0) NOT NULL,
	[Pwr_CTGGas] [real]  DEFAULT(0) NOT NULL,
	[Mult_CTGGas] [real]  DEFAULT(0) NOT NULL,
	[Pwr_SiteFuel] [real]  DEFAULT(0) NOT NULL,
	[Mult_SiteFuel] [real]  DEFAULT(0) NOT NULL,
	[Pwr_ETech] [real]  DEFAULT(0) NOT NULL,
	[Mult_ETech] [real]  DEFAULT(0) NOT NULL,
	[Pwr_FTech] [real]  DEFAULT(0) NOT NULL,
	[Mult_FTech] [real]  DEFAULT(0) NOT NULL,
	[Pwr_AeroTech] [real]  DEFAULT(0) NOT NULL,
	[Mult_AeroTech] [real]  DEFAULT(0) NOT NULL,
	[Pwr_NumSTs] [real]  DEFAULT(0) NOT NULL,
	[Mult_NumSTs] [real]  DEFAULT(0) NOT NULL,
	[Pwr_NumGens] [real]  DEFAULT(0) NOT NULL,
	[Mult_NumGens] [real]  DEFAULT(0) NOT NULL,
	[Pwr_TotEquip] [real]  DEFAULT(0) NOT NULL,
	[Mult_TotEquip] [real] DEFAULT(0) NOT NULL,
	[Pwr_MWH] [real] DEFAULT(0) NOT NULL,
	[Mult_MWH] [real] DEFAULT(0) NOT NULL,
	[Pwr_Subcritical] [real] DEFAULT(0) NOT NULL,
	[Mult_Subcritical] [real] DEFAULT(0) NOT NULL,
	[Pwr_POHours] [real] DEFAULT(0) NOT NULL,
	[Mult_POHours] [real] DEFAULT(0) NOT NULL,
	[Pwr_ServiceHours] [real] DEFAULT(0) NOT NULL,
	[Mult_ServiceHours] [real] DEFAULT(0) NOT NULL,
	[Pwr_HRSGCap] [real] DEFAULT(0) NOT NULL,
	[Mult_HRSGCap] [real] DEFAULT(0) NOT NULL,
	[Pwr_CTGLiquidFuel] REAL DEFAULT(0) NOT NULL,
	[Mult_CTGLiquidFuel] REAL DEFAULT(0) NOT NULL,
	[Pwr_HRSGCapN] REAL DEFAULT(0) NOT NULL,
	[Mult_HRSGCapN] REAL DEFAULT(0) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


