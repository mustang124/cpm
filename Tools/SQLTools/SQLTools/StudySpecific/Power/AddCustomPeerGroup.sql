﻿/*

NUON: Develop custom European peer groups: EU Super critical, EU sub critical,  EU “F” Class, and EU non “F” class

*/


--Select * From CRVGroup

--DECLARE @ListName VARCHAR(100)
--, @reportTitle VARCHAR(100)
--, @batch VARCHAR(10)
--, @description VARCHAR(100)
--, @refNums AS RefNumTable


--SET @ListName = 'Power16: SubCritical'
--SET @reportTitle = 'Power16'
--SET @batch = 'P16Custom'
--SET @description = '2016 Power SubCritical'

--INSERT @refNums
--SELECT Distinct t.Refnum
--FROM TSort t
--INNER JOIN breaks b on b.refnum = t.refnum
--WHERE  b.CRVGroup NOT IN ('LSC', 'ESC')  -- Ed says anything not LSC and ESC is subcritical
--AND T.Studyyear = 2016
--AND T.Continent = 'Europe'
--AND T.refnum NOT IN (SELECT RefNum FROM dbo.GetRollOvers(0))


--SELECT * FROM @refNums


----EXEC sp_CreateCustomGroup @ListName, @reportTitle, @batch, @refNums, @description



--/*****************************************************************************************************************************************************************************/



--DECLARE @ListName VARCHAR(100)
--, @reportTitle VARCHAR(100)
--, @batch VARCHAR(10)
--, @description VARCHAR(100)
--, @refNums AS RefNumTable


--SET @ListName = 'Power16: FClass'
--SET @reportTitle = 'Power16'
--SET @batch = 'P16Custom'
--SET @description = '2016 Power FClass'

--INSERT @refNums
--SELECT Distinct t.Refnum
--FROM TSort t
--INNER JOIN CTGData b on b.refnum = t.refnum
--WHERE  b.Class = 'F'  -- Ed says anything not LSC and ESC is subcritical
--AND T.Studyyear = 2016
--AND T.Continent = 'Europe'
--AND T.refnum NOT IN (SELECT RefNum FROM dbo.GetRollOvers(0))


--SELECT * FROM @refNums


--EXEC sp_CreateCustomGroup @ListName, @reportTitle, @batch, @refNums, @description



/*****************************************************************************************************************************************************************************/



DECLARE @ListName VARCHAR(100)
, @reportTitle VARCHAR(100)
, @batch VARCHAR(10)
, @description VARCHAR(100)
, @refNums AS RefNumTable


SET @ListName = 'Power16: NonFClass'
SET @reportTitle = 'Power16'
SET @batch = 'P16Custom'
SET @description = '2016 Power NonFClass'

INSERT @refNums
SELECT Distinct t.Refnum
FROM TSort t
INNER JOIN CTGData b on b.refnum = t.refnum
WHERE  b.Class <> 'F'  -- Ed says anything not LSC and ESC is subcritical
AND T.Studyyear = 2016
AND T.Continent = 'Europe'
AND T.refnum NOT IN (SELECT RefNum FROM dbo.GetRollOvers(0))


SELECT * FROM @refNums


EXEC sp_CreateCustomGroup @ListName, @reportTitle, @batch, @refNums, @description