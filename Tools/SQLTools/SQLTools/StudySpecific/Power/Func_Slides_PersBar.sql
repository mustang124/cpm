﻿
  Declare @CRVGroup VARCHAR(5) = 'CG3'
  , @Filter3 VARCHAR(50) = 'Cogen'
  , @ShortListName VARCHAR(50) = 'Power16'
  , @CompanyName VARCHAR(100) = 'NUON'
  , @GroupWA REAL
  , @CompanyWA REAL
  , @PaceWA REAL
  , @Divisor INT = 100

  DECLARE @NMC TABLE
  (
	NMC REAL
	, LVL VARCHAR(10)
  )

  INSERT @NMC
  SELECT (NMC/100), 'GROUP'
	From NERCFactors
	Where Refnum IN
	(
		Select b.Refnum
		From Breaks b
		Inner Join TSort t ON b.Refnum = t.Refnum
		Where b.Refnum IN
		(
			Select Refnum
			From Report.ReportGroupRefs
			Where ReportRefnum in
			(
				Select Refnum
				From Report.ReportGroups
				Where Refnum = @ShortListName
			)
		)
		AND b.CRVSizeGroup = @CRVGroup
		--AND t.CompanyName Like @CompanyName
	) 
	GROUP BY NMC


  INSERT @NMC
  SELECT (NMC/100), 'COMPANY'
	From NERCFactors
	Where Refnum IN
	(
		Select b.Refnum
		From Breaks b
		Inner Join TSort t ON b.Refnum = t.Refnum
		Where b.Refnum IN
		(
			Select Refnum
			From Report.ReportGroupRefs
			Where ReportRefnum in
			(
				Select Refnum
				From Report.ReportGroups
				Where Refnum = @ShortListName
			)
			AND Refnum NOT LIKE '%P'
		)
		AND b.CRVSizeGroup = @CRVGroup
		AND t.CompanyName Like @CompanyName
	) 
	GROUP BY NMC


	INSERT @NMC
	SELECT (NMC/100), 'PACE'
	From NERCFactors
	Where Refnum IN
	(
		Select b.Refnum
		From Breaks b
		Inner Join TSort t ON b.Refnum = t.Refnum
		Where b.Refnum IN
		(
			Select Refnum
			From Report.ReportGroupRefs
			Where ReportRefnum in 
			(
				SELECT Refnum
				FROM Report.ReportGroups
				Where Batch = @ShortListName
				And UserGroup = @CRVGroup
			)
		) 
	)

  --SELECT * FROM @NMC
  
  SELECT @GroupWA = (SELECT COUNT(NMC) FROM @NMC WHERE LVL = 'GROUP')
  
  SELECT @CompanyWA = (SELECT COUNT(NMC) FROM @NMC WHERE LVL = 'COMPANY')
  
  SELECT @PaceWA = (SELECT COUNT(NMC) FROM @NMC WHERE LVL = 'PACE')

  --SELECT @GroupWA, @CompanyWA, @PaceWA

  SELECT 'Group Average' [UnitLabel]
		 , [Site]
		 , [Centralized]
		 , [Non-Overhaul Contract]
		 , [Overhaul Contract]
		 , [A&G]
  FROM
  (
	  /* totals by type */
	  Select CASE WHEN PersType = 'Contract' THEN 'Non-Overhaul Contract' 
				  WHEN PersType = 'Central' THEN 'Centralized'
				  WHEN PersType = 'AG' THEN 'A&G'
				  ELSE epmw.PersType 
			 END [PersType]
			, CASE WHEN PersType = 'Contract' 
					  THEN (Sum(epmw.TotalPers) - (SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul)))/@GroupWA
					  ELSE Sum(epmw.TotalPers)/@GroupWA
			  END [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
	
		Select Refnum
		, Weighting
		From Report.ReportGroupRefs
		Where ReportRefnum in 
		(
		  SELECT Refnum
		  FROM Report.ReportGroups
		  Where Batch = @ShortListName
		  AND Refnum like 'CC%'+SUBSTRING(@ShortListName,(LEN(@ShortListName)-1), 2)+@Filter3+SUBSTRING(@CRVGroup,LEN(@CRVGroup),1)
		)
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType <> 'Total'
	  Group By PersType
	  UNION
	  /* overhaul/non-overhaul */
	  Select 'Overhaul Contract' [PersType]
			, (
				(SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul))/@GroupWA
			  ) [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
		Select Refnum
		, Weighting
		From Report.ReportGroupRefs
		Where ReportRefnum in 
		(
		  SELECT Refnum
		  FROM Report.ReportGroups
		  Where Batch = @ShortListName
		  AND Refnum like 'CC%'+SUBSTRING(@ShortListName,(LEN(@ShortListName)-1), 2)+@Filter3+SUBSTRING(@CRVGroup,LEN(@CRVGroup),1)
		)
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType = 'Contract'
	  AND PersType <> 'Total'
  ) b
  PIVOT (MAX(TotalPers) FOR PersType IN ([Site], [Centralized], [Non-Overhaul Contract], [Overhaul Contract], [A&G])) pvt

  UNION ALL

  SELECT 'Pace- setter' [UnitLabel]
		 , [Site]
		 , [Centralized]
		 , [Non-Overhaul Contract]
		 , [Overhaul Contract]
		 , [A&G]
  FROM
  (
	  /* totals by type */
	  Select CASE WHEN PersType = 'Contract' THEN 'Non-Overhaul Contract' 
				  WHEN PersType = 'Central' THEN 'Centralized'
				  WHEN PersType = 'AG' THEN 'A&G'
				  ELSE epmw.PersType 
			 END [PersType]
			, CASE WHEN PersType = 'Contract' 
					  THEN (Sum(epmw.TotalPers) - (SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul)))/@PaceWA
					  ELSE Sum(epmw.TotalPers)/@PaceWA
			  END [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
	
		Select Refnum
		, Weighting
		From Report.ReportGroupRefs
		Where ReportRefnum in 
		(
		  SELECT Refnum
		  FROM Report.ReportGroups
		  Where Batch = @ShortListName
		  And UserGroup = @CRVGroup
		)
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType <> 'Total'
	  Group By PersType
	  UNION
	  /* overhaul/non-overhaul */
	  Select 'Overhaul Contract' [PersType]
			, (
				(SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul))/@PaceWA
			  ) [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
		Select Refnum
		, Weighting
		From Report.ReportGroupRefs
		Where ReportRefnum in 
		(
		  SELECT Refnum
		  FROM Report.ReportGroups
		  Where Batch = @ShortListName
		  And UserGroup = @CRVGroup
		)
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType = 'Contract'
	  AND PersType <> 'Total'
  ) b
  PIVOT (MAX(TotalPers) FOR PersType IN ([Site], [Centralized], [Non-Overhaul Contract], [Overhaul Contract], [A&G])) pvt

  UNION ALL

  SELECT 'Company Average' [UnitLabel]
		 , [Site]
		 , [Centralized]
		 , [Non-Overhaul Contract]
		 , [Overhaul Contract]
		 , [A&G]
  FROM
  (
	  /* totals by type */
	  Select CASE WHEN PersType = 'Contract' THEN 'Non-Overhaul Contract' 
				    WHEN PersType = 'Central' THEN 'Centralized'
				    WHEN PersType = 'AG' THEN 'A&G'
				    ELSE epmw.PersType 
			   END [PersType]
			, CASE WHEN PersType = 'Contract' 
					  THEN (Sum(epmw.TotalPers) - (SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul)))/@CompanyWA
					  ELSE Sum(epmw.TotalPers)/@CompanyWA
			  END [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
		Select b.Refnum
		  From Breaks b
		  Inner Join TSort t ON b.Refnum = t.Refnum
		  Where b.Refnum IN
		  (
			  Select Refnum
			  From Report.ReportGroupRefs
			  Where ReportRefnum in
			  (
				  Select Refnum
				  From Report.ReportGroups
				  Where Refnum = @ShortListName
			  )
			  AND Refnum NOT LIKE '%P'
		  )
		  AND b.CRVSizeGroup = @CRVGroup
		  AND t.CompanyName Like @CompanyName
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType <> 'Total'
	  Group By PersType
	  UNION
	  /* overhaul/non-overhaul */
	  Select 'Overhaul Contract' [PersType]
			, (
				(SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul))/@CompanyWA
			  ) [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
		  Select b.Refnum
		  From Breaks b
		  Inner Join TSort t ON b.Refnum = t.Refnum
		  Where b.Refnum IN
		  (
			  Select Refnum
			  From Report.ReportGroupRefs
			  Where ReportRefnum in
			  (
				  Select Refnum
				  From Report.ReportGroups
				  Where Refnum = @ShortListName
			  )
			  AND Refnum NOT LIKE '%P'
		  )
		  AND b.CRVSizeGroup = @CRVGroup
		  AND t.CompanyName Like @CompanyName
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType = 'Contract'
	  AND PersType <> 'Total'
  ) b
  PIVOT (MAX(TotalPers) FOR PersType IN ([Site], [Centralized], [Non-Overhaul Contract], [Overhaul Contract], [A&G])) pvt

  UNION ALL

  SELECT [UnitLabel]
		 , [Site]
		 , [Centralized]
		 , [Non-Overhaul Contract]
		 , [Overhaul Contract]
		 , [A&G]
  FROM
  (
	  /* totals by type */
	  Select UnitLabel
			 , CASE WHEN PersType = 'Contract' THEN 'Non-Overhaul Contract' 
				    WHEN PersType = 'Central' THEN 'Centralized'
				    WHEN PersType = 'AG' THEN 'A&G'
				    ELSE epmw.PersType 
			   END [PersType]
			, CASE WHEN PersType = 'Contract' 
					  THEN Sum(epmw.TotalPers) - (SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul)) 
					  ELSE Sum(epmw.TotalPers)
			  END [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
		Select b.Refnum
				 , t.UnitLabel
		  From Breaks b
		  Inner Join TSort t ON b.Refnum = t.Refnum
		  Where b.Refnum IN
		  (
			  Select Refnum
			  From Report.ReportGroupRefs
			  Where ReportRefnum in
			  (
				  Select Refnum
				  From Report.ReportGroups
				  Where Refnum = @ShortListName
			  )
			  AND Refnum NOT LIKE '%P'
		  )
		  AND b.CRVSizeGroup = @CRVGroup
		  AND t.CompanyName Like @CompanyName
		  AND t.Refnum <> '%P'
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType <> 'Total'
	  Group By UnitLabel, PersType
	  UNION
	  /* overhaul/non-overhaul */
	  Select UnitLabel
			, 'Overhaul Contract' [PersType]
			, (
				SUM(epmw.OCCLTSA) + SUM(epmw.OCCOverhaul) + SUM(epmw.MPSLTSA) + SUM(epmw.MPSOverhaul)
			  ) [TotalPers]
	  From Report.EffPersMW epmw
	  Inner Join 
	  (
		  Select b.Refnum
				 , t.UnitLabel
		  From Breaks b
		  Inner Join TSort t ON b.Refnum = t.Refnum
		  Where b.Refnum IN
		  (
			  Select Refnum
			  From Report.ReportGroupRefs
			  Where ReportRefnum in
			  (
				  Select Refnum
				  From Report.ReportGroups
				  Where Refnum = @ShortListName
			  )
			  AND Refnum NOT LIKE '%P'
		  )
		  AND b.CRVSizeGroup = @CRVGroup
		  AND t.CompanyName Like @CompanyName
		  AND t.Refnum <> '%P'
	  ) sub on epmw.Refnum = sub.Refnum
	  WHERE PersType = 'Contract'
	  AND PersType <> 'Total'
	  Group By UnitLabel
  ) b
  PIVOT (MAX(TotalPers) FOR PersType IN ([Site], [Centralized], [Non-Overhaul Contract], [Overhaul Contract], [A&G])) pvt



 -- Select *
 -- From Report.EffPersMW epmw
 -- Inner Join 
 -- (
	
	--Select Refnum
	--, Weighting
	--From Report.ReportGroupRefs
	--Where ReportRefnum in 
	--(
	--  SELECT Refnum
	--  FROM Report.ReportGroups
	--  Where Batch = 'Power16'
	--  And UserGroup = 'CG1'
	--)
 -- ) sub on epmw.Refnum = sub.Refnum
 -- WHERE PersType <> 'Total'


 --Declare @CRVGroup VARCHAR(5) = 'CG1'
 --, @Filter3 VARCHAR(50) = 'Cogen'
 --, @ShortListName VARCHAR(50) = 'Power16'

 	 -- SELECT *
	  --FROM Report.ReportGroups
	  --Where Batch = 'Power16'
	  --AND Refnum like 'CC%'+SUBSTRING(@ShortListName,(LEN(@ShortListName)-1), 2)+@Filter3+SUBSTRING(@CRVGroup,LEN(@CRVGroup),1)



  --Select (SUM(NMC)/COUNT(*))/100
  --From NERCFactors
  --Where Refnum IN
  --(
	 -- Select b.Refnum
	 -- From Breaks b
	 -- Inner Join TSort t ON b.Refnum = t.Refnum
	 -- Where b.Refnum IN
	 -- (
		--  Select Refnum
		--  From Report.ReportGroupRefs
		--  Where ReportRefnum in
		--  (
		--	  Select Refnum
		--	  From Report.ReportGroups
		--	  Where Refnum = 'Power16'
		--  )
	 -- )
	 -- AND b.CRVSizeGroup = 'CG1'
	 -- AND t.CompanyName Like 'NUON'
  --) 


