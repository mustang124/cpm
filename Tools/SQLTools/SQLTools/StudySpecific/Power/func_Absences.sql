﻿USE [Power]
GO
/****** Object:  UserDefinedFunction [dbo].[Absences]    Script Date: 11/28/2017 3:34:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************************************************************
Description:      
Return Employee Absences.  This is the second version of this function using a lot
of the same code.

Author: Rodrick Blanton
Date: 28 NOV 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/
CREATE FUNCTION [dbo].[Absences]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30)
)
RETURNS @ReturnTable TABLE (
								[UnitLabel] VARCHAR(100)
								, [Vacation and Holidays] REAL
								, [Other] REAL
								, [SortOrder1] real			-- to be sorted ASC
								, [SortOrder2] real			-- to be sorted DESC
						   )
AS 
BEGIN



	DECLARE
	@crvFuelGroup VARCHAR(50)
	, @GroupWA REAL
	, @CompanyWA REAL
	, @PaceWA REAL
	, @Divisor INT = 100
	, @CompanyName VARCHAR(100)
	, @Multiplier REAL -- Not used in this query
	, @gAverageLabel VARCHAR(50) = 'Group' + CHAR(13) + CHAR(10) + 'Avg'
	, @pAverageLabel VARCHAR(50) = 'Pace' + CHAR(13) + CHAR(10) + 'Setter'
	, @cAverageLabel VARCHAR(50) = 'Avg'
	, @ReportRefNum VARCHAR(50) = REPLACE(@ShortListName + @CRVGroup,' ','')





	DECLARE @RawTable TABLE (
								[UnitLabel] VARCHAR(100)
								, [Vacation and Holidays] REAL
								, [Other] REAL
								, [SortOrder1] real			-- to be sorted ASC
								, [Total] real				-- to be sorted DESC
							)




	DECLARE @RN TABLE
	(
		RefNum VARCHAR(50)
		, LVL VARCHAR(50)
		, UnitLabel VARCHAR(50)
		, Weighting REAL
	)




	/* Get Company Name from ListName */
	SELECT @CompanyName = Element FROM GlobalDB.dbo.SplitString(REPLACE(LTRIM(RTRIM(@ListName)),' ','/'),'/') WHERE ElementID = 2

	SELECT @cAverageLabel = @CompanyName + CHAR(13) + CHAR(10) + @cAverageLabel





	SELECT @crvFuelGroup = (SELECT crvFuelGroup FROM GetCRVFuelGroup(@CRVGroup))


	INSERT INTO @RN
	SELECT RefNum, 'GROUP', '', CompNumPers
	From PersSTCalc
	Where Refnum IN
	(
		Select Refnum
		FROM GetStackBarLists(@ListName, @CRVGroup, 'GROUP', @ReportRefNum, @StudyYear)
	) 
	GROUP BY CompNumPers, Refnum


	INSERT INTO @RN
	SELECT RefNum, 'COMPANY', '', CompNumPers
	From PersSTCalc
	Where Refnum IN
	(
		Select Refnum
		FROM GetStackBarLists(@ListName, @CRVGroup, 'COMPANY', @ReportRefNum, @StudyYear)
	) 
	GROUP BY CompNumPers, Refnum





	INSERT @RawTable
	SELECT TOP 1000 UnitLabel, [Vacation and Holidays], Other, SortOrder, Total FROM (

		SELECT 'OCC Study' [UnitLabel],
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END [Vacation and Holidays],
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END [Other], 
			SortOrder = 1,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END
			+ 
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END [Total]
		FROM TSort t
			INNER JOIN PersSTCalc p ON p.Refnum = t.Refnum AND p.SectionID = 'TOCC' 
			INNER JOIN (SELECT Refnum, SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN OCCAbsPcnt ELSE 0 END) AS VacHol,
				SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN 0 ELSE OCCAbsPcnt END) AS Other FROM AbsenceCalc a GROUP BY Refnum) x ON x.Refnum = t.Refnum
			LEFT JOIN Breaks b on b.Refnum = t.Refnum 
		WHERE t.studyyear = @StudyYear
			AND t.Refnum IN (SELECT Refnum FROM @RN WHERE LVL = 'GROUP')
			AND (
				(b.FuelGroup = @crvFuelGroup) OR 
				(b.FuelGroup = @crvFuelGroup AND b.CombinedCycle = 'N') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec = 'Elec') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec= 'Cogen')
				)

	UNION

		SELECT 'OCC Company',
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END, 
			SortOrder = 2,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END
			+
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END
		FROM TSort t
			INNER JOIN PersSTCalc p ON p.Refnum = t.Refnum AND p.SectionID = 'TOCC' 
			INNER JOIN (SELECT Refnum, SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN OCCAbsPcnt ELSE 0 END) AS VacHol,
				SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN 0 ELSE OCCAbsPcnt END) AS Other FROM AbsenceCalc a GROUP BY Refnum) x ON x.Refnum = t.Refnum
			LEFT JOIN Breaks b on b.Refnum = t.Refnum 
		WHERE t.studyyear = @StudyYear
			AND t.Refnum IN (SELECT Refnum FROM @RN WHERE LVL = 'COMPANY')
			AND (
				(b.FuelGroup = @crvFuelGroup) OR 
				(b.FuelGroup = @crvFuelGroup AND b.CombinedCycle = 'N') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec = 'Elec') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec= 'Cogen')
				)

	UNION

		SELECT 'MPS Study' [UnitLabel],
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END [Vacation and Holidays],
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END [Other], 
			SortOrder = 3,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END
			+
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END [Total]
		FROM TSort t
			INNER JOIN PersSTCalc p ON p.Refnum = t.Refnum AND p.SectionID = 'TMPS' 
			INNER JOIN (SELECT Refnum, SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN OCCAbsPcnt ELSE 0 END) AS VacHol,
				SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN 0 ELSE OCCAbsPcnt END) AS Other FROM AbsenceCalc a GROUP BY Refnum) x ON x.Refnum = t.Refnum
			LEFT JOIN Breaks b on b.Refnum = t.Refnum 
		WHERE t.studyyear = @StudyYear
			AND t.Refnum IN (SELECT Refnum FROM @RN WHERE LVL = 'GROUP')
			AND (
				(b.FuelGroup = @crvFuelGroup) OR 
				(b.FuelGroup = @crvFuelGroup AND b.CombinedCycle = 'N') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec = 'Elec') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec= 'Cogen')
				)

	UNION

		SELECT 'MPS Company',
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END, 
			SortOrder = 4,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) END
			+
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) END
		FROM TSort t
			INNER JOIN PersSTCalc p ON p.Refnum = t.Refnum AND p.SectionID = 'TMPS' 
			INNER JOIN (SELECT Refnum, SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN MPSAbsPcnt ELSE 0 END) AS VacHol,
				SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN 0 ELSE MPSAbsPcnt END) AS Other FROM AbsenceCalc a GROUP BY Refnum) x ON x.Refnum = t.Refnum
			LEFT JOIN Breaks b on b.Refnum = t.Refnum 
		WHERE t.studyyear = @StudyYear
			AND t.Refnum IN (SELECT Refnum FROM @RN WHERE LVL = 'COMPANY')
			AND (
				(b.FuelGroup = @crvFuelGroup) OR 
				(b.FuelGroup = @crvFuelGroup AND b.CombinedCycle = 'N') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec = 'Elec') OR
				(b.FuelGroup = @crvFuelGroup AND b.CogenElec= 'Cogen')
				)

	) c ORDER BY SortOrder


	INSERT @ReturnTable
	SELECT [UnitLabel]
		, [Vacation and Holidays]
		, [Other]
		, SortOrder1 = SortOrder1
		, SortOrder2 = [Total]
	FROM @RawTable 
	ORDER BY SortOrder1 ASC, Total DESC


	RETURN

END


