﻿/***********************************************************************************
Description:      
As of 08 Aug 2017 this trigger is not in use.  The purpose of this table is to
calculate the EGC total from the other EGC parameters.

Author: Rodrick Blanton
Date: 03 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rodrick Blanton
-- Create date: 03 Aug 2017
-- Description:	Creates EGC Total
-- =============================================
ALTER TRIGGER dbo.EGCTotals_SumEGC
   ON  dbo.EGCTotals
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @total INT
	, @sessionID UNIQUEIDENTIFIER

	IF EXISTS(SELECT TOP 1 * FROM inserted)
	BEGIN
		SELECT @total = 
					(
						(
							[EGC_Scrubber]
							+[EGC_PrecBag]
							+[EGC_SCR]
							+[EGC_Frequency50]
							+[EGC_Frequency60]
							+[EGC_Starts]
							+[EGC_SiteEffect]
							+[EGC_OtherSiteCap]
							+[EGC_SteamSales]
							+[EGC_SolidFuel]
							+[EGC_NatGasH2Gas]
							+[EGC_OffGas]
							+[EGC_DieselJet]
							+[EGC_HeavyFuel]
							+[EGC_Age]
							+[EGC_EquivBoiler]
							+[EGC_Supercritical]
							+[EGC_CTGGas]
							+[EGC_SiteFuel]
							+[EGC_ETech]
							+[EGC_FTech]
							+[EGC_AeroTech]
							+[EGC_NumSTs]
							+[EGC_NumGens]
							+[EGC_TotEquip]
							+[EGC_Subcritical]
							+[EGC_MWH]
							+[EGC_Taxes]
							+[EGC_POHours]
							+[EGC_ServiceHours]
							+[EGC_HRSGCap]
							+[EGC_HRSGCapN]
							+[EGC_CTGLiquidFuel]
						) * [EGC_Multiplier]
					) / [EGC_Diviser]
			, @sessionID = [ID]
		FROM inserted


		UPDATE dbo.EGCTotals
		SET EGC_Total = @total
		WHERE ID = @sessionID
	END

END
GO
