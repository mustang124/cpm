﻿USE [Power]
GO
/****** Object:  UserDefinedFunction [dbo].[PerfSum]    Script Date: 8/31/2017 1:27:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER FUNCTION [dbo].[PerfSum]
(	
	@ListName VARCHAR(30),
	@StudyYear VARCHAR(30),
	@CRVGroup VARCHAR(30),
	@RankVariable INT,
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4),
	@Metric BIT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT BarTitle,
		Minimum = CASE WHEN @RankVariable = 1 AND @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Minimum,'BTU','MJ') ELSE Minimum END,
		Q12 = CASE WHEN @RankVariable = 1 AND @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Q12,'BTU','MJ') ELSE Q12 END,
		Q23 = CASE WHEN @RankVariable = 1 AND @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Q23,'BTU','MJ') ELSE Q23 END,
		Q34 = CASE WHEN @RankVariable = 1 AND @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Q34,'BTU','MJ') ELSE Q34 END,
		Maximum = CASE WHEN @RankVariable = 1 AND @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Maximum,'BTU','MJ') ELSE Maximum END,
		Pacesetter = CASE WHEN @RankVariable = 1 AND @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Pacesetter,'BTU','MJ') ELSE Pacesetter END

	FROM (
		SELECT TOP 1000 BarTitle, 
			Minimum = CASE WHEN @RankVariable IN (6, 7, 8, 81, 86, 88) THEN Minimum * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE Minimum END,
			Q12 = CASE WHEN @RankVariable IN (6, 7, 8, 81, 86, 88) THEN Q12 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE Q12 END,
			Q23 = CASE WHEN @RankVariable IN (6, 7, 8, 81, 86, 88) THEN Q23 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE Q23 END,
			Q34 = CASE WHEN @RankVariable IN (6, 7, 8, 81, 86, 88) THEN Q34 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE Q34 END,
			Maximum = CASE WHEN @RankVariable IN (6, 7, 8, 81, 86, 88) THEN Maximum * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE Maximum END,
			Pacesetter = CASE WHEN @RankVariable IN (6, 7, 8, 81, 86, 88) THEN Pacesetter * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE Pacesetter END
		FROM (--top 1000 so we can use ORDER to get the quartile data to the top row

			SELECT BarTitle = CASE @RankVariable
								WHEN 1 THEN 'Heat Rate, Btu/kWh'
								WHEN 6 THEN 'Maintenance Index, ' + RTRIM(@CurrencyCode) + '/MWh'
								WHEN 7 THEN 'Overhaul Index, ' + RTRIM(@CurrencyCode) + '/MWh'
								WHEN 8 THEN 'Non Overhaul Index, ' + RTRIM(@CurrencyCode) + '/MWh'
								WHEN 46 THEN 'Unplanned Commercial Unavailability, %'
								WHEN 47 THEN 'Unplanned Commercial Unavailability Index'
								WHEN 49 THEN 'Planned Commercial Unavailability Index'
								WHEN 55 THEN 'Auxiliaries, %'
								WHEN 78 THEN 'Thermal Efficiency, %'
								WHEN 81 THEN 'LTSA Index, ' + RTRIM(@CurrencyCode) + '/MWh'
								WHEN 86 THEN 'Total Cash Less Fuel & Emissions, ' + RTRIM(@CurrencyCode) + '/MWh'
								WHEN 88 THEN 'Total Cash Less Fuel & Emissions, k ' + RTRIM(@CurrencyCode) + '/MW'
								ELSE 'what the?' END,
				Minimum = [MINIMUM],
				Q12 = [Q12],
				Q23 = [Q23],
				Q34 = [Q34],
				Maximum = [MAXIMUM],
				Pacesetter = [PACESETTER]
			FROM
				(SELECT Label, 
					Value 
				FROM SlideSourceData 
				WHERE Source = @ShortListName
					AND [Type] = 'PerfSum' 
					AND Filter1 = @CRVGroup
					AND Filter2 = @RankVariable) b
				PIVOT (MAX(Value) FOR Label IN ([MINIMUM],[Q12],[Q23],[Q34],[MAXIMUM],[PACESETTER])) pvt

			UNION

			SELECT t.UnitLabel, 
				CASE @RankVariable
					WHEN 1 THEN ISNULL(gs.HeatRate,0)
					WHEN 6 THEN ISNULL(mtc.AnnMaintCostMWH,0)
					WHEN 7 THEN ISNULL(mtc.AnnOHCostMWH,0)
					WHEN 8 THEN ISNULL(mtc.AnnNonOHCostMWH,0)
					WHEN 46 THEN ISNULL(c.PeakUnavail_Unp,0)
					WHEN 47 THEN ISNULL(c.CUTI_Unp,0)
					WHEN 49 THEN ISNULL(c.CUTI_P,0)
					WHEN 55 THEN ISNULL(gtc.InternalLessScrubberPcnt,0)
					WHEN 78 THEN ISNULL(cc.ThermEff,0)
					WHEN 81 THEN ISNULL(mtc.AnnLTSACostMWH,0)
					WHEN 86 THEN ISNULL(o.TotCashLessFuelEm,0)
					WHEN 88 THEN ISNULL(o2.TotCashLessFuelEm,0)
					ELSE 0 END,
				NULL, NULL, NULL, NULL, NULL
			FROM tsort t
				LEFT JOIN CommUnavail c ON c.Refnum = t.Refnum
				LEFT JOIN GenSum gs ON gs.Refnum = t.Refnum
				LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN CogenCalc cc ON cc.Refnum = t.Refnum
				LEFT JOIN Breaks b ON b.Refnum = t.Refnum
				LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum and o.DataType = 'MWH'
				LEFT JOIN OpExCalc o2 ON o2.Refnum = t.Refnum and o2.DataType = 'KW'
				LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
			WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) 
				AND t.StudyYear = @StudyYear
				AND ((b.CRVGroup = @CRVGroup or b.CRVSizeGroup = @CRVGroup))-- OR (t.Refnum LIKE '%G'))
				--AND (c.PeakUnavail_Unp IS NOT NULL OR CUTI_Unp IS NOT NULL OR CUTI_P IS NOT NULL) --not sure about this
		) b
		ORDER BY Q12 DESC, Minimum ASC --because it is the first field that is only populated by the quartile data
	) c

)



