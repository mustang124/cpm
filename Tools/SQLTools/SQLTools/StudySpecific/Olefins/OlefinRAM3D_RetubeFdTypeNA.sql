﻿/***********************************************************************************
Description:      
Column P Script for "Retube$ FdType-NA" worksheet in Olefin Reliability & Maintenance
3D Data Basis - NA Template.xlsm

Author: Rodrick Blanton
Date: 01 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		Jeanne doesn't want this query in a stored proc.  It's used in excel and
		she wants to be able to see it.  Values will remain hardcoded for now so
		they easily be modified by the user.
***********************************************************************************/



SELECT DISTINCT
       fur.Refnum  
       ,t.CoLoc
       ,(SELECT SUM(InflAdjAnn_MaintRetubeTot_Cur) FROM Olefins.dbo.Furnaces WHERE Refnum = fur.Refnum) [InflAdjAnn_MaintRetubeTot_Cur]
       ,t.region
       ,(SELECT SUM(ActCap) FROM Olefins.dbo.Furnaces WHERE Refnum = fur.Refnum) [ActCap]
       ,ISNULL(SUM(furEth.InflAdjAnn_MaintRetubeTot_Cur)/SUM((furEth.actcap/1000)),0) [Ethane]
       ,ISNULL(SUM(furProp.InflAdjAnn_MaintRetubeTot_Cur)/SUM((furProp.actcap/1000)),0) [Propane]
       ,ISNULL(SUM(furBut.InflAdjAnn_MaintRetubeTot_Cur)/SUM((furBut.actcap/1000)),0) [Butane]
       ,ISNULL(SUM(furNap.InflAdjAnn_MaintRetubeTot_Cur)/SUM((furNap.actcap/1000)),0) [Naphtha]
       ,CASE
              WHEN ISNULL(SUM(furG390.InflAdjAnn_MaintRetubeTot_Cur)/SUM((furG390.actcap/1000)) ,0)=0 
              THEN 0
              WHEN ISNULL(SUM(furG390.InflAdjAnn_MaintRetubeTot_Cur)/SUM((furG390.actcap/1000)) ,0)>0 
              THEN ISNULL(SUM(furG390.InflAdjAnn_MaintRetubeTot_Cur)/SUM((furG390.actcap/1000)) ,0)
              ELSE 0
              END [GasOil]
FROM Olefins.dbo.Furnaces fur
FULL JOIN Olefins.dbo.TSort t ON fur.Refnum=t.Refnum
INNER JOIN Olefins.dbo.GENSUM g ON g.Refnum=t.refnum
LEFT JOIN Olefins.dbo.Furnaces furEth ON fur.Refnum = furEth.Refnum 
       AND furEth.FurnFeed = 'Ethane'
          AND ISNULL(furEth.InflAdjAnn_MaintRetubeTot_Cur,0)>0
          AND ISNULL(furEth.actcap,0)>0
LEFT JOIN Olefins.dbo.Furnaces furProp ON fur.Refnum = furProp.Refnum 
       AND furProp.FurnFeed = 'Propane'
          AND ISNULL(furProp.InflAdjAnn_MaintRetubeTot_Cur,0)>0
          AND ISNULL(furProp.actcap,0)>0
LEFT JOIN Olefins.dbo.Furnaces furBut ON fur.Refnum = furBut.Refnum 
       AND furBut.FurnFeed = 'Butane'
          AND ISNULL(furBut.InflAdjAnn_MaintRetubeTot_Cur,0)>0
          AND ISNULL(furBut.actcap,0)>0
LEFT JOIN Olefins.dbo.Furnaces furNap ON fur.Refnum = furNap.Refnum 
       AND furNap.FurnFeed = 'Naphtha'
          AND ISNULL(furNap.InflAdjAnn_MaintRetubeTot_Cur,0)>0
          AND ISNULL(furNap.actcap,0)>0
LEFT JOIN Olefins.dbo.Furnaces furG390 ON fur.Refnum = furG390.Refnum 
       AND (furG390.FurnFeed = 'GasOil>390'
			OR
			furG390.FurnFeed = 'GasOil<390'
			)
          AND ISNULL(furG390.InflAdjAnn_MaintRetubeTot_Cur,0)>0
          AND ISNULL(furG390.actcap,0)>0
WHERE LEN(ISNULL(fur.Refnum,''))>0
AND    fur.Refnum IN 
       (
              '2015PCH089'
              , '2015PCH091'
       ) 
AND fur.Refnum IN 
       (
              SELECT refnum 
              FROM dbo._rl 
              WHERE listname IN 
                           (
                                  '15PCH+LATE'
                           )
       )
AND t.region LIKE 'NA'
GROUP BY
       fur.Refnum
       , fur.FurnFeed
       , t.CoLoc
       , t.Region
ORDER BY Refnum ASC