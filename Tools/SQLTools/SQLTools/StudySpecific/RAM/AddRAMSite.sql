﻿/***********************************************************************************
Description:      
This script is to simplify adding a company to the RAM study.

Author: Rodrick Blanton
Date: 16 Aug 2017
Change History: 
example 01/09/2018:  rvb - xxxx xxxxxxxx xxxxxxxxx

-----------------------------------------------------------------------------------
Notes:
		None
***********************************************************************************/


DECLARE
@StudyYear smallint = NULL
, @CompanyDatasetID int = NULL
, @SiteName nvarchar(50) = '[SITENAME]'
, @SiteID varchar(12) = '[SITEID]'

/*
	These parms go to Answers table and may be left Null 
	and filled later by client
*/
, @Country varchar(255) = NULL
, @LocalCurrency varchar(255) = NULL
, @ExchangeRate real = NULL
, @UserNo int = NULL
, @UserID nvarchar(60) = NULL
, @UnitCntLimit smallint = NULL

/*
	Leave these parms null
*/
, @CompanySID varchar(12) = NULL
, @SiteSID varchar(15) = NULL

SELECT 
@SiteSID = @SiteID + LEFT(RIGHT(CONVERT(VARCHAR(7), GETDATE(), 120), 5),2)


SELECT
@CompanySID
, @CompanyDatasetID
, @SiteName
, @SiteSID
, @SiteID
, @Country
, @LocalCurrency
, @ExchangeRate
, @UserNo
, @UserID
, @UnitCntLimit


--EXEC AddSite 
--	@CompanySID, @CompanyDatasetID, @SiteName, 
--	@SiteSID, @SiteID, @Country, 
--	@LocalCurrency, @ExchangeRate, @UserNo, @UserID,
--	@UnitCntLimit