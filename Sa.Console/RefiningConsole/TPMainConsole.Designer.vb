﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TPMainConsole
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TPMainConsole))
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Consultant's RefNums")
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cboStudy = New System.Windows.Forms.ComboBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cboRefNum = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboConsultant = New System.Windows.Forms.ComboBox()
        Me.btnValidate = New System.Windows.Forms.Button()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.tabSS = New System.Windows.Forms.TabPage()
        Me.btnDirRefresh = New System.Windows.Forms.Button()
        Me.btnSecureSend = New System.Windows.Forms.Button()
        Me.tvwCompCorr2 = New System.Windows.Forms.TreeView()
        Me.tvwCompCorr = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence2 = New System.Windows.Forms.TreeView()
        Me.tvwDrawings2 = New System.Windows.Forms.TreeView()
        Me.tvwClientAttachments2 = New System.Windows.Forms.TreeView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboDir2 = New System.Windows.Forms.ComboBox()
        Me.tvwClientAttachments = New System.Windows.Forms.TreeView()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDir = New System.Windows.Forms.ComboBox()
        Me.tvwDrawings = New System.Windows.Forms.TreeView()
        Me.tvwCorrespondence = New System.Windows.Forms.TreeView()
        Me.tabIssues = New System.Windows.Forms.TabPage()
        Me.txtConsultingOpps = New System.Windows.Forms.TextBox()
        Me.txtContinuingIssues = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.tabCorr = New System.Windows.Forms.TabPage()
        Me.btnOpenCorrFolder = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnOpenValFax = New System.Windows.Forms.Button()
        Me.btnValFax = New System.Windows.Forms.Button()
        Me.btnBuildVRFile = New System.Windows.Forms.Button()
        Me.btnReceiptAck = New System.Windows.Forms.Button()
        Me.lstReturnFiles = New System.Windows.Forms.ListBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.lstVRFiles = New System.Windows.Forms.ListBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.lstVFFiles = New System.Windows.Forms.ListBox()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.lstVFNumbers = New System.Windows.Forms.ListBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.tabCheckList = New System.Windows.Forms.TabPage()
        Me.tvIssues = New System.Windows.Forms.TreeView()
        Me.txtPostedOn = New System.Windows.Forms.TextBox()
        Me.txtPostedBy = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnCancelIssue = New System.Windows.Forms.Button()
        Me.btnUpdateIssue = New System.Windows.Forms.Button()
        Me.lblItemCount = New System.Windows.Forms.Label()
        Me.btnAddIssue = New System.Windows.Forms.Button()
        Me.cboCheckListView = New System.Windows.Forms.ComboBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txtCompletedOn = New System.Windows.Forms.TextBox()
        Me.txtCompletedBy = New System.Windows.Forms.TextBox()
        Me.txtIssueName = New System.Windows.Forms.TextBox()
        Me.txtIssueID = New System.Windows.Forms.TextBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.ValCheckList = New System.Windows.Forms.CheckedListBox()
        Me.tabSummary = New System.Windows.Forms.TabPage()
        Me.btnClientTable = New System.Windows.Forms.Button()
        Me.btnValidatedInput = New System.Windows.Forms.Button()
        Me.btnUnlockPN = New System.Windows.Forms.Button()
        Me.btnCreatePN = New System.Windows.Forms.Button()
        Me.btnSavePN = New System.Windows.Forms.Button()
        Me.lblOSIMUpload = New System.Windows.Forms.Label()
        Me.lblOU = New System.Windows.Forms.Label()
        Me.lblCalcUpload = New System.Windows.Forms.Label()
        Me.lblCU = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtValidationIssues = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.lblLastCalcDate = New System.Windows.Forms.Label()
        Me.lblLastUpload = New System.Windows.Forms.Label()
        Me.lblLastFileSave = New System.Windows.Forms.Label()
        Me.lblValidationStatus = New System.Windows.Forms.Label()
        Me.lblLU = New System.Windows.Forms.Label()
        Me.lblLFS = New System.Windows.Forms.Label()
        Me.lblLCD = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.tabRefinery = New System.Windows.Forms.TabPage()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtSendFormat = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCopyRetPassword = New System.Windows.Forms.Button()
        Me.txtReturnPassword = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblRefCoPhone = New System.Windows.Forms.Label()
        Me.lblRefCoEmail = New System.Windows.Forms.Label()
        Me.lblRefCoName = New System.Windows.Forms.Label()
        Me.btnShowRefCo = New System.Windows.Forms.Button()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.tabCompany = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblPhone = New System.Windows.Forms.Label()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCopyCompanyPassword = New System.Windows.Forms.Button()
        Me.txtCompanyPassword = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ConsoleTabs = New System.Windows.Forms.TabControl()
        Me.tabConsultant = New System.Windows.Forms.TabPage()
        Me.btnRefreshConsultantTab = New System.Windows.Forms.Button()
        Me.tvConsultant = New System.Windows.Forms.TreeView()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cbConsultants = New System.Windows.Forms.ComboBox()
        Me.tabDD = New System.Windows.Forms.TabPage()
        Me.txtMessageFilename = New System.Windows.Forms.TextBox()
        Me.lblMessage = New System.Windows.Forms.Label()
        Me.btnGVClear = New System.Windows.Forms.Button()
        Me.btnFilesSave = New System.Windows.Forms.Button()
        Me.dgFiles = New System.Windows.Forms.DataGridView()
        Me.OriginalFilename = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Filenames = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FileDestination = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.tabClippy = New System.Windows.Forms.TabPage()
        Me.lblError = New System.Windows.Forms.Label()
        Me.chkSQL = New System.Windows.Forms.CheckBox()
        Me.dgClippyResults = New System.Windows.Forms.DataGridView()
        Me.btnClippySearch = New System.Windows.Forms.Button()
        Me.txtClippySearch = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.lblCompany = New System.Windows.Forms.Label()
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.txtVersion = New System.Windows.Forms.TextBox()
        Me.VersionToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ConsoleTimer = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnKillProcesses = New System.Windows.Forms.Button()
        Me.btnValStat = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.btnValDet = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabSS.SuspendLayout()
        Me.tabIssues.SuspendLayout()
        Me.tabCorr.SuspendLayout()
        Me.tabCheckList.SuspendLayout()
        Me.tabSummary.SuspendLayout()
        Me.tabRefinery.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.tabCompany.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ConsoleTabs.SuspendLayout()
        Me.tabConsultant.SuspendLayout()
        Me.tabDD.SuspendLayout()
        CType(Me.dgFiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabClippy.SuspendLayout()
        CType(Me.dgClippyResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboStudy
        '
        Me.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStudy.FormattingEnabled = True
        Me.cboStudy.Location = New System.Drawing.Point(254, 25)
        Me.cboStudy.Name = "cboStudy"
        Me.cboStudy.Size = New System.Drawing.Size(90, 21)
        Me.cboStudy.TabIndex = 1
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(183, 25)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(44, 17)
        Me.Label59.TabIndex = 21
        Me.Label59.Text = "Study"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(183, 57)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(59, 17)
        Me.Label60.TabIndex = 23
        Me.Label60.Text = "RefNum"
        '
        'cboRefNum
        '
        Me.cboRefNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboRefNum.FormattingEnabled = True
        Me.cboRefNum.Location = New System.Drawing.Point(254, 57)
        Me.cboRefNum.Name = "cboRefNum"
        Me.cboRefNum.Size = New System.Drawing.Size(90, 21)
        Me.cboRefNum.TabIndex = 22
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(27, 25)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(135, 96)
        Me.PictureBox1.TabIndex = 24
        Me.PictureBox1.TabStop = False
        '
        'lblWarning
        '
        Me.lblWarning.AutoSize = True
        Me.lblWarning.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(245, 9)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(108, 13)
        Me.lblWarning.TabIndex = 25
        Me.lblWarning.Text = "Not Current Study"
        Me.lblWarning.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(183, 90)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 17)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "Assigned"
        '
        'cboConsultant
        '
        Me.cboConsultant.FormattingEnabled = True
        Me.cboConsultant.Location = New System.Drawing.Point(255, 90)
        Me.cboConsultant.MaxLength = 3
        Me.cboConsultant.Name = "cboConsultant"
        Me.cboConsultant.Size = New System.Drawing.Size(90, 21)
        Me.cboConsultant.TabIndex = 26
        '
        'btnValidate
        '
        Me.btnValidate.Location = New System.Drawing.Point(498, 90)
        Me.btnValidate.Name = "btnValidate"
        Me.btnValidate.Size = New System.Drawing.Size(84, 23)
        Me.btnValidate.TabIndex = 29
        Me.btnValidate.Text = "Validate"
        Me.btnValidate.UseVisualStyleBackColor = True
        '
        'btnHelp
        '
        Me.btnHelp.Location = New System.Drawing.Point(733, 4)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(27, 23)
        Me.btnHelp.TabIndex = 32
        Me.btnHelp.Text = "&?"
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'tabSS
        '
        Me.tabSS.Controls.Add(Me.btnDirRefresh)
        Me.tabSS.Controls.Add(Me.btnSecureSend)
        Me.tabSS.Controls.Add(Me.tvwCompCorr2)
        Me.tabSS.Controls.Add(Me.tvwCompCorr)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence2)
        Me.tabSS.Controls.Add(Me.tvwDrawings2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments2)
        Me.tabSS.Controls.Add(Me.Label13)
        Me.tabSS.Controls.Add(Me.cboDir2)
        Me.tabSS.Controls.Add(Me.tvwClientAttachments)
        Me.tabSS.Controls.Add(Me.Label12)
        Me.tabSS.Controls.Add(Me.cboDir)
        Me.tabSS.Controls.Add(Me.tvwDrawings)
        Me.tabSS.Controls.Add(Me.tvwCorrespondence)
        Me.tabSS.Location = New System.Drawing.Point(4, 22)
        Me.tabSS.Name = "tabSS"
        Me.tabSS.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSS.Size = New System.Drawing.Size(789, 411)
        Me.tabSS.TabIndex = 7
        Me.tabSS.Text = "Secure Send"
        Me.tabSS.UseVisualStyleBackColor = True
        '
        'btnDirRefresh
        '
        Me.btnDirRefresh.Location = New System.Drawing.Point(688, 386)
        Me.btnDirRefresh.Name = "btnDirRefresh"
        Me.btnDirRefresh.Size = New System.Drawing.Size(84, 23)
        Me.btnDirRefresh.TabIndex = 50
        Me.btnDirRefresh.Text = "Refresh"
        Me.btnDirRefresh.UseVisualStyleBackColor = True
        '
        'btnSecureSend
        '
        Me.btnSecureSend.Location = New System.Drawing.Point(598, 386)
        Me.btnSecureSend.Name = "btnSecureSend"
        Me.btnSecureSend.Size = New System.Drawing.Size(84, 23)
        Me.btnSecureSend.TabIndex = 49
        Me.btnSecureSend.Text = "Secure Send"
        Me.btnSecureSend.UseVisualStyleBackColor = True
        '
        'tvwCompCorr2
        '
        Me.tvwCompCorr2.AllowDrop = True
        Me.tvwCompCorr2.CheckBoxes = True
        Me.tvwCompCorr2.ImageIndex = 0
        Me.tvwCompCorr2.ImageList = Me.ImageList1
        Me.tvwCompCorr2.Location = New System.Drawing.Point(395, 34)
        Me.tvwCompCorr2.Name = "tvwCompCorr2"
        Me.tvwCompCorr2.SelectedImageIndex = 0
        Me.tvwCompCorr2.Size = New System.Drawing.Size(377, 346)
        Me.tvwCompCorr2.TabIndex = 48
        Me.tvwCompCorr2.Visible = False
        '
        'tvwCompCorr
        '
        Me.tvwCompCorr.AllowDrop = True
        Me.tvwCompCorr.CheckBoxes = True
        Me.tvwCompCorr.ImageIndex = 0
        Me.tvwCompCorr.ImageList = Me.ImageList1
        Me.tvwCompCorr.Location = New System.Drawing.Point(17, 34)
        Me.tvwCompCorr.Name = "tvwCompCorr"
        Me.tvwCompCorr.SelectedImageIndex = 0
        Me.tvwCompCorr.Size = New System.Drawing.Size(369, 346)
        Me.tvwCompCorr.TabIndex = 47
        Me.tvwCompCorr.Visible = False
        '
        'tvwCorrespondence2
        '
        Me.tvwCorrespondence2.AllowDrop = True
        Me.tvwCorrespondence2.CheckBoxes = True
        Me.tvwCorrespondence2.ImageIndex = 0
        Me.tvwCorrespondence2.ImageList = Me.ImageList1
        Me.tvwCorrespondence2.Location = New System.Drawing.Point(404, 35)
        Me.tvwCorrespondence2.Name = "tvwCorrespondence2"
        Me.tvwCorrespondence2.SelectedImageIndex = 0
        Me.tvwCorrespondence2.Size = New System.Drawing.Size(368, 345)
        Me.tvwCorrespondence2.TabIndex = 46
        '
        'tvwDrawings2
        '
        Me.tvwDrawings2.AllowDrop = True
        Me.tvwDrawings2.CheckBoxes = True
        Me.tvwDrawings2.ImageIndex = 0
        Me.tvwDrawings2.ImageList = Me.ImageList1
        Me.tvwDrawings2.Location = New System.Drawing.Point(404, 34)
        Me.tvwDrawings2.Name = "tvwDrawings2"
        Me.tvwDrawings2.SelectedImageIndex = 0
        Me.tvwDrawings2.Size = New System.Drawing.Size(368, 346)
        Me.tvwDrawings2.TabIndex = 45
        Me.tvwDrawings2.Visible = False
        '
        'tvwClientAttachments2
        '
        Me.tvwClientAttachments2.AllowDrop = True
        Me.tvwClientAttachments2.CheckBoxes = True
        Me.tvwClientAttachments2.ImageIndex = 0
        Me.tvwClientAttachments2.ImageList = Me.ImageList1
        Me.tvwClientAttachments2.Location = New System.Drawing.Point(395, 34)
        Me.tvwClientAttachments2.Name = "tvwClientAttachments2"
        Me.tvwClientAttachments2.SelectedImageIndex = 0
        Me.tvwClientAttachments2.Size = New System.Drawing.Size(377, 346)
        Me.tvwClientAttachments2.TabIndex = 43
        Me.tvwClientAttachments2.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(404, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 17)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Directory:"
        '
        'cboDir2
        '
        Me.cboDir2.FormattingEnabled = True
        Me.cboDir2.Location = New System.Drawing.Point(479, 8)
        Me.cboDir2.Name = "cboDir2"
        Me.cboDir2.Size = New System.Drawing.Size(164, 21)
        Me.cboDir2.TabIndex = 41
        '
        'tvwClientAttachments
        '
        Me.tvwClientAttachments.AllowDrop = True
        Me.tvwClientAttachments.CheckBoxes = True
        Me.tvwClientAttachments.ImageIndex = 0
        Me.tvwClientAttachments.ImageList = Me.ImageList1
        Me.tvwClientAttachments.Location = New System.Drawing.Point(17, 35)
        Me.tvwClientAttachments.Name = "tvwClientAttachments"
        Me.tvwClientAttachments.SelectedImageIndex = 0
        Me.tvwClientAttachments.Size = New System.Drawing.Size(369, 345)
        Me.tvwClientAttachments.TabIndex = 40
        Me.tvwClientAttachments.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(14, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 17)
        Me.Label12.TabIndex = 39
        Me.Label12.Text = "Directory:"
        '
        'cboDir
        '
        Me.cboDir.FormattingEnabled = True
        Me.cboDir.Location = New System.Drawing.Point(89, 7)
        Me.cboDir.Name = "cboDir"
        Me.cboDir.Size = New System.Drawing.Size(164, 21)
        Me.cboDir.TabIndex = 38
        '
        'tvwDrawings
        '
        Me.tvwDrawings.AllowDrop = True
        Me.tvwDrawings.CheckBoxes = True
        Me.tvwDrawings.ImageIndex = 0
        Me.tvwDrawings.ImageList = Me.ImageList1
        Me.tvwDrawings.Location = New System.Drawing.Point(17, 34)
        Me.tvwDrawings.Name = "tvwDrawings"
        Me.tvwDrawings.SelectedImageIndex = 0
        Me.tvwDrawings.Size = New System.Drawing.Size(369, 346)
        Me.tvwDrawings.TabIndex = 36
        Me.tvwDrawings.Visible = False
        '
        'tvwCorrespondence
        '
        Me.tvwCorrespondence.AllowDrop = True
        Me.tvwCorrespondence.CheckBoxes = True
        Me.tvwCorrespondence.ImageIndex = 0
        Me.tvwCorrespondence.ImageList = Me.ImageList1
        Me.tvwCorrespondence.Location = New System.Drawing.Point(17, 35)
        Me.tvwCorrespondence.Name = "tvwCorrespondence"
        Me.tvwCorrespondence.SelectedImageIndex = 0
        Me.tvwCorrespondence.Size = New System.Drawing.Size(369, 345)
        Me.tvwCorrespondence.TabIndex = 0
        '
        'tabIssues
        '
        Me.tabIssues.Controls.Add(Me.txtConsultingOpps)
        Me.tabIssues.Controls.Add(Me.txtContinuingIssues)
        Me.tabIssues.Controls.Add(Me.Label54)
        Me.tabIssues.Controls.Add(Me.Label53)
        Me.tabIssues.Location = New System.Drawing.Point(4, 22)
        Me.tabIssues.Name = "tabIssues"
        Me.tabIssues.Padding = New System.Windows.Forms.Padding(3)
        Me.tabIssues.Size = New System.Drawing.Size(789, 411)
        Me.tabIssues.TabIndex = 6
        Me.tabIssues.Text = "Issues"
        Me.tabIssues.UseVisualStyleBackColor = True
        '
        'txtConsultingOpps
        '
        Me.txtConsultingOpps.Location = New System.Drawing.Point(6, 223)
        Me.txtConsultingOpps.Multiline = True
        Me.txtConsultingOpps.Name = "txtConsultingOpps"
        Me.txtConsultingOpps.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtConsultingOpps.Size = New System.Drawing.Size(769, 178)
        Me.txtConsultingOpps.TabIndex = 15
        '
        'txtContinuingIssues
        '
        Me.txtContinuingIssues.Location = New System.Drawing.Point(8, 30)
        Me.txtContinuingIssues.Multiline = True
        Me.txtContinuingIssues.Name = "txtContinuingIssues"
        Me.txtContinuingIssues.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtContinuingIssues.Size = New System.Drawing.Size(769, 164)
        Me.txtContinuingIssues.TabIndex = 14
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.Location = New System.Drawing.Point(7, 205)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(145, 13)
        Me.Label54.TabIndex = 1
        Me.Label54.Text = "Consulting Opportunities"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(9, 13)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(107, 13)
        Me.Label53.TabIndex = 0
        Me.Label53.Text = "Continuing Issues"
        '
        'tabCorr
        '
        Me.tabCorr.Controls.Add(Me.btnOpenCorrFolder)
        Me.tabCorr.Controls.Add(Me.btnRefresh)
        Me.tabCorr.Controls.Add(Me.btnOpenValFax)
        Me.tabCorr.Controls.Add(Me.btnValFax)
        Me.tabCorr.Controls.Add(Me.btnBuildVRFile)
        Me.tabCorr.Controls.Add(Me.btnReceiptAck)
        Me.tabCorr.Controls.Add(Me.lstReturnFiles)
        Me.tabCorr.Controls.Add(Me.Label58)
        Me.tabCorr.Controls.Add(Me.lstVRFiles)
        Me.tabCorr.Controls.Add(Me.Label57)
        Me.tabCorr.Controls.Add(Me.lstVFFiles)
        Me.tabCorr.Controls.Add(Me.Label56)
        Me.tabCorr.Controls.Add(Me.lstVFNumbers)
        Me.tabCorr.Controls.Add(Me.Label55)
        Me.tabCorr.Location = New System.Drawing.Point(4, 22)
        Me.tabCorr.Name = "tabCorr"
        Me.tabCorr.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCorr.Size = New System.Drawing.Size(789, 411)
        Me.tabCorr.TabIndex = 4
        Me.tabCorr.Text = "Correspondence"
        Me.tabCorr.UseVisualStyleBackColor = True
        '
        'btnOpenCorrFolder
        '
        Me.btnOpenCorrFolder.Location = New System.Drawing.Point(660, 77)
        Me.btnOpenCorrFolder.Name = "btnOpenCorrFolder"
        Me.btnOpenCorrFolder.Size = New System.Drawing.Size(105, 59)
        Me.btnOpenCorrFolder.TabIndex = 13
        Me.btnOpenCorrFolder.Text = "Open Correspondence Folder"
        Me.btnOpenCorrFolder.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(660, 21)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(105, 53)
        Me.btnRefresh.TabIndex = 12
        Me.btnRefresh.Text = "Refresh this Screen"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnOpenValFax
        '
        Me.btnOpenValFax.Location = New System.Drawing.Point(558, 75)
        Me.btnOpenValFax.Name = "btnOpenValFax"
        Me.btnOpenValFax.Size = New System.Drawing.Size(96, 61)
        Me.btnOpenValFax.TabIndex = 11
        Me.btnOpenValFax.Text = "Open Validation Document"
        Me.btnOpenValFax.UseVisualStyleBackColor = True
        '
        'btnValFax
        '
        Me.btnValFax.Location = New System.Drawing.Point(558, 22)
        Me.btnValFax.Name = "btnValFax"
        Me.btnValFax.Size = New System.Drawing.Size(96, 52)
        Me.btnValFax.TabIndex = 10
        Me.btnValFax.Text = "New Validation Document"
        Me.btnValFax.UseVisualStyleBackColor = True
        '
        'btnBuildVRFile
        '
        Me.btnBuildVRFile.Location = New System.Drawing.Point(196, 113)
        Me.btnBuildVRFile.Name = "btnBuildVRFile"
        Me.btnBuildVRFile.Size = New System.Drawing.Size(122, 23)
        Me.btnBuildVRFile.TabIndex = 9
        Me.btnBuildVRFile.Text = "Build a VR File"
        Me.btnBuildVRFile.UseVisualStyleBackColor = True
        '
        'btnReceiptAck
        '
        Me.btnReceiptAck.Location = New System.Drawing.Point(62, 113)
        Me.btnReceiptAck.Name = "btnReceiptAck"
        Me.btnReceiptAck.Size = New System.Drawing.Size(127, 23)
        Me.btnReceiptAck.TabIndex = 8
        Me.btnReceiptAck.Text = "Receipt Acknowledged"
        Me.btnReceiptAck.UseVisualStyleBackColor = True
        '
        'lstReturnFiles
        '
        Me.lstReturnFiles.ColumnWidth = 300
        Me.lstReturnFiles.FormattingEnabled = True
        Me.lstReturnFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstReturnFiles.Location = New System.Drawing.Point(62, 286)
        Me.lstReturnFiles.Name = "lstReturnFiles"
        Me.lstReturnFiles.Size = New System.Drawing.Size(465, 82)
        Me.lstReturnFiles.TabIndex = 7
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(59, 269)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(63, 13)
        Me.Label58.TabIndex = 6
        Me.Label58.Text = "Return Files"
        '
        'lstVRFiles
        '
        Me.lstVRFiles.ColumnWidth = 300
        Me.lstVRFiles.FormattingEnabled = True
        Me.lstVRFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVRFiles.Location = New System.Drawing.Point(62, 173)
        Me.lstVRFiles.Name = "lstVRFiles"
        Me.lstVRFiles.Size = New System.Drawing.Size(465, 82)
        Me.lstVRFiles.TabIndex = 5
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(59, 156)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(142, 13)
        Me.Label57.TabIndex = 4
        Me.Label57.Text = "Validation Reply Files (VR*.*)"
        '
        'lstVFFiles
        '
        Me.lstVFFiles.ColumnWidth = 300
        Me.lstVFFiles.FormattingEnabled = True
        Me.lstVFFiles.Items.AddRange(New Object() {"File Name 1.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/1/2011", "File Name 2.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/2/2011", "Really really long file name that should never be used.doc" & Global.Microsoft.VisualBasic.ChrW(9) & "1/3/2011"})
        Me.lstVFFiles.Location = New System.Drawing.Point(62, 25)
        Me.lstVFFiles.Name = "lstVFFiles"
        Me.lstVFFiles.Size = New System.Drawing.Size(465, 82)
        Me.lstVFFiles.TabIndex = 3
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(59, 7)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(157, 13)
        Me.Label56.TabIndex = 2
        Me.Label56.Text = "Validation Question Files (VQ*.*)"
        '
        'lstVFNumbers
        '
        Me.lstVFNumbers.FormattingEnabled = True
        Me.lstVFNumbers.Location = New System.Drawing.Point(12, 24)
        Me.lstVFNumbers.Name = "lstVFNumbers"
        Me.lstVFNumbers.Size = New System.Drawing.Size(41, 342)
        Me.lstVFNumbers.Sorted = True
        Me.lstVFNumbers.TabIndex = 1
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label55.Location = New System.Drawing.Point(9, 7)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(32, 13)
        Me.Label55.TabIndex = 0
        Me.Label55.Text = "VQ#"
        '
        'tabCheckList
        '
        Me.tabCheckList.Controls.Add(Me.tvIssues)
        Me.tabCheckList.Controls.Add(Me.txtPostedOn)
        Me.tabCheckList.Controls.Add(Me.txtPostedBy)
        Me.tabCheckList.Controls.Add(Me.Label21)
        Me.tabCheckList.Controls.Add(Me.Label22)
        Me.tabCheckList.Controls.Add(Me.btnCancelIssue)
        Me.tabCheckList.Controls.Add(Me.btnUpdateIssue)
        Me.tabCheckList.Controls.Add(Me.lblItemCount)
        Me.tabCheckList.Controls.Add(Me.btnAddIssue)
        Me.tabCheckList.Controls.Add(Me.cboCheckListView)
        Me.tabCheckList.Controls.Add(Me.Label52)
        Me.tabCheckList.Controls.Add(Me.txtCompletedOn)
        Me.tabCheckList.Controls.Add(Me.txtCompletedBy)
        Me.tabCheckList.Controls.Add(Me.txtIssueName)
        Me.tabCheckList.Controls.Add(Me.txtIssueID)
        Me.tabCheckList.Controls.Add(Me.txtDescription)
        Me.tabCheckList.Controls.Add(Me.Label51)
        Me.tabCheckList.Controls.Add(Me.Label50)
        Me.tabCheckList.Controls.Add(Me.Label49)
        Me.tabCheckList.Controls.Add(Me.Label48)
        Me.tabCheckList.Controls.Add(Me.Label47)
        Me.tabCheckList.Controls.Add(Me.ValCheckList)
        Me.tabCheckList.Location = New System.Drawing.Point(4, 22)
        Me.tabCheckList.Name = "tabCheckList"
        Me.tabCheckList.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCheckList.Size = New System.Drawing.Size(789, 411)
        Me.tabCheckList.TabIndex = 3
        Me.tabCheckList.Text = "Checklist"
        Me.tabCheckList.UseVisualStyleBackColor = True
        '
        'tvIssues
        '
        Me.tvIssues.CheckBoxes = True
        Me.tvIssues.Font = New System.Drawing.Font("Courier New", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvIssues.Location = New System.Drawing.Point(3, 54)
        Me.tvIssues.Name = "tvIssues"
        Me.tvIssues.ShowLines = False
        Me.tvIssues.ShowPlusMinus = False
        Me.tvIssues.ShowRootLines = False
        Me.tvIssues.Size = New System.Drawing.Size(293, 365)
        Me.tvIssues.TabIndex = 33
        '
        'txtPostedOn
        '
        Me.txtPostedOn.Location = New System.Drawing.Point(678, 54)
        Me.txtPostedOn.Name = "txtPostedOn"
        Me.txtPostedOn.Size = New System.Drawing.Size(97, 20)
        Me.txtPostedOn.TabIndex = 30
        '
        'txtPostedBy
        '
        Me.txtPostedBy.Location = New System.Drawing.Point(596, 54)
        Me.txtPostedBy.Name = "txtPostedBy"
        Me.txtPostedBy.Size = New System.Drawing.Size(78, 20)
        Me.txtPostedBy.TabIndex = 29
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(677, 77)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(74, 13)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "Completed On"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(593, 77)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(72, 13)
        Me.Label22.TabIndex = 29
        Me.Label22.Text = "Completed By"
        '
        'btnCancelIssue
        '
        Me.btnCancelIssue.Location = New System.Drawing.Point(381, 6)
        Me.btnCancelIssue.Name = "btnCancelIssue"
        Me.btnCancelIssue.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelIssue.TabIndex = 32
        Me.btnCancelIssue.UseVisualStyleBackColor = True
        '
        'btnUpdateIssue
        '
        Me.btnUpdateIssue.Location = New System.Drawing.Point(301, 6)
        Me.btnUpdateIssue.Name = "btnUpdateIssue"
        Me.btnUpdateIssue.Size = New System.Drawing.Size(74, 23)
        Me.btnUpdateIssue.TabIndex = 26
        Me.btnUpdateIssue.Text = "Update"
        Me.btnUpdateIssue.UseVisualStyleBackColor = True
        '
        'lblItemCount
        '
        Me.lblItemCount.AutoSize = True
        Me.lblItemCount.Location = New System.Drawing.Point(6, 36)
        Me.lblItemCount.Name = "lblItemCount"
        Me.lblItemCount.Size = New System.Drawing.Size(63, 13)
        Me.lblItemCount.TabIndex = 26
        Me.lblItemCount.Text = "Issue Name"
        '
        'btnAddIssue
        '
        Me.btnAddIssue.Location = New System.Drawing.Point(194, 6)
        Me.btnAddIssue.Name = "btnAddIssue"
        Me.btnAddIssue.Size = New System.Drawing.Size(102, 23)
        Me.btnAddIssue.TabIndex = 25
        Me.btnAddIssue.Text = "Add New Issue"
        Me.btnAddIssue.UseVisualStyleBackColor = True
        '
        'cboCheckListView
        '
        Me.cboCheckListView.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckListView.FormattingEnabled = True
        Me.cboCheckListView.Items.AddRange(New Object() {"Incomplete", "Complete", "All"})
        Me.cboCheckListView.Location = New System.Drawing.Point(36, 8)
        Me.cboCheckListView.Name = "cboCheckListView"
        Me.cboCheckListView.Size = New System.Drawing.Size(152, 21)
        Me.cboCheckListView.TabIndex = 24
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(3, 11)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(30, 13)
        Me.Label52.TabIndex = 23
        Me.Label52.Text = "View"
        '
        'txtCompletedOn
        '
        Me.txtCompletedOn.Location = New System.Drawing.Point(678, 93)
        Me.txtCompletedOn.Name = "txtCompletedOn"
        Me.txtCompletedOn.Size = New System.Drawing.Size(97, 20)
        Me.txtCompletedOn.TabIndex = 22
        '
        'txtCompletedBy
        '
        Me.txtCompletedBy.Location = New System.Drawing.Point(596, 93)
        Me.txtCompletedBy.Name = "txtCompletedBy"
        Me.txtCompletedBy.Size = New System.Drawing.Size(78, 20)
        Me.txtCompletedBy.TabIndex = 21
        '
        'txtIssueName
        '
        Me.txtIssueName.Location = New System.Drawing.Point(396, 54)
        Me.txtIssueName.Name = "txtIssueName"
        Me.txtIssueName.Size = New System.Drawing.Size(192, 20)
        Me.txtIssueName.TabIndex = 28
        '
        'txtIssueID
        '
        Me.txtIssueID.Location = New System.Drawing.Point(301, 54)
        Me.txtIssueID.Name = "txtIssueID"
        Me.txtIssueID.Size = New System.Drawing.Size(89, 20)
        Me.txtIssueID.TabIndex = 27
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(301, 120)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(480, 270)
        Me.txtDescription.TabIndex = 31
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(675, 37)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(61, 13)
        Me.Label51.TabIndex = 18
        Me.Label51.Text = "Entered On"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(591, 37)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(59, 13)
        Me.Label50.TabIndex = 17
        Me.Label50.Text = "Entered By"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(393, 37)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(63, 13)
        Me.Label49.TabIndex = 16
        Me.Label49.Text = "Issue Name"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(299, 36)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(46, 13)
        Me.Label48.TabIndex = 15
        Me.Label48.Text = "Issue ID"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(300, 100)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(60, 13)
        Me.Label47.TabIndex = 14
        Me.Label47.Text = "Description"
        '
        'ValCheckList
        '
        Me.ValCheckList.FormattingEnabled = True
        Me.ValCheckList.HorizontalScrollbar = True
        Me.ValCheckList.Location = New System.Drawing.Point(0, 52)
        Me.ValCheckList.Name = "ValCheckList"
        Me.ValCheckList.Size = New System.Drawing.Size(294, 364)
        Me.ValCheckList.TabIndex = 0
        '
        'tabSummary
        '
        Me.tabSummary.Controls.Add(Me.btnClientTable)
        Me.tabSummary.Controls.Add(Me.btnValidatedInput)
        Me.tabSummary.Controls.Add(Me.btnUnlockPN)
        Me.tabSummary.Controls.Add(Me.btnCreatePN)
        Me.tabSummary.Controls.Add(Me.btnSavePN)
        Me.tabSummary.Controls.Add(Me.lblOSIMUpload)
        Me.tabSummary.Controls.Add(Me.lblOU)
        Me.tabSummary.Controls.Add(Me.lblCalcUpload)
        Me.tabSummary.Controls.Add(Me.lblCU)
        Me.tabSummary.Controls.Add(Me.Label11)
        Me.tabSummary.Controls.Add(Me.txtValidationIssues)
        Me.tabSummary.Controls.Add(Me.Label46)
        Me.tabSummary.Controls.Add(Me.lblLastCalcDate)
        Me.tabSummary.Controls.Add(Me.lblLastUpload)
        Me.tabSummary.Controls.Add(Me.lblLastFileSave)
        Me.tabSummary.Controls.Add(Me.lblValidationStatus)
        Me.tabSummary.Controls.Add(Me.lblLU)
        Me.tabSummary.Controls.Add(Me.lblLFS)
        Me.tabSummary.Controls.Add(Me.lblLCD)
        Me.tabSummary.Controls.Add(Me.Label35)
        Me.tabSummary.Location = New System.Drawing.Point(4, 22)
        Me.tabSummary.Name = "tabSummary"
        Me.tabSummary.Padding = New System.Windows.Forms.Padding(3)
        Me.tabSummary.Size = New System.Drawing.Size(789, 411)
        Me.tabSummary.TabIndex = 2
        Me.tabSummary.Text = "Summary"
        Me.tabSummary.UseVisualStyleBackColor = True
        '
        'btnClientTable
        '
        Me.btnClientTable.Enabled = False
        Me.btnClientTable.Location = New System.Drawing.Point(653, 35)
        Me.btnClientTable.Margin = New System.Windows.Forms.Padding(1)
        Me.btnClientTable.Name = "btnClientTable"
        Me.btnClientTable.Size = New System.Drawing.Size(132, 32)
        Me.btnClientTable.TabIndex = 35
        Me.btnClientTable.Text = "Client Table"
        Me.btnClientTable.UseVisualStyleBackColor = True
        '
        'btnValidatedInput
        '
        Me.btnValidatedInput.Enabled = False
        Me.btnValidatedInput.Location = New System.Drawing.Point(653, 69)
        Me.btnValidatedInput.Margin = New System.Windows.Forms.Padding(1)
        Me.btnValidatedInput.Name = "btnValidatedInput"
        Me.btnValidatedInput.Size = New System.Drawing.Size(132, 32)
        Me.btnValidatedInput.TabIndex = 2
        Me.btnValidatedInput.Text = "Validated Input"
        Me.btnValidatedInput.UseVisualStyleBackColor = True
        '
        'btnUnlockPN
        '
        Me.btnUnlockPN.Location = New System.Drawing.Point(528, 77)
        Me.btnUnlockPN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnUnlockPN.Name = "btnUnlockPN"
        Me.btnUnlockPN.Size = New System.Drawing.Size(89, 28)
        Me.btnUnlockPN.TabIndex = 34
        Me.btnUnlockPN.Text = "Unlock PN File"
        Me.btnUnlockPN.UseVisualStyleBackColor = True
        Me.btnUnlockPN.Visible = False
        '
        'btnCreatePN
        '
        Me.btnCreatePN.Location = New System.Drawing.Point(528, 121)
        Me.btnCreatePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnCreatePN.Name = "btnCreatePN"
        Me.btnCreatePN.Size = New System.Drawing.Size(89, 28)
        Me.btnCreatePN.TabIndex = 33
        Me.btnCreatePN.Text = "Create PN File"
        Me.btnCreatePN.UseVisualStyleBackColor = True
        '
        'btnSavePN
        '
        Me.btnSavePN.Location = New System.Drawing.Point(251, 122)
        Me.btnSavePN.Margin = New System.Windows.Forms.Padding(1)
        Me.btnSavePN.Name = "btnSavePN"
        Me.btnSavePN.Size = New System.Drawing.Size(74, 28)
        Me.btnSavePN.TabIndex = 13
        Me.btnSavePN.Text = "Save"
        Me.btnSavePN.UseVisualStyleBackColor = True
        '
        'lblOSIMUpload
        '
        Me.lblOSIMUpload.AutoSize = True
        Me.lblOSIMUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOSIMUpload.Location = New System.Drawing.Point(105, 85)
        Me.lblOSIMUpload.Name = "lblOSIMUpload"
        Me.lblOSIMUpload.Size = New System.Drawing.Size(130, 13)
        Me.lblOSIMUpload.TabIndex = 29
        Me.lblOSIMUpload.Text = "6/8/2010 3:51:08 PM"
        Me.lblOSIMUpload.Visible = False
        '
        'lblOU
        '
        Me.lblOU.AutoSize = True
        Me.lblOU.Location = New System.Drawing.Point(9, 85)
        Me.lblOU.Name = "lblOU"
        Me.lblOU.Size = New System.Drawing.Size(74, 13)
        Me.lblOU.TabIndex = 28
        Me.lblOU.Text = "OSIM Upload:"
        Me.lblOU.Visible = False
        '
        'lblCalcUpload
        '
        Me.lblCalcUpload.AutoSize = True
        Me.lblCalcUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalcUpload.Location = New System.Drawing.Point(105, 101)
        Me.lblCalcUpload.Name = "lblCalcUpload"
        Me.lblCalcUpload.Size = New System.Drawing.Size(130, 13)
        Me.lblCalcUpload.TabIndex = 27
        Me.lblCalcUpload.Text = "6/8/2010 3:51:08 PM"
        Me.lblCalcUpload.Visible = False
        '
        'lblCU
        '
        Me.lblCU.AutoSize = True
        Me.lblCU.Location = New System.Drawing.Point(9, 101)
        Me.lblCU.Name = "lblCU"
        Me.lblCU.Size = New System.Drawing.Size(68, 13)
        Me.lblCU.TabIndex = 26
        Me.lblCU.Text = "Calc Upload:"
        Me.lblCU.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(675, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 13)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "Support Files"
        '
        'txtValidationIssues
        '
        Me.txtValidationIssues.Location = New System.Drawing.Point(15, 158)
        Me.txtValidationIssues.Multiline = True
        Me.txtValidationIssues.Name = "txtValidationIssues"
        Me.txtValidationIssues.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtValidationIssues.Size = New System.Drawing.Size(617, 208)
        Me.txtValidationIssues.TabIndex = 12
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(12, 129)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(209, 13)
        Me.Label46.TabIndex = 11
        Me.Label46.Text = "Validation Issues/Presenter's Notes"
        '
        'lblLastCalcDate
        '
        Me.lblLastCalcDate.AutoSize = True
        Me.lblLastCalcDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastCalcDate.Location = New System.Drawing.Point(105, 69)
        Me.lblLastCalcDate.Name = "lblLastCalcDate"
        Me.lblLastCalcDate.Size = New System.Drawing.Size(130, 13)
        Me.lblLastCalcDate.TabIndex = 7
        Me.lblLastCalcDate.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastUpload
        '
        Me.lblLastUpload.AutoSize = True
        Me.lblLastUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastUpload.Location = New System.Drawing.Point(105, 53)
        Me.lblLastUpload.Name = "lblLastUpload"
        Me.lblLastUpload.Size = New System.Drawing.Size(130, 13)
        Me.lblLastUpload.TabIndex = 6
        Me.lblLastUpload.Text = "6/8/2010 3:51:08 PM"
        '
        'lblLastFileSave
        '
        Me.lblLastFileSave.AutoSize = True
        Me.lblLastFileSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastFileSave.Location = New System.Drawing.Point(105, 37)
        Me.lblLastFileSave.Name = "lblLastFileSave"
        Me.lblLastFileSave.Size = New System.Drawing.Size(130, 13)
        Me.lblLastFileSave.TabIndex = 5
        Me.lblLastFileSave.Text = "6/8/2010 3:51:08 PM"
        '
        'lblValidationStatus
        '
        Me.lblValidationStatus.AutoSize = True
        Me.lblValidationStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValidationStatus.Location = New System.Drawing.Point(105, 14)
        Me.lblValidationStatus.Name = "lblValidationStatus"
        Me.lblValidationStatus.Size = New System.Drawing.Size(63, 13)
        Me.lblValidationStatus.TabIndex = 4
        Me.lblValidationStatus.Text = "Validating"
        '
        'lblLU
        '
        Me.lblLU.AutoSize = True
        Me.lblLU.Location = New System.Drawing.Point(9, 53)
        Me.lblLU.Name = "lblLU"
        Me.lblLU.Size = New System.Drawing.Size(67, 13)
        Me.lblLU.TabIndex = 3
        Me.lblLU.Text = "Last Upload:"
        '
        'lblLFS
        '
        Me.lblLFS.AutoSize = True
        Me.lblLFS.Location = New System.Drawing.Point(9, 37)
        Me.lblLFS.Name = "lblLFS"
        Me.lblLFS.Size = New System.Drawing.Size(77, 13)
        Me.lblLFS.TabIndex = 2
        Me.lblLFS.Text = "Last File Save:"
        '
        'lblLCD
        '
        Me.lblLCD.AutoSize = True
        Me.lblLCD.Location = New System.Drawing.Point(9, 69)
        Me.lblLCD.Name = "lblLCD"
        Me.lblLCD.Size = New System.Drawing.Size(80, 13)
        Me.lblLCD.TabIndex = 1
        Me.lblLCD.Text = "Last Calc Date:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(9, 14)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(89, 13)
        Me.Label35.TabIndex = 0
        Me.Label35.Text = "Validation Status:"
        '
        'tabRefinery
        '
        Me.tabRefinery.Controls.Add(Me.Panel4)
        Me.tabRefinery.Controls.Add(Me.Panel3)
        Me.tabRefinery.Location = New System.Drawing.Point(4, 22)
        Me.tabRefinery.Name = "tabRefinery"
        Me.tabRefinery.Padding = New System.Windows.Forms.Padding(3)
        Me.tabRefinery.Size = New System.Drawing.Size(789, 411)
        Me.tabRefinery.TabIndex = 1
        Me.tabRefinery.Text = "Terminal/Pipeline"
        Me.tabRefinery.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.txtSendFormat)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(Me.btnCopyRetPassword)
        Me.Panel4.Controls.Add(Me.txtReturnPassword)
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(3, 257)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(783, 151)
        Me.Panel4.TabIndex = 23
        '
        'txtSendFormat
        '
        Me.txtSendFormat.Location = New System.Drawing.Point(183, 49)
        Me.txtSendFormat.Name = "txtSendFormat"
        Me.txtSendFormat.ReadOnly = True
        Me.txtSendFormat.Size = New System.Drawing.Size(272, 20)
        Me.txtSendFormat.TabIndex = 27
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(162, 13)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Method for Sending Attachments"
        '
        'btnCopyRetPassword
        '
        Me.btnCopyRetPassword.Location = New System.Drawing.Point(461, 7)
        Me.btnCopyRetPassword.Name = "btnCopyRetPassword"
        Me.btnCopyRetPassword.Size = New System.Drawing.Size(43, 23)
        Me.btnCopyRetPassword.TabIndex = 25
        Me.btnCopyRetPassword.Text = "Copy"
        Me.btnCopyRetPassword.UseVisualStyleBackColor = True
        '
        'txtReturnPassword
        '
        Me.txtReturnPassword.Location = New System.Drawing.Point(183, 9)
        Me.txtReturnPassword.Name = "txtReturnPassword"
        Me.txtReturnPassword.ReadOnly = True
        Me.txtReturnPassword.Size = New System.Drawing.Size(272, 20)
        Me.txtReturnPassword.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(11, 12)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(107, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Return File Password"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblRefCoPhone)
        Me.Panel3.Controls.Add(Me.lblRefCoEmail)
        Me.Panel3.Controls.Add(Me.lblRefCoName)
        Me.Panel3.Controls.Add(Me.btnShowRefCo)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label17)
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(783, 254)
        Me.Panel3.TabIndex = 22
        '
        'lblRefCoPhone
        '
        Me.lblRefCoPhone.AutoSize = True
        Me.lblRefCoPhone.Location = New System.Drawing.Point(576, 32)
        Me.lblRefCoPhone.Name = "lblRefCoPhone"
        Me.lblRefCoPhone.Size = New System.Drawing.Size(142, 13)
        Me.lblRefCoPhone.TabIndex = 28
        Me.lblRefCoPhone.Text = "Company Coordinator Phone"
        '
        'lblRefCoEmail
        '
        Me.lblRefCoEmail.AutoSize = True
        Me.lblRefCoEmail.Location = New System.Drawing.Point(330, 32)
        Me.lblRefCoEmail.Name = "lblRefCoEmail"
        Me.lblRefCoEmail.Size = New System.Drawing.Size(136, 13)
        Me.lblRefCoEmail.TabIndex = 27
        Me.lblRefCoEmail.Text = "Company Coordinator Email"
        '
        'lblRefCoName
        '
        Me.lblRefCoName.AutoSize = True
        Me.lblRefCoName.Location = New System.Drawing.Point(139, 32)
        Me.lblRefCoName.Name = "lblRefCoName"
        Me.lblRefCoName.Size = New System.Drawing.Size(139, 13)
        Me.lblRefCoName.TabIndex = 26
        Me.lblRefCoName.Text = "Company Coordinator Name"
        '
        'btnShowRefCo
        '
        Me.btnShowRefCo.Location = New System.Drawing.Point(6, 27)
        Me.btnShowRefCo.Name = "btnShowRefCo"
        Me.btnShowRefCo.Size = New System.Drawing.Size(112, 23)
        Me.btnShowRefCo.TabIndex = 23
        Me.btnShowRefCo.Text = "Data Coordinator"
        Me.btnShowRefCo.UseVisualStyleBackColor = True
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(576, 9)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(67, 13)
        Me.Label16.TabIndex = 22
        Me.Label16.Text = "Telephone"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(330, 9)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(86, 13)
        Me.Label17.TabIndex = 21
        Me.Label17.Text = "Email Address"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(139, 9)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(39, 13)
        Me.Label18.TabIndex = 20
        Me.Label18.Text = "Name"
        '
        'tabCompany
        '
        Me.tabCompany.Controls.Add(Me.Panel2)
        Me.tabCompany.Controls.Add(Me.Panel1)
        Me.tabCompany.Location = New System.Drawing.Point(4, 22)
        Me.tabCompany.Name = "tabCompany"
        Me.tabCompany.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCompany.Size = New System.Drawing.Size(789, 411)
        Me.tabCompany.TabIndex = 0
        Me.tabCompany.Text = "Company"
        Me.tabCompany.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.lblPhone)
        Me.Panel2.Controls.Add(Me.lblEmail)
        Me.Panel2.Controls.Add(Me.lblName)
        Me.Panel2.Controls.Add(Me.btnShow)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(783, 85)
        Me.Panel2.TabIndex = 21
        '
        'lblPhone
        '
        Me.lblPhone.AutoSize = True
        Me.lblPhone.Location = New System.Drawing.Point(635, 32)
        Me.lblPhone.Name = "lblPhone"
        Me.lblPhone.Size = New System.Drawing.Size(142, 13)
        Me.lblPhone.TabIndex = 28
        Me.lblPhone.Text = "Company Coordinator Phone"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(332, 32)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(136, 13)
        Me.lblEmail.TabIndex = 27
        Me.lblEmail.Text = "Company Coordinator Email"
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(139, 32)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(139, 13)
        Me.lblName.TabIndex = 26
        Me.lblName.Text = "Company Coordinator Name"
        '
        'btnShow
        '
        Me.btnShow.Location = New System.Drawing.Point(6, 27)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(116, 23)
        Me.btnShow.TabIndex = 23
        Me.btnShow.Text = "Company Coordinator"
        Me.btnShow.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(635, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 22
        Me.Label6.Text = "Telephone"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(332, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Email Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(139, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = "Name"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnCopyCompanyPassword)
        Me.Panel1.Controls.Add(Me.txtCompanyPassword)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(783, 405)
        Me.Panel1.TabIndex = 20
        '
        'btnCopyCompanyPassword
        '
        Me.btnCopyCompanyPassword.Location = New System.Drawing.Point(460, 114)
        Me.btnCopyCompanyPassword.Name = "btnCopyCompanyPassword"
        Me.btnCopyCompanyPassword.Size = New System.Drawing.Size(43, 23)
        Me.btnCopyCompanyPassword.TabIndex = 26
        Me.btnCopyCompanyPassword.Text = "Copy"
        Me.btnCopyCompanyPassword.UseVisualStyleBackColor = True
        '
        'txtCompanyPassword
        '
        Me.txtCompanyPassword.Location = New System.Drawing.Point(182, 116)
        Me.txtCompanyPassword.Name = "txtCompanyPassword"
        Me.txtCompanyPassword.ReadOnly = True
        Me.txtCompanyPassword.Size = New System.Drawing.Size(272, 20)
        Me.txtCompanyPassword.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 116)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Company Password"
        '
        'ConsoleTabs
        '
        Me.ConsoleTabs.AllowDrop = True
        Me.ConsoleTabs.Controls.Add(Me.tabConsultant)
        Me.ConsoleTabs.Controls.Add(Me.tabCompany)
        Me.ConsoleTabs.Controls.Add(Me.tabRefinery)
        Me.ConsoleTabs.Controls.Add(Me.tabSummary)
        Me.ConsoleTabs.Controls.Add(Me.tabCheckList)
        Me.ConsoleTabs.Controls.Add(Me.tabCorr)
        Me.ConsoleTabs.Controls.Add(Me.tabIssues)
        Me.ConsoleTabs.Controls.Add(Me.tabSS)
        Me.ConsoleTabs.Controls.Add(Me.tabDD)
        Me.ConsoleTabs.Controls.Add(Me.tabClippy)
        Me.ConsoleTabs.Enabled = False
        Me.ConsoleTabs.Location = New System.Drawing.Point(-4, 156)
        Me.ConsoleTabs.Name = "ConsoleTabs"
        Me.ConsoleTabs.SelectedIndex = 0
        Me.ConsoleTabs.Size = New System.Drawing.Size(797, 437)
        Me.ConsoleTabs.TabIndex = 0
        '
        'tabConsultant
        '
        Me.tabConsultant.Controls.Add(Me.btnRefreshConsultantTab)
        Me.tabConsultant.Controls.Add(Me.tvConsultant)
        Me.tabConsultant.Controls.Add(Me.Label20)
        Me.tabConsultant.Controls.Add(Me.cbConsultants)
        Me.tabConsultant.Location = New System.Drawing.Point(4, 22)
        Me.tabConsultant.Name = "tabConsultant"
        Me.tabConsultant.Padding = New System.Windows.Forms.Padding(3)
        Me.tabConsultant.Size = New System.Drawing.Size(789, 411)
        Me.tabConsultant.TabIndex = 10
        Me.tabConsultant.UseVisualStyleBackColor = True
        '
        'btnRefreshConsultantTab
        '
        Me.btnRefreshConsultantTab.Location = New System.Drawing.Point(210, 15)
        Me.btnRefreshConsultantTab.Name = "btnRefreshConsultantTab"
        Me.btnRefreshConsultantTab.Size = New System.Drawing.Size(75, 23)
        Me.btnRefreshConsultantTab.TabIndex = 32
        Me.btnRefreshConsultantTab.Text = "Refresh"
        Me.btnRefreshConsultantTab.UseVisualStyleBackColor = True
        '
        'tvConsultant
        '
        Me.tvConsultant.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvConsultant.Location = New System.Drawing.Point(23, 54)
        Me.tvConsultant.Name = "tvConsultant"
        TreeNode1.Name = "Node0"
        TreeNode1.Text = "Consultant's RefNums"
        Me.tvConsultant.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode1})
        Me.tvConsultant.Size = New System.Drawing.Size(745, 337)
        Me.tvConsultant.TabIndex = 30
        Me.tvConsultant.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(20, 16)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(79, 17)
        Me.Label20.TabIndex = 29
        Me.Label20.Text = "Consultant:"
        '
        'cbConsultants
        '
        Me.cbConsultants.FormattingEnabled = True
        Me.cbConsultants.Location = New System.Drawing.Point(105, 15)
        Me.cbConsultants.Name = "cbConsultants"
        Me.cbConsultants.Size = New System.Drawing.Size(90, 21)
        Me.cbConsultants.TabIndex = 28
        '
        'tabDD
        '
        Me.tabDD.Controls.Add(Me.txtMessageFilename)
        Me.tabDD.Controls.Add(Me.lblMessage)
        Me.tabDD.Controls.Add(Me.btnGVClear)
        Me.tabDD.Controls.Add(Me.btnFilesSave)
        Me.tabDD.Controls.Add(Me.dgFiles)
        Me.tabDD.Location = New System.Drawing.Point(4, 22)
        Me.tabDD.Name = "tabDD"
        Me.tabDD.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDD.Size = New System.Drawing.Size(789, 411)
        Me.tabDD.TabIndex = 8
        Me.tabDD.Text = "Drag and Drop"
        Me.tabDD.UseVisualStyleBackColor = True
        '
        'txtMessageFilename
        '
        Me.txtMessageFilename.Location = New System.Drawing.Point(209, 29)
        Me.txtMessageFilename.Name = "txtMessageFilename"
        Me.txtMessageFilename.Size = New System.Drawing.Size(544, 20)
        Me.txtMessageFilename.TabIndex = 37
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.Location = New System.Drawing.Point(20, 29)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(183, 17)
        Me.lblMessage.TabIndex = 36
        Me.lblMessage.Text = "Filename for Email Text:"
        '
        'btnGVClear
        '
        Me.btnGVClear.Location = New System.Drawing.Point(585, 374)
        Me.btnGVClear.Name = "btnGVClear"
        Me.btnGVClear.Size = New System.Drawing.Size(75, 23)
        Me.btnGVClear.TabIndex = 2
        Me.btnGVClear.Text = "Clear"
        Me.btnGVClear.UseVisualStyleBackColor = True
        '
        'btnFilesSave
        '
        Me.btnFilesSave.Location = New System.Drawing.Point(678, 374)
        Me.btnFilesSave.Name = "btnFilesSave"
        Me.btnFilesSave.Size = New System.Drawing.Size(75, 23)
        Me.btnFilesSave.TabIndex = 1
        Me.btnFilesSave.Text = "Save"
        Me.btnFilesSave.UseVisualStyleBackColor = True
        '
        'dgFiles
        '
        Me.dgFiles.AllowUserToAddRows = False
        Me.dgFiles.AllowUserToDeleteRows = False
        Me.dgFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgFiles.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OriginalFilename, Me.Filenames, Me.FileDestination})
        Me.dgFiles.Location = New System.Drawing.Point(23, 63)
        Me.dgFiles.Name = "dgFiles"
        Me.dgFiles.Size = New System.Drawing.Size(730, 295)
        Me.dgFiles.TabIndex = 0
        '
        'OriginalFilename
        '
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.OriginalFilename.DefaultCellStyle = DataGridViewCellStyle1
        Me.OriginalFilename.HeaderText = "Original Filename"
        Me.OriginalFilename.Name = "OriginalFilename"
        Me.OriginalFilename.ReadOnly = True
        Me.OriginalFilename.Width = 250
        '
        'Filenames
        '
        Me.Filenames.HeaderText = """Save As"" Filename"
        Me.Filenames.Name = "Filenames"
        Me.Filenames.Width = 250
        '
        'FileDestination
        '
        Me.FileDestination.DropDownWidth = 200
        Me.FileDestination.HeaderText = "Save Attachment Location"
        Me.FileDestination.Name = "FileDestination"
        Me.FileDestination.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.FileDestination.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.FileDestination.Width = 185
        '
        'tabClippy
        '
        Me.tabClippy.Controls.Add(Me.lblError)
        Me.tabClippy.Controls.Add(Me.chkSQL)
        Me.tabClippy.Controls.Add(Me.dgClippyResults)
        Me.tabClippy.Controls.Add(Me.btnClippySearch)
        Me.tabClippy.Controls.Add(Me.txtClippySearch)
        Me.tabClippy.Controls.Add(Me.Label25)
        Me.tabClippy.Location = New System.Drawing.Point(4, 22)
        Me.tabClippy.Name = "tabClippy"
        Me.tabClippy.Size = New System.Drawing.Size(789, 411)
        Me.tabClippy.TabIndex = 11
        Me.tabClippy.Text = "Query"
        Me.tabClippy.UseVisualStyleBackColor = True
        '
        'lblError
        '
        Me.lblError.AutoSize = True
        Me.lblError.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblError.ForeColor = System.Drawing.Color.DarkRed
        Me.lblError.Location = New System.Drawing.Point(0, 398)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(0, 13)
        Me.lblError.TabIndex = 45
        '
        'chkSQL
        '
        Me.chkSQL.AutoSize = True
        Me.chkSQL.Checked = True
        Me.chkSQL.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSQL.Location = New System.Drawing.Point(111, 133)
        Me.chkSQL.Name = "chkSQL"
        Me.chkSQL.Size = New System.Drawing.Size(47, 17)
        Me.chkSQL.TabIndex = 44
        Me.chkSQL.Text = "SQL"
        Me.chkSQL.UseVisualStyleBackColor = True
        '
        'dgClippyResults
        '
        Me.dgClippyResults.AllowUserToAddRows = False
        Me.dgClippyResults.AllowUserToDeleteRows = False
        Me.dgClippyResults.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgClippyResults.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgClippyResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgClippyResults.Location = New System.Drawing.Point(21, 169)
        Me.dgClippyResults.Name = "dgClippyResults"
        Me.dgClippyResults.ReadOnly = True
        Me.dgClippyResults.Size = New System.Drawing.Size(751, 222)
        Me.dgClippyResults.TabIndex = 43
        '
        'btnClippySearch
        '
        Me.btnClippySearch.Location = New System.Drawing.Point(21, 129)
        Me.btnClippySearch.Name = "btnClippySearch"
        Me.btnClippySearch.Size = New System.Drawing.Size(69, 23)
        Me.btnClippySearch.TabIndex = 42
        Me.btnClippySearch.Text = "Search"
        Me.btnClippySearch.UseVisualStyleBackColor = True
        '
        'txtClippySearch
        '
        Me.txtClippySearch.Location = New System.Drawing.Point(21, 34)
        Me.txtClippySearch.Multiline = True
        Me.txtClippySearch.Name = "txtClippySearch"
        Me.txtClippySearch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtClippySearch.Size = New System.Drawing.Size(751, 89)
        Me.txtClippySearch.TabIndex = 39
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(8, 11)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(83, 17)
        Me.Label25.TabIndex = 38
        Me.Label25.Text = "SQL Query:"
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(391, 58)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(58, 17)
        Me.lblCompany.TabIndex = 37
        Me.lblCompany.Text = "Pipeline"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboCompany
        '
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(498, 57)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(287, 21)
        Me.cboCompany.Sorted = True
        Me.cboCompany.TabIndex = 38
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.SystemColors.Control
        Me.txtVersion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVersion.Location = New System.Drawing.Point(27, 4)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.ReadOnly = True
        Me.txtVersion.Size = New System.Drawing.Size(100, 15)
        Me.txtVersion.TabIndex = 39
        Me.VersionToolTip.SetToolTip(Me.txtVersion, "This is the new version")
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'ConsoleTimer
        '
        Me.ConsoleTimer.AutoSize = True
        Me.ConsoleTimer.ForeColor = System.Drawing.Color.Green
        Me.ConsoleTimer.Location = New System.Drawing.Point(22, 129)
        Me.ConsoleTimer.Name = "ConsoleTimer"
        Me.ConsoleTimer.Size = New System.Drawing.Size(0, 13)
        Me.ConsoleTimer.TabIndex = 40
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(29, 131)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 13)
        Me.Label14.TabIndex = 41
        '
        'btnKillProcesses
        '
        Me.btnKillProcesses.Location = New System.Drawing.Point(766, 4)
        Me.btnKillProcesses.Name = "btnKillProcesses"
        Me.btnKillProcesses.Size = New System.Drawing.Size(27, 23)
        Me.btnKillProcesses.TabIndex = 42
        Me.btnKillProcesses.Text = "X"
        Me.btnKillProcesses.UseVisualStyleBackColor = True
        '
        'btnValStat
        '
        Me.btnValStat.Location = New System.Drawing.Point(598, 90)
        Me.btnValStat.Name = "btnValStat"
        Me.btnValStat.Size = New System.Drawing.Size(84, 23)
        Me.btnValStat.TabIndex = 43
        Me.btnValStat.Text = "Val Stat"
        Me.btnValStat.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Black
        Me.lblStatus.Location = New System.Drawing.Point(22, 603)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(47, 15)
        Me.lblStatus.TabIndex = 44
        Me.lblStatus.Text = "Ready"
        '
        'btnValDet
        '
        Me.btnValDet.Location = New System.Drawing.Point(701, 90)
        Me.btnValDet.Name = "btnValDet"
        Me.btnValDet.Size = New System.Drawing.Size(84, 23)
        Me.btnValDet.TabIndex = 45
        Me.btnValDet.Text = "Val Det"
        Me.btnValDet.UseVisualStyleBackColor = True
        '
        'TPMainConsole
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 627)
        Me.Controls.Add(Me.btnValDet)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.btnValStat)
        Me.Controls.Add(Me.btnKillProcesses)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.ConsoleTimer)
        Me.Controls.Add(Me.txtVersion)
        Me.Controls.Add(Me.cboCompany)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnValidate)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cboConsultant)
        Me.Controls.Add(Me.lblWarning)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.cboRefNum)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.cboStudy)
        Me.Controls.Add(Me.ConsoleTabs)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "TPMainConsole"
        Me.Text = "Terminals & Pipelines Validation Console"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabSS.ResumeLayout(False)
        Me.tabSS.PerformLayout()
        Me.tabIssues.ResumeLayout(False)
        Me.tabIssues.PerformLayout()
        Me.tabCorr.ResumeLayout(False)
        Me.tabCorr.PerformLayout()
        Me.tabCheckList.ResumeLayout(False)
        Me.tabCheckList.PerformLayout()
        Me.tabSummary.ResumeLayout(False)
        Me.tabSummary.PerformLayout()
        Me.tabRefinery.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.tabCompany.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ConsoleTabs.ResumeLayout(False)
        Me.tabConsultant.ResumeLayout(False)
        Me.tabConsultant.PerformLayout()
        Me.tabDD.ResumeLayout(False)
        Me.tabDD.PerformLayout()
        CType(Me.dgFiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabClippy.ResumeLayout(False)
        Me.tabClippy.PerformLayout()
        CType(Me.dgClippyResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboStudy As System.Windows.Forms.ComboBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents cboRefNum As System.Windows.Forms.ComboBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboConsultant As System.Windows.Forms.ComboBox
    Friend WithEvents btnValidate As System.Windows.Forms.Button
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents tabSS As System.Windows.Forms.TabPage
    Friend WithEvents tabIssues As System.Windows.Forms.TabPage
    Friend WithEvents txtConsultingOpps As System.Windows.Forms.TextBox
    Friend WithEvents txtContinuingIssues As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents tabCorr As System.Windows.Forms.TabPage
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnOpenValFax As System.Windows.Forms.Button
    Friend WithEvents btnValFax As System.Windows.Forms.Button
    Friend WithEvents btnBuildVRFile As System.Windows.Forms.Button
    Friend WithEvents btnReceiptAck As System.Windows.Forms.Button
    Friend WithEvents lstReturnFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents lstVRFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents lstVFFiles As System.Windows.Forms.ListBox
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents lstVFNumbers As System.Windows.Forms.ListBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents tabCheckList As System.Windows.Forms.TabPage
    Friend WithEvents lblItemCount As System.Windows.Forms.Label
    Friend WithEvents btnAddIssue As System.Windows.Forms.Button
    Friend WithEvents cboCheckListView As System.Windows.Forms.ComboBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents txtIssueName As System.Windows.Forms.TextBox
    Friend WithEvents txtIssueID As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents ValCheckList As System.Windows.Forms.CheckedListBox
    Friend WithEvents tabSummary As System.Windows.Forms.TabPage
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtValidationIssues As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents lblLastCalcDate As System.Windows.Forms.Label
    Friend WithEvents lblLastUpload As System.Windows.Forms.Label
    Friend WithEvents lblLastFileSave As System.Windows.Forms.Label
    Friend WithEvents lblLU As System.Windows.Forms.Label
    Friend WithEvents lblLFS As System.Windows.Forms.Label
    Friend WithEvents lblLCD As System.Windows.Forms.Label
    Friend WithEvents tabRefinery As System.Windows.Forms.TabPage
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblRefCoPhone As System.Windows.Forms.Label
    Friend WithEvents lblRefCoEmail As System.Windows.Forms.Label
    Friend WithEvents lblRefCoName As System.Windows.Forms.Label
    Friend WithEvents btnShowRefCo As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents tabCompany As System.Windows.Forms.TabPage
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblPhone As System.Windows.Forms.Label
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents btnShow As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ConsoleTabs As System.Windows.Forms.TabControl
    Friend WithEvents tvwCorrespondence As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboDir As System.Windows.Forms.ComboBox
    Friend WithEvents tvwDrawings As System.Windows.Forms.TreeView
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents txtCompanyPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtReturnPassword As System.Windows.Forms.TextBox
    Friend WithEvents tabDD As System.Windows.Forms.TabPage
    Friend WithEvents dgFiles As System.Windows.Forms.DataGridView
    Friend WithEvents btnGVClear As System.Windows.Forms.Button
    Friend WithEvents btnFilesSave As System.Windows.Forms.Button
    Friend WithEvents txtMessageFilename As System.Windows.Forms.TextBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents OriginalFilename As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Filenames As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FileDestination As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents tabConsultant As System.Windows.Forms.TabPage
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cbConsultants As System.Windows.Forms.ComboBox
    Friend WithEvents tvConsultant As System.Windows.Forms.TreeView
    Friend WithEvents btnCopyRetPassword As System.Windows.Forms.Button
    Friend WithEvents btnCopyCompanyPassword As System.Windows.Forms.Button
    Friend WithEvents tvwClientAttachments As System.Windows.Forms.TreeView
    Friend WithEvents btnOpenCorrFolder As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboDir2 As System.Windows.Forms.ComboBox
    Friend WithEvents tvwClientAttachments2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwCompCorr As System.Windows.Forms.TreeView
    Friend WithEvents tvwCorrespondence2 As System.Windows.Forms.TreeView
    Friend WithEvents tvwDrawings2 As System.Windows.Forms.TreeView
    Friend WithEvents btnUpdateIssue As System.Windows.Forms.Button
    Friend WithEvents btnCancelIssue As System.Windows.Forms.Button
    Friend WithEvents txtPostedOn As System.Windows.Forms.TextBox
    Friend WithEvents txtPostedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtCompletedOn As System.Windows.Forms.TextBox
    Friend WithEvents txtCompletedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents btnSecureSend As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSendFormat As System.Windows.Forms.TextBox
    Friend WithEvents tvIssues As System.Windows.Forms.TreeView
    Friend WithEvents txtVersion As System.Windows.Forms.TextBox
    Friend WithEvents VersionToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents lblOSIMUpload As System.Windows.Forms.Label
    Friend WithEvents lblOU As System.Windows.Forms.Label
    Friend WithEvents lblCalcUpload As System.Windows.Forms.Label
    Friend WithEvents lblCU As System.Windows.Forms.Label
    Friend WithEvents btnDirRefresh As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents btnRefreshConsultantTab As System.Windows.Forms.Button
    Friend WithEvents ConsoleTimer As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents tabClippy As System.Windows.Forms.TabPage
    Friend WithEvents txtClippySearch As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents dgClippyResults As System.Windows.Forms.DataGridView
    Friend WithEvents btnClippySearch As System.Windows.Forms.Button
    Friend WithEvents chkSQL As System.Windows.Forms.CheckBox
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblValidationStatus As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents btnKillProcesses As System.Windows.Forms.Button
    Friend WithEvents btnValStat As System.Windows.Forms.Button
    Friend WithEvents btnSavePN As System.Windows.Forms.Button
    Friend WithEvents btnCreatePN As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents btnUnlockPN As System.Windows.Forms.Button
    Friend WithEvents btnClientTable As System.Windows.Forms.Button
    Friend WithEvents btnValidatedInput As System.Windows.Forms.Button
    Friend WithEvents btnValDet As System.Windows.Forms.Button


End Class
