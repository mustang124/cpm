﻿Imports System.Web
Imports System.Net.Mail
Imports SA.Internal.Console.DataObject
Imports System.Collections
Imports TFSWorkItem


Public Class frmBug

    Private _study As ComboBox.ObjectCollection
    Public Property Study() As ComboBox.ObjectCollection
        Get
            Return _study
        End Get
        Set(ByVal value As ComboBox.ObjectCollection)
            _study = value
        End Set
    End Property
    Private _username As String
    Public Property UserName() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property

    Private _refnum As ComboBox.ObjectCollection
    Public Property RefNum() As ComboBox.ObjectCollection
        Get
            Return _refnum
        End Get
        Set(ByVal value As ComboBox.ObjectCollection)
            _refnum = value
        End Set
    End Property



    Public Sub New(currentstudy As String, study As ComboBox.ObjectCollection, currentrefnum As String, username As String)
        InitializeComponent()
        _study = study
        _username = username
        _refnum = refnum
        cboPriority.SelectedIndex = 0

        For Each i In _study
            cboStudy.Items.Add(i)
        Next
        cboStudy.Text = currentstudy
        cboRefNum.Text = currentrefnum

    End Sub


    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSubmit_Click(sender As System.Object, e As System.EventArgs) Handles btnSubmit.Click
        Me.Cursor = Cursors.WaitCursor
        Dim requestID As String = InsertRequest()

        SendEmail(requestID, _username, cboPriority.Text, txtChangeRequest.Text & vbCrLf & vbCrLf & " -" & UserName & " - " & Now.ToString)
        Me.Cursor = Cursors.Default
    End Sub
    Private Function GetStudyType(sType As String) As DataObject.StudyTypes
        Dim ST As DataObject.StudyTypes
        Select Case sType
            Case "EUR", "PAC", "LUB", "NSA"
                ST = DataObject.StudyTypes.REFINING
        End Select
        Return ST
    End Function
    Private Function InsertRequest() As String
        Dim rt As String = "0"
        Dim ST As DataObject.StudyTypes
        ST = GetStudyType(cboStudy.Text.Substring(0, 3))
        'CHANGE
        'Dim db As New DataObject(ST, "MGV", "solomon2012")
        Dim ps As New List(Of String)
        ps.Add("@Study/" & cboStudy.Text)
        ps.Add("@Priority/" & cboPriority.Text)
        ps.Add("@Request/" & txtChangeRequest.Text)
        ps.Add("@Status/1 - REQUESTED")
        ps.Add("@Requestor/" & _username)

        Dim requestID As Integer
        requestID = db.ExecuteScalar("Console.SubmitRequest", ps)
        InsertTFSWorkItem()
        Return requestID
    End Function

    Private Sub InsertTFSWorkItem()
        Try
            Dim WorkItem As New TFSWorkItem.TFSWorkItem
            WorkItem.Server = "http://dbs7:8080/tfs/CPA_20130129"
            WorkItem.Project = "Sa.Console"
            If cboPriority.Text = "Bug" Then
                WorkItem.WorkItemType = "Bug"
            Else
                WorkItem.WorkItemType = "Task"
            End If

            WorkItem.WorkItemText = txtChangeRequest.Text

            Dim rtn As String = WorkItem.InsertWorkItem()

            If rtn <> "1" Then
                MessageBox.Show(rtn, "Error in TFS")
            End If
        Catch Ex As System.Exception
            MessageBox.Show("There was a problem connecting to TFS - contact Rogge Heflin x1830")
        End Try


    End Sub

    Private Sub SendEmail(RequestID As String, From As String, Priority As String, ChangeRequest As String)

        Dim MailTo As String = My.MySettings.Default.ErrorMailTo

        Dim fromAddress As New MailAddress(From & "@solomononline.com")
        If cboPriority.Text.Substring(0, 1) = "4" Then _username = "jdw@solomononline.com"
        Dim toAddress As New MailAddress(MailTo)
        Dim message As New MailMessage(fromAddress, toAddress)
        Select Case Priority.Substring(0, 1)
            Case "1", "2"
                message.Priority = MailPriority.High
            Case "3"
                message.Priority = MailPriority.Normal

            Case Else
                message.Priority = MailPriority.Low
        End Select

        Dim mailSender As SmtpClient = New SmtpClient("EX1.dc1.solomononline.com", 25)
        With mailSender
            '     CHANGE
            '    .Credentials = New Net.NetworkCredential("greg.vestal", "solomon2012")
            .UseDefaultCredentials = False
            .EnableSsl = True

        End With

        message.Bcc.Add(fromAddress)
        message.Subject = "Change Request #" & RequestID & "  -  Priority: " & Priority
        message.IsBodyHtml = False
        message.Body = ChangeRequest


        Try
            mailSender.Send(message)
            MsgBox("Request Submitted.", MsgBoxStyle.Information)
            Me.Close()
        Catch ex As SystemException
            MsgBox(ex.ToString, MsgBoxStyle.Critical)
            Throw New SystemException(ex.ToString)
        End Try

    End Sub


    Private Sub cboStudy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboStudy.SelectedIndexChanged
        cboRefNum.Items.Clear()
        'CHANGE
        'Dim db As New SA.Internal.Console.DataObject.DataObject(DataObject.StudyTypes.REFINING, "mgv", "solomon2012")
        Dim ds As DataSet
        Dim params As List(Of String)
        params = New List(Of String)
        params.Add("StudyYear/" + "20" + cboStudy.Text.Substring(3, 2))
        params.Add("Study/" + cboStudy.Text.Substring(0, 3))

        ds = db.ExecuteStoredProc("Console." & "GetRefNums", params)
        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
            For Each r In ds.Tables(0).Rows
                cboRefNum.Items.Add(r(0).ToString)
            Next
            cboRefNum.SelectedIndex = 0
        End If
    End Sub
End Class


