﻿
Imports System.Data.SqlClient
Imports System.Configuration
Imports Excel = Microsoft.Office.Interop.Excel
Imports Word = Microsoft.Office.Interop.Word
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.ComponentModel
Imports System.Threading
Imports System.IO
Imports System.Text
Imports System.IO.Compression
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports sa.Internal.Console.DataObject
Imports System.Text.RegularExpressions

Public Class MainConsole
    Public Class WordTemplate
        Public Field As New List(Of String)
        Public RField As New List(Of String)
    End Class

#Region "PRIVATE VARIABLES"

    Dim FirstTime As Boolean = True
    Dim CI As String
    Dim CILastEditedDate As String
    Dim CILastEditedBy As String
    Dim CIIssueDate As String
    Dim CIConsultant As String
    Dim CIDeleted As String
    Dim CIDeletedDate As String
    Dim CIDeletedBy As String
    Dim SavedFile As String = Nothing
    Dim CompleteIssue As String = "N"
    Dim CurrentTab As String
    Dim ValidationIssuesIsDirty As Boolean = False
    Dim Time As Integer = 0
    Dim LastTime As Integer = 0
    Dim CompanyContact As Contacts
    Dim AltCompanyContact As Contacts
    Dim OlefinsContact As Contacts
    Dim RefineryContact As Contacts
    Dim InterimContact As Contacts
    Dim PlantContact As Contacts
    Dim PowerContact As Contacts
    Dim OpsContact As Contacts
    Dim TechContact As Contacts
    Dim PricingContact As Contacts
    Dim DataContact As Contacts
    Dim CompanyPassword As String = ""
    Dim SelectedRow As Integer
    Dim TabDirty(9) As Boolean
    Dim ds As DataSet
    'Dim db As New DataAccess()
    Dim db As DataObject


    Dim sDir(100) As String
    Dim StudyRegion As String = ""
    Dim Study As String = ""

    Dim SANumber As String

    Dim CurrentConsultant As String
    Dim CurrentRefNum As String
    Dim ValidationFile As String
    Dim IDRPath As String
    Dim JustLooking As Boolean = False
    Dim CurrentCompany As String
    Dim Company As String
    Dim Coloc As String
    Dim CompCorrPath As String
    Dim GeneralPath As String
    Dim ProjectPath As String
    Dim TempPath As String
    Dim OldTempPath As String = ""
    Dim TemplatePath As String
    Dim DirResult() As String
    Dim PolishPath As String

    Dim StudyDrive As String

    Dim CurrentPath As String
    Dim RefNum As String
    Dim bJustLooking As Boolean = True
    Dim RefNumPart As String
    Dim LubRefNum As String
    Dim StudySave(12) As String
    Dim RefNumSave(12) As String

    Dim KeyEntered As Boolean = False

    Private mDBConnection As String
    Private mStudyYear As String
    Private mPassword As String
    Private mRefNum As String
    Private mSpawn As Boolean
    Private mStudyType As String
    Private mUserName As String
    Private mReturnPassword As String
    Private VFFiles As New List(Of String)
    Private VRFiles As New List(Of String)
    Private VAFiles As New List(Of String)
    Private RetFiles As New List(Of String)

    Dim VF, VR, VA, Ret, No As Integer
    Private Err As ErrorLog
#End Region

#Region "PUBLIC PROPERTIES"

    Private mVPN As Boolean
    Public Property VPN() As Boolean
        Get
            Return mVPN
        End Get
        Set(ByVal value As Boolean)
            mVPN = value
        End Set
    End Property

    Public Property DBConnection() As String
        Get
            Return mDBConnection
        End Get
        Set(ByVal value As String)
            mDBConnection = value
        End Set
    End Property
    Private mCurrentStudyYear As String
    Public Property CurrentStudyYear() As String
        Get
            Return mCurrentStudyYear
        End Get
        Set(ByVal value As String)
            mCurrentStudyYear = value
        End Set
    End Property
    Public Property StudyYear() As String
        Get
            Return mStudyYear
        End Get
        Set(ByVal value As String)
            mStudyYear = value
        End Set
    End Property

    Public Property UserName() As String

        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set

    End Property
    Public Property ReturnPassword() As String
        Get
            Return mReturnPassword
        End Get
        Set(ByVal value As String)
            mReturnPassword = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property


    Public Property StudyType() As String
        Get
            Return mStudyType
        End Get
        Set(ByVal value As String)
            mStudyType = value
        End Set
    End Property

    Public Property ReferenceNum() As String
        Get
            Return mRefNum
        End Get
        Set(ByVal value As String)
            mRefNum = value
        End Set
    End Property

    Public Property Spawn() As Boolean
        Get
            Return mSpawn
        End Get
        Set(ByVal value As Boolean)
            mSpawn = value
        End Set
    End Property
#End Region

#Region "INITIALIZATION"
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub Initialize()
        Try

            Dim assembly As Assembly = System.Reflection.Assembly.GetExecutingAssembly()
            Dim version As System.Version = assembly.GetName().Version
            txtVersion.Text = String.Format("Version {0}", version)

            'Populate Consultant Tab with login
            SetTabDirty(True)

            'Populate the Form Title
            Me.Text = StudyType & " Console"

            'Fill Study Combo Box
            cboStudy.Items.Add("NSA14")
            cboStudy.Items.Add("EUR14")
            cboStudy.Items.Add("PAC14")
            cboStudy.Items.Add("LUB14")
            cboStudy.Items.Add("NSA12")
            cboStudy.Items.Add("EUR12")
            cboStudy.Items.Add("PAC12")
            cboStudy.Items.Add("LUB12")
            cboStudy.Items.Add("NSA10")
            cboStudy.Items.Add("EUR10")
            cboStudy.Items.Add("PAC10")
            cboStudy.Items.Add("LUB10")

            StudyDrive = "K:\STUDY\REFINING\"
            TempPath = Path.GetTempPath() & "REFINING\"
            If StudyType = "REFINING" Then
                db = New DataObject(DataObject.StudyTypes.REFINING, UserName, Password)
            Else
                db = New DataObject(DataObject.StudyTypes.REFININGDEV, UserName, Password)
            End If

            If File.Exists(TempPath & "settings.txt") Then
                File.Delete(TempPath & "settings.txt")
            End If
            If File.Exists(TempPath & "settings14.txt") Then
                File.Delete(TempPath & "settings14.txt")
            End If

            'Setup Database connection for ErrorLog
            Err = New ErrorLog(StudyType, UserName, Password)

            'Assigned Combo Box
            ds = db.ExecuteStoredProc("Console." & "GetAllConsultants")

            If ds.Tables.Count > 0 Then

                For Each Row In ds.Tables(0).Rows
                    cboConsultant.Items.Add(Row("Consultant").ToString.ToUpper.Trim)
                Next

            End If

            'Fill Directory Drop Down
            cboDir.Items.Add("Refinery Correspondence")
            cboDir2.Items.Add("Refinery Correspondence")
            cboDir.Items.Add("Company Correspondence")
            cboDir.Items.Add("Client Attachments")
            cboDir2.Items.Add("Company Correspondence")
            cboDir2.Items.Add("Client Attachments")
            cboDir.Items.Add("Drawings")
            cboDir2.Items.Add("Drawings")

            cboDir.SelectedIndex = 0
            cboDir2.SelectedIndex = 2

            RemoveTab("tabSS")
            RemoveTab("tabDD")
            RemoveTab("tabTimeGrade")

            If Not CheckRole("QA", UserName) Then
                RemoveTab("tabQA")
            End If


            If CheckRole("FL", UserName) Then btnFLValidation.Visible = True

            If CheckRole("UnlockPN", UserName) Then btnUnlockPN.Visible = True

            If Not CheckRole("DB", UserName) Then RemoveTab("tabClippy")

            'Check for existance of Settings file, if missing, copy default one
            GetSettingsFile()

            'If not spawned off an existing instance of the program, read the settings file and set the drop downs accordingly
            If Not Spawn Then
                ReadLastStudy()
            Else
                If ReferenceNum <> "" Then
                    cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(ReferenceNum, 2) & StudyYear)
                Else
                    cboStudy.SelectedIndex = 0
                End If

            End If

            btnMain.Text = GetMainConsultant()

            SetPaths()
            CheckForCurrentStudyYear()
            ConsoleTabs.SelectedIndex = 1
            lblStatus.ForeColor = Color.DarkGreen
            lblStatus.Text = My.Settings.News

        Catch ex As System.Exception
            Err.WriteLog("Initialize", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

#Region "BUILD TABS"

    Private Sub BuildSummaryTab()


        SetSummaryFileButtons()
        lblLastCalcDate.Text = ""
        lblLastFileSave.Text = ""
        lblLastUpload.Text = ""
        lblRedFlags.Text = ""
        lblBlueFlags.Text = ""
        lblItemsRemaining.Text = ""

        btnPolishRpt.Enabled = False
        If CheckRole("MissingPNs", UserName) Then
            btnCreateMissingPNFiles.Visible = True
        Else
            btnCreateMissingPNFiles.Visible = False
        End If


        Try

            If Dir(RefineryPath & ParseRefNum(RefNum, 0) & ".xls*") = "" Then
                lblValidationStatus.ForeColor = Color.DarkRed
                lblValidationStatus.Text = "Not Received"
            Else
                If File.Exists(RefineryPath & ParseRefNum(RefNum, 0) & ".xls") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(RefNum, 0) & ".xls")
                End If
                If File.Exists(RefineryPath & ParseRefNum(RefNum, 0) & ".xlsm") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(RefNum, 0) & ".xlsm")
                End If
                lblValidationStatus.ForeColor = Color.DarkGreen
                lblValidationStatus.Text = "In Progress"
                End If

                If IsInHolding() Then
                    lblValidationStatus.Text = "In Holding"
                If File.Exists(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xls") Then
                    lblLastFileSave.Text = FileDateTime(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xls")
                End If
                If File.Exists(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xlsm") Then
                    lblLastFileSave.Text = FileDateTime(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xlsm")
                End If

                lblValidationStatus.ForeColor = Color.DarkGoldenrod
                lblValidationStatus.Text = "In Holding"

            End If

            If VChecked(RefNum, "V2") Then
                If File.Exists(RefineryPath & ParseRefNum(RefNum, 0) & ".xls") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(RefNum, 0) & ".xls")
                End If
                If File.Exists(RefineryPath & ParseRefNum(RefNum, 0) & ".xlsm") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(RefNum, 0) & ".xlsm")
                End If



                lblValidationStatus.ForeColor = Color.Black
                lblValidationStatus.Text = "Complete"
            End If

            If VChecked(RefNum, "V1") And Not VChecked(RefNum, "V2") Then
                If File.Exists(RefineryPath & ParseRefNum(RefNum, 0) & ".xls") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(RefNum, 0) & ".xls")
                End If
                If File.Exists(RefineryPath & ParseRefNum(RefNum, 0) & ".xlsm") Then
                    lblLastFileSave.Text = FileDateTime(RefineryPath & ParseRefNum(RefNum, 0) & ".xlsm")
                End If



                lblValidationStatus.ForeColor = Color.DarkGoldenrod
                lblValidationStatus.Text = "Polishing"

            End If

            Dim mRefNum As String = RefNum

            Dim dt As String
            Dim row As DataRow
            Dim params = New List(Of String)

            params.Add("RefNum/" + RefNum)
            params.Add("MessageType/53")
            ds = db.ExecuteStoredProc("Console." & "GetMessageLog", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    dt = row(0).ToString
                    lblLastUpload.Text = dt.ToString
                End If
            End If

            ' Get the last time it was Calced from MessageLog
            'Add Parameters and call GetConsultants
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("MessageType/44")
            ds = db.ExecuteStoredProc("Console." & "GetMessageLog", params)

            If ds.Tables.Count > 0 Then

                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    dt = row(0).ToString
                    lblLastCalcDate.Text = dt.ToString
                End If

            End If
            ' Get the last time it was Calced from MessageLog
            'Add Parameters and call GetConsultants
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)

            ' Load the Red and Blue Flags count labels
            ds = db.ExecuteStoredProc("Console." & "GetFlags", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)

                    If row("DCRedFlags").ToString = "" Then
                        lblRedFlags.Text = "0 Red Flags"
                    Else
                        lblRedFlags.Text = row("DCRedFlags").ToString & " Red Flag"
                        If Convert.ToInt32(row("DCRedFlags")) <> 1 Then
                            lblRedFlags.Text += "s"
                        End If
                    End If

                    If row("DCBlueFlags").ToString = "" Then
                        lblBlueFlags.Text = "0 Blue Flags"
                    Else
                        lblBlueFlags.Text = row("DCBlueFlags").ToString & " Blue Flag"
                        If Convert.ToInt32(row("DCBlueFlags")) <> 1 Then
                            lblBlueFlags.Text += "s"
                        End If
                    End If
                End If
            End If

            RefNum = mRefNum

            ' Load the Uncompleted Checklist Item count label
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Mode/N")
            ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    If Convert.ToInt32(row(0)) = 0 Then
                        lblItemsRemaining.Visible = False
                    End If
                    lblItemsRemaining.Text = row(0).ToString & " Item"
                    If Convert.ToInt32(row(0)) <> 1 Then
                        lblItemsRemaining.Text += "s"
                    End If
                End If
            Else
                lblItemsRemaining.Text = "0 Items"
            End If

        Catch ex As System.Exception
            'MessageBox.Show(ex.Message)
            Err.WriteLog("BuildSummaryTab", ex)
        End Try


        'TODO:  This should be ROLE BASED when we get ACTIVE DIRECTORY

        ' Only the polishers get to push the button to lock the notes and create the Word file.
        ' Not sure if CRT and JDW are supposed to be on this list, but not a big deal.
        ' Added LS 9/9/09 per DEJ so she can go thru and extract the ones that have not been done.
        mUserName = mUserName.ToUpper
        If lblLastCalcDate.Text.Length > 1 Then btnPolishRpt.Enabled = True
    End Sub


    Private Sub BuildNotesTab()

        Dim mRefNum As String = RefNum
        If ParseRefNum(RefNum, 2) = "LUB" Then RefNum = RefNum.Replace("LUB", "LIV")
        Dim params = New List(Of String)
        txtNotes.Text = ""
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)

        btnCreatePN.Text = "Create PN File"
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each row In ds.Tables(0).Rows
                    If ds.Tables(0).Rows.Count = 1 Then
                        txtNotes.Text = row("Note").ToString
                    Else
                        txtNotes.Text = txtNotes.Text & vbCrLf & row("Note").ToString
                    End If

                Next
            Else
                txtNotes.Text = ""
            End If

        End If

       
    End Sub
    Private Sub PresenterNotesReport()

        Dim ps = New List(Of String)
        Dim MissingPNs = New List(Of String)
        Dim InHolding = New List(Of String)
        Dim Polishing = New List(Of String)
        Dim NotReceived = New List(Of String)
        Dim Validating = New List(Of String)
        Dim InProgress = New List(Of String)
        Dim Successful = New List(Of String)
        Dim _path As String = ""
        Dim NewRefNum As String = ""
        Dim NewColoc As String = ""


        Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word files?   This will lock the fields and will have to be unlocked manually.", "Wait", MessageBoxButtons.YesNo)
        If dr = System.Windows.Forms.DialogResult.Yes Then
            Dim NewStudy As String = cboStudy.Text.Substring(0, 3)

            ps.Add("@Study/" & Study)
            ps.Add("@StudyYear/" & StudyYear)
            Dim reader As SqlDataReader = db.ExecuteReader("Console.GetMissingPNs", ps)
            While reader.Read()

                NewRefNum = reader.GetString(0).Trim
                NewColoc = reader.GetString(1).Trim

                Dim _status As String = GetStatus(NewRefNum)

                _path = StudyDrive & StudyYear & "\" & StudyRegion & "\Correspondence\" & NewRefNum & "\"

                If File.Exists(_path & "PN_" & NewColoc & ".doc") = False And _status = "Complete" Then
                    MissingPNs.Add(NewRefNum & "," & NewColoc)
                Else
                    Select Case _status
                        Case "Not Received"
                            NotReceived.Add(NewRefNum & " - " & NewColoc)
                        Case "Polishing"
                            Polishing.Add(NewRefNum & " - " & NewColoc)
                        Case "In Holding"
                            InHolding.Add(NewRefNum & " - " & NewColoc)
                        Case "Validating"
                            Validating.Add(NewRefNum & " - " & NewColoc)
                        Case "In Progress"
                            InProgress.Add(NewRefNum & " - " & NewColoc)
                    End Select
                End If
            End While

            Dim pbForm As PNProgressBar = New PNProgressBar()
            pbForm.Show()
            pbForm.InitializePB(1, MissingPNs.Count, 1)

            For Each RN As String In MissingPNs
                Dim NRN As String = RN.Split(",")(0).ToString()
                Dim NCL As String = RN.Split(",")(1).ToString()


                Try
                    Dim myText As String = "Presenter Notes for" & vbCrLf & NCL & vbCrLf & vbCrLf
                    Dim params = New List(Of String)
                    Dim row As DataRow

                    lblStatus.Text = "Creating Presenter's Word file..."
                    params.Add("RefNum/" + NRN)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            row = ds.Tables(0).Rows(0)
                            myText += row("ValidationNotes").ToString
                        Else
                            myText += ""
                        End If
                    End If
                    Dim strw As New StreamWriter(TempPath & "PN_" & NCL & ".txt")

                    strw.WriteLine(myText)
                    strw.Close()
                    strw.Dispose()
                    Dim doc As String = Utilities.ConvertToDoc(TempPath, TempPath & "PN_" & NCL & ".txt")
                    Dim NewPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Correspondence\" & NRN & "\"
                    File.Move(TempPath & "PN_" & NCL & ".doc", NewPath & "PN_" & NCL & ".doc")
                    File.Delete(TempPath & "PN_" & NCL & ".txt")

                    params.Add("RefNum/" + NRN)
                    params.Add("Notes/" + txtNotes.Text)
                    Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)

                    Dim _status As String = UpdatePN(NRN)
                    If _status = "IDR Notes updated successfully" Then
                        pbForm.PerformStepPB("PN_" & NCL & " - " & NRN)
                        Successful.Add(NRN & " - " & NCL)
                    Else
                        lblStatus.Text = _status
                    End If

                Catch ex As System.Exception
                    lblStatus.Text = "Error: " & ex.Message
                End Try

            Next

            pbForm.Close()
            pbForm.Dispose()
            While Not pbForm.IsDisposed
                Thread.Sleep(100)
            End While

            Dim _stream As New StreamWriter(TempPath & " Presentation Document Log.txt", False)
            _stream.WriteLine(Study & "Presentation Document Log")
            _stream.WriteLine("---------------------------------------------")
            _stream.WriteLine()
            _stream.WriteLine("Presentation Documents Created: " & Successful.Count)
            _stream.WriteLine()
            _stream.WriteLine("Refnums In Holding: " & InHolding.Count)
            For i = 0 To InHolding.Count - 1
                _stream.WriteLine(InHolding(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums Not Received: " & NotReceived.Count)
            For i = 0 To NotReceived.Count - 1
                _stream.WriteLine(NotReceived(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums In Progress: " & InProgress.Count)
            For i = 0 To InProgress.Count - 1
                _stream.WriteLine(InProgress(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums Polishing: " & Polishing.Count)
            For i = 0 To Polishing.Count - 1
                _stream.WriteLine(Polishing(i))
            Next
            _stream.WriteLine()
            _stream.WriteLine("Refnums Validating: " & Validating.Count)
            For i = 0 To Validating.Count - 1
                _stream.WriteLine(Validating(i))
            Next

            _stream.Close()

            Process.Start(TempPath & "Presentation Document Log.txt")
        End If

    End Sub
    Private Sub BuildCheckListTab(strYesNoAll As String) ' Y for completed, N for not completed, A for all

        txtIssueID.Text = ""
        txtIssueName.Text = ""
        txtDescription.Text = ""

        Select Case strYesNoAll
            Case "Complete"
                strYesNoAll = "Y"

            Case "Incomplete"
                strYesNoAll = "N"

            Case Else
                strYesNoAll = "A"
        End Select

        UpdateCheckList(strYesNoAll)

    End Sub

    Private Sub BuildCorrespondenceTab()

        Dim I As Integer
        Dim blnAlready As Boolean
        GetConsultants()
        SetSummaryFileButtons()
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()
        CorrPath = BuildCorrPath(cboRefNum.Text)
        If CorrPath = "" Then Exit Sub
        lstVFNumbers.Items.Clear()
        DirResult = Directory.GetFiles(CorrPath, "VF*")
        If Dir(CorrPath & "*0.xls*") <> "" Then lstVFNumbers.Items.Add("0")
        For Each f In DirResult
            f = Utilities.ExtractFileName(f)
            ' Get the next VF file.
            I = 0

            blnAlready = False
            ' Is it already in the list?
            Do While I < lstVFNumbers.Items.Count
                If lstVFNumbers.Items(I) = IIf(f.Contains("VFFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            ' No, put it there
            If Not blnAlready Then
                lstVFNumbers.Items.Add(IIf(f.Contains("VFFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)))
            End If
            ' Look for next file

        Next

        ' Now look for any orphan VR files
        ' Same logic as above except VR.
        DirResult = Directory.GetFiles(CorrPath, "VR*")
        For Each f In DirResult
            f = Utilities.ExtractFileName(f)
            I = 0
            blnAlready = False
            Do While I < lstVFNumbers.Items.Count
                If lstVFNumbers.Items(I) = IIf(f.Contains("VRFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            If Not blnAlready Then
                lstVFNumbers.Items.Add(IIf(f.Contains("VRFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)))
            End If

        Next

        lstVFNumbers.SelectedIndex = lstVFNumbers.Items.Count - 1
        If lstVFNumbers.SelectedIndex > -1 Then SetCorr()



    End Sub

    Sub SetCorr()

        Dim strDirResult() As String
        Dim choice As String = lstVFNumbers.SelectedItem.ToString
        lstVFFiles.Items.Clear()
        lstVRFiles.Items.Clear()
        lstReturnFiles.Items.Clear()

        strDirResult = Directory.GetFiles(CorrPath, "VF" & choice & "*")

        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = IIf(f.Contains("VFFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                lstVFFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            End If
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VR" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = IIf(f.Contains("VRFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            End If
        Next

        strDirResult = Directory.GetFiles(CorrPath, "VA" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = IIf(f.Contains("VAFL"), "FL" & Mid(f, 5, 1), Mid(f, 3, 1)) Then
                lstVRFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            End If
        Next

        strDirResult = Directory.GetFiles(CorrPath, "*Return" & choice & "*")
        For Each f In strDirResult
            f = Utilities.ExtractFileName(f)
            If lstVFNumbers.SelectedItem = "" Then
                lstReturnFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
            Else
                If (lstVFNumbers.SelectedItem = Mid(f, Len(f) - 4, 1) _
                        Or InStr(1, f, "Return" & lstVFNumbers.SelectedItem) > 0) Then
                    lstReturnFiles.Items.Add(Format(FileDateTime(CorrPath & f).ToString("dd-MMM-yy") & " | " & f))
                End If
            End If
        Next


    End Sub

    Private Sub BuildDirectoriesTab()

        PopulateCompCorrTreeView(tvwCompCorr)
        PopulateCorrespondenceTreeView(tvwCorrespondence)
        PopulateCompCorrTreeView(tvwCompCorr2)
        PopulateCorrespondenceTreeView(tvwCorrespondence2)
        PopulateDrawingTreeView(tvwDrawings)
        PopulateCATreeView(tvwClientAttachments)
        PopulateDrawingTreeView(tvwDrawings2)
        PopulateCATreeView(tvwClientAttachments2)

    End Sub

#End Region

#Region "UTILITIES"

    Private Function IsInHolding() As Boolean
        Try

            If File.Exists(IDRPath & "Holding\" & ParseRefNum(RefNum, 0) & ".xls") Then
                lblValidationStatus.Text = "In Holding"
                Return True
            End If
            Return False

        Catch ex As System.Exception
            Err.WriteLog("IsInHolding", ex)
        End Try


    End Function
    Private Function StripEmail(email As String) As String
        Return email.Substring(email.IndexOf("-") + 2, email.Length - (email.IndexOf("-") + 2))
    End Function

    Private Sub SetTabDirty(status As Boolean)
        For i = 0 To 8
            TabDirty(i) = status
        Next
    End Sub

    Public Sub BuildSupportTab()
        SetSummaryFileButtons()
        BuildGradeGrid()
        ResetGradeTab(1)
    End Sub

    Private Function BuildNewSARecord() As String
        Dim params = New List(Of String)
        params.Add("RefNum/" + RefNum)

        If Not CheckSAMaster(RefNum) Then
            ds = db.ExecuteStoredProc("Console.InsertSARecord", params)
        Else
            MsgBox(CurrentCompany & " already has a record in SAMaster for " & Study & StudyYear)
            Return ""
        End If
        Return ds.Tables(0).Rows(0).Item(0)
    End Function

    Private Function CheckSAMaster(RefNum) As Boolean
        Dim params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console.CheckSANumber", params)
        If ds.Tables.Count = 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function RemovePassword(File As String, dFile As String, password As String, companypassword As String) As Boolean
        Dim success As Boolean = True
        Dim ext As String = GetExtension(File)
        If ext.Length > 2 AndAlso ext.ToUpper.Substring(0, 3) = "XLS" Then
            If File.ToUpper.Contains("RETURN") Or dFile.ToUpper.Contains("RETURN") Then
                success = Utilities.ExcelPassword(File, password, 1)
            Else
                success = Utilities.ExcelPassword(File, companypassword, 1)
            End If
        End If
        If File.Substring(File.Length - 3, 3).ToUpper = "DOC" Or File.Substring(File.Length - 3, 3).ToUpper = "OCX" Then
            success = Utilities.WordPassword(File, companypassword, 1)
        End If
        Return success
    End Function

    Private Function FixFilename(ext As String, strFile As String)
        Dim strNewFilename As String
        Dim NewExt As String = GetExtension(strFile)
        strNewFilename = strFile.Replace(NewExt, ext)
        Return strNewFilename
    End Function

    Private Function GetExtension(strFile As String) As String
        Dim ext As String
        Try
            ext = strFile.Substring(strFile.LastIndexOf(".") + 1, strFile.Length - strFile.LastIndexOf(".") - 1)
        Catch
            ext = ""
        End Try
        Return ext
    End Function

    Function FindModifier(strNextNum As String) As String
        ' Given a number to use in VRx, see if we need to add a letter to it.
        Dim strWork1 As String
        Dim strWork2 As String

        strWork1 = ""
        strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & cboCompany.Text & " Email Text.txt")

        If strWork2 > strWork1 Then
            strWork1 = strWork2
        End If

        For I = 0 To dgFiles.Rows.Count - 1
            If dgFiles.Rows(I).Cells(0).ToString.Substring(dgFiles.Rows(I).Cells(0).ToString.Length - 4, 4).ToUpper = ".XLS" And dgFiles.Rows(I).Cells(0).ToString.ToUpper.Contains("RETURN") Then
                strWork2 = PlugLetterIfNeeded("Return" & strNextNum & ".xls")
            Else
                strWork2 = PlugLetterIfNeeded("VR" & strNextNum & " - " & dgFiles.Rows(I).Cells(0).ToString)
            End If
            If strWork2 > strWork1 Then
                strWork1 = strWork2
            End If
        Next I
        Return strWork1
    End Function

    Public Function PlugLetterIfNeeded(strFileName As String) As String
        Dim strWorkFileName As String
        Dim strDirResult As String
        Dim strCorrPath As String
        Dim strABC(10) As String
        Dim I As Integer
        Dim intInsertPoint As Integer

        strABC(0) = ""
        strABC(1) = "A"
        strABC(2) = "B"
        strABC(3) = "C"
        strABC(4) = "D"
        strABC(5) = "E"
        strABC(6) = "F"
        strABC(7) = "G"
        strABC(8) = "H"
        strABC(9) = "I"

        strCorrPath = CorrPath
        If strFileName.Substring(0, 6).ToUpper = "RETURN" Then
            intInsertPoint = 7
        Else
            intInsertPoint = 3
        End If

        For I = 0 To 9
            strWorkFileName = strFileName.Substring(0, intInsertPoint) & strABC(I) & Mid(strFileName, intInsertPoint + 1, 99)
            strDirResult = Dir(CorrPath & strWorkFileName)
            If strDirResult = "" Then
                Return strABC(I)
                Exit Function
            End If
        Next I

        MsgBox("More than 9 iterations of same file?", vbOKOnly, "Check with Joe Waters (JDW)?")

        PlugLetterIfNeeded = strFileName

    End Function

    Private Sub GetSettingsFile()
        Dim yr As String
        Try

            If Directory.Exists(TempPath) = False Then Directory.CreateDirectory(TempPath)

            If File.Exists(TempPath & "Settings2014.txt") = False Then
                Dim objWriter As New System.IO.StreamWriter(TempPath & "Settings2014.txt", False)

                For i = 10 To 14 Step 2
                    yr = i.ToString
                    If yr.Length = 1 Then yr = "0" & yr

                    objWriter.WriteLine("NSA" & yr)
                    objWriter.WriteLine("100NSA" & yr)
                    objWriter.WriteLine("EUR" & yr)
                    objWriter.WriteLine("102EUR" & yr)
                    objWriter.WriteLine("PAC" & yr)
                    objWriter.WriteLine("117PAC" & yr)
                    objWriter.WriteLine("LUB" & yr)
                    objWriter.WriteLine("101LUB" & yr)
                Next
                objWriter.WriteLine("EUR14")
                objWriter.WriteLine("102EUR14")


                objWriter.Close()
            End If

        Catch ex As System.Exception
            Err.WriteLog("GetSettingsFile", ex)
            cboStudy.SelectedIndex = 1
        End Try

    End Sub

    Private Sub ReadLastStudy()
        Dim rn As String = ""
        Try
            If Not File.Exists(TempPath & "settings2014.txt") Then GetSettingsFile()

            Dim objReader As New IO.StreamReader(TempPath & "settings2014.txt")

            For i = 0 To 11
                StudySave(i) = objReader.ReadLine()
                RefNumSave(i) = objReader.ReadLine()
            Next


            Dim lastStudy As String = objReader.ReadLine()
            cboStudy.SelectedIndex = cboStudy.FindString(lastStudy)
            rn = objReader.ReadLine()
            cboRefNum.Text = rn
            RefNum = rn
            objReader.Close()

            cboRefNum.Text = rn

        Catch ex As System.Exception
            Err.WriteLog("ReadLastStudy: " & rn, ex)
            cboStudy.SelectedIndex = 0
        End Try

    End Sub

    Private Sub SaveLastStudy()
        Try
            If cboRefNum.Text <> "" Then
                'If Directory.Exists(TempPath) = False Then GetSettingsFile()
                Dim objWriter As New System.IO.StreamWriter(TempPath & "settings2014.txt", False)


                For i = 0 To 11
                    objWriter.WriteLine(StudySave(i))
                    objWriter.WriteLine(RefNumSave(i))
                Next


                objWriter.WriteLine(cboStudy.Text)
                objWriter.WriteLine(cboRefNum.Text)
                objWriter.Close()
            End If
        Catch ex As System.Exception
            Err.WriteLog("SaveLastStudy", ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ClearFields()

        lblMaintMgrName.Text = ""
        lblMaintMgrEmail.Text = ""
        lblOpsMgrName.Text = ""
        lblOpsMgrEmail.Text = ""
        lblTechMgrName.Text = ""
        lblTechMgrEmail.Text = ""
        lblRefMgrName.Text = ""
        lblRefMgrEmail.Text = ""
        lblName.Text = ""
        lblEmail.Text = ""
        lblPhone.Text = ""
        lblAltName.Text = ""
        lblAltEmail.Text = ""
        lblAltPhone.Text = ""
        lblRefCoName.Text = ""
        lblRefCoEmail.Text = ""
        lblRefCoPhone.Text = ""
        lblRefAltName.Text = ""
        lblRefAltEmail.Text = ""
        lblRefAltPhone.Text = ""
        lblMaintMgrName.Text = ""
        lblMaintMgrEmail.Text = ""
        lblOpsMgrName.Text = ""
        lblOpsMgrEmail.Text = ""
        lblTechMgrName.Text = ""
        lblTechMgrEmail.Text = ""
        lblRefMgrName.Text = ""
        lblRefMgrEmail.Text = ""
        btnCompanyPW.Text = ""
        lblInterimName.Text = ""
        lblInterimEmail.Text = ""
        lblInterimPhone.Text = ""

        txtDescription.Text = ""
        txtIssueName.Text = ""
        lblBlueFlags.Text = ""
        lblRedFlags.Text = ""
        lblItemCount.Text = ""
        lblItemsRemaining.Text = ""


    End Sub
    Private Sub ValFax_Build(strValFaxTemplate As String)

        ' This routine will create a new validation fax using the
        ' appropriate template, build a file with data to be substituted
        ' into the new document, and call a routine in the document
        ' to do the substitutions.


        Dim strDeadline As String
        Dim datDeadline As Date
        Dim intNumDays As Integer
        Dim strName As String = ""
        Dim timStart As Date
        Dim strProgress As String
        Dim strDirResult As String
        Dim ds As DataSet
        Dim row As DataRow = Nothing
        Dim params As List(Of String)
        Dim TemplateValues As New SA.Console.WordTemplate()
        Dim MSWord As Word.Application
        Dim Coloc As String = ""
        Dim strFaxName As String = ""
        Dim strFaxEmail As String = ""
        Dim Filename As String = ""
        Dim frmTemp = New frmTemplates()

        Try
    
            RefNum = RefNum.Trim
            If File.Exists(TempPath & "ValFax.txt") Then File.Delete(TempPath & "ValFax.txt")
            If strValFaxTemplate = "" Then
                ' Find out which template they want to use.
                strProgress = "cboTemplates.Clear"
                frmTemp.cboTemplates.Items.Clear()

                strDirResult = Dir(TemplatePath & "*.doc*")
                If strDirResult = "" Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)

                    Exit Sub
                End If
                strProgress = "Loading cboTemplates"
                Do While strDirResult <> ""
                    If InStr(1, strDirResult, "Cover") > 0 And strDirResult.Contains(ParseRefNum(RefNum, 2)) Then
                        frmTemp.cboTemplates.Items.Add(strDirResult)
                        'Debug.Print strDirResult, frmTemplates.cboTemplates.ListCount
                    End If
                    strDirResult = Dir()
                Loop

                If frmTemp.cboTemplates.Items.Count < 1 Then
                    MsgBox("No templates found for this study. Please contact Joe Waters (JDW).", vbOKOnly)

                    Exit Sub
                End If

                ' Default to first one.
                frmTemp.cboTemplates.SelectedIndex = 0

                ' If there is only one, don't make them choose,
                ' just go.
                If frmTemp.cboTemplates.Items.Count > 1 Then
                    'MsgBox "Show form modal"
                    ' We use this form for other purposes as well (like selecting which SpecFrac file to open).
                    ' So, reload the caption and label items to the origian "ValFax" values.
                    frmTemp.Text = "Select a Cover Template"

                    ' Ready to show them the form.
                    frmTemp.ShowDialog()
                    If Not frmTemp.OKClick Then Exit Sub

                    ' Load strValFaxTemplate from what they selected on
                    ' frmTemplates. (strTemplateFile is a global variable.)
                    strValFaxTemplate = frmTemp.cboTemplates.SelectedItem
                    'MsgBox "After show form modal"
                Else
                    strValFaxTemplate = frmTemp.cboTemplates.SelectedItem

                End If

            End If

            ' At this point we have the name of the template to use.
            'MsgBox "Prepare new doc"
            'btnValFax.Text = "Preparing New Document"
            strProgress = "Preparing New Document"
            ' Create the file to pass the data in.
            'Open gstrTempFolder & "ValFax.txt" For Output As #1

            Dim objWriter As New System.IO.StreamWriter(TempPath & "ValFax.txt", True)


            ' Todays date
            strProgress = "Printing Long Date"
            objWriter.WriteLine(Format(Now, "dd-MMM-yy"))
            TemplateValues.Field.Add("_TodaysDate")
            TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))



            strProgress = "Getting business days to respond."
            ' Due date
            ' Need a way to make sure we get valid input here.
            If Dir(CorrPath & "vf*") = "" Then
                intNumDays = 10
            Else
                intNumDays = 5
            End If
            strDeadline = InputBox("How many business days do you want to give them to respond?", "Deadline", intNumDays)
            datDeadline = Utilities.ValFaxDateDue(strDeadline)
            objWriter.WriteLine(Format(datDeadline, "dd-MMM-yy"))
            TemplateValues.Field.Add("_DueDate")
            TemplateValues.RField.Add(Format(datDeadline, "dd-MMM-yy"))
            strProgress = "Getting Company name"
            ' Company Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)

            If ds.Tables.Count > 0 Then
                row = ds.Tables(0).Rows(0)
                objWriter.WriteLine(row("Company"))
                TemplateValues.Field.Add("_Company")
                TemplateValues.RField.Add(row("Company").ToString)

                strProgress = "Getting Location"
                ' Refinery Name
                objWriter.WriteLine(row("Location"))
                TemplateValues.Field.Add("_Refinery")
                TemplateValues.RField.Add(row("Location").ToString)
                Coloc = row("Coloc").ToString

            End If

            strProgress = "Getting Contact info"


            ' Contact Name
            If strValFaxTemplate = "LUB14 Cover FLCOMP.doc" Then
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetFuelLubeContactInfo", params)
            Else
                params = New List(Of String)
                params.Add("RefNum/" + RefNum)
                ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
            End If
            If ds.Tables.Count = 0 Then

                If MsgBox("The contact information for this refinery is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then

                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim lfn As String = ""
                    For Each row In ds.Tables(0).Rows
                        If lfn <> row("RefName").ToString Then
                            strFaxName += row("RefName").ToString + IIf(ds.Tables(0).Rows.Count > 1, ", ", "")
                            lfn = row("RefName").ToString
                            strFaxEmail += row("RefEmail").ToString + IIf(ds.Tables(0).Rows.Count > 1, "; ", "")
                        Else
                            strFaxName = strFaxName.Replace(",", "")
                            strFaxEmail = strFaxEmail.Replace(";", "")
                        End If
                    Next
                End If
            End If

            objWriter.WriteLine(strFaxName)

            TemplateValues.Field.Add("_Contact")
            TemplateValues.RField.Add(strFaxName)
            ' Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_EMail")
            TemplateValues.RField.Add(strFaxEmail)
            strFaxEmail = ""
            strFaxName = ""
            strProgress = "Getting consultant info"
            ' Consultant Name
            params = New List(Of String)
            params.Add("Initials/" + mUserName)
            ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)

                    If row("ConsultantName") Is Nothing Or IsDBNull(row("ConsultantName")) Then
                        strName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
                    Else
                        strName = row("ConsultantName").ToString
                    End If
                Else

                    strName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
                End If
            End If

            objWriter.WriteLine(strName)
            TemplateValues.Field.Add("_ConsultantName")
            TemplateValues.RField.Add(strName)
            ' Consultant Initials
            objWriter.WriteLine(mUserName)
            TemplateValues.Field.Add("_Initials")

            TemplateValues.RField.Add(mUserName)
            strProgress = "Path to store it in"
            ' Path to store it in
            ' If it is Trans Pricing or FLCOMP, put it, without VF#, into Refinery Corr folder.
            If strValFaxTemplate.Substring(0, 17) = "Trans Pricing.doc" Then
                Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\" & Coloc & "\"
                ' If it has "Company Wide" in the name, store it in the Company Corr folder
            ElseIf InStr(1, strValFaxTemplate, "Company Wide") > 0 Then
                Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Coloc & "\"
                ' Otherwise -- MOST CASES HERE -- store it in the Refinery Corr folder with VF# on the front.
            Else
                If strValFaxTemplate <> "LUB14 Cover FLCOMP.doc" Then
                    ' Request from DEJ 3/2011 -- if it a Lubes refinery, put "Lube" at end of doc name.
                    Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\VF" & NextVF() & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"
                Else
                    Filename = StudyDrive & "\" & StudyYear & "\" & StudyRegion & "\Correspondence\" & RefNum & "\VFFL" & NextVFFL() & " " & Coloc & IIf(Study = "LUB", " LUBES", "") & ".doc"

                End If

            End If

            objWriter.WriteLine(Filename)


            ' --------- Add new items here 4/20/2007 ---------

            strProgress = "Getting Company Contact info"

            ' Company Contact Name
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("ContactType/Coord")
            params.Add("StudyYear/" + StudyYear)
            ds = db.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)

            If ds.Tables.Count = 0 Then
                If MsgBox("The contact information for this COMPANY is not available. Do you want to continue?", vbYesNo, "Refinery probably has not been uploaded.") = vbNo Then
                    Exit Sub
                Else
                    strFaxName = "NAME UNAVAILABLE"
                    strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
                End If
            Else
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each row In ds.Tables(0).Rows
                        strFaxName = row("FirstName").ToString & " " & row("LastName").ToString
                        strFaxEmail = row("Email").ToString
                    Next
                End If
            End If

            objWriter.WriteLine(strFaxName)
            TemplateValues.Field.Add("_CoCoContact")
            TemplateValues.RField.Add(strFaxName)

            ' Company Contact Email Address
            objWriter.WriteLine(strFaxEmail)
            TemplateValues.Field.Add("_CoCoEMail")
            TemplateValues.RField.Add(strFaxEmail)
            objWriter.Close()
            ' --------- End of new items 4/20/2007 ---------



            ' Make sure the template file is available to copy.
            'On Error GoTo File1NotAvailable
            'intAttr = GetAttr(strTemplatePath & strValFaxTemplate)
            ' Next 2 lines commented 12/1/04 to try new approach
            ' due to problem with compression of network files.
            'SetAttr strTemplatePath & strValFaxTemplate, intAttr
            'On Error GoTo 0

            strProgress = "Copy doc to hard drive"
            ' Bring the doc down to the hard drive.

            If File.Exists(TempPath & strValFaxTemplate) Then

                Kill(TempPath & strValFaxTemplate)

            End If

            If File.Exists(TemplatePath & strValFaxTemplate) Then
                TemplatePath = TemplatePath
            End If

            ' Copy the Template to my work area.
            File.Copy(TemplatePath & strValFaxTemplate, TempPath & strValFaxTemplate, True)
            'varShellReturn = Shell("XCopy """ & strTemplatePath & strValFaxTemplate & """ """ & gstrTempFolder & """ /Y")
            'Stop
            timStart = Now()
            'MsgBox Now()
            Do While File.Exists(TempPath & strValFaxTemplate) = False

                If timStart < DateAdd(DateInterval.Second, (30 * (((1 / 24) / 60) / 60)), Now) Then ' 30 seconds
                    MsgBox("Still waiting for " & strValFaxTemplate & " to be copied.")
                    'Exit Do
                End If
            Loop
            Utilities.WordTemplateReplace(TempPath & strValFaxTemplate, TemplateValues, Filename, 1)


        Catch ex As System.Exception
            MSWord = Nothing
            Err.WriteLog("Validation: " & CurrentRefNum.ToString, ex)
            MessageBox.Show(ex.Message)
        End Try
        MSWord = Nothing
    End Sub
    Function NextFile(findFile As String, findExt As String)

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 20
            strDirResult = Dir(findFile & CStr(I) & "*." & findExt)
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextVF()

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 20
            strDirResult = Dir(CorrPath & "\vf" & CStr(I) & "*.doc")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextVFFL()

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 20
            strDirResult = Dir(CorrPath & "\vffl" & CStr(I) & "*.doc")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function

    Function NextVR()

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 20
            strDirResult = Dir(CorrPath & "\vr" & CStr(I) & "*.doc")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function
    Function NextVRFL()

        Dim strDirResult As String
        Dim I As Integer
        For I = 1 To 20
            strDirResult = Dir(CorrPath & "\vrfl" & CStr(I) & "*.doc")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing
    End Function

    Function NextReturnFile()
        Dim strDirResult As String
        Dim I As Integer
        For I = 0 To 20
            strDirResult = Dir(CorrPath & "*F_Return" & CStr(I) & "*.xls")
            If strDirResult = "" Then
                Return I
                Exit For
            End If
        Next I
        Return Nothing

    End Function


    Function CacheShellIcon(ByVal argPath As String) As String
        Dim mKey As String = Nothing
        ' determine the icon key for the file/folder specified in argPath
        If IO.Directory.Exists(argPath) = True Then
            Return mKey
        ElseIf IO.File.Exists(argPath) = True Then
            mKey = IO.Path.GetExtension(argPath)
        End If
        ' check if an icon for this key has already been added to the collection
        If ImageList1.Images.ContainsKey(mKey) = False Then
            ImageList1.Images.Add(mKey, GetShellIconAsImage(argPath))
            If mKey = "folder" Then ImageList1.Images.Add(mKey & "-open", GetShellOpenIconAsImage(argPath))
        End If
        Return mKey
    End Function

    Function GetStatus(refnum As String)

        refnum = refnum.Trim
        Dim status As String = ""
        Dim path = StudyDrive & StudyYear & "\" & StudyRegion & "\refinery\" & refnum & "\"
        If Dir(path & ParseRefNum(refnum, 0) & ".xls*") = "" Then
            status = "Not Received"
        Else
            status = "In Progress"
        End If

        If Dir(IDRPath & "Holding\" & ParseRefNum(refnum, 0) & ".xls*") <> "" Then
            status = "In Holding"
        End If

        If VChecked(refnum, "V1") Then
            status = "Polishing"
        End If


        If VChecked(refnum, "V2") Then
            status = "Complete"
        End If

        Return status

    End Function

    

    Private Sub BuildVR()
        Dim strDirResult As String
        Dim strTextForFile As String

        strDirResult = Dir(CorrPath & "VR" & SelectedRow & ".txt")
        If strDirResult <> "" Then
            MsgBox("There already is a VR" & Me.SelectedRow & " file.")
            Exit Sub
        End If

        strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        If strTextForFile <> "" Then
            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VR" & SelectedRow & ".txt", True)
            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strTextForFile)
            objWriter.Close()
            MsgBox("VR" & SelectedRow & ".txt has been built.")
        Else
            MsgBox("VR file build was cancelled.")
        End If
    End Sub
    Private Sub btnBuildVRFile_Click_1(sender As Object, e As EventArgs)
        BuildVR()
    End Sub
    

    Function DatePulledFromVAFile(ByVal strDirAndFileName As String) As Date

        Dim strWork As String = ""
        ' Read the second line of the VA file. Get the date from there.
        Dim objReader As New System.IO.StreamReader(strDirAndFileName, True)

        ' Read and toss the first line
        strWork = objReader.ReadLine()
        ' Read the date-time line
        strWork = objReader.ReadLine()

        DatePulledFromVAFile = strWork

    End Function

    Sub ResetGradeTab(intSectionORVersion As Integer)

        Dim intVersion As Integer
        Dim params As New List(Of String)
        Dim rdr As SqlDataReader


        If intSectionORVersion = 1 Then
            ' Load the data by Section.
            ' First determined which version we need to get.
            params.Add("RefNum/" & ParseRefNum(RefNum, 0))

            rdr = db.ExecuteReader("Console.GetSectionGrade", params)

            If rdr.Read() Then
                intVersion = rdr.GetInt16(0)
            End If
            rdr.Close()
            params.Clear()
            params.Add("RefNum/" & ParseRefNum(RefNum, 0))
            params.Add("Version/" & intVersion)

            ds = db.ExecuteStoredProc("Console.GetGradesBySection", params)

            dgGrade.DataSource = ds.Tables(0)

        Else
            ' Load the data by Version.

            params.Clear()
            params.Add("RefNum/" & RefNum)
            params.Add("Version/" & intVersion)

            ds = db.ExecuteStoredProc("Console.GetGradesByVersion", params)

            dgGrade.DataSource = ds.Tables(0)

        End If
        TimeHistory_Load()
        TimeSummary_Load()

    End Sub

    Private Function VChecked(mRefNum As String, mode As String) As Boolean
        Dim ret As Boolean = False
        Dim params As New List(Of String)
        params.Add("RefNum/" + mRefNum)
        params.Add("IssueID/" + mode)
        Dim ds As DataSet = db.ExecuteStoredProc("Console." & "IssueCheckV", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0)(0).ToString = "Y" Then Return True
            End If
        End If
        Return False
    End Function
    Private Sub GetDirectories(ByVal subDirs() As DirectoryInfo, ByVal nodeToAddTo As TreeNode)

        Dim aNode As TreeNode
        Dim subSubDirs() As DirectoryInfo
        Dim subDir As DirectoryInfo
        For Each subDir In subDirs
            aNode = New TreeNode(subDir.Name, 0, 0)
            aNode.Tag = subDir
            aNode.ImageKey = "folder"
            subSubDirs = subDir.GetDirectories()
            If subSubDirs.Length <> 0 Then
                GetDirectories(subSubDirs, aNode)
            End If
            nodeToAddTo.Nodes.Add(aNode)
        Next subDir

    End Sub
    Private Sub GeneratePolishRpt()
        Dim xl As Excel.Application
        xl = Utilities.GetExcelProcess()
        Dim w As Excel.Workbook
        Me.Cursor = Cursors.WaitCursor

        w = xl.Workbooks.Open(PolishPath & "Database Version of Outliers.xlsm", , True)
        xl.Visible = True
        Try
            xl.Run("Console_OpenPolishRpt_RefNum", CurrentRefNum)
            w.Close()
        Catch
        Finally
            Me.Cursor = Cursors.Default
            If File.Exists(CorrPath & CurrentRefNum & "_Polish.xls") Then Process.Start(CorrPath & CurrentRefNum & "_Polish.xls")
        End Try
    End Sub
    Private Sub OpenPolishReport()
        Dim rtn As DialogResult = System.Windows.Forms.DialogResult.OK
        If Not File.Exists(CorrPath & CurrentRefNum & "_Polish.xls") Then
            rtn = MessageBox.Show("Generate Polish Report?", "Polish Report", MessageBoxButtons.OKCancel)
            If rtn = System.Windows.Forms.DialogResult.OK Then
                GeneratePolishRpt()
            End If
        Else
            GeneratePolishRpt()
        End If
    End Sub
    'Validation
    Private Sub IDR(JustLooking As Boolean)
        Dim strFLMacroDev As String = ""
        Dim AlreadyOpen As Boolean = False
        Dim mTempPath As String
        'Launch Excel Validation
        ' Create new Application.
        Dim xl As Excel.Application
        Dim Desktop As String
        If StudyYear = "2014" Then
            Desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\IDR14\"
        Else
            Desktop = TempPath
        End If

        xl = Utilities.GetExcelProcess()

        Try
            If StudyYear = "2014" Then
                If Not Directory.Exists(Desktop & "Temp\") Then Directory.CreateDirectory(Desktop & "Temp\")
                mTempPath = Desktop & "Temp\"
            Else
                mTempPath = TempPath
            End If


            For Each wb In xl.Workbooks
                If wb.Name = ValidationFile Then
                    AlreadyOpen = True
                    Exit For
                End If
            Next wb

            Dim w As Excel.Workbook
            ' Open Excel spreadsheet.
            If Not AlreadyOpen Then
                If CheckRole("FLMacrosDev", UserName) Then
                    If MsgBox("Use FLMacros from Development?", vbYesNoCancel, "Careful!") = vbYes Then
                        strFLMacroDev = "Development\"
                    End If
                End If

                If IO.File.Exists(mTempPath & ValidationFile) Then File.Delete(mTempPath & ValidationFile)
                FileCopy(IDRPath & strFLMacroDev & ValidationFile, mTempPath & ValidationFile)
                If (VPN) Then
                    Dim b As DialogResult = MessageBox.Show("Since you are on VPN, we are going to pause to give the file enough time to load", "Wait", MessageBoxButtons.OK)
                    System.Threading.Thread.Sleep(3000)
                End If

                w = xl.Workbooks.Open(mTempPath & ValidationFile)
                xl.Visible = True
                w.RunAutoMacros(1)


            End If

            If xl.Workbooks(ValidationFile).Windows(ValidationFile).Visible = False Then xl.Workbooks(ValidationFile).Windows(ValidationFile).Visible = True
            If bJustLooking Then
                'Run Validation Macro
                xl.Run("JustLookin", CurrentRefNum)
            Else
                xl.Run("DelayLoadRefinery", CurrentRefNum)
            End If

        Catch ex As System.Exception
            Err.WriteLog("Validation: " & CurrentRefNum.ToString, ex)
            MessageBox.Show("Trouble launching " & ValidationFile & ".  Make sure your Macro File is not already opened" & vbCrLf & ex.Message)
        End Try

    End Sub
    'Set all directory paths
    Private Sub SetPaths()
        Try
            Dim mRefNum As String
            StudyRegion = cboStudy.Text.Substring(0, 3)
            Coloc = GetCompanyName(cboRefNum.Text)
            Company = Company.Substring(0, Company.IndexOf("-") - 1)
            mRefNum = cboRefNum.Text
            GeneralPath = StudyDrive & "General\" & StudyRegion & "\" & ParseRefNum(mRefNum, 1) & StudyRegion & "\"
            ProjectPath = StudyDrive & StudyYear & "\Project\"

            RefineryPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Refinery\" & ParseRefNum(mRefNum, 0) & "\"
            CorrPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Correspondence\" & mRefNum & "\"
            If Directory.Exists(StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\") Then
                CompCorrPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\"
            Else
                Directory.CreateDirectory(StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\")
                CompCorrPath = StudyDrive & StudyYear & "\" & StudyRegion & "\Company Correspondence\" & Company & "\"
            End If
            CompanyPath = StudyDrive & StudyYear & "\" & StudyRegion + "\" & "Company\" & Company & "\"
            If StudyYear <> "2014" Then
                IDRPath = StudyDrive & StudyYear & "\Software\Validate\"
                DrawingPath = GeneralPath & "Drawings\"
                ClientAttachmentsPath = IDRPath & "Client Attachments\"
                TemplatePath = IDRPath & "Template\"
                If Not Directory.Exists("c:\users\" & UserName & "\Console") Then Directory.CreateDirectory("c:\users\" & UserName & "\Console")
                If Not Directory.Exists("c:\users\" & UserName & "\Console\temp") Then Directory.CreateDirectory("c:\users\" & UserName & "\Console\temp")
                TempPath = "c:\users\" & UserName & "\Console\Temp\"

                PolishPath = IDRPath & "Polish\"
            Else
                IDRPath = StudyDrive & StudyYear & "\Software\IDR\"
                DrawingPath = GeneralPath & "Drawings\"
                ClientAttachmentsPath = IDRPath & "Client Attachments\"
                TemplatePath = IDRPath & "Template\"
                TempPath = Path.GetTempPath() & StudyType & "\"

                PolishPath = IDRPath & "Polish\"
            End If
            BuildDirectoriesTab()
        Catch ex As System.Exception
            Err.WriteLog("SetPaths", ex)
        End Try

    End Sub
    'Method to remove specific tab from tabcontrol
    Private Sub RemoveTab(tabname As String)
        Dim tab As TabPage
        tab = ConsoleTabs.TabPages(tabname)
        ConsoleTabs.TabPages.Remove(tab)
    End Sub

    Private Function ParseRefNum(mRefNum As String, mode As Integer) As String
        Dim parsed As String = Utilities.GetRefNumPart(mRefNum, mode)
        Return parsed
    End Function

    'Method to save last refnum visited per Study for settings file
    Private Sub SaveRefNum(strStudy As String, strRefNum As String)

        For I = 0 To 8
            If StudySave(I) = strStudy Then
                RefNumSave(I) = strRefNum
            End If
        Next

    End Sub
#End Region

#Region "DATABASE LOOKUPS"

    Private Sub btnClippySearch_Click(sender As System.Object, e As System.EventArgs) Handles btnClippySearch.Click
        Dim da As SqlDataAdapter
        Dim conn As New SqlConnection(db.ConnectionString)
        Dim cmd As SqlCommand

        If chkSQL.Checked Then

            If txtClippySearch.Text.ToUpper.Contains("INSERT") Or txtClippySearch.Text.ToUpper.Contains("DELETE") Or txtClippySearch.Text.ToUpper.Contains("UPDATE") Or txtClippySearch.Text.ToUpper.Contains("DROP") Then
                lblError.Text = "CAN ONLY SELECT DATA - READ ONLY"
                Exit Sub
            End If
            cmd = New SqlCommand
            da = New SqlDataAdapter
            ds = New DataSet
            lblError.Text = ""
            Try
                Try
                    conn.Open()
                Catch ex As System.Exception
                    lblError.Text = ex.Message
                    Exit Sub
                End Try
                cmd.Connection = conn
                cmd.CommandType = CommandType.Text
                txtClippySearch.Text = txtClippySearch.Text.ToUpper.Replace("SELECT *", "SELECT TOP 1000 *")
                cmd.CommandText = txtClippySearch.Text
                cmd.CommandTimeout = 15

                da.SelectCommand = cmd
                Me.Cursor = Cursors.WaitCursor
                da.Fill(ds)
                dgClippyResults.DataSource = ds.Tables(0)
                Me.Cursor = Cursors.Default
            Catch ex As System.Exception
                MessageBox.Show(ex.Message, "SQL ERROR")
            End Try

            conn.Close()

        Else
            'DO CLIPPY SEARCH
        End If
    End Sub

    Private Function IsValidRefNum(refnum As String) As String
        Dim ret As String = ""
        Dim params = New List(Of String)

        params.Add("@RefNum/" + cboRefNum.Text)
        params.Add("@StudyYear/" + StudyYear.Substring(2, 2))

        Dim c As DataSet = db.ExecuteStoredProc("Console." & "IsValidRefNum", params)
        If c.Tables.Count > 0 Then
            If c.Tables(0).Rows.Count > 0 Then
                ret = c.Tables(0).Rows(0)(0).ToString
            End If
        End If
        Return ret

    End Function

    Private Function FindString(lb As ComboBox, str As String) As Integer
        Dim count As Integer = 0
        For Each item As String In lb.Items
            If item.Contains(str) Then
                Return count
            End If
            count += 1
        Next
    End Function


    Private Sub UpdateConsultant()

        Dim rslt As DialogResult = MessageBox.Show("Are you sure you want to change the current consultant?", "Warning", MessageBoxButtons.YesNo)
        If rslt = System.Windows.Forms.DialogResult.Yes Then
            Dim params = New List(Of String)

            params.Add("RefNum/" + ParseRefNum(RefNum, 0))
            params.Add("Consultant/" + cboConsultant.Text)
            Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateConsultant", params)
            If c = 0 Then
                MessageBox.Show("Consultant did not save.")
            End If
        Else
            RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
            cboConsultant.Text = CurrentConsultant
            AddHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged

        End If
    End Sub

    Private Function CheckRole(role As String, initials As String) As Boolean

        Dim params As List(Of String)
        params = New List(Of String)
        Dim roles As DataSet
        params.Add("UserID/" + UserName)
        roles = db.ExecuteStoredProc("Console.GetRoles", params)
        For Each dr In roles.Tables(0).Rows
            If dr("Role").ToString = role Then Return True
        Next
        Return False

    End Function
    Private Sub UpdateIssue(status As String)
        Dim Success As Boolean = False
        Dim CheckV2 As Boolean = False
        Dim params As List(Of String)
        params = New List(Of String)
        If Me.tvIssues.SelectedNode IsNot Nothing Then
            If tvIssues.SelectedNode.Text.ToUpper.Contains("V2") Then
                CheckV2 = True
                
                        If CheckRole("CheckV2", UserName) Then
                            Success = True

                        End If

            End If
            params.Clear()
            If Success Or Not CheckV2 Then
                Dim node As String = tvIssues.SelectedNode.Text.ToString.Trim

                params.Add("RefNum/" + RefNum)
                params.Add("IssueTitle/" & node.Substring(0, node.IndexOf("-") - 1).Trim)
                params.Add("Status/" & status)
                params.Add("UpdatedBy/" & UserName)
                Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateIssue", params)
            Else
                RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
                tvIssues.SelectedNode.Checked = Not tvIssues.SelectedNode.Checked
                AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck
                MessageBox.Show("You do not have approval rights for this Issue, see the Project Manager")
            End If
        End If
        UpdateCheckList()

    End Sub

    Public Function GetMainConsultant() As String
        Dim params As New List(Of String)

        params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        ds = db.ExecuteStoredProc("Console.GetMainConsultant", params)

        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0).ToString().Trim() & " - " & ds.Tables(0).Rows(0)(1).ToString() & "m"
        End If
        Return ""
    End Function

    Public Sub TimeSummary_Load()

        Dim params As New List(Of String)
        params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        ds = db.ExecuteStoredProc("Console.GetTimeSummary", params)
        dgSummary.DataSource = ds.Tables(0)

    End Sub

    Public Sub TimeHistory_Load()

        Dim params As New List(Of String)
        params.Add("RefNum/" & ParseRefNum(RefNum, 0))
        ds = db.ExecuteStoredProc("Console.GetTimeHistory", params)
        dgHistory.DataSource = ds.Tables(0)

    End Sub
    Private Function FillRefNums() As Integer

        Dim rns As List(Of String)
        rns = GetRefNumsByConsultant()

        For Each r In rns
            cboRefNum.Items.Add(r)
        Next

        cboRefNum.Items.Add("--------------------")

        Dim params As List(Of String)
        params = New List(Of String)
        params.Add("StudyYear/" + StudyYear)
        params.Add("Study/" + Study)

        ds = db.ExecuteStoredProc("Console." & "GetRefNums", params)
        Dim row As DataRow 'Represents a row of data in Systems.data.datatable
        For Each row In ds.Tables(0).Rows
            cboRefNum.Items.Add(row("RefNum").ToString.Trim)
            cboCompany.Items.Add(row("CoLoc").ToString.Trim)
        Next
        Return ds.Tables(0).Rows.Count

    End Function

    'Method to populate refnums combo box
    Private Sub GetRefNums(Optional strRefNum As String = "")
        Dim NoRefNums As Integer = 0
        Dim selectItem As String = cboStudy.SelectedItem
        CheckForCurrentStudyYear()
        If Not strRefNum Is Nothing Then
            cboRefNum.Items.Clear()
            cboCompany.Items.Clear()

            'Study RefNum Box
            NoRefNums = FillRefNums()

            If NoRefNums = 0 Then
                MessageBox.Show("There are no records for " & StudyYear & " study.", "Not Found")
                RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
                cboStudy.Text = "OLE11"
                StudyYear = "2011"
                NoRefNums = FillRefNums()
                AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            End If



            If Not strRefNum Is Nothing And strRefNum.Length > 5 Then

                cboRefNum.SelectedIndex = cboRefNum.FindString(strRefNum)
            Else
                If ReferenceNum Is Nothing Then
                    'RefNum = cboRefNum.Items(0).ToString
                    cboRefNum.SelectedIndex = 0
                Else
                    RefNum = ReferenceNum.Trim
                    cboRefNum.SelectedIndex = cboRefNum.FindString(ReferenceNum)
                End If
            End If
        End If
        CheckFuelsLubes()
    End Sub

    'Method to see if there is a combo refinery and show and populate the button
    Private Sub CheckFuelsLubes()
        btnFuelLube.Visible = False
        lblCombo.Visible = False

        Dim params As New List(Of String)
        Dim RNum As String = ParseRefNum(cboRefNum.Text, 0).Trim
        params.Add("RefNum/" + RNum)
        ds = db.ExecuteStoredProc("Console.CheckForCombo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                btnFuelLube.Text = ds.Tables(0).Rows(0)(0).ToString.Trim
                btnFuelLube.Visible = True
                lblCombo.Visible = True

            Else
                btnFuelLube.Visible = False
                lblCombo.Visible = False
            End If
        Else
            btnFuelLube.Visible = False
            lblCombo.Visible = False

        End If

    End Sub

    'Method to retrieve and populate consultants drop down box
    Private Sub GetConsultants()

        Dim params As List(Of String)
        Try
            params = New List(Of String)
            params.Add("StudyYear/" + StudyYear)
            params.Add("Study/" + Study)
            params.Add("RefNum/" + cboRefNum.Text)
            ds = db.ExecuteStoredProc("Console." & "GetConsultants", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    CurrentConsultant = ds.Tables(0).Rows(0).Item("Consultant").ToString.ToUpper.Trim
                End If
            End If
            RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
            cboConsultant.Text = CurrentConsultant
            AddHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
        Catch ex As System.Exception
            Err.WriteLog("GetConsultants", ex)
        End Try

    End Sub

    'Function to retrieve Company name by RefNum
    Private Function GetCompanyName(str As String) As String

        'Get Company Name
        Dim params As List(Of String)
        Try
            params = New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            ds = db.ExecuteStoredProc("Console." & "GetCompanyName", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then Return ds.Tables(0).Rows(0)("CoLoc").ToString.Trim
            End If

            ClearFields()
            Return ""
        Catch ex As System.Exception
            Err.WriteLog("GetCompanyName", ex)
        End Try

    End Function

    'If exists, this will retrieve Interim Contact info
    Private Sub GetInterimContactInfo()
        InterimContact = New Contacts()
        Dim params As New List(Of String)

        'Check for Interim Contact Info
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.ExecuteStoredProc("Console." & "GetInterimContactInfo", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                InterimContact.FirstName = ds.Tables(0).Rows(0).Item("FirstName").ToString
                InterimContact.LastName = ds.Tables(0).Rows(0).Item("LastName").ToString
                InterimContact.Email = ds.Tables(0).Rows(0).Item("Email").ToString
                InterimContact.Phone = ds.Tables(0).Rows(0).Item("Phone").ToString
            End If
        Else
            InterimContact.Clear()
        End If

    End Sub
    

    Public Sub GetPrimaryClientCoordinator()
        lblPCCEmail.Text = ""
        lblPCCName.Text = ""
        Dim params As New List(Of String)
        Try
            'Check for Interim Contact Info
            params = New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            ds = db.ExecuteStoredProc("Console." & "GetPrimaryCoordinator", params)
            Dim PCC As String
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    PCC = ds.Tables(0).Rows(0).Item("PCC").ToString
                    lblPCCName.Text = Utilities.GetConsultantName(PCC)
                    lblPCCEmail.Text = PCC & IIf(PCC = "", "", "@solomononline.com")

                End If
            End If
        Catch ex As System.Exception
            Err.WriteLog("GetPrimaryClientCoordinator", ex)
        End Try
    End Sub

    Private Sub GetPricingContactInfo()
        lblPricingContact.Text = ""
        lblPricingContactEmail.Text = ""
        lblPricingPhone.Text = ""
        Dim params As New List(Of String)
        Try
            'Check for Interim Contact Info
            params = New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            ds = db.ExecuteStoredProc("Console." & "GetPricingContact", params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    lblPricingContact.Text = ds.Tables(0).Rows(0).Item("PricingName").ToString
                    lblPricingContactEmail.Text = ds.Tables(0).Rows(0).Item("PricingEmail").ToString
                    lblPricingPhone.Text = ds.Tables(0).Rows(0).Item("PricingPhone").ToString
                End If
            End If
        Catch ex As System.Exception
            Err.WriteLog("GetPricingContactInfo", ex)
        End Try
    End Sub

    'Method to retrieve Company Contact Information
    Private Sub GetCompanyContactInfo()
        Try
            CompanyContact = New Contacts()
            Dim row As DataRow
            Dim params As New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            params.Add("ContactType/COORD")
            params.Add("StudyYear/")
            ds = db.GetCompanyContacts(params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    'cboConsultant.SelectedItem = CurrentConsultant

                    CompanyContact.FirstName = row("FirstName").ToString
                    CompanyContact.LastName = row("LastName").ToString
                    CompanyContact.Email = row("Email").ToString
                    CompanyContact.Phone = row("Phone").ToString
                    CompanyContact.AltFirstName = row("AltFirstName").ToString
                    CompanyContact.AltLastName = row("AltLastName").ToString
                    CompanyContact.AltEmail = row("AltEmail").ToString
                    CompanyContact.Fax = row("Fax").ToString
                    CompanyContact.MailAdd1 = row("MailAddr1").ToString
                    CompanyContact.MailAdd2 = row("MailAddr2").ToString
                    CompanyContact.MailAdd3 = row("MailAddr3").ToString
                    CompanyContact.MailCity = row("MailCity").ToString
                    CompanyContact.MailState = row("MailState").ToString
                    CompanyContact.MailZip = row("MailZip").ToString
                    CompanyContact.MailCountry = row("MailCountry").ToString
                    CompanyContact.StreetAdd1 = row("StrAddr1").ToString
                    CompanyContact.StreetAdd2 = row("StrAddr2").ToString
                    CompanyContact.StreetAdd3 = row("StrAdd3").ToString
                    CompanyContact.StreetCity = row("StrCity").ToString
                    CompanyContact.StreetState = row("StrState").ToString
                    CompanyContact.StreetZip = row("StrZip").ToString
                    CompanyContact.StreetCountry = row("StrCountry").ToString
                    CompanyContact.Title = row("JobTitle").ToString
                    CompanyContact.Password = row("CompanyPassword").ToString.Trim
                    CompanyPassword = CompanyContact.Password
                    CompanyContact.SendMethod = row("CompanySendMethod").ToString
                    'txtSendFormat.Text = CompanyContact.SendMethod
                    'txtCorrNotes.Text = row("Comment").ToString
                    SANumber = row("SANumber").ToString
                    
                End If

            Else
                CompanyContact.Clear()
            End If
            If CompanyPassword.Length = 0 Then
                CompanyContact.Password = GetCompanyPassword().Trim

            End If
        Catch ex As System.Exception
            Err.WriteLog("GetCompanyContactInfo", ex)
        End Try

    End Sub
    Private Function GetCompanyPassword() As String
        Dim params As New List(Of String)
        Dim row As DataRow
        params.Add("RefNum/" + cboRefNum.Text)
        ds = db.GetCompanyPassword(params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then

                row = ds.Tables(0).Rows(0)
                Return row(0).ToString
            End If
        End If
        Return ""
    End Function

    Private Sub GetAltCompanyContactInfo()
        Try
            AltCompanyContact = New Contacts()
            Dim row As DataRow
            Dim params As New List(Of String)
            params.Add("RefNum/" + cboRefNum.Text)
            params.Add("ContactType/ALT")
            params.Add("StudyYear/")
            ds = db.GetCompanyContacts(params)

            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    cboConsultant.SelectedItem = CurrentConsultant

                    AltCompanyContact.FirstName = row("FirstName").ToString
                    AltCompanyContact.LastName = row("LastName").ToString
                    AltCompanyContact.Email = row("Email").ToString
                    AltCompanyContact.Phone = row("Phone").ToString
                    AltCompanyContact.AltFirstName = row("AltFirstName").ToString
                    AltCompanyContact.AltLastName = row("AltLastName").ToString
                    AltCompanyContact.AltEmail = row("AltEmail").ToString
                    AltCompanyContact.Fax = row("Fax").ToString
                    AltCompanyContact.MailAdd1 = row("MailAddr1").ToString
                    AltCompanyContact.MailAdd2 = row("MailAddr2").ToString
                    AltCompanyContact.MailAdd3 = row("MailAddr3").ToString
                    AltCompanyContact.MailCity = row("MailCity").ToString
                    AltCompanyContact.MailState = row("MailState").ToString
                    AltCompanyContact.MailZip = row("MailZip").ToString
                    AltCompanyContact.MailCountry = row("MailCountry").ToString
                    AltCompanyContact.StreetAdd1 = row("StrAddr1").ToString
                    AltCompanyContact.StreetAdd2 = row("StrAddr2").ToString
                    AltCompanyContact.StreetAdd3 = row("StrAdd3").ToString
                    AltCompanyContact.StreetCity = row("StrCity").ToString
                    AltCompanyContact.StreetState = row("StrState").ToString
                    AltCompanyContact.StreetZip = row("StrZip").ToString
                    AltCompanyContact.StreetCountry = row("StrCountry").ToString
                    AltCompanyContact.Title = row("JobTitle").ToString
                    AltCompanyContact.Password = row("CompanyPassword").ToString.Trim
                    CompanyPassword = AltCompanyContact.Password
                    AltCompanyContact.SendMethod = row("CompanySendMethod").ToString
                    SANumber = row("SANumber").ToString


                Else
                    AltCompanyContact.Clear()
                End If
            End If
        Catch ex As System.Exception
            Err.WriteLog("GetAltCompanyContactInfo", ex)
        End Try
    End Sub

    'Method to retrieve Refinery Contact information
    Private Sub GetContacts()

        Dim row As DataRow
        Dim params As New List(Of String)
        Try
            params.Add("RefNum/" + cboRefNum.Text)
            ds = db.GetContacts(params)

            RefineryContact = New Contacts()
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    row = ds.Tables(0).Rows(0)
                    RefineryContact.FullName = row("CoordName").ToString
                    RefineryContact.AltFullName = row("RefName2").ToString
                    RefineryContact.Title = row("RefTitle").ToString
                    RefineryContact.Email = row("CoordEmail").ToString
                    RefineryContact.AltEmail = row("RefEmail2").ToString
                    RefineryContact.Phone = row("CoordPhone").ToString
                    RefineryContact.AltPhone = row("RefPhone2").ToString
                    RefineryContact.MaintMgrName = row("MaintMgrName").ToString
                    RefineryContact.MaintEmail = row("MaintMgrEmail").ToString
                    RefineryContact.OpsMgrName = row("OpsMgrName").ToString
                    RefineryContact.OpsEmail = row("OpsMgrEmail").ToString
                    RefineryContact.TechMgrName = row("TechMgrName").ToString
                    RefineryContact.TechEmail = row("TechMgrEmail").ToString
                    RefineryContact.RefMgrName = row("RefMgrName").ToString
                    RefineryContact.RefEmail = row("RefMgrEmail").ToString
                    RefineryContact.RefinerySPD = row("location").ToString
                    RefineryContact.RefineryCountry = row("country").ToString
                Else
                    RefineryContact.Clear()
                End If
            End If

            GetPrimaryClientCoordinator()
        Catch ex As System.Exception
            Err.WriteLog("GetContacts", ex)
        End Try
    End Sub

    'Method to retrieve all infomation per Ref Num
    Public Sub GetARefNumRecord(mRefNum As String)

        ConsoleTabs.Enabled = True
        If mRefNum = "" Then mRefNum = RefNum

        CurrentRefNum = mRefNum
        RefNum = mRefNum

        'If Refnum is lubes, change variable according to need
        If CurrentRefNum.Contains("LIV") Then
            LubRefNum = CurrentRefNum
            CurrentRefNum = CurrentRefNum.Replace("LIV", "LUB")
        End If
        If CurrentRefNum.Contains("LEV") Then
            LubRefNum = CurrentRefNum
            CurrentRefNum = CurrentRefNum.Replace("LEV", "LUB")
        End If
        Company = GetCompanyName(mRefNum)
        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        cboCompany.Text = Company
        StudyRegion = ParseRefNum(mRefNum, 2)
        cboRefNum.Text = CurrentRefNum
        SetPaths()
        btnMain.Text = GetMainConsultant()

    End Sub

    'Method to retrieve all infomation per Ref Num
    Public Sub GetRefNumRecord(mRefNum As String)
        Dim Level As String = mRefNum
        Try
            CompanyPassword = ""
            ConsoleTabs.Enabled = True
            If mRefNum = "" Then
                cboStudy.SelectedIndex = 1
            End If


            CurrentRefNum = mRefNum
            RefNum = mRefNum

            'If Refnum is lubes, change variable according to need
            If CurrentRefNum.Contains("LIV") Then
                LubRefNum = CurrentRefNum
                CurrentRefNum = CurrentRefNum.Replace("LIV", "LUB")
            End If
            If CurrentRefNum.Contains("LEV") Then
                LubRefNum = CurrentRefNum
                CurrentRefNum = CurrentRefNum.Replace("LEV", "LUB")
            End If
            Level = "1"
            StudyRegion = ParseRefNum(mRefNum, 2)
            Level = "2"
            Company = GetCompanyName(mRefNum)
            Level = "3"
            If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
            cboCompany.Text = Company

            cboRefNum.Text = CurrentRefNum
            'Main 
            Level = "4"
            IsInHolding()
            btnMain.Text = GetMainConsultant()
            'SetTabDirty(True)
            BuildCorrespondenceTab()
            BuildTab(CurrentTab)
            BuildTab("Contacts")
        Catch ex As System.Exception
            Err.WriteLog("GetRefNumRecord: " & Level, ex)
        End Try

    End Sub

    'Retrieve all Refnums per Consultant, Study Type and Study Year for Consultant Tab


    'Routine to populate the Consultant Treeview with Refnums and outstanding issues

    Function R3Data(refnum As String) As String
        Dim strTempCorrPath As String = Nothing
        Dim lstVFNumbers As New List(Of String)
        Dim dteFileDateLatest As Date
        Dim dteFileDate As Date
        Dim strDirResult As String
        Dim blnAlready As Boolean
        Dim I As Integer
        Dim R3 As String = Nothing

        ' lstVFNumbers is a non-visible list control that I use to
        ' hold and sort the VF numbers.
        strTempCorrPath = BuildCorrPath(refnum)
        strDirResult = Dir(strTempCorrPath & "VF*")
        Do While strDirResult <> ""
            I = 0
            blnAlready = False
            Do While I < lstVFNumbers.Count
                If lstVFNumbers(I) = Mid(strDirResult, 3, 1) Then
                    blnAlready = True
                    Exit Do
                End If
                I = I + 1
            Loop
            If Not blnAlready Then
                If IsNumeric(Mid(strDirResult, 3, 1)) Then
                    lstVFNumbers.Add(Mid(strDirResult, 3, 1))
                End If
            End If
            dteFileDate = FileDateTime(strTempCorrPath & strDirResult)
            If dteFileDate > dteFileDateLatest Then
                dteFileDateLatest = dteFileDate
            End If
            strDirResult = Dir()
        Loop

        Dim intHighVF As Integer
        If lstVFNumbers.Count > 0 Then
            If lstVFNumbers(lstVFNumbers.Count - 1) <> "" Then
                intHighVF = lstVFNumbers(lstVFNumbers.Count - 1)
                strDirResult = Dir(strTempCorrPath & "VR" & CStr(intHighVF) & "*")
                If strDirResult = "" Then
                    R3 = intHighVF & "      " & DateDiff(DateInterval.Day, dteFileDateLatest, Now) & "       " & Format(dteFileDateLatest, "dd-MMM-yy")
                Else
                    R3 = "        "
                End If
            End If
        Else
            R3 = "        "
        End If

        Return R3
    End Function

    Private Function BuildCorrPath(refnum As String) As String

        Dim path As String = Nothing
        If Directory.Exists(StudyDrive & StudyYear & "\" & ParseRefNum(refnum, 2) & "\Correspondence\" & refnum & "\") Then
            path = StudyDrive & StudyYear & "\" & ParseRefNum(refnum, 2) & "\Correspondence\" & refnum & "\"
        End If
        Return path

    End Function

#End Region

#Region "UI METHODS"

    Private Sub btnSendPrelimPricing_Click(sender As System.Object, e As System.EventArgs) Handles btnSendPrelimPricing.Click
        Dim params As New List(Of String)
        Dim emails As New List(Of String)
        Dim nRow As DataRow
        Dim AddtoEmails As Boolean

        Dim PricingEmail As String = ""
        Dim RefineryEmail As String = ""
        
        params.Clear()
        params.Add("RefNum/" + cboRefNum.Text)
       
        ds = db.ExecuteStoredProc("Console.GetPricingContact", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                PricingEmail = ds.Tables(0).Rows(0)("PricingEmail").ToString
            End If
        End If

        ds = db.ExecuteStoredProc("Console.GetContactEmails", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then emails.Add(ds.Tables(0).Rows(0)("Email").ToString.ToUpper)
            For I = 1 To ds.Tables(0).Rows.Count - 1
                nRow = ds.Tables(0).Rows(I)
                AddtoEmails = True
                For Each mail In emails
                    If mail.Contains(nRow("Email").ToString.ToUpper) Then AddtoEmails = False
                Next
                If AddtoEmails Then emails.Add(nRow("Email").ToString.ToUpper)
            Next

            For Each em In emails
                RefineryEmail += em & ";"
            Next
        End If

        If PricingEmail <> "" Then
            SendPreliminaryPricing(PricingEmail, RefineryEmail)
        Else
            SendPreliminaryPricing(RefineryEmail, "")
        End If

    End Sub
    Private Sub SendPreliminaryPricing(strTo As String, strCC As String)

        Dim attachments As New List(Of String)
        Dim subj As String = ""
        Dim AddDays As String = ""
        Dim body As String = ""

        subj = "Preliminary Pricing for " & cboCompany.Text & " (" & RefNum & ")"
        Try
            If File.Exists(IDRPath & "PrelimPricing\" & Study & "\EmailText.txt") Then
                AddDays = InputBox("Number of working days to allow for response?", "How much time?", "10")
                subj = My.Computer.FileSystem.OpenTextFileReader(IDRPath & "PrelimPricing\" & Study & "\EmailText.txt").ReadLine
                body = My.Computer.FileSystem.ReadAllText(IDRPath & "\PrelimPricing\" & Study & "\EmailText.txt")
                body = body.Replace("~DueDate~", Utilities.ValFaxDateDue(Convert.ToInt32(AddDays)).ToString("dd-MMM-yy"))
                body = body.Replace("~Refinery~", Coloc)
                subj = subj.Replace("~Refinery~", Coloc)
            Else
                MessageBox.Show(IDRPath & "\PrelimPricing\" & Study & "\EmailText.txt is missing")
                body = ""
            End If

            If File.Exists(IDRPath & "\PrelimPricing\" & Study & "\" & StudyYear & " Preliminary Price Letter.PDF") Then
                attachments.Add(IDRPath & "\PrelimPricing\" & Study & "\" & StudyYear & " Preliminary Price Letter.PDF")
            Else
                MessageBox.Show(IDRPath & "\PrelimPricing\" & Study & "\" & StudyYear & " Preliminary Price Letter.PDF is missing")
            End If
            If File.Exists(IDRPath & "\PrelimPricing\" & Study & "\" & cboCompany.Text & "_PP.xls") Then
                attachments.Add(IDRPath & "\PrelimPricing\" & Study & "\" & cboCompany.Text & "_PP.xls")
            Else
                MessageBox.Show(IDRPath & "\PrelimPricing\" & Study & "\" & cboCompany.Text & "_PP.xls is missing")
            End If

            If Study = "LUB" Then
                If File.Exists(IDRPath & "\PrelimPricing\" & Study & "\" & StudyYear & " Lube Study Product Pricing Basis.PDF") Then
                    attachments.Add(IDRPath & "\PrelimPricing\" & Study & "\" & StudyYear & " Lube Study Product Pricing Basis.PDF")
                Else
                    MessageBox.Show(IDRPath & "\PrelimPricing\" & Study & "\" & StudyYear & " Lube Study Product Pricing Basis.PDF is missing")
                End If
            End If
            Utilities.SendEmail(strTo, strCC, subj, body, attachments, True)

        Catch ex As System.Exception
            Err.WriteLog("SendPreliminaryPricing", ex)
            MessageBox.Show(ex.Message, "SendPreliminaryPricing")

        End Try

    End Sub


    Private Sub UpdateNotes()

        Dim params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Notes/" + Utilities.FixQuotes(txtNotes.Text))
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)

    End Sub
    Private Sub txtValidationIssues_LostFocus(sender As Object, e As System.EventArgs) Handles txtNotes.LostFocus
        If txtNotes.Text <> "" Then SaveNotes()
    End Sub


    Private Sub tvIssues_AfterCheck(sender As Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterCheck

        txtIssueID.Text = ""
        txtIssueName.Text = ""

        txtDescription.Text = ""

        Try
            tvIssues.SelectedNode = e.Node

            If e.Node.Checked Then
                If tvIssues.SelectedNode.Text.Contains("C 0.1") Then
                    If IsInHolding() Then
                        MessageBox.Show("You cannot check this off if it is in a HOLDING status", "Refinery in Holding")
                        e.Node.Checked = False
                        Exit Sub
                    End If
                End If
                If tvIssues.SelectedNode.Text.Contains("V2") Then
                    Dim dr As DialogResult = MessageBox.Show("Would you like to change the assigned consultant back to " & btnMain.Text & " ?", "Wait", MessageBoxButtons.YesNo)
                    If dr = System.Windows.Forms.DialogResult.No Then Exit Sub
                    RemoveHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
                    cboConsultant.Text = btnMain.Text.Substring(0, btnMain.Text.IndexOf("-") - 1).Trim
                    AddHandler cboConsultant.SelectedIndexChanged, AddressOf cboConsultant_SelectedIndexChanged
                    ShowTimeGrade()
                End If

                UpdateIssue("Y")
            Else

                UpdateIssue("N")
            End If

        Catch ex As System.Exception
            Err.WriteLog("tvIssues_AfterCheck:" & RefNum, ex)
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub tvIssues_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvIssues.AfterSelect
        Dim IssueID As String = Nothing
        If Not tvIssues.SelectedNode Is Nothing Then


            IssueID = tvIssues.SelectedNode.Text
            IssueID = IssueID.Substring(0, IssueID.IndexOf("-")).Trim
            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueID/" & IssueID)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString

                    txtDescription.Text = row("IssueText").ToString
                End If
            End If
        End If

    End Sub

    Private Sub btnPricing_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "Pricing", RefNum, GetCompanyName(RefNum), mDBConnection)
        ContactForm.Show()
    End Sub

    Private Sub btnDataCoordinator_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "DC", RefNum, GetCompanyName(RefNum), mDBConnection)
        ContactForm.Show()
    End Sub

    Private Sub btnDensity_Click(sender As System.Object, e As System.EventArgs)
        If Dir("K:\Study\Refining\General\DensityCalc.xls") <> "" Then
            Process.Start("K:\Study\Refining\General\DensityCalc.xls")
        End If
    End Sub

    Dim WithEvents ContactForm As ContactFormPopup

    'Show Contact Popup form
    Private Sub btnShow_Click(sender As System.Object, e As System.EventArgs) Handles btnShow.Click
        ContactForm = New ContactFormPopup(UserName, "COORD", RefNum, GetCompanyName(RefNum), StudyType, StudyYear)
        ContactForm.Show()
    End Sub

    Private Sub btnShowAltCo_Click(sender As System.Object, e As System.EventArgs) Handles btnShowAltCo.Click
        ContactForm = New ContactFormPopup(UserName, "ALT", RefNum, GetCompanyName(RefNum), StudyType, StudyYear)
        ContactForm.Show()
    End Sub

    Private Sub btnShowIntCo_Click(sender As System.Object, e As System.EventArgs) Handles btnShowIntCo.Click
        ContactForm = New ContactFormPopup(UserName, "INTERIM", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()
    End Sub


    Private Sub btnShowRefCo_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "REFINERY", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()
    End Sub

    Private Sub btnShowRefAltCo_Click(sender As System.Object, e As System.EventArgs)
        ContactForm = New ContactFormPopup(UserName, "ALTREF", RefNum, GetCompanyName(RefNum), StudyType)
        ContactForm.Show()
    End Sub

    Private Sub ContactForm_UpdateContact() Handles ContactForm.UpdateContact
        GetCompanyContactInfo()
        GetContacts()
        GetInterimContactInfo()
        FillContacts()
    End Sub

    Private Sub ContactForm_UpdateAltContact() Handles ContactForm.UpdateAltContact
        GetAltCompanyContactInfo()
        FillContacts()
    End Sub

    Private Function GetRefNumsByConsultant() As List(Of String)

        Dim rns As List(Of String) = New List(Of String)
        Dim params = New List(Of String)
        params.Add("@Consultant/" + UserName)
        params.Add("@Study/" + Study)
        params.Add("@StudyYear/" + StudyYear)
        ds = db.ExecuteStoredProc("Console." & "GetRefNumsByConsultant", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables.Count > 0 Then
                For Each r In ds.Tables(0).Rows
                    rns.Add(r(0))
                Next
            End If
        End If
        Return rns

    End Function


    Private Sub tvwCorrespondence_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles tvwCorrespondence.KeyDown, tvwDrawings.KeyDown, tvwClientAttachments.KeyDown, tvwCompCorr.KeyDown, tvwCorrespondence2.KeyDown, tvwDrawings2.KeyDown, tvwClientAttachments2.KeyDown, tvwCompCorr2.KeyDown
        If e.KeyCode = Keys.Space Then
            Process.Start("C:\")
        End If
    End Sub


    Private Sub cboCompany_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        RemoveHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged
        Dim params = New List(Of String)

        Try
            params.Add("CoLoc/" + cboCompany.Text)
            params.Add("Study/" + Study)
            params.Add("StudyYear/" + StudyYear)
            ds = db.ExecuteStoredProc("Console." & "GetRefNumsByCompany", params)
            If ds.Tables.Count > 0 Then
                If ds.Tables.Count > 0 Then
                    cboRefNum.SelectedIndex = cboRefNum.FindString(ds.Tables(0).Rows(0)("RefNum").ToString.Trim)
                    GetARefNumRecord(cboRefNum.Text)

                End If
            End If
            Company = cboCompany.Text

            BuildCorrespondenceTab()
            BuildTab("Contacts")
            AddHandler cboRefNum.SelectedIndexChanged, AddressOf cboRefNum_SelectedIndexChanged
            CheckPreliminaryPricing()
        Catch ex As System.Exception
            Err.WriteLog("cboCompany_SelectedIndexChanged", ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CheckPreliminaryPricing()
        Try
            If File.Exists(IDRPath & "\PrelimPricing\" & Study & "\" & cboRefNum.Text & "_PP.xls") Then
                If CheckRole("PrelimPricing", UserName) Then
                    btnSendPrelimPricing.Visible = True
                Else
                    btnSendPrelimPricing.Visible = False
                End If
            Else
                btnSendPrelimPricing.Visible = False
            End If
        Catch ex As System.Exception
            Err.WriteLog("CheckPreliminaryPricing", ex)
        End Try

    End Sub

    Private Sub btnOpenCorrFolder_Click(sender As System.Object, e As System.EventArgs)
        Process.Start(CorrPath)
    End Sub
    Public Function UpdatePN(RN As String) As String
        Dim params = New List(Of String)

        params.Add("RefNum/" + RN)
        params.Add("Issue/" + txtNotes.Text)
        params.Add("Consultant/" + UserName)
        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)
        If c > 0 Then
            Return "Review Notes updated successfully"
        Else
            Return RN & " did not create"
            MessageBox.Show("Review Notes did not update")
        End If
    End Function
    Private Sub btnUpdatePresenterNotes_Click(sender As System.Object, e As System.EventArgs)
        If txtNotes.Text <> "" Then
            lblStatus.Text = UpdatePN(cboRefNum.Text)
        End If
    End Sub


    Private Sub btnUpdateIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateIssue.Click
        Dim params As List(Of String)
        ' Be sure they gave us an IssueID and an IssueTitle
        If txtIssueID.Text = "" Or txtIssueName.Text = "" Then
            MsgBox("Please enter both an ID and a Title")
            Exit Sub
        End If

        ' Drop any single quotes in ID, Title, and Text.
        txtIssueID.Text = Utilities.DropQuotes(txtIssueID.Text)
        txtIssueName.Text = Utilities.DropQuotes(txtIssueName.Text)
        txtDescription.Text = Utilities.DropQuotes(txtDescription.Text)

        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("IssueID/" + txtIssueID.Text)
        ' Be sure it does not already exist.
        ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                MsgBox("That IssueID already exists for this Refnum.", vbOKOnly)
                Exit Sub
            End If

        End If

        ' Everything looks OK, add it here.
        'Study Combo Box
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("IssueID/" + txtIssueID.Text)
        params.Add("IssueTitle/" + txtIssueName.Text)
        params.Add("IssueText/" + txtDescription.Text)
        params.Add("UserName/" + UserName)
        ds = db.ExecuteStoredProc("Console." & "AddIssue", params)
        BuildCheckListTab("Incomplete")
        btnAddIssue.Enabled = True
        txtDescription.ReadOnly = True
    End Sub

    Private Sub btnCancelIssue_Click(sender As System.Object, e As System.EventArgs)
        btnAddIssue.Enabled = True
    End Sub

    Private Function BuildIDREmail(mfile As String) As String
        Dim TemplateValues As New SA.Console.WordTemplate()

        Dim Coloc As String

        TemplateValues.Field.Add("_TodaysDate")
        TemplateValues.RField.Add(Format(Now, "dd-MMM-yy"))


        Dim Row As DataRow
        Dim params As List(Of String)
        ' Company Name
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetTSortData", params)

        If ds.Tables.Count > 0 Then
            Row = ds.Tables(0).Rows(0)

            TemplateValues.Field.Add("_Company")
            TemplateValues.RField.Add(Row("Company").ToString)


            TemplateValues.Field.Add("_Refinery")
            TemplateValues.RField.Add(Row("Location").ToString)
            Coloc = Row("Coloc").ToString

        End If

        Dim strFaxName As String
        Dim strFaxEmail As String


        ' Contact Name
        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetContactInfo", params)
        If ds.Tables.Count = 0 Then
            strFaxName = "NAME UNAVAILABLE"
            strFaxEmail = "EMAIL ADDRESS UNAVAILABLE"
        Else
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)
                strFaxName = Row("CoordName").ToString
                strFaxEmail = Row("CoordEmail").ToString
            End If
        End If



        TemplateValues.Field.Add("_Contact")
        TemplateValues.RField.Add(strFaxName)
        ' Contact Email Address

        TemplateValues.Field.Add("_EMail")
        TemplateValues.RField.Add(strFaxEmail)

        ' Consultant Nant
        params = New List(Of String)
        params.Add("Initials/" + UserName)
        ds = db.ExecuteStoredProc("Console." & "GetConsultant", params)
        Dim strName As String

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Row = ds.Tables(0).Rows(0)

                If Row("ConsultantName") Is Nothing Or IsDBNull(Row("ConsultantName")) Then
                    strName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
                Else
                    strName = Row("ConsultantName").ToString
                End If
            Else

                strName = InputBox("I cannot find you in the employee table, please give me your name (Contact Rogge Heflin to add name): ", "Employee data base")
            End If
        End If


        TemplateValues.Field.Add("_ConsultantName")
        TemplateValues.RField.Add(strName)
        ' Consultant Initials

        TemplateValues.Field.Add("_Initials")

        TemplateValues.RField.Add(UserName)
        Utilities.WordTemplateReplace(TemplatePath & mfile, TemplateValues, TempPath & "SecureSend\" & mfile, 0)
        Return TempPath & mfile
    End Function
    Private Sub btnSecureSend_Click_1(sender As System.Object, e As System.EventArgs) Handles btnSecureSend.Click
        If Not Directory.Exists(TempPath & "SecureSend\") Then Directory.CreateDirectory(TempPath & "SecureSend\")
        Dim ss As New SecureSend(StudyType, tvwCorrespondence, tvwDrawings, tvwClientAttachments, tvwCompCorr, tvwCorrespondence2, tvwDrawings2, tvwClientAttachments2, tvwCompCorr2, Nothing, Nothing, UserName, Password)


        ss.IDRFile = ""
        If chkIDR.Checked Then

            If System.IO.File.Exists(TemplatePath & StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc") Then
                Dim NewIDR As String = BuildIDREmail(StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc")
                'FileCopy(TemplatePath & StudyType & " IDR Instructions.doc", TempPath & NewIDR)
                ss.IDRFile = TempPath & "SecureSend\" & StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc"
            Else
                MessageBox.Show(TemplatePath & StudyRegion & StudyYear.Substring(2, 2) & " IDR Instructions.doc is missing.", "Missing")
            End If
        End If



        ss.CompanyContactName = lblRefCoName.Text
        ss.RefNum = RefNum
        ss.LubeRefNum = LubRefNum
        ss.Study = StudyType
        ss.TemplatePath = TemplatePath
        ss.CorrPath = CorrPath
        ss.DrawingPath = DrawingPath
        ss.ClientAttachmentsPath = ClientAttachmentsPath
        ss.Loc = Company
        ss.User = UserName
        ss.PCCEmail = lblPCCEmail.Text
        ss.StudyType = cboStudy.Text
        ss.CompCorrPath = CompCorrPath
        ss.CorrPath2 = CorrPath
        ss.DrawingPath2 = DrawingPath
        ss.ClientAttachmentsPath2 = ClientAttachmentsPath
        ss.SendIDR = chkIDR.Checked
        ss.CompCorrPath2 = CompCorrPath

        ss.TempPath = TempPath & "SecureSend\"

        ss.CompanyPassword = CompanyPassword

        ss.Show()


    End Sub


   

    Private Sub cboRefNum_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs)
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True

            cboRefNum.SelectedIndex = FindString(cboRefNum, cboRefNum.Text)

            'If cboRefNum.SelectedIndex <> -1 Then
            '    RefNumChanged(cboRefNum.Text)
            'Else
            '    cboRefNum.SelectedIndex = cboRefNum.FindString(CurrentRefNum)
            'End If
        End If

    End Sub



    Private Sub btnSection_Click(sender As System.Object, e As System.EventArgs) Handles btnSection.Click
        If btnSection.Text = "Section" Then
            btnSection.Text = "Revision"
            ResetGradeTab(2)
        Else
            btnSection.Text = "Section"
            ResetGradeTab(1)
        End If
    End Sub

    Private Sub btnFilesSave_Click(sender As System.Object, e As System.EventArgs) Handles btnFilesSave.Click

        Dim CopyFile As String
        Dim Dest As String
        Dim DestFile As String
        Dim ext As String
        Dim success As Boolean = True
        Try

            CompanyPassword = btnCompanyPW.Text
            For I = 0 To dgFiles.Rows.Count - 1

                Dest = dgFiles.Rows(I).Cells(2).Value
                If Dest <> "Do Not Save" Then
                    lblStatus.Text = "Saving " & dgFiles.Rows(I).Cells(0).Value & "...."
                    Try
                        ext = GetExtension(dgFiles.Rows(I).Cells(0).Value)
                    Catch
                        ext = ""
                    End Try

                    CopyFile = TempPath & FixFilename(ext, dgFiles.Rows(I).Cells(0).Value)

                    DestFile = FixFilename(ext, dgFiles.Rows(I).Cells(1).Value)

                    Select Case (Dest)
                        Case "Correspondence"
                            sDir(I) = CorrPath
                        Case "Drawings"
                            sDir(I) = DrawingPath
                            DestFile = Utilities.AddDateStamp(DestFile)
                        Case "Company"
                            sDir(I) = CompCorrPath
                        Case Else
                            sDir(I) = GeneralPath

                    End Select
                    If (ext.ToUpper = "DOC" Or ext.ToUpper = "DOCX" Or ext.ToUpper.Substring(0, 3) = "XLS") Then
                        If CompanyPassword.Length = 0 Then
                            MessageBox.Show("Must have a Company Password to Encrypt files", "Save Error")
                            Exit Sub
                        End If

                        success = RemovePassword(CopyFile, DestFile, GetReturnPassword(RefNum, "fuelslubes"), CompanyPassword)
                        If success Then
                            File.Copy(CopyFile, sDir(I) & DestFile, True)
                            File.Delete(CopyFile)
                        Else
                            MessageBox.Show("Appears the password is incorrect for " & CopyFile & ".  Make sure this is the correct company: " & Company)
                            Exit Sub
                        End If




                    Else
                        File.Copy(CopyFile, sDir(I) & DestFile, True)
                        File.Delete(CopyFile)
                    End If
                End If
            Next

            If optCompanyCorr.Checked Then
                File.Copy(TempPath & "Email.txt", CompCorrPath & Utilities.CleanFileName(txtMessageFilename.Text), True)
            Else
                File.Copy(TempPath & "Email.txt", CorrPath & Utilities.CleanFileName(txtMessageFilename.Text), True)
            End If

            File.Delete(TempPath & "Email.txt")


            dgFiles.Rows.Clear()

            'Dim d As DialogResult = MessageBox.Show("Email and attachments saved.  Would you like to open the correspondence directory?", "Open Folder", MessageBoxButtons.YesNo)
            'If d = System.Windows.Forms.DialogResult.Yes Then Process.Start(CorrPath)
            txtMessageFilename.Text = ""
            btnFilesSave.Enabled = False
            If OldTempPath.Length > 0 Then TempPath = OldTempPath
            lblStatus.Text = "Attachments Saved"
            ConsoleTabs.TabPages.Remove(tabDD)
            ConsoleTabs.SelectedIndex = 1
        Catch ex As System.Exception
            Err.WriteLog("btnSaveFiles", ex)
            lblStatus.Text = "Error saving files: " & ex.Message
        End Try
    End Sub





    Private Sub cboDir_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir.SelectedIndexChanged
        Select Case cboDir.Text

            Case "Refinery Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = True

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Client Attachments"
                tvwClientAttachments.Visible = True
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = False

            Case "Drawings"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = True
                tvwCompCorr.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments.Visible = False
                tvwCorrespondence.Visible = False

                tvwDrawings.Visible = False
                tvwCompCorr.Visible = True
        End Select
    End Sub
    Private Sub cboDir2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDir2.SelectedIndexChanged
        Select Case cboDir2.Text

            Case "Refinery Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = True

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Client Attachments"
                tvwClientAttachments2.Visible = True
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = False

            Case "Drawings"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = True
                tvwCompCorr2.Visible = False
            Case "Company Correspondence"
                tvwClientAttachments2.Visible = False
                tvwCorrespondence2.Visible = False

                tvwDrawings2.Visible = False
                tvwCompCorr2.Visible = True
        End Select
    End Sub




    Private Sub btnFLComp_Click(sender As System.Object, e As System.EventArgs)
        File.Copy(IDRPath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls", CorrPath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls")
        Process.Start(CorrPath & "Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls")
    End Sub

    Private Sub btnTP_Click(sender As System.Object, e As System.EventArgs)
        File.Copy(IDRPath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls", CorrPath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls")
        Process.Start(CorrPath & "Transfer Pricing " & StudyYear.Substring(2, 2) & ".xls")
    End Sub


    Private Sub btnOpenWord_Click(sender As System.Object, e As System.EventArgs)
        Dim msword As New Word.Application
        Dim docPN As New Word.Document
        Try
            If Dir(CorrPath & "PN_" & cboCompany.Text & ".doc") = "" Then
                If MsgBox("Do you want to create PN_" & cboCompany.Text & " .doc and stop updating the IDR Notes / Presenter's Notes field through Console?", vbYesNoCancel + vbDefaultButton2, "Click Yes to create the file.") = vbYes Then
                    'MsgBox "Create the file, lock the field, open the file with word."

                    Dim strWork As String = "~ " & Now & " The Presenters Notes document has been built so this field is locked. Please use the Presenters Notes button to open the file. " & Chr(13) & Chr(10) & "----------" & Chr(13) & Chr(10)

                    ' Add the tilde to the Issues Notes field
                    ' First, see if a record exists for this refnum
                    Dim row As DataRow
                    Dim params As New List(Of String)
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

                    If ds.Tables.Count > 0 Then
                        row = ds.Tables(0).Rows(0)

                        txtNotes.Text = row("ValidationNotes").ToString


                    Else

                        params = New List(Of String)
                        params.Add("RefNum/" + RefNum)
                        params.Add("Notes/" + txtNotes.Text)

                        Dim Count As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)

                    End If

                    Dim objWriter As New System.IO.StreamWriter(TempPath & "PNotes.txt", True)
                    objWriter.WriteLine(RefNum)
                    objWriter.WriteLine(txtNotes.Text)
                    objWriter.Close()

                    ' Create the document.
                    File.Copy(TemplatePath & "PN_Template.doc", TempPath & "PN_Template.doc", True)
                    msword = New Word.Application
                    'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                    docPN = msword.Documents.Open(TempPath & "PN_Template.doc")

                    docPN.SaveAs(CorrPath & "PN_" & cboCompany.Text & ".doc")

                    ' Put the CoLoc and the Notes into the document.
                    msword.Run("SubsAuto")
                End If
            Else

                msword = New Word.Application
                'Set docPN = MSWord.Documents.Open(gstrtempfolder & "PNotes.txt")

                docPN = msword.Documents.Open(CorrPath & "PN_" & cboCompany.Text & ".doc")
                msword.Visible = True
            End If

        Catch Ex As System.Exception
            Err.WriteLog("btnOpenWord", Ex)
        End Try


    End Sub




    Private Sub btnFuelLube_Click(sender As System.Object, e As System.EventArgs) Handles btnFuelLube.Click
        Dim StudyType As String = ParseRefNum(btnFuelLube.Text, 2).Trim
        Dim StudyYear As String = ParseRefNum(btnFuelLube.Text, 3).Trim.Substring(0, 2)
        ReferenceNum = btnFuelLube.Text
        RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
        cboStudy.SelectedIndex = cboStudy.FindString(StudyType & StudyYear)
        AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
        ChangedStudy(ReferenceNum)
        GetRefNums(cboRefNum.Text)
        CheckFuelsLubes()
    End Sub
    Private Sub MainConsole_DragDrop(ByVal sender As Object, _
                        ByVal e As System.Windows.Forms.DragEventArgs) _
                        Handles MyBase.DragDrop, ConsoleTabs.DragDrop

        If Not ConsoleTabs.TabPages.Contains(tabDD) Then ConsoleTabs.TabPages.Add(tabDD)
        ConsoleTabs.SelectTab(tabDD)
        Dim strFile As String = String.Empty
        Dim ext As String
        Dim zext As String
        Dim strNextNum = NextVF().ToString
        Dim strNextRetNum = NextReturnFile().ToString
        Dim SavedFiles As New List(Of String)
        Dim strFileName As String
        Dim ReturnFile As Boolean = False
        Dim OrigFile As Boolean = False
        Dim NextLetter As String = ""

        If cboRefNum.Text <> "" Then
            Dim strNumModifier = FindModifier(strNextNum)
            Try
                Dim filenames() As String = Directory.GetFiles(TempPath, "*.*")
                For Each sFile In filenames
                    Try
                        File.Delete(sFile)
                    Catch ex As System.Exception
                        Err.WriteLog("DragDrop", ex)
                        Debug.Print(ex.Message)
                    End Try
                Next

                Me.ConsoleTabs.SelectedIndex = 9


                If dgFiles.Rows.Count = 0 Then
                    If Not e.Data.GetDataPresent(DataFormats.Text) = False Then
                        'check if this is an outlook message. The outlook messages, all contain a FileContents attribute. If not, exit.
                        Dim formats() As String = e.Data.GetFormats()

                        If formats.Contains("FileContents") = False Then Exit Sub

                        'they are dragging the attachment
                        If (e.Data.GetDataPresent("Object Descriptor")) Then
                            Dim app As New Microsoft.Office.Interop.Outlook.Application() ' // get current selected items
                            Dim selection As Microsoft.Office.Interop.Outlook.Selection
                            Dim myText As String = ""
                            selection = app.ActiveExplorer.Selection
                            If selection IsNot Nothing Then

                                Dim mailItem As Outlook._MailItem
                                For i As Integer = 0 To selection.Count - 1

                                    mailItem = TryCast(selection.Item(i + 1), Outlook._MailItem)

                                    If mailItem IsNot Nothing Then
                                        Try

                                            myText = "DATE SENT: " & mailItem.SentOn.ToString() & vbCrLf & vbCrLf

                                            myText += e.Data.GetData("Text")  'header text
                                            myText += vbCrLf + vbCrLf
                                            myText += mailItem.Body  'Plain Text Body Message


                                            'now save the attachments with the same file name and then 1,2,3 next to it
                                            For Each att As Attachment In mailItem.Attachments
                                                btnFilesSave.Enabled = True
                                                If att.FileName.Substring(0, 5).ToUpper <> "IMAGE" Then
                                                    Try
                                                        ext = GetExtension(att.FileName)
                                                       
                                                    Catch
                                                        ext = ""
                                                    End Try

                                                    If ext.ToUpper = "ZIP" Then
                                                        If Dir(TempPath & "Extracted Files\") = "" Then Directory.CreateDirectory(TempPath & "\Extracted Files\")
                                                        Dim zfilenames() As String = Directory.GetFiles(TempPath & "Extracted Files", "*.*")
                                                        For Each sFile In zfilenames
                                                            Try
                                                                File.Delete(sFile)
                                                            Catch ex As System.Exception
                                                                Err.WriteLog("DragDrop", ex)
                                                                Debug.Print(ex.Message)
                                                            End Try
                                                        Next

                                                        att.SaveAsFile(TempPath & "Extracted Files\" & att.FileName)
                                                        Utilities.UnZipFiles(TempPath & "Extracted Files\" & att.FileName, TempPath, CompanyPassword)
                                                        File.Delete(TempPath & "Extracted Files\" & att.FileName)
                                                        OldTempPath = TempPath & "Extracted Files\"

                                                        For Each mfile In Directory.GetFiles(OldTempPath)
                                                            mfile = Utilities.ExtractFileName(mfile)
                                                            Dim zr = dgFiles.Rows.Add()

                                                            Dim zcbcell = New DataGridViewComboBoxCell
                                                            zcbcell.Items.Add("Do Not Save")
                                                            zcbcell.Items.Add("Correspondence")
                                                            zcbcell.Items.Add("Company")
                                                            zcbcell.Items.Add("Drawings")
                                                            zcbcell.Items.Add("General")
                                                            zcbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton
                                                            dgFiles.Rows(zr).Cells(2) = zcbcell
                                                            zext = GetExtension(mfile)
                                                            Select Case (zext.ToUpper)
                                                                Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM"
                                                                    zcbcell.Value = "Correspondence"
                                                                Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG", "VSO"
                                                                    zcbcell.Value = "Drawings"

                                                                Case Else
                                                                    zcbcell.Value = "General"

                                                            End Select
                                                            File.Copy(OldTempPath & strFile, TempPath & strFile)
                                                            dgFiles.Rows(zr).Cells(0).Value = mfile
                                                            If mfile.Contains("VFFL") Then
                                                                strFile = mfile
                                                                dgFiles.Rows(zr).Cells(1).Value = "VRFL" & NextFile(CorrPath & "VRFL", "doc") & "-" & mfile.Substring(6, Len(mfile) - 6)
                                                            Else

                                                                If mfile.Substring(0, 2).ToUpper() = "VF" Then
                                                                    strFile = mfile
                                                                    dgFiles.Rows(zr).Cells(1).Value = "VR" & NextVR() & "-" & mfile.Substring(4, Len(mfile) - 4)
                                                                Else
                                                                    strFile = mfile.Substring(0, mfile.Length - 4) & "." & zext
                                                                End If
                                                            End If

                                                            If mfile.ToUpper.Contains("RETURN") Then
                                                                dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & strNextRetNum & "." & zext
                                                            Else
                                                                dgFiles.Rows(zr).Cells(1).Value = mfile.Substring(0, mfile.Length - 4) & "." & zext
                                                            End If




                                                        Next

                                                    Else
                                                        Dim mfile As String
                                                        Dim r = dgFiles.Rows.Add()
                                                        dgFiles.Rows(r).Cells(0).Value = att.FileName
                                                        mfile = att.FileName
                                                        Dim cbcell = New DataGridViewComboBoxCell

                                                        cbcell.Items.Add("Do Not Save")
                                                        cbcell.Items.Add("Correspondence")
                                                        cbcell.Items.Add("Company")
                                                        cbcell.Items.Add("Drawings")
                                                        cbcell.Items.Add("General")
                                                        cbcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton

                                                        dgFiles.Rows(r).Cells(2) = cbcell


                                                        Select Case (ext.ToUpper.Substring(0, 3))
                                                            Case "DOC", "XLS", "PDF", "XL", "TXT", "OCX", "XLSX", "DOCX", "DOCM"
                                                                cbcell.Value = "Correspondence"


                                                            Case "JPG", "GIF", "BMP", "JPEG", "PSD", "TIF", "PNG"
                                                                cbcell.Value = "Drawings"

                                                            Case Else
                                                                cbcell.Value = "General"


                                                        End Select
                                                        

                                                    att.SaveAsFile(TempPath & mfile)

                                                        If mfile.Contains("VFFL") Then
                                                            strFile = "VRFL" & NextVRFL() & "-" & mfile.Substring(6, Len(mfile) - 6)
                                                            dgFiles.Rows(r).Cells(1).Value = strFile
                                                        Else
                                                            If mfile.Substring(0, 4).ToUpper().Contains("VF") Then
                                                                ReturnFile = True
                                                                strFile = "VR" & NextVR() & "-" & mfile.Substring(4, Len(mfile) - 4)
                                                                dgFiles.Rows(r).Cells(1).Value = strFile
                                                            Else
                                                                strFile = mfile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                                                                If strFile.ToUpper.Contains("RETURN") Then
                                                                    ReturnFile = True
                                                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & strNextRetNum & "." & ext
                                                                Else
                                                                    dgFiles.Rows(r).Cells(1).Value = strFile.Substring(0, mfile.Length - ext.Length - 1) & "." & ext
                                                                End If
                                                            End If
                                                        End If
                                                        mfile = dgFiles.Rows(r).Cells(1).Value
                                                    End If
                                                End If
                                            Next
                                            Dim strEmailFile As String
                                            dgFiles.EditMode = DataGridViewEditMode.EditOnEnter
                                            'save the mail message

                                            strEmailFile = TempPath & "email.txt"
                                            Dim strw As New StreamWriter(strEmailFile, False)
                                            strw.WriteLine(myText)
                                            strw.Close()
                                            strw.Dispose()

                                            strFileName = cboCompany.Text


                                            ' More often, use this technique to name the text file.
                                            If strFileName.Contains("VFFL") Then
                                                txtMessageFilename.Text = "VRFL" & NextVRFL() & " " & mailItem.Subject.Trim.Replace(":", " ") & ".txt"
                                            Else
                                                If strFileName.Contains("VF") Then

                                                    txtMessageFilename.Text = "VR" & NextVR() & " " & mailItem.Subject.Trim.Replace(":", " ") & ".txt"
                                                Else
                                                    If OrigFile Then
                                                        txtMessageFilename.Text = mailItem.Subject.Trim.Replace(":", " ") & "-" & NextLetter & ".txt"
                                                    Else
                                                        txtMessageFilename.Text = mailItem.Subject.Trim.Replace(":", " ") & ".txt"
                                                    End If
                                                End If
                                            End If





                                        Catch ex As System.Exception
                                            Err.WriteLog("DragDrop", ex)
                                        Finally
                                            Marshal.ReleaseComObject(mailItem)
                                        End Try

                                    End If
                                Next
                            End If

                        End If
                    End If
                End If
            Catch ex As System.Exception
                Err.WriteLog("DragDrop", ex)
                MessageBox.Show(ex.Message)
            End Try
            If txtMessageFilename.Text.Length > 0 Then btnFilesSave.Enabled = True
        End If
    End Sub

    Private Function GetFileName(fn As String) As String
        Return fn.Substring(0, fn.Length - 4)
    End Function
    Private Function GVNextFile(findFile As String, findExt As String) As String
        Dim rtn As Integer = 1
        Dim result As Integer
        For Each r As DataGridViewRow In dgFiles.Rows

            If r.Cells(0).Value.ToString Like findFile & "*." & findExt Then
                If Not r.Cells(1).Value Is Nothing Then
                    Dim tf As String = GetFileName(r.Cells(1).Value).ToString.Substring(GetFileName(r.Cells(1).Value).ToString.Length - 1, 1)
                    If Int32.TryParse(tf, result) Then rtn = rtn + 1

                End If
            End If
        Next
        Return rtn.ToString
    End Function
    Private Function IncrementFileNumber(strFile As String, ext As String) As String

        Dim NewFile As String = strFile & "-A." & ext

        Dim Letters As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        For Each letter In Letters
            If File.Exists(CorrPath & strFile & "-" & letter & ext) Then
                NewFile = strFile & "-" & Chr(Asc(letter) + 1)
            End If
        Next


        Return NewFile


    End Function



    Private Sub dgFiles_CellEnter(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgFiles.CellEnter
        If e.ColumnIndex = 1 Then
            SendKeys.Send("{F4}")
        End If
        SavedFile = dgFiles.CurrentCell.Value
    End Sub

    Private Sub btnGVClear_Click(sender As System.Object, e As System.EventArgs) Handles btnGVClear.Click
        dgFiles.RowCount = 1
        dgFiles.Rows.Clear()
        For Each foundFile As String In Directory.GetFiles(TempPath)
            File.Delete(foundFile)
        Next
        txtMessageFilename.Text = ""
        btnFilesSave.Enabled = False
    End Sub
    Private Sub tvwCorrespondence_BeforeExpand(sender As System.Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles tvwCorrespondence.BeforeExpand, tvwDrawings.BeforeExpand, tvwClientAttachments.BeforeExpand, tvwCompCorr.BeforeExpand, tvwCorrespondence2.BeforeExpand, tvwDrawings2.BeforeExpand, tvwClientAttachments2.BeforeExpand, tvwCompCorr2.BeforeExpand
        e.Node.Nodes.Clear()
        ' get the directory representing this node
        Dim mNodeDirectory As IO.DirectoryInfo
        mNodeDirectory = New IO.DirectoryInfo(e.Node.Tag.ToString)

        ' add each subdirectory from the file system to the expanding node as a child node
        For Each mDirectory As IO.DirectoryInfo In mNodeDirectory.GetDirectories
            ' declare a child TreeNode for the next subdirectory
            Dim mDirectoryNode As New TreeNode
            ' store the full path to this directory in the child TreeNode's Tag property
            mDirectoryNode.Tag = mDirectory.FullName
            ' set the child TreeNodes's display text
            mDirectoryNode.Text = mDirectory.Name
            ' add a dummy TreeNode to this child TreeNode to make it expandable
            mDirectoryNode.Nodes.Add("*DUMMY*")
            mDirectoryNode.ImageKey = CacheShellIcon(mDirectoryNode.Tag)
            mDirectoryNode.SelectedImageKey = mDirectoryNode.ImageKey & "-open"
            ' add this child TreeNode to the expanding TreeNode
            e.Node.Nodes.Add(mDirectoryNode)
        Next

        ' add each file from the file system that is a child of the argNode that was passed in
        For Each mFile As IO.FileInfo In mNodeDirectory.GetFiles
            ' declare a TreeNode for this file
            Dim mFileNode As New TreeNode
            ' store the full path to this file in the file TreeNode's Tag property
            mFileNode.Tag = mFile.FullName
            ' set the file TreeNodes's display text
            mFileNode.Text = mFile.Name
            mFileNode.ImageKey = CacheShellIcon(mFileNode.Tag)
            mFileNode.SelectedImageKey = mFileNode.ImageKey
            mFileNode.SelectedImageKey = mFileNode.ImageKey & "-open"
            ' add this file TreeNode to the TreeNode that is being populated
            e.Node.Nodes.Add(mFileNode)
        Next

    End Sub

    Private Sub tvwCorrespondence_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvwCorrespondence.NodeMouseDoubleClick, tvwDrawings.NodeMouseDoubleClick, tvwClientAttachments.NodeMouseDoubleClick, tvwCompCorr.NodeMouseDoubleClick, tvwCorrespondence2.NodeMouseDoubleClick, tvwDrawings2.NodeMouseDoubleClick, tvwClientAttachments2.NodeMouseDoubleClick, tvwCompCorr2.NodeMouseDoubleClick
        ' try to open the file
        Try
            Process.Start(e.Node.Tag)
        Catch ex As System.Exception
            Err.WriteLog("tvwCorrespondence_NodeMouseDoubleClick", ex)
            MessageBox.Show("Error opening file: " & ex.Message)
        End Try
    End Sub
    Private Sub MainConsole_DragEnter(ByVal sender As Object, _
                           ByVal e As System.Windows.Forms.DragEventArgs) _
                           Handles MyBase.DragEnter, ConsoleTabs.DragEnter, tvwCorrespondence.DragEnter, tvwDrawings.DragEnter, tvwCompCorr.DragEnter, tvwCompCorr2.DragEnter, tvwClientAttachments.DragEnter, tvwCorrespondence2.DragEnter, tvwDrawings2.DragEnter, tvwClientAttachments2.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        ElseIf (e.Data.GetDataPresent("FileGroupDescriptor")) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub
    Private Sub btnAddIssue_Click(sender As System.Object, e As System.EventArgs) Handles btnAddIssue.Click

        txtIssueID.Text = ""
        txtIssueName.Text = ""

        txtDescription.Text = ""
        txtIssueID.Focus()
        btnAddIssue.Enabled = False

        'IssueForm.DBConn = mDBConnection
        'IssueForm.RefNum = RefNum
        'IssueForm.StudyType = StudyType
        'IssueForm.UserName = mUserName
        'IssueForm.Show()
        txtDescription.ReadOnly = False

    End Sub


    Private Sub lstVFNumbers_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstVFNumbers.Click

        SetCorr()

    End Sub

    Private Sub btnHelp_Click(sender As System.Object, e As System.EventArgs) Handles btnHelp.Click



        Dim strHelpFilePath As String

        strHelpFilePath = My.Settings.Item("HelpPath")

        If File.Exists(strHelpFilePath) Then
            Process.Start(strHelpFilePath)
        Else
            MessageBox.Show("Help file missing", "Missing")
        End If
    End Sub

    Private Sub btnOpenValFax_Click(sender As System.Object, e As System.EventArgs)

        ' This routine will look for a personal ValFax for this user.
        ' If it does not exist, give a message to the user.
        ' If it does exist, open it in Word
        Dim docMyValFax As Word.Document

        Dim strTemplateFile As String
        Dim strDirResult As String

        'On Error GoTo MyValFaxError

        ' If they have a "WithSubstitutions" template,
        ' Call ValFax_Build and do not do anything else in this routine.
        strTemplateFile = Dir(TemplatePath & "MyValFax\WithSubstitutions\" & mUserName & ".doc")
        If strTemplateFile > "" Then
            ValFax_Build(strTemplateFile)
            Exit Sub
        End If

        ' No WithSubtitutions template, look for a vanilla template.
        strDirResult = Dir(TemplatePath & "MyValFax\" & mUserName & ".doc")
        If strDirResult = "" Then
            MsgBox("No MyValFax was found for " & mUserName & ". Please create the Word document " & TemplatePath & "MyValFax\" & mUserName & ".doc. See Joe Waters if you have questions.", vbOKOnly)
            'gblnTemplateOK = False
            Exit Sub
        End If

        ' Found a vanilla template, process it.
        strTemplateFile = strDirResult
        Dim MSWord As New Word.Application

        ' Make Word visible
        MSWord.Visible = True
        ' Open the document
        ' This was .Add which added a new document based on a .dot
        ' Changed it to open followed by .Run of AutoNew.


        ' Open the document
        docMyValFax = MSWord.Documents.Open(TemplatePath & "MyValFax\" & strTemplateFile)

        ' Cut the connection.
        MSWord = Nothing


    End Sub

    Private Sub btnLog_Click(sender As System.Object, e As System.EventArgs)
        ' These are the "file" buttons on the right of the
        ' Validation Summary tab.
        ' Basically, here we just open the path/file stored in the tag
        ' property of the button.
        ' This property was set by SetFileButtons earlier when the Refnum
        ' was set/changed.
        Dim wb As Object
        Dim xl As New Excel.Application


        Try

            xl.Visible = True
            wb = xl.Workbooks.Open("K:\Study\" & "Console." & StudyType & "\" & StudyYear & "\" & cboStudy.SelectedItem.ToString.Substring(0, 3) & "\Refinery\" & RefNum & "\" & RefNum & "_Log.xls")
            xl.Visible = True

            ' Next line is important.
            ' If you don't do this, if the user closes Excel then clicks a file button
            ' again, you will get an Excel window with just the 'frame'. The inside
            ' of the 'window' will be empty!
            xl = Nothing

        Catch ex As System.Exception

            MsgBox("Unable to open file. If you have Excel open, and are editing a cell, get out of that mode and retry. ", vbOKOnly, "Unable to open file")
            Err.WriteLog("btnLOG_Click", ex)
        End Try
    End Sub


    Private Sub MainConsole_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Initialize()
    End Sub

    Private Sub MainConsole_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Not Spawn Then SaveLastStudy()
        End
    End Sub
    Private Sub BuildGradeGrid()

        Dim params As List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Consultant/" & cboConsultant.SelectedItem)
        ds = db.ExecuteStoredProc("Console." & "GetValidatorData", params)
        If ds.Tables.Count > 0 Then dgGrade.DataSource = ds.Tables(0)

    End Sub

    Private Sub lstVFFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstVFFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstVFFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVFFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVFFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub lstVRFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstVRFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstVRFiles.SelectedItem.ToString.IndexOf("|")
        file = lstVRFiles.SelectedItem.ToString.Substring(FindTab + 1, lstVRFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub lstReturnFiles_DoubleClick(sender As System.Object, e As System.EventArgs) Handles lstReturnFiles.DoubleClick

        Dim file As String
        Dim FindTab As String = lstReturnFiles.SelectedItem.ToString.IndexOf("|")
        file = lstReturnFiles.SelectedItem.ToString.Substring(FindTab + 1, lstReturnFiles.SelectedItem.ToString.Length - FindTab - 1)
        Process.Start(CorrPath & file.Trim)

    End Sub

    Private Sub btnReceiptAck_Click(sender As System.Object, e As System.EventArgs) Handles btnReceiptAck.Click

        Dim strDirResult As String
        Dim strDateTimeToPrint As String

        strDirResult = Dir(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt")
        If strDirResult <> "" Then
            MsgBox("Receipt of VF" & Me.lstVFNumbers.Text & " has already been noted.")
            Exit Sub
        End If

        strDateTimeToPrint = InputBox("Enter the date/time that you want to show. Click OK to use present.", "Now or earlier?", Now())

        If strDateTimeToPrint <> "" Then

            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)

            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strDateTimeToPrint)
            objWriter.Close()

            MsgBox("VA" & Me.lstVFNumbers.Text & ".txt has been built.")
        Else
            MsgBox("Receipt Acknowledgement was canceled.")
        End If

    End Sub

    Private Sub btnBuildVRFile_Click(sender As System.Object, e As System.EventArgs)

        Dim strDirResult As String
        Dim strTextForFile As String

        strDirResult = Dir(CorrPath & "VR" & Me.lstVFNumbers.Text & ".txt")
        If strDirResult <> "" Then
            MsgBox("There already is a VR" & Me.lstVFNumbers.Text & " file.")
            Exit Sub
        End If

        strTextForFile = InputBox("Enter the text for the VR file.", "Enter the client's response.")

        If strTextForFile <> "" Then
            Dim objWriter As New System.IO.StreamWriter(CorrPath & "VA" & Me.lstVFNumbers.Text & ".txt", True)
            objWriter.WriteLine(mUserName)
            objWriter.WriteLine(strTextForFile)
            objWriter.Close()
            MsgBox("VR" & Me.lstVFNumbers.Text & ".txt has been built.")
        Else
            MsgBox("VR file build was cancelled.")
        End If

    End Sub



    Private Sub ValCheckList_ItemCheck(sender As Object, e As System.Windows.Forms.ItemCheckEventArgs) Handles ValCheckList.ItemCheck

        Try
            If e.CurrentValue = CheckState.Checked Then
                e.NewValue = CheckState.Unchecked
                UpdateIssue("N")
            Else
                e.NewValue = CheckState.Checked
                UpdateIssue("Y")
            End If

        Catch ex As System.Exception
            Err.WriteLog("ValCheckList_ItemCheck", ex)
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ValCheckList_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ValCheckList.SelectedIndexChanged

        If Not ValCheckList.SelectedItem Is Nothing Then

            Dim row As DataRow
            Dim params As List(Of String)

            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("IssueTitle/" & Me.ValCheckList.SelectedItem.ToString.Trim)
            ds = db.ExecuteStoredProc("Console." & "GetAnIssue", params)


            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then

                    row = ds.Tables(0).Rows(0)

                    txtIssueID.Text = row("IssueID").ToString
                    txtIssueName.Text = row("IssueTitle").ToString

                    txtDescription.Text = row("IssueText").ToString

                End If


            End If

        End If



    End Sub

    Public Sub SetTree(tvw As TreeView, mRootPath As String)
        Try
            tvw.Nodes.Clear()
            Dim files() As String = Directory.GetFiles(mRootPath)
            Dim nf As String
            Dim mRootNode As New TreeNode
            For Each f In files
                nf = Path.GetFileName(f)
                If f.Substring(f.Length - 9, 9) <> "Thumbs.db" Then
                    If f.Substring(f.Length - 3, 3) = "lnk" Then
                        f = Utilities.GetFileFromLink(f)
                    End If
                    mRootNode = tvw.Nodes.Add(nf.Replace(" - Shortcut.lnk", "").Replace(".lnk", ""))
                    mRootNode.ImageKey = CacheShellIcon(f)
                    mRootNode.SelectedImageKey = mRootNode.ImageKey & "-open"
                    mRootNode.Tag = f
                End If
            Next

            tvw.ExpandAll()
            tvw.Refresh()
        Catch ex As System.Exception
            Err.WriteLog("SetTree", ex)
        End Try
    End Sub

    Private Sub btnUpdateNotes_Click(sender As System.Object, e As System.EventArgs)


        Dim bIssues As Boolean = False

        Dim params As New List(Of String)
        params.Add("RefNum/" + RefNum)
        ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

        If ds.Tables.Count = 0 Then
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtNotes.Text))

            ds = db.ExecuteStoredProc("Console." & "InsertValidationNotes2014", params)

        Else
            params = New List(Of String)
            params.Add("RefNum/" + RefNum)
            params.Add("Notes/" & Utilities.FixQuotes(txtNotes.Text))

            ds = db.ExecuteStoredProc("Console." & "UpdateValidationNotes2014", params)
        End If

        params = New List(Of String)
        params.Add("RefNum/" + RefNum)


        ds = db.ExecuteStoredProc("Console." & "UpdateComments", params)


        BuildSummaryTab()



    End Sub



    Private Sub UpdateCheckList(Optional status As String = "N")

        Dim I As Integer
        Dim row As DataRow
        Dim params As List(Of String)
        Dim intCompleteCount As Integer, intNotCompleteCount As Integer
        Dim node As TreeNode
        RemoveHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck

        ValCheckList.Items.Clear()
        tvIssues.Nodes.Clear()

        ' Load the Uncompleted Checklist Item count label
        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Mode/Y")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Mode/N")
        ds = db.ExecuteStoredProc("Console." & "GetIssueCounts", params)


        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                intNotCompleteCount = Convert.ToInt32(ds.Tables(0).Rows(0).Item("Issues"))
            End If
        End If

        lblItemCount.Text = intCompleteCount & " Completed    " & intNotCompleteCount & " Not Complete."




        params = New List(Of String)
        params.Add("RefNum/" + cboRefNum.Text)
        params.Add("Completed/Y")
        ds = db.ExecuteStoredProc("Console." & "GetIssues", params)
        If ds.Tables(0).Rows.Count > 0 Then
            For I = 0 To ds.Tables(0).Rows.Count - 1
                row = ds.Tables(0).Rows(I)

                node = tvIssues.Nodes.Add(Utilities.Pad(row("IssueID"), 8) & "-" & row("IssueTitle"))
                If row("Completed") = "Y" Then
                    node.Checked = True
                Else
                    node.Checked = False
                End If

            Next

        End If




        AddHandler tvIssues.AfterCheck, AddressOf tvIssues_AfterCheck

    End Sub
    Private Sub SetSummaryFileButtons()
        RefNum = cboRefNum.Text
        If RefNum <> "" Then

            If Directory.Exists(DrawingPath) Then
                btnDrawings.Enabled = True
                btnDrawings.Tag = DrawingPath
            Else
                btnDrawings.Enabled = False
            End If

            If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_UnitReview.xls") Then
                btnUnitReview.Tag = GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_UnitReview.xls"
                btnUnitReview.Enabled = True
            Else
                btnUnitReview.Enabled = False
            End If


            If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_SF.xls") Then
                btnSpecFrac.Enabled = True
                btnSpecFrac.Tag = GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_SF.xls"
            Else
                btnSpecFrac.Enabled = False
            End If


            If File.Exists(GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_HYC.xls") Then
                btnHYC.Enabled = True
                btnHYC.Tag = GeneralPath & ParseRefNum(RefNum, 1) & ParseRefNum(RefNum, 2) & "_HYC.xls"
            Else
                btnHYC.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_CT.xls") Then
                btnCT.Enabled = True
                btnCT.Tag = RefineryPath & RefNum & "_CT.xls"
            Else
                btnCT.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_PA.xls") Then
                btnPA.Enabled = True
                btnPA.Tag = RefineryPath & RefNum & "_PA.xls"
            Else
                btnPA.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_PT.xls") Then
                btnPT.Enabled = True
                btnPT.Tag = RefineryPath & RefNum & "_PT.xls"
            Else
                btnPT.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_RAM.xls") Then
                btnRAM.Enabled = True
                btnRAM.Tag = RefineryPath & RefNum & "_RAM.xls"
            Else
                btnRAM.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_SC.xls") Then
                btnSC.Enabled = True
                btnSC.Tag = RefineryPath & RefNum & "_SC.xls"
            Else
                btnSC.Enabled = False
            End If

            If File.Exists(RefineryPath & RefNum & "_VI.xls") Then
                btnVI.Enabled = True
                btnVI.Tag = RefineryPath & RefNum & "_VI.xls"
            Else
                btnVI.Enabled = False
            End If

            
        End If

    End Sub
    Private Sub PopulateCompCorrTreeView(tvw As TreeView)

        If Directory.Exists(CompCorrPath) Then
            SetTree(tvw, CompCorrPath)
        Else
            CompCorrPath = StudyDrive & StudyYear & "\" & StudyRegion + "\" & "Company Correspondence\" & CompanyName
            SetTree(tvw, CompCorrPath)
        End If

    End Sub
    Private Sub PopulateCorrespondenceTreeView(tvw As TreeView)

        If Directory.Exists(CorrPath) Then
            SetTree(tvw, CorrPath)
        Else
            CorrPath = StudyDrive & StudyYear & "\" & StudyRegion + "\" & "Correspondence\" & RefNum
            SetTree(tvw, CorrPath)
        End If

    End Sub


    Private Sub PopulateDrawingTreeView(tvw As TreeView)

        If Directory.Exists(DrawingPath) Then
            SetTree(tvw, DrawingPath)
        Else
            DrawingPath = StudyDrive & "\" & StudyType & "\" & StudyRegion & "\General\"
            SetTree(tvw, DrawingPath)
        End If

    End Sub
    Private Sub PopulateCATreeView(tvw As TreeView)

        If Directory.Exists(ClientAttachmentsPath) Then
            SetTree(tvw, ClientAttachmentsPath)
        End If

    End Sub
    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnJustLooking.Click

        LaunchValidation(True)

    End Sub
    Private Sub btnValidate_Click(sender As System.Object, e As System.EventArgs) Handles btnValidate.Click
        btnValidate.Enabled = False
        LaunchValidation(False)

    End Sub

    Private Sub LaunchValidation(JustLooking As Boolean)
        Try

            bJustLooking = JustLooking
            If cboConsultant.Text = "" Then
                MessageBox.Show("You have not assigned a consultant.")
                btnValidate.Enabled = True
            Else
                btnValidate.Enabled = False
                btnValidate.Text = "Reviewing"
                'Launch Validation
                If mStudyYear <> "2014" Then
                    ValidationFile = "FL" & StudyYear.Substring(2, 2) & "Macros.xls"
                Else
                    ValidationFile = "FL" & StudyYear.Substring(2, 2) & "Macros_IDR.xls"
                End If

                IDR(bJustLooking)
                btnValidate.Enabled = True
                btnValidate.Text = "IDR"

            End If
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "LaunchValidation")
            Err.WriteLog("LaunchValidation", ex)
        End Try

    End Sub
    Private Sub ChangedStudy(Optional mRefNum As String = "")

        If mRefNum = "" Then

            For I = 0 To 14
                If StudySave(I) = cboStudy.Text Then
                    GetRefNums(RefNumSave(I))
                    Exit Sub
                End If
            Next

        Else
            GetRefNums(mRefNum)
        End If


    End Sub
    'If Study Changes, then re-populate RefNums
    Private Sub cboStudy_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboStudy.SelectedIndexChanged
        ChangedStudy()
    End Sub

    'Method to populate the labels for Contacts
    Private Sub FillContacts()
        Try
            ClearFields()

            If Not RefineryContact Is Nothing Then

                lblRefCoName.Text = RefineryContact.FullName
                lblRefCoEmail.Text = RefineryContact.Email
                lblRefCoPhone.Text = RefineryContact.Phone
                lblMaintMgrName.Text = RefineryContact.MaintMgrName
                lblMaintMgrEmail.Text = RefineryContact.MaintEmail


                lblOpsMgrName.Text = RefineryContact.OpsMgrName
                lblOpsMgrEmail.Text = RefineryContact.OpsEmail

                lblTechMgrName.Text = RefineryContact.TechMgrName
                lblTechMgrEmail.Text = RefineryContact.TechEmail

                lblRefMgrName.Text = RefineryContact.RefMgrName
                lblRefMgrEmail.Text = RefineryContact.RefEmail


                lblRefAltName.Text = RefineryContact.AltFullName
                lblRefAltEmail.Text = RefineryContact.AltEmail
                lblRefAltPhone.Text = RefineryContact.AltPhone


            End If

            If Not InterimContact Is Nothing Then
                lblInterimName.Text = InterimContact.FirstName & " " & InterimContact.LastName
                lblInterimEmail.Text = InterimContact.Email
                lblInterimPhone.Text = InterimContact.Phone
            End If




            lblName.Text = CompanyContact.FirstName & " " & CompanyContact.LastName
            lblEmail.Text = CompanyContact.Email
            lblPhone.Text = CompanyContact.Phone
            btnCompanyPW.Text = CompanyContact.Password
            CompanyPassword = btnCompanyPW.Text

            lblAltName.Text = AltCompanyContact.FirstName & " " & AltCompanyContact.LastName
            lblAltEmail.Text = AltCompanyContact.Email
            lblAltPhone.Text = AltCompanyContact.Phone

            btnReturnPW.Text = GetReturnPassword(Trim(UCase(RefNum)), "fuelslubes")

            ReturnPassword = btnReturnPW.Text

        Catch ex As System.Exception
            Err.WriteLog("FillContacts", ex)
        End Try

    End Sub

    Private Function GetReturnPassword(RefNum As String, salt As String) As String
        Return Utilities.XOREncryption(Trim(UCase(RefNum)), salt)
    End Function

    Public Sub BuildTabs()
        Try
            cboRefNum.Text = cboRefNum.Text.Trim
            Company = GetCompanyName(cboRefNum.Text)
            If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
            SetPaths()
            GetCompanyContactInfo()
            GetPricingContactInfo()
            GetAltCompanyContactInfo()
            GetInterimContactInfo()
            GetContacts()
            FillContacts()
            GetConsultants()
            BuildSummaryTab()
           
            BuildNotesTab()
            BuildContinuingIssuesTab()
            BuildCheckListTab("")
            BuildCorrespondenceTab()
            BuildSupportTab()
            CheckFuelsLubes()
            CheckPreliminaryPricing()
            btnValidate.Enabled = True

        Catch ex As System.Exception
            Err.WriteLog("BuildTabs", ex)
        End Try
    End Sub
    Public Sub RefNumChanged(Optional mRefNum As String = "")
        mRefNum = cboRefNum.Text
        Dim pattern As String = "^\d{1,3}[A-Za-z]{3}\d{2}[A-Za-z]*$"

        Dim match As Match = Regex.Match(mRefNum, pattern, RegexOptions.IgnoreCase)
        If (mRefNum <> String.Empty And match.Success) Then
            'RemoveHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(mRefNum, 2) & ParseRefNum(mRefNum, 3).Substring(0, 2))
            'AddHandler cboStudy.SelectedIndexChanged, AddressOf cboStudy_SelectedIndexChanged
            'If Lubes, make button visible Fuel Lubes and Transmit Pricing
            If ParseRefNum(mRefNum, 2) = "LUB" Then
                'btnFLComp.Visible = True
                'btnTP.Visible = True
            End If

            RemoveHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged

            SetTabDirty(True)
            SetPaths()
            'cboRefNum.Text = mRefNum
            GetRefNumRecord(mRefNum)
            SaveRefNum(cboStudy.Text, mRefNum)
            'BuildTabs()
            AddHandler cboCompany.SelectedIndexChanged, AddressOf cboCompany_SelectedIndexChanged
            If mRefNum.Length > 0 Then RefNum = mRefNum
            If File.Exists(CorrPath & "PN_" & cboCompany.Text & ".doc") Then
                btnCreatePN.Text = "Open PN File"
                txtNotes.ReadOnly = True
            Else
                txtNotes.ReadOnly = False
            End If
        End If
    End Sub
    Private Sub cboRefNum_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboRefNum.SelectedIndexChanged
        If cboRefNum.Text <> "--------------------" Then
            Me.Cursor = Cursors.WaitCursor
            RefNumChanged(RefNum)
            cboRefNum.SelectedIndex = cboRefNum.FindString(cboRefNum.Text.ToUpper)
            If cboRefNum.SelectedIndex = -1 Then
                cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(cboRefNum.Text, 2) & ParseRefNum(cboRefNum.Text, 3))
                ChangedStudy(cboRefNum.Text)
            End If

            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub btnDirRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnDirRefresh.Click
        ConsoleTabs.TabPages.Remove(tabSS)
    End Sub

    Private Sub chkSQL_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkSQL.CheckedChanged
        If Not chkSQL.Checked Then
            Label25.Text = "Clippy Query"
        Else
            Label25.Text = "SQL Query"
        End If
    End Sub

    Private Sub btnKillProcesses_Click(sender As System.Object, e As System.EventArgs) Handles btnKillProcesses.Click
        Dim dr As New DialogResult()
        dr = MessageBox.Show("This will kill all OPEN Excel and Word Documents.  Please <SAVE> any Word or Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel)
        If dr = DialogResult.OK Then
            Utilities.KillProcesses("WinWord")
            Utilities.KillProcesses("Excel")
        End If
    End Sub


#End Region

    Private Sub CheckForCurrentStudyYear()
        If cboStudy.Text.Length = 5 Then
            StudyYear = cboStudy.Text.Substring(cboStudy.Text.Length - 2, 2)
            If "20" & StudyYear <> CurrentStudyYear Then
                cboStudy.BackColor = Color.Yellow
                lblWarning.Visible = True
            Else
                cboStudy.BackColor = Color.White
                lblWarning.Visible = False
            End If

            Study = cboStudy.Text.Substring(0, cboStudy.Text.Length - 2)
            If (StudyYear > 50) Then
                StudyYear = "19" + StudyYear
            Else
                StudyYear = "20" + StudyYear
            End If
        End If
    End Sub
    
    Private Sub SaveNotes()
        Dim params As New List(Of String)


        params = New List(Of String)
        params.Add("RefNum/" + RefNum)
        params.Add("Consultant/" + UserName)
        params.Add("Issue/" & Utilities.FixQuotes(txtNotes.Text))

        Dim c As Integer = db.ExecuteNonQuery("Console." & "UpdateValidationNotes2014", params)
        If c = 0 Then
            lblStatus.Text = "IDR Notes did not update"
        Else
            lblStatus.Text = "IDR Notes Saved...."
        End If


    End Sub

    Private Sub btnSavePN_Click(sender As System.Object, e As System.EventArgs) Handles btnSavePN.Click

        SaveNotes()
        BuildNotesTab()

    End Sub

    Private Sub btnCreatePN_Click(sender As System.Object, e As System.EventArgs) Handles btnCreatePN.Click
        If File.Exists(CorrPath & "PN_" & cboCompany.Text & ".doc") Then
            Process.Start(CorrPath & "PN_" & cboCompany.Text & ".doc")
        Else
            Dim dr As DialogResult = MessageBox.Show("Are you sure you want to create the PN Word file?   This will lock the field.", "Wait", MessageBoxButtons.YesNo)
            If dr = System.Windows.Forms.DialogResult.Yes Then
                Try

                    Dim myText As String = "Presenter Notes for" & vbCrLf & Coloc & vbCrLf & vbCrLf
                    Dim params = New List(Of String)
                    Dim row As DataRow

                    lblStatus.Text = "Creating Presenter's Word file..."
                    params.Add("RefNum/" + RefNum)
                    ds = db.ExecuteStoredProc("Console." & "GetValidationNotes2014", params)

                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            row = ds.Tables(0).Rows(0)
                            myText += row("Note").ToString
                        Else
                            myText += ""
                        End If
                    End If
                    Dim strw As New StreamWriter(TempPath & "PN_" & Coloc & ".txt")

                    strw.WriteLine(myText)
                    strw.Close()
                    strw.Dispose()
                    Dim doc As String = Utilities.ConvertToDoc(TempPath, TempPath & "PN_" & Coloc & ".txt")
                    File.Move(TempPath & "PN_" & cboCompany.Text & ".doc", CorrPath & "PN_" & Coloc & ".doc")
                    File.Delete(TempPath & "PN_" & cboCompany.Text & ".txt")
                    lblStatus.Text = "Presenter's Word file created and locked..."
                    txtNotes.Text = "~" & txtNotes.Text
                    lblStatus.Text = UpdatePN(RefNum)
                    BuildNotesTab()
                    Process.Start(CorrPath & "PN_" & Coloc & ".doc")
                    btnCreatePN.Text = "Open PN File"

                Catch ex As System.Exception
                    lblStatus.Text = "Error: " & ex.Message
                End Try
            End If
        End If
    End Sub

    Private Sub btnUnlockPN_Click(sender As System.Object, e As System.EventArgs) Handles btnUnlockPN.Click
        If txtNotes.Text.Substring(0, 1) = "~" Then
            txtNotes.Text = txtNotes.Text.Substring(1, txtNotes.Text.Length - 1)
            btnCreatePN.Text = "Create PN File"
            lblStatus.Text = UpdatePN(cboRefNum.Text)
            lblStatus.Text = "PN File unlocked..."
            btnUnlockPN.Enabled = False
        End If
    End Sub

    Private Sub btnCreateMissingPNFiles_Click(sender As System.Object, e As System.EventArgs) Handles btnCreateMissingPNFiles.Click
        PresenterNotesReport()
    End Sub


    Private Sub lblName_DoubleClick(sender As System.Object, e As System.EventArgs)

        Dim Contacts As String = Nothing
        Contacts = lblName.Text & vbTab & lblEmail.Text & vbTab & lblPhone.Text & vbCrLf
        Contacts += lblAltName.Text & vbTab & lblAltEmail.Text & vbTab & lblAltPhone.Text
        Clipboard.SetText(Contacts)
        MessageBox.Show("Data Copied to Clipboard")

    End Sub



    Private Sub lblRefCoName_DoubleClick(sender As System.Object, e As System.EventArgs)

        Dim Contacts As String = Nothing
        Contacts = lblRefCoName.Text & vbTab & lblRefCoEmail.Text & vbTab & lblRefCoPhone.Text & vbCrLf
        Contacts += lblRefAltName.Text & vbTab & lblRefAltEmail.Text & vbTab & lblRefAltPhone.Text & vbCrLf
        Contacts += lblOpsMgrName.Text & vbTab & lblOpsMgrEmail.Text & vbCrLf
        Contacts += lblMaintMgrName.Text & vbTab & lblMaintMgrEmail.Text & vbCrLf
        Contacts += lblTechMgrName.Text & vbTab & lblTechMgrEmail.Text & vbCrLf
        Clipboard.SetText(Contacts)
        MessageBox.Show("Data Copied to Clipboard")

    End Sub

    Private Sub btnBug_Click(sender As System.Object, e As System.EventArgs) Handles btnBug.Click
        Dim bug As New frmBug(cboStudy.Text, cboStudy.Items, cboRefNum.Text, UserName)
        bug.Show()
    End Sub
    Private Sub OpenEmail(MsgTo As String)
        If MsgTo.Length > 10 And MsgTo.Contains("@") Then
            Utilities.SendEmail(MsgTo, "", "", "", Nothing, True)
        Else
            MessageBox.Show("Bad Email Format", "Warning...")
        End If

    End Sub
    Private Sub CCEmail1_Click(sender As Object, e As EventArgs) Handles PCCEmail.Click, PCEmail.Click, CCEmail1.Click, ACEmail2.Click, ICEmail.Click, RCEmail.Click, RACEmail.Click, RMEmail.Click, OMEmail.Click, MMEmail.Click, TMEmail.Click

        Select Case (CType(sender, PictureBox).Name)

            Case "CCEmail1"
                If lblEmail.Text.Contains("@") And lblEmail.Text.Length > 9 Then OpenEmail(lblEmail.Text)
            Case "ACEmail2"
                If lblAltEmail.Text.Contains("@") And lblAltEmail.Text.Length > 9 Then OpenEmail(lblAltEmail.Text)
            Case "ICEmail"
                If lblInterimEmail.Text.Contains("@") And lblInterimEmail.Text.Length > 9 Then OpenEmail(lblInterimEmail.Text)
            Case "RCEmail"
                If lblRefCoEmail.Text.Contains("@") And lblRefCoEmail.Text.Length > 9 Then OpenEmail(lblRefCoEmail.Text)
            Case "RACEmail"
                If lblRefAltEmail.Text.Contains("@") And lblRefAltEmail.Text.Length > 9 Then OpenEmail(lblRefAltEmail.Text)
            Case "PCEmail"
                If lblPricingContactEmail.Text.Contains("@") And lblPricingContactEmail.Text.Length > 9 Then OpenEmail(lblPricingContactEmail.Text)
            Case "RMEmail"
                If lblRefMgrEmail.Text.Contains("@") And lblRefMgrEmail.Text.Length > 9 Then OpenEmail(lblRefMgrEmail.Text)
            Case "OMEmail"
                If lblOpsMgrEmail.Text.Contains("@") And lblOpsMgrEmail.Text.Length > 9 Then OpenEmail(lblOpsMgrEmail.Text)
            Case "MMEmail"
                If lblMaintMgrEmail.Text.Contains("@") And lblMaintMgrEmail.Text.Length > 9 Then OpenEmail(lblMaintMgrEmail.Text)
            Case "TMEmail"
                If lblTechMgrEmail.Text.Contains("@") And lblTechMgrEmail.Text.Length > 9 Then OpenEmail(lblTechMgrEmail.Text)
            Case "PCCEmail"
                If lblPCCEmail.Text.Contains("@") And lblPCCEmail.Text.Length > 9 Then OpenEmail(lblPCCEmail.Text)


        End Select


    End Sub

    Private Sub btnSS_Click(sender As Object, e As EventArgs) Handles btnSS.Click
        If Not ConsoleTabs.TabPages.Contains(tabSS) Then ConsoleTabs.TabPages.Add(tabSS)
        ConsoleTabs.SelectTab(tabSS)
    End Sub

    Private Sub btnDBQuery_Click(sender As Object, e As EventArgs)
        If Not ConsoleTabs.TabPages.Contains(tabClippy) Then ConsoleTabs.TabPages.Add(tabClippy)
        ConsoleTabs.SelectTab(tabClippy)
    End Sub

    Private Sub btnQueryQuit_Click(sender As Object, e As EventArgs) Handles btnQueryQuit.Click
        ConsoleTabs.TabPages.Remove(tabClippy)
    End Sub

    Private Sub btnGradeExit_Click(sender As Object, e As EventArgs) Handles btnGradeExit.Click
        ConsoleTabs.TabPages.Remove(tabTimeGrade)
    End Sub

    Private Sub lblItemsRemaining_Click(sender As Object, e As EventArgs) Handles lblItemsRemaining.Click
        ConsoleTabs.SelectTab(tabCheckList)

    End Sub

    Private Sub cboNotes_SelectedIndexChanged(sender As Object, e As EventArgs)
        BuildNotesTab()
    End Sub
    Private Sub ShowTimeGrade()
        If Not ConsoleTabs.TabPages.Contains(tabTimeGrade) Then ConsoleTabs.TabPages.Add(tabTimeGrade)
        ConsoleTabs.SelectTab(tabTimeGrade)
        BuildGradeGrid()
        TimeHistory_Load()
        TimeSummary_Load()
    End Sub

    Private Sub btnMain_Click(sender As Object, e As EventArgs) Handles btnMain.Click, lblRedFlags.Click, lblBlueFlags.Click
        ShowTimeGrade()
    End Sub

    Private Sub txtCompanyPassword_Click_1(sender As Object, e As EventArgs) Handles btnCompanyPW.Click, btnReturnPW.Click
        If CType(sender, Button).Name = "btnCompanyPW" Then
            If btnCompanyPW.Text.Length > 1 Then Clipboard.SetText(btnCompanyPW.Text)
        Else
            If btnReturnPW.Text.Length > 1 Then Clipboard.SetText(btnReturnPW.Text)
        End If
    End Sub

    Private Sub btnValFax_Click_1(sender As Object, e As EventArgs) Handles btnValFax.Click
        ValFax_Build("")
    End Sub

    Private Sub btnBuildVRFile_Click_2(sender As Object, e As EventArgs) Handles btnBuildVRFile.Click
        BuildVR()
    End Sub

    Private Function TabIt(str1 As String, str2 As String) As String
        Dim NewString As String

        NewString = str1 & Space(50 - str1.Length) & str2

        Return NewString
    End Function

    Private Sub btnSC_Click(sender As Object, e As EventArgs) Handles btnSC.Click
        Process.Start(btnSC.Tag)
    End Sub

    Private Sub btnPT_Click(sender As Object, e As EventArgs) Handles btnPT.Click
        Process.Start(btnPT.Tag)
    End Sub

    Private Sub btnUnitReview_Click(sender As Object, e As EventArgs) Handles btnUnitReview.Click
        Process.Start(btnUnitReview.Tag)
    End Sub

    Private Sub btnSpecFrac_Click(sender As Object, e As EventArgs) Handles btnSpecFrac.Click
        Process.Start(btnSpecFrac.Tag)
    End Sub

    Private Sub btnHYC_Click(sender As Object, e As EventArgs) Handles btnHYC.Click
        Process.Start(btnHYC.Tag)
    End Sub

    Private Sub btnDrawings_Click(sender As Object, e As EventArgs) Handles btnDrawings.Click
        Process.Start(btnDrawings.Tag)
    End Sub

    Private Sub btnCT_Click(sender As Object, e As EventArgs) Handles btnCT.Click
        Process.Start(btnCT.Tag)
    End Sub

    Private Sub btnPA_Click(sender As Object, e As EventArgs) Handles btnPA.Click
        Process.Start(btnPA.Tag)
    End Sub

    Private Sub btnRAM_Click(sender As Object, e As EventArgs) Handles btnRAM.Click
        Process.Start(btnRAM.Tag)
    End Sub

    Private Sub btnVI_Click(sender As Object, e As EventArgs) Handles btnVI.Click
        Process.Start(btnVI.Tag)
    End Sub


    Private Sub btnPolishRpt_Click(sender As Object, e As EventArgs) Handles btnPolishRpt.Click
        OpenPolishReport()
    End Sub
    Private Function FormatRefNum(refnum As String) As String
        refnum = refnum.Replace("LIV", "LUB").Replace("LEV", "LUB")
        If refnum.Substring(refnum.Length - 1, 1) = "A" Then
            Return refnum.Substring(0, refnum.Length - 3)
        Else
            Return refnum.Substring(0, refnum.Length - 2)
        End If
    End Function
    Private Sub btnSaveCI_Click(sender As Object, e As EventArgs) Handles btnSaveCI.Click
        Dim params As New List(Of String)
        dgContinuingIssues.Height = 385

        params = New List(Of String)
        params.Add("RefNum/" + FormatRefNum(RefNum))
        params.Add("ID/" + CI)
        params.Add("EditedBy/" + UserName)
        params.Add("Issue/" & Utilities.FixQuotes(txtCI.Text))

        If btnSaveCI.Text = "Update Issue" Then
            ds = db.ExecuteStoredProc("Console.UpdateContinuingIssues2014", params)
            Try
                CI = ds.Tables(0).Rows(0)(0)
            Catch

            End Try
        Else
            ds = db.ExecuteStoredProc("Console.InsertContinuingIssues2014", params)
        End If


        BuildContinuingIssuesTab()

    End Sub

    Private Sub txtCI_TextChanged(sender As Object, e As EventArgs) Handles txtCI.TextChanged
        If txtCI.Text.Length > 1 Then

            btnSaveCI.Enabled = True
        End If
    End Sub



    Private Sub btnFLValidation_Click(sender As Object, e As EventArgs) Handles btnFLValidation.Click
        If File.Exists(IDRPath & "Combined Site\Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls") Then
            Process.Start(IDRPath & "Combined Site\Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls")
        Else
            MessageBox.Show(IDRPath & "Combined Site\Compare Fuels and Lubes " & StudyYear.Substring(2, 2) & ".xls is Missing.  Contact Sung.Pak@solomononline.com", "Missing")
        End If
    End Sub


    Private Sub dgContinuingIssues_Click(sender As Object, e As EventArgs) Handles dgContinuingIssues.Click
        Dim row As DataGridViewRow
        dgContinuingIssues.Height = 240
        txtCI.Text = ""
        If dgContinuingIssues.SelectedRows.Count > 0 Then
            row = dgContinuingIssues.SelectedRows(0)

            CI = row.Cells(0).Value.ToString
            txtCI.Text = row.Cells(2).Value.ToString
            CIConsultant = row.Cells(3).Value.ToString
            CIIssueDate = row.Cells(1).Value.ToString
            CIDeletedBy = row.Cells(4).Value.ToString
            CIDeletedDate = row.Cells(5).Value.ToString
            CIDeleted = row.Cells(6).Value.ToString
            btnSaveCI.Text = "Update Issue"


        End If


    End Sub
    Private Sub BuildContinuingIssuesTab()
        Dim mRefNum As String = RefNum
        If ParseRefNum(RefNum, 2) = "LUB" Then RefNum = RefNum.Replace("LUB", "LIV")
        Dim dt As String
        Dim row As DataRow
        Dim params = New List(Of String)
        'txtNotes.Text = ""
        params = New List(Of String)

        params.Add("RefNum/" + FormatRefNum(RefNum))


        dgContinuingIssues.Rows.Clear()
        dgContinuingIssues.Height = 385
        ds = db.ExecuteStoredProc("Console." & "GetContinuingIssues2014", params)

        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr In ds.Tables(0).Rows
                If dr("note").ToString.Trim.Length > 1 Then
                    dgContinuingIssues.Rows.Add({dr("IssueID").ToString, Convert.ToDateTime(dr("EntryDate")), dr("note").ToString, dr("consultant").ToString, dr("deletedby").ToString, dr("deletedDate").ToString, dr("deleted").ToString})
                End If
            Next
            btnExportCI.Enabled = True
        End If

    End Sub
    Private Sub btnNewIssue_Click(sender As Object, e As EventArgs) Handles btnNewIssue.Click

        dgContinuingIssues.Height = 220
        txtCI.Text = ""
        btnSaveCI.Text = "Save Issue"
        btnSaveCI.Enabled = True

    End Sub


    Private Sub cboConsultant_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboConsultant.SelectedIndexChanged

        UpdateConsultant()
        
    End Sub
    Private Sub SearchQA(searchString As String)
        Dim results As String = ""

        tv.Nodes.Clear()
        Dim root As New TreeNode("SearchResults")
        tv.Nodes.Add(root)


        Dim params As New List(Of String)
        'CHANGE
        'Dim dd As New DataObject(DataObject.StudyTypes.REFINING_DEV, "mgv", "solomon2012")


        params = New List(Of String)

        params.Add("SearchString/" + searchString)
        If chkUseRefnum.Checked Then
            params.Add("RefNum/" + cboRefNum.Text)
        Else
            params.Add("RefNum/ALL")
        End If

        ds = dd.ExecuteStoredProc("SearchQA", params)

        Dim count As Integer = 1
        For Each r In ds.Tables(0).Rows
            tv.Nodes(0).Nodes.Add(count.ToString & ". " & r(1))
            count = count + 1
        Next
        tv.ExpandAll()


    End Sub

    Private Sub SearchQALink(searchString As String)
        Dim results As String = ""
        Dim n As TreeNode
        tv.Nodes.Clear()
        Dim root As New TreeNode("SearchResults")
        tv.Nodes.Add(root)


        Dim params As New List(Of String)
        'CHANGE
        'Dim dd As New DataObject(DataObject.StudyTypes.REFINING_DEV, "mgv", "solomon2012")


        params = New List(Of String)

        params.Add("SearchString/" + searchString)
        If chkUseRefnum.Checked Then
            params.Add("RefNum/" + cboRefNum.Text)
        Else
            params.Add("RefNum/ALL")
        End If

        ds = dd.ExecuteStoredProc("SearchQACategory", params)

        Dim count As Integer = 1
        For Each r In ds.Tables(0).Rows
            n = New TreeNode(count.ToString & ". " & r(1))

            tv.Nodes(0).Nodes.Add(n)
            count = count + 1
        Next
        tv.ExpandAll()


    End Sub

    Private Sub lnkRefineryHistory_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lnkRefineryHistory.LinkClicked, lnkEnergy.LinkClicked, lnkMaintenance.LinkClicked, lnkMaterialBalance.LinkClicked, lnkMisc.LinkClicked, lnkOpex.LinkClicked, lnkPersonnel.LinkClicked, lnkPricing.LinkClicked, lnkProcessData.LinkClicked, lnkResultsPresentation.LinkClicked, lnkStudyBoundary.LinkClicked
        SearchQALink(CType(sender, LinkLabel).Tag)

    End Sub

    Private Sub btnExportCI_Click(sender As Object, e As EventArgs) Handles btnExportCI.Click
        Dim mRefNum As String = RefNum
        If ParseRefNum(RefNum, 2) = "LUB" Then RefNum = RefNum.Replace("LUB", "LIV")
        Dim dt As String
        Dim row As DataRow
        Dim params = New List(Of String)

        params = New List(Of String)
        params.Add("RefNum/" + FormatRefNum(RefNum))


        ds = db.ExecuteStoredProc("Console." & "GetContinuingIssues2014", params)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                BuildCIExport(ds.Tables(0).Rows, RefNum)

            End If
        End If
    End Sub

    Private Sub BuildCIExport(rows As DataRowCollection, RefNum As String)
        Dim Body As String
        Dim Header As String
        Dim Footer As String

        Header = "Refinery ID: " & FormatRefNum(RefNum)

        Footer = "Copyright Solomon Associates 2015"

        Dim file As System.IO.FileStream
        Try
            ' Indicate whether the text file exists
            If My.Computer.FileSystem.FileExists(TempPath & "CI.TXT") Then
                System.IO.File.Delete(TempPath & "CI.TXT")
            End If

            Dim addInfo As New System.IO.StreamWriter(TempPath & "CI.TXT")

            addInfo.WriteLine(Header)
            addInfo.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------")
            addInfo.WriteLine("") ' blank line of text
            For Each r In rows
                If Not chkNoDates.Checked Then
                    addInfo.WriteLine(r(1) & " - " & r(2))
                End If
                addInfo.WriteLine(r(3))
                addInfo.WriteLine("") ' blank line of text
            Next
            addInfo.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------")
            addInfo.WriteLine(Footer)
            addInfo.Close()
        Catch ex As FileIO.MalformedLineException
            MessageBox.Show(ex.Message)
        End Try
        Process.Start(TempPath & "CI.TXT")
    End Sub
    Private Sub DeleteContinuingIssue(id As Integer)
        Dim params As New List(Of String)
        params = New List(Of String)

        params.Add("IssueID/" + CI)
        params.Add("DeletedBy/" + UserName)


        ds = db.ExecuteStoredProc("Console.DeleteContinuingIssue2014", params)
    End Sub



    Private Sub dgContinuingIssues_KeyDown(sender As Object, e As KeyEventArgs) Handles dgContinuingIssues.KeyDown
        If dgContinuingIssues.SelectedRows.Count > 0 Then
            Dim Row As DataGridViewRow = dgContinuingIssues.SelectedRows(0)
            If e.KeyCode = Keys.Delete Then
                Dim dr As DialogResult = MessageBox.Show("This will remove this issue from list, are you sure you want to delete?", "Wait", MessageBoxButtons.YesNo)
                If dr = System.Windows.Forms.DialogResult.Yes Then
                    DeleteContinuingIssue(Row.Cells(0).Value)
                    BuildContinuingIssuesTab()
                    txtCI.Text = ""
                End If
            End If
        End If
    End Sub


    Private Sub QASearch_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QASearch.KeyPress
        If e.KeyChar = Chr(13) Then SearchQA(QASearch.Text)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Process.Start(CorrPath)
    End Sub


    Private Sub MainConsole_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F5 Then
            Me.Cursor = Cursors.WaitCursor
            BuildTabs()
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub btnIssueCancel_Click(sender As Object, e As EventArgs) Handles btnIssueCancel.Click
        txtCI.Text = ""
        dgContinuingIssues.Height = 385
        btnSaveCI.Text = "Update Issue"
        btnSaveCI.Enabled = False
    End Sub

    Private Sub btnDDExit_Click(sender As Object, e As EventArgs) Handles btnDDExit.Click
        RemoveTab("tabDD")
        ConsoleTabs.SelectedIndex = 1
    End Sub
    Private Sub cboCompany_KeyDown(sender As Object, e As KeyEventArgs) Handles cboCompany.KeyDown
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True
            For Each i In cboCompany.Items
                If (i.StartsWith(cboCompany.Text.ToUpper)) Then
                    cboCompany.SelectedItem = i
                    Exit For
                End If
            Next
        End If

        'End If
    End Sub

    Private Sub cboRefNum_KeyDown_1(sender As Object, e As KeyEventArgs) Handles cboRefNum.KeyDown
        Dim rmRefNum As String = CurrentRefNum

        If e.KeyCode = Keys.Enter Then
            KeyEntered = True
            RefNumChanged(rmRefNum)
            cboRefNum.SelectedIndex = cboRefNum.FindString(cboRefNum.Text.ToUpper)
            If cboRefNum.SelectedIndex = -1 Then
                cboStudy.SelectedIndex = cboStudy.FindString(ParseRefNum(rmRefNum, 2) & ParseRefNum(rmRefNum, 3))
                ChangedStudy(rmRefNum)
            End If

        End If
    End Sub

    Private Sub txtNotes_Leave(sender As Object, e As EventArgs) Handles txtNotes.Leave
        SaveNotes()
    End Sub
    Private Sub BuildContactTab()
        GetCompanyContactInfo()
        GetPricingContactInfo()
        GetAltCompanyContactInfo()
        GetInterimContactInfo()
        GetContacts()
        FillContacts()
    End Sub
    Private Sub BuildTab(current As String)
        Select Case current
            Case "Correspondence"
                BuildCorrespondenceTab()
            Case "Checklist"
                BuildCheckListTab("")
            Case "Notes"
                BuildNotesTab()
            Case "Continuing Issues"
                BuildContinuingIssuesTab()
            Case "Time/Grade"
                ShowTimeGrade()

        End Select
        BuildSummaryTab()
        CheckFuelsLubes()
        BuildContactTab()
        CheckPreliminaryPricing()
        cboRefNum.Text = cboRefNum.Text.Trim
        Company = GetCompanyName(cboRefNum.Text)
        If Company.Length > 0 Then CurrentCompany = Company.Substring(0, Company.IndexOf("-") - 1)
        SetPaths()
        btnValidate.Enabled = True
    End Sub
    Private Sub ConsoleTabs_Selected(sender As Object, e As TabControlEventArgs) Handles ConsoleTabs.Selected
        CurrentTab = ConsoleTabs.SelectedTab.Text
        BuildTab(CurrentTab)
    End Sub
End Class
