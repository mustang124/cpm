﻿Imports System.IO
Imports System.Collections.Generic
Imports System.Security.Cryptography
Imports System.Text
Imports Outlook = Microsoft.Office.Interop.Outlook
Imports System.Runtime.InteropServices
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.Core
Imports Ionic.Zip
Imports System.Threading




Public Class WordTemplate
    Public Field As New List(Of String)
    Public RField As New List(Of String)
End Class

Public Class Utilities
    Dim err As ErrorLog()


    Public Shared Function ConvertToDoc(TempPath As String, ByVal in_file As String) As String
        Const FORMAT_DOC As Integer = 0
        Dim out_file As String = Utilities.ExtractFileName(in_file)
        out_file = TempPath & out_file.Substring(0, out_file.LastIndexOf(".")) & ".doc"
        Dim word_server As Object ' Word.Application

        word_server = CreateObject("Word.Application")

        ' Open the input file.
        word_server.Documents.Open( _
            FileName:=in_file, _
            ReadOnly:=False, _
            AddToRecentFiles:=False)

        ' Save the output file.
        word_server.ActiveDocument.SaveAs( _
            FileName:=out_file, _
            FileFormat:=FORMAT_DOC, _
            LockComments:=False, _
            AddToRecentFiles:=True)

        ' Exit the server without prompting.
        word_server.ActiveDocument.Close(False)
        word_server = Nothing
        Return out_file
    End Function
    Public Shared Function AddDateStamp(f As String) As String
        Dim rtn As String
        Dim fn As String
        Dim ex As String

        fn = f.Substring(0, f.IndexOf("."))
        ex = f.Substring(f.IndexOf(".") + 1, f.Length - f.IndexOf(".") - 1)

        rtn = fn & " " & Today.ToString("dd-MMM-yy") & "." & ex
        Return rtn
    End Function
    Public Shared Sub SendEmail(strTo As String, strCC As String, subj As String, body As String, attachments As List(Of String), mode As Boolean)

        Dim outlook As Outlook.Application
        Dim outlookprocess As Process

        If strTo.Length < 3 Then Exit Sub
        outlookprocess = Utilities.CheckProcess("C:\Program Files (x86)\Microsoft Office\Office14\", "Outlook")
        Try

            If outlookprocess IsNot Nothing Then
                outlook = DirectCast(Marshal.GetActiveObject("Outlook.Application"), Outlook.Application)
            Else

                outlook = New Outlook.Application
            End If

            Dim AddDays As String = ""


            Dim msg As Outlook.MailItem



            msg = outlook.CreateItem(OlItemType.olMailItem)

            msg.To = strTo
            If msg.To <> strCC And strCC <> "" Then
                msg.CC = strCC
            End If
            msg.Subject = subj
            msg.Body = body

            If attachments IsNot Nothing Then
                For Each att In attachments
                    msg.Attachments.Add(att)
                Next
            End If
            If mode Then
                msg.Display(False)
            Else
                msg.Send()
                outlook = Nothing
            End If

            msg = Nothing
        Catch
            MessageBox.Show("Error Launching OUTLOOK.  Must launch Outlook manually then try and resend.", "Error")
        End Try


    End Sub
    Public Shared Function EncryptedPassword(aString As String, salt As String) As String

        Dim EncryptionIV As String = "SACTW0S1G2L3C4D5B6R7M8F9"
        Dim PartialKey As String = "SOALCDMSCCHOOL1996DUSTYRHODES"

        ' Note that the key and IV must be the same for the encrypt and decrypt calls.
        Dim results As String
        Dim tdesEngine As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        Dim key As String = salt + PartialKey
        Dim keyBytes As Byte() = Encoding.ASCII.GetBytes(key.Substring(0, 24))
        Dim ivBytes As Byte() = Encoding.ASCII.GetBytes(EncryptionIV)

        Dim transform As ICryptoTransform = tdesEngine.CreateEncryptor(keyBytes, ivBytes)
        Dim mesg As Byte() = Encoding.ASCII.GetBytes(aString)
        Dim ecn As Byte() = transform.TransformFinalBlock(mesg, 0, mesg.Length)

        results = Convert.ToBase64String(ecn)

        Return results


    End Function
    Public Shared Function CleanFileName(strFileName As String) As String
        Dim strBadChar As String
        Dim strRepChar As String
        Dim I As Integer
        Dim J As Integer
        Dim blnChanged As Boolean
        blnChanged = False

        strBadChar = "\/:*?""<>|%"
        ' A
        strBadChar = strBadChar & Chr(192)
        strBadChar = strBadChar & Chr(193)
        strBadChar = strBadChar & Chr(194)
        strBadChar = strBadChar & Chr(195)
        strBadChar = strBadChar & Chr(196)
        strBadChar = strBadChar & Chr(197)
        strBadChar = strBadChar & Chr(198)
        ' E
        strBadChar = strBadChar & Chr(200)
        strBadChar = strBadChar & Chr(201)
        strBadChar = strBadChar & Chr(202)
        strBadChar = strBadChar & Chr(203)
        ' I
        strBadChar = strBadChar & Chr(204)
        strBadChar = strBadChar & Chr(205)
        strBadChar = strBadChar & Chr(206)
        strBadChar = strBadChar & Chr(207)
        ' O
        strBadChar = strBadChar & Chr(210)
        strBadChar = strBadChar & Chr(211)
        strBadChar = strBadChar & Chr(212)
        strBadChar = strBadChar & Chr(213)
        strBadChar = strBadChar & Chr(214)
        ' U
        strBadChar = strBadChar & Chr(217)
        strBadChar = strBadChar & Chr(218)
        strBadChar = strBadChar & Chr(219)
        strBadChar = strBadChar & Chr(220)

        ' a
        strBadChar = strBadChar & Chr(224)
        strBadChar = strBadChar & Chr(225)
        strBadChar = strBadChar & Chr(226)
        strBadChar = strBadChar & Chr(227)
        strBadChar = strBadChar & Chr(228)
        strBadChar = strBadChar & Chr(229)
        ' e
        strBadChar = strBadChar & Chr(232)
        strBadChar = strBadChar & Chr(233)
        strBadChar = strBadChar & Chr(234)
        strBadChar = strBadChar & Chr(235)
        ' i
        strBadChar = strBadChar & Chr(236)
        strBadChar = strBadChar & Chr(237)
        strBadChar = strBadChar & Chr(238)
        strBadChar = strBadChar & Chr(239)
        ' o
        strBadChar = strBadChar & Chr(242)
        strBadChar = strBadChar & Chr(243)
        strBadChar = strBadChar & Chr(244)
        strBadChar = strBadChar & Chr(245)
        strBadChar = strBadChar & Chr(246)
        ' u
        strBadChar = strBadChar & Chr(249)
        strBadChar = strBadChar & Chr(250)
        strBadChar = strBadChar & Chr(251)
        strBadChar = strBadChar & Chr(252)

        strRepChar = "  -      AAAAAAAEEEEIIIIOOOOOUUUUaaaaaaeeeeiiiiooooouuuu"
        For I = 1 To Len(strBadChar)
            For J = 1 To Len(strFileName)
                If Mid(strFileName, J, 1) = Mid(strBadChar, I, 1) Then
                    Mid(strFileName, J, 1) = Mid(strRepChar, I, 1)
                    blnChanged = True
                End If
            Next J
        Next I
        CleanFileName = Trim(strFileName)


    End Function


    Public Shared Function DecryptPassword(aString As String, salt As String)

        Dim EncryptionIV As String = "SACTW0S1G2L3C4D5B6R7M8F9"
        Dim PartialKey As String = "SOALCDMSCCHOOL1996DUSTYRHODES"
        Dim results As String
        aString = aString.Replace(" ", "+")
        Dim tdesEngine As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        Dim key As String = salt + PartialKey
        Dim keyBytes As Byte() = Encoding.ASCII.GetBytes(key.Substring(0, 24))
        Dim ivBytes As Byte() = Encoding.ASCII.GetBytes(EncryptionIV)

        Dim transform As ICryptoTransform = tdesEngine.CreateEncryptor(keyBytes, ivBytes)
        Dim mesg As Byte() = Convert.FromBase64String(aString)

        Dim ecn As Byte() = transform.TransformFinalBlock(mesg, 0, mesg.Length)

        Results = Encoding.ASCII.GetString(ecn)
        Return results

    End Function


    Public Shared Function ExtractFileName(fn As String) As String
        Dim ret As String
        ret = fn.Substring(fn.LastIndexOf("\") + 1, fn.Length - fn.LastIndexOf("\") - 1)
        Return ret
    End Function
    Public Shared Function Pad(str As String, space As Integer) As String
        Dim len As Integer = str.Length
        If len > space Then Return str.Substring(0, space)
        For i = len To space
            str += " "
        Next
        Return str
    End Function
    Public Shared Function GetExcelProcess() As Excel.Application
        Dim process As Excel.Application
        Try
            process = CType(GetObject(, "Excel.Application"), Excel.Application)
        Catch ex As System.Exception
            process = New Excel.Application
        End Try

        Return process
    End Function

    Public Shared Function PadWith(str As String, chr As String, replications As Integer) As String
        Dim len As Integer = str.Length
        If len > replications Then Return str.Substring(0, replications)
        For i = len To replications
            str = chr + str
        Next
        Return str
    End Function
    Public Shared Function GetFileFromLink(link As String) As String
        Dim ret As String = Nothing
        '
        ' Initialize
        Dim wshLink, strPath, wshShell
        strPath = link
        '
        ' Create Windows Scripting Host Object
        wshShell = CreateObject("WScript.Shell")
        '
        ' Get Shortcut
        wshLink = wshShell.CreateShortcut(strPath)
        '
        ' Display Target Path
        ret = wshLink.TargetPath
        '
        ' Housekeeping
        wshLink = Nothing
        wshShell = Nothing
        '
        Return ret
    End Function

    

    Public Shared Function CheckUser(_Username As String) As Boolean

        If _Username = "DBB" _
                    Or _Username = "EJE" _
                    Or _Username = "BLT" _
                    Or _Username = "JKZ" Then

                    Or _Username = "SWP" _
                    Or _Username = "JDW" _
                    Or _Username = "SAM" Then

            Return True
        Else
            Return False
        End If

        'Return db.IsValid(txtLogin.Text, txtPassword.Text)


    End Function
    Public Shared Sub UnZipFiles(zfile As String, tempdir As String, pw As String)
        Dim UnpackDirectory As String = "Extracted Files"
        Using zip1 As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(zfile)
            Dim e As Ionic.Zip.ZipEntry
            ' here, we extract every entry, but we could extract conditionally,
            ' based on entry name, size, date, checkbox status, etc.   
            For Each e In zip1
                Try
                    e.ExtractWithPassword(tempdir & UnpackDirectory, ExtractExistingFileAction.OverwriteSilently, pw)
                Catch
                    e.Extract(tempdir & UnpackDirectory, ExtractExistingFileAction.OverwriteSilently)
                End Try

            Next
        End Using
    End Sub

    Public Shared Function FixQuotes(strString As String) As String
        ' If there is a single quote in the subject or body of the email,
        ' you must make it two single quotes together. If you don't,
        ' you will end up with an empty email!!!
        Dim I As Integer
        For I = 1 To Len(strString) - 1
            ' Is this character a single quote?
            If Mid(strString, I, 1) = Chr(39) Then
                ' Is the next character a single quote?
                If Mid(strString, I + 1, 1) = Chr(39) Then
                    ' Yes, just index past it.
                    I = I + 1
                Else
                    ' No, insert another single quote right after this one.
                    strString = Left(strString, I) & Chr(39) & Right(strString, Len(strString) - I)
                    ' And index past the single quote we just inserted.
                    I = I + 1
                End If
            End If
        Next I
        ' If the last character of the string is a quote,
        ' then put one extra quote on the end.
        If Right(strString, 1) = Chr(39) Then
            strString = strString & Chr(39)
        End If
        ' Return the repaired string 
        FixQuotes = strString.Replace("/", "-")
        FixQuotes = FixQuotes.Replace(",", " ")
    End Function
    Public Shared Function isNumeric(val As String) As Boolean

        Dim result As Integer
        Return Integer.TryParse(val, result)

    End Function
    Public Shared Function Zipfile(filesPath As String, password As String, outPathName As String) As String

        Dim fsOut As FileStream = System.IO.File.Create(outPathName)
        Dim zipStream As New ICSharpCode.SharpZipLib.Zip.ZipOutputStream(fsOut)

        zipStream.SetLevel(3)       '0-9, 9 being the highest level of compression
        zipStream.Password = password   ' optional. Null is the same as not setting.

        ' This setting will strip the leading part of the folder path in the entries, to
        ' make the entries relative to the starting folder.
        ' To include the full path for each entry up to the drive root, assign folderOffset = 0.
        Dim folderOffset As Integer = filesPath.Length + (If(filesPath.EndsWith("\"), 0, 1))

        CompressFolder(filesPath, zipStream, folderOffset)

        zipStream.IsStreamOwner = True
        ' Makes the Close also Close the underlying stream
        zipStream.Close()
        Return outPathName

    End Function
    ' Recurses down the folder structure
    '
    Public Shared Sub CompressFolder(path As String, zipStream As ICSharpCode.SharpZipLib.Zip.ZipOutputStream, folderOffset As Integer)

        Dim files As String() = Directory.GetFiles(path)

        For Each filename As String In files

            Dim fi As New FileInfo(filename)

            Dim entryName As String = filename.Substring(folderOffset)  ' Makes the name in zip based on the folder
            entryName = ICSharpCode.SharpZipLib.Zip.ZipEntry.CleanName(entryName)       ' Removes drive from name and fixes slash direction
            Dim newEntry As New ICSharpCode.SharpZipLib.Zip.ZipEntry(entryName)
            newEntry.DateTime = fi.LastWriteTime            ' Note the zip format stores 2 second granularity

            ' Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
            '   newEntry.AESKeySize = 256;

            ' To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
            ' you need to do one of the following: Specify UseZip64.Off, or set the Size.
            ' If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
            ' but the zip will be in Zip64 format which not all utilities can understand.
            '   zipStream.UseZip64 = UseZip64.Off;
            newEntry.Size = fi.Length

            zipStream.PutNextEntry(newEntry)

            ' Zip the file in buffered chunks
            ' the "using" will close the stream even if an exception occurs
            Dim buffer As Byte() = New Byte(4095) {}
            Using streamReader As FileStream = File.OpenRead(filename)
                StreamUtils.Copy(streamReader, zipStream, buffer)
            End Using
            zipStream.CloseEntry()
        Next
        'Dim folders As String() = Directory.GetDirectories(path)
        'For Each folder As String In folders
        '    CompressFolder(folder, zipStream, folderOffset)
        'Next
    End Sub
    Public Shared Function XOREncryption(CodeKey As String, DataIn As String) As String
        Dim lonDataPtr As Long
        Dim strDataOut As String = ""
        Dim intXOrValue1 As Integer, intXOrValue2 As Integer

        If CodeKey <> "" Then
            For lonDataPtr = 1 To Len(DataIn)
                'The first value to be XOr-ed comes from
                '     the data to be encrypted
                intXOrValue1 = Asc(Mid$(DataIn, lonDataPtr, 1))
                'The second value comes from the code ke
                '     y
                intXOrValue2 = Asc(Mid$(CodeKey, ((lonDataPtr Mod Len(CodeKey)) + 1), 1))
                If (intXOrValue1 Xor intXOrValue2) < 33 Then
                    strDataOut += Chr((intXOrValue1 Xor intXOrValue2) + 33)
                ElseIf (intXOrValue1 Xor intXOrValue2) > 126 Then
                    strDataOut = strDataOut + Chr((intXOrValue1 Xor intXOrValue2) - 33)
                Else
                    strDataOut = strDataOut + Chr(intXOrValue1 Xor intXOrValue2)
                End If
            Next lonDataPtr
        End If
        XOREncryption = strDataOut
    End Function
    Public Shared Sub DeleteFiles(dir As String)

        Dim di As New System.IO.DirectoryInfo(dir)
        Dim fi() As FileInfo = di.GetFiles()
        Try

            For Each f In fi
                If Not f.FullName.Contains("IDR Instructions") Then f.Delete()
            Next
        Catch ex As System.Exception
            Dim dr As New DialogResult()
            dr = MessageBox.Show(ex.Message & vbCrLf & "You have a document open in memory that needs to be deleted." & vbCrLf & "The document is stuck open in memory and needs to be shut down." & vbCrLf & "<SAVE>  any Word or Excel documents you have open and then click OK when ready", "Error Deleting Temp Files", MessageBoxButtons.OKCancel)
            If dr = DialogResult.OK Then
                KillProcesses("WinWord")
                KillProcesses("Excel")
            End If
        End Try
    End Sub
    Public Shared Sub KillProcesses(p As String)
        Dim pProcess() As Process = System.Diagnostics.Process.GetProcessesByName(p)

        For Each ep As Process In pProcess
            Try
                ep.Kill()
            Catch
            End Try
        Next

    End Sub
    Public Shared Function FileTooBig(fn As String) As Boolean
        Dim file As New FileInfo(fn)
        Dim sizeInBytes As Long = file.Length
        If sizeInBytes > 1048576 And fn.ToUpper.Contains("RETURN") Then
            Return True
        End If
        Return False
    End Function
    Public Shared Function DropQuotes(strString As String) As String
        ' If there is a single quote in the supplied string, drop it.
        Dim I As Integer
        For I = 1 To Len(strString)
            ' Is this character a single quote?
            If Mid(strString, I, 1) = Chr(39) Then
                ' Drop the single quote
                strString = Left(strString, I - 1) & Right(strString, Len(strString) - I)
                ' Back up one character in case they have 2 together
                I = I - 1
            End If
        Next I
        ' If the last character of the string is a quote,
        ' then drop it.
        Do While Right(strString, 1) = Chr(39)
            strString = Left(strString, Len(strString) - 1)
        Loop
        ' Return the repaired string
        DropQuotes = strString
    End Function
    Public Shared Sub WordTemplateReplace(strfile As String, fields As SA.Console.WordTemplate, strfilename As String, mode As Integer)
        Dim fpath As String
        Dim first, second As Integer
        Dim oWord As Object
        Dim oDoc As Object
        Dim range As Object
        Dim DoubleCheck As String = "These are the template fields: " & vbCrLf
        For count = 0 To fields.Field.Count - 1
            DoubleCheck += fields.Field(count) & " --> " & fields.RField(count) & vbCrLf
        Next
        If mode = 1 Then MessageBox.Show(DoubleCheck, "Template Field Check")

        oWord = CreateObject("Word.Application")
        If mode <> 1 Then
            oWord.Visible = False
        Else
            oWord.Visible = True
        End If

        oDoc = oWord.Documents.Open(strfile)
        range = oDoc.Content
        Try
            For count = 0 To fields.Field.Count - 1
                With oWord.Selection.Find
                    .Text = fields.Field(count)
                    .Replacement.Text = fields.RField(count)
                    .Forward = True
                    .Wrap = Microsoft.Office.Interop.Word.WdFindWrap.wdFindContinue
                    .Format = False
                    .MatchCase = False
                    .MatchWholeWord = False
                    .MatchWildcards = False
                    .MatchSoundsLike = False
                    .MatchAllWordForms = False
                End With
                oWord.Selection.Find.Execute(Replace:=Microsoft.Office.Interop.Word.WdReplace.wdReplaceAll)
            Next

            


            If mode = 1 Then
                If File.Exists(strfilename) Then
                    strfilename = InputBox("CAUTION: The suggested file name already exists. If possible, contact FRS, if not, change the file name to avoid overwriting an existing document.", "CAUTION", strfilename)
                Else
                    'Stop

                    first = strfilename.LastIndexOf("\") + 1
                    second = strfilename.Length - first
                    fpath = strfilename.Substring(0, first)
                    If mode = 1 Then
                        strfilename = InputBox("Here is the suggested name for this New Vetted Fax document." & Chr(10) & _
                        "Click OK to accept it, or change, then click OK.", "SaveAs", strfilename.Substring(first, second))
                    Else
                        strfilename = strfilename.Substring(first, second)
                    End If
                End If
                ' If they hit cancel, just stop.
                If strfilename = "" Then Exit Sub
                '
                oDoc.SaveAs(fpath & strfilename, ADDTORECENTFILES:=True)
                MessageBox.Show("Name is now " & oDoc.Name)
            Else
                oDoc.SaveAs(strfilename, ADDTORECENTFILES:=True)
                oDoc.Close()
                If Not oWord Is Nothing Then
                    Marshal.FinalReleaseComObject(oWord)
                    oWord = Nothing
                End If

            End If

        Catch ex As System.Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Shared Function ExcelPassword(file As String, password As String, Optional mode As Integer = 0) As Boolean

        Dim xl As New Excel.Application
        Dim w As Excel.Workbook
        Try
            If Not file.ToUpper.Contains("MACRO") Then
                If mode = 0 Then
                    w = xl.Workbooks.Open(file)
                    w.Password = password
                Else
                    w = xl.Workbooks.Open(file, , , , password)
                    w.Password = ""
                    'w.Unprotect(password)
                End If
                w.Save()
                w.Close()
            End If
            xl = Nothing
            Return True
        Catch ex As System.Exception
            xl = Nothing
            Debug.Print(ex.Message)

            Return False
        End Try

    End Function

    Public Shared Function WordPassword(mfile As String, password As String, Optional mode As Integer = 0, Optional user As String = "") As Boolean

        Dim Word As New Word.Application
        Dim doc As Word.Document = Nothing
        Dim NewPath As String = mfile.Substring(0, mfile.LastIndexOf("\") + 1)
        Try

            If mode = 0 Then

                doc = Word.Documents.Open(mfile)
                doc.Password = password.Trim
                doc.SaveAs(mfile, Password:=password.Trim)
                doc.Close()
            Else
                If user = "BHH" Then MessageBox.Show("NewPath = " & NewPath & "  File = " & mfile & "  Password=" & password)
                doc = Word.Documents.Open(mfile, PasswordDocument:=password)
                doc.Activate()
                doc.SaveAs(NewPath & "TEMP.DOC", Password:="")

                doc.Close()
                doc = Word.Documents.Open(NewPath & "TEMP.DOC")
                File.Delete(mfile)
                doc.SaveAs(mfile, Password:="")
                doc.Close()
                File.Delete(NewPath & "TEMP.DOC")

            End If


            Word = Nothing
            Return True
        Catch ex As System.Exception
            MessageBox.Show(ex.Message, "WordPassword")
            doc.Close()
            Word = Nothing
            Return True
        End Try

    End Function
    Public Shared Function ValFaxDateDue(txtDaysToAdd) As Date

        ' Take today's date and add the number of working days they put on the screen.
        ' Return the result.
        Dim I As Integer

        ValFaxDateDue = Now
        I = 0
        Do While I < Val(txtDaysToAdd)
            ValFaxDateDue = DateAdd(DateInterval.Day, 1, ValFaxDateDue)
            If Weekday(ValFaxDateDue) > 1 And Weekday(ValFaxDateDue) < 7 Then
                I = I + 1
            End If
        Loop
        Return ValFaxDateDue
    End Function
    Public Shared Sub GetLinkPath(ByRef link As String)
        If link.Substring(link.Length - 3, 3).ToUpper = "LNK" Then
            link = Utilities.GetFileFromLink(link)
        End If

    End Sub
    Public Shared Function GetConsultantName(initials As String) As String
        Dim ds As DataSet
        'Dim db As New DataAccess()
        'CHANGE
        'Dim db As New SA.Internal.Console.DataObject.DataObject(Internal.Console.DataObject.DataObject.StudyTypes.REFINING, "mgv", "solomon2012")
        Dim cons As String = initials
        Dim params As New List(Of String)
        'Check for Interim Contact Info
        params = New List(Of String)
        params.Add("Initials/" + initials)
        ds = db.ExecuteStoredProc("Console." & "GetConsultantName", params)

        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Return ds.Tables(0).Rows(0)(0)

            End If
        End If
        Return cons

    End Function
    
    Public Shared Function CheckProcess(path As String, name As String) As Process

        For Each mProcess In Process.GetProcesses()
            If mProcess.ProcessName.Contains(name.ToUpper) Then
                Return mProcess
            End If
        Next
        MessageBox.Show("Outlook must be started to use Secure Send features.  Console will attempt to start using Outlook 2010.  If you have a different version, you will have to launch it manually.", "Warning")
        Process.Start(path & name)
        Thread.Sleep(5000)
        Return Nothing
    End Function
    Public Shared Function GetDataSetID(strRefNum As String, CurrentSortMode As Integer) As String
        If strRefNum = "" Then Return ""
        If CurrentSortMode = 0 Then
            Return strRefNum.Substring(0, strRefNum.IndexOf("-") - 1)
        Else
            Return strRefNum.Substring(strRefNum.LastIndexOf("-") + 1, strRefNum.Length - strRefNum.LastIndexOf("-") - 1)
        End If

    End Function
    Public Shared Function GetSite(strRefNum As String, CurrentSortMode As Integer) As String
        If strRefNum = "" Then Return ""
        If CurrentSortMode = 0 Then
            Return strRefNum.Substring(strRefNum.LastIndexOf("-") + 1, strRefNum.Length - strRefNum.LastIndexOf("-") - 1).Trim
        Else
            Return strRefNum.Substring(0, strRefNum.IndexOf("-") - 1).Trim
        End If
    End Function
    Public Shared Function GetRefNumPart(refnum As String, mode As Integer) As String

        Dim FirstPart As String
        Dim SecondPart As String
        Dim ThirdPart As String
        Dim Buffer As String
        Dim Looper As Integer
        Dim StopPoint As Integer
        Dim Pos As Integer
        'CHANGE
        'Dim E As New SA.Console.ErrorLog("REFINING", "mgv", "solomon2012")
        Try
            If refnum <> "" Then
                Pos = 1

                While isNumeric(refnum.Substring(Pos, 1))
                    Pos = Pos + 1
                End While
                StopPoint = Pos

                FirstPart = refnum.Substring(0, StopPoint)
                Looper = 0

                While Not isNumeric(refnum.Substring(Pos, 1))
                    Pos = Pos + 1
                    Looper = Looper + 1
                End While

                SecondPart = refnum.Substring(StopPoint, Looper)

                If SecondPart = "LIV" Or SecondPart = "LEV" Then
                    SecondPart = "LUB"
                End If

                Looper = 0
                StopPoint = Pos

                While Pos <= refnum.Length
                    Looper = Looper + 1
                    Pos = Pos + 1
                End While

                ThirdPart = refnum.Substring(StopPoint, Looper - 1)

                Buffer = "INVALID MODE"
                If mode = 0 Then
                    Buffer = FirstPart + SecondPart + ThirdPart
                End If

                If mode = 1 Then
                    Buffer = FirstPart
                End If

                If mode = 2 Then
                    Buffer = SecondPart
                End If

                If mode = 3 Then
                    Buffer = ThirdPart
                End If

                Return Buffer

            Else
                Return ""
            End If
        Catch ex As System.Exception

            E.WriteLog("GetRefNumPart", ex)
        End Try

    End Function

    Public Shared Function GetStudyType(RefNum As String) As String

        Select Case GetRefNumPart(RefNum, 2)
            Case "NSA", "EUR", "PAC"
                Return "REFINING"
            Case "LUB"
                Return "LUBES"
            Case "PA"
                Return "PIPELINE"
            Case "TA"
                Return "TERMINAL"
            Case "PN", "PNP"
                Return "POWER"
            Case "EBSM"
                Return "STYREME"
            Case "PCH", "OLE"
                Return "OLEFINS"
            Case Else
                Return ""
        End Select
    End Function
End Class
