﻿Imports System.Security.Principal

Imports System.IO

Public Class frmTemplates
    Private _OKClick As Boolean
    Public Property OKClick() As Boolean
        Get
            Return _OKClick
        End Get
        Set(ByVal value As Boolean)
            _OKClick = value
        End Set
    End Property

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        _OKClick = True
        Me.Close()


    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        _OKClick = False
        Me.Close()
    End Sub


End Class
