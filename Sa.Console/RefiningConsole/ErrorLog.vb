﻿Imports Microsoft.Office.Interop.Outlook
Imports SA.Internal.Console.DataObject

Public Class ErrorLog

    Private da As DataObject
    Private mLogType As Integer
    Private dbConn As String
    Private st As String

    Public Sub New(StudyType As String, username As String, password As String)

        st = StudyType

        Select Case st
            Case "REFINING"
                da = New DataObject(DataObject.StudyTypes.REFINING, username, password)
            Case "OLEFINS"
                da = New DataObject(DataObject.StudyTypes.OLEFINS, username, password)
            Case "POWER"
                da = New DataObject(DataObject.StudyTypes.POWER, username, password)
            Case "RAM"
                da = New DataObject(DataObject.StudyTypes.RAM, username, password)

        End Select
    End Sub


    Public Sub WriteLog(LogType As String, ex As System.Exception)

        If st <> "RAM" Then
            Try
                Dim email As Integer = My.Settings.SendErrorEmail
                Dim params As New List(Of String)


                params.Add("UserName/" + Environment.UserName)
                params.Add("LogType/" + LogType)
                params.Add("Message/" + ex.Message.ToString)
                da.ExecuteNonQuery("Console.WriteLog", params)

                If email = 1 Then

                    Dim body As String = "Method: " & LogType & vbCrLf & vbCrLf & "Error: " & ex.Message


                    'Utilities.SendEmail(My.Settings.LogEmail, "", st & " Console Error", body, Nothing, False)

                End If
            Catch
            End Try
        End If
    End Sub

End Class
