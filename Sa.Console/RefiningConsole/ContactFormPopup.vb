﻿Imports System.Data.SqlClient
Imports SA.Internal.Console.DataObject


Public Class ContactFormPopup
    Dim Contact As Contacts
    Dim Olentact As Contacts
    Dim RefContact As Contacts
    Dim IntContact As Contacts
    Dim PltContact As Contacts
    Dim CoType As String
    Dim StudyType As String
    Dim RefNum As String
    Dim UserName As String
    Public mPassword As String
    Dim StudyYear As String
    Dim Company As String
    Dim da As DataObject
    Event UpdateContact()
    Event UpdateAltContact()
    Dim CopyContent As String




    Public Sub New(ByVal User As String, ContactType As String, ReferenceNum As String, mCompanyName As String, mStudyType As String, Optional mStudyYear As String = "")

        InitializeComponent()
        CoType = ContactType
        RefNum = ReferenceNum
        UserName = User
        StudyType = mStudyType
        Company = mCompanyName
        StudyYear = mStudyYear


        Select Case StudyType.ToUpper
            Case "REFINING"
                da = New DataObject(DataObject.StudyTypes.REFINING, UserName, mPassword)
            Case "OLEFINS"
                da = New DataObject(DataObject.StudyTypes.OLEFINS, UserName, mPassword)
            Case "POWER"
                da = New DataObject(DataObject.StudyTypes.POWER, UserName, mPassword)
            Case "RAM"
                da = New DataObject(DataObject.StudyTypes.RAM, UserName, mPassword)
            Case "TERMINALS"
                da = New DataObject(DataObject.StudyTypes.PIPELINES, UserName, mPassword)
        End Select

        Initform()


    End Sub

    Private Sub Initform()
        CoType = CoType.ToUpper
        If CoType = "COMPANY" Or CoType = "ALTCONTACT" Then btnUpdate.Visible = True
        If CoType = "ALT" Then
            txtPhone.ReadOnly = True
            txtPassword.ReadOnly = True
            txtAddress1.ReadOnly = True
            txtAddress2.ReadOnly = True
            txtAddress3.ReadOnly = True
            txtCity.ReadOnly = True
            txtState.ReadOnly = True
            txtZip.ReadOnly = True
            txtFax.ReadOnly = True
            txtStreetAddress1.ReadOnly = True
            txtStreetAddress2.ReadOnly = True
            txtStreetAddress3.ReadOnly = True
            txtStreetAddress4.ReadOnly = True
            txtStreetAddress5.ReadOnly = True
            txtStreetAddress6.ReadOnly = True
            txtStreetAddress7.ReadOnly = True
            txtCountry.ReadOnly = True
            'txtTitle.Text = "Alternate Contact"
            txtName.ReadOnly = True
        Else

            txtPhone.ReadOnly = False
            txtPassword.ReadOnly = False
            txtAddress1.ReadOnly = False
            txtAddress2.ReadOnly = False
            txtAddress3.ReadOnly = False
            txtCity.ReadOnly = False
            txtState.ReadOnly = False
            txtZip.ReadOnly = False
            txtFax.ReadOnly = False
            txtStreetAddress1.ReadOnly = False
            txtStreetAddress2.ReadOnly = False
            txtStreetAddress3.ReadOnly = False
            txtStreetAddress4.ReadOnly = False
            txtStreetAddress5.ReadOnly = False
            txtStreetAddress6.ReadOnly = False
            txtStreetAddress7.ReadOnly = False
            txtCountry.ReadOnly = False
        End If
    End Sub
    Private Sub LoadContacts(Optional ContactType = "")
        Dim LubRefNum As String = ""
        Dim params As List(Of String)
        Dim ds As DataSet

        params = New List(Of String)
        Try
            Select Case CoType.ToUpper


                Case "POWERPLANT"
                    params.Add("@Refnum/" & RefNum)
                    ds = da.ExecuteStoredProc("Console." & "GetContactInfo", params)
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then


                            Dim row As DataRow = ds.Tables(0).Rows(0)
                            txtFirstName.Visible = False
                            txtLastName.Visible = False
                            txtName.Visible = True
                            CoType = "PowerPlant Contact"
                            txtName.Text = row("CoordName").ToString.ToUpper
                            txtEmail.Text = row("CoordEmail").ToString.ToUpper
                            txtFax.Text = row("CoordFax").ToString.ToUpper
                            txtPhone.Text = row("CoordPhone").ToString.ToUpper
                            txtAddress1.Text = row("CoordAddr1").ToString.ToUpper
                            txtAddress2.Text = row("CoordAddr2").ToString.ToUpper
                            txtCity.Text = row("CoordCity").ToString.ToUpper
                            txtState.Text = row("CoordState").ToString.ToUpper
                            txtZip.Text = row("CoordZip").ToString.ToUpper


                        End If
                    End If

                Case "COORD", "ALT"
                    txtFirstName.Visible = True
                    txtLastName.Visible = True
                    txtName.Visible = False
                    params.Add("@RefNum/" & RefNum)
                    params.Add("@StudyYear/" & StudyYear)
                    params.Add("@ContactType/" & CoType)
                   
                    'params.Add("@StudyYear/" & StudyYear)
                    If LubRefNum <> "" Then params.Add("LubRefNum/" + LubRefNum)
                    ds = da.ExecuteStoredProc("Console." & "GetCompanyContactInfo", params)

                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then

                            Select Case CoType.ToUpper
                                Case "COORD"

                                    Dim row As DataRow = ds.Tables(0).Rows(0)
                                    txtFirstName.Text = row("FirstName").ToString.ToUpper
                                    txtLastName.Text = row("LastName").ToString.ToUpper.ToUpper
                                    txtTitle.Text = row("JobTitle").ToString.ToUpper.ToUpper
                                    txtEmail.Text = row("Email").ToString.ToUpper.ToUpper
                                    txtPhone.Text = row("Phone").ToString.ToUpper.ToUpper

                                    txtPassword.Text = row("CompanyPassword").ToString

                                    txtFax.Text = row("Fax").ToString.ToUpper
                                    txtAddress1.Text = row("MailAddr1").ToString.ToUpper
                                    txtAddress2.Text = row("MailAddr2").ToString.ToUpper
                                    txtAddress3.Text = row("MailAddr3").ToString.ToUpper
                                    txtCity.Text = row("MailCity").ToString.ToUpper
                                    txtState.Text = row("MailState").ToString.ToUpper
                                    txtZip.Text = row("MailZip").ToString.ToUpper
                                    txtCountry.Text = row("MailCountry").ToString.ToUpper
                                    txtStreetAddress1.Text = row("StrAddr1").ToString.ToUpper
                                    txtStreetAddress2.Text = row("StrAddr2").ToString.ToUpper
                                    If StudyType.ToUpper <> "TERMINALS" Then txtStreetAddress3.Text = row("StrAdd3").ToString.ToUpper
                                    txtStreetAddress4.Text = row("StrCity").ToString.ToUpper
                                    txtStreetAddress5.Text = row("StrState").ToString.ToUpper
                                    txtStreetAddress6.Text = row("StrZip").ToString.ToUpper
                                    txtStreetAddress7.Text = row("StrCountry").ToString.ToUpper



                                Case "ALT"
                                    Dim row As DataRow = ds.Tables(0).Rows(0)
                                    txtFirstName.Text = row("FirstName").ToString.ToUpper
                                    txtLastName.Text = row("LastName").ToString.ToUpper.ToUpper
                                    txtTitle.Text = row("JobTitle").ToString.ToUpper.ToUpper
                                    txtEmail.Text = row("Email").ToString.ToUpper.ToUpper
                                    txtPhone.Text = row("Phone").ToString.ToUpper.ToUpper

                                    txtPassword.Text = row("CompanyPassword").ToString

                                    txtFax.Text = row("Fax").ToString.ToUpper
                                    txtAddress1.Text = row("MailAddr1").ToString.ToUpper
                                    txtAddress2.Text = row("MailAddr2").ToString.ToUpper
                                    txtAddress3.Text = row("MailAddr3").ToString.ToUpper
                                    txtCity.Text = row("MailCity").ToString.ToUpper
                                    txtState.Text = row("MailState").ToString.ToUpper
                                    txtZip.Text = row("MailZip").ToString.ToUpper
                                    txtCountry.Text = row("MailCountry").ToString.ToUpper
                                    txtStreetAddress1.Text = row("StrAddr1").ToString.ToUpper
                                    txtStreetAddress2.Text = row("StrAddr2").ToString.ToUpper
                                    If StudyType.ToUpper <> "TERMINALS" Then txtStreetAddress3.Text = row("StrAdd3").ToString.ToUpper
                                    txtStreetAddress4.Text = row("StrCity").ToString.ToUpper
                                    txtStreetAddress5.Text = row("StrState").ToString.ToUpper
                                    txtStreetAddress6.Text = row("StrZip").ToString.ToUpper
                                    txtStreetAddress7.Text = row("StrCountry").ToString.ToUpper
                            End Select
                        Else
                            'MessageBox.Show("Error Reading Contact Info")
                        End If
                    End If


                Case "INTERIM"

                    params.Add("@RefNum/" & RefNum)
                    If LubRefNum <> "" Then params.Add("LubRefNum/" + LubRefNum)
                    ds = da.ExecuteStoredProc("Console." & "GetInterimContactInfo", params)
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Dim row As DataRow = ds.Tables(0).Rows(0)
                            txtFirstName.Visible = True
                            txtLastName.Visible = True
                            txtName.Visible = False
                            txtFirstName.Text = row("FirstName").ToString.ToUpper
                            txtLastName.Text = row("LastName").ToString.ToUpper
                            txtEmail.Text = row("Email").ToString.ToUpper
                            txtPhone.Text = row("Phone").ToString.ToUpper

                        End If
                    End If

                Case "REFINERY", "ALTREF", "PLANT", "PRICING", "DC"
                    btnUpdate.Visible = False
                    params.Add("@RefNum/" & RefNum)
                    If LubRefNum <> "" Then params.Add("LubRefNum/" + LubRefNum)
                    ds = da.ExecuteStoredProc("Console." & "GetContactInfo", params)

                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            Dim row As DataRow = ds.Tables(0).Rows(0)
                            Select Case CoType.ToUpper
                                Case "REFINERY"

                                    txtFirstName.Visible = False
                                    txtLastName.Visible = False
                                    txtName.Visible = True
                                    CoType = "Refinery Contact"
                                    txtName.Text = row("RefName").ToString.ToUpper
                                    txtTitle.Text = row("RefTitle").ToString.ToUpper
                                    txtEmail.Text = row("RefEmail").ToString.ToUpper
                                    txtFax.Text = row("RefFax").ToString.ToUpper
                                    txtPhone.Text = row("RefPhone").ToString.ToUpper
                                    txtAddress1.Text = row("MailAddr1").ToString.ToUpper
                                    txtAddress2.Text = row("MailAddr2").ToString.ToUpper
                                    'txtAddress3.Text = row("MailAddr3").ToString.ToUpper
                                    txtCity.Text = row("MailAddr3").ToString.ToUpper
                                    txtState.Text = row("MailAddr4").ToString.ToUpper
                                    txtZip.Text = row("MailAddr5").ToString.ToUpper
                                    txtCountry.Text = row("MailAddr6").ToString.ToUpper
                                    txtStreetAddress1.Text = row("StreetAddr1").ToString.ToUpper
                                    txtStreetAddress2.Text = row("StreetAddr2").ToString.ToUpper
                                    txtStreetAddress3.Text = row("StreetAddr3").ToString.ToUpper
                                    txtStreetAddress4.Text = row("StreetAddr4").ToString.ToUpper
                                    txtStreetAddress5.Text = row("StreetAddr5").ToString.ToUpper
                                    txtStreetAddress6.Text = row("StreetAddr6").ToString.ToUpper
                                    'txtStreetAddress7.Text = row("StreetAddr7").ToString.ToUpper

                                Case "ALTREF"
                                    txtFirstName.Visible = False
                                    txtLastName.Visible = False
                                    txtName.Visible = True
                                    CoType = "Refinery Contact"
                                    txtName.Text = row("RefName2").ToString.ToUpper
                                    txtTitle.Text = row("RefTitle2").ToString.ToUpper
                                    txtEmail.Text = row("RefEmail2").ToString.ToUpper
                                    txtFax.Text = row("RefFax2").ToString.ToUpper
                                    txtPhone.Text = row("RefPhone2").ToString.ToUpper
                                    txtAddress1.Text = row("MailAddr1").ToString.ToUpper
                                    txtAddress2.Text = row("MailAddr2").ToString.ToUpper
                                    'txtAddress3.Text = row("MailAddr3").ToString.ToUpper
                                    txtCity.Text = row("MailAddr3").ToString.ToUpper
                                    txtState.Text = row("MailAddr4").ToString.ToUpper
                                    txtZip.Text = row("MailAddr5").ToString.ToUpper
                                    txtCountry.Text = row("MailAddr6").ToString.ToUpper
                                    txtStreetAddress1.Text = row("StreetAddr1").ToString.ToUpper
                                    txtStreetAddress2.Text = row("StreetAddr2").ToString.ToUpper
                                    txtStreetAddress3.Text = row("StreetAddr3").ToString.ToUpper
                                    txtStreetAddress4.Text = row("StreetAddr4").ToString.ToUpper
                                    txtStreetAddress5.Text = row("StreetAddr5").ToString.ToUpper
                                    txtStreetAddress6.Text = row("StreetAddr6").ToString.ToUpper
                                    'txtStreetAddress7.Text = row("StreetAddr7").ToString.ToUpper

                                Case "PLANT"
                                    txtFirstName.Visible = False
                                    txtLastName.Visible = False
                                    txtName.Visible = True
                                    CoType = "Plant Contact"
                                    txtName.Text = row("CoordName").ToString.ToUpper
                                    txtTitle.Text = row("CoordTitle").ToString.ToUpper
                                    txtEmail.Text = row("www").ToString.ToUpper
                                    txtFax.Text = row("fax").ToString.ToUpper
                                    txtPhone.Text = row("telephone").ToString.ToUpper
                                    txtAddress1.Text = row("Street").ToString.ToUpper
                                    txtCity.Text = row("City").ToString.ToUpper
                                    txtState.Text = row("State").ToString.ToUpper
                                    txtZip.Text = row("Zip").ToString.ToUpper
                                    txtCountry.Text = row("Country").ToString.ToUpper


                                Case "PRICING"
                                    txtFirstName.Visible = False
                                    txtLastName.Visible = False
                                    txtName.Visible = True
                                    CoType = "Pricing Contact"
                                    txtName.Text = row("PricingContact").ToString.ToUpper
                                    txtTitle.Text = "PRICING CONTACT"
                                    txtEmail.Text = row("PricingContactEmail").ToString.ToUpper

                                Case "DC"
                                    txtFirstName.Visible = False
                                    txtLastName.Visible = False
                                    txtName.Visible = True
                                    CoType = "Data Coordinator"
                                    txtName.Text = row("DCContact").ToString.ToUpper
                                    txtTitle.Text = "DATA COORDINATOR"
                                    txtEmail.Text = row("DCContactEmail").ToString.ToUpper
                            End Select
                        Else
                            MessageBox.Show("Error Reading Contact Info")
                        End If
                    End If

            End Select
            'txtTitle.Text = CoType

        Catch Ex As System.Exception
            MessageBox.Show(Ex.Message)
        End Try
    End Sub
    Private Sub ContactForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        If Utilities.CheckUser(UserName) Then

            btnUpdate.Visible = True
            txtFirstName.ReadOnly = False
            txtLastName.ReadOnly = False
            txtTitle.ReadOnly = False
            txtEmail.ReadOnly = False
            txtPhone.ReadOnly = False
            txtPassword.ReadOnly = False
            txtFax.ReadOnly = False
            txtAddress1.ReadOnly = False
            txtAddress2.ReadOnly = False
            txtAddress3.ReadOnly = False
            txtCity.ReadOnly = False
            txtState.ReadOnly = False
            txtZip.ReadOnly = False
            txtCountry.ReadOnly = False
            txtStreetAddress1.ReadOnly = False
            txtStreetAddress2.ReadOnly = False
            txtStreetAddress3.ReadOnly = False
            txtStreetAddress4.ReadOnly = False
            txtStreetAddress5.ReadOnly = False
            txtStreetAddress6.ReadOnly = False
            txtStreetAddress7.ReadOnly = False

        Else

            btnUpdate.Visible = False
            txtFirstName.ReadOnly = True
            txtLastName.ReadOnly = True
            txtTitle.ReadOnly = True
            txtEmail.ReadOnly = True
            txtPhone.ReadOnly = True
            txtPassword.ReadOnly = True
            txtFax.ReadOnly = True
            txtAddress1.ReadOnly = True
            txtAddress2.ReadOnly = True
            txtAddress3.ReadOnly = True
            txtCity.ReadOnly = True
            txtState.ReadOnly = True
            txtZip.ReadOnly = True
            txtCountry.ReadOnly = True
            txtStreetAddress1.ReadOnly = True
            txtStreetAddress2.ReadOnly = True
            txtStreetAddress3.ReadOnly = True
            txtStreetAddress4.ReadOnly = True
            txtStreetAddress5.ReadOnly = True
            txtStreetAddress6.ReadOnly = True
            txtStreetAddress7.ReadOnly = True
        End If

        LoadContacts()

    End Sub

    Private Sub UpdateInterimContact()
        Dim params As List(Of String)

        params = New List(Of String)
        params.Add("@RefNum/" & RefNum)
        params.Add("@FirstName/" & txtFirstName.Text.Trim)
        params.Add("@LastName/" & Utilities.FixQuotes(txtLastName.Text.Trim))
        params.Add("@Email/" & Utilities.FixQuotes(txtEmail.Text.Trim))
        params.Add("@Phone/" & Utilities.FixQuotes(txtPhone.Text.Trim))

        Dim result As Integer = 0
        result = da.ExecuteNonQuery("Console.UpdateInterimContactInfo", params)

        If result > 0 Then
            MessageBox.Show("Interim Company Contact Updated")
        Else
            MessageBox.Show("Interim Company Contact not Updated")
        End If
        Me.Close()
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
    Private Sub UpdateCompanyContact(ContactType As String)

        Dim params As List(Of String)
        params = New List(Of String)

        params.Add("@RefNum/" & RefNum)
        params.Add("@JobTitle/" & Utilities.FixQuotes(txtTitle.Text.Trim.Replace(",", "")))
        params.Add("@FirstName/" & txtFirstName.Text.Trim)
        params.Add("@LastName/" & Utilities.FixQuotes(txtLastName.Text.Trim))
        params.Add("@Address1/" & Utilities.FixQuotes(txtAddress1.Text.Trim))
        params.Add("@Address2/" & Utilities.FixQuotes(txtAddress2.Text.Trim))
        params.Add("@Address3/" & Utilities.FixQuotes(txtAddress3.Text.Trim))
        params.Add("@City/" & txtCity.Text.Trim)
        params.Add("@State/" & txtState.Text.Trim)
        params.Add("@Zip/" & txtZip.Text.Trim)
        params.Add("@Country/" & txtCountry.Text.Trim)
        params.Add("@StAddress1/" & Utilities.FixQuotes(txtStreetAddress1.Text.Trim))
        params.Add("@StAddress2/" & Utilities.FixQuotes(txtStreetAddress2.Text.Trim))
        params.Add("@StAddress3/" & Utilities.FixQuotes(txtStreetAddress3.Text.Trim))
        params.Add("@StAddress4/" & Utilities.FixQuotes(txtStreetAddress4.Text.Trim))
        params.Add("@StAddress5/" & Utilities.FixQuotes(txtStreetAddress5.Text.Trim))
        params.Add("@StAddress6/" & Utilities.FixQuotes(txtStreetAddress6.Text.Trim))
        params.Add("@StAddress7/" & Utilities.FixQuotes(txtStreetAddress7.Text.Trim))
        params.Add("@Email/" & Utilities.FixQuotes(txtEmail.Text.Trim))
        params.Add("@Phone/" & txtPhone.Text.Trim)
        params.Add("@Password/" & txtPassword.Text.Trim)
        params.Add("@Fax/" & txtFax.Text.Trim)
        params.Add("@ContactType/" & ContactType)
        'params.Add("@StudyYear/" & StudyYear)
        Dim result As Integer = 0
        If StudyType = "OLEFINS" Then
            result = da.ExecuteNonQuery("Console.UpdateContact", params)
            If result > 0 Then
                MessageBox.Show(CoType & " Contact Updated. REFNUM: " & RefNum)
            Else
                MessageBox.Show(CoType & " Contact Not Updated. REFNUM: " & RefNum)
            End If
        Else
            result = da.ExecuteNonQuery("Console.UpdateCoContact", params)
            If result > 0 Then
                MessageBox.Show(CoType & " Contact Updated")
            Else
                MessageBox.Show(CoType & " Contact Not Updated")
            End If

        End If

        Me.Close()
    End Sub
    Private Sub UpdateAlternateContact()

        Dim params As List(Of String)

        params = New List(Of String)
        If StudyType = "OLEFINS" Then params.Add("@StudyYear/" & StudyYear)
        params.Add("@RefNum/" & RefNum)
        params.Add("@AltFirstName/" & txtFirstName.Text.Trim)
        params.Add("@AltLastName/" & Utilities.FixQuotes(txtLastName.Text.Trim))
        params.Add("@AltEmail/" & Utilities.FixQuotes(txtEmail.Text.Trim))

        Dim result As Integer = 0
        If StudyType = "OLEFINS" Then
            result = da.ExecuteNonQuery("Console.UpdateOleAltContact", params)
        Else
            result = da.ExecuteNonQuery("Console.UpdateCoAltContact", params)
        End If
        If result > 0 Then
            MessageBox.Show("Alternate Company Contact Updated")
        Else
            MessageBox.Show("Alternate Company Contact not Updated")
        End If
        Me.Close()
    End Sub


    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click

        Select Case CoType.ToUpper
            Case "COORD"
                UpdateCompanyContact("COORD")
                RaiseEvent UpdateContact()
            Case "ALT"
                UpdateCompanyContact("ALT")
                RaiseEvent UpdateAltContact()
            Case "INTERIM"
                UpdateInterimContact()
                RaiseEvent UpdateContact()
        End Select

    End Sub


    Private Sub btnCopy_Click(sender As System.Object, e As System.EventArgs) Handles btnCopy.Click
        CopyContent = txtName.Text + vbCrLf
        CopyContent += txtFirstName.Text + vbCrLf
        CopyContent += txtLastName.Text & vbCrLf
        CopyContent += txtTitle.Text & vbCrLf
        CopyContent += txtEmail.Text & vbCrLf
        CopyContent += txtPhone.Text & vbCrLf
        CopyContent += txtPassword.Text & vbCrLf
        CopyContent += txtFax.Text & vbCrLf
        CopyContent += txtAddress1.Text & vbCrLf
        CopyContent += txtAddress2.Text & vbCrLf
        CopyContent += txtAddress3.Text & vbCrLf
        CopyContent += txtCity.Text & vbCrLf
        CopyContent += txtState.Text & vbCrLf
        CopyContent += txtZip.Text & vbCrLf
        CopyContent += txtCountry.Text & vbCrLf
        CopyContent += txtStreetAddress1.Text & vbCrLf
        CopyContent += txtStreetAddress2.Text & vbCrLf
        CopyContent += txtStreetAddress3.Text & vbCrLf
        CopyContent += txtStreetAddress4.Text & vbCrLf
        CopyContent += txtStreetAddress5.Text & vbCrLf
        CopyContent += txtStreetAddress6.Text & vbCrLf
        CopyContent += txtStreetAddress7.Text
        Clipboard.SetText(CopyContent)

    End Sub

    Private Sub btnPopulateStreetAddress_Click(sender As System.Object, e As System.EventArgs) Handles btnPopulateStreetAddress.Click
        txtStreetAddress1.Text = txtAddress1.Text
        txtStreetAddress2.Text = txtAddress2.Text
        txtStreetAddress3.Text = txtAddress3.Text
        txtStreetAddress4.Text = txtCity.Text
        txtStreetAddress5.Text = txtState.Text
        txtStreetAddress7.Text = txtCountry.Text
        txtStreetAddress6.Text = txtZip.Text
    End Sub
End Class
