﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Solomon.Common.Cryptography
{
    /// <summary>
    /// Objective:	Create, Derive and verify passwords.
    /// </summary>
    public static class Password
    {
        #region Derived Passwords

        /// <summary>
        /// Compares byte by byte of the passwords to slow the verification process. The slow comparison is by design.
        /// </summary>
        /// <param name="actualPassword"></param>
        /// <param name="expectedPassword"></param>
        /// <returns>Success or failure of the password verification.</returns>
        internal static bool SlowEquals(byte[] enteredPassword, byte[] expectedPassword)
        {
            try
            {
                int diff = enteredPassword.Length ^ expectedPassword.Length;

                int indexMax = System.Math.Max(enteredPassword.Length, expectedPassword.Length);
                int indexEntered;
                int indexExpected;

                for (int i = 0; i < indexMax; i++)
                {
                    indexEntered = System.Math.Min(i, enteredPassword.Length);
                    indexExpected = System.Math.Min(i, expectedPassword.Length);

                    diff |= enteredPassword[indexEntered] ^ expectedPassword[indexExpected];
                }

                return diff == 0;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Derives a password from a 'password' and a 'salt'.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt">Must be at least 8 bytes (64 bits).</param>
        /// <param name="passwordLength">Must be at least 16 bytes (128 Bits); 64 bytes (512 bits) is recommended.</param>
        /// <param name="derivedPassword"></param>
        /// <returns>Success or failure of the password derivation; Zero byte array is created if password derivation fails.</returns>
        public static bool Derive(byte[] password, byte[] salt, int passwordLength, out byte[] derivedPassword)
        {
            //	http://www.isprimenumber.com/prime/4096
            //	when 4099 becomes too few iterations, please use 8191 (Mersenne prime); this roughly doubles the time to derive a password
            const int RfcIterations = 4099;

            try
            {
                if (salt.Length >= 8 && passwordLength >= 16)
                {
                    using (Rfc2898DeriveBytes rfcKey = new Rfc2898DeriveBytes(password, salt, RfcIterations))
                    {
                        derivedPassword = rfcKey.GetBytes(passwordLength);
                        rfcKey.Reset();
                        return true;
                    }
                }
                else
                {
                    derivedPassword = new byte[0];
                    return false;
                }
            }
            catch
            {
                derivedPassword = new byte[0];
                return false;
            }
        }

        /// <summary>
        /// Verifies a derived password.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt">Must be at least 8 bytes (64 bits).</param>
        /// <param name="expectedPassword"></param>
        /// <returns>Success or failure of the password verification; Zero byte array is created if password verification fails.</returns>
        /// <remarks>Verification should take about 0.3 ms (1/3 second) on a server. The speed is set by Derive.RfcIterations.</remarks>
        public static bool Verify(byte[] password, byte[] salt, byte[] expectedPassword)
        {
            byte[] enteredPassword;

            Password.Derive(password, salt, expectedPassword.Length, out enteredPassword);

            return Password.SlowEquals(enteredPassword, expectedPassword);
        }

        #endregion

        /// <summary>
        /// Generates a cryptographically random password of the specified length.
        /// </summary>
        /// <param name="passwordLength">64 bytes (512 bits) is recommended.</param>
        /// <param name="generatedPassword"></param>
        /// <returns>Success or failure of the random password generation; Zero byte array is created if password generation fails.</returns>
        public static bool Generate(int passwordLength, out byte[] generatedPassword)
        {
            generatedPassword = new byte[passwordLength];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(generatedPassword);
            }

            return true;
        }
    }
}
