﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Deployment;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Security.Cryptography;
using System.IO;
using System.Threading;
using System.Data.Odbc;

namespace VerifySend
{
    public partial class ThisAddIn
    {
        Outlook.Inspectors inspectors;
        private Office.CommandBar menuBar;
        private Office.CommandBarPopup newMenuBar;
        private Office.CommandBarButton buttonOne;
        private Office.CommandBarButton btnSearchSelected;
        bool hasMismatch = false;
        Form frmVerifySendSettings = new Form();
        CheckBox cbEnabled = new CheckBox();
        Form frmMismatchReport = new Form();
        DataTable dt = new DataTable();
        string context = string.Empty;
        string strRegex = string.Empty;
        bool myCancel = true;
        //bool myCancel = false;
        [DllImport("wininet.dll", SetLastError = true)]
        public static extern bool InternetGetConnectedState(out int flags, int reserved);
        [DllImport("wininet.dll", SetLastError = true)]
        public static extern int InternetAttemptConnect(uint res);
        private static int ERROR_SUCCESS = 0;
        Form frmProgressWindow = new Form();
        Label lblStatus = new Label();
        Label lblWarning = new Label();
        DataGridView dgv = new DataGridView();
        Button btnMistmatchReportOK = new Button();
        Button btnMistmatchReportCancel = new Button();
        bool hasAttachmentToExtrnal = false;
        string localDataDirectory = string.Empty;
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr SetLastError(int dwErrCode);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool EndTask(IntPtr hWnd, bool fShutDown, bool fForce);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, int lpdwProcessId);
        Form frmAttachmentPasswordForm = new Form();
        string attachmentPassword = string.Empty;
        Form frmSearchProgress = new Form();
        ProgressBar pbScanStatus = new ProgressBar();
        Label lblScanStatus = new Label();
        bool isAttachmentPasswordValid;
        TextBox txtAttachmentPassword = new TextBox();
        Label lblAttachmentPassword = new Label();
        bool passwordGiveup;
        string currentFile = string.Empty;
        private byte[] Key = { 248, 215, 80, 175, 235, 183, 19, 200, 195, 97, 212, 123, 110, 0, 192, 83, 148, 33, 49, 121, 147, 240, 138, 188, 113, 177, 209, 112, 145, 219, 53, 78 };
        private byte[] Vector = { 86, 102, 197, 82, 75, 101, 134, 69, 250, 94, 79, 187, 146, 83, 18, 128 };
        IntPtr iHandlePP = IntPtr.Zero;
        string CaptionPP = string.Empty;
        //Outlook.Explorer explorer;
        System.ComponentModel.BackgroundWorker backgroundWorker1x = new System.ComponentModel.BackgroundWorker();
        bool GlobalCancel = false;
        //private AutoResetEvent _resetEvent = new AutoResetEvent(false);
        PictureBox picboxClose = new PictureBox();
        List<string> DLMFiles = new List<string>();

        #region Add-in Startup
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            inspectors = this.Application.Inspectors;
            //Application.ItemSend += new Microsoft.Office.Interop.Outlook.ApplicationEvents_11_ItemSendEventHandler(verifyEmail);
            Application.ItemSend += new Microsoft.Office.Interop.Outlook.ApplicationEvents_11_ItemSendEventHandler(verifyEmail2);
            //Application.ItemSend += new Microsoft.Office.Interop.Outlook.ApplicationEvents_11_ItemSendEventHandler(verifyEmail3);
            
            //explorer = this.Application.ActiveExplorer();
            //explorer.BeforeItemPaste += new Microsoft.Office.Interop.Outlook.ExplorerEvents_10_BeforeItemPasteEventHandler(Explorer_BeforeItemPaste);

            AddMenuBar();

            createSettingsForm();

            dt.Columns.Add("Mismatch");
            dt.Columns.Add("Location");
            dt.Columns.Add("Context");

            frmMismatchReport.ControlBox = false;
            frmMismatchReport.FormClosing += new FormClosingEventHandler(frmMismatchReport_FormClosing);

            createMismatchDataGridView();

            Properties.Settings.Default.showNoConnectionWarning = true;
            Properties.Settings.Default.isEnabled = true;

            localDataDirectory = System.Environment.CurrentDirectory;

            System.IO.FileInfo fi = new System.IO.FileInfo(localDataDirectory + "\\verifysend.csv");
            TimeSpan span = DateTime.Now - fi.LastWriteTime;
            if (span.Days > 3)
            {
                if (IsSqlConnected())
                    saveDataLocal();
                else
                {
                    try
                    {
                        System.Net.WebClient wc = new System.Net.WebClient();
                        wc.DownloadFile("https://webservices.solomononline.com/RAMService/verifySend.csv", localDataDirectory + "\\verifySendENC.csv");
                        File.WriteAllText(localDataDirectory + "\\verifySend.csv", decrypt(Convert.FromBase64String(File.ReadAllText(localDataDirectory + "\\verifySendENC.csv"))));
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            createAttachmentPasswordForm();
            CreateSearchProgressForm();
        }
        #endregion

        #region Decrypt
        /// <summary>
        /// Decrypts .CSV of alias table downloaded from the website
        /// </summary>
        /// <param name="EncryptedValue"></param>
        /// <returns></returns>
        private string decrypt(byte[] EncryptedValue)
        {
            try
            {
                using (RijndaelManaged rm = new RijndaelManaged())
                {
                    ICryptoTransform DecryptorTransform = rm.CreateDecryptor(Key, Vector);
                    UTF8Encoding UTFEncoder = new System.Text.UTF8Encoding();

                    using (MemoryStream encryptedStream = new MemoryStream())
                    {
                        using (CryptoStream decryptStream = new CryptoStream(encryptedStream, DecryptorTransform, CryptoStreamMode.Write))
                        {
                            decryptStream.Write(EncryptedValue, 0, EncryptedValue.Length);
                            decryptStream.FlushFinalBlock();

                            encryptedStream.Position = 0;
                            Byte[] decryptedBytes = new Byte[encryptedStream.Length];
                            encryptedStream.Read(decryptedBytes, 0, decryptedBytes.Length);
                            encryptedStream.Close();

                            return UTFEncoder.GetString(decryptedBytes);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("decrypt", ex.Message, ex.StackTrace);
                return "";
            }
        }
        #endregion

        #region Ensure Process Killed
        /// <summary>
        /// Kills Excel/Word process after i'm finished with it.
        /// </summary>
        /// <param name="MainWindowHandle"></param>
        /// <param name="Caption"></param>
        public void EnsureProcessKilled(IntPtr MainWindowHandle, string Caption)
        {
            try
            {
                SetLastError(0);

                if (IntPtr.Equals(MainWindowHandle, IntPtr.Zero))
                    MainWindowHandle = FindWindow(null, Caption);

                if (IntPtr.Equals(MainWindowHandle, IntPtr.Zero))
                    return;

                int iRes = 0;
                int iProcID = 0;
                iRes = GetWindowThreadProcessId(MainWindowHandle, iProcID);

                if (iProcID == 0)
                {
                    if (EndTask(MainWindowHandle, true, true))
                        return;
                    throw new ApplicationException("Failed to close.");
                }

                System.Diagnostics.Process proc = null;
                proc = System.Diagnostics.Process.GetProcessById(iProcID);
                proc.CloseMainWindow();
                proc.Refresh();

                if (proc.HasExited)
                    return;

                proc.Kill();
            }
            catch (Exception ex)
            {
                SendErrorEmail("EnsureProcessKilled", ex.Message, ex.StackTrace);
            }
        }
        #endregion      

        #region Extract files from Zip
        /// <summary>
        /// Extracts files from zip attachments and sends files contained within to search function.
        /// </summary>
        /// <param name="ds">DataSet of search terms</param>
        /// <param name="att">Outlook.Attachment attachment</param>
        /// <param name="attachmentPassword">Attachment Password</param>
        private void extractZip(DataSet ds, Outlook.Attachment att, string attachmentPassword)
        {
            try
            {
                ICollection<string> zippedFiles;
                att.SaveAsFile(System.IO.Path.GetTempPath() + att.FileName);
                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(System.IO.Path.GetTempPath() + att.FileName))
                {
                    zip.Password = attachmentPassword;
                    zippedFiles = zip.EntryFileNames;
                    zip.ExtractAll(System.IO.Path.GetTempPath(), Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                    var files = from f in zippedFiles
                                where f.ToLower().Contains(".xls") || f.ToLower().Contains(".xlsx") || f.ToLower().Contains(".doc") || f.ToLower().Contains(".docx")
                                select f;
                    int q = 0;
                    foreach (string filename in zippedFiles)
                    {
                        if (filename.ToLower().Contains(".xls") || filename.ToLower().Contains(".xlsx") || filename.ToLower().Contains(".doc") || filename.ToLower().Contains(".docx"))
                        {
                            q++;
                            //zip.ExtractSelectedEntries("*.*", null, System.IO.Path.GetTempPath(), Ionic.Zip.ExtractExistingFileAction.OverwriteSilently);
                            string attType = string.Empty;
                            if (filename.ToLower().Contains(".xls") || filename.ToLower().Contains(".xlsx"))
                                attType = "Excel";
                            if (filename.ToLower().Contains(".doc") || filename.ToLower().Contains(".docx"))
                                attType = "Word";
                            //pbScanStatus.Value = 0;
                            updatePBValue(0);
                            updateScanStatusLabel("Scanning ZIP contents. File " + q.ToString() + " of " + files.Count() + ". Please wait...");
                            SearchAttachment(ds, filename, attachmentPassword, attType);
                            if (!isAttachmentPasswordValid)
                                return;
                        }
                    }

                    if (q == 0)
                        isAttachmentPasswordValid = true;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("The password did not match."))
                    return;
                else
                    SendErrorEmail("extractZip", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Search Attachment
        /// <summary>
        /// Searches email attachments (Excel/Word/PowerPoint).
        /// </summary>
        /// <param name="ds">DataSet of search terms</param>
        /// <param name="fileName">Attachment FileName</param>
        /// <param name="attachmentPassword">Attachment Password</param>
        /// <param name="attachmentType">Attachment Type ["Word"/"Excel"/"PowerPoint"]</param>
        private void SearchAttachment(DataSet ds, string fileName, string attachmentPassword, string attachmentType)
        {

            showSearchProgressCloseButton();

            //pbScanStatus.Value = 0;
            updatePBValue(0);

            if (attachmentType == "PowerPoint")
            {
                if (!passwordGiveup)
                {
                    PowerPoint.Application ppApp = new PowerPoint.Application();
                    ppApp.Caption = Guid.NewGuid().ToString();
                    CaptionPP = ppApp.Caption;

                    try
                    {
                        DateTime ppstart = DateTime.Now;

                        PowerPoint.Presentation pres = ppApp.Presentations.Open(System.IO.Path.GetTempPath() + fileName, Office.MsoTriState.msoTrue, Office.MsoTriState.msoFalse, Office.MsoTriState.msoFalse);

                        isAttachmentPasswordValid = true;
                        //pbScanStatus.Maximum = pres.Slides.Count; //ds.Tables[0].Rows.Count;// * ((PowerPoint.Presentation)pres).Slides.Count;
                        updatePBMaximum(pres.Slides.Count);

                        string ppText = string.Empty;

                        foreach (PowerPoint.Slide sld in pres.Slides)
                        {
                            foreach (PowerPoint.Shape shp in sld.Shapes)
                            {
                                //string str = shp.TextFrame.TextRange.Text;
                                if (shp.HasTextFrame == Office.MsoTriState.msoTrue)
                                {
                                    ppText = ppText + " " + shp.TextFrame.TextRange.Text;
                                }
                            }
                            //pbScanStatus.Increment(1);
                            incrementPB(1);

                            if (GlobalCancel == true)
                            {
                                var dialogResult = MessageBox.Show("Warning: if you cancel attachment scanning, you will not be able to send this email. Click OK to continue scanning or Cancel to abort.", "Warning", MessageBoxButtons.OKCancel);
                                if (dialogResult == DialogResult.Cancel)
                                {
                                    passwordGiveup = true;
                                    backgroundWorker1x.CancelAsync();
                                    return;
                                }
                                else
                                    GlobalCancel = false;
                            }

                        }

                        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                        {
                            string searchText = ds.Tables[0].Rows[x][0].ToString();
                            strRegex = @"\b" + ds.Tables[0].Rows[x][0] + @"\b";

                            int ppcount = Regex.Matches(ppText.Replace("\n", " ").Replace("\r", " ").Replace("\v", " "), strRegex, RegexOptions.IgnoreCase).Count;
                            if (ppcount > 0)
                            {
                                for (int q = 0; q < ppcount; q++)
                                {
                                    hasMismatch = true;
                                    int index = Regex.Matches(ppText, strRegex, RegexOptions.IgnoreCase)[q].Index;
                                    if (ppText.Substring(index).Length > 80)
                                        dt.Rows.Add(ds.Tables[0].Rows[x][0], fileName, ppText.Substring(index, 80));
                                    else
                                        dt.Rows.Add(ds.Tables[0].Rows[x][0], fileName, ppText.Substring(index));
                                }
                            }
                        }
                        TimeSpan totaltime = DateTime.Now - ppstart;
                        pres.Close();
                        releaseObject(pres);
                        ppApp.Quit();
                        releaseObject(ppApp);
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.ToString());
                        if (ex.Message.Contains("E_FAIL"))
                        {
                            passwordGiveup = true;
                            return;
                        }
                        else if (ex.Message.Contains("The password is incorrect"))
                        {
                            MessageBox.Show("The password you supplied is not correct for attachment " + fileName + ". Verify that the CAPS LOCK key is off and be sure to use the correct capitalization.");
                            isAttachmentPasswordValid = false;
                            currentFile = fileName;
                            return;
                        }
                        else
                            SendErrorEmail("SearchAttachment", ex.Message, ex.StackTrace);
                    }

                }
            }

            if (attachmentType == "Word")
            {
                if (!passwordGiveup)
                {
                    if (attachmentPassword == "")
                        attachmentPassword = "whatever";
                    Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
                    wordApp.Visible = false;
                    wordApp.Caption = Guid.NewGuid().ToString();

                    try
                    {
                        Word.Document doc = wordApp.Documents.Open(System.IO.Path.GetTempPath() + fileName, false, true, false, attachmentPassword, missing, missing, missing, missing, missing, missing, false, false, missing, true, missing);
                        isAttachmentPasswordValid = true;
                        //pbScanStatus.Maximum = ds.Tables[0].Rows.Count;
                        updatePBMaximum(ds.Tables[0].Rows.Count);

                        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                        {
                            string searchText = ds.Tables[0].Rows[x][0].ToString();

                            Word.Range rgFound = doc.Content;

                            //pbScanStatus.Increment(1);
                            incrementPB(1);

                            if (GlobalCancel == true)
                            {
                                var dialogResult = MessageBox.Show("Warning: if you cancel attachment scanning, you will not be able to send this email. Click OK to continue scanning or Cancel to abort.", "Warning", MessageBoxButtons.OKCancel);
                                if (dialogResult == DialogResult.Cancel)
                                {
                                    passwordGiveup = true;
                                    backgroundWorker1x.CancelAsync();
                                    return;
                                }
                                else
                                    GlobalCancel = false;
                            }

                            rgFound.Find.ClearFormatting();
                            rgFound.Find.Execute(searchText, false, true, false, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
                            if (rgFound != null)
                            {
                                while (rgFound.Find.Found)
                                {
                                    rgFound.Find.Execute(searchText, false, true, false, false, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);
                                    Word.Range sel = doc.Content;
                                    sel.Start = rgFound.Start - 20;
                                    sel.End = rgFound.End + 20;
                                    sel.Select();
                                    hasMismatch = true;
                                    //dt.Rows.Add(searchText, "Attachment", sel.Text);
                                    dt.Rows.Add(searchText, fileName, sel.Text);
                                }
                            }
                        }
                        //doc.Close(false, missing, missing);
                        ((Word._Document)doc).Close(false, missing, missing);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("The password is incorrect"))
                        {
                            MessageBox.Show("The password you supplied is not correct for attachment " + fileName + ". Verify that the CAPS LOCK key is off and be sure to use the correct capitalization.");
                            isAttachmentPasswordValid = false;
                            currentFile = fileName;
                            return;
                        }
                        //else
                        //    throw ex;
                        else
                            SendErrorEmail("SearchAttachment", ex.Message, ex.StackTrace);
                    }
                    finally
                    {
                        //wordApp.Quit(false, missing, missing);
                        ((Word._Application)wordApp).Quit(false, missing, missing);
                    }
                }
            }

            if (attachmentType == "Excel")
            {
                if (!passwordGiveup)
                {
                    Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                    excelApp.Visible = false;
                    excelApp.Caption = Guid.NewGuid().ToString();

                    excelApp.DisplayAlerts = false;

                    try
                    {
                        Excel.Workbook wb = excelApp.Application.Workbooks.Open(System.IO.Path.GetTempPath() + fileName, false, true, missing, attachmentPassword, missing, true, missing, missing, false, missing, missing, missing, missing, missing);
                        isAttachmentPasswordValid = true;

                        //pbScanStatus.Maximum = ds.Tables[0].Rows.Count * wb.Worksheets.Count;
                        updatePBMaximum(ds.Tables[0].Rows.Count * wb.Worksheets.Count);
                        //System.Threading.Tasks.Parallel.Invoke(() => { updatePBMaximum(ds.Tables[0].Rows.Count * wb.Worksheets.Count); });

                        for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                        {
                            strRegex = @"\b" + ds.Tables[0].Rows[x][0] + @"\b";
                            string searchText = strRegex.Replace("\\b", "");

                            Excel.Range rgFound;
                            string FirstFoundAddress;

                            foreach (Excel.Worksheet ws in wb.Worksheets)
                            {
                                //pbScanStatus.Increment(1);
                                incrementPB(1);
                                //System.Threading.Tasks.Parallel.Invoke(() => { incrementPB(1); });
                                //System.ComponentModel.BackgroundWorker backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
                                //backgroundWorker1.WorkerSupportsCancellation = true;

                                //backgroundWorker1.DoWork += (o, e) =>
                                //{
                                //    incrementPB(1);
                                //};
                                //backgroundWorker1.RunWorkerAsync();         

                                //frmSearchProgress.Update();

                                if (GlobalCancel == true)
                                {
                                    var dialogResult = MessageBox.Show("Warning: if you cancel attachment scanning, you will not be able to send this email. Click OK to continue scanning or Cancel to abort.", "Warning", MessageBoxButtons.OKCancel);
                                    if (dialogResult == DialogResult.Cancel)
                                    {
                                        passwordGiveup = true;
                                        backgroundWorker1x.CancelAsync();
                                        return;
                                    }
                                    else
                                        GlobalCancel = false;
                                }

                                rgFound = ws.Cells.Find(searchText, ws.Cells[1, 1], Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlPart, missing, Excel.XlSearchDirection.xlNext, false, missing, missing);

                                if (rgFound != null)
                                {
                                    FirstFoundAddress = rgFound.get_Address(true, true, Excel.XlReferenceStyle.xlA1, missing, missing);

                                    rgFound = ws.Cells.FindNext(rgFound);
                                    string sAddress = rgFound.get_Address(true, true, Excel.XlReferenceStyle.xlA1, missing, missing);
                                    if (Regex.Matches(rgFound.Value2.ToString(), strRegex, RegexOptions.IgnoreCase).Count > 0)
                                    {
                                        hasMismatch = true;
                                        //dt.Rows.Add(searchText, "Attachment", rgFound.Value2.Substring(Regex.Matches(rgFound.Value2.ToString(), strRegex, RegexOptions.IgnoreCase)[0].Index));
                                        dt.Rows.Add(searchText, fileName, rgFound.Value2.Substring(Regex.Matches(rgFound.Value2.ToString(), strRegex, RegexOptions.IgnoreCase)[0].Index));
                                    }

                                    while (!sAddress.Equals(FirstFoundAddress))
                                    {
                                        rgFound = ws.Cells.FindNext(rgFound);
                                        sAddress = rgFound.get_Address(true, true, Excel.XlReferenceStyle.xlA1, missing, missing);
                                        if (Regex.Matches(rgFound.Value2.ToString(), strRegex, RegexOptions.IgnoreCase).Count > 0)
                                        {
                                            hasMismatch = true;
                                            //dt.Rows.Add(searchText, "Attachment", rgFound.Value2.Substring(Regex.Matches(rgFound.Value2.ToString(), strRegex, RegexOptions.IgnoreCase)[0].Index));
                                            dt.Rows.Add(searchText, fileName, rgFound.Value2.Substring(Regex.Matches(rgFound.Value2.ToString(), strRegex, RegexOptions.IgnoreCase)[0].Index));
                                        }
                                    }
                                }
                            }
                        }

                        wb.Close(false, missing, missing);
                        releaseObject(wb);

                        excelApp.Workbooks.Close();
                        releaseObject(excelApp.Workbooks);

                        excelApp.Quit();
                        //releaseObject(excelApp);

                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("The password you supplied"))
                        {
                            MessageBox.Show("The password you supplied is not correct for attachment " + fileName + ". Verify that the CAPS LOCK key is off and be sure to use the correct capitalization.");
                            isAttachmentPasswordValid = false;
                            currentFile = fileName;
                            return;
                        }
                        //else
                        //    throw ex;
                        else
                            SendErrorEmail("SearchAttachment", ex.Message, ex.StackTrace);
                    }
                    finally
                    {
                        IntPtr iHandle = IntPtr.Zero;
                        iHandle = new IntPtr(Convert.ToInt32(excelApp.Parent.Hwnd));

                        EnsureProcessKilled(iHandle, excelApp.Caption);
                    }
                }
            }

            if (attachmentType == "PDF")
            {
                //isAttachmentPasswordValid = true;
                if (!passwordGiveup)
                {
                    try
                    {
                        PDFParser pdfparser = new PDFParser();
                        //string pdfText = pdfparser.searchPDF(System.IO.Path.GetTempPath() + fileName, attachmentPassword);
                        PDFParser.pdfSearchResults pdfsr = new PDFParser.pdfSearchResults();
                        pdfsr = pdfparser.searchPDF(System.IO.Path.GetTempPath() + fileName, attachmentPassword);
                        if (pdfsr.Success == true)
                        {
                            isAttachmentPasswordValid = true;

                            updatePBMaximum(ds.Tables[0].Rows.Count);

                            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                            {
                                incrementPB(1);
                                if (GlobalCancel == true)
                                {
                                    var dialogResult = MessageBox.Show("Warning: if you cancel attachment scanning, you will not be able to send this email. Click OK to continue scanning or Cancel to abort.", "Warning", MessageBoxButtons.OKCancel);
                                    if (dialogResult == DialogResult.Cancel)
                                    {
                                        passwordGiveup = true;
                                        backgroundWorker1x.CancelAsync();
                                        return;
                                    }
                                    else
                                        GlobalCancel = false;
                                }

                                string searchText = ds.Tables[0].Rows[x][0].ToString();
                                strRegex = @"\b" + ds.Tables[0].Rows[x][0] + @"\b";

                                int pdfcount = Regex.Matches(pdfsr.Result, strRegex, RegexOptions.IgnoreCase).Count;
                                if (pdfcount > 0)
                                {
                                    for (int q = 0; q < pdfcount; q++)
                                    {
                                        hasMismatch = true;
                                        int index = Regex.Matches(pdfsr.Result, strRegex, RegexOptions.IgnoreCase)[q].Index;
                                        if (pdfsr.Result.Substring(index).Length > 80)
                                            dt.Rows.Add(ds.Tables[0].Rows[x][0], fileName, pdfsr.Result.Substring(index, 80));
                                        else
                                            dt.Rows.Add(ds.Tables[0].Rows[x][0], fileName, pdfsr.Result.Substring(index));
                                    }
                                }
                            }
                        }
                        else
                        {
                            //MessageBox.Show(pdfsr.Result);
                            //if (pdfsr.Result == "Error processing pdf.")
                            //    isAttachmentPasswordValid = true;
                            //currentFile = fileName;
                            //return;
                            MessageBox.Show("Unable to scan PDF file. Please manually verify document does not contain mismatched client names/data.");
                            isAttachmentPasswordValid = true;
                            currentFile = fileName;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("Error processing pdf."))
                        {
                            MessageBox.Show("Could not process PDF file.");
                        }
                        else
                            SendErrorEmail("SearchAttachment", ex.Message, ex.StackTrace);
                    }
                }
            }
        }
        #endregion

        #region Send Error Email
        private void SendErrorEmail(string Method, string Error, string StackTrace)
        {
            try
            {
                Properties.Settings.Default.isEnabled = false;

                string displayVer = string.Empty;
                if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                {
                    System.Deployment.Application.ApplicationDeployment currDeploy = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                    Version pubVer = currDeploy.CurrentVersion;
                    displayVer = pubVer.Major.ToString() + ":" + pubVer.Minor.ToString() + ":" + pubVer.Build.ToString() + ":" + pubVer.Revision.ToString();
                }

                string body = string.Empty;
                body = "<html>VerifySend has reported an error. The details of the exception are below:<br /><br /><table border=\"0\" cellspacing=\"2\" cellpadding=\"2\"><tr><td width=\"100\"><b>User:</b></td><td>" + System.Environment.UserName;
                body += "</td></tr><tr><td><b>Time:</b></td><td>" + DateTime.Now;
                body += "</td></tr><tr><td><b>Version:</b></td><td>" + displayVer;
                body += "</td></tr><tr><td><b>Method:</b></td><td>" + Method;
                body += "</td></tr><tr><td valign=\"top\"><b>Exception Message:</b></td><td valign=\"top\">" + Error;
                body += "</td></tr><tr><td valign=\"top\"><b>Stack Trace:</b></td><td valign=\"top\">" + StackTrace;
                body += "</td></tr></table></html>";

                Outlook.MailItem msg = (Outlook.MailItem)this.Application.CreateItem(Outlook.OlItemType.olMailItem);
                msg.Recipients.Add("jbf@solomononline.com");
                msg.Subject = "VerifySend Error Report";
                msg.HTMLBody = body;
                msg.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
                msg.DeleteAfterSubmit = true;
                ((Outlook._MailItem)msg).Send();
            }
            catch (Exception)
            {
            }
            finally
            {
                Properties.Settings.Default.isEnabled = true;
            }
        }
        #endregion

        #region Release Object
        /// <summary>
        /// Attempts to release COM objects
        /// </summary>
        /// <param name="obj"></param>
        private static void releaseObject(object obj)
        {
            try
            {
                while (Marshal.ReleaseComObject(obj) > 0) ;
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }
        #endregion

        #region Save Data Local
        /// <summary>
        /// Saves database table locally to .csv file for use when not connected to network
        /// </summary>
        private void saveDataLocal()
        {
            try
            {
                string query = @"SELECT COMPANYID, ALIAS, ALIASTYPE FROM GlobalDB.SAMail.Aliases";
                DataSet ds = new DataSet();

                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
                using (conn)
                {
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    da.Fill(ds);
                }

                System.Xml.XmlDocument verifySendXML = new System.Xml.XmlDocument();
                verifySendXML.LoadXml(ds.GetXml());

                XDocument doc = XDocument.Parse(verifySendXML.InnerXml);
                StringBuilder sb = new StringBuilder();

                sb.Append("COMPANYID,ALIAS,ALIASTYPE");
                sb.AppendLine();
                foreach (XElement node in doc.Descendants("Table"))
                {
                    foreach (XElement innerNode in node.Elements())
                    {
                        sb.AppendFormat("{0},", innerNode.Value);
                    }
                    sb.Remove(sb.Length - 1, 1);
                    sb.AppendLine();
                }

                System.IO.File.WriteAllText(System.Environment.CurrentDirectory + "\\verifySend.csv", sb.ToString());
                localDataDirectory = System.Environment.CurrentDirectory;
            }
            catch (Exception ex)
            {
                SendErrorEmail("saveDataLocal", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Dataset Local Data
        /// <summary>
        /// Returns a dataset from local data source (.csv).
        /// </summary>
        /// <param name="Aliases"></param>
        /// <returns></returns>
        private DataSet dsLocalData(string Aliases)
        {
            try
            {
                //string query1 = @"SELECT Alias FROM verifySend.csv WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL AND EXISTS (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + "))";
                //string query2 = @"SELECT DISTINCT CompanyID, Alias FROM verifySend.csv WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";
                //string query3 = @"SELECT Alias from verifySend.csv WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                //string query1 = @"SELECT Alias FROM verifySend.csv WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL ";
                //string query2 = @"SELECT DISTINCT CompanyID, Alias FROM verifySend.csv WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";
                //string query3 = @"SELECT Alias from verifySend.csv WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                string query1 = @"SELECT DISTINCT Alias FROM verifySend.csv WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL ";
                string query2 = @"SELECT DISTINCT CompanyID, Alias FROM verifySend.csv WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";
                string query3 = @"SELECT Alias from verifySend.csv WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM verifySend.csv WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";


                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet ds3 = new DataSet();
                string strCSVConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + localDataDirectory + "\\';Extended Properties='text;HDR=YES;'";

                System.Data.OleDb.OleDbDataAdapter oleda2 = new System.Data.OleDb.OleDbDataAdapter(query1, strCSVConnString);
                using (oleda2)
                {
                    oleda2.Fill(ds1);
                }

                System.Data.OleDb.OleDbDataAdapter oleda3 = new System.Data.OleDb.OleDbDataAdapter(query2, strCSVConnString);
                using (oleda3)
                {
                    oleda3.Fill(ds2);
                }

                System.Data.OleDb.OleDbDataAdapter oleda4 = new System.Data.OleDb.OleDbDataAdapter(query3, strCSVConnString);
                using (oleda4)
                {
                    oleda4.Fill(ds3);
                }

                DataTable table1 = new DataTable();
                table1 = ds2.Tables[0].Copy();
                table1.TableName = "Table1";

                DataTable table2 = new DataTable();
                table2 = ds3.Tables[0].Copy();
                table2.TableName = "Table2";

                ds1.Tables.Add(table1);
                ds1.Tables.Add(table2);

                return ds1;
            }
            catch (Exception ex)
            {
                SendErrorEmail("dsLocalData", ex.Message, ex.StackTrace);
                return null;
            }
        }
        #endregion

        #region Is Internet Connected
        /// <summary>
        /// Determines if the computer has any network connection at all; doesn't necessarily test for an internet connection.
        /// </summary>
        /// <returns></returns>
        public static bool IsInternetConnected()
        {
            int dwConnectionFlags = 0;
            if (!InternetGetConnectedState(out dwConnectionFlags, 0))
                return false;

            if (InternetAttemptConnect(0) != ERROR_SUCCESS)
                return false;

            return true;
        }
        #endregion

        #region Is SQL Connected
        /// <summary>
        /// Attempts to make a connection to sql
        /// </summary>
        /// <returns></returns>
        public static bool IsSqlConnected()
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
            using (conn)
            {
                try
                {
                    conn.Open();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region frmMismatchReport Form Closing Event
        void frmMismatchReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            e.Cancel = true;
        }
        #endregion

        #region Add-in Shutdown
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }
        #endregion

        #region Create Search Progress Form
        /// <summary>
        /// Creates a form with a progress bar that updates as the email/attachment is scanned.
        /// </summary>
        private void CreateSearchProgressForm()
        {
            frmSearchProgress.Width = 250;
            frmSearchProgress.Height = 80;
            System.Drawing.Size sz = new System.Drawing.Size(250, 80);
            //frmSearchProgress.MaximumSize = sz;
            frmSearchProgress.MinimumSize = sz;
            frmSearchProgress.Text = string.Empty;
            frmSearchProgress.ControlBox = false;
            frmSearchProgress.FormBorderStyle = FormBorderStyle.Sizable;
            frmSearchProgress.StartPosition = FormStartPosition.CenterScreen;
            frmSearchProgress.AutoSize = true;
            frmSearchProgress.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            picboxClose.Width = 17;
            picboxClose.Height = 16;
            picboxClose.Image = Properties.Resources.close;
            picboxClose.Top = picboxClose.Top + 4;
            picboxClose.Dock = DockStyle.Right;
            picboxClose.Click += new EventHandler(picboxClose_Click);
            frmSearchProgress.Controls.Add(picboxClose);

            //Button btntest = new Button();
            //btntest.Text = "testing";
            //btntest.Dock = DockStyle.Right;
            //btntest.Click += new EventHandler(btntest_Click);
            //frmSearchProgress.Controls.Add(btntest);

            pbScanStatus.Style = ProgressBarStyle.Blocks;
            //pbScanStatus.Style = ProgressBarStyle.Marquee;
            pbScanStatus.Maximum = 100;
            pbScanStatus.Minimum = 0;
            pbScanStatus.Value = 0;
            //pbScanStatus.Value = 50;
            pbScanStatus.Width = 200;
            pbScanStatus.Height = 10;
            pbScanStatus.Left = (frmSearchProgress.Width - pbScanStatus.Width) / 2 - 8;
            pbScanStatus.Top = (frmSearchProgress.Height - pbScanStatus.Height) / 2 + 10;
            frmSearchProgress.Controls.Add(pbScanStatus);

            //lblScanStatus.Text = "Scanning Attachment. Please wait...";
            lblScanStatus.Width = 191;
            lblScanStatus.Left = (frmSearchProgress.Width - lblScanStatus.Width) / 2 - 7;
            lblScanStatus.Top = (frmSearchProgress.Height - lblScanStatus.Height) / 2 - 10;
            lblScanStatus.AutoSize = true;
            frmSearchProgress.Controls.Add(lblScanStatus);

            //frmSearchProgress.Show();
        }

        void btntest_Click(object sender, EventArgs e)
        {
            backgroundWorker1x.CancelAsync();
            GlobalCancel = true;
        }

        void picboxClose_Click(object sender, EventArgs e)
        {
            backgroundWorker1x.CancelAsync();
            //_resetEvent.WaitOne();
            GlobalCancel = true;
        }
        #endregion

        public class _attachments : List<Outlook.Attachment>
        {
            Outlook.Attachment myatt { get; set; }
        }

        //private void verifyEmail3(object Item, ref bool Cancel)
        //{
        //    Cancel = true;
        //    object[] obs = new object[1];
        //    obs[0] = Item;
        //    System.ComponentModel.BackgroundWorker backgroundWorker2x = new System.ComponentModel.BackgroundWorker();
        //    backgroundWorker2x.WorkerSupportsCancellation = true;
        //    backgroundWorker2x.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorker2x_DoWork);
        //    backgroundWorker2x.RunWorkerAsync(obs);

        //}

        //void backgroundWorker2x_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        //{
        //    if (backgroundWorker1x.CancellationPending)
        //    {
        //        e.Cancel = true;
        //    }
        //    else
        //    {
        //        verifyEmail4(e);
        //    }
            
        //}

        //private void verifyEmail4(System.ComponentModel.DoWorkEventArgs e)
        //{
        //    object[] backgroundObs = (object[])e.Argument;
        //    Outlook.MailItem Item = (Outlook.MailItem)backgroundObs[0];
        //    dt.Clear();
        //    hasMismatch = false;
        //    hasAttachmentToExtrnal = false;
        //    if (Properties.Settings.Default.isEnabled && Item is Outlook.MailItem)
        //    {
        //        //Cancel = true;
        //        var msg = Item as Outlook.MailItem;
        //        string domain = string.Empty;
        //        List<string> lstDomains = new List<string>();
        //        string Aliases = string.Empty;
        //        foreach (Outlook.Recipient recip in msg.Recipients)
        //        {
        //            domain = getDomainStringFromAddress(recip.Address).ToLower();
        //            if (!lstDomains.Contains(domain) && domain != string.Empty)
        //            {
        //                lstDomains.Add(domain);
        //            }
        //        }
        //    }
        //}

        private void verifyEmail2(object Item, ref bool Cancel)
        {
            if (Properties.Settings.Default.isEnabled && Item is Outlook.MailItem)
            {
                if (!backgroundWorker1x.CancellationPending)
                {
                    Cancel = true;
                    object[] obs = new object[1];
                    obs[0] = Item;
                    backgroundWorker1x.WorkerSupportsCancellation = true;
                    backgroundWorker1x.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorker1x_DoWork);
                    backgroundWorker1x.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorker1x_RunWorkerCompleted);
                    backgroundWorker1x.RunWorkerAsync(obs);
                    showSearchProgressWindow();
                    hidefrmSearchProgress();
                }
            }
        }

        void backgroundWorker1x_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            string sdfsf = "sdf";
        }

        void backgroundWorker1x_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            if (backgroundWorker1x.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                verifyEmail(e, true);
            }
        }

        #region Validate Recipients
        /// <summary>
        /// Checks the recipient from list of valid recipients in Console Contact Tables
        /// 9/6/2013 - MGV</summary>
        private bool ValidRecipient(string domain, string recip)
        {
            bool result = false;
            
            if (IsSqlConnected())
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "GetContactEmails";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CompanyID", domain));
                SqlDataReader rdr = new SqlDataReader();
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    if (recip == rdr.GetString(2))
                        return true;
                }
                rdr.Close();
                conn.Close();
                conn = null;

            }
            
            return result;

        }


#endregion

        #region Verify Email
        /// <summary>
        /// 1. Gets all the recipients of the email (and warns if > 1).
        /// 2. Gets company names for all companies not in the recipient list from sql or local .csv data source if not connected to the network, and scans email/attachments for mismatches.
        /// 3. Shows various warnings if applicable.
        /// </summary>
        /// <param name="Item">Outlook.MailItem</param>
        /// <param name="Cancel">bool</param>
        //private void verifyEmail(object Item, ref bool Cancel, string body, Outlook.Recipients recips, string attachment, string sub, string bodyformat)
        //private void verifyEmail(object Item, ref bool Cancel)
        private void verifyEmail(System.ComponentModel.DoWorkEventArgs e, bool Cancel)
        {
            GlobalCancel = false;
            object[] backgroundObs = (object[])e.Argument;
            Outlook.MailItem Item = (Outlook.MailItem)backgroundObs[0];
            var msg = Item as Outlook.MailItem;
            dt.Clear();
            hasMismatch = false;
            hasAttachmentToExtrnal = false;
            if (Properties.Settings.Default.isEnabled && Item is Outlook.MailItem)
            {
                Cancel = true;
                //var msg = Item as Outlook.MailItem;
                //Outlook.MailItem msg = (Outlook.MailItem)this.Application.CreateItem(Outlook.OlItemType.olMailItem);
                //if (bodyformat == "2")
                //    msg.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
                //msg.Subject = sub;
                //msg.Body = body;
                //msg.Recipients.Add(recips[1].Name);
                //msg.Attachments.Add(System.IO.Path.GetTempPath() + attachment);
                string domain = string.Empty;
                List<string> lstDomains = new List<string>();
                List<string> lstRecips = new List<string>();
                string Aliases = string.Empty;
                
                foreach (Outlook.Recipient recip in msg.Recipients)
                {
                    domain = getDomainStringFromAddress(recip.Address).ToLower();
                    //domain = getDomainStringFromAddress(recip.Name).ToLower();
                    if (!lstDomains.Contains(domain) && domain != string.Empty)
                    {
                        lstDomains.Add(domain);
                    }

                    if (!ValidRecipient(domain, recip.Address))
                        lstRecips.Add(recip.Address);

                }

                //if (lstRecips.Count > 0)
                //{
                //    var result = MessageBox.Show(@"Warning: Message is addressed to an invalid recipient. Are you sure you want to continue?", "Warning!", MessageBoxButtons.OKCancel);
                //    if (result == DialogResult.Cancel)
                //    {
                //        Cancel = true;
                //        return;
                //    }
                //}

                if (lstDomains.Count > 0)
                {
                    if (lstDomains.Count > 1)
                    {
                        var result = MessageBox.Show(@"Warning: Message may be addressed to multiple companies. Are you sure you want to continue?", "Warning!", MessageBoxButtons.OKCancel);
                        if (result == DialogResult.Cancel)
                        {
                            Cancel = true;
                            return;
                        }
                        else
                        {
                            foreach (string alias in lstDomains)
                            {
                                Aliases += "'" + alias + "',";
                            }
                            Aliases = Aliases.Remove(Aliases.Length - 1);
                        }
                    }
                    else
                    {
                        Aliases = "'" + lstDomains[0] + "'";
                    }

                    //string query = @"SELECT Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL AND EXISTS (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + "))" +
                    //                "SELECT DISTINCT CompanyID, Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL " +
                    //                "SELECT Alias from GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                    //string query = @"SELECT Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL " +
                    //                "SELECT DISTINCT CompanyID, Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL " +
                    //                "SELECT Alias from GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                    string query = @"SELECT DISTINCT Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL " +
                                    "SELECT DISTINCT CompanyID, Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL " +
                                    "SELECT Alias from GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                    DataSet ds = new DataSet();

                    if (IsSqlConnected())
                    {
                        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
                        using (conn)
                        {
                            SqlDataAdapter da = new SqlDataAdapter(query, conn);
                            conn.Open();
                            da.Fill(ds);
                        }
                    }
                    else
                    {
                        ds = dsLocalData(Aliases);
                    }

                    updateScanStatusLabel("Scanning Email. Please wait...");
                    //CreateSearchProgressForm();
                    showSearchProgressWindow();
                    updateScanStatusLabel("Scanning Email. Please wait...");

                    //System.Threading.Tasks.Parallel.Invoke(() => { searchEmailText(ds, msg); });
                    searchEmailText(ds, msg);

                    //frmSearchProgress.Hide();
                    hidefrmSearchProgress();

                    DateTime ppstart = DateTime.Now;
                    if (msg.Attachments.Count > 0)
                    {
                        isAttachmentPasswordValid = false;
                        passwordGiveup = false;
                        attachmentPassword = "";
                        currentFile = "";
                        int q = 0;

                        //_attachments atts = new _attachments();
                        //foreach (Outlook.Attachment attx in msg.Attachments)
                        //{
                        //    atts.Add(attx);
                        //}
                        //System.Threading.Tasks.Parallel.ForEach(atts, currentFilex =>
                        //{
                        //    string abc = "sdfsdf";
                        //    string sdfsdf = "sdf";
                        //});

                        txtAttachmentPassword.Text = "";
                        foreach (Outlook.Attachment att in msg.Attachments)
                        {
                            q++;
                            //txtAttachmentPassword.Text = "";
                            if (att.FileName.ToLower().Contains(".xls") || att.FileName.ToLower().Contains(".xlsx") || att.FileName.ToLower().Contains(".doc") || att.FileName.ToLower().Contains(".docx") || att.FileName.ToLower().Contains(".zip") || att.FileName.ToLower().Contains(".ppt") || att.FileName.ToLower().Contains(".pptx") || att.FileName.ToLower().Contains(".pdf"))
                            {
                                att.SaveAsFile(System.IO.Path.GetTempPath() + att.FileName);
                                currentFile = att.FileName;
                                lblAttachmentPassword.Text = currentFile + " (leave blank if no password): ";
                                if (txtAttachmentPassword.Text == "")
                                    frmAttachmentPasswordForm.ShowDialog();
                                
                                //if (attachmentPassword == "" && !att.FileName.ToLower().Contains(".ppt") && !att.FileName.ToLower().Contains(".pptx"))
                                    //frmAttachmentPasswordForm.ShowDialog();
                                updateScanStatusLabel("Scanning Attachment " + q.ToString() + " of " + msg.Attachments.Count.ToString() + ". Please wait...");

                                //CreateSearchProgressForm();
                                showSearchProgressWindow();
                                string attType = string.Empty;
                                if (att.FileName.ToLower().Contains(".xls") || att.FileName.ToLower().Contains(".xlsx"))
                                    attType = "Excel";
                                if (att.FileName.ToLower().Contains(".doc") || att.FileName.ToLower().Contains(".docx"))
                                    attType = "Word";
                                if (att.FileName.ToLower().Contains(".ppt") || att.FileName.ToLower().Contains(".pptx"))
                                    attType = "PowerPoint";
                                if (att.FileName.ToLower().Contains(".pdf"))
                                    attType = "PDF";
                                //SearchAttachment(ds, att, attachmentPassword, attType);
                                if (attType == "Excel" || attType == "Word" || attType == "PowerPoint" || attType == "PDF")
                                {
                                    SearchAttachment(ds, att.FileName, attachmentPassword, attType);
                                    //System.Threading.Tasks.Parallel.Invoke(() => { SearchAttachment(ds, att.FileName, attachmentPassword, attType); });
                                    //System.Threading.Tasks.Task task = System.Threading.Tasks.Task.Factory.StartNew(() => { SearchAttachment(ds, att.FileName, attachmentPassword, attType); });
                                    
                                    //System.Threading.ThreadStart processTaskThread = delegate { SearchAttachment(ds, att.FileName, attachmentPassword, attType); };
                                    //new System.Threading.Thread(processTaskThread).Join();//.IsBackground = true;//.Start();
                                    //new System.Threading.Thread(processTaskThread).Start();

                                    

                                    
                                }
                                else
                                    extractZip(ds, att, attachmentPassword);
                                //frmSearchProgress.Hide();
                                hidefrmSearchProgress();

                                if (!isAttachmentPasswordValid)
                                {
                                    do
                                    {
                                        if (passwordGiveup)
                                        {
                                            backgroundWorker1x.CancelAsync();
                                            Cancel = true;
                                            return;
                                        }
                                        lblAttachmentPassword.Text = currentFile + " (leave blank if no password): ";
                                        frmAttachmentPasswordForm.ShowDialog();
                                        //CreateSearchProgressForm();
                                        showSearchProgressWindow();
                                        //SearchAttachment(ds, att, attachmentPassword, attType);
                                        if (attType == "Excel" || attType == "Word" || attType == "PowerPoint" || attType == "PDF")
                                            SearchAttachment(ds, att.FileName, attachmentPassword, attType);
                                        else
                                            extractZip(ds, att, attachmentPassword);
                                        //frmSearchProgress.Hide();
                                        hidefrmSearchProgress();
                                    }
                                    while (!isAttachmentPasswordValid || passwordGiveup);
                                }
                            }
                        }
                    }
                    TimeSpan totaltime = DateTime.Now - ppstart;

                    //frmSearchProgress.Hide();
                    hidefrmSearchProgress();

                    if (hasMismatch)
                    {
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            string Companies = string.Empty;
                            foreach (DataRow dr in ds.Tables[1].Rows)
                            {
                                Companies += dr[1] + ",";
                            }
                            Companies = Companies.Remove(Companies.Length - 1);
                            lblWarning.Text = "Warning: Message and/or attachment/file names may contain company names that do not match recipient company (" + Companies + "). Are you sure you want to continue?";
                            dgv.Top = lblWarning.Bottom;
                            btnMistmatchReportOK.Top = dgv.Bottom + 2;
                            btnMistmatchReportCancel.Top = dgv.Bottom + 2;
                        }
                        else
                        {
                            lblWarning.Text = "Warning: Message and/or attachment/file names may contain company names that do not match recipient company. Are you sure you want to continue?";
                            dgv.Top = lblWarning.Bottom;
                            btnMistmatchReportOK.Top = dgv.Bottom + 2;
                            btnMistmatchReportCancel.Top = dgv.Bottom + 2;
                        }
                        if (GlobalCancel == false)
                        {
                            showMismatchReportForm();
                            Cancel = myCancel;
                        }
                        

                        if (Cancel == false)
                        {
                            backgroundWorker1x.CancelAsync();
                            ((Outlook._MailItem)msg).Send();
                        }
                    }
                    else
                    {
                        Cancel = false;
                        if (hasAttachmentToExtrnal && !GlobalCancel)
                        {
                            var attachmentResult = MessageBox.Show(@"Warning: This message is being sent to an external email address and contains an attachment. Are you sure you want to continue?", "Warning!", MessageBoxButtons.OKCancel);
                            if (attachmentResult == DialogResult.Cancel)
                            {
                                Cancel = true;
                                backgroundWorker1x.CancelAsync();
                            }
                        }
                        if (Cancel == false && !GlobalCancel)
                        {
                            backgroundWorker1x.CancelAsync();
                            ((Outlook._MailItem)msg).Send();
                        }
                    }
                }
                else
                {
                    Cancel = false;
                    backgroundWorker1x.CancelAsync();
                    ((Outlook._MailItem)msg).Send();
                }
            }
            else
            {
                Cancel = false;
                backgroundWorker1x.CancelAsync();
                ((Outlook._MailItem)msg).Send();
            }
        }
        #endregion

        #region Create Attachment Password Form
        /// <summary>
        /// Creates form asking for attachment password.
        /// </summary>
        private void createAttachmentPasswordForm()
        {
            frmAttachmentPasswordForm.StartPosition = FormStartPosition.CenterParent;
            frmAttachmentPasswordForm.HorizontalScroll.Visible = false;            
            frmAttachmentPasswordForm.Width = 300;
            frmAttachmentPasswordForm.Text = "Password";
            frmAttachmentPasswordForm.FormBorderStyle = FormBorderStyle.Sizable;
            frmAttachmentPasswordForm.AutoSize = true;
            frmAttachmentPasswordForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            frmAttachmentPasswordForm.ShowIcon = false;
            frmAttachmentPasswordForm.MinimizeBox = false;
            frmAttachmentPasswordForm.MaximizeBox = false;
            frmAttachmentPasswordForm.FormClosing += new FormClosingEventHandler(frmAttachmentPasswordForm_FormClosing);

            Padding p1 = new Padding(3, 0, 0, 0);

            Label lblAttachmentPasswordHeader = new Label();
            lblAttachmentPasswordHeader.Text = "Enter password to open file";
            lblAttachmentPasswordHeader.Width = 300;
            lblAttachmentPasswordHeader.Height = 15;
            lblAttachmentPasswordHeader.Top = lblAttachmentPasswordHeader.Top + 7;
            lblAttachmentPasswordHeader.Padding = p1;
            frmAttachmentPasswordForm.Controls.Add(lblAttachmentPasswordHeader);

            lblAttachmentPassword.Width = 300;
            lblAttachmentPassword.Height = 30;
            lblAttachmentPassword.Top = lblAttachmentPasswordHeader.Bottom;
            lblAttachmentPassword.Padding = p1;
            frmAttachmentPasswordForm.Controls.Add(lblAttachmentPassword);

            txtAttachmentPassword.Width = 295;
            txtAttachmentPassword.Left = lblAttachmentPassword.Left + 5;
            txtAttachmentPassword.Text = "";
            txtAttachmentPassword.PasswordChar = '*';
            txtAttachmentPassword.Top = lblAttachmentPassword.Bottom;
            frmAttachmentPasswordForm.Controls.Add(txtAttachmentPassword);

            Panel pnl1 = new Panel();
            pnl1.Top = txtAttachmentPassword.Bottom + 10;
            pnl1.Width = 300;
            pnl1.Height = 23;
            frmAttachmentPasswordForm.Controls.Add(pnl1);

            Button btnSubmitPassword = new Button();
            btnSubmitPassword.Text = "OK";
            btnSubmitPassword.Click += new System.EventHandler(btnSubmitPassword_Click);
            btnSubmitPassword.Dock = DockStyle.Right;
            pnl1.Controls.Add(btnSubmitPassword);

            Button btnCancelPassword = new Button();
            btnCancelPassword.Text = "Cancel";
            btnCancelPassword.Dock = DockStyle.Right;
            btnCancelPassword.Top = btnSubmitPassword.Top;
            btnCancelPassword.Click += new EventHandler(btnCancelPassword_Click);
            pnl1.Controls.Add(btnCancelPassword);

            frmAttachmentPasswordForm.AcceptButton = btnSubmitPassword;
        }
        #endregion

        #region frmAttachmentPasswordForm_FormClosing Event
        void frmAttachmentPasswordForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                passwordGiveup = true;
                frmAttachmentPasswordForm.Hide();
                return;
            }
        }
        #endregion

        #region btnCancelPassword_Click Event
        private void btnCancelPassword_Click(object sender, EventArgs e)
        {
            passwordGiveup = true;
            frmAttachmentPasswordForm.Hide();
            return;
        }
        #endregion

        #region btnSubmitPassword_Click Event
        private void btnSubmitPassword_Click(object sender, EventArgs e)
        {
            attachmentPassword = frmAttachmentPasswordForm.Controls[2].Text;
            frmAttachmentPasswordForm.Hide();
        }
        #endregion
       
        #region Search Email Text
        /// <summary>
        /// This is the function that actually searches the email for mismatches.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="msg"></param>
        //private void searchEmailText(DataSet ds, Outlook.MailItem msg, string cellValues, Excel.Workbook wb)
        private void searchEmailText(DataSet ds, Outlook.MailItem msg)
        {
            //object[] obs = new object[2];
            //obs[0] = dsx;
            //obs[1] = msgx;
            //System.ComponentModel.BackgroundWorker backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            //backgroundWorker1.WorkerSupportsCancellation = true;
            //backgroundWorker1.DoWork += (o, e) =>
            //{
            //    if (backgroundWorker1.CancellationPending)
            //    {
            //        e.Cancel = true;
            //    }
            //    else
            //    {
            //        object[] backgroundObs = (object[])e.Argument;
            //        DataSet ds = (DataSet)backgroundObs[0];
            //        Outlook.MailItem msg = (Outlook.MailItem)backgroundObs[1];


            hidefrmSearchProgressCloseButton();

            updatePBValue(0);

            //search for DLM links
            DLMFiles.Clear();
            List<string> DLMKeys = new List<string>();
            if (IsSqlConnected())
            {
                try
                {
                    string strRegexFilename = @".*?(http://webservices\.solomononline\.com/dlm/Authenticate\.aspx\?key=).*?";
                    Regex rxFilename = new Regex(strRegexFilename, RegexOptions.IgnoreCase);
                    int keyCount = Regex.Matches(msg.Body, strRegexFilename, RegexOptions.IgnoreCase).Count;
                    if (keyCount > 0)
                    {
                        OdbcConnection conn = new OdbcConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DLMConn"].ToString());
                        using (conn)
                        {
                            conn.Open();
                            for (int q = 0; q < keyCount; q++)
                            {
                                int index = Regex.Matches(msg.Body, strRegexFilename, RegexOptions.IgnoreCase)[q].Index;
                                //DLMKeys.Add(msg.Body.Substring(index + 63, 32));
                                string query = "SELECT FilePath FROM FileKey WHERE RTRIM(FileKey) = '" + msg.Body.Substring(index + 63, 32) + "'";
                                OdbcCommand command = new OdbcCommand(query, conn);

                                OdbcDataReader reader = command.ExecuteReader();
                                while (reader.Read())
                                {
                                    DLMFiles.Add(reader[0].ToString().Replace("D:\\dlm_ftproot\\", ""));
                                }

                                //DLMFiles.Add((string)command.ExecuteScalar());

                                //SqlDataAdapter dax = new SqlDataAdapter(queryx, connx);
                                //conn.Open();
                                //DataSet dsFiles = new DataSet();
                                //dax.Fill(dsFiles);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SendErrorEmail("Search for DLM links", ex.Message, ex.StackTrace);
                }
            }

            if (msg.Attachments.Count > 0)
                hasAttachmentToExtrnal = true;

            int imagesCount = 0;
            foreach (Outlook.Attachment att in msg.Attachments)
            {
                if (att.FileName.Contains(".png") || att.FileName.Contains(".gif") || att.FileName.Contains(".jpg") || att.FileName.Contains(".jpeg") || att.FileName.Contains(".bmp"))
                    ++imagesCount;
            }
            if (imagesCount == msg.Attachments.Count)
                hasAttachmentToExtrnal = false;

            //pbScanStatus.Maximum = ds.Tables[0].Rows.Count;
            updatePBMaximum(ds.Tables[0].Rows.Count);

            for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
            {

                //pbScanStatus.Increment(1);
                incrementPB(1);

                strRegex = @"\b" + ds.Tables[0].Rows[x][0] + @"\b";

                Regex rx = new Regex(strRegex, RegexOptions.IgnoreCase);
                if (msg.Body != null)
                {
                    int count = Regex.Matches(msg.Body, strRegex, RegexOptions.IgnoreCase).Count;
                    if (count > 0)
                    {
                        for (int q = 0; q < count; q++)
                        {
                            hasMismatch = true;
                            int index = Regex.Matches(msg.Body, strRegex, RegexOptions.IgnoreCase)[q].Index;
                            try
                            {
                                context = msg.Body.Substring(index - 20, 40); ;
                            }
                            catch
                            {
                                context = msg.Body.Substring(index);
                            }
                            dt.Rows.Add(ds.Tables[0].Rows[x][0], "Body", context);
                        }
                    }
                }
                if (msg.Subject != null)
                {
                    int subcount = Regex.Matches(msg.Subject, strRegex, RegexOptions.IgnoreCase).Count;
                    if (subcount > 0)
                    {
                        for (int q = 0; q < subcount; q++)
                        {
                            hasMismatch = true;
                            int index = Regex.Matches(msg.Subject, strRegex, RegexOptions.IgnoreCase)[q].Index;
                            dt.Rows.Add(ds.Tables[0].Rows[x][0], "Subject", msg.Subject.Substring(index));
                        }
                    }
                }
                foreach (Outlook.Attachment att in msg.Attachments)
                {
                    int attcount = Regex.Matches(att.FileName, strRegex, RegexOptions.IgnoreCase).Count;
                    if (attcount > 0)
                    {
                        for (int q = 0; q < attcount; q++)
                        {
                            hasMismatch = true;
                            dt.Rows.Add(ds.Tables[0].Rows[x][0].ToString(), "Attachment Filename", att.FileName);
                        }
                    }
                    //if (att.FileName.ToLower().Contains(ds.Tables[0].Rows[x][0].ToString().ToLower()))
                    //{
                    //    hasMismatch = true;
                    //    dt.Rows.Add(ds.Tables[0].Rows[x][0].ToString(), "Attachment Filename", att.FileName);
                    //}
                    
                    if (ds.Tables[2].Rows.Count > 0 && x < ds.Tables[2].Rows.Count)
                    {
                        if (att.FileName.ToLower().Contains(ds.Tables[2].Rows[x][0].ToString().ToLower()))
                        {
                            hasAttachmentToExtrnal = false;
                        }
                    }
                }

                if (DLMFiles.Count > 0)
                {
                    foreach (string filename in DLMFiles)
                    {
                        int filecount = Regex.Matches(filename, strRegex, RegexOptions.IgnoreCase).Count;
                        if (filecount > 0)
                        {
                            for (int q = 0; q < filecount; q++)
                            {
                                hasMismatch = true;
                                dt.Rows.Add(ds.Tables[0].Rows[x][0].ToString(), "DLM Filename", filename);
                            }
                        }
                    }
                }


            }


                
            //};
            //backgroundWorker1.RunWorkerAsync(obs);

        }
        #endregion

        #region Generate Mismatch Report
        /// <summary>
        /// Adds a row to the mismatch grid/report.
        /// </summary>
        /// <param name="Mismatch"></param>
        /// <param name="Location"></param>
        /// <param name="Context"></param>
        /// <param name="Position"></param>
        private void generateMismatchReport(string Mismatch, string Location, string Context, string Position)
        {            
            dt.Rows.Add(Mismatch, Location, Context, Position);
        }
        #endregion

        #region Create Mismatch Data Grid View
        /// <summary>
        /// Creates the grid/report that shows any mismatches that were found.
        /// </summary>
        private void createMismatchDataGridView()
        {
            Panel pnlMisMatch = new Panel();
            pnlMisMatch.AutoSize = true;
            pnlMisMatch.AutoSizeMode = AutoSizeMode.GrowOnly;

            PictureBox pb = new PictureBox();
            pb.Width = 36;
            pb.Height = 33;
            pb.Image = Properties.Resources.warning;
            pb.Top = pb.Top + 4;
            frmMismatchReport.Controls.Add(pb);

            lblWarning.Left = pb.Right;
            lblWarning.Font = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, System.Drawing.SystemFonts.DefaultFont.Size + 2);
            Padding paddingGS = new Padding(0, 3, 0, 0);
            lblWarning.Padding = paddingGS;
            //System.Drawing.Size szWarning = new System.Drawing.Size(457, 0);
            System.Drawing.Size szWarning = new System.Drawing.Size(757, 0);
            lblWarning.MaximumSize = szWarning;
            //lblWarning.Width = 770;
            //lblWarning.Height = 50;
            lblWarning.AutoSize = true;
            lblWarning.BackColor = frmMismatchReport.BackColor;
            lblWarning.ForeColor = System.Drawing.Color.Red;
            frmMismatchReport.Controls.Add(lblWarning);

            dgv.ReadOnly = true;
            dgv.AllowUserToAddRows = false;
            dgv.RowHeadersVisible = false;
            dgv.BackgroundColor = frmMismatchReport.BackColor;
            dgv.BorderStyle = BorderStyle.FixedSingle;
            dgv.Top = lblWarning.Bottom;
            frmMismatchReport.Controls.Add(dgv);

            dgv.AutoGenerateColumns = false;
            dgv.DataSource = dt;

            DataGridViewTextBoxColumn dgvc1 = new DataGridViewTextBoxColumn();
            dgvc1.DataPropertyName = "Mismatch";
            dgvc1.HeaderText = "Mismatch";
            dgvc1.ReadOnly = true;
            dgvc1.FillWeight = 1;
            dgvc1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            DataGridViewTextBoxColumn dgvc2 = new DataGridViewTextBoxColumn();
            dgvc2.DataPropertyName = "Location";
            dgvc2.HeaderText = "Location";
            dgvc2.ReadOnly = true;
            dgvc2.FillWeight = 2;
            dgvc2.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            DataGridViewTextBoxColumn dgvc3 = new DataGridViewTextBoxColumn();
            dgvc3.DataPropertyName = "Context";
            dgvc3.HeaderText = "Context";
            dgvc3.ReadOnly = true;
            dgvc3.FillWeight = 3;
            dgvc3.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgvc3.MinimumWidth = 440;

            dgv.Columns.Add(dgvc1);
            dgv.Columns.Add(dgvc2);
            dgv.Columns.Add(dgvc3);

            dgv.Sort(dgv.Columns[1], System.ComponentModel.ListSortDirection.Descending);

            frmMismatchReport.Width = 801;

            btnMistmatchReportOK.AutoSize = true;
            btnMistmatchReportOK.Left = frmMismatchReport.Width / 2 - 86;
            btnMistmatchReportOK.Top = dgv.Bottom + 2;
            btnMistmatchReportOK.Text = "Send E-Mail";
            btnMistmatchReportOK.Font = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, System.Drawing.SystemFonts.DefaultFont.Size);
            btnMistmatchReportOK.Click += new System.EventHandler(btnMistmatchReportOK_Click);
            frmMismatchReport.Controls.Add(btnMistmatchReportOK);

            btnMistmatchReportCancel.Left = btnMistmatchReportOK.Right + 10;
            btnMistmatchReportCancel.Top = dgv.Bottom + 2;
            btnMistmatchReportCancel.Text = "Cancel";
            btnMistmatchReportCancel.Font = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, System.Drawing.SystemFonts.DefaultFont.Size);
            btnMistmatchReportCancel.Click += new System.EventHandler(btnMistmatchReportCancel_Click);
            frmMismatchReport.Controls.Add(btnMistmatchReportCancel);

            frmMismatchReport.StartPosition = FormStartPosition.CenterParent;

            frmMismatchReport.HorizontalScroll.Visible = false;

            dgv.ScrollBars = ScrollBars.Vertical;
            dgv.Width = 791;
            System.Drawing.Size sz = new System.Drawing.Size(791, 100);
            dgv.MinimumSize = sz;

            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            //dgv.AutoSize = true;

            
            frmMismatchReport.Text = "Warning!";
            frmMismatchReport.FormBorderStyle = FormBorderStyle.Sizable;

            //frmMismatchReport.AutoSize = true;
            //frmMismatchReport.AutoSizeMode = AutoSizeMode.GrowOnly;
        }
        #endregion

        #region Arrange Grid
        /// <summary>
        /// A way to adjust grid width based on columns. Not being used at the moment.
        /// </summary>
        /// <param name="Grid"></param>
        public static void ArrangeGrid(DataGridView Grid)
        {
            int twidth = 0;
            if (Grid.Rows.Count > 0)
            {
                twidth = (Grid.Width * Grid.Columns.Count) / 100;
                for (int i = 0; i < Grid.Columns.Count; i++)
                {
                    Grid.Columns[i].Width = twidth;
                }

            }
        }
        #endregion

        #region Get Domain String From Address
        /// <summary>
        /// Takes an email address and strips everything up to and including the @
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private static string getDomainStringFromAddress(string address)
        { 
            if (string.IsNullOrWhiteSpace(address) || !address.Contains("@"))
                return string.Empty;
            return address.Substring(address.IndexOf('@') + 1);
        }
        #endregion

        #region Get DB Connection
        /// <summary>
        /// Returns a database connection. Not being used at the moment.
        /// </summary>
        /// <returns></returns>
        private SqlConnection getDBConnection()
        {
            try
            {
                //SqlConnection QCConn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
                SqlConnection QCConn = new SqlConnection();
                var connBuilder = new SqlConnectionStringBuilder(System.Configuration.ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
                connBuilder.Password = "SolMailChecker";
                QCConn.ConnectionString = connBuilder.ConnectionString;
                
                return QCConn;
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
        #endregion

        #region Add Menu Bar
        /// <summary>
        /// Adds verifysend and submenu items to the ribbon
        /// </summary>
        private void AddMenuBar()
        {
            try
            {
                menuBar = this.Application.ActiveExplorer().CommandBars.ActiveMenuBar;
                newMenuBar = (Office.CommandBarPopup)menuBar.Controls.Add(Office.MsoControlType.msoControlPopup, missing, missing, missing, true);
                if (newMenuBar != null)
                {
                    string version = string.Empty;
                    if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
                        version = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
                    else
                        version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

                    newMenuBar.Caption = "VerifySend v" + version;
                    buttonOne = (Office.CommandBarButton)newMenuBar.Controls.Add(Office.MsoControlType.msoControlButton, missing, missing, 1, true);
                    buttonOne.Style = Office.MsoButtonStyle.msoButtonIconAndCaption;
                    buttonOne.Caption = "Settings";
                    buttonOne.FaceId = 65;
                    buttonOne.Tag = "c123";
                    buttonOne.Click += new Office._CommandBarButtonEvents_ClickEventHandler(buttonOne_Click);
                    newMenuBar.Visible = true;

                    btnSearchSelected = (Office.CommandBarButton)newMenuBar.Controls.Add(Office.MsoControlType.msoControlButton, missing, missing, 1, true);
                    btnSearchSelected.Style = Office.MsoButtonStyle.msoButtonCaption;
                    btnSearchSelected.Caption = "Run on selected emails";
                    btnSearchSelected.FaceId = 66;
                    btnSearchSelected.Tag = "c234";
                    btnSearchSelected.Click += new Office._CommandBarButtonEvents_ClickEventHandler(btnSearchSelected_Click);
                    btnSearchSelected.Visible = false;
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region btnSearchSelected_Click Event
        void btnSearchSelected_Click(Office.CommandBarButton Ctrl, ref bool CancelDefault)
        {
            searchSelected();
        }
        #endregion

        #region buttonOne_Click Event
        private void buttonOne_Click(Office.CommandBarButton ctrl, ref bool cancel)
        {
            cbEnabled.Checked = Properties.Settings.Default.isEnabled;
            frmVerifySendSettings.Show();
        }
        #endregion

        #region btnOptionsOK_Click Event
        private void btnOptionsOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.isEnabled = cbEnabled.Checked;
            Properties.Settings.Default.Save();
            frmVerifySendSettings.Hide();
        }
        #endregion

        #region btnOptions Cancel_Click Event
        private void btnOptionsCancel_Click(object sender, EventArgs e)
        {
            frmVerifySendSettings.Hide();
        }
        #endregion

        #region btnMismatchReportOk_Click Event
        private void btnMistmatchReportOK_Click(object sender, EventArgs e)
        {
            myCancel = false;
            hideMismatchReportForm();
        }
        #endregion

        #region btnMismatchReportCancel_Click Event
        private void btnMistmatchReportCancel_Click(object sender, EventArgs e)
        {
            myCancel = true;
            backgroundWorker1x.CancelAsync();
            hideMismatchReportForm();
        }
        #endregion

        #region Create Settings Form
        private void createSettingsForm()
        {
            cbEnabled.Text = "Enabled";
            cbEnabled.Font = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, System.Drawing.SystemFonts.DefaultFont.Size);

            if (System.Environment.UserName != "JBF" && System.Environment.UserName != "JDW")
                cbEnabled.Enabled = false;

            frmVerifySendSettings.Width = 340;
            frmVerifySendSettings.Height = 250;
            System.Drawing.Size sz = new System.Drawing.Size(340, 200);
            frmVerifySendSettings.MaximumSize = sz;
            frmVerifySendSettings.MinimumSize = sz;
            frmVerifySendSettings.VerticalScroll.Visible = false;
            frmVerifySendSettings.Text = "VerifySend Settings";

            Panel pnlSettings = new Panel();
            pnlSettings.BackColor = System.Drawing.Color.White;
            pnlSettings.BorderStyle = BorderStyle.FixedSingle;
            pnlSettings.Width = 310;
            pnlSettings.Height = 120;
            pnlSettings.Left = (frmVerifySendSettings.Width - pnlSettings.Width) / 2 - 8;
            pnlSettings.Top = 7;

            Label lblGeneralSettings = new Label();
            lblGeneralSettings.Font = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, System.Drawing.SystemFonts.DefaultFont.Size + 1);
            Padding paddingGS = new Padding(0, 3, 0, 0);
            lblGeneralSettings.Padding = paddingGS;
            lblGeneralSettings.Top = 7;
            lblGeneralSettings.Text = "General Settings";
            lblGeneralSettings.Width = 295;
            lblGeneralSettings.BackColor = System.Drawing.Color.GhostWhite;
            lblGeneralSettings.ForeColor = System.Drawing.Color.Black;
            lblGeneralSettings.Left = (pnlSettings.Width - lblGeneralSettings.Width) / 2;

            pnlSettings.Controls.Add(lblGeneralSettings);
            pnlSettings.Anchor = AnchorStyles.None;

            cbEnabled.Top = lblGeneralSettings.Top + 25;
            cbEnabled.Left = lblGeneralSettings.Left;

            pnlSettings.Controls.Add(cbEnabled);
            frmVerifySendSettings.Controls.Add(pnlSettings);

            Button btnOptionsOK = new Button();
            btnOptionsOK.Text = "OK";
            btnOptionsOK.Top = pnlSettings.Bottom + 5;
            btnOptionsOK.Left = pnlSettings.Right - 160;
            btnOptionsOK.Font = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, System.Drawing.SystemFonts.DefaultFont.Size);
            btnOptionsOK.Click += new System.EventHandler(btnOptionsOK_Click);

            Button btnOptionsCancel = new Button();
            btnOptionsCancel.Text = "Cancel";
            btnOptionsCancel.Top = pnlSettings.Bottom + 5;
            btnOptionsCancel.Left = btnOptionsOK.Left + 85;
            btnOptionsCancel.Font = new System.Drawing.Font(System.Drawing.SystemFonts.DefaultFont.FontFamily, System.Drawing.SystemFonts.DefaultFont.Size);
            btnOptionsCancel.Click += new System.EventHandler(btnOptionsCancel_Click);

            frmVerifySendSettings.Controls.Add(btnOptionsOK);
            frmVerifySendSettings.Controls.Add(btnOptionsCancel);

            frmVerifySendSettings.StartPosition = FormStartPosition.CenterParent;
        }
        #endregion

        #region Search Selected
        /// <summary>
        /// Loop through selected emails to search. There isn't really a need for anybody to use this,
        /// but it's something Dave wanted for testing. I may remove/hide it in a future version
        /// </summary>
        private void searchSelected()
        {
            createProgressForm();            
            frmProgressWindow.Show();

            System.ComponentModel.BackgroundWorker backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            backgroundWorker1.WorkerSupportsCancellation = true;

            backgroundWorker1.DoWork += (o, e) =>
            {
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    int x = 0;
                    dt.Clear();
                    string subs = string.Empty;
                    string report = string.Empty;

                    foreach (object obj in Globals.ThisAddIn.Application.ActiveExplorer().Selection)
                    {
                        if (obj is Outlook.MailItem)
                        {
                            hasAttachmentToExtrnal = false;
                            x++;
                            updateProgressLabel(x, Globals.ThisAddIn.Application.ActiveExplorer().Selection.Count);
                            Outlook.MailItem msg = (obj as Outlook.MailItem);

                            dt.Clear();
                            searchSelectedEmail(msg);
                            if (dt.Rows.Count > 0)
                            {
                                hidefrmProgressWindow();
                                showMismatchReportForm();
                                if (!hasAttachmentToExtrnal)
                                    showProgressWindow();
                            }
                            if (hasAttachmentToExtrnal)
                            {
                                MessageBox.Show("Message sent to an external email address and contains an attachment.");
                            }
                        }
                    }

                    hidefrmProgressWindow();
                }
            };
            backgroundWorker1.RunWorkerAsync();
        }
        #endregion

        #region Create Progress Form
        private void createProgressForm()
        {
            frmProgressWindow.Width = 250;
            frmProgressWindow.Height = 80;
            System.Drawing.Size sz = new System.Drawing.Size(250, 80);
            frmProgressWindow.MaximumSize = sz;
            frmProgressWindow.MinimumSize = sz;
            ProgressBar pb = new ProgressBar();
            pb.Style = ProgressBarStyle.Marquee;
            pb.Maximum = 100;
            pb.Minimum = 0;
            pb.Value = 50;
            pb.Width = 200;
            pb.Height = 10;
            pb.Left = (frmProgressWindow.Width - pb.Width) / 2 - 8;
            pb.Top = (frmProgressWindow.Height - pb.Height) / 2 + 10;
            frmProgressWindow.Controls.Add(pb);
            frmProgressWindow.Text = string.Empty;
            frmProgressWindow.ControlBox = false;
            frmProgressWindow.FormBorderStyle = FormBorderStyle.Sizable;
            frmProgressWindow.StartPosition = FormStartPosition.CenterScreen;
            lblStatus.Width = 191;
            lblStatus.Left = (frmProgressWindow.Width - lblStatus.Width) / 2 - 7;
            lblStatus.Top = (frmProgressWindow.Height - lblStatus.Height) / 2 - 10;
            frmProgressWindow.Controls.Add(lblStatus);
        }
        #endregion

        #region Search Selected Email
        /// <summary>
        /// Search selected emails for mismatches
        /// </summary>
        /// <param name="msg"></param>
        private void searchSelectedEmail(Outlook.MailItem msg)
        {
            string report = string.Empty;
            string domain = string.Empty;
            List<string> lstDomains = new List<string>();
            string Aliases = string.Empty;

            foreach (Outlook.Recipient recip in msg.Recipients)
            {
                domain = getDomainStringFromAddress(recip.Address).ToLower();
                if (!lstDomains.Contains(domain) && domain != string.Empty)
                {
                    lstDomains.Add(domain);
                }
            }

            if (lstDomains.Count > 0)
            {
                if (lstDomains.Count > 1)
                {
                    foreach (string alias in lstDomains)
                    {
                        Aliases += "'" + alias + "',";
                    }
                    Aliases = Aliases.Remove(Aliases.Length - 1);
                }
                else
                {
                    Aliases = "'" + lstDomains[0] + "'";
                }

                //string query = @"SELECT Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL AND EXISTS (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + "))" +
                //                "SELECT DISTINCT CompanyID, Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL " +
                //                "SELECT Alias from GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                //string query = @"SELECT Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL " +
                //                "SELECT DISTINCT CompanyID, Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL " +
                //                "SELECT Alias from GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                string query = @"SELECT DISTINCT Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID NOT IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType IS NOT NULL " +
                                "SELECT DISTINCT CompanyID, Alias FROM GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL " +
                                "SELECT Alias from GlobalDB.SAMail.Aliases WHERE CompanyID IN (SELECT DISTINCT CompanyID FROM GlobalDB.SAMail.Aliases WHERE Alias IN (" + Aliases + ")) AND AliasType = 5 AND AliasType IS NOT NULL";

                DataSet ds = new DataSet();
                
                if (IsSqlConnected())
                {
                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
                    using (conn)
                    {
                        SqlDataAdapter da = new SqlDataAdapter(query, conn);
                        conn.Open();
                        da.Fill(ds);
                    }
                }
                else
                {
                    ds = dsLocalData(Aliases);
                }

                if (msg.Attachments.Count > 0)
                    hasAttachmentToExtrnal = true;
                
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    generateSelectedReport(ds.Tables[0].Rows[x], msg);

                    foreach (Outlook.Attachment att in msg.Attachments)
                    {
                        if (ds.Tables[2].Rows.Count > 0 && x < ds.Tables[2].Rows.Count)
                        {
                            if (att.FileName.ToLower().Contains(ds.Tables[2].Rows[x][0].ToString().ToLower()))
                            {
                                hasAttachmentToExtrnal = false;
                            }
                            else
                            {
                                hasAttachmentToExtrnal = true;
                            }
                        }
                    }
                }

                //if (msg.Attachments.Count > 0)
                //{
                //    foreach (Outlook.Attachment att in msg.Attachments)
                //    {
                //        if (att.FileName.Contains(".xls") || att.FileName.Contains(".xlsx"))
                //        {
                //            SearchExcel(ds, att, "");
                //        }
                //    }
                //}

                int imagesCount = 0;
                foreach (Outlook.Attachment att in msg.Attachments)
                {
                    if (att.FileName.Contains(".png") || att.FileName.Contains(".gif") || att.FileName.Contains(".jpg") || att.FileName.Contains(".jpeg") || att.FileName.Contains(".bmp"))
                        ++imagesCount;
                }
                if (imagesCount == msg.Attachments.Count)
                    hasAttachmentToExtrnal = false;

                if (ds.Tables[1].Rows.Count > 0)
                {
                    string Companies = string.Empty;
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        Companies += dr[1] + ",";
                    }
                    Companies = Companies.Remove(Companies.Length - 1);
                    updateWarningLabel("Warning: Message and/or attachment/file names may contain company names that do not match recipient company (" + Companies + ").");
                    dgv.Top = lblWarning.Bottom;
                    btnMistmatchReportOK.Top = dgv.Bottom + 2;
                    btnMistmatchReportCancel.Top = dgv.Bottom + 2;
                }
                
            }
        }
        #endregion

        #region Generate Selected Mismatch Report
        /// <summary>
        /// Create report showing mismatches on selected emails
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="msg"></param>
        private void generateSelectedReport(DataRow dr, Outlook.MailItem msg)
        {
            strRegex = @"\b" + dr[0] + @"\b";

            Regex rx = new Regex(strRegex, RegexOptions.IgnoreCase);
            int count = Regex.Matches(msg.Body, strRegex, RegexOptions.IgnoreCase).Count;
            if (count > 0)
            {
                for (int q = 0; q < count; q++)
                {
                    hasMismatch = true;
                    int index = Regex.Matches(msg.Body, strRegex, RegexOptions.IgnoreCase)[q].Index;
                    
                    try
                    {
                        context = msg.Body.Substring(index - 20, 40); ;
                    }
                    catch
                    {
                        context = msg.Body.Substring(index);
                    }
                    dt.Rows.Add(dr[0], "Body", context);
                }
            }
            int subcount = Regex.Matches(msg.Subject, strRegex, RegexOptions.IgnoreCase).Count;
            if (subcount > 0)
            {
                for (int q = 0; q < subcount; q++)
                {
                    hasMismatch = true;
                    int index = Regex.Matches(msg.Subject, strRegex, RegexOptions.IgnoreCase)[q].Index;
                    
                    dt.Rows.Add(dr, "Subject", msg.Subject.Substring(index));
                }
            }

            foreach (Outlook.Attachment att in msg.Attachments)
            {
                if (att.FileName.ToLower().Contains(dr[0].ToString().ToLower()))
                {
                    hasMismatch = true;
                    dt.Rows.Add(dr, "Attachment", att.FileName);
                }
            }
        }
        #endregion

        #region Update Progress Label
        private void updateProgressLabel(int counter, int total)
        {
            this.updateProgressLabelThread(counter, total);
        }
        #endregion

        #region Update progress Label Thread
        private void updateProgressLabelThread(int counter, int total)
        {
            if (this.lblStatus.InvokeRequired)
            {
                lblStatus.BeginInvoke(new MethodInvoker(delegate() { updateProgressLabelThread(counter, total); }));
                return;
            }
            else
            {
                this.lblStatus.Text = "Processing email " + counter.ToString() + " of " + total.ToString() + ". Please wait.";
            }
        }
        #endregion

        #region Update Warning Label
        private void updateWarningLabel(string text)
        {
            this.updateWarningLabelThread(text);
        }
        #endregion

        #region Update Warning Label Thread
        private void updateWarningLabelThread(string text)
        {
            if (this.lblWarning.InvokeRequired)
            {
                lblWarning.BeginInvoke(new MethodInvoker(delegate() { updateWarningLabelThread(text); }));
                return;
            }
            else
            {
                this.lblWarning.Text = text;
            }
        }
        #endregion

        #region Update Scan Status Label
        private void updateScanStatusLabel(string text)
        {
            this.updateScanStatusLabelThread(text);
        }
        #endregion

        #region Update Scan Status Label Thread
        private void updateScanStatusLabelThread(string text)
        {
            if (this.lblScanStatus.InvokeRequired)
            {
                lblScanStatus.BeginInvoke(new MethodInvoker(delegate() { updateScanStatusLabelThread(text); }));
                return;
            }
            else
            {
                this.lblScanStatus.Text = text;
            }
        }
        #endregion

        #region Hide Progress Window
        private void hidefrmProgressWindow()
        {
            this.hidefrmProgressWindowThread();
        }
        #endregion

        #region Hide Progress Window Thread
        private void hidefrmProgressWindowThread()
        {
            if (frmProgressWindow.InvokeRequired)
            {
                frmProgressWindow.BeginInvoke(new MethodInvoker(hidefrmProgressWindowThread));
                return;
            }
            else
            {
                this.frmProgressWindow.Hide();
            }
        }
        #endregion

        #region Show Progress Window
        private void showProgressWindow()
        {
            this.showProgressWindowThread();
        }
        #endregion

        #region Show Progress Window Thread
        private void showProgressWindowThread()
        {
            if (this.frmProgressWindow.InvokeRequired)
            {
                frmProgressWindow.BeginInvoke(new MethodInvoker(showProgressWindowThread));
                return;
            }
            else
            {
                if (frmProgressWindow.Visible == false)
                    this.frmProgressWindow.Show();
            }
        }
        #endregion

        #region Show Mismatch Report Form
        private void showMismatchReportForm()
        {
            this.showMismatchReportFormThread();
        }
        #endregion

        #region Show Mismatch Report Form Thread
        private void showMismatchReportFormThread()
        {
            if (this.frmMismatchReport.InvokeRequired)
            {
                frmMismatchReport.BeginInvoke(new MethodInvoker(showMismatchReportFormThread));
                return;
            }
            else
            {
                if (frmMismatchReport.Visible == false)
                    this.frmMismatchReport.ShowDialog();
            }
        }
        #endregion

        #region Hide Mismatch Report Form
        private void hideMismatchReportForm()
        {
            this.hideMismatchReportFormThread();
        }
        #endregion

        #region Hide Mismatch Report Form Thread
        private void hideMismatchReportFormThread()
        {
            if (frmMismatchReport.InvokeRequired)
            {
                frmMismatchReport.BeginInvoke(new MethodInvoker(hideMismatchReportFormThread));
                return;
            }
            else
            {
                this.frmMismatchReport.Hide();
            }
        }
        #endregion




        #region Update Progress Bar Maximum
        private void updatePBMaximum(int max)
        {
            this.updatePBMaximumThread(max);
        }
        #endregion

        #region Update Progress Bar Maximum Thread
        private void updatePBMaximumThread(int max)
        {
            if (pbScanStatus.InvokeRequired)
            {
                pbScanStatus.BeginInvoke(new MethodInvoker(delegate() { updatePBMaximumThread(max); }));
                return;
            }
            else
            {
                this.pbScanStatus.Maximum = max;
            }
        }
        #endregion

        #region Increment Progress Bar
        private void incrementPB(int increment)
        {
            this.incrementPBThread(increment);
        }
        #endregion

        #region Increment Progress Bar Thread
        private void incrementPBThread(int increment)
        {
            if (pbScanStatus.InvokeRequired)
            {
                pbScanStatus.BeginInvoke(new MethodInvoker(delegate() { incrementPBThread(increment); }));
                return;
            }
            else
            {
                this.pbScanStatus.Increment(increment);
            }
        }
        #endregion




        #region Update Progress Bar Value
        private void updatePBValue(int value)
        {
            this.updatePBValueThread(value);
        }
        #endregion

        #region Update Progress Bar Value Thread
        private void updatePBValueThread(int value)
        {
            if (pbScanStatus.InvokeRequired)
            {
                pbScanStatus.BeginInvoke(new MethodInvoker(delegate() { updatePBValueThread(value); }));
                return;
            }
            else
            {
                this.pbScanStatus.Value = value;
            }
        }
        #endregion

        #region Show Search Progress Window
        private void showSearchProgressWindow()
        {
            this.showSearchProgressWindowThread();
        }
        #endregion

        #region Show Search Progress Window Thread
        private void showSearchProgressWindowThread()
        {
            if (this.frmSearchProgress.InvokeRequired)
            {
                frmSearchProgress.BeginInvoke(new MethodInvoker(showSearchProgressWindowThread));
                return;
            }
            else
            {
                if (frmSearchProgress.Visible == false)
                {
                    this.frmSearchProgress.Show();
                    //this.frmSearchProgress.Update();
                }
            }
        }
        #endregion

        #region Hide Search Progress Window
        private void hidefrmSearchProgress()
        {
            this.hidefrmSearchProgressWindowThread();
        }
        #endregion

        #region Hide Search Progress Window Thread
        private void hidefrmSearchProgressWindowThread()
        {
            if (frmSearchProgress.InvokeRequired)
            {
                frmSearchProgress.BeginInvoke(new MethodInvoker(hidefrmSearchProgressWindowThread));
                return;
            }
            else
            {
                this.frmSearchProgress.Hide();
            }
        }
        #endregion

        

        #region Hide Search Progress Close Button
        private void hidefrmSearchProgressCloseButton()
        {
            this.hidefrmSearchProgressCloseButtonThread();
        }
        #endregion

        #region Hide Search Progress Close Button Thread
        private void hidefrmSearchProgressCloseButtonThread()
        {
            if (picboxClose.InvokeRequired)
            {
                picboxClose.BeginInvoke(new MethodInvoker(hidefrmSearchProgressCloseButtonThread));
                return;
            }
            else
            {
                this.picboxClose.Hide();
            }
        }
        #endregion

        #region Show Search Progress Close Button
        private void showSearchProgressCloseButton()
        {
            this.showSearchProgressCloseButtonThread();
        }
        #endregion

        #region Show Search Progress Close Button Thread
        private void showSearchProgressCloseButtonThread()
        {
            if (this.picboxClose.InvokeRequired)
            {
                picboxClose.BeginInvoke(new MethodInvoker(showSearchProgressCloseButtonThread));
                return;
            }
            else
            {
                if (picboxClose.Visible == false)
                {
                    this.picboxClose.Show();
                }
            }
        }
        #endregion

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
