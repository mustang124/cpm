﻿CREATE FUNCTION [dbo].[RoundNumber](@value float) RETURNS decimal(19,5)
AS
BEGIN
DECLARE @result decimal(19,5)
IF @value IS NULL
	SELECT @result = NULL
ELSE IF @value = 0
	SELECT @result = 0
ELSE BEGIN
	SELECT @result = ROUND(@value, 1 - ROUND(LOG10(ABS(@value)), 0))
END
RETURN @result

END
GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[RoundNumber] TO PUBLIC
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[RoundNumber] TO PUBLIC
    AS [dbo];

