﻿
CREATE FUNCTION [dbo].[InflationFactor](@CurrCode char(4), @FromYear smallint, @ToYear smallint)
RETURNS real
AS
BEGIN
	RETURN dbo.InflationIndex(@CurrCode, @ToYear, 1)/dbo.InflationIndex(@CurrCode, @FromYear, 1)
END
GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[InflationFactor] TO PUBLIC
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[InflationFactor] TO PUBLIC
    AS [dbo];

