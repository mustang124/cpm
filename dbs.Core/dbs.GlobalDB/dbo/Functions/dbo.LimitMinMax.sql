﻿
CREATE FUNCTION [dbo].[LimitMinMax](@Value float, @Min float, @Max float)
RETURNS float
AS
BEGIN
	DECLARE @ResultVar float
	IF @Value < @Min
		SET @ResultVar = @Min
	ELSE IF @Value > @Max
		SET @ResultVar = @Max
	ELSE
		SET @ResultVar = @Value

	RETURN @ResultVar

END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LimitMinMax] TO PUBLIC
    AS [dbo];

