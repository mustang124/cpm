﻿

create function [dbo].[HoursInYear](@Yr smallint)
RETURNS smallint
BEGIN
	DECLARE @hrs smallint
	SELECT @hrs = dbo.DaysInYear(@Yr)*24
	RETURN @hrs
END
GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[HoursInYear] TO PUBLIC
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[HoursInYear] TO PUBLIC
    AS [dbo];

