﻿

create function [dbo].[IsLeapYear](@Yr smallint)
RETURNS bit
BEGIN
DECLARE @result bit
	SELECT @result = 0
	IF ((@Yr % 4) = 0 AND (@Yr % 100) <> 0) OR (@Yr % 400) = 0
		SELECT @result = 1
	RETURN @result
END
GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[IsLeapYear] TO PUBLIC
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[IsLeapYear] TO PUBLIC
    AS [dbo];

