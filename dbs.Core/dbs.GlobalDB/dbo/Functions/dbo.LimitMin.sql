﻿
CREATE FUNCTION [dbo].[LimitMin](@Value float, @Min float)
RETURNS float
AS
BEGIN
	DECLARE @ResultVar float
	IF @Value < @Min
		SET @ResultVar = @Min
	ELSE
		SET @ResultVar = @Value

	RETURN @ResultVar

END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LimitMin] TO PUBLIC
    AS [dbo];

