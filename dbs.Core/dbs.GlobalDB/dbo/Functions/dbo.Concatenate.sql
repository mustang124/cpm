﻿CREATE AGGREGATE [dbo].[Concatenate](@valueA NVARCHAR (200))
    RETURNS NVARCHAR (MAX)
    EXTERNAL NAME [CustomAggregates].[CustomAggregates.Concatenate];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[Concatenate] TO PUBLIC
    AS [dbo];

