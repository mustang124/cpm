﻿

CREATE  function dbo.GetUnitsText(@DisplayUnits varchar(255))
RETURNS varchar(255)
BEGIN
	DECLARE @DivLoc int, @i int, @Result varchar(255)
	SET @Result = @DisplayUnits
	IF CHARINDEX('/', @DisplayUnits) > 0 
	BEGIN
		SELECT @DivLoc = CHARINDEX('/', @DisplayUnits)
		SELECT @Result = dbo.GetUnitsText(SUBSTRING(@DisplayUnits, 1, @DivLoc-1)) + '/' +
                          dbo.GetUnitsText(SUBSTRING(@DisplayUnits, @DivLoc+1, LEN(@DisplayUnits)))
	END
	ELSE IF dbo.CaseSensitiveSearch(@DisplayUnits, 'p', 1) > 0 
	BEGIN
		SELECT @DivLoc = dbo.CaseSensitiveSearch(@DisplayUnits, 'p', 1)
		SELECT @Result = dbo.GetUnitsText(SUBSTRING(@DisplayUnits, 1, @DivLoc-1)) + '/' +
			  dbo.GetUnitsText(SUBSTRING(@DisplayUnits, @DivLoc+1, LEN(@DisplayUnits)))
	END
	ELSE IF CHARINDEX('*', @DisplayUnits) > 0 
	BEGIN
		SELECT @DivLoc = CHARINDEX('*', @DisplayUnits);
		SELECT @Result = dbo.GetUnitsText(SUBSTRING(@DisplayUnits, 1, @DivLoc-1)) + '-' +
			  dbo.GetUnitsText(SUBSTRING(@DisplayUnits, @DivLoc+1, LEN(@DisplayUnits)))
	END
	ELSE
		SELECT @Result = UOMDesc FROM UOM WHERE UOMCode = @DisplayUnits

	RETURN @Result
END
GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[GetUnitsText] TO PUBLIC
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[GetUnitsText] TO PUBLIC
    AS [dbo];

