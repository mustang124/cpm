﻿
CREATE FUNCTION [dbo].[InflationIndex](@CurrCode char(4), @Year smallint, @GetClosest bit) 
RETURNS real
AS
BEGIN
	DECLARE @Value real, @FirstYear smallint, @LastYear smallint
	SELECT @Value = InflIndex FROM dbo.Inflation WHERE CurrencyCode = @CurrCode AND Year = @Year
	IF @Value IS NULL AND @GetClosest = 1
	BEGIN
		IF EXISTS (SELECT * FROM dbo.Inflation WHERE CurrencyCode=@CurrCode)
		BEGIN
			IF EXISTS (SELECT * FROM dbo.Inflation WHERE CurrencyCode=@CurrCode AND Year < @Year)
			 AND EXISTS (SELECT * FROM dbo.Inflation WHERE CurrencyCode=@CurrCode AND Year > @Year)
				SELECT @Value = NULL
			ELSE BEGIN
				IF NOT EXISTS (SELECT * FROM dbo.Inflation WHERE CurrencyCode=@CurrCode AND Year < @Year)
					SELECT @Value = InflIndex FROM dbo.Inflation 
					WHERE CurrencyCode=@CurrCode AND Year = (SELECT MIN(Year) FROM dbo.Inflation WHERE CurrencyCode=@CurrCode)
				ELSE
					SELECT @Value = InflIndex FROM dbo.Inflation 
					WHERE CurrencyCode=@CurrCode AND Year = (SELECT MAX(Year) FROM dbo.Inflation WHERE CurrencyCode=@CurrCode)
			END
		END
		ELSE BEGIN
			SELECT @Value = NULL
		END
	END
	RETURN @Value
END
GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[InflationIndex] TO PUBLIC
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[InflationIndex] TO PUBLIC
    AS [dbo];

