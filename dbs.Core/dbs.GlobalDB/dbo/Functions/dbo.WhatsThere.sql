﻿CREATE AGGREGATE [dbo].[WhatsThere](@valueA NVARCHAR (200))
    RETURNS NVARCHAR (MAX)
    EXTERNAL NAME [CustomAggregates].[CustomAggregates.WhatsThere];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[WhatsThere] TO PUBLIC
    AS [dbo];

