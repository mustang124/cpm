﻿CREATE TABLE [dbo].[UOMGroups] (
    [UOMGroup] VARCHAR (20) NOT NULL,
    [BaseUOM]  VARCHAR (20) NULL
);


GO
GRANT SELECT
    ON OBJECT::[dbo].[UOMGroups] TO PUBLIC
    AS [dbo];

