﻿CREATE FUNCTION [dbo].[GetProfileLiteKPIs](@RefineryID char(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5))
RETURNS @averages TABLE (
	RefUtilPcnt real NULL, 
	UtilOSTA real NULL, 
	Edc real NULL, 
	UtilUEdc real NULL,
	ProcessUtilPcnt real NULL, 
	TotProcessEdc real NULL, 
	TotProcessUEdc real NULL,
	MechAvail real NULL, 
	OpAvail real NULL, 
	MechUnavailRout real NULL, 
	MechUnavailTA real NULL, 
	RegUnavail real NULL, 
	EII real NULL, 
	EnergyUseDay real NULL, 
	TotStdEnergy real NULL,
	VEI real NULL, 
	ReportLossGain real NULL, 
	EstGain real NULL, 
	ProcessEffIndex real,
	GainPcnt real NULL, 
	RawMatlKBpD real NULL, 
	ProdYieldKBpD real NULL, 
	NetInputBPD real NULL,
	TotMaintForceWHrEdc real NULL, 
	MaintTAWHr real NULL, 
	MaintNonTAWHr real NULL,
	PersIndex real NULL, 
	NonMaintWHr real NULL, 
	TotMaintForceWHr real NULL, 
	PersEffIndex real NULL,
	PersEffDiv real NULL,
	NMPersEffIndex real NULL,
	NMPersEffDiv real NULL,
	MaintIndex real NULL,
	MaintEffIndex real NULL,
	AnnTACost real NULL, 
	RoutCost real NULL,
	MaintEffDiv real NULL,
	OpExUEdc real NULL, 
	NEOpExEdc real NULL,	
	NEOpEx real NULL, 
	OpExEdc real NULL, 
	EnergyCost real NULL, 
	TotCashOpEx real NULL,
	UEdc real NULL
	)
AS BEGIN

IF NOT EXISTS (SELECT * FROM dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodEnd = @EndDate AND UseSubmission = 1)
	SELECT @EndDate = MAX(PeriodEnd) FROM dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND PeriodEnd <= @EndDate AND UseSubmission = 1
	
DECLARE @EII real, @VEI real, @RefUtilPcnt real, @UtilOSTA real, @Edc real, @UEdc real, @ProcessUtilPcnt real, @TotProcessEdc real, @TotProcessUEdc real,
		@Complexity real, @EnergyUseDay real, @TotStdEnergy real, @ReportLossGain real, @EstGain real,
		@nmPersEffDiv real, @MaintEffDiv real, @PersEffDiv real
SELECT 	@Edc = AvgEdc, @UEdc = AvgUEdc, @Complexity = Complexity, @RefUtilPcnt = UtilPcnt, 
		@ProcessUtilPcnt = ProcessUtilPcnt, @TotProcessEdc = AvgProcessEdc, @TotProcessUEdc = AvgProcessUEdc, @UtilOSTA = UtilOSTA,
		@EII = EII, @EnergyUseDay = EnergyUseDay, @TotStdEnergy = AvgStdEnergy, 
		@VEI = VEI, @ReportLossGain = LossGainBpD, @EstGain = AvgStdGainBpD, 
		@nmPersEffDiv = NonMaintPersEffDiv, @MaintEffDiv = MaintEffDiv, @PersEffDiv = PersEffDiv
FROM dbo.CalcAverageFactors(@RefineryID, @DataSet, @FactorSet, @StartDate, @EndDate) a

DECLARE @MaintIndex real, @MaintEffIndex real
SELECT @MaintIndex = MaintIndex, @MaintEffIndex = MaintEffIndex
FROM dbo.CalcMaintIndex(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet, @Currency)

DECLARE @PersIndex real, @TotMaintForceWHrEdc real, @PersEffIndex real, @nmPersEffIndex real
SELECT @PersIndex = a.TotWHrEdc, @TotMaintForceWHrEdc  = a.TotMaintForceWHrEdc, @nmPersEffIndex = NonMaintPEI, @PersEffIndex = PEI
FROM dbo.CalcAveragePersKPIs(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet) a

DECLARE @MechUnavailRout real, @MechUnavailTA real, @RegUnavail real, @MechAvail real, @OpAvail real, @ProcessEffIndex real
SELECT @MechUnavailRout = [$(GlobalDB)].dbo.WtAvg(100-(MechAvail_Act+MechUnavailTA_Act), f.TotProcessEdc*s.FractionOfYear), 
	@MechUnavailTA = [$(GlobalDB)].dbo.WtAvg(MechUnavailTA_Ann, CASE WHEN s.PeriodEnd = @EndDate THEN 1 ELSE 0 END),
	@RegUnavail = [$(GlobalDB)].dbo.WtAvg(MechAvail_Act-OpAvail_Act, f.TotProcessEdc*s.FractionOfYear), 
/*	@TotProcessEdc = SUM(f.TotProcessEdc*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEdc = SUM(f.TotProcessUEdc*s.NumDays*1.0)/SUM(s.NumDays*1.0),
*/	@EnergyUseDay = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0)/1000.0, 
	@TotStdEnergy = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0)/1000.0,
	@ReportLossGain = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain = SUM(f.EstGain)/SUM(s.NumDays*1.0)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN dbo.Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @MechAvail = 100 - @MechUnavailTA - @MechUnavailRout
SELECT @OpAvail = @MechAvail - @RegUnavail

SELECT @ProcessEffIndex = dbo.AvgProcessEffIndex(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet)

DECLARE @GainPcnt real, @RawMatlKBpD real, @ProdYieldKBpD real, @NetInputBPD real
SELECT @GainPcnt = SUM(GainBbl)/SUM(NetInputBbl)*100, @RawMatlKBpD = SUM(NetInputBbl)/SUM(s.NumDays*1000.0), @ProdYieldKBpD = SUM(NetInputBbl+GainBbl)/SUM(s.NumDays*1000.0),
	@NetInputBPD = SUM(NetInputBbl)/SUM(s.NumDays*1.0)
FROM MaterialTot m INNER JOIN dbo.Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1 AND m.NetInputBbl > 0

DECLARE @OpExUEdc real, @NEOpExEdc real
SELECT @OpExUEdc = [$(GlobalDB)].dbo.WtAvg(TotCashOpEx,Divisor)
FROM OpExCalc o INNER JOIN dbo.Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
AND o.DataType = 'UEdc' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NEOpExEdc = [$(GlobalDB)].dbo.WtAvg(NEOpEx,Divisor)
FROM OpExCalc o INNER JOIN dbo.Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
AND o.DataType = 'Edc' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

DECLARE @NonMaintWHr real, @TotMaintForceWHr real, @MaintTAWHr real, @MaintNonTAWHr real
SELECT @NonMaintWHr = SUM(NonMaintWHr/1000), @TotMaintForceWHr = SUM(TotMaintForceWHr/1000), @MaintTAWHr = SUM(TotNonTAWHr - TotNonMaintWHr)/1000, @MaintNonTAWHr = SUM(TotMaintForceWHr - (TotNonTAWHr - TotNonMaintWHr))/1000
FROM PersTot p INNER JOIN dbo.Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1

DECLARE @AnnTACost real, @RoutCost real
SELECT @AnnTACost = SUM(AllocAnnTACost)/1000, @RoutCost = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN dbo.Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1 AND mtc.Currency = @Currency

DECLARE @NEOpEx real, @OpExEdc real, @EnergyCost real, @TotCashOpEx real
SELECT @NEOpEx = SUM(NEOpEx*Divisor/1000), @OpExEdc = SUM(TotCashOpEx*Divisor)/SUM(Divisor), @EnergyCost = SUM(EnergyCost*Divisor/1000), @TotCashOpEx = SUM(TotCashOpEx*Divisor/1000)
FROM OpExCalc o INNER JOIN dbo.Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1 
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'Edc'

INSERT @averages (RefUtilPcnt, UtilOSTA, Edc, UEdc, ProcessUtilPcnt, TotProcessEdc, TotProcessUEdc
	, MechAvail, OpAvail, MechUnavailRout, MechUnavailTA, RegUnavail
	, EII, EnergyUseDay, TotStdEnergy
	, VEI, ProcessEffIndex, ReportLossGain, EstGain, GainPcnt, RawMatlKBpD, ProdYieldKBpD, NetInputBPD
	, TotMaintForceWHrEdc, MaintTAWHr, MaintNonTAWHr
	, PersIndex, NonMaintWHr, TotMaintForceWHr, PersEffIndex, PersEffDiv
	, NMPersEffIndex, NMPersEffDiv
	, MaintIndex, AnnTACost, RoutCost, MaintEffIndex, MaintEffDiv
	, OpExUEdc, NEOpExEdc, NEOpEx, OpExEdc, EnergyCost, TotCashOpEx)
VALUES (@RefUtilPcnt, @UtilOSTA, @Edc, @UEdc, @ProcessUtilPcnt, @TotProcessEdc, @TotProcessUEdc
	, @MechAvail, @OpAvail, @MechUnavailRout, @MechUnavailTA, @RegUnavail
	, @EII, @EnergyUseDay, @TotStdEnergy
	, @VEI, @ProcessEffIndex, @ReportLossGain, @EstGain, @GainPcnt, @RawMatlKBpD, @ProdYieldKBpD, @NetInputBPD
	, @TotMaintForceWHrEdc, @MaintTAWHr, @MaintNonTAWHr
	, @PersIndex, @NonMaintWHr, @TotMaintForceWHr, @PersEffIndex, @PersEffDiv
	, @NMPersEffIndex, @NMPersEffDiv
	, @MaintIndex, @AnnTACost, @RoutCost, @MaintEffIndex, @MaintEffDiv
	, @OpExUEdc, @NEOpExEdc, @NEOpEx, @OpExEdc, @EnergyCost, @TotCashOpEx)
	
IF @UOM = 'MET'
	UPDATE @averages
	SET EnergyUseDay = EnergyUseDay * 1.055, TotStdEnergy = TotStdEnergy * 1.055
IF @Currency = 'RUB'
	UPDATE @averages SET OpExUEdc = OpExUEdc/100

UPDATE @averages SET UtilUEdc = Edc*(RefUtilPcnt/100.0)

RETURN

END


