﻿
CREATE FUNCTION [dbo].[SLAverageOpExByDataType](@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @DataType varchar(8))
RETURNS @OpEx TABLE (FactorSet varchar(8) NOT NULL, Currency dbo.CurrencyCode NOT NULL, Scenario dbo.Scenario NOT NULL, DataType varchar(8) NOT NULL
	, OCCSal real NULL, MPSSal real NULL, OCCBen real NULL, MPSBen real NULL
	, MaintMatl real NULL, ContMaintMatl real NULL, MaintMatlST real NULL
	, ContMaintLabor real NULL, ContMaintInspect real NULL, ContMaintLaborST real NULL, OthCont real NULL
	, EquipMaint real NULL, EquipNonMaint real NULL, Equip real NULL
	, Tax real NULL, InsurBI real NULL, InsurPC real NULL, InsurOth real NULL, Insur real NULL
	, TAAdj real NULL, Envir real NULL, OthNonVol real NULL, GAPers real NULL, STNonVol real NULL
	, Antiknock real NULL, Chemicals real NULL, Catalysts real NULL, Royalties real NULL
	, PurElec real NULL, PurSteam real NULL, PurOth real NULL, PurFG real NULL, PurLiquid real NULL, PurSolid real NULL, RefProdFG real NULL, RefProdOth real NULL
	, EmissionsPurch real NULL, EmissionsCredits real NULL, EmissionsTaxes real NULL, OthVol real NULL, STVol real NULL
	, TotCashOpEx real NULL
	, GANonPers real NULL, InvenCarry real NULL, Depreciation real NULL, Interest real NULL, STNonCash real NULL, TotRefExp real NULL
	, Cogen real NULL, OthRevenue real NULL
	, ThirdPartyTerminalRM real NULL, ThirdPartyTerminalProd real NULL, POXO2 real NULL, PMAA real NULL
	, FireSafetyLoss real NULL, EnvirFines real NULL, ExclOth real NULL, TotExpExcl real NULL
	, STSal real NULL, STBen real NULL, PersCostExclTA real NULL, PersCost real NULL
	, EnergyCost real NULL, NEOpEx real NULL, Divisor real NULL)
AS BEGIN
	INSERT @OpEx(FactorSet, Currency, Scenario, DataType
		, OCCSal, MPSSal, OCCBen, MPSBen
		, MaintMatl, ContMaintMatl, MaintMatlST
		, ContMaintLabor, ContMaintInspect, ContMaintLaborST, OthCont
		, EquipMaint, EquipNonMaint, Equip
		, Tax, InsurBI, InsurPC, InsurOth, Insur
		, TAAdj
		, Envir, OthNonVol, GAPers
		, STNonVol
		, Antiknock, Chemicals, Catalysts, Royalties
		, PurElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth
		, EmissionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol
		, TotCashOpEx
		, GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp
		, Cogen, OthRevenue
		, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA
		, FireSafetyLoss, EnvirFines, ExclOth, TotExpExcl
		, STSal, STBen, PersCostExclTA
		, PersCost, EnergyCost, NEOpEx, Divisor)
	SELECT x.FactorSet, x.Currency, x.Scenario, x.DataType
		, x.OCCSal, x.MPSSal, x.OCCBen, x.MPSBen
		, x.MaintMatl, x.ContMaintMatl, x.MaintMatlST
		, x.ContMaintLabor, x.ContMaintInspect, x.ContMaintLaborST, x.OthCont
		, x.EquipMaint, x.EquipNonMaint, x.Equip
		, x.Tax, x.InsurBI, x.InsurPC, x.InsurOth, x.Insur
		, TAAdj = ta.PeriodTAAdj/x.Divisor
		, x.Envir, x.OthNonVol, x.GAPers
		, STNonVol = x.STNonVol - x.TAAdj + ta.PeriodTAAdj/x.Divisor
		, x.Antiknock, x.Chemicals, x.Catalysts, x.Royalties
		, x.PurElec, x.PurSteam, x.PurOth, x.PurFG, x.PurLiquid, x.PurSolid, x.RefProdFG, x.RefProdOth
		, x.EmissionsPurch, x.EmissionsCredits, x.EmissionsTaxes, x.OthVol, x.STVol
		, TotCashOpEx = x.TotCashOpEx - x.TAAdj + ta.PeriodTAAdj/x.Divisor
		, x.GANonPers, x.InvenCarry, x.Depreciation, x.Interest, x.STNonCash, x.TotRefExp
		, x.Cogen, x.OthRevenue
		, x.ThirdPartyTerminalRM, x.ThirdPartyTerminalProd, x.POXO2, x.PMAA
		, x.FireSafetyLoss, x.EnvirFines, x.ExclOth, x.TotExpExcl
		, x.STSal, x.STBen, x.PersCostExclTA
		, PersCost = x.PersCostExclTA + 0.55*(ta.PeriodTAAdj/x.Divisor)
		, x.EnergyCost
		, NEOpEx = x.NEOpEx - x.TAAdj + ta.PeriodTAAdj/x.Divisor
		, x.Divisor
	FROM (
	SELECT o.FactorSet, o.Currency, Scenario = ISNULL(@Scenario, o.Scenario), o.DataType
		, OCCSal = [$(GlobalDB)].dbo.WtAvg(OCCSal, Divisor)
		, MPSSal = [$(GlobalDB)].dbo.WtAvg(MPSSal, Divisor)
		, OCCBen = [$(GlobalDB)].dbo.WtAvg(OCCBen, Divisor)
		, MPSBen = [$(GlobalDB)].dbo.WtAvg(MPSBen, Divisor)
		, MaintMatl = [$(GlobalDB)].dbo.WtAvg(MaintMatl, Divisor)
		, ContMaintLabor = [$(GlobalDB)].dbo.WtAvg(ContMaintLabor, Divisor)
		, ContMaintInspect = [$(GlobalDB)].dbo.WtAvg(ContMaintInspect, Divisor)
		, ContMaintLaborST = [$(GlobalDB)].dbo.WtAvg(ContMaintLaborST, Divisor)
		, ContMaintMatl = [$(GlobalDB)].dbo.WtAvg(ContMaintMatl, Divisor)
		, MaintMatlST = [$(GlobalDB)].dbo.WtAvg(MaintMatlST, Divisor)
		, OthCont = [$(GlobalDB)].dbo.WtAvg(OthCont, Divisor)
		, EquipMaint = [$(GlobalDB)].dbo.WtAvg(EquipMaint, Divisor)
		, EquipNonMaint = [$(GlobalDB)].dbo.WtAvg(EquipNonMaint, Divisor)
		, Equip = [$(GlobalDB)].dbo.WtAvg(Equip, Divisor)
		, Tax = [$(GlobalDB)].dbo.WtAvg(Tax, Divisor)
		, InsurBI = [$(GlobalDB)].dbo.WtAvg(InsurBI, Divisor)
		, InsurPC = [$(GlobalDB)].dbo.WtAvg(InsurPC, Divisor)
		, InsurOth = [$(GlobalDB)].dbo.WtAvg(InsurOth, Divisor)
		, Insur = [$(GlobalDB)].dbo.WtAvg(Insur, Divisor)
		, TAAdj = [$(GlobalDB)].dbo.WtAvg(TAAdj, Divisor)
		, Envir = [$(GlobalDB)].dbo.WtAvg(Envir, Divisor)
		, OthNonVol = [$(GlobalDB)].dbo.WtAvg(OthNonVol, Divisor)
		, GAPers = [$(GlobalDB)].dbo.WtAvg(GAPers, Divisor)
		, STNonVol = [$(GlobalDB)].dbo.WtAvg(STNonVol, Divisor)
		, Antiknock = [$(GlobalDB)].dbo.WtAvg(Antiknock, Divisor)
		, Chemicals = [$(GlobalDB)].dbo.WtAvg(Chemicals, Divisor)
		, Catalysts = [$(GlobalDB)].dbo.WtAvg(Catalysts, Divisor)
		, Royalties = [$(GlobalDB)].dbo.WtAvg(Royalties, Divisor)
		, PurElec = [$(GlobalDB)].dbo.WtAvg(PurElec, Divisor)
		, PurSteam = [$(GlobalDB)].dbo.WtAvg(PurSteam, Divisor)
		, PurOth = [$(GlobalDB)].dbo.WtAvg(PurOth, Divisor)
		, PurFG = [$(GlobalDB)].dbo.WtAvg(PurFG, Divisor)
		, PurLiquid = [$(GlobalDB)].dbo.WtAvg(PurLiquid, Divisor)
		, PurSolid = [$(GlobalDB)].dbo.WtAvg(PurSolid, Divisor)
		, RefProdFG = [$(GlobalDB)].dbo.WtAvg(RefProdFG, Divisor)
		, RefProdOth = [$(GlobalDB)].dbo.WtAvg(RefProdOth, Divisor)
		, EmissionsPurch = [$(GlobalDB)].dbo.WtAvg(EmissionsPurch, Divisor)
		, EmissionsCredits = [$(GlobalDB)].dbo.WtAvg(EmissionsCredits, Divisor)
		, EmissionsTaxes = [$(GlobalDB)].dbo.WtAvg(EmissionsTaxes, Divisor)
		, OthVol = [$(GlobalDB)].dbo.WtAvg(OthVol, Divisor)
		, STVol = [$(GlobalDB)].dbo.WtAvg(STVol, Divisor)
		, TotCashOpEx = [$(GlobalDB)].dbo.WtAvg(TotCashOpEx, Divisor)
		, GANonPers = [$(GlobalDB)].dbo.WtAvg(GANonPers, Divisor)
		, InvenCarry = [$(GlobalDB)].dbo.WtAvg(InvenCarry, Divisor)
		, Depreciation = [$(GlobalDB)].dbo.WtAvg(Depreciation, Divisor)
		, Interest = [$(GlobalDB)].dbo.WtAvg(Interest, Divisor)
		, STNonCash = [$(GlobalDB)].dbo.WtAvg(STNonCash, Divisor)
		, TotRefExp = [$(GlobalDB)].dbo.WtAvg(TotRefExp, Divisor)
		, Cogen = [$(GlobalDB)].dbo.WtAvg(Cogen, Divisor)
		, OthRevenue = [$(GlobalDB)].dbo.WtAvg(OthRevenue, Divisor)
		, ThirdPartyTerminalRM = [$(GlobalDB)].dbo.WtAvg(ThirdPartyTerminalRM, Divisor)
		, ThirdPartyTerminalProd = [$(GlobalDB)].dbo.WtAvg(ThirdPartyTerminalProd, Divisor)
		, POXO2 = [$(GlobalDB)].dbo.WtAvg(POXO2, Divisor)
		, PMAA = [$(GlobalDB)].dbo.WtAvg(PMAA, Divisor)
		, FireSafetyLoss = [$(GlobalDB)].dbo.WtAvg(FireSafetyLoss, Divisor)
		, EnvirFines = [$(GlobalDB)].dbo.WtAvg(EnvirFines, Divisor)
		, ExclOth = [$(GlobalDB)].dbo.WtAvg(ExclOth, Divisor)
		, TotExpExcl = [$(GlobalDB)].dbo.WtAvg(TotExpExcl, Divisor)
		, STSal = [$(GlobalDB)].dbo.WtAvg(STSal, Divisor)
		, STBen = [$(GlobalDB)].dbo.WtAvg(STBen, Divisor)
		, PersCostExclTA = [$(GlobalDB)].dbo.WtAvg(PersCostExclTA, Divisor)
		, PersCost = [$(GlobalDB)].dbo.WtAvg(PersCost, Divisor)
		, EnergyCost = [$(GlobalDB)].dbo.WtAvg(EnergyCost, Divisor)
		, NEOpEx = [$(GlobalDB)].dbo.WtAvg(NEOpEx, Divisor)
		, Divisor = SUM(Divisor)
	FROM OpExCalc o INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = o.SubmissionID
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	WHERE o.Scenario = 'CLIENT' AND o.FactorSet = ISNULL(@FactorSet, o.FactorSet) AND o.Currency = ISNULL(@Currency, o.Currency) AND o.DataType = ISNULL(@DataType, o.DataType)
	GROUP BY o.FactorSet, o.Currency, o.Scenario, o.DataType) x
	INNER JOIN dbo.SLTAAdj(@SubmissionList, @FactorSet, @Currency) ta ON ta.FactorSet = x.FactorSet AND ta.Currency = x.Currency
	
	RETURN
END

