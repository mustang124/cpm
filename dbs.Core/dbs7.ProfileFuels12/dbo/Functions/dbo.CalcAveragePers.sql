﻿CREATE FUNCTION [dbo].[CalcAveragePers](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT OCCAbsPcnt = dbo.GetAvgOCCAbsPcnt(@RefineryID, @DataSet, @StartDate, @EndDate)
		, MPSAbsPcnt = dbo.GetAvgMPSAbsPcnt(@RefineryID, @DataSet, @StartDate, @EndDate)
		, OccoVtPcnt = dbo.GetAvgOvertimePcnt(@RefineryID, @DataSet, @StartDate, @EndDate, 'TO')
		, MPSOvtPcnt = dbo.GetAvgOvertimePcnt(@RefineryID, @DataSet, @StartDate, @EndDate, 'TM')
		, ProcoCcmpsRatio = dbo.GetAvgOCCMPSRatio(@RefineryID, @DataSet, @StartDate, @EndDate, 'OO', 'MO')
		, MaintOCCMPSRatio = dbo.GetAvgOCCMPSRatio(@RefineryID, @DataSet, @StartDate, @EndDate, 'OM', 'MM')
	)
