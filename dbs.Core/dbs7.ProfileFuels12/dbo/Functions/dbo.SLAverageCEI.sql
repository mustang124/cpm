﻿
CREATE  FUNCTION [dbo].[SLAverageCEI](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet FactorSet)
RETURNS real
AS
BEGIN
	DECLARE @CEI real
	SELECT @CEI = [$(GlobalDB)].dbo.WtAvgNZ(CEI, SCE_Total*s.NumDays)
	FROM CEI2008 a INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = a.SubmissionID   
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	WHERE a.FactorSet = @FactorSet AND a.CEI > 0
	
	RETURN @CEI
END

