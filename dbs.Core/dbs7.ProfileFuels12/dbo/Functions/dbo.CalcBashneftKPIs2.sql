﻿CREATE FUNCTION [dbo].[CalcBashneftKPIs2] (@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5))
RETURNS @KPIData TABLE (
	ProcessUtilPcnt real NULL, ProcessEdc real NULL, ProcessUEdc real NULL, OffsitesEdc real NULL, Complexity real NULL,
	EII real NULL, EnergyUseDay real NULL, TotStdEnergy real NULL, FuelUseDay real NULL,
	OpAvail real NULL, MechUnavailTA real NULL, NonTAOpUnavail real NULL, TADD real NULL, NTAMDD real NULL, RPDD real NULL, NumDays real NULL,
	MechAvail real NULL, NonTAMechUnavail real NULL,
	MaintEffIndex real NULL, AnnTACost real NULL, RoutCost real NULL, MaintEffDiv real NULL, TAIndex real NULL,
	mPersEffIndex real NULL, MaintWHr real NULL, OCCMaintWHr real NULL, MPSMaintWHr real NULL, mPersEffDiv real NULL, 
	NEOpExEdc real NULL, NEOpEx real NULL, Edc real NULL,
	TotCashOpExUEdc real NULL, TAAdj real NULL, EnergyCost real NULL, TotCashOpEx real NULL, UEdc real NULL, ExchRate real NULL
)
AS
BEGIN

DECLARE	
	@ProcessUtilPcnt real, @TotProcessEdc real, @TotProcessUEdc real, @OffsitesEdc real, @Complexity real,
	@EII real, @EnergyUseDay real, @FuelUseDay real, @TotStdEnergy real,
	@OpAvail real, @MechAvail real, @MechUnavailTA real, @NonTAMechUnavail real, @NonTAOpUnavail real, @RegUnavail real,
	@TADD real, @NTAMDD real, @RPDD real, @NumDays real,
	@MaintEffIndex real, @TAAdj real, @AnnTACost real, @RoutCost real, @MaintEffDiv real, @Edc real, @TAIndex real, @TAEffIndex real,
	@mPersEffIndex real, @MaintWHr real, @OCCMaintWHr real, @MPSMaintWHr real, @mPersEffDiv real,
	@OpExUEdc real, @EnergyCost real, @TotCashOpEx real, @UEdc real, @NEOpExEdc real, @NEOpEx real, @ExchRate real

SELECT @ProcessUtilPcnt = ProcessUtilPcnt, @TotProcessEdc = TotProcessEdc/1000, @TotProcessUEdc = TotProcessUEdc/1000, @Complexity = Complexity 
	, @EII = EII, @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
	, @OpAvail = OpAvail, @MechAvail = MechAvail, @MechUnavailTA = MechUnavailTA, @NonTAMechUnavail = MechUnavailRout, @NonTAOpUnavail = RegUnavail + MechUnavailRout, @RegUnavail = RegUnavail
	, @NumDays = NumDays
	, @MaintEffIndex = MaintEffIndex, @TAAdj = AnnTACost, @AnnTACost = AnnTACost, @RoutCost = RoutCost, @MaintEffDiv = MaintEffDiv, @Edc = Edc/1000, @TAIndex = TAIndex, @TAEffIndex = TAEffIndex
	, @mPersEffIndex = mPersEffIndex, @MaintWHr = MaintWHr_k, @OCCMaintWHr = OCCMaintWHr_k, @MPSMaintWHr = MPSMaintWHr_k, @mPersEffDiv = mPersEffDiv
	, @OpExUEdc = OpExUEdc, @EnergyCost = EnergyCost, @TotCashOpEx = TotCashOpEx, @UEdc = UEdc/1000, @NEOpExEdc = NEOpExEdc, @NEOpEx = NEOpEx
FROM dbo.SLProfileLiteKPIs(@SubmissionList, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @OffsitesEdc = @Edc - @TotProcessEdc

SELECT @FuelUseDay = SUM(PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU)/(SUM(s.NumDays*1.0)/COUNT(DISTINCT s.RefineryID+s.DataSet))*CASE WHEN LEFT(@UOM,3)='MET' THEN 1.055 ELSE 1.0 END
FROM EnergyTot e INNER JOIN Submissions s ON s.SubmissionID = e.SubmissionID
INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID

SELECT @TADD = @MechUnavailTA/100*@NumDays, @NTAMDD = @NonTAMechUnavail/100*@NumDays, @RPDD = @RegUnavail/100*@NumDays

SELECT @MaintWHr = @mPersEffIndex*@mPersEffDiv/100
	, @OCCMaintWHr = @mPersEffIndex*@mPersEffDiv/100*(OCCWHrEdc/TotWHrEdc)
	, @MPSMaintWHr = @mPersEffIndex*@mPersEffDiv/100*(MpsWhrEdc/TotWHrEdc)
FROM dbo.SLAveragePersKPIs(@SubmissionList, @FactorSet)

SELECT @ExchRate = dbo.AvgExchangeRate('RUB', MIN(PeriodStart), MAX(PeriodEnd))
FROM Submissions WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)

INSERT @KPIData (ProcessUtilPcnt, ProcessEdc, ProcessUEdc, OffsitesEdc, Complexity,
	EII, EnergyUseDay, TotStdEnergy, FuelUseDay, 
	OpAvail, MechUnavailTA, NonTAOpUnavail, TADD, NTAMDD, RPDD, NumDays,
	MechAvail, NonTAMechUnavail,
	MaintEffIndex, AnnTACost, RoutCost, MaintEffDiv, TAIndex,
	mPersEffIndex, MaintWHr, OCCMaintWHr, MPSMaintWHr, mPersEffDiv, 
	NEOpExEdc, NEOpEx, Edc,
	TotCashOpExUEdc, TAAdj, EnergyCost, TotCashOpEx, UEdc, ExchRate)
SELECT @ProcessUtilPcnt, @TotProcessEdc, @TotProcessUEdc, @OffsitesEdc, @Complexity, 	
	@EII, @EnergyUseDay, @TotStdEnergy, @FuelUseDay,
	@OpAvail, @MechUnavailTA, @NonTAOpUnavail, TADD = @MechUnavailTA/100*@NumDays, NTAMDD = @NonTAMechUnavail/100*@NumDays, RPDD = @RegUnavail/100*@NumDays, @NumDays,
	@MechAvail, @NonTAMechUnavail,
	@MaintEffIndex, @TAAdj/1000, @RoutCost/1000, @MaintEffDiv/1000000, @TAIndex,
	@mPersEffIndex, @MaintWHr/1000, @OCCMaintWHr/1000, @MPSMaintWHr/1000, @mPersEffDiv/1000,
	@NEOpExEdc, @NEOpEx/1000, @Edc,
	@OpExUEdc, @TAAdj/1000, @EnergyCost/1000, @TotCashOpEx/1000, @UEdc, @ExchRate

RETURN

END

