﻿CREATE FUNCTION [dbo].[GetAvgOvertimePcnt](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @SectionID char(2))
RETURNS real
AS
BEGIN
	DECLARE @OVTPcnt real
	SELECT @OVTPcnt = SUM(p.OVTHours)/SUM(p.STH)*100
	FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartDate, @EndDate) s 
	INNER JOIN PersST p ON p.SubmissionID = s.SubmissionID AND p.SectionID = @SectionID AND p.STH > 0
	
	RETURN @OVTPcnt
END
