﻿CREATE FUNCTION [dbo].[SLAvgOCCAbsPcnt](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS real
AS
BEGIN
	DECLARE @AbsPcnt real, @AbsHrs float, @STH float

	SELECT @AbsHrs = OCCAbs FROM dbo.SLSumAbsenceTot(@SubmissionList)
	SELECT @STH = STH FROM dbo.SLSumPersSection(@SubmissionList, 'TO')
	
	IF @STH > 0
		SELECT @AbsPcnt = @AbsHrs/@STH*100
	ELSE
		SET @AbsPcnt = NULL
		
	RETURN @AbsPcnt
END
