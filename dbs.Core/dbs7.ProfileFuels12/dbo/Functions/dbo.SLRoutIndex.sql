﻿CREATE FUNCTION [dbo].[SLRoutIndex](@SubmissionList dbo.SubmissionIDList READONLY, @NumMonths tinyint, @FactorSet varchar(8), @Currency char(4))
RETURNS @tblIndexes TABLE (
	FactorSet varchar(8) NOT NULL,
	Currency char(4) NOT NULL,
	RoutIndex real NULL,
	RoutMatlIndex real NULL,
	RoutEffIndex real NULL)
AS
BEGIN
	DECLARE @StartDate smalldatetime, @EndDate smalldatetime
	SELECT @EndDate = MAX(PeriodEnd) FROM dbo.SubmissionsAll WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)
	SELECT @StartDate = DATEADD(MM, -@NumMonths, @EndDate)
	
	IF @NumMonths > 0
	BEGIN
		DECLARE @DataSets TABLE (
			RefineryID char(6) NOT NULL,
			DataSet varchar(15) NOT NULL)
		INSERT @DataSets SELECT DISTINCT RefineryID, DataSet FROM dbo.SubmissionsAll WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList sl)
		
		DECLARE @tblRout TABLE (
			RefineryID char(6) NOT NULL,
			DataSet varchar(15) NOT NULL,
			Currency char(4) NOT NULL,
			PeriodStart smalldatetime NOT NULL,
			PeriodEnd smalldatetime NOT NULL,
			RoutCost real NULL,
			RoutMatl real NULL)
		INSERT INTO @tblRout (RefineryID, DataSet, Currency, PeriodStart, PeriodEnd, RoutCost, RoutMatl)
		SELECT mrh.RefineryID, mrh.DataSet, mrh.Currency, mrh.PeriodStart, mrh.PeriodEnd, mrh.RoutCost, mrh.RoutMatl
		FROM MaintRoutHist mrh INNER JOIN @DataSets r 
			ON r.RefineryID = mrh.RefineryID AND r.DataSet = mrh.DataSet
		WHERE mrh.PeriodStart >= @StartDate AND mrh.PeriodEnd <= @EndDate AND Currency = ISNULL(@Currency, Currency)
		
		SELECT @StartDate = MIN(PeriodStart), @EndDate = MAX(PeriodEnd) FROM @tblRout

		DECLARE @tblEdc TABLE (
			RefineryID char(6) NOT NULL,
			DataSet varchar(15) NOT NULL,
			FactorSet char(8) NOT NULL,
			PeriodStart smalldatetime NOT NULL,
			PeriodEnd smalldatetime NOT NULL,
			PlantEdc real NULL,
			PlantMaintEffDiv real NULL)
		INSERT INTO @tblEdc (RefineryID, DataSet, FactorSet, PeriodStart, PeriodEnd, PlantEdc, PlantMaintEffDiv)
		SELECT s.RefineryID, s.DataSet, f.FactorSet, s.PeriodStart, s.PeriodEnd, f.PlantEdc, f.PlantMaintEffDiv
		FROM @DataSets r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartDate, @EndDate) s INNER JOIN FactorTotCalc f ON f.SubmissionID = s.SubmissionID
		WHERE f.FactorSet = ISNULL(@FactorSet, f.FactorSet)
		/*
		DECLARE @MinStart smalldatetime, @MinStartID int
		SELECT TOP 1 @MinStart = PeriodStart, @MinStartID = SubmissionID
		FROM Submissions
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UseSubmission = 1
		AND EXISTS (SELECT * FROM FactorTotCalc f WHERE f.SubmissionID = Submissions.SubmissionID AND PlantEdc > 0)
		ORDER BY PeriodStart ASC
		*/
		IF EXISTS (SELECT * FROM @tblRout r WHERE NOT EXISTS (SELECT * FROM @tblEdc e WHERE e.RefineryID = r.RefineryID AND e.DataSet = r.DataSet AND e.PeriodStart = r.PeriodStart))
			INSERT INTO @tblEdc (RefineryID, DataSet, FactorSet, PeriodStart, PeriodEnd, PlantEdc, PlantMaintEffDiv)
			SELECT r.RefineryID, r.DataSet, e.FactorSet, r.PeriodStart, r.PeriodEnd, e.PlantEdc, e.PlantMaintEffDiv
			FROM @tblEdc e INNER JOIN @tblRout r ON r.RefineryID = e.RefineryID AND r.DataSet = e.DataSet
			INNER JOIN (SELECT RefineryID, DataSet, MIN(PeriodStart) AS MinPeriodStart FROM @tblEdc GROUP BY RefineryID, DataSet) fs 
				ON fs.RefineryID = e.RefineryID AND fs.DataSet = e.DataSet AND fs.MinPeriodStart = e.PeriodStart
			WHERE NOT EXISTS (SELECT * FROM @tblEdc x WHERE x.RefineryID = r.RefineryID AND x.DataSet = r.DataSet AND x.PeriodStart = r.PeriodStart)

		INSERT @tblIndexes(FactorSet, Currency, RoutIndex, RoutMatlIndex, RoutEffIndex)
		SELECT e.FactorSet, r.Currency, 
			RoutIndex = r.RoutCost*1000/e.PlantEdc, 
			RoutMatlIndex = r.RoutMatl*1000/e.PlantEdc, 
			RoutEffIndex = r.RoutCost*100000/e.PlantMaintEffDiv
		FROM (SELECT Currency, RoutCost = SUM(RoutCost), RoutMatl = SUM(RoutMatl) FROM @tblRout GROUP BY Currency) r,
			(SELECT FactorSet, PlantEdc = SUM(PlantEdc*CAST(DATEDIFF(dd, PeriodStart, PeriodEnd) AS real)/365), 
				PlantMaintEffDiv = SUM(PlantMaintEffDiv*CAST(DATEDIFF(dd, PeriodStart, PeriodEnd) AS real)/365)
			 FROM @tblEdc GROUP BY FactorSet) e
	END
	ELSE BEGIN
		INSERT @tblIndexes(FactorSet, Currency, RoutIndex, RoutMatlIndex, RoutEffIndex)
		SELECT mi.FactorSet, mi.Currency
			, RoutIndex = [$(GlobalDB)].dbo.WtAvg(mi.RoutIndex, ftc.PlantEdc*s.FractionOfYear)
			, RoutMatlIndex = [$(GlobalDB)].dbo.WtAvg(mi.RoutMatlIndex, ftc.PlantEdc*s.FractionOfYear)
			, RoutEffIndex = [$(GlobalDB)].dbo.WtAvg(mi.RoutEffIndex, ftc.PlantMaintEffDiv*s.FractionOfYear)
		FROM dbo.MaintIndex mi INNER JOIN dbo.FactorTotCalc ftc ON ftc.SubmissionID = mi.SubmissionID AND ftc.FactorSet = mi.FactorSet
		INNER JOIN @SubmissionList sl ON sl.SubmissionID = mi.SubmissionID
		INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = mi.SubmissionID
		WHERE mi.FactorSet = ISNULL(@FactorSet, mi.FactorSet) AND mi.Currency = ISNULL(@Currency, mi.Currency)
		GROUP BY mi.FactorSet, mi.Currency
	END
RETURN

END

		