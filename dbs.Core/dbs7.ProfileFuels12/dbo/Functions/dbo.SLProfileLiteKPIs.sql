﻿
CREATE FUNCTION [dbo].[SLProfileLiteKPIs](@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5))
RETURNS @averages TABLE (
	RefUtilPcnt real NULL, 
	UtilOSTA real NULL, 
	Edc real NULL, 
	UtilUEdc real NULL,
	ProcessUtilPcnt real NULL, 
	TotProcessEdc real NULL, 
	TotProcessUEdc real NULL,
	MechAvail real NULL, 
	OpAvail real NULL, 
	MechUnavailRout real NULL, 
	MechUnavailTA real NULL, 
	RegUnavail real NULL, 
	EII real NULL, 
	EnergyUseDay real NULL, 
	TotStdEnergy real NULL,
	VEI real NULL, 
	ReportLossGain real NULL, 
	EstGain real NULL, 
	ProcessEffIndex real,
	GainPcnt real NULL, 
	RawMatlKBpD real NULL, 
	ProdYieldKBpD real NULL, 
	NetInputBPD real NULL,
	TotMaintForceWHrEdc real NULL, 
	MaintTAWHr_k real NULL, 
	MaintNonTAWHr_k real NULL,
	TotNonTAWHr_k real NULL,
	PersIndex real NULL, 
	NonMaintWHr_k real NULL, 
	TotMaintForceWHr_k real NULL, 
	PersEffIndex real NULL,
	PersEffDiv real NULL,
	mPersEffIndex real NULL, 
	MaintWHr_k real NULL, 
	OCCMaintWHr_k real NULL, 
	MPSMaintWHr_k real NULL, 
	mPersEffDiv real NULL,
	nmPersEffIndex real NULL,
	nmPersEffDiv real NULL,
	MaintIndex real NULL,
	TAIndex real NULL,
	RoutIndex real NULL,
	MaintEffIndex real NULL,
	TAEffIndex real NULL,
	RoutEffIndex real NULL,
	AnnTACost real NULL, 
	RoutCost real NULL,
	MaintEffDiv real NULL,
	OpExUEdc real NULL, 
	NEOpExEdc real NULL,	
	NEOpEx real NULL, 
	OpExEdc real NULL, 
	EnergyCost real NULL, 
	TotCashOpEx real NULL,
	UEdc real NULL,
	NumDays real NULL,
	Complexity real NULL,
	MechUnavailTA_Act real NULL
	)
AS BEGIN

DECLARE @EII real, @VEI real, @RefUtilPcnt real, @UtilOSTA real, @Edc real, @UEdc real, @ProcessUtilPcnt real, @TotProcessEdc real, @TotProcessUEdc real,
		@Complexity real, @EnergyUseDay real, @TotStdEnergy real, @ReportLossGain real, @EstGain real,
		@MaintEffDiv real, @mPersEffDiv real, @nmPersEffDiv real, @PersEffDiv real, @NumDays real

SELECT 	@Edc = AvgEdc, @UEdc = AvgUEdc, @Complexity = Complexity, @RefUtilPcnt = UtilPcnt, 
		@ProcessUtilPcnt = ProcessUtilPcnt, @TotProcessEdc = AvgProcessEdc, @TotProcessUEdc = AvgProcessUEdc, @UtilOSTA = UtilOSTA,
		@EII = EII, @EnergyUseDay = EnergyUseDay, @TotStdEnergy = AvgStdEnergy, 
		@VEI = VEI, @ReportLossGain = LossGainBpD, @EstGain = AvgStdGainBpD, 
		@mPersEffDiv = MaintPersEffDiv, @nmPersEffDiv = NonMaintPersEffDiv, @MaintEffDiv = MaintEffDiv, @PersEffDiv = PersEffDiv, @NumDays = NumDays
FROM dbo.SLAverageFactors(@SubmissionList, @FactorSet) a

DECLARE @MaintIndex real, @MaintEffIndex real, @TAIndex real, @TAEffIndex real, @RoutIndex real, @RoutEffIndex real
SELECT @MaintIndex = MaintIndex, @MaintEffIndex = MaintEffIndex, @TAIndex = TAIndex, @TAEffIndex = TAEffIndex, @RoutIndex = RoutIndex, @RoutEffIndex = RoutEffIndex
FROM dbo.SLMaintIndex(@SubmissionList, 0, @FactorSet, @Currency)

DECLARE @PersIndex real, @TotMaintForceWHrEdc real, @PersEffIndex real, @nmPersEffIndex real, @mPersEffIndex real, @MaintWHr real, @OCCMaintWHr real, @MPSMaintWHr real
SELECT @PersIndex = a.TotWHrEdc, @TotMaintForceWHrEdc  = a.TotMaintForceWHrEdc, @mPersEffIndex = MaintPEI, @nmPersEffIndex = NonMaintPEI, @PersEffIndex = PEI
FROM dbo.SLAveragePersKPIs(@SubmissionList, @FactorSet) a

DECLARE @MechUnavailRout real, @MechUnavailTA real, @RegUnavail real, @NonTAOpUnavail real, @MechAvail real, @OpAvail real, @ProcessEffIndex real, @MechUnavailTA_Act real
SELECT @OpAvail = OpAvail_Ann, @MechAvail = MechAvail_Ann,
	@MechUnavailTA = MechUnavailTA_Ann,
	@MechUnavailRout = MechUnavailRout, 
	@RegUnavail = RegUnavail,
	@NonTAOpUnavail = MechUnavailRout + RegUnavail,
	@MechUnavailTA_Act = MechUnavailTA_Act
FROM dbo.SLAvailability(@SubmissionList, @FactorSet)

IF @MechUnavailRout < 0.05
	SELECT @MechUnavailRout = 0
IF @NonTAOpUnavail < 0.05
	SELECT @NonTAOpUnavail = 0, @RegUnavail = 0
	
SELECT @OpAvail = 100 - @NonTAOpUnavail - @MechUnavailTA, @MechAvail = 100 - @MechUnavailRout - @MechUnavailTA

SELECT 	@EnergyUseDay = SUM(EnergyUse)/SUM(NumDays), 
	@TotStdEnergy = SUM(StdEnergy)/SUM(NumDays),
	@ReportLossGain = SUM(LossGain)/SUM(NumDays), 
	@EstGain = SUM(EstGain)/SUM(NumDays)
FROM (
	SELECT EnergyUse = SUM(f.EnergyUseDay*s.NumDays*1.0)
		, StdEnergy = SUM(f.TotStdEnergy*s.NumDays*1.0)
		, LossGain = SUM(f.ReportLossGain)
		, EstGain = SUM(f.EstGain)
		, NumDays = AVG(s.NumDays*1.0)
	FROM FactorTotCalc f 
	INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = f.SubmissionID
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	WHERE f.FactorSet = @FactorSet
	GROUP BY s.PeriodStart) x

SELECT @MechAvail = 100 - @MechUnavailTA - @MechUnavailRout
SELECT @OpAvail = @MechAvail - @RegUnavail

SELECT @ProcessEffIndex = dbo.SLProcessEffIndex(@SubmissionList, @FactorSet)

DECLARE @GainPcnt real, @RawMatlKBpD real, @ProdYieldKBpD real, @NetInputBPD real
SELECT @GainPcnt = SUM(GainBbl)/SUM(NetInputBbl)*100, @RawMatlKBpD = SUM(NetInputBbl)/SUM(NumDays*1000.0), @ProdYieldKBpD = SUM(NetInputBbl+GainBbl)/SUM(NumDays*1000.0),
	@NetInputBPD = SUM(NetInputBbl)/SUM(NumDays)
FROM (
	SELECT GainBbl = SUM(m.GainBbl), NetInputBbl = SUM(m.NetInputBbl), NumDays = AVG(NumDays*1.0)
	FROM MaterialTot m INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = m.SubmissionID
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	WHERE m.NetInputBbl > 0
	GROUP BY s.PeriodStart) x

DECLARE @OpExUEdc real, @NEOpExEdc real
DECLARE @NEOpEx real, @OpExEdc real, @EnergyCost real, @TotCashOpEx real, @TAAdj real
SELECT @NEOpEx = NEOpEx*Divisor, @EnergyCost = EnergyCost*Divisor, @TotCashOpEx = TotCashOpEx*Divisor, @TAAdj = TAAdj*Divisor
FROM dbo.SLAverageOpExByDataType(@SubmissionList, @FactorSet, 'CLIENT', @Currency, 'ADJ')

SELECT @OpExUEdc = TotCashOpEx
FROM dbo.SLAverageOpExByDataType(@SubmissionList, @FactorSet, 'CLIENT', @Currency, 'UEdc')

SELECT @NEOpExEdc = NEOpEx, @OpExEdc = TotCashOpEx
FROM dbo.SLAverageOpExByDataType(@SubmissionList, @FactorSet, 'CLIENT', @Currency, 'Edc')

DECLARE @NonMaintWHr real, @TotMaintForceWHr real, @MaintTAWHr real, @MaintNonTAWHr real, @TotNonTAWHr real
SELECT @NonMaintWHr = SUM(NonMaintWHr/1000), @TotMaintForceWHr = SUM(TotMaintForceWHr/1000), @MaintNonTAWHr = SUM(TotNonTAWHr - TotNonMaintWHr)/1000,
	@TotNonTAWHr = SUM(TotNonTAWHr)/1000
FROM dbo.SLSumPersTot(@SubmissionList) p

SELECT @MaintTAWHr = (OCCTAmPEI+MPSTAmPEI)*@mPersEffDiv/100/1000
FROM dbo.SLPersTAAdj(@SubmissionList, @FactorSet)
	
SELECT @MaintWHr = @mPersEffIndex*@mPersEffDiv/100/1000
	, @OCCMaintWHr = @mPersEffIndex*@mPersEffDiv/100*(OCCWHrEdc/TotWHrEdc)/1000
	, @MPSMaintWHr = @mPersEffIndex*@mPersEffDiv/100*(MpsWhrEdc/TotWHrEdc)/1000
	, @MaintTAWHr = TAMaintPEI*@mPersEffDiv/100/1000
FROM dbo.SLAveragePersKPIs(@SubmissionList, @FactorSet)

DECLARE @RoutCost real
SELECT @RoutCost = CurrRoutCost FROM dbo.SLSumMaintCost(@SubmissionList, @Currency)

INSERT @averages (RefUtilPcnt, UtilOSTA, Edc, UEdc, ProcessUtilPcnt, TotProcessEdc, TotProcessUEdc
	, MechAvail, OpAvail, MechUnavailRout, MechUnavailTA, RegUnavail
	, EII, EnergyUseDay, TotStdEnergy
	, VEI, ProcessEffIndex, ReportLossGain, EstGain, GainPcnt, RawMatlKBpD, ProdYieldKBpD, NetInputBPD
	, TotMaintForceWHrEdc, MaintTAWHr_k, MaintNonTAWHr_k, MaintWHr_k, OCCMaintWHr_k, MPSMaintWHr_k
	, PersIndex, NonMaintWHr_k, TotMaintForceWHr_k, TotNonTAWHr_k, PersEffIndex, PersEffDiv
	, nmPersEffIndex, nmPersEffDiv, mPersEffIndex, mPersEffDiv
	, MaintIndex, TAIndex, RoutIndex, AnnTACost, RoutCost
	, MaintEffIndex, MaintEffDiv, TAEffIndex, RoutEffIndex
	, OpExUEdc, NEOpExEdc, NEOpEx, OpExEdc, EnergyCost, TotCashOpEx, NumDays, Complexity, MechUnavailTA_Act)
VALUES (@RefUtilPcnt, @UtilOSTA, @Edc, @UEdc, @ProcessUtilPcnt, @TotProcessEdc, @TotProcessUEdc
	, @MechAvail, @OpAvail, @MechUnavailRout, @MechUnavailTA, @RegUnavail
	, @EII, @EnergyUseDay, @TotStdEnergy
	, @VEI, @ProcessEffIndex, @ReportLossGain, @EstGain, @GainPcnt, @RawMatlKBpD, @ProdYieldKBpD, @NetInputBPD
	, @TotMaintForceWHrEdc, @MaintTAWHr, @MaintNonTAWHr, @MaintWHr, @OCCMaintWHr, @MPSMaintWHr
	, @PersIndex, @NonMaintWHr, @TotMaintForceWHr, @TotNonTAWHr, @PersEffIndex, @PersEffDiv
	, @NMPersEffIndex, @NMPersEffDiv, @mPersEffIndex, @mPersEffDiv
	, @MaintIndex, @TAIndex, @RoutIndex, @TAAdj, @RoutCost
	, @MaintEffIndex, @MaintEffDiv, @TAEffIndex, @RoutEffIndex
	, @OpExUEdc, @NEOpExEdc, @NEOpEx, @OpExEdc, @EnergyCost, @TotCashOpEx, @NumDays, @Complexity, @MechUnavailTA_Act)
	
IF @Currency = 'RUB'
	UPDATE @averages SET OpExUEdc = OpExUEdc/100

IF RIGHT(@UOM,3) = 'MET'
	UPDATE @averages SET EnergyUseDay = EnergyUseDay*1.055, TotStdEnergy = TotStdEnergy*1.055
	
UPDATE @averages SET UtilUEdc = Edc*(RefUtilPcnt/100.0)

RETURN

END


