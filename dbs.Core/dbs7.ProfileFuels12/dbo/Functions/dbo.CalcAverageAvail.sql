﻿CREATE FUNCTION [dbo].[CalcAverageAvail](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT m.FactorSet, 
	MechAvail_Ann=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Ann,f.TotProcessEdc*s.FractionOfYear),
	MechAvailSlow_Ann=[$(GlobalDB)].dbo.WtAvg(MechAvailSlow_Ann,f.TotProcessEdc*s.FractionOfYear), 
	OpAvail_Ann=[$(GlobalDB)].dbo.WtAvg(OpAvail_Ann,f.TotProcessEdc*s.FractionOfYear), 
	OpAvailSlow_Ann=[$(GlobalDB)].dbo.WtAvg(OpAvailSlow_Ann,f.TotProcessEdc*s.FractionOfYear), 
	OnStream_Ann=[$(GlobalDB)].dbo.WtAvg(OnStream_Ann,f.TotProcessEdc*s.FractionOfYear), 
	OnStreamSlow_Ann=[$(GlobalDB)].dbo.WtAvg(OnStreamSlow_Ann,f.TotProcessEdc*s.FractionOfYear), 
	MechAvail_Act=[$(GlobalDB)].dbo.WtAvg(MechAvail_Act,f.TotProcessEdc*s.FractionOfYear), 
	MechAvailSlow_Act=[$(GlobalDB)].dbo.WtAvg(MechAvailSlow_Act,f.TotProcessEdc*s.FractionOfYear), 
	OpAvail_Act=[$(GlobalDB)].dbo.WtAvg(OpAvail_Act,f.TotProcessEdc*s.FractionOfYear), 
	OpAvailSlow_Act=[$(GlobalDB)].dbo.WtAvg(OpAvailSlow_Act,f.TotProcessEdc*s.FractionOfYear), 
	OnStream_Act=[$(GlobalDB)].dbo.WtAvg(OnStream_Act,f.TotProcessEdc*s.FractionOfYear), 
	OnStreamSlow_Act=[$(GlobalDB)].dbo.WtAvg(OnStreamSlow_Act,f.TotProcessEdc*s.FractionOfYear), 
	MechAvailOSTA=[$(GlobalDB)].dbo.WtAvg(MechAvailOSTA,f.TotProcessEdc*s.FractionOfYear)
	FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
	INNER JOIN dbo.Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	GROUP BY m.FactorSet
)


