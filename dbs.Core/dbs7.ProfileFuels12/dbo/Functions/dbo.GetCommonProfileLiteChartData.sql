﻿CREATE FUNCTION [dbo].[GetCommonProfileLiteChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@numMonths tinyint = 12)
RETURNS @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	RefUtilPcnt real NULL, 
	ProcessUtilPcnt real NULL,
	UtilOSTA real NULL,
	VEI real NULL, 
	ProcessEffIndex real NULL, 
	MechAvail real NULL,
	OpAvail real NULL, 
	MaintIndex real NULL,
	PersIndex real NULL, 
	NEOpExEdc real NULL,
	OpExUEdc real NULL,
	MaintEffIndex real NULL,
	nmPersEffIndex real NULL,
	mPersEffIndex real NULL,
	RoutIndex real NULL
)
AS
BEGIN
	IF @numMonths = NULL
		SET @numMonths = 12

	DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
	
	SELECT @PeriodEnd = PeriodEnd
	FROM dbo.Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet AND UseSubmission = 1
	
	SELECT @PeriodStart12Mo = DATEADD(mm, -@numMonths, @PeriodEnd)

	IF @PeriodEnd IS NULL
		RETURN /* Data has not been submitted yet */
	IF EXISTS (SELECT * FROM dbo.Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND UseSubmission = 1 AND CalcsNeeded IS NOT NULL)
		RETURN /* Calculations still need to be done */

	--- Everything Already Available in GenSum
	INSERT INTO @data (PeriodStart, PeriodEnd, RefUtilPcnt, ProcessUtilPcnt, UtilOSTA, MechAvail, OpAvail, 
		EII, VEI, ProcessEffIndex, PersIndex, MaintIndex, NEOpExEdc, OpExUEdc, MaintEffIndex, nmPersEffIndex, mPersEffIndex, RoutIndex)
	SELECT	s.PeriodStart, s.PeriodEnd, GenSum.UtilPcnt, GenSum.ProcessUtilPcnt, GenSum.UtilOSTA, GenSum.MechAvail, GenSum.OpAvail, 
		GenSum.EII, GenSum.VEI, ProcessEffIndex = f.PEI, PersIndex = TotWHrEdc, MaintIndex = GenSum.RoutIndex + GenSum.TAIndex_Avg, NEOpExEdc, TotCashOpExUEdc,
		MaintEffIndex = mi.RoutEffIndex + mi.TAEffIndex_Avg, nmPersEffIndex = GenSum.NonMaintPEI, mPersEffIndex = GenSum.MaintPEI, GenSum.RoutIndex
	FROM GenSum LEFT JOIN FactorTotCalc f ON f.SubmissionID = GenSum.SubmissionID AND f.FactorSet = GenSum.FactorSet
	 LEFT JOIN MaintIndex mi ON mi.SubmissionID = GenSum.SubmissionID AND mi.Currency = GenSum.Currency AND mi.FactorSet = GenSum.FactorSet
	 INNER JOIN dbo.Submissions s ON s.SubmissionID = GenSum.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @PeriodStart12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
	AND GenSum.FactorSet = @FactorSet AND GenSum.Currency = @Currency AND GenSum.UOM = @UOM AND GenSum.Scenario = @Scenario
	

	IF (SELECT COUNT(*) FROM @data) < @numMonths
	BEGIN
		DECLARE @Period smalldatetime
		SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
		WHILE @Period >= @PeriodStart12Mo
		BEGIN
			IF NOT EXISTS (SELECT * FROM @data WHERE PeriodStart = @Period)
				INSERT @data (PeriodStart) VALUES (@Period)
			SELECT @Period = DATEADD(mm, -1, @Period)
		END
	END

	IF @Currency = 'RUB'
		UPDATE @data SET OpExUEdc = OpExUEdc/100
		
	RETURN
END



