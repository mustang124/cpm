﻿
CREATE FUNCTION [dbo].[SLProcessEffIndex](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet varchar(8))
RETURNS real
AS
BEGIN
	DECLARE @EstGain real, @ReportLossGain real, @PEI real, @NetInputBbl real
	SELECT 	@EstGain = SUM(ftc.EstGain), @NetInputBbl = SUM(mt.NetInputBbl), @ReportLossGain = SUM(mt.GainBbl)
	FROM @SubmissionList s INNER JOIN FactorTotCalc ftc ON ftc.SubmissionID = s.SubmissionID
	INNER JOIN MaterialTot mt ON mt.SubmissionID = s.SubmissionID
	WHERE ftc.FactorSet = @FactorSet AND mt.NetInputBbl > 0

	IF @NetInputBbl>0 
		SELECT @PEI = 100*(@NetInputBbl+@ReportLossGain) / (@NetInputBbl+@EstGain)
	ELSE
		SELECT @PEI = NULL
	
	RETURN @PEI
END


