﻿CREATE FUNCTION dbo.SLGetProcessFactors(@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet)
RETURNS TABLE
AS

	RETURN 
	(
		SELECT fpc.SubmissionID, Edc, UEdc, UtilPcnt, RV, RVBbl, InServicePcnt, YearsOper, UtilOSTA, UEdcOSTA, NEOpExEffDiv, MaintEffDiv, PersEffDiv, MaintPersEffDiv, NonMaintPersEffDiv
		FROM FactorProcessCalc fpc INNER JOIN @SubmissionList sl ON sl.SubmissionID = fpc.SubmissionID
		WHERE fpc.ProcessID = CASE WHEN EXISTS (SELECT * FROM FactorSets WHERE IdleUnitsInProcessResults = 'N' AND FactorSet = @FactorSet) THEN 'OperProc' ELSE 'TotProc' END
	)

