﻿
CREATE FUNCTION [dbo].[SLSumPersSection](@SubmissionList dbo.SubmissionIDList READONLY, @SectionID char(2))
RETURNS TABLE
AS
RETURN (
	SELECT p.SectionID, NumPers = SUM(p.NumPers), STH = SUM(p.STH), OVTHours = SUM(p.OVTHours), OVTPcnt = CASE WHEN SUM(p.STH) > 0 THEN SUM(p.OVTHours)/SUM(p.STH)*100 END, [Contract] = SUM(p.Contract), GA = SUM(p.GA), AbsHrs = SUM(p.AbsHrs)
		, CompEqP = [$(GlobalDB)].dbo.WtAvg(p.CompEqP, s.FractionOfYear), ContEqP = [$(GlobalDB)].dbo.WtAvg(p.ContEqP, s.FractionOfYear), GAEqP = [$(GlobalDB)].dbo.WtAvg(p.GAEqP, s.FractionOfYear), TotEqP = [$(GlobalDB)].dbo.WtAvg(p.TotEqP, s.FractionOfYear)
		, CompWHr = SUM(p.CompWHr), ContWHr = SUM(p.ContWHr), GAWHr = SUM(p.GAWHr), TotWHr = SUM(p.TotWHr)
	FROM @SubmissionList l INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = l.SubmissionID
	INNER JOIN PersST p ON p.SubmissionID = s.SubmissionID AND p.SectionID = ISNULL(@SectionID, p.SectionID)
	GROUP BY p.SectionID
)

