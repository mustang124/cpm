﻿
CREATE FUNCTION [dbo].[SLAverageWorkingCptl](@SubmissionList dbo.SubmissionIDList READONLY, @Scenario dbo.Scenario, @Currency dbo.CurrencyCode)
RETURNS TABLE
AS
RETURN (
	WITH MonthlyTotals AS 
	(
		SELECT mc.Scenario, mc.Currency, s.PeriodYear, s.PeriodMonth
			, RefCount = COUNT(*), NumDays = AVG(s.NumDays), FractionOfYear = AVG(s.FractionOfYear)
			, Inven = SUM(i.Inven)
			, AvgInvenValue = [$(GlobalDB)].dbo.WtAvg(mc.RMC,i.Inven) 
		FROM InventoryTot i INNER JOIN MarginCalc mc ON mc.SubmissionID = i.SubmissionID
		INNER JOIN @SubmissionList sl ON sl.SubmissionID = i.SubmissionID
		INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = sl.SubmissionID
		WHERE mc.Scenario = ISNULL(@Scenario, mc.Scenario) AND mc.Currency = ISNULL(@Currency, mc.Currency) AND mc.DataType = 'Bbl'
		GROUP BY mc.Scenario, mc.Currency, s.PeriodYear, s.PeriodMonth
	)
	SELECT Scenario, Currency
		, Inven = [$(GlobalDB)].dbo.WtAvg(Inven, NumDays)
		, AvgInvenValue = [$(GlobalDB)].dbo.WtAvg(AvgInvenValue, Inven*NumDays)
		, WorkingCptlM = [$(GlobalDB)].dbo.WtAvg(Inven*AvgInvenValue/1e6, NumDays)
		, NumDays = SUM(NumDays)
	FROM MonthlyTotals WHERE RefCount = (SELECT MAX(RefCount) FROM MonthlyTotals)
	GROUP BY Scenario, Currency
	)


