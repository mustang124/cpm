﻿CREATE FUNCTION [dbo].[GetAvgMPSAbsPcnt](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS real
AS
BEGIN
	DECLARE @AbsPcnt real, @AbsHrs float, @STH float

	SELECT @AbsHrs = MPSAbs FROM dbo.SumAbsenceTot(@RefineryID, @DataSet, @StartDate, @EndDate)
	SELECT @STH = STH FROM dbo.SumPersSection(@RefineryID, @DataSet, @StartDate, @EndDate, 'TM')
	
	IF @STH > 0
		SELECT @AbsPcnt = @AbsHrs/@STH*100
	ELSE
		SET @AbsPcnt = NULL
		
	RETURN @AbsPcnt
	END
