﻿CREATE FUNCTION [dbo].[CalcAverageProcessAvail](@SubmissionID int)
RETURNS TABLE
AS
RETURN (
	SELECT ProcessID = pg.ProcessGrouping, f.FactorSet, 
	MechAvail_Ann_Avg=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Ann_Avg,f.EdcNoMult), 
	MechAvailSlow_Ann_Avg=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Ann_Avg,f.EdcNoMult), 
	OpAvail_Ann_Avg=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Ann_Avg,f.EdcNoMult), 
	OpAvailSlow_Ann_Avg=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Ann_Avg,f.EdcNoMult), 
	OnStream_Ann_Avg=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Ann_Avg,f.EdcNoMult), 
	OnStreamSlow_Ann_Avg=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Ann_Avg,f.EdcNoMult), 
	MechAvail_Act_Avg=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Act_Avg,f.EdcNoMult), 
	MechAvailSlow_Act_Avg=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Act_Avg,f.EdcNoMult), 
	OpAvail_Act_Avg=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Act_Avg,f.EdcNoMult), 
	OpAvailSlow_Act_Avg=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Act_Avg,f.EdcNoMult), 
	OnStream_Act_Avg=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Act_Avg,f.EdcNoMult), 
	OnStreamSlow_Act_Avg=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Act_Avg,f.EdcNoMult), 
	MechAvail_Ann_Ytd=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Ann_Ytd,f.EdcNoMult), 
	MechAvailSlow_Ann_Ytd=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Ann_Ytd,f.EdcNoMult), 
	OpAvail_Ann_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Ann_Ytd,f.EdcNoMult), 
	OpAvailSlow_Ann_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Ann_Ytd,f.EdcNoMult), 
	OnStream_Ann_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Ann_Ytd,f.EdcNoMult), 
	OnStreamSlow_Ann_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Ann_Ytd,f.EdcNoMult), 
	MechAvail_Act_Ytd=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Act_Ytd,f.EdcNoMult), 
	MechAvailSlow_Act_Ytd=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Act_Ytd,f.EdcNoMult), 
	OpAvail_Act_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Act_Ytd,f.EdcNoMult), 
	OpAvailSlow_Act_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Act_Ytd,f.EdcNoMult), 
	OnStream_Act_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Act_Ytd,f.EdcNoMult), 
	OnStreamSlow_Act_Ytd=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Act_Ytd,f.EdcNoMult), 
	MechAvailOSTA_Avg=[$(GlobalDB)].dbo.WtAvg(m.MechAvailOSTA_Avg,f.EdcNoMult), 
	MechAvailOSTA_Ytd=[$(GlobalDB)].dbo.WtAvg(m.MechAvailOSTA_Ytd,f.EdcNoMult), 
	MechAvail_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Ann_Target,f.EdcNoMult), 
	MechAvailSlow_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Ann_Target,f.EdcNoMult), 
	OpAvail_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Ann_Target,f.EdcNoMult), 
	OpAvailSlow_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Ann_Target,f.EdcNoMult), 
	OnStream_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Ann_Target,f.EdcNoMult), 
	OnStreamSlow_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Ann_Target,f.EdcNoMult), 
	MechAvail_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Act_Target,f.EdcNoMult), 
	MechAvailSlow_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Act_Target,f.EdcNoMult), 
	OpAvail_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Act_Target,f.EdcNoMult), 
	OpAvailSlow_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Act_Target,f.EdcNoMult), 
	OnStream_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Act_Target,f.EdcNoMult), 
	OnStreamSlow_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Act_Target,f.EdcNoMult)
	FROM MaintCalc m 
	INNER JOIN FactorCalc f ON f.SubmissionID = m.SubmissionID AND f.UnitID = m.UnitID
	INNER JOIN RefProcessGroupings pg ON pg.SubmissionID = m.SubmissionID AND pg.UnitID = m.UnitID
	WHERE m.SubmissionID = @SubmissionID AND f.EdcNoMult > 0
	GROUP BY pg.ProcessGrouping, f.FactorSet
)
