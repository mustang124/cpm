﻿CREATE  FUNCTION [dbo].[CalcAverageOpExByDataType](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @DataType varchar(8))
RETURNS TABLE 
AS

	RETURN (
	 -- Using Client Energy prices for all pricing scenarios
	SELECT o.FactorSet, o.Currency, Scenario = ISNULL(@Scenario, o.Scenario), o.DataType
		, TotCashOpEx = [$(GlobalDB)].dbo.WtAvg(TotCashOpEx, Divisor)
		, STVol = [$(GlobalDB)].dbo.WtAvg(STVol, Divisor)
		, STNonVol = [$(GlobalDB)].dbo.WtAvg(STNonVol, Divisor)
		, NEOpEx = [$(GlobalDB)].dbo.WtAvg(NEOpEx, Divisor)
	FROM OpExCalc o INNER JOIN dbo.Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
	AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	AND o.Scenario = 'CLIENT' AND o.FactorSet = ISNULL(@FactorSet, o.FactorSet) AND o.Currency = ISNULL(@Currency, o.Currency) AND o.DataType = ISNULL(@DataType, o.DataType)
	GROUP BY o.FactorSet, o.Currency, o.Scenario, o.DataType
	)


