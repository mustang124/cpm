﻿CREATE  FUNCTION [dbo].[GetAverageCEI](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @FactorSet FactorSet)
RETURNS real
AS
BEGIN
	DECLARE @CEI real
	SELECT @CEI = [$(GlobalDB)].dbo.WtAvgNZ(CEI, SCE_Total*s.NumDays)
	FROM CEI2008 a INNER JOIN dbo.Submissions s ON s.SubmissionID = a.SubmissionID   
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
	AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	AND a.FactorSet = @FactorSet AND a.CEI > 0
	
	RETURN @CEI
END


