﻿CREATE TABLE [dbo].[SubmissionsAll] (
    [SubmissionID]   INT           IDENTITY (1, 1) NOT NULL,
    [RefineryID]     CHAR (6)      NOT NULL,
    [DataSet]        VARCHAR (15)  NOT NULL,
    [PeriodStart]    SMALLDATETIME NULL,
    [PeriodEnd]      SMALLDATETIME NULL,
    [RptCurrency]    VARCHAR (5)   NOT NULL,
    [RptCurrencyT15] VARCHAR (5)   NULL,
    [UOM]            VARCHAR (5)   NOT NULL,
    [Company]        NVARCHAR (30) NULL,
    [Location]       NVARCHAR (30) NULL,
    [CoordName]      NVARCHAR (50) NULL,
    [CoordTitle]     NVARCHAR (50) NULL,
    [CoordPhone]     VARCHAR (40)  NULL,
    [CoordEmail]     VARCHAR (255) NULL,
    [NumDays]        SMALLINT      NULL,
    [FractionOfYear] REAL          NULL,
    [Submitted]      SMALLDATETIME NULL,
    [LastCalc]       SMALLDATETIME NULL,
    [CalcsNeeded]    CHAR (1)      NULL,
    [PeriodYear]     SMALLINT      NULL,
    [PeriodMonth]    TINYINT       NULL,
    [BridgeVersion]  VARCHAR (10)  NULL,
    [ClientVersion]  VARCHAR (10)  NULL,
    [UseSubmission]  BIT           DEFAULT ((1)) NULL,
    CONSTRAINT [PK_Submissions] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_SubmissionsRefineryID]
    ON [dbo].[SubmissionsAll]([RefineryID] ASC);

