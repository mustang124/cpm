﻿CREATE TABLE [dbo].[MaterialCostCalc] (
    [SubmissionID] INT                   NOT NULL,
    [Scenario]     [dbo].[Scenario]      NOT NULL,
    [SortKey]      INT                   NOT NULL,
    [MaterialID]   [dbo].[MaterialID]    NULL,
    [Category]     [dbo].[YieldCategory] NULL,
    [Bbl]          FLOAT (53)            NULL,
    [PriceLocal]   REAL                  NULL,
    [PriceUS]      REAL                  NULL,
    CONSTRAINT [PK_MaterialCostCalc_1__14] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90)
);

