﻿CREATE TABLE [dbo].[Pers_LU] (
    [PersId]            [dbo].[PersId] NOT NULL,
    [Description]       CHAR (50)      NULL,
    [PersCode]          SMALLINT       NULL,
    [SectionID]         CHAR (2)       NULL,
    [IncludeForInput]   CHAR (1)       NULL,
    [MaintForceGroup]   CHAR (10)      NULL,
    [STMaintForceGroup] CHAR (10)      NULL,
    [RoutMatlCalc]      CHAR (1)       NULL,
    [ReportCategory]    VARCHAR (10)   NULL,
    [SortKey]           INT            NULL,
    [ParentID]          [dbo].[PersId] NULL,
    [Indent]            TINYINT        NULL,
    [DetailStudy]       CHAR (5)       NULL,
    [DetailProfile]     CHAR (5)       NULL,
    CONSTRAINT [persLU_key] PRIMARY KEY CLUSTERED ([PersId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [PersLUReportCat]
    ON [dbo].[Pers_LU]([ReportCategory] ASC) WITH (FILLFACTOR = 90);

