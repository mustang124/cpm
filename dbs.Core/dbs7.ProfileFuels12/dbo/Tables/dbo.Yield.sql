﻿CREATE TABLE [dbo].[Yield] (
    [SubmissionID] INT                   NOT NULL,
    [SortKey]      INT                   NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [MaterialID]   CHAR (5)              NOT NULL,
    [MaterialName] NVARCHAR (50)         CONSTRAINT [DF_Yield_MaterialName] DEFAULT ('') NOT NULL,
    [Bbl]          FLOAT (53)            CONSTRAINT [DF_Yield_BBL] DEFAULT (0) NOT NULL,
    [Gravity]      REAL                  NULL,
    [Density]      REAL                  NULL,
    [MT]           FLOAT (53)            NULL,
    [PriceLocal]   REAL                  CONSTRAINT [DF_Yield_PriceLocal] DEFAULT (0) NOT NULL,
    [PriceUS]      REAL                  NULL,
    CONSTRAINT [PK_Yield] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [YieldByCategory]
    ON [dbo].[Yield]([Category] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [YieldByMaterialID]
    ON [dbo].[Yield]([MaterialID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [YieldSubmissionIDCat]
    ON [dbo].[Yield]([SubmissionID] ASC, [Category] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [YieldSubmissionIDMat]
    ON [dbo].[Yield]([SubmissionID] ASC, [MaterialID] ASC) WITH (FILLFACTOR = 90);

