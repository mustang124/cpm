﻿CREATE TABLE [dbo].[MaterialNet] (
    [SubmissionID] INT                   NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [MaterialID]   [dbo].[MaterialID]    NOT NULL,
    [Bbl]          FLOAT (53)            NULL,
    [MT]           FLOAT (53)            NULL,
    CONSTRAINT [PK_MaterialNet] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Category] ASC, [MaterialID] ASC) WITH (FILLFACTOR = 90)
);

