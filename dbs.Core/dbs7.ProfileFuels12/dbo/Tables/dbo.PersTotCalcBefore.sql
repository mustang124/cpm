﻿CREATE TABLE [dbo].[PersTotCalcBefore] (
    [SubmissionID] INT               NOT NULL,
    [FactorSet]    [dbo].[FactorSet] NOT NULL,
    [MaintPEI]     REAL              NULL,
    [NonMaintPEI]  REAL              NULL
);

