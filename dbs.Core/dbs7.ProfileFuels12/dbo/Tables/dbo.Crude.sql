﻿CREATE TABLE [dbo].[Crude] (
    [SubmissionID] INT              NOT NULL,
    [CrudeID]      [dbo].[CrudeID]  NOT NULL,
    [CNum]         [dbo].[CrudeNum] NOT NULL,
    [CrudeName]    NVARCHAR (50)    CONSTRAINT [DF_Crude_CrudeName] DEFAULT ('') NOT NULL,
    [Bbl]          FLOAT (53)       CONSTRAINT [DF_Crude_BBL] DEFAULT (0) NOT NULL,
    [Gravity]      REAL             NULL,
    [Sulfur]       REAL             NULL,
    [CostPerBbl]   REAL             CONSTRAINT [DF_Crude_CostPerBbl] DEFAULT (0) NOT NULL,
    [Density]      REAL             NULL,
    [MT]           FLOAT (53)       NULL,
    CONSTRAINT [PK_Crude] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [CrudeID] ASC) WITH (FILLFACTOR = 90)
);

