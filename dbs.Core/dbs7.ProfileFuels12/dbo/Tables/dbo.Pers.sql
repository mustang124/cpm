﻿CREATE TABLE [dbo].[Pers] (
    [SubmissionID] INT                   NOT NULL,
    [PersId]       [dbo].[PersId]        NOT NULL,
    [SortKey]      SMALLINT              NULL,
    [SectionID]    [dbo].[PersSectionID] NULL,
    [NumPers]      REAL                  NULL,
    [STH]          REAL                  NULL,
    [OVTHours]     REAL                  NULL,
    [OVTPcnt]      REAL                  NULL,
    [Contract]     REAL                  NULL,
    [GA]           REAL                  NULL,
    [AbsHrs]       REAL                  NULL,
    [MaintPcnt]    REAL                  NULL,
    [CompEqP]      REAL                  NULL,
    [ContEqP]      REAL                  NULL,
    [GAEqP]        REAL                  NULL,
    [TotEqP]       REAL                  NULL,
    [CompWHr]      REAL                  NULL,
    [ContWHr]      REAL                  NULL,
    [GAWHr]        REAL                  NULL,
    [TotWHr]       REAL                  NULL,
    CONSTRAINT [PK_Pers_1__18] PRIMARY KEY NONCLUSTERED ([SubmissionID] ASC, [PersId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE CLUSTERED INDEX [PersSortKey]
    ON [dbo].[Pers]([SubmissionID] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90);

