﻿CREATE TABLE [dbo].[Datacheck_LU] (
    [DataCheckId]    CHAR (10)      NOT NULL,
    [Header]         VARCHAR (150)  NULL,
    [DataCheckText]  VARCHAR (4000) NOT NULL,
    [RptOrder]       SMALLINT       NOT NULL,
    [ItemDescHeader] VARCHAR (100)  NULL,
    [Value1Header]   VARCHAR (100)  NULL,
    [Value2Header]   VARCHAR (100)  NULL,
    [Value3Header]   VARCHAR (100)  NULL,
    [Value4Header]   VARCHAR (100)  NULL,
    [Value5Header]   VARCHAR (100)  NULL,
    [Value6Header]   VARCHAR (100)  NULL,
    [Value7Header]   VARCHAR (100)  NULL,
    CONSTRAINT [PK_Datacheck_LU] PRIMARY KEY CLUSTERED ([DataCheckId] ASC) WITH (FILLFACTOR = 90)
);

