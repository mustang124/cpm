﻿





CREATE       VIEW [dbo].[PersTotCalc]
AS
SELECT p.SubmissionID, f.FactorSet, o.Scenario, o.Currency,
OCCWages = CASE WHEN p.OCCNumPers > 0 THEN o.OCCSal*1000/p.OCCNumPers/s.FractionOfYear END,
MPSWages = CASE WHEN p.MPSNumPers > 0 THEN o.MPSSal*1000/p.MPSNumPers/s.FractionOfYear END,
TotWages = CASE WHEN p.TotNumPers > 0 THEN o.STSal*1000/p.TotNumPers/s.FractionOfYear END,
OCCBen = CASE WHEN p.OCCNumPers > 0 THEN o.OCCBen*1000/p.OCCNumPers/s.FractionOfYear END,
MPSBen = CASE WHEN p.MPSNumPers > 0 THEN o.MPSBen*1000/p.MPSNumPers/s.FractionOfYear END,
TotBen = CASE WHEN p.TotNumPers > 0 THEN o.STBen*1000/p.TotNumPers/s.FractionOfYear END,
OCCComp = CASE WHEN p.OCCNumPers > 0 THEN (o.OCCSal+o.OCCBen)*1000/p.OCCNumPers/s.FractionOfYear END,
MPSComp = CASE WHEN p.MPSNumPers > 0 THEN (o.MPSSal+o.MPSBen)*1000/p.MPSNumPers/s.FractionOfYear END,
TotComp = CASE WHEN p.TotNumPers > 0 THEN (o.STSal+o.STBen)*1000/p.TotNumPers/s.FractionOfYear END,
ContMaintLaborCost = CASE WHEN p.ContMaintLaborWHr > 0 THEN o.ContMaintLabor*1000/p.ContMaintLaborWHr END,
OthContLaborCost = CASE WHEN p.OthContWHr > 0 THEN o.OthCont*1000/p.OthContWHr END,
ProcessOCCMPSRatio, MaintOCCMPSRatio, AdminOCCMPSRatio,
NonMaintEqP, NonMaintEqPEdc = p.NonMaintEqP/(f.PlantEdc/100000),
OCCCompNonTASTH, MPSCompNonTASTH, TotCompNonTASTH, 
NonMaintWHr, NonMaintWHrEdc = p.NonMaintWHr/(f.PlantEdc/100*s.FractionOfYear),
TotMaintForceWHr, TotMaintForceWHrEdc = p.TotMaintForceWHr/(f.PlantEdc/100*s.FractionOfYear),
MaintForceCompWHr, MaintForceCompWHrEdc = p.MaintForceCompWHr/(f.PlantEdc/100*s.FractionOfYear),
MaintForceContWHr, MaintForceContWHrEdc = p.MaintForceContWHr/(f.PlantEdc/100*s.FractionOfYear),
TotMaintForceEqP, TotMaintForceEqPEdc = p.TotMaintForceEqP/(f.PlantEdc/100000),
MaintForceCompEqP, MaintForceCompEqPEdc = p.MaintForceCompEqP/(f.PlantEdc/100000),
MaintForceContEqP, MaintForceContEqPEdc = p.MaintForceContEqP/(f.PlantEdc/100000),
MaintForceCompPcnt, MaintForceContPcnt, 
ContMaintLaborWHr, OthContWHr, OCCNumPers, MPSNumPers, TotNumPers,
ProcessWHr, AdminWHr, CompWHr, ProcessContPcnt, MaintContPcnt, TechContPcnt, AdminContPcnt, CompOvtPcnt,
CompOCCNonMaintEqP, CompMPSNonMaintEqP, CompNonMaintEqP, ContNonMaintEqP, GANonMaintEqP, TotNonMaintEqP,
CompOCCNonMaintWHr, CompMPSNonMaintWHr, CompNonMaintWHr, ContNonMaintWHr, GANonMaintWHr, TotNonMaintWHr,
CompOCCNonTAWHr, CompMPSNonTAWHr, CompNonTAWHr, ContNonTAWHr, GANonTAWHr, TotNonTAWHr,
CompOCCNonMaintPcnt, CompMPSNonMaintPcnt, CompNonMaintPcnt, ContNonMaintPcnt, GANonMaintPcnt, TotNonMaintPcnt,
AvgNonMaintComp = CASE WHEN (p.CompOCCNonMaintEqP + p.CompMPSNonMaintEqP + p.ContNonMaintEqP) = 0 THEN NULL ELSE
		  (CASE WHEN p.OCCNumPers > 0 THEN (o.OCCSal+o.OCCBen)*1000/p.OCCNumPers END * CompOCCNonMaintEqP +
		   CASE WHEN p.MPSNumPers > 0 THEN (o.MPSSal+o.MPSBen)*1000/p.MPSNumPers END * CompMPSNonMaintEqP +
		   o.OthCont*1000) / (p.CompOCCNonMaintEqP + p.CompMPSNonMaintEqP + p.ContNonMaintEqP) END,
MaintPEI = p.TotMaintForceWHr/(f.PlantMaintPersEffDiv/100*s.FractionOfYear),
NonMaintPEI = p.NonMaintWHr/(f.PlantNonMaintPersEffDiv/100*s.FractionOfYear),
f.PlantEdc/100000 AS EqPEdcDivisor,
f.PlantEdc*s.FractionOfYear/100 AS WHrEdcDivisor,
f.PlantMaintPersEffDiv/100*s.FractionOfYear AS MaintPEIDivisor,
f.PlantNonMaintPersEffDiv/100*s.FractionOfYear AS NonMaintPEIDivisor,
NonTAMaintForceWHr, NonTAMaintForceWHrEdc = p.NonTAMaintForceWHr/(f.PlantEdc/100*s.FractionOfYear),
NonTAMaintForceCompWHr, NonTAMaintForceCompWHrEdc = p.NonTAMaintForceCompWHr/(f.PlantEdc/100*s.FractionOfYear),
NonTAMaintForceContWHr, NonTAMaintForceContWHrEdc = p.NonTAMaintForceContWHr/(f.PlantEdc/100*s.FractionOfYear),
NonTAMaintForceEqP, NonTAMaintForceEqPEdc = p.NonTAMaintForceEqP/(f.PlantEdc/100000),
NonTAMaintForceCompEqP, NonTAMaintForceCompEqPEdc = p.NonTAMaintForceCompEqP/(f.PlantEdc/100000),
NonTAMaintForceContEqP, NonTAMaintForceContEqPEdc = p.NonTAMaintForceContEqP/(f.PlantEdc/100000),
NonTAMaintPEI = p.NonTAMaintForceWHr/(f.PlantMaintPersEffDiv/100*s.FractionOfYear)
FROM PersTot p INNER JOIN FactorTotCalc f ON f.SubmissionID = p.SubmissionID
INNER JOIN OpExAll o ON o.SubmissionID = p.SubmissionID AND o.DataType = 'ADJ'
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = p.SubmissionID
WHERE f.PlantEdc>0 














