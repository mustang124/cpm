﻿

CREATE VIEW [dbo].[ProcessFactors]
AS
		SELECT fpc.SubmissionID, fpc.FactorSet, Edc, UEdc, UtilPcnt, RV, RVBbl, InServicePcnt, YearsOper, UtilOSTA, UEdcOSTA, NEOpExEffDiv, MaintEffDiv, PersEffDiv, MaintPersEffDiv, NonMaintPersEffDiv
		FROM FactorProcessCalc fpc 
		WHERE fpc.ProcessID = CASE WHEN EXISTS (SELECT * FROM FactorSets WHERE IdleUnitsInProcessResults = 'N' AND FactorSet = fpc.FactorSet) THEN 'OperProc' ELSE 'TotProc' END



