﻿
CREATE VIEW [dbo].[PersSTCalcNonTA]
AS
SELECT p.SubmissionID, p.FactorSet, p.SectionID, p.Description
	, NumPers = p.NumPers - SUM(ISNULL(pc.NumPers,0)), STH = p.STH - SUM(ISNULL(pc.STH,0)), OVTHours = p.OVTHours - SUM(ISNULL(pc.OVTHours,0)), Contract = p.Contract - SUM(ISNULL(pc.Contract,0)), GA = p.GA - SUM(ISNULL(pc.GA,0)), AbsHrs = p.AbsHrs - SUM(ISNULL(pc.AbsHrs,0))
	, CompWHr = p.CompWHr - SUM(ISNULL(pc.CompWHr,0)), ContWHr = p.ContWHr - SUM(ISNULL(pc.ContWHr,0)), GAWHr = p.GAWHr - SUM(ISNULL(pc.GAWHr,0)), TotWHr = p.TotWHr - SUM(ISNULL(pc.TotWHr,0))
	, CompWHrEdc = p.CompWHrEdc - SUM(ISNULL(pc.CompWHrEdc,0)), ContWHrEdc = p.ContWHrEdc - SUM(ISNULL(pc.ContWHrEdc,0)), GAWHrEdc = p.GAWHrEdc - SUM(ISNULL(pc.GAWHrEdc,0)), TotWHrEdc = p.TotWHrEdc - SUM(ISNULL(pc.TotWHrEdc,0))
	, TotWHrEffIndex = p.TotWHrEffIndex - SUM(ISNULL(pc.TotWHrEffIndex,0)), p.WHrEdcDivisor, p.EffDivisor
FROM PersSTCalc p LEFT JOIN ((VALUES ('OM','OCCTAADJ'),('TO','OCCTAADJ'),('MM','MPSTAADJ'),('TM','MPSTAADJ'),('TP','OCCTAADJ'),('TP','MPSTAADJ')) s(SectionID,TAPersId) 
INNER JOIN PersCalc pc ON pc.PersId = s.TAPersId) ON pc.SubmissionID = p.SubmissionID AND pc.FactorSet = p.FactorSet AND s.SectionID = p.SectionID
GROUP BY p.SubmissionID, p.FactorSet, p.SectionID, p.Description, p.NumPers, p.STH, p.OVTHours, p.OVTPcnt, p.Contract, p.GA, p.AbsHrs
	, p.CompWHr, p.CompWHrEdc, p.ContWHr, p.ContWHrEdc, p.GAWHr, p.GAWHrEdc, p.TotWHr, p.TotWHrEdc
	, p.TotWHrEffIndex, p.WHrEdcDivisor, p.EffDivisor
	

