﻿








CREATE    VIEW [dbo].[PersCalc] AS
SELECT e.SubmissionID, e.FactorSet, p.PersId, Description = (SELECT Description FROM Pers_LU WHERE Pers_LU.PersId = p.PersId), 
p.SortKey, p.SectionID, 
p.CompEqP, p.CompEqP*100000/PlantEdc AS CompEqPEdc,
p.ContEqP, p.ContEqP*100000/PlantEdc AS ContEqPEdc, 
p.GAEqP, p.GAEqP*100000/PlantEdc AS GAEqPEdc,
p.TotEqP, p.TotEqP*100000/PlantEdc AS TotEqPEdc, 
p.CompWHr, p.CompWHr*100/(PlantEdc*s.FractionOfYear) AS CompWHrEdc, 
p.ContWHr, p.ContWHr*100/(PlantEdc*s.FractionOfYear) AS ContWHrEdc,
p.GAWHr, p.GAWHr*100/(PlantEdc*s.FractionOfYear) AS GAWHrEdc, 
p.TotWHr, p.TotWHr*100/(PlantEdc*s.FractionOfYear) AS TotWHrEdc,
p.TotWHr*100/CASE WHEN e.PersEffDiv > 0 THEN e.PersEffDiv*s.FractionOfYear END AS TotWHrEffIndex,
p.TotWHr*100/CASE WHEN lu.MaintForceGroup IS NOT NULL AND e.MaintPersEffDiv > 0 THEN e.MaintPersEffDiv*s.FractionOfYear END AS MaintPersEffIndex,
p.NumPers, p.STH, p.OVTHours, p.OVTPcnt, p.Contract, p.GA, p.AbsHrs,
e.PlantEdc*s.FractionOfYear/100 AS WHrEdcDivisor,
e.PersEffDiv*s.FractionOfYear/100 AS EffDivisor,
e.MaintPersEffDiv*s.FractionOfYear/100 AS MaintPersEffDivisor
FROM Pers p INNER JOIN FactorTotCalc e ON e.SubmissionID = p.SubmissionID 
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = p.SubmissionID
INNER JOIN Pers_LU lu ON lu.PersId = p.PersId
WHERE e.PlantEdc>0


