﻿
CREATE VIEW [dbo].[Submissions]
AS
SELECT SubmissionID, RefineryID, DataSet, PeriodStart, PeriodEnd, RptCurrency, RptCurrencyT15, UOM, Company, Location,
	CoordName, CoordTitle, CoordPhone, CoordEmail, NumDays, FractionOfYear, Submitted, LastCalc, CalcsNeeded, PeriodYear, PeriodMonth,
	BridgeVersion, ClientVersion, UseSubmission
FROM dbo.SubmissionsAll WHERE UseSubmission = 1

