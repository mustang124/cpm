﻿CREATE PROC [dbo].[SS_GetInputMaintTA_Other]
	@RefineryID nvarchar(10)
AS

SELECT mt.TAID,mt.UnitID,P.SortKey,RTRIM(P.ProcessID) AS ProcessID,mt.TADate,mt.TAHrsDown, 
            mt.TACostLocal,mt.TAExpLocal, mt.TACptlLocal, mt.TAOvhdLocal, mt.TALaborCostLocal, mt.TAOCCSTH,mt.TAOCCOVT,mt.TAMPSSTH,mt.TampSovtPcnt, 
            RTRIM(P.Description) as UnitName,mt.TAContOCC,mt.TAContMPS,mt.PrevTADate, 
            mt.TAExceptions  
            FROM dbo.MaintTA mt, ProcessID_LU P WHERE  mt.RefineryID=@RefineryID AND  
            mt.ProcessID = P.ProcessID AND ProfileProcFacility = 'N' AND MaintDetails = 'Y'
