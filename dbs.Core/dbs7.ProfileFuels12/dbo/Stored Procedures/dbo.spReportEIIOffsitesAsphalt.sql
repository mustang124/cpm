﻿


CREATE    PROC [dbo].[spReportEIIOffsitesAsphalt] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
DECLARE @OffsitesEnergyConstant decimal(5,0), @OffsitesEnergyCmplxFactor decimal(5,1), @OffsitesFormula varchar(20), @ASPEIIFormula varchar(255)
SELECT @OffsitesEnergyConstant = OffsitesEnergyConstant, @OffsitesEnergyCmplxFactor = OffsitesEnergyCmplxFactor
FROM FactorSets WHERE FactorSet = @FactorSet AND RefineryType = 'FUELS'
SELECT @OffsitesFormula = CONVERT(varchar(6), @OffsitesEnergyConstant) + '+' + CONVERT(varchar(6), @OffsitesEnergyCmplxFactor) + '*(A)'
SELECT @ASPEIIFormula = EIIFormula FROM Factors WHERE ProcessID = 'ASP' AND FactorSet = @FactorSet

SELECT s.SubmissionID, s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, 
AspUtilCap, AspStdEnergy, OffsitesUtilCap, OffsitesStdEnergy, Complexity
INTO #Monthly
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE FactorSet=@FactorSet
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1

SELECT s1.SubmissionID, 
ASPUtilCap_Ytd = SUM(fc.AspUtilCap*s2.NumDays)/SUM(s2.NumDays), EstASPMBTU_Ytd = SUM(fc.AspStdEnergy*s2.NumDays), AspStdEnergy_Ytd = SUM(fc.AspStdEnergy*s2.NumDays)/SUM(s2.NumDays),
OffsitesUtilCap_Ytd = SUM(fc.OffsitesUtilCap*s2.NumDays)/SUM(s2.NumDays), EstOffsitesMBTU_Ytd = SUM(fc.OffsitesStdEnergy*s2.NumDays), OffsitesStdEnergy_Ytd = SUM(fc.OffsitesStdEnergy*s2.NumDays)/SUM(s2.NumDays)
INTO #YTD
FROM Submissions s1 INNER JOIN Submissions s2 ON s2.RefineryID = s1.RefineryID AND s2.DataSet = s1.DataSet AND s2.PeriodYear = s1.PeriodYear AND s2.PeriodMonth <= s1.PeriodMonth
INNER JOIN FactorTotCalc fc ON fc.SubmissionID = s2.SubmissionID 
WHERE fc.FactorSet=@FactorSet 
And s1.RefineryID = @RefineryID AND s1.DataSet=@DataSet AND s1.PeriodYear = ISNULL(@PeriodYear, s1.PeriodYear) AND s1.PeriodMonth = ISNULL(@PeriodMonth, s1.PeriodMonth) AND s1.UseSubmission = 1 AND s2.UseSubmission = 1
GROUP BY s1.SubmissionID

SET NOCOUNT OFF

SELECT m.Location, m.PeriodStart, m.PeriodEnd, m.DaysInPeriod, Currency= @Currency, UOM = @UOM, 
m.AspUtilCap, m.AspStdEnergy, ASPEIIFormula = @ASPEIIFormula,
m.OffsitesUtilCap, m.OffsitesStdEnergy, @OffsitesFormula As OffsitesFormula, m.Complexity,
y.ASPUtilCap_Ytd, y.EstASPMBTU_Ytd, y.AspStdEnergy_Ytd, y.OffsitesUtilCap_Ytd, y.EstOffsitesMBTU_Ytd, y.OffsitesStdEnergy_Ytd
FROM #Monthly m INNER JOIN #YTD y ON y.SubmissionID = m.SubmissionID
ORDER BY m.PeriodStart DESC

DROP TABLE #Monthly
DROP TABLE #YTD


