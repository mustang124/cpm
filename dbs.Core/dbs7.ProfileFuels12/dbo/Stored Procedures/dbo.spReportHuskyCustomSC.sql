﻿CREATE PROC [dbo].[spReportHuskyCustomSC] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE	
	@EII real, @EII_Ytd real, @EII_Avg real, 
	@EnergyUseDay real, @EnergyUseDay_Ytd real, @EnergyUseDay_Avg real, 
	@TotStdEnergy real, @TotStdEnergy_Ytd real, @TotStdEnergy_Avg real, 

	@RefUtilPcnt real, @RefUtilPcnt_Ytd real, @RefUtilPcnt_Avg real, 
	@Edc real, @Edc_Ytd real, @Edc_Avg real, 
/*	
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_Avg real, 
	@TotProcessEdc real, @TotProcessEdc_QTR real, @TotProcessEdc_Avg real, 
	@TotProcessUEdc real, @TotProcessUEdc_QTR real, @TotProcessUedc_Avg real, 
*/	
	@MechAvail real, @MechAvail_Ytd real, @MechAvail_Avg real, 
	@MechUnavailTA real, @MechUnavailTA_Ytd real, @MechUnavailTA_Avg real, 
	@NonTAUnavail real, @NonTAUnavail_Ytd real, @NonTAUnavail_Avg real, 

	@PersIndex real, @PersIndex_Ytd real, @PersIndex_Avg real, 
	@CompWHr real, @CompWHr_Ytd real, @CompWHr_Avg real,
	@ContWHr real, @ContWHr_Ytd real, @ContWHr_Avg real,

	@MaintIndex real, @MaintIndex_Ytd real, @MaintIndex_Avg real, 
	@TAAdj real, @TAAdj_Ytd real, @TAAdj_Avg real,
	@AnnTACost real, @AnnTACost_Ytd real, @AnnTACost_Avg real, 
	@RoutCost real, @RoutCost_Ytd real, @RoutCost_Avg real, 

	@NEOpExUEdc real, @NEOpExUEdc_Ytd real, @NEOpExUedc_Avg real, 
	@NEOpEx real, @NEOpEx_Ytd real, @NEOpEx_Avg real, 

	@OpExUEdc real, @OpExUEdc_Ytd real, @OpExUedc_Avg real,
	@EnergyCost real, @EnergyCost_Ytd real, @EnergyCost_Avg real, 
	@TotCashOpEx real, @TotCashOpEx_Ytd real, @TotCashOpEx_Avg real,
	@UEdc real, @UEdc_Ytd real, @Uedc_Avg real
DECLARE @EII_Target real, @RefUtilPcnt_Target real, @MechAvail_Target real, @MaintIndex_Target real, @PersIndex_Target real, @NEOpExUEdc_Target real, @OpExUEdc_Target real
	
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)

SELECT @EII = NULL, @EII_Ytd = NULL, @EII_Avg = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_Ytd = NULL, @EnergyUseDay_Avg = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_Ytd = NULL, @TotStdEnergy_Avg = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_Ytd = NULL, @RefUtilPcnt_Avg = NULL, 
	@Edc = NULL, @Edc_Ytd = NULL, @Edc_Avg = NULL, 
	@UEdc = NULL, @UEdc_Ytd = NULL, @Uedc_Avg = NULL, 
	@MechAvail = NULL, @MechAvail_Ytd = NULL, @MechAvail_Avg = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_Ytd = NULL, @MechUnavailTA_Avg = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_Ytd = NULL, @NonTAUnavail_Avg = NULL, 
	@RoutCost = NULL, @RoutCost_Ytd = NULL, @RoutCost_Avg = NULL, 
	@PersIndex = NULL, @PersIndex_Ytd = NULL, @PersIndex_Avg = NULL, 
	@CompWHr = NULL, @CompWHr_Ytd = NULL, @CompWHr_Avg = NULL,
	@ContWHr = NULL, @ContWHr_Ytd = NULL, @ContWHr_Avg = NULL,
	@NEOpExUEdc = NULL, @NEOpExUEdc_Ytd = NULL, @NEOpExUedc_Avg = NULL, 
	@NEOpEx = NULL, @NEOpEx_Ytd = NULL, @NEOpEx_Avg = NULL, 
	@OpExUEdc = NULL, @OpExUEdc_Ytd = NULL, @OpExUedc_Avg = NULL, 
	@TAAdj = NULL, @TAAdj_Ytd = NULL, @TAAdj_Avg = NULL,
	@EnergyCost = NULL, @EnergyCost_Ytd = NULL, @EnergyCost_Avg = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_Ytd = NULL, @TotCashOpEx_Avg = NULL,
	@EII_Target = NULL, @RefUtilPcnt_Target = NULL, @MechAvail_Target = NULL, @MaintIndex_Target = NULL, @PersIndex_Target = NULL, @NEOpExUEdc_Target = NULL, @OpExUEdc_Target = NULL
	
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
DECLARE @NumDays24Mo real, @NumDays12Mo real
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)

--- Everything Already Available in GenSum
SELECT	@RefUtilPcnt = UtilPcnt, @RefUtilPcnt_Ytd = UtilPcnt_Ytd, @RefUtilPcnt_Avg = UtilPcnt_Avg,
	@MechAvail = MechAvail, @MechAvail_Ytd = MechAvail_Ytd, @MechAvail_Avg = MechAvail_Avg,
	@EII = EII, @EII_Ytd = EII_Ytd, @EII_Avg = EII_Avg,
	@PersIndex = TotWHrEdc, @PersIndex_Ytd = TotWhrEdc_Ytd, @PersIndex_Avg = TotWhrEdc_Avg,
	@Edc = Edc/1000, @Edc_Ytd = Edc_Ytd/1000, @Edc_Avg = Edc_Avg/1000,
	@UEdc = UEdc/1000, @UEdc_Ytd = UEdc_Ytd/1000, @Uedc_Avg = Uedc_Avg/1000,
	@MaintIndex = RoutIndex + TAIndex_Avg, @MaintIndex_Ytd = MaintIndex_Ytd, @MaintIndex_Avg = MaintIndex_Avg,
	@NEOpExUEdc = NEOpExUEdc, @NEOpExUEdc_Ytd = NEOpExUEdc_Ytd, @NEOpExUedc_Avg = NEOpExUedc_Avg,
	@EII_Target = EII_Target, @RefUtilPcnt_Target = UtilPcnt_Target, @MechAvail_Target = MechAvail_Target, @MaintIndex_Target = MaintIndex_Target, 
	@PersIndex_Target = TotWHrEdc_Target, @NEOpExUEdc_Target = NEOpExUEdc_Target, 
	@OpExUEdc = TotCashOpExUEdc, @OpExUEdc_Ytd = TotCashOpExUEdc_Ytd, @OpExUedc_Avg = TotCashOpExUedc_Avg, @OpExUEdc_Target = TotCashOpExUEdc_Target
FROM GenSum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_Ytd = MechUnavailTA_Ann, @MechUnavailTA_Avg = MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet
/*
SELECT @MechUnavailTA_Ytd=SUM(MechUnavailTA_Ann*f.TotProcessEdc*s.FractionOfYear)/SUM(f.TotProcessEdc*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet
*/
SELECT @MechUnavailTA_Avg=SUM(MechUnavailTA_Ann*f.TotProcessEdc*s.FractionOfYear)/SUM(f.TotProcessEdc*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail = 100 - @MechAvail - @MechUnavailTA, @NonTAUnavail_Ytd = 100 - @MechAvail_Ytd - @MechUnavailTA_Ytd, @NonTAUnavail_Avg = 100 - @MechAvail_Avg - @MechUnavailTA_Avg
IF @NonTAUnavail < 0.05
	SET @NonTAUnavail = 0
IF @NonTAUnavail_Ytd < 0.05
	SET @NonTAUnavail_Ytd = 0
IF @NonTAUnavail_Avg < 0.05
	SET @NonTAUnavail_Avg = 0
	
SELECT @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet
/*
SELECT @EnergyUseDay_Ytd = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_Ytd = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet
*/
SELECT @EnergyUseDay_Avg = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_Avg = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@NumDays12Mo = SUM(s.NumDays)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @CompWHr = CompWHr+GAWHr, @ContWHr = ContWHr
FROM PersST WHERE SubmissionID = @SubmissionID AND SectionID = 'TP'
/*
SELECT @CompWHr_Ytd = SUM(CompWHr+GAWHr), @ContWHr_Ytd = SUM(ContWHr)
FROM PersST p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.SectionID = 'TP'
*/
SELECT @CompWHr_Avg = SUM(CompWHr+GAWHr), @ContWHr_Avg = SUM(ContWHr)
FROM PersST p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND p.SectionID = 'TP'

SELECT @AnnTACost = AllocAnnTACost, @RoutCost = CurrRoutCost
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency
/*
SELECT @AnnTACost_Ytd = SUM(AllocAnnTACost), @RoutCost_Ytd = SUM(CurrRoutCost)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency
*/
SELECT @RoutCost_Avg = SUM(CurrRoutCost), @NumDays24Mo = SUM(s.NumDays)
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency

SELECT @NEOpEx = NEOpEx*Divisor, @EnergyCost = EnergyCost*Divisor, @TotCashOpEx = TotCashOpEx*Divisor, @TAAdj = TAAdj*Divisor
FROM OpExCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'Edc'

/*
SELECT @NEOpEx_Ytd = SUM(NEOpEx*Divisor), @EnergyCost_Ytd = SUM(EnergyCost*Divisor)
	, @TotCashOpEx_Ytd = SUM(TotCashOpEx*Divisor), @TAAdj_Ytd = SUM(TAAdj*Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'Edc'

SELECT @OpExUEdc_Ytd = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEdc'
*/
SELECT @NEOpEx_Avg = SUM(NEOpEx*Divisor), @EnergyCost_Avg = SUM(EnergyCost*Divisor), @TAAdj_Avg = SUM(TAAdj*Divisor)
	, @TotCashOpEx_Avg = SUM(TotCashOpEx*Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'Edc'
/*
SELECT @OpExUedc_Avg = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEdc'
*/
IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_Ytd = @EnergyUseDay_Ytd * 1.055, @EnergyUseDay_Avg = @EnergyUseDay_Avg * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_Ytd = @TotStdEnergy_Ytd * 1.055, @TotStdEnergy_Avg = @TotStdEnergy_Avg * 1.055

SELECT 
	@AnnTACost = @AnnTACost/365.25, 
	@RoutCost = @RoutCost/@NumDays, @RoutCost_Avg = @RoutCost_Avg/@NumDays24Mo, 
	@CompWHr = @CompWHr/@NumDays, @CompWHr_Avg = @CompWHr_Avg/@NumDays12Mo, 
	@ContWHr = @ContWHr/@NumDays, @ContWHr_Avg = @ContWHr_Avg/@NumDays12Mo, 
	@NEOpEx = @NEOpEx/@NumDays, @NEOpEx_Avg = @NEOpEx_Avg/@NumDays12Mo, 
	@EnergyCost = @EnergyCost/@NumDays, @EnergyCost_Avg = @EnergyCost_Avg/@NumDays12Mo, 
	@TotCashOpEx = @TotCashOpEx/@NumDays, @TotCashOpEx_Avg = @TotCashOpEx_Avg/@NumDays12Mo
SELECT @AnnTACost_Avg = @AnnTACost

DECLARE @EII_Study real, @EII_StudyP1 real, @EnergyUseDay_Study real, @EnergyUseDay_StudyP1 real, @TotStdEnergy_Study real, @TotStdEnergy_StudyP1 real
DECLARE @RefUtilPcnt_Study real, @RefUtilPcnt_StudyP1 real, @Edc_Study real, @Edc_StudyP1 real, @UtilUEdc_Study real, @UtilUEdc_StudyP1 real
DECLARE @MechAvail_Study real, @MechAvail_StudyP1 real, @MechUnavailTA_Study real, @MechUnavailTA_StudyP1 real, @NonTAUnavail_Study real, @NonTAUnavail_StudyP1 real
DECLARE @MaintIndex_Study real, @MaintIndex_StudyP1 real, @TAAdj_Study real, @TAAdj_StudyP1 real, @RoutCost_Study real, @RoutCost_StudyP1 real
DECLARE @PersIndex_Study real, @PersIndex_StudyP1 real, @CompWHr_Study real, @CompWHr_StudyP1 real, @ContWHr_Study real, @ContWHr_StudyP1 real
DECLARE @NEOpExUEdc_Study real, @NEOpExUEdc_StudyP1 real, @NEOpEx_Study real, @NEOpEx_StudyP1 real, @UEdc_Study real, @UEdc_StudyP1 real
DECLARE @OpExUEdc_Study real, @OpExUEdc_StudyP1 real, @EnergyCost_Study real, @EnergyCost_StudyP1 real, @TotCashOpEx_Study real, @TotCashOpEx_StudyP1 real

-- Get study values
SELECT @EII_Study = StudyValue, @EII_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EII'
SELECT @EnergyUseDay_Study = StudyValue, @EnergyUseDay_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EnergyUseDay'
SELECT @TotStdEnergy_Study = StudyValue, @TotStdEnergy_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotStdEnergy'
SELECT @RefUtilPcnt_Study = StudyValue, @RefUtilPcnt_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'RefUtilPcnt'
SELECT @Edc_Study = StudyValue, @Edc_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'Edc'
SELECT @UtilUEdc_Study = StudyValue, @UtilUEdc_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'UEdc_Util'
SELECT @MechAvail_Study = StudyValue, @MechAvail_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MechAvail'
SELECT @MechUnavailTA_Study = StudyValue, @MechUnavailTA_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MechUnavailTA'
SELECT @NonTAUnavail_Study = 100 - @MechAvail_Study - @MechUnavailTA_Study, @NonTAUnavail_StudyP1 = 100 - @MechAvail_StudyP1 - @MechUnavailTA_StudyP1
SELECT @MaintIndex_Study = StudyValue, @MaintIndex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'MaintIndex'
SELECT @TAAdj_Study = StudyValue, @TAAdj_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'AnnTACostK$/Day'
SELECT @RoutCost_Study = StudyValue, @RoutCost_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'AnnRoutCostK$/Day'
SELECT @PersIndex_Study = StudyValue, @PersIndex_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'PersIndex'
SELECT @CompWHr_Study = StudyValue, @CompWHr_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'CompWHr/Day'
SELECT @ContWHr_Study = StudyValue, @ContWHr_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'ContWHr/Day'
SELECT @NEOpExUEdc_Study = StudyValue, @NEOpExUEdc_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'NEOpExUEdc'
SELECT @NEOpEx_Study = StudyValue, @NEOpEx_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'NEOpExK$/Day'
SELECT @UEdc_Study = StudyValue, @UEdc_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'UEdc_OpEx'
SELECT @OpExUEdc_Study = StudyValue, @OpExUEdc_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotCashOpExUEdc'
SELECT @EnergyCost_Study = StudyValue, @EnergyCost_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'EnergyCostK$/Day'
SELECT @TotCashOpEx_Study = StudyValue, @TotCashOpEx_StudyP1 = StudyP1Value FROM StudyValues WHERE RefineryID = @RefineryID AND @PeriodStart BETWEEN EffDate AND EffUntil AND Property = 'TotCashOpExK$/Day'

SELECT 
	EII = @EII, EII_Avg = @EII_Avg, EII_Study = @EII_Study, EII_StudyP1 = @EII_StudyP1, EII_Target = @EII_Target,
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_Avg = @EnergyUseDay_Avg, EnergyUseDay_Study = @EnergyUseDay_Study, EnergyUseDay_StudyP1 = @EnergyUseDay_StudyP1, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_Avg = @TotStdEnergy_Avg, TotStdEnergy_Study = @TotStdEnergy_Study, TotStdEnergy_StudyP1 = @TotStdEnergy_StudyP1, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_Avg = @RefUtilPcnt_Avg, UtilPcnt_Study = @RefUtilPcnt_Study, UtilPcnt_StudyP1 = @RefUtilPcnt_StudyP1, UtilPcnt_Target = @RefUtilPcnt_Target,
	Edc = @Edc, Edc_Avg = @Edc_Avg, Edc_Study = @Edc_Study, Edc_StudyP1 = @Edc_StudyP1,
	UtilUEdc = @Edc*@RefUtilPcnt/100, UtilUedc_Avg = @Edc_Avg*@RefUtilPcnt_Avg/100, UtilUEdc_Study = @UtilUEdc_Study, UtilUEdc_StudyP1 = @UtilUEdc_StudyP1,
	
	MechAvail = @MechAvail, MechAvail_Avg = @MechAvail_Avg, MechAvail_Study = @MechAvail_Study, MechAvail_StudyP1 = @MechAvail_StudyP1, MechAvail_Target = @MechAvail_Target,
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_Avg = @MechUnavailTA_Avg, MechUnavailTA_Study = @MechUnavailTA_Study, MechUnavailTA_StudyP1 = @MechUnavailTA_StudyP1,
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_Avg = @NonTAUnavail_Avg, NonTAUnavail_Study = @NonTAUnavail_Study, NonTAUnavail_StudyP1 = @NonTAUnavail_StudyP1,

	MaintIndex = @MaintIndex, MaintIndex_Avg = @MaintIndex_Avg, MaintIndex_Study = @MaintIndex_Study, MaintIndex_StudyP1 = @MaintIndex_StudyP1, MaintIndex_Target = @MaintIndex_Target,
	TAAdj = @AnnTACost, TAAdj_Avg = @AnnTACost_Avg, TAAdj_Study = @TAAdj_Study, TAAdj_StudyP1 = @TAAdj_StudyP1,
	RoutCost = @RoutCost, RoutCost_Avg = @RoutCost_Avg, RoutCost_Study = @RoutCost_Study, RoutCost_StudyP1 = @RoutCost_StudyP1,

	TotWHrEdc = @PersIndex, TotWhrEdc_Avg = @PersIndex_Avg, TotWHrEdc_Study = @PersIndex_Study, TotWHrEdc_StudyP1 = @PersIndex_StudyP1, TotWHrEdc_Target = @PersIndex_Target,
	CompWHr = @CompWHr, CompWHr_Avg = @CompWHr_Avg, CompWHr_Study = @CompWHr_Study, CompWHr_StudyP1 = @CompWHr_StudyP1,
	ContWHr = @ContWHr, ContWHr_Avg = @ContWHr_Avg, ContWHr_Study = @ContWHr_Study, ContWHr_StudyP1 = @ContWHr_StudyP1,

	NEOpExUEdc = @NEOpExUEdc, NEOpExUedc_Avg = @NEOpExUedc_Avg, NEOpExUEdc_Study = @NEOpExUEdc_Study, NEOpExUEdc_StudyP1 = @NEOpExUEdc_StudyP1, NEOpExUEdc_Target = @NEOpExUEdc_Target,
	NEOpEx = @NEOpEx, NEOpEx_Avg = @NEOpEx_Avg, NEOpEx_Study = @NEOpEx_Study, NEOpEx_StudyP1 = @NEOpEx_StudyP1,
	UEdc = @UEdc,  Uedc_Avg = @Uedc_Avg, UEdc_Study = @UEdc_Study, UEdc_StudyP1 = @UEdc_StudyP1,

	TotCashOpExUEdc = @OpExUEdc, TotCashOpExUedc_Avg = @OpExUedc_Avg, TotCashOpExUEdc_Study = @OpExUEdc_Study, TotCashOpExUEdc_StudyP1 = @OpExUEdc_StudyP1, TotCashOpExUEdc_Target = @OpExUEdc_Target,
	EnergyCost = @EnergyCost, EnergyCost_Avg = @EnergyCost_Avg, EnergyCost_Study = @EnergyCost_Study, EnergyCost_StudyP1 = @EnergyCost_StudyP1,
	TotCashOpEx = @TotCashOpEx, TotCashOpEx_Avg = @TotCashOpEx_Avg, TotCashOpEx_Study = @TotCashOpEx_Study, TotCashOpEx_StudyP1 = @TotCashOpEx_StudyP1

