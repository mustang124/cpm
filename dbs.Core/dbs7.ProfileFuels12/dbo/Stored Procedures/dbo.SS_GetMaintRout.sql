﻿CREATE PROC [dbo].[SS_GetMaintRout]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@DataSet nvarchar(20)='ACTUAL'
	
AS

SELECT mr.UnitID,RTRIM(mr.ProcessID) AS ProcessID,mr.RoutCostLocal,mr.RoutExpLocal,mr.RoutCptlLocal,mr.RoutOvhdLocal,
            mr.RegNum,mr.MaintNum,mr.OthNum,mr.OthDownEconomic,mr.OthDownExternal,
            mr.OthDownUnitUpsets,mr.OthDownOffsiteUpsets,
            mr.RegDown,mr.MaintDown,mr.OthDown,
            cfg.SortKey,ISNULL(RTRIM(cfg.UnitName), RTRIM(lu.Description)) AS UnitName
             FROM dbo.MaintRout mr LEFT JOIN dbo.Config cfg ON mr.SubmissionID = cfg.SubmissionID AND mr.UnitID=cfg.UnitID 
             LEFT JOIN dbo.ProcessID_LU lu ON lu.ProcessID = mr.ProcessID 
              WHERE  mr.SubmissionID IN (SELECT DISTINCT SubmissionID FROM dbo.Submissions
             WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1
             AND (PeriodStart BETWEEN @PeriodStart AND 
            (DateAdd(Day, -1, @PeriodEnd))))

