﻿CREATE      PROC [dbo].[spReportOpEx] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod, 
	OpEx.Currency, DataType, OCCSalary = OCCSal, MPSSalary = MPSSal, OCCBenefits = OCCBen, MPSBenefits = MPSBen, MaintMatl, ContMaintLabor, ContMaintInspect, ContMaintLaborST, ContMaintMatl, OpEx.MaintMatlST,
	OthCont, EquipMaint, EquipNonMaint, Equip = ISNULL(EquipMaint,0)+ISNULL(EquipNonMaint,0), Tax, 
	InsurBI, InsurPC, InsurOth, Insur = ISNULL(InsurBI,0)+ISNULL(InsurPC,0)+ISNULL(InsurOth,0), TAAdj, Envir, OthNonVol, GAPers, STNonVol, 
	ChemicalsAntiknock AS Antiknock, Chemicals, Catalysts, Royalties, PurElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth, EmissionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol, TotCashOpEx, 
	GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, FireSafetyLoss = ExclFireSafety, EnvirFines = ExclEnvirFines, ExclOth, TotExpExcl, 
	STSal, STBen, PersCostExclTA, PersCost, EnergyCost, NEOpEx,
(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS PurchasedEnergy,
(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS ProducedEnergy
FROM OpExAll OpEx INNER JOIN Submissions s ON s.SubmissionID = OpEx.SubmissionID
WHERE DataType='ADJ' AND OpEx.Currency=@Currency
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1

UNION ALL

SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod, 
	oc.Currency, DataType, OCCSalary = OCCSal, MPSSalary = MPSSal, OCCBenefits = OCCBen, MPSBenefits = MPSBen, MaintMatl, ContMaintLabor, ContMaintInspect, ContMaintLaborST, ContMaintMatl, MaintMatlST, 
	OthCont, EquipMaint, EquipNonMaint, Equip, Tax, InsurBI, InsurPC, InsurOth, Insur, TAAdj, Envir, OthNonVol, GAPers, STNonVol, 
	Antiknock, Chemicals, Catalysts, Royalties, PurElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth, EmissionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol, TotCashOpEx, 
	GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, FireSafetyLoss, EnvirFines, ExclOth, TotExpExcl, 
	STSal, STBen, PersCostExclTA, PersCost, EnergyCost, NEOpEx,
(ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurLiquid,0)+ISNULL(PurSolid,0)) AS PurchasedEnergy,
(ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0)) AS ProducedEnergy
FROM OpExCalc oc INNER JOIN Submissions s ON s.SubmissionID = oc.SubmissionID
WHERE oc.DataType IN ('C/Bbl','UEdc') AND oc.FactorSet=@FactorSet AND oc.Currency=@Currency
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
ORDER BY s.PeriodStart DESC


