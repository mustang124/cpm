﻿

CREATE      PROC [dbo].[spReportCEI] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod, 
	c.CEI, c.AE_Total, c.ACE_Total, c.SE_Total, c.SCE_Total, c.AE_EII_Total, c.SE_EII_Total
	, c.AE_Fungible, c.ACE_Fungible, c.AE_NG, c.ACE_NG, c.AE_FG, c.ACE_FG, c.SE_FG, c.SCE_FG
	, c.AE_LBG, c.ACE_LBG, c.SE_COKLBG, c.SCE_COKLBG, c.SE_U73POX, c.SCE_U73POX, c.SE_POX, c.SCE_POX
	, c.AE_MC, c.ACE_MC, c.SE_CALCIN, c.SCE_CALCIN, c.AE_NMC, c.ACE_NMC, c.SE_FCFXCoke, c.SCE_FCFXCoke
	, c.AE_CaC, c.ACE_CaC, c.AE_COC, c.ACE_COC, c.SE_COC, c.SCE_COC
	, c.AE_StmPur, c.ACE_StmPur, c.AE_StmTransIn, c.ACE_StmTransIn
	, c.AE_PurElec, c.ACE_PurElec, c.SE_PurElec, c.SCE_PurElec, c.AE_ProdElecAdj, c.ACE_ElecTransIn
	, c.ACE_H2_SMR, c.SCE_H2_SMR, c.ACE_H2_POX, c.SCE_H2_POX, c.SCE_MEOH, c.ACE_H2Prod, c.SCE_H2Prod
	, c.ACE_H2Pur, c.ACE_H2TransIn, c.ACE_H2TransOut, c.ACE_H2Sales, c.ACE_H2Deficit
	, c.ACE_ASP_CO2, c.SCE_ASP_CO2, c.ACE_FlareLoss, c.SCE_FlareLoss, c.ACE_CO2Sales, c.SCE_CO2Sales, c.ACE_OthProcessCO2, c.SCE_OthProcessCO2
	, c.SCE_CH4, c.ACE_N2O, c.SCE_N2O, c.ACE_TotalCO2_Energy, c.SCE_TotalCO2_Energy, c.ACE_TotalCO2, c.SCE_TotalCO2, c.CEI_Avg, c.CEI_Ytd
FROM CEI2008 c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
WHERE c.FactorSet = @FactorSet
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) 
AND s.UseSubmission = 1
