﻿CREATE PROC [dbo].[SS_GetInputPers]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT  
s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            RTRIM(PersId) AS PersId,NumPers,AbsHrs,STH,OVTHours,OVTPcnt,Contract,GA,MaintPcnt 
            FROM  
            dbo.Pers p 
            ,dbo.Submissions s  
            WHERE   
            p.SubmissionID = s.SubmissionID AND 
            (p.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions 
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1)
            AND PersId NOT IN ('OCCTAADJ','MPSTAADJ','OCCTAEXCL','MPSTAEXCL','OCCTAEXC','MPSTAEXC'))

