﻿CREATE PROCEDURE [dbo].[DS_InventorySchema]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS
BEGIN
	SELECT RTRIM(TankType) as TankType, NumTank, FuelsStorage, AvgLevel FROM Inventory i 
	WHERE SubmissionID=(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet ORDER BY PeriodStart DESC)
END

