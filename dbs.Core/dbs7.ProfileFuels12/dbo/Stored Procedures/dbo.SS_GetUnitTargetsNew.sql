﻿CREATE PROC [dbo].[SS_GetUnitTargetsNew]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@DataSet nvarchar(20)='ACTUAL'
	
AS

SELECT ut.UnitID,c.UnitName,c.ProcessID,ut.Property,ut.Target,ut.CurrencyCode,
             ul.USDescription, ul.MetDescription, ul.USDecPlaces, ul.MetDecPlaces,ul.SortKey 
				FROM UnitTargetsNew ut,UnitTargets_LU ul, Config c 
				WHERE ut.SubmissionID = c.SubmissionID AND ut.UnitID = c.UnitID AND ul.Property = ut.Property 
					AND (ul.ProcessID='ALL' OR ul.ProcessID=c.ProcessID) 
					AND c.SubmissionID IN 
						(SELECT DISTINCT SubmissionID FROM dbo.Submissions
										WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1
											AND (PeriodStart BETWEEN @PeriodStart AND 
												DateAdd(Day, -1, @PeriodEnd))) 
			ORDER BY C.SortKey

