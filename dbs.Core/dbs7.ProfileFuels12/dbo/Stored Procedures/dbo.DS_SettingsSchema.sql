﻿CREATE PROCEDURE [dbo].[DS_SettingsSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(S.Company) AS Company, RTRIM(S.Location) AS Location, RTRIM(S.CoordName) AS CoordName, RTRIM(S.CoordTitle) AS CoordTitle, 
                  RTRIM(S.CoordPhone) AS CoordPhone, RTRIM(S.CoordEmail) AS CoordEmail, RTRIM(S.RptCurrency) AS RptCurrency,RTRIM(S.RptCurrencyT15) AS RptCurrencyT15, (CASE RTRIM(UOM) 
                  WHEN 'US' THEN 'US Units' WHEN 'MET' THEN 'Metric' END) AS UOM, '  ' AS BridgeLocation, T.FuelsLubesCombo, 
                  PeriodYear, PeriodMonth,DataSet,BridgeVersion,ClientVersion  
                  FROM dbo.Submissions S ,
                  dbo.TSort T WHERE S.RefineryID = T.RefineryID AND S.RefineryID=@RefineryID AND S.SubmissionID IN 
                  (SELECT Top 1 SubmissionID FROM  Submissions WHERE RefineryID=@RefineryID ORDER BY PeriodStart DESC)               
END

