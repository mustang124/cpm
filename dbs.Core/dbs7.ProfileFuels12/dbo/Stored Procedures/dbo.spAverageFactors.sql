﻿CREATE      PROC [dbo].[spAverageFactors](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @EII real OUTPUT, @VEI real OUTPUT, @UtilPcnt real OUTPUT, @UtilOSTA real OUTPUT, @Edc float OUTPUT, @UEdc float OUTPUT,
	@ProcessUtilPcnt real OUTPUT, @TotProcessEdc float = NULL OUTPUT, @TotProcessUEdc float = NULL OUTPUT)
AS
SELECT @EII = 100*CASE WHEN SUM(a.TotStdEnergy*s.NumDays) > 0 THEN SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) ELSE NULL END,
@VEI = 100*SUM(CASE WHEN VEI IS NOT NULL THEN ReportLossGain END)/SUM(CASE WHEN VEI IS NOT NULL THEN EstGain END),
@UtilPcnt = CASE WHEN SUM(Edc) > 0 THEN SUM(UtilPcnt*Edc*s.NumDays)/SUM(Edc*s.NumDays) ELSE NULL END,
@ProcessUtilPcnt = 100*CASE WHEN SUM(TotProcessEdc) > 0 THEN SUM(TotProcessUEdc*s.NumDays)/SUM(TotProcessEdc*s.NumDays) ELSE NULL END,
@UtilOSTA = CASE WHEN SUM(TotProcessEdc) > 0 THEN SUM(UtilOSTA*TotProcessEdc*s.NumDays)/SUM(TotProcessEdc*s.NumDays) ELSE NULL END,
@Edc = SUM(Edc*s.NumDays)/SUM(s.NumDays), @UEdc = SUM(UEdc*s.NumDays)/SUM(s.NumDays),
@TotProcessEdc = SUM(TotProcessEdc*s.NumDays)/SUM(s.NumDays), @TotProcessUEdc = SUM(TotProcessUEdc*s.NumDays)/SUM(s.NumDays)
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND a.FactorSet = @FactorSet

