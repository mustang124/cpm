﻿CREATE   PROC [dbo].[spReportProcessDataSA] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT p.SubmissionID, p.UnitID, p.Property, p.SAValue, t.FormulaSymbol, t.USDescription, t.USDecPlaces, USDescriptionRussian = ISNULL(t.USDescriptionRussian, t.USDescription)
FROM ProcessData p INNER JOIN Config c ON c.SubmissionID = p.SubmissionID AND p.UnitID = c.UnitID 
INNER JOIN Table2_LU t ON p.Property = t.Property AND c.ProcessID = t.ProcessID
INNER JOIN Factors f ON f.ProcessID = c.ProcessID AND f.ProcessType = c.ProcessType AND f.FactorSet=@FactorSet AND f.CapType IN ('E','')
WHERE c.SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet))
AND t.FormulaSymbol IS NOT NULL AND CHARINDEX(t.FormulaSymbol, f.FormulaSymbols) > 0
ORDER BY c.UnitID, t.FormulaSymbol

