﻿CREATE PROC [dbo].[GetDataByPeriod]
	
	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime
	
	
	AS
	
BEGIN
	EXEC SS_GetSettings @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetConfig @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetConfigRS @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetInventory @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetProcessData @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetOpEx @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetOpExAdd @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetPers @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetAbsence @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetMaintTA_Process @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetMaintTA_Other @RefineryID
	EXEC SS_GetMaintRout @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetCrude @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetYieldRM @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetYieldRMB @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetYieldProd @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetEnergy @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetElectric @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetRefTargets @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetUnitTargets @RefineryID, @PeriodStart, @PeriodEnd
	EXEC GetMaintRoutHist @RefineryID, 'ACTUAL'
	EXEC SS_GetUserDefined @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetUnitTargetsNew @RefineryID, @PeriodStart, @PeriodEnd
	EXEC SS_GetEdcStabilizers @RefineryID
	EXEC SS_GetOpExAll @RefineryID, @PeriodStart, @PeriodEnd

END
