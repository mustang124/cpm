﻿CREATE PROC [dbo].[GetMaintRoutHist] (@RefineryID varchar(6), @DataSet varchar(15))
AS

DECLARE @FirstEntry smalldatetime, @Currency CurrencyCode
DECLARE @Months tinyint, @HistDate as smalldatetime
DECLARE @RoutCost real
SELECT @FirstEntry = MIN(PeriodStart) FROM dbo.Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet

DECLARE @hist TABLE (
	PeriodStart smalldatetime NOT NULL, 
	RoutCostLocal real NULL
)

SELECT TOP 1 @Currency = RptCurrency FROM dbo.Submissions 
WHERE RefineryID = @RefineryID AND DataSet = @DataSet
ORDER BY PeriodStart DESC

SELECT @Months = 0
WHILE @Months < 24
BEGIN
	SELECT @Months = @Months + 1, @RoutCost = NULL
	SELECT @HistDate = DATEADD(mm, -@Months, @FirstEntry)
	
	SELECT @RoutCost = RoutCost
	FROM MaintRoutHist
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND ABS(DATEDIFF(dd, PeriodStart, @HistDate)) <= 1
	AND Currency = @Currency
	
	INSERT INTO @hist(PeriodStart, RoutCostLocal)
	VALUES (@HistDate, @RoutCost)
END

SELECT * FROM @Hist ORDER BY PeriodStart

