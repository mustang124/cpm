﻿




CREATE   PROC [dbo].[spReportDemoChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	UtilPcnt real NULL, 
	VEI real NULL, 
	ProcessEffIndex real NULL, 
	OpAvail real NULL, 
	MaintIndex real NULL,
	PersIndex real NULL, 
	NEOpExEdc real NULL,
	OpExUEdc real NULL
)
DECLARE @msgString varchar(255)
SELECT @msgString = CAST(@PeriodEnd as varchar(20))

--- Everything Already Available in GenSum
INSERT INTO @Data (PeriodStart, PeriodEnd, UtilPcnt, OpAvail, EII, VEI, ProcessEffIndex, PersIndex, MaintIndex, NEOpExEdc, OpExUEdc)
SELECT PeriodStart, PeriodEnd, RefUtilPcnt, OpAvail, EII, VEI, ProcessEffIndex, PersIndex, MaintIndex, NEOpExEdc, OpExUEdc
FROM dbo.GetCommonProfileLiteChartData(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM, 12)

IF (SELECT COUNT(*) FROM @Data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @Data WHERE PeriodStart = @Period)
			INSERT @Data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilPcnt, VEI, OpAvail, PersIndex, MaintIndex, NEOpExEdc, OpExUEdc, ProcessEffIndex
FROM @Data
ORDER BY PeriodStart ASC








