﻿


CREATE PROC [dbo].[spReportEIISensHeat] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE @SensHeatFormula varchar(50), @BarrelDesc varchar(50), @BarrelDescRussian nvarchar(100)
SELECT 	@SensHeatFormula = CAST(SensHeatConstant as varchar(4)) + CASE WHEN ISNULL(SensHeatAPIFactor, 0) = 0 THEN '' ELSE CASE WHEN SensHeatAPIFactor < 0 THEN '-(' ELSE '+(' END + CAST(ABS(SensHeatAPIFactor) AS varchar(4)) + '*CrudeGravity)' END,
	@BarrelDesc = CASE WHEN CrudeInSensHeat = 'Y' THEN 'Gross Input Barrels' ELSE 'Gross Non-Crude Processed Barrels' END,
	@BarrelDescRussian = CASE WHEN CrudeInSensHeat = 'Y' THEN N'Gross Input Barrels' ELSE N'Суммарный объем переработки прочего сырья (за исключением нефти), барр.' END
FROM FactorSets WHERE RefineryType = ISNULL(dbo.GetRefineryType(@RefineryID), 'FUELS') AND FactorSet = @FactorSet

SELECT s.SubmissionID, s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod,
f.SensHeatUtilCap AS SensGrossInput, f.SensHeatStdEnergy, c.AvgGravity AS CrudeGravity
INTO #Monthly
FROM FactorTotCalc f INNER JOIN CrudeTot c ON c.SubmissionID = f.SubmissionID
INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE FactorSet=@FactorSet
AND s.RefineryID = @RefineryID AND s.DataSet=@DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1

SELECT s1.SubmissionID, s1.PeriodStart, NumDays_Ytd = SUM(s2.NumDays), 
SensGrossInput_Ytd = SUM(fc.SensHeatUtilCap*s2.NumDays)/SUM(s2.NumDays), EstSensHeatMBTU_Ytd = SUM(fc.SensHeatStdEnergy*s2.NumDays), SensHeatStdEnergy_Ytd = SUM(fc.SensHeatStdEnergy*s2.NumDays)/SUM(s2.NumDays)
INTO #YTD
FROM Submissions s1 INNER JOIN Submissions s2 ON s2.RefineryID = s1.RefineryID AND s2.DataSet = s1.DataSet AND s2.PeriodYear = s1.PeriodYear AND s2.PeriodMonth <= s1.PeriodMonth
INNER JOIN FactorTotCalc fc ON fc.SubmissionID = s2.SubmissionID 
WHERE fc.FactorSet=@FactorSet 
And s1.RefineryID = @RefineryID AND s1.DataSet=@DataSet AND s1.PeriodYear = ISNULL(@PeriodYear, s1.PeriodYear) AND s1.PeriodMonth = ISNULL(@PeriodMonth, s1.PeriodMonth) AND s1.UseSubmission = 1 AND s2.UseSubmission = 1
GROUP BY s1.SubmissionID, s1.PeriodStart

SET NOCOUNT OFF

SELECT m.Location, m.PeriodStart, m.PeriodEnd, m.DaysInPeriod, Currency= @Currency, UOM = @UOM, 
m.SensGrossInput, m.SensHeatStdEnergy, SensHeatFormula = @SensHeatFormula, m.CrudeGravity, BarrelDesc = @BarrelDesc, BarrelDescRussian = @BarrelDescRussian,
y.SensGrossInput_Ytd, y.EstSensHeatMBTU_Ytd, y.SensHeatStdEnergy_Ytd
FROM #Monthly m INNER JOIN #YTD y ON y.SubmissionID = m.SubmissionID
ORDER BY m.PeriodStart DESC

DROP TABLE #Monthly
DROP TABLE #YTD


