﻿CREATE PROC [dbo].[SS_GetEdcStabilizers]

	@RefineryID nvarchar(10)
	
AS

SELECT RefineryID,EffDate,AnnInputBbl,
           AnnCokeBbl,AnnElecConsMWH,AnnRSCRUDE_RAIL,AnnRSCRUDE_TT,
            AnnRSCRUDE_TB,AnnRSCRUDE_OMB,AnnRSCRUDE_BB,AnnRSPROD_RAIL,
            AnnRSCRUDE_TT,AnnRSCRUDE_TB,AnnRSCRUDE_OMB,AnnRSPROD_BB
            FROM dbo.LoadedEdcStabilizers
             WHERE RefineryID=@RefineryID
             