﻿
CREATE PROCEDURE [dbo].[DS_EnergySchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT RTRIM(e.TransType) as TransType, RTRIM(e.TransferTo) as TransferTo, RTRIM(e.EnergyType) as EnergyType, e.TransCode,elu.SortKey, 0.0 AS RptSource, 0.0 AS RptPriceLocal,
                  e.Hydrogen,e.Methane,e.Ethane,e.Ethylene,e.Propane,e.Propylene,e.Butane,e.Isobutane,e.Butylenes,e.C5Plus,e.H2S,e.CO,e.NH3, e.SO2, e.CO2,e.N2 
        FROM Energy e , Energy_LU elu 
        WHERE SubmissionID=(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID ORDER BY PeriodStart DESC) 
        AND e.TransferTo = elu.TransferTo AND e.TransType = elu.TransType AND e.EnergyType = elu.EnergyType
                  
END



