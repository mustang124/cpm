﻿CREATE PROC [dbo].[CalcCEICOC](@SubmissionID int, @FactorSet FactorSet, @AE_COC float OUTPUT, @ACE_COC float OUTPUT, @SE_COC float OUTPUT, @SCE_COC float OUTPUT, @DEBUG bit = 0)
AS

SELECT @AE_COC = 0, @ACE_COC = 0, @SE_COC = 0, @SCE_COC = 0

DECLARE @NumDays real; SELECT @NumDays = NumDays FROM dbo.SubmissionsAll WHERE SubmissionID = @SubmissionID;
DECLARE @CO2_C_Ratio real; SELECT @CO2_C_Ratio = 44.0/12.0;

SELECT @AE_COC = SUM(SourceMBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO' AND EnergyType = 'COC' 
IF @AE_COC IS NULL
	SELECT @AE_COC = 0
IF @AE_COC = 0
	RETURN 0
	
	DECLARE @TotFCCStdEnergy float
	SELECT @TotFCCStdEnergy = SUM(StdEnergy) FROM FactorCalc WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND ProcessID = 'FCC'
	IF @TotFCCStdEnergy = 0
		SET @TotFCCStdEnergy = NULL

	DECLARE @tblFCC_COC TABLE
	(
		UnitID int NOT NULL,
		ProcessType varchar(4) NULL,
		UtilCap real NOT NULL,
		FeedTemp real NULL,
		PreheatTemp real NULL,
		Conv real NULL,
		CatOilRatio real NULL,
		Coke real NULL,
		FeedGravity	real NULL,
		FeedDensity real NULL,
		FeedSulfur real NULL,
		FeedConcarbon real NULL,
		FeedASTM10 real NULL,
		FeedASTM90 real NULL,
		FeedAniline	real NULL,
		UOPK real NULL,
		FeedUVirginGO real NULL,
		FeedUCrackedGO real NULL,
		FeedUAGO real NULL,
		FeedUVGO real NULL,
		FeedUDAO real NULL,
		FeedUCGO real NULL,
		FeedUARC real NULL,
		FeedUVR real NULL,
		FeedULube real NULL,
		FeedHVirginGO real NULL,
		FeedHCrackedGO real NULL,
		FeedHAGO real NULL,
		FeedHVGO real NULL,
		FeedHDAO real NULL,
		FeedHCGO real NULL,
		FeedHARC real NULL,
		FeedHVR real NULL,
		FeedHLube real NULL,
		FeedUVGOSulfur real NULL,
		FeedUVGOAniline real NULL,
		FeedUVGOUOPK real NULL,
		FeedUVGOConCarbon real NULL,
		FeedUVRSulfur real NULL,
		FeedUVRConCarbon real NULL,
		FeedHVGOSulfur real NULL,
		FeedHVGOAniline real NULL,
		FeedHVGOUOPK real NULL,
		FeedHVGOConCarbon real NULL,
		FeedHVRSulfur real NULL,
		FeedHVRConCarbon real NULL,
		FeedUVGOGravity real NULL,
		FeedUVRGravity real NULL,
		FeedHVGOGravity real NULL,
		FeedHVRGravity real NULL,
		FeedUVGODensity real NULL,
		FeedUVRDensity real NULL,
		FeedHVGODensity real NULL,
		FeedHVRDensity real NULL,
		FeedUOth real NULL,
		FeedHOth real NULL,
		FeedOth real NULL,
		FeedUVGOUCap real NULL,
		FeedUVRUCap real NULL,
		EstFeedUOthUCap real NULL,
		FeedHVGOUCap real NULL,
		FeedHVRUCap real NULL,
		EstFeedHOthUCap real NULL,
		FeedUOthUCap real NULL,
		FeedHOthUCap real NULL,
		FeedOthUCap real NULL,
		FeedUVGOUCapMT real NULL,
		FeedUVRUCapMT real NULL,
		EstFeedUOthUCapMT real NULL,
		FeedHVGOUCapMT real NULL,
		FeedHVRUCapMT real NULL,
		EstFeedHOthUCapMT real NULL,
		UtilCapMT real NULL,
		EstFeedOthUCapMT real NULL,
		EstFeedOthDensity real NULL,
		EstFeedOthSulfur real NULL,
		EstCokeSulfur real NULL,
		StdEnergy real NULL,
		AllocCOC_MBTU float NULL,
		AllocCOC_KBTUPerBbl real NULL,
		UtilCap_MLbPerDay real NULL,
		CokeMake_MLbPerDay real NULL,
		AllocCOC_MBTUPerDay float NULL,
		CokeCarbonFraction_Calc real NULL,
		CokeCarbonFraction_Min real NULL,
		CokeCarbonFraction_Max real NULL,
		CokeCarbonFraction real NULL,
		ActualCE_MLbPD real NULL,
		ActualCE_kgPerbbl real NULL,
		ActualCE_gPerKBTUCoke real NULL,
		StdCE_kgPerbbl real NULL,
		CE_COC float NULL,
		SE_COC float NULL,
		SE_COC_Adj float NULL
	)

INSERT @tblFCC_COC (UnitID, ProcessType, UtilCap, FeedTemp, PreheatTemp, Conv, CatOilRatio, Coke, FeedGravity, FeedSulfur, FeedConcarbon, FeedASTM10, FeedASTM90, FeedAniline, UOPK,
		FeedUVirginGO, FeedUCrackedGO, FeedUAGO, FeedUVGO, FeedUDAO, FeedUCGO, FeedUARC, FeedUVR, FeedULube,
		FeedHVirginGO, FeedHCrackedGO, FeedHAGO, FeedHVGO, FeedHDAO, FeedHCGO, FeedHARC, FeedHVR, FeedHLube,
		FeedUVGOSulfur, FeedUVGOAniline, FeedUVGOUOPK, FeedUVGOConCarbon, FeedUVRSulfur, FeedUVRConCarbon,
		FeedHVGOSulfur, FeedHVGOAniline, FeedHVGOUOPK, FeedHVGOConCarbon, FeedHVRSulfur, FeedHVRConCarbon,
		FeedUVGOGravity, FeedUVRGravity, FeedHVGOGravity, FeedHVRGravity)
SELECT c.UnitID, c.ProcessType, c.UtilCap, 
FeedTemp=AVG(CASE WHEN Property='FeedTemp' THEN SAValue ELSE NULL END), 
PreheatTemp=AVG(CASE WHEN Property='PreheatTemp' THEN SAValue ELSE NULL END), 
Conv=AVG(CASE WHEN Property='Conv' THEN SAValue ELSE NULL END), 
CatOilRatio=AVG(CASE WHEN Property='CatOilRatio' AND SAValue > 0 THEN SAValue ELSE NULL END), 
Coke=AVG(CASE WHEN Property='Coke' THEN SAValue ELSE NULL END), 
--RiserTemp=AVG(CASE WHEN Property='RiserTemp' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedGravity=AVG(CASE WHEN Property='FeedGravity' THEN SAValue ELSE NULL END), 
FeedSulfur=AVG(CASE WHEN Property='FeedSulfur' THEN SAValue ELSE NULL END), 
FeedConCarbon=AVG(CASE WHEN Property='FeedConCarbon' THEN SAValue ELSE NULL END), 
FeedASTM10=AVG(CASE WHEN Property='FeedASTM10' THEN SAValue ELSE NULL END), 
FeedASTM90=AVG(CASE WHEN Property='FeedASTM90' THEN SAValue ELSE NULL END), 
FeedAniline=AVG(CASE WHEN Property='FeedAniline' THEN SAValue ELSE NULL END), 
UOPK=AVG(CASE WHEN Property='UOPK' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVirginGO=SUM(CASE WHEN Property='FeedUVirginGO' THEN SAValue ELSE 0 END), 
FeedUCrackedGO=SUM(CASE WHEN Property='FeedUCrackedGO' THEN SAValue ELSE 0 END), 
FeedUAGO=SUM(CASE WHEN Property='FeedUAGO' THEN SAValue ELSE 0 END), 
FeedUVGO=SUM(CASE WHEN Property='FeedUVGO' THEN SAValue ELSE 0 END), 
FeedUDAO=SUM(CASE WHEN Property='FeedUDAO' THEN SAValue ELSE 0 END), 
FeedUCGO=SUM(CASE WHEN Property='FeedUCGO' THEN SAValue ELSE 0 END), 
FeedUARC=SUM(CASE WHEN Property='FeedUARC' THEN SAValue ELSE 0 END), 
FeedUVR=SUM(CASE WHEN Property='FeedUVR' THEN SAValue ELSE 0 END), 
FeedULube=SUM(CASE WHEN Property='FeedULube' THEN SAValue ELSE 0 END), 
FeedHVirginGO=SUM(CASE WHEN Property='FeedHVirginGO' THEN SAValue ELSE 0 END), 
FeedHCrackedGO=SUM(CASE WHEN Property='FeedHCrackedGO' THEN SAValue ELSE 0 END), 
FeedHAGO=SUM(CASE WHEN Property='FeedHAGO' THEN SAValue ELSE 0 END), 
FeedHVGO=SUM(CASE WHEN Property='FeedHVGO' THEN SAValue ELSE 0 END), 
FeedHDAO=SUM(CASE WHEN Property='FeedHDAO' THEN SAValue ELSE 0 END), 
FeedHCGO=SUM(CASE WHEN Property='FeedHCGO' THEN SAValue ELSE 0 END), 
FeedHARC=SUM(CASE WHEN Property='FeedHARC' THEN SAValue ELSE 0 END), 
FeedHVR=SUM(CASE WHEN Property='FeedHVR' THEN SAValue ELSE 0 END), 
FeedHLube=SUM(CASE WHEN Property='FeedHLube' THEN SAValue ELSE 0 END), 
FeedUVGOSulfur=AVG(CASE WHEN Property='FeedUVGOSulfur' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVGOAniline=AVG(CASE WHEN Property='FeedUVGOAniline' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVGOUOPK=AVG(CASE WHEN Property='FeedUVGOUOPK' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVGOConCarbon=AVG(CASE WHEN Property='FeedUVGOConCarbon' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVRSulfur=AVG(CASE WHEN Property='FeedUVRSulfur' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVRConCarbon=AVG(CASE WHEN Property='FeedUVRConCarbon' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedHVGOSulfur=AVG(CASE WHEN Property='FeedHVGOSulfur' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedHVGOAniline=AVG(CASE WHEN Property='FeedHVGOAniline' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedHVGOUOPK=AVG(CASE WHEN Property='FeedHVGOUOPK' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedHVGOConCarbon=AVG(CASE WHEN Property='FeedHVGOConCarbon' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedHVRSulfur=AVG(CASE WHEN Property='FeedHVRSulfur' AND SAValue > 0 THEN SAValue ELSE NULL END),
FeedHVRConCarbon=AVG(CASE WHEN Property='FeedHVRConCarbon' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVGOGravity=AVG(CASE WHEN Property='FeedUVGOGravity' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedUVRGravity=AVG(CASE WHEN Property='FeedUVRGravity' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedHVGOGravity=AVG(CASE WHEN Property='FeedHVGOGravity' AND SAValue > 0 THEN SAValue ELSE NULL END), 
FeedHVRGravity=AVG(CASE WHEN Property='FeedHVRGravity' AND SAValue > 0 THEN SAValue ELSE NULL END)
FROM Config c INNER JOIN ProcessData pd ON pd.SubmissionID = c.SubmissionID AND pd.UnitID = c.UnitID
WHERE c.ProcessID = 'FCC' AND c.SubmissionID = @SubmissionID AND c.UtilCap > 0
GROUP BY c.SubmissionID, c.UnitID, c.ProcessType, c.UtilCap

UPDATE fcc
SET Conv = sd.Conv, CatOilRatio = sd.CatOilRatio, Coke = sd.Coke, 
FeedGravity = sd.FeedGravity, FeedDensity = sd.FeedDensity, FeedSulfur = sd.FeedSulfur, FeedConCarbon = sd.FeedConCarbon,
FeedASTM10 = sd.FeedASTM10, FeedASTM90 = sd.FeedASTM90, FeedAniline = sd.FeedAniline, UOPK = sd.UOPK, EstCokeSulfur = sd.EstCokeSulfur, 
FeedUVirginGO = sd.FeedUVirginGO, FeedUCrackedGO = sd.FeedUCrackedGO, FeedUAGO = sd.FeedUAGO, FeedUVGO = sd.FeedUVGO, FeedUDAO = sd.FeedUDAO, FeedUCGO = sd.FeedUCGO, FeedUARC = sd.FeedUARC, FeedUVR = sd.FeedUVR, FeedULube = sd.FeedULube, 
FeedHVirginGO = sd.FeedHVirginGO, FeedHCrackedGO = sd.FeedHCrackedGO, FeedHAGO = sd.FeedHAGO, FeedHVGO = sd.FeedHVGO, FeedHDAO = sd.FeedHDAO, FeedHCGO = sd.FeedHCGO, FeedHARC = sd.FeedHARC, FeedHVR = sd.FeedHVR, FeedHLube = sd.FeedHLube, 
FeedUVGOGravity = sd.FeedUVGOGravity, FeedUVGODensity = sd.FeedUVGODensity, FeedUVGOSulfur = sd.FeedUVGOSulfur, FeedUVGOAniline = sd.FeedUVGOAniline, FeedUVGOUOPK = sd.FeedUVGOUOPK, 
FeedUVRGravity = sd.FeedUVRGravity, FeedUVRDensity = sd.FeedUVRDensity, FeedUVRSulfur = sd.FeedUVRSulfur, FeedUVRConCarbon = sd.FeedUVRConCarbon, 
FeedHVGOGravity = sd.FeedHVGOGravity, FeedHVGODensity = sd.FeedHVGODensity, FeedHVGOSulfur = sd.FeedHVGOSulfur, FeedHVGOAniline = sd.FeedHVGOAniline, FeedHVGOUOPK = sd.FeedHVGOUOPK, 
FeedHVRGravity = sd.FeedHVRGravity, FeedHVRDensity = sd.FeedHVRDensity, FeedHVRSulfur = sd.FeedHVRSulfur, FeedHVRConCarbon = sd.FeedHVRConCarbon, 
FeedUVGOUCap = sd.FeedUVGOUCap, FeedUVRUCap = sd.FeedUVRUCap, FeedHVGOUCap = sd.FeedHVGOUCap, FeedHVRUCap = sd.FeedHVRUCap,
UtilCapMT = sd.UtilCapMT, FeedUVGOUCapMT = sd.FeedUVGOUCapMT, FeedUVRUCapMT = sd.FeedUVRUCapMT, FeedHVGOUCapMT = sd.FeedHVGOUCapMT, FeedHVRUCapMT = sd.FeedHVRUCapMT, 
FeedUOthUCap = sd.FeedUOthUCap, FeedHOthUCap = sd.FeedHOthUCap, FeedOthUCap = sd.FeedOthUCap, 
EstFeedUOthUCapMT = sd.EstFeedUOthUCapMT, EstFeedHOthUCapMT = sd.EstFeedHOthUCapMT, EstFeedOthUCapMT = sd.EstFeedOthUCapMT, 
EstFeedOthDensity = sd.EstFeedOthDensity, EstFeedOthSulfur = sd.EstFeedOthSulfur
FROM @tblFCC_COC fcc INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = @SubmissionID
INNER JOIN StudyT2_FCC sd ON sd.RefineryID = s.RefineryID AND s.PeriodStart >= sd.EffDate AND s.PeriodStart < sd.EffUntil AND sd.UnitID = fcc.UnitID
WHERE (ISNULL(fcc.FeedUVGO, 0) + ISNULL(fcc.FeedUVR, 0) + ISNULL(fcc.FeedUVirginGO, 0) + ISNULL(fcc.FeedUCrackedGO, 0) + ISNULL(fcc.FeedUAGO, 0) + ISNULL(fcc.FeedUDAO, 0) + ISNULL(fcc.FeedUCGO, 0) + ISNULL(fcc.FeedUARC, 0) + ISNULL(fcc.FeedULube, 0) + ISNULL(fcc.FeedHVGO, 0) + ISNULL(fcc.FeedHVR, 0) + ISNULL(fcc.FeedHVirginGO, 0) + ISNULL(fcc.FeedHCrackedGO, 0) + ISNULL(fcc.FeedHAGO, 0) + ISNULL(fcc.FeedHDAO, 0) + ISNULL(fcc.FeedHCGO, 0) + ISNULL(fcc.FeedHARC, 0) + ISNULL(fcc.FeedHLube, 0)) NOT BETWEEN 90 AND 110

UPDATE fcc
SET Conv = ISNULL(fcc.Conv, sd.Conv), CatOilRatio = ISNULL(fcc.CatOilRatio, sd.CatOilRatio), Coke = ISNULL(fcc.Coke, sd.Coke), 
FeedGravity = ISNULL(fcc.FeedGravity, sd.FeedGravity), FeedSulfur = ISNULL(fcc.FeedSulfur, sd.FeedSulfur), FeedConCarbon = ISNULL(fcc.FeedConcarbon, sd.FeedConCarbon),
FeedASTM10 = ISNULL(fcc.FeedASTM10, sd.FeedASTM10), FeedASTM90 = ISNULL(fcc.FeedASTM90, sd.FeedASTM90), FeedAniline = ISNULL(fcc.FeedAniline, sd.FeedAniline), UOPK = ISNULL(fcc.UOPK, sd.UOPK),
FeedUVGOGravity = ISNULL(fcc.FeedUVGOGravity, sd.FeedUVGOGravity), FeedUVGOSulfur = ISNULL(fcc.FeedUVGOSulfur, sd.FeedUVGOSulfur), FeedUVGOAniline = ISNULL(fcc.FeedUVGOAniline, sd.FeedUVGOAniline), FeedUVGOUOPK = ISNULL(fcc.FeedUVGOUOPK, sd.FeedUVGOUOPK), 
FeedUVRGravity = ISNULL(fcc.FeedUVRGravity, sd.FeedUVRGravity), FeedUVRSulfur = ISNULL(fcc.FeedUVRSulfur, sd.FeedUVRSulfur), FeedUVRConCarbon = ISNULL(fcc.FeedUVRConCarbon, sd.FeedUVRConCarbon), 
FeedHVGOGravity = ISNULL(fcc.FeedHVGOGravity, sd.FeedHVGOGravity), FeedHVGOSulfur = ISNULL(fcc.FeedHVGOSulfur, sd.FeedHVGOSulfur), FeedHVGOAniline = ISNULL(fcc.FeedHVGOAniline, sd.FeedHVGOAniline), FeedHVGOUOPK = ISNULL(fcc.FeedHVGOUOPK, sd.FeedHVGOUOPK), 
FeedHVRGravity = ISNULL(fcc.FeedHVRGravity, sd.FeedHVRGravity), FeedHVRSulfur = ISNULL(fcc.FeedHVRSulfur, sd.FeedHVRSulfur), FeedHVRConCarbon = ISNULL(fcc.FeedHVRConCarbon, sd.FeedHVRConCarbon)
FROM @tblFCC_COC fcc INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = @SubmissionID
INNER JOIN StudyT2_FCC sd ON sd.RefineryID = s.RefineryID AND s.PeriodStart >= sd.EffDate AND s.PeriodStart < sd.EffUntil AND sd.UnitID = fcc.UnitID

UPDATE fcc 
SET StdEnergy = fc.StdEnergy
FROM @tblFCC_COC fcc INNER JOIN FactorCalc fc ON fc.SubmissionID = @SubmissionID AND fc.UnitID = fcc.UnitID AND fc.FactorSet = @FactorSet

UPDATE @tblFCC_COC
SET FeedDensity = dbo.UnitsConv(FeedGravity, 'API', 'KGM3'), FeedUVGODensity = dbo.UnitsConv(FeedUVGOGravity, 'API', 'KGM3'), FeedUVRDensity = dbo.UnitsConv(FeedUVRGravity, 'API', 'KGM3'), FeedHVGODensity = dbo.UnitsConv(FeedHVGOGravity, 'API', 'KGM3'), FeedHVRDensity = dbo.UnitsConv(FeedHVRGravity, 'API', 'KGM3'), 
	FeedUOth = ISNULL(FeedUVirginGO, 0) + ISNULL(FeedUCrackedGO, 0) + ISNULL(FeedUAGO, 0) + ISNULL(FeedUDAO, 0) + ISNULL(FeedUCGO, 0) + ISNULL(FeedUARC, 0) + ISNULL(FeedULube, 0),
	FeedHOth = ISNULL(FeedHVirginGO, 0) + ISNULL(FeedHCrackedGO, 0) + ISNULL(FeedHAGO, 0) + ISNULL(FeedHDAO, 0) + ISNULL(FeedHCGO, 0) + ISNULL(FeedHARC, 0) + ISNULL(FeedHLube, 0)

UPDATE @tblFCC_COC
SET FeedUOthUCap = UtilCap * FeedUOth/100,
	FeedHOthUCap = UtilCap * FeedHOth/100,
	FeedOth = FeedUOth + FeedHOth,
	FeedOthUCap = UtilCap * (FeedUOth + FeedHOth)/100,
	FeedUVGOUCap = UtilCap*ISNULL(FeedUVGO,0),
	FeedUVRUCap = UtilCap * ISNULL(FeedUVR,0),
	FeedHVGOUCap = UtilCap * ISNULL(FeedHVGO,0),
	FeedHVRUCap = UtilCap * ISNULL(FeedHVR,0)

UPDATE @tblFCC_COC
SET UtilCapMT = UtilCap*0.159*FeedDensity/1000,
	FeedUVGOUCapMT = UtilCap*ISNULL(FeedUVGO,0)/100*0.159*ISNULL(FeedUVGODensity, FeedDensity)/1000,
	FeedUVRUCapMT = UtilCap * ISNULL(FeedUVR,0)/100*0.159*ISNULL(FeedUVRDensity, FeedDensity)/1000,
	FeedHVGOUCapMT = UtilCap * ISNULL(FeedHVGO,0)/100*0.159*ISNULL(FeedHVGODensity, FeedDensity)/1000,
	FeedHVRUCapMT = UtilCap * ISNULL(FeedHVR,0)/100*0.159*ISNULL(FeedHVRDensity, FeedDensity)/1000

UPDATE @tblFCC_COC
SET EstFeedOthUCapMT = UtilCapMT - FeedUVGOUCapMT - FeedUVRUCapMT - FeedHVGOUCapMT - FeedHVRUCapMT
	--, EstFeedOthUCap = UtilCap - FeedUVGOUCap - FeedUVRUCap - FeedHVGOUCap - FeedHVRUCap

UPDATE @tblFCC_COC
SET EstFeedUOthUCapMT = 0, EstFeedHOthUCapMT = 0, EstFeedOthDensity = FeedDensity, EstFeedOthSulfur = FeedSulfur
WHERE FeedOthUCap = 0

UPDATE @tblFCC_COC
SET EstFeedUOthUCapMT = EstFeedOthUCapMT * FeedUOthUCap/FeedOthUCap,
	EstFeedHOthUCapMT = EstFeedOthUCapMT * FeedHOthUCap/FeedOthUCap,
	EstFeedOthDensity = EstFeedOthUCapMT/(FeedOthUCap*0.159/1000),
	EstFeedOthSulfur = (UtilCapMT*FeedSulfur - FeedUVGOUCapMT*ISNULL(FeedUVGOSulfur, FeedSulfur) - FeedUVRUCapMT*ISNULL(FeedUVRSulfur, FeedSulfur) - FeedHVGOUCapMT*ISNULL(FeedHVGOSulfur, FeedSulfur) - FeedHVRUCapMT*ISNULL(FeedHVRSulfur, FeedSulfur))/EstFeedOthUCapMT
WHERE FeedOthUCap > 0 AND FeedOthUCap > 0 AND EstFeedOthUCapMT > 0

UPDATE @tblFCC_COC
SET EstCokeSulfur = (((FeedUVGOUCapMT*ISNULL(FeedUVGOSulfur, FeedSulfur) + FeedUVRUCapMT*ISNULL(FeedUVRSulfur, FeedSulfur) + EstFeedUOthUCapMT*ISNULL(EstFeedOthSulfur, FeedSulfur))*0.1 +
				   (FeedHVGOUCapMT*ISNULL(FeedHVGOSulfur, FeedSulfur) + FeedHVRUCapMT*ISNULL(FeedHVRSulfur, FeedSulfur) + EstFeedHOthUCapMT*ISNULL(EstFeedOthSulfur, FeedSulfur))*0.2) * ISNULL(Conv,0)/100)/(UtilCapMT*Coke/100)

UPDATE @tblFCC_COC
SET	AllocCOC_MBTU = @AE_COC*StdEnergy/@TotFCCStdEnergy,
	UtilCap_MLbPerDay = UtilCap*42*8.34*(FeedDensity/1000)/1000000

UPDATE @tblFCC_COC
SET	AllocCOC_KBTUPerBbl = AllocCOC_MBTU/(UtilCap*@NumDays)*1000,
	CokeMake_MLbPerDay = UtilCap_MLbPerDay*Coke/100,
	AllocCOC_MBTUPerDay = AllocCOC_MBTU/@NumDays


UPDATE @tblFCC_COC
SET	CokeCarbonFraction_Calc= 1.4424 - AllocCOC_MBTUPerDay/(31892*CokeMake_MLbPerDay) - 1.3176*EstCokeSulfur/100,
	CokeCarbonFraction_Min = 0.85-EstCokeSulfur/100,
	CokeCarbonFraction_Max = 0.98-EstCokeSulfur/100
UPDATE @tblFCC_COC
SET	CokeCarbonFraction = CASE WHEN CokeCarbonFraction_Calc < CokeCarbonFraction_Min THEN CokeCarbonFraction_Min WHEN CokeCarbonFraction_Calc > CokeCarbonFraction_Max THEN CokeCarbonFraction_Max ELSE CokeCarbonFraction_Calc END
UPDATE @tblFCC_COC
SET	ActualCE_MLbPD = CokeCarbonFraction*CokeMake_MLbPerDay
UPDATE @tblFCC_COC
SET	ActualCE_kgPerbbl = ActualCE_MLbPD*1000000/2.205/UtilCap
UPDATE @tblFCC_COC
SET	ActualCE_gPerKBTUCoke = CASE WHEN AllocCOC_KBTUPerBbl <> 0 THEN ActualCE_kgPerbbl/AllocCOC_KBTUPerBbl*1000 END

UPDATE @tblFCC_COC
SET	StdCE_kgPerbbl = CASE WHEN ProcessType IN ('RCC','MRCC') THEN 0.6024 ELSE 0 END 
	+ 0.5704*POWER(ISNULL(FeedConcarbon, 1.17), 1.1248) 
	- 4.9044*POWER(ISNULL(UOPK, 11.8), 0.4254)
	+ 3.1232E-6*POWER(ISNULL(FeedDensity, 910.6), 2.2206)
	+ 0.4481*POWER(ISNULL(CatOilRatio, 6.64), 0.7374)
	+ 0.1918*POWER(ISNULL(Conv, 74.2), 0.6878)
	+ 0.1518*(ISNULL(Conv, 74.2)/(100-ISNULL(Conv, 74.2)))
	+ 0.06229*POWER(ISNULL(FeedASTM90,1013), 0.5593)
	- 2.0853E-4*POWER(CASE WHEN ISNULL(FeedTemp, -1)>ISNULL(PreHeatTemp, 0) THEN ISNULL(FeedTemp, 259) ELSE ISNULL(PreHeatTemp, 259) END, 1.2713)

UPDATE @tblFCC_COC
SET	CE_COC = @NumDays*StdCE_kgPerbbl*UtilCap/1000
UPDATE @tblFCC_COC
SET	SE_COC =CE_COC/CokeCarbonFraction/UtilCap/@NumDays/0.159/FeedDensity/Coke*100000*AllocCOC_MBTU
UPDATE @tblFCC_COC
SET	SE_COC_Adj = CASE WHEN SE_COC > CE_COC/0.022 THEN CE_COC/0.022 WHEN SE_COC < CE_COC/0.028 THEN CE_COC/0.028 ELSE SE_COC END

SELECT @SE_COC = SUM(SE_COC_Adj), @SCE_COC = SUM(CE_COC)*@CO2_C_Ratio, @ACE_COC= SUM(ActualCE_gPerKBTUCoke*AllocCOC_MBTU)*@CO2_C_Ratio/1000
FROM @tblFCC_COC
SELECT @SE_COC = ISNULL(@SE_COC, 0), @SCE_COC = ISNULL(@SCE_COC, 0), @ACE_COC = ISNULL(@ACE_COC, 0)

IF @AE_COC > 0
BEGIN
	IF @ACE_COC <= 0
		SELECT @ACE_COC = @AE_COC*0.0293
	ELSE IF @ACE_COC > 0.028*@AE_COC*@CO2_C_Ratio
		SELECT @ACE_COC = 0.028*@AE_COC*@CO2_C_Ratio
	ELSE IF @ACE_COC < 0.022*@AE_COC*@CO2_C_Ratio
		SELECT @ACE_COC = 0.022*@AE_COC*@CO2_C_Ratio
END

IF @DEBUG = 1
	SELECT *, ACE_COC = (ActualCE_gPerKBTUCoke*AllocCOC_MBTU)/1000 FROM @tblFCC_COC
