﻿CREATE PROC [dbo].[spReportEIIMaterialBal] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

DECLARE @T14Crude float, @RMB float, @OTHRM float, @M106 float, @PROD float, @COKE float, @ASP float, @RFC float, @RFS float, @RFCG float
DECLARE @TotInputBbl float, @TotProdBbl float, @GainBbl float
DECLARE @ProdMBTUCons float, @MBTUSold float
DECLARE @T1CDUCharge float, @CrudeDelta float

SELECT @T14Crude = [CRD], @RMB = [RMB], @OTHRM = [OTHRM], @M106 = [M106], @PROD = [PROD], @COKE = [COKE], @ASP = [ASP], @RFC = [RFC], @RFS = [RFS], @RFCG = [RFCG]
FROM (
	SELECT Category = CASE WHEN y.Category IN ('RMI','RPF') THEN y.MaterialID WHEN y.MaterialID = 'GAIN' THEN 'GAIN' ELSE Category END, Bbl
	FROM Yield y INNER JOIN Submissions s ON s.SubmissionID = y.SubmissionID
	WHERE s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1) src
PIVOT (SUM(Bbl) FOR Category IN ([CRD],[OTHRM],[RMB],[M106],[PROD],[COKE],[ASP],[RFC],[RFS],[RFCG])) pvt

SELECT @TotInputBbl = mt.TotInputBbl, @GainBbl = mt.GainBbl
FROM MaterialTot mt INNER JOIN Submissions s ON s.SubmissionID = mt.SubmissionID
WHERE s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
/*
SELECT @ProdMBTUCons = SUM(MBTU)
FROM EnergyCons e INNER JOIN Submissions s ON s.SubmissionID = e.SubmissionID
WHERE s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
AND TransType = 'CPR'

SELECT @MBTUSold = SUM(UsageMBTU)
FROM Energy e INNER JOIN Submissions s ON s.SubmissionID = e.SubmissionID
WHERE s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
AND ((e.TransType = 'DST' AND e.TransferTo = 'AFF') OR e.TransType = 'SOL')
*/
SELECT @TotProdBbl = @TotInputBbl + @GainBbl

SELECT @T1CDUCharge = SUM(c.UtilCap*s.NumDays)
FROM Config c INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
WHERE s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
AND c.ProcessID = 'CDU'

SELECT @CrudeDelta = ISNULL(@T1CDUCharge,0) - ISNULL(@T14Crude,0)

DECLARE @EnergyConv real = 1.0
IF @UOM = 'MET'
	SELECT @EnergyConv = 1.055
	
SELECT CrudeBbl = ISNULL(@T14Crude,0), RMB = ISNULL(@RMB,0), OTHRM = ISNULL(@OTHRM,0), M106 = ISNULL(@M106,0), TotInputBbl = ISNULL(@TotInputBbl,0)
	, FinProd = @PROD, Coke = @COKE, Asphalt = @ASP, RPFConsFOE = @RFC, RPFSoldFOE = @RFS, CogenSalesFOE = @RFCG, TotProd = @TotProdBbl, Gain = @GainBbl
	, RPFCons = @RFC*6.05*@EnergyConv, RPFSold = @RFS*6.05*@EnergyConv, RPFCogen = @RFCG*6.05*@EnergyConv
	, CDUCharge = @T1CDUCharge, CrudeDelta = @CrudeDelta

