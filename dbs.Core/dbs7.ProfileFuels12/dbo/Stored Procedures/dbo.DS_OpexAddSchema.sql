﻿CREATE PROCEDURE [dbo].[DS_OpExAddSchema]
	
AS
BEGIN
SELECT OCCBenAbs,OCCBenInsur,OCCBenPension,OCCBenSub,OCCBenStock, 
                OCCBenTaxPen,OCCBenTaxMed,OCCBenTaxOth,MPSBenAbs,MPSBenInsur, 
                MPSBenPension,MPSBenSub,MPSBenStock,MPSBenTaxPen,MPSBenTaxMed,MPSBenTaxOth, 
                MaintMatl,ContMaintMatl,EquipMaint,OthContProcOp,OthContTransOp, 
                OthContFire,OthContVacTrucks,OthContConsult,OthContInsp,OthContSecurity, 
                OthContComputing,OthContJan,OthContLab,OthContFoodSvc,OthContAdmin, 
                OthContLegal,OthContOth,EnvirDisp,EnvirPermits,EnvirFines, 
                EnvirSpill,EnvirLab,EnvirEng,EnvirOth,EquipNonMaint,Tax,Insur, 
                OthNonVolSupply,OthNonVolSafety,OthNonVolComm,OthNonVolDonations,OthNonVolDues, 
                OthNonVolTravel,OthNonVolTrain,OthNonVolComputer,OthNonVolTanks, 
                OthNonVolOth,ChemicalsAlkyAcid,ChemicalsLube,ChemicalsH2OTreat, 
                ChemicalsProcess,ChemicalsOthAcid,ChemicalsGasAdd,ChemicalsDieselAdd, 
                ChemicalsOthAdd,ChemicalsO2,ChemicalsClay,ChemicalsAmines,ChemicalsASESolv, 
                ChemicalsWasteH2O,ChemicalsNMP,ChemicalsFurfural,ChemicalsMIBK, 
                ChemicalsMEK,ChemicalsToluene,ChemicalsPropane,ChemicalsOthSolv, 
                ChemicalsDewaxAids,ChemicalsOth,CatalystsFCC,CatalystsHYC, 
                CatalystsNKSHYT,CatalystsDHYT,CatalystsVHYT,CatalystsRHYT, 
                CatalystsHYFT,CatalystsCDWax,CatalystsREF,CatalystsHYG, 
                CatalystsS2Plant,CatalystsPetChem,CatalystsOth,PurOthN2,PurOthH2O, 
                PurOthOth,Royalties,OthVolDemCrude,OthVolDemLightering,OthVolDemProd, 
                EmissionsTaxes,EmissionsPurch,EmissionsCredits,OthVolOth  
                FROM OpExAdd WHERE SubmissionID=0
                
END
