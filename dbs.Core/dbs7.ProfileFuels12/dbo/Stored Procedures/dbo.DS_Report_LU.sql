﻿CREATE PROC [dbo].[DS_Report_LU]
	@CompanyID nvarchar(20)
AS

SELECT RTRIM(ReportCode) AS ReportCode, RTRIM(ReportName) AS ReportName, SortKey FROM Report_LU WHERE CustomGroup=0 OR CustomGroup IN ( SELECT CustomGroup FROM CoCustom WHERE CompanyID=@CompanyID and CustomType='R') ORDER BY SortKey
