﻿



CREATE    PROC [dbo].[spReportChevronKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
IF @RefineryID = '179EUR'
	RETURN 1

DECLARE 
	@BU_UtilPcnt real, @BU_KEdc real, @BU_KUEdc real, 
	@BU_MechAvail real, @BU_KMAEdc real, @BU_KProcEdc real,
	@BU_EII real, @BU_KEnergyUseDay real, @BU_KTotStdEnergy real,
	@BU_AdjMaintIndex real, @BU_TAIndex_Avg real, @BU_RoutIndex real,   
	@BU_TotEqPEdc real, 
	@BU_NEOpExUEdc real, @BU_TotCashOpExUEdc real,
	@BU_CashMargin real, @BU_NetInputBPD real
DECLARE 
	@CT_UtilPcnt real, @CT_KEdc real, @CT_KUEdc real, 
	@CT_MechAvail real, @CT_KMAEdc real, @CT_KProcEdc real,
	@CT_EII real, @CT_KEnergyUseDay real, @CT_KTotStdEnergy real,
	@CT_AdjMaintIndex real, @CT_TAIndex_Avg real, @CT_RoutIndex real,   
	@CT_TotEqPEdc real, 
	@CT_NEOpExUEdc real, @CT_TotCashOpExUEdc real,
	@CT_CashMargin real, @CT_NetInputBPD real
DECLARE 
	@ES_UtilPcnt real, @ES_KEdc real, @ES_KUEdc real, 
	@ES_MechAvail real, @ES_KMAEdc real, @ES_KProcEdc real,
	@ES_EII real, @ES_KEnergyUseDay real, @ES_KTotStdEnergy real,
	@ES_AdjMaintIndex real, @ES_TAIndex_Avg real, @ES_RoutIndex real,   
	@ES_TotEqPEdc real, 
	@ES_NEOpExUEdc real, @ES_TotCashOpExUEdc real,
	@ES_CashMargin real, @ES_NetInputBPD real
DECLARE 
	@HI_UtilPcnt real, @HI_KEdc real, @HI_KUEdc real, 
	@HI_MechAvail real, @HI_KMAEdc real, @HI_KProcEdc real,
	@HI_EII real, @HI_KEnergyUseDay real, @HI_KTotStdEnergy real,
	@HI_AdjMaintIndex real, @HI_TAIndex_Avg real, @HI_RoutIndex real,   
	@HI_TotEqPEdc real, 
	@HI_NEOpExUEdc real, @HI_TotCashOpExUEdc real,
	@HI_CashMargin real, @HI_NetInputBPD real
DECLARE 
	@PA_UtilPcnt real, @PA_KEdc real, @PA_KUEdc real, 
	@PA_MechAvail real, @PA_KMAEdc real, @PA_KProcEdc real,
	@PA_EII real, @PA_KEnergyUseDay real, @PA_KTotStdEnergy real,
	@PA_AdjMaintIndex real, @PA_TAIndex_Avg real, @PA_RoutIndex real,   
	@PA_TotEqPEdc real, 
	@PA_NEOpExUEdc real, @PA_TotCashOpExUEdc real,
	@PA_CashMargin real, @PA_NetInputBPD real
DECLARE 
	@RI_UtilPcnt real, @RI_KEdc real, @RI_KUEdc real, 
	@RI_MechAvail real, @RI_KMAEdc real, @RI_KProcEdc real,
	@RI_EII real, @RI_KEnergyUseDay real, @RI_KTotStdEnergy real,
	@RI_AdjMaintIndex real, @RI_TAIndex_Avg real, @RI_RoutIndex real,   
	@RI_TotEqPEdc real, 
	@RI_NEOpExUEdc real, @RI_TotCashOpExUEdc real,
	@RI_CashMargin real, @RI_NetInputBPD real
DECLARE 
	@SL_UtilPcnt real, @SL_KEdc real, @SL_KUEdc real, 
	@SL_MechAvail real, @SL_KMAEdc real, @SL_KProcEdc real,
	@SL_EII real, @SL_KEnergyUseDay real, @SL_KTotStdEnergy real,
	@SL_AdjMaintIndex real, @SL_TAIndex_Avg real, @SL_RoutIndex real,   
	@SL_TotEqPEdc real, 
	@SL_NEOpExUEdc real, @SL_TotCashOpExUEdc real,
	@SL_CashMargin real, @SL_NetInputBPD real

DECLARE 
	@CHEVRON_UtilPcnt real, @CHEVRON_KEdc real, @CHEVRON_KUEdc real, 
	@CHEVRON_MechAvail real, @CHEVRON_KMAEdc real, @CHEVRON_KProcEdc real,
	@CHEVRON_EII real, @CHEVRON_KEnergyUseDay real, @CHEVRON_KTotStdEnergy real,
	@CHEVRON_AdjMaintIndex real, @CHEVRON_TAIndex_Avg real, @CHEVRON_RoutIndex real,   
	@CHEVRON_TotEqPEdc real, 
	@CHEVRON_NEOpExUEdc real, @CHEVRON_TotCashOpExUEdc real,
	@CHEVRON_CashMargin real, @CHEVRON_NetInputBPD real

EXEC [dbo].[spReportChevronKPI_REF] '162NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@BU_UtilPcnt OUTPUT, @KEdc=@BU_KEdc OUTPUT, @KUEdc=@BU_KUEdc OUTPUT, 
	@MechAvail=@BU_MechAvail OUTPUT, @KMAEdc=@BU_KMAEdc OUTPUT, @KProcEdc=@BU_KProcEdc OUTPUT,
	@EII=@BU_EII OUTPUT, @KEnergyUseDay=@BU_KEnergyUseDay OUTPUT, @KTotStdEnergy=@BU_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@BU_AdjMaintIndex OUTPUT, @TAIndex_Avg=@BU_TAIndex_Avg OUTPUT, @RoutIndex=@BU_RoutIndex OUTPUT,   
	@TotEqPEdc=@BU_TotEqPEdc OUTPUT, 
	@NEOpExUEdc=@BU_NEOpExUEdc OUTPUT, @TotCashOpExUEdc=@BU_TotCashOpExUEdc OUTPUT,
	@CashMargin=@BU_CashMargin OUTPUT, @NetInputBPD=@BU_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '53PAC', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@CT_UtilPcnt OUTPUT, @KEdc=@CT_KEdc OUTPUT, @KUEdc=@CT_KUEdc OUTPUT, 
	@MechAvail=@CT_MechAvail OUTPUT, @KMAEdc=@CT_KMAEdc OUTPUT, @KProcEdc=@CT_KProcEdc OUTPUT,
	@EII=@CT_EII OUTPUT, @KEnergyUseDay=@CT_KEnergyUseDay OUTPUT, @KTotStdEnergy=@CT_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@CT_AdjMaintIndex OUTPUT, @TAIndex_Avg=@CT_TAIndex_Avg OUTPUT, @RoutIndex=@CT_RoutIndex OUTPUT,   
	@TotEqPEdc=@CT_TotEqPEdc OUTPUT, 
	@NEOpExUEdc=@CT_NEOpExUEdc OUTPUT, @TotCashOpExUEdc=@CT_TotCashOpExUEdc OUTPUT,
	@CashMargin=@CT_CashMargin OUTPUT, @NetInputBPD=@CT_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '15NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@ES_UtilPcnt OUTPUT, @KEdc=@ES_KEdc OUTPUT, @KUEdc=@ES_KUEdc OUTPUT, 
	@MechAvail=@ES_MechAvail OUTPUT, @KMAEdc=@ES_KMAEdc OUTPUT, @KProcEdc=@ES_KProcEdc OUTPUT,
	@EII=@ES_EII OUTPUT, @KEnergyUseDay=@ES_KEnergyUseDay OUTPUT, @KTotStdEnergy=@ES_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@ES_AdjMaintIndex OUTPUT, @TAIndex_Avg=@ES_TAIndex_Avg OUTPUT, @RoutIndex=@ES_RoutIndex OUTPUT,   
	@TotEqPEdc=@ES_TotEqPEdc OUTPUT, 
	@NEOpExUEdc=@ES_NEOpExUEdc OUTPUT, @TotCashOpExUEdc=@ES_TotCashOpExUEdc OUTPUT,
	@CashMargin=@ES_CashMargin OUTPUT, @NetInputBPD=@ES_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '16NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@HI_UtilPcnt OUTPUT, @KEdc=@HI_KEdc OUTPUT, @KUEdc=@HI_KUEdc OUTPUT, 
	@MechAvail=@HI_MechAvail OUTPUT, @KMAEdc=@HI_KMAEdc OUTPUT, @KProcEdc=@HI_KProcEdc OUTPUT,
	@EII=@HI_EII OUTPUT, @KEnergyUseDay=@HI_KEnergyUseDay OUTPUT, @KTotStdEnergy=@HI_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@HI_AdjMaintIndex OUTPUT, @TAIndex_Avg=@HI_TAIndex_Avg OUTPUT, @RoutIndex=@HI_RoutIndex OUTPUT,   
	@TotEqPEdc=@HI_TotEqPEdc OUTPUT, 
	@NEOpExUEdc=@HI_NEOpExUEdc OUTPUT, @TotCashOpExUEdc=@HI_TotCashOpExUEdc OUTPUT,
	@CashMargin=@HI_CashMargin OUTPUT, @NetInputBPD=@HI_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '17NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@PA_UtilPcnt OUTPUT, @KEdc=@PA_KEdc OUTPUT, @KUEdc=@PA_KUEdc OUTPUT, 
	@MechAvail=@PA_MechAvail OUTPUT, @KMAEdc=@PA_KMAEdc OUTPUT, @KProcEdc=@PA_KProcEdc OUTPUT,
	@EII=@PA_EII OUTPUT, @KEnergyUseDay=@PA_KEnergyUseDay OUTPUT, @KTotStdEnergy=@PA_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@PA_AdjMaintIndex OUTPUT, @TAIndex_Avg=@PA_TAIndex_Avg OUTPUT, @RoutIndex=@PA_RoutIndex OUTPUT,   
	@TotEqPEdc=@PA_TotEqPEdc OUTPUT, 
	@NEOpExUEdc=@PA_NEOpExUEdc OUTPUT, @TotCashOpExUEdc=@PA_TotCashOpExUEdc OUTPUT,
	@CashMargin=@PA_CashMargin OUTPUT, @NetInputBPD=@PA_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '79FL', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@RI_UtilPcnt OUTPUT, @KEdc=@RI_KEdc OUTPUT, @KUEdc=@RI_KUEdc OUTPUT, 
	@MechAvail=@RI_MechAvail OUTPUT, @KMAEdc=@RI_KMAEdc OUTPUT, @KProcEdc=@RI_KProcEdc OUTPUT,
	@EII=@RI_EII OUTPUT, @KEnergyUseDay=@RI_KEnergyUseDay OUTPUT, @KTotStdEnergy=@RI_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@RI_AdjMaintIndex OUTPUT, @TAIndex_Avg=@RI_TAIndex_Avg OUTPUT, @RoutIndex=@RI_RoutIndex OUTPUT,   
	@TotEqPEdc=@RI_TotEqPEdc OUTPUT, 
	@NEOpExUEdc=@RI_NEOpExUEdc OUTPUT, @TotCashOpExUEdc=@RI_TotCashOpExUEdc OUTPUT,
	@CashMargin=@RI_CashMargin OUTPUT, @NetInputBPD=@RI_NetInputBPD OUTPUT

EXEC [dbo].[spReportChevronKPI_REF] '21NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt=@SL_UtilPcnt OUTPUT, @KEdc=@SL_KEdc OUTPUT, @KUEdc=@SL_KUEdc OUTPUT, 
	@MechAvail=@SL_MechAvail OUTPUT, @KMAEdc=@SL_KMAEdc OUTPUT, @KProcEdc=@SL_KProcEdc OUTPUT,
	@EII=@SL_EII OUTPUT, @KEnergyUseDay=@SL_KEnergyUseDay OUTPUT, @KTotStdEnergy=@SL_KTotStdEnergy OUTPUT,
	@AdjMaintIndex=@SL_AdjMaintIndex OUTPUT, @TAIndex_Avg=@SL_TAIndex_Avg OUTPUT, @RoutIndex=@SL_RoutIndex OUTPUT,   
	@TotEqPEdc=@SL_TotEqPEdc OUTPUT, 
	@NEOpExUEdc=@SL_NEOpExUEdc OUTPUT, @TotCashOpExUEdc=@SL_TotCashOpExUEdc OUTPUT,
	@CashMargin=@SL_CashMargin OUTPUT, @NetInputBPD=@SL_NetInputBPD OUTPUT


SELECT	@CHEVRON_KEdc = ISNULL(@BU_KEdc,0) + ISNULL(@CT_KEdc,0) + ISNULL(@ES_KEdc,0) + ISNULL(@HI_KEdc,0) 
					  + ISNULL(@PA_KEdc,0) + ISNULL(@RI_KEdc,0) + ISNULL(@SL_KEdc,0),
		@CHEVRON_KUEdc = ISNULL(@BU_KUEdc,0) + ISNULL(@CT_KUEdc,0) + ISNULL(@ES_KUEdc,0) + ISNULL(@HI_KUEdc,0) 
					   + ISNULL(@PA_KUEdc,0) + ISNULL(@RI_KUEdc,0) + ISNULL(@SL_KUEdc,0),
		@CHEVRON_KMAEdc = ISNULL(@BU_KMAEdc,0) + ISNULL(@CT_KMAEdc,0) + ISNULL(@ES_KMAEdc,0) + ISNULL(@HI_KMAEdc,0) 
					    + ISNULL(@PA_KMAEdc,0) + ISNULL(@RI_KMAEdc,0) + ISNULL(@SL_KMAEdc,0),
		@CHEVRON_KProcEdc = ISNULL(@BU_KProcEdc,0) + ISNULL(@CT_KProcEdc,0) + ISNULL(@ES_KProcEdc,0) + ISNULL(@HI_KProcEdc,0) 
							 + ISNULL(@PA_KProcEdc,0) + ISNULL(@RI_KProcEdc,0) + ISNULL(@SL_KProcEdc,0),
		@CHEVRON_NetInputBPD = ISNULL(@BU_NetInputBPD,0) + ISNULL(@CT_NetInputBPD,0) + ISNULL(@ES_NetInputBPD,0) + ISNULL(@HI_NetInputBPD,0) 
							 + ISNULL(@PA_NetInputBPD,0) + ISNULL(@RI_NetInputBPD,0) + ISNULL(@SL_NetInputBPD,0),
		@CHEVRON_KEnergyUseDay = ISNULL(@BU_KEnergyUseDay,0) + ISNULL(@CT_KEnergyUseDay,0) + ISNULL(@ES_KEnergyUseDay,0) + ISNULL(@HI_KEnergyUseDay,0) 
							   + ISNULL(@PA_KEnergyUseDay,0) + ISNULL(@RI_KEnergyUseDay,0) + ISNULL(@SL_KEnergyUseDay,0),
		@CHEVRON_KTotStdEnergy = ISNULL(@BU_KTotStdEnergy,0) + ISNULL(@CT_KTotStdEnergy,0) + ISNULL(@ES_KTotStdEnergy,0) + ISNULL(@HI_KTotStdEnergy,0) 
							   + ISNULL(@PA_KTotStdEnergy,0) + ISNULL(@RI_KTotStdEnergy,0) + ISNULL(@SL_KTotStdEnergy,0)

IF @CHEVRON_KEdc > 0
	SELECT	@CHEVRON_UtilPcnt = 100.0 * @CHEVRON_KUEdc / @CHEVRON_KEdc,
			@CHEVRON_AdjMaintIndex = (ISNULL(@BU_KEdc,0)*ISNULL(@BU_AdjMaintIndex,0) + ISNULL(@CT_KEdc,0)*ISNULL(@CT_AdjMaintIndex,0) + ISNULL(@ES_KEdc,0)*ISNULL(@ES_AdjMaintIndex,0) + ISNULL(@HI_KEdc,0)*ISNULL(@HI_AdjMaintIndex,0) 
			   						+ ISNULL(@PA_KEdc,0)*ISNULL(@PA_AdjMaintIndex,0) + ISNULL(@RI_KEdc,0)*ISNULL(@RI_AdjMaintIndex,0) + ISNULL(@SL_KEdc,0)*ISNULL(@SL_AdjMaintIndex,0))/@CHEVRON_KEdc,
			@CHEVRON_TAIndex_Avg = (ISNULL(@BU_KEdc,0)*ISNULL(@BU_TAIndex_Avg,0) + ISNULL(@CT_KEdc,0)*ISNULL(@CT_TAIndex_Avg,0) + ISNULL(@ES_KEdc,0)*ISNULL(@ES_TAIndex_Avg,0) + ISNULL(@HI_KEdc,0)*ISNULL(@HI_TAIndex_Avg,0) 
								  + ISNULL(@PA_KEdc,0)*ISNULL(@PA_TAIndex_Avg,0) + ISNULL(@RI_KEdc,0)*ISNULL(@RI_TAIndex_Avg,0) + ISNULL(@SL_KEdc,0)*ISNULL(@SL_TAIndex_Avg,0))/@CHEVRON_KEdc,
			@CHEVRON_RoutIndex = (ISNULL(@BU_KEdc,0)*ISNULL(@BU_RoutIndex,0) + ISNULL(@CT_KEdc,0)*ISNULL(@CT_RoutIndex,0) + ISNULL(@ES_KEdc,0)*ISNULL(@ES_RoutIndex,0) + ISNULL(@HI_KEdc,0)*ISNULL(@HI_RoutIndex,0) 
								+ ISNULL(@PA_KEdc,0)*ISNULL(@PA_RoutIndex,0) + ISNULL(@RI_KEdc,0)*ISNULL(@RI_RoutIndex,0) + ISNULL(@SL_KEdc,0)*ISNULL(@SL_RoutIndex,0))/@CHEVRON_KEdc,
			@CHEVRON_TotEqPEdc = (ISNULL(@BU_KEdc,0)*ISNULL(@BU_TotEqPEdc,0) + ISNULL(@CT_KEdc,0)*ISNULL(@CT_TotEqPEdc,0) + ISNULL(@ES_KEdc,0)*ISNULL(@ES_TotEqPEdc,0) + ISNULL(@HI_KEdc,0)*ISNULL(@HI_TotEqPEdc,0) 
								+ ISNULL(@PA_KEdc,0)*ISNULL(@PA_TotEqPEdc,0) + ISNULL(@RI_KEdc,0)*ISNULL(@RI_TotEqPEdc,0) + ISNULL(@SL_KEdc,0)*ISNULL(@SL_TotEqPEdc,0))/@CHEVRON_KEdc

IF @CHEVRON_KUEdc > 0
	SELECT	@CHEVRON_NEOpExUEdc = (ISNULL(@BU_KUEdc,0)*ISNULL(@BU_NEOpExUEdc,0) + ISNULL(@CT_KUEdc,0)*ISNULL(@CT_NEOpExUEdc,0) + ISNULL(@ES_KUEdc,0)*ISNULL(@ES_NEOpExUEdc,0) + ISNULL(@HI_KUEdc,0)*ISNULL(@HI_NEOpExUEdc,0) 
								 + ISNULL(@PA_KUEdc,0)*ISNULL(@PA_NEOpExUEdc,0) + ISNULL(@RI_KUEdc,0)*ISNULL(@RI_NEOpExUEdc,0) + ISNULL(@SL_KUEdc,0)*ISNULL(@SL_NEOpExUEdc,0))/@CHEVRON_KUEdc,
			@CHEVRON_TotCashOpExUEdc = (ISNULL(@BU_KUEdc,0)*ISNULL(@BU_TotCashOpExUEdc,0) + ISNULL(@CT_KUEdc,0)*ISNULL(@CT_TotCashOpExUEdc,0) + ISNULL(@ES_KUEdc,0)*ISNULL(@ES_TotCashOpExUEdc,0) + ISNULL(@HI_KUEdc,0)*ISNULL(@HI_TotCashOpExUEdc,0) 
									  + ISNULL(@PA_KUEdc,0)*ISNULL(@PA_TotCashOpExUEdc,0) + ISNULL(@RI_KUEdc,0)*ISNULL(@RI_TotCashOpExUEdc,0) + ISNULL(@SL_KUEdc,0)*ISNULL(@SL_TotCashOpExUEdc,0))/@CHEVRON_KUEdc

IF @CHEVRON_KProcEdc > 0
	SELECT	@CHEVRON_MechAvail = 100.0 * @CHEVRON_KMAEdc / @CHEVRON_KProcEdc

IF @CHEVRON_KTotStdEnergy > 0
	SELECT	@CHEVRON_EII = 100.0 * @CHEVRON_KEnergyUseDay / @CHEVRON_KTotStdEnergy

IF @CHEVRON_NetInputBPD > 0
	SELECT	@CHEVRON_CashMargin = (ISNULL(@BU_NetInputBPD,0)*ISNULL(@BU_CashMargin,0) + ISNULL(@CT_NetInputBPD,0)*ISNULL(@CT_CashMargin,0) + ISNULL(@ES_NetInputBPD,0)*ISNULL(@ES_CashMargin,0) + ISNULL(@HI_NetInputBPD,0)*ISNULL(@HI_CashMargin,0) 
									  + ISNULL(@PA_NetInputBPD,0)*ISNULL(@PA_CashMargin,0) + ISNULL(@RI_NetInputBPD,0)*ISNULL(@RI_CashMargin,0) + ISNULL(@SL_NetInputBPD,0)*ISNULL(@SL_CashMargin,0))/@CHEVRON_NetInputBPD

SET NOCOUNT OFF

SELECT  ReportPeriod = CAST(CAST(@PeriodMonth as varchar(2)) + '/1/' + CAST(@PeriodYear as varchar(4)) as smalldatetime),
	BU_UtilPcnt=@BU_UtilPcnt, BU_KEdc=@BU_KEdc, BU_KUEdc=@BU_KUEdc, 
	BU_MechAvail=@BU_MechAvail, BU_KMAEdc=@BU_KMAEdc, BU_KProcEdc=@BU_KProcEdc,
	BU_EII=@BU_EII, BU_KEnergyUseDay=@BU_KEnergyUseDay, BU_KTotStdEnergy=@BU_KTotStdEnergy,
	BU_AdjMaintIndex=@BU_AdjMaintIndex, BU_TAIndex_Avg=@BU_TAIndex_Avg, BU_RoutIndex=@BU_RoutIndex,   
	BU_TotEqPEdc=@BU_TotEqPEdc, 
	BU_NEOpExUEdc=@BU_NEOpExUEdc, BU_TotCashOpExUEdc=@BU_TotCashOpExUEdc,
	BU_CashMargin=@BU_CashMargin, BU_NetInputBPD=@BU_NetInputBPD,

	CT_UtilPcnt=@CT_UtilPcnt, CT_KEdc=@CT_KEdc, CT_KUEdc=@CT_KUEdc, 
	CT_MechAvail=@CT_MechAvail, CT_KMAEdc=@CT_KMAEdc, CT_KProcEdc=@CT_KProcEdc,
	CT_EII=@CT_EII, CT_KEnergyUseDay=@CT_KEnergyUseDay, CT_KTotStdEnergy=@CT_KTotStdEnergy,
	CT_AdjMaintIndex=@CT_AdjMaintIndex, CT_TAIndex_Avg=@CT_TAIndex_Avg, CT_RoutIndex=@CT_RoutIndex,   
	CT_TotEqPEdc=@CT_TotEqPEdc, 
	CT_NEOpExUEdc=@CT_NEOpExUEdc, CT_TotCashOpExUEdc=@CT_TotCashOpExUEdc,
	CT_CashMargin=@CT_CashMargin, CT_NetInputBPD=@CT_NetInputBPD,

	ES_UtilPcnt=@ES_UtilPcnt, ES_KEdc=@ES_KEdc, ES_KUEdc=@ES_KUEdc, 
	ES_MechAvail=@ES_MechAvail, ES_KMAEdc=@ES_KMAEdc, ES_KProcEdc=@ES_KProcEdc,
	ES_EII=@ES_EII, ES_KEnergyUseDay=@ES_KEnergyUseDay, ES_KTotStdEnergy=@ES_KTotStdEnergy,
	ES_AdjMaintIndex=@ES_AdjMaintIndex, ES_TAIndex_Avg=@ES_TAIndex_Avg, ES_RoutIndex=@ES_RoutIndex,   
	ES_TotEqPEdc=@ES_TotEqPEdc, 
	ES_NEOpExUEdc=@ES_NEOpExUEdc, ES_TotCashOpExUEdc=@ES_TotCashOpExUEdc,
	ES_CashMargin=@ES_CashMargin, ES_NetInputBPD=@ES_NetInputBPD,

	HI_UtilPcnt=@HI_UtilPcnt, HI_KEdc=@HI_KEdc, HI_KUEdc=@HI_KUEdc, 
	HI_MechAvail=@HI_MechAvail, HI_KMAEdc=@HI_KMAEdc, HI_KProcEdc=@HI_KProcEdc,
	HI_EII=@HI_EII, HI_KEnergyUseDay=@HI_KEnergyUseDay, HI_KTotStdEnergy=@HI_KTotStdEnergy,
	HI_AdjMaintIndex=@HI_AdjMaintIndex, HI_TAIndex_Avg=@HI_TAIndex_Avg, HI_RoutIndex=@HI_RoutIndex,   
	HI_TotEqPEdc=@HI_TotEqPEdc, 
	HI_NEOpExUEdc=@HI_NEOpExUEdc, HI_TotCashOpExUEdc=@HI_TotCashOpExUEdc,
	HI_CashMargin=@HI_CashMargin, HI_NetInputBPD=@HI_NetInputBPD,

	PA_UtilPcnt=@PA_UtilPcnt, PA_KEdc=@PA_KEdc, PA_KUEdc=@PA_KUEdc, 
	PA_MechAvail=@PA_MechAvail, PA_KMAEdc=@PA_KMAEdc, PA_KProcEdc=@PA_KProcEdc,
	PA_EII=@PA_EII, PA_KEnergyUseDay=@PA_KEnergyUseDay, PA_KTotStdEnergy=@PA_KTotStdEnergy,
	PA_AdjMaintIndex=@PA_AdjMaintIndex, PA_TAIndex_Avg=@PA_TAIndex_Avg, PA_RoutIndex=@PA_RoutIndex,   
	PA_TotEqPEdc=@PA_TotEqPEdc, 
	PA_NEOpExUEdc=@PA_NEOpExUEdc, PA_TotCashOpExUEdc=@PA_TotCashOpExUEdc,
	PA_CashMargin=@PA_CashMargin, PA_NetInputBPD=@PA_NetInputBPD,

	RI_UtilPcnt=@RI_UtilPcnt, RI_KEdc=@RI_KEdc, RI_KUEdc=@RI_KUEdc, 
	RI_MechAvail=@RI_MechAvail, RI_KMAEdc=@RI_KMAEdc, RI_KProcEdc=@RI_KProcEdc,
	RI_EII=@RI_EII, RI_KEnergyUseDay=@RI_KEnergyUseDay, RI_KTotStdEnergy=@RI_KTotStdEnergy,
	RI_AdjMaintIndex=@RI_AdjMaintIndex, RI_TAIndex_Avg=@RI_TAIndex_Avg, RI_RoutIndex=@RI_RoutIndex,   
	RI_TotEqPEdc=@RI_TotEqPEdc, 
	RI_NEOpExUEdc=@RI_NEOpExUEdc, RI_TotCashOpExUEdc=@RI_TotCashOpExUEdc,
	RI_CashMargin=@RI_CashMargin, RI_NetInputBPD=@RI_NetInputBPD,

	SL_UtilPcnt=@SL_UtilPcnt, SL_KEdc=@SL_KEdc, SL_KUEdc=@SL_KUEdc, 
	SL_MechAvail=@SL_MechAvail, SL_KMAEdc=@SL_KMAEdc, SL_KProcEdc=@SL_KProcEdc,
	SL_EII=@SL_EII, SL_KEnergyUseDay=@SL_KEnergyUseDay, SL_KTotStdEnergy=@SL_KTotStdEnergy,
	SL_AdjMaintIndex=@SL_AdjMaintIndex, SL_TAIndex_Avg=@SL_TAIndex_Avg, SL_RoutIndex=@SL_RoutIndex,   
	SL_TotEqPEdc=@SL_TotEqPEdc, 
	SL_NEOpExUEdc=@SL_NEOpExUEdc, SL_TotCashOpExUEdc=@SL_TotCashOpExUEdc,
	SL_CashMargin=@SL_CashMargin, SL_NetInputBPD=@SL_NetInputBPD,

	CHEVRON_UtilPcnt=@CHEVRON_UtilPcnt, CHEVRON_KEdc=@CHEVRON_KEdc, CHEVRON_KUEdc=@CHEVRON_KUEdc, 
	CHEVRON_MechAvail=@CHEVRON_MechAvail, CHEVRON_KMAEdc=@CHEVRON_KMAEdc, CHEVRON_KProcEdc=@CHEVRON_KProcEdc,
	CHEVRON_EII=@CHEVRON_EII, CHEVRON_KEnergyUseDay=@CHEVRON_KEnergyUseDay, CHEVRON_KTotStdEnergy=@CHEVRON_KTotStdEnergy,
	CHEVRON_AdjMaintIndex=@CHEVRON_AdjMaintIndex, CHEVRON_TAIndex_Avg=@CHEVRON_TAIndex_Avg, CHEVRON_RoutIndex=@CHEVRON_RoutIndex,   
	CHEVRON_TotEqPEdc=@CHEVRON_TotEqPEdc, 
	CHEVRON_NEOpExUEdc=@CHEVRON_NEOpExUEdc, CHEVRON_TotCashOpExUEdc=@CHEVRON_TotCashOpExUEdc,
	CHEVRON_CashMargin=@CHEVRON_CashMargin, CHEVRON_NetInputBPD=@CHEVRON_NetInputBPD


