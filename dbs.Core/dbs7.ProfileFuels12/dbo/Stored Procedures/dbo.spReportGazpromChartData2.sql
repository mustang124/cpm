﻿


CREATE   PROC [dbo].[spReportGazpromChartData2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012'

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	UtilPcnt real NULL, 
	VEI real NULL, 
	OpAvail real NULL, 
	RoutIndex real NULL,
	PersIndex real NULL, 
	NEOpExEdc real NULL,
	OpExUEdc real NULL,

	EII_Ytd real NULL, 
	UtilPcnt_Ytd real NULL, 
	VEI_Ytd real NULL, 
	OpAvail_Ytd real NULL, 
	RoutIndex_Ytd real NULL,
	PersIndex_Ytd real NULL, 
	NEOpExEdc_Ytd real NULL,
	OpExUEdc_Ytd real NULL,

	EII_LastYTD real NULL, 
	UtilPcnt_LastYTD real NULL, 
	VEI_LastYTD real NULL, 
	OpAvail_LastYTD real NULL, 
	RoutIndex_LastYTD real NULL,
	PersIndex_LastYTD real NULL, 
	NEOpExEdc_LastYTD real NULL,
	OpExUEdc_LastYTD real NULL,

	ProcessUtilPcnt real NULL, 
	MaintIndex real NULL
)

--- Everything Already Available in GenSum
INSERT INTO @Data (PeriodStart, PeriodEnd, UtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex, RoutIndex, MaintIndex, NEOpExEdc, OpExUEdc
	, UtilPcnt_Ytd, OpAvail_Ytd, EII_Ytd, VEI_Ytd, PersIndex_Ytd, RoutIndex_Ytd, NEOpExEdc_Ytd, OpExUEdc_Ytd)
SELECT	s.PeriodStart, s.PeriodEnd, UtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex = TotWHrEdc, RoutIndex, MaintIndex, NEOpExEdc, TotCashOpExUEdc
	, UtilPcnt_Ytd, OpAvail_Ytd, EII_Ytd, VEI_Ytd, TotWhrEdc_Ytd, RoutIndex_Ytd, NEOpExEdc_Ytd, TotCashOpExUEdc_Ytd
FROM GenSum g INNER JOIN Submissions s ON s.SubmissionID = g.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @PeriodStart12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND g.FactorSet = @FactorSet AND g.Currency = 'USD' AND g.UOM = @UOM AND g.Scenario = @Scenario


IF (SELECT COUNT(*) FROM @Data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @Data WHERE PeriodStart = @Period)
			INSERT @Data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

UPDATE @Data
SET EII = NULL, UtilPcnt = NULL, VEI = NULL, RoutIndex = NULL, PersIndex = NULL, NEOpExEdc = NULL, OpExUEdc = NULL, OpAvail = NULL,
	EII_Ytd = NULL, UtilPcnt_Ytd = NULL, VEI_Ytd = NULL, RoutIndex_Ytd = NULL, PersIndex_Ytd = NULL, NEOpExEdc_Ytd = NULL, OpExUEdc_Ytd = NULL, OpAvail_Ytd = NULL
WHERE DATEPART(yy, PeriodStart) < 2011

UPDATE @Data
SET EII_LastYTD = EII_Ytd, UtilPcnt_LastYTD = UtilPcnt_Ytd, VEI_LastYTD = VEI_Ytd, RoutIndex_LastYTD = RoutIndex_Ytd, PersIndex_LastYTD = PersIndex_Ytd
	, NEOpExEdc_LastYTD = NEOpExEdc_Ytd, OpExUEdc_LastYTD = OpExUEdc_Ytd, OpAvail_LastYTD = OpAvail_Ytd
	, EII_Ytd = NULL, UtilPcnt_Ytd = NULL, VEI_Ytd = NULL, RoutIndex_Ytd = NULL, PersIndex_Ytd = NULL, NEOpExEdc_Ytd = NULL, OpExUEdc_Ytd = NULL, OpAvail_Ytd = NULL
WHERE DATEPART(yy, PeriodStart) < @PeriodYear

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilPcnt, VEI, OpAvail, RoutIndex, PersIndex, NEOpExEdc, OpExUEdc
	, EII_Ytd, UtilPcnt_Ytd, VEI_Ytd, OpAvail_Ytd, RoutIndex_Ytd, PersIndex_Ytd, NEOpExEdc_Ytd, OpExUEdc_Ytd
	, EII_LastYTD, UtilPcnt_LastYTD, VEI_LastYTD, OpAvail_LastYTD, RoutIndex_LastYTD, PersIndex_LastYTD, NEOpExEdc_LastYTD, OpExUEdc_LastYTD
FROM @Data
ORDER BY PeriodStart ASC








