﻿



CREATE PROCEDURE [dbo].[spSaveFactorSet](@SaveTo FactorSet, @SetToSave FactorSet)
AS

DELETE FROM dbo.FactorSets WHERE FactorSet = @SaveTo
SELECT * INTO #CopyFS
FROM dbo.FactorSets WHERE FactorSet = @SetToSave
UPDATE #CopyFS
SET FactorSet = @SaveTo
INSERT dbo.FactorSets
SELECT * FROM #CopyFS
DROP TABLE #CopyFS

DELETE FROM dbo.Factors WHERE FactorSet = @SaveTo
SELECT * INTO #CopyFactors
FROM dbo.Factors WHERE FactorSet = @SetToSave
UPDATE #CopyFactors
SET FactorSet = @SaveTo
INSERT dbo.Factors
SELECT * FROM #CopyFactors
DROP TABLE #CopyFactors

DELETE FROM dbo.MultLimits WHERE FactorSet = @SaveTo
INSERT INTO dbo.MultLimits (FactorSet, MultGroup, MinMultiplicity, MaxMultiplicity)
SELECT @SaveTo, MultGroup, MinMultiplicity, MaxMultiplicity
FROM dbo.MultLimits WHERE FactorSet = @SetToSave

DELETE FROM dbo.MultFactors WHERE FactorSet = @SaveTo
INSERT INTO dbo.MultFactors (FactorSet, MultGroup, NumUnits, MaintFactor, MaintExp, PersFactor, PersExp)
SELECT @SaveTo, MultGroup, NumUnits, MaintFactor, MaintExp, PersFactor, PersExp
FROM dbo.MultFactors WHERE FactorSet = @SetToSave


