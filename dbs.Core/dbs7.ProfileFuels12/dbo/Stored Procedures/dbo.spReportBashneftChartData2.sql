﻿
CREATE PROC [dbo].[spReportBashneftChartData2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks
DECLARE @CompAvg bit = 0
IF (@RefineryID IN ('330EUR','331EUR','154FL') AND @DataSet = 'BASHONEREF') OR @RefineryID = '155FL'
	SET @CompAvg = 1
	
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
IF @CompAvg = 1
	SELECT @PeriodEnd = MAX(PeriodEnd)
	FROM Submissions WHERE RefineryID IN ('330EUR','331EUR','154FL') AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth
ELSE
	SELECT @PeriodEnd = PeriodEnd
	FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF @CompAvg = 1
BEGIN
	IF EXISTS (SELECT * FROM Submissions WHERE RefineryID IN ('330EUR','331EUR','154FL') AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND CalcsNeeded IS NOT NULL)
		RETURN 2
END
ELSE BEGIN
	IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
		RETURN 2
END

SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	ProcessUtilPcnt real NULL, 
	EII real NULL, 
	OpAvail real NULL, 
	MechAvail real NULL, 
	MaintEffIndex real NULL,
	mPersEffIndex real NULL, 
	OpExUEdc real NULL,
	NEOpExEdc real NULL
)
DECLARE @msgString varchar(255)
SELECT @msgString = CAST(@PeriodEnd as varchar(20))

IF @CompAvg = 1
BEGIN
	DECLARE @GetYear smallint, @GetMonth smallint, @GetMonthStart smalldatetime, @GetMonthEnd smalldatetime, @SubList dbo.SubmissionIDList
	SELECT @GetYear = @PeriodYear, @GetMonth = @PeriodMonth
	
	DECLARE @cnt tinyint = 0
	WHILE @cnt < 12
	BEGIN
		SET @GetMonth = @GetMonth - 1
		IF @GetMonth = 0
			SELECT @GetMonth = 12, @GetYear = @GetYear - 1
		SELECT @GetMonthStart = dbo.BuildDate(@GetYear, @GetMonth, 1)
		SELECT @GetMonthEnd = DATEADD(M, 1, @GetMonthStart)
		
		DELETE FROM @SubList
		
		INSERT @SubList(SubmissionID) 
		SELECT SubmissionID 
		FROM Submissions WHERE UseSubmission = 1 AND PeriodYear = @GetYear AND PeriodMonth = @GetMonth 
			AND ((RefineryID IN ('330EUR','331EUR') AND DataSet = 'Actual') OR (RefineryID = '154FL' AND DataSet = 'Fuels'))

		IF (SELECT COUNT(*) FROM @SubList) = 3
			INSERT @Data(PeriodStart, PeriodEnd, EII, ProcessUtilPcnt, MechAvail, OpAvail, MaintEffIndex, mPersEffIndex, NEOpExEdc, OpExUEdc)
			SELECT @GetMonthStart, @GetMonthEnd, EII, ProcessUtilPcnt, MechAvail, OpAvail, MaintEffIndex, mPersEffIndex, NEOpExEdc, TotCashOpExUEdc
			FROM dbo.CalcBashneftKPIs2(@SubList, @FactorSet, @Scenario, @Currency, @UOM) m
		
		SET @cnt = @cnt + 1
	END
END
ELSE
	--- Everything Already Available in GenSum
	INSERT INTO @Data (PeriodStart, PeriodEnd, EII, ProcessUtilPcnt, MechAvail, OpAvail, MaintEffIndex, mPersEffIndex, NEOpExEdc, OpExUEdc)
	SELECT PeriodStart, PeriodEnd, EII, ProcessUtilPcnt, MechAvail, OpAvail, MaintEffIndex, mPersEffIndex, NEOpExEdc, OpExUEdc
	FROM dbo.GetCommonProfileLiteChartData(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM, 12)

IF (SELECT COUNT(*) FROM @Data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = MIN(PeriodStart) FROM @Data
	IF @Period IS NULL SET @Period = @PeriodEnd
	WHILE (SELECT COUNT(*) FROM @Data) < 12
	BEGIN
		SELECT @Period = DATEADD(mm, 1, @Period)
		IF NOT EXISTS (SELECT * FROM @Data WHERE PeriodStart = @Period)
			INSERT @Data (PeriodStart) VALUES (@Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, ProcessUtilPcnt, EII, OpAvail, MechAvail, MaintEffIndex, mPersEffIndex, OpExUEdc, NEOpExEdc
FROM @Data
ORDER BY PeriodStart ASC



