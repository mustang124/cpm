﻿CREATE    PROC [dbo].[spMarginCalc](@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @Scenario Scenario, @Currency CurrencyCode, @PeriodStart smalldatetime, @FractionOfYear real
DECLARE @ProdValue real, @RawMatCost real, @OthRev real, @CashOpEx real, @GrossMargin real, @CashMargin real
DECLARE @NetInputBbl real, @Inven real, @WorkingCptl real

SELECT @PeriodStart = PeriodStart, @FractionOfYear = FractionOfYear FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
SELECT @NetInputBbl = NetInputBbl FROM MaterialTot WHERE SubmissionID = @SubmissionID
SELECT @Inven = Inven FROM InventoryTot WHERE SubmissionID = @SubmissionID

DELETE FROM MarginCalc WHERE SubmissionID = @SubmissionID 
DELETE FROM ROICalc WHERE SubmissionID = @SubmissionID

DECLARE cCases CURSOR LOCAL FORWARD_ONLY READ_ONLY
FOR
SELECT Scenario, Currency, ProdValueK = ProdValue*1000, RawMatCostK = RawMatCost*1000 
FROM MaterialTotCost WHERE SubmissionID = @SubmissionID
OPEN cCases
FETCH NEXT FROM cCases INTO @Scenario, @Currency, @ProdValue, @RawMatCost
WHILE @@FETCH_STATUS = 0
BEGIN
	IF EXISTS (SELECT * FROM OpExAll WHERE SubmissionID = @SubmissionID AND DataType = 'ADJ' AND Currency = @Currency AND Scenario = @Scenario)
		SELECT @OthRev = ISNULL(OthRevenue, 0), @CashOpEx = TotCashOpEx
		FROM OpExAll WHERE SubmissionID = @SubmissionID AND DataType = 'ADJ' AND Currency = @Currency AND Scenario = @Scenario
	ELSE
		SELECT @OthRev = ISNULL(OthRevenue, 0), @CashOpEx = TotCashOpEx
		FROM OpExAll WHERE SubmissionID = @SubmissionID AND DataType = 'ADJ' AND Currency = @Currency AND Scenario = 'CLIENT'

	SELECT @GrossMargin = @ProdValue - @RawMatCost
	SELECT @CashMargin = @GrossMargin + @OthRev - @CashOpEx

	INSERT INTO MarginCalc(SubmissionID, Scenario, Currency, DataType, GPV, RMC, GrossMargin, OthRev, CashOpEx, CashMargin, Divisor)
	VALUES (@SubmissionID, @Scenario, @Currency, 'ADJ', @ProdValue, @RawMatCost, @GrossMargin, @OthRev, @CashOpEx, @CashMargin, 1)
	IF @NetInputBbl > 0
	BEGIN
		INSERT INTO MarginCalc(SubmissionID, Scenario, Currency, DataType, GPV, RMC, GrossMargin, OthRev, CashOpEx, CashMargin, Divisor)
		SELECT 	@SubmissionID, @Scenario, @Currency, 'Bbl', @ProdValue*1000/@NetInputBbl, @RawMatCost*1000/@NetInputBbl, 
			@GrossMargin*1000/@NetInputBbl, @OthRev*1000/@NetInputBbl, @CashOpEx*1000/@NetInputBbl, @CashMargin*1000/@NetInputBbl, @NetInputBbl/1000

		SELECT 	@WorkingCptl = ISNULL(@Inven*(@RawMatCost/@NetInputBbl/1000), 0)
	END


	INSERT INTO ROICalc (SubmissionID, Scenario, Currency, FactorSet, RV, WorkingCptl, VAI, CMI, NetOperVAI, GrossOperVAI)
	SELECT @SubmissionID, @Scenario, @Currency, FactorSet, 
	RV = RV*dbo.ExchangeRate('USD',@Currency,@PeriodStart), @WorkingCptl, 
	VAI = CASE WHEN (f.UEdc*s.NumDays) > 0 THEN @GrossMargin*1000*100/(f.UEdc*s.NumDays) END,
	CMI = CASE WHEN (f.UEdc*s.NumDays) > 0 THEN @CashMargin*1000*100/(f.UEdc*s.NumDays) END,
	NetOperVAI = CASE WHEN (@RawMatCost+@CashOpEx) > 0 THEN 100*@ProdValue/(@RawMatCost+@CashOpEx) END,
	GrossOperVAI = CASE WHEN @RawMatCost > 0 THEN 100*@ProdValue/@RawMatCost END
	FROM FactorTotCalc f INNER JOIN SubmissionsAll s ON s.SubmissionID = f.SubmissionID
	WHERE f.SubmissionID = @SubmissionID

	UPDATE ROICalc
	SET 	TotCptl = RV + WorkingCptl, 
		RVCptlRatio = 100*RV/(RV + WorkingCptl),
		ROI = 100*(@CashMargin/1000)/((RV + WorkingCptl)*@FractionOfYear), 
		RORV = 100*(@CashMargin/1000)/(RV*@FractionOfYear)
	WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Currency = @Currency AND RV > 0

	FETCH NEXT FROM cCases INTO @Scenario, @Currency, @ProdValue, @RawMatCost
END
CLOSE cCases
DEALLOCATE cCases

