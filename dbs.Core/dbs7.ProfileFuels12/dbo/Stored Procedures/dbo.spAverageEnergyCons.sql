﻿CREATE PROC [dbo].[spAverageEnergyCons](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @KBTUPerBbl real OUTPUT, @MJPerBbl real OUTPUT)
AS
SELECT @KBTUPerBbl = CASE WHEN SUM(m.NetInputBbl)>0 THEN SUM(e.TotEnergyConsMBTU)/SUM(m.NetInputBbl/1000) ELSE NULL END
FROM MaterialTot m INNER JOIN EnergyTot e ON e.SubmissionID = m.SubmissionID
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND PeriodStart>=@PeriodStart AND PeriodStart < @PeriodEnd

SELECT @MJPerBbl = @KBTUPerBbl * 1.055


