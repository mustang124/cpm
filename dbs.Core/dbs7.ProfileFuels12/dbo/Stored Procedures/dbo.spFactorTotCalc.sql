﻿CREATE             PROC [dbo].[spFactorTotCalc] (@SubmissionID int)
AS

SET NOCOUNT ON
DELETE FROM FactorProcessCalc WHERE SubmissionID = @SubmissionID

DECLARE @RefineryType varchar(5)
SELECT @RefineryType = dbo.GetRefineryType(SubmissionsAll.RefineryID)
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID

IF EXISTS (SELECT * FROM MultFactors WHERE FactorSet IN (SELECT FactorSet FROM FactorSets WHERE Calculate = 'Y'))
BEGIN
	SELECT f.FactorSet, c.ProcessID, f.MultGroup, c.Cap, f.EdcNoMult
	INTO #UnitsForMult
	FROM FactorCalc f INNER JOIN Config c ON c.SubmissionID = f.SubmissionID AND c.UnitID = f.UnitID
	WHERE f.SubmissionID = @SubmissionID AND MultGroup <> '' AND c.Cap > 0 AND c.InServicePcnt > 50
	
	SELECT FactorSet, ProcessID, MultGroup, COUNT(*) AS NumUnits, SUM(Cap) AS Cap, SUM(EdcNoMult) AS EdcNoMult, MultiFactor = CAST(1 AS real)
	INTO #MultGroups
	FROM #UnitsForMult
	GROUP BY FactorSet, ProcessID, MultGroup
	
	DECLARE @FactorSet FactorSet, @ProcessID ProcessID, @MultGroup varchar(8), 
		@NumUnits smallint, @Cap real, @MultiFactor real
	DECLARE cMultGroups CURSOR LOCAL FORWARD_ONLY FOR
	SELECT FactorSet, ProcessID, MultGroup, NumUnits, Cap FROM #MultGroups
	WHERE NumUnits > 1
	FOR UPDATE OF MultiFactor
	OPEN cMultGroups
	FETCH NEXT FROM cMultGroups INTO @FactorSet, @ProcessID, @MultGroup, @NumUnits, @Cap
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @NumUnits = MAX(NumUnits) FROM MultFactors
		WHERE FactorSet = @FactorSet AND MultGroup = @MultGroup AND NumUnits <= @NumUnits

		SELECT @MultiFactor = 1

		SELECT @MultiFactor = 0.5*(SUM(m.MaintFactor*POWER(u.Cap, 1+m.MaintExp))/(m1.MaintFactor*POWER(@Cap, 1+m1.MaintExp))
			+ SUM(m.PersFactor*POWER(u.Cap, 1+m.PersExp))/(m1.PersFactor*POWER(@Cap, 1+m1.PersExp)))
		FROM MultFactors m INNER JOIN MultFactors m1 
			ON m.FactorSet = m1.FactorSet AND m.MultGroup = m1.MultGroup AND m1.NumUnits = 1
		INNER JOIN #UnitsForMult u ON u.FactorSet = m.FactorSet AND u.MultGroup = m.MultGroup
		WHERE m.FactorSet = @FactorSet AND m.MultGroup = @MultGroup AND m.NumUnits = @NumUnits
		GROUP BY m1.MaintFactor, m1.MaintExp, m1.PersFactor, m1.PersExp

		SELECT @MultiFactor = CASE WHEN @MultiFactor < MinMultiplicity THEN MinMultiplicity 
					WHEN @MultiFactor > MaxMultiplicity THEN MaxMultiplicity 
					ELSE @MultiFactor END
		FROM MultLimits
		WHERE FactorSet = @FactorSet AND MultGroup = @MultGroup

		UPDATE #MultGroups
		SET MultiFactor = @MultiFactor
		WHERE CURRENT OF cMultGroups

		FETCH NEXT FROM cMultGroups INTO @FactorSet, @ProcessID, @MultGroup, @NumUnits, @Cap
	END
	CLOSE cMultGroups
	DEALLOCATE cMultGroups
	
	UPDATE FactorCalc
	SET MultiFactor = ISNULL((SELECT MultiFactor FROM #MultGroups m WHERE m.FactorSet = FactorCalc.FactorSet AND m.ProcessID = FactorCalc.ProcessID AND m.MultGroup = FactorCalc.MultGroup), 1)
	WHERE SubmissionID = @SubmissionID
	
	DROP TABLE #UnitsForMult
	DROP TABLE #MultGroups
END
ELSE
	UPDATE FactorCalc
	SET MultiFactor = 1
	WHERE SubmissionID = @SubmissionID

INSERT INTO FactorProcessCalc(SubmissionID, FactorSet, ProcessID, EdcNoMult, UEdcNoMult, MultiFactor, Edc, UEdc, UtilPcnt, 
	NEOpExEffDiv, MaintEffDiv, PersEffDiv, NonMaintPersEffDiv, MaintPersEffDiv, RV, RVBbl, StdEnergy, StdGain, YearsOper, 
	NonProratedEdc, PlantEdc, NonProratedPlantEdc, NonProratedNEOpExEffDiv, PlantNEOpExEffDiv, NonProratedPlantNEOpExEffDiv, 
	NonProratedMaintEffDiv, PlantMaintEffDiv, NonProratedPlantMaintEffDiv, NonProratedPersEffDiv, PlantPersEffDiv, NonProratedPlantPersEffDiv, 
	NonProratedNonMaintPersEffDiv, PlantNonMaintPersEffDiv, NonProratedPlantNonMaintPersEffDiv, 
	NonProratedMaintPersEffDiv, PlantMaintPersEffDiv, NonProratedPlantMaintPersEffDiv, 
	NonProratedRV, PlantRV, NonProratedPlantRV, UtilOSTA, ConvEdc, ConvUEdc, ConvPcnt)
SELECT @SubmissionID, pg.FactorSet, ProcessID = pg.ProcessGrouping,
	SUM(EdcNoMult), SUM(UEdcNoMult), ISNULL([$(GlobalDB)].dbo.WtAvg(u.MultiFactor, EdcNoMult), 1), SUM(EdcNoMult*u.MultiFactor), SUM(UEdcNoMult*u.MultiFactor), 
	CASE WHEN SUM(EdcNoMult) > 0 THEN SUM(UEdcNoMult)/SUM(EdcNoMult)*100 WHEN SUM(Cap) > 0 THEN SUM(UtilCap)/SUM(Cap)*100 END, 
	SUM(NEOpExEffDiv), SUM(MaintEffDiv), SUM(PersEffDiv), SUM(NonMaintPersEffDiv), SUM(MaintPersEffDiv), SUM(RV), CASE WHEN SUM(ActualCap) > 0 THEN SUM(RV)*1000000/SUM(ActualCap) END, 
	SUM(StdEnergy), SUM(StdGain), CASE WHEN SUM(NonProratedPlantEdc) > 0 THEN [$(GlobalDB)].dbo.WtAvgNZ(YearsOper,NonProratedPlantEdc) WHEN SUM(ActualCap) > 0 THEN [$(GlobalDB)].dbo.WtAvgNZ(YearsOper, ActualCap) ELSE AVG(YearsOper) END,
	SUM(NonProratedEdc*u.MultiFactor), SUM(PlantEdc*u.MultiFactor), SUM(NonProratedPlantEdc*u.MultiFactor), 
	SUM(NonProratedNEOpExEffDiv), SUM(PlantNEOpExEffDiv), SUM(NonProratedPlantNEOpExEffDiv), 
	SUM(NonProratedMaintEffDiv), SUM(PlantMaintEffDiv), SUM(NonProratedPlantMaintEffDiv), 
	SUM(NonProratedPersEffDiv), SUM(PlantPersEffDiv), SUM(NonProratedPlantPersEffDiv), 
	SUM(NonProratedNonMaintPersEffDiv), SUM(PlantNonMaintPersEffDiv), SUM(NonProratedPlantNonMaintPersEffDiv), 
	SUM(NonProratedMaintPersEffDiv), SUM(PlantMaintPersEffDiv), SUM(NonProratedPlantMaintPersEffDiv), 
	SUM(NonProratedRV), SUM(PlantRV), SUM(NonProratedPlantRV), 
	[$(GlobalDB)].dbo.WtAvg(UtilOSTA, EdcNoMult), 
	/*[$(GlobalDB)].dbo.WtAvg(UtilPcntOSSD, EdcNoMult/OSSDFactor), */
	SUM(ConvEdc*u.MultiFactor), SUM(ConvUEdc*u.MultiFactor), /*SUM(ConvEdc*u.MultiFactor*ConvUtilOSTA/100),*/
	CASE WHEN SUM(EdcNoMult) > 0 THEN SUM(ConvEdc)/SUM(EdcNoMult)*100 END
	/*, MFUEdc = SUM(MFUEdcNoMult*u.MultiFactor),*/ /*, OnStreamEdc = SUM(EdcNoMult*u.MultiFactor/OSSDFactor)*/
	/*, UPC_ProcessUEdcNM = SUM(CASE WHEN InUPC = 1 THEN EdcNoMult*UtilPcnt/100 ELSE 0 END)*/
	/*, IsProcessUnit = CASE WHEN SUM(CASE WHEN u.IsProcessUnit = 1 THEN 1 ELSE 0 END) > 0 THEN CASE WHEN SUM(CASE WHEN u.IsProcessUnit = 0 THEN 1 ELSE 0 END) > 0 THEN NULL ELSE 1 END ELSE 0 END*/
FROM dbo.RefProcessGroupings pg LEFT JOIN FactorCalc u ON u.SubmissionID = pg.SubmissionID AND u.FactorSet = pg.FactorSet AND u.UnitID = pg.UnitID
LEFT JOIN (SELECT UnitID, Cap, UtilCap, ActualCap, YearsOper FROM Config WHERE SubmissionID = @SubmissionID
	UNION  SELECT UnitID, BPD, BPD,     BPD,       NULL      FROM ConfigRS WHERE SubmissionID = @SubmissionID) c ON c.UnitID = pg.UnitID
WHERE pg.SubmissionID = @SubmissionID
GROUP BY pg.FactorSet, pg.ProcessGrouping

UPDATE FactorProcessCalc
SET UtilPcnt = CASE WHEN EdcNoMult > 0 THEN UEdcNoMult/EdcNoMult * 100 END
WHERE SubmissionID = @SubmissionID

UPDATE FactorProcessCalc
SET EdcNoMult = ISNULL(EdcNoMult, 0), Edc = ISNULL(Edc, 0), UEdc = ISNULL(UEdc, 0), NEOpExEffDiv = ISNULL(NEOpExEffDiv,0), MaintEffDiv = ISNULL(MaintEffDiv,0), PersEffDiv = ISNULL(PersEffDiv,0), RV = ISNULL(RV,0), StdEnergy = ISNULL(StdEnergy,0), StdGain = ISNULL(StdGain,0)
	, NonProratedEdc = ISNULL(NonProratedEdc,0), PlantEdc = ISNULL(PlantEdc,0), NonProratedPlantEdc = ISNULL(NonProratedPlantEdc,0)
	, NonProratedNEOpExEffDiv = ISNULL(NonProratedNEOpExEffDiv,0), PlantNEOpExEffDiv = ISNULL(PlantNEOpExEffDiv,0), NonProratedPlantNEOpExEffDiv = ISNULL(NonProratedPlantNEOpExEffDiv,0)
	, NonProratedMaintEffDiv = ISNULL(NonProratedMaintEffDiv,0), PlantMaintEffDiv = ISNULL(PlantMaintEffDiv,0), NonProratedPlantMaintEffDiv = ISNULL(NonProratedPlantMaintEffDiv,0)
	, NonProratedPersEffDiv = ISNULL(NonProratedPersEffDiv,0), PlantPersEffDiv = ISNULL(PlantPersEffDiv,0), NonProratedPlantPersEffDiv = ISNULL(NonProratedPlantPersEffDiv,0)
	, NonProratedRV = ISNULL(NonProratedRV,0), PlantRV = ISNULL(PlantRV,0), NonProratedPlantRV = ISNULL(NonProratedPlantRV,0)
	, ConvEdc = ISNULL(ConvEdc,0), ConvUEdc = ISNULL(ConvUEdc,0)
	, NonMaintPersEffDiv = ISNULL(NonMaintPersEffDiv,0), NonProratedNonMaintPersEffDiv = ISNULL(NonProratedNonMaintPersEffDiv,0), PlantNonMaintPersEffDiv = ISNULL(PlantNonMaintPersEffDiv,0), NonProratedPlantNonMaintPersEffDiv = ISNULL(NonProratedPlantNonMaintPersEffDiv,0)
	, MaintPersEffDiv = ISNULL(MaintPersEffDiv,0), NonProratedMaintPersEffDiv = ISNULL(NonProratedMaintPersEffDiv,0), PlantMaintPersEffDiv = ISNULL(PlantMaintPersEffDiv,0), NonProratedPlantMaintPersEffDiv = ISNULL(NonProratedPlantMaintPersEffDiv,0)
WHERE SubmissionID = @SubmissionID AND ProcessID IN (SELECT ProcessGrouping FROM ProcessGrouping_LU WHERE ProcessGrouping IS NOT NULL)

DELETE FROM FactorTotCalc WHERE SubmissionID = @SubmissionID

DECLARE @Edc real, @UEdc real, @UtilPcnt real, @Complexity real, @EdcNoMult real, @NonProratedEdc real, 
	@InServicePcnt real, @PlantEdc real, @AllocPcntOfTime real, @NonProratedPlantEdc real, 
	@TotProcessEdc real, @TotProcessUEdc real, @TotProcessUtilPcnt real, @TotProcessComp real, @TotProcessEdcNoMult real, @PlantTotProcessEdc real, @NonProRatedProcessEdc real, 
	@TotRSBPSD real, @TotRSBPCD real, @TotRSEdc real, @TotRSUEdc real, @VACEdc real,
	@RV real, @TotProcessRV real, @ProcessOffsitesRV real, @NonProcessOffsitesRV real, @CatChemRV real, @SparePartsRV real, @NonProRatedRV real, @NonProRatedProcessRV real, 
	@PlantRV real, @PlantTotProcessRV real, @PlantProcessOffsitesRV real, @PlantNonProcessOffsitesRV real, @PlantCatChemRV real, @PlantSparePartsRV real, @NonProratedPlantRV real, 
	@EII real, @EnergyUseDay real, @TotStdEnergy real, @SensHeatUtilCap real, @SensHeatStdEnergy real, @TotSensHeatPcnt real, 
	@OffsitesUtilCap real, @OffsitesStdEnergy real, @TotOffsitesPcnt real, @AspUtilCap real, @AspStdEnergy real, @TotAspPcnt real, 
	@EstGain real, @ReportLossGain real, @VEI real, @PEI real, @RefAge real, 
	@UPC real, @UPC_ProcessUEdcNM real, @OthProcessEdc real, @NumDays real, @UEdcForUtil real
DECLARE @NetInputBbl real, @AvgInputBPD real, @LWBbl real, @LWSBbl real, @TotInputBbl real
DECLARE @CDUCap real, @VACCap real, @cmplxCap real, @NPCmplx real, @PlantCmplx real, @NPPlantCmplx real, @CrudeAPI real
DECLARE @rvProcOffsite real, @rvNonProcOffsite real, @rvCatChem real, @rvSpareParts real, @rvTotal real, @PostRVComplexityMessage bit
DECLARE @RefStorage real

SELECT @NumDays = NumDays FROM SubmissionsAll WHERE SubmissionID = @SubmissionID

SELECT 	@TotRSBPSD = SUM(BPD), @TotRSBPCD = SUM(BPD)
FROM ConfigRS
WHERE SubmissionID = @SubmissionID

SELECT 	@NetInputBbl = NetInputBbl, @ReportLossGain = GainBbl, @LWBbl = LWBbl, @LWSBbl = LWSBbl, @TotInputbbl = TotInputBbl 
FROM MaterialTot WHERE SubmissionID = @SubmissionID

SELECT 	@EnergyUseDay = TotEnergyConsMBTU/@NumDays FROM EnergyTot WHERE SubmissionID = @SubmissionID

SELECT	@AspUtilCap = ISNULL(SUM(Bbl)/@NumDays, 0)
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'ASP'

SELECT	@CrudeAPI = AvgGravity FROM CrudeTot WHERE SubmissionID = @SubmissionID

SELECT @AvgInputBPD = AnnInputBbl/365.0
FROM EdcStabilizers
WHERE SubmissionID = @SubmissionID 

IF @AvgInputBPD IS NULL
	SELECT @AvgInputBPD = TotInputBbl/@NumDays FROM MaterialTot
	WHERE SubmissionID = @SubmissionID

SELECT @RefStorage = InventoryTot.FuelsStorage FROM InventoryTot WHERE SubmissionID = @SubmissionID

-- Sensible Heat Criteria based on configuration
SELECT S.CriteriaNum, SUM(C.Cap) TotCap
INTO #shcrit
FROM Config C, SensHeatCriteria S
WHERE C.ProcessID = S.ProcessID AND C.SubmissionID = @SubmissionID
AND (C.ProcessType = S.ProcessType OR S.ProcessType = '')
GROUP BY S.CriteriaNum

INSERT INTO FactorTotCalc (SubmissionID, FactorSet, Edc, UEdc, EdcNoMult, NonProratedEdc, PlantEdc, NonProratedPlantEdc, 
TotProcessEdc, TotProcessUEdc, TotProcessEdcNoMult, PlantTotProcessEdc, NonProRatedProcessEdc, OthProcessEdc, 
TotRSBPSD, TotRSBPCD, TotRSEdc, TotRSUEdc, TotProcessRV, PlantTotProcessRV, NonProRatedProcessRV, 
RV, NonProRatedRV, PlantRV, NonProratedPlantRV, 
TotStdEnergy, EstGain, RefAge,
NEOpExEffDiv, NonProratedNEOpExEffDiv, PlantNEOpExEffDiv, NonProratedPlantNEOpExEffDiv, 
MaintEffDiv, NonProratedMaintEffDiv, PlantMaintEffDiv, NonProratedPlantMaintEffDiv, 
PersEffDiv, NonProratedPersEffDiv, PlantPersEffDiv, NonProratedPlantPersEffDiv, 
MaintPersEffDiv, NonProratedMaintPersEffDiv, PlantMaintPersEffDiv, NonProratedPlantMaintPersEffDiv, 
NonMaintPersEffDiv, NonProratedNonMaintPersEffDiv, PlantNonMaintPersEffDiv, NonProratedPlantNonMaintPersEffDiv, 
TotRSNEOpExEffDiv, TotRsMaintEffDiv, TotRsPersEffDiv, TotRsMaintPersEffDiv, TotRsNonMaintPersEffDiv, 
OthProcessNEOpExEffDiv, OthProcessMaintEffDiv, OthProcessPersEffDiv, OthProcessMaintPersEffDiv, OthProcessNonMaintPersEffDiv)
SELECT t.SubmissionID, t.FactorSet, t.Edc, t.UEdc, t.EdcNoMult, t.NonProratedEdc, t.PlantEdc, t.NonProratedPlantEdc,
	p.Edc, p.UEdc, p.EdcNoMult, p.PlantEdc, p.NonProratedEdc, o.Edc, 
	@TotRSBPSD, @TotRSBPCD, rs.Edc, rs.UEdc, p.RV, p.PlantRV, p.NonProratedRV, 
	t.RV, t.NonProratedRV, t.PlantRV, t.NonProratedPlantRV, 
	t.StdEnergy, t.StdGain*@NumDays, t.YearsOper,
	t.NEOpExEffDiv, t.NonProratedNEOpExEffDiv, t.PlantNEOpExEffDiv, t.NonProratedPlantNEOpExEffDiv, 
	t.MaintEffDiv, t.NonProratedMaintEffDiv, t.PlantMaintEffDiv, t.NonProratedPlantMaintEffDiv, 
	t.PersEffDiv, t.NonProratedPersEffDiv, t.PlantPersEffDiv, t.NonProratedPlantPersEffDiv, 
	t.MaintPersEffDiv, t.NonProratedMaintPersEffDiv, t.PlantMaintPersEffDiv, t.NonProratedPlantMaintPersEffDiv, 
	t.NonMaintPersEffDiv, t.NonProratedNonMaintPersEffDiv, t.PlantNonMaintPersEffDiv, t.NonProratedPlantNonMaintPersEffDiv, 
	rs.NEOpExEffDiv, rs.MaintEffDiv, rs.PersEffDiv, rs.MaintPersEffDiv, rs.NonMaintPersEffDiv, 
	o.NEOpExEffDiv, o.MaintEffDiv, o.PersEffDiv, o.MaintPersEffDiv, o.NonMaintPersEffDiv
FROM FactorProcessCalc t LEFT JOIN FactorProcessCalc p ON p.SubmissionID = t.SubmissionID AND p.FactorSet = t.FactorSet AND p.ProcessID = 'TotProc'
LEFT JOIN FactorProcessCalc o ON o.SubmissionID = t.SubmissionID AND o.FactorSet = t.FactorSet AND o.ProcessID = 'OthProc'
LEFT JOIN FactorProcessCalc rs ON rs.SubmissionID = t.SubmissionID AND rs.FactorSet = t.FactorSet AND rs.ProcessID = 'TotRS'
WHERE t.SubmissionID = @SubmissionID AND t.ProcessID = 'TotRef'

SELECT @CDUCap = SUM(CASE WHEN ProcessID = 'CDU' THEN InServiceCap ELSE 0 END),
@VACCap = SUM(CASE WHEN ProcessID = 'VAC' THEN InServiceCap ELSE 0 END)
FROM ProcessTotCalc
WHERE SubmissionID = @SubmissionID AND ProcessID IN ('CDU','VAC')

DECLARE cFactorSets CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
	SELECT 	FactorSet, Edc, TotProcessEdc, NonProratedEdc, PlantEdc, NonProratedPlantEdc,
		RV, NonProratedRV, PlantRV, NonProratedPlantRV, TotStdEnergy, EstGain,
		VACEdc = ISNULL((SELECT Edc FROM FactorProcessCalc v WHERE v.FactorSet = FactorTotCalc.FactorSet AND v.ProcessID = 'VAC' AND v.SubmissionID = @SubmissionID), 0)
	FROM FactorTotCalc WHERE SubmissionID = @SubmissionID
OPEN cFactorSets
FETCH NEXT FROM cFactorSets INTO @FactorSet, @Edc, @TotProcessEdc, @NonProratedEdc, @PlantEdc, @NonProratedPlantEdc, 
		@RV, @NonProratedRV, @PlantRV, @NonProratedPlantRV, @TotStdEnergy, @EstGain, @VACEdc
WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @TnkStdCap real, @TnkStdEdc real
	DECLARE @PurElecUCap real, @PurStmUCap real, @PurElecUEdc real, @PurStmUEdc real
	DECLARE @FactorConstant real, @FactorExponent real, @MinCap real, @MaxCap real, @Factor real

	SELECT @UEdcForUtil = NULL, @PurElecUEdc = 0, @PurStmUEdc = 0, @TnkStdCap = 0, @Factor = NULL, @TnkStdEdc = NULL
	SELECT @FactorConstant = NULL, @FactorExponent = NULL, @MinCap = NULL, @MaxCap = NULL
	SELECT @PurStmUCap = 0, @PurElecUCap = 0, @PurStmUEdc = 0, @PurElecUEdc = 0
	
	SELECT @TnkStdCap = TnkStdCapBPDConstant*POWER(@AvgInputBPD, TnkStdCapBPDExponent)
			+ TnkStdCapPEdcConstant*POWER(@TotProcessEdc, TnkStdCapPEdcExponent)
			+ ISNULL(TnkStdCapPEdcxVACConstant * power(@TotProcessEdc-ISNULL(@VACEdc,0), TnkStdCapPEdcxVACExponent) * CASE WHEN @VACEdc>0 THEN LIVTnkStdMult ELSE 1 END, 0)
	FROM FactorSets WHERE FactorSet = @FactorSet AND RefineryType = @RefineryType

	SELECT @FactorConstant = EdcConstant, @FactorExponent = EdcExponent, @MinCap = EdcMinCap, @MaxCap = EdcMaxCap
	FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'TNKSTD' AND ProcessType = CASE WHEN @RefineryType = 'LUBES' THEN 'LTNK' ELSE 'FTNK' END
	
	EXEC spCalcFactor @TnkStdCap, @FactorConstant, @FactorExponent, @MinCap, @MaxCap, @Factor OUTPUT, @TnkStdEdc OUTPUT
	SELECT @TnkStdEdc = ISNULL(@TnkStdEdc, 0)

	IF EXISTS (SELECT * FROM FactorSets WHERE FactorSet = @FactorSet AND RefineryType = @RefineryType AND CalcPurElecStmUEdc = 'Y')
	BEGIN
		SELECT @PurStmUCap = SUM(ISNULL(SourceMBTU, 0) - ISNULL(UsageMBTU, 0)) *1000/1100/(24*@NumDays) 
		FROM Energy WHERE SubmissionID = @SubmissionID AND EnergyType IN ('STM','LLH') AND TransType IN ('PUR','DST')
		SELECT @PurElecUCap = SUM(ISNULL(SourceMWH, 0) - ISNULL(UsageMWH, 0))*1000/(24*@NumDays) 
		FROM Electric WHERE SubmissionID = @SubmissionID AND EnergyType = 'ELE' AND TransType IN ('PUR','DST')
		SELECT @PurStmUcap = 0 WHERE @PurStmUCap < 0
		SELECT @PurElecUcap = 0 WHERE @PurElecUCap < 0
		SELECT @FactorConstant = EdcConstant, @FactorExponent = EdcExponent, @MinCap = EdcMinCap, @MaxCap = EdcMaxCap
		FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'FTCOGEN' AND ProcessType = 'INDT' AND CapType = 'E'
		EXEC spCalcFactor @PurElecUCap, @FactorConstant, @FactorExponent, NULL, NULL, @Factor OUTPUT, @PurElecUEdc OUTPUT
		SELECT @PurElecUEdc = ISNULL(@PurElecUEdc, 0)
		SELECT @FactorConstant = EdcConstant, @FactorExponent = EdcExponent, @MinCap = EdcMinCap, @MaxCap = EdcMaxCap
		FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'FTCOGEN' AND ProcessType = 'INDT' AND CapType = 'Stm'
		EXEC spCalcFactor @PurStmUCap, @FactorConstant, @FactorExponent, NULL, NULL, @Factor OUTPUT, @PurStmUEdc OUTPUT
		SELECT @PurStmUEdc = ISNULL(@PurStmUEdc, 0)
	END
	ELSE
		SELECT @PurElecUEdc = 0, @PurStmUEdc = 0

	SELECT	@Edc = @Edc + @TnkStdEdc,
		@NonProratedEdc = @NonProratedEdc + @TnkStdEdc, 
		@PlantEdc = @PlantEdc + @TnkStdEdc, 
		@NonProratedPlantEdc = @NonProratedPlantEdc + @TnkStdEdc

	UPDATE FactorTotCalc
	SET TnkStdCap = @TnkStdCap, TnkStdEdc = @TnkStdEdc, 
	PurStmUEdc = @PurStmUEdc, PurElecUEdc = @PurElecUEdc,
	EdcNoMult = EdcNoMult + @TnkStdEdc, Edc = Edc + @TnkStdEdc, 
	UEdc = UEdc + @TnkStdEdc + @PurStmUEdc + @PurElecUEdc, 
	NonProratedEdc = NonProratedEdc + @TnkStdEdc, 
	PlantEdc = PlantEdc + @TnkStdEdc, 
	NonProratedPlantEdc = NonProratedPlantEdc + @TnkStdEdc
	WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

	-- Refinery Complexity
	IF @RefineryType = 'LUBES'
	BEGIN
		IF @LWBbl > 0
		BEGIN
			SELECT @UEdcForUtil = (UEdc-PurElecUEdc-PurStmUEdc)
			FROM FactorTotCalc WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

			SELECT @Complexity = @UEdcForUtil*@NumDays/@LWBbl,
					@TotProcessComp = @TotProcessUEdc*@NumDays/@LWBbl
		END
		ELSE BEGIN
			SELECT @Complexity = (SELECT CmplxDefault FROM FactorSets fs WHERE fs.FactorSet = @FactorSet AND fs.RefineryType = @RefineryType)
			SET @TotProcessComp = @Complexity
		END
		SELECT @NPCmplx = @Complexity, @PlantCmplx = @Complexity, @NPPlantCmplx = @Complexity
	END
	ELSE BEGIN
		IF @CDUCap > 0
			SELECT @cmplxCap = @CDUCap 
		ELSE
			SELECT @cmplxCap = @VACCap*CmplxVacCapMult, @Complexity = CmplxDefault
			FROM FactorSets WHERE RefineryType = @RefineryType AND FactorSet = @FactorSet
			
		IF @cmplxCap > 0
			SELECT 	@Complexity = @Edc/@cmplxCap, @TotProcessComp = @TotProcessEdc/@cmplxCap,
				@NPCmplx = @NonProratedEdc/@cmplxCap, @PlantCmplx = @PlantEdc/@cmplxCap,
				@NPPlantCmplx = @NonProratedPlantEdc/@cmplxCap
		ELSE
			SELECT 	@TotProcessComp = @Complexity, @NPCmplx = @Complexity, 
				@PlantCmplx = @Complexity, @NPPlantCmplx = @Complexity
	END

	-- RV
	EXEC dbo.RVComponents @FactorSet, @RefineryType, @Complexity, @RV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @ProcessOffsitesRV = @rvProcOffsite, @NonProcessOffsitesRV = @rvNonProcOffsite, @CatChemRV = @rvCatChem, @SparePartsRV = @rvSpareParts, @RV = @rvTotal
	-- Non-prorated RV
	EXEC dbo.RVComponents @FactorSet, @RefineryType, @NPCmplx, @NonProratedRV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @NonProratedRV = @rvTotal
	-- Plant replacement values
	EXEC dbo.RVComponents @FactorSet, @RefineryType, @PlantCmplx, @PlantRV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @PlantProcessOffsitesRV = @rvProcOffsite, @PlantNonProcessOffsitesRV = @rvNonProcOffsite, @PlantCatChemRV = @rvCatChem, @PlantSparePartsRV = @rvSpareParts, @PlantRV = @rvTotal
	-- Non-prorated Plant replacement values
	EXEC dbo.RVComponents @FactorSet, @RefineryType, @NPPlantCmplx, @NonProratedPlantRV, @rvProcOffsite OUTPUT, @rvNonProcOffsite OUTPUT, @rvCatChem OUTPUT, @rvSpareParts OUTPUT, @rvTotal OUTPUT, @PostRVComplexityMessage OUTPUT
	SELECT @NonProratedPlantRV = @rvTotal
	
	-- Sensible Heat
	SELECT @SensHeatUtilCap = SUM(Y.Bbl)
	FROM Yield Y, Material_LU L
	WHERE Y.SubmissionID = @SubmissionID AND Category IN ('RMI', 'OTHRM') AND Y.MaterialID <> 'CRD'
	AND Y.MaterialID = L.MaterialID AND L.SensHeatCriteria IS NOT NULL
	AND (L.SensHeatCriteria = 0 OR L.SensHeatCriteria IN (SELECT CriteriaNum FROM #shcrit WHERE #shcrit.TotCap > 0))

	IF EXISTS (SELECT * FROM FactorSets WHERE FactorSet = @FactorSet AND RefineryType = @RefineryType AND CrudeInSensHeat = 'Y')
		SELECT @SensHeatUtilCap = ISNULL(@SensHeatUtilCap, 0) + ISNULL(TotBbl, 0) FROM CrudeTot WHERE SubmissionID = @SubmissionID
	SELECT 	@SensHeatUtilCap = ISNULL(@SensHeatUtilCap/@NumDays, 0)
	SELECT 	@SensHeatStdEnergy = (@SensHeatUtilCap/1000) * (SensHeatConstant + SensHeatAPIFactor* ISNULL(@CrudeAPI, 32)),
		@OffsitesStdEnergy = (@OffsitesUtilCap/1000) * (OffsitesEnergyConstant + OffsitesEnergyCmplxFactor * (CASE WHEN @Complexity > OffsitesEnergyCmplxMax THEN OffsitesEnergyCmplxDef ELSE @Complexity END))
	FROM FactorSets WHERE RefineryType = @RefineryType AND FactorSet = @FactorSet

	-- Offsites Standard Energy
	IF @RefineryType = 'LUBES'
	BEGIN
		SELECT @OffsitesUtilCap = @SensHeatUtilCap
		SELECT @OffsitesStdEnergy = ISNULL(fs.OffsitesEnergyConstant,0) + (ISNULL(fs.OffsitesEnergyLWSFactor*@LWSBbl/@NumDays,0) 
						+ ISNULL(fs.OffsitesEnergyUEdcFactor*@UEdcForUtil, 0) 
						+ ISNULL(fs.OffsitesEnergyStorageFactor*@RefStorage,0))/1000
		FROM FactorSets fs WHERE fs.FactorSet = @FactorSet AND fs.RefineryType = @RefineryType
	END
	ELSE BEGIN
		IF @CDUCap > 0 OR @VACCap > 0
			SELECT @OffsitesUtilCap = @NetInputBbl/@NumDays
		ELSE
			SELECT @OffsitesUtilCap = @SensHeatUtilCap

		SELECT 	@OffsitesStdEnergy = (@OffsitesUtilCap/1000) * (OffsitesEnergyConstant + OffsitesEnergyCmplxFactor * (CASE WHEN @Complexity > OffsitesEnergyCmplxMax THEN OffsitesEnergyCmplxDef ELSE @Complexity END))
		FROM FactorSets WHERE RefineryType = @RefineryType AND FactorSet = @FactorSet
	END  

	-- Asphalt Standard Energy
	IF @AspUtilCap > 0
		SELECT @AspStdEnergy = @AspUtilCap/1000 * CONVERT(real, EIIFormula)
		FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = 'ASP'
	ELSE
		SELECT @AspStdEnergy = 0
		
	-- Total Standard Energy
	SELECT @TotStdEnergy = ISNULL(@TotStdEnergy, 0) + ISNULL(@SensHeatStdEnergy, 0) + ISNULL(@OffsitesStdEnergy, 0) + ISNULL(@AspStdEnergy, 0)
	IF @TotStdEnergy > 0
		SELECT 	@TotSensHeatPcnt = @SensHeatStdEnergy/@TotStdEnergy*100, 
			@TotOffsitesPcnt = @OffsitesStdEnergy/@TotStdEnergy*100, 
			@TotAspPcnt = @AspStdEnergy/@TotStdEnergy*100, 
			@EII = @EnergyUseDay/@TotStdEnergy*100
	ELSE
		SELECT 	@TotSensHeatPcnt = NULL, @TotOffsitesPcnt = NULL, @TotAspPcnt = NULL, @EII = NULL

	IF NOT EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID IN ('FCC','HYC','COK'))
		SELECT @VEI = NULL
	ELSE IF @EstGain = 0
		SELECT @VEI = NULL
	ELSE
		SELECT @VEI = @ReportLossGain/@EstGain*100

	UPDATE FactorTotCalc
	SET Complexity = @Complexity, TotProcessComp = @TotProcessComp, 
	UtilPcnt = CASE WHEN Edc=0 THEN NULL ELSE (UEdc-PurElecUEdc-PurStmUEdc)/Edc*100 END,
	TotProcessUtilPcnt = CASE WHEN TotProcessEdc=0 THEN NULL ELSE TotProcessUEdc/TotProcessEdc*100 END,
	InServicePcnt = CASE WHEN NonProratedProcessEdc = 0 THEN NULL ELSE TotProcessEdc/NonProratedProcessEdc*100 END,
        AllocPcntOfTime = CASE WHEN PlantEdc = 0 THEN NULL ELSE Edc/PlantEdc*100 END,
	ProcessOffsitesRV = @ProcessOffsitesRV, NonProcessOffsitesRV = @NonProcessOffsitesRV, CatChemRV = @CatChemRV, SparePartsRV = @SparePartsRV, RV = @RV,
	NonProratedRV = @NonProratedRV, NonProratedPlantRV = @NonProratedPlantRV,
	PlantProcessOffsitesRV = @PlantProcessOffsitesRV, PlantNonProcessOffsitesRV = @PlantNonProcessOffsitesRV, PlantCatChemRV = @PlantCatChemRV, PlantSparePartsRV = @PlantSparePartsRV, PlantRV = @PlantRV,
	SensHeatUtilCap = @SensHeatUtilCap, SensHeatStdEnergy = @SensHeatStdEnergy, TotSensHeatPcnt = @TotSensHeatPcnt, 
	OffsitesUtilCap = @OffsitesUtilCap, OffsitesStdEnergy = @OffsitesStdEnergy, TotOffsitesPcnt = @TotOffsitesPcnt, 
	AspUtilCap = @AspUtilCap, AspStdEnergy = @AspStdEnergy, TotAspPcnt = @TotAspPcnt, 
	EII = @EII, EnergyUseDay = @EnergyUseDay, TotStdEnergy = @TotStdEnergy,
	ReportLossGain = @ReportLossGain, EstGain = @EstGain, 
	VEI = @VEI,
	PEI = CASE WHEN @NetInputBbl>0 THEN 100*(@NetInputBbl+@ReportLossGain) / (@NetInputBbl+@EstGain) END
	WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet


	FETCH NEXT FROM cFactorSets INTO @FactorSet, @Edc, @TotProcessEdc, @NonProratedEdc, @PlantEdc, @NonProratedPlantEdc, 
		@RV, @NonProratedRV, @PlantRV, @NonProratedPlantRV, @TotStdEnergy, @EstGain, @VACEdc
END
CLOSE cFactorSets
DEALLOCATE cFactorSets
DROP TABLE #shcrit

UPDATE FactorCalc
SET UnitEII = c.EnergyPcnt*ftc.EnergyUseDay/CASE WHEN ftc.FactorSet = '2004' AND c.ProcessID = 'CDU' AND ftc.SensHeatUtilCap <> 0 THEN FactorCalc.StdEnergy + (c.UtilCap/ftc.SensHeatUtilCap * ftc.SensHeatStdEnergy) ELSE FactorCalc.StdEnergy END
FROM FactorCalc INNER JOIN Config c ON c.SubmissionID = FactorCalc.SubmissionID AND c.UnitID = FactorCalc.UnitID
INNER JOIN FactorTotCalc ftc ON ftc.SubmissionID = FactorCalc.SubmissionID AND ftc.FactorSet = FactorCalc.FactorSet
WHERE FactorCalc.StdEnergy > 0 AND FactorCalc.SubmissionID = @SubmissionID

