﻿CREATE   PROC [dbo].[spReportGazpromCoKPI] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

IF @RefineryID NOT IN ('106FL','150FL','322EUR')
	RETURN 1

/*
106FL = O
150FL = Y
322EUR = M
*/
DECLARE	
	@M_EII real, @M_EII_QTR real, @M_EII_Avg real, 
	@M_RefUtilPcnt real, @M_RefUtilPcnt_QTR real, @M_RefUtilPcnt_Avg real, 
	@M_VEI real, @M_VEI_QTR real, @M_VEI_Avg real, 
	@M_OpAvail real, @M_OpAvail_QTR real, @M_OpAvail_Avg real, 
	@M_RoutIndex real, @M_RoutIndex_QTR real, @M_RoutIndex_Avg real,
	@M_PersIndex real, @M_PersIndex_QTR real, @M_PersIndex_Avg real, 
	@M_NEOpExEdc real, @M_NEOpExEdc_QTR real, @M_NEOpExEdc_Avg real, 
	@M_OpExUEdc real, @M_OpExUEdc_QTR real, @M_OpExUedc_Avg real, 
	@O_EII real, @O_EII_QTR real, @O_EII_Avg real, 
	@O_RefUtilPcnt real, @O_RefUtilPcnt_QTR real, @O_RefUtilPcnt_Avg real, 
	@O_VEI real, @O_VEI_QTR real, @O_VEI_Avg real, 
	@O_OpAvail real, @O_OpAvail_QTR real, @O_OpAvail_Avg real, 
	@O_RoutIndex real, @O_RoutIndex_QTR real, @O_RoutIndex_Avg real,
	@O_PersIndex real, @O_PersIndex_QTR real, @O_PersIndex_Avg real, 
	@O_NEOpExEdc real, @O_NEOpExEdc_QTR real, @O_NEOpExEdc_Avg real, 
	@O_OpExUEdc real, @O_OpExUEdc_QTR real, @O_OpExUedc_Avg real, 
	@Y_EII real, @Y_EII_QTR real, @Y_EII_Avg real, 
	@Y_RefUtilPcnt real, @Y_RefUtilPcnt_QTR real, @Y_RefUtilPcnt_Avg real, 
	@Y_VEI real, @Y_VEI_QTR real, @Y_VEI_Avg real, 
	@Y_OpAvail real, @Y_OpAvail_QTR real, @Y_OpAvail_Avg real, 
	@Y_RoutIndex real, @Y_RoutIndex_QTR real, @Y_RoutIndex_Avg real,
	@Y_PersIndex real, @Y_PersIndex_QTR real, @Y_PersIndex_Avg real, 
	@Y_NEOpExEdc real, @Y_NEOpExEdc_QTR real, @Y_NEOpExEdc_Avg real, 
	@Y_OpExUEdc real, @Y_OpExUEdc_QTR real, @Y_OpExUedc_Avg real 

DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc] '322EUR', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @M_EII OUTPUT, @EII_QTR = @M_EII_QTR OUTPUT, @EII_Avg = @M_EII_Avg OUTPUT, 
	@RefUtilPcnt = @M_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @M_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @M_RefUtilPcnt_Avg OUTPUT, 
	@VEI = @M_VEI OUTPUT, @VEI_QTR = @M_VEI_QTR OUTPUT, @VEI_Avg = @M_VEI_Avg OUTPUT, 
	@OpAvail = @M_OpAvail OUTPUT, @OpAvail_QTR = @M_OpAvail_QTR OUTPUT, @OpAvail_Avg = @M_OpAvail_Avg OUTPUT, 
	@RoutIndex = @M_RoutIndex OUTPUT, @RoutIndex_QTR = @M_RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @M_RoutIndex_Avg OUTPUT,
	@PersIndex = @M_PersIndex OUTPUT, @PersIndex_QTR = @M_PersIndex_QTR OUTPUT, @PersIndex_Avg = @M_PersIndex_Avg OUTPUT, 
	@NEOpExEdc = @M_NEOpExEdc OUTPUT, @NEOpExEdc_QTR = @M_NEOpExEdc_QTR OUTPUT, @NEOpExEdc_Avg = @M_NEOpExEdc_Avg OUTPUT, 
	@OpExUEdc = @M_OpExUEdc OUTPUT, @OpExUEdc_QTR = @M_OpExUEdc_QTR OUTPUT, @OpExUedc_Avg = @M_OpExUedc_Avg OUTPUT

IF @spResult > 1	
	RETURN @spResult

EXEC @spResult = [dbo].[spReportGazpromKPICalc] '106FL', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @O_EII OUTPUT, @EII_QTR = @O_EII_QTR OUTPUT, @EII_Avg = @O_EII_Avg OUTPUT, 
	@RefUtilPcnt = @O_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @O_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @O_RefUtilPcnt_Avg OUTPUT, 
	@VEI = @O_VEI OUTPUT, @VEI_QTR = @O_VEI_QTR OUTPUT, @VEI_Avg = @O_VEI_Avg OUTPUT, 
	@OpAvail = @O_OpAvail OUTPUT, @OpAvail_QTR = @O_OpAvail_QTR OUTPUT, @OpAvail_Avg = @O_OpAvail_Avg OUTPUT, 
	@RoutIndex = @O_RoutIndex OUTPUT, @RoutIndex_QTR = @O_RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @O_RoutIndex_Avg OUTPUT,
	@PersIndex = @O_PersIndex OUTPUT, @PersIndex_QTR = @O_PersIndex_QTR OUTPUT, @PersIndex_Avg = @O_PersIndex_Avg OUTPUT, 
	@NEOpExEdc = @O_NEOpExEdc OUTPUT, @NEOpExEdc_QTR = @O_NEOpExEdc_QTR OUTPUT, @NEOpExEdc_Avg = @O_NEOpExEdc_Avg OUTPUT, 
	@OpExUEdc = @O_OpExUEdc OUTPUT, @OpExUEdc_QTR = @O_OpExUEdc_QTR OUTPUT, @OpExUedc_Avg = @O_OpExUedc_Avg OUTPUT

IF @spResult > 1	
	RETURN @spResult

EXEC @spResult = [dbo].[spReportGazpromKPICalc] '150FL', @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @Y_EII OUTPUT, @EII_QTR = @Y_EII_QTR OUTPUT, @EII_Avg = @Y_EII_Avg OUTPUT, 
	@RefUtilPcnt = @Y_RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @Y_RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @Y_RefUtilPcnt_Avg OUTPUT, 
	@VEI = @Y_VEI OUTPUT, @VEI_QTR = @Y_VEI_QTR OUTPUT, @VEI_Avg = @Y_VEI_Avg OUTPUT, 
	@OpAvail = @Y_OpAvail OUTPUT, @OpAvail_QTR = @Y_OpAvail_QTR OUTPUT, @OpAvail_Avg = @Y_OpAvail_Avg OUTPUT, 
	@RoutIndex = @Y_RoutIndex OUTPUT, @RoutIndex_QTR = @Y_RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @Y_RoutIndex_Avg OUTPUT,
	@PersIndex = @Y_PersIndex OUTPUT, @PersIndex_QTR = @Y_PersIndex_QTR OUTPUT, @PersIndex_Avg = @Y_PersIndex_Avg OUTPUT, 
	@NEOpExEdc = @Y_NEOpExEdc OUTPUT, @NEOpExEdc_QTR = @Y_NEOpExEdc_QTR OUTPUT, @NEOpExEdc_Avg = @Y_NEOpExEdc_Avg OUTPUT, 
	@OpExUEdc = @Y_OpExUEdc OUTPUT, @OpExUEdc_QTR = @Y_OpExUEdc_QTR OUTPUT, @OpExUedc_Avg = @Y_OpExUedc_Avg OUTPUT

IF @spResult > 1	
	RETURN @spResult

SELECT 
	M_EII = @M_EII, M_EII_QTR = @M_EII_QTR, M_EII_Avg = @M_EII_Avg, 
	M_UtilPcnt = @M_RefUtilPcnt, M_UtilPcnt_QTR = @M_RefUtilPcnt_QTR, M_UtilPcnt_Avg = @M_RefUtilPcnt_Avg, 
	M_VEI = @M_VEI, M_VEI_QTR = @M_VEI_QTR, M_VEI_Avg = @M_VEI_Avg, 
	M_OpAvail = @M_OpAvail, M_OpAvail_QTR = @M_OpAvail_QTR, M_OpAvail_Avg = @M_OpAvail_Avg, 
	M_RoutIndex = @M_RoutIndex, M_RoutIndex_QTR = @M_RoutIndex_QTR, M_RoutIndex_Avg = @M_RoutIndex_Avg, 
	M_TotWHrEdc = @M_PersIndex, M_TotWHrEdc_QTR = @M_PersIndex_QTR, M_TotWhrEdc_Avg = @M_PersIndex_Avg, 
	M_NEOpExEdc = @M_NEOpExEdc, M_NEOpExEdc_QTR = @M_NEOpExEdc_QTR, M_NEOpExEdc_Avg = @M_NEOpExEdc_Avg, 
	M_TotCashOpExUEdc = @M_OpExUEdc, M_TotCashOpExUEdc_QTR = @M_OpExUEdc_QTR, M_TotCashOpExUedc_Avg = @M_OpExUedc_Avg,

	O_EII = @O_EII, O_EII_QTR = @O_EII_QTR, O_EII_Avg = @O_EII_Avg, 
	O_UtilPcnt = @O_RefUtilPcnt, O_UtilPcnt_QTR = @O_RefUtilPcnt_QTR, O_UtilPcnt_Avg = @O_RefUtilPcnt_Avg, 
	O_VEI = @O_VEI, O_VEI_QTR = @O_VEI_QTR, O_VEI_Avg = @O_VEI_Avg, 
	O_OpAvail = @O_OpAvail, O_OpAvail_QTR = @O_OpAvail_QTR, O_OpAvail_Avg = @O_OpAvail_Avg, 
	O_RoutIndex = @O_RoutIndex, O_RoutIndex_QTR = @O_RoutIndex_QTR, O_RoutIndex_Avg = @O_RoutIndex_Avg, 
	O_TotWHrEdc = @O_PersIndex, O_TotWHrEdc_QTR = @O_PersIndex_QTR, O_TotWhrEdc_Avg = @O_PersIndex_Avg, 
	O_NEOpExEdc = @O_NEOpExEdc, O_NEOpExEdc_QTR = @O_NEOpExEdc_QTR, O_NEOpExEdc_Avg = @O_NEOpExEdc_Avg, 
	O_TotCashOpExUEdc = @O_OpExUEdc, O_TotCashOpExUEdc_QTR = @O_OpExUEdc_QTR, O_TotCashOpExUedc_Avg = @O_OpExUedc_Avg,

	Y_EII = @Y_EII, Y_EII_QTR = @Y_EII_QTR, Y_EII_Avg = @Y_EII_Avg, 
	Y_UtilPcnt = @Y_RefUtilPcnt, Y_UtilPcnt_QTR = @Y_RefUtilPcnt_QTR, Y_UtilPcnt_Avg = @Y_RefUtilPcnt_Avg, 
	Y_VEI = @Y_VEI, Y_VEI_QTR = @Y_VEI_QTR, Y_VEI_Avg = @Y_VEI_Avg, 
	Y_OpAvail = @Y_OpAvail, Y_OpAvail_QTR = @Y_OpAvail_QTR, Y_OpAvail_Avg = @Y_OpAvail_Avg, 
	Y_RoutIndex = @Y_RoutIndex, Y_RoutIndex_QTR = @Y_RoutIndex_QTR, Y_RoutIndex_Avg = @Y_RoutIndex_Avg, 
	Y_TotWHrEdc = @Y_PersIndex, Y_TotWHrEdc_QTR = @Y_PersIndex_QTR, Y_TotWhrEdc_Avg = @Y_PersIndex_Avg, 
	Y_NEOpExEdc = @Y_NEOpExEdc, Y_NEOpExEdc_QTR = @Y_NEOpExEdc_QTR, Y_NEOpExEdc_Avg = @Y_NEOpExEdc_Avg, 
	Y_TotCashOpExUEdc = @Y_OpExUEdc, Y_TotCashOpExUEdc_QTR = @Y_OpExUEdc_QTR, Y_TotCashOpExUedc_Avg = @Y_OpExUedc_Avg

