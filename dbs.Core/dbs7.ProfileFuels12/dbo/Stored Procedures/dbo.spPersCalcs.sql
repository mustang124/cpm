﻿
CREATE         PROC [dbo].[spPersCalcs] (@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @FractionOfYear real
SELECT	@FractionOfYear = FractionOfYear
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
DECLARE @OCCTAPersId varchar(10), @MPSTAPersId varchar(10)
SELECT @OCCTAPersId = 'OCCTAADJ', @MPSTAPersId = 'MPSTAADJ'
DELETE FROM Pers WHERE PersId IN (@OCCTAPersId,@MPSTAPersId) AND SubmissionID = @SubmissionID

UPDATE Pers SET MaintPcnt = 100
WHERE SectionID = 'MT' AND PersId IN (SELECT PersId FROM Pers_LU WHERE SectionID = 'MT' AND MaintForceGroup IS NOT NULL) AND MaintPcnt IS NULL
AND SubmissionID = @SubmissionID

IF EXISTS (SELECT * FROM Pers WHERE SubmissionID = @SubmissionID AND SectionID = 'MT' AND MaintPcnt > 0)
	UPDATE Pers
	SET MaintPcnt = (SELECT [$(GlobalDB)].dbo.WtAvg(MaintPcnt, TotWHr) FROM Pers d WHERE d.SubmissionID = Pers.SubmissionID AND d.SectionID = Pers.SectionID AND d.PersId <> Pers.PersId)
	WHERE Pers.PersId = 'MPSTS' AND Pers.SubmissionID = @SubmissionID

INSERT Pers (SubmissionID, PersId, SortKey, SectionID, NumPers, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, MaintPcnt)
SELECT d.SubmissionID, lu.PersId, lu.SortKey, lu.SectionID, SUM(NumPers), SUM(STH), SUM(OVTHours), [$(GlobalDB)].dbo.WtAvg(d.OVTPcnt, d.STH), SUM(Contract), SUM(GA), SUM(AbsHrs), [$(GlobalDB)].dbo.WtAvg(CASE WHEN d.PersId IN ('MPSTMNT','MPSTINSP') THEN 100 ELSE 0 END, ISNULL(STH,0)+ISNULL(Contract,0))
FROM Pers d INNER JOIN Pers_LU lu ON lu.SectionID = d.SectionID AND lu.IncludeForInput = 'Y'
WHERE SubmissionID = @SubmissionID
AND NOT EXISTS (SELECT * FROM Pers i WHERE i.SubmissionID = d.SubmissionID AND i.SectionID = d.SectionID AND i.PersId = lu.PersId)
AND d.PersId NOT IN (@OCCTAPersId, @MPSTAPersId) AND lu.DetailStudy = 'S'
GROUP BY d.SubmissionID, lu.PersId, lu.SortKey, lu.SectionID

UPDATE Pers
SET SectionID = (SELECT SectionID FROM Pers_LU WHERE Pers_LU.PersId = Pers.PersId)
WHERE SectionID IS NULL AND SubmissionID = @SubmissionID
DECLARE @TAOCCSTH real, @TAOCCOVT real, @TAOCCCont real,
	@TAMPSSTH real, @TAMPSOVT real, @TAMPSCont real
SELECT 	@TAOCCSTH = SUM(AnnTAOCCSTH), @TAOCCOVT = SUM(AnnTAOCCOVT), @TAOCCCont = SUM(AnnTAContOCCWHr),
	@TAMPSSTH = SUM(AnnTAMPSSTH), @TAMPSOVT = SUM(AnnTAMPSOVT), @TAMPSCont = SUM(AnnTAContMPSWHr)
FROM TAForSubmission
WHERE SubmissionID = @SubmissionID
INSERT INTO Pers (SubmissionID, SectionID, PersId, STH, OVTHours, Contract) 
VALUES (@SubmissionID, 'OM', @OCCTAPersId, @TAOCCSTH*@FractionOfYear, @TAOCCOVT*@FractionOfYear, @TAOCCCont*@FractionOfYear)
INSERT INTO Pers (SubmissionID, SectionID, PersId, STH, OVTPcnt, Contract) 
SELECT @SubmissionID, 'MM', @MPSTAPersId, @TAMPSSTH*@FractionOfYear, CASE WHEN @TAMPSSTH > 0 THEN @TAMPSOVT/@TAMPSSTH*100 END, @TAMPSCont*@FractionOfYear
DECLARE @OCCSTH real, @MPSSTH real
SELECT @OCCSTH = SUM(STH) FROM Pers WHERE SubmissionID = @SubmissionID
AND SectionID IN ('OO','OM','OT','OA') AND PersId <> @OCCTAPersId
SELECT @MPSSTH = SUM(STH) FROM Pers WHERE SubmissionID = @SubmissionID
AND SectionID IN ('MO','MM','MT','MA') AND PersId <> @MPSTAPersId
UPDATE Absence
SET 	TotAbs = ISNULL(OCCAbs, 0) + ISNULL(MPSAbs, 0),
	OCCPcnt = CASE WHEN @OCCSTH > 0 THEN ISNULL(OCCAbs, 0)/@OCCSTH*100 END,
	MPSPcnt = CASE WHEN @MPSSTH > 0 THEN ISNULL(MPSAbs, 0)/@MPSSTH*100 END,
	TotPcnt = CASE WHEN (ISNULL(@OCCSTH, 0)+ISNULL(@MPSSTH,0)) > 0 THEN (ISNULL(OCCAbs, 0) + ISNULL(MPSAbs, 0))/(ISNULL(@OCCSTH, 0)+ISNULL(@MPSSTH,0))*100 END
WHERE SubmissionID = @SubmissionID
DELETE FROM AbsenceTot WHERE SubmissionID = @SubmissionID
INSERT INTO AbsenceTot(SubmissionID, OCCAbs, MPSAbs, TotAbs, OCCPcnt, MPSPcnt, TotPcnt)
SELECT @SubmissionID, SUM(OCCAbs), SUM(MPSAbs), SUM(TotAbs), SUM(OCCPcnt), SUM(MPSPcnt), SUM(TotPcnt)
FROM Absence WHERE SubmissionID = @SubmissionID
UPDATE Pers
SET	AbsHrs = ISNULL(STH*CASE WHEN Pers.SectionID LIKE 'O_' THEN a.OCCPcnt ELSE a.MPSPcnt END/100, 0)
FROM Pers INNER JOIN AbsenceTot a ON a.SubmissionID = Pers.SubmissionID
WHERE Pers.SubmissionID = @SubmissionID 
AND Pers.SectionID IN ('OO','OM','OT','OA','MO','MM','MT','MA')
AND Pers.PersId NOT IN (@OCCTAPersId,@MPSTAPersId)
UPDATE Pers
SET OVTPcnt = CASE WHEN STH > 0 THEN OVTHours/STH*100 END
WHERE SubmissionID = @SubmissionID AND SectionID LIKE 'O_' 
UPDATE Pers
SET OVTHours = ISNULL(OVTPcnt*STH/100,0)
WHERE SubmissionID = @SubmissionID AND SectionID LIKE 'M_'
UPDATE Pers
SET 	CompWHr = (ISNULL(STH, 0) + ISNULL(OVTHours, 0) - ISNULL(AbsHrs, 0)),
	ContWHr = ISNULL(Contract, 0), 
	GAWHr = ISNULL(GA, 0), 
	TotWHr = (ISNULL(STH, 0) + ISNULL(OVTHours, 0) - ISNULL(AbsHrs, 0) + ISNULL(Contract, 0) + ISNULL(GA, 0))
WHERE SubmissionID = @SubmissionID
UPDATE Pers
SET	CompEqP = (ISNULL(STH, 0) + ISNULL(OVTHours, 0))/(2080*@FractionOfYear),
	ContEqP = ISNULL(Contract, 0)/(2080*@FractionOfYear),
	GAEqP = ISNULL(GA, 0)/(2080*@FractionOfYear),
	TotEqP = (ISNULL(STH, 0) + ISNULL(OVTHours, 0) + ISNULL(Contract, 0) + ISNULL(GA, 0))/(2080*@FractionOfYear)
WHERE SubmissionID = @SubmissionID

DELETE FROM PersST WHERE SubmissionID = @SubmissionID

INSERT INTO PersST(SubmissionID, SectionID, NumPers, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, CompEqP, ContEqP, GAEqP, TotEqP, CompWHr, ContWHr, GAWHr, TotWHr)
SELECT SubmissionID, SectionID, SUM(NumPers), SUM(STH), SUM(OVTHours), CASE WHEN SUM(STH)>0 THEN SUM(OVTHours)/SUM(STH)*100 END, 
SUM(Contract), SUM(GA), SUM(AbsHrs), SUM(CompEqP), SUM(ContEqP), SUM(GAEqP), SUM(TotEqP), SUM(CompWHr), SUM(ContWHr), SUM(GAWHr), SUM(TotWHr)
FROM Pers
WHERE SubmissionID = @SubmissionID AND PersId IN (SELECT PersId FROM Pers_LU WHERE IncludeForInput = 'Y' OR PersId IN (@OCCTAPersId, @MPSTAPersId))
GROUP BY SubmissionID, SectionID

INSERT INTO PersST(SubmissionID, SectionID, NumPers, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, CompEqP, ContEqP, GAEqP, TotEqP, CompWHr, ContWHr, GAWHr, TotWHr)
SELECT SubmissionID, 'TO', SUM(NumPers), SUM(STH), SUM(OVTHours), CASE WHEN SUM(STH)>0 THEN SUM(OVTHours)/SUM(STH)*100 END, 
SUM(Contract), SUM(GA), SUM(AbsHrs), SUM(CompEqP), SUM(ContEqP), SUM(GAEqP), SUM(TotEqP), SUM(CompWHr), SUM(ContWHr), SUM(GAWHr), SUM(TotWHr)
FROM PersST
WHERE SubmissionID = @SubmissionID AND SectionID IN ('OO','OM','OT','OA')
GROUP BY SubmissionID
INSERT INTO PersST(SubmissionID, SectionID, NumPers, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, CompEqP, ContEqP, GAEqP, TotEqP, CompWHr, ContWHr, GAWHr, TotWHr)
SELECT SubmissionID, 'TM', SUM(NumPers), SUM(STH), SUM(OVTHours), CASE WHEN SUM(STH)>0 THEN SUM(OVTHours)/SUM(STH)*100 END, 
SUM(Contract), SUM(GA), SUM(AbsHrs), SUM(CompEqP), SUM(ContEqP), SUM(GAEqP), SUM(TotEqP), SUM(CompWHr), SUM(ContWHr), SUM(GAWHr), SUM(TotWHr)
FROM PersST
WHERE SubmissionID = @SubmissionID AND SectionID IN ('MO','MM','MT','MA')
GROUP BY SubmissionID
INSERT INTO PersST(SubmissionID, SectionID, NumPers, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, CompEqP, ContEqP, GAEqP, TotEqP, CompWHr, ContWHr, GAWHr, TotWHr)
SELECT SubmissionID, 'TP', SUM(NumPers), SUM(STH), SUM(OVTHours), CASE WHEN SUM(STH)>0 THEN SUM(OVTHours)/SUM(STH)*100 END, 
SUM(Contract), SUM(GA), SUM(AbsHrs), SUM(CompEqP), SUM(ContEqP), SUM(GAEqP), SUM(TotEqP), SUM(CompWHr), SUM(ContWHr), SUM(GAWHr), SUM(TotWHr)
FROM PersST
WHERE SubmissionID = @SubmissionID AND SectionID IN ('TO','TM')
GROUP BY SubmissionID

DECLARE @NumPersOO real, @NumPersMO real, @NumPersOM real, @NumPersMM real, @NumPersOA real, @NumPersMA real, @NumPersMT real, @NumPersOT real
DECLARE @OCCCompNonTASTH real, @MPSCompNonTASTH real, @ContMaintLaborWHr real
DECLARE @CompOCCNonMaintEqP real, @CompMPSNonMaintEqP real, @CompNonMaintEqP real, @ContNonMaintEqP real, @GANonMaintEqP real, @TotNonMaintEqP real
DECLARE @CompOCCNonMaintWHr real, @CompMPSNonMaintWHr real, @CompNonMaintWHr real, @ContNonMaintWHr real, @GANonMaintWHr real, @TotNonMaintWHr real
DECLARE @CompOCCNonTAWHr real, @CompMPSNonTAWHr real, @CompNonTAWHr real, @ContNonTAWHr real, @GANonTAWHr real, @TotNonTAWHr real
DECLARE @TotMaintForceWHr real, @MaintForceCompWHr real, @MaintForceContWHr real, @TotMaintForceEqP real, @MaintForceCompEqP real, @MaintForceContEqP real
DECLARE @NonTAMaintForceWHr real, @NonTAMaintForceCompWHr real, @NonTAMaintForceContWHr real, @NonTAMaintForceEqP real, @NonTAMaintForceCompEqP real, @NonTAMaintForceContEqP real

SELECT 	@NumPersOO = SUM(CASE WHEN SectionId = 'OO' THEN NumPers ELSE 0 END),
	@NumPersOM = SUM(CASE WHEN SectionId = 'OM' THEN NumPers ELSE 0 END),
	@NumPersOT = SUM(CASE WHEN SectionId = 'OT' THEN NumPers ELSE 0 END),
	@NumPersOA = SUM(CASE WHEN SectionId = 'OA' THEN NumPers ELSE 0 END),
	@NumPersMO = SUM(CASE WHEN SectionId = 'MO' THEN NumPers ELSE 0 END),
	@NumPersMM = SUM(CASE WHEN SectionId = 'MM' THEN NumPers ELSE 0 END),
	@NumPersMT = SUM(CASE WHEN SectionId = 'MT' THEN NumPers ELSE 0 END),
	@NumPersMA = SUM(CASE WHEN SectionId = 'MA' THEN NumPers ELSE 0 END),
	@OCCCompNonTASTH = SUM(CASE WHEN SectionID IN ('OO','OM','OT','OA') THEN STH ELSE 0 END),
	@MPSCompNonTASTH = SUM(CASE WHEN SectionID IN ('MO','MM','MT','MA') THEN STH ELSE 0 END),
	@ContMaintLaborWHr = SUM(CASE WHEN SectionID IN ('OM','MM') THEN Contract ELSE 0 END),
	@CompOCCNonMaintEqP = SUM(CASE WHEN SectionID IN ('OO','OT','OA') THEN CompEqP*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@CompMPSNonMaintEqP = SUM(CASE WHEN SectionID IN ('MO','MT','MA') THEN CompEqP*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@CompNonMaintEqP = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN CompEqP*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@ContNonMaintEqP = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN ContEqP*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@GANonMaintEqP = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN GAEqP*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@TotNonMaintEqP = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN TotEqP*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@CompOCCNonMaintWHr = SUM(CASE WHEN SectionID IN ('OO','OT','OA') THEN CompWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@CompMPSNonMaintWHr = SUM(CASE WHEN SectionID IN ('MO','MT','MA') THEN CompWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@CompNonMaintWHr = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN CompWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@ContNonMaintWHr = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN ContWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@GANonMaintWHr = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN GAWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@TotNonMaintWHr = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN TotWHr*ISNULL(1-MaintPcnt/100,1) ELSE 0 END),
	@CompOCCNonTAWHr = SUM(CASE WHEN SectionID IN ('OO','OT','OM','OA') THEN CompWHr ELSE 0 END), 
	@CompMPSNonTAWHr = SUM(CASE WHEN SectionID IN ('MO','OT','MM','MT','MA') THEN CompWHr ELSE 0 END), 
	@CompNonTAWHr = SUM(CASE WHEN SectionID IN ('OO','OM','OT','OA','MO','MM','MT','MA') THEN CompWHr ELSE 0 END), 
	@ContNonTAWHr = SUM(CASE WHEN SectionID IN ('OO','OM','OT','OA','MO','MM','MT','MA') THEN ContWHr ELSE 0 END), 
	@GANonTAWHr  = SUM(CASE WHEN SectionID IN ('OO','OM','OT','OA','MO','MM','MT','MA') THEN GAWHr ELSE 0 END),
	@TotNonTAWHr = SUM(CASE WHEN SectionID IN ('OO','OM','OT','OA','MO','MM','MT','MA') THEN TotWHr ELSE 0 END)
FROM Pers 
WHERE SubmissionID = @SubmissionID AND PersId IN (SELECT PersId FROM Pers_LU WHERE IncludeForInput = 'Y')

DECLARE @UseDetails bit
IF EXISTS (SELECT * FROM Pers WHERE SubmissionID = @SubmissionID AND TotWHr > 0 AND SectionID IN ('MT')
		AND PersId NOT IN (SELECT PersId FROM Pers_LU WHERE IncludeForInput = 'Y' AND DetailStudy = 'S'))
	SET @UseDetails = 1
ELSE
	SET @UseDetails = 0

UPDATE Pers
SET MaintPcnt = NULL
WHERE PersId IN (SELECT PersId FROM Pers_LU WHERE MaintForceGroup IS NOT NULL AND PersId <> 'MPSTS')
AND SubmissionID = @SubmissionID

SELECT @MaintForceCompWHr = SUM((ISNULL(Pers.CompWHr,0)+ISNULL(Pers.GAWHr, 0))*(ISNULL(Pers.MaintPcnt,100)/100)), 
@MaintForceContWHr = SUM(ISNULL(Pers.ContWHr,0)*(ISNULL(Pers.MaintPcnt,100)/100)),
@TotMaintForceWHr = SUM((ISNULL(Pers.CompWHr,0)+ISNULL(Pers.GAWHr, 0)+ISNULL(Pers.ContWHr,0))*(ISNULL(Pers.MaintPcnt,100)/100)),
@MaintForceCompEqP = SUM((ISNULL(Pers.CompEqP,0)+ISNULL(Pers.GAEqP, 0))*(ISNULL(Pers.MaintPcnt,100)/100)), 
@MaintForceContEqP = SUM(ISNULL(Pers.ContEqP,0)*(ISNULL(Pers.MaintPcnt,100)/100)),
@TotMaintForceEqP = SUM((ISNULL(Pers.CompEqP,0)+ISNULL(Pers.GAEqP, 0)+ISNULL(Pers.ContEqP,0))*(ISNULL(Pers.MaintPcnt,100)/100)),

@NonTAMaintForceCompWHr = SUM((ISNULL(Pers.CompWHr,0)+ISNULL(Pers.GAWHr, 0))*(ISNULL(Pers.MaintPcnt,100)/100)*CASE WHEN Pers.PersId IN (@OCCTAPersId, @MPSTAPersId) THEN 0 ELSE 1 END), 
@NonTAMaintForceContWHr = SUM(ISNULL(Pers.ContWHr,0)*(ISNULL(Pers.MaintPcnt,100)/100)*CASE WHEN Pers.PersId IN (@OCCTAPersId, @MPSTAPersId) THEN 0 ELSE 1 END),
@NonTAMaintForceWHr = SUM((ISNULL(Pers.CompWHr,0)+ISNULL(Pers.GAWHr, 0)+ISNULL(Pers.ContWHr,0))*(ISNULL(Pers.MaintPcnt,100)/100)*CASE WHEN Pers.PersId IN (@OCCTAPersId, @MPSTAPersId) THEN 0 ELSE 1 END),
@NonTAMaintForceCompEqP = SUM((ISNULL(Pers.CompEqP,0)+ISNULL(Pers.GAEqP, 0))*(ISNULL(Pers.MaintPcnt,100)/100)*CASE WHEN Pers.PersId IN (@OCCTAPersId, @MPSTAPersId) THEN 0 ELSE 1 END), 
@NonTAMaintForceContEqP = SUM(ISNULL(Pers.ContEqP,0)*(ISNULL(Pers.MaintPcnt,100)/100)*CASE WHEN Pers.PersId IN (@OCCTAPersId, @MPSTAPersId) THEN 0 ELSE 1 END),
@NonTAMaintForceEqP = SUM((ISNULL(Pers.CompEqP,0)+ISNULL(Pers.GAEqP, 0)+ISNULL(Pers.ContEqP,0))*(ISNULL(Pers.MaintPcnt,100)/100)*CASE WHEN Pers.PersId IN (@OCCTAPersId, @MPSTAPersId) THEN 0 ELSE 1 END)
FROM Pers INNER JOIN Pers_LU lu ON lu.PersId = Pers.PersId
WHERE lu.MaintForceGroup IS NOT NULL AND (
		(@UseDetails = 0 AND ((lu.IncludeForInput = 'Y' AND lu.DetailStudy = 'S') OR Pers.PersId IN (@OCCTAPersId, @MPSTAPersId) OR (Pers.PersId = 'MPSTS' AND Pers.MaintPcnt > 0)))
	 OR 	(@UseDetails = 1 AND Pers.PersId <> 'MPSTS' AND lu.DetailStudy <> 'S'))
AND SubmissionID = @SubmissionID

DECLARE @OthContWHr real, @ProcessWHr real, @AdminWHr real, @MaintWHr real, @CompWHr real, 
	@ProcessContWHr real, @AdminContWHr real, @MaintContWHr real, @TechWHr real, @TechContWHr real,
	@CompOVTPcnt real
SELECT 	@OthContWHr = SUM(CASE WHEN SectionID IN ('OO','OT','OA','MO','MT','MA') THEN ContWHr ELSE 0 END),
	@ProcessWHr = SUM(CASE WHEN SectionID IN ('OO','MO') THEN TotWHr ELSE 0 END),
	@AdminWHr = SUM(CASE WHEN SectionID IN ('OA','MA') THEN TotWHr ELSE 0 END),
	@MaintWHr = SUM(CASE WHEN SectionID IN ('OM','MM') THEN TotWHr ELSE 0 END),
	@TechWHr = SUM(CASE WHEN SectionID IN ('OT','MT') THEN TotWHr ELSE 0 END),
	@CompWHr = SUM(CASE WHEN SectionID = 'TP' THEN CompWHr ELSE 0 END),
	@ProcessContWHr = SUM(CASE WHEN SectionID IN ('OO','MO') THEN ContWHr ELSE 0 END),
	@AdminContWHr = SUM(CASE WHEN SectionID IN ('OA','MA') THEN ContWHr ELSE 0 END),
	@MaintContWHr = SUM(CASE WHEN SectionID IN ('OM','MM') THEN ContWHr ELSE 0 END),
	@TechContWHr = SUM(CASE WHEN SectionID IN ('OT','MT') THEN ContWHr ELSE 0 END),
	@CompOVTPcnt = AVG(CASE WHEN SectionID = 'TP' THEN OVTPcnt END)
FROM PersST
WHERE SubmissionID = @SubmissionID
DELETE FROM PersTot WHERE SubmissionID = @SubmissionID
	
INSERT INTO PersTot (SubmissionID, ProcessOCCMPSRatio, MaintOCCMPSRatio, AdminOCCMPSRatio, 
	NonMaintEqP, OCCCompNonTASTH, MPSCompNonTASTH, TotCompNonTASTH, 
	ContMaintLaborWHr, NonMaintWHr, OthContWHr, OCCNumPers, MPSNumPers, TotNumPers, 
	ProcessWHr, MaintWHr, AdminWHr, CompWHr, ProcessContPcnt, MaintContPcnt, TechContPcnt, AdminContPcnt, 
	CompOvtPcnt, CompOCCNonMaintEqP, CompMPSNonMaintEqP, CompNonMaintEqP, ContNonMaintEqP, GANonMaintEqP, TotNonMaintEqP, 
	CompOCCNonMaintWHr, CompMPSNonMaintWHr, CompNonMaintWHr, ContNonMaintWHr, GANonMaintWHr, TotNonMaintWHr, 
	CompOCCNonTAWHr, CompMPSNonTAWHr, CompNonTAWHr, ContNonTAWHr, GANonTAWHr, TotNonTAWHr, 
	CompOCCNonMaintPcnt, CompMPSNonMaintPcnt, CompNonMaintPcnt, ContNonMaintPcnt, GANonMaintPcnt, TotNonMaintPcnt,
	TotMaintForceWHr, MaintForceCompWHr, MaintForceContWHr, TotMaintForceEqP, MaintForceCompEqP, MaintForceContEqP, 
	NonTAMaintForceWHr, NonTAMaintForceCompWHr, NonTAMaintForceContWHr, NonTAMaintForceEqP, NonTAMaintForceCompEqP, NonTAMaintForceContEqP, 
	MaintForceCompPcnt, MaintForceContPcnt)
SELECT @SubmissionID, CASE WHEN @NumPersMO > 0 THEN @NumPersOO/@NumPersMO END, CASE WHEN @NumPersMM > 0 THEN @NumPersOM/@NumPersMM END, CASE WHEN @NumPersMA > 0 THEN @NumPersOA/@NumPersMA END,
@TotNonMaintEqP, @OCCCompNonTASTH, @MPSCompNonTASTH, ISNULL(@OCCCompNonTASTH, 0) + ISNULL(@MPSCompNonTASTH, 0),
@ContMaintLaborWHr, @TotNonMaintWHr, @OthContWHr,
OCCNumPers = ISNULL(@NumPersOO, 0) + ISNULL(@NumPersOM, 0) + ISNULL(@NumPersOT, 0) + ISNULL(@NumPersOA, 0),
MPSNumPers = ISNULL(@NumPersMO, 0) + ISNULL(@NumPersMM, 0) + ISNULL(@NumPersMT, 0) + ISNULL(@NumPersMA, 0),
TotNumPers = ISNULL(@NumPersOO, 0) + ISNULL(@NumPersOM, 0) + ISNULL(@NumPersOT, 0) + ISNULL(@NumPersOA, 0) 
		  + ISNULL(@NumPersMO, 0) + ISNULL(@NumPersMM, 0) + ISNULL(@NumPersMT, 0) + ISNULL(@NumPersMA, 0),
@ProcessWHr, @MaintWHr, @AdminWHr, @CompWHr,
CASE WHEN @ProcessWHr > 0 THEN 100*ISNULL(@ProcessContWHr, 0)/@ProcessWHr END,
CASE WHEN @MaintWHr > 0 THEN 100*ISNULL(@MaintContWHr, 0)/@MaintWHr END,
CASE WHEN @TechWHr > 0 THEN 100*ISNULL(@TechContWHr, 0)/@TechWHr END,
CASE WHEN @AdminWHr > 0 THEN 100*ISNULL(@AdminContWHr, 0)/@AdminWHr END,
@CompOVTPcnt, @CompOCCNonMaintEqP, @CompMPSNonMaintEqP, @CompNonMaintEqP, @ContNonMaintEqP, @GANonMaintEqP, @TotNonMaintEqP,
@CompOCCNonMaintWHr, @CompMPSNonMaintWHr, @CompNonMaintWHr, @ContNonMaintWHr, @GANonMaintWHr, @TotNonMaintWHr,
@CompOCCNonTAWHr, @CompMPSNonTAWHr, @CompNonTAWHr, @ContNonTAWHr, @GANonTAWHr, @TotNonTAWHr,
CompOCCNonMaintPcnt = CASE WHEN @CompOCCNonTAWHr > 0 THEN @CompOCCNonMaintWHr/@CompOCCNonTAWHr*100 END,
CompMPSNonMaintPcnt = CASE WHEN @CompMPSNonTAWHr > 0 THEN @CompMPSNonMaintWHr/@CompMPSNonTAWHr*100 END,
CompNonMaintPcnt = CASE WHEN @CompNonTAWHr > 0 THEN @CompNonMaintWHr/@CompNonTAWHr*100 END,
ContNonMaintPcnt = CASE WHEN @ContNonTAWHr > 0 THEN @ContNonMaintWHr/@ContNonTAWHr*100 END,
GANonMaintPcnt = CASE WHEN @GANonTAWHr > 0 THEN @GANonMaintWHr/@GANonTAWHr*100 END,
TotNonMaintPcnt = CASE WHEN @TotNonTAWHr > 0 THEN @TotNonMaintWHr/@TotNonTAWHr*100 END,
@TotMaintForceWHr, @MaintForceCompWHr, @MaintForceContWHr, @TotMaintForceEqP, @MaintForceCompEqP, @MaintForceContEqP, 
@NonTAMaintForceWHr, @NonTAMaintForceCompWHr, @NonTAMaintForceContWHr, @NonTAMaintForceEqP, @NonTAMaintForceCompEqP, @NonTAMaintForceContEqP, 
MaintForceCompPcnt = CASE WHEN @TotMaintForceWHr > 0 THEN 100*@MaintForceCompWHr/@TotMaintForceWHr END, 
MaintForceContPcnt = CASE WHEN @TotMaintForceWHr > 0 THEN 100*@MaintForceContWHr/@TotMaintForceWHr END


