﻿CREATE PROC [dbo].[TransferUnitTargets](@SubmissionID int)
AS
IF EXISTS (SELECT * FROM SubmissionsAll WHERE SubmissionID = @SubmissionID AND ClientVersion IS NULL)
BEGIN
	DELETE FROM UnitTargetsNew 
	WHERE SubmissionID = @SubmissionID 
	AND Property IN ('MechAvail_Target', 'OpAvail_Target', 'OnStream_Target', 'UtilPcnt_Target', 'RoutCost_Target', 'TACost_Target')

	INSERT UnitTargetsNew (SubmissionID, UnitID, Property, Target, CurrencyCode)
	SELECT SubmissionID, UnitID, Property = 'MechAvail_Target', MechAvail, CurrencyCode
	FROM UnitTargets 
	WHERE SubmissionID = @SubmissionID AND MechAvail Is NOT NULL

	INSERT UnitTargetsNew (SubmissionID, UnitID, Property, Target, CurrencyCode)
	SELECT SubmissionID, UnitID, Property = 'OpAvail_Target', OpAvail, CurrencyCode
	FROM UnitTargets 
	WHERE SubmissionID = @SubmissionID AND OpAvail Is NOT NULL

	INSERT UnitTargetsNew (SubmissionID, UnitID, Property, Target, CurrencyCode)
	SELECT SubmissionID, UnitID, Property = 'OnStream_Target', OnStream, CurrencyCode
	FROM UnitTargets 
	WHERE SubmissionID = @SubmissionID AND OnStream Is NOT NULL

	INSERT UnitTargetsNew (SubmissionID, UnitID, Property, Target, CurrencyCode)
	SELECT SubmissionID, UnitID, Property = 'UtilPcnt_Target', UtilPcnt, CurrencyCode
	FROM UnitTargets 
	WHERE SubmissionID = @SubmissionID AND UtilPcnt Is NOT NULL

	INSERT UnitTargetsNew (SubmissionID, UnitID, Property, Target, CurrencyCode)
	SELECT SubmissionID, UnitID, Property = 'RoutCost_Target', RoutCost, CurrencyCode
	FROM UnitTargets 
	WHERE SubmissionID = @SubmissionID AND RoutCost Is NOT NULL

	INSERT UnitTargetsNew (SubmissionID, UnitID, Property, Target, CurrencyCode)
	SELECT SubmissionID, UnitID, Property = 'TACost_Target', TACost, CurrencyCode
	FROM UnitTargets 
	WHERE SubmissionID = @SubmissionID AND TACost Is NOT NULL
END
ELSE BEGIN
	DELETE FROM UnitTargets WHERE SubmissionID = @SubmissionID
	INSERT UnitTargets (SubmissionID, UnitID, MechAvail, OpAvail, OnStream, UtilPcnt, RoutCost, TACost, UnitEII, CurrencyCode)
	SELECT SubmissionID, UnitID, 
	MechAvail = AVG(CASE WHEN Property = 'MechAvail_Target' THEN Target END),
	OpAvail = AVG(CASE WHEN Property = 'OpAvail_Target' THEN Target END),
	OnStream = AVG(CASE WHEN Property = 'OnStream_Target' THEN Target END),
	UtilPcnt = AVG(CASE WHEN Property = 'UtilPcnt_Target' THEN Target END),
	RoutCost = AVG(CASE WHEN Property = 'RoutCost_Target' THEN Target END),
	TACost = AVG(CASE WHEN Property = 'TACost_Target' THEN Target END),
	UnitEII = AVG(CASE WHEN Property = 'UnitEII_Target' THEN Target END),
	CurrencyCode
	FROM UnitTargetsNew
	WHERE SubmissionID = @SubmissionID
	GROUP BY SubmissionID, UnitID, CurrencyCode
END

