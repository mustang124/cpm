﻿CREATE         PROC [dbo].[spOpEx] (@SubmissionID int)
AS
SET NOCOUNT ON 
DECLARE @PeriodStart smalldatetime, @RptCurrency CurrencyCode, @RefineryID char(6), @RefineryType varchar(5)
DECLARE @Currency CurrencyCode, @ConvRate real
SELECT @RptCurrency = RptCurrency, @PeriodStart = PeriodStart, @RefineryID = RefineryID, @RefineryType = dbo.GetRefineryType(RefineryID)
FROM SubmissionsAll 
WHERE SubmissionID = @SubmissionID

DELETE FROM OpEx WHERE DataType <> 'RPT' AND SubmissionID = @SubmissionID
DELETE FROM OpExAll WHERE DataType <> 'RPT' AND SubmissionID = @SubmissionID

DECLARE cCurrencies CURSOR LOCAL SCROLL READ_ONLY 
FOR 	SELECT Currency, dbo.ExchangeRate(@RptCurrency, Currency, @PeriodStart)
	FROM CurrenciesToCalc 
	WHERE RefineryID = @RefineryID
OPEN cCurrencies
FETCH NEXT FROM cCurrencies INTO @Currency, @ConvRate
WHILE @@FETCH_STATUS = 0
BEGIN
	INSERT INTO OpEx (SubmissionID, Currency, Scenario, DataType, OCCSal, MPSSal, OCCBen, MPSBen, MaintMatl, ContMaintLabor, ContMaintMatl, OthCont, Equip, Tax, Insur, TAAdj, Envir, OthNonVol, GAPers, STNonVol, Antiknock, Chemicals, Catalysts, Royalties, PurElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth, EmissionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol, TotCashOpEx, GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, FireSafetyLoss, EnvirFines, ExclOth, TotExpExcl)
	SELECT @SubmissionID, @Currency, '', 'RPTC', OCCSal*@ConvRate, MPSSal*@ConvRate, OCCBen*@ConvRate, MPSBen*@ConvRate, MaintMatl*@ConvRate, ContMaintLabor*@ConvRate, ContMaintMatl*@ConvRate, OthCont*@ConvRate, Equip*@ConvRate, Tax*@ConvRate, Insur*@ConvRate, TAAdj*@ConvRate, Envir*@ConvRate, OthNonVol*@ConvRate, GAPers*@ConvRate, STNonVol*@ConvRate, Antiknock*@ConvRate, Chemicals*@ConvRate, Catalysts*@ConvRate, Royalties*@ConvRate, PurElec*@ConvRate, PurSteam*@ConvRate, PurOth*@ConvRate, PurFG*@ConvRate, PurLiquid*@ConvRate, PurSolid*@ConvRate, RefProdFG*@ConvRate, RefProdOth*@ConvRate, EmissionsPurch*@ConvRate, EmissionsCredits*@ConvRate, EmissionsTaxes*@ConvRate, OthVol*@ConvRate, STVol*@ConvRate, TotCashOpEx*@ConvRate, GANonPers*@ConvRate, InvenCarry*@ConvRate, Depreciation*@ConvRate, Interest*@ConvRate, STNonCash*@ConvRate, TotRefExp*@ConvRate, Cogen*@ConvRate, OthRevenue*@ConvRate, ThirdPartyTerminalRM*@ConvRate, ThirdPartyTerminalProd*@ConvRate, POXO2*@ConvRate, PMAA*@ConvRate, FireSafetyLoss*@ConvRate, EnvirFines*@ConvRate, ExclOth*@ConvRate, TotExpExcl*@ConvRate
	FROM OpEx WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

	INSERT INTO OpEx (SubmissionID, Currency, Scenario, DataType, OCCSal, MPSSal, OCCBen, MPSBen, MaintMatl, ContMaintLabor, ContMaintMatl, OthCont, Equip, Tax, Insur, TAAdj, Envir, OthNonVol, GAPers, STNonVol, Antiknock, Chemicals, Catalysts, Royalties, PurElec, PurSteam, PurOth, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth, EmissionsPurch, EmissionsCredits, EmissionsTaxes, OthVol, STVol, TotCashOpEx, GANonPers, InvenCarry, Depreciation, Interest, STNonCash, TotRefExp, Cogen, OthRevenue, ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, FireSafetyLoss, EnvirFines, ExclOth, TotExpExcl)
	SELECT o.SubmissionID, o.Currency, e.Scenario, 'ADJ', o.OCCSal, o.MPSSal, o.OCCBen, o.MPSBen, o.MaintMatl, o.ContMaintLabor, o.ContMaintMatl, o.OthCont, o.Equip, o.Tax, o.Insur, m.AllocAnnTACost*s.FractionOfYear, o.Envir, o.OthNonVol, o.GAPers, o.STNonVol, o.Antiknock, o.Chemicals, o.Catalysts, o.Royalties, e.PurPowerCostK, e.PurSteamCostK, o.PurOth, e.PurFGCostK, e.PurLiquidCostK, e.PurSolidCostK, e.ProdFGCostK, e.ProdOthCostK, o.EmissionsPurch, o.EmissionsCredits, o.EmissionsTaxes, o.OthVol, o.STVol, o.TotCashOpEx, o.GANonPers, o.InvenCarry, o.Depreciation, o.Interest, o.STNonCash, o.TotRefExp, o.Cogen, o.OthRevenue, o.ThirdPartyTerminalRM, o.ThirdPartyTerminalProd, o.POXO2, o.PMAA, o.FireSafetyLoss, o.EnvirFines, o.ExclOth, o.TotExpExcl
	FROM OpEx o INNER JOIN EnergyTotCost e ON e.SubmissionID = o.SubmissionID AND e.Currency = o.Currency
	INNER JOIN MaintTotCost m ON m.SubmissionID = o.SubmissionID AND m.Currency = o.Currency
	INNER JOIN SubmissionsAll s ON s.SubmissionID = o.SubmissionID
	WHERE o.SubmissionID = @SubmissionID AND o.DataType = 'RPTC' AND o.Currency = @Currency

	UPDATE OpEx 
	SET 	STNonVol = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0) + ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0) + ISNULL(MaintMatl, 0) + ISNULL(ContMaintLabor, 0) + ISNULL(ContMaintMatl, 0) + ISNULL(OthCont, 0) + ISNULL(Equip, 0) + ISNULL(Tax, 0) + ISNULL(Insur, 0) + ISNULL(TAAdj, 0) + ISNULL(Envir, 0) + ISNULL(OthNonVol, 0) + ISNULL(GAPers, 0),
		STVol = ISNULL(Antiknock, 0) + ISNULL(Chemicals, 0) + ISNULL(Catalysts, 0) + ISNULL(Royalties, 0) + ISNULL(PurOth, 0) + ISNULL(PurElec, 0) + ISNULL(PurSteam, 0) + ISNULL(PurFG, 0) + ISNULL(PurLiquid, 0) + ISNULL(PurSolid, 0) + ISNULL(RefProdFG, 0) + ISNULL(RefProdOth, 0) + ISNULL(EmissionsPurch, 0) + ISNULL(EmissionsCredits, 0) + ISNULL(EmissionsTaxes, 0) + ISNULL(OthVol, 0),
		STNonCash = ISNULL(GANonPers, 0) + ISNULL(InvenCarry, 0) + ISNULL(Depreciation, 0) + ISNULL(Interest, 0),
		TotExpExcl = ISNULL(FireSafetyLoss, 0) + ISNULL(EnvirFines, 0) + ISNULL(ExclOth, 0),
		STSal = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0),
		STBen = ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0),
		PersCostExclTA = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0) + ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0) + ISNULL(ContMaintLabor, 0) + ISNULL(OthCont, 0) + ISNULL(GAPers, 0),
		PersCost = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0) + ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0) + ISNULL(ContMaintLabor, 0) + ISNULL(OthCont, 0) + ISNULL(GAPers, 0) + 0.55*ISNULL(TAAdj, 0),
		EnergyCost = ISNULL(PurElec, 0) + ISNULL(PurSteam, 0) + ISNULL(PurFG, 0) + ISNULL(PurLiquid, 0) + ISNULL(PurSolid, 0) + ISNULL(RefProdFG, 0) + ISNULL(RefProdOth, 0)
	WHERE SubmissionID = @SubmissionID 
	UPDATE OpEx 
	SET 	TotCashOpEx = STNonVol + STVol,
		TotRefExp = STNonVol + STVol + STNonCash,
		NEOpEx = STNonVol + STVol - EnergyCost
	WHERE SubmissionID = @SubmissionID 

	INSERT INTO OpExAll (SubmissionID, Currency, Scenario, DataType, 
		OCCSal, MPSSal, 
		OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, OCCBen, 
		MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock, MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, 
		MaintMatl, ContMaintMatl, EquipMaint, MaintMatlST, 
		ContMaintLabor, ContMaintInspect, ContMaintLaborST, 
		OthContProcOp, OthContTransOp, OthContFire, OthContVacTrucks, OthContConsult, OthContSecurity, OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin, OthContLegal, OthContOth, OthCont, 
		TAAdj, 
		EnvirDisp, EnvirPermits, EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirMonitor, EnvirOth, Envir, 
		EquipNonMaint, 
		Tax, InsurBI, InsurPC, InsurOth, 
		OthNonVolSupply, OthNonVolSafety, OthNonVolNonPersSafety, OthNonVolComm, OthNonVolDonations, OthNonVolNonContribPers, OthNonVolDues, OthNonVolTravel, OthNonVolTrain, OthNonVolComputer, OthNonVolTanks, OthNonVolExtraExpat, OthNonVolOth, OthNonVol, 
		GAPers, 
		ChemicalsAntiknock, ChemicalsAlkyAcid, ChemicalsLube, ChemicalsH2OTreat, ChemicalsProcess, ChemicalsOthAcid, ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, ChemicalsClay, ChemicalsAmines, 
		ChemicalsASESolv, ChemicalsWasteH2O, ChemicalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene, ChemicalsPropane, ChemicalsOthSolv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth, Chemicals, 
		CatalystsFCC, CatalystsFCCAdditives, CatalystsFCCECat, CatalystsFCCECatSales, CatalystsFCCFresh, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, CatalystsHYG, CatalystsS2Plant, CatalystsPetChem, CatalystsOth, Catalysts, 
		PurElec, PurSteam, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth, 
		PurOthN2, PurOthH2O, PurOthOth, PurOth, 
		Royalties, 
		EmissionsTaxes, EmissionsPurch, EmissionsCredits, 
		OthVolDemCrude, OthVolDemLightering, OthVolDemProd, OthVolOth, OthVol, 
		GANonPers, InvenCarry, Depreciation, Interest, 
		Cogen, OthRevenue, 
		ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, 
		ExclFireSafety, ExclEnvirFines, ExclOth)
	SELECT @SubmissionID, @Currency, '', 'RPTC', 
		OCCSal*@ConvRate, MPSSal*@ConvRate, 
		OCCBenAbs*@ConvRate, OCCBenInsur*@ConvRate, OCCBenPension*@ConvRate, OCCBenSub*@ConvRate, OCCBenStock*@ConvRate, OCCBenTaxPen*@ConvRate, OCCBenTaxMed*@ConvRate, OCCBenTaxOth*@ConvRate, OCCBen*@ConvRate, 
		MPSBenAbs*@ConvRate, MPSBenInsur*@ConvRate, MPSBenPension*@ConvRate, MPSBenSub*@ConvRate, MPSBenStock*@ConvRate, MPSBenTaxPen*@ConvRate, MPSBenTaxMed*@ConvRate, MPSBenTaxOth*@ConvRate, MPSBen*@ConvRate, 
		MaintMatl*@ConvRate, ContMaintMatl*@ConvRate, EquipMaint*@ConvRate, MaintMatlST*@ConvRate, 
		ContMaintLabor*@ConvRate, ContMaintInspect*@ConvRate, ContMaintLaborST*@ConvRate, 
		OthContProcOp*@ConvRate, OthContTransOp*@ConvRate, OthContFire*@ConvRate, OthContVacTrucks*@ConvRate, OthContConsult*@ConvRate, OthContSecurity*@ConvRate, OthContComputing*@ConvRate, OthContJan*@ConvRate, OthContLab*@ConvRate, OthContFoodSvc*@ConvRate, OthContAdmin*@ConvRate, OthContLegal*@ConvRate, OthContOth*@ConvRate, OthCont*@ConvRate, 
		TAAdj*@ConvRate, 
		EnvirDisp*@ConvRate, EnvirPermits*@ConvRate, EnvirFines*@ConvRate, EnvirSpill*@ConvRate, EnvirLab*@ConvRate, EnvirEng*@ConvRate, EnvirMonitor*@ConvRate, EnvirOth*@ConvRate, Envir*@ConvRate, 
		EquipNonMaint*@ConvRate, 
		Tax*@ConvRate, InsurBI*@ConvRate, InsurPC*@ConvRate, InsurOth*@ConvRate, 
		OthNonVolSupply*@ConvRate, OthNonVolSafety*@ConvRate, OthNonVolNonPersSafety*@ConvRate, OthNonVolComm*@ConvRate, OthNonVolDonations*@ConvRate, OthNonVolNonContribPers*@ConvRate, OthNonVolDues*@ConvRate, OthNonVolTravel*@ConvRate, OthNonVolTrain*@ConvRate, OthNonVolComputer*@ConvRate, OthNonVolTanks*@ConvRate, OthNonVolExtraExpat*@ConvRate, OthNonVolOth*@ConvRate, OthNonVol*@ConvRate, 
		GAPers*@ConvRate, 
		ChemicalsAntiknock*@ConvRate, ChemicalsAlkyAcid*@ConvRate, ChemicalsLube*@ConvRate, ChemicalsH2OTreat*@ConvRate, ChemicalsProcess*@ConvRate, ChemicalsOthAcid*@ConvRate, ChemicalsGasAdd*@ConvRate, ChemicalsDieselAdd*@ConvRate, ChemicalsOthAdd*@ConvRate, ChemicalsO2*@ConvRate, ChemicalsClay*@ConvRate, ChemicalsAmines*@ConvRate, 
		ChemicalsASESolv*@ConvRate, ChemicalsWasteH2O*@ConvRate, ChemicalsNMP*@ConvRate, ChemicalsFurfural*@ConvRate, ChemicalsMIBK*@ConvRate, ChemicalsMEK*@ConvRate, ChemicalsToluene*@ConvRate, ChemicalsPropane*@ConvRate, ChemicalsOthSolv*@ConvRate, ChemicalsAcid*@ConvRate, ChemicalsDewaxAids*@ConvRate, ChemicalsOth*@ConvRate, Chemicals*@ConvRate, 
		CatalystsFCC*@ConvRate, CatalystsFCCAdditives*@ConvRate, CatalystsFCCECat*@ConvRate, CatalystsFCCECatSales*@ConvRate, CatalystsFCCFresh*@ConvRate, CatalystsHYC*@ConvRate, CatalystsNKSHYT*@ConvRate, CatalystsDHYT*@ConvRate, CatalystsVHYT*@ConvRate, CatalystsRHYT*@ConvRate, CatalystsHYFT*@ConvRate, CatalystsCDWax*@ConvRate, CatalystsREF*@ConvRate, CatalystsHYG*@ConvRate, CatalystsS2Plant*@ConvRate, CatalystsPetChem*@ConvRate, CatalystsOth*@ConvRate, Catalysts*@ConvRate, 
		PurElec*@ConvRate, PurSteam*@ConvRate, PurFG*@ConvRate, PurLiquid*@ConvRate, PurSolid*@ConvRate, RefProdFG*@ConvRate, RefProdOth*@ConvRate, 
		PurOthN2*@ConvRate, PurOthH2O*@ConvRate, PurOthOth*@ConvRate, PurOth*@ConvRate, 
		Royalties*@ConvRate, 
		EmissionsTaxes*@ConvRate, EmissionsPurch*@ConvRate, EmissionsCredits*@ConvRate, 
		OthVolDemCrude*@ConvRate, OthVolDemLightering*@ConvRate, OthVolDemProd*@ConvRate, OthVolOth*@ConvRate, OthVol*@ConvRate, 
		GANonPers*@ConvRate, InvenCarry*@ConvRate, Depreciation*@ConvRate, Interest*@ConvRate, 
		Cogen*@ConvRate, OthRevenue*@ConvRate, 
		ThirdPartyTerminalRM*@ConvRate, ThirdPartyTerminalProd*@ConvRate, POXO2*@ConvRate, PMAA*@ConvRate, 
		ExclFireSafety*@ConvRate, ExclEnvirFines*@ConvRate, ExclOth*@ConvRate
	FROM OpExAll WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

	INSERT INTO OpExAll (SubmissionID, Currency, Scenario, DataType, 
		OCCSal, MPSSal, 
		OCCBenAbs, OCCBenInsur, OCCBenPension, OCCBenSub, OCCBenStock, OCCBenTaxPen, OCCBenTaxMed, OCCBenTaxOth, OCCBen, 
		MPSBenAbs, MPSBenInsur, MPSBenPension, MPSBenSub, MPSBenStock, MPSBenTaxPen, MPSBenTaxMed, MPSBenTaxOth, MPSBen, 
		MaintMatl, ContMaintMatl, EquipMaint, MaintMatlST, 
		ContMaintLabor, ContMaintInspect, ContMaintLaborST, 
		OthContProcOp, OthContTransOp, OthContFire, OthContVacTrucks, OthContConsult, OthContSecurity, OthContComputing, OthContJan, OthContLab, OthContFoodSvc, OthContAdmin, OthContLegal, OthContOth, OthCont, 
		TAAdj, 
		EnvirDisp, EnvirPermits, EnvirFines, EnvirSpill, EnvirLab, EnvirEng, EnvirMonitor, EnvirOth, Envir, 
		EquipNonMaint, 
		Tax, InsurBI, InsurPC, InsurOth, 
		OthNonVolSupply, OthNonVolSafety, OthNonVolNonPersSafety, OthNonVolComm, OthNonVolDonations, OthNonVolNonContribPers, OthNonVolDues, OthNonVolTravel, OthNonVolTrain, OthNonVolComputer, OthNonVolTanks, OthNonVolExtraExpat, OthNonVolOth, OthNonVol, 
		GAPers, 
		ChemicalsAntiknock, ChemicalsAlkyAcid, ChemicalsLube, ChemicalsH2OTreat, ChemicalsProcess, ChemicalsOthAcid, ChemicalsGasAdd, ChemicalsDieselAdd, ChemicalsOthAdd, ChemicalsO2, ChemicalsClay, ChemicalsAmines, 
		ChemicalsASESolv, ChemicalsWasteH2O, ChemicalsNMP, ChemicalsFurfural, ChemicalsMIBK, ChemicalsMEK, ChemicalsToluene, ChemicalsPropane, ChemicalsOthSolv, ChemicalsAcid, ChemicalsDewaxAids, ChemicalsOth, Chemicals, 
		CatalystsFCC, CatalystsFCCAdditives, CatalystsFCCECat, CatalystsFCCECatSales, CatalystsFCCFresh, CatalystsHYC, CatalystsNKSHYT, CatalystsDHYT, CatalystsVHYT, CatalystsRHYT, CatalystsHYFT, CatalystsCDWax, CatalystsREF, CatalystsHYG, CatalystsS2Plant, CatalystsPetChem, CatalystsOth, Catalysts, 
		PurElec, PurSteam, PurFG, PurLiquid, PurSolid, RefProdFG, RefProdOth, 
		PurOthN2, PurOthH2O, PurOthOth, PurOth, 
		Royalties, 
		EmissionsTaxes, EmissionsPurch, EmissionsCredits, 
		OthVolDemCrude, OthVolDemLightering, OthVolDemProd, OthVolOth, OthVol, 
		GANonPers, InvenCarry, Depreciation, Interest, 
		Cogen, OthRevenue, 
		ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2, PMAA, 
		ExclFireSafety, ExclEnvirFines, ExclOth)
	SELECT o.SubmissionID, o.Currency, e.Scenario, 'ADJ', 
		o.OCCSal, o.MPSSal, 
		o.OCCBenAbs, o.OCCBenInsur, o.OCCBenPension, o.OCCBenSub, o.OCCBenStock, o.OCCBenTaxPen, o.OCCBenTaxMed, o.OCCBenTaxOth, o.OCCBen, 
		o.MPSBenAbs, o.MPSBenInsur, o.MPSBenPension, o.MPSBenSub, o.MPSBenStock, o.MPSBenTaxPen, o.MPSBenTaxMed, o.MPSBenTaxOth, o.MPSBen, 
		o.MaintMatl, o.ContMaintMatl, o.EquipMaint, o.MaintMatlST, 
		o.ContMaintLabor, o.ContMaintInspect, o.ContMaintLaborST, 
		o.OthContProcOp, o.OthContTransOp, o.OthContFire, o.OthContVacTrucks, o.OthContConsult, o.OthContSecurity, o.OthContComputing, o.OthContJan, o.OthContLab, o.OthContFoodSvc, o.OthContAdmin, o.OthContLegal, o.OthContOth, o.OthCont, 
		m.AllocAnnTACost*s.FractionOfYear, 
		o.EnvirDisp, o.EnvirPermits, o.EnvirFines, o.EnvirSpill, o.EnvirLab, o.EnvirEng, o.EnvirMonitor, o.EnvirOth, o.Envir, 
		o.EquipNonMaint, 
		o.Tax, o.InsurBI, o.InsurPC, o.InsurOth, 
		o.OthNonVolSupply, o.OthNonVolSafety, o.OthNonVolNonPersSafety, o.OthNonVolComm, o.OthNonVolDonations, o.OthNonVolNonContribPers, o.OthNonVolDues, o.OthNonVolTravel, o.OthNonVolTrain, o.OthNonVolComputer, o.OthNonVolTanks, o.OthNonVolExtraExpat, o.OthNonVolOth, o.OthNonVol, 
		o.GAPers, 
		o.ChemicalsAntiknock, o.ChemicalsAlkyAcid, o.ChemicalsLube, o.ChemicalsH2OTreat, o.ChemicalsProcess, o.ChemicalsOthAcid, o.ChemicalsGasAdd, o.ChemicalsDieselAdd, o.ChemicalsOthAdd, o.ChemicalsO2, o.ChemicalsClay, o.ChemicalsAmines, 
		o.ChemicalsASESolv, o.ChemicalsWasteH2O, o.ChemicalsNMP, o.ChemicalsFurfural, o.ChemicalsMIBK, o.ChemicalsMEK, o.ChemicalsToluene, o.ChemicalsPropane, o.ChemicalsOthSolv, o.ChemicalsAcid, o.ChemicalsDewaxAids, o.ChemicalsOth, o.Chemicals, 
		o.CatalystsFCC, o.CatalystsFCCAdditives, o.CatalystsFCCECat, o.CatalystsFCCECatSales, o.CatalystsFCCFresh, o.CatalystsHYC, o.CatalystsNKSHYT, o.CatalystsDHYT, o.CatalystsVHYT, o.CatalystsRHYT, o.CatalystsHYFT, o.CatalystsCDWax, o.CatalystsREF, o.CatalystsHYG, o.CatalystsS2Plant, o.CatalystsPetChem, o.CatalystsOth, o.Catalysts, 
		e.PurPowerCostK, e.PurSteamCostK, e.PurFGCostK, e.PurLiquidCostK, e.PurSolidCostK, e.ProdFGCostK, e.ProdOthCostK, 
		o.PurOthN2, o.PurOthH2O, o.PurOthOth, o.PurOth, 
		o.Royalties, 
		o.EmissionsTaxes, o.EmissionsPurch, o.EmissionsCredits, 
		o.OthVolDemCrude, o.OthVolDemLightering, o.OthVolDemProd, o.OthVolOth, o.OthVol, 
		o.GANonPers, o.InvenCarry, o.Depreciation, o.Interest, 
		o.Cogen, o.OthRevenue, 
		o.ThirdPartyTerminalRM, o.ThirdPartyTerminalProd, o.POXO2, o.PMAA, 
		o.ExclFireSafety, o.ExclEnvirFines, o.ExclOth
	FROM OpExAll o INNER JOIN EnergyTotCost e ON e.SubmissionID = o.SubmissionID AND e.Currency = o.Currency
	INNER JOIN MaintTotCost m ON m.SubmissionID = o.SubmissionID AND m.Currency = o.Currency
	INNER JOIN SubmissionsAll s ON s.SubmissionID = o.SubmissionID
	WHERE o.SubmissionID = @SubmissionID AND o.DataType = 'RPTC' AND o.Currency = @Currency

	UPDATE OpExAll 
	SET 	STNonVol = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0) + ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0) + ISNULL(MaintMatlST, 0) + ISNULL(ContMaintLaborST, 0) + ISNULL(OthCont, 0) + ISNULL(TAAdj, 0) + ISNULL(Envir, 0) + ISNULL(OthNonVol, 0) + ISNULL(GAPers, 0),
		STVol = ISNULL(Chemicals, 0) + ISNULL(Catalysts, 0) + ISNULL(Royalties, 0) + ISNULL(PurOth, 0) + ISNULL(PurElec, 0) + ISNULL(PurSteam, 0) + ISNULL(PurFG, 0) + ISNULL(PurLiquid, 0) + ISNULL(PurSolid, 0) + ISNULL(RefProdFG, 0) + ISNULL(RefProdOth, 0) + ISNULL(OthVolDemCrude, 0) + ISNULL(OthVolDemLightering, 0) + ISNULL(OthVolDemProd, 0) + ISNULL(OthVol, 0),
		STNonCash = ISNULL(GANonPers, 0) + ISNULL(InvenCarry, 0) + ISNULL(Depreciation, 0) + ISNULL(Interest, 0),
		TotExpExcl = ISNULL(ExclFireSafety, 0) + ISNULL(ExclEnvirFines, 0) + ISNULL(ExclOth, 0),
		STSal = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0),
		STBen = ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0),
		PersCostExclTA = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0) + ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0) + ISNULL(ContMaintLaborST, 0) + ISNULL(OthCont, 0) + ISNULL(GAPers, 0),
		PersCost = ISNULL(OCCSal, 0) + ISNULL(MPSSal, 0) + ISNULL(OCCBen, 0) + ISNULL(MPSBen, 0) + ISNULL(ContMaintLaborST, 0) + ISNULL(OthCont, 0) + ISNULL(GAPers, 0) + 0.55*ISNULL(TAAdj, 0),
		EnergyCost = ISNULL(PurElec, 0) + ISNULL(PurSteam, 0) + ISNULL(PurFG, 0) + ISNULL(PurLiquid, 0) + ISNULL(PurSolid, 0) + ISNULL(RefProdFG, 0) + ISNULL(RefProdOth, 0)
	WHERE SubmissionID = @SubmissionID AND DataType <> 'RPT'
	UPDATE OpExAll
	SET 	TotCashOpEx = STNonVol + STVol,
		TotRefExp = STNonVol + STVol + STNonCash,
		NEOpEx = STNonVol + STVol - EnergyCost
	WHERE SubmissionID = @SubmissionID AND DataType <> 'RPT'

	FETCH NEXT FROM cCurrencies INTO @Currency, @ConvRate
END
CLOSE cCurrencies
DEALLOCATE cCurrencies

DELETE FROM Divisors WHERE SubmissionID = @SubmissionID
INSERT INTO Divisors(SubmissionID, FactorSet, Divisor, DivValue)
SELECT SubmissionID, f.FactorSet, 'Bbl', NetInputBbl/1000
FROM FactorSets f, MaterialTot m 
WHERE f.RefineryType = @RefineryType AND f.Calculate = 'Y'
AND m.SubmissionID = @SubmissionID AND NetInputBbl > 0
INSERT INTO Divisors(SubmissionID, FactorSet, Divisor, DivValue)
SELECT SubmissionID, f.FactorSet, 'C/Bbl', NetInputBbl/100000
FROM FactorSets f, MaterialTot m 
WHERE f.RefineryType = @RefineryType AND f.Calculate = 'Y'
AND m.SubmissionID = @SubmissionID AND NetInputBbl > 0
INSERT INTO Divisors(SubmissionID, FactorSet, Divisor, DivValue)
SELECT f.SubmissionID, f.FactorSet, 'Edc', f.Edc/1000*s.FractionOfYear
FROM FactorTotCalc f INNER JOIN SubmissionsAll s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND Edc > 0
INSERT INTO Divisors(SubmissionID, FactorSet, Divisor, DivValue)
SELECT s.SubmissionID, f.FactorSet, 'UEdc', f.UEdc*s.NumDays/100000
FROM FactorTotCalc f INNER JOIN SubmissionsAll s ON s.SubmissionID = f.SubmissionID
WHERE s.SubmissionID = @SubmissionID AND f.UEdc > 0
INSERT INTO Divisors(SubmissionID, FactorSet, Divisor, DivValue)
SELECT f.SubmissionID, f.FactorSet, 'NEI', f.NEOpExEffDiv/100000*s.FractionOfYear
FROM FactorTotCalc f INNER JOIN SubmissionsAll s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND NEOpExEffDiv > 0
INSERT INTO Divisors(SubmissionID, FactorSet, Divisor, DivValue)
SELECT SubmissionID, f.FactorSet, 'ADJ', 1
FROM FactorSets f, MaterialTot m 
WHERE f.RefineryType = @RefineryType AND f.Calculate = 'Y'
AND m.SubmissionID = @SubmissionID

