﻿CREATE PROC [dbo].[spEdcStabilizers](@SubmissionID int)
AS
DELETE FROM EdcStabilizers WHERE SubmissionID = @SubmissionID
INSERT INTO EdcStabilizers(SubmissionID, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, 
	AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, 
	AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB)
SELECT SubmissionID, AnnInputBbl, AnnCokeBbl, AnnElecConsMWH, 
	AnnRSCRUDE_RAIL, AnnRSCRUDE_TT, AnnRSCRUDE_TB, AnnRSCRUDE_OMB, AnnRSCRUDE_BB, 
	AnnRSPROD_RAIL, AnnRSPROD_TT, AnnRSPROD_TB, AnnRSPROD_OMB, AnnRSPROD_BB
FROM SubmissionsAll s INNER JOIN LoadedEdcStabilizers l ON l.RefineryID = s.RefineryID AND l.DataSet = s.DataSet
WHERE s.SubmissionID = @SubmissionID AND s.PeriodStart >= l.EffDate AND s.PeriodStart < l.EffUntil

