﻿CREATE PROCEDURE [dbo].[DUMP_GPV_SecondaryProducts]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@DataSetID nvarchar(10),
	@RefNum nvarchar(10),
	@StartDate nvarchar(20)
	
	AS
	
	SELECT s.Location, 
                 s.PeriodStart, 
                 s.PeriodEnd,s.NumDays as DaysInPeriod, 
                 @CurrencyCode as Currency,@UOM as UOM,
                (CASE Category 
                 WHEN 'MPROD' THEN 'Miscellaneous' 
                 WHEN 'ASP' THEN 'ASPHALT' 
                 WHEN 'SOLV' THEN 'Speciality Solvents' 
                 WHEN 'FCHEM' THEN 'Ref. Feedstocks To Chemical Plant' 
                 WHEN 'FLUBE' THEN 'Ref. Feedstocks To Lube Refining' 
                 WHEN 'COKE' THEN 'Saleable Petroleum Coke (FOE)' 
                 WHEN 'OTHRM' THEN 'Other Raw Materials' 
                  END) AS MaterialName,
                 ISNULL(NetBbl,0) as Bbl, ISNULL(NetValueMUS,0)*1000000/ISNULL(NetBbl,1)*dbo.ExchangeRate('USD',@CurrencyCode ,@StartDate) as PriceLocal, 
                 ISNULL(NetValueMUS,0)*1000000/ISNULL(NetBbl,1) as PriceUS, ISNULL(NetValueMUS,0)*1000 As GPVUS  ,ISNULL(NetValueMUS,0)*1000*dbo.ExchangeRate('USD',@CurrencyCode ,@StartDate) As GPVLocal 
                 FROM MaterialSTCalc y,Submissions s  
                WHERE y.Scenario=' + Scenario + ' AND s.SubmissionID=y.SubmissionID AND NetBbl > 0 AND Category in ('ASP', 'COKE', 'FCHEM', 'FLUBE', 'MPROD', 'SOLV') AND y.SubmissionID IN 
                (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=@DataSetID
                 AND RefineryID=@RefNum
                 )  ORDER BY s.PeriodStart DESC

