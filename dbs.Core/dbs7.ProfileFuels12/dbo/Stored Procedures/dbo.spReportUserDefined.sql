﻿CREATE   PROC [dbo].[spReportUserDefined] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, 
u.HeaderText, u.VariableDesc, u.RptValue, u.RptValue_Target, u.RptValue_Avg, u.RptValue_Ytd, u.DecPlaces
FROM UserDefined u INNER JOIN Submissions s ON u.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
ORDER BY s.PeriodStart, u.HeaderText, u.VariableDesc

