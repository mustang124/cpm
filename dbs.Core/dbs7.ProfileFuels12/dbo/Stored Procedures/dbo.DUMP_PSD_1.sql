﻿CREATE PROCEDURE [dbo].[DUMP_PSD_1]
	@CurrencyCode nvarchar(10),
	@StudyYear nvarchar(10),
	@DataSetID nvarchar(10),
	@RefNum nvarchar(10),
	@CapColName nvarchar(20)
	
	AS
	
	SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod, c.UnitID, RTRIM(p.Description) As Description, RTRIM(c.UnitName) As UnitName, 
                RTRIM(c.ProcessID) As ProcessID, RTRIM(c.ProcessType) As ProcessType,   @CapColName  , d.DisplayTextUS, d.DisplayTextMet, 
                fc.EdcNoMult * fpc.MultiFactor as Edc, fc.UEdcNoMult * fpc.MultiFactor as UEdc,  
                MechAvail_Ann as MechAvail, MechAvail as MechAvail_Target,  
                OpAvail_Ann as OpAvail, OpAvail as OpAvail_Target, 
                OnStream_Ann as OnStream, OnStream as OnStream_Target, 
                ISNULL(AnnTACost/  @CapColName  ,0) as TACost, TACost as TACost_Target,  
                ISNULL(AnnRoutCost/  @CapColName  ,0) as RoutCost, RoutCost as RoutCost_Target, u.CurrencyCode  
                FROM Config c  
                INNER JOIN ProcessID_LU p on c.ProcessID = p.ProcessID  
                INNER JOIN FactorCalc fc on fc.SubmissionID = c.SubmissionID and c.UnitID = fc.UnitID  
                INNER JOIN FactorProcessCalc fpc on fc.SubmissionID = fpc.SubmissionID and fpc.ProcessID = c.ProcessID  
                INNER JOIN MaintCalc m on m.SubmissionID = c.SubmissionID and c.UnitID = m.UnitID  
                INNER JOIN MaintCost mc on mc.SubmissionID = c.SubmissionID and c.UnitID = mc.UnitID  
                INNER JOIN DisplayUnits_LU d on p.DisplayUnits = d.DisplayUnits  
                INNER JOIN Submissions s on s.SubmissionID = c.SubmissionID  
                LEFT JOIN UnitTargets u on c.SubmissionID = u.SubmissionID and c.UnitID = u.UnitID  
                WHERE  fc.FactorSet=fpc.FactorSet AND fc.FactorSet=@StudyYear AND  mc.Currency =@CurrencyCode 
                 AND c.Cap > 0 AND p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD')  
                 AND  c.SubmissionID  IN   
                    (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=@DataSetID
                      AND RefineryID=@RefNum 
                     ) ORDER BY s.PeriodStart DESC

