﻿CREATE PROCEDURE [dbo].[DS_MaintTA_Process]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS
BEGIN
	SELECT DISTINCT C.UnitID,TAID,C.SortKey, RTRIM(C.ProcessID) as ProcessID,
                    RTRIM(C.UnitName) as UnitName, TADate, TAHrsDown, TAExpLocal, TACptlLocal, TAOvhdLocal, TACostLocal,
                    TALaborCostLocal,  TAOCCSTH, TAOCCOVT, TAMPSSTH, TampSovtPcnt, TAContOCC, TAContMPS, 
                    PrevTADate, TAExceptions
                    FROM MaintTA M, Config C
                    WHERE RefineryID = @RefineryID AND M.UnitID = C.UnitID AND C.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND C.SubmissionID IN
                    (SELECT Top 1 SubmissionID FROM Submissions WHERE RefineryID=@RefineryID  and DataSet = @DataSet and UseSubmission=1 ORDER BY PeriodStart DESC)
END

