﻿CREATE   PROC [dbo].[spReportValeroBUR] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

DECLARE @AD_KEdc real, @AD_KEdc_Ytd real, @AD_MechAvail real, @AD_MechAvail_Ytd real, @AD_AdjMaintIndex real, @AD_MaintIndex_Ytd real, 
	@AD_UtilPcnt real, @AD_UtilPcnt_Ytd real, @AD_NEOpExEdc real, @AD_NEOpExEdc_Ytd real, 
	@AD_EII real, @AD_EII_Ytd real, @AD_TotWHrEdc real, @AD_TotWhrEdc_Ytd real
DECLARE @BN_KEdc real, @BN_KEdc_Ytd real, @BN_MechAvail real, @BN_MechAvail_Ytd real, @BN_AdjMaintIndex real, @BN_MaintIndex_Ytd real, 
	@BN_UtilPcnt real, @BN_UtilPcnt_Ytd real, @BN_NEOpExEdc real, @BN_NEOpExEdc_Ytd real, 
	@BN_EII real, @BN_EII_Ytd real, @BN_TotWHrEdc real, @BN_TotWhrEdc_Ytd real
DECLARE @CC_KEdc real, @CC_KEdc_Ytd real, @CC_MechAvail real, @CC_MechAvail_Ytd real, @CC_AdjMaintIndex real, @CC_MaintIndex_Ytd real, 
	@CC_UtilPcnt real, @CC_UtilPcnt_Ytd real, @CC_NEOpExEdc real, @CC_NEOpExEdc_Ytd real, 
	@CC_EII real, @CC_EII_Ytd real, @CC_TotWHrEdc real, @CC_TotWhrEdc_Ytd real
DECLARE @HO_KEdc real, @HO_KEdc_Ytd real, @HO_MechAvail real, @HO_MechAvail_Ytd real, @HO_AdjMaintIndex real, @HO_MaintIndex_Ytd real, 
	@HO_UtilPcnt real, @HO_UtilPcnt_Ytd real, @HO_NEOpExEdc real, @HO_NEOpExEdc_Ytd real, 
	@HO_EII real, @HO_EII_Ytd real, @HO_TotWHrEdc real, @HO_TotWhrEdc_Ytd real
DECLARE @MK_KEdc real, @MK_KEdc_Ytd real, @MK_MechAvail real, @MK_MechAvail_Ytd real, @MK_AdjMaintIndex real, @MK_MaintIndex_Ytd real, 
	@MK_UtilPcnt real, @MK_UtilPcnt_Ytd real, @MK_NEOpExEdc real, @MK_NEOpExEdc_Ytd real, 
	@MK_EII real, @MK_EII_Ytd real, @MK_TotWHrEdc real, @MK_TotWhrEdc_Ytd real
DECLARE @MP_KEdc real, @MP_KEdc_Ytd real, @MP_MechAvail real, @MP_MechAvail_Ytd real, @MP_AdjMaintIndex real, @MP_MaintIndex_Ytd real, 
	@MP_UtilPcnt real, @MP_UtilPcnt_Ytd real, @MP_NEOpExEdc real, @MP_NEOpExEdc_Ytd real, 
	@MP_EII real, @MP_EII_Ytd real, @MP_TotWHrEdc real, @MP_TotWhrEdc_Ytd real
DECLARE @PA_KEdc real, @PA_KEdc_Ytd real, @PA_MechAvail real, @PA_MechAvail_Ytd real, @PA_AdjMaintIndex real, @PA_MaintIndex_Ytd real, 
	@PA_UtilPcnt real, @PA_UtilPcnt_Ytd real, @PA_NEOpExEdc real, @PA_NEOpExEdc_Ytd real, 
	@PA_EII real, @PA_EII_Ytd real, @PA_TotWHrEdc real, @PA_TotWhrEdc_Ytd real
DECLARE @QB_KEdc real, @QB_KEdc_Ytd real, @QB_MechAvail real, @QB_MechAvail_Ytd real, @QB_AdjMaintIndex real, @QB_MaintIndex_Ytd real, 
	@QB_UtilPcnt real, @QB_UtilPcnt_Ytd real, @QB_NEOpExEdc real, @QB_NEOpExEdc_Ytd real, 
	@QB_EII real, @QB_EII_Ytd real, @QB_TotWHrEdc real, @QB_TotWhrEdc_Ytd real
DECLARE @SC_KEdc real, @SC_KEdc_Ytd real, @SC_MechAvail real, @SC_MechAvail_Ytd real, @SC_AdjMaintIndex real, @SC_MaintIndex_Ytd real, 
	@SC_UtilPcnt real, @SC_UtilPcnt_Ytd real, @SC_NEOpExEdc real, @SC_NEOpExEdc_Ytd real, 
	@SC_EII real, @SC_EII_Ytd real, @SC_TotWHrEdc real, @SC_TotWhrEdc_Ytd real
DECLARE @TC_KEdc real, @TC_KEdc_Ytd real, @TC_MechAvail real, @TC_MechAvail_Ytd real, @TC_AdjMaintIndex real, @TC_MaintIndex_Ytd real, 
	@TC_UtilPcnt real, @TC_UtilPcnt_Ytd real, @TC_NEOpExEdc real, @TC_NEOpExEdc_Ytd real, 
	@TC_EII real, @TC_EII_Ytd real, @TC_TotWHrEdc real, @TC_TotWhrEdc_Ytd real
DECLARE @TR_KEdc real, @TR_KEdc_Ytd real, @TR_MechAvail real, @TR_MechAvail_Ytd real, @TR_AdjMaintIndex real, @TR_MaintIndex_Ytd real, 
	@TR_UtilPcnt real, @TR_UtilPcnt_Ytd real, @TR_NEOpExEdc real, @TR_NEOpExEdc_Ytd real, 
	@TR_EII real, @TR_EII_Ytd real, @TR_TotWHrEdc real, @TR_TotWhrEdc_Ytd real
DECLARE @WM_KEdc real, @WM_KEdc_Ytd real, @WM_MechAvail real, @WM_MechAvail_Ytd real, @WM_AdjMaintIndex real, @WM_MaintIndex_Ytd real, 
	@WM_UtilPcnt real, @WM_UtilPcnt_Ytd real, @WM_NEOpExEdc real, @WM_NEOpExEdc_Ytd real, 
	@WM_EII real, @WM_EII_Ytd real, @WM_TotWHrEdc real, @WM_TotWhrEdc_Ytd real
DECLARE @VALERO_KEdc real, @VALERO_KEdc_Ytd real, @VALERO_MechAvail real, @VALERO_MechAvail_Ytd real, @VALERO_AdjMaintIndex real, @VALERO_MaintIndex_Ytd real, 
	@VALERO_UtilPcnt real, @VALERO_UtilPcnt_Ytd real, @VALERO_NEOpExEdc real, @VALERO_NEOpExEdc_Ytd real, 
	@VALERO_EII real, @VALERO_EII_Ytd real, @VALERO_TotWHrEdc real, @VALERO_TotWhrEdc_Ytd real

SELECT @AD_KEdc = Edc/1000, @AD_KEdc_Ytd = Edc_Ytd/1000, 
	@AD_MechAvail = MechAvail, @AD_MechAvail_Ytd = MechAvail_Ytd,
	@AD_AdjMaintIndex = RoutIndex + TAIndex_Avg, @AD_MaintIndex_Ytd = MaintIndex_Ytd,
	@AD_UtilPcnt = UtilPcnt, @AD_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@AD_NEOpExEdc = NEOpExEdc, @AD_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@AD_EII = EII, @AD_EII_Ytd = EII_Ytd, @AD_TotWHrEdc = TotWHrEdc, @AD_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('75NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @BN_KEdc = Edc/1000, @BN_KEdc_Ytd = Edc_Ytd/1000, 
	@BN_MechAvail = MechAvail, @BN_MechAvail_Ytd = MechAvail_Ytd,
	@BN_AdjMaintIndex = RoutIndex + TAIndex_Avg, @BN_MaintIndex_Ytd = MaintIndex_Ytd,
	@BN_UtilPcnt = UtilPcnt, @BN_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@BN_NEOpExEdc = NEOpExEdc, @BN_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@BN_EII = EII, @BN_EII_Ytd = EII_Ytd, @BN_TotWHrEdc = TotWHrEdc, @BN_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('33NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @CC_KEdc = Edc/1000, @CC_KEdc_Ytd = Edc_Ytd/1000, 
	@CC_MechAvail = MechAvail, @CC_MechAvail_Ytd = MechAvail_Ytd,
	@CC_AdjMaintIndex = RoutIndex + TAIndex_Avg, @CC_MaintIndex_Ytd = MaintIndex_Ytd,
	@CC_UtilPcnt = UtilPcnt, @CC_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@CC_NEOpExEdc = NEOpExEdc, @CC_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@CC_EII = EII, @CC_EII_Ytd = EII_Ytd, @CC_TotWHrEdc = TotWHrEdc, @CC_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('218NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @HO_KEdc = Edc/1000, @HO_KEdc_Ytd = Edc_Ytd/1000, 
	@HO_MechAvail = MechAvail, @HO_MechAvail_Ytd = MechAvail_Ytd,
	@HO_AdjMaintIndex = RoutIndex + TAIndex_Avg, @HO_MaintIndex_Ytd = MaintIndex_Ytd,
	@HO_UtilPcnt = UtilPcnt, @HO_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@HO_NEOpExEdc = NEOpExEdc, @HO_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@HO_EII = EII, @HO_EII_Ytd = EII_Ytd, @HO_TotWHrEdc = TotWHrEdc, @HO_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('98NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @MK_KEdc = Edc/1000, @MK_KEdc_Ytd = Edc_Ytd/1000, 
	@MK_MechAvail = MechAvail, @MK_MechAvail_Ytd = MechAvail_Ytd,
	@MK_AdjMaintIndex = RoutIndex + TAIndex_Avg, @MK_MaintIndex_Ytd = MaintIndex_Ytd,
	@MK_UtilPcnt = UtilPcnt, @MK_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@MK_NEOpExEdc = NEOpExEdc, @MK_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@MK_EII = EII, @MK_EII_Ytd = EII_Ytd, @MK_TotWHrEdc = TotWHrEdc, @MK_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('28NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @MP_KEdc = Edc/1000, @MP_KEdc_Ytd = Edc_Ytd/1000, 
	@MP_MechAvail = MechAvail, @MP_MechAvail_Ytd = MechAvail_Ytd,
	@MP_AdjMaintIndex = RoutIndex + TAIndex_Avg, @MP_MaintIndex_Ytd = MaintIndex_Ytd,
	@MP_UtilPcnt = UtilPcnt, @MP_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@MP_NEOpExEdc = NEOpExEdc, @MP_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@MP_EII = EII, @MP_EII_Ytd = EII_Ytd, @MP_TotWHrEdc = TotWHrEdc, @MP_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('38NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @PA_KEdc = Edc/1000, @PA_KEdc_Ytd = Edc_Ytd/1000, 
	@PA_MechAvail = MechAvail, @PA_MechAvail_Ytd = MechAvail_Ytd,
	@PA_AdjMaintIndex = RoutIndex + TAIndex_Avg, @PA_MaintIndex_Ytd = MaintIndex_Ytd,
	@PA_UtilPcnt = UtilPcnt, @PA_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@PA_NEOpExEdc = NEOpExEdc, @PA_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@PA_EII = EII, @PA_EII_Ytd = EII_Ytd, @PA_TotWHrEdc = TotWHrEdc, @PA_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('19NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @QB_KEdc = Edc/1000, @QB_KEdc_Ytd = Edc_Ytd/1000, 
	@QB_MechAvail = MechAvail, @QB_MechAvail_Ytd = MechAvail_Ytd,
	@QB_AdjMaintIndex = RoutIndex + TAIndex_Avg, @QB_MaintIndex_Ytd = MaintIndex_Ytd,
	@QB_UtilPcnt = UtilPcnt, @QB_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@QB_NEOpExEdc = NEOpExEdc, @QB_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@QB_EII = EII, @QB_EII_Ytd = EII_Ytd, @QB_TotWHrEdc = TotWHrEdc, @QB_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('110NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @SC_KEdc = Edc/1000, @SC_KEdc_Ytd = Edc_Ytd/1000, 
	@SC_MechAvail = MechAvail, @SC_MechAvail_Ytd = MechAvail_Ytd,
	@SC_AdjMaintIndex = RoutIndex + TAIndex_Avg, @SC_MaintIndex_Ytd = MaintIndex_Ytd,
	@SC_UtilPcnt = UtilPcnt, @SC_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@SC_NEOpExEdc = NEOpExEdc, @SC_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@SC_EII = EII, @SC_EII_Ytd = EII_Ytd, @SC_TotWHrEdc = TotWHrEdc, @SC_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('217NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @TC_KEdc = Edc/1000, @TC_KEdc_Ytd = Edc_Ytd/1000, 
	@TC_MechAvail = MechAvail, @TC_MechAvail_Ytd = MechAvail_Ytd,
	@TC_AdjMaintIndex = RoutIndex + TAIndex_Avg, @TC_MaintIndex_Ytd = MaintIndex_Ytd,
	@TC_UtilPcnt = UtilPcnt, @TC_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@TC_NEOpExEdc = NEOpExEdc, @TC_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@TC_EII = EII, @TC_EII_Ytd = EII_Ytd, @TC_TotWHrEdc = TotWHrEdc, @TC_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('72NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @TR_KEdc = Edc/1000, @TR_KEdc_Ytd = Edc_Ytd/1000, 
	@TR_MechAvail = MechAvail, @TR_MechAvail_Ytd = MechAvail_Ytd,
	@TR_AdjMaintIndex = RoutIndex + TAIndex_Avg, @TR_MaintIndex_Ytd = MaintIndex_Ytd,
	@TR_UtilPcnt = UtilPcnt, @TR_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@TR_NEOpExEdc = NEOpExEdc, @TR_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@TR_EII = EII, @TR_EII_Ytd = EII_Ytd, @TR_TotWHrEdc = TotWHrEdc, @TR_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('29NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @WM_KEdc = Edc/1000, @WM_KEdc_Ytd = Edc_Ytd/1000, 
	@WM_MechAvail = MechAvail, @WM_MechAvail_Ytd = MechAvail_Ytd,
	@WM_AdjMaintIndex = RoutIndex + TAIndex_Avg, @WM_MaintIndex_Ytd = MaintIndex_Ytd,
	@WM_UtilPcnt = UtilPcnt, @WM_UtilPcnt_Ytd = UtilPcnt_Ytd,
	@WM_NEOpExEdc = NEOpExEdc, @WM_NEOpExEdc_Ytd = NEOpExEdc_Ytd,
	@WM_EII = EII, @WM_EII_Ytd = EII_Ytd, @WM_TotWHrEdc = TotWHrEdc, @WM_TotWhrEdc_Ytd = TotWhrEdc_Ytd
FROM GenSum
WHERE SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission('91NSA', @PeriodYear, @PeriodMonth, 'Actual'))
	AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT 	@VALERO_KEdc = SUM(Edc)/1E6, @VALERO_KEdc_Ytd = SUM(Edc_Ytd)/1E6, 
	@VALERO_MechAvail = SUM(MechAvail*Edc)/SUM(CASE WHEN Edc > 0 THEN Edc END), 
	@VALERO_MechAvail_Ytd = SUM(MechAvail_Ytd*Edc_Ytd)/SUM(CASE WHEN Edc_Ytd > 0 THEN Edc_Ytd END),
	@VALERO_AdjMaintIndex = SUM((RoutIndex + TAIndex_Avg)*Edc)/SUM(CASE WHEN Edc>0 THEN Edc END), 
	@VALERO_MaintIndex_Ytd = SUM(MaintIndex_Ytd*Edc_Ytd)/SUM(CASE WHEN Edc > 0 THEN Edc_Ytd END),
	@VALERO_UtilPcnt = SUM(UtilPcnt*Edc)/SUM(CASE WHEN Edc>0 THEN Edc END),
	@VALERO_UtilPcnt_Ytd = SUM(UtilPcnt_Ytd*Edc_Ytd)/SUM(CASE WHEN Edc > 0 THEN Edc_Ytd END),
	@VALERO_NEOpExEdc = SUM(NEOpExEdc*Edc)/SUM(CASE WHEN Edc>0 THEN Edc END),
	@VALERO_NEOpExEdc_Ytd = SUM(NEOpExEdc_Ytd*Edc_Ytd)/SUM(CASE WHEN Edc > 0 THEN Edc_Ytd END),
	@VALERO_TotWHrEdc = SUM(TotWHrEdc*Edc)/SUM(CASE WHEN Edc>0 THEN Edc END),
	@VALERO_TotWhrEdc_Ytd = SUM(TotWhrEdc_Ytd*Edc_Ytd)/SUM(CASE WHEN Edc > 0 THEN Edc_Ytd END)
FROM GenSum
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA') 
AND DataSet='Actual' AND DATEPART(yyyy, PeriodStart) = @PeriodYear AND DATEPART(mm, PeriodStart) = @PeriodMonth
AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario
AND EXISTS (SELECT * FROM Submissions s WHERE s.SubmissionID = GenSum.SubmissionID AND s.UseSubmission = 1)

SELECT @VALERO_EII = 100*SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays)
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA') 
AND s.DataSet = 'Actual' AND s.PeriodYear = @PeriodYear AND s.PeriodMonth = @PeriodMonth AND s.UseSubmission = 1
AND a.FactorSet = @FactorSet AND a.EnergyUseDay > 0 AND a.TotStdEnergy> 0 AND s.NumDays > 0

SELECT @VALERO_EII_Ytd = 100*SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) 
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA') 
AND s.DataSet = 'Actual' AND s.PeriodYear = @PeriodYear AND s.PeriodMonth <= @PeriodMonth AND s.UseSubmission = 1
AND a.FactorSet = @FactorSet AND a.EnergyUseDay > 0 AND a.TotStdEnergy> 0 AND s.NumDays > 0

SELECT  AD_KEdc = @AD_KEdc , AD_KEdc_Ytd = @AD_KEdc_Ytd, AD_MechAvail = @AD_MechAvail, AD_MechAvail_Ytd = @AD_MechAvail_Ytd, 
	AD_AdjMaintIndex = @AD_AdjMaintIndex, AD_AdjMaintIndex_Ytd = @AD_MaintIndex_Ytd, 
	AD_UtilPcnt = @AD_UtilPcnt, AD_UtilPcnt_Ytd = @AD_UtilPcnt_Ytd, 
	AD_NEOpExEdc = @AD_NEOpExEdc, AD_NEOpExEdc_Ytd = @AD_NEOpExEdc_Ytd, 
	AD_EII = @AD_EII, AD_EII_Ytd = @AD_EII_Ytd, AD_TotWHrEdc = @AD_TotWHrEdc, AD_TotWhrEdc_Ytd = @AD_TotWhrEdc_Ytd,
	BN_KEdc = @BN_KEdc, BN_KEdc_Ytd = @BN_KEdc_Ytd, BN_MechAvail = @BN_MechAvail, BN_MechAvail_Ytd = @BN_MechAvail_Ytd, 
	BN_AdjMaintIndex = @BN_AdjMaintIndex, BN_AdjMaintIndex_Ytd = @BN_MaintIndex_Ytd, 
	BN_UtilPcnt = @BN_UtilPcnt, BN_UtilPcnt_Ytd = @BN_UtilPcnt_Ytd, 
	BN_NEOpExEdc = @BN_NEOpExEdc, BN_NEOpExEdc_Ytd = @BN_NEOpExEdc_Ytd, 
	BN_EII = @BN_EII, BN_EII_Ytd = @BN_EII_Ytd, BN_TotWHrEdc = @BN_TotWHrEdc, BN_TotWhrEdc_Ytd = @BN_TotWhrEdc_Ytd,
	CC_KEdc = @CC_KEdc, CC_KEdc_Ytd = @CC_KEdc_Ytd, CC_MechAvail = @CC_MechAvail, CC_MechAvail_Ytd = @CC_MechAvail_Ytd, 
	CC_AdjMaintIndex = @CC_AdjMaintIndex, CC_AdjMaintIndex_Ytd = @CC_MaintIndex_Ytd, 
	CC_UtilPcnt = @CC_UtilPcnt, CC_UtilPcnt_Ytd = @CC_UtilPcnt_Ytd, 
	CC_NEOpExEdc = @CC_NEOpExEdc, CC_NEOpExEdc_Ytd = @CC_NEOpExEdc_Ytd, 
	CC_EII = @CC_EII, CC_EII_Ytd = @CC_EII_Ytd, CC_TotWHrEdc = @CC_TotWHrEdc, CC_TotWhrEdc_Ytd = @CC_TotWhrEdc_Ytd,
	HO_KEdc = @HO_KEdc, HO_KEdc_Ytd = @HO_KEdc_Ytd, HO_MechAvail = @HO_MechAvail, HO_MechAvail_Ytd = @HO_MechAvail_Ytd, 
	HO_AdjMaintIndex = @HO_AdjMaintIndex, HO_AdjMaintIndex_Ytd = @HO_MaintIndex_Ytd, 
	HO_UtilPcnt = @HO_UtilPcnt, HO_UtilPcnt_Ytd = @HO_UtilPcnt_Ytd, 
	HO_NEOpExEdc = @HO_NEOpExEdc, HO_NEOpExEdc_Ytd = @HO_NEOpExEdc_Ytd, 
	HO_EII = @HO_EII, HO_EII_Ytd = @HO_EII_Ytd, HO_TotWHrEdc = @HO_TotWHrEdc, HO_TotWhrEdc_Ytd = @HO_TotWhrEdc_Ytd,
	MK_KEdc = @MK_KEdc, MK_KEdc_Ytd = @MK_KEdc_Ytd, MK_MechAvail = @MK_MechAvail, MK_MechAvail_Ytd = @MK_MechAvail_Ytd, 
	MK_AdjMaintIndex = @MK_AdjMaintIndex, MK_AdjMaintIndex_Ytd = @MK_MaintIndex_Ytd, 
	MK_UtilPcnt = @MK_UtilPcnt, MK_UtilPcnt_Ytd = @MK_UtilPcnt_Ytd, 
	MK_NEOpExEdc = @MK_NEOpExEdc, MK_NEOpExEdc_Ytd = @MK_NEOpExEdc_Ytd, 
	MK_EII = @MK_EII, MK_EII_Ytd = @MK_EII_Ytd, MK_TotWHrEdc = @MK_TotWHrEdc, MK_TotWhrEdc_Ytd = @MK_TotWhrEdc_Ytd,
	MP_KEdc = @MP_KEdc, MP_KEdc_Ytd = @MP_KEdc_Ytd, MP_MechAvail = @MP_MechAvail, MP_MechAvail_Ytd = @MP_MechAvail_Ytd, 
	MP_AdjMaintIndex = @MP_AdjMaintIndex, MP_AdjMaintIndex_Ytd = @MP_MaintIndex_Ytd, 
	MP_UtilPcnt = @MP_UtilPcnt, MP_UtilPcnt_Ytd = @MP_UtilPcnt_Ytd, 
	MP_NEOpExEdc = @MP_NEOpExEdc, MP_NEOpExEdc_Ytd = @MP_NEOpExEdc_Ytd, 
	MP_EII = @MP_EII, MP_EII_Ytd = @MP_EII_Ytd, MP_TotWHrEdc = @MP_TotWHrEdc, MP_TotWhrEdc_Ytd = @MP_TotWhrEdc_Ytd,
	PA_KEdc = @PA_KEdc, PA_KEdc_Ytd = @PA_KEdc_Ytd, PA_MechAvail = @PA_MechAvail, PA_MechAvail_Ytd = @PA_MechAvail_Ytd, 
	PA_AdjMaintIndex = @PA_AdjMaintIndex, PA_AdjMaintIndex_Ytd = @PA_MaintIndex_Ytd, 
	PA_UtilPcnt = @PA_UtilPcnt, PA_UtilPcnt_Ytd = @PA_UtilPcnt_Ytd, 
	PA_NEOpExEdc = @PA_NEOpExEdc, PA_NEOpExEdc_Ytd = @PA_NEOpExEdc_Ytd, 
	PA_EII = @PA_EII, PA_EII_Ytd = @PA_EII_Ytd, PA_TotWHrEdc = @PA_TotWHrEdc, PA_TotWhrEdc_Ytd = @PA_TotWhrEdc_Ytd,
	QB_KEdc = @QB_KEdc, QB_KEdc_Ytd = @QB_KEdc_Ytd, QB_MechAvail = @QB_MechAvail, QB_MechAvail_Ytd = @QB_MechAvail_Ytd, 
	QB_AdjMaintIndex = @QB_AdjMaintIndex, QB_AdjMaintIndex_Ytd = @QB_MaintIndex_Ytd, 
	QB_UtilPcnt = @QB_UtilPcnt, QB_UtilPcnt_Ytd = @QB_UtilPcnt_Ytd, 
	QB_NEOpExEdc = @QB_NEOpExEdc, QB_NEOpExEdc_Ytd = @QB_NEOpExEdc_Ytd, 
	QB_EII = @QB_EII, QB_EII_Ytd = @QB_EII_Ytd, QB_TotWHrEdc = @QB_TotWHrEdc, QB_TotWhrEdc_Ytd = @QB_TotWhrEdc_Ytd,
	SC_KEdc = @SC_KEdc, SC_KEdc_Ytd = @SC_KEdc_Ytd, SC_MechAvail = @SC_MechAvail, SC_MechAvail_Ytd = @SC_MechAvail_Ytd, 
	SC_AdjMaintIndex = @SC_AdjMaintIndex, SC_AdjMaintIndex_Ytd = @SC_MaintIndex_Ytd, 
	SC_UtilPcnt = @SC_UtilPcnt, SC_UtilPcnt_Ytd = @SC_UtilPcnt_Ytd, 
	SC_NEOpExEdc = @SC_NEOpExEdc, SC_NEOpExEdc_Ytd = @SC_NEOpExEdc_Ytd, 
	SC_EII = @SC_EII, SC_EII_Ytd = @SC_EII_Ytd, SC_TotWHrEdc = @SC_TotWHrEdc, SC_TotWhrEdc_Ytd = @SC_TotWhrEdc_Ytd,
	TC_KEdc = @TC_KEdc, TC_KEdc_Ytd = @TC_KEdc_Ytd, TC_MechAvail = @TC_MechAvail, TC_MechAvail_Ytd = @TC_MechAvail_Ytd, 
	TC_AdjMaintIndex = @TC_AdjMaintIndex, TC_AdjMaintIndex_Ytd = @TC_MaintIndex_Ytd, 
	TC_UtilPcnt = @TC_UtilPcnt, TC_UtilPcnt_Ytd = @TC_UtilPcnt_Ytd, 
	TC_NEOpExEdc = @TC_NEOpExEdc, TC_NEOpExEdc_Ytd = @TC_NEOpExEdc_Ytd, 
	TC_EII = @TC_EII, TC_EII_Ytd = @TC_EII_Ytd, TC_TotWHrEdc = @TC_TotWHrEdc, TC_TotWhrEdc_Ytd = @TC_TotWhrEdc_Ytd,
	TR_KEdc = @TR_KEdc, TR_KEdc_Ytd = @TR_KEdc_Ytd, TR_MechAvail = @TR_MechAvail, TR_MechAvail_Ytd = @TR_MechAvail_Ytd, 
	TR_AdjMaintIndex = @TR_AdjMaintIndex, TR_AdjMaintIndex_Ytd = @TR_MaintIndex_Ytd, 
	TR_UtilPcnt = @TR_UtilPcnt, TR_UtilPcnt_Ytd = @TR_UtilPcnt_Ytd, 
	TR_NEOpExEdc = @TR_NEOpExEdc, TR_NEOpExEdc_Ytd = @TR_NEOpExEdc_Ytd, 
	TR_EII = @TR_EII, TR_EII_Ytd = @TR_EII_Ytd, TR_TotWHrEdc = @TR_TotWHrEdc, TR_TotWhrEdc_Ytd = @TR_TotWhrEdc_Ytd,
	WM_KEdc = @WM_KEdc, WM_KEdc_Ytd = @WM_KEdc_Ytd, WM_MechAvail = @WM_MechAvail, WM_MechAvail_Ytd = @WM_MechAvail_Ytd, 
	WM_AdjMaintIndex = @WM_AdjMaintIndex, WM_AdjMaintIndex_Ytd = @WM_MaintIndex_Ytd, 
	WM_UtilPcnt = @WM_UtilPcnt, WM_UtilPcnt_Ytd = @WM_UtilPcnt_Ytd, 
	WM_NEOpExEdc = @WM_NEOpExEdc, WM_NEOpExEdc_Ytd = @WM_NEOpExEdc_Ytd, 
	WM_EII = @WM_EII, WM_EII_Ytd = @WM_EII_Ytd, WM_TotWHrEdc = @WM_TotWHrEdc, WM_TotWhrEdc_Ytd = @WM_TotWhrEdc_Ytd,
	VALERO_KEdc = @VALERO_KEdc, VALERO_KEdc_Ytd = @VALERO_KEdc_Ytd, VALERO_MechAvail = @VALERO_MechAvail, VALERO_MechAvail_Ytd = @VALERO_MechAvail_Ytd, 
	VALERO_AdjMaintIndex = @VALERO_AdjMaintIndex, VALERO_AdjMaintIndex_Ytd = @VALERO_MaintIndex_Ytd, 
	VALERO_UtilPcnt = @VALERO_UtilPcnt, VALERO_UtilPcnt_Ytd = @VALERO_UtilPcnt_Ytd, 
	VALERO_NEOpExEdc = @VALERO_NEOpExEdc, VALERO_NEOpExEdc_Ytd = @VALERO_NEOpExEdc_Ytd, 
	VALERO_EII = @VALERO_EII, VALERO_EII_Ytd = @VALERO_EII_Ytd, VALERO_TotWHrEdc = @VALERO_TotWHrEdc, VALERO_TotWhrEdc_Ytd = @VALERO_TotWhrEdc_Ytd

