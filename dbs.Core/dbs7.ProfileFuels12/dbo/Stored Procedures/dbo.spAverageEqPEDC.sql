﻿CREATE  PROC [dbo].[spAverageEqPEdc](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @OCCEqPEdc real OUTPUT, @MPSEqPEdc real OUTPUT, @TotEqPEdc real OUTPUT)
AS
SELECT p.SectionID, TotEqPEdc = SUM(p.TotEqPEdc*p.EqPEdcDivisor)/SUM(p.EqPEdcDivisor)
INTO #calc
FROM PersSTCalc p 
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND p.SectionID IN ('TO','TM','TP')
GROUP BY p.FactorSet, p.SectionID
HAVING SUM(p.EqPEdcDivisor) > 0

SELECT 	@OCCEqPEdc = AVG(CASE WHEN SectionID = 'TO' THEN TotEqPEdc END),
	@MPSEqPEdc = AVG(CASE WHEN SectionID = 'TM' THEN TotEqPEdc END),
	@TotEqPEdc = AVG(CASE WHEN SectionID = 'TP' THEN TotEqPEdc END)
FROM #calc

