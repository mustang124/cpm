﻿CREATE      PROC [dbo].[spAverageOpEx](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @Currency CurrencyCode, @Scenario Scenario,
	@TotCashOpExUEdc real OUTPUT, @VolOpExUEdc real OUTPUT, @NonVolOpExUEdc real OUTPUT, 
	@NEOpExUEdc real OUTPUT, @NEOpExEdc real OUTPUT, @ROI real OUTPUT, @VAI real OUTPUT, @NEI real OUTPUT, @RV real OUTPUT, @TotCptl real OUTPUT)
AS

SELECT @ROI = CASE WHEN SUM(r.TotCptl*s.FractionOfYear) > 0 THEN SUM(r.ROI*r.TotCptl*s.FractionOfYear)/SUM(r.TotCptl*s.FractionOfYear) ELSE NULL END,
@VAI = CASE WHEN SUM(f.UEdc*s.NumDays) > 0 THEN SUM(r.VAI*f.UEdc*s.NumDays)/SUM(f.UEdc*s.NumDays) ELSE NULL END,
@RV = SUM(r.RV*s.FractionOfYear)/SUM(s.FractionOfYear),
@TotCptl = SUM(r.TotCptl*s.FractionOfYear)/SUM(s.FractionOfYear)
FROM ROICalc r INNER JOIN Submissions s ON s.SubmissionID = r.SubmissionID
INNER JOIN FactorTotCalc f ON f.SubmissionID = r.SubmissionID AND f.FactorSet = r.FactorSet
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND r.Scenario = @Scenario AND r.FactorSet = @FactorSet AND r.Currency = @Currency

SET @Scenario = 'CLIENT' -- Using Client Energy prices for all pricing scenarios

SELECT @TotCashOpExUEdc = SUM(TotCashOpEx*Divisor)/SUM(Divisor),
@VolOpExUEdc = SUM(STVol*Divisor)/SUM(Divisor),
@NonVolOpExUEdc = SUM(STNonVol*Divisor)/SUM(Divisor),
@NEOpExUEdc = SUM(NEOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'UEdc' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NEOpExEdc = SUM(NEOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'Edc' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

SELECT @NEI = SUM(NEOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND o.DataType = 'NEI' AND o.Scenario = @Scenario AND o.FactorSet = @FactorSet AND o.Currency = @Currency

