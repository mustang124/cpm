﻿CREATE           PROC [dbo].[spLoadFactorData](@SubmissionID int)
AS
SET NOCOUNT ON

DELETE FROM UnitFactorData WHERE SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, FeedC3Olefin, FeedC4Olefin, FeedC5Olefin, FeedTotalOlefin)
SELECT SubmissionID, UnitID, ProcessID,
FeedC3Olefin = ISNULL((SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedC3Olefin'),0),
FeedC4Olefin = ISNULL((SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedC4Olefin'),0),
FeedC5Olefin = ISNULL((SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedC5Olefin'),0),
FeedTotalOlefin = ISNULL((SELECT SUM(SAValue) FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property IN ('FeedC3Olefin','FeedC4Olefin','FeedC5Olefin')),0)
FROM Config
WHERE ProcessID = 'ALKY'
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, FeedGravity, BtmProdPcnt, FZTemp, FZPress)
SELECT SubmissionID, UnitID, ProcessID,
FeedGravity = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedGravity'),
BtmProdPcnt = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'BtmProdPcnt'),
FZTemp = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FZTemp'),
FZPress = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FZPress')
FROM Config
WHERE ProcessID IN ('CDU', 'RERUN', 'VAC', 'VBR')
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, FeedGravity, FeedConcarbon)
SELECT SubmissionID, UnitID, ProcessID,
FeedGravity = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedGravity'),
FeedConcarbon = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedConCarbon')
FROM Config
WHERE ProcessID = 'COK'
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, Conv, Coke, FeedAniline, FeedASTM10, FeedASTM90)
SELECT SubmissionID, UnitID, ProcessID,
Conv = ISNULL((SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'Conv'),(SELECT 100-SUM(SAValue) FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property IN ('ProdDSO','ProdHCO','ProdLCO'))),
Coke = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'Coke'),
FeedAniline = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedAniline'),
FeedASTM10 = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedASTM10'),
FeedASTM90 = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedASTM90')
FROM Config
WHERE ProcessID = 'FCC'
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, ReactorPress, ProdDiesel, ProdHGO, ProdHvHycCrk, ProdLtHycCrk, ProdBal,
		H2Cons, VGOFeed, EIIVGODesulf)
SELECT SubmissionID, UnitID, ProcessID,
ReactorPress = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ReactorPressure'),
ProdDiesel = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ProdDiesel'),
ProdHGO = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ProdHGO'),
ProdHvHycCrk = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ProdHvHycCrk'),
ProdLtHycCrk = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ProdLtHycCrk'),
ProdBal = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ProdBal'),
H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'H2Cons'),
--Yield = ISNULL((SELECT SUM(SAValue) FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property IN ('ProdC2','ProdC3','ProdC4','ProdC4Lt','ProdLtHycCrk','ProdHvHycCrk','ProdKJet','ProdDiesel','ProdHGO','ProdBal')), 0),
--TotalFeed = ISNULL((SELECT SUM(SAValue) FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property IN ('FeedVirginGO','FeedVirgin','FeedCrack','FeedCrackedLGO','FeedAGO','FeedARC','FeedVacResid','FeedCrackedHGO','FeedVGO','FeedDAO','FeedLubeExtracts')), 0),
VGOFeed = ISNULL((SELECT SUM(SAValue) FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property IN ('FeedCrackedHGO','FeedVGO','FeedDAO','FeedLubeExtracts')), 0),
EIIVGODesulf = ISNULL((SELECT SUM(SAValue) FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property IN ('ProdHGO','ProdBal')), 0)
FROM Config
WHERE ProcessID = 'HYC'
AND SubmissionID = @SubmissionID

UPDATE UnitFactorData
SET 	EIIVGOtoLt = CASE WHEN VGOFeed > EIIVGODesulf THEN VGOFeed - EIIVGODesulf ELSE 0 END,
	EIIDieselDesulf = CASE WHEN VGOFeed < EIIVGODesulf THEN ProdDiesel WHEN EIIVGODesulf + ISNULL(ProdDiesel, 0) - VGOFeed < 0 THEN 0 ELSE EIIVGODesulf + ISNULL(ProdDiesel, 0) - VGOFeed END
WHERE ProcessID = 'HYC' AND SubmissionID = @SubmissionID

UPDATE UnitFactorData
SET 	EIIDieselToLt = 100 - (EIIVGOtoLt + EIIVGODesulf + EIIDieselDesulf)
WHERE ProcessID = 'HYC' AND SubmissionID = @SubmissionID

UPDATE UnitFactorData
SET 	TotalFeed = NULL, VGOFeed = NULL, EIIVGODesulf = NULL, EIIVGOtoLt = NULL, EIIDieselDesulf = NULL, EIIDieselToLt = NULL
WHERE ProcessID = 'HYC' AND SubmissionID = @SubmissionID AND NOT EXISTS (SELECT * FROM ProcessData p WHERE p.SubmissionID = UnitFactorData.SubmissionID AND p.UnitID = UnitFactorData.UnitID AND p.Property IN ('FeedCrackedHGO','FeedVGO','FeedDAO','FeedLubeExtracts'))

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, FeedH2, FeedRateGas)
SELECT SubmissionID, UnitID, ProcessID,
FeedH2 = ISNULL((SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedH2'), 0),
FeedRateGas = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedRateGas')
FROM Config
WHERE ProcessID = 'HYG'
AND SubmissionID = @SubmissionID

IF EXISTS (SELECT * FROM SubmissionsAll WHERE SubmissionID = @SubmissionID AND PeriodYear < 2010)
	UPDATE UnitFactorData
	SET EIIFeedH2PcntProdH2 = 0
	WHERE SubmissionID = @SubmissionID AND UnitID IN (SELECT UnitID FROM Config WHERE Config.SubmissionID = UnitFactorData.SubmissionID AND Config.ProcessID = 'HYG' AND Config.ProcessType = 'HSN')


INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, FeedCrack)
SELECT SubmissionID, UnitID, ProcessID,
FeedCrack = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedCrack')
FROM Config
WHERE ProcessID = 'NHYT'
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, FeedGravity, FeedNap, FeedArom, ProdRONC, SumTempDiff)
SELECT SubmissionID, UnitID, ProcessID,
FeedGravity = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedGravity'),
FeedNap = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedNap'),
FeedArom = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'FeedArom'),
ProdRONC = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ProdRONC'),
SumTempDiff = (SELECT ABS(SAValue) FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'SumTempDiff')
FROM Config
WHERE ProcessID = 'REF'
AND SubmissionID = @SubmissionID

UPDATE UnitFactorData
SET Nap2Arom = ISNULL(FeedNap, 0) + 2*ISNULL(FeedArom, 0)
WHERE ProcessID = 'REF' AND SubmissionID = @SubmissionID 

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, SolventRatio)
SELECT SubmissionID, UnitID, ProcessID,
SolventRatio = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'SolventRatio')
FROM Config
WHERE ProcessID = 'SDA'
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, ReactorPress)
SELECT SubmissionID, UnitID, ProcessID,
ReactorPress = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ReactorPressure')
FROM Config
WHERE ProcessID = 'WHYFT'
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, ReactorPress, LN_H2Cons, MN_H2Cons, HN_H2Cons, BRS_H2Cons)
SELECT SubmissionID, UnitID, ProcessID,
ReactorPress = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'ReactorPressure'),
LN_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'LN_H2Cons'),
MN_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'MN_H2Cons'),
HN_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'HN_H2Cons'),
BRS_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'BRS_H2Cons')
FROM Config
WHERE ProcessID = 'LHYFT'
AND SubmissionID = @SubmissionID

INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, 
LN_H2Cons, LN_SolventRatio, LN_FeedCap, LN_OperPcnt, 
MN_H2Cons, MN_SolventRatio, MN_FeedCap, MN_OperPcnt, 
HN_H2Cons, HN_SolventRatio, HN_FeedCap, HN_OperPcnt, 
BRS_H2Cons, BRS_SolventRatio, BRS_FeedCap, BRS_OperPcnt)
SELECT SubmissionID, UnitID, ProcessID,
LN_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'LN_H2Cons'),
LN_SolventRatio = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'LN_SolventRatio'),
LN_FeedCap = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'LN_FeedCap'),
LN_OperPcnt = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'LN_OperPcnt'),
MN_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'MN_H2Cons'),
MN_SolventRatio = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'MN_SolventRatio'),
MN_FeedCap = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'MN_FeedCap'),
MN_OperPcnt = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'MN_OperPcnt'),
HN_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'HN_H2Cons'),
HN_SolventRatio = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'HN_SolventRatio'),
HN_FeedCap = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'HN_FeedCap'),
HN_OperPcnt = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'HN_OperPcnt'),
BRS_H2Cons = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'BRS_H2Cons'),
BRS_SolventRatio = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'BRS_SolventRatio'),
BRS_FeedCap = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'BRS_FeedCap'),
BRS_OperPcnt = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = Config.SubmissionID AND ProcessData.UnitID = Config.UnitID AND ProcessData.Property = 'BRS_OperPcnt')
FROM Config
WHERE ProcessID IN ('SDWAX', 'SOLVEX')
AND SubmissionID = @SubmissionID

UPDATE UnitFactorData
SET 	EIILNFraction  = ISNULL(LN_OperPcnt*LN_FeedCap,0)/(ISNULL(LN_OperPcnt*LN_FeedCap,0)+ISNULL(MN_OperPcnt*MN_FeedCap,0)+ISNULL(HN_OperPcnt*HN_FeedCap,0)+ISNULL(BRS_OperPcnt*BRS_FeedCap,0)), 
	EIIMNFraction  = ISNULL(MN_OperPcnt*MN_FeedCap,0)/(ISNULL(LN_OperPcnt*LN_FeedCap,0)+ISNULL(MN_OperPcnt*MN_FeedCap,0)+ISNULL(HN_OperPcnt*HN_FeedCap,0)+ISNULL(BRS_OperPcnt*BRS_FeedCap,0)), 
	EIIHNFraction  = ISNULL(HN_OperPcnt*HN_FeedCap,0)/(ISNULL(LN_OperPcnt*LN_FeedCap,0)+ISNULL(MN_OperPcnt*MN_FeedCap,0)+ISNULL(HN_OperPcnt*HN_FeedCap,0)+ISNULL(BRS_OperPcnt*BRS_FeedCap,0)), 
	EIIBRSFraction = ISNULL(BRS_OperPcnt*BRS_FeedCap,0)/(ISNULL(LN_OperPcnt*LN_FeedCap,0)+ISNULL(MN_OperPcnt*MN_FeedCap,0)+ISNULL(HN_OperPcnt*HN_FeedCap,0)+ISNULL(BRS_OperPcnt*BRS_FeedCap,0))
WHERE ProcessID IN ('SDWAX', 'SOLVEX')
AND (ISNULL(LN_OperPcnt*LN_FeedCap,0)+ISNULL(MN_OperPcnt*MN_FeedCap,0)+ISNULL(HN_OperPcnt*HN_FeedCap,0)+ISNULL(BRS_OperPcnt*BRS_FeedCap,0))>0
AND SubmissionID = @SubmissionID
IF EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'WDOIL')
BEGIN
	DECLARE @RefWax real, @MicroWax real, @TotalWax real
	SELECT 	@RefWax = SUM(CASE WHEN MaterialID IN ('W135','W160') THEN Bbl ELSE 0 END),
		@MicroWax = SUM(CASE WHEN MaterialID IN ('W180','WG180') THEN Bbl ELSE 0 END)
	FROM Yield
	WHERE SubmissionID = @SubmissionID AND MaterialID IN ('W135', 'W160', 'W180', 'WG180')
	
	IF @RefWax IS NULL 
		SELECT @RefWax = 0
	IF @MicroWax IS NULL 
		SELECT @MicroWax = 0
	SELECT @TotalWax = @RefWax + @MicroWax
	INSERT INTO UnitFactorData (SubmissionID, UnitID, ProcessID, EIIRefWaxPcnt, EIIMicroWaxPcnt)
	SELECT SubmissionID, UnitID, ProcessID, 
	CASE WHEN @TotalWax > 0 THEN @RefWax/@TotalWax*100 ELSE 0 END, 
	CASE WHEN @TotalWax > 0 THEN @MicroWax/@TotalWax*100 ELSE 0 END
	FROM Config
	WHERE ProcessID = 'WDOIL' AND SubmissionID = @SubmissionID
END

UPDATE UnitFactorData
SET FeedGravity = (SELECT AvgGravity FROM CrudeTot WHERE CrudeTot.SubmissionID = UnitFactorData.SubmissionID)
		* CASE ProcessID WHEN 'VAC' THEN 0.5 ELSE 1 END
WHERE ProcessID IN ('CDU', 'VAC', 'RERUN') AND FeedGravity IS NULL
AND SubmissionID = @SubmissionID

UPDATE ufd SET
Conv = ISNULL(ufd.Conv, def.Conv),
ReactorPress = ISNULL(ufd.ReactorPress, def.ReactorPress),
Coke = ISNULL(ufd.Coke, def.Coke),
FeedGravity = ISNULL(ufd.FeedGravity, def.FeedGravity),
FeedConcarbon = ISNULL(ufd.FeedConcarbon, def.FeedConcarbon),
FeedCrack = ISNULL(ufd.FeedCrack, def.FeedCrack),
FeedAniline = ISNULL(ufd.FeedAniline, def.FeedAniline),
FeedASTM10 = ISNULL(ufd.FeedASTM10, def.FeedASTM10),
FeedASTM90 = ISNULL(ufd.FeedASTM90, def.FeedASTM90),
FeedH2 = ISNULL(ufd.FeedH2, def.FeedH2),
FeedC3Olefin = CASE WHEN ufd.FeedTotalOlefin = 0 THEN def.FeedC3Olefin ELSE ufd.FeedC3Olefin END,
FeedC4Olefin = CASE WHEN ufd.FeedTotalOlefin = 0 THEN def.FeedC4Olefin ELSE ufd.FeedC4Olefin END,
FeedC5Olefin = CASE WHEN ufd.FeedTotalOlefin = 0 THEN def.FeedC5Olefin ELSE ufd.FeedC5Olefin END,
FeedTotalOlefin = CASE WHEN ufd.FeedTotalOlefin = 0 THEN def.FeedTotalOlefin ELSE ufd.FeedTotalOlefin END,
FeedNap = ISNULL(ufd.FeedNap, def.FeedNap),
FeedArom = ISNULL(ufd.FeedArom, def.FeedArom),
Nap2Arom = ISNULL(ufd.Nap2Arom, def.Nap2Arom),
ProdRONC = ISNULL(ufd.ProdRONC, def.ProdRONC),
--ProdDiesel = ISNULL(ufd.ProdDiesel, def.ProdDiesel),
--ProdHGO = ISNULL(ufd.ProdHGO, def.ProdHGO),
--ProdHvHycCrk = ISNULL(ufd.ProdHvHycCrk, def.ProdHvHycCrk),
--ProdLtHycCrk = ISNULL(ufd.ProdLtHycCrk, def.ProdLtHycCrk),
--ProdBal = ISNULL(ufd.ProdBal, def.ProdBal),
LN_H2Cons = ISNULL(ufd.LN_H2Cons, def.LN_H2Cons),
LN_SolventRatio = ISNULL(ufd.LN_SolventRatio, def.LN_SolventRatio),
LN_FeedCap = ISNULL(ufd.LN_FeedCap, def.LN_FeedCap),
LN_OperPcnt = ISNULL(ufd.LN_OperPcnt, def.LN_OperPcnt),
EIILNFraction = ISNULL(ufd.EIILNFraction, def.EIILNFraction),
MN_H2Cons = ISNULL(ufd.MN_H2Cons, def.MN_H2Cons),
MN_SolventRatio = ISNULL(ufd.MN_SolventRatio, def.MN_SolventRatio),
MN_FeedCap = ISNULL(ufd.MN_FeedCap, def.MN_FeedCap),
MN_OperPcnt = ISNULL(ufd.MN_OperPcnt, def.MN_OperPcnt),
EIIMNFraction = ISNULL(ufd.EIIMNFraction, def.EIIMNFraction),
HN_H2Cons = ISNULL(ufd.HN_H2Cons, def.HN_H2Cons),
HN_SolventRatio = ISNULL(ufd.HN_SolventRatio, def.HN_SolventRatio),
HN_FeedCap = ISNULL(ufd.HN_FeedCap, def.HN_FeedCap),
HN_OperPcnt = ISNULL(ufd.HN_OperPcnt, def.HN_OperPcnt),
EIIHNFraction = ISNULL(ufd.EIIHNFraction, def.EIIHNFraction),
BRS_H2Cons = ISNULL(ufd.BRS_H2Cons, def.BRS_H2Cons),
BRS_SolventRatio = ISNULL(ufd.BRS_SolventRatio, def.BRS_SolventRatio),
BRS_FeedCap = ISNULL(ufd.BRS_FeedCap, def.BRS_FeedCap),
BRS_OperPcnt = ISNULL(ufd.BRS_OperPcnt, def.BRS_OperPcnt),
EIIBRSFraction = ISNULL(ufd.EIIBRSFraction, def.EIIBRSFraction),
EIIRefWaxPcnt = ISNULL(ufd.EIIRefWaxPcnt, def.EIIRefWaxPcnt),
EIIMicroWaxPcnt = ISNULL(ufd.EIIMicroWaxPcnt, def.EIIMicroWaxPcnt),
FeedDensity = ISNULL(ufd.FeedDensity, def.FeedDensity),
BtmProdPcnt = ISNULL(ufd.BtmProdPcnt, def.BtmProdPcnt),
FZTemp = ISNULL(ufd.FZTemp, def.FZTemp),
FZPress = ISNULL(ufd.FZPress, def.FZPress),
EIIVGODesulf = ISNULL(ufd.EIIVGODesulf, def.EIIVGODesulf),
EIIDieselDesulf = ISNULL(ufd.EIIDieselDesulf, def.EIIDieselDesulf),
EIIVGOtoLt = ISNULL(ufd.EIIVGOtoLt, def.EIIVGOtoLt),
EIIDieselToLt = ISNULL(ufd.EIIDieselToLt, def.EIIDieselToLt),
H2Cons = ISNULL(ufd.H2Cons, def.H2Cons),
SolventRatio = ISNULL(ufd.SolventRatio, def.SolventRatio),
SumTempDiff = ISNULL(ufd.SumTempDiff, def.SumTempDiff),
Yield = ISNULL(ufd.Yield, def.Yield),
TotalFeed = ISNULL(ufd.TotalFeed, def.TotalFeed),
VGOFeed = ISNULL(ufd.VGOFeed, def.VGOFeed)
FROM UnitFactorData ufd INNER JOIN SubmissionsAll s ON s.SubmissionID = ufd.SubmissionID
INNER JOIN DefUnitFactorData def ON def.RefineryID = s.RefineryID AND def.UnitID = ufd.UnitID
WHERE ufd.SubmissionID = @SubmissionID

UPDATE UnitFactorData
SET FeedDensity = dbo.APItoKGM3(FeedGravity)
WHERE FeedGravity IS NOT NULL AND SubmissionID = @SubmissionID

