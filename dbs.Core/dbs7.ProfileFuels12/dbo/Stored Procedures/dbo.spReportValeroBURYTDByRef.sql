﻿CREATE    PROC [dbo].[spReportValeroBURYTDByRef] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1,
	@KEdc_Ytd real = NULL OUTPUT, @MechAvail_Ytd real = NULL OUTPUT, @AdjMaintIndex_Ytd real = NULL OUTPUT, 
	@UtilPcnt_Ytd real = NULL OUTPUT, @NEOpExEdc_Ytd real = NULL OUTPUT, @EII_Ytd real = NULL OUTPUT, 
	@TotWhrEdc_Ytd real = NULL OUTPUT, @NetInputKBPD_Ytd real = NULL OUTPUT, @CrudeKBPD_Ytd real = NULL OUTPUT, @FCCRateKBPD_Ytd real = NULL OUTPUT,  
	@NumContPers_Ytd real = NULL OUTPUT, @RoutMechUnavail_Ytd real = NULL OUTPUT, @TAMechUnavail_Ytd real = NULL OUTPUT)

AS
SET NOCOUNT ON
DECLARE @SubmissionID int
SELECT @SubmissionID = SubmissionID
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, 'Actual')

SELECT	@KEdc_Ytd = NULL, @MechAvail_Ytd = NULL, @AdjMaintIndex_Ytd = NULL, 
	@UtilPcnt_Ytd = NULL, @NEOpExEdc_Ytd = NULL, @EII_Ytd = NULL, 
	@TotWhrEdc_Ytd = NULL, @NetInputKBPD_Ytd = NULL, @CrudeKBPD_Ytd = NULL, @FCCRateKBPD_Ytd = NULL,  
	@NumContPers_Ytd = NULL, @RoutMechUnavail_Ytd = NULL, @TAMechUnavail_Ytd = NULL
IF @SubmissionID IS NULL
	RETURN 0

CREATE TABLE #Subs
(	SubmissionID int, 
	NumDays real,
	FCCUtilCap real NULL,
	CDUUtilCap real NULL
)
INSERT #Subs 
SELECT SubmissionID, NumDays,
	FCCUtilCap = (SELECT SUM(UtilCap) FROM Config WHERE SubmissionID = Submissions.SubmissionID AND ProcessID = 'FCC'),
	CDUUtilCap = (SELECT SUM(UtilCap) FROM Config WHERE SubmissionID = Submissions.SubmissionID AND ProcessID = 'CDU')
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth <= @PeriodMonth AND DataSet = 'Actual' AND UseSubmission = 1

SELECT @KEdc_Ytd = Edc_Ytd/1000, @MechAvail_Ytd = MechAvail_Ytd, @AdjMaintIndex_Ytd = MaintIndex_Ytd,
	@UtilPcnt_Ytd = UtilPcnt_Ytd, @NEOpExEdc_Ytd = NEOpExEdc_Ytd, @EII_Ytd = EII_Ytd, @TotWhrEdc_Ytd = TotWhrEdc_Ytd,
	@NetInputKBPD_Ytd = NetInputBPD_Ytd, @CrudeKBPD_Ytd = NULL, @FCCRateKBPD_Ytd = NULL, 
	@RoutMechUnavail_Ytd = NULL, @TAMechUnavail_Ytd = NULL
FROM GenSum 
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @NumContPers_Ytd = SUM(Contract)/(SELECT SUM(NumDays) FROM #Subs)*365.0/2080.0
FROM Pers p 
WHERE SubmissionID IN (SELECT SubmissionID FROM #Subs) 
AND p.PersId NOT IN ('OCCTAADJ','MPSTAADJ') AND p.SectionID IN ('OO','OM','OA','MO','MM','MT','MA','MG')

SELECT @RoutMechUnavail_Ytd = 100-MechAvail_Ann_Ytd - MechUnavailTA_Ann, @TAMechUnavail_Ytd = MechUnavailTA_Ann 
FROM MaintAvailCalc 
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet
IF @RoutMechUnavail_Ytd < 0
	SET @RoutMechUnavail_Ytd = 0

SELECT @FCCRateKBPD_Ytd = SUM(FCCUtilCap*NumDays)/SUM(NumDays)/1000,
	@CrudeKBPD_Ytd = SUM(CDUUtilCap*NumDays)/SUM(NumDays)/1000
FROM #Subs

