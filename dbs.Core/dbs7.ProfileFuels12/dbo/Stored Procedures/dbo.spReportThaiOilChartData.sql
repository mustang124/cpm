﻿CREATE   PROC [dbo].[spReportThaiOilChartData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	ProcessUtilPcnt real NULL, 
	OpAvail real NULL, 
	EII real NULL, 
	VEI real NULL, 
	GainPcnt real NULL, 
	PersIndex real NULL, 
	TotMaintForceWHrEdc real NULL, 
	MaintIndex real NULL, 
	NEOpExEdc real NULL,
	OpExEdc real NULL,
	OpExUEdc real NULL,
	ProcessUtilPcnt_3Mo real NULL, 
	OpAvail_3Mo real NULL, 
	EII_3Mo real NULL, 
	VEI_3Mo real NULL, 
	GainPcnt_3Mo real NULL, 
	PersIndex_3Mo real NULL, 
	TotMaintForceWHrEdc_3Mo real NULL, 
	MaintIndex_3Mo real NULL, 
	NEOpExEdc_3Mo real NULL,
	OpExEdc_3Mo real NULL,
	OpExUEdc_3Mo real NULL,
	ProcessUtilPcnt_12Mo real NULL, 
	OpAvail_24Mo real NULL, 
	EII_12Mo real NULL, 
	VEI_12Mo real NULL, 
	GainPcnt_12Mo real NULL, 
	PersIndex_12Mo real NULL, 
	TotMaintForceWHrEdc_12Mo real NULL, 
	MaintIndex_24Mo real NULL, 
	NEOpExEdc_12Mo real NULL,
	OpExEdc_12Mo real NULL,
	OpExUEdc_12Mo real NULL	
)

--- Everything Already Available in GenSum (missing gain and OpEx on an Edc basis)
INSERT INTO @Data (PeriodStart, PeriodEnd, ProcessUtilPcnt, OpAvail, EII, VEI, GainPcnt, PersIndex, TotMaintForceWHrEdc, MaintIndex, NEOpExEdc, OpExUEdc, OpExEdc,
	ProcessUtilPcnt_12Mo, OpAvail_24Mo, EII_12Mo, VEI_12Mo, GainPcnt_12Mo, PersIndex_12Mo, TotMaintForceWHrEdc_12Mo, MaintIndex_24Mo, NEOpExEdc_12Mo, OpExUEdc_12Mo)
SELECT	s.PeriodStart, s.PeriodEnd, ProcessUtilPcnt, OpAvail, EII, VEI, -GainPcnt, PersIndex = TotWHrEdc, TotMaintForceWHrEdc, MaintIndex = RoutIndex + TAIndex_Avg, NEOpExEdc, TotCashOpExUEdc
	, OpExEdc = (SELECT TotCashOpEx FROM OpExCalc o WHERE o.SubmissionID = g.SubmissionID AND o.Currency = g.Currency AND o.Scenario = 'CLIENT' AND o.FactorSet = g.FactorSet AND o.DataType = 'Edc')
	, ProcessUtilPcnt_Avg, OpAvail_Avg, EII_Avg, VEI_Avg, -GainPcnt_Avg, TotWhrEdc_Avg, TotMaintForceWHrEdc_Avg, MaintIndex_Avg, NEOpExEdc_Avg, TotCashOpExUedc_Avg
FROM GenSum g INNER JOIN Submissions s ON s.SubmissionID = g.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @PeriodStart12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND g.FactorSet = @FactorSet AND g.Currency = 'USD' AND g.UOM = @UOM AND g.Scenario = @Scenario


DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @Start24Mo smalldatetime
DECLARE @ProcessUtilPcnt_3Mo real, @OpAvail_3Mo real, @EII_3Mo real, @VEI_3Mo real, @GainPcnt_3Mo real, @PersIndex_3Mo real, @TotMaintForceWHrEdc_3Mo real, @MaintIndex_3Mo real, @NEOpExEdc_3Mo real, @OpExUEdc_3Mo real, @OpExEdc_3Mo real, @OpExEdc_12Mo real

DECLARE cMonths CURSOR LOCAL FAST_FORWARD
FOR SELECT PeriodStart, PeriodEnd FROM @Data
OPEN cMonths 
FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @Start24Mo = DATEADD(mm, -24, @PeriodEnd),
			@ProcessUtilPcnt_3Mo = NULL, @OpAvail_3Mo = NULL, @EII_3Mo = NULL, @VEI_3Mo = NULL, @GainPcnt_3Mo = NULL, @PersIndex_3Mo = NULL, 
			@TotMaintForceWHrEdc_3Mo = NULL, @MaintIndex_3Mo = NULL, @NEOpExEdc_3Mo = NULL, @OpExUEdc_3Mo = NULL, @OpExEdc_3Mo = NULL, @OpExEdc_12Mo = NULL
			
	EXEC spAverageFactors @RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, 
		@EII = @EII_3Mo OUTPUT, @VEI = @VEI_3Mo OUTPUT, @UtilPcnt = NULL, @UtilOSTA = NULL, @Edc = NULL, @UEdc = NULL, 
		@ProcessUtilPcnt = @ProcessUtilPcnt_3Mo OUTPUT, @TotProcessEdc = NULL, @TotProcessUEdc = NULL

	SELECT @GainPcnt_3Mo = -SUM(GainBbl)/SUM(NetInputBbl)*100
	FROM MaterialTot m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND m.NetInputBbl > 0

	SELECT @TotMaintForceWHrEdc_3Mo = SUM(p.TotMaintForceWHrEdc*p.WHrEdcDivisor)/SUM(p.WHrEdcDivisor)
	, @PersIndex_3Mo = SUM(g.TotWHrEdc*p.WHrEdcDivisor)/SUM(p.WHrEdcDivisor)
	, @MaintIndex_3Mo = SUM((g.RoutIndex + g.TAIndex_Avg)*g.Edc)/SUM(g.Edc)
	FROM PersTotCalc p INNER JOIN GenSum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
	INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 
	AND p.FactorSet = @FactorSet AND g.UOM = @UOM AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

	SELECT @OpExUEdc_3Mo = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
	FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
	AND o.DataType = 'UEdc' AND o.Scenario = 'CLIENT' AND o.FactorSet = @FactorSet AND o.Currency = @Currency

	SELECT @OpExEdc_3Mo = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
	, @NEOpExEdc_3Mo = SUM(NEOpEx*Divisor)/SUM(Divisor)
	FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
	AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'Edc'

	SELECT @OpExEdc_12Mo = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
	FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
	AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'Edc'

	SELECT @OpAvail_3Mo=SUM(OpAvail_Ann*f.TotProcessEdc*s.FractionOfYear)/SUM(f.TotProcessEdc*s.FractionOfYear)
	FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
	INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
	AND m.FactorSet = @FactorSet

	UPDATE @Data
	SET ProcessUtilPcnt_3Mo = @ProcessUtilPcnt_3Mo, 
		OpAvail_3Mo = @OpAvail_3Mo, 
		EII_3Mo = @EII_3Mo, 
		VEI_3Mo = @VEI_3Mo, 
		GainPcnt_3Mo = @GainPcnt_3Mo, 
		PersIndex_3Mo = @PersIndex_3Mo, 
		TotMaintForceWHrEdc_3Mo = @TotMaintForceWHrEdc_3Mo, 
		MaintIndex_3Mo = @MaintIndex_3Mo, 
		NEOpExEdc_3Mo = @NEOpExEdc_3Mo,
		OpExUEdc_3Mo = @OpExUEdc_3Mo,
		OpExEdc_3Mo = @OpExEdc_3Mo,
		OpExEdc_12Mo = @OpExEdc_12Mo	
	WHERE PeriodStart = @PeriodStart

	FETCH NEXT FROM cMonths INTO @PeriodStart, @PeriodEnd
END
CLOSE cMonths
DEALLOCATE cMonths

IF (SELECT COUNT(*) FROM @Data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @Data WHERE PeriodStart = @Period)
			INSERT @Data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, ProcessUtilPcnt, OpAvail, EII, VEI, /*GainPcnt,*/ OpExUEdc, PersIndex, TotMaintForceWHrEdc, MaintIndex, NEOpExEdc, OpExEdc
	, ProcessUtilPcnt_3Mo, OpAvail_3Mo, EII_3Mo, VEI_3Mo, /*GainPcnt_3Mo,*/ OpExUEdc_3Mo, PersIndex_3Mo, TotMaintForceWHrEdc_3Mo, MaintIndex_3Mo, NEOpExEdc_3Mo, OpExEdc_3Mo
	, ProcessUtilPcnt_12Mo, OpAvail_24Mo, EII_12Mo, VEI_12Mo, /*GainPcnt_12Mo,*/ OpExUEdc_12Mo, PersIndex_12Mo, TotMaintForceWHrEdc_12Mo, MaintIndex_24Mo, NEOpExEdc_12Mo, OpExEdc_12Mo
FROM @Data
ORDER BY PeriodStart ASC

