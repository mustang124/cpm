﻿CREATE RULE [dbo].[chkYorN]
    AS @Value IN ('Y', 'N',NULL) OR @Value IS NULL;


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FiredHeaters].[SteamSuperHeated]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Crude_LU].[Display]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Gasoline].[VOCControlled]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Emissions].[IndirectIncluded]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FiredHeaterData].[SteamSuperHeated]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[Hidden]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Absence_LU].[IncludeForInput]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[IsProcessUnit]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[SulfurAdj]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Emissions].[GovtRpt]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[InMajorMechAvailOSTA]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[InUtilOSTA]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[InUPC]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[MaintDetails]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ProcessID_LU].[ProfileProcFacility]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FactorSets].[CalcFCCCOBoiler]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FactorSets].[CurrentFactors]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FactorSets].[CalcPurElecStmUEdc]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FactorSets].[CalcCEI]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FactorSets].[IdleUnitsInProcessResults]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FactorSets].[Calculate]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Material_LU].[IncludeForMarginCalc]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[FactorSets].[CrudeInSensHeat]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[YorN]';

