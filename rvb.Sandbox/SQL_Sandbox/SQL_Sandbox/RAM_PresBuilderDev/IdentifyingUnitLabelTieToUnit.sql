﻿
--SELECT * FROM UnitLabels('INEOS15')



SELECT DISTINCT
		(
		SELECT FacilityName
		FROM Dataset_LU
		WHERE DatasetID = dslu.ParentID
		) AS SiteName
	,'<FMT>' + CHAR(149) + '                 ' + dslu.PresentationLabel + ' - ' +  dslu.FacilityName + '</FMT>' AS Label
	--,'<FMT>' + CHAR(149) + '                 ' + ' - ' +  dslu.FacilityName + '</FMT>' AS Label
FROM
OUTPUT.UnitSummary us
--INNER JOIN OUTPUT.GroupUnits gu ON us.SubGroupID = gu.SubGroupID
INNER JOIN OUTPUT.GroupUnits gu ON us.GroupID = gu.GroupID
INNER JOIN Dataset_LU dslu ON gu.DataSetID = dslu.DatasetID
WHERE gu.DatasetID IN (
		SELECT CAST(datasetid AS VARCHAR(25))
		FROM Dataset_LU
		WHERE Deleted = 0
			AND ParentID IN (
				SELECT DatasetID
				FROM Dataset_LU
				WHERE ParentID IN (
						SELECT DatasetID
						FROM Dataset_LU
						WHERE FacilitySID = 'INEOS15'
						)
				)
		)