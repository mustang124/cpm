﻿



DECLARE @term VARCHAR(50) = '%Family%'

SELECT TOP 1000 [PropertyName]
      ,[PropertyNo]
      ,[FriendlyName]
      ,[DataLevel]
      ,[DataType]
      ,[DecimalPlaces]
      ,[USUOM]
      ,[MetricUOM]
      ,[InputTable]
      ,[ValueField]
      ,[KeyField1]
      ,[KeyValue1]
      ,[KeyField2]
      ,[KeyValue2]
      ,[QuestionChoicesID]
      ,[CalcMethod]
      ,[HelpID]
      ,[Prepopulate]
      ,[RolloverEscalator]
  FROM [RAM].[dbo].[Properties]
  WHERE FriendlyName like @term
  OR PropertyName like @term
  Order By PropertyNo




--  Select *
----Select Distinct strValue
--From Answers
--WHERE DatasetID IN
--(
--	7290
--,7291
--,7292
--,7293
--,7294
--,7295
--,7296
--,7297
--,7298
--,7299
--,7300
--,7369
--,7370
--,7371
--,7372
--,7373
--,7374
--,7375
--,7376
--,7377
--,7378
--,7302
--,7380
--,7381
--,7382
--,7383
--,7384
--,7385
--,7386
--,7387
--,7388
--,7389
--,7303
--,7304
--,7305
--,7306
--,7307
--,7308
--,7309
--,7310
--,7311
--,7312
--)
--AND PropertyNo IN
--(
--	325
--)
--Order By PropertyNo, DatasetID