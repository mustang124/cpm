﻿




-- Ineos Sites BAT and CHO
Select *
From Answers
Where DatasetID in
(
7389
,7303
)




SELECT 
	   DISTINCT
	   [GroupID]
      ,[SubgroupID]
      --,[DatasetID]
  FROM [RAM].[Output].[GroupUnits]
  --WHERE GroupID like 'RAM15%'
  WHERE 
  (
  GroupID like 'RAM15%'
  AND SubGroupID in
  (
	'C_PLAST'
	,'OthChem'
	,'PM_OLE'
	,'Utilities'
	--,'ALL'
  ))
  OR
  (
		GroupID like 'RAM15%C_PLAST'
	  OR GroupID like 'RAM15%OthChem'
	  OR GroupID like 'RAM15%PM_OLE'
	  OR GroupID like 'RAM15%Utilities'
	  OR GroupID like 'RAM15%ALL'
  )