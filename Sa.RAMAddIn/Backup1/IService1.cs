﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string GetData(string value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        string GetSiteData(string xmlstring);

        [OperationContract]
        //string GetXMLData(string xmlstring, int loginid);
        string GetXMLData(byte[] xmlstring, int loginid, byte[] salt, byte[] key, byte[] vector, double dsid);
        
        [OperationContract]
        Handshake InitiateHandshake();

        [OperationContract]
        Handshake CompleteHandshake(Handshake handshake);

        [OperationContract]
        DataSet PullData(double dsid);

        [OperationContract]
        byte[] pushAnswers(int dsid, byte[] salt, byte[] key, byte[] vector);

        [OperationContract]
        byte[] pushAnswersPrePop(int dsid, byte[] salt, byte[] key, byte[] vector);

        [OperationContract]
        byte[] pushAnswerswithhistory(int dsid, byte[] salt, byte[] key, byte[] vector);

        [OperationContract]
        int addUnit(int siteID, string unitName, string processFamily, string genProductName, string primaryProductName, int userNo, string userID);

        [OperationContract]
        int addSite(int companyDatasetID, string siteName, string siteLocation, string currency, float exchangeRate, int userNo, string userID);

        [OperationContract]
        DataSet getSiteUnits(int dsid);

        // TODO: Add your service operations here
    }

    [DataContract]
    public class Handshake
    {
        byte[] value1;
        byte[] value2;
        bool status;
        int? SID;
        int LID;
        byte[] value3;
        byte[] value4;

        [DataMember]
        public byte[] Value1
        {
            get { return value1; }
            set { value1 = value; }
        }

        [DataMember]
        public byte[] Value2
        {
            get { return value2; }
            set { value2 = value; }
        }

        [DataMember]
        public bool Status
        {
            get { return status; }
            set { status = value; }
        }

        [DataMember]
        public int? sid
        {
            get { return SID; }
            set { SID = value; }
        }

        [DataMember]
        public int lid
        {
            get { return LID; }
            set { LID = value; }
        }

        [DataMember]
        public byte[] Value3
        {
            get { return value3; }
            set { value3 = value; }
        }

        [DataMember]
        public byte[] Value4
        {
            get { return value4; }
            set { value4 = value; }
        }
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]    
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
