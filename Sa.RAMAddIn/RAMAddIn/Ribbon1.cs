﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.IO;
using MyExcelService = RAMAddIn.RAMService;
using Excel = Microsoft.Office.Interop.Excel;
using System.Xml;
using System.Data;
using System.ComponentModel;

namespace RAMAddIn
{
    public partial class Ribbon1
    {
        TextBox txtLoginUsername = new TextBox();
        TextBox txtLoginPassword = new TextBox();
        Form frmLoginWindow = new Form();
        delegate void setFormCallback();
        string username = string.Empty;
        string password = string.Empty;
        Form frmProgressWindow = new Form();
        Form frmUnitSelection = new Form();
        int?[] sites = new int?[900];
        int?[] units = new int?[900];
        private byte[] Key = { 248, 215, 80, 175, 235, 183, 19, 200, 195, 97, 212, 123, 110, 0, 192, 83, 148, 33, 49, 121, 147, 240, 138, 188, 113, 177, 209, 112, 145, 219, 53, 78 };
        private byte[] Vector = { 86, 102, 197, 82, 75, 101, 134, 69, 250, 94, 79, 187, 146, 83, 18, 128 };
        [DllImport("wininet.dll", SetLastError = true)]
        public static extern bool InternetGetConnectedState(out int flags, int reserved);
        [DllImport("wininet.dll", SetLastError = true)]
        public static extern int InternetAttemptConnect(uint res);
        private static int ERROR_SUCCESS = 0;
        int DataSetsToSend = 0;
        int SendCount = 0;
        Crypt cryptor = new Crypt();
        System.Data.DataSet dsSiteUnits = new System.Data.DataSet();

        #region Ribbon1 Load Event
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {
            frmProgressWindow.Width = 250;
            frmProgressWindow.Height = 80;
            System.Drawing.Size sz = new System.Drawing.Size(250, 80);
            frmProgressWindow.MaximumSize = sz;
            frmProgressWindow.MinimumSize = sz;
            ProgressBar pb = new ProgressBar();
            pb.Style = ProgressBarStyle.Marquee;
            pb.Maximum = 100;
            pb.Minimum = 0;
            pb.Value = 50;
            pb.Width = 200;
            pb.Height = 10;
            pb.Left = (frmProgressWindow.Width - pb.Width) / 2 - 8;
            pb.Top = (frmProgressWindow.Height - pb.Height) / 2 + 10;
            frmProgressWindow.Controls.Add(pb);
            Label lblStatus = new Label();
            lblStatus.Width = 181;
            lblStatus.Text = "Please wait while data is transferred";
            lblStatus.Left = (frmProgressWindow.Width - lblStatus.Width) / 2 - 7;
            lblStatus.Top = (frmProgressWindow.Height - lblStatus.Height) / 2 - 10;
            frmProgressWindow.Controls.Add(lblStatus);
            frmProgressWindow.Text = string.Empty;
            frmProgressWindow.ControlBox = false;
            frmProgressWindow.FormBorderStyle = FormBorderStyle.Sizable;
            frmProgressWindow.StartPosition = FormStartPosition.CenterScreen;
        }
        #endregion
            
        private void btnRibbonSubmit_Click(object sender, RibbonControlEventArgs e)
        {
            DataSetsToSend = 0;
            Array.Clear(sites, 0, sites.Length);

            if (!IsInternetConnected())
            {
                MessageBox.Show("No internet connection detected. Please make sure that your computer is connected to the internet and try again.", "Connection Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string websiteAvailable = IsWebsiteAvailable();
            if (websiteAvailable != "")
            {
                MessageBox.Show("Could not reach the RAM Service. Please make sure that your computer is connected to the internet and can reach secure websites (SSL/Port 443). Contact your Help Desk or System Adminitrator for help. The message returned follows: " + System.Environment.NewLine + websiteAvailable, "Connection Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CreateUnitSelectionForm();
        }

        private void btnPullData_Click(object sender, RibbonControlEventArgs e)
        {
            //string abc = string.Empty;
            //Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[2];
            //Excel.Range rng = activeSheet.Range["M5", "M355"];
            
            //foreach (Excel.Range cell in rng.Cells)
            //{
            //    abc = ((Excel.Name)cell.Name).Name;

            //    foreach (Excel.Name name in activeSheet.Names)
            //    {
            //        abc = name.Name;
            //    }
            //}


            DataSetsToSend = 0;
            Array.Clear(sites, 0, sites.Length);

            if (!IsInternetConnected())
            {
                MessageBox.Show("No internet connection detected. Please make sure that your computer is connected to the internet and try again.", "Connection Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string websiteAvailable = IsWebsiteAvailable();
            if (websiteAvailable != "")
            {
                MessageBox.Show("Could not reach the RAM Service. Please make sure that your computer is connected to the internet and can reach secure websites (SSL/Port 443). Contact your Help Desk or System Adminitrator for help. The message returned follows: " + System.Environment.NewLine + websiteAvailable, "Connection Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CreatePullDataSelectionForm();
        }

        private void btnPullPrePop_Click(object sender, RibbonControlEventArgs e)
        {
            DataSetsToSend = 0;
            Array.Clear(sites, 0, sites.Length);

            if (!IsInternetConnected())
            {
                MessageBox.Show("No internet connection detected. Please make sure that your computer is connected to the internet and try again.", "Connection Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            string websiteAvailable = IsWebsiteAvailable();
            if (websiteAvailable != "")
            {
                MessageBox.Show("Could not reach the RAM Service. Please make sure that your computer is connected to the internet and can reach secure websites (SSL/Port 443). Contact your Help Desk or System Adminitrator for help. The message returned follows: " + System.Environment.NewLine + websiteAvailable, "Connection Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            CreatePullPrePopDataSelectionForm();
        }

        #region Check for Internet Connection
        private bool IsInternetConnected()
        {
            int dwConnectionFlags = 0;
            if (!InternetGetConnectedState(out dwConnectionFlags, 0))
                return false;

            if (InternetAttemptConnect(0) != ERROR_SUCCESS)
                return false;

            return true;
        }
        #endregion

        public string IsWebsiteAvailable()
        {
            System.Net.WebRequest request = System.Net.WebRequest.Create("https://webservices.solomononline.com/RAMService/Service1.svc");
            try
            {
                using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    if (response == null || response.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        return response.StatusDescription;
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return string.Empty;
        }

        #region Encrypt Byte
        public byte[] Encrypt(string TextValue, Byte[] salt, byte[] key, byte[] vector)
        {
            RijndaelManaged rm = new RijndaelManaged();

            ICryptoTransform EncryptorTransform = rm.CreateEncryptor(key, vector);
            ICryptoTransform DecryptorTransform = rm.CreateDecryptor(key, vector);

            //Used to translate bytes to text and vice versa
            UTF8Encoding UTFEncoder = new System.Text.UTF8Encoding();

            //Translates our text value into a byte array.
            Byte[] pepper = UTFEncoder.GetBytes(TextValue);

            int Salt = salt.Length;
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(salt);
            Byte[] bytes = new byte[2 * Salt + pepper.Length];
            System.Buffer.BlockCopy(salt, 0, bytes, 0, Salt);
            System.Buffer.BlockCopy(pepper, 0, bytes, Salt, pepper.Length);
            using (crypto)
            {
                crypto.GetNonZeroBytes(salt);
            }
            System.Buffer.BlockCopy(salt, 0, bytes, Salt + pepper.Length, Salt);

            //Used to stream the data in and out of the CryptoStream.
            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cs = new CryptoStream(memoryStream, EncryptorTransform, CryptoStreamMode.Write);
            cs.Write(bytes, 0, bytes.Length);
            cs.FlushFinalBlock();

            memoryStream.Position = 0;
            byte[] encrypted = new byte[memoryStream.Length];
            memoryStream.Read(encrypted, 0, encrypted.Length);

            cs.Close();
            memoryStream.Close();
            rm.Dispose();

            return encrypted;
        }
        #endregion

        #region Create Unit Selection Form
        void CreateUnitSelectionForm()
        {
            frmUnitSelection.Width = 340;
            frmUnitSelection.Height = 380;
            System.Drawing.Size sz = new System.Drawing.Size(340, 380);
            //frmUnitSelection.MaximumSize = sz;
            //frmUnitSelection.MinimumSize = sz;
            frmUnitSelection.VerticalScroll.Visible = false;
            frmUnitSelection.Text = "Unit Selection";
            Label lblTitle = new Label();
            lblTitle.Width = 330;
            System.Drawing.Font fontTitle = new System.Drawing.Font("Arial", 10);
            lblTitle.Font = fontTitle;
            lblTitle.Text = "Please select the site(s)/unit(s) you wish to submit:";
            frmUnitSelection.Font = new System.Drawing.Font("Arial ", 8);
            frmUnitSelection.Controls.Add(lblTitle);
            int n = 0;
            int y = 20;
            int i = Globals.ThisAddIn.Application.Worksheets.Count;
            foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
            {
                if ((worksheet.Range["D7"].Value2 == "Unit" || worksheet.Range["D7"].Value2 == "Site") && worksheet.Visible == Excel.XlSheetVisibility.xlSheetVisible)
                {
                    CheckBox[] _cb = new CheckBox[i];
                    _cb[n] = new CheckBox();
                    _cb[n].Name = worksheet.Range["M9"].Value2;
                    _cb[n].Text = worksheet.Range["M9"].Value2;
                    _cb[n].Top = y;
                    _cb[n].Width = 330;
                    frmUnitSelection.Controls.Add(_cb[n]);
                    y += 20;
                    n += 1;
                }

            }
            Button btnNext = new Button();
            btnNext.Text = "Next";
            btnNext.Top = y + 7;
            btnNext.Click += new EventHandler(btnNext_Click);

            Button btnUnitCancel = new Button();
            btnUnitCancel.Text = "Cancel";
            btnUnitCancel.Top = y + 7;
            btnUnitCancel.Left = btnNext.Left + 75;
            btnUnitCancel.Click += new EventHandler(btnUnitCancel_Click);

            frmUnitSelection.Controls.Add(btnNext);
            frmUnitSelection.Controls.Add(btnUnitCancel);

            frmUnitSelection.StartPosition = FormStartPosition.CenterScreen;
            frmUnitSelection.ShowDialog();
            frmUnitSelection.FormBorderStyle = FormBorderStyle.Sizable;
            
        }
        #endregion

        #region btnNext Click Event
        void btnNext_Click(object sender, EventArgs e)
        {
            int i = 0;
            foreach (Control c in frmUnitSelection.Controls)
            {
                if (c is CheckBox)
                {
                    if (((CheckBox)c).Checked)
                    {
                        foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
                        {
                            if (Convert.ToString(worksheet.Range["M9"].Value2) == c.Text && worksheet.Range["D7"].Value2 == "Site")
                                sites[i] = worksheet.Index;
                            else if (Convert.ToString(worksheet.Range["M9"].Value2) == c.Text)
                                units[i] = worksheet.Index;
                            i += 1;
                        }
                        DataSetsToSend++;
                    }
                }
            }
            frmUnitSelection.Hide();
            PopulateLoginForm();
        }
        #endregion

        #region btnUnitCancel Click Event
        void btnUnitCancel_Click(object sender, EventArgs e)
        {
            frmUnitSelection.Hide();
            foreach (Control c in frmUnitSelection.Controls)
            {
                if (c is CheckBox)
                {
                    ((CheckBox)c).Checked = false;
                }
            }
            DataSetsToSend = 0;
            Array.Clear(sites, 0, sites.Length);
        }
        #endregion

        #region btnSubmit Click Event
        void btnSubmit_Click(object sender, EventArgs e)
        {
            username = ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[2].Text;
            password = ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[3].Text;

            if (username == "" || password == "")
            {
                System.Windows.Forms.MessageBox.Show(frmLoginWindow, "Please enter username and password", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var result = MessageBox.Show(frmLoginWindow, "Submitting new data will cause all previous data to be overwritten. Click OK to continue or Cancel to abort.", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            if (result == DialogResult.OK)
            {
                MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();

                excelSvc.ClientCredentials.UserName.UserName = "sfd";
                excelSvc.ClientCredentials.UserName.Password = "sdfsdf";

                MyExcelService.Handshake handshake = new MyExcelService.Handshake();
                handshake = excelSvc.InitiateHandshake();

                handshake.Value1 = cryptor.Encrypt(username + "|" + password, handshake.Value2, handshake.Value3, handshake.Value4);
                handshake = excelSvc.CompleteHandshake(handshake);
                if (handshake.Status)
                {
                    var selectedSites = sites.Where(item => item != null);
                    foreach (int value in selectedSites)
                    {
                        int siteID = 0;
                        double? dsid = Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2;
                        if (dsid < 1 || dsid == null)
                        {
                            siteID = addSite(Convert.ToInt32(Globals.ThisAddIn.Application.Worksheets[value].Range["B7"].Value2), value);
                            Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2 = siteID;
                        }
                        SendExcelDataToWCFService(cryptor.Encrypt(CreateSiteDataXMLString(value), handshake.Value2, handshake.Value3, handshake.Value4), excelSvc, handshake.lid, handshake.Value2, handshake.Value3, handshake.Value4, Globals.ThisAddIn.Application.Worksheets[2].Range["C7"].Value2);
                    }

                    var selectedUnits = units.Where(item => item != null);
                    foreach (int value in selectedUnits)
                    {
                        int siteID = 0;
                        int unitID = 0;
                        double? dsid = Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2;
                        if (dsid < 1 || dsid == null)
                        {
                            foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
                            {
                                if (worksheet.Range["D7"].Value2 == "Site")
                                {
                                    siteID = Convert.ToInt32(worksheet.Range["C7"].Value2);
                                    break;
                                }
                            }
                            unitID = addUnit(siteID, value);
                            Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2 = unitID;
                        }
                        SendExcelDataToWCFService(cryptor.Encrypt(CreateXMLString(value), handshake.Value2, handshake.Value3, handshake.Value4), excelSvc, handshake.lid, handshake.Value2, handshake.Value3, handshake.Value4, Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2);
                    }
                }
                else
                {
                    MessageBox.Show(frmLoginWindow, "Invalid username/password", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[2].Text = "";
                ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[3].Text = "";
            }
        }
        #endregion

        #region Create Site Data XML String
        private string CreateSiteDataXMLString(int Sheet)
        {
            XmlDocument xmldoc = new XmlDocument();
            using (MemoryStream stream = new MemoryStream())
            {
                Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[Sheet];
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                XmlWriter xmlWriter = XmlWriter.Create(stream, settings);
                using (xmlWriter)
                {
                    xmlWriter.WriteStartDocument();
                    SiteData sd = new SiteData();
                    sd.CompanyName = (activeSheet.Range["M8"].Value2 == null) ? "" : activeSheet.Range["M8"].Value2;
                    sd.SiteName = (activeSheet.Range["M9"].Value2 == null) ? "" : activeSheet.Range["M9"].Value2;
                    sd.SiteCountry = (activeSheet.Range["M10"].Value2 == null) ? "" : activeSheet.Range["M10"].Value2;
                    sd.StreetAddr1 = (activeSheet.Range["M11"].Value2 == null) ? "" : activeSheet.Range["M11"].Value2;
                    sd.MailAddr1 = (activeSheet.Range["M12"].Value2 == null) ? "" : activeSheet.Range["M12"].Value2;
                    sd.SiteMgr = (activeSheet.Range["M13"].Value2 == null) ? "" : activeSheet.Range["M13"].Value2;
                    sd.SiteMgrEmail = (activeSheet.Range["M14"].Value2 == null) ? "" : activeSheet.Range["M14"].Value2;
                    sd.MaintMgr = (activeSheet.Range["M15"].Value2 == null) ? "" : activeSheet.Range["M15"].Value2;
                    sd.MaintMgrEmail = (activeSheet.Range["M16"].Value2 == null) ? "" : activeSheet.Range["M16"].Value2;
                    sd.SiteNumPersInt = (activeSheet.Range["M26"].Value2 == null) ? "" : activeSheet.Range["M26"].Value2;
                    sd.SiteAreaUOM = (activeSheet.Range["M34"].Value2 == null) ? "" : activeSheet.Range["M34"].Value2;
                    sd.SiteAreaTotal = (activeSheet.Range["M35"].Value2 == null) ? "" : activeSheet.Range["M35"].Value2;
                    sd.SiteAreaUnit = (activeSheet.Range["M36"].Value2 == null) ? "" : activeSheet.Range["M36"].Value2;
                    sd.LocalCurrency = (activeSheet.Range["M39"].Value2 == null) ? "" : activeSheet.Range["M39"].Value2;
                    sd.ExchangeRate = (activeSheet.Range["M40"].Value2 == null) ? "" : activeSheet.Range["M40"].Value2;
                    sd.SitePRV = (activeSheet.Range["M43"].Value2 == null) ? "" : activeSheet.Range["M43"].Value2;
                    sd.PRVCalcMethod = (activeSheet.Range["M44"].Value2 == null) ? "" : activeSheet.Range["M44"].Value2;
                    sd.SiteSafetyCurrInjuryCntComp_RT = (activeSheet.Range["M53"].Value2 == null) ? "" : activeSheet.Range["M53"].Value2;
                    sd.SiteSafetyCurrInjuryCntCont_RT = (activeSheet.Range["M54"].Value2 == null) ? "" : activeSheet.Range["M54"].Value2;
                    sd.SiteSafetyCurrInjuryCntTot_RT = (activeSheet.Range["M55"].Value2 == null) ? "" : activeSheet.Range["M55"].Value2;
                    sd.SiteSafetyPreTaskAnalysis = (activeSheet.Range["M58"].Value2 == null) ? "" : activeSheet.Range["M58"].Value2;
                    sd.SiteSafetySafeWorkPermit = (activeSheet.Range["M59"].Value2 == null) ? "" : activeSheet.Range["M59"].Value2;
                    sd.SiteSafetyReturnToOperation = (activeSheet.Range["M60"].Value2 == null) ? "" : activeSheet.Range["M60"].Value2;
                    sd.SiteStrategyLCCA = (activeSheet.Range["M63"].Value2 == null) ? "" : activeSheet.Range["M63"].Value2;
                    sd.SiteStrategyReliModeling = (activeSheet.Range["M64"].Value2 == null) ? "" : activeSheet.Range["M64"].Value2;
                    sd.SiteStrategyPlanDocYrs = (activeSheet.Range["M65"].Value2 == null) ? "" : activeSheet.Range["M65"].Value2;
                    sd.SiteStrategyEquipRelDocPcnt = (activeSheet.Range["M66"].Value2 == null) ? "" : activeSheet.Range["M66"].Value2;
                    sd.SiteStrategyObjectivesDoc = (activeSheet.Range["M67"].Value2 == null) ? "" : activeSheet.Range["M67"].Value2;
                    sd.SiteStrategyMgrObjectives = (activeSheet.Range["M68"].Value2 == null) ? "" : activeSheet.Range["M68"].Value2;
                    sd.SiteStrategyNumKPI = (activeSheet.Range["M69"].Value2 == null) ? "" : activeSheet.Range["M69"].Value2;
                    sd.SiteStrategyNumKPIDaily = (activeSheet.Range["M71"].Value2 == null) ? "" : activeSheet.Range["M71"].Value2;
                    sd.SiteStrategyGoalsRamStaff = (activeSheet.Range["M74"].Value2 == null) ? "" : activeSheet.Range["M74"].Value2;
                    sd.SiteStrategyGoalsRamCraft = (activeSheet.Range["M75"].Value2 == null) ? "" : activeSheet.Range["M75"].Value2;
                    sd.SiteStrategyGoalsProdStaff = (activeSheet.Range["M76"].Value2 == null) ? "" : activeSheet.Range["M76"].Value2;
                    sd.SiteStrategyGoalsProdTechs = (activeSheet.Range["M77"].Value2 == null) ? "" : activeSheet.Range["M77"].Value2;
                    sd.SiteStrategyGoalsEngTech = (activeSheet.Range["M78"].Value2 == null) ? "" : activeSheet.Range["M78"].Value2;
                    sd.SiteStrategyGoalsSuppProc = (activeSheet.Range["M79"].Value2 == null) ? "" : activeSheet.Range["M79"].Value2;
                    sd.SiteStrategyPersonnelCert = (activeSheet.Range["M80"].Value2 == null) ? "" : activeSheet.Range["M80"].Value2;
                    sd.SiteStrategyBadActorList = (activeSheet.Range["M81"].Value2 == null) ? "" : activeSheet.Range["M81"].Value2;
                    sd.SiteStrategyMOC = (activeSheet.Range["M82"].Value2 == null) ? "" : activeSheet.Range["M82"].Value2;
                    sd.SiteExpCompLabor_RT = (activeSheet.Range["M89"].Value2 == null) ? "" : activeSheet.Range["M89"].Value2;
                    sd.SiteExpContLabor_RT = (activeSheet.Range["M90"].Value2 == null) ? "" : activeSheet.Range["M90"].Value2;
                    sd.SiteExpTotLabor_RT = (activeSheet.Range["M91"].Value2 == null) ? "" : activeSheet.Range["M91"].Value2;
                    sd.SiteExpCompMatl_RT = (activeSheet.Range["M92"].Value2 == null) ? "" : activeSheet.Range["M92"].Value2;
                    sd.SiteExpContMatl_RT = (activeSheet.Range["M93"].Value2 == null) ? "" : activeSheet.Range["M93"].Value2;
                    sd.SiteExpTotMatl_RT = (activeSheet.Range["M94"].Value2 == null) ? "" : activeSheet.Range["M94"].Value2;
                    sd.SiteExpTotDirect_RT = (activeSheet.Range["M95"].Value2 == null) ? "" : activeSheet.Range["M95"].Value2;
                    sd.SiteExpIndirectCompSupport_RT = (activeSheet.Range["M98"].Value2 == null) ? "" : activeSheet.Range["M98"].Value2;
                    sd.SiteExpIndirectContSupport_RT = (activeSheet.Range["M99"].Value2 == null) ? "" : activeSheet.Range["M99"].Value2;
                    sd.SiteExpTotIndirect_RT = (activeSheet.Range["M100"].Value2 == null) ? "" : activeSheet.Range["M100"].Value2;
                    sd.SiteExpTotMaint_RT = (activeSheet.Range["M102"].Value2 == null) ? "" : activeSheet.Range["M102"].Value2;
                    sd.SiteExpCompLabor_RT = (activeSheet.Range["M106"].Value2 == null) ? "" : activeSheet.Range["M106"].Value2;
                    sd.SiteExpContLabor_RT = (activeSheet.Range["M107"].Value2 == null) ? "" : activeSheet.Range["M107"].Value2;
                    sd.SiteExpTotLabor_RT = (activeSheet.Range["M108"].Value2 == null) ? "" : activeSheet.Range["M108"].Value2;
                    sd.SiteMCptlCompLabor_RT = (activeSheet.Range["M111"].Value2 == null) ? "" : activeSheet.Range["M111"].Value2;
                    sd.SiteMCptlContLabor_RT = (activeSheet.Range["M112"].Value2 == null) ? "" : activeSheet.Range["M112"].Value2;
                    sd.SiteMCptlTotLabor_RT = (activeSheet.Range["M113"].Value2 == null) ? "" : activeSheet.Range["M113"].Value2;
                    sd.SiteExpCompLabor_TA = (activeSheet.Range["M120"].Value2 == null) ? "" : activeSheet.Range["M120"].Value2;
                    sd.SiteExpContLabor_TA = (activeSheet.Range["M121"].Value2 == null) ? "" : activeSheet.Range["M121"].Value2;
                    sd.SiteExpTotLabor_TA = (activeSheet.Range["M122"].Value2 == null) ? "" : activeSheet.Range["M122"].Value2;
                    sd.SiteExpCompMatl_TA = (activeSheet.Range["M123"].Value2 == null) ? "" : activeSheet.Range["M123"].Value2;
                    sd.SiteExpContMatl_TA = (activeSheet.Range["M124"].Value2 == null) ? "" : activeSheet.Range["M124"].Value2;
                    sd.SiteExpTotMatl_TA = (activeSheet.Range["M125"].Value2 == null) ? "" : activeSheet.Range["M125"].Value2;
                    sd.SiteExpTotDirect_TA = (activeSheet.Range["M126"].Value2 == null) ? "" : activeSheet.Range["M126"].Value2;
                    sd.SiteExpIndirectCompSupport_TA = (activeSheet.Range["M129"].Value2 == null) ? "" : activeSheet.Range["M129"].Value2;
                    sd.SiteExpIndirectContSupport_TA = (activeSheet.Range["M130"].Value2 == null) ? "" : activeSheet.Range["M130"].Value2;
                    sd.SiteExpTotIndirect_TA = (activeSheet.Range["M131"].Value2 == null) ? "" : activeSheet.Range["M131"].Value2;
                    sd.SiteExpTotMaint_TA = (activeSheet.Range["M133"].Value2 == null) ? "" : activeSheet.Range["M133"].Value2;
                    sd.SiteExpCompLabor_TA = (activeSheet.Range["M137"].Value2 == null) ? "" : activeSheet.Range["M137"].Value2;
                    sd.SiteExpContLabor_TA = (activeSheet.Range["M138"].Value2 == null) ? "" : activeSheet.Range["M138"].Value2;
                    sd.SiteExpTotLabor_TA = (activeSheet.Range["M139"].Value2 == null) ? "" : activeSheet.Range["M139"].Value2;
                    sd.SiteMCptlCompLabor_TA = (activeSheet.Range["M142"].Value2 == null) ? "" : activeSheet.Range["M142"].Value2;
                    sd.SiteMCptlContLabor_TA = (activeSheet.Range["M143"].Value2 == null) ? "" : activeSheet.Range["M143"].Value2;
                    sd.SiteMCptlTotLabor_TA = (activeSheet.Range["M144"].Value2 == null) ? "" : activeSheet.Range["M144"].Value2;
                    sd.CostCatScaffolding_RT = (activeSheet.Range["M151"].Value2 == null) ? "" : activeSheet.Range["M151"].Value2;
                    sd.CostCatPainting_RT = (activeSheet.Range["M152"].Value2 == null) ? "" : activeSheet.Range["M152"].Value2;
                    sd.CostCatInsulation_RT = (activeSheet.Range["M153"].Value2 == null) ? "" : activeSheet.Range["M153"].Value2;
                    sd.CostCatTotGeneral_RT = (activeSheet.Range["M154"].Value2 == null) ? "" : activeSheet.Range["M154"].Value2;
                    sd.SiteWorkEnvirPlanTrainHrs_CORE = (activeSheet.Range["M161"].Value2 == null) ? "" : activeSheet.Range["M161"].Value2;
                    sd.SiteWorkEnvirAvgTrainHrs_CORE = (activeSheet.Range["M162"].Value2 == null) ? "" : activeSheet.Range["M162"].Value2;
                    sd.SiteWorkEnvirSector_CORE = (activeSheet.Range["M163"].Value2 == null) ? "" : activeSheet.Range["M163"].Value2;
                    sd.SiteWorkEnvirExperInJob_CORE = (activeSheet.Range["M164"].Value2 == null) ? "" : activeSheet.Range["M164"].Value2;
                    sd.SiteWorkEnvirPlanTrainHrs_COFP = (activeSheet.Range["M167"].Value2 == null) ? "" : activeSheet.Range["M167"].Value2;
                    sd.SiteWorkEnvirAvgTrainHrs_COFP = (activeSheet.Range["M168"].Value2 == null) ? "" : activeSheet.Range["M168"].Value2;
                    sd.SiteWorkEnvirSector_COFP = (activeSheet.Range["M169"].Value2 == null) ? "" : activeSheet.Range["M169"].Value2;
                    sd.SiteWorkEnvirExperInJob_COFP = (activeSheet.Range["M170"].Value2 == null) ? "" : activeSheet.Range["M170"].Value2;
                    sd.SiteWorkEnvirPlanTrainHrs_COIE = (activeSheet.Range["M173"].Value2 == null) ? "" : activeSheet.Range["M173"].Value2;
                    sd.SiteWorkEnvirAvgTrainHrs_COIE = (activeSheet.Range["M174"].Value2 == null) ? "" : activeSheet.Range["M174"].Value2;
                    sd.SiteWorkEnvirSector_COIE = (activeSheet.Range["M175"].Value2 == null) ? "" : activeSheet.Range["M175"].Value2;
                    sd.SiteWorkEnvirExperInJob_COIE = (activeSheet.Range["M176"].Value2 == null) ? "" : activeSheet.Range["M176"].Value2;
                    sd.SiteWorkEnvirLaborUnion_CO = (activeSheet.Range["M179"].Value2 == null) ? "" : activeSheet.Range["M179"].Value2;
                    sd.SiteWorkEnvirOTPcnt_CO = (activeSheet.Range["M180"].Value2 == null) ? "" : activeSheet.Range["M180"].Value2;
                    sd.SiteWorkEnvirRegsCert_CO = (activeSheet.Range["M181"].Value2 == null) ? "" : activeSheet.Range["M181"].Value2;
                    sd.SiteWorkEnvirRCARCFA_CO = (activeSheet.Range["M182"].Value2 == null) ? "" : activeSheet.Range["M182"].Value2;
                    sd.SiteWorkEnvirReqDocFail_CO = (activeSheet.Range["M183"].Value2 == null) ? "" : activeSheet.Range["M183"].Value2;
                    sd.SiteSparesCatalogLineItemCnt = (activeSheet.Range["M190"].Value2 == null) ? "" : activeSheet.Range["M190"].Value2;
                    sd.SiteSparesInventory = (activeSheet.Range["M191"].Value2 == null) ? "" : activeSheet.Range["M191"].Value2;
                    sd.SiteSparesCosignOrVendorPcnt = (activeSheet.Range["M192"].Value2 == null) ? "" : activeSheet.Range["M192"].Value2;
                    sd.SiteSparesPlantStoresIssues = (activeSheet.Range["M193"].Value2 == null) ? "" : activeSheet.Range["M193"].Value2;
                    sd.SiteWhsOrgOpCost = (activeSheet.Range["M194"].Value2 == null) ? "" : activeSheet.Range["M194"].Value2;
                    sd.SiteWhsOrgPcntCriticalRank = (activeSheet.Range["M196"].Value2 == null) ? "" : activeSheet.Range["M196"].Value2;
                    sd.SiteWhsOrgPrevMaintStrRmItems = (activeSheet.Range["M197"].Value2 == null) ? "" : activeSheet.Range["M197"].Value2;
                    sd.SiteWhsOrgLCCAStrRmItems = (activeSheet.Range["M198"].Value2 == null) ? "" : activeSheet.Range["M198"].Value2;
                    sd.SiteWhsOrgPcntKitted = (activeSheet.Range["M199"].Value2 == null) ? "" : activeSheet.Range["M199"].Value2;
                    sd.SiteWhsOrgPcntDelivered = (activeSheet.Range["M200"].Value2 == null) ? "" : activeSheet.Range["M200"].Value2;
                    sd.SiteWhsOrgAvgStockOutPcnt = (activeSheet.Range["M201"].Value2 == null) ? "" : activeSheet.Range["M201"].Value2;
                    sd.SiteWhsOrgClosedStoredPolicy = (activeSheet.Range["M202"].Value2 == null) ? "" : activeSheet.Range["M202"].Value2;
                    sd.SiteWhsOrgLowCostFreeIssue = (activeSheet.Range["M203"].Value2 == null) ? "" : activeSheet.Range["M203"].Value2;
                    sd.SiteWhsOrgOutsourced = (activeSheet.Range["M204"].Value2 == null) ? "" : activeSheet.Range["M204"].Value2;
                    sd.SiteOrgCompMaintOrg = (activeSheet.Range["M210"].Value2 == null) ? "" : activeSheet.Range["M210"].Value2;
                    sd.SiteOrgCompMaintOrgReportsTo = (activeSheet.Range["M211"].Value2 == null) ? "" : activeSheet.Range["M211"].Value2;
                    sd.SiteOrgOverallPlant = (activeSheet.Range["M215"].Value2 == null) ? "" : activeSheet.Range["M215"].Value2;
                    sd.SiteOrgPrimaryLeader = (activeSheet.Range["M216"].Value2 == null) ? "" : activeSheet.Range["M216"].Value2;
                    sd.CorpPersMaintMgr_CO = (activeSheet.Range["M224"].Value2 == null) ? "" : activeSheet.Range["M224"].Value2;
                    sd.CorpPersRelEngr_CO = (activeSheet.Range["M225"].Value2 == null) ? "" : activeSheet.Range["M225"].Value2;
                    sd.CorpPersMaintEngr_CO = (activeSheet.Range["M226"].Value2 == null) ? "" : activeSheet.Range["M226"].Value2;
                    sd.SitePersMaintMgr_CO = (activeSheet.Range["M227"].Value2 == null) ? "" : activeSheet.Range["M227"].Value2;
                    sd.SitePersMaintSupv_CO = (activeSheet.Range["M228"].Value2 == null) ? "" : activeSheet.Range["M228"].Value2;
                    sd.SitePersMaintPlanner_CO = (activeSheet.Range["M229"].Value2 == null) ? "" : activeSheet.Range["M229"].Value2;
                    sd.SitePersMaintSched_CO = (activeSheet.Range["M230"].Value2 == null) ? "" : activeSheet.Range["M230"].Value2;
                    sd.SitePersTechnical_CO = (activeSheet.Range["M231"].Value2 == null) ? "" : activeSheet.Range["M231"].Value2;
                    sd.SitePersMaintEngr_CO = (activeSheet.Range["M232"].Value2 == null) ? "" : activeSheet.Range["M232"].Value2;
                    sd.SitePersMaintAdmin_CO = (activeSheet.Range["M233"].Value2 == null) ? "" : activeSheet.Range["M233"].Value2;
                    sd.SitePersMaintMSPMStaff_CO = (activeSheet.Range["M234"].Value2 == null) ? "" : activeSheet.Range["M234"].Value2;
                    sd.SitePersMaintStoreroomMgrs_CO = (activeSheet.Range["M235"].Value2 == null) ? "" : activeSheet.Range["M235"].Value2;
                    sd.SitePersTotSupport_CO = (activeSheet.Range["M236"].Value2 == null) ? "" : activeSheet.Range["M236"].Value2;
                    sd.SiteOrgUpperLevels = (activeSheet.Range["M240"].Value2 == null) ? "" : activeSheet.Range["M240"].Value2;
                    sd.SiteOrgLevelsGMtoCraft = (activeSheet.Range["M241"].Value2 == null) ? "" : activeSheet.Range["M241"].Value2;
                    sd.SiteOrgLevelsCEOtoCraft = (activeSheet.Range["M242"].Value2 == null) ? "" : activeSheet.Range["M242"].Value2;
                    sd.SiteDocDefinedWorkflow_RT = (activeSheet.Range["M248"].Value2 == null) ? "" : activeSheet.Range["M248"].Value2;
                    sd.SiteDocWorkflowFollowed_RT = (activeSheet.Range["M249"].Value2 == null) ? "" : activeSheet.Range["M249"].Value2;
                    sd.SiteDocWorkflowExtAssessed_RT = (activeSheet.Range["M250"].Value2 == null) ? "" : activeSheet.Range["M250"].Value2;
                    sd.SiteRelCritEquipMeth_RE = (activeSheet.Range["M306"].Value2 == null) ? "" : activeSheet.Range["M306"].Value2;
                    sd.SiteRelDowntimeAutoRec_RE = (activeSheet.Range["M307"].Value2 == null) ? "" : activeSheet.Range["M307"].Value2;
                    sd.SiteRelLCCA_RE = (activeSheet.Range["M308"].Value2 == null) ? "" : activeSheet.Range["M308"].Value2;
                    sd.SiteRelRCM_RE = (activeSheet.Range["M309"].Value2 == null) ? "" : activeSheet.Range["M309"].Value2;
                    sd.SiteRelEquipEngrStd_RE = (activeSheet.Range["M310"].Value2 == null) ? "" : activeSheet.Range["M310"].Value2;
                    sd.SiteRelRiskBasedInsp_RE = (activeSheet.Range["M311"].Value2 == null) ? "" : activeSheet.Range["M311"].Value2;
                    sd.SiteRelFailureAnalysis_RE = (activeSheet.Range["M312"].Value2 == null) ? "" : activeSheet.Range["M312"].Value2;
                    sd.SiteRelPIP5S = (activeSheet.Range["M316"].Value2 == null) ? "" : activeSheet.Range["M316"].Value2;
                    sd.SiteRelPIPTPM = (activeSheet.Range["M317"].Value2 == null) ? "" : activeSheet.Range["M317"].Value2;
                    sd.SiteRelPIP6SigmaGreenBelt = (activeSheet.Range["M318"].Value2 == null) ? "" : activeSheet.Range["M318"].Value2;
                    sd.SiteRelPIP6SigmaBlackBelt = (activeSheet.Range["M319"].Value2 == null) ? "" : activeSheet.Range["M319"].Value2;
                    sd.SiteRelPIPMonteCarloSim = (activeSheet.Range["M322"].Value2 == null) ? "" : activeSheet.Range["M322"].Value2;
                    sd.SiteRelPIPPoissonDist = (activeSheet.Range["M323"].Value2 == null) ? "" : activeSheet.Range["M323"].Value2;
                    sd.SiteRelPIPWeibullAnalysis = (activeSheet.Range["M324"].Value2 == null) ? "" : activeSheet.Range["M324"].Value2;
                    sd.SiteRelCMMSSuppRAMProcess = (activeSheet.Range["M327"].Value2 == null) ? "" : activeSheet.Range["M327"].Value2;
                    sd.SiteRelCMMSSuppPlanning = (activeSheet.Range["M329"].Value2 == null) ? "" : activeSheet.Range["M329"].Value2;
                    sd.SiteRelCMMSSuppScheduling = (activeSheet.Range["M330"].Value2 == null) ? "" : activeSheet.Range["M330"].Value2;
                    sd.SiteRelCMMSSuppMgmtChng = (activeSheet.Range["M331"].Value2 == null) ? "" : activeSheet.Range["M331"].Value2;
                    sd.SiteTATaken = (activeSheet.Range["M338"].Value2 == null) ? "" : activeSheet.Range["M338"].Value2;
                    sd.SiteTA10YearCnt = (activeSheet.Range["M340"].Value2 == null) ? "" : activeSheet.Range["M340"].Value2;
                    sd.SiteTAAvgDaysDown = (activeSheet.Range["M341"].Value2 == null) ? "" : activeSheet.Range["M341"].Value2;
                    sd.StudyEffortDataCollectNumPers = (activeSheet.Range["M347"].Value2 == null) ? "" : activeSheet.Range["M347"].Value2;
                    sd.StudyEffortDataCollectHrs = (activeSheet.Range["M348"].Value2 == null) ? "" : activeSheet.Range["M348"].Value2;
                    sd.StudyEffortDataEntryNumPers = (activeSheet.Range["M349"].Value2 == null) ? "" : activeSheet.Range["M349"].Value2;
                    sd.StudyEffortDataEntryHrs = (activeSheet.Range["M350"].Value2 == null) ? "" : activeSheet.Range["M350"].Value2;
                    sd.StudyEffortDataCollectHrs_Person = (activeSheet.Range["M351"].Value2 == null) ? "" : activeSheet.Range["M351"].Value2;
                    sd.StudyEffortDataEntryHrs_Person = (activeSheet.Range["M352"].Value2 == null) ? "" : activeSheet.Range["M352"].Value2;
                    sd.StudyDataCollectMethod_Char = (activeSheet.Range["M356"].Value2 == null) ? "" : activeSheet.Range["M356"].Value2;
                    sd.StudyDataCollectMethod_RTCost = (activeSheet.Range["M357"].Value2 == null) ? "" : activeSheet.Range["M357"].Value2;
                    sd.StudyDataCollectMethod_TACost = (activeSheet.Range["M358"].Value2 == null) ? "" : activeSheet.Range["M358"].Value2;
                    sd.StudyDataCollectMethod_MCptl = (activeSheet.Range["M359"].Value2 == null) ? "" : activeSheet.Range["M359"].Value2;
                    sd.StudyDataCollectMethod_MHrs = (activeSheet.Range["M360"].Value2 == null) ? "" : activeSheet.Range["M360"].Value2;
                    sd.StudyDataCollectMethod_MRO = (activeSheet.Range["M361"].Value2 == null) ? "" : activeSheet.Range["M361"].Value2;
                    sd.StudyDataCollectMethod_MaintOrg = (activeSheet.Range["M362"].Value2 == null) ? "" : activeSheet.Range["M362"].Value2;
                    sd.StudyDataCollectMethod_WorkProcess = (activeSheet.Range["M363"].Value2 == null) ? "" : activeSheet.Range["M363"].Value2;
                    sd.StudyDataCollectMethod_Reliability = (activeSheet.Range["M364"].Value2 == null) ? "" : activeSheet.Range["M364"].Value2;
                    sd.StudyDataCollectMethod_ProcessChar = (activeSheet.Range["M365"].Value2 == null) ? "" : activeSheet.Range["M365"].Value2;
                    sd.StudyDataCollectMethod_EquipData = (activeSheet.Range["M366"].Value2 == null) ? "" : activeSheet.Range["M366"].Value2;
                    sd.StudyDataCollectMethod_WorkTypes = (activeSheet.Range["M367"].Value2 == null) ? "" : activeSheet.Range["M367"].Value2;
                    sd.SitePRVUSD = (activeSheet.Range["N43"].Value2 == null) ? "" : activeSheet.Range["N43"].Value2;
                    sd.SiteSafetyCurrTRIRComp_RT = (activeSheet.Range["N55"].Value2 == null) ? "" : activeSheet.Range["N55"].Value2;
                    sd.SiteStrategyNumKPIWeekly = (activeSheet.Range["N71"].Value2 == null) ? "" : activeSheet.Range["N71"].Value2;
                    sd.SiteMCptlCompLabor_RT = (activeSheet.Range["N89"].Value2 == null) ? "" : activeSheet.Range["N89"].Value2;
                    sd.SiteMCptlContLabor_RT = (activeSheet.Range["N90"].Value2 == null) ? "" : activeSheet.Range["N90"].Value2;
                    sd.SiteMCptlTotLabor_RT = (activeSheet.Range["N91"].Value2 == null) ? "" : activeSheet.Range["N91"].Value2;
                    sd.SiteMCptlCompMatl_RT = (activeSheet.Range["N92"].Value2 == null) ? "" : activeSheet.Range["N92"].Value2;
                    sd.SiteMCptlContMatl_RT = (activeSheet.Range["N93"].Value2 == null) ? "" : activeSheet.Range["N93"].Value2;
                    sd.SiteMCptlTotMatl_RT = (activeSheet.Range["N94"].Value2 == null) ? "" : activeSheet.Range["N94"].Value2;
                    sd.SiteMCptlTotDirectCost_RT = (activeSheet.Range["N95"].Value2 == null) ? "" : activeSheet.Range["N95"].Value2;
                    sd.SiteMCptlCompIndirectCost_RT = (activeSheet.Range["N98"].Value2 == null) ? "" : activeSheet.Range["N98"].Value2;
                    sd.SiteMCptlContIndirectCost_RT = (activeSheet.Range["N99"].Value2 == null) ? "" : activeSheet.Range["N99"].Value2;
                    sd.SiteMCptlTotIndirectCost_RT = (activeSheet.Range["N100"].Value2 == null) ? "" : activeSheet.Range["N100"].Value2;
                    sd.SiteMCptlTotCost_RT = (activeSheet.Range["N102"].Value2 == null) ? "" : activeSheet.Range["N102"].Value2;
                    sd.SiteExpCompManHrs_RT = (activeSheet.Range["N106"].Value2 == null) ? "" : activeSheet.Range["N106"].Value2;
                    sd.SiteExpContManHrs_RT = (activeSheet.Range["N107"].Value2 == null) ? "" : activeSheet.Range["N107"].Value2;
                    sd.SiteExpTotManHrs_RT = (activeSheet.Range["N108"].Value2 == null) ? "" : activeSheet.Range["N108"].Value2;
                    sd.SiteMCptlCompManHrs_RT = (activeSheet.Range["N111"].Value2 == null) ? "" : activeSheet.Range["N111"].Value2;
                    sd.SiteMCptlContManHrs_RT = (activeSheet.Range["N112"].Value2 == null) ? "" : activeSheet.Range["N112"].Value2;
                    sd.SiteMCptlTotManHrs_RT = (activeSheet.Range["N113"].Value2 == null) ? "" : activeSheet.Range["N113"].Value2;
                    sd.SiteMCptlCompLabor_TA = (activeSheet.Range["N120"].Value2 == null) ? "" : activeSheet.Range["N120"].Value2;
                    sd.SiteMCptlContLabor_TA = (activeSheet.Range["N121"].Value2 == null) ? "" : activeSheet.Range["N121"].Value2;
                    sd.SiteMCptlTotLabor_TA = (activeSheet.Range["N122"].Value2 == null) ? "" : activeSheet.Range["N122"].Value2;
                    sd.SiteMCptlCompMatl_TA = (activeSheet.Range["N123"].Value2 == null) ? "" : activeSheet.Range["N123"].Value2;
                    sd.SiteMCptlContMatl_TA = (activeSheet.Range["N124"].Value2 == null) ? "" : activeSheet.Range["N124"].Value2;
                    sd.SiteMCptlTotMatl_TA = (activeSheet.Range["N125"].Value2 == null) ? "" : activeSheet.Range["N125"].Value2;
                    sd.SiteMCptlTotDirectCost_TA = (activeSheet.Range["N126"].Value2 == null) ? "" : activeSheet.Range["N126"].Value2;
                    sd.SiteMCptlCompIndirectCost_TA = (activeSheet.Range["N129"].Value2 == null) ? "" : activeSheet.Range["N129"].Value2;
                    sd.SiteMCptlContIndirectCost_TA = (activeSheet.Range["N130"].Value2 == null) ? "" : activeSheet.Range["N130"].Value2;
                    sd.SiteMCptlTotIndirectCost_TA = (activeSheet.Range["N131"].Value2 == null) ? "" : activeSheet.Range["N131"].Value2;
                    sd.SiteMCptlTotCost_TA = (activeSheet.Range["N133"].Value2 == null) ? "" : activeSheet.Range["N133"].Value2;
                    sd.SiteExpCompManHrs_TA = (activeSheet.Range["N137"].Value2 == null) ? "" : activeSheet.Range["N137"].Value2;
                    sd.SiteExpContManHrs_TA = (activeSheet.Range["N138"].Value2 == null) ? "" : activeSheet.Range["N138"].Value2;
                    sd.SiteExpTotManHrs_TA = (activeSheet.Range["N139"].Value2 == null) ? "" : activeSheet.Range["N139"].Value2;
                    sd.SiteMCptlCompManHrs_TA = (activeSheet.Range["N142"].Value2 == null) ? "" : activeSheet.Range["N142"].Value2;
                    sd.SiteMCptlContManHrs_TA = (activeSheet.Range["N143"].Value2 == null) ? "" : activeSheet.Range["N143"].Value2;
                    sd.SiteMCptlTotManHrs_TA = (activeSheet.Range["N144"].Value2 == null) ? "" : activeSheet.Range["N144"].Value2;
                    sd.CostCatScaffolding_TA = (activeSheet.Range["N151"].Value2 == null) ? "" : activeSheet.Range["N151"].Value2;
                    sd.CostCatPainting_TA = (activeSheet.Range["N152"].Value2 == null) ? "" : activeSheet.Range["N152"].Value2;
                    sd.CostCatInsulation_TA = (activeSheet.Range["N153"].Value2 == null) ? "" : activeSheet.Range["N153"].Value2;
                    sd.CostCatTotGeneral_TA = (activeSheet.Range["N154"].Value2 == null) ? "" : activeSheet.Range["N154"].Value2;
                    sd.SiteWorkEnvirPlanTrainHrs_CRRE = (activeSheet.Range["N161"].Value2 == null) ? "" : activeSheet.Range["N161"].Value2;
                    sd.SiteWorkEnvirAvgTrainHrs_CRRE = (activeSheet.Range["N162"].Value2 == null) ? "" : activeSheet.Range["N162"].Value2;
                    sd.SiteWorkEnvirSector_CRRE = (activeSheet.Range["N163"].Value2 == null) ? "" : activeSheet.Range["N163"].Value2;
                    sd.SiteWorkEnvirExperInJob_CRRE = (activeSheet.Range["N164"].Value2 == null) ? "" : activeSheet.Range["N164"].Value2;
                    sd.SiteWorkEnvirPlanTrainHrs_CRFP = (activeSheet.Range["N167"].Value2 == null) ? "" : activeSheet.Range["N167"].Value2;
                    sd.SiteWorkEnvirAvgTrainHrs_CRFP = (activeSheet.Range["N168"].Value2 == null) ? "" : activeSheet.Range["N168"].Value2;
                    sd.SiteWorkEnvirSector_CRFP = (activeSheet.Range["N169"].Value2 == null) ? "" : activeSheet.Range["N169"].Value2;
                    sd.SiteWorkEnvirExperInJob_CRFP = (activeSheet.Range["N170"].Value2 == null) ? "" : activeSheet.Range["N170"].Value2;
                    sd.SiteWorkEnvirPlanTrainHrs_CRIE = (activeSheet.Range["N173"].Value2 == null) ? "" : activeSheet.Range["N173"].Value2;
                    sd.SiteWorkEnvirAvgTrainHrs_CRIE = (activeSheet.Range["N174"].Value2 == null) ? "" : activeSheet.Range["N174"].Value2;
                    sd.SiteWorkEnvirSector_CRIE = (activeSheet.Range["N175"].Value2 == null) ? "" : activeSheet.Range["N175"].Value2;
                    sd.SiteWorkEnvirExperInJob_CRIE = (activeSheet.Range["N176"].Value2 == null) ? "" : activeSheet.Range["N176"].Value2;
                    sd.SiteWorkEnvirLaborUnion_CR = (activeSheet.Range["N179"].Value2 == null) ? "" : activeSheet.Range["N179"].Value2;
                    sd.SiteWorkEnvirOTPcnt_CR = (activeSheet.Range["N180"].Value2 == null) ? "" : activeSheet.Range["N180"].Value2;
                    sd.SiteWorkEnvirRegsCert_CR = (activeSheet.Range["N181"].Value2 == null) ? "" : activeSheet.Range["N181"].Value2;
                    sd.SiteWorkEnvirRCARCFA_CR = (activeSheet.Range["N182"].Value2 == null) ? "" : activeSheet.Range["N182"].Value2;
                    sd.SiteWorkEnvirReqDocFail_CR = (activeSheet.Range["N183"].Value2 == null) ? "" : activeSheet.Range["N183"].Value2;
                    sd.SiteSparesInventoryUSD = (activeSheet.Range["N191"].Value2 == null) ? "" : activeSheet.Range["N191"].Value2;
                    sd.SiteSparesPlantStoresIssuesUSD = (activeSheet.Range["N193"].Value2 == null) ? "" : activeSheet.Range["N193"].Value2;
                    sd.SiteWhsOrgOpCostUSD = (activeSheet.Range["N194"].Value2 == null) ? "" : activeSheet.Range["N194"].Value2;
                    sd.CorpPersMaintMgr_CR = (activeSheet.Range["N224"].Value2 == null) ? "" : activeSheet.Range["N224"].Value2;
                    sd.CorpPersRelEngr_CR = (activeSheet.Range["N225"].Value2 == null) ? "" : activeSheet.Range["N225"].Value2;
                    sd.CorpPersMaintEngr_CR = (activeSheet.Range["N226"].Value2 == null) ? "" : activeSheet.Range["N226"].Value2;
                    sd.SitePersMaintMgr_CR = (activeSheet.Range["N227"].Value2 == null) ? "" : activeSheet.Range["N227"].Value2;
                    sd.SitePersMaintSupv_CR = (activeSheet.Range["N228"].Value2 == null) ? "" : activeSheet.Range["N228"].Value2;
                    sd.SitePersMaintPlanner_CR = (activeSheet.Range["N229"].Value2 == null) ? "" : activeSheet.Range["N229"].Value2;
                    sd.SitePersMaintSched_CR = (activeSheet.Range["N230"].Value2 == null) ? "" : activeSheet.Range["N230"].Value2;
                    sd.SitePersTechnical_CR = (activeSheet.Range["N231"].Value2 == null) ? "" : activeSheet.Range["N231"].Value2;
                    sd.SitePersMaintEngr_CR = (activeSheet.Range["N232"].Value2 == null) ? "" : activeSheet.Range["N232"].Value2;
                    sd.SitePersMaintAdmin_CR = (activeSheet.Range["N233"].Value2 == null) ? "" : activeSheet.Range["N233"].Value2;
                    sd.SitePersMaintMSPMStaff_CR = (activeSheet.Range["N234"].Value2 == null) ? "" : activeSheet.Range["N234"].Value2;
                    sd.SitePersMaintStoreroomMgrs_CR = (activeSheet.Range["N235"].Value2 == null) ? "" : activeSheet.Range["N235"].Value2;
                    sd.SitePersTotSupport_CR = (activeSheet.Range["N236"].Value2 == null) ? "" : activeSheet.Range["N236"].Value2;
                    sd.SiteDocDefinedWorkflow_TA = (activeSheet.Range["N248"].Value2 == null) ? "" : activeSheet.Range["N248"].Value2;
                    sd.SiteDocWorkflowFollowed_TA = (activeSheet.Range["N249"].Value2 == null) ? "" : activeSheet.Range["N249"].Value2;
                    sd.SiteDocWorkflowExtAssessed_TA = (activeSheet.Range["N250"].Value2 == null) ? "" : activeSheet.Range["N250"].Value2;
                    sd.SiteWorkOrdersTotalOrders_CORE = (activeSheet.Range["N254"].Value2 == null) ? "" : activeSheet.Range["N254"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PRORE = (activeSheet.Range["N255"].Value2 == null) ? "" : activeSheet.Range["N255"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PRERE = (activeSheet.Range["N256"].Value2 == null) ? "" : activeSheet.Range["N256"].Value2;
                    sd.SiteWorkOrdersTotalOrders_CAPRE = (activeSheet.Range["N257"].Value2 == null) ? "" : activeSheet.Range["N257"].Value2;
                    sd.SiteWorkOrdersTotalOrders_TTLRE = (activeSheet.Range["N258"].Value2 == null) ? "" : activeSheet.Range["N258"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_CORE = (activeSheet.Range["N260"].Value2 == null) ? "" : activeSheet.Range["N260"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PRORE = (activeSheet.Range["N261"].Value2 == null) ? "" : activeSheet.Range["N261"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PRERE = (activeSheet.Range["N262"].Value2 == null) ? "" : activeSheet.Range["N262"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_CAPRE = (activeSheet.Range["N263"].Value2 == null) ? "" : activeSheet.Range["N263"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_TTLRE = (activeSheet.Range["N264"].Value2 == null) ? "" : activeSheet.Range["N264"].Value2;
                    sd.SiteWorkOrdersTotalSched_CORE = (activeSheet.Range["N266"].Value2 == null) ? "" : activeSheet.Range["N266"].Value2;
                    sd.SiteWorkOrdersTotalSched_PRORE = (activeSheet.Range["N267"].Value2 == null) ? "" : activeSheet.Range["N267"].Value2;
                    sd.SiteWorkOrdersTotalSched_PRERE = (activeSheet.Range["N268"].Value2 == null) ? "" : activeSheet.Range["N268"].Value2;
                    sd.SiteWorkOrdersTotalSched_CAPRE = (activeSheet.Range["N269"].Value2 == null) ? "" : activeSheet.Range["N269"].Value2;
                    sd.SiteWorkOrdersTotalSched_TTLRE = (activeSheet.Range["N270"].Value2 == null) ? "" : activeSheet.Range["N270"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_CORE = (activeSheet.Range["N272"].Value2 == null) ? "" : activeSheet.Range["N272"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PRORE = (activeSheet.Range["N273"].Value2 == null) ? "" : activeSheet.Range["N273"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PRERE = (activeSheet.Range["N274"].Value2 == null) ? "" : activeSheet.Range["N274"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_CAPRE = (activeSheet.Range["N275"].Value2 == null) ? "" : activeSheet.Range["N275"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_TTLRE = (activeSheet.Range["N276"].Value2 == null) ? "" : activeSheet.Range["N276"].Value2;
                    sd.SiteWorkOrdersEmergencyOrders_RE = (activeSheet.Range["N278"].Value2 == null) ? "" : activeSheet.Range["N278"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_CORE = (activeSheet.Range["N282"].Value2 == null) ? "" : activeSheet.Range["N282"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_PRORE = (activeSheet.Range["N283"].Value2 == null) ? "" : activeSheet.Range["N283"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_PRERE = (activeSheet.Range["N284"].Value2 == null) ? "" : activeSheet.Range["N284"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_CORE = (activeSheet.Range["N286"].Value2 == null) ? "" : activeSheet.Range["N286"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_PRORE = (activeSheet.Range["N287"].Value2 == null) ? "" : activeSheet.Range["N287"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_PRERE = (activeSheet.Range["N288"].Value2 == null) ? "" : activeSheet.Range["N288"].Value2;
                    sd.SiteWorkOrdersLibraryStorage_COTOT = (activeSheet.Range["N290"].Value2 == null) ? "" : activeSheet.Range["N290"].Value2;
                    sd.SiteWorkOrdersLibraryReport_COTOT = (activeSheet.Range["N291"].Value2 == null) ? "" : activeSheet.Range["N291"].Value2;
                    sd.SiteWorkOrdersPcntCapacitySched = (activeSheet.Range["N294"].Value2 == null) ? "" : activeSheet.Range["N294"].Value2;
                    sd.SiteWorkOrdersFreqMeasureCmpl = (activeSheet.Range["N295"].Value2 == null) ? "" : activeSheet.Range["N295"].Value2;
                    sd.SiteRelCritEquipMeth_FP = (activeSheet.Range["N306"].Value2 == null) ? "" : activeSheet.Range["N306"].Value2;
                    sd.SiteRelDowntimeAutoRec_FP = (activeSheet.Range["N307"].Value2 == null) ? "" : activeSheet.Range["N307"].Value2;
                    sd.SiteRelLCCA_FP = (activeSheet.Range["N308"].Value2 == null) ? "" : activeSheet.Range["N308"].Value2;
                    sd.SiteRelRCM_FP = (activeSheet.Range["N309"].Value2 == null) ? "" : activeSheet.Range["N309"].Value2;
                    sd.SiteRelEquipEngrStd_FP = (activeSheet.Range["N310"].Value2 == null) ? "" : activeSheet.Range["N310"].Value2;
                    sd.SiteRelRiskBasedInsp_FP = (activeSheet.Range["N311"].Value2 == null) ? "" : activeSheet.Range["N311"].Value2;
                    sd.SiteRelFailureAnalysis_FP = (activeSheet.Range["N312"].Value2 == null) ? "" : activeSheet.Range["N312"].Value2;
                    sd.SiteStrategyNumKPIMonthly = (activeSheet.Range["O71"].Value2 == null) ? "" : activeSheet.Range["O71"].Value2;
                    sd.SiteCostsCompLabor_RT = (activeSheet.Range["O89"].Value2 == null) ? "" : activeSheet.Range["O89"].Value2;
                    sd.SiteCostsContLabor_RT = (activeSheet.Range["O90"].Value2 == null) ? "" : activeSheet.Range["O90"].Value2;
                    sd.SiteCostsTotLabor_RT = (activeSheet.Range["O91"].Value2 == null) ? "" : activeSheet.Range["O91"].Value2;
                    sd.SiteCostsCompMatl_RT = (activeSheet.Range["O92"].Value2 == null) ? "" : activeSheet.Range["O92"].Value2;
                    sd.SiteCostsContMatl_RT = (activeSheet.Range["O93"].Value2 == null) ? "" : activeSheet.Range["O93"].Value2;
                    sd.SiteCostsTotMatl_RT = (activeSheet.Range["O94"].Value2 == null) ? "" : activeSheet.Range["O94"].Value2;
                    sd.SiteCostsTotDirect_RT = (activeSheet.Range["O95"].Value2 == null) ? "" : activeSheet.Range["O95"].Value2;
                    sd.SiteCostsIndirectCompSupport_RT = (activeSheet.Range["O98"].Value2 == null) ? "" : activeSheet.Range["O98"].Value2;
                    sd.SiteCostsIndirectContSupport_RT = (activeSheet.Range["O99"].Value2 == null) ? "" : activeSheet.Range["O99"].Value2;
                    sd.SiteCostsTotIndirect_RT = (activeSheet.Range["O100"].Value2 == null) ? "" : activeSheet.Range["O100"].Value2;
                    sd.SiteCostsTotMaint_RT = (activeSheet.Range["O102"].Value2 == null) ? "" : activeSheet.Range["O102"].Value2;
                    sd.SiteExpCompWageRate_RT = (activeSheet.Range["O106"].Value2 == null) ? "" : activeSheet.Range["O106"].Value2;
                    sd.SiteExpContWageRate_RT = (activeSheet.Range["O107"].Value2 == null) ? "" : activeSheet.Range["O107"].Value2;
                    sd.SiteExpAvgWageRate_RT = (activeSheet.Range["O108"].Value2 == null) ? "" : activeSheet.Range["O108"].Value2;
                    sd.SiteMCptlCompWageRate_RT = (activeSheet.Range["O111"].Value2 == null) ? "" : activeSheet.Range["O111"].Value2;
                    sd.SiteMCptlContWageRate_RT = (activeSheet.Range["O112"].Value2 == null) ? "" : activeSheet.Range["O112"].Value2;
                    sd.SiteMCptlAvgWageRate_RT = (activeSheet.Range["O113"].Value2 == null) ? "" : activeSheet.Range["O113"].Value2;
                    sd.SiteCostsCompLabor_TA = (activeSheet.Range["O120"].Value2 == null) ? "" : activeSheet.Range["O120"].Value2;
                    sd.SiteCostsContLabor_TA = (activeSheet.Range["O121"].Value2 == null) ? "" : activeSheet.Range["O121"].Value2;
                    sd.SiteCostsTotLabor_TA = (activeSheet.Range["O122"].Value2 == null) ? "" : activeSheet.Range["O122"].Value2;
                    sd.SiteCostsCompMatl_TA = (activeSheet.Range["O123"].Value2 == null) ? "" : activeSheet.Range["O123"].Value2;
                    sd.SiteCostsContMatl_TA = (activeSheet.Range["O124"].Value2 == null) ? "" : activeSheet.Range["O124"].Value2;
                    sd.SiteCostsTotMatl_TA = (activeSheet.Range["O125"].Value2 == null) ? "" : activeSheet.Range["O125"].Value2;
                    sd.SiteCostsTotDirect_TA = (activeSheet.Range["O126"].Value2 == null) ? "" : activeSheet.Range["O126"].Value2;
                    sd.SiteCostsIndirectCompSupport_TA = (activeSheet.Range["O129"].Value2 == null) ? "" : activeSheet.Range["O129"].Value2;
                    sd.SiteCostsIndirectContSupport_TA = (activeSheet.Range["O130"].Value2 == null) ? "" : activeSheet.Range["O130"].Value2;
                    sd.SiteCostsTotIndirect_TA = (activeSheet.Range["O131"].Value2 == null) ? "" : activeSheet.Range["O131"].Value2;
                    sd.SiteCostsTotMaint_TA = (activeSheet.Range["O133"].Value2 == null) ? "" : activeSheet.Range["O133"].Value2;
                    sd.SiteExpCompWageRate_TA = (activeSheet.Range["O137"].Value2 == null) ? "" : activeSheet.Range["O137"].Value2;
                    sd.SiteExpContWageRate_TA = (activeSheet.Range["O138"].Value2 == null) ? "" : activeSheet.Range["O138"].Value2;
                    sd.SiteExpAvgWageRate_TA = (activeSheet.Range["O139"].Value2 == null) ? "" : activeSheet.Range["O139"].Value2;
                    sd.SiteMCptlCompWageRate_TA = (activeSheet.Range["O142"].Value2 == null) ? "" : activeSheet.Range["O142"].Value2;
                    sd.SiteMCptlContWageRate_TA = (activeSheet.Range["O143"].Value2 == null) ? "" : activeSheet.Range["O143"].Value2;
                    sd.SiteMCptlAvgWageRate_TA = (activeSheet.Range["O144"].Value2 == null) ? "" : activeSheet.Range["O144"].Value2;
                    sd.CorpPersMaintMgr_Tot = (activeSheet.Range["O224"].Value2 == null) ? "" : activeSheet.Range["O224"].Value2;
                    sd.CorpPersRelEngr_Tot = (activeSheet.Range["O225"].Value2 == null) ? "" : activeSheet.Range["O225"].Value2;
                    sd.CorpPersMaintEngr_Tot = (activeSheet.Range["O226"].Value2 == null) ? "" : activeSheet.Range["O226"].Value2;
                    sd.SitePersMaintMgr_Tot = (activeSheet.Range["O227"].Value2 == null) ? "" : activeSheet.Range["O227"].Value2;
                    sd.SitePersMaintSupv_Tot = (activeSheet.Range["O228"].Value2 == null) ? "" : activeSheet.Range["O228"].Value2;
                    sd.SitePersMaintPlanner_Tot = (activeSheet.Range["O229"].Value2 == null) ? "" : activeSheet.Range["O229"].Value2;
                    sd.SitePersMaintSched_Tot = (activeSheet.Range["O230"].Value2 == null) ? "" : activeSheet.Range["O230"].Value2;
                    sd.SitePersTechnical_Tot = (activeSheet.Range["O231"].Value2 == null) ? "" : activeSheet.Range["O231"].Value2;
                    sd.SitePersMaintEngr_Tot = (activeSheet.Range["O232"].Value2 == null) ? "" : activeSheet.Range["O232"].Value2;
                    sd.SitePersMaintAdmin_Tot = (activeSheet.Range["O233"].Value2 == null) ? "" : activeSheet.Range["O233"].Value2;
                    sd.SitePersMaintMSPMStaff_TOT = (activeSheet.Range["O234"].Value2 == null) ? "" : activeSheet.Range["O234"].Value2;
                    sd.SitePersMaintStoreroomMgrs_TOT = (activeSheet.Range["O235"].Value2 == null) ? "" : activeSheet.Range["O235"].Value2;
                    sd.SitePersTotSupport_Tot = (activeSheet.Range["O236"].Value2 == null) ? "" : activeSheet.Range["O236"].Value2;
                    sd.SiteWorkOrdersTotalOrders_COFP = (activeSheet.Range["O254"].Value2 == null) ? "" : activeSheet.Range["O254"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PROFP = (activeSheet.Range["O255"].Value2 == null) ? "" : activeSheet.Range["O255"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PREFP = (activeSheet.Range["O256"].Value2 == null) ? "" : activeSheet.Range["O256"].Value2;
                    sd.SiteWorkOrdersTotalOrders_CAPFP = (activeSheet.Range["O257"].Value2 == null) ? "" : activeSheet.Range["O257"].Value2;
                    sd.SiteWorkOrdersTotalOrders_TTLFP = (activeSheet.Range["O258"].Value2 == null) ? "" : activeSheet.Range["O258"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_COFP = (activeSheet.Range["O260"].Value2 == null) ? "" : activeSheet.Range["O260"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PROFP = (activeSheet.Range["O261"].Value2 == null) ? "" : activeSheet.Range["O261"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PREFP = (activeSheet.Range["O262"].Value2 == null) ? "" : activeSheet.Range["O262"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_CAPFP = (activeSheet.Range["O263"].Value2 == null) ? "" : activeSheet.Range["O263"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_TTLFP = (activeSheet.Range["O264"].Value2 == null) ? "" : activeSheet.Range["O264"].Value2;
                    sd.SiteWorkOrdersTotalSched_COFP = (activeSheet.Range["O266"].Value2 == null) ? "" : activeSheet.Range["O266"].Value2;
                    sd.SiteWorkOrdersTotalSched_PROFP = (activeSheet.Range["O267"].Value2 == null) ? "" : activeSheet.Range["O267"].Value2;
                    sd.SiteWorkOrdersTotalSched_PREFP = (activeSheet.Range["O268"].Value2 == null) ? "" : activeSheet.Range["O268"].Value2;
                    sd.SiteWorkOrdersTotalSched_CAPFP = (activeSheet.Range["O269"].Value2 == null) ? "" : activeSheet.Range["O269"].Value2;
                    sd.SiteWorkOrdersTotalSched_TTLFP = (activeSheet.Range["O270"].Value2 == null) ? "" : activeSheet.Range["O270"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_COFP = (activeSheet.Range["O272"].Value2 == null) ? "" : activeSheet.Range["O272"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PROFP = (activeSheet.Range["O273"].Value2 == null) ? "" : activeSheet.Range["O273"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PREFP = (activeSheet.Range["O274"].Value2 == null) ? "" : activeSheet.Range["O274"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_CAPFP = (activeSheet.Range["O275"].Value2 == null) ? "" : activeSheet.Range["O275"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_TTLFP = (activeSheet.Range["O276"].Value2 == null) ? "" : activeSheet.Range["O276"].Value2;
                    sd.SiteWorkOrdersEmergencyOrders_FP = (activeSheet.Range["O278"].Value2 == null) ? "" : activeSheet.Range["O278"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_COFP = (activeSheet.Range["O282"].Value2 == null) ? "" : activeSheet.Range["O282"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_PROFP = (activeSheet.Range["O283"].Value2 == null) ? "" : activeSheet.Range["O283"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_PREFP = (activeSheet.Range["O284"].Value2 == null) ? "" : activeSheet.Range["O284"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_COFP = (activeSheet.Range["O286"].Value2 == null) ? "" : activeSheet.Range["O286"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_PROFP = (activeSheet.Range["O287"].Value2 == null) ? "" : activeSheet.Range["O287"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_PREFP = (activeSheet.Range["O288"].Value2 == null) ? "" : activeSheet.Range["O288"].Value2;
                    sd.SiteWorkOrdersLibraryStorage_PROTOT = (activeSheet.Range["O290"].Value2 == null) ? "" : activeSheet.Range["O290"].Value2;
                    sd.SiteWorkOrdersLibraryReport_PROTOT = (activeSheet.Range["O291"].Value2 == null) ? "" : activeSheet.Range["O291"].Value2;
                    sd.SiteRelCritEquipMeth_IE = (activeSheet.Range["O306"].Value2 == null) ? "" : activeSheet.Range["O306"].Value2;
                    sd.SiteRelDowntimeAutoRec_IE = (activeSheet.Range["O307"].Value2 == null) ? "" : activeSheet.Range["O307"].Value2;
                    sd.SiteRelLCCA_IE = (activeSheet.Range["O308"].Value2 == null) ? "" : activeSheet.Range["O308"].Value2;
                    sd.SiteRelRCM_IE = (activeSheet.Range["O309"].Value2 == null) ? "" : activeSheet.Range["O309"].Value2;
                    sd.SiteRelEquipEngrStd_IE = (activeSheet.Range["O310"].Value2 == null) ? "" : activeSheet.Range["O310"].Value2;
                    sd.SiteRelRiskBasedInsp_IE = (activeSheet.Range["O311"].Value2 == null) ? "" : activeSheet.Range["O311"].Value2;
                    sd.SiteRelFailureAnalysis_IE = (activeSheet.Range["O312"].Value2 == null) ? "" : activeSheet.Range["O312"].Value2;
                    sd.SiteExpCompWageRateUSD_RT = (activeSheet.Range["P106"].Value2 == null) ? "" : activeSheet.Range["P106"].Value2;
                    sd.SiteExpContWageRateUSD_RT = (activeSheet.Range["P107"].Value2 == null) ? "" : activeSheet.Range["P107"].Value2;
                    sd.SiteExpAvgWageRateUSD_RT = (activeSheet.Range["P108"].Value2 == null) ? "" : activeSheet.Range["P108"].Value2;
                    sd.SiteMCptlCompWageRateUSD_RT = (activeSheet.Range["P111"].Value2 == null) ? "" : activeSheet.Range["P111"].Value2;
                    sd.SiteMCptlContWageRateUSD_RT = (activeSheet.Range["P112"].Value2 == null) ? "" : activeSheet.Range["P112"].Value2;
                    sd.SiteMCptlAvgWageRateUSD_RT = (activeSheet.Range["P113"].Value2 == null) ? "" : activeSheet.Range["P113"].Value2;
                    sd.SiteExpCompWageRateUSD_TA = (activeSheet.Range["P137"].Value2 == null) ? "" : activeSheet.Range["P137"].Value2;
                    sd.SiteExpContWageRateUSD_TA = (activeSheet.Range["P138"].Value2 == null) ? "" : activeSheet.Range["P138"].Value2;
                    sd.SiteExpAvgWageRateUSD_TA = (activeSheet.Range["P139"].Value2 == null) ? "" : activeSheet.Range["P139"].Value2;
                    sd.SiteMCptlCompWageRateUSD_TA = (activeSheet.Range["P142"].Value2 == null) ? "" : activeSheet.Range["P142"].Value2;
                    sd.SiteMCptlContWageRateUSD_TA = (activeSheet.Range["P143"].Value2 == null) ? "" : activeSheet.Range["P143"].Value2;
                    sd.SiteMCptlAvgWageRateUSD_TA = (activeSheet.Range["P144"].Value2 == null) ? "" : activeSheet.Range["P144"].Value2;
                    sd.SiteWorkOrdersTotalOrders_COIE = (activeSheet.Range["P254"].Value2 == null) ? "" : activeSheet.Range["P254"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PROIE = (activeSheet.Range["P255"].Value2 == null) ? "" : activeSheet.Range["P255"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PREIE = (activeSheet.Range["P256"].Value2 == null) ? "" : activeSheet.Range["P256"].Value2;
                    sd.SiteWorkOrdersTotalOrders_CAPIE = (activeSheet.Range["P257"].Value2 == null) ? "" : activeSheet.Range["P257"].Value2;
                    sd.SiteWorkOrdersTotalOrders_TTLIE = (activeSheet.Range["P258"].Value2 == null) ? "" : activeSheet.Range["P258"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_COIE = (activeSheet.Range["P260"].Value2 == null) ? "" : activeSheet.Range["P260"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PROIE = (activeSheet.Range["P261"].Value2 == null) ? "" : activeSheet.Range["P261"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PREIE = (activeSheet.Range["P262"].Value2 == null) ? "" : activeSheet.Range["P262"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_CAPIE = (activeSheet.Range["P263"].Value2 == null) ? "" : activeSheet.Range["P263"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_TTLIE = (activeSheet.Range["P264"].Value2 == null) ? "" : activeSheet.Range["P264"].Value2;
                    sd.SiteWorkOrdersTotalSched_COIE = (activeSheet.Range["P266"].Value2 == null) ? "" : activeSheet.Range["P266"].Value2;
                    sd.SiteWorkOrdersTotalSched_PROIE = (activeSheet.Range["P267"].Value2 == null) ? "" : activeSheet.Range["P267"].Value2;
                    sd.SiteWorkOrdersTotalSched_PREIE = (activeSheet.Range["P268"].Value2 == null) ? "" : activeSheet.Range["P268"].Value2;
                    sd.SiteWorkOrdersTotalSched_CAPIE = (activeSheet.Range["P269"].Value2 == null) ? "" : activeSheet.Range["P269"].Value2;
                    sd.SiteWorkOrdersTotalSched_TTLIE = (activeSheet.Range["P270"].Value2 == null) ? "" : activeSheet.Range["P270"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_COIE = (activeSheet.Range["P272"].Value2 == null) ? "" : activeSheet.Range["P272"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PROIE = (activeSheet.Range["P273"].Value2 == null) ? "" : activeSheet.Range["P273"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PREIE = (activeSheet.Range["P274"].Value2 == null) ? "" : activeSheet.Range["P274"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_CAPIE = (activeSheet.Range["P275"].Value2 == null) ? "" : activeSheet.Range["P275"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_TTLIE = (activeSheet.Range["P276"].Value2 == null) ? "" : activeSheet.Range["P276"].Value2;
                    sd.SiteWorkOrdersEmergencyOrders_IE = (activeSheet.Range["P278"].Value2 == null) ? "" : activeSheet.Range["P278"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_COIE = (activeSheet.Range["P282"].Value2 == null) ? "" : activeSheet.Range["P282"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_PROIE = (activeSheet.Range["P283"].Value2 == null) ? "" : activeSheet.Range["P283"].Value2;
                    sd.SiteWorkOrdersAvgPlanBacklog_PREIE = (activeSheet.Range["P284"].Value2 == null) ? "" : activeSheet.Range["P284"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_COIE = (activeSheet.Range["P286"].Value2 == null) ? "" : activeSheet.Range["P286"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_PROIE = (activeSheet.Range["P287"].Value2 == null) ? "" : activeSheet.Range["P287"].Value2;
                    sd.SiteWorkOrdersAvgReadyBacklog_PREIE = (activeSheet.Range["P288"].Value2 == null) ? "" : activeSheet.Range["P288"].Value2;
                    sd.SiteWorkOrdersLibraryStorage_PRETOT = (activeSheet.Range["P290"].Value2 == null) ? "" : activeSheet.Range["P290"].Value2;
                    sd.SiteWorkOrdersLibraryReport_PRETOT = (activeSheet.Range["P291"].Value2 == null) ? "" : activeSheet.Range["P291"].Value2;
                    sd.SiteWorkOrdersTotalOrders_COTOT = (activeSheet.Range["Q254"].Value2 == null) ? "" : activeSheet.Range["Q254"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PROTOT = (activeSheet.Range["Q255"].Value2 == null) ? "" : activeSheet.Range["Q255"].Value2;
                    sd.SiteWorkOrdersTotalOrders_PRETOT = (activeSheet.Range["Q256"].Value2 == null) ? "" : activeSheet.Range["Q256"].Value2;
                    sd.SiteWorkOrdersTotalOrders_CAPTOT = (activeSheet.Range["Q257"].Value2 == null) ? "" : activeSheet.Range["Q257"].Value2;
                    sd.SiteWorkOrdersTotalOrders_TTLTOT = (activeSheet.Range["Q258"].Value2 == null) ? "" : activeSheet.Range["Q258"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_COTOT = (activeSheet.Range["Q260"].Value2 == null) ? "" : activeSheet.Range["Q260"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PROTOT = (activeSheet.Range["Q261"].Value2 == null) ? "" : activeSheet.Range["Q261"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_PRETOT = (activeSheet.Range["Q262"].Value2 == null) ? "" : activeSheet.Range["Q262"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_CAPTOT = (activeSheet.Range["Q263"].Value2 == null) ? "" : activeSheet.Range["Q263"].Value2;
                    sd.SiteWorkOrdersTotalPlanned_TTLTOT = (activeSheet.Range["Q264"].Value2 == null) ? "" : activeSheet.Range["Q264"].Value2;
                    sd.SiteWorkOrdersTotalSched_COTOT = (activeSheet.Range["Q266"].Value2 == null) ? "" : activeSheet.Range["Q266"].Value2;
                    sd.SiteWorkOrdersTotalSched_PROTOT = (activeSheet.Range["Q267"].Value2 == null) ? "" : activeSheet.Range["Q267"].Value2;
                    sd.SiteWorkOrdersTotalSched_PRETOT = (activeSheet.Range["Q268"].Value2 == null) ? "" : activeSheet.Range["Q268"].Value2;
                    sd.SiteWorkOrdersTotalSched_CAPTOT = (activeSheet.Range["Q269"].Value2 == null) ? "" : activeSheet.Range["Q269"].Value2;
                    sd.SiteWorkOrdersTotalSched_TTLTOT = (activeSheet.Range["Q270"].Value2 == null) ? "" : activeSheet.Range["Q270"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_COTOT = (activeSheet.Range["Q272"].Value2 == null) ? "" : activeSheet.Range["Q272"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PROTOT = (activeSheet.Range["Q273"].Value2 == null) ? "" : activeSheet.Range["Q273"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_PRETOT = (activeSheet.Range["Q274"].Value2 == null) ? "" : activeSheet.Range["Q274"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_CAPTOT = (activeSheet.Range["Q275"].Value2 == null) ? "" : activeSheet.Range["Q275"].Value2;
                    sd.SiteWorkOrdersTotalSchedCmpl_TTLTOT = (activeSheet.Range["Q276"].Value2 == null) ? "" : activeSheet.Range["Q276"].Value2;
                    sd.SiteWorkOrdersEmergencyOrders_Tot = (activeSheet.Range["Q278"].Value2 == null) ? "" : activeSheet.Range["Q278"].Value2;




                    new System.Xml.Serialization.XmlSerializer(typeof(SiteData)).Serialize(xmlWriter, sd);

                    xmlWriter.Flush();
                    stream.Position = 0;
                    xmldoc.Load(stream);
                }
            }
            return xmldoc.OuterXml;
        }
        #endregion

        #region Add Site
        public int addSite(int CompanyDatasetID, int Sheet)
        {
            Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[Sheet];
            MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();
            return excelSvc.addSite(CompanyDatasetID, activeSheet.Range["M9"].Value2, activeSheet.Range["M10"].Value2, activeSheet.Range["M39"].Value2, (float)(activeSheet.Range["M40"].Value2), 710, "brian.franze/04TEST11");
        }
        #endregion

        #region Add Unit
        public int addUnit(int SiteID, int Sheet)
        {
            //CREATE UNIT
            Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[Sheet];
            string GenChemName = activeSheet.Range["M13"].Value2;
            string ProcessFamilyShort = getProcessFamily(GenChemName);
            string GenericProductNameShort = getProductGenericName(GenChemName, activeSheet.Range["M14"].Value2);
            MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();
            return excelSvc.addUnit(SiteID, activeSheet.Range["M9"].Value2, ProcessFamilyShort, GenericProductNameShort, activeSheet.Range["M10"].Value2, 710, "brian.franze/04TEST11");
        }
        #endregion

        #region Upload Glossary
        public void uploadGlossary(int Sheet)
        {
            XmlDocument xmldoc = new XmlDocument();
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[Sheet];

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                XmlWriter xmlWriter = XmlWriter.Create(stream, settings);
                using (xmlWriter)
                {
                    xmlWriter.WriteStartDocument();

                    Excel.Range usedRange = activeSheet.UsedRange;
                    bool isUsed = (usedRange.Count > 1);
                    if (usedRange.Count == 1)
                    {
                        isUsed = (usedRange.Value2 != null) && (!string.IsNullOrEmpty(usedRange.Value2.ToString()));
                    }

                    if (isUsed)
                    {
                        // worksheet cells not empty
                        string abc = "";
                    }
                }
            }
        }
        #endregion

        #region Get Process Family
        public string getProcessFamily(string PFamilyLong)
        {
            string PFamilyShort = string.Empty;
            switch (PFamilyLong)
            {
                case "Primary_Olefins": PFamilyShort = "PM_OLE"; break;
                case "Intermediates_from_Olefins": PFamilyShort = "INT_OLE"; break;
                case "Primary_Aromatics": PFamilyShort = "PM_ARM"; break;
                case "Intermediates_from_Aromatics": PFamilyShort = "INT_ARM"; break;
                case "Chlorinated_Hydrocarbons": PFamilyShort = "Cl_HC"; break;
                case "Chlor_Alkali_Products": PFamilyShort = "Cl_ALK"; break;
                case "Commodity_Thermoplastics": PFamilyShort = "C_PLAST"; break;
                case "Engineering_Thermoplastics": PFamilyShort = "E_PLAST"; break;
                case "Elastomers_And_Rubbers": PFamilyShort = "ELASM"; break;
                case "Thermosets": PFamilyShort = "THMSETS"; break;
                case "Fibers_Films_and_Fabrics": PFamilyShort = "FFF"; break;
                case "Industrial_Gases": PFamilyShort = "IND_GAS"; break;
                case "Liquid_Inorganics": PFamilyShort = "LIQ_IORG"; break;
                case "Solid_Inorganics": PFamilyShort = "S_IORG"; break;
                case "Solid_Organics": PFamilyShort = "S_ORG"; break;
                case "Pharmaceuticals_Active_Ingredient": PFamilyShort = "PHARM_ACT"; break;
                case "Pharmaceuticals_Formulation": PFamilyShort = "PHARM_FORM"; break;
                case "Agricultural_Active_Ingredient": PFamilyShort = "AGR_ACT"; break;
                case "Agricultural_Formulation": PFamilyShort = "AGR_FORM"; break;
                case "Adhesives_Paints_and_Coatings": PFamilyShort = "COAT"; break;
                case "Other Chemical Process": PFamilyShort = "OthChem"; break;
                case "Alkylation Unit": PFamilyShort = "ALKY"; break;
                case "Atmospheric Crude Distillation Unit": PFamilyShort = "CDU"; break;
                case "Catalytic Reforming Unit": PFamilyShort = "REF"; break;
                case "Coking Unit": PFamilyShort = "COK"; break;
                case "Cracking Feed or Vacuum Gas Oil Desulfurization": PFamilyShort = "VHYT"; break;
                case "Distillate/Light Gas Oil Desulfurization and Treating": PFamilyShort = "DHYT"; break;
                case "Fluid Catalytic Cracking Unit": PFamilyShort = "FCC"; break;
                case "Hydrocracking Unit": PFamilyShort = "HYC"; break;
                case "Hydrogen Generation Unit": PFamilyShort = "HYG"; break;
                case "Hydrogen Purification Unit": PFamilyShort = "H2PURE"; break;
                case "Kerosene Desulfurization and Treating": PFamilyShort = "KHYT"; break;
                case "Naphtha/Gasoline Desulfurization and Treating": PFamilyShort = "NHYT"; break;
                case "Other Refining Process Unit- please provide a description": PFamilyShort = "OTH"; break;
                case "Residual Desulfurization": PFamilyShort = "RHYT"; break;
                case "Selective Hydrotreating": PFamilyShort = "SHYT"; break;
                case "Sulfur Recovery Unit": PFamilyShort = "SRU"; break;
                case "Tail Gas Recovery Unit": PFamilyShort = "TRU"; break;
                case "Vacuum Crude Distillation Unit": PFamilyShort = "VAC"; break;
                case "Other Refining Process": PFamilyShort = "OthRef"; break;
                case "Utilities": PFamilyShort = "Utilities"; break;
            }
            return PFamilyShort;
        }
        #endregion

        #region Get Product Generic Name
        public string getProductGenericName(string PFamilyLong, string PGenericNameLong)
        {
            string PGenericNameShort = string.Empty;
            switch (PFamilyLong)
            {
                case "Utilities":
                    switch (PGenericNameLong)
                    {
                        case "Electrical Generation": PGenericNameShort = "Electrical-Gen"; break;
                        case "Electrical Generation - Cogeneration": PGenericNameShort = "Electrical-Gen-Cogen"; break;
                        case "Steam Boilers": PGenericNameShort = "Steam-Boilers"; break;
                        case "Cooling Water": PGenericNameShort = "Cooling-Water"; break;
                        case "Waste Water Treatment": PGenericNameShort = "Waste-Water-Treatment"; break;
                        case "Others (Combinations)": PGenericNameShort = "Others-Combinations"; break;
                    }
                    break;
                case "Primary_Olefins":
                    switch (PGenericNameLong)
                    {
                        case "Acetylene": PGenericNameShort = "Acetylene"; break;
                        case "Benzene": PGenericNameShort = "Benzene-Ole"; break;
                        case "Butadiene": PGenericNameShort = "Butadiene"; break;
                        case "Butanes": PGenericNameShort = "Butanes"; break;
                        case "Butenes": PGenericNameShort = "Butenes"; break;
                        case "Ethylene": PGenericNameShort = "Ethylene"; break;
                        case "Hydrogen via Pyrolysis": PGenericNameShort = "H2-Pyrolysis"; break;
                        case "Methane": PGenericNameShort = "Methane"; break;
                        case "Mixed C4s": PGenericNameShort = "Mixed C4s"; break;
                        case "Mixed C5s": PGenericNameShort = "Mixed C5s"; break;
                        case "Propylene": PGenericNameShort = "Propylene"; break;
                        case "Pyrolysis Gasoline": PGenericNameShort = "PyGas"; break;
                        case "Pyrolysis Naphtha/Gas Oil": PGenericNameShort = "PyNap"; break;
                        case "Toluene": PGenericNameShort = "Toluene-OLE"; break;
                        case "Xylenes": PGenericNameShort = "Xylenes-OLE"; break;
                    }
                    break;
                case "Intermediates_from_Olefins":
                    switch (PGenericNameLong)
                    {
                        case "Acetaldehyde": PGenericNameShort = "Acetaldehyde"; break;
                        case "Acetate Esters": PGenericNameShort = "Acetate Esters"; break;
                        case "Acetates": PGenericNameShort = "Acetates"; break;
                        case "Acetic Acid": PGenericNameShort = "Acetic Acid"; break;
                        case "Acetone": PGenericNameShort = "Acetone"; break;
                        case "Acrylates": PGenericNameShort = "Acrylates"; break;
                        case "Acrylic Acid (AA)": PGenericNameShort = "AA"; break;
                        case "Acrylic Aldehyde (Acrolein)": PGenericNameShort = "Acrolein"; break;
                        case "Acrylonitrile (AN)": PGenericNameShort = "Acrylonitrile"; break;
                        case "Adipic Acid": PGenericNameShort = "ADA-OLE"; break;
                        case "Adiponitrile (ADN) via C3/C4 Process": PGenericNameShort = "ADN-C3/C4"; break;
                        case "Aldehydes": PGenericNameShort = "Aldehydes"; break;
                        case "Amines": PGenericNameShort = "Amines-OLE"; break;
                        case "Butane Diol (BDO)": PGenericNameShort = "BDO"; break;
                        case "Butanol (BUOH)": PGenericNameShort = "BUOH"; break;
                        case "Diols": PGenericNameShort = "Diols"; break;
                        case "Esters": PGenericNameShort = "Esters"; break;
                        case "Ethanol": PGenericNameShort = "Ethanol"; break;
                        case "Ethers": PGenericNameShort = "Ethers"; break;
                        case "Ethoxylated Products": PGenericNameShort = "Ethoxylated Products"; break;
                        case "Ethoxylates": PGenericNameShort = "Ethoxylates"; break;
                        case "Ethyl Acetate (EA)": PGenericNameShort = "EA"; break;
                        case "Ethylene Glycol": PGenericNameShort = "EG"; break;
                        case "Ethylene Oxide (EO)": PGenericNameShort = "EO"; break;
                        case "Formaldehyde (Methanal)": PGenericNameShort = "Formaldehyde"; break;
                        case "Glycerin (Glycerol)": PGenericNameShort = "Glycerin"; break;
                        case "Glycol Ethers": PGenericNameShort = "Glycol Ethers"; break;
                        case "Hydrogen Cyanide (Formonitrile; Prussic Acid)": PGenericNameShort = "HCN"; break;
                        case "Isobutylene": PGenericNameShort = "Isobutylene"; break;
                        case "Isoprene": PGenericNameShort = "Isoprene"; break;
                        case "Isopropyl alcohol (IPA)": PGenericNameShort = "IPA"; break;
                        case "Ketones": PGenericNameShort = "Ketones"; break;
                        case "Maleic Anhydride (MA)": PGenericNameShort = "MA"; break;
                        case "Methanol (MEOH)": PGenericNameShort = "MEOH"; break;
                        case "Methyl ethyl ketone (MEK)": PGenericNameShort = "MEK"; break;
                        case "Methyl isobutyl ketone (MIBK)": PGenericNameShort = "MIBK"; break;
                        case "Methyl Tertiary Butyl Ether (MTBE)": PGenericNameShort = "MTBE"; break;
                        case "Organic Acids": PGenericNameShort = "Organic Acids"; break;
                        case "Other Alcohols": PGenericNameShort = "Other Alcohols"; break;
                        case "Piperylene": PGenericNameShort = "Piperylene"; break;
                        case "Polyethers": PGenericNameShort = "Polyethers"; break;
                        case "Polyol": PGenericNameShort = "Polyol"; break;
                        case "Propanol (PROH)": PGenericNameShort = "PROH"; break;
                        case "Propylene Glycol (PG)": PGenericNameShort = "PG"; break;
                        case "Propylene Oxide (PO)": PGenericNameShort = "PO"; break;
                        case "Raffinate": PGenericNameShort = "Raffinate"; break;
                        case "Recovered C4s or C5s": PGenericNameShort = "Recovered C4/C5"; break;
                        case "Tertiary Butyl Ether (TBA)": PGenericNameShort = "TBA"; break;
                        case "Vinyl Acetate (VA)": PGenericNameShort = "VA"; break;
                        case "Vinyl Acetate Monomer (VAM)": PGenericNameShort = "VAM"; break;
                    }
                    break;
                case "Primary_Aromatics":
                    switch (PGenericNameLong)
                    {
                        case "Benzene": PGenericNameShort = "Benzene-Arm"; break;
                        case "Cumene": PGenericNameShort = "Cumene-PriArm"; break;
                        case "Hydrogen via Reforming": PGenericNameShort = "H2-Reforming"; break;
                        case "Toluene": PGenericNameShort = "Toluene-ARM"; break;
                        case "Xylenes": PGenericNameShort = "Xylenes-ARM"; break;
                    }
                    break;
                case "Intermediates_from_Aromatics":
                    switch (PGenericNameLong)
                    {
                        case "Adipic Acid": PGenericNameShort = "ADA-ARM"; break;
                        case "Adiponitrile (ADN) via Adiptic Acid": PGenericNameShort = "ADN-ADA"; break;
                        case "Amines": PGenericNameShort = "Amines-ARM"; break;
                        case "Aniline": PGenericNameShort = "Aniline"; break;
                        case "Bisphenol A (BPA)": PGenericNameShort = "BPA"; break;
                        case "Caprolactam": PGenericNameShort = "Caprolactam"; break;
                        case "Cumene": PGenericNameShort = "Cumene-IntArm"; break;
                        case "Cyclohexane": PGenericNameShort = "Cyclohexane"; break;
                        case "Cyclohexanone": PGenericNameShort = "Cyclohexanone"; break;
                        case "Cyclohexanone Oxime": PGenericNameShort = "Cyclohexanone Oxime"; break;
                        case "Dimethyl Carbonate": PGenericNameShort = "DMC"; break;
                        case "Dimethyl Terephthalate (DMT)": PGenericNameShort = "DMT"; break;
                        case "Dinitrotoluene (DNT)": PGenericNameShort = "DNT"; break;
                        case "Dioctyl Phthalate (DOP or DHEP)": PGenericNameShort = "DOP"; break;
                        case "Diphenyl Carbonate": PGenericNameShort = "DPC"; break;
                        case "Ethylbenzene (EB)": PGenericNameShort = "EB"; break;
                        case "Hydroquinone": PGenericNameShort = "Hydroquinone"; break;
                        case "Isophthalic Acid": PGenericNameShort = "IP Acid"; break;
                        case "Methylene Diphenyl Diisocyanate (MDI)": PGenericNameShort = "MDI"; break;
                        case "Nitrotoluene (or Methylnitrobenzene)": PGenericNameShort = "Nitrotoluene"; break;
                        case "Nylon 6": PGenericNameShort = "Nylon6FromArom"; break;
                        case "Other Aromatic Amines": PGenericNameShort = "Other AA"; break;
                        case "Phenol": PGenericNameShort = "Phenol"; break;
                        case "Phenol-Acetone": PGenericNameShort = "Phenol-Acetone"; break;
                        case "Phthalic Anhydride": PGenericNameShort = "Phthalic Anhydride"; break;
                        case "Styrene": PGenericNameShort = "Styrene"; break;
                        case "Terephthalic Acid (TPA)": PGenericNameShort = "TPA"; break;
                        case "Toluene Diisocyanate (TDI)": PGenericNameShort = "TDI"; break;
                        case "Trinitrotoluene (TNT)": PGenericNameShort = "TNT"; break;
                    }
                    break;
                case "Chlorinated_Hydrocarbons":
                    switch (PGenericNameLong)
                    {
                        case "Allyl Bromine": PGenericNameShort = "Allyl Bromine"; break;
                        case "Allyl Chloride": PGenericNameShort = "Allyl Chloride"; break;
                        case "Chlorobenzene": PGenericNameShort = "Chlorobenzene"; break;
                        case "Chloroethane": PGenericNameShort = "Chloroethane"; break;
                        case "Chlorofluorocarbon (CFC)": PGenericNameShort = "CFC"; break;
                        case "Chloromethane": PGenericNameShort = "Chloromethane"; break;
                        case "Chloropropylene": PGenericNameShort = "Chloropropylene"; break;
                        case "Ethylene Chloride": PGenericNameShort = "EC"; break;
                        case "Ethylene Dichloride (EDC)": PGenericNameShort = "EDC"; break;
                        case "Tetrachloroethylene": PGenericNameShort = "TCE"; break;
                        case "Trichloromethane (Chloroform)": PGenericNameShort = "Chloroform"; break;
                        case "Vinyl Chloride Monomer (VCM)": PGenericNameShort = "VCM"; break;
                    }
                    break;
                case "Chlor_Alkali_Products":
                    switch (PGenericNameLong)
                    {
                        case "Cell Effluent": PGenericNameShort = "Cell Effluent"; break;
                        case "Chlor-Alkali by-products": PGenericNameShort = "Chlor-Alkali by-prod"; break;
                        case "Chlorine": PGenericNameShort = "Chlorine"; break;
                        case "Hydrogen via Electrolysis": PGenericNameShort = "H2-Electrolysis"; break;
                    }
                    break;
                case "Commodity_Thermoplastics":
                    switch (PGenericNameLong)
                    {
                        case "Acrylic": PGenericNameShort = "Acrylic-Plast"; break;
                        case "Acrylonitrile butadiene styrene (ABS)": PGenericNameShort = "ABS"; break;
                        case "Biopolymers (PLA Polyethylene via Biomass)": PGenericNameShort = "Biopolymers"; break;
                        case "Expandable Polystyrene (EPS)": PGenericNameShort = "EPS"; break;
                        case "High Density Polyethylene (HDPE)": PGenericNameShort = "HDPE"; break;
                        case "High Impact Polystyrene (HIPS)": PGenericNameShort = "HIPS"; break;
                        case "Linear Low Density Polyethylene (LLDPE)": PGenericNameShort = "LLDPE"; break;
                        case "Low Density Polyethylene (LDPE)": PGenericNameShort = "LDPE"; break;
                        case "Polycaprolactone (PCL)": PGenericNameShort = "PCL"; break;
                        case "Polyethylene (PE)": PGenericNameShort = "PE"; break;
                        case "Polyethylene terephthalate (PET)": PGenericNameShort = "PET"; break;
                        case "Polymethyl methacrylate (PMMA)": PGenericNameShort = "PMMA"; break;
                        case "Polypropylene (PP)": PGenericNameShort = "PP"; break;
                        case "Polystyrene (PS)": PGenericNameShort = "PS"; break;
                        case "Polyvinyl Alcohol (PVA; PVOH)": PGenericNameShort = "PVA"; break;
                        case "Polyvinyl Chloride (PVC)": PGenericNameShort = "PVC"; break;
                        case "Superabsorbent Polymers": PGenericNameShort = "SAP"; break;
                    }
                    break;
                case "Engineering_Thermoplastics":
                    switch (PGenericNameLong)
                    {
                        case "Acetal Homopolymer or Copolymer": PGenericNameShort = "POM-H"; break;
                        case "Cellulosics- Acetate, Butyrate, or Propionate": PGenericNameShort = "Cellulosics"; break;
                        case "Liquid Crystal Polymers": PGenericNameShort = "LCP"; break;
                        case "Nylon 6, Nylon 6/6": PGenericNameShort = "Nylon 6"; break;
                        case "Other Fluoropolymers (ECTFE, PVDF)": PGenericNameShort = "Other FP"; break;
                        case "Polyamide": PGenericNameShort = "Polyamide"; break;
                        case "Polyarylate": PGenericNameShort = "Polyarylate"; break;
                        case "Polybutylene Terephthalate (PBT)": PGenericNameShort = "PBT"; break;
                        case "Polycarbonate (PC)": PGenericNameShort = "PC"; break;
                        case "Polyester": PGenericNameShort = "Polyester-Plast"; break;
                        case "Polyetherketone (PEEK)": PGenericNameShort = "PEEK"; break;
                        case "Polyetheylene terephthalate (PET) Composite": PGenericNameShort = "PET Composite"; break;
                        case "Polyimide/ Polyetherimide": PGenericNameShort = "Polyimide"; break;
                        case "Polymethylpentene (PMP, TPX)": PGenericNameShort = "PMP"; break;
                        case "Polyphenyl Ether (PPE)": PGenericNameShort = "PPE"; break;
                        case "Polyphenylene Oxide (PPO)": PGenericNameShort = "PPO"; break;
                        case "Polyphenylene Sulfide (PPS)": PGenericNameShort = "PPS"; break;
                        case "Polyphthalamide (PPA)": PGenericNameShort = "PPA"; break;
                        case "Polysulfone (PSU)": PGenericNameShort = "PSU"; break;
                        case "Polytetrafluoroethylene (PTFE)/ Teflon®": PGenericNameShort = "PTFE"; break;
                        case "Polytrimethylene terephthalate (PTT)": PGenericNameShort = "PTT"; break;
                        case "Polyurethane (PU)": PGenericNameShort = "PUR-Plast"; break;
                        case "Styrene maleic anhydride (SMA)": PGenericNameShort = "SMA"; break;
                        case "Styrene-acrylonitrile resin (SAN)": PGenericNameShort = "SAN"; break;
                        case "Ultra-high-molecular-weight Polyethylene (UHMW PE; HPPE)": PGenericNameShort = "UHMW PE"; break;
                    }
                    break;
                case "Elastomers_And_Rubbers":
                    switch (PGenericNameLong)
                    {
                        case "Butyl Rubber (PIB)": PGenericNameShort = "PIB"; break;
                        case "Elastomeric Alloys (TPE-v or TPV)": PGenericNameShort = "TPE-v"; break;
                        case "Engineering thermoplastic vulcanizates (ETPV)": PGenericNameShort = "ETPV"; break;
                        case "Ethylene propylene diene monomer (EPDM)": PGenericNameShort = "EPDM"; break;
                        case "Ethylene-Propylene Rubber (EP)": PGenericNameShort = "EP"; break;
                        case "Ethylene-vinyl acetate (EVA)": PGenericNameShort = "EVA"; break;
                        case "Fluoroelastomer (FKM and FPEM)": PGenericNameShort = "Fluoroelastomer"; break;
                        case "Neoprene polychloroprene": PGenericNameShort = "CR"; break;
                        case "Nitrile Butadiene Rubber (NBR)": PGenericNameShort = "NBR"; break;
                        case "Polybutadiene (BR; Butadiene Rubber)": PGenericNameShort = "Polybutadiene"; break;
                        case "Polyolefin Blends": PGenericNameShort = "Polyolefin Blends"; break;
                        case "Polyurethanes": PGenericNameShort = "PUR-Elasm"; break;
                        case "Rubber (natural or synthetic polyisoprene)": PGenericNameShort = "Rubber"; break;
                        case "Silicone/Fluorosilicone Rubber": PGenericNameShort = "Si Rubber"; break;
                        case "Styrene-Butadiene Rubber (SBR)": PGenericNameShort = "SBR"; break;
                        case "Styrenic Block Copolymers (SBS, SIS)": PGenericNameShort = "SBS"; break;
                        case "Thermoplastic copolyester": PGenericNameShort = "TPCP"; break;
                        case "Thermoplastic polyamides": PGenericNameShort = "TPPA"; break;
                        case "Vulcanized Rubber": PGenericNameShort = "Vulc Rubber"; break;
                    }
                    break;
                case "Thermosets":
                    switch (PGenericNameLong)
                    {
                        case "Bakelite": PGenericNameShort = "Bakelite"; break;
                        case "Cyanate Esters": PGenericNameShort = "Cyanate Esters"; break;
                        case "Epoxy resin": PGenericNameShort = "Epoxy resin"; break;
                        case "Melamine Molding Compound": PGenericNameShort = "MMC"; break;
                        case "Phenolic": PGenericNameShort = "Phenolic"; break;
                        case "Polyester Unsaturated": PGenericNameShort = "Polyester-Unsat"; break;
                        case "Polyurethane Isocyanates": PGenericNameShort = "PUR Isocyanates"; break;
                        case "Urea Molding Compound": PGenericNameShort = "Urea Molding"; break;
                        case "Vinyl Ester": PGenericNameShort = "Vinyl Ester"; break;
                    }
                    break;
                case "Fibers_Films_and_Fabrics":
                    switch (PGenericNameLong)
                    {
                        case "Cellulose Acetate Films or Fibers": PGenericNameShort = "CAF"; break;
                        case "Fabric/ Yarn": PGenericNameShort = "Fabric"; break;
                        case "Films- Excruded, Blown, or Molded": PGenericNameShort = "Films"; break;
                        case "Housewrap (Tyvek®, WeathermateTM)": PGenericNameShort = "Housewrap"; break;
                        case "Latex": PGenericNameShort = "Latex"; break;
                        case "Non-wovens": PGenericNameShort = "Non-wovens"; break;
                        case "Synthetic Fibers- Acrylic": PGenericNameShort = "Acrylic-Fiber"; break;
                        case "Synthetic Fibers- Nylon": PGenericNameShort = "Nylon"; break;
                        case "Synthetic Fibers- Polyester": PGenericNameShort = "Polyester-Fiber"; break;
                        case "Synthetic Fibers- Polyolefin": PGenericNameShort = "Polyolefin"; break;
                    }
                    break;
                case "Industrial_Gases":
                    switch (PGenericNameLong)
                    {
                        case "Air": PGenericNameShort = "Air"; break;
                        case "Air Gases (N2, O2)": PGenericNameShort = "Air Gases"; break;
                        case "Ammonia (NH3)": PGenericNameShort = "NH3"; break;
                        case "Carbon Dioxide (CO2)": PGenericNameShort = "CO2"; break;
                        case "Carbon Monoxide (CO)": PGenericNameShort = "CO"; break;
                        case "Halogen Elements (Cl2, F2)": PGenericNameShort = "Halogen Elements"; break;
                        case "Hydrocarbon Gases": PGenericNameShort = "HC Gases"; break;
                        case "Hydrogen (H2)": PGenericNameShort = "H2-IND"; break;
                        case "Hydrogen Chloride (HCl)": PGenericNameShort = "HCl-IND"; break;
                        case "Hydrogen Sulfide (H2S)": PGenericNameShort = "H2S"; break;
                        case "Nitrous Oxide (N2O)": PGenericNameShort = "N2O"; break;
                        case "Noble Gases (He, Ar, Kr, Ne, Xe)": PGenericNameShort = "Noble Gases"; break;
                    }
                    break;
                case "Liquid_Inorganics":
                    switch (PGenericNameLong)
                    {
                        case "Hydrochloric Acid": PGenericNameShort = "HCl-LiqInorg"; break;
                        case "Hydroxylamine (Liquid)": PGenericNameShort = "HydroxylamineL"; break;
                        case "Liquid Ammonia": PGenericNameShort = "NH3-Liq"; break;
                        case "Liquid Sulfur Dioxide": PGenericNameShort = "SO2-Liq"; break;
                        case "Nitric Acid": PGenericNameShort = "Nitric Acid"; break;
                        case "Sulfuric Acid": PGenericNameShort = "Sulfuric Acid"; break;
                        case "Sulfuryl Chloride": PGenericNameShort = "Sulfuryl Chloride"; break;
                    }
                    break;
                case "Solid_Inorganics":
                    switch (PGenericNameLong)
                    {
                        case "Ammonium Nitrate": PGenericNameShort = "AN"; break;
                        case "Boric Acid": PGenericNameShort = "Boric Acid"; break;
                        case "Calcium Carbonate": PGenericNameShort = "CaCO3"; break;
                        case "Carbon Black (PBk7)": PGenericNameShort = "Carbon Black"; break;
                        case "Catalyst (Zeolites)": PGenericNameShort = "Catalyst"; break;
                        case "Caustic (NaOH; Lye)": PGenericNameShort = "Caustic"; break;
                        case "Hydroxylamine (Solid)": PGenericNameShort = "HydroxylamineS"; break;
                        case "Iodine": PGenericNameShort = "Iodine"; break;
                        case "Metals and Alloys": PGenericNameShort = "Metals"; break;
                        case "Phosphates": PGenericNameShort = "Phosphates"; break;
                        case "Potasium Nitrate": PGenericNameShort = "KNO3"; break;
                        case "Salt (NaCl)": PGenericNameShort = "Salt"; break;
                        case "Solid Explosives": PGenericNameShort = "Solid Explosives"; break;
                        case "Sulfur": PGenericNameShort = "Sulfur"; break;
                        case "Titanium Dioxide (TiO2)": PGenericNameShort = "TiO2"; break;
                    }
                    break;
                case "Solid_Organics":
                    switch (PGenericNameLong)
                    {
                        case "Cardboard/Paper": PGenericNameShort = "Paper"; break;
                        case "Cellulose Esters": PGenericNameShort = "Cellulose Esters"; break;
                        case "Food": PGenericNameShort = "Food"; break;
                        case "Paraffin Wax": PGenericNameShort = "Paraffin Wax"; break;
                        case "Urea (or Carbamide)": PGenericNameShort = "Urea"; break;
                    }
                    break;
                case "Pharmaceuticals_Active_Ingredient":
                    switch (PGenericNameLong)
                    {
                        case "Pharmaceuticals": PGenericNameShort = "Pharm-Form"; break;
                    }
                    break;
                case "Pharmaceuticals_Formulation":
                    switch (PGenericNameLong)
                    {
                        case "	Pharmaceuticals	": PGenericNameShort = "	Pharm-Form	"; break;
                    }
                    break;
                case "Agricultural_Active_Ingredient":
                    switch (PGenericNameLong)
                    {
                        case "Pesticides (herbicides, insecticides, fungicides)": PGenericNameShort = "Pesticides-Act"; break;
                        case "Synthetic fertilizers, harmones, and other growth agents": PGenericNameShort = "SynFertilizer-Act"; break;
                    }
                    break;
                case "Agricultural_Formulation":
                    switch (PGenericNameLong)
                    {
                        case "Pesticides (herbicides, insecticides, fungicides)": PGenericNameShort = "Pesticides-Form"; break;
                        case "Synthetic fertilizers, harmones, and other growth agents": PGenericNameShort = "SynFertilizer-Form"; break;
                    }
                    break;
                case "Adhesives_Paints_and_Coatings":
                    switch (PGenericNameLong)
                    {
                        case "Adhesive/Glue": PGenericNameShort = "Adhesive"; break;
                        case "Caulk": PGenericNameShort = "Caulk"; break;
                        case "Latex (emulsion)": PGenericNameShort = "Latex-Emulsion"; break;
                        case "Paint": PGenericNameShort = "Paint "; break;
                        case "Sealant": PGenericNameShort = "Sealant"; break;
                        case "Surfactant/Detergent": PGenericNameShort = "Surfactant"; break;
                        case "Varnish": PGenericNameShort = "Varnish"; break;
                    }
                    break;
            }

            return PGenericNameShort;
        }
        #endregion

        #region Create Unit Data XML String        
        private string CreateXMLString(int Sheet)
        {
            XmlDocument xmldoc = new XmlDocument();
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[Sheet];

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                XmlWriter xmlWriter = XmlWriter.Create(stream, settings);                
                using (xmlWriter)
                {
                    xmlWriter.WriteStartDocument();

                    WorksheetData wsd = new WorksheetData();
                    wsd.SheetName = "RAM2012";
                    wsd.Username = username;
                    wsd.Password = password;

                    wsd.UnitName = (activeSheet.Range["M9"].Value2 == null) ? "" : activeSheet.Range["M9"].Value2;
                    wsd.UnitInfoPrimaryProductName = (activeSheet.Range["M10"].Value2 == null) ? "" : activeSheet.Range["M10"].Value2;

                    wsd.UnitSafetyCurrInjuryCntComp_RT = (activeSheet.Range["M17"].Value2 == null) ? "" : activeSheet.Range["M17"].Value2;
                    wsd.UnitSafetyCurrInjuryCntCont_RT = (activeSheet.Range["M18"].Value2 == null) ? "" : activeSheet.Range["M18"].Value2;
                    wsd.UnitSafetyCurrInjuryCntTot_RT = (activeSheet.Range["M19"].Value2 == null) ? "" : activeSheet.Range["M19"].Value2;

                    wsd.UnitInfoPlantAge = (activeSheet.Range["M21"].Value2 == null) ? "" : activeSheet.Range["M21"].Value2;
                    wsd.UnitInfoPRV = (activeSheet.Range["M23"].Value2 == null) ? "" : activeSheet.Range["M23"].Value2;
                    wsd.UnitInfoPRVUSD = (activeSheet.Range["N23"].Value2 == null) ? "" : activeSheet.Range["N23"].Value2;
                    wsd.UnitInfoMaxDailyProdRate = (activeSheet.Range["M25"].Value2 == null) ? "" : activeSheet.Range["M25"].Value2;
                    wsd.UnitInfoProdRateUOM = (activeSheet.Range["N25"].Value2 == null) ? "" : activeSheet.Range["N25"].Value2;
                    wsd.UnitInfoContinuosOrBatch = (activeSheet.Range["M26"].Value2 == null) ? "" : activeSheet.Range["M26"].Value2;

                    wsd.UnitInfoTrainCnt = (activeSheet.Range["M27"].Value2 == null) ? "" : activeSheet.Range["M27"].Value2;

                    wsd.UnitInfoPhasePredominentFeedstock = (activeSheet.Range["M30"].Value2 == null) ? "" : activeSheet.Range["M30"].Value2;
                    wsd.UnitInfoPhasePredominentProduct = (activeSheet.Range["M31"].Value2 == null) ? "" : activeSheet.Range["M31"].Value2;
                    wsd.SeverityToxicity = (activeSheet.Range["M33"].Value2 == null) ? "" : activeSheet.Range["M33"].Value2;
                    wsd.SeverityPressure = (activeSheet.Range["M34"].Value2 == null) ? "" : activeSheet.Range["M34"].Value2;
                    wsd.SeverityTemperature = (activeSheet.Range["M35"].Value2 == null) ? "" : activeSheet.Range["M35"].Value2;
                    wsd.SeverityFlammability = (activeSheet.Range["M36"].Value2 == null) ? "" : activeSheet.Range["M36"].Value2;
                    wsd.SeverityExplosivity = (activeSheet.Range["M37"].Value2 == null) ? "" : activeSheet.Range["M37"].Value2;
                    wsd.SeverityCorrosivity = (activeSheet.Range["M38"].Value2 == null) ? "" : activeSheet.Range["M38"].Value2;
                    wsd.SeverityErosivity = (activeSheet.Range["M39"].Value2 == null) ? "" : activeSheet.Range["M39"].Value2;
                    wsd.SeverityProcessComplexity = (activeSheet.Range["M40"].Value2 == null) ? "" : activeSheet.Range["M40"].Value2;
                    wsd.SeverityViscosity = (activeSheet.Range["M41"].Value2 == null) ? "" : activeSheet.Range["M41"].Value2;
                    wsd.SeverityFreezePt = (activeSheet.Range["M42"].Value2 == null) ? "" : activeSheet.Range["M42"].Value2;
                    wsd.SeverityConstMaterials = (activeSheet.Range["M43"].Value2 == null) ? "" : activeSheet.Range["M43"].Value2;
                    wsd.EquipCntTurbines = (activeSheet.Range["M51"].Value2 == null) ? "" : activeSheet.Range["M51"].Value2;
                    wsd.EquipCntTurbinesSpares = (activeSheet.Range["M52"].Value2 == null) ? "" : activeSheet.Range["M52"].Value2;
                    wsd.EquipCntCompressorsRecip = (activeSheet.Range["M53"].Value2 == null) ? "" : activeSheet.Range["M53"].Value2;
                    wsd.EquipCntCompressorsRecipSpares = (activeSheet.Range["M54"].Value2 == null) ? "" : activeSheet.Range["M54"].Value2;
                    wsd.EquipCntCompressorsRotating = (activeSheet.Range["M55"].Value2 == null) ? "" : activeSheet.Range["M55"].Value2;
                    wsd.EquipCntCompressorsRotatingSpares = (activeSheet.Range["M56"].Value2 == null) ? "" : activeSheet.Range["M56"].Value2;
                    wsd.EquipCntPumpsCentrifugal = (activeSheet.Range["M57"].Value2 == null) ? "" : activeSheet.Range["M57"].Value2;
                    wsd.EquipCntPumpsCentrifugalSpares = (activeSheet.Range["M58"].Value2 == null) ? "" : activeSheet.Range["M58"].Value2;
                    wsd.EquipCntPumpsPosDisp = (activeSheet.Range["M59"].Value2 == null) ? "" : activeSheet.Range["M59"].Value2;
                    wsd.EquipCntBlowersFans = (activeSheet.Range["M60"].Value2 == null) ? "" : activeSheet.Range["M60"].Value2;
                    wsd.EquipCntRotaryDryers = (activeSheet.Range["M61"].Value2 == null) ? "" : activeSheet.Range["M61"].Value2;
                    wsd.EquipCntCrushers = (activeSheet.Range["M62"].Value2 == null) ? "" : activeSheet.Range["M62"].Value2;
                    wsd.EquipCntCentrifugesMain = (activeSheet.Range["M63"].Value2 == null) ? "" : activeSheet.Range["M63"].Value2;
                    wsd.EquipCntAgitatorsInVessels = (activeSheet.Range["M64"].Value2 == null) ? "" : activeSheet.Range["M64"].Value2;
                    wsd.EquipCntFurnaceBoilers = (activeSheet.Range["M68"].Value2 == null) ? "" : activeSheet.Range["M68"].Value2;
                    wsd.EquipCntVessels = (activeSheet.Range["M69"].Value2 == null) ? "" : activeSheet.Range["M69"].Value2;
                    wsd.EquipCntDistTowers = (activeSheet.Range["M70"].Value2 == null) ? "" : activeSheet.Range["M70"].Value2;
                    wsd.EquipCntCoolingTowers = (activeSheet.Range["M71"].Value2 == null) ? "" : activeSheet.Range["M71"].Value2;
                    wsd.EquipCntStorageTanksISBL = (activeSheet.Range["M72"].Value2 == null) ? "" : activeSheet.Range["M72"].Value2;
                    wsd.EquipCntHeatExch = (activeSheet.Range["M73"].Value2 == null) ? "" : activeSheet.Range["M73"].Value2;
                    wsd.EquipCntHeatExchSpares = (activeSheet.Range["M74"].Value2 == null) ? "" : activeSheet.Range["M74"].Value2;
                    wsd.EquipCntHeatExchOth = (activeSheet.Range["M75"].Value2 == null) ? "" : activeSheet.Range["M75"].Value2;
                    wsd.EquipCntHeatExchFinFan = (activeSheet.Range["M76"].Value2 == null) ? "" : activeSheet.Range["M76"].Value2;
                    wsd.EquipCntRefrigUnits = (activeSheet.Range["M77"].Value2 == null) ? "" : activeSheet.Range["M77"].Value2;
                    wsd.EquipCntSilosISBL = (activeSheet.Range["M78"].Value2 == null) ? "" : activeSheet.Range["M78"].Value2;
                    wsd.EquipCntSafetyValves = (activeSheet.Range["M79"].Value2 == null) ? "" : activeSheet.Range["M79"].Value2;
                    wsd.EquipCntMotorsMain = (activeSheet.Range["M83"].Value2 == null) ? "" : activeSheet.Range["M83"].Value2;
                    wsd.EquipCntVarSpeedDrives = (activeSheet.Range["M84"].Value2 == null) ? "" : activeSheet.Range["M84"].Value2;
                    wsd.EquipCntISBLSubStationTransformer = (activeSheet.Range["M85"].Value2 == null) ? "" : activeSheet.Range["M85"].Value2;
                    wsd.EquipCntDistVoltage = (activeSheet.Range["M86"].Value2 == null) ? "" : activeSheet.Range["M86"].Value2;
                    wsd.EquipCntControlValves = (activeSheet.Range["M87"].Value2 == null) ? "" : activeSheet.Range["M87"].Value2;
                    wsd.EquipCntAnalyzerProcCtrl = (activeSheet.Range["M88"].Value2 == null) ? "" : activeSheet.Range["M88"].Value2;
                    wsd.EquipCntAnalyzerBlending = (activeSheet.Range["M89"].Value2 == null) ? "" : activeSheet.Range["M89"].Value2;
                    wsd.EquipCntAnalyzerEmissions = (activeSheet.Range["M90"].Value2 == null) ? "" : activeSheet.Range["M90"].Value2;
                    wsd.TAAvgHrsShutdown = (activeSheet.Range["M98"].Value2 == null) ? "" : activeSheet.Range["M98"].Value2;
                    wsd.TAAvgHrsExecution = (activeSheet.Range["M99"].Value2 == null) ? "" : activeSheet.Range["M99"].Value2;
                    wsd.TAAvgHrsStartup = (activeSheet.Range["M100"].Value2 == null) ? "" : activeSheet.Range["M100"].Value2;
                    wsd.TAAvgHrsDown = (activeSheet.Range["M101"].Value2 == null) ? "" : activeSheet.Range["M101"].Value2;

                    wsd.TAAvgPlanHrsDown = (activeSheet.Range["M102"].Value2 == null) ? "" : activeSheet.Range["M102"].Value2;

                    wsd.TAAvgInterval = (activeSheet.Range["M103"].Value2 == null) ? "" : activeSheet.Range["M103"].Value2;
                    wsd.TAAvgAnnHrsDown = (activeSheet.Range["M104"].Value2 == null) ? "" : activeSheet.Range["M104"].Value2;
                    wsd.ShortOHAvgHrsDown = (activeSheet.Range["M109"].Value2 == null) ? "" : activeSheet.Range["M109"].Value2;
                    wsd.ShortOH2YrCnt = (activeSheet.Range["M110"].Value2 == null) ? "" : activeSheet.Range["M110"].Value2;
                    wsd.ShortOHAnnHrsDown = (activeSheet.Range["M111"].Value2 == null) ? "" : activeSheet.Range["M111"].Value2;
                    wsd.LossHrsForAnnTA = (activeSheet.Range["M116"].Value2 == null) ? "" : activeSheet.Range["M1116"].Value2;
                    wsd.LossHrsForShortOH = (activeSheet.Range["M117"].Value2 == null) ? "" : activeSheet.Range["M117"].Value2;
                    wsd.LossHrsForUnschedMaint = (activeSheet.Range["M118"].Value2 == null) ? "" : activeSheet.Range["M118"].Value2;
                    wsd.LossHrsForProcessReliability = (activeSheet.Range["M120"].Value2 == null) ? "" : activeSheet.Range["M120"].Value2;
                    wsd.LossHrsForTotalProdLoss = (activeSheet.Range["M121"].Value2 == null) ? "" : activeSheet.Range["M121"].Value2;
                    wsd.LostProductionOutages = (activeSheet.Range["M126"].Value2 == null) ? "" : activeSheet.Range["M126"].Value2;
                    wsd.LostProductionRateReductions = (activeSheet.Range["M127"].Value2 == null) ? "" : activeSheet.Range["M127"].Value2;
                    wsd.NonMaintOutagesMaintEquivHours = (activeSheet.Range["M132"].Value2 == null) ? "" : activeSheet.Range["M132"].Value2;
                    wsd.TACausePcnt_RE = (activeSheet.Range["M139"].Value2 == null) ? "" : activeSheet.Range["M139"].Value2;
                    wsd.ShortOHCausePcnt_RE = (activeSheet.Range["N139"].Value2 == null) ? "" : activeSheet.Range["N139"].Value2;
                    wsd.StoppagePcnt_RE = (activeSheet.Range["O139"].Value2 == null) ? "" : activeSheet.Range["O139"].Value2;
                    wsd.RAMCausePcnt_RE = (activeSheet.Range["P139"].Value2 == null) ? "" : activeSheet.Range["P139"].Value2;
                    wsd.TACausePcnt_FP = (activeSheet.Range["M140"].Value2 == null) ? "" : activeSheet.Range["M140"].Value2;
                    wsd.ShortOHCausePcnt_FP = (activeSheet.Range["N140"].Value2 == null) ? "" : activeSheet.Range["N140"].Value2;
                    wsd.StoppagePcnt_FP = (activeSheet.Range["O140"].Value2 == null) ? "" : activeSheet.Range["O140"].Value2;
                    wsd.RAMCausePcnt_FP = (activeSheet.Range["P140"].Value2 == null) ? "" : activeSheet.Range["P140"].Value2;
                    wsd.TACausePcnt_IE = (activeSheet.Range["M141"].Value2 == null) ? "" : activeSheet.Range["M141"].Value2;
                    wsd.ShortOHCausePcnt_IE = (activeSheet.Range["N141"].Value2 == null) ? "" : activeSheet.Range["N141"].Value2;
                    wsd.StoppagePcnt_IE = (activeSheet.Range["O141"].Value2 == null) ? "" : activeSheet.Range["O141"].Value2;
                    wsd.RAMCausePcnt_IE = (activeSheet.Range["P141"].Value2 == null) ? "" : activeSheet.Range["P141"].Value2;
                    wsd.TACausePcnt_Tot = (activeSheet.Range["M142"].Value2 == null) ? "" : activeSheet.Range["M142"].Value2;
                    wsd.StoppagePcnt_Tot = (activeSheet.Range["O142"].Value2 == null) ? "" : activeSheet.Range["O142"].Value2;
                    wsd.ShortOHCausePcnt_Tot = (activeSheet.Range["N142"].Value2 == null) ? "" : activeSheet.Range["N142"].Value2;
                    wsd.RAMCausePcnt_Tot = (activeSheet.Range["P142"].Value2 == null) ? "" : activeSheet.Range["P142"].Value2;
                    wsd.TARegulatoryInspect = (activeSheet.Range["M146"].Value2 == null) ? "" : activeSheet.Range["M146"].Value2;
                    wsd.TADiscretionaryInspect = (activeSheet.Range["M147"].Value2 == null) ? "" : activeSheet.Range["M147"].Value2;
                    wsd.TADeterioration = (activeSheet.Range["M148"].Value2 == null) ? "" : activeSheet.Range["M148"].Value2;
                    wsd.TAUnplannedFailure = (activeSheet.Range["M149"].Value2 == null) ? "" : activeSheet.Range["M149"].Value2;
                    wsd.TALowDemand = (activeSheet.Range["M150"].Value2 == null) ? "" : activeSheet.Range["M150"].Value2;
                    wsd.TAFixedSchedule = (activeSheet.Range["M151"].Value2 == null) ? "" : activeSheet.Range["M151"].Value2;
                    wsd.TACptlProject = (activeSheet.Range["M152"].Value2 == null) ? "" : activeSheet.Range["M152"].Value2;
                    wsd.TAContAvail = (activeSheet.Range["M153"].Value2 == null) ? "" : activeSheet.Range["M153"].Value2;
                    wsd.TAWeather = (activeSheet.Range["M154"].Value2 == null) ? "" : activeSheet.Range["M154"].Value2;
                    wsd.TAHeatExchOpened = (activeSheet.Range["M158"].Value2 == null) ? "" : activeSheet.Range["M158"].Value2;
                    wsd.TAControlValves = (activeSheet.Range["M159"].Value2 == null) ? "" : activeSheet.Range["M159"].Value2;
                    wsd.TAValvesCalibrated = (activeSheet.Range["M160"].Value2 == null) ? "" : activeSheet.Range["M160"].Value2;
                    wsd.TAFiredFurnaces = (activeSheet.Range["M161"].Value2 == null) ? "" : activeSheet.Range["M161"].Value2;
                    wsd.TAValvesOpened = (activeSheet.Range["M162"].Value2 == null) ? "" : activeSheet.Range["M162"].Value2;
                    wsd.TADistTowersOpened = (activeSheet.Range["M163"].Value2 == null) ? "" : activeSheet.Range["M163"].Value2;
                    wsd.TAPumps = (activeSheet.Range["M164"].Value2 == null) ? "" : activeSheet.Range["M164"].Value2;
                    wsd.TACompressors = (activeSheet.Range["M165"].Value2 == null) ? "" : activeSheet.Range["M165"].Value2;
                    wsd.TACritPath = (activeSheet.Range["M168"].Value2 == null) ? "" : activeSheet.Range["M168"].Value2;
                    wsd.TACritPathOth = (activeSheet.Range["M175"].Value2 == null) ? "" : activeSheet.Range["M175"].Value2;
                    wsd.RelProcessImproveProg_RE = (activeSheet.Range["M179"].Value2 == null) ? "" : activeSheet.Range["M179"].Value2;
                    wsd.RelProcessImproveProg_FP = (activeSheet.Range["N179"].Value2 == null) ? "" : activeSheet.Range["N179"].Value2;
                    wsd.RelProcessImproveProg_IE = (activeSheet.Range["O179"].Value2 == null) ? "" : activeSheet.Range["O179"].Value2;
                    wsd.ProgramTimeBased_RE = (activeSheet.Range["M180"].Value2 == null) ? "" : activeSheet.Range["M180"].Value2;
                    wsd.ProgramTimeBased_FP = (activeSheet.Range["N180"].Value2 == null) ? "" : activeSheet.Range["N180"].Value2;
                    wsd.ProgramTimeBased_IE = (activeSheet.Range["O180"].Value2 == null) ? "" : activeSheet.Range["O180"].Value2;
                    wsd.ProgramCBM_RE = (activeSheet.Range["M181"].Value2 == null) ? "" : activeSheet.Range["M181"].Value2;
                    wsd.ProgramCBM_FP = (activeSheet.Range["N181"].Value2 == null) ? "" : activeSheet.Range["N181"].Value2;
                    wsd.ProgramCBM_IE = (activeSheet.Range["O181"].Value2 == null) ? "" : activeSheet.Range["O181"].Value2;
                    wsd.ProgramRCM_RE = (activeSheet.Range["M182"].Value2 == null) ? "" : activeSheet.Range["M182"].Value2;
                    wsd.ProgramRCM_FP = (activeSheet.Range["N182"].Value2 == null) ? "" : activeSheet.Range["N182"].Value2;
                    wsd.ProgramRCM_IE = (activeSheet.Range["O182"].Value2 == null) ? "" : activeSheet.Range["O182"].Value2;
                    wsd.RiskMgmtEquipRBI_RE = (activeSheet.Range["M183"].Value2 == null) ? "" : activeSheet.Range["M183"].Value2;
                    wsd.RiskMgmtEquipRBI_FP = (activeSheet.Range["N183"].Value2 == null) ? "" : activeSheet.Range["N183"].Value2;
                    wsd.RiskMgmtEquipRBI_IE = (activeSheet.Range["O183"].Value2 == null) ? "" : activeSheet.Range["O183"].Value2;
                    wsd.EquipCriticalityRanking_RE = (activeSheet.Range["M184"].Value2 == null) ? "" : activeSheet.Range["M184"].Value2;
                    wsd.EquipCriticalityRanking_FP = (activeSheet.Range["N184"].Value2 == null) ? "" : activeSheet.Range["N184"].Value2;
                    wsd.EquipCriticalityRanking_IE = (activeSheet.Range["O184"].Value2 == null) ? "" : activeSheet.Range["O184"].Value2;
                    wsd.ProgramCUI_RE = (activeSheet.Range["M185"].Value2 == null) ? "" : activeSheet.Range["M185"].Value2;
                    wsd.ProgramCUI_FP = (activeSheet.Range["N185"].Value2 == null) ? "" : activeSheet.Range["N185"].Value2;
                    wsd.ProgramCUI_IE = (activeSheet.Range["O185"].Value2 == null) ? "" : activeSheet.Range["O185"].Value2;
                    wsd.ProgramIRThermography_RE = (activeSheet.Range["M186"].Value2 == null) ? "" : activeSheet.Range["M186"].Value2;
                    wsd.ProgramIRThermography_FP = (activeSheet.Range["N186"].Value2 == null) ? "" : activeSheet.Range["N186"].Value2;
                    wsd.ProgramIRThermography_IE = (activeSheet.Range["O186"].Value2 == null) ? "" : activeSheet.Range["O186"].Value2;
                    wsd.ProgramVibrationAnalysis_RE = (activeSheet.Range["M187"].Value2 == null) ? "" : activeSheet.Range["M187"].Value2;
                    wsd.ProgramAcousticEmission_RE = (activeSheet.Range["M188"].Value2 == null) ? "" : activeSheet.Range["M188"].Value2;
                    wsd.ProgramAcousticEmission_FP = (activeSheet.Range["N188"].Value2 == null) ? "" : activeSheet.Range["N188"].Value2;
                    wsd.ProgramAcousticEmission_IE = (activeSheet.Range["O188"].Value2 == null) ? "" : activeSheet.Range["O188"].Value2;
                    wsd.ProgramOilAnalysis_RE = (activeSheet.Range["M189"].Value2 == null) ? "" : activeSheet.Range["M189"].Value2;
                    wsd.ProgramOilAnalysis_IE = (activeSheet.Range["O189"].Value2 == null) ? "" : activeSheet.Range["O189"].Value2;
                    wsd.EventsTurbines = (activeSheet.Range["M194"].Value2 == null) ? "" : activeSheet.Range["M194"].Value2;
                    wsd.MTBFTurbines = (activeSheet.Range["O194"].Value2 == null) ? "" : activeSheet.Range["O194"].Value2;
                    wsd.EventsCompressorsRecip = (activeSheet.Range["M195"].Value2 == null) ? "" : activeSheet.Range["M195"].Value2;
                    wsd.MTBFCompressorsRecip = (activeSheet.Range["O195"].Value2 == null) ? "" : activeSheet.Range["O195"].Value2;
                    wsd.EventsCompressorsRotating = (activeSheet.Range["M196"].Value2 == null) ? "" : activeSheet.Range["M196"].Value2;
                    wsd.MTBFCompressorsRotating = (activeSheet.Range["O196"].Value2 == null) ? "" : activeSheet.Range["O196"].Value2;
                    wsd.EventsPumpsCentrifugal = (activeSheet.Range["M197"].Value2 == null) ? "" : activeSheet.Range["M197"].Value2;
                    wsd.MTBFPumpsCentrifugal = (activeSheet.Range["O197"].Value2 == null) ? "" : activeSheet.Range["O197"].Value2;
                    wsd.EventsPumpsPosDisp = (activeSheet.Range["M198"].Value2 == null) ? "" : activeSheet.Range["M198"].Value2;
                    wsd.MTBFPumpsPosDisp = (activeSheet.Range["O198"].Value2 == null) ? "" : activeSheet.Range["O198"].Value2;
                    wsd.EventsFurnaceBoilers = (activeSheet.Range["M200"].Value2 == null) ? "" : activeSheet.Range["M200"].Value2;
                    wsd.MTBFFurnaceBoilers = (activeSheet.Range["O200"].Value2 == null) ? "" : activeSheet.Range["O200"].Value2;
                    wsd.EventsVessels = (activeSheet.Range["M201"].Value2 == null) ? "" : activeSheet.Range["M201"].Value2;
                    wsd.MTBFVessels = (activeSheet.Range["O201"].Value2 == null) ? "" : activeSheet.Range["O201"].Value2;
                    wsd.EventsDistTowers = (activeSheet.Range["M202"].Value2 == null) ? "" : activeSheet.Range["M202"].Value2;
                    wsd.MTBFDistTowers = (activeSheet.Range["O202"].Value2 == null) ? "" : activeSheet.Range["O202"].Value2;
                    wsd.EventsHeatExch = (activeSheet.Range["M203"].Value2 == null) ? "" : activeSheet.Range["M203"].Value2;
                    wsd.MTBFHeatExch = (activeSheet.Range["O203"].Value2 == null) ? "" : activeSheet.Range["O203"].Value2;
                    wsd.EventsMotorsMain = (activeSheet.Range["M205"].Value2 == null) ? "" : activeSheet.Range["M205"].Value2;
                    wsd.MTBFMotorsMain = (activeSheet.Range["O205"].Value2 == null) ? "" : activeSheet.Range["O205"].Value2;
                    wsd.EventsVarSpeedDrives = (activeSheet.Range["M206"].Value2 == null) ? "" : activeSheet.Range["M206"].Value2;
                    wsd.MTBFVarSpeedDrives = (activeSheet.Range["O206"].Value2 == null) ? "" : activeSheet.Range["O206"].Value2;
                    wsd.EventsControlValves = (activeSheet.Range["M207"].Value2 == null) ? "" : activeSheet.Range["M207"].Value2;
                    wsd.MTBFControlValves = (activeSheet.Range["O207"].Value2 == null) ? "" : activeSheet.Range["O207"].Value2;
                    wsd.EventsAnalyzerProcCtrl = (activeSheet.Range["M208"].Value2 == null) ? "" : activeSheet.Range["M208"].Value2;
                    wsd.MTBFAnalyzerProcCtrl = (activeSheet.Range["O208"].Value2 == null) ? "" : activeSheet.Range["O208"].Value2;
                    wsd.EventsAnalyzerBlending = (activeSheet.Range["M209"].Value2 == null) ? "" : activeSheet.Range["M209"].Value2;
                    wsd.MTBFAnalyzerBlending = (activeSheet.Range["O209"].Value2 == null) ? "" : activeSheet.Range["O209"].Value2;
                    wsd.EventsAnalyzerEmissions = (activeSheet.Range["M210"].Value2 == null) ? "" : activeSheet.Range["M210"].Value2;
                    wsd.MTBFAnalyzerEmissions = (activeSheet.Range["O210"].Value2 == null) ? "" : activeSheet.Range["O210"].Value2;
                    
                    wsd.RoutCraftmanWhrRE = (activeSheet.Range["M218"].Value2 == null) ? "" : activeSheet.Range["M218"].Value2;
                    wsd.RoutCraftmanWhrFP = (activeSheet.Range["N218"].Value2 == null) ? "" : activeSheet.Range["N218"].Value2;
                    wsd.RoutCraftmanWhrIE = (activeSheet.Range["O218"].Value2 == null) ? "" : activeSheet.Range["O218"].Value2;
                    wsd.RoutCraftmanWhrTOT = (activeSheet.Range["P218"].Value2 == null) ? "" : activeSheet.Range["P218"].Value2;
                    wsd.RoutCraftmanMtCapWhrRE = (activeSheet.Range["M219"].Value2 == null) ? "" : activeSheet.Range["M219"].Value2;
                    wsd.RoutCraftmanMtCapWhrFP = (activeSheet.Range["N219"].Value2 == null) ? "" : activeSheet.Range["N219"].Value2;
                    wsd.RoutCraftmanMtCapWhrIE = (activeSheet.Range["O219"].Value2 == null) ? "" : activeSheet.Range["O219"].Value2;
                    wsd.RoutCraftmanMtCapWhrTOT = (activeSheet.Range["P219"].Value2 == null) ? "" : activeSheet.Range["P219"].Value2;
                    wsd.RoutCraftmanTotWhrRE = (activeSheet.Range["M220"].Value2 == null) ? "" : activeSheet.Range["M220"].Value2;
                    wsd.RoutCraftmanTotWhrFP = (activeSheet.Range["N220"].Value2 == null) ? "" : activeSheet.Range["N220"].Value2;
                    wsd.RoutCraftmanTotWhrIE = (activeSheet.Range["O220"].Value2 == null) ? "" : activeSheet.Range["O220"].Value2;
                    wsd.RoutCraftmanTotWhrTOT = (activeSheet.Range["P220"].Value2 == null) ? "" : activeSheet.Range["P220"].Value2;
                    wsd.TACraftmanWhrRE = (activeSheet.Range["M225"].Value2 == null) ? "" : activeSheet.Range["M225"].Value2;
                    wsd.TACraftmanWhrFP = (activeSheet.Range["N225"].Value2 == null) ? "" : activeSheet.Range["N225"].Value2;
                    wsd.TACraftmanWhrIE = (activeSheet.Range["O225"].Value2 == null) ? "" : activeSheet.Range["O225"].Value2;
                    wsd.TACraftmanWhrTOT = (activeSheet.Range["P225"].Value2 == null) ? "" : activeSheet.Range["P225"].Value2;
                    wsd.TACraftmanMtCapWhrRE = (activeSheet.Range["M226"].Value2 == null) ? "" : activeSheet.Range["M226"].Value2;
                    wsd.TACraftmanMtCapWhrFP = (activeSheet.Range["N226"].Value2 == null) ? "" : activeSheet.Range["N226"].Value2;
                    wsd.TACraftmanMtCapWhrIE = (activeSheet.Range["O226"].Value2 == null) ? "" : activeSheet.Range["O226"].Value2;
                    wsd.TACraftmanMtCapWhrTOT = (activeSheet.Range["P226"].Value2 == null) ? "" : activeSheet.Range["P226"].Value2;
                    wsd.TACraftmanTotWhrRE = (activeSheet.Range["M227"].Value2 == null) ? "" : activeSheet.Range["M227"].Value2;
                    wsd.TACraftmanTotWhrFP = (activeSheet.Range["N227"].Value2 == null) ? "" : activeSheet.Range["N227"].Value2;
                    wsd.TACraftmanTotWhrIE = (activeSheet.Range["O227"].Value2 == null) ? "" : activeSheet.Range["O227"].Value2;
                    wsd.TACraftmanTotWhrTOT = (activeSheet.Range["P227"].Value2 == null) ? "" : activeSheet.Range["P227"].Value2;
                    wsd.RoutMatlRE = (activeSheet.Range["M232"].Value2 == null) ? "" : activeSheet.Range["M232"].Value2;
                    wsd.RoutMatlFP = (activeSheet.Range["N232"].Value2 == null) ? "" : activeSheet.Range["N232"].Value2;
                    wsd.RoutMatlIE = (activeSheet.Range["O232"].Value2 == null) ? "" : activeSheet.Range["O232"].Value2;
                    wsd.RoutMatlTOT = (activeSheet.Range["P232"].Value2 == null) ? "" : activeSheet.Range["P232"].Value2;
                    wsd.RoutMatlMtCapRE = (activeSheet.Range["M233"].Value2 == null) ? "" : activeSheet.Range["M233"].Value2;
                    wsd.RoutMatlMtCapFP = (activeSheet.Range["N233"].Value2 == null) ? "" : activeSheet.Range["N233"].Value2;
                    wsd.RoutMatlMtCapIE = (activeSheet.Range["O233"].Value2 == null) ? "" : activeSheet.Range["O233"].Value2;
                    wsd.RoutMatlMtCapTOT = (activeSheet.Range["P233"].Value2 == null) ? "" : activeSheet.Range["P233"].Value2;
                    wsd.RoutMatlTotRE = (activeSheet.Range["M234"].Value2 == null) ? "" : activeSheet.Range["M234"].Value2;
                    wsd.RoutMatlTotFP = (activeSheet.Range["N234"].Value2 == null) ? "" : activeSheet.Range["N234"].Value2;
                    wsd.RoutMatlTotIE = (activeSheet.Range["O234"].Value2 == null) ? "" : activeSheet.Range["O234"].Value2;
                    wsd.RoutMatlTotTOT = (activeSheet.Range["P234"].Value2 == null) ? "" : activeSheet.Range["P234"].Value2;
                    wsd.AnnTAMatlRE = (activeSheet.Range["M238"].Value2 == null) ? "" : activeSheet.Range["M238"].Value2;
                    wsd.AnnTAMatlFP = (activeSheet.Range["N238"].Value2 == null) ? "" : activeSheet.Range["N238"].Value2;
                    wsd.AnnTAMatlIE = (activeSheet.Range["O238"].Value2 == null) ? "" : activeSheet.Range["O238"].Value2;
                    wsd.AnnTAMatlTOT = (activeSheet.Range["P238"].Value2 == null) ? "" : activeSheet.Range["P238"].Value2;
                    wsd.AnnTAMatlMtCapRE = (activeSheet.Range["M239"].Value2 == null) ? "" : activeSheet.Range["M239"].Value2;
                    wsd.AnnTAMatlMtCapFP = (activeSheet.Range["N239"].Value2 == null) ? "" : activeSheet.Range["N239"].Value2;
                    wsd.AnnTAMatlMtCapIE = (activeSheet.Range["O239"].Value2 == null) ? "" : activeSheet.Range["O239"].Value2;
                    wsd.AnnTAMatlMtCapTOT = (activeSheet.Range["P239"].Value2 == null) ? "" : activeSheet.Range["P239"].Value2;
                    wsd.AnnTAMatlTotRE = (activeSheet.Range["M240"].Value2 == null) ? "" : activeSheet.Range["M240"].Value2;
                    wsd.AnnTAMatlTotFP = (activeSheet.Range["N240"].Value2 == null) ? "" : activeSheet.Range["N240"].Value2;
                    wsd.AnnTAMatlTotIE = (activeSheet.Range["O240"].Value2 == null) ? "" : activeSheet.Range["O240"].Value2;
                    wsd.AnnTAMatlTotTOT = (activeSheet.Range["P240"].Value2 == null) ? "" : activeSheet.Range["P240"].Value2;
                    wsd.TaskEmergency_RE = (activeSheet.Range["M247"].Value2 == null) ? "" : activeSheet.Range["M247"].Value2;
                    wsd.TaskEmergency_FP = (activeSheet.Range["N247"].Value2 == null) ? "" : activeSheet.Range["N247"].Value2;
                    wsd.TaskEmergency_IE = (activeSheet.Range["O247"].Value2 == null) ? "" : activeSheet.Range["O247"].Value2;
                    wsd.TaskEmergency_Tot = (activeSheet.Range["P247"].Value2 == null) ? "" : activeSheet.Range["P247"].Value2;
                    wsd.TaskRoutCorrective_RE = (activeSheet.Range["M249"].Value2 == null) ? "" : activeSheet.Range["M249"].Value2;
                    wsd.TaskRoutCorrective_FP = (activeSheet.Range["N249"].Value2 == null) ? "" : activeSheet.Range["N249"].Value2;
                    wsd.TaskRoutCorrective_IE = (activeSheet.Range["O249"].Value2 == null) ? "" : activeSheet.Range["O249"].Value2;
                    wsd.TaskRoutCorrective_Tot = (activeSheet.Range["P249"].Value2 == null) ? "" : activeSheet.Range["P249"].Value2;
                    wsd.TaskPreventive_RE = (activeSheet.Range["M251"].Value2 == null) ? "" : activeSheet.Range["M251"].Value2;
                    wsd.TaskPreventive_FP = (activeSheet.Range["N251"].Value2 == null) ? "" : activeSheet.Range["N251"].Value2;
                    wsd.TaskPreventive_IE = (activeSheet.Range["O251"].Value2 == null) ? "" : activeSheet.Range["O251"].Value2;
                    wsd.TaskPreventive_Tot = (activeSheet.Range["P251"].Value2 == null) ? "" : activeSheet.Range["P251"].Value2;
                    wsd.TaskConditionMonitor_RE = (activeSheet.Range["M253"].Value2 == null) ? "" : activeSheet.Range["M253"].Value2;
                    wsd.TaskConditionMonitor_FP = (activeSheet.Range["N253"].Value2 == null) ? "" : activeSheet.Range["N253"].Value2;
                    wsd.TaskConditionMonitor_IE = (activeSheet.Range["O253"].Value2 == null) ? "" : activeSheet.Range["O253"].Value2;
                    wsd.TaskConditionMonitor_Tot = (activeSheet.Range["P253"].Value2 == null) ? "" : activeSheet.Range["P253"].Value2;
                    wsd.TaskTotal_RE = (activeSheet.Range["M255"].Value2 == null) ? "" : activeSheet.Range["M255"].Value2;
                    wsd.TaskTotal_FP = (activeSheet.Range["N255"].Value2 == null) ? "" : activeSheet.Range["N255"].Value2;
                    wsd.TaskTotal_IE = (activeSheet.Range["O255"].Value2 == null) ? "" : activeSheet.Range["O255"].Value2;
                    wsd.TaskTotal_Tot = (activeSheet.Range["P255"].Value2 == null) ? "" : activeSheet.Range["P255"].Value2;
                    
                    new System.Xml.Serialization.XmlSerializer(typeof(WorksheetData)).Serialize(xmlWriter, wsd);

                    xmlWriter.Flush();
                    stream.Position = 0;
                    xmldoc.Load(stream);
                }
            }

            return xmldoc.OuterXml;
        }
        #endregion

        #region Serialize object
        public static string Serialize(object obj)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            using (StreamReader reader = new StreamReader(memoryStream))
            {
                System.Runtime.Serialization.DataContractSerializer serializer = new System.Runtime.Serialization.DataContractSerializer(obj.GetType());
                serializer.WriteObject(memoryStream, obj);
                memoryStream.Position = 0;
                return reader.ReadToEnd();
            }
        }
        #endregion

        #region Create Login Form
        private void CreateLoginForm()
        {            
        }
        #endregion

        #region Populate Login Form
        private void PopulateLoginForm()
        {
            frmLoginWindow.Font = new System.Drawing.Font("Arial", 8);
            frmLoginWindow.ControlBox = false;
            TextBox txtLoginUsername = new TextBox();
            TextBox txtLoginPassword = new TextBox();
            frmLoginWindow.Text = "Login";
            frmLoginWindow.Width = 230;
            frmLoginWindow.Height = 120;
            Label lblLoginUsername = new Label();
            lblLoginUsername.Text = "Username: ";
            lblLoginUsername.Top = lblLoginUsername.Top + 5;
            Label lblLoginPassword = new Label();
            lblLoginPassword.Text = "Password: ";
            lblLoginPassword.Top = lblLoginUsername.Top + 25;
            txtLoginUsername.Left = lblLoginUsername.Right + 5;
            txtLoginUsername.Top = lblLoginUsername.Top;
            txtLoginPassword.Left = txtLoginUsername.Left;
            txtLoginPassword.Top = lblLoginPassword.Top;
            txtLoginPassword.UseSystemPasswordChar = true;
            Button btnCancel = new Button();
            btnCancel.Text = "Cancel";
            btnCancel.Top = txtLoginPassword.Top + 24;
            btnCancel.Left = txtLoginPassword.Left + 25;

            Button btnSubmit = new Button();
            btnSubmit.Text = "Submit";
            btnSubmit.Top = btnCancel.Top;
            btnSubmit.Left = btnCancel.Left - 75;

            frmLoginWindow.Controls.Remove(btnSubmit);
            frmLoginWindow.Controls.Remove(btnCancel);

            frmLoginWindow.Controls.Add(btnSubmit);
            frmLoginWindow.Controls.Add(btnCancel);
            frmLoginWindow.Controls.Add(txtLoginUsername);
            frmLoginWindow.Controls.Add(txtLoginPassword);
            frmLoginWindow.Controls.Add(lblLoginUsername);
            frmLoginWindow.Controls.Add(lblLoginPassword);
            frmLoginWindow.StartPosition = FormStartPosition.CenterScreen;
            System.Drawing.Size sz = new System.Drawing.Size(230, 120);
            frmLoginWindow.MaximumSize = sz;
            frmLoginWindow.MinimumSize = sz;

            btnSubmit.Click -= btnPullDataNext_Click;

            btnSubmit.Click += new EventHandler(btnSubmit_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);

            showLoginForm();
            txtLoginUsername.Focus();
        }
        #endregion

        #region btnCancel Click Event
        void btnCancel_Click(object sender, EventArgs e)
        {
            hideLoginForm();
            ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[2].Text = "";
            ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[3].Text = "";
        }
        #endregion

        #region Send Site Data
        public void SendSiteDataToWCFService(byte[] xmlstring, MyExcelService.Service1Client excelSvc, int loginid, byte[] salt, byte[] key, byte[] vector, double dsid)
        {
            try
            {
                frmProgressWindow.Show();
                RunAsyncWCFOperation(xmlstring, excelSvc, loginid, salt, key, vector, dsid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Send Excel Data
        public void SendExcelDataToWCFService(byte[] xmlstring, MyExcelService.Service1Client excelSvc, int loginid, byte[] salt, byte[] key, byte[] vector, double dsid)
        {
            try
            {
                frmProgressWindow.Show();
                RunAsyncWCFOperation(xmlstring, excelSvc, loginid, salt, key, vector, dsid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Run Async WCF Operation
        private void RunAsyncWCFOperation(byte[] xmlstring, MyExcelService.Service1Client excelSvc, int loginid, byte[] salt, byte[] key, byte[] vector, double dsid)
        {
            try
            {
                BackgroundWorker backgroundWorker1 = new BackgroundWorker();
                backgroundWorker1.WorkerSupportsCancellation = true;

                backgroundWorker1.DoWork += (o, e) =>
                {
                    if (backgroundWorker1.CancellationPending)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        e.Result = excelSvc.GetXMLData(xmlstring, loginid, salt, key, vector, dsid);
                    }
                };

                backgroundWorker1.RunWorkerCompleted += (o, e) =>
                {
                    if (e.Error != null)
                    {
                        var newEx = new InvalidOperationException("An error occured during processing.", e.Error);
                        MessageBox.Show(newEx.ToString());

                    }
                    else if (e.Cancelled)
                    {
                        MessageBox.Show("User cancelled operation!");
                    }
                    else
                    {
                        hideProgressForm();
                        SendCount++;
                        if (SendCount == DataSetsToSend)
                            MessageBox.Show(e.Result.ToString(), "Status");
                        if (e.Result.ToString() == "Data Received and Saved Successfully.")
                            hideLoginForm();
                        else
                            showLoginForm();
                    }

                    if (excelSvc.State == System.ServiceModel.CommunicationState.Opened)
                        excelSvc.Close();
                };

                backgroundWorker1.RunWorkerAsync();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Hide Progress Form
        private void hideProgressForm()
        {
            this.hideProgressFormThread();
        }
        #endregion

        #region Hide Progress Form Thread
        private void hideProgressFormThread()
        {
            if (this.frmProgressWindow.InvokeRequired)
            {
                frmProgressWindow.BeginInvoke(new MethodInvoker(hideProgressForm));
                return;
            }
            else
            {
                this.frmProgressWindow.Hide();
            }
        }
        #endregion

        #region Show Login Form
        private void showLoginForm()
        {
            this.showLoginFormThread();
        }
        #endregion

        #region Show Login Form Thread
        private void showLoginFormThread()
        {
            if (this.frmLoginWindow.InvokeRequired)
            {
                frmLoginWindow.BeginInvoke(new MethodInvoker(showLoginForm));
                return;
            }
            else
            {
                if (frmLoginWindow.Visible == false)
                    this.frmLoginWindow.ShowDialog();
            }
        }
        #endregion

        #region Hide Login Form
        private void hideLoginForm()
        {
            this.hideLoginFormThread();
        }
        #endregion

        #region Hide Login Form Thread
        private void hideLoginFormThread()
        {
            if (frmLoginWindow.InvokeRequired)
            {
                frmLoginWindow.BeginInvoke(new MethodInvoker(hideLoginFormThread));
                return;
            }
            else
            {
                this.frmLoginWindow.Hide();
            }
        }
        #endregion

        #region Create Unit Selection Form
        void CreatePullDataSelectionForm()
        {

            frmUnitSelection.Controls.Clear();

            frmUnitSelection.Width = 390;
            frmUnitSelection.Height = 180;
            System.Drawing.Size sz = new System.Drawing.Size(390, 350);
            frmUnitSelection.MaximumSize = sz;
            frmUnitSelection.MinimumSize = sz;
            frmUnitSelection.VerticalScroll.Visible = true;
            frmUnitSelection.VerticalScroll.Enabled = true;
            frmUnitSelection.AutoScroll = true;
            frmUnitSelection.AutoScrollMinSize = new System.Drawing.Size(1, 1);
            frmUnitSelection.Text = "Unit Selection";
            Label lblTitle = new Label();
            lblTitle.Width = 350;
            System.Drawing.Font fontTitle = new System.Drawing.Font("Arial", 10);
            lblTitle.Font = fontTitle;
            lblTitle.Text = "Please select the site(s)/unit(s) you wish to pull data for:";
            frmUnitSelection.Font = new System.Drawing.Font("Arial ", 8);
            frmUnitSelection.Controls.Add(lblTitle);
            int n = 0;
            int y = 20;
            int i = Globals.ThisAddIn.Application.Worksheets.Count;
            foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
            {
                if ((worksheet.Range["D7"].Value2 == "Unit" || worksheet.Range["D7"].Value2 == "Site") && worksheet.Visible == Excel.XlSheetVisibility.xlSheetVisible)
                {
                    CheckBox[] _cb = new CheckBox[i];
                    _cb[n] = new CheckBox();
                    _cb[n].Name = worksheet.Range["M9"].Value2;
                    _cb[n].Text = worksheet.Range["M9"].Value2;
                    _cb[n].Top = y;
                    _cb[n].Width = 350;
                    frmUnitSelection.Controls.Add(_cb[n]);
                    y += 20;
                    n += 1;
                }

            }
            Button btnNext = new Button();
            btnNext.Text = "Next";
            btnNext.Top = y + 7;
            btnNext.Click += new EventHandler(btnNextPD_Click);

            Button btnUnitCancel = new Button();
            btnUnitCancel.Text = "Cancel";
            btnUnitCancel.Top = y + 7;
            btnUnitCancel.Left = btnNext.Left + 75;
            btnUnitCancel.Click += new EventHandler(btnUnitCancel_Click);

            frmUnitSelection.Controls.Remove(btnNext);
            frmUnitSelection.Controls.Remove(btnUnitCancel);

            frmUnitSelection.Controls.Add(btnNext);
            frmUnitSelection.Controls.Add(btnUnitCancel);

            frmUnitSelection.StartPosition = FormStartPosition.CenterScreen;
            frmUnitSelection.ShowDialog();
        }
        #endregion

        #region btnNextPD Click Event
        void btnNextPD_Click(object sender, EventArgs e)
        {
            int i = 0;
            foreach (Control c in frmUnitSelection.Controls)
            {
                if (c is CheckBox)
                {
                    if (((CheckBox)c).Checked)
                    {
                        foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
                        {
                            if (Convert.ToString(worksheet.Range["M9"].Value2) == c.Text && worksheet.Range["D7"].Value2 == "Site")
                                sites[i] = worksheet.Index;
                            else if (Convert.ToString(worksheet.Range["M9"].Value2) == c.Text)
                                units[i] = worksheet.Index;
                            i += 1;
                        }
                        DataSetsToSend++;
                    }
                }
            }
            frmUnitSelection.Hide();
            PopulateLoginFormPD();
        }
        #endregion

        #region Populate Login Form PD
        private void PopulateLoginFormPD()
        {
            frmLoginWindow.Font = new System.Drawing.Font("Arial", 8);
            frmLoginWindow.ControlBox = false;
            TextBox txtLoginUsername = new TextBox();
            TextBox txtLoginPassword = new TextBox();
            frmLoginWindow.Text = "Login";
            frmLoginWindow.Width = 230;
            frmLoginWindow.Height = 120;
            Label lblLoginUsername = new Label();
            lblLoginUsername.Text = "Username: ";
            lblLoginUsername.Top = lblLoginUsername.Top + 5;
            Label lblLoginPassword = new Label();
            lblLoginPassword.Text = "Password: ";
            lblLoginPassword.Top = lblLoginUsername.Top + 25;
            txtLoginUsername.Left = lblLoginUsername.Right + 5;
            txtLoginUsername.Top = lblLoginUsername.Top;
            txtLoginPassword.Left = txtLoginUsername.Left;
            txtLoginPassword.Top = lblLoginPassword.Top;
            txtLoginPassword.UseSystemPasswordChar = true;
            Button btnCancel = new Button();
            btnCancel.Text = "Cancel";
            btnCancel.Top = txtLoginPassword.Top + 24;
            btnCancel.Left = txtLoginPassword.Left + 25;

            Button btnSubmit = new Button();
            btnSubmit.Text = "Submit";
            btnSubmit.Top = btnCancel.Top;
            btnSubmit.Left = btnCancel.Left - 75;

            frmLoginWindow.Controls.Add(btnSubmit);
            frmLoginWindow.Controls.Add(btnCancel);
            frmLoginWindow.Controls.Add(txtLoginUsername);
            frmLoginWindow.Controls.Add(txtLoginPassword);
            frmLoginWindow.Controls.Add(lblLoginUsername);
            frmLoginWindow.Controls.Add(lblLoginPassword);
            frmLoginWindow.StartPosition = FormStartPosition.CenterScreen;
            System.Drawing.Size sz = new System.Drawing.Size(230, 120);
            frmLoginWindow.MaximumSize = sz;
            frmLoginWindow.MinimumSize = sz;

            btnSubmit.Click += new EventHandler(btnPullDataNext_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);

            showLoginForm();
            txtLoginUsername.Focus();
        }
        #endregion

        #region Pull Data
        private void btnPullDataNext_Click(object sender, EventArgs e)
        {
            username = ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[2].Text;
            password = ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[3].Text;

            if (username == "" || password == "")
            {
                System.Windows.Forms.MessageBox.Show(frmLoginWindow, "Please enter username and password", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();
            
            excelSvc.ClientCredentials.UserName.UserName = "jbf";
            excelSvc.ClientCredentials.UserName.Password = "Cabinet9";
            //excelSvc.ClientCredentials.Windows.ClientCredential.UserName = "jbf";
            //excelSvc.ClientCredentials.Windows.ClientCredential.Password = "Cabinet9";
            
            MyExcelService.Handshake handshake = new MyExcelService.Handshake();
            handshake = excelSvc.InitiateHandshake();

            handshake.Value1 = cryptor.Encrypt(username + "|" + password, handshake.Value2, handshake.Value3, handshake.Value4);
            handshake = excelSvc.CompleteHandshake(handshake);
            if (handshake.Status)
            {
                var selectedSites = sites.Where(item => item != null);
                foreach (int value in selectedSites)
                {
                    int dsid = Convert.ToInt32(Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2);
                    dsSiteUnits = excelSvc.getSiteUnits(dsid);
                    //PullAnswers(dsid, value, handshake.Value2, handshake.Value3, handshake.Value4, false, false);
                    PullAnswersWithHistory(dsid, value, handshake.Value2, handshake.Value3, handshake.Value4, false, false);
                }

                var selectedUnits = units.Where(item => item != null);
                foreach (int value in selectedUnits)
                {
                    int dsid = Convert.ToInt32(Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2);
                    //PullAnswers(dsid, value, handshake.Value2, handshake.Value3, handshake.Value4, true, false);
                    PullAnswersWithHistory(dsid, value, handshake.Value2, handshake.Value3, handshake.Value4, true, false);
                }

                if (selectedUnits.Count() == 0)
                {
                    //int x = 0;
                    //do
                    //{
                    //    worksheet1.Range["C7"].Value2 = dsSiteUnits.Tables[0].Rows[x]["DatasetID"].ToString();
                    //}
                    //while (x < dsSiteUnits.Tables[0].Rows.Count);
                    foreach (DataRow dr in dsSiteUnits.Tables[0].Rows)
                    {
                        Excel.Worksheet worksheet1 = ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[4]);
                        worksheet1.Unprotect("AABAABAAAAA>");
                        worksheet1.Range["C7"].Value2 = dr["DatasetID"].ToString();
                        worksheet1.Copy(System.Type.Missing, ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[4]));
                        //PullAnswers(Convert.ToInt32(worksheet1.Range["C7"].Value2), worksheet1.Index + 1, handshake.Value2, handshake.Value3, handshake.Value4, true, false);
                        PullAnswersWithHistory(Convert.ToInt32(worksheet1.Range["C7"].Value2), worksheet1.Index + 1, handshake.Value2, handshake.Value3, handshake.Value4, true, false);
                        ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[worksheet1.Index + 1]).Protect("AABAABAAAAA>", System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing);
                        //break;
                    }
                    Excel.Worksheet worksheet2 = ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[4]);
                    worksheet2.Visible = Excel.XlSheetVisibility.xlSheetHidden;
                }
                selectedUnits = null;
                Array.Clear(units, 0, units.Length);
                selectedSites = null;
                Array.Clear(sites, 0, sites.Length);

                //JBF 08/23/2016 Hide Solomon Field Names on site sheet and protect sheet and remove all comments
                //Put this in while i was created all VI files. Commented it out when i was done
                //Excel.Range rngFieldName;
                //rngFieldName = Globals.ThisAddIn.Application.Names.Item("SAFieldNames").RefersToRange;
                //rngFieldName.Columns.EntireColumn.Hidden = true;
                //Excel.Worksheet worksheetX = ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[2]);
                //worksheetX.Protect("AABAABAAAAA>");
                //foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
                //{
                //    foreach (Excel.Comment comment in worksheet.Comments)
                //    {
                //        comment.Delete();
                //    }
                //}
            }
            else
            {
                MessageBox.Show(frmLoginWindow, "Invalid username/password", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[2].Text = "";
            ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[3].Text = "";
            

        }
        #endregion

        #region Pull Answers
        public void PullAnswers(int dsid, int sheet, byte[] salt, byte[] key, byte[] vector, bool isUnit, bool PrePop)
        {
            MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();
            byte[] byteAnswers;
            if (PrePop)
                byteAnswers = excelSvc.pushAnswersPrePop(dsid, salt, key, vector);
            else
                byteAnswers = excelSvc.pushAnswers(dsid, salt, key, vector);
            string strAnswers = cryptor.Decrypt(byteAnswers, salt.Length, key, vector);

            MemoryStream memStream = new MemoryStream((Encoding.ASCII.GetBytes(strAnswers)));
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            memStream.Position = 0;
            System.Data.DataSet dsAnswers = new System.Data.DataSet();
            dsAnswers.ReadXml(memStream);

            Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[sheet];

            Excel.Range rngFieldName;
            var value = new object();
            int x = 1;

            if (isUnit)
            {
                rngFieldName = activeSheet.Names.Item("UnitFieldNames").RefersToRange;
                //rngFieldName = Globals.ThisAddIn.Application.Names.Item("UnitFieldNames").RefersToRange;
                try
                {
                    activeSheet.Range["M10"].Value2 = dsAnswers.Tables[0].Rows[0]["primaryproductname"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M12"].Value2 = dsAnswers.Tables[0].Rows[0]["processfamily"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M13"].Value2 = dsAnswers.Tables[0].Rows[0]["GenericProductName"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M14"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoProcessType"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Name = dsAnswers.Tables[0].Rows[0]["UnitName"].ToString().Replace(":", " ").Replace("/", " ").Replace("\\", " ").Replace("?", " ").Replace("*", " ").Replace("[", " ").Replace("]", " ");
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M9"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitName"].ToString();
                }
                catch (Exception ex) { }
            }
            else
            {
                rngFieldName = Globals.ThisAddIn.Application.Names.Item("SiteFieldNames").RefersToRange;
            }
            try
            {
                //activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[sheet];
                //throw (new Exception());
                for (x = 1; x <= rngFieldName.Rows.Count; x++)
                {
                    if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 1].Value))
                    {
                        if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 1].Value.Trim()))
                        {
                            try
                            {
                                rngFieldName.Cells[x, 12].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 1].Value.Trim()].ToString();
                            }
                            catch (Exception ex) { }
                        }
                    }
                    if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 2].Value))
                    {
                        if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 2].Value.Trim()))
                        {
                            try
                            {
                                rngFieldName.Cells[x, 13].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 2].Value.Trim()].ToString();
                            }
                            catch (Exception ex) { }
                        }
                    }
                    if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 3].Value))
                    {
                        if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 3].Value.Trim()))
                        {
                            try
                            {
                                rngFieldName.Cells[x, 14].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 3].Value.Trim()].ToString();
                            }
                            catch (Exception ex) { }
                        }
                    }
                    if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 4].Value))
                    {
                        if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 4].Value.Trim()))
                        {
                            try
                            {
                                rngFieldName.Cells[x, 15].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 4].Value.Trim()].ToString();
                            }
                            catch (Exception ex) { }
                        }
                    }
                    if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 5].Value))
                    {
                        if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 5].Value.Trim()))
                        {
                            try
                            {
                                rngFieldName.Cells[x, 16].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 5].Value.Trim()].ToString();
                            }
                            catch (Exception ex) { }
                        }
                    }
                }

                //if (isUnit)
                //    MessageBox.Show(activeSheet.Name + " complete.");
            }
            catch (Exception ex)
            {
            }
            hideLoginForm();
        }
        #endregion

        #region Decompress Data
        public string DecompressData(byte[] data)
        {
            using (var memStreamIn = new MemoryStream(data))
            using (var memStreamOut = new MemoryStream())
            {
                using (var gzStream = new System.IO.Compression.GZipStream(memStreamIn, System.IO.Compression.CompressionMode.Decompress))
                {
                    gzStream.CopyTo(memStreamOut);
                }
                return Encoding.UTF8.GetString(memStreamOut.ToArray());
            }
        }
        #endregion

        #region Pull Answers with history
        public void PullAnswersWithHistory(int dsid, int sheet, byte[] salt, byte[] key, byte[] vector, bool isUnit, bool PrePop)
        {
            MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();

            excelSvc.ClientCredentials.UserName.UserName = "jbf";
            excelSvc.ClientCredentials.UserName.Password = "Cabinet9";
            //excelSvc.ClientCredentials.Windows.ClientCredential.UserName = "jbf";
            //excelSvc.ClientCredentials.Windows.ClientCredential.Password = "Cabinet9";

            byte[] byteAnswers;
            if (PrePop)
                byteAnswers = excelSvc.pushAnswersPrePop(dsid, salt, key, vector);
            else
                byteAnswers = excelSvc.pushAnswerswithhistory(dsid, salt, key, vector);
            byte[] byteAnswersDecrypted = cryptor.DecryptCompressedBytes(byteAnswers, salt.Length, key, vector);
            //string strAnswers = cryptor.Decrypt(byteAnswers, salt.Length, key, vector);

            string strAnswers = DecompressData(byteAnswersDecrypted);

            MemoryStream memStream = new MemoryStream((Encoding.ASCII.GetBytes(strAnswers)));
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            memStream.Position = 0;
            System.Data.DataSet dsAnswers = new System.Data.DataSet();
            dsAnswers.ReadXml(memStream);

            Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[sheet];

            Excel.Range rngFieldName;
            var value = new object();
            int x = 1;

            if (isUnit)
            {
                rngFieldName = activeSheet.Names.Item("UnitFieldNames").RefersToRange;
                DataRow[] dr;
                dr = dsAnswers.Tables[0].Select("PropertyName = 'primaryproductname'");
                activeSheet.Range["M10"].Value2 = dr[0]["strValue"].ToString();
                dr = dsAnswers.Tables[0].Select("PropertyName = 'processfamily'");
                activeSheet.Range["M12"].Value2 = dr[0]["strValue"].ToString();
                dr = dsAnswers.Tables[0].Select("PropertyName = 'GenericProductName'");
                activeSheet.Range["M13"].Value2 = dr[0]["strValue"].ToString();
                dr = dsAnswers.Tables[0].Select("PropertyName = 'UnitInfoProcessType'");
                activeSheet.Range["M14"].Value2 = dr[0]["strValue"].ToString();
                dr = dsAnswers.Tables[0].Select("PropertyName = 'UnitName'");
                activeSheet.Range["M9"].Value2 = dr[0]["strValue"].ToString();
                string unitName = string.Empty;
                if (dr[0]["strValue"].ToString().Length > 31)
                    unitName = dr[0]["strValue"].ToString().Substring(0, 30);
                else
                    unitName = dr[0]["strValue"].ToString();
                //activeSheet.Name = dr[0]["strValue"].ToString().Replace(":", " ").Replace("/", " ").Replace("\\", " ").Replace("?", " ").Replace("*", " ").Replace("[", " ").Replace("]", " ");
                activeSheet.Name = unitName.Replace(":", " ").Replace("/", " ").Replace("\\", " ").Replace("?", " ").Replace("*", " ").Replace("[", " ").Replace("]", " ");
            }
            else
                rngFieldName = Globals.ThisAddIn.Application.Names.Item("SiteFieldNames").RefersToRange;

            for (x = 1; x <= rngFieldName.Rows.Count; x++)
            {
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 1].Value))
                {
                    PopulateCells(rngFieldName, dsAnswers, x, 12, 1);
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 2].Value))
                {
                    PopulateCells(rngFieldName, dsAnswers, x, 13, 2);
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 3].Value))
                {
                    PopulateCells(rngFieldName, dsAnswers, x, 14, 3);
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 4].Value))
                {
                    PopulateCells(rngFieldName, dsAnswers, x, 15, 4);
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 5].Value))
                {
                    PopulateCells(rngFieldName, dsAnswers, x, 16, 5);
                }
            }

        }
        #endregion

        #region Populate Cells
        public void PopulateCells(Excel.Range rngFieldName, DataSet dsAnswers, int rowNum, int colNum, int rngColNum)
        {
            DataView dv = new DataView(dsAnswers.Tables[0], "PropertyName = '" + rngFieldName.Cells[rowNum, rngColNum].Value.Trim() + "'", "PropertyName,UpdateDate", System.Data.DataViewRowState.CurrentRows);
            if (dv.Count > 0)
            {
                foreach (DataRowView drv in dv)
                {
                    try
                    {
                        rngFieldName.Cells[rowNum, colNum].Value2 = (drv["numValue"].ToString().Length > 0 ? drv["numValue"].ToString() : drv["strValue"].ToString());

                        if (drv["UpdateDate"].ToString() != "")
                        {
                            if (rngFieldName.Cells[rowNum, colNum].Comment == null)
                                rngFieldName.Cells[rowNum, colNum].AddComment((drv["UpdateNumValue"].ToString().Length > 0 ? drv["UpdateNumValue"].ToString() + ", " + drv["UpdateDate"].ToString() + ", " + drv["UpdateUserName"].ToString() + "\r\n" : drv["UpdateStrValue"].ToString() + ", " + drv["UpdateDate"].ToString() + ", " + drv["UpdateUserName"].ToString() + "\r\n"));
                            else
                            {
                                string strComment = rngFieldName.Cells[rowNum, colNum].Comment.Text.ToString();
                                rngFieldName.Cells[rowNum, colNum].Comment.Delete();
                                rngFieldName.Cells[rowNum, colNum].AddComment((drv["UpdateNumValue"].ToString().Length > 0 ? strComment + drv["UpdateNumValue"].ToString() + ", " + drv["UpdateDate"].ToString() + ", " + drv["UpdateUserName"].ToString() + "\r\n" : strComment + drv["UpdateStrValue"].ToString() + ", " + drv["UpdateDate"].ToString() + ", " + drv["UpdateUserName"].ToString() + "\r\n"));
                            }
                            rngFieldName.Cells[rowNum, colNum].Comment.Shape.TextFrame.AutoSize = true;
                        }
                    }
                    catch (Exception ex) { }
                }
            }
        }
        #endregion

        #region PullAnswers_OLD
        public void PullAnswers_OLD(int dsid, int sheet, byte[] salt, byte[] key, byte[] vector, bool isUnit, bool PrePop)
        {
            /*
            MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();
            byte[] byteAnswers;
            if (PrePop)
                byteAnswers = excelSvc.pushAnswersPrePop(dsid, salt, key, vector);
            else
                byteAnswers = excelSvc.pushAnswers(dsid, salt, key, vector);
            string strAnswers = cryptor.Decrypt(byteAnswers, salt.Length, key, vector);

            MemoryStream memStream = new MemoryStream((Encoding.ASCII.GetBytes(strAnswers)));
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            memStream.Position = 0;
            System.Data.DataSet dsAnswers = new System.Data.DataSet();
            dsAnswers.ReadXml(memStream);

            Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[sheet];

            if (isUnit)
            {
                try
                {
                    activeSheet.Range["M10"].Value2 = dsAnswers.Tables[0].Rows[0]["primaryproductname"].ToString();
                }
                catch (Exception ex) { }

                try
                {
                    activeSheet.Range["M12"].Value2 = dsAnswers.Tables[0].Rows[0]["processfamily"].ToString();
                }
                catch (Exception ex) { }

                try
                {
                    activeSheet.Range["M13"].Value2 = dsAnswers.Tables[0].Rows[0]["GenericProductName"].ToString();
                }
                catch (Exception ex) { }

                try
                {
                    activeSheet.Range["M14"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoProcessType"].ToString();
                }
                catch (Exception ex) { }

                try
                {
                    activeSheet.Name = dsAnswers.Tables[0].Rows[0]["UnitName"].ToString().Replace(":", " ").Replace("/", " ").Replace("\\", " ").Replace("?", " ").Replace("*", " ").Replace("[", " ").Replace("]", " ");
                }
                catch (Exception ex) { }

                try
                {
                    activeSheet.Range["M9"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitName"].ToString();
                }
                catch (Exception ex) { }


                try
                {
                    activeSheet.Range["M17"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoPlantAge"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M19"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoPRV"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N19"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoPRVUSD"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M21"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoMaxDailyProdRate"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N21"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoProdRateUOM"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M22"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoContinuosOrBatch"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M25"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoPhasePredominentFeedstock"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M26"].Value2 = dsAnswers.Tables[0].Rows[0]["UnitInfoPhasePredominentProduct"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M28"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityToxicity"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M29"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityPressure"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M30"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityTemperature"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M31"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityFlammability"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M32"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityExplosivity"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M33"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityCorrosivity"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M34"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityErosivity"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M35"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityProcessComplexity"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M36"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityViscosity"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M37"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityFreezePt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M38"].Value2 = dsAnswers.Tables[0].Rows[0]["SeverityConstMaterials"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M45"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntTurbines"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M46"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntTurbinesSpares"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M47"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntCompressorsRecip"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M48"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntCompressorsRecipSpares"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M49"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntCompressorsRotating"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M50"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntCompressorsRotatingSpares"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M51"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntPumpsCentrifugal"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M52"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntPumpsCentrifugalSpares"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M53"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntPumpsPosDisp"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M54"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntBlowersFans"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M55"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntRotaryDryers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M56"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntCrushers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M57"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntCentrifugesMain"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M58"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntAgitatorsInVessels"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M61"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntFurnaceBoilers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M62"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntVessels"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M63"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntDistTowers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M64"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntCoolingTowers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M65"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntStorageTanksISBL"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M66"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntHeatExch"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M67"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntHeatExchSpares"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M68"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntHeatExchOth"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M69"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntHeatExchFinFan"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M70"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntRefrigUnits"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M71"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntSilosISBL"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M72"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntSafetyValves"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M75"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntMotorsMain"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M76"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntVarSpeedDrives"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M77"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntISBLSubStationTransformer"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M78"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntDistVoltage"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M79"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntControlValves"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M80"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntAnalyzerProcCtrl"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M81"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntAnalyzerBlending"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M82"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCntAnalyzerEmissions"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M90"].Value2 = dsAnswers.Tables[0].Rows[0]["TAAvgHrsShutdown"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M91"].Value2 = dsAnswers.Tables[0].Rows[0]["TAAvgHrsExecution"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M92"].Value2 = dsAnswers.Tables[0].Rows[0]["TAAvgHrsStartup"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M93"].Value2 = dsAnswers.Tables[0].Rows[0]["TAAvgHrsDown"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M94"].Value2 = dsAnswers.Tables[0].Rows[0]["TAAvgInterval"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M95"].Value2 = dsAnswers.Tables[0].Rows[0]["TAAvgAnnHrsDown"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M100"].Value2 = dsAnswers.Tables[0].Rows[0]["ShortOHAvgHrsDown"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M101"].Value2 = dsAnswers.Tables[0].Rows[0]["ShortOH2YrCnt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M102"].Value2 = dsAnswers.Tables[0].Rows[0]["ShortOHAnnHrsDown"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M107"].Value2 = dsAnswers.Tables[0].Rows[0]["LossHrsForAnnTA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M108"].Value2 = dsAnswers.Tables[0].Rows[0]["LossHrsForShortOH"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M109"].Value2 = dsAnswers.Tables[0].Rows[0]["LossHrsForUnschedMaint"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M111"].Value2 = dsAnswers.Tables[0].Rows[0]["LossHrsForProcessReliability"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M112"].Value2 = dsAnswers.Tables[0].Rows[0]["LossHrsForTotalProdLoss"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M117"].Value2 = dsAnswers.Tables[0].Rows[0]["LostProductionOutages"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M118"].Value2 = dsAnswers.Tables[0].Rows[0]["LostProductionRateReductions"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M123"].Value2 = dsAnswers.Tables[0].Rows[0]["NonMaintOutagesMaintEquivHours"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M130"].Value2 = dsAnswers.Tables[0].Rows[0]["TACausePcnt_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N130"].Value2 = dsAnswers.Tables[0].Rows[0]["ShortOHCausePcnt_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O130"].Value2 = dsAnswers.Tables[0].Rows[0]["StoppagePcnt_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P130"].Value2 = dsAnswers.Tables[0].Rows[0]["RAMCausePcnt_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M131"].Value2 = dsAnswers.Tables[0].Rows[0]["TACausePcnt_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N131"].Value2 = dsAnswers.Tables[0].Rows[0]["ShortOHCausePcnt_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O131"].Value2 = dsAnswers.Tables[0].Rows[0]["StoppagePcnt_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P131"].Value2 = dsAnswers.Tables[0].Rows[0]["RAMCausePcnt_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M132"].Value2 = dsAnswers.Tables[0].Rows[0]["TACausePcnt_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N132"].Value2 = dsAnswers.Tables[0].Rows[0]["ShortOHCausePcnt_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O132"].Value2 = dsAnswers.Tables[0].Rows[0]["StoppagePcnt_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P132"].Value2 = dsAnswers.Tables[0].Rows[0]["RAMCausePcnt_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M133"].Value2 = dsAnswers.Tables[0].Rows[0]["TACausePcnt_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O133"].Value2 = dsAnswers.Tables[0].Rows[0]["StoppagePcnt_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N133"].Value2 = dsAnswers.Tables[0].Rows[0]["ShortOHCausePcnt_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P133"].Value2 = dsAnswers.Tables[0].Rows[0]["RAMCausePcnt_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M137"].Value2 = dsAnswers.Tables[0].Rows[0]["TARegulatoryInspect"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M138"].Value2 = dsAnswers.Tables[0].Rows[0]["TADiscretionaryInspect"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M139"].Value2 = dsAnswers.Tables[0].Rows[0]["TADeterioration"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M140"].Value2 = dsAnswers.Tables[0].Rows[0]["TAUnplannedFailure"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M141"].Value2 = dsAnswers.Tables[0].Rows[0]["TALowDemand"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M142"].Value2 = dsAnswers.Tables[0].Rows[0]["TAFixedSchedule"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M143"].Value2 = dsAnswers.Tables[0].Rows[0]["TACptlProject"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M144"].Value2 = dsAnswers.Tables[0].Rows[0]["TAContAvail"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M145"].Value2 = dsAnswers.Tables[0].Rows[0]["TAWeather"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M149"].Value2 = dsAnswers.Tables[0].Rows[0]["TAHeatExchOpened"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M150"].Value2 = dsAnswers.Tables[0].Rows[0]["TAControlValves"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M151"].Value2 = dsAnswers.Tables[0].Rows[0]["TAValvesCalibrated"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M152"].Value2 = dsAnswers.Tables[0].Rows[0]["TAFiredFurnaces"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M153"].Value2 = dsAnswers.Tables[0].Rows[0]["TAValvesOpened"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M154"].Value2 = dsAnswers.Tables[0].Rows[0]["TADistTowersOpened"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M155"].Value2 = dsAnswers.Tables[0].Rows[0]["TAPumps"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M156"].Value2 = dsAnswers.Tables[0].Rows[0]["TACompressors"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M159"].Value2 = dsAnswers.Tables[0].Rows[0]["TACritPath"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M166"].Value2 = dsAnswers.Tables[0].Rows[0]["TACritPathOth"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M170"].Value2 = dsAnswers.Tables[0].Rows[0]["RelProcessImproveProg_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N170"].Value2 = dsAnswers.Tables[0].Rows[0]["RelProcessImproveProg_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O170"].Value2 = dsAnswers.Tables[0].Rows[0]["RelProcessImproveProg_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M171"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramTimeBased_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N171"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramTimeBased_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O171"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramTimeBased_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M172"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramCBM_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N172"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramCBM_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O172"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramCBM_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M173"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramRCM_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N173"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramRCM_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O173"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramRCM_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M174"].Value2 = dsAnswers.Tables[0].Rows[0]["RiskMgmtEquipRBI_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N174"].Value2 = dsAnswers.Tables[0].Rows[0]["RiskMgmtEquipRBI_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O174"].Value2 = dsAnswers.Tables[0].Rows[0]["RiskMgmtEquipRBI_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M175"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCriticalityRanking_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N175"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCriticalityRanking_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O175"].Value2 = dsAnswers.Tables[0].Rows[0]["EquipCriticalityRanking_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M176"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramCUI_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N176"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramCUI_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O176"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramCUI_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M177"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramIRThermography_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N177"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramIRThermography_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O177"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramIRThermography_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M187"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramVibrationAnalysis_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M179"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramAcousticEmission_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N179"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramAcousticEmission_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O179"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramAcousticEmission_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M180"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramOilAnalysis_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O180"].Value2 = dsAnswers.Tables[0].Rows[0]["ProgramOilAnalysis_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M185"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsTurbines"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O185"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFTurbines"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M186"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsCompressorsRecip"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O186"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFCompressorsRecip"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M187"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsCompressorsRotating"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O187"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFCompressorsRotating"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M188"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsPumpsCentrifugal"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O188"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFPumpsCentrifugal"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M189"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsPumpsPosDisp"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O189"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFPumpsPosDisp"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M191"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsFurnaceBoilers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O191"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFFurnaceBoilers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M192"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsVessels"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O192"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFVessels"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M193"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsDistTowers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O193"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFDistTowers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M194"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsHeatExch"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O194"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFHeatExch"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M196"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsMotorsMain"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O196"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFMotorsMain"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M197"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsVarSpeedDrives"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O197"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFVarSpeedDrives"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M198"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsControlValves"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O198"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFControlValves"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M199"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsAnalyzerProcCtrl"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O199"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFAnalyzerProcCtrl"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M200"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsAnalyzerBlending"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O200"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFAnalyzerBlending"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M201"].Value2 = dsAnswers.Tables[0].Rows[0]["EventsAnalyzerEmissions"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O201"].Value2 = dsAnswers.Tables[0].Rows[0]["MTBFAnalyzerEmissions"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M209"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanWhrRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N209"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanWhrFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O209"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanWhrIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P209"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanWhrTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M210"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanMtCapWhrRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N210"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanMtCapWhrFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O210"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanMtCapWhrIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P210"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanMtCapWhrTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M211"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanTotWhrRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N211"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanTotWhrFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O211"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanTotWhrIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P211"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutCraftmanTotWhrTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M216"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanWhrRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N216"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanWhrFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O216"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanWhrIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P216"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanWhrTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M217"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanMtCapWhrRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N217"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanMtCapWhrFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O217"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanMtCapWhrIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P217"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanMtCapWhrTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M218"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanTotWhrRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N218"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanTotWhrFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O218"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanTotWhrIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P218"].Value2 = dsAnswers.Tables[0].Rows[0]["TACraftmanTotWhrTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M223"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N223"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O223"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P223"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M224"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlMtCapRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N224"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlMtCapFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O224"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlMtCapIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P224"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlMtCapTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M225"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlTotRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N225"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlTotFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O225"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlTotIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P225"].Value2 = dsAnswers.Tables[0].Rows[0]["RoutMatlTotTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M229"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N229"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O229"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P229"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M230"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlMtCapRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N230"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlMtCapFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O230"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlMtCapIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P230"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlMtCapTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M231"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlTotRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N231"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlTotFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O231"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlTotIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P231"].Value2 = dsAnswers.Tables[0].Rows[0]["AnnTAMatlTotTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M238"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskEmergency_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N238"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskEmergency_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O238"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskEmergency_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P238"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskEmergency_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M240"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskRoutCorrective_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N240"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskRoutCorrective_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O240"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskRoutCorrective_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P240"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskRoutCorrective_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M242"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskPreventive_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N242"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskPreventive_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O242"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskPreventive_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P242"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskPreventive_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M244"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskConditionMonitor_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N244"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskConditionMonitor_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O244"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskConditionMonitor_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P244"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskConditionMonitor_Tot"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M246"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskTotal_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N246"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskTotal_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O246"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskTotal_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P246"].Value2 = dsAnswers.Tables[0].Rows[0]["TaskTotal_Tot"].ToString();
                }
                catch (Exception ex) { }

                MessageBox.Show(activeSheet.Name + " complete.");
            }

            else
            {
                try
                {
                    activeSheet.Range["M8"].Value2 = dsAnswers.Tables[0].Rows[0]["CompanyName"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M9"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteName"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M10"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteCountry"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M11"].Value2 = dsAnswers.Tables[0].Rows[0]["StreetAddr1"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M12"].Value2 = dsAnswers.Tables[0].Rows[0]["MailAddr1"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M13"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMgr"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M14"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMgrEmail"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M15"].Value2 = dsAnswers.Tables[0].Rows[0]["MaintMgr"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M16"].Value2 = dsAnswers.Tables[0].Rows[0]["MaintMgrEmail"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M26"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteNumPersInt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M34"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteAreaUOM"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M35"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteAreaTotal"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M36"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteAreaUnit"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M39"].Value2 = dsAnswers.Tables[0].Rows[0]["LocalCurrency"].ToString();
                }
                catch (Exception ex) { }




                try
                {
                    activeSheet.Range["M53"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteSafetyCurrInjuryCntComp_RT"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M54"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteSafetyCurrInjuryCntCont_RT"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M55"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteSafetyCurrInjuryCntTot_RT"].ToString();
                }
                catch (Exception ex) { }



                try
                {
                    activeSheet.Range["M58"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyLCCA"].ToString();
                }
                catch (Exception ex) { }


                try
                {
                    activeSheet.Range["M40"].Value2 = dsAnswers.Tables[0].Rows[0]["ExchangeRate"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M43"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePRV"].ToString();
                }
                catch (Exception ex) { }
                try
                {
                    activeSheet.Range["M44"].Value2 = dsAnswers.Tables[0].Rows[0]["PRVCalcMethod"].ToString();
                }
                catch (Exception ex) { }


                try
                {
                    activeSheet.Range["M59"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyReliModeling"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M60"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyPlanDoc"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M61"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyObjectivesDoc"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M62"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyMgrObjectives"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M63"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyPerfIndicators"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M65"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyGoalsRamStaff"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M66"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyGoalsRamCraft"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M67"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyGoalsProdStaff"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M68"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyGoalsProdTechs"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M69"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyGoalsEngTech"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M70"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyGoalsSuppProc"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M71"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyPersonnelCert"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M72"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyBadActorList"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M73"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteStrategyMOC"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M80"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpCompLabor_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N80"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompLabor_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M81"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpContLabor_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N81"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContLabor_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M83"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpCompMatl_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N83"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompMatl_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M84"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpContMatl_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N84"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContMatl_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M89"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpIndirectCompSupport_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N89"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompIndirectCost_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M90"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpIndirectContSupport_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N90"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContIndirectCost_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N97"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpCompManHrs_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N98"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpContManHrs_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N102"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompManHrs_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N103"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContManHrs_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M111"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpCompLabor_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N111"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompLabor_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M112"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpContLabor_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N112"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContLabor_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M114"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpCompMatl_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N114"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompMatl_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M115"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpContMatl_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N115"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContMatl_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M120"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpIndirectCompSupport_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N120"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompIndirectCost_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M121"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpIndirectContSupport_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N121"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContIndirectCost_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N128"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpCompManHrs_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N129"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteExpContManHrs_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N133"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlCompManHrs_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N134"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteMCptlContManHrs_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M142"].Value2 = dsAnswers.Tables[0].Rows[0]["CostCatScaffolding_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N142"].Value2 = dsAnswers.Tables[0].Rows[0]["CostCatScaffolding_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M143"].Value2 = dsAnswers.Tables[0].Rows[0]["CostCatPainting_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N143"].Value2 = dsAnswers.Tables[0].Rows[0]["CostCatPainting_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M144"].Value2 = dsAnswers.Tables[0].Rows[0]["CostCatInsulation_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N144"].Value2 = dsAnswers.Tables[0].Rows[0]["CostCatInsulation_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M152"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirAvgTrainHrs_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N152"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirAvgTrainHrs_CRRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M153"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirSector_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N153"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirSector_CRRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M154"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirExperInJob_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N154"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirExperInJob_CRRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M157"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirAvgTrainHrs_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N157"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirAvgTrainHrs_CRFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M158"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirSector_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N158"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirSector_CRFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M159"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirExperInJob_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N159"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirExperInJob_CRFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M162"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirAvgTrainHrs_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N162"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirAvgTrainHrs_CRIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M163"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirSector_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N163"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirSector_CRIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M164"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirExperInJob_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N164"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirExperInJob_CRIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M167"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirLaborUnion_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N167"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirLaborUnion_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M168"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirOTPcnt_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N168"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirOTPcnt_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M169"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirRegsCert_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N169"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirRegsCert_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M170"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirRCARCFA_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N170"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirRCARCFA_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M171"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirReqDocFail_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N171"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkEnvirReqDocFail_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M198"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteOrgCompMaintOrg"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M199"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteOrgCompMaintOrgReportsTo"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M203"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteOrgOverallPlant"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M204"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteOrgPrimaryLeader"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M212"].Value2 = dsAnswers.Tables[0].Rows[0]["CorpPersMaintMgr_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N212"].Value2 = dsAnswers.Tables[0].Rows[0]["CorpPersMaintMgr_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M213"].Value2 = dsAnswers.Tables[0].Rows[0]["CorpPersRelEngr_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N213"].Value2 = dsAnswers.Tables[0].Rows[0]["CorpPersRelEngr_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M214"].Value2 = dsAnswers.Tables[0].Rows[0]["CorpPersMaintEngr_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N214"].Value2 = dsAnswers.Tables[0].Rows[0]["CorpPersMaintEngr_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M215"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintMgr_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N215"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintMgr_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M216"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintSupv_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N216"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintSupv_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M217"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintPlanner_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N217"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintPlanner_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M218"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintSched_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N218"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintSched_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M219"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersTechnical_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N219"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersTechnical_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M220"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintEngr_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N220"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintEngr_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M221"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintAdmin_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N221"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintAdmin_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M222"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintMSPMStaff_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N222"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintMSPMStaff_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M223"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintStoreroomMgrs_CO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N223"].Value2 = dsAnswers.Tables[0].Rows[0]["SitePersMaintStoreroomMgrs_CR"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M228"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteOrgUpperLevels"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M229"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteOrgLevelsGMtoCraft"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M230"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteOrgLevelsCEOtoCraft"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M178"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteSparesCatalogLineItemCnt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M179"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteSparesInventory"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M180"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteSparesCosignOrVendorPcnt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M181"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteSparesPlantStoresIssues"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M182"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgOpCost"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M184"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgPcntCriticalRank"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M185"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgPrevMaintStrRmItems"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M186"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgLCCAStrRmItems"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M187"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgPcntKitted"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M188"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgPcntDelivered"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M189"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgAvgStockOutPcnt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M190"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgClosedStoredPolicy"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M191"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgLowCostFreeIssue"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M192"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWhsOrgOutsourced"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M236"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteDocDefinedWorkflow_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N236"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteDocDefinedWorkflow_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M237"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteDocWorkflowFollowed_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N237"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteDocWorkflowFollowed_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M238"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteDocWorkflowExtAssessed_RT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N238"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteDocWorkflowExtAssessed_TA"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N242"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O242"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P242"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N243"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_PRORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O243"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_PROFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P243"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_PROIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N244"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_PRERE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O244"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_PREFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P244"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_PREIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N245"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_CAPRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O245"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_CAPFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P245"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalOrders_CAPIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N248"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O248"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P248"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N249"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_PRORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O249"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_PROFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P249"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_PROIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N250"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_PRERE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O250"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_PREFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P250"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_PREIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N251"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_CAPRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O251"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_CAPFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P251"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalPlanned_CAPIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N254"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O254"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P254"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N255"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_PRORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O255"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_PROFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P255"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_PROIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N256"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_PRERE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O256"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_PREFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P256"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_PREIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N257"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_CAPRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O257"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_CAPFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P257"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSched_CAPIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N260"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O260"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P260"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N261"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_PRORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O261"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_PROFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P261"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_PROIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N262"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_PRERE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O262"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_PREFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P262"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_PREIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N262"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_CAPRE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O263"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_CAPFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P263"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersTotalSchedCmpl_CAPIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N266"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersEmergencyOrders_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O266"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersEmergencyOrders_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P266"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersEmergencyOrders_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N270"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O270"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P270"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N271"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_PRORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O271"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_PROFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P271"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_PROIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N272"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_PRERE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O272"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_PREFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P272"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgPlanBacklog_PREIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N274"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_CORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O274"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_COFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P274"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_COIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N275"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_PRORE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O275"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_PROFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P275"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_PROIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N276"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_PRERE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O276"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_PREFP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P276"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersAvgReadyBacklog_PREIE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N278"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersLibraryStorage_COTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O278"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersLibraryStorage_PROTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P278"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersLibraryStorage_PRETOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N279"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersLibraryReport_COTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O279"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersLibraryReport_PROTOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["P279"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersLibraryReport_PRETOT"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N282"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersPcntCapacitySched"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N283"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteWorkOrdersFreqMeasureCmpl"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M294"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelCritEquipMeth_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N294"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelCritEquipMeth_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O294"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelCritEquipMeth_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M295"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelDowntimeAutoRec_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N295"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelDowntimeAutoRec_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O295"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelDowntimeAutoRec_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M296"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelLCCA_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N296"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelLCCA_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O296"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelLCCA_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M297"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelRCM_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N297"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelRCM_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O297"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelRCM_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M298"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelEquipEngrStd_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N298"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelEquipEngrStd_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O298"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelEquipEngrStd_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M299"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelRiskBasedInsp_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N299"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelRiskBasedInsp_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O299"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelRiskBasedInsp_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M300"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelFailureAnalysis_RE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["N300"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelFailureAnalysis_FP"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["O300"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelFailureAnalysis_IE"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M304"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelPIP5S"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M305"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelPIPTPM"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M306"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelPIP6SigmaGreenBelt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M307"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelPIP6SigmaBlackBelt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M310"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelPIPMonteCarloSim"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M311"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelPIPPoissonDist"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M312"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelPIPWeibullAnalysis"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M315"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelCMMSSuppRAMProcess"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M317"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelCMMSSuppPlanning"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M318"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelCMMSSuppScheduling"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M319"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteRelCMMSSuppMgmtChng"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M326"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteTATaken"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M328"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteTA10YearCnt"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M329"].Value2 = dsAnswers.Tables[0].Rows[0]["SiteTAAvgDaysDown"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M335"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyEffortDataCollectNumPers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M336"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyEffortDataCollectHrs"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M337"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyEffortDataEntryNumPers"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M338"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyEffortDataEntryHrs"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M344"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_Char"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M345"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_RTCost"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M346"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_TACost"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M347"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_MCptl"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M348"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_MHrs"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M349"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_MRO"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M350"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_MaintOrg"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M351"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_WorkProcess"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M352"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_Reliability"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M353"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_ProcessChar"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M354"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_EquipData"].ToString();
                }
                catch (Exception ex) { } try
                {
                    activeSheet.Range["M355"].Value2 = dsAnswers.Tables[0].Rows[0]["StudyDataCollectMethod_WorkTypes"].ToString();
                }
                catch (Exception ex) { }
            }
            hideLoginForm();
             */
        }
        #endregion

        #region btnNextPD Click Event
        void btnNextPDPrePop_Click(object sender, EventArgs e)
        {
            int i = 0;
            foreach (Control c in frmUnitSelection.Controls)
            {
                if (c is CheckBox)
                {
                    if (((CheckBox)c).Checked)
                    {
                        foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
                        {
                            if (Convert.ToString(worksheet.Range["M9"].Value2) == c.Text && worksheet.Range["D7"].Value2 == "Site")
                                sites[i] = worksheet.Index;
                            else if (Convert.ToString(worksheet.Range["M9"].Value2) == c.Text)
                                units[i] = worksheet.Index;
                            i += 1;
                        }
                        DataSetsToSend++;
                    }
                }
            }
            frmUnitSelection.Hide();
            PopulateLoginFormPDPrePop();
        }
        #endregion

        #region Create Unit Selection Form
        void CreatePullPrePopDataSelectionForm()
        {
            frmUnitSelection.Width = 390;
            frmUnitSelection.Height = 180;
            System.Drawing.Size sz = new System.Drawing.Size(390, 250);
            frmUnitSelection.MaximumSize = sz;
            frmUnitSelection.MinimumSize = sz;
            frmUnitSelection.VerticalScroll.Visible = true;
            frmUnitSelection.VerticalScroll.Enabled = true;
            frmUnitSelection.AutoScroll = true;
            frmUnitSelection.AutoScrollMinSize = new System.Drawing.Size(1, 1);
            frmUnitSelection.Text = "Unit Selection";
            Label lblTitle = new Label();
            lblTitle.Width = 350;
            System.Drawing.Font fontTitle = new System.Drawing.Font("Arial", 10);
            lblTitle.Font = fontTitle;
            lblTitle.Text = "Please select the site(s)/unit(s) you wish to pull data for:";
            frmUnitSelection.Font = new System.Drawing.Font("Arial ", 8);
            frmUnitSelection.Controls.Add(lblTitle);
            int n = 0;
            int y = 20;
            int i = Globals.ThisAddIn.Application.Worksheets.Count;
            foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
            {
                if ((worksheet.Range["D7"].Value2 == "Unit" || worksheet.Range["D7"].Value2 == "Site") && worksheet.Visible == Excel.XlSheetVisibility.xlSheetVisible)
                {
                    CheckBox[] _cb = new CheckBox[i];
                    _cb[n] = new CheckBox();
                    _cb[n].Name = worksheet.Range["M9"].Value2;
                    _cb[n].Text = worksheet.Range["M9"].Value2;
                    _cb[n].Top = y;
                    _cb[n].Width = 350;
                    frmUnitSelection.Controls.Add(_cb[n]);
                    y += 20;
                    n += 1;
                }

            }
            Button btnNext = new Button();
            btnNext.Text = "Next";
            btnNext.Top = y + 7;
            btnNext.Click += new EventHandler(btnNextPDPrePop_Click);

            Button btnUnitCancel = new Button();
            btnUnitCancel.Text = "Cancel";
            btnUnitCancel.Top = y + 7;
            btnUnitCancel.Left = btnNext.Left + 75;

            frmUnitSelection.Controls.Remove(btnNext);
            frmUnitSelection.Controls.Remove(btnUnitCancel);

            frmUnitSelection.Controls.Add(btnNext);
            frmUnitSelection.Controls.Add(btnUnitCancel);

            frmUnitSelection.StartPosition = FormStartPosition.CenterScreen;
            frmUnitSelection.ShowDialog();
        }
        #endregion

        #region Populate Login Form PD
        private void PopulateLoginFormPDPrePop()
        {
            frmLoginWindow.Font = new System.Drawing.Font("Arial", 8);
            frmLoginWindow.ControlBox = false;
            TextBox txtLoginUsername = new TextBox();
            TextBox txtLoginPassword = new TextBox();
            frmLoginWindow.Text = "Login";
            frmLoginWindow.Width = 230;
            frmLoginWindow.Height = 120;
            Label lblLoginUsername = new Label();
            lblLoginUsername.Text = "Username: ";
            lblLoginUsername.Top = lblLoginUsername.Top + 5;
            Label lblLoginPassword = new Label();
            lblLoginPassword.Text = "Password: ";
            lblLoginPassword.Top = lblLoginUsername.Top + 25;
            txtLoginUsername.Left = lblLoginUsername.Right + 5;
            txtLoginUsername.Top = lblLoginUsername.Top;
            txtLoginPassword.Left = txtLoginUsername.Left;
            txtLoginPassword.Top = lblLoginPassword.Top;
            txtLoginPassword.UseSystemPasswordChar = true;
            Button btnCancel = new Button();
            btnCancel.Text = "Cancel";
            btnCancel.Top = txtLoginPassword.Top + 24;
            btnCancel.Left = txtLoginPassword.Left + 25;

            Button btnSubmit = new Button();
            btnSubmit.Text = "Submit";
            btnSubmit.Top = btnCancel.Top;
            btnSubmit.Left = btnCancel.Left - 75;

            frmLoginWindow.Controls.Add(btnSubmit);
            frmLoginWindow.Controls.Add(btnCancel);
            frmLoginWindow.Controls.Add(txtLoginUsername);
            frmLoginWindow.Controls.Add(txtLoginPassword);
            frmLoginWindow.Controls.Add(lblLoginUsername);
            frmLoginWindow.Controls.Add(lblLoginPassword);
            frmLoginWindow.StartPosition = FormStartPosition.CenterScreen;
            System.Drawing.Size sz = new System.Drawing.Size(230, 120);
            frmLoginWindow.MaximumSize = sz;
            frmLoginWindow.MinimumSize = sz;

            btnSubmit.Click += new EventHandler(btnPullDataNextPrePop_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);

            showLoginForm();
            txtLoginUsername.Focus();
        }
        #endregion

        private void btnPullDataNextPrePop_Click(object sender, EventArgs e)
        {
            username = ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[2].Text;
            password = ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[3].Text;

            if (username == "" || password == "")
            {
                System.Windows.Forms.MessageBox.Show(frmLoginWindow, "Please enter username and password", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            MyExcelService.Service1Client excelSvc = new MyExcelService.Service1Client();

            MyExcelService.Handshake handshake = new MyExcelService.Handshake();
            handshake = excelSvc.InitiateHandshake();

            handshake.Value1 = cryptor.Encrypt(username + "|" + password, handshake.Value2, handshake.Value3, handshake.Value4);
            handshake = excelSvc.CompleteHandshake(handshake);
            if (handshake.Status)
            {
                var selectedSites = sites.Where(item => item != null);
                foreach (int value in selectedSites)
                {
                    int dsid = Convert.ToInt32(Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2);
                    dsSiteUnits = excelSvc.getSiteUnits(dsid);
                    PullAnswers(dsid, value, handshake.Value2, handshake.Value3, handshake.Value4, false, true);
                }

                var selectedUnits = units.Where(item => item != null);
                foreach (int value in selectedUnits)
                {
                    int dsid = Convert.ToInt32(Globals.ThisAddIn.Application.Worksheets[value].Range["C7"].Value2);
                    PullAnswers(dsid, value, handshake.Value2, handshake.Value3, handshake.Value4, true, true);
                }

                if (selectedUnits.Count() == 0)
                {
                    
                    foreach (DataRow dr in dsSiteUnits.Tables[0].Rows)
                    {
                        Excel.Worksheet worksheet1 = ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[4]);
                        worksheet1.Unprotect("AABAABAAAAA>");
                        worksheet1.Range["C7"].Value2 = dr["DatasetID"].ToString();
                        worksheet1.Copy(System.Type.Missing, ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[4]));
                        PullAnswers(Convert.ToInt32(worksheet1.Range["C7"].Value2), worksheet1.Index + 1, handshake.Value2, handshake.Value3, handshake.Value4, true, false);
                        ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[worksheet1.Index + 1]).Protect("AABAABAAAAA>", System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing);
                    }
                    Excel.Worksheet worksheet2 = ((Excel.Worksheet)Globals.ThisAddIn.Application.Worksheets[4]);
                    worksheet2.Visible = Excel.XlSheetVisibility.xlSheetHidden;
                }
                selectedUnits = null;
                Array.Clear(units, 0, units.Length);
                selectedSites = null;
                Array.Clear(sites, 0, sites.Length);

            }
            else
            {
                MessageBox.Show(frmLoginWindow, "Invalid username/password", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[2].Text = "";
            ((System.Windows.Forms.ButtonBase)(sender)).Parent.Controls[3].Text = "";
        }


        private void button1_Click_1(object sender, RibbonControlEventArgs e)
        {
            Microsoft.Office.Interop.Excel.Worksheet activeSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.Sheets[1];
            SiteData sd = new SiteData();

            Excel.Range cell;// = Excel.Range();
            Excel.Range rngFieldName = Globals.ThisAddIn.Application.Names.Item("FieldNames").RefersToRange;
            //rngFieldName = activeSheet.Range["FieldName"];
            var value = new object();

            int x = 1;
            int q = 11;

            for (x = 1; x <= rngFieldName.Rows.Count; x++)
            {
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 1].Value))
                {
                    var cellValue = rngFieldName.Cells[x, 12].Value;
                    if (cellValue != null)
                    {
                        value = rngFieldName.Cells[x, 12].Value2;
                        System.Reflection.PropertyInfo pi = sd.GetType().GetProperty(rngFieldName.Cells[x, 1].Value);
                        pi.SetValue(sd, value, null);
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 2].Value))
                {
                    var cellValue = rngFieldName.Cells[x, 13].Value;
                    if (cellValue != null)
                    {
                        value = rngFieldName.Cells[x, 13].Value2;
                        System.Reflection.PropertyInfo pi = sd.GetType().GetProperty(rngFieldName.Cells[x, 2].Value);
                        pi.SetValue(sd, value, null);
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 3].Value))
                {
                    var cellValue = rngFieldName.Cells[x, 14].Value;
                    if (cellValue != null)
                    {
                        value = rngFieldName.Cells[x, 14].Value2;
                        System.Reflection.PropertyInfo pi = sd.GetType().GetProperty(rngFieldName.Cells[x, 3].Value);
                        pi.SetValue(sd, value, null);
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 4].Value))
                {
                    var cellValue = rngFieldName.Cells[x, 15].Value;
                    if (cellValue != null)
                    {
                        value = rngFieldName.Cells[x, 15].Value2;
                        System.Reflection.PropertyInfo pi = sd.GetType().GetProperty(rngFieldName.Cells[x, 4].Value);
                        pi.SetValue(sd, value, null);
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 5].Value))
                {
                    var cellValue = rngFieldName.Cells[x, 16].Value;
                    if (cellValue != null)
                    {
                        value = rngFieldName.Cells[x, 16].Value2;
                        System.Reflection.PropertyInfo pi = sd.GetType().GetProperty(rngFieldName.Cells[x, 5].Value);
                        pi.SetValue(sd, value, null);
                    }
                }
            }

            XmlDocument xmldoc = new XmlDocument();
            using (MemoryStream stream = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                XmlWriter xmlWriter = XmlWriter.Create(stream, settings);
                using (xmlWriter)
                {
                    xmlWriter.WriteStartDocument();
                    new System.Xml.Serialization.XmlSerializer(typeof(SiteData)).Serialize(xmlWriter, sd);
                }
                xmlWriter.Flush();
                stream.Position = 0;
                xmldoc.Load(stream);
            }


            //populate
            System.Data.DataSet dsAnswers = new System.Data.DataSet();
            System.Data.DataTable dt = new System.Data.DataTable();

            dt.Columns.Add("companyname");
            dt.Rows.Add("Shell");
            dsAnswers.Tables.Add();
            dsAnswers.Tables[0].Columns.Add("companyname");
            dsAnswers.Tables[0].ImportRow(dt.Rows[0]);

            for (x = 1; x <= rngFieldName.Rows.Count; x++)
            {
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 1].Value))
                {
                    if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 1].Value))
                    {
                        try
                        {
                            rngFieldName.Cells[x, 12].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 1].Value].ToString();
                        }
                        catch (Exception ex) { }
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 2].Value))
                {
                    if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 2].Value))
                    {
                        try
                        {
                            rngFieldName.Cells[x, 13].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 2].Value].ToString();
                        }
                        catch (Exception ex) { }
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 3].Value))
                {
                    if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 3].Value))
                    {
                        try
                        {
                            rngFieldName.Cells[x, 14].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 3].Value].ToString();
                        }
                        catch (Exception ex) { }
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 4].Value))
                {
                    if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 4].Value))
                    {
                        try
                        {
                            rngFieldName.Cells[x, 15].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 4].Value].ToString();
                        }
                        catch (Exception ex) { }
                    }
                }
                if (!string.IsNullOrEmpty(rngFieldName.Cells[x, 5].Value))
                {
                    if (dsAnswers.Tables[0].Columns.Contains(rngFieldName.Cells[x, 5].Value))
                    {
                        try
                        {
                            rngFieldName.Cells[x, 16].Value2 = dsAnswers.Tables[0].Rows[0][rngFieldName.Cells[x, 5].Value].ToString();
                        }
                        catch (Exception ex) { }
                    }
                }
            }

           

        }

       

        private void btnDeleteComments_Click(object sender, RibbonControlEventArgs e)
        {
            foreach (Excel.Worksheet worksheet in Globals.ThisAddIn.Application.Worksheets)
            {
                foreach (Excel.Comment xComment in worksheet.Comments)
                {
                    xComment.Delete();
                }
            }
    
        }







    }
    
}
