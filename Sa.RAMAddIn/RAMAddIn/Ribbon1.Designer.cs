﻿namespace RAMAddIn
{
    partial class Ribbon1 : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon1()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RAM = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.lblRibbonDesc = this.Factory.CreateRibbonLabel();
            this.btnRibbonSubmit = this.Factory.CreateRibbonButton();
            this.btnPullData = this.Factory.CreateRibbonButton();
            this.btnPlaceHolder = this.Factory.CreateRibbonButton();
            this.btnPullPrePop = this.Factory.CreateRibbonButton();
            this.btnDeleteComments = this.Factory.CreateRibbonButton();
            this.btnPlaceHolder2 = this.Factory.CreateRibbonButton();
            this.button1 = this.Factory.CreateRibbonButton();
            this.lblPlaceHolder = this.Factory.CreateRibbonLabel();
            this.RAM.SuspendLayout();
            this.group1.SuspendLayout();
            // 
            // RAM
            // 
            this.RAM.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.RAM.Groups.Add(this.group1);
            this.RAM.Label = "TabAddIns";
            this.RAM.Name = "RAM";
            // 
            // group1
            // 
            this.group1.Items.Add(this.lblRibbonDesc);
            this.group1.Items.Add(this.btnRibbonSubmit);
            this.group1.Items.Add(this.btnPullData);
            this.group1.Items.Add(this.btnPlaceHolder);
            this.group1.Items.Add(this.btnPullPrePop);
            this.group1.Items.Add(this.btnDeleteComments);
            this.group1.Items.Add(this.btnPlaceHolder2);
            this.group1.Items.Add(this.button1);
            this.group1.Items.Add(this.lblPlaceHolder);
            this.group1.Label = "group1";
            this.group1.Name = "group1";
            // 
            // lblRibbonDesc
            // 
            this.lblRibbonDesc.Label = "HSB Solomon Associates RAM Study";
            this.lblRibbonDesc.Name = "lblRibbonDesc";
            // 
            // btnRibbonSubmit
            // 
            this.btnRibbonSubmit.Label = "Submit Data";
            this.btnRibbonSubmit.Name = "btnRibbonSubmit";
            this.btnRibbonSubmit.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnRibbonSubmit_Click);
            // 
            // btnPullData
            // 
            this.btnPullData.Label = "Pull Data";
            this.btnPullData.Name = "btnPullData";
            this.btnPullData.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnPullData_Click);
            // 
            // btnPlaceHolder
            // 
            this.btnPlaceHolder.Label = "button2";
            this.btnPlaceHolder.Name = "btnPlaceHolder";
            this.btnPlaceHolder.ShowLabel = false;
            // 
            // btnPullPrePop
            // 
            this.btnPullPrePop.Label = "Pull Data  (Pre-Populated Only)";
            this.btnPullPrePop.Name = "btnPullPrePop";
            this.btnPullPrePop.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnPullPrePop_Click);
            // 
            // btnDeleteComments
            // 
            this.btnDeleteComments.Label = "Delete Comments";
            this.btnDeleteComments.Name = "btnDeleteComments";
            this.btnDeleteComments.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnDeleteComments_Click);
            // 
            // btnPlaceHolder2
            // 
            this.btnPlaceHolder2.Label = "button2";
            this.btnPlaceHolder2.Name = "btnPlaceHolder2";
            this.btnPlaceHolder2.ShowLabel = false;
            // 
            // button1
            // 
            this.button1.Label = "TODO";
            this.button1.Name = "button1";
            this.button1.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.button1_Click_1);
            // 
            // lblPlaceHolder
            // 
            this.lblPlaceHolder.Label = " ";
            this.lblPlaceHolder.Name = "lblPlaceHolder";
            // 
            // Ribbon1
            // 
            this.Name = "Ribbon1";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.RAM);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon1_Load);
            this.RAM.ResumeLayout(false);
            this.RAM.PerformLayout();
            this.group1.ResumeLayout(false);
            this.group1.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab RAM;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblRibbonDesc;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnRibbonSubmit;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnPullData;
        internal Microsoft.Office.Tools.Ribbon.RibbonLabel lblPlaceHolder;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnPullPrePop;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton button1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnDeleteComments;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnPlaceHolder;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnPlaceHolder2;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon1 Ribbon1
        {
            get { return this.GetRibbon<Ribbon1>(); }
        }
    }
}
