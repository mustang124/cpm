﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace Sa
{
	class Common
	{
		internal static Excel.Application NewExcelApplication()
		{
			Excel.Application xla = new Excel.Application();

			xla.AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable;
			xla.AskToUpdateLinks = false;
			xla.DisplayAlerts = false;
			xla.FileValidation = Microsoft.Office.Core.MsoFileValidationMode.msoFileValidationSkip;
			xla.Interactive = false;
			xla.ScreenUpdating = false;
			xla.Visible = false;
			xla.EnableEvents = false;

			return xla;
		}

		internal static Excel.Workbook OpenWorkbook_ReadOnly(Excel.Application xla, String PathFile)
		{
			PathFile = System.IO.Path.GetFullPath(PathFile);
			Excel.Workbook wkb = xla.Workbooks.Open(PathFile, Excel.XlUpdateLinks.xlUpdateLinksNever, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, false, Type.Missing, Excel.XlCorruptLoad.xlNormalLoad);
			wkb.EnableAutoRecover = false;

			return wkb;
		}

		internal static void CloseExcel(ref Excel.Application xla, ref Excel.Workbook wkb)
		{
			if (wkb != null)
			{
				wkb.Close(false);
				Marshal.FinalReleaseComObject(wkb);
				wkb = null;
			}

			if (xla != null)
			{
				xla.Quit();
				Marshal.FinalReleaseComObject(xla);
				xla = null;
			}

			System.GC.WaitForPendingFinalizers();
			System.GC.Collect();
			System.GC.WaitForFullGCComplete();
		}

		internal static Boolean RangeHasValue(Excel.Range rng)
		{
			string s = string.Empty;

			foreach (Excel.Range r in rng.Cells)
			{
				s = Convert.ToString(r.Text).Trim();

				if (s.Length > 0) s = Convert.ToString(r.Value2).Trim();

				if (s.Length > 0) return true;
			}
			return false;
		}

		#region Return Functions

		internal static string ReturnString(Excel.Range rng, UInt32 FieldLength)
		{
			try
			{
				string t = Convert.ToString(rng.Value2).Trim();
				return t.Substring(0, Math.Min(t.Length, (int)FieldLength));
			}
			catch
			{
				return string.Empty;
			}
		}

		internal static Single ReturnSingle(Excel.Range rng)
		{
			try
			{
				return (Single)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return Single.Parse(s);
			}
		}

		internal static double ReturnDouble(Excel.Range rng)
		{
			try
			{
				return (double)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return double.Parse(s);
			}
		}

		internal static DateTime ReturnDateTime(Excel.Range rng)
		{
			try
			{
				return (DateTime)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value).Trim();
				return DateTime.Parse(s);
			}
		}

		internal static byte ReturnByte(Excel.Range rng, Boolean ConvertTrueFalse = false)
		{
			if (ConvertTrueFalse)
			{
				byte b = 0;

				string s = Convert.ToString(rng.Value2).Substring(0, 1).ToUpper();

				if (s == "T" || s == "X" || s == "Y" || s == "1" || s == "-") b = 1;

				return b;
			}
			else
			{
				try
				{
					return (byte)rng.Value2;
				}
				catch
				{
					string s = Convert.ToString(rng.Value2).Trim();
					return byte.Parse(s);
				}
			}
		}

		internal static ushort ReturnUShort(Excel.Range rng)
		{
			try
			{
				return (ushort)rng.Value2;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return ushort.Parse(s);
			}
		}

		internal static short ReturnInt16(Excel.Range rng)
		{
			try
			{
				return (short)rng.Value2;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return short.Parse(s);
			}
		}

		internal static int ReturnInt32(Excel.Range rng)
		{
			try
			{
				return (int)rng.Value2;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return int.Parse(s);
			}
		}

		#endregion
	}
}
