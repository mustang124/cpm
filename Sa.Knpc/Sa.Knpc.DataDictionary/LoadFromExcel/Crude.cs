﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Crude(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Crude"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Crude.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.CrudeRow dr = dd.Crude.NewCrudeRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.Period = Common.ReturnString(rng, 2);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.CrudeID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Cnum = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.CrudeName = Common.ReturnString(rng, 50);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.BBL = Common.ReturnDouble(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Gravity = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Sulfur = Common.ReturnSingle(rng);

					dd.Crude.AddCrudeRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Crude.AcceptChanges();
				dd.Crude.EndLoadData();
			}
		}
	}
}