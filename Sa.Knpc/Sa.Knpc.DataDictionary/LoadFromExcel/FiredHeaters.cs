﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void FiredHeaters(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["FiredHeaters"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.FiredHeaters.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.FiredHeatersRow dr = dd.FiredHeaters.NewFiredHeatersRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.HeaterNo = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.UnitID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.ProcessID = Common.ReturnString(rng, 8);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.HeaterName = Common.ReturnString(rng, 50);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Service = Common.ReturnString(rng, 6);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ProcessFluid = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ThroughputRpt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ThroughputUOM = Common.ReturnString(rng, 12);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.FiredDuty = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.FuelType = Common.ReturnString(rng, 4);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.OthCombDuty = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.FurnInTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.FurnOutTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.StackTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.StackO2 = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.HeatLossPcnt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.CombAirTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.AbsorbedDuty = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ProcessDuty = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.SteamDuty = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.SteamSuperHeated = Common.ReturnString(rng, 1);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ShaftDuty = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.OtherDuty = Common.ReturnSingle(rng);

					dd.FiredHeaters.AddFiredHeatersRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.FiredHeaters.AcceptChanges();
				dd.FiredHeaters.EndLoadData();
			}
		}
	}
}