﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void MaintTA(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["MaintTA"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.MaintTA.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.MaintTARow dr = dd.MaintTA.NewMaintTARow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.UnitID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.ProcessID = Common.ReturnString(rng, 8);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TADate = Common.ReturnDateTime(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAHrsDown = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TACostLocal = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAExpLocal = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TACptlLocal = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAOvhdLocal = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TALaborCostLocal = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAOCCSTH = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAOCCOVT = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAMPSSTH = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAMPSOVTPcnt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAContOCC = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAContMPS = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.PrevTADate = Common.ReturnDateTime(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TAExceptions = Common.ReturnInt16(rng);

					dd.MaintTA.AddMaintTARow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.MaintTA.AcceptChanges();
				dd.MaintTA.EndLoadData();
			}
		}
	}
}