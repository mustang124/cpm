﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void ConfigBuoy(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["ConfigBuoy"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.ConfigBuoy.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.ConfigBuoyRow dr = dd.ConfigBuoy.NewConfigBuoyRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.UnitID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.ProcessID = Common.ReturnString(rng, 8);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.UnitName = Common.ReturnString(rng, 50);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ShipCap = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.LineSize = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.PcntOwnership = Common.ReturnSingle(rng);

					dd.ConfigBuoy.AddConfigBuoyRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.ConfigBuoy.AcceptChanges();
				dd.ConfigBuoy.EndLoadData();
			}
		}
	}
}