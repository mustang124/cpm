﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Emissions(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Emissions"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Emissions.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.EmissionsRow dr = dd.Emissions.NewEmissionsRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.EmissionType = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RefEmissions = Common.ReturnSingle(rng);

					dd.Emissions.AddEmissionsRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Emissions.AcceptChanges();
				dd.Emissions.EndLoadData();
			}
		}
	}
}