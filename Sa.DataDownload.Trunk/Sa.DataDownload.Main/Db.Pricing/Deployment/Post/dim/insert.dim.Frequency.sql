﻿PRINT 'insert.dim.Frequency...';

DECLARE @Frequency TABLE
(
	[FQ_Tag]			CHAR(2)				NOT	NULL	CHECK([FQ_Tag] <> ''),
														UNIQUE CLUSTERED([FQ_Tag] ASC),

	[FQ_Name]			NVARCHAR(14)		NOT	NULL	CHECK([FQ_Name] <> '')
														UNIQUE([FQ_Name] ASC),

	[FQ_Detail]			NVARCHAR(30)		NOT	NULL	CHECK([FQ_Detail] <> '')
														UNIQUE([FQ_Detail] ASC),

	[FQ_Parent]			CHAR(2)				NOT	NULL	CHECK([FQ_Parent] <> '')
);

INSERT INTO @Frequency([FQ_Tag], [FQ_Name], [FQ_Detail], [FQ_Parent])
SELECT [t].[FQ_Tag], [t].[FQ_Name], [t].[FQ_Detail], [t].[FQ_Parent]
FROM (VALUES
	('yy', 'Yearly', 'Yearly', 'yy'),
	('qq', 'Quarterly', 'Quarterly', 'qq'),
	('mm', 'Monthly', 'Monthly', 'mm'),
	('wk', 'Weekly', 'Weekly', 'wk'),
	('dd', 'Daily', 'Daily', 'dd'),
	
	('fn', 'Fortnightly', 'Fortnightly', 'fn'),

	('sy', 'Semi-Yearly', 'Semi-Yearly (twice per year)', 'sy'),
	('sm', 'Semi-Monthly', 'Semi-Monthly (twice per month)', 'sm'),
	('sw', 'Semi-Weekly', 'Semi-Weekly (twice per week)', 'sw'),

	('by', 'Bi-Yearly', 'Bi-Yearly (every other year)', 'by'),
	('bm', 'Bi-Monthly', 'Bi-Monthly (every other month)', 'bm'),
	('bw', 'Bi-Weekly', 'Bi-Weekly (every other week)', 'bw'),

	('d7', 'Daily (7 days)', 'Daily (7 days)', 'd7'),
	('d5', 'Daily (5 days)', 'Daily (weekday)', 'd5'),

	('id', 'Intraday', 'Intraday', 'id'),

	('hh', 'Hourly', 'Hourly', 'hh')

	)[t]([FQ_Tag], [FQ_Name], [FQ_Detail], [FQ_Parent]);

INSERT INTO [dim].[FQ_Frequency_LookUp]
(
	[FQ_Tag],
	[FQ_Name],
	[FQ_Detail]
)
SELECT
	[t].[FQ_Tag],
	[t].[FQ_Name],
	[t].[FQ_Detail]
FROM
	@Frequency					[t]
LEFT OUTER JOIN
	[dim].[FQ_Frequency_LookUp]	[x]
		ON	[x].[FQ_Tag]	= [t].[FQ_Tag]
WHERE
	[x].[FQ_FrequencyId]		IS NULL;