﻿CREATE VIEW [etl].[MP_Argus]
WITH SCHEMABINDING
AS
SELECT
	[p].[MP_Code],
		[MP_CommodityId]			= [c].[CO_CommodityId],
	[p].[MP_FrequencyId],
	[p].[MP_CurrencyId],
	[p].[MP_CurrencyMultiplier],
	[p].[MP_UomId],
	[p].[MP_UomMultiplier]
FROM
	[etl].[MP_Argus_Posit]			[p]
INNER JOIN
	[dim].[CO_Commodity_LookUp]		[c]
		ON	[c].[CO_Tag]			= [p].[MP_Code];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__MP_Argus]
ON [etl].[MP_Argus]
(
	[MP_Code]			ASC
)
WITH(FILLFACTOR = 95);
GO