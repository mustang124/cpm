﻿CREATE VIEW [stage].[PA_PricingArgusCsv_Posit_Pivot]
WITH SCHEMABINDING
AS
SELECT
	[t].[PA_FilePath],
	[t].[PA_Code],
	[t].[PA_Date],
		[PA_ChangedAt]		= CONVERT(DATETIMEOFFSET(7), [t].[PA_Date] + ' ' + [t].[PA_Time]),
	[t].[PA_PositReliability],

		[PA_Open]				= [t].[open],
		[PA_Close]				= [t].[settlement],
		[PA_Low]				= [t].[value low],
		[PA_High]				= [t].[value high],
		[PA_Volume]				= [t].[volume],

		[PA_Index]				= [t].[index],
		[PA_Mean]				= [t].[average],
		[PA_Ask]				= [t].[system ask],
		[PA_Bid]				= [t].[system bid],

		[PA_Rate]				= [t].[rate],
		[PA_Margin]				= [t].[margin],

		[PA_DiffLow]			= [t].[diff low],
		[PA_DiffHigh]			= [t].[diff high],
		[PA_DiffIndex]			= [t].[diff index],
		[PA_DiffMidpoint]		= [t].[diff midpoint],

		[PA_Midpoint]			= [t].[midpoint],
		[PA_Netback]			= [t].[netback],
		[PA_NetbackMargin]		= [t].[netback margin],

		[PA_RGV]				= [t].[RGV],
		[PA_CumulativeIndex]	= [t].[cumulative index],
		[PA_CumulativeVolume]	= [t].[cumulative volume],
		[PA_TransportCosts]		= [t].[transport costs]
FROM (
	SELECT
		[a].[PA_FilePath],
		[a].[PA_Code],
			[PA_Date]		= CONVERT(CHAR(10), [a].[PA_Date]),
			[PA_Time]		= CASE [a].[PA_TimeStampId]
				WHEN  0 THEN '00:00:00 +00:00'	--
				WHEN  1 THEN '12:00:00 +00:00'	--	London midday
				WHEN  2 THEN '17:00:00 -06:00'	--	Houston close
				WHEN  6 THEN '17:30:00 +00:00'	--	London close
				WHEN  8 THEN '18:30:00 +08:00'	--	Singapore close
				WHEN  9 THEN '17:00:00 -05:00'	--	Exchange settlement (NYMEX Close)
				WHEN 13 THEN '09:00:00 +00:00'	--	London open
				WHEN 16 THEN '15:30:00 +09:00'	--	TOCOM close
				WHEN 17 THEN '11:00:00 +00:00'	--	Polish electricity close
				WHEN 21 THEN '17:00:00 -05:00'	--	Washington close
				WHEN 22 THEN '10:00:00 -05:00'	--	US day-ahead options close
				WHEN 23 THEN '10:00:00 -05:00'	--	US day-ahead options close
				WHEN 24 THEN '00:00:00 +00:00'	--	London midnight
				WHEN 26 THEN '17:30:00 +03:00'	--	Moscow close
				WHEN 27 THEN '12:00:00 -06:00'	--	Houston midday
				WHEN 28 THEN '13:30:00 -06:00'	--	Houston 13:30
				WHEN 29 THEN '14:30:00 -06:00'	--	Houston 14:30
				ELSE NULL
				END,
		[a].[PA_PositReliability],
		[a].[PA_Value],
		[a].[PA_RecordStatus],
		[p].[AR_Description]
	FROM
		[stage].[PA_PricingArgusCsv_Posit]	[a]
	INNER JOIN
		[ref].[AR_PriceType_Posit]			[p]
			ON	[p].[AR_PriceTypeId]	= [a].[PA_PriceTypeId]
) [p]
PIVOT (
	MAX([p].[PA_Value]) FOR [p].[AR_Description] IN
	(
		[value low],
		[value high],
		[diff midpoint],
		[index],
		[settlement],
		[diff low],
		[diff high],
		[midpoint],
		[open],
		[average],
		[netback],
		[RGV],
		[rate],
		[margin],
		[volume],
		[system bid],
		[system ask],
		[cumulative index],
		[cumulative volume],
		[diff index],
		[netback margin],
		[transport costs]
	)
) [t];