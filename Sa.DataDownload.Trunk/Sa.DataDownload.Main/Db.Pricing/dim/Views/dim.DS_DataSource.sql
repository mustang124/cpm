﻿CREATE VIEW [dim].[DS_DataSource]
WITH SCHEMABINDING
AS
SELECT
	[b].[DS_MethodologyId],
	[b].[DS_DataSourceId],
	[a].[DS_SortKey],
	[a].[DS_Hierarchy],
	[b].[DS_DescendantId],
	[d].[DS_Operator]
FROM [dim].[DS_DataSource_Bridge]			[b]
INNER JOIN [dim].[DS_DataSource_Parent]		[a]
	ON	[a].[DS_MethodologyId]	= [b].[DS_MethodologyId]
	AND	[a].[DS_DataSourceId]	= [b].[DS_DataSourceId]
INNER JOIN [dim].[DS_DataSource_Parent]		[d]
	ON	[d].[DS_MethodologyId]	= [b].[DS_MethodologyId]
	AND	[d].[DS_DataSourceId]	= [b].[DS_DescendantId];
GO

--CREATE UNIQUE CLUSTERED INDEX [PK__DS_DataSource]
--ON [dim].[DS_DataSource]
--(
--	[DS_MethodologyId]		ASC,
--	[DS_DataSourceId]		ASC,
--	[DS_DescendantId]		ASC
--);
--GO