﻿CREATE TABLE [fact].[PR_VAL_PricingCommodity_Value_Posit]
(
	[PR_VAL_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[PR_VAL_PR_ID]				INT					NOT	NULL	CONSTRAINT [FK__PR_VAL_PricingCommodity_Value_Posit_PR_ID]				REFERENCES [fact].[PR_PricingCommodity]([PR_ID]),

	[PR_VAL_Open]				FLOAT					NULL,
	[PR_VAL_Close]				FLOAT					NULL,
	[PR_VAL_Low]				FLOAT					NULL,
	[PR_VAL_High]				FLOAT					NULL,
	[PR_VAL_Volume]				FLOAT					NULL,

	[PR_VAL_Checksum]			AS BINARY_CHECKSUM([PR_VAL_Open], [PR_VAL_Close], [PR_VAL_Low], [PR_VAL_High], [PR_VAL_Volume])
								PERSISTED			NOT	NULL,
	[PR_VAL_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Posit_ChangedAt]			DEFAULT(SYSDATETIMEOFFSET()),

	[PR_VAL_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Posit_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PR_VAL_PricingCommodity_Value_Posit_RowGuid]			UNIQUE NONCLUSTERED([PR_VAL_RowGuid]),

	CONSTRAINT [PK__PR_VAL_PricingCommodity_Value_Posit] PRIMARY KEY([PR_VAL_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__PR_VAL_PricingCommodity_Value_Posit] UNIQUE CLUSTERED([PR_VAL_PR_ID] ASC, [PR_VAL_ChangedAt] DESC, [PR_VAL_Checksum] ASC)
		WITH(FILLFACTOR = 95)
);
GO

CREATE TRIGGER [fact].[PR_VAL_PricingCommodity_Value_Posit_Delete]
ON [fact].[PR_VAL_PricingCommodity_Value_Posit]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_VAL_PricingCommodity_Value_Posit].', 16, 1);
		ROLLBACK;
	END;

END;
GO