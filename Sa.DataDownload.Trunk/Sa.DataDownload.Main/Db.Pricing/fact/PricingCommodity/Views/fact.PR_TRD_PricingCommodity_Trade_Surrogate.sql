﻿CREATE VIEW [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
WITH SCHEMABINDING
AS
SELECT
	[p].[PR_TRD_ID],
	[p].[PR_TRD_PR_ID],
	[p].[PR_TRD_Index],
	[p].[PR_TRD_Mean],
	[p].[PR_TRD_Ask],
	[p].[PR_TRD_Bid],
	[p].[PR_TRD_PrevInterest],
	[p].[PR_TRD_Rate],
	[p].[PR_TRD_Margin],
	[p].[PR_TRD_DiffLow],
	[p].[PR_TRD_DiffHigh],
	[p].[PR_TRD_DiffIndex],
	[p].[PR_TRD_DiffMidpoint],
	[p].[PR_TRD_Midpoint],
	[p].[PR_TRD_Netback],
	[p].[PR_TRD_NetbackMargin],
	[p].[PR_TRD_RGV],
	[p].[PR_TRD_CumulativeIndex],
	[p].[PR_TRD_CumulativeVolume],
	[p].[PR_TRD_TransportCosts],
	[p].[PR_TRD_ChangedAt],

	[a].[PR_TRD_PositedBy],
	[a].[PR_TRD_PositedAt],
	[a].[PR_TRD_PositReliability],
	[a].[PR_TRD_PositReliable]
FROM
	[fact].[PR_TRD_PricingCommodity_Trade_Posit]	[p]
INNER JOIN
	[fact].[PR_TRD_PricingCommodity_Trade_Annex]	[a]
		ON	[a].[PR_TRD_ID]		= [p].[PR_TRD_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__PR_TRD_PricingValue_Surrogate]
ON [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
(
	[PR_TRD_PR_ID]				ASC,
	[PR_TRD_ChangedAt]			DESC,
	[PR_TRD_PositedAt]			DESC,
	[PR_TRD_PositedBy]			ASC,
	[PR_TRD_PositReliable]		ASC
)
WITH(FILLFACTOR = 95);
GO

CREATE NONCLUSTERED INDEX [IX__PR_TRD_PricingValue_Surrogate]
ON [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
(
	[PR_TRD_ChangedAt]			DESC,
	[PR_TRD_PositedAt]			DESC
)
INCLUDE
(
	[PR_TRD_ID],
	[PR_TRD_PR_ID],
	[PR_TRD_Index],
	[PR_TRD_Mean],
	[PR_TRD_Ask],
	[PR_TRD_Bid],
	[PR_TRD_PrevInterest],
	[PR_TRD_Rate],
	[PR_TRD_Margin],
	[PR_TRD_DiffLow],
	[PR_TRD_DiffHigh],
	[PR_TRD_DiffIndex],
	[PR_TRD_DiffMidpoint],
	[PR_TRD_Midpoint],
	[PR_TRD_Netback],
	[PR_TRD_NetbackMargin],
	[PR_TRD_RGV],
	[PR_TRD_CumulativeIndex],
	[PR_TRD_CumulativeVolume],
	[PR_TRD_TransportCosts],

	[PR_TRD_PositedBy],
	[PR_TRD_PositReliability],
	[PR_TRD_PositReliable]
)
WITH(FILLFACTOR = 95);
GO

CREATE STATISTICS [ST__PR_TRD_PricingValue_Surrogate_PR_TRD_ID]
ON [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
(
	[PR_TRD_ID]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE STATISTICS [ST__PR_TRD_PricingValue_Surrogate_PR_TRD_ChangedAt]
ON [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
(
	[PR_TRD_ChangedAt]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE STATISTICS [ST__PR_TRD_PricingValue_Surrogate_PR_TRD_PositedAt]
ON [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
(
	[PR_TRD_PositedAt]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE STATISTICS [ST__PR_TRD_PricingValue_Surrogate_PR_TRD_PositReliable]
ON [fact].[PR_TRD_PricingCommodity_Trade_Surrogate]
(
	[PR_TRD_PositReliable]
)
WITH SAMPLE 100 PERCENT;
GO

