﻿CREATE TABLE [ref].[AR_PriceType_Posit]
(
	[AR_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[AR_PriceTypeId]			TINYINT				NOT	NULL,
	[AR_Description]			VARCHAR(26)			NOT	NULL	CONSTRAINT [CL__AR_PriceType_Posit_AR_Description]		CHECK([AR_Description] <> ''),

	[AR_ChangedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_PriceType_Posit_AR_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__AR_PriceType_Posit_AR_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__AR_PriceType_Posit_AR_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AR_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_PriceType_Posit_AR_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__AR_PriceType_Posit_AR_Reliability]		DEFAULT(50),
	[AR_PositReliable]			AS CONVERT(BIT, CASE WHEN [AR_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[AR_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AR_PriceType_Posit_AR_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__AR_PriceType_Posit_AR_RowGuid]			UNIQUE NONCLUSTERED([AR_RowGuid]),

	CONSTRAINT [PK__AR_PriceType_Posit] PRIMARY KEY([AR_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__AR_PriceType_Posit] UNIQUE CLUSTERED([AR_PriceTypeId] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [IX__AR_PriceType_Posit] UNIQUE NONCLUSTERED([AR_Description] ASC)
		WITH(FILLFACTOR = 95)
);