﻿*** Processing the files ***

	1) Use a FTP client to download files from the provider's (Argus or Platts) FTP site to the corresponding Download folder:

		C:\FtpPricing
		├ ArgusCsv
		│ ├ Archvie
		│ ├ Download
		│ │ └ <Dated folder (YYYYMMDD)>		this folder will be manually created
		│ │   ├ <Argus file>
		│ │   ├ <Argus file>
		│ │   └ <Argus file>
		│ └ Upload
		└ PlattsFtp
		  ├ Archvie
		  ├ Download
		  │ ├ <Dated folder (YYYYMMDD)>		Platts has this structure in its FTP site
		  │ │ ├ <Platts file>
		  │ │ ├ <Platts file>
		  │ │ └ <Platts file>
		  │ └ <Dated folder (YYYYMMDD)>
		  │   ├ <Platts file>
		  │   ├ <Platts file>
		  │   └ <Platts file>
		  └ Upload


		Archive		- folders/files that have been stored in the database
		Download	- folders/files that have been downloaded from the data providers FTP site
		Uplaod		- folders/files that are currently being uploaded into the database

		The Downlaod folder has the following structure:

			Download
			├ <Dated folder (YYYYMMDD)>
			│ ├ <Platts file>
			│ ├ <Platts file>
			│ └ <Platts file>
			└ <Dated folder (YYYYMMDD)>
			  ├ <Platts file>
			  ├ <Platts file>
			  └ <Platts file>


	2) Run the SQL Job 'Commodity Pricing'

		The SSIS Package:
		1) Loops through the folders in 'Download' then through the files in '<Dated folder>'.
		2) Moves 'Download\<Dated folder>' to 'Upload' when processing.
		3) Moves 'Upload\<Dated folder>' to 'Archive' when upload is finished.



*** Database deployment - DATA TIER APPLICATION  ***

	Since the database is being deployed/publiched as a Data-tier application AND there may be drift from the deployed version.
	the Data-tier application will have to be deleted:

		1) Right click "Pricing"
		2) Select Tasks -> Delete Data-tier Application...
		3) Click "Next"
		4) Click "Next"
		5) Click "Finished"

	NOTE: I cant determine why adding [SOLOMONWORLD\SsisPricing] in the deployment caues drift...

	NOTE: The Post Deployment Script has been disabled.



*** Naive Speed Improvement Technique  ***

	Two views are used during the upload process to find recent duplicates:

		1) [stage].[PA_PricingArgusCsv_Posit_Recent]
		2) [stage].[PP_PricingPlattsFtp_Posit_Recent]

	These views have
		1) a unique clustered index
		2) Where clause

	The where clause only selects records from the date specified in the view; change this date to change the data being selected.



*** Least Privileged Principle  ***

	Login:		SOLOMONWORLD\SsisPricing
	User:		SOLOMONWORLD\SsisPricing	(Maps to login SOLOMONWORLD\SsisPricing)
	Credential:	SsisPricing					(Maps to user SOLOMONWORLD\SsisPricing)
	Proxy:		PxySsisPricing				(Maps to credential SsisPricing)

	The User (SOLOMONWORLD\SsisPricing) has db_datareader, db_datawriter, CONNECT, and EXECUTE privileges.
	The SQL Job runs the SSIS Package using the proxy PxySsisPricing



*** Database Maintenace ***

	To improve performance, 'Pricing Maintenance Plan' runs one a week at Tuesday at 21:00 (9:00 PM):

		1) Check database integrity (Database torn page verification is set to CHECKSUM)
		2) Shrinks database (keep free space in database files)
		3) Rebuild indexs
		4) Update statistics (All indexes, full scan)
		5) Clean up history (All history older than 4 weeks)



*** Bulk data load files must be on the SQL Server ***

	C:\FtpPricing
	└ Seed (copy of db.Pricing\Deployment\Seed)

	Refer to the bulk insert scripts for target directory on SQL Server.
	Creating bulk insert XML Format Files:	https://msdn.microsoft.com/en-us/library/ms187833.aspx



*** SSIS Package Signing ***

	Could not determine how to sign the SSIS package and then run from a SQL Job.
	Maybe, the signing certificate must be in the Current User Store / Personal of the user executing the SQL job