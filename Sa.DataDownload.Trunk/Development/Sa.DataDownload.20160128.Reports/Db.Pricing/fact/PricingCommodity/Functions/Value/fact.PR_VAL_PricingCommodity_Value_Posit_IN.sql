﻿CREATE FUNCTION [fact].[PR_VAL_PricingCommodity_Value_Posit_IN]
(
	@ChangedAfter	DATETIMEOFFSET(7)	=   '01-01-01',
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[PR_VAL_ID],
	[p].[PR_VAL_PR_ID],
	[p].[PR_VAL_Open],
	[p].[PR_VAL_Close],
	[p].[PR_VAL_Low],
	[p].[PR_VAL_High],
	[p].[PR_VAL_Volume],
	[p].[PR_VAL_ChangedAt]
FROM
	[fact].[PR_VAL_PricingCommodity_Value_Posit]		[p]
WHERE	[p].[PR_VAL_ChangedAt] >= @ChangedAfter
	AND	[p].[PR_VAL_ChangedAt] <  @ChangedBefore;