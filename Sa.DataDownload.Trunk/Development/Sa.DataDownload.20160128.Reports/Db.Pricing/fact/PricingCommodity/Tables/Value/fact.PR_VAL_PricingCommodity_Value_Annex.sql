﻿CREATE TABLE [fact].[PR_VAL_PricingCommodity_Value_Annex]
(
	[PR_VAL_ID]					INT					NOT	NULL	CONSTRAINT [FK__PR_VAL_PricingCommodity_Value_Annex_PR_VAL_ID]			REFERENCES [fact].[PR_VAL_PricingCommodity_Value_Posit]([PR_VAL_ID]),

	[PR_VAL_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_PositedBy]			DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__PR_VAL_PricingCommodity_Value_Annex_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[PR_VAL_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[PR_VAL_PositReliability]	DECIMAL(5, 2)		NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_Reliability]		DEFAULT(0.0),
	[PR_VAL_PositReliable]		AS CONVERT(BIT, CASE WHEN [PR_VAL_PositReliability] > 0.0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[PR_VAL_PositAssertion]		AS CONVERT(CHAR(1), COALESCE(
									CASE
										WHEN [PR_VAL_PositReliability] > 0.0 THEN '+'
										WHEN [PR_VAL_PositReliability] < 0.0 THEN '-'
										ELSE '?'
									END,
									'?'))
								PERSISTED			NOT	NULL,

	[PR_VAL_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PR_VAL_PricingCommodity_Value_Annex_RowGuid]			UNIQUE NONCLUSTERED([PR_VAL_RowGuid]),

	CONSTRAINT [PK__PR_VAL_PricingCommodity_Value_Annex]		PRIMARY KEY CLUSTERED([PR_VAL_ID] ASC, [PR_VAL_PositedBy] ASC, [PR_VAL_PositedAt] DESC)
		WITH(FILLFACTOR = 95)
);
GO

CREATE NONCLUSTERED INDEX [IX__PR_VAL_PricingCommodity_Value_Annex]
ON [fact].[PR_VAL_PricingCommodity_Value_Annex]
(
	[PR_VAL_PositedAt] DESC
)
INCLUDE
(
	[PR_VAL_ID],
	[PR_VAL_PositedBy],
	[PR_VAL_PositReliability],
	[PR_VAL_PositReliable]
)
WITH(FILLFACTOR = 95);
GO

CREATE TRIGGER [fact].[PR_VAL_PricingCommodity_Value_Annex_Delete]
ON [fact].[PR_VAL_PricingCommodity_Value_Annex]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_VAL_PricingCommodity_Value_Annex].', 16, 1);
		ROLLBACK;
	END;

END;
GO