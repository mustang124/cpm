﻿CREATE TABLE [fact].[PR_TRD_PricingCommodity_Trade_Annex]
(
	[PR_TRD_ID]					INT					NOT	NULL	CONSTRAINT [FK__PR_TRD_PricingCommodity_Trade_Annex_PR_TRD_ID]			REFERENCES [fact].[PR_TRD_PricingCommodity_Trade_Posit]([PR_TRD_ID]),

	[PR_TRD_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_PositedBy]			DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__PR_TRD_PricingCommodity_Trade_Annex_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[PR_TRD_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[PR_TRD_PositReliability]	DECIMAL(5, 2)		NOT	NULL	CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_Reliability]		DEFAULT(0.0),
	[PR_TRD_PositReliable]		AS CONVERT(BIT, CASE WHEN [PR_TRD_PositReliability] > 0.0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[PR_TRD_PositAssertion]		AS CONVERT(CHAR(1), COALESCE(
									CASE
										WHEN [PR_TRD_PositReliability] > 0.0 THEN '+'
										WHEN [PR_TRD_PositReliability] < 0.0 THEN '-'
										ELSE '?'
									END,
									'?'))
								PERSISTED			NOT	NULL,

	[PR_TRD_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PR_TRD_PricingCommodity_Trade_Annex_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PR_TRD_PricingCommodity_Trade_Annex_RowGuid]			UNIQUE NONCLUSTERED([PR_TRD_RowGuid]),

	CONSTRAINT [PK__PR_TRD_PricingCommodity_Trade_Annex]		PRIMARY KEY CLUSTERED([PR_TRD_ID] ASC, [PR_TRD_PositedBy] ASC, [PR_TRD_PositedAt] DESC)
		WITH(FILLFACTOR = 95)
);
GO

CREATE NONCLUSTERED INDEX [IX__PR_TRD_PricingCommodity_Trade_Annex]
ON [fact].[PR_TRD_PricingCommodity_Trade_Annex]
(
	[PR_TRD_PositedAt] DESC
)
INCLUDE
(
	[PR_TRD_ID],
	[PR_TRD_PositedBy],
	[PR_TRD_PositReliability],
	[PR_TRD_PositReliable]
)
WITH(FILLFACTOR = 95);
GO

CREATE TRIGGER [fact].[PR_TRD_PricingCommodity_Trade_Annex_Delete]
ON [fact].[PR_TRD_PricingCommodity_Trade_Annex]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_TRD_PricingCommodity_Trade_Annex].', 16, 1);
		ROLLBACK;
	END;

END;
GO