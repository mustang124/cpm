﻿CREATE TABLE [stage].[PP_PricingPlattsFtp_Duple]
(
	[PP_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[PP_FilePath]				VARCHAR(260)		NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FilePath]				CHECK([PP_FilePath] <> ''),
	[PP_FileCopyright]			VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileCopyright]			CHECK([PP_FileCopyright] <> ''),
	[PP_FileIdentifier]			VARCHAR(16)			NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileIdentifier]		CHECK([PP_FileIdentifier] <> ''),
	[PP_FileCreated_Stamp]		CHAR(12)			NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileCreated_Stamp]		CHECK([PP_FileCreated_Stamp] <> ''),
	[PP_FileCreated]			DATETIMEOFFSET(0)	NOT	NULL,

	[PP_FileItemCount]			INT					NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileItemCount]			CHECK([PP_FileItemCount] > 0),
	[PP_FileLabel]				VARCHAR(9)			NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileLabel]				CHECK([PP_FileLabel] <> ''),
	[PP_FileCategoryCode]		VARCHAR(3)			NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileCategoryCode]		CHECK([PP_FileCategoryCode] <> ''),
	[PP_FileAsOf_Stamp]			CHAR(8)				NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_FileAsOf_Stamp]		CHECK([PP_FileAsOf_Stamp] <> ''),
	[PP_FileAsOf]				DATETIMEOFFSET(0)	NOT	NULL,

	[PP_ItemTrans]				VARCHAR(2)			NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemTrans]				CHECK([PP_ItemTrans] <> ''),
	[PP_ItemSymbol]				CHAR(7)				NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemSymbol]			CHECK([PP_ItemSymbol] <> ''),
	[PP_ItemBateCode]			CHAR(1)				NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemBateCode]			CHECK([PP_ItemBateCode] <> ''),
	[PP_ItemAssessment_Stamp]	CHAR(12)			NOT	NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemAssessment_Stamp]	CHECK([PP_ItemAssessment_Stamp] <> ''),
	[PP_ItemAssessment]			DATETIMEOFFSET(0)	NOT	NULL,

	[PP_ItemPrice]				DECIMAL(14, 4)			NULL,

	[PP_ItemXAction_Stamp]		CHAR(12)				NULL,	CONSTRAINT [CL__PP_PricingPlattsFtp_Duple_PP_ItemXAction_Stamp]		CHECK([PP_ItemXAction_Stamp] <> ''),
	[PP_ItemXAction]			DATETIMEOFFSET(0)		NULL,

	[PP_ChangedAt]				DATETIMEOFFSET(7)	NOT	NULL,
	[PP_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_PositedBy]				DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__PP_PricingPlattsFtp_Duple_PP_PositedBy]				REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[PP_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_PositedAt]				DEFAULT(SYSDATETIMEOFFSET()),
	[PP_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_Reliability]			DEFAULT(50),
	[PP_PositReliable]			AS CONVERT(BIT, CASE WHEN [PP_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[PP_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PP_PricingPlattsFtp_Duple_PP_RowGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PP_PricingPlattsFtp_Duple_PP_RowGuid]				UNIQUE NONCLUSTERED([PP_RowGuid]),

	CONSTRAINT [PK__PP_PricingPlattsFtp_Duple] PRIMARY KEY([PP_ID] ASC)
		WITH(FILLFACTOR = 95)
);