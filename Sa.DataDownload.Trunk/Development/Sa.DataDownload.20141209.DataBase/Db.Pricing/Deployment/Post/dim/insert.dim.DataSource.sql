﻿PRINT 'insert.dim.DataSource...';

DECLARE @DataSource	TABLE
(
	[DS_Tag]				VARCHAR(24)			NOT	NULL	CHECK([DS_Tag] <> ''),
															UNIQUE CLUSTERED([DS_Tag] ASC),

	[DS_Name]				NVARCHAR(48)		NOT	NULL	CHECK([DS_Name] <> '')
															UNIQUE([DS_Name] ASC),

	[DS_Detail]				NVARCHAR(96)		NOT	NULL	CHECK([DS_Detail] <> '')
															UNIQUE([DS_Detail] ASC),

	[DS_Parent]				VARCHAR(24)			NOT	NULL	CHECK([DS_Parent] <> '')
)

INSERT INTO @DataSource([DS_Tag], [DS_Name], [DS_Detail], [DS_Parent])
SELECT [t].[DS_Tag], [t].[DS_Name], [t].[DS_Detail], [t].[DS_Parent]
FROM (VALUES
	('Platts', 'Platts', 'Platts - McGraw Hill Financial', 'Platts'),
	('PlattsMarketData', 'Platts Market Data', 'Platts Market Data', 'Platts'),
	('Argus', 'Argus', 'Argus', 'Argus')
	)[t]([DS_Tag], [DS_Name], [DS_Detail], [DS_Parent]);

INSERT INTO [dim].[DS_DataSource_LookUp]([DS_Tag], [DS_Name], [DS_Detail])
SELECT [t].[DS_Tag], [t].[DS_Name], [t].[DS_Detail]
FROM
	@DataSource						[t]
LEFT OUTER JOIN
	[dim].[DS_DataSource_LookUp]	[x]
		ON	[x].[DS_Tag]		= [t].[DS_Tag]
WHERE
	[x].[DS_DataSourceId]		IS NULL;

INSERT INTO [dim].[DS_DataSource_Parent]([DS_MethodologyId], [DS_DataSourceId], [DS_ParentId], [DS_Operator], [DS_SortKey], [DS_Hierarchy])
SELECT 0, [i].[DS_DataSourceId], [p].[DS_DataSourceId], '+', 0, '/'
FROM
	@DataSource						[t]
INNER JOIN
	[dim].[DS_DataSource_LookUp]	[i]
		ON	[i].[DS_Tag]	= [t].[DS_Tag]
INNER JOIN
	[dim].[DS_DataSource_LookUp]	[p]
		ON	[p].[DS_Tag]	= [t].[DS_Parent]
LEFT OUTER JOIN
	[dim].[DS_DataSource_Parent]	[x]
		ON	[x].[DS_DataSourceId]	= [i].[DS_DataSourceId]
		AND	[x].[DS_ParentId]		= [p].[DS_DataSourceId]
WHERE
	[x].[DS_DataSourceId]		IS NULL;

EXECUTE [dim].[Update_Parent] 'dim', 'DS_DataSource_Parent', 'DS', 'DataSourceId', DEFAULT, DEFAULT, DEFAULT, DEFAULT;
EXECUTE [dim].[Merge_Bridge] 'dim', 'DS_DataSource_Parent', 'dim', 'DS_DataSource_Bridge', 'DS', 'DataSourceId', DEFAULT, DEFAULT, DEFAULT, DEFAULT;