﻿CREATE VIEW [stage].[PA_PricingArgusCsv_Posit_Recent]
WITH SCHEMABINDING
AS SELECT
	[p].[PA_Code],
	[p].[PA_TimeStampId],
	[p].[PA_PriceTypeId],
	[p].[PA_Date],
	[p].[PA_ChangedAt],
	[p].[PA_PositedAt]
FROM
	[stage].[PA_PricingArgusCsv_Posit]	[p]
WHERE
	[p].[PA_PositedAt] >= CONVERT(DATE, '20150101', 112);
GO

CREATE UNIQUE CLUSTERED INDEX [PK__PA_PricingArgusCsv_Posit_Recent]
ON [stage].[PA_PricingArgusCsv_Posit_Recent]
(
	[PA_Code]				ASC,
	[PA_TimeStampId]		ASC,
	[PA_PriceTypeId]		ASC,
	[PA_Date]				DESC,
	[PA_ChangedAt]			DESC,
	[PA_PositedAt]			DESC
)
WITH(FILLFACTOR = 95);
GO