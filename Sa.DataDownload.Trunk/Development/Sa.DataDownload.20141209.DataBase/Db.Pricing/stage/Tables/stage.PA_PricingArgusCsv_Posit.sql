﻿CREATE TABLE [stage].[PA_PricingArgusCsv_Posit]
(
	[PA_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[PA_FilePath]				VARCHAR(260)		NOT	NULL,	CONSTRAINT [CL__PA_PricingArgusCsv_Posit_PA_FilePath]			CHECK([PA_FilePath] <> ''),
	[PA_Code]					CHAR(9)				NOT	NULL,	CONSTRAINT [CL__PA_PricingArgusCsv_Posit_PA_Code]				CHECK([PA_Code] <> ''),

	[PA_TimeStampId]			TINYINT				NOT	NULL,
	[PA_PriceTypeId]			TINYINT				NOT	NULL,
	[PA_Date]					DATE				NOT	NULL,
	[PA_Value]					DECIMAL(14, 4)		NOT	NULL,
	[PA_ContForwardPeriod]		SMALLINT			NOT	NULL,
	[PA_DiffBaseRoll]			SMALLINT			NOT	NULL,
	[PA_Year]					SMALLINT			NOT	NULL,
	[PA_ContFwd]				TINYINT				NOT	NULL,
	[PA_RecordStatus]			CHAR(1)				NOT	NULL,	CONSTRAINT [CL__PA_PricingArgusCsv_Posit_PA_RecordStatus]		CHECK([PA_RecordStatus] <> ''),

	[PA_ChangedAt]				DATETIMEOFFSET(7)	NOT	NULL,
	[PA_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Posit_PA_PositedBy]			DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__PA_PricingArgusCsv_Posit_PA_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[PA_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Posit_PA_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[PA_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Posit_PA_Reliability]		DEFAULT(50),
	[PA_PositReliable]			AS CONVERT(BIT, CASE WHEN [PA_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[PA_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Posit_PA_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PA_PricingArgusCsv_Posit_PA_RowGuid]			UNIQUE NONCLUSTERED([PA_RowGuid]),

	CONSTRAINT [PK__PA_PricingArgusCsv_Posit] PRIMARY KEY([PA_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__PA_PricingArgusCsv_Posit] UNIQUE CLUSTERED([PA_Code] ASC, [PA_PriceTypeId] ASC, [PA_TimeStampId] ASC, [PA_RecordStatus] ASC, [PA_ChangedAt] ASC, [PA_PositedAt] ASC)
		WITH(FILLFACTOR = 95)
);