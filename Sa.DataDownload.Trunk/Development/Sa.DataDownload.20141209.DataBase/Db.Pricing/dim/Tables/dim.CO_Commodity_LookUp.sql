﻿CREATE TABLE [dim].[CO_Commodity_LookUp]
(
	[CO_CommodityId]		INT					NOT NULL	IDENTITY(1, 1),

	[CO_Tag]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL__CO_Commodity_LookUp_CO_Tag]				CHECK([CO_Tag] <> ''),
	[CO_Name]				NVARCHAR(256)		NOT	NULL	CONSTRAINT [CL__CO_Commodity_LookUp_CO_Name]			CHECK([CO_Name] <> ''),
	[CO_Detail]				AS [CO_Name] + N' (' + CONVERT(NVARCHAR(12), [CO_Tag]) + N')'
							PERSISTED			NOT	NULL,

	[CO_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__CO_Commodity_LookUp_CO_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__CO_Commodity_LookUp_CO_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CO_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CO_Commodity_LookUp_CO_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CO_Commodity_LookUp_CO_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__CO_Commodity_LookUp_CO_RowGuid]			UNIQUE NONCLUSTERED([CO_RowGuid]),

	CONSTRAINT [PK__CO_Commodity_LookUp]			PRIMARY KEY([CO_CommodityId] ASC),
	CONSTRAINT [UK__CO_Commodity_LookUp_CO_Tag]		UNIQUE CLUSTERED([CO_Tag] ASC),
	CONSTRAINT [UX__CO_Commodity_LookUp_CO_Detail]	UNIQUE([CO_Detail] ASC),
);