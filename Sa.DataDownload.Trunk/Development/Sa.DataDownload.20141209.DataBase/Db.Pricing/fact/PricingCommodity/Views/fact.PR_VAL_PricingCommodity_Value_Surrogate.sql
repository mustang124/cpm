﻿CREATE VIEW [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
WITH SCHEMABINDING
AS
SELECT
	[p].[PR_VAL_ID],
	[p].[PR_VAL_PR_ID],
	[p].[PR_VAL_Open],
	[p].[PR_VAL_Close],
	[p].[PR_VAL_Low],
	[p].[PR_VAL_High],
	[p].[PR_VAL_Index],
	[p].[PR_VAL_Ask],
	[p].[PR_VAL_Bid],
	[p].[PR_VAL_Mean],
	[p].[PR_VAL_PrevInterest],
	[p].[PR_VAL_Latest],
	[p].[PR_VAL_Change],
	[p].[PR_VAL_Volume],
	[p].[PR_VAL_ChangedAt],

	[a].[PR_VAL_PositedBy],
	[a].[PR_VAL_PositedAt],
	[a].[PR_VAL_PositReliability],
	[a].[PR_VAL_PositReliable]
FROM
	[fact].[PR_VAL_PricingCommodity_Value_Posit]	[p]
INNER JOIN
	[fact].[PR_VAL_PricingCommodity_Value_Annex]	[a]
		ON	[a].[PR_VAL_ID]		= [p].[PR_VAL_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__PR_VAL_PricingValue_Surrogate]
ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
(
	[PR_VAL_PR_ID]				ASC,
	[PR_VAL_ChangedAt]			DESC,
	[PR_VAL_PositedAt]			DESC,
	[PR_VAL_PositedBy]			ASC,
	[PR_VAL_PositReliable]		ASC
)
WITH(FILLFACTOR = 95);
GO

CREATE NONCLUSTERED INDEX [IX__PR_VAL_PricingValue_Surrogate]
ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
(
	[PR_VAL_ChangedAt]			DESC,
	[PR_VAL_PositedAt]			DESC
)
INCLUDE
(
	[PR_VAL_ID],
	[PR_VAL_PR_ID],
	[PR_VAL_Open],
	[PR_VAL_Close],
	[PR_VAL_Low],
	[PR_VAL_High],
	[PR_VAL_Index],
	[PR_VAL_Ask],
	[PR_VAL_Bid],
	[PR_VAL_Mean],
	[PR_VAL_PrevInterest],
	[PR_VAL_Latest],
	[PR_VAL_Change],
	[PR_VAL_Volume],
	[PR_VAL_PositedBy],
	[PR_VAL_PositReliability],
	[PR_VAL_PositReliable]
)
WITH(FILLFACTOR = 95);
GO

CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_ID]
ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
(
	[PR_VAL_ID]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_ChangedAt]
ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
(
	[PR_VAL_ChangedAt]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_PositedAt]
ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
(
	[PR_VAL_PositedAt]
)
WITH SAMPLE 100 PERCENT;
GO

CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_PositReliable]
ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
(
	[PR_VAL_PositReliable]
)
WITH SAMPLE 100 PERCENT;
GO

