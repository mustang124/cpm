﻿CREATE FUNCTION [fact].[PR_VAL_PricingCommodity_Value_RW]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[PR_VAL_PR_ID],
	[p].[PR_VAL_ID],
	[p].[PR_VAL_ChangedAt],
	[a].[PR_VAL_PositedAt]
FROM
	[fact].[PR_VAL_PricingCommodity_Value_Posit_RW](@ChangedBefore)				[p]
INNER JOIN
	[fact].[PR_VAL_PricingCommodity_Value_Annex_RW](@PositedBefore)				[a]
		ON	[a].[PR_VAL_ID]			= [p].[PR_VAL_ID]
		AND	[a].[PR_VAL_PositedAt]	= (
			SELECT TOP 1
				[s].[PR_VAL_PositedAt]
			FROM
				[fact].[PR_VAL_PricingCommodity_Value_Annex_RW](@PositedBefore)	[s]
			WHERE
				[s].[PR_VAL_ID]		= [p].[PR_VAL_ID]
			ORDER BY
				[s].[PR_VAL_PositedAt]	DESC
			);