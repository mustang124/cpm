﻿CREATE FUNCTION [fact].[PR_PricingCommodity_Interval]
(
	@ChangedAfter	DATETIMEOFFSET(7)	=   '01-01-01',
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[z].[PR_ID],
	[z].[PR_CommodityId],
	[z].[PR_PriceDate],
	[z].[PR_CurrencyId],
	[z].[PR_UomId],
	[z].[PR_DataSourceId],

	[p].[PR_VAL_ID],
	[p].[PR_VAL_PR_ID],
	[p].[PR_VAL_Open],
	[p].[PR_VAL_Close],
	[p].[PR_VAL_Low],
	[p].[PR_VAL_High],
	[p].[PR_VAL_Index],
	[p].[PR_VAL_Ask],
	[p].[PR_VAL_Bid],
	[p].[PR_VAL_Mean],
	[p].[PR_VAL_PrevInterest],
	[p].[PR_VAL_Latest],
	[p].[PR_VAL_Change],
	[p].[PR_VAL_Volume],

	[p].[PR_VAL_ChangedAt],
	[p].[PR_VAL_PositedBy],
	[p].[PR_VAL_PositedAt],
	[p].[PR_VAL_PositReliability],
	[p].[PR_VAL_PositReliable]
FROM
	[fact].[PR_PricingCommodity]					[z]
INNER JOIN
	[fact].[PR_VAL_PricingCommodity_Value_Interval](@ChangedAfter, @ChangedBefore, @PositedBefore)	[p]
		ON	[p].[PR_VAL_PR_ID]			= [z].[PR_ID]
		AND	[p].[PR_VAL_PositReliable]	= @Reliable