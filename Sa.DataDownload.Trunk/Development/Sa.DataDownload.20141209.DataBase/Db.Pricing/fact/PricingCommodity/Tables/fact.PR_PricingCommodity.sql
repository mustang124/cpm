﻿CREATE TABLE [fact].[PR_PricingCommodity]
(
	[PR_ID]					INT					NOT	NULL	IDENTITY(1, 1),

	[PR_CommodityId]		INT					NOT	NULL	CONSTRAINT [FK__PR_PricingCommodity_PR_CommodityId]		REFERENCES [dim].[CO_Commodity_LookUp]([CO_CommodityId]),
	[PR_PriceDate]			DATE				NOT	NULL,
	[PR_CurrencyId]			INT					NOT	NULL	CONSTRAINT [FK__PR_PricingCommodity_PR_CurrencyId]		REFERENCES [dim].[CU_Currency_LookUp]([CU_CurrencyId]),
	[PR_UomId]				INT					NOT	NULL	CONSTRAINT [FK__PR_PricingCommodity_PR_UomId]			REFERENCES [dim].[UM_Uom_LookUp]([UM_UomId]),
	[PR_DataSourceId]		INT					NOT	NULL	CONSTRAINT [FK__PR_PricingCommodity_PR_DataSourceId]	REFERENCES [dim].[DS_DataSource_LookUp]([DS_DataSourceId]),

	[PR_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PR_PricingCommodity_PR_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__PR_PricingCommodity_PR_RowGuid]			UNIQUE NONCLUSTERED([PR_RowGuid]),

	CONSTRAINT [PK__PR_PricingCommodity] PRIMARY KEY([PR_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__PR_PricingCommodity] UNIQUE CLUSTERED([PR_CommodityId] ASC, [PR_PriceDate] DESC, [PR_CurrencyId] ASC, [PR_UomId] ASC, [PR_DataSourceId] ASC)
		WITH(FILLFACTOR = 95)
);
GO

CREATE TRIGGER [fact].[PR_PricingCommodity_Delete]
ON [fact].[PR_PricingCommodity]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_PricingCommodity].', 16, 1);
		ROLLBACK;
	END;

END;
GO