﻿CREATE TABLE [fact].[PR_VAL_PricingCommodity_Value_Posit]
(
	[PR_VAL_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[PR_VAL_PR_ID]				INT					NOT	NULL	CONSTRAINT [FK__PR_VAL_PricingCommodity_Value_Posit_PR_ID]				REFERENCES [fact].[PR_PricingCommodity]([PR_ID]),

	[PR_VAL_Open]				FLOAT					NULL,
	[PR_VAL_Close]				FLOAT					NULL,
	[PR_VAL_Low]				FLOAT					NULL,
	[PR_VAL_High]				FLOAT					NULL,
	[PR_VAL_Index]				FLOAT					NULL,
	[PR_VAL_Ask]				FLOAT					NULL,
	[PR_VAL_Bid]				FLOAT					NULL,
	[PR_VAL_Mean]				FLOAT					NULL,
	[PR_VAL_Volume]				FLOAT					NULL	CONSTRAINT [CR__PR_VAL_PricingCommodity_Value_Posit_Volume]				CHECK([PR_VAL_Volume] >= 0.0),

	--	Design Note: The SPARSE columns should be in a different table.
	[PR_VAL_PrevInterest]		FLOAT			SPARSE	NULL,
	[PR_VAL_Latest]				FLOAT			SPARSE	NULL,
	[PR_VAL_Change]				FLOAT			SPARSE	NULL,

	[PR_VAL_DiffLow]			FLOAT			SPARSE	NULL,
	[PR_VAL_DiffHigh]			FLOAT			SPARSE	NULL,
	[PR_VAL_DiffIndex]			FLOAT			SPARSE	NULL,
	[PR_VAL_DiffMidpoint]		FLOAT			SPARSE	NULL,
	[PR_VAL_Midpoint]			FLOAT			SPARSE	NULL,
	[PR_VAL_Netback]			FLOAT			SPARSE	NULL,
	[PR_VAL_NetbackMargin]		FLOAT			SPARSE	NULL,
	[PR_VAL_RGV]				FLOAT			SPARSE	NULL,
	[PR_VAL_Rate]				FLOAT			SPARSE	NULL,

	[PR_VAL_Margin]				FLOAT			SPARSE	NULL,
	[PR_VAL_CumulativeIndex]	FLOAT			SPARSE	NULL,
	[PR_VAL_CumulativeVolume]	FLOAT			SPARSE	NULL,
	[PR_VAL_TransportCosts]		FLOAT			SPARSE	NULL,

	[PR_VAL_Checksum]			AS BINARY_CHECKSUM([PR_VAL_Open], [PR_VAL_Close], [PR_VAL_Low], [PR_VAL_High], [PR_VAL_Index], [PR_VAL_Ask], [PR_VAL_Bid], [PR_VAL_Mean], [PR_VAL_Volume])
									--[PR_VAL_PrevInterest], [PR_VAL_Latest], [PR_VAL_Change],
									--[PR_VAL_DiffMidpoint], [PR_VAL_DiffLow], [PR_VAL_DiffHigh], [PR_VAL_Midpoint], [PR_VAL_Netback], [PR_VAL_RGV], [PR_VAL_Rate], [PR_VAL_DiffIndex], [PR_VAL_NetbackMargin],
									--[PR_VAL_Margin], [PR_VAL_CumulativeIndex], [PR_VAL_CumulativeVolume], [PR_VAL_TransportCosts]
									--)
								PERSISTED			NOT	NULL,
	[PR_VAL_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Posit_ChangedAt]			DEFAULT(SYSDATETIMEOFFSET()),

	[PR_VAL_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Posit_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PR_VAL_PricingCommodity_Value_Posit_RowGuid]			UNIQUE NONCLUSTERED([PR_VAL_RowGuid]),

	CONSTRAINT [PK__PR_VAL_PricingCommodity_Value_Posit] PRIMARY KEY([PR_VAL_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__PR_VAL_PricingCommodity_Value_Posit] UNIQUE CLUSTERED([PR_VAL_PR_ID] ASC, [PR_VAL_ChangedAt] DESC, [PR_VAL_Checksum] ASC)
		WITH(FILLFACTOR = 95)
);
GO

CREATE TRIGGER [fact].[PR_VAL_PricingCommodity_Value_Posit_Delete]
ON [fact].[PR_VAL_PricingCommodity_Value_Posit]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_VAL_PricingCommodity_Value_Posit].', 16, 1);
		ROLLBACK;
	END;

END;
GO