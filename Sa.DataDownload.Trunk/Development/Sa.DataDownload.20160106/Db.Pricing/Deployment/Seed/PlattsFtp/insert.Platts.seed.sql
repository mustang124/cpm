﻿SET NOCOUNT ON;

PRINT 'bulk.insert.Platts.Seed...';

IF OBJECT_ID('tempdb..#PlattsFtp')			IS NOT NULL DROP TABLE #PlattsFtp;
IF OBJECT_ID('tempdb..#PlattsFtpData')		IS NOT NULL DROP TABLE #PlattsFtpData;
IF OBJECT_ID('tempdb..#PlattsFtpHeader')	IS NOT NULL DROP TABLE #PlattsFtpHeader;

--© 2015 by McGraw Hill Financial, Inc.
--PlattsMarketData 201501090040 00101 FINAL     AE  20150108
--N AAIBH00c 201501080000 605
--N AAIBH00l 201501080000 604.5
--...
--N PUAGN00c 201501090000 227.75
--N PUAGO00h 201501090000 192.714
--N PUAGO00l 201501090000 192.291
--N PUAGO00c 201501090000 192.503
--C PUAYW00h 201501080000 246 201501091521
--C PUAYW00l 201501080000 245 201501091521
--C PUAYW00c 201501080000 245.5 201501091521

CREATE TABLE #PlattsFtpHeader
(
	[PP_FileCopyright]			NVARCHAR(37)		NOT	NULL,	CHECK([PP_FileCopyright] <> ''),
	[PP_FileIdentifier]			VARCHAR(16)			NOT	NULL,	CHECK([PP_FileIdentifier] <> ''),
	[PP_FileCreated_Stamp]		CHAR(12)			NOT	NULL,	CHECK([PP_FileCreated_Stamp] <> ''),
	[PP_FileItemCount]			INT					NOT	NULL,	CHECK([PP_FileItemCount] > 0),
	[PP_FileLabel]				VARCHAR(9)			NOT	NULL,	CHECK([PP_FileLabel] <> ''),
	[PP_FileCategoryCode]		VARCHAR(3)			NOT	NULL,	CHECK([PP_FileCategoryCode] <> ''),
	[PP_FileAsOf_Stamp]			CHAR(8)				NOT	NULL,	CHECK([PP_FileAsOf_Stamp] <> ''),
	PRIMARY KEY CLUSTERED([PP_FileCreated_Stamp] ASC, [PP_FileCategoryCode] ASC)
);

CREATE TABLE #PlattsFtpData
(
	[PP_ItemTrans]				VARCHAR(2)			NOT	NULL,	CHECK([PP_ItemTrans] <> ''),
	[PP_ItemSymbol]				CHAR(7)				NOT	NULL,	CHECK([PP_ItemSymbol] <> ''),
	[PP_ItemBateCode]			CHAR(1)				NOT	NULL,	CHECK([PP_ItemBateCode] <> ''),
	[PP_ItemAssessment_Stamp]	CHAR(12)			NOT	NULL,	CHECK([PP_ItemAssessment_Stamp] <> ''),
	[PP_ItemPriceXaction]		VARCHAR(48)				NULL,
	
	[PP_ItemPrice]				AS CONVERT(DECIMAL(14, 4), 
									LEFT([PP_ItemPriceXaction], LEN([PP_ItemPriceXaction]) - CHARINDEX(' ', REVERSE([PP_ItemPriceXaction])))
									),

	[PP_ItemXAction_Stamp]		AS	CASE WHEN CHARINDEX(' ', [PP_ItemPriceXaction]) > 0 THEN
										LTRIM(RIGHT([PP_ItemPriceXaction], CHARINDEX(' ', REVERSE([PP_ItemPriceXaction]))))
									END,

	PRIMARY KEY CLUSTERED([PP_ItemSymbol] ASC, [PP_ItemTrans] ASC, [PP_ItemBateCode] ASC, [PP_ItemAssessment_Stamp] ASC)
);

DECLARE @PlattsFolders TABLE
(
	[CrLfType]	VARCHAR(4)	NOT	NULL,
	[Folder]	CHAR(8)		NOT NULL
);

DECLARE @PlattsFiles TABLE
(
	[File]	CHAR(6) NOT NULL
);

INSERT INTO @PlattsFolders
(
	[CrLfType],
	[Folder]
)
SELECT TOP 3
	[t].[CrLfType],
	CONVERT(CHAR(8), [t].[Folder])
FROM (VALUES
	('dos', 20150108),
	('dos', 20150109),
	('dos', 20150112),
	('dos', 20150113),
	('dos', 20150114),
	('dos', 20150115),
	('dos', 20150116),
	('dos', 20150119),
	('dos', 20150120),
	('dos', 20150121),
	('dos', 20150122),
	('dos', 20150123),
	('dos', 20150126),
	('dos', 20150127),
	('dos', 20150128),
	('dos', 20150129),
	('dos', 20150130),
	('dos', 20150202),
	('dos', 20150203),
	('dos', 20150204),
	('dos', 20150205),
	('dos', 20150206),
	('dos', 20150209),
	('dos', 20150210),
	('dos', 20150211),
	('dos', 20150212),
	('dos', 20150213),
	('dos', 20150216),
	('dos', 20150217),
	('dos', 20150218),
	('dos', 20150219),
	('dos', 20150220),
	('dos', 20150223),
	('dos', 20150224),
	('dos', 20150225),
	('dos', 20150226),
	('dos', 20150227),
	('dos', 20150302),
	('dos', 20150303),
	('dos', 20150304),
	('dos', 20150305),
	('dos', 20150306),
	('dos', 20150309),
	('dos', 20150310),
	('dos', 20150311),
	('dos', 20150312),
	('dos', 20150313),
	('dos', 20150316),
	('dos', 20150317),
	('dos', 20150318),
	('dos', 20150319),
	('dos', 20150320),
	('dos', 20150323),
	('dos', 20150324),
	('dos', 20150325),
	('dos', 20150326),
	('dos', 20150327),
	('dos', 20150330),
	('dos', 20150331),

	('unix', 20151009),
	('unix', 20151012),
	('unix', 20151013),
	('unix', 20151014),
	('unix', 20151015),
	('unix', 20151016),
	('unix', 20151019),
	('unix', 20151020),
	('unix', 20151021),
	('unix', 20151022),
	('unix', 20151023),
	('unix', 20151026),
	('unix', 20151027),
	('unix', 20151028),
	('unix', 20151029),
	('unix', 20151030),
	('unix', 20151102),
	('unix', 20151103),
	('unix', 20151104),
	('unix', 20151105),
	('unix', 20151106),
	('unix', 20151109),
	('unix', 20151110),
	('unix', 20151111),
	('unix', 20151112),
	('unix', 20151113),
	('unix', 20151116),
	('unix', 20151117),
	('unix', 20151118),
	('unix', 20151119),
	('unix', 20151120),
	('unix', 20151123),
	('unix', 20151124),
	('unix', 20151125),
	('unix', 20151126),
	('unix', 20151127),
	('unix', 20151130),
	('unix', 20151201),
	('unix', 20151202),
	('unix', 20151203),
	('unix', 20151204),
	('unix', 20151207),
	('unix', 20151208),
	('unix', 20151209),
	('unix', 20151210),
	('unix', 20151211),
	('unix', 20151214),
	('unix', 20151215),
	('unix', 20151216),
	('unix', 20151217),
	('unix', 20151218),
	('unix', 20151221),
	('unix', 20151222),
	('unix', 20151223),
	('unix', 20151224)
	) [t]([CrLfType], [Folder]);

INSERT INTO @PlattsFiles
(
	[File]
)
SELECT
	[t].[File]
FROM (VALUES
('ae.ftp'),
('ag.ftp'),
('al.ftp'),
('au.ftp'),
('cc.ftp'),
('cj.ftp'),
('cl.ftp'),
('cs.ftp'),
('cx.ftp'),
('eb.ftp'),
('fs.ftp'),
('hf.ftp'),
('ho.ftp'),
('hp.ftp'),
('hs.ftp'),
('im.ftp'),
('lg.ftp'),
('li.ftp'),
('lu.ftp'),
('ma.ftp'),
('pg.ftp'),
('pl.ftp'),
('pn.ftp'),
('ps.ftp'),
('pz.ftp'),
('rc.ftp'),
('ri.ftp'),
('rp.ftp'),
('ru.ftp'),
('rw.ftp'),
('ug.ftp'),
('uw.ftp'),
('uy.ftp'),
('uz.ftp'),
('wc.ftp')
	) [t]([File]);

DECLARE @CrLfSeedPlatts		VARCHAR(4);
DECLARE @PathSeedPlatts		VARCHAR(15);
DECLARE @SqlSeedPlatts		VARCHAR(512);

DECLARE CurPlattsPath CURSOR FAST_FORWARD FOR
SELECT
	[p].[CrLfType],
	[p].[Folder] + '\' + [f].[File]
FROM
	@PlattsFolders	[p]
CROSS APPLY
	@PlattsFiles	[f];

OPEN CurPlattsPath;

FETCH NEXT FROM CurPlattsPath
INTO @CrLfSeedPlatts, @PathSeedPlatts;

WHILE @@FETCH_STATUS = 0
BEGIN

	DELETE FROM #PlattsFtpData;
	DELETE FROM #PlattsFtpHeader;

	SET @SqlSeedPlatts = 'BULK INSERT #PlattsFtpHeader FROM ''C:\FtpPricing\Seed\PlattsFtp\Data\' + @CrLfSeedPlatts + '\' + @PathSeedPlatts + ''' WITH(FORMATFILE = ''C:\FtpPricing\Seed\PlattsFtp\Data\insert.Platts.seed.header.' + @CrLfSeedPlatts + '.xml'', LASTROW=1);'
	EXECUTE(@SqlSeedPlatts);

	SET @SqlSeedPlatts = 'BULK INSERT #PlattsFtpData FROM ''C:\FtpPricing\Seed\PlattsFtp\Data\' + @CrLfSeedPlatts + '\' + @PathSeedPlatts + ''' WITH(FORMATFILE = ''C:\FtpPricing\Seed\PlattsFtp\Data\insert.Platts.seed.data.' + @CrLfSeedPlatts + '.xml'', FIRSTROW=3);'
	EXECUTE(@SqlSeedPlatts);

	INSERT INTO [stage].[PP_PricingPlattsFtp_Posit]
	(
		[PP_FilePath],
		[PP_FileCopyright],

		[PP_FileIdentifier],
		[PP_FileCreated_Stamp],
		[PP_FileCreated],

		[PP_FileItemCount],
		[PP_FileLabel],
		[PP_FileCategoryCode],
		[PP_FileAsOf_Stamp],
		[PP_FileAsOf],

		[PP_ItemTrans],
		[PP_ItemSymbol],
		[PP_ItemBateCode],
		[PP_ItemAssessment_Stamp],
		[PP_ItemAssessment],

		[PP_ItemPrice],

		[PP_ItemXAction_Stamp],
		[PP_ItemXAction],

		[PP_ChangedAt]
	)
	SELECT
			[PP_FilePath]			= @PathSeedPlatts,
			[PP_FileCopyright]		= REPLACE([h].[PP_FileCopyright], N'⌐', N'©'),

		[h].[PP_FileIdentifier],
		[h].[PP_FileCreated_Stamp],
			[PP_FileCreated]		= CONVERT(DATETIMEOFFSET,  FORMAT(CONVERT(BIGINT, [PP_FileCreated_Stamp]), '####-##-## ##:##')),

		[h].[PP_FileItemCount],
		[h].[PP_FileLabel],
		[h].[PP_FileCategoryCode],
		[h].[PP_FileAsOf_Stamp],
			[PP_FileAsOf]			= CONVERT(DATETIMEOFFSET, FORMAT(CONVERT(BIGINT, [PP_FileAsOf_Stamp]), '####-##-##')),

		[d].[PP_ItemTrans],
		[d].[PP_ItemSymbol],
		[d].[PP_ItemBateCode],
		[d].[PP_ItemAssessment_Stamp],
			[PP_ItemAssessment]		= CONVERT(DATETIMEOFFSET, FORMAT(CONVERT(BIGINT, [PP_ItemAssessment_Stamp]), '####-##-## ##:##')),

		[d].[PP_ItemPrice],

		[d].[PP_ItemXAction_Stamp],
			[PP_ItemXAction]		= CONVERT(DATETIMEOFFSET, FORMAT(CONVERT(BIGINT, [PP_ItemXAction_Stamp]), '####-##-## ##:##')),

			[PP_ChangedAt]			= CONVERT(DATETIMEOFFSET, FORMAT(CONVERT(BIGINT, [PP_ItemAssessment_Stamp]), '####-##-## ##:##'))
	FROM 
		#PlattsFtpHeader	[h]
	CROSS JOIN
		#PlattsFtpData		[d];

	FETCH NEXT FROM CurPlattsPath
	INTO @CrLfSeedPlatts, @PathSeedPlatts;

END;

CLOSE CurPlattsPath;

DEALLOCATE CurPlattsPath;

IF OBJECT_ID('tempdb..#PlattsFtp')			IS NOT NULL DROP TABLE #PlattsFtp;
IF OBJECT_ID('tempdb..#PlattsFtpData')		IS NOT NULL DROP TABLE #PlattsFtpData;
IF OBJECT_ID('tempdb..#PlattsFtpHeader')	IS NOT NULL DROP TABLE #PlattsFtpHeader;