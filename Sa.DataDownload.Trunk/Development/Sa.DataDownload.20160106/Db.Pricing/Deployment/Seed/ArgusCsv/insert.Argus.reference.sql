﻿SET NOCOUNT ON;

PRINT 'insert.Argus.reference...';

IF OBJECT_ID('tempdb..#ArgusTimeStamp')		IS NOT NULL DROP TABLE #ArgusTimeStamp;
IF OBJECT_ID('tempdb..#ArgusPriceType')		IS NOT NULL DROP TABLE #ArgusPriceType;
IF OBJECT_ID('tempdb..#ArgusCategory')		IS NOT NULL DROP TABLE #ArgusCategory;
IF OBJECT_ID('tempdb..#ArgusCodes')			IS NOT NULL DROP TABLE #ArgusCodes;
IF OBJECT_ID('tempdb..#ArgusModules')		IS NOT NULL DROP TABLE #ArgusModules;
IF OBJECT_ID('tempdb..#ArgusModuleDetail')	IS NOT NULL DROP TABLE #ArgusModuleDetail;
IF OBJECT_ID('tempdb..#ArgusQuotes')		IS NOT NULL DROP TABLE #ArgusQuotes;

CREATE TABLE #ArgusTimeStamp
(
	[TS_TimeStampId]			TINYINT			NOT	NULL,
	[TS_Description]			VARCHAR(26)		NOT	NULL,
	PRIMARY KEY CLUSTERED([TS_TimeStampId] ASC)
);

BULK INSERT #ArgusTimeStamp
FROM 'C:\FtpPricing\Seed\ArgusCsv\Reference\latestTimeStamp.csv'
WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);

CREATE TABLE #ArgusPriceType
(
	[PT_PriceTypeId]			TINYINT			NOT	NULL,
	[PT_Description]			VARCHAR(17)		NOT	NULL,
	PRIMARY KEY CLUSTERED([PT_PriceTypeId] ASC)
);

BULK INSERT	#ArgusPriceType
FROM 'C:\FtpPricing\Seed\ArgusCsv\Reference\latestPriceType.csv'
WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);

CREATE TABLE #ArgusCategory
(
	[AC_Code]					CHAR(9)			NOT	NULL,
	[AC_DisplayName]			VARCHAR(128)	NOT	NULL,
	[AC_Category]				VARCHAR(64)		NOT	NULL,
	PRIMARY KEY CLUSTERED([AC_Code] ASC, [AC_Category] ASC)
);

BULK INSERT #ArgusCategory
FROM 'C:\FtpPricing\Seed\ArgusCsv\Reference\latestCategory.csv'
WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);

CREATE TABLE #ArgusCodes
(
	[AC_Code]					CHAR(9)			NOT	NULL,
	[AC_DisplayName]			VARCHAR(128)	NOT	NULL,
	[AC_DeliveryMode]			VARCHAR(12)		NOT	NULL,
	[AC_Unit]					VARCHAR(16)		NOT	NULL,
	[AC_Frequency]				VARCHAR(12)		NOT	NULL,
	PRIMARY KEY CLUSTERED([AC_Code] ASC, [AC_DeliveryMode] ASC)
);

BULK INSERT #ArgusCodes
FROM 'C:\FtpPricing\Seed\ArgusCsv\Reference\latestCodes.csv'
WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);

CREATE TABLE #ArgusModules
(
	[AM_Module]					VARCHAR(16)		NOT	NULL,
	[AM_Path]					VARCHAR(24)		NOT	NULL,
	[AM_FileName]				VARCHAR(16)		NOT	NULL,
	[AM_Description]			VARCHAR(64)		NOT	NULL,
	[AM_Folder]					VARCHAR(24)		NOT	NULL,
	[AM_Time]					CHAR(12)		NOT	NULL,
	[AM_LocalTime]				TIME			NOT	NULL,
	[AM_LocalTimeZone]			VARCHAR(24)		NOT	NULL,
	PRIMARY KEY CLUSTERED([AM_Module] ASC)
);

BULK INSERT #ArgusModules
FROM 'C:\FtpPricing\Seed\ArgusCsv\Reference\latestModules.csv'
WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);

CREATE TABLE #ArgusModuleDetail
(
	[MD_Module]					VARCHAR(16)		NOT	NULL,
	[MD_Code]					CHAR(9)			NOT	NULL,
	[MD_TimeStampId]			TINYINT			NOT	NULL,
	[MD_PriceTypeId]			TINYINT			NOT	NULL,
	[MD_ContForwardPeriod]		TINYINT			NOT	NULL,
	[MD_StartDate]				DATE			NOT	NULL,
	[MD_EndDate]				DATE			NOT	NULL
);

BULK INSERT #ArgusModuleDetail
FROM 'C:\FtpPricing\Seed\ArgusCsv\Reference\latestModuleDetails.csv'
WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);

CREATE TABLE #ArgusQuotes
(
	[AQ_Code]						CHAR(9)			NOT	NULL,
	[AQ_ContForwardPeriod]			INT				NOT	NULL,
	[AQ_Timing]						VARCHAR(12)		NOT	NULL,
	[AQ_FwdPersiodDescription]		VARCHAR(128)	NOT	NULL,
	[AQ_TimeStampId]				INT				NOT	NULL,
	[AQ_PriceTypeId]				INT				NOT	NULL,
	[AQ_DifferentialBasis]			VARCHAR(56)		NOT	NULL,
	[AQ_DifferentialBasisTiming]	VARCHAR(5)		NOT	NULL,
	[AQ_StartDate]					DATE			NOT	NULL,
	[AQ_EndDate]					DATE				NULL,
	[AQ_OldCode]					VARCHAR(9)		NOT	NULL,
	[AQ_DecimalPlaces]				INT				NOT	NULL
)

BULK INSERT #ArgusQuotes
FROM 'C:\FtpPricing\Seed\ArgusCsv\Reference\latestQuotes.csv'
WITH(FIELDTERMINATOR = ',', ROWTERMINATOR = '\n', FIRSTROW = 2);

INSERT INTO [ref].[AR_TimeStamp_Posit]
(
	[AR_TimeStampId],
	[AR_Description]
)
SELECT
	[t].[TS_TimeStampId],
	[t].[TS_Description]
FROM
	#ArgusTimeStamp					[t]
LEFT OUTER JOIN
	[ref].[AR_TimeStamp_Posit]		[r]
		ON	[r].[AR_TimeStampId]	= [t].[TS_TimeStampId]
WHERE
	[r].[AR_ID]	IS NULL;

INSERT INTO [ref].[AR_PriceType_Posit]
(
	[AR_PriceTypeId],
	[AR_Description]
)
SELECT
	[t].[PT_PriceTypeId],
	[t].[PT_Description]
FROM
	#ArgusPriceType					[t]
LEFT OUTER JOIN
	[ref].[AR_PriceType_Posit]	[r]
		ON	[r].[AR_PriceTypeId]	= [t].[PT_PriceTypeId]
WHERE
	[r].[AR_ID]	IS NULL;

INSERT INTO [ref].[AR_Category_Posit]
(
	[AR_Code],
	[AR_DisplayName],
	[AR_Category]
)
SELECT
	[t].[AC_Code],
	[t].[AC_DisplayName],
	[t].[AC_Category]
FROM
	#ArgusCategory					[t]
LEFT OUTER JOIN
	[ref].[AR_Category_Posit]		[r]
		ON	[r].[AR_Code]			= [t].[AC_Code]
		AND	[r].[AR_Category]		= [t].[AC_Category]
WHERE
	[r].[AR_ID]	IS NULL;

INSERT INTO [ref].[AR_Codes_Posit]
(
	[AR_Code],
	[AR_DisplayName],
	[AR_DeliveryMode],
	[AR_Unit],
	[AR_Frequency]
)
SELECT
	[t].[AC_Code],
	[t].[AC_DisplayName],
	[t].[AC_DeliveryMode],
	[t].[AC_Unit],
	[t].[AC_Frequency]
FROM
	#ArgusCodes						[t]
LEFT OUTER JOIN
	[ref].[AR_Codes_Posit]			[r]
		ON	[r].[AR_Code]			= [t].[AC_Code]
		AND	[r].[AR_DeliveryMode]	= [t].[AC_DeliveryMode]
WHERE
	[r].[AR_ID]	IS NULL;

INSERT INTO [ref].[AR_Modules_Posit]
(
	[AR_Module],
	[AR_Path],
	[AR_FileName],
	[AR_Description],
	[AR_Folder],
	[AR_Time],
	[AR_LocalTime],
	[AR_LocalTimeZone]
)
SELECT
	[t].[AM_Module],
	[t].[AM_Path],
	[t].[AM_FileName],
	[t].[AM_Description],
	[t].[AM_Folder],
	[t].[AM_Time],
	[t].[AM_LocalTime],
	[t].[AM_LocalTimeZone]
FROM
	#ArgusModules					[t]
LEFT OUTER JOIN
	[ref].[AR_Modules_Posit]		[r]
		ON	[r].[AR_Module]			= [t].[AM_Module]
WHERE
	[r].[AR_ID]	IS NULL;

INSERT INTO [ref].[AR_ModuleDetails_Posit]
(
	[AR_Module],
	[AR_Code],
	[AR_TimeStampId],
	[AR_PriceTypeId],
	[AR_ContForwardPeriod],
	[AR_StartDate],
	[AR_EndDate]
)
SELECT
	[t].[MD_Module],
	[t].[MD_Code],
	[t].[MD_TimeStampId],
	[t].[MD_PriceTypeId],
	[t].[MD_ContForwardPeriod],
	[t].[MD_StartDate],
	[t].[MD_EndDate]
FROM
	#ArgusModuleDetail				[t]
LEFT OUTER JOIN
	[ref].[AR_ModuleDetails_Posit]	[r]
		ON	[r].[AR_Module]				= [t].[MD_Module]
		AND	[r].[AR_Code]				= [t].[MD_Code]
		AND	[r].[AR_TimeStampId]		= [t].[MD_TimeStampId]
		AND	[r].[AR_PriceTypeId]		= [t].[MD_PriceTypeId]
		AND [r].[AR_ContForwardPeriod]	= [t].[MD_ContForwardPeriod]
		AND	[r].[AR_StartDate]			= [t].[MD_StartDate]
		AND	[r].[AR_EndDate]			= [t].[MD_EndDate]
WHERE
	[r].[AR_ID]	IS NULL;

INSERT INTO [ref].[AR_Quotes_Posit]
(
	[AR_Code],
	[AR_ContForwardPeriod],
	[AR_Timing],
	[AR_FwdPersiodDescription],
	[AR_TimeStampId],
	[AR_PriceTypeId],
	[AR_DifferentialBasis],
	[AR_DifferentialBasisTiming],
	[AR_StartDate],
	[AR_EndDate],
	[AR_OldCode],
	[AR_DecimalPlaces]
)
SELECT DISTINCT
	[t].[AQ_Code],
	[t].[AQ_ContForwardPeriod],
	[t].[AQ_Timing]	,
	[t].[AQ_FwdPersiodDescription],
	[t].[AQ_TimeStampId],
	[t].[AQ_PriceTypeId],
	[t].[AQ_DifferentialBasis],
		[AQ_DifferentialBasisTiming]	= CASE WHEN [t].[AQ_DifferentialBasisTiming] <> '' THEN [t].[AQ_DifferentialBasisTiming] ELSE NULL END,
	[t].[AQ_StartDate],
	[t].[AQ_EndDate],
		[AQ_OldCode]					= CASE WHEN [t].[AQ_OldCode] <> '' THEN [t].[AQ_OldCode] ELSE NULL END,
	[t].[AQ_DecimalPlaces]
FROM
	#ArgusQuotes					[t]
LEFT OUTER JOIN
	[ref].[AR_Quotes_Posit]			[r]
		ON	[r].[AR_Code]						= [t].[AQ_Code]
		AND	[r].[AR_ContForwardPeriod]			= [t].[AQ_ContForwardPeriod]
		AND	[r].[AR_Timing]						= [t].[AQ_Timing]
		AND	[r].[AR_FwdPersiodDescription]		= [t].[AQ_FwdPersiodDescription]
		AND	[r].[AR_TimeStampId]				= [t].[AQ_TimeStampId]
		AND	[r].[AR_PriceTypeId]				= [t].[AQ_PriceTypeId]
		AND	[r].[AR_DifferentialBasis]			= [t].[AQ_DifferentialBasis]
		AND	[r].[AR_DifferentialBasisTiming]	= [t].[AQ_DifferentialBasisTiming]
		AND	[r].[AR_StartDate]					= [t].[AQ_StartDate]
		AND	[r].[AR_OldCode]					= [t].[AQ_OldCode]
		AND	[r].[AR_DecimalPlaces]				= [t].[AQ_DecimalPlaces]
WHERE
	[r].[AR_ID]	IS NULL;

IF OBJECT_ID('tempdb..#ArgusTimeStamp')		IS NOT NULL DROP TABLE #ArgusTimeStamp;
IF OBJECT_ID('tempdb..#ArgusPriceType')		IS NOT NULL DROP TABLE #ArgusPriceType;
IF OBJECT_ID('tempdb..#ArgusCategory')		IS NOT NULL DROP TABLE #ArgusCategory;
IF OBJECT_ID('tempdb..#ArgusCodes')			IS NOT NULL DROP TABLE #ArgusCodes;
IF OBJECT_ID('tempdb..#ArgusModules')		IS NOT NULL DROP TABLE #ArgusModules;
IF OBJECT_ID('tempdb..#ArgusModuleDetail')	IS NOT NULL DROP TABLE #ArgusModuleDetail;
IF OBJECT_ID('tempdb..#ArgusQuotes')		IS NOT NULL DROP TABLE #ArgusQuotes;
