﻿PRINT 'insert.dim.Operator...';

INSERT INTO [dim].[OP_Operator_LookUp]([OP_OperatorId])
SELECT [t].[OperatorId]
FROM (VALUES
	('+'),
	('-'),
	('~')
	)[t]([OperatorId])
LEFT OUTER JOIN
	[dim].[OP_Operator_LookUp]	[l]
		ON	[l].[OP_OperatorId]	= [t].[OperatorId]
WHERE
	[l].[OP_OperatorId]	IS NULL;