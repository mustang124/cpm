﻿CREATE TABLE [dim].[LO_Location_Parent]
(
	[LO_MethodologyId]		INT					NOT	NULL,
	[LO_LocationId]			INT					NOT NULL	CONSTRAINT [FK__LO_Location_Parent_LO_LocationId]		REFERENCES [dim].[LO_Location_LookUp]([LO_LocationId]),
	[LO_ParentId]			INT					NOT NULL	CONSTRAINT [FK__LO_Location_Parent_LO_Parent]			REFERENCES [dim].[LO_Location_LookUp]([LO_LocationId])
															CONSTRAINT [FK__LO_Location_Parent_LO_ParentParent]
															FOREIGN KEY ([LO_MethodologyId], [LO_LocationId])
															REFERENCES [dim].[LO_Location_Parent]([LO_MethodologyId], [LO_LocationId]),

	[LO_Operator]			CHAR(1)				NOT	NULL	CONSTRAINT [DF__LO_Location_Parent_LO_Operator]			DEFAULT('+')
															CONSTRAINT [FK__LO_Location_Parent_LO_Operator]			REFERENCES [dim].[OP_Operator_LookUp]([OP_OperatorId]),
	[LO_SortKey]			INT					NOT	NULL,
	[LO_Hierarchy]			SYS.HIERARCHYID		NOT	NULL,

	[LO_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__LO_Location_Parent_LO_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__LO_Location_Parent_LO_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[LO_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__LO_Location_Parent_LO_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[LO_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__LO_Location_Parent_LO_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__LO_Location_Parent_LO_RowGuid]			UNIQUE NONCLUSTERED([LO_RowGuid]),

	CONSTRAINT [PK__LO_Location_Parent]			PRIMARY KEY CLUSTERED ([LO_MethodologyId] DESC, [LO_LocationId] ASC)
);