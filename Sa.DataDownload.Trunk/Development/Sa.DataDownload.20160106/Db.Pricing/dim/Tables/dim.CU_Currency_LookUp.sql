﻿CREATE TABLE [dim].[CU_Currency_LookUp]
(
	[CU_CurrencyId]				INT					NOT NULL	IDENTITY(1, 1),

	[CU_Tag]					VARCHAR(4)			NOT	NULL	CONSTRAINT [CL__CU_Currency_LookUp_CU_Tag]				CHECK([CU_Tag] <> ''),
																CONSTRAINT [UK__CU_Currency_LookUp_CU_Tag]				UNIQUE CLUSTERED([CU_Tag] ASC),

	[CU_Name]					NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__CU_Currency_LookUp_CU_Name]				CHECK([CU_Name] <> '')
																CONSTRAINT [UX__CU_Currency_LookUp_CU_Name]				UNIQUE([CU_Name] ASC),

	[CU_Detail]					NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__CU_Currency_LookUp_CU_Detail]			CHECK([CU_Detail] <> '')
																CONSTRAINT [UX__CU_Currency_LookUp_CU_Detail]			UNIQUE([CU_Detail] ASC),

	[CU_Glyph]					NCHAR(1)				NULL	CONSTRAINT [CL__CU_Currency_LookUp_CU_Glyph]			CHECK([CU_Glyph] <> ''),

	[CU_SubUnit]				VARCHAR(48)				NULL	CONSTRAINT [CL__CU_Currency_LookUp_CU_SubUnit]			CHECK([CU_SubUnit] <> ''),
	[CU_SubUnitGlyph]			NCHAR(1)				NULL	CONSTRAINT [CL__CU_Currency_LookUp_CU_SubUnitGlyph]		CHECK([CU_SubUnitGlyph] <> ''),

	[CU_MultiplierSubUnit]		FLOAT					NULL	CONSTRAINT [CR__CU_Currency_LookUp_CU_MultiplierSubUnit]	CHECK([CU_MultiplierSubUnit] > 0.0),
	[CU_MultiplierStored]		FLOAT					NULL	CONSTRAINT [CR__CU_Currency_LookUp_CU_MultiplierStored]		CHECK([CU_MultiplierStored] > 0.0),
	[CU_MultiplierReported]		FLOAT					NULL	CONSTRAINT [CR__CU_Currency_LookUp_CU_MultiplierReported]	CHECK([CU_MultiplierReported] > 0.0),
	[CU_LastUsed_Year]			SMALLINT				NULL	CONSTRAINT [CR__CU_Currency_LookUp_CU_LastUsed_Year]		CHECK([CU_LastUsed_Year] > 1990),

	[CU_ReplacementCurrencyID]	INT						NULL	CONSTRAINT [FK__CU_Currency_LookUp_CU_ReplacementCurrencyID]
																REFERENCES [dim].[CU_Currency_LookUp]([CU_CurrencyId]),

	[CU_ISONumeric]				VARCHAR(4)				NULL	CONSTRAINT [CL__CU_Currency_LookUp_CU_ISONumeric]		CHECK([CU_ISONumeric] <> ''),

	[CU_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__CU_Currency_LookUp_CU_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__CU_Currency_LookUp_CU_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CU_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CU_Currency_LookUp_CU_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CU_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CU_Currency_LookUp_CU_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CU_Currency_LookUp_CU_RowGuid]			UNIQUE NONCLUSTERED([CU_RowGuid]),

	CONSTRAINT [PK__CU_Currency_LookUp]			PRIMARY KEY([CU_CurrencyId] ASC)
);