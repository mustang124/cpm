﻿CREATE TABLE [dim].[DS_DataSource_Parent]
(
	[DS_MethodologyId]		INT					NOT	NULL,
	[DS_DataSourceId]		INT					NOT NULL	CONSTRAINT [FK__DS_DataSource_Parent_DS_DataSourceId]		REFERENCES [dim].[DS_DataSource_LookUp]([DS_DataSourceId]),
	[DS_ParentId]			INT					NOT NULL	CONSTRAINT [FK__DS_DataSource_Parent_DS_Parent]				REFERENCES [dim].[DS_DataSource_LookUp]([DS_DataSourceId])
															CONSTRAINT [FK__DS_DataSource_Parent_DS_ParentParent]		FOREIGN KEY([DS_MethodologyId], [DS_DataSourceId])
																														REFERENCES [dim].[DS_DataSource_Parent]([DS_MethodologyId], [DS_DataSourceId]),

	[DS_Operator]			CHAR(1)				NOT	NULL	CONSTRAINT [DF__DS_DataSource_Parent_DS_Operator]			DEFAULT('+')
															CONSTRAINT [FK__DS_DataSource_Parent_DS_Operator]			REFERENCES [dim].[OP_Operator_LookUp]([OP_OperatorId]),
	[DS_SortKey]			INT					NOT	NULL,
	[DS_Hierarchy]			SYS.HIERARCHYID		NOT	NULL,

	[DS_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__DS_DataSource_Parent_DS_PositedBy]			DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__DS_DataSource_Parent_DS_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[DS_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__DS_DataSource_Parent_DS_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[DS_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__DS_DataSource_Parent_DS_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__DS_DataSource_Parent_DS_RowGuid]			UNIQUE NONCLUSTERED([DS_RowGuid]),

	CONSTRAINT [PK__DS_DataSource_Parent]			PRIMARY KEY CLUSTERED ([DS_MethodologyId] DESC, [DS_DataSourceId] ASC)
);