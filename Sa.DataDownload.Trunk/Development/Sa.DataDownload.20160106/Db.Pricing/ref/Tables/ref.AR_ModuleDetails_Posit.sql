﻿CREATE TABLE [ref].[AR_ModuleDetails_Posit]
(
	[AR_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[AR_Module]					VARCHAR(16)			NOT	NULL	CONSTRAINT [CL__AR_ModuleDetails_Posit_AR_Module]				CHECK([AR_Module] <> ''),
	[AR_Code]					CHAR(9)				NOT	NULL	CONSTRAINT [CL__AR_ModuleDetails_Posit_AR_Code]					CHECK([AR_Code] <> ''),
	[AR_TimeStampId]			TINYINT				NOT	NULL	CONSTRAINT [FK__AR_ModuleDetails_Posit_AR_TimeStamp_Posit]		REFERENCES [ref].[AR_TimeStamp_Posit]([AR_TimeStampId]),
	[AR_PriceTypeId]			TINYINT				NOT	NULL	CONSTRAINT [FK__AR_ModuleDetails_Posit_AR_PricingType_Posit]	REFERENCES [ref].[AR_PriceType_Posit]([AR_PriceTypeId]),
	[AR_ContForwardPeriod]		TINYINT				NOT	NULL,
	[AR_StartDate]				DATE				NOT	NULL,
	[AR_EndDate]				DATE				NOT	NULL,

	[AR_ChangedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_ModuleDetails_Posit_AR_ChangedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__AR_ModuleDetails_Posit_AR_PositedBy]			DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__AR_ModuleDetails_Posit_AR_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AR_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_ModuleDetails_Posit_AR_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__AR_ModuleDetails_Posit_AR_Reliability]			DEFAULT(50),
	[AR_PositReliable]			AS CONVERT(BIT, CASE WHEN [AR_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[AR_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AR_ModuleDetails_Posit_AR_RowGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__AR_ModuleDetails_Posit_AR_RowGuid]				UNIQUE NONCLUSTERED([AR_RowGuid]),

	CONSTRAINT [PK__AR_ModuleDetails_Posit] PRIMARY KEY([AR_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__AR_ModuleDetails_Posit] UNIQUE CLUSTERED([AR_ID] ASC)
		WITH(FILLFACTOR = 95)
);