﻿CREATE TABLE [ref].[AR_Quotes_Posit]
(
	[AR_ID]							INT					NOT	NULL	IDENTITY(1, 1),

	[AR_Code]						CHAR(9)				NOT	NULL	CONSTRAINT [CL__AR_Quotes_Posit_AR_Code]						CHECK([AR_Code] <> ''),
	[AR_ContForwardPeriod]			INT					NOT	NULL,
	[AR_Timing]						VARCHAR(12)			NOT	NULL	CONSTRAINT [CL__AR_Quotes_Posit_AR_Timing]						CHECK([AR_Timing] <> ''),
	[AR_FwdPersiodDescription]		VARCHAR(128)		NOT	NULL	CONSTRAINT [CL__AR_Quotes_Posit_AR_FwdPersiodDescription]		CHECK([AR_FwdPersiodDescription] <> ''),
	[AR_TimeStampId]				INT					NOT	NULL,
	[AR_PriceTypeId]				INT					NOT	NULL,
	[AR_DifferentialBasis]			VARCHAR(56)			NOT	NULL	CONSTRAINT [CL__AR_Quotes_Posit_AR_DifferentialBasis]			CHECK([AR_DifferentialBasis] <> ''),
	[AR_DifferentialBasisTiming]	VARCHAR(5)				NULL	CONSTRAINT [CL__AR_Quotes_Posit_AR_DifferentialBasisTiming]		CHECK([AR_DifferentialBasisTiming] <> ''),
	[AR_StartDate]					DATE				NOT	NULL,
	[AR_EndDate]					DATE					NULL,
	[AR_OldCode]					VARCHAR(9)				NULL	CONSTRAINT [CL__AR_Quotes_Posit_AR_OldCode]						CHECK([AR_OldCode] <> ''),
	[AR_DecimalPlaces]				INT					NOT	NULL,

	[AR_ChangedAt]					DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_Quotes_Posit_AR_ChangedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositedBy]					INT					NOT	NULL	CONSTRAINT [DF__AR_Quotes_Posit_AR_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																	CONSTRAINT [FK__AR_Quotes_Posit_AR_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AR_PositedAt]					DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AR_Quotes_Posit_AR_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[AR_PositReliability]			TINYINT				NOT	NULL	CONSTRAINT [DF__AR_Quotes_Posit_AR_Reliability]		DEFAULT(50),
	[AR_PositReliable]				AS CONVERT(BIT, CASE WHEN [AR_PositReliability] > 0 THEN 1 ELSE 0 END)
									PERSISTED			NOT	NULL,
	[AR_RowGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AR_Quotes_Posit_AR_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																	CONSTRAINT [UX__AR_Quotes_Posit_AR_RowGuid]			UNIQUE NONCLUSTERED([AR_RowGuid]),

	CONSTRAINT [PK__AR_Quotes_Posit] PRIMARY KEY([AR_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__AR_Quotes_Posit] UNIQUE CLUSTERED([AR_ID] ASC)
		WITH(FILLFACTOR = 95)
);