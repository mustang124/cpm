﻿CREATE TABLE [stage].[PA_PricingArgusCsv_Error]
(
	[PA_ID]						INT					NOT	NULL	IDENTITY(1, 1),
	
	[PA_FilePath]				VARCHAR(260)			NULL,
	[PA_Code]					CHAR(9)					NULL,

	[PA_TimeStampId]			TINYINT					NULL,
	[PA_PriceTypeId]			TINYINT					NULL,
	[PA_Date]					DATE					NULL,
	[PA_Value]					DECIMAL(14, 4)			NULL,
	[PA_ContForwardPeriod]		SMALLINT				NULL,
	[PA_DiffBaseRoll]			SMALLINT				NULL,
	[PA_Year]					SMALLINT				NULL,
	[PA_ContFwd]				TINYINT					NULL,
	[PA_RecordStatus]			CHAR(1)					NULL,

	[PA_ChangedAt]				DATETIMEOFFSET(7)		NULL,
	[PA_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Error_PA_PositedBy]			DEFAULT([posit].[Get_PositorId]()),
	[PA_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Error_PA_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[PA_PositReliability]		TINYINT				NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Error_PA_Reliability]		DEFAULT(50),
	[PA_PositReliable]			AS CONVERT(BIT, CASE WHEN [PA_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[PA_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PA_PricingArgusCsv_Error_PA_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__PA_PricingArgusCsv_Error_PA_RowGuid]			UNIQUE NONCLUSTERED([PA_RowGuid]),

	[PA_ErrorCode]				INT						NULL,
	[PA_ErrorColumn]			INT						NULL,

	CONSTRAINT [PK__PA_PricingArgusCsv_Error] PRIMARY KEY([PA_ID] ASC)
		WITH(FILLFACTOR = 95),
	CONSTRAINT [UK__PA_PricingArgusCsv_Error] UNIQUE CLUSTERED([PA_Code] ASC, [PA_PriceTypeId] ASC, [PA_TimeStampId] ASC, [PA_RecordStatus] ASC, [PA_ChangedAt] ASC, [PA_PositedAt] ASC)
		WITH(FILLFACTOR = 95)
);