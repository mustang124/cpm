﻿CREATE TABLE [etl].[MP_Platts_Posit]
(
	[MP_MapId]					INT					NOT NULL	IDENTITY(1, 1),

	[MP_ItemSymbol]				CHAR(7)				NOT	NULL	CONSTRAINT [CL__MP_Platts_Posit_MP_ItemSymbol]			CHECK([MP_ItemSymbol] <> ''),

	[MP_UomId]					INT					NOT	NULL	CONSTRAINT [FK__MP_Platts_Posit_MP_UomId]				REFERENCES [dim].[UM_Uom_LookUp]([UM_UomId]),
	[MP_UomMultiplier]			FLOAT				NOT	NULL,
	[MP_CurrencyId]				INT					NOT	NULL	CONSTRAINT [FK__MP_Platts_Posit_MP_CurrencyId]			REFERENCES [dim].[CU_Currency_LookUp]([CU_CurrencyId]),
	[MP_CurrencyMultiplier]		FLOAT				NOT	NULL,

	[MP_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__MP_Platts_Posit_MP_PositedBy]			DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__MP_Platts_Posit_MP_PositedBy]			REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[MP_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__MP_Platts_Posit_MP_PositedAt]			DEFAULT(SYSDATETIMEOFFSET()),
	[MP_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__MP_Platts_Posit_MP_RowGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__MP_Platts_Posit_MP_RowGuid]				UNIQUE NONCLUSTERED([MP_RowGuid]),

	CONSTRAINT [PK__MP_Platts_Posit]		PRIMARY KEY([MP_MapId]),
	CONSTRAINT [UK__MP_Platts_Posit]		UNIQUE CLUSTERED([MP_ItemSymbol] ASC),
);