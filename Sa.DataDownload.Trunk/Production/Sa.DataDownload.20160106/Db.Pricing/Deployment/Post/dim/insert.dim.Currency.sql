﻿PRINT 'insert.dim.Currency...';

DECLARE @Currency	TABLE
(
	[CU_Tag]					VARCHAR(4)			NOT	NULL	CHECK([CU_Tag] <> ''),
																UNIQUE CLUSTERED([CU_Tag] ASC),

	[CU_Name]					NVARCHAR(48)		NOT	NULL	CHECK([CU_Name] <> '')
																UNIQUE([CU_Name] ASC),

	[CU_Detail]					NVARCHAR(48)		NOT	NULL	CHECK([CU_Detail] <> '')
																UNIQUE([CU_Detail] ASC),

	[CU_Glyph]					NCHAR(1)				NULL	CHECK([CU_Glyph] <> ''),

	[CU_SubUnit]				VARCHAR(48)				NULL	CHECK([CU_SubUnit] <> ''),
	[CU_SubUnitGlyph]			NCHAR(1)				NULL	CHECK([CU_SubUnitGlyph] <> ''),

	[CU_MultiplierSubUnit]		FLOAT					NULL	CHECK([CU_MultiplierSubUnit] > 0.0),
	[CU_MultiplierStored]		FLOAT					NULL	CHECK([CU_MultiplierStored] > 0.0),
	[CU_MultiplierReported]		FLOAT					NULL	CHECK([CU_MultiplierReported] > 0.0),
	[CU_LastUsed_Year]			SMALLINT				NULL	CHECK([CU_LastUsed_Year] > 1990),

	[CU_ReplacementCurrencyID]	VARCHAR(4)				NULL,

	[CU_ISONumeric]				VARCHAR(4)				NULL	CHECK([CU_ISONumeric] <> '')
)

INSERT INTO @Currency([CU_Tag], [CU_Name], [CU_Detail], [CU_Glyph],
	[CU_SubUnit], [CU_SubUnitGlyph],
	[CU_MultiplierSubUnit], [CU_MultiplierStored], [CU_MultiplierReported],
	[CU_LastUsed_Year], [CU_ReplacementCurrencyID], [CU_ISONumeric]
	)
SELECT [t].[CU_Tag], [t].[CU_Name], [t].[CU_Detail], [t].[CU_Glyph],
	[t].[CU_SubUnit], [t].[CU_SubUnitGlyph],
	[t].[CU_MultiplierSubUnit], [t].[CU_MultiplierStored], [t].[CU_MultiplierReported],
	[t].[CU_LastUsed_Year], [t].[CU_ReplacementCurrencyID], [t].[CU_ISONumeric]
FROM (VALUES
	('AED', 'UAE Dirham', 'UAE Dirham', NULL, 'Fils', NULL, 100, 1000, 1, NULL, NULL, '784'),
	('AFN', 'Afghani', 'Afghani', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '971'),
	('ALL', 'Lek', 'Lek', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '008'),
	('AMD', 'Armenian Dram', 'Armenian Dram', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '051'),
	('ANG', 'Netherlands Antillian Guilder', 'Netherlands Antillian Guilder', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '532'),
	('AOA', 'Kwanza', 'Kwanza', NULL, 'Centimos', NULL, 100, 1000, 1, NULL, NULL, '973'),
	('ARS', 'Argentine Peso', 'Argentine Peso', NULL, 'Centavos', NULL, 100, 1000, 1, NULL, NULL, '032'),
	('AUD', 'Australian Dollar', 'Australian Dollar', NULL, 'A. Cents', NULL, 100, 1000, 1, NULL, NULL, '036'),
	('AWG', 'Aruban Guilder', 'Aruban Guilder', NULL, 'Arubian Cents', NULL, 100, 1000, 1, NULL, NULL, '533'),
	('AZN', 'Azerbaijanian Manat', 'Azerbaijanian Manat', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '944'),
	('BAM', 'Convertible Marks', 'Convertible Marks', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '977'),
	('BBD', 'Barbados Dollar', 'Barbados Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '052'),
	('BDT', 'Taka', 'Taka', NULL, 'Paisa', NULL, 100, 1000, 1, NULL, NULL, '050'),
	('BGN', 'Bulgarian Lev', 'Bulgarian Lev', NULL, 'Stotinki', NULL, 100, 1000, 1, NULL, NULL, '975'),
	('BHD', 'Bahraini Dinar', 'Bahraini Dinar', NULL, 'Fils', NULL, 1000, 1000, 1, NULL, NULL, '048'),
	('BIF', 'Burundi Franc', 'Burundi Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '108'),
	('BMD', 'Bermudian Dollar', 'Bermudian Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '060'),
	('BND', 'Brunei Dollar', 'Brunei Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '096'),
	('BOB', 'Boliviano', 'Boliviano', NULL, 'Centavos', NULL, 100, 1000, 1, NULL, NULL, '068'),
	('BOV', 'Mvdol', 'Mvdol', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '984'),
	('BRL', 'Brazilian Real', 'Brazilian Real', NULL, 'Centavos', NULL, 100, 1000, 1, NULL, NULL, '986'),
	('BSD', 'Bahamian Dollar', 'Bahamian Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '044'),
	('BTN', 'Ngultrum', 'Ngultrum', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '064'),
	('BWP', 'Pula', 'Pula', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '072'),
	('BYR', 'Belarussian Ruble', 'Belarussian Ruble', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '974'),
	('BZD', 'Belize Dollar', 'Belize Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '084'),
	('CAD', 'Canadian Dollar', 'Canadian Dollar', NULL, 'Canadian Cents', NULL, 100, 1000, 1, NULL, NULL, '124'),
	('CADX', 'Canadian Dollar (Special)', 'Canadian Dollar (Special)', NULL, 'Canadian Cents', NULL, 100, 1000, 1, NULL, NULL, NULL),
	('CDF', 'Congolese Franc', 'Congolese Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '976'),
	('CHE', 'WIR Euro', 'WIR Euro', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '947'),
	('CHF', 'Swiss Franc', 'Swiss Franc', NULL, 'Rappen', NULL, 100, 1000, 1, NULL, NULL, '756'),
	('CHW', 'WIR Franc', 'WIR Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '948'),
	('CLF', 'Unidades de fomento', 'Unidades de fomento', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '990'),
	('CLP', 'Chilean Peso', 'Chilean Peso', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '152'),
	('CNY', 'Yuan Renminbi', 'Yuan Renminbi', NULL, NULL, NULL, NULL, 1000, 1, NULL, NULL, '156'),
	('COP', 'Colombian Peso', 'Colombian Peso', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '170'),
	('COU', 'Unidad de Valor Real', 'Unidad de Valor Real', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '970'),
	('CRC', 'Costa Rican Colon', 'Costa Rican Colon', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '188'),
	('CUC', 'Peso Convertible', 'Peso Convertible', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '931'),
	('CUP', 'Cuban Peso', 'Cuban Peso', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '192'),
	('CVE', 'Cape Verde Escudo', 'Cape Verde Escudo', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '132'),
	('CZK', 'Czech Koruna', 'Czech Koruna', NULL, 'Haleru', NULL, 100, 1000, 1, NULL, NULL, '203'),
	('DJF', 'Djibouti Franc', 'Djibouti Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '262'),
	('DKK', 'Danish Krone', 'Danish Krone', NULL, 'Øre', NULL, 100, 1000, 1, NULL, NULL, '208'),
	('DOP', 'Dominican Peso', 'Dominican Peso', NULL, 'Centavos', NULL, 100, 1000, 1, NULL, NULL, '214'),
	('DZD', 'Algerian Dinar', 'Algerian Dinar', NULL, 'Santeem', NULL, 100, 1000, 1, NULL, NULL, '012'),
	('EEK', 'Kroon', 'Kroon', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '233'),
	('EGP', 'Egyptian Pound', 'Egyptian Pound', NULL, 'Piasters', NULL, 100, 1000, 1, NULL, NULL, '818'),
	('ERN', 'Nakfa', 'Nakfa', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '232'),
	('ETB', 'Ethiopian Birr', 'Ethiopian Birr', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '230'),
	('EUR', 'Euro', 'Euro', '€', 'Euro Cents', NULL, 100, 1000, 1, NULL, NULL, '978'),
	('FJD', 'Fiji Dollar', 'Fiji Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '242'),
	('FKP', 'Falkland Islands Pound', 'Falkland Islands Pound', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '238'),
	('GBP', 'Pound Sterling', 'Pound Sterling', '£', 'Pence', NULL, 100, 1000, 1, NULL, NULL, '826'),
	('GEL', 'Lari', 'Lari', NULL, 'Tetri', NULL, 100, 1000, 1, NULL, NULL, '981'),
	('GHS', 'Cedi', 'Cedi', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '936'),
	('GIP', 'Gibraltar Pound', 'Gibraltar Pound', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '292'),
	('GMD', 'Dalasi', 'Dalasi', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '270'),
	('GNF', 'Guinea Franc', 'Guinea Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '324'),
	('GTQ', 'Quetzal', 'Quetzal', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '320'),
	('GYD', 'Guyana Dollar', 'Guyana Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '328'),
	('HKD', 'Hong Kong Dollar', 'Hong Kong Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '344'),
	('HNL', 'Lempira', 'Lempira', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '340'),
	('HRK', 'Croatian Kuna', 'Croatian Kuna', NULL, 'Lipas', NULL, 100, 1000, 1, NULL, NULL, '191'),
	('HTG', 'Gourde', 'Gourde', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '332'),
	('HUF', 'Forint', 'Forint', NULL, NULL, NULL, NULL, 1000, 1, '1998', 'Hufk', '348'),
	('IDR', 'Rupiah', 'Rupiah', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '360'),
	('ILS', 'New Israeli Sheqel', 'New Israeli Sheqel', NULL, 'Agorot', NULL, 100, 1000, 1, NULL, NULL, '376'),
	('INR', 'Indian Rupee', 'Indian Rupee', NULL, 'Paise', NULL, 100, 1000, 1, NULL, NULL, '356'),
	('IQD', 'Iraqi Dinar', 'Iraqi Dinar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '368'),
	('IRR', 'Iranian Rial', 'Iranian Rial', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '364'),
	('ISK', 'Iceland Krona', 'Iceland Krona', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '352'),
	('JMD', 'Jamaican Dollar', 'Jamaican Dollar', NULL, 'J. Cents', NULL, 100, 1000, 1, NULL, NULL, '388'),
	('JOD', 'Jordanian Dinar', 'Jordanian Dinar', NULL, 'Fils', NULL, 1000, 1000, 1, NULL, NULL, '400'),
	('JPY', 'Yen', 'Yen', '¥', NULL, NULL, NULL, NULL, 1, NULL, NULL, '392'),
	('KES', 'Kenyan Shilling', 'Kenyan Shilling', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '404'),
	('KGS', 'Som', 'Som', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '417'),
	('KHR', 'Riel', 'Riel', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '116'),
	('KMF', 'Comoro Franc', 'Comoro Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '174'),
	('KPW', 'North Korean Won', 'North Korean Won', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '408'),
	('KRW', 'Won', 'Won', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '410'),
	('KWD', 'Kuwaiti Dinar', 'Kuwaiti Dinar', NULL, 'Fils', NULL, 1000, 1000, 1, NULL, NULL, '414'),
	('KYD', 'Cayman Islands Dollar', 'Cayman Islands Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '136'),
	('KZT', 'Tenge', 'Tenge', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '398'),
	('LAK', 'Kip', 'Kip', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '418'),
	('LBP', 'Lebanese Pound', 'Lebanese Pound', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '422'),
	('LKR', 'Sri Lanka Rupee', 'Sri Lanka Rupee', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '144'),
	('LRD', 'Liberian Dollar', 'Liberian Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '430'),
	('LSL', 'Loti', 'Loti', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '426'),
	('LTL', 'Lithuanian Litas', 'Lithuanian Litas', NULL, 'Centu', NULL, 100, 1000, 1, NULL, NULL, '440'),
	('LVL', 'Latvian Lats', 'Latvian Lats', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '428'),
	('LYD', 'Libyan Dinar', 'Libyan Dinar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '434'),
	('MAD', 'Moroccan Dirham', 'Moroccan Dirham', NULL, 'Centimes', NULL, 100, 1000, 1, NULL, NULL, '504'),
	('MDL', 'Moldovan Leu', 'Moldovan Leu', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '498'),
	('MGA', 'Malagasy Ariary', 'Malagasy Ariary', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '969'),
	('MKD', 'Denar', 'Denar', NULL, 'Deni', NULL, 100, 1000, 1, NULL, NULL, '807'),
	('MMK', 'Kyat', 'Kyat', NULL, 'Pyas', NULL, 100, 1000, 1, NULL, NULL, '104'),
	('MNT', 'Tugrik', 'Tugrik', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '496'),
	('MOP', 'Pataca', 'Pataca', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '446'),
	('MRO', 'Ouguiya', 'Ouguiya', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '478'),
	('MUR', 'Mauritius Rupee', 'Mauritius Rupee', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '480'),
	('MVR', 'Rufiyaa', 'Rufiyaa', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '462'),
	('MWK', 'Kwacha', 'Kwacha', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '454'),
	('MXN', 'Mexican Peso', 'Mexican Peso', NULL, 'Centavos', NULL, 100, 1000, 1, NULL, NULL, '484'),
	('MXV', 'Mexican Unidad de Inversion (UDI)', 'Mexican Unidad de Inversion (UDI)', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '979'),
	('MYR', 'Malaysian Ringgit', 'Malaysian Ringgit', NULL, 'Sen', NULL, 100, 1000, 1, NULL, NULL, '458'),
	('MZN', 'Metical', 'Metical', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '943'),
	('NAD', 'Namibia Dollar', 'Namibia Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '516'),
	('NGN', 'Naira', 'Naira', NULL, 'Kobo', NULL, 100, 1000, 1, NULL, NULL, '566'),
	('NIO', 'Cordoba Oro', 'Cordoba Oro', NULL, 'Centavos', NULL, 100, 1000, 1, NULL, NULL, '558'),
	('NOK', 'Norwegian Krone', 'Norwegian Krone', NULL, 'Øre', NULL, 100, 1000, 1, NULL, NULL, '578'),
	('NPR', 'Nepalese Rupee', 'Nepalese Rupee', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '524'),
	('NZD', 'New Zealand Dollar', 'New Zealand Dollar', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '554'),
	('OMR', 'Rial Omani', 'Rial Omani', NULL, 'Baizas', NULL, 1000, 1000, 1, NULL, NULL, '512'),
	('PAB', 'Balboa', 'Balboa', NULL, 'Centesimos', NULL, 100, 1000, 1, NULL, NULL, '590'),
	('PEN', 'Nuevo Sol', 'Nuevo Sol', NULL, 'Centimos', NULL, 100, 1000, 1, NULL, NULL, '604'),
	('PGK', 'Kina', 'Kina', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '598'),
	('PHP', 'Philippine Peso', 'Philippine Peso', NULL, 'Centavos', NULL, 100, 1000, 1, NULL, NULL, '608'),
	('PKR', 'Pakistan Rupee', 'Pakistan Rupee', NULL, 'Paisa', NULL, 100, 1000, 1, NULL, NULL, '586'),
	('PLN', 'Zloty', 'Zloty', NULL, 'Groszy', NULL, 100, 1000, 1, NULL, NULL, '985'),
	('PYG', 'Guarani', 'Guarani', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '600'),
	('QAR', 'Qatari Rial', 'Qatari Rial', NULL, 'Dirhams', NULL, 100, 1000, 1, NULL, NULL, '634'),
	('RON', 'New Leu', 'New Leu', NULL, 'Bani', NULL, 100, 1000, 1, NULL, NULL, '946'),
	('RSD', 'Serbian Dinar', 'Serbian Dinar', NULL, 'Para', NULL, 100, 1000, 1, NULL, NULL, '941'),
	('RUB', 'Russian Ruble', 'Russian Ruble', NULL, 'Kopeks', NULL, 100, 1000, 1, NULL, NULL, '643'),
	('RWF', 'Rwanda Franc', 'Rwanda Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '646'),
	('SAR', 'Saudi Riyal', 'Saudi Riyal', NULL, 'Halalat', NULL, 100, 1000, 1, NULL, NULL, '682'),
	('SBD', 'Solomon Islands Dollar', 'Solomon Islands Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '090'),
	('SCR', 'Seychelles Rupee', 'Seychelles Rupee', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '690'),
	('SDG', 'Sudanese Pound', 'Sudanese Pound', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '938'),
	('SEK', 'Swedish Krona', 'Swedish Krona', NULL, 'Ören', NULL, 100, 1000, 1, NULL, NULL, '752'),
	('SGD', 'Singapore Dollar', 'Singapore Dollar', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '702'),
	('SHP', 'Saint Helena Pound', 'Saint Helena Pound', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '654'),
	('SLL', 'Leone', 'Leone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '694'),
	('SOS', 'Somali Shilling', 'Somali Shilling', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '706'),
	('SRD', 'Surinam Dollar', 'Surinam Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '968'),
	('STD', 'Dobra', 'Dobra', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '678'),
	('SVC', 'El Salvador Colon', 'El Salvador Colon', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '222'),
	('SYP', 'Syrian Pound', 'Syrian Pound', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '760'),
	('SZL', 'Lilangeni', 'Lilangeni', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '748'),
	('THB', 'Baht', 'Baht', NULL, 'Stang', NULL, 100, 1000, 1, NULL, NULL, '764'),
	('TJS', 'Somoni', 'Somoni', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '972'),
	('TMT', 'Manat', 'Manat', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '934'),
	('TND', 'Tunisian Dinar', 'Tunisian Dinar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '788'),
	('TOP', 'Pa''anga', 'Pa''anga', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '776'),
	('TRY', 'Turkish Lira', 'Turkish Lira', NULL, 'Kurus', NULL, 100, 1000, 1, NULL, NULL, '949'),
	('TTD', 'Trinidad and Tobago Dollar', 'Trinidad and Tobago Dollar', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '780'),
	('TWD', 'New Taiwan Dollar', 'New Taiwan Dollar', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '901'),
	('TZS', 'Tanzanian Shilling', 'Tanzanian Shilling', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '834'),
	('UAH', 'Hryvnia', 'Hryvnia', NULL, 'Kopiykas', NULL, 100, 1000, 1, NULL, NULL, '980'),
	('UGX', 'Uganda Shilling', 'Uganda Shilling', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '800'),
	('USD', 'US Dollar', 'US Dollar', '$', 'Us Cents', NULL, 100, 1000, 1, NULL, NULL, '840'),
	('USN', 'US Dollar (Next day)', 'US Dollar (Next day)', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '997'),
	('USS', 'US Dollar (Same day)', 'US Dollar (Same day)', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '998'),
	('UYI', 'Uruguay Peso en Unidades Indexadas', 'Uruguay Peso en Unidades Indexadas', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '940'),
	('UYU', 'Peso Uruguayo', 'Peso Uruguayo', NULL, 'Centésimos', NULL, 100, 1000, 1, NULL, NULL, '858'),
	('UZS', 'Uzbekistan Sum', 'Uzbekistan Sum', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '860'),
	('VEF', 'Bolivar Fuerte', 'Bolivar Fuerte', NULL, 'Céntimos', NULL, 100, 1000, 1, NULL, NULL, '937'),
	('VND', 'Dong', 'Dong', NULL, 'Dong', NULL, 1000, 1000000, 1, NULL, NULL, '704'),
	('VUV', 'Vatu', 'Vatu', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '548'),
	('WST', 'Tala', 'Tala', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '882'),
	('XAF', 'CFA Franc BEAC ‡', 'CFA Franc BEAC ‡', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '950'),
	('XCD', 'East Caribbean Dollar', 'East Caribbean Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '951'),
	('XOF', 'CFA Franc BCEAO †', 'CFA Franc BCEAO †', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '952'),
	('XPF', 'CFP Franc', 'CFP Franc', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '953'),
	('YER', 'Yemeni Rial', 'Yemeni Rial', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '886'),
	('ZAR', 'Rand', 'Rand', NULL, 'Cents', NULL, 100, 1000, 1, NULL, NULL, '710'),
	('ZMK', 'Zambian Kwacha', 'Zambian Kwacha', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '894'),
	('ZWL', 'Zimbabwe Dollar', 'Zimbabwe Dollar', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, '932'),
	('ATS', 'Schilling', 'Austria <= 2000', NULL, NULL, NULL, 100, NULL, NULL, '2000', 'EUR', NULL),
	('BEF', 'Belgium Franc', 'Belgium <= 2000', NULL, NULL, NULL, NULL, NULL, NULL, '2000', 'EUR', NULL),
	('BRCK', 'Cruzeiro', 'Brazil < = 1993', NULL, 'Cruzeiro', NULL, 1000, NULL, NULL, '1992', 'BRL', NULL),
	('CLPK', 'Peso (Chile)', 'Chile', NULL, 'Peso', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('COPK', 'Peso', 'Colombia', NULL, 'Peso', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('CYP', 'Pound (Cyprus)', 'Cyprus', NULL, 'Cents', NULL, 100, NULL, NULL, '2007', 'EUR', NULL),
	('DEM', 'Deutsche Mark', 'Germany <= 2000', NULL, NULL, NULL, NULL, NULL, NULL, '2000', 'EUR', NULL),
	('ECSK', 'Sucre', 'Ecuador', NULL, 'Sucre', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('ESPK', 'Peseta', 'Spain <= 2000', NULL, 'Peseta', NULL, 1000, NULL, NULL, '2000', 'EUR', NULL),
	('FIM', 'Markkaa', 'Finland <= 2000', NULL, NULL, NULL, NULL, NULL, NULL, '2000', 'EUR', NULL),
	('FRF', 'Franc (France)', 'France <= 2000', NULL, NULL, NULL, NULL, NULL, NULL, '2000', 'EUR', NULL),
	('GRDK', 'Drachma', 'Greece <= 2000', NULL, 'Drachma', NULL, 1000, NULL, NULL, '2000', 'EUR', NULL),
	('HUFK', 'Forint (Hungary)', 'Hungary', NULL, 'Forint', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('IDRK', 'Rupiah (Indonesia)', 'Indonesia', NULL, 'Rupiah', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('IEP', 'Pound', 'Ireland <= 2000', NULL, NULL, NULL, NULL, NULL, NULL, '2000', 'EUR', NULL),
	('ITLK', 'Lire', 'Italy <= 2000', NULL, 'Lire', NULL, 1000, NULL, NULL, '2000', 'EUR', NULL),
	('JPYK', 'Yen (Japan)', 'Japan', NULL, 'Yen', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('KRWK', 'Won (South Korea)', 'South Korea', NULL, 'Won', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('KZTK', 'Tenge (Kazakhstan)', 'Kazakhstan', NULL, 'Tenge', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('NLG', 'Guilder', 'Netherlands <= 2000', NULL, 'cents', NULL, 100, NULL, NULL, '2000', 'EUR', NULL),
	('PTEK', 'Escudo', 'Portugal <= 2000', NULL, 'Escudo', NULL, 1000, NULL, NULL, '2000', 'EUR', NULL),
	('ROLK', 'Lei', 'Romania', NULL, 'Lei', NULL, 1000, NULL, NULL, NULL, NULL, NULL),
	('RUR', 'Rubles', 'Russia', NULL, 'kopecks', NULL, 100, NULL, NULL, '1998', 'RUB', NULL),
	('SKK', 'Koruny', 'Slovak Republic', NULL, 'halierov', NULL, 100, NULL, NULL, NULL, NULL, NULL),
	('TRLK', 'Lira (Turkey)', 'Turkey <= 1996', NULL, 'Lira', NULL, 1000, NULL, NULL, '1998', 'TRLM', NULL),
	('TRLM', 'Lira', 'Turkey', NULL, 'new kurus', NULL, 100, NULL, NULL, '2005', 'TRY', NULL),
	('VEB', 'Bolivar <= 1996', 'Venezuela <= 1996', NULL, 'centimos', NULL, 100, NULL, NULL, '1998', 'VEBK', NULL),
	('VEBK', 'Bolivar', 'Venezuela', NULL, 'Bolivar', NULL, 1000, NULL, NULL, '2007', 'VEF', NULL),
	('XOFK', 'Franc', 'Ivory Coast', NULL, 'Franc', NULL, 1000, NULL, NULL, NULL, NULL, NULL),

	('UNS', 'Unspecified', 'Unspecified', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	('N/A', 'Not Applicapable', 'Not Applicapable', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)

	)[t]([CU_Tag], [CU_Name], [CU_Detail], [CU_Glyph],
	[CU_SubUnit], [CU_SubUnitGlyph],
	[CU_MultiplierSubUnit], [CU_MultiplierStored], [CU_MultiplierReported],
	[CU_LastUsed_Year], [CU_ReplacementCurrencyID], [CU_ISONumeric]);

INSERT INTO [dim].[CU_Currency_LookUp]([CU_Tag], [CU_Name], [CU_Detail], [CU_Glyph],
	[CU_SubUnit], [CU_SubUnitGlyph],
	[CU_MultiplierSubUnit], [CU_MultiplierStored], [CU_MultiplierReported],
	[CU_LastUsed_Year], [CU_ReplacementCurrencyID], [CU_ISONumeric]
	)
SELECT [t].[CU_Tag], [t].[CU_Name], [t].[CU_Detail], [t].[CU_Glyph],
	[t].[CU_SubUnit], [t].[CU_SubUnitGlyph],
	[t].[CU_MultiplierSubUnit], [t].[CU_MultiplierStored], [t].[CU_MultiplierReported],
	[t].[CU_LastUsed_Year], [t].[CU_ReplacementCurrencyID], [t].[CU_ISONumeric]
FROM
	@Currency							[t]
LEFT OUTER JOIN
	[dim].[CU_Currency_LookUp]			[x]
		ON	[x].[CU_Tag]	= [t].[CU_Tag]
WHERE	[x].[CU_CurrencyId]				IS NULL
	AND	[t].[CU_ReplacementCurrencyID]	IS NULL;

INSERT INTO [dim].[CU_Currency_LookUp]([CU_Tag], [CU_Name], [CU_Detail], [CU_Glyph],
	[CU_SubUnit], [CU_SubUnitGlyph],
	[CU_MultiplierSubUnit], [CU_MultiplierStored], [CU_MultiplierReported],
	[CU_LastUsed_Year], [CU_ReplacementCurrencyID], [CU_ISONumeric]
	)
SELECT [t].[CU_Tag], [t].[CU_Name], [t].[CU_Detail], [t].[CU_Glyph],
	[t].[CU_SubUnit], [t].[CU_SubUnitGlyph],
	[t].[CU_MultiplierSubUnit], [t].[CU_MultiplierStored], [t].[CU_MultiplierReported],
	[t].[CU_LastUsed_Year], [l].[CU_CurrencyId], [t].[CU_ISONumeric]
FROM
	@Currency							[t]
LEFT OUTER JOIN
	[dim].[CU_Currency_LookUp]			[x]
		ON	[x].[CU_Tag]	= [t].[CU_Tag]
INNER JOIN [dim].[CU_Currency_LookUp]	[l]
	ON	[l].[CU_Tag] = [t].[CU_ReplacementCurrencyID]
WHERE	[x].[CU_CurrencyId]				IS NULL
	AND	[t].[CU_ReplacementCurrencyID]	IS NOT NULL;