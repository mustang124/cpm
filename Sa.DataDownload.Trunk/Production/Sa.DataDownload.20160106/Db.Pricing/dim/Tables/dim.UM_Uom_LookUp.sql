﻿CREATE TABLE [dim].[UM_Uom_LookUp]
(
	[UM_UomId]				INT					NOT NULL	IDENTITY(1, 1),

	[UM_Tag]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL__UM_Uom_LookUp_UM_Tag]			CHECK([UM_Tag] <> ''),
															CONSTRAINT [UK__UM_Uom_LookUp_UM_Tag]			UNIQUE CLUSTERED([UM_Tag] ASC),

	[UM_Name]				NVARCHAR(24)		NOT	NULL	CONSTRAINT [CL__UM_Uom_LookUp_UM_Name]			CHECK([UM_Name] <> '')
															CONSTRAINT [UX__UM_Uom_LookUp_UM_Name]			UNIQUE([UM_Name] ASC),

	[UM_Detail]				NVARCHAR(24)		NOT	NULL	CONSTRAINT [CL__UM_Uom_LookUp_UM_Detail]		CHECK([UM_Detail] <> '')
															CONSTRAINT [UX__UM_Uom_LookUp_UM_Detail]		UNIQUE([UM_Detail] ASC),
	
	[UM_Glyph]				NVARCHAR(5)				NULL	CONSTRAINT [CL__UM_Uom_LookUp_UM_Glyph]			CHECK([UM_Glyph] <> ''),

	[UM_Factor]				FLOAT					NULL	CONSTRAINT [CR__UM_Uom_LookUp_UM_Factor]		CHECK([UM_Factor] > 0.0),

	[UM_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__UM_Uom_LookUp_UM_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__UM_Uom_LookUp_UM_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[UM_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__UM_Uom_LookUp_UM_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[UM_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__UM_Uom_LookUp_UM_RowGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__UM_Uom_LookUp_UM_RowGuid]		UNIQUE NONCLUSTERED([UM_RowGuid]),

	CONSTRAINT [PK__UM_Uom_LookUp]				PRIMARY KEY([UM_UomId] ASC)
);