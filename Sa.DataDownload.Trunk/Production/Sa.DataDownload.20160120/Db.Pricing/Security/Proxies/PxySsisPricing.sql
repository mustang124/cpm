﻿EXECUTE [msdb].[dbo].sp_update_proxy
	@proxy_name			= N'PxySsisPricing',
	@credential_name	= N'SsisPricing';
GO

BEGIN TRY

	EXECUTE [msdb].[dbo].sp_grant_proxy_to_subsystem
		@proxy_name		= N'PxySsisPricing',
		@subsystem_name = N'Dts';

END TRY
BEGIN CATCH
END CATCH;