﻿PRINT 'insert.Platts.reference...';

:setvar pathPlattsReference "C:\FtpPricing\Seed\PlattsFtp\Reference\"

IF OBJECT_ID('tempdb..#PlattsSymbols')	IS NOT NULL DROP TABLE #PlattsSymbols;

CREATE TABLE #PlattsSymbols
(
	[PL_Category]				VARCHAR(3)			NOT	NULL,
	[PL_SymbolBateLegacy]		CHAR(13)			NOT	NULL,
	[PL_SymbolBate]				CHAR(8)				NOT	NULL,
	[PL_SymbolLegacy]			VARCHAR(10)				NULL,
	[PL_Frequency]				CHAR(2)				NOT	NULL,
	[PL_Currency]				CHAR(3)				NOT	NULL,
	[PL_Uom]					VARCHAR(3)			NOT	NULL,
	[PL_Earliest]				CHAR(7)				NOT	NULL,
	[PL_Latest]					CHAR(7)				NOT	NULL,
	[PL_Detail]					VARCHAR(192)		NOT	NULL,

	[PL_Symbol]					AS LEFT([PL_SymbolBate], 7)
								PERSISTED			NOT	NULL,
	[PL_Bate]					AS RIGHT([PL_SymbolBate], 1)
								PERSISTED			NOT	NULL,
	[PL_EarliestDate]			AS CONVERT(DATE, CASE WHEN RTRIM([PL_Earliest])	<> '' THEN RTRIM([PL_Earliest])	END, 12),
	[PL_LatestDate]				AS CONVERT(DATE, CASE WHEN RTRIM([PL_Latest])	<> '' THEN RTRIM([PL_Latest])	END, 12)
);

DECLARE @FilesReferencePlatts TABLE
(
	[Name]	VARCHAR(7) NOT NULL
);

INSERT INTO @FilesReferencePlatts
(
	[Name]
)
SELECT
	[t].[Name]
FROM (VALUES
	('aa.sym'),
	('ad.sym'),
	('adc.sym'),
	('ae.sym'),
	('af.sym'),
	('ag.sym'),
	('ah.sym'),
	('ahc.sym'),
	('ahd.sym'),
	('ahe.sym'),
	('ahf.sym'),
	('ahl.sym'),
	('ahm.sym'),
	('aho.sym'),
	('ai.sym'),
	('al.sym'),
	('alc.sym'),
	('ald.sym'),
	('ale.sym'),
	('alf.sym'),
	('all.sym'),
	('alm.sym'),
	('alo.sym'),
	('am.sym'),
	('amc.sym'),
	('ao.sym'),
	('ap.sym'),
	('as.sym'),
	('at.sym'),
	('au.sym'),
	('ay.sym'),
	('ba.sym'),
	('bf.sym'),
	('bl.sym'),
	('bx.sym'),
	('cc.sym'),
	('cd.sym'),
	('cf.sym'),
	('ch.sym'),
	('ci.sym'),
	('cj.sym'),
	('cl.sym'),
	('cm.sym'),
	('co.sym'),
	('cry.sym'),
	('cs.sym'),
	('cta.sym'),
	('cu.sym'),
	('cx.sym'),
	('da.sym'),
	('dc.sym'),
	('di.sym'),
	('dp.sym'),
	('dr.sym'),
	('du.sym'),
	('eb.sym'),
	('edc.sym'),
	('ee.sym'),
	('eg.sym'),
	('eh.sym'),
	('ej.sym'),
	('ek.sym'),
	('em.sym'),
	('emc.sym'),
	('es.sym'),
	('et.sym'),
	('eu.sym'),
	('fa.sym'),
	('fdc.sym'),
	('fg.sym'),
	('fk.sym'),
	('fmc.sym'),
	('fs.sym'),
	('fx.sym'),
	('ga.sym'),
	('gc.sym'),
	('gd.sym'),
	('gdc.sym'),
	('gf.sym'),
	('gm.sym'),
	('gmc.sym'),
	('gn.sym'),
	('grc.sym'),
	('grw.sym'),
	('gry.sym'),
	('gs.sym'),
	('hf.sym'),
	('ho.sym'),
	('hp.sym'),
	('hs.sym'),
	('hx.sym'),
	('ia.sym'),
	('ib.sym'),
	('ic.sym'),
	('id.sym'),
	('if.sym'),
	('ig.sym'),
	('ih.sym'),
	('ik.sym'),
	('il.sym'),
	('ilo.sym'),
	('im.sym'),
	('in.sym'),
	('ino.sym'),
	('io.sym'),
	('ip.sym'),
	('is.sym'),
	('iso.sym'),
	('ix.sym'),
	('jdc.sym'),
	('ji.sym'),
	('jmc.sym'),
	('lf.sym'),
	('lg.sym'),
	('li.sym'),
	('lm.sym'),
	('lt.sym'),
	('lu.sym'),
	('ma.sym'),
	('mc.sym'),
	('mdc.sym'),
	('mf.sym'),
	('mi.sym'),
	('ml.sym'),
	('mm.sym'),
	('mmc.sym'),
	('mpa.sym'),
	('mpr.sym'),
	('mpu.sym'),
	('mry.sym'),
	('mw.sym'),
	('mx.sym'),
	('na.sym'),
	('nb.sym'),
	('ne.sym'),
	('ni.sym'),
	('nj.sym'),
	('nk.sym'),
	('nm.sym'),
	('np.sym'),
	('nq.sym'),
	('nr.sym'),
	('nt.sym'),
	('nv.sym'),
	('nw.sym'),
	('nx.sym'),
	('ny.sym'),
	('odc.sym'),
	('omc.sym'),
	('os.sym'),
	('ox.sym'),
	('pa.sym'),
	('pc.sym'),
	('pdc.sym'),
	('pe.sym'),
	('pfa.sym'),
	('pfr.sym'),
	('pfu.sym'),
	('pg.sym'),
	('pl.sym'),
	('pm.sym'),
	('pmc.sym'),
	('pn.sym'),
	('pp.sym'),
	('pr.sym'),
	('ps.sym'),
	('pu.sym'),
	('px.sym'),
	('pz.sym'),
	('qf.sym'),
	('ra.sym'),
	('rc.sym'),
	('rdc.sym'),
	('re.sym'),
	('rf.sym'),
	('rhc.sym'),
	('rhd.sym'),
	('rhe.sym'),
	('rhf.sym'),
	('rhl.sym'),
	('rhm.sym'),
	('rho.sym'),
	('ri.sym'),
	('rm.sym'),
	('rmc.sym'),
	('rn.sym'),
	('rp.sym'),
	('rsc.sym'),
	('rsd.sym'),
	('rse.sym'),
	('rsf.sym'),
	('rsl.sym'),
	('rsm.sym'),
	('rso.sym'),
	('ru.sym'),
	('rw.sym'),
	('sa.sym'),
	('sb.sym'),
	('sdc.sym'),
	('sg.sym'),
	('si.sym'),
	('sk.sym'),
	('sm.sym'),
	('smc.sym'),
	('sp.sym'),
	('sq.sym'),
	('sr.sym'),
	('ss.sym'),
	('st.sym'),
	('sv.sym'),
	('sx.sym'),
	('sy.sym'),
	('ta.sym'),
	('tb.sym'),
	('tc.sym'),
	('td.sym'),
	('te.sym'),
	('tg.sym'),
	('th.sym'),
	('tj.sym'),
	('tk.sym'),
	('tq.sym'),
	('tw.sym'),
	('udc.sym'),
	('ug.sym'),
	('ulc.sym'),
	('uld.sym'),
	('ule.sym'),
	('ulf.sym'),
	('ull.sym'),
	('ulm.sym'),
	('ulo.sym'),
	('umc.sym'),
	('usc.sym'),
	('usd.sym'),
	('use.sym'),
	('usf.sym'),
	('usl.sym'),
	('usm.sym'),
	('uso.sym'),
	('uw.sym'),
	('uy.sym'),
	('uz.sym'),
	('wc.sym'),
	('wdc.sym'),
	('wmc.sym'),
	('wo.sym'),
	('xl.sym'),
	('yc.sym'),
	('yh.sym'),
	('yi.sym'),
	('yl.sym'),
	('yn.sym'),
	('yr.sym'),
	('ys.sym'),
	('yu.sym'),
	('za.sym')
	) [t]([Name]);

DECLARE @NameReferencePlatts	VARCHAR(128);
DECLARE @SqlReferencePlatts		VARCHAR(512);

DECLARE CurPlattsSymbols CURSOR FAST_FORWARD FOR
SELECT
	[f].[Name]
FROM
	@FilesReferencePlatts	[f]

OPEN CurPlattsSymbols;

FETCH NEXT FROM CurPlattsSymbols
INTO @NameReferencePlatts;

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @SqlReferencePlatts = 'BULK INSERT #PlattsSymbols FROM ''$(pathPlattsReference)sym\' + @NameReferencePlatts + ''' WITH(FORMATFILE = ''$(pathPlattsReference)insert.Platts.reference.sym.xml'', FIRSTROW=2);'

	EXECUTE(@SqlReferencePlatts);

	FETCH NEXT FROM CurPlattsSymbols
	INTO @NameReferencePlatts;

END;

CLOSE CurPlattsSymbols;
DEALLOCATE CurPlattsSymbols;

INSERT INTO [ref].[PL_PlattsItemSymbols_Posit]
(
	[PL_Category],
	[PL_Symbol],
	[PL_Bate],
	[PL_Frequency],
	[PL_Currency],
	[PL_Uom],
	[PL_Earliest],
	[PL_Latest],
	[PL_Detail]
)
SELECT DISTINCT
	[PL_Category]		= LTRIM(RTRIM(MAX([b].[PL_Category]) OVER(PARTITION BY [b].[PL_Symbol]))),
	[PL_Symbol]			= LTRIM(RTRIM([b].[PL_Symbol])),
	[PL_Bate]			= LTRIM(RTRIM([b].[PL_Bate])),
	[PL_Frequency]		= LTRIM(RTRIM([b].[PL_Frequency])),
	[PL_Currency]		= LTRIM(RTRIM([b].[PL_Currency])),
	[PL_Uom]			= LTRIM(RTRIM([b].[PL_Uom])),
	[PL_EarliestDate]	= CASE WHEN 
								MAX([b].[PL_EarliestDate]) OVER(PARTITION BY [b].[PL_Symbol]) <=  MAX([b].[PL_LatestDate]) OVER(PARTITION BY [b].[PL_Symbol])
							THEN
								MAX([b].[PL_EarliestDate]) OVER(PARTITION BY [b].[PL_Symbol])
							END,
	[PL_LatestDate]		= MAX([b].[PL_LatestDate]) OVER(PARTITION BY [b].[PL_Symbol]),
	[PL_Detail]			= LTRIM(RTRIM([b].[PL_Detail]))
FROM
	#PlattsSymbols								[b]
LEFT OUTER JOIN
	[ref].[PL_PlattsItemSymbols_Posit]			[p]
		ON	[p].[PL_Symbol]		= [b].[PL_Symbol]
		AND	[p].[PL_Bate]		= [b].[PL_Bate]
WHERE	[p].[PL_Symbol]			IS		NULL
	AND	[b].[PL_Category]		IS	NOT	NULL
	AND	[b].[PL_Symbol]			IS	NOT	NULL
	AND	[b].[PL_Bate]			IS	NOT	NULL
	AND	[b].[PL_Frequency]		IS	NOT	NULL
	AND	[b].[PL_Currency]		IS	NOT	NULL
	AND	[b].[PL_Uom]			IS	NOT	NULL
	AND	[b].[PL_Detail]			IS	NOT	NULL;

IF OBJECT_ID('tempdb..#PlattsSymbols')	IS NOT NULL DROP TABLE #PlattsSymbols;
