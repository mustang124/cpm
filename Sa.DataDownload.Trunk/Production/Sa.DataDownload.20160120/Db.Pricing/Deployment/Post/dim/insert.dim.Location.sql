﻿PRINT 'insert.dim.Location...';

DECLARE @Locations TABLE
(
	[LO_Tag]			VARCHAR(24)			NOT	NULL	CHECK([LO_Tag] <> ''),
														UNIQUE CLUSTERED([LO_Tag] ASC),

	[LO_Name]			NVARCHAR(48)		NOT	NULL	CHECK([LO_Name] <> '')
														UNIQUE([LO_Name] ASC),

	[LO_Detail]			NVARCHAR(96)		NOT	NULL	CHECK([LO_Detail] <> '')
														UNIQUE([LO_Detail] ASC),

	[LO_Parent]			VARCHAR(24)			NOT	NULL	CHECK([LO_Parent] <> '')
);

INSERT INTO @Locations([LO_Tag], [LO_Name], [LO_Detail], [LO_Parent])
SELECT [t].[LO_Tag], [t].[LO_Name], [t].[LO_Detail], [t].[LO_Parent]
FROM (VALUES
	('NA', 'North America', 'North America', 'NA'),
	('CAN', 'Canada', 'Canada', 'NA'),
	('USA', 'United States', 'United States of America', 'NA'),
	
	('USA-TX', 'Texas', 'Texas', 'USA'),
	('USA-TX-DAL', 'Dallas', 'Dallas', 'USA-TX'),
	('USA-TX-HOU', 'Houston', 'Houston', 'USA-TX'),

	('USA-CA', 'California', 'California', 'USA'),
	('USA-CA-LA', 'Los Angeles', 'Los Angeles', 'USA-CA'),
	('USA-CA-ES', 'El Segundo', 'El Segundo', 'USA-CA')
	)[t]([LO_Tag], [LO_Name], [LO_Detail], [LO_Parent]);

INSERT INTO [dim].[LO_Location_LookUp]
(
	[LO_Tag],
	[LO_Name],
	[LO_Detail]
)
SELECT
	[t].[LO_Tag],
	[t].[LO_Name],
	[t].[LO_Detail]
FROM
	@Locations					[t]
LEFT OUTER JOIN
	[dim].[LO_Location_LookUp]	[x]
		ON	[x].[LO_Tag]	= [t].[LO_Tag]
WHERE
	[x].[LO_LocationId]		IS NULL;

INSERT INTO [dim].[LO_Location_Parent]
(
	[LO_MethodologyId],
	[LO_LocationId],
	[LO_ParentId],
	[LO_Operator],
	[LO_SortKey],
	[LO_Hierarchy]
)
SELECT
		[LO_MethodologyId]	= 0,
	[i].[LO_LocationId],
	[p].[LO_LocationId],
		[LO_Operator]		= '+',
		[LO_SortKey]		= 0,
		[LO_Hierarchy]		= '/'
FROM
	@Locations							[t]
INNER JOIN
	[dim].[LO_Location_LookUp]			[i]
		ON	[i].[LO_Tag]	= [t].[LO_Tag]
INNER JOIN
	[dim].[LO_Location_LookUp]			[p]
		ON	[p].[LO_Tag]	= [t].[LO_Parent]
LEFT OUTER JOIN
	[dim].[LO_Location_Parent]			[x]
		ON	[x].[LO_LocationId]	= [i].[LO_LocationId]
		AND	[x].[LO_ParentId]	= [p].[LO_LocationId]
WHERE
	[x].[LO_LocationId]		IS NULL;

EXECUTE [dim].[Update_Parent] 'dim', 'LO_Location_Parent', 'LO', 'LocationId', DEFAULT, DEFAULT, DEFAULT, DEFAULT;
EXECUTE [dim].[Merge_Bridge] 'dim', 'LO_Location_Parent', 'dim', 'LO_Location_Bridge', 'LO', 'LocationId', DEFAULT, DEFAULT, DEFAULT, DEFAULT;

--SELECT
--	REPLICATE(' ', POWER([p].[LO_Hierarchy].GetLevel(), 2)) + [l].[LO_Tag],
--	[l].[LO_Tag],
--	[l].[LO_Name],
--	[l].[LO_Detail],
--	[p].[LO_Hierarchy].GetLevel(),
--	[p].[LO_Hierarchy].ToString(),
--	[p].[LO_Operator]
--FROM [dim].[LO_Location_Parent]			[p]
--INNER JOIN [dim].[LO_Location_LookUp]	[l]
--	ON	[l].[LO_LocationId] = [p].[LO_LocationId]
--ORDER BY [p].[LO_Hierarchy];
