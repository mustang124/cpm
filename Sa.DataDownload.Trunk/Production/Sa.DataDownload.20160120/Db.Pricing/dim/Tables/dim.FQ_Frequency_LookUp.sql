﻿CREATE TABLE [dim].[FQ_Frequency_LookUp]
(
	[FQ_FrequencyId]		INT					NOT NULL	IDENTITY(1, 1),

	[FQ_Tag]				CHAR(2)				NOT	NULL	CONSTRAINT [CL__FQ_Frequency_LookUp_FQ_Tag]				CHECK([FQ_Tag] <> ''),
															CONSTRAINT [UK__FQ_Frequency_LookUp_FQ_Tag]				UNIQUE CLUSTERED([FQ_Tag] ASC),

	[FQ_Name]				NVARCHAR(14)		NOT	NULL	CONSTRAINT [CL__FQ_Frequency_LookUp_FQ_Name]			CHECK([FQ_Name] <> '')
															CONSTRAINT [UX__FQ_Frequency_LookUp_FQ_Name]			UNIQUE([FQ_Name] ASC),

	[FQ_Detail]				NVARCHAR(30)		NOT	NULL	CONSTRAINT [CL__FQ_Frequency_LookUp_FQ_Detail]			CHECK([FQ_Detail] <> '')
															CONSTRAINT [UX__FQ_Frequency_LookUp_FQ_Detail]			UNIQUE([FQ_Detail] ASC),

	[FQ_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__FQ_Frequency_LookUp_FQ_PositedBy]		DEFAULT([posit].[Get_PositorId]())
															CONSTRAINT [FK__FQ_Frequency_LookUp_FQ_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[FQ_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__FQ_Frequency_LookUp_FQ_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[FQ_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__FQ_Frequency_LookUp_FQ_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX__FQ_Frequency_LookUp_FQ_RowGuid]			UNIQUE NONCLUSTERED([FQ_RowGuid]),

	CONSTRAINT [PK__FQ_Frequency_LookUp]		PRIMARY KEY([FQ_FrequencyId] ASC)
);