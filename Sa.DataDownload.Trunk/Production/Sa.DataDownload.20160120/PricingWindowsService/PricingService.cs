﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Pricing;
using System.Reflection;
using Solomon.Common.Utility;
using Solomon.Common.Email;
using PricingFtp.Classes;
using Solomon.Common.FileOps;

namespace PricingWindowsService
{
    public partial class PricingService : ServiceBase
    {
        public PricingService()
        {
            InitializeComponent();
        }
        static string ftpSite = string.Empty;
        static string ftpSitePort = string.Empty;
        static string ftpUser = string.Empty;
        static string ftpPassword = string.Empty;
        static string ftpDirectory = string.Empty;
        static string ftpHostKey = string.Empty;

        static string rootDrive = string.Empty;
        static string localPricingDir = string.Empty;
        static string processDirectory = string.Empty;
        static string processUploadDir = string.Empty;
        static string processDownloadDir = string.Empty;
        static string processArchiveDir = string.Empty;

        static bool logging = false;
        static string error = string.Empty;

        public PricingFtpManager()
        {
            InitializeComponent();
            RunProcess();
        }

        private void RunProcess()
        {
            Pricing.Manager pricingMgr = new Pricing.Manager();
            Pricing.Ftp ftp = new Ftp();
            MethodBase mb = MethodBase.GetCurrentMethod();
            List<string> directoryList = new List<string>();

            string error = string.Empty;

            try
            {

                //Platts processing
                if (ConfigurationManager.AppSettings["ProcessPlatts"] == null)
                {
                    System.Exception programError = new Exception("Configuration not found - ProcessPlatts. Cannot continue.");
                    LogError("PricingWinService", "Program", mb.Name, ref programError);
                    return;
                }

                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessPlatts"]) == true)
                {

                    bool configsSetPlatts = PopulateProcessVariablesFromConfig("Platts");

                    if (configsSetPlatts == false)
                    {
                        System.Exception programError = new Exception("PopulateProcessVariables(Platts) FAILED. Cannot continue.");
                        LogError("PricingWinService", "Program", mb.Name, ref programError);
                        return;
                    }
                    else
                    {
                        string plattsWorkingUploadDir = rootDrive + localPricingDir + processDirectory + processUploadDir;
                        string latestProcessPlattsDirectory = pricingMgr.GetMostRecentDirectory(rootDrive + localPricingDir + processDirectory + processArchiveDir);
                        bool success = ProcessPlattsUsingWinScp(latestProcessPlattsDirectory);

                        if (!success)
                        {
                            System.Exception programError = new Exception("ERROR - ProcessPlattsUsingWinScp. Cannot continue. EXCEPTION: " + error);
                            LogError("PricingWinService", "Program", mb.Name, ref programError);
                            return;
                        }
                    }

                }

                //Argus processing
                if (ConfigurationManager.AppSettings["ProcessArgus"] == null)
                {
                    System.Exception programError = new Exception("Configuration not found - ProcessArgus. Cannot continue.");
                    LogError("PricingWinService", "Program", mb.Name, ref programError);
                    return;
                }


                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessArgus"]) == true)
                {
                    bool configsSetArgus = PopulateProcessVariablesFromConfig("Argus");

                    if (configsSetArgus == false)
                    {
                        System.Exception programError = new Exception("PopulateProcessVariables(Argus) FAILED. Cannot continue.");
                        LogError("PricingWinService", "Program", mb.Name, ref programError);
                        return;
                    }
                    else
                    {
                        string argusWorkingUploadDir = rootDrive + localPricingDir + processDirectory + processUploadDir;
                        string latestProcessArgusDirectory = pricingMgr.GetMostRecentDirectory(rootDrive + localPricingDir + processDirectory + processArchiveDir);
                        bool success = ProcessArgusUsingWinScp(ref error, latestProcessArgusDirectory);

                        if (!success)
                        {
                            System.Exception programError = new Exception("ERROR - ProcessArgusUsingWinScp. Cannot continue. EXCEPTION: " + error);
                            LogError("PricingWinService", "Program", mb.Name, ref programError);
                            return;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                System.Exception programError = new Exception("ERROR - RunProcess. Cannot continue. EXCEPTION: " + ex.Message);
                LogError("PricingWinService", "Program", mb.Name, ref programError);
                return;
            }

        }

        private bool ProcessPlatts(string lastDirProcessed)
        {
            bool success = false;
            Pricing.Manager pricingMgr = new Pricing.Manager();
            Pricing.Ftp ftp = new Ftp();
            MethodBase mb = MethodBase.GetCurrentMethod();
            List<string> directoryList = new List<string>();
            List<string> directoryListDetail = new List<string>();
            List<string> fileListInDir = new List<string>();

            string response = string.Empty;

            try
            {

                bool isValidPlattsFtpConnection = ftp.IsValidFtpConnection(ref response, ftpSite, ftpUser, ftpPassword);

                if (isValidPlattsFtpConnection)
                {

                    //Get ftp directories and build string list of directory names. Log the step.
                    LogIt("PricingWinService", "Program", mb.Name, "Started, GetDirectories from Platts.");

                    directoryList = pricingMgr.GetDirectoriesFromFtpSite(ref error, ftpSite, ftpUser, ftpPassword, ftpDirectory);

                    if (directoryList == null && error != string.Empty)
                    {
                        System.Exception programError = new Exception("ERROR: " + error);
                        LogError("PricingWinService", "Program", mb.Name, ref programError);
                        return success;
                    }
                    else
                    {
                        LogIt("PricingWinService", "Program", mb.Name, "Completed, GetDirectories from Platts. Directory count is : " + Convert.ToString(directoryList.Count));
                        success = true;
                    }

                    if (directoryList.Count > 0)
                    {

                        Ftp ftpDownload = new Ftp(ftpSite, ftpUser, ftpPassword);
                        //Get directories from ftp site

                        foreach (var dir in directoryList)
                        {
                            //Create the directory locally
                            string processUpldDir = rootDrive + localPricingDir + processDirectory + processUploadDir + dir.ToString();

                            if (Convert.ToInt32(dir.ToString()) > Convert.ToInt32(lastDirProcessed))
                            {
                                bool createDirSuccess = pricingMgr.CreateDirectoryFromFtpSite(ref error, dir.ToString(), processUpldDir);

                                if (error != string.Empty && createDirSuccess == false)
                                {
                                    System.Exception programError = new Exception("ERROR: pricingMgr.CreateDirectoryFromFtpSite. MESSAGE: " + error + ". Cannot continue.");
                                    LogError("PricingWinService", "Program", mb.Name, ref programError);
                                    break;
                                }


                                //Get files from ftp and save them to the current directory
                                fileListInDir = new List<string>();
                                fileListInDir = pricingMgr.GetFilesFromFtpSiteDirectory(ref error, dir.ToString(), ftpSite, ftpUser, ftpPassword, "Platts");

                                if (fileListInDir.Count > 0)
                                {
                                    foreach (var file in fileListInDir)
                                    {
                                        pricingMgr.DownloadAndSaveFileFromFtpSite(ref error, ref ftpDownload, dir.ToString(), processUploadDir, file.ToString());

                                        if (error != string.Empty)
                                        {
                                            System.Exception programError = new Exception("ERROR: pricingMgr.DownloadAndSaveFileFromFtpSite. MESSAGE: " + error + ". Cannot continue.");
                                            LogError("PricingWinService", "Program", mb.Name, ref programError);
                                        }
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        LogIt("PricingWinService", "Program", mb.Name, "Completed, GetDirectories from Platts. WARNING: Directory count is ZERO. May want to investigate.");
                    }

                    success = true;
                }
                else
                {
                    System.Exception programError = new Exception("Ftp connection for Platts FAILED. Cannot continue.");
                    LogError("PricingWinService", "Program", mb.Name, ref programError);
                }

                return success;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private bool ProcessPlattsUsingWinScp(string lastDirProcessed)
        {
            bool success = false;
            MethodBase mb = MethodBase.GetCurrentMethod();
            List<string> fileListInDir = new List<string>();
            List<string> currDirectoryList = new List<string>();

            string response = string.Empty;

            try
            {
                Pricing.Manager pricingMgr = new Pricing.Manager();

                string upldDir = rootDrive + localPricingDir + processDirectory + processUploadDir;

                SessionOptions sessionOptionsForDir = SetFtpSessionOptions("Platts");
                Session sessionDir = new Session();

                //Create processing directory list for this run. We are doing it this way because there is not a way filter 
                //using the WinSCP get on the root dir. We would always get ALL the directories everytime. We 
                //don't want that
                currDirectoryList = GetProcessDirectoryListAndCreateDirectory(lastDirProcessed, upldDir);

                if (currDirectoryList.Count > 0)
                {
                    //Connect
                    sessionDir.Open(sessionOptionsForDir);

                    foreach (var dir in currDirectoryList)
                    {
                        WinScpDownloadFiles(sessionDir, ref error, "Platts", dir.ToString(), upldDir + dir.ToString());
                    }
                }
                else
                {
                    LogIt("PricingWinService", "Program", mb.Name, "Completed, GetDirectories from Platts. WARNING: Directory count is ZERO. May want to investigate.");
                }


                success = true;

                return success;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                throw ex;
            }

        }



        private List<string> GetProcessDirectoryListAndCreateDirectory(string latestProcessedDirectory, string path)
        {
            List<string> pdList = new List<string>();

            DateTime lastProcessedDate = new DateTime();

            DateTime currrentDate = DateTime.Now;
            string currDateString = String.Format("{0:MM/dd/yyyy}", currrentDate);
            FileHandler fh = new FileHandler();

            string currProcMonth = currDateString.Substring(0, 2);
            string currProcDay = currDateString.Substring(3, 2);
            string currProcYear = currDateString.Substring(6, 4);


            int newDirectoryCreate = Convert.ToInt32(latestProcessedDirectory) + 1;
            int currLastDirectoryCreate = Convert.ToInt32(currProcYear + currProcMonth + currProcDay);


            for (int i = newDirectoryCreate; i < currLastDirectoryCreate; i++)
            {
                int tempDirName = i + 1;
                if (IsValidDateToCreateDirectory(Convert.ToString(tempDirName)))
                {
                    //Delete the file if it already exist. This should not happen, but just in case...
                    if (fh.DirectoryExists(path + Convert.ToString(tempDirName))) { fh.DeleteFile(Convert.ToString(tempDirName)); }
                    fh.CreateDirectory(path + Convert.ToString(tempDirName));
                    pdList.Add(Convert.ToString(tempDirName));
                }
            }


            return pdList;
        }

        private bool IsValidDateToCreateDirectory(string directoryDateValue)
        {
            bool isValid = false;
            Pricing.Manager pricingManager = new Pricing.Manager();

            string currProcMonth = directoryDateValue.Substring(4, 2);
            string currProcDay = directoryDateValue.Substring(6, 2);
            string currProcYear = directoryDateValue.Substring(0, 4);

            string tempDate = currProcMonth + "/" + currProcDay + "/" + currProcYear;

            if (pricingManager.IsDate(tempDate))
            {
                DateTime tempDateTime = new DateTime();
                tempDateTime = Convert.ToDateTime(tempDate);

                //Only want to create weekday directories
                if (pricingManager.IsWeekday(tempDateTime))
                {
                    isValid = true;
                }
            }

            return isValid;

        }

        private SessionOptions SetFtpSessionOptions(string process)
        {
            SessionOptions sessionOptions = new SessionOptions();

            if (process == "Argus")
            {
                sessionOptions.PortNumber = Convert.ToInt32(ftpSitePort);
                sessionOptions.Protocol = Protocol.Ftp;
                sessionOptions.FtpSecure = FtpSecure.Implicit;
                sessionOptions.FtpMode = FtpMode.Passive;
                sessionOptions.TlsHostCertificateFingerprint = ftpHostKey;
                sessionOptions.TimeoutInMilliseconds = 25000;

            }

            if (process == "Platts")
            {
                sessionOptions.Protocol = Protocol.Sftp;
                sessionOptions.SshHostKeyFingerprint = ftpHostKey;
            }
            sessionOptions.HostName = ftpSite;
            sessionOptions.UserName = ftpUser;
            sessionOptions.Password = ftpPassword;


            return sessionOptions;
        }

        //private bool ProcessArgus(string lastDirProcessed)
        //{
        //    bool success = false;
        //    Pricing.Manager pricingMgr = new Pricing.Manager();
        //    Pricing.Ftp ftp = new Ftp();
        //    MethodBase mb = MethodBase.GetCurrentMethod();
        //    List<string> fileList = new List<string>();
        //    List<string> fileListDetail = new List<string>();
        //    List<string> fileListInDir = new List<string>();

        //    string response = string.Empty;

        //    try
        //    {

        //        bool isValidArgusFtpConnection = ftp.IsValidFtpConnection(ref response, ftpSite, ftpUser, ftpPassword);

        //        if (isValidArgusFtpConnection)
        //        {

        //            //Get ftp files and build string list of file names. Log the step.
        //            LogIt("PricingWinService", "Program", mb.Name, "Started, GetFilesFromFtpSiteDirectory from Argus.");

        //            fileList = pricingMgr.GetFilesFromFtpSiteDirectory(ref error, ftpDirectory, ftpSite, ftpUser, ftpPassword, "Argus");

        //            if (fileList == null && error != string.Empty)
        //            {
        //                System.Exception programError = new Exception("ERROR: " + error);
        //                LogError("PricingWinService", "Program", mb.Name, ref programError);
        //                return success;
        //            }
        //            else
        //            {
        //                LogIt("PricingWinService", "Program", mb.Name, "Completed, GetFilesFromFtpSiteDirectory from Argus. File count is : " + Convert.ToString(fileList.Count));
        //                success = true;
        //            }

        //            if (fileList.Count > 0)
        //            {
        //                Argus argus = new Argus();
        //                Ftp ftpDownload = new Ftp(ftpSite, ftpUser, ftpPassword);

        //                //Get files from ftp site
        //                foreach (var file in fileList)
        //                {
        //                    //Create the directory locally if 
        //                    bool validFileType = argus.IsValidFileType(file);

        //                    if (validFileType)
        //                    {

        //                        bool isValidArgusFile = argus.IsValidFileToProcess(file, 8, 0, 8);

        //                        if (isValidArgusFile)
        //                        {
        //                            string newDirectoryName = file.Substring(0, 8);

        //                            if (Convert.ToInt32(newDirectoryName) > Convert.ToInt32(lastDirProcessed))
        //                            {

        //                                //Create directory based on first 8 character of the file name. This is a valid date format used in processing
        //                                string processUploadDir = rootDrive + localPricingDir + processDirectory + processUploadDir + newDirectoryName;
        //                                bool createDirSuccess = pricingMgr.CreateDirectoryFromFtpSite(ref error, newDirectoryName, processUploadDir);

        //                                if (error != string.Empty && createDirSuccess == false)
        //                                {
        //                                    System.Exception programError = new Exception("ERROR: pricingMgr.CreateDirectoryFromFtpSite. MESSAGE: " + error + ". Cannot continue.");
        //                                    LogError("PricingWinService", "Program", mb.Name, ref programError);
        //                                    break;
        //                                }

        //                                //Now save the file to the directory we just created. Argus does a weekly file which is created on friday of every week                                    
        //                                pricingMgr.DownloadAndSaveFileFromFtpSite(ref error, ref ftpDownload, ftpDirectory, processUploadDir, file.ToString());

        //                                if (error != string.Empty)
        //                                {
        //                                    System.Exception programError = new Exception("ERROR: pricingMgr.DownloadAndSaveFileFromFtpSite. MESSAGE: " + error + ". Cannot continue.");
        //                                    LogError("PricingWinService", "Program", mb.Name, ref programError);
        //                                }

        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                LogIt("PricingWinService", "Program", mb.Name, "Completed, GetDirectories from Argus. WARNING: Directory count is ZERO. May want to investigate.");
        //            }

        //            success = true;
        //        }
        //        else
        //        {
        //            System.Exception programError = new Exception("Ftp connection for Argus FAILED. Cannot continue.");
        //            LogError("PricingWinService", "Program", mb.Name, ref programError);
        //        }

        //        return success;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.Write(ex.Message);
        //        throw ex;
        //    }

        //}

        private bool ProcessArgusUsingWinScp(ref string error, string lastDirProcessed)
        {
            bool success = false;
            MethodBase mb = MethodBase.GetCurrentMethod();
            List<string> directoryList = new List<string>();
            List<string> directoryListDetail = new List<string>();
            List<string> fileListInDir = new List<string>();

            string response = string.Empty;

            try
            {

                //Get ftp directories and build string list of directory names. Log the step.
                LogIt("PricingWinService", "Program", mb.Name, "Started, Get Files from Argus.");

                Pricing.Manager pricingMgr = new Pricing.Manager();

                string upldDir = rootDrive + localPricingDir + processDirectory + processUploadDir;

                SessionOptions sessionOptionsForDir = SetFtpSessionOptions("Argus");
                Session sessionDir = new Session();

                //Connect to Ftp Server
                sessionDir.Open(sessionOptionsForDir);

                //Get files from ftp and save them in the DAGM directory
                WinScpDownloadFiles(sessionDir, ref error, "Platts", ftpDirectory, upldDir);

                //If there are any files, then we are going to create directories in the, Argus/Upload directory based on file name
                fileListInDir = new List<string>();

                fileListInDir = pricingMgr.GetFilesFromDirectory(ref error, upldDir + ftpDirectory, "csv");

                if (error != string.Empty)
                {
                    System.Exception programError = new Exception("ERROR: pricingMgr.GetFilesFromDirectory FAILED. MESSAGE: " + error + ". Cannot continue.");
                    LogError("PricingWinService", "Program", mb.Name, ref programError);
                    return success;
                }

                if (fileListInDir.Count > 0)
                {
                    Argus argus = new Argus();
                    
                    foreach (var file in fileListInDir)
                    {
                        
                        //Check if valid file type
                        bool validFileType = argus.IsValidFileType(pricingMgr.GetFileNameFromDirectoryString(file));

                        if (validFileType)
                        {

                            bool isValidArgusFile = argus.IsValidFileToProcess(pricingMgr.GetFileNameFromDirectoryString(file), 8, 0, 8);

                            if (isValidArgusFile)
                            {
                                string fileName = pricingMgr.GetFileNameFromDirectoryString(Convert.ToString(file));
                                
                                //Derived from part of the file name
                                string newDirectoryName = fileName.Substring(0, 8);

                                if (Convert.ToInt32(newDirectoryName) > Convert.ToInt32(lastDirProcessed))
                                {

                                    //Create directory based on first 8 characters of the file name. This is a valid date format for this process
                                    string procUpldDirectory = rootDrive + localPricingDir + processDirectory + processUploadDir;
                                    bool createDirSuccess = pricingMgr.CreateDirectoryFromFileName(ref error, newDirectoryName, procUpldDirectory);

                                    if (error != string.Empty && createDirSuccess == false)
                                    {
                                        System.Exception programError = new Exception("ERROR: pricingMgr.CreateDirectoryFromFileName. MESSAGE: " + error + ". Cannot continue.");
                                        LogError("PricingWinService", "Program", mb.Name, ref programError);
                                        break;
                                    }
                                    
                                    //Now copy the file to the directory we just created. Argus does a weekly file which is created on friday of every week  
                                    bool copyFileSuccess = false;

                                    if (fileName.Length > 0)
                                    {
                                        copyFileSuccess = pricingMgr.CopyFile(ref error, procUpldDirectory + ftpDirectory, procUpldDirectory + newDirectoryName, fileName);
                                    }

                                    if (!copyFileSuccess)
                                    {
                                        System.Exception programError = new Exception("ERROR: pricingMgr.CopyFile. MESSAGE: " + error + ". Cannot continue.");
                                        LogError("PricingWinService", "Program", mb.Name, ref programError);
                                        break;
                                    }

                                }
                            }
                        }
                    }

                    //Clean up. Delete temp files in DAGM local directory
                    FileHandler fileHandler = new FileHandler();
                    string delAndAddDir = rootDrive + localPricingDir + processDirectory + processUploadDir + ftpDirectory;
                    if (fileHandler.DirectoryExists(delAndAddDir))
                    {
                        fileHandler.DeleteDirectory(delAndAddDir);                    
                        fileHandler.CreateDirectory(delAndAddDir);
                    }
                    
                }
                else
                {
                    LogIt("PricingWinService", "Program", mb.Name, "Completed, ProcessArgusUsingWinScp. WARNING: File count is ZERO. May want to investigate.");
                }

               
                success = true;


                return success;
            }
            catch (Exception ex)
            {
                LogError("PricingWinService", "Program", mb.Name, ref ex);
                return false;
            }

        }


        private static bool PopulateProcessVariablesFromConfig(string runType)
        {

            bool retVal = false;

            //Set logging
            if (ConfigurationManager.AppSettings["Logging"] == null) return false;
            logging = Convert.ToBoolean(ConfigurationManager.AppSettings["Logging"]);

            if (ConfigurationManager.AppSettings["RootDriveLetter"] == null) return false;
            if (ConfigurationManager.AppSettings["LocalPricingDir"] == null) return false;
            if (ConfigurationManager.AppSettings["ArchiveDir"] == null) return false;
            if (ConfigurationManager.AppSettings["DownloadDir"] == null) return false;
            if (ConfigurationManager.AppSettings["UploadDir"] == null) return false;


            rootDrive = Convert.ToString(ConfigurationManager.AppSettings["RootDriveLetter"]);
            localPricingDir = Convert.ToString(ConfigurationManager.AppSettings["LocalPricingDir"]);
            processArchiveDir = Convert.ToString(ConfigurationManager.AppSettings["ArchiveDir"]);
            processUploadDir = Convert.ToString(ConfigurationManager.AppSettings["UploadDir"]);
            processDownloadDir = Convert.ToString(ConfigurationManager.AppSettings["DownloadDir"]);


            if (runType == "Argus")
            {

                //FTP Argus
                if (ConfigurationManager.AppSettings["FtpSiteArgus"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusSitePortNumber"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusUser"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusPwd"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusDir"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpArgusHostKey"] == null) return false;

                if (ConfigurationManager.AppSettings["ArgusDir"] == null) return false;

                ftpSite = Convert.ToString(ConfigurationManager.AppSettings["FtpSiteArgus"]);
                ftpSitePort = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusSitePortNumber"]);
                ftpUser = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusUser"]);
                ftpPassword = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusPwd"]);
                ftpDirectory = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusDir"]);
                ftpHostKey = Convert.ToString(ConfigurationManager.AppSettings["FtpArgusHostKey"]);

                processDirectory = Convert.ToString(ConfigurationManager.AppSettings["ArgusDir"]);

                retVal = true;

            }


            if (runType == "Platts")
            {
                //FTP Platts
                if (ConfigurationManager.AppSettings["FtpSitePlatts"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsUser"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsPwd"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsDir"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsDir"] == null) return false;
                if (ConfigurationManager.AppSettings["FtpPlattsHostKey"] == null) return false;

                ftpSite = Convert.ToString(ConfigurationManager.AppSettings["FtpSitePlatts"]);
                ftpUser = Convert.ToString(ConfigurationManager.AppSettings["FtpPlattsUser"]);
                ftpPassword = Convert.ToString(ConfigurationManager.AppSettings["FtpPlattsPwd"]);
                ftpDirectory = Convert.ToString(ConfigurationManager.AppSettings["FtpPlattsDir"]);
                ftpHostKey = Convert.ToString(ConfigurationManager.AppSettings["FtpPlattsHostKey"]);

                processDirectory = Convert.ToString(ConfigurationManager.AppSettings["PlattsDir"]);

                retVal = true;
            }

            return retVal;
        }


        private static void SendEmail(bool isError, string content)
        {
            MethodBase mb = MethodBase.GetCurrentMethod();
            string appSetting = string.Empty;

            try
            {

                if (IsValidEmailCredentials(ref appSetting))
                {
                    Solomon.Common.Email.Manager emailMgr = new Solomon.Common.Email.Manager();


                    if (isError)
                    {
                        emailMgr.SendEmail(Convert.ToString(ConfigurationManager.AppSettings["EmailFrom"]),
                                                                    Convert.ToString(ConfigurationManager.AppSettings["EmailTo"]),
                                                                        Convert.ToString(ConfigurationManager.AppSettings["EmailAppErrorSubject"]),
                                                                            content, Convert.ToString(ConfigurationManager.AppSettings["SmtpServer"]));

                    }
                    else
                    {
                        emailMgr.SendEmail(Convert.ToString(ConfigurationManager.AppSettings["EmailFrom"]),
                                                                   Convert.ToString(ConfigurationManager.AppSettings["EmailTo"]),
                                                                       Convert.ToString(ConfigurationManager.AppSettings["EmailAppSubject"]),
                                                                           content, Convert.ToString(ConfigurationManager.AppSettings["SmtpServer"]));
                    }
                }
                else
                {
                    System.Exception programError = new Exception(appSetting + " string is null in configuration. Email will not be sent.");
                    LogError("PricingWinService", "Program", mb.Name, ref programError);
                    return;
                }

            }
            catch (Exception ex)
            {
                LogError("PricingWinService", "Program", mb.Name, ref ex);
            }
        }

        private static void LogIt(string thisAssembly, string thisClass, string thisMethod, string logText)
        {
            if (logging == true)
            {
                Log.Instance().Info("LOG: ", "Assembly: " + thisAssembly + " |Class: " + thisClass + " |Method: " + thisMethod + " |Message: " + logText);
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"])) { SendEmail(false, logText); }
            }

        }

        private static void LogError(string thisAssembly, string thisClass, string thisMethod, ref System.Exception ex)
        {
            if (logging == true)
            {
                Log.Instance().Error("ERROR: Assembly: " + thisAssembly + " |Class: " + thisClass + " |Method: " + thisMethod, ex);
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmailWithError"])) { SendEmail(true, ex.Message + Environment.NewLine + ex.StackTrace); }
            }

        }

        private static bool IsValidEmailCredentials(ref string appSetting)
        {

            if (ConfigurationManager.AppSettings["SendEmail"] == null) { appSetting = "SendEmail"; return false; }
            if (ConfigurationManager.AppSettings["SendEmailWithError"] == null) { appSetting = "SendEmailWithError"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServer"] == null) { appSetting = "SmtpServer"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServerPort"] == null) { appSetting = "SmtpServerPort"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServerUser"] == null) { appSetting = "SmtpServerUser"; return false; }
            if (ConfigurationManager.AppSettings["SmtpServerUserPassword"] == null) { appSetting = "SmtpServerUserPassword"; return false; }
            if (ConfigurationManager.AppSettings["EmailTo"] == null) { appSetting = "EmailTo"; return false; }
            if (ConfigurationManager.AppSettings["EmailCc"] == null) { appSetting = "EmailCc"; return false; }
            if (ConfigurationManager.AppSettings["EmailFrom"] == null) { appSetting = "EmailFrom"; return false; }
            if (ConfigurationManager.AppSettings["EmailAppErrorSubject"] == null) { appSetting = "EmailAppErrorSubject"; return false; }
            if (ConfigurationManager.AppSettings["EmailAppSubject"] == null) { appSetting = "EmailAppSubject"; return false; }
            if (ConfigurationManager.AppSettings["EmailAppOwner"] == null) { appSetting = "EmailAppOwner"; return false; }

            return true;
        }


        public void WinScpDownloadDirectory(ref SessionOptions sessionOptions, ref Session session, ref string error, string process, string localDirectory)
        {
            try
            {

                if (sessionOptions != null)
                {

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    //This only gets the directories. There will not be any files within the directory. Will have to loop afterward and get files
                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles(ftpDirectory + "*", localDirectory, false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                }
                else
                {
                    error = "ERROR: WinScpDownloadDirectories exception - " + processDirectory + Environment.NewLine + "ERROR MESSAGE: SessionOption object is NULL";
                    return;
                }
            }
            catch (Exception ex)
            {
                error = "ERROR: WinScpDownloadDirectories exception - " + processDirectory + Environment.NewLine + "ERROR MESSAGE: " + ex.ToString();
                return;
            }

        }

        private void WinScpDownloadFiles(Session session, ref string error, string process, string ftpDir, string localProcessDir)
        {
            try
            {

                // Download files
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;

                //This only gets the directories. There will not be any files within the directory. Will have to loop afterward and get files
                TransferOperationResult transferResult;
                transferResult = session.GetFiles(ftpDir + "*", localProcessDir, false, transferOptions);

                // Throw on any error
                transferResult.Check();
            }
            catch (Exception ex)
            {
                error = "ERROR: WinScpDownloadFiles exception - " + processDirectory + Environment.NewLine + "ERROR MESSAGE: " + ex.ToString();
                return;
            }


        }
    }
}
