       /*********************************************************
                  
          Loads flash video into the flash player region. This 
          gets called whenever a video is selected in list.

        **********************************************************/
        function loadVideo(video)
        {
           	var videoName =  video.folder+"/"+video.name;
                var videoDesc =  "<strong>" + video.title + "</strong>";
                var videoLink = "<a class='tt' style='font-size: 10pt; color: #ffffff; font-family: verdana;' href='http://webservices.solomononline.com/dbap/video/"+ 
                                         video.folder +"/"+ video.videoName +
                                         "'>Download Audio/Video<span class='tooltip'><span class='top'></span><span class='middle'>Right-click mouse button and select 'Save Target As'</span><span class='bottom'></span></span></a>";
		var pdfLink   = "<a class='tt' style='font-size: 10pt; color: #ffffff; font-family: verdana;' href='http://webservices.solomononline.com/dbap/video/"+ 
                                         video.folder +"/"+ video.pdfLink +
                                         "'>Download PDF<span class='tooltip'><span class='top'></span><span class='middle'>Right-click mouse button and select 'Save Target As'</span><span class='bottom'></span></span></a>"
           	$("#vMedia").flashembed(videoName,{"autostart": false,
                                              "thumbnail": video.folder+"/"+video.image, 
                                              "thumbscale":65});
                 
		$("#pdfLink").html(pdfLink);
           	$("#vDesc").html(videoDesc);
                $("#vLink").html(videoLink);
           	$(document).scrollTop(0);

		
		
		var videotimelines = "<span style='color: #ffffff'>" + video.videotimelines + "</span>";
                $("#videotimelines").html(videotimelines);
		document.getElementById("timelineheader").style.visibility = 'visible';

		
		var frameSrc = video.HTMLSrc;
		document.getElementById("lastframe").src = frameSrc;

        }


        /*******************************************************
                 
                  Load video information from config.js into the list 
                
        *******************************************************/
        function loadVideoList(){
                //class='videoitem' onclick='loadVideo(videoConfig.videos["+ i + "]);'
                var content="";
             				
             	for(i=0; i < videoConfig.videos.length; i++){
               	    content += "<td onmouseover='javascript:this.bgColor=\"#292929\"' onmouseout='javascript:this.bgColor=\"#333333\"' style='padding-right: 7px; padding-left: 7px; cursor: pointer;' id='vid"+ i + "' class='videoitem' onclick='loadVideo(videoConfig.videos["+ i + "]);'> " + 
                               "<img height=108 width=144 src='"+videoConfig.videos[i].folder+"/FirstFrame.png'/>" + 
                               "<span style='font-size: 13pt; color: #ffffff'>" + (i+1) +" - " +  videoConfig.videos[i].title + "</span> "+ 
                               "</td>";
                 }
 
             	$("#videoGallery").html(content);

		loadVideo(videoConfig.videos[0]);
    
        }
 