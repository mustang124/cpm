﻿-- POSITOR METADATA ---------------------------------------------------------------------------------------------------
--
-- Sets up a table containing the list of available positors. Since at least one positor
-- must be available the table is set up with a default positor with identity 0.
--
-- Positor table ------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Positor', 'U') IS NULL
BEGIN
    CREATE TABLE [dbo].[_Positor] (
        Positor tinyint not null,
        constraint pk_Positor primary key (
            Positor asc
        )
    );
    INSERT INTO [dbo].[_Positor] (
        Positor
    )
    VALUES (
        0 -- the default positor
    );
END
GO
-- KNOTS --------------------------------------------------------------------------------------------------------------
--
-- Knots are used to store finite sets of values, normally used to describe states
-- of entities (through knotted attributes) or relationships (through knotted ties).
-- Knots have their own surrogate identities and are therefore immutable.
-- Values can be added to the set over time though.
-- Knots should have values that are mutually exclusive and exhaustive.
--
-- ANCHORS AND ATTRIBUTES ---------------------------------------------------------------------------------------------
--
-- Anchors are used to store the identities of entities.
-- Anchors are immutable.
-- Attributes are used to store values for properties of entities.
-- Attributes are mutable, their values may change over one or more types of time.
-- Attributes have four flavors: static, historized, knotted static, and knotted historized.
-- Anchors may have zero or more adjoined attributes.
--
-- Anchor table -------------------------------------------------------------------------------------------------------
-- PE_People table (with 1 attributes)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PE_People', 'U') IS NULL
CREATE TABLE [dbo].[PE_People] (
    PE_ID int IDENTITY(1,1) not null,
    PE_Dummy bit null,
    constraint pkPE_People primary key (
        PE_ID asc
    )
);
GO
-- Historized attribute posit table -----------------------------------------------------------------------------------
-- PE_NME_People_Name_Posit table (on PE_People)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PE_NME_People_Name_Posit', 'U') IS NULL
CREATE TABLE [dbo].[PE_NME_People_Name_Posit] (
    PE_NME_ID int not null,
    PE_NME_PE_ID int not null,
    PE_NME_People_Name VARCHAR(24) not null,
    PE_NME_ChangedAt datetime not null,
    constraint fkPE_NME_People_Name_Posit foreign key (
        PE_NME_PE_ID
    ) references [dbo].[PE_People](PE_ID),
    constraint pkPE_NME_People_Name_Posit primary key nonclustered (
        PE_NME_ID asc
    ),
    constraint uqPE_NME_People_Name_Posit unique clustered (
        PE_NME_PE_ID asc,
        PE_NME_ChangedAt desc,
        PE_NME_People_Name asc
    )
);
GO
-- Attribute annex table ----------------------------------------------------------------------------------------------
-- PE_NME_People_Name_Annex table (of PE_NME_People_Name_Posit on PE_People)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PE_NME_People_Name_Annex', 'U') IS NULL
CREATE TABLE [dbo].[PE_NME_People_Name_Annex] (
    PE_NME_ID int not null,
    PE_NME_PositedAt datetime not null,
    PE_NME_Positor tinyint not null,
    PE_NME_Reliability tinyint not null,
    PE_NME_Reliable as isnull(cast(
        case
            when PE_NME_Reliability < 1 then 0
            else 1
        end
    as tinyint), 1) persisted,
    constraint fkPE_NME_People_Name_Annex foreign key (
        PE_NME_ID
    ) references [dbo].[PE_NME_People_Name_Posit](PE_NME_ID),
    constraint pkPE_NME_People_Name_Annex primary key clustered (
        PE_NME_ID asc,
        PE_NME_Positor asc,
        PE_NME_PositedAt desc
    )
);
GO
-- ATTRIBUTE ASSEMBLED VIEWS ------------------------------------------------------------------------------------------
--
-- The assembled view of an attribute combines the posit and annex table of the attribute.
-- It can be used to maintain entity integrity through a primary key, which cannot be
-- defined elsewhere.
--
-- Attribute assembled view -------------------------------------------------------------------------------------------
-- PE_NME_People_Name assembled view of the posit and annex tables,
-- pkPE_NME_People_Name optional temporal consistency constraint
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.PE_NME_People_Name', 'V') IS NULL
BEGIN
    EXEC('
    CREATE VIEW [dbo].[PE_NME_People_Name]
    WITH SCHEMABINDING AS
    SELECT
        p.PE_NME_ID,
        p.PE_NME_PE_ID,
        p.PE_NME_People_Name,
        p.PE_NME_ChangedAt,
        a.PE_NME_PositedAt,
        a.PE_NME_Positor,
        a.PE_NME_Reliability,
        a.PE_NME_Reliable
    FROM
        [dbo].[PE_NME_People_Name_Posit] p
    JOIN
        [dbo].[PE_NME_People_Name_Annex] a
    ON
        a.PE_NME_ID = p.PE_NME_ID;
    ');
    -- Constraint ensuring that recorded and erased posits are temporally consistent
    EXEC('
    CREATE UNIQUE CLUSTERED INDEX [pkPE_NME_People_Name]
    ON [dbo].[PE_NME_People_Name] (
        PE_NME_Reliable desc,
        PE_NME_PE_ID asc,
        PE_NME_ChangedAt desc,
        PE_NME_PositedAt desc,
        PE_NME_Positor asc
    );
    ');
END
GO
-- ATTRIBUTE REWINDERS AND FORWARDERS ---------------------------------------------------------------------------------
--
-- These table valued functions rewind an attribute posit table to the given
-- point in changing time, or an attribute annex table to the given point
-- in positing time. It does not pick a temporal perspective and
-- instead shows all rows that have been in effect before that point
-- in time. The forwarder is the opposite of the rewinder, such that the 
-- union of the two will produce all rows in a posit table.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to rewind to (defaults to End of Time, no rewind)
-- @positingTimepoint the point in positing time to rewind to (defaults to End of Time, no rewind)
--
-- Attribute posit rewinder -------------------------------------------------------------------------------------------
-- rPE_NME_People_Name_Posit rewinding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rPE_NME_People_Name_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rPE_NME_People_Name_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        PE_NME_ID,
        PE_NME_PE_ID,
        PE_NME_People_Name,
        PE_NME_ChangedAt
    FROM
        [dbo].[PE_NME_People_Name_Posit]
    WHERE
        PE_NME_ChangedAt <= @changingTimepoint;
    ');
END
GO
-- Attribute posit forwarder ------------------------------------------------------------------------------------------
-- fPE_NME_People_Name_Posit forwarding over changing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fPE_NME_People_Name_Posit','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fPE_NME_People_Name_Posit] (
        @changingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        PE_NME_ID,
        PE_NME_PE_ID,
        PE_NME_People_Name,
        PE_NME_ChangedAt
    FROM
        [dbo].[PE_NME_People_Name_Posit]
    WHERE
        PE_NME_ChangedAt > @changingTimepoint;
    ');
END
GO
-- Attribute annex rewinder -------------------------------------------------------------------------------------------
-- rPE_NME_People_Name_Annex rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rPE_NME_People_Name_Annex','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rPE_NME_People_Name_Annex] (
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        PE_NME_ID,
        PE_NME_PositedAt,
        PE_NME_Positor,
        PE_NME_Reliability,
        PE_NME_Reliable
    FROM
        [dbo].[PE_NME_People_Name_Annex]
    WHERE
        PE_NME_PositedAt <= @positingTimepoint;
    ');
END
GO
-- Attribute assembled rewinder ---------------------------------------------------------------------------------------
-- rPE_NME_People_Name rewinding over changing and positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rPE_NME_People_Name','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rPE_NME_People_Name] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.PE_NME_ID,
        a.PE_NME_PositedAt,
        a.PE_NME_Positor,
        a.PE_NME_Reliability,
        a.PE_NME_Reliable,
        p.PE_NME_PE_ID,
        p.PE_NME_People_Name,
        p.PE_NME_ChangedAt
    FROM
        [dbo].[rPE_NME_People_Name_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rPE_NME_People_Name_Annex](@positingTimepoint) a
    ON
        a.PE_NME_ID = p.PE_NME_ID
    AND
        a.PE_NME_Positor = @positor
    AND
        a.PE_NME_PositedAt = (
            SELECT TOP 1
                sub.PE_NME_PositedAt
            FROM
                [dbo].[rPE_NME_People_Name_Annex](@positingTimepoint) sub
            WHERE
                sub.PE_NME_ID = p.PE_NME_ID
            AND
                sub.PE_NME_Positor = @positor
            ORDER BY
                sub.PE_NME_PositedAt DESC
        )
    ');
END
GO
-- Attribute assembled forwarder --------------------------------------------------------------------------------------
-- fPE_NME_People_Name forwarding over changing and rewinding over positing time function
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.fPE_NME_People_Name','IF') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[fPE_NME_People_Name] (
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS TABLE WITH SCHEMABINDING AS RETURN
    SELECT
        p.PE_NME_ID,
        a.PE_NME_PositedAt,
        a.PE_NME_Positor,
        a.PE_NME_Reliability,
        a.PE_NME_Reliable,
        p.PE_NME_PE_ID,
        p.PE_NME_People_Name,
        p.PE_NME_ChangedAt
    FROM
        [dbo].[fPE_NME_People_Name_Posit](@changingTimepoint) p
    JOIN
        [dbo].[rPE_NME_People_Name_Annex](@positingTimepoint) a
    ON
        a.PE_NME_ID = p.PE_NME_ID
    AND
        a.PE_NME_Positor = @positor
    AND
        a.PE_NME_PositedAt = (
            SELECT TOP 1
                sub.PE_NME_PositedAt
            FROM
                [dbo].[rPE_NME_People_Name_Annex](@positingTimepoint) sub
            WHERE
                sub.PE_NME_ID = p.PE_NME_ID
            AND
                sub.PE_NME_Positor = @positor
            ORDER BY
                sub.PE_NME_PositedAt DESC
        )
    ');
END
GO
-- Attribute previous value -------------------------------------------------------------------------------------------
-- prePE_NME_People_Name function for getting previous value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.prePE_NME_People_Name','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[prePE_NME_People_Name] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(24)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            pre.PE_NME_People_Name
        FROM
            [dbo].[rPE_NME_People_Name](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) pre
        WHERE
            pre.PE_NME_PE_ID = @id
        AND
            pre.PE_NME_ChangedAt < @changingTimepoint
        AND
            pre.PE_NME_Reliable = 1
        ORDER BY
            pre.PE_NME_ChangedAt DESC,
            pre.PE_NME_PositedAt DESC
    );
    END
    ');
END
GO
-- Attribute following value ------------------------------------------------------------------------------------------
-- folPE_NME_People_Name function for getting following value
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.folPE_NME_People_Name','FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[folPE_NME_People_Name] (
        @id int,
        @positor tinyint = 0,
        @changingTimepoint datetime = ''9999-12-31'',
        @positingTimepoint datetime = ''9999-12-31''
    )
    RETURNS VARCHAR(24)
    AS
    BEGIN RETURN (
        SELECT TOP 1
            fol.PE_NME_People_Name
        FROM
            [dbo].[fPE_NME_People_Name](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) fol
        WHERE
            fol.PE_NME_PE_ID = @id
        AND
            fol.PE_NME_ChangedAt > @changingTimepoint
        AND
            fol.PE_NME_Reliable = 1
        ORDER BY
            fol.PE_NME_ChangedAt ASC,
            fol.PE_NME_PositedAt DESC
    );
    END
    ');
END
GO
-- ATTRIBUTE RESTATEMENT CONSTRAINTS ----------------------------------------------------------------------------------
--
-- Attributes may be prevented from storing restatements.
-- A restatement is when the same value occurs for two adjacent points
-- in changing time. Note that restatement checking is not done for
-- unreliable information as this could prevent demotion.
--
-- returns 1 for at least one equal surrounding value, 0 for different surrounding values
--
-- @posit the id of the posit that should be checked
-- @posited the time when this posit was made
-- @positor the one who made the posit
-- @reliable whether this posit is considered reliable (1) or unreliable (0)
--
-- Restatement Finder Function and Constraint -------------------------------------------------------------------------
-- rfPE_NME_People_Name restatement finder, also used by the insert and update triggers for idempotent attributes
-- rcPE_NME_People_Name restatement constraint (available only in attributes that cannot have restatements)
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.rfPE_NME_People_Name', 'FN') IS NULL
BEGIN
    EXEC('
    CREATE FUNCTION [dbo].[rfPE_NME_People_Name] (
        @posit int,
        @posited datetime,
        @positor tinyint,
        @reliable tinyint
    )
    RETURNS tinyint AS
    BEGIN
    DECLARE @id int;
    DECLARE @value VARCHAR(24);
    DECLARE @changed datetime;
    SELECT
        @id = PE_NME_PE_ID,
        @value = PE_NME_People_Name,
        @changed = PE_NME_ChangedAt
    FROM
        [dbo].[PE_NME_People_Name_Posit]
    WHERE
        PE_NME_ID = @posit;
    RETURN (
        CASE
        WHEN @reliable = 0
        THEN 0
        WHEN EXISTS (
            SELECT
                @value
            WHERE
                @value = (
                    SELECT TOP 1
                        pre.PE_NME_People_Name
                    FROM
                        [dbo].[rPE_NME_People_Name](
                            @positor,
                            @changed,
                            @posited
                        ) pre
                    WHERE
                        pre.PE_NME_PE_ID = @id
                    AND
                        pre.PE_NME_ChangedAt < @changed
                    AND
                        pre.PE_NME_Reliable = 1
                    ORDER BY
                        pre.PE_NME_ChangedAt DESC,
                        pre.PE_NME_PositedAt DESC
                )
        ) OR EXISTS (
            SELECT
                @value
            WHERE
                @value = ( 
                    SELECT TOP 1
                        fol.PE_NME_People_Name
                    FROM
                        [dbo].[fPE_NME_People_Name](
                            @positor,
                            @changed,
                            @posited
                        ) fol
                    WHERE
                        fol.PE_NME_PE_ID = @id
                    AND
                        fol.PE_NME_ChangedAt > @changed
                    AND
                        fol.PE_NME_Reliable = 1
                    ORDER BY
                        fol.PE_NME_ChangedAt ASC,
                        fol.PE_NME_PositedAt DESC
                )
        )
        THEN 1
        ELSE 0
        END
    );
    END
    ');
END
GO
-- ANCHOR TEMPORAL PERSPECTIVES ---------------------------------------------------------------------------------------
--
-- These table valued functions simplify temporal querying by providing a temporal
-- perspective of each anchor. There are five types of perspectives: time traveling, latest,
-- point-in-time, difference, and now. They also denormalize the anchor, its attributes,
-- and referenced knots from sixth to third normal form.
--
-- The time traveling perspective shows information as it was or will be based on a number
-- of input parameters.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to travel to (defaults to End of Time)
-- @positingTimepoint the point in positing time to travel to (defaults to End of Time)
-- @reliable whether to show reliable (1) or unreliable (0) results
--
-- The latest perspective shows the latest available (changing & positing) information for each anchor.
-- The now perspective shows the information as it is right now, with latest positing time.
-- The point-in-time perspective lets you travel through the information to the given timepoint,
-- with latest positing time and the given point in changing time.
--
-- @changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints, and for
-- changes in all or a selection of attributes, with latest positing time.
--
-- @intervalStart the start of the interval for finding changes
-- @intervalEnd the end of the interval for finding changes
-- @selection a list of mnemonics for tracked attributes, ie 'MNE MON ICS', or null for all
--
-- Drop perspectives --------------------------------------------------------------------------------------------------
IF Object_ID('dbo.dPE_People', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[dPE_People];
IF Object_ID('dbo.nPE_People', 'V') IS NOT NULL
DROP VIEW [dbo].[nPE_People];
IF Object_ID('dbo.pPE_People', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[pPE_People];
IF Object_ID('dbo.lPE_People', 'V') IS NOT NULL
DROP VIEW [dbo].[lPE_People];
IF Object_ID('dbo.tPE_People', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[tPE_People];
GO
-- Time traveling perspective -----------------------------------------------------------------------------------------
-- tPE_People viewed as given by the input parameters
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[tPE_People] (
    @positor tinyint = 0,
    @changingTimepoint datetime2(7) = '9999-12-31',
    @positingTimepoint datetime = '9999-12-31',
    @reliable tinyint = 1
)
RETURNS TABLE WITH SCHEMABINDING AS RETURN
SELECT
    [PE].PE_ID,
    [NME].PE_NME_PE_ID,
    [NME].PE_NME_ID,
    [NME].PE_NME_ChangedAt,
    [NME].PE_NME_PositedAt,
    [NME].PE_NME_Positor,
    [NME].PE_NME_Reliability,
    [NME].PE_NME_Reliable,
    [NME].PE_NME_People_Name
FROM
    [dbo].[PE_People] [PE]
LEFT JOIN
    [dbo].[rPE_NME_People_Name](
        @positor,
        @changingTimepoint,
        @positingTimepoint
    ) [NME]
ON
    [NME].PE_NME_ID = (
        SELECT TOP 1
            sub.PE_NME_ID
        FROM
            [dbo].[rPE_NME_People_Name](
                @positor,
                @changingTimepoint,
                @positingTimepoint
            ) sub
        WHERE
            sub.PE_NME_PE_ID = [PE].PE_ID
        AND
            sub.PE_NME_Reliable = @reliable
        ORDER BY
            sub.PE_NME_ChangedAt DESC,
            sub.PE_NME_PositedAt DESC
    );
GO
-- Latest perspective -------------------------------------------------------------------------------------------------
-- lPE_People viewed by the latest available information for all positors (may include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[lPE_People]
AS
SELECT
    p.*, 
    1 as Reliable,
    [PE].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tPE_People] (
        p.Positor,
        DEFAULT,
        DEFAULT,
        DEFAULT
    ) [PE];
GO
-- Point-in-time perspective ------------------------------------------------------------------------------------------
-- pPE_People viewed as it was on the given timepoint
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[pPE_People] (
    @changingTimepoint datetime2(7)
)
RETURNS TABLE AS RETURN
SELECT
    p.*, 
    1 as Reliable,
    [PE].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tPE_People] (
        p.Positor,
        @changingTimepoint,
        DEFAULT,
        DEFAULT
    ) [PE];
GO
-- Now perspective ----------------------------------------------------------------------------------------------------
-- nPE_People viewed as it currently is (cannot include future versions)
-----------------------------------------------------------------------------------------------------------------------
CREATE VIEW [dbo].[nPE_People]
AS
SELECT
    p.*, 
    1 as Reliable,
    [PE].*
FROM
    [dbo].[_Positor] p
CROSS APPLY
    [dbo].[tPE_People] (
        p.Positor,
        sysdatetime(),
        DEFAULT,
        DEFAULT
    ) [PE];
GO
-- Difference perspective ---------------------------------------------------------------------------------------------
-- dPE_People showing all differences between the given timepoints and optionally for a subset of attributes
-----------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[dPE_People] (
    @intervalStart datetime2(7),
    @intervalEnd datetime2(7),
    @selection varchar(max) = null
)
RETURNS TABLE AS RETURN
SELECT
    timepoints.inspectedTimepoint,
    [PE].*
FROM
    [dbo].[_Positor] p
JOIN (
    SELECT DISTINCT
        PE_NME_Positor AS positor,
        PE_NME_PE_ID AS PE_ID,
        PE_NME_ChangedAt AS inspectedTimepoint,
        'NME' AS mnemonic
    FROM
        [dbo].[PE_NME_People_Name]
    WHERE
        (@selection is null OR @selection like '%NME%')
    AND
        PE_NME_ChangedAt BETWEEN @intervalStart AND @intervalEnd
) timepoints
ON
    timepoints.positor = p.Positor
CROSS APPLY
    [dbo].[tPE_People] (
        timepoints.positor,
        timepoints.inspectedTimepoint,
        DEFAULT,
        DEFAULT
    ) [PE]
 WHERE
    [PE].PE_ID = timepoints.PE_ID;
GO
-- KEY GENERATORS -----------------------------------------------------------------------------------------------------
--
-- These stored procedures can be used to generate identities of entities.
-- Corresponding anchors must have an incrementing identity column.
--
-- Key Generation Stored Procedure ------------------------------------------------------------------------------------
-- kPE_People identity by surrogate key generation stored procedure
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.kPE_People', 'P') IS NULL
BEGIN
    EXEC('
    CREATE PROCEDURE [dbo].[kPE_People] (
        @requestedNumberOfIdentities bigint
    ) AS
    BEGIN
        SET NOCOUNT ON;
        IF @requestedNumberOfIdentities > 0
        BEGIN
            WITH idGenerator (idNumber) AS (
                SELECT
                    1
                UNION ALL
                SELECT
                    idNumber + 1
                FROM
                    idGenerator
                WHERE
                    idNumber < @requestedNumberOfIdentities
            )
            INSERT INTO [dbo].[PE_People] (
                PE_Dummy
            )
            OUTPUT
                inserted.PE_ID
            SELECT
                null
            FROM
                idGenerator
            OPTION (maxrecursion 0);
        END
    END
    ');
END
GO
-- ATTRIBUTE TRIGGERS ------------------------------------------------------------------------------------------------
--
-- The following triggers on the assembled views make them behave like tables.
-- There is one 'instead of' trigger for: insert.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_PE_NME_People_Name instead of INSERT trigger on PE_NME_People_Name
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.it_PE_NME_People_Name', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[it_PE_NME_People_Name];
GO
CREATE TRIGGER [dbo].[it_PE_NME_People_Name] ON [dbo].[PE_NME_People_Name]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @maxVersion int;
    DECLARE @currentVersion int;

    DECLARE @PE_NME_People_Name TABLE (
        PE_NME_PE_ID int not null,
        PE_NME_ChangedAt datetime not null,
        PE_NME_Positor tinyint not null,
        PE_NME_PositedAt datetime not null,
        PE_NME_Reliability tinyint not null,
        PE_NME_Reliable tinyint not null,
        PE_NME_People_Name VARCHAR(24) not null,
        PE_NME_Version bigint not null,
        PE_NME_StatementType char(1) not null,
        primary key(
            PE_NME_Version,
            PE_NME_Positor,
            PE_NME_PE_ID
        )
    );

    INSERT INTO @PE_NME_People_Name
    SELECT
        i.PE_NME_PE_ID,
        i.PE_NME_ChangedAt,
        i.PE_NME_Positor,
        i.PE_NME_PositedAt,
        i.PE_NME_Reliability,
        case
            when i.PE_NME_Reliability < 1 then 0
            else 1
        end,
        i.PE_NME_People_Name,
        DENSE_RANK() OVER (
            PARTITION BY
                i.PE_NME_Positor,
                i.PE_NME_PE_ID
            ORDER BY
                i.PE_NME_ChangedAt ASC,
                i.PE_NME_PositedAt ASC,
                i.PE_NME_Reliability ASC
        ),
        'X'
    FROM
        inserted i;

    SELECT
        @maxVersion = max(PE_NME_Version),
        @currentVersion = 0
    FROM
        @PE_NME_People_Name;
    WHILE (@currentVersion < @maxVersion)
    BEGIN
        SET @currentVersion = @currentVersion + 1;
        UPDATE v
        SET
            v.PE_NME_StatementType =
                CASE
                    WHEN EXISTS (
                        SELECT TOP 1
                            t.PE_NME_ID
                        FROM
                            [dbo].[tPE_People](v.PE_NME_Positor, v.PE_NME_ChangedAt, v.PE_NME_PositedAt, v.PE_NME_Reliable) t
                        WHERE
                            t.PE_NME_PE_ID = v.PE_NME_PE_ID
                        AND
                            t.PE_NME_ChangedAt = v.PE_NME_ChangedAt
                        AND
                            t.PE_NME_Reliability = v.PE_NME_Reliability
                        AND
                            t.PE_NME_People_Name = v.PE_NME_People_Name
                    )
                    THEN 'D' -- duplicate assertion
                    WHEN p.PE_NME_PE_ID is not null
                    THEN 'S' -- duplicate statement
                    WHEN EXISTS (
                        SELECT
                            v.PE_NME_People_Name
                        WHERE
                            v.PE_NME_People_Name =
                                dbo.prePE_NME_People_Name (
                                    v.PE_NME_PE_ID,
                                    v.PE_NME_Positor,
                                    v.PE_NME_ChangedAt,
                                    v.PE_NME_PositedAt
                                )
                    ) OR EXISTS (
                        SELECT
                            v.PE_NME_People_Name
                        WHERE
                            v.PE_NME_People_Name =
                                dbo.folPE_NME_People_Name (
                                    v.PE_NME_PE_ID,
                                    v.PE_NME_Positor,
                                    v.PE_NME_ChangedAt,
                                    v.PE_NME_PositedAt
                                )
                    )
                    THEN 'R' -- restatement
                    ELSE 'N' -- new statement
                END
        FROM
            @PE_NME_People_Name v
        LEFT JOIN
            [dbo].[PE_NME_People_Name_Posit] p
        ON
            p.PE_NME_PE_ID = v.PE_NME_PE_ID
        AND
            p.PE_NME_ChangedAt = v.PE_NME_ChangedAt
        AND
            p.PE_NME_People_Name = v.PE_NME_People_Name
        WHERE
            v.PE_NME_Version = @currentVersion;

        INSERT INTO [dbo].[PE_NME_People_Name_Posit] (
            PE_NME_PE_ID,
            PE_NME_ChangedAt,
            PE_NME_People_Name
        )
        SELECT
            PE_NME_PE_ID,
            PE_NME_ChangedAt,
            PE_NME_People_Name
        FROM
            @PE_NME_People_Name
        WHERE
            PE_NME_Version = @currentVersion
        AND
            PE_NME_StatementType in ('N','D','R');

        INSERT INTO [dbo].[PE_NME_People_Name_Annex] (
            PE_NME_ID,
            PE_NME_Positor,
            PE_NME_PositedAt,
            PE_NME_Reliability
        )
        SELECT
            p.PE_NME_ID,
            v.PE_NME_Positor,
            v.PE_NME_PositedAt,
            v.PE_NME_Reliability
        FROM
            @PE_NME_People_Name v
        JOIN
            [dbo].[PE_NME_People_Name_Posit] p
        ON
            p.PE_NME_PE_ID = v.PE_NME_PE_ID
        AND
            p.PE_NME_ChangedAt = v.PE_NME_ChangedAt
        AND
            p.PE_NME_People_Name = v.PE_NME_People_Name
        WHERE
            v.PE_NME_Version = @currentVersion
        AND
            PE_NME_StatementType in ('S','N','D','R');
    END
END
GO
-- ANCHOR TRIGGERS ---------------------------------------------------------------------------------------------------
--
-- The following triggers on the latest view make it behave like a table.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent attributes, only changes that represent a value different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Insert trigger -----------------------------------------------------------------------------------------------------
-- it_lPE_People instead of INSERT trigger on lPE_People
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[it_lPE_People] ON [dbo].[lPE_People]
INSTEAD OF INSERT
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();

    DECLARE @PE TABLE (
        Row bigint IDENTITY(1,1) not null primary key,
        PE_ID int not null
    );

    INSERT INTO [dbo].[PE_People] (
        PE_Dummy
    )
    OUTPUT
        inserted.PE_ID
    INTO
        @PE
    SELECT
        null
    FROM
        inserted
    WHERE
        inserted.PE_ID is null;

    DECLARE @inserted TABLE (
        PE_ID int not null,
        PE_NME_PE_ID int null,
        PE_NME_ChangedAt datetime null,
        PE_NME_Positor tinyint null,
        PE_NME_PositedAt datetime null,
        PE_NME_Reliability tinyint null,
        PE_NME_People_Name VARCHAR(24) null
    );

    INSERT INTO @inserted
    SELECT
        ISNULL(i.PE_ID, a.PE_ID),
        ISNULL(ISNULL(i.PE_NME_PE_ID, i.PE_ID), a.PE_ID),
        ISNULL(i.PE_NME_ChangedAt, @now),
        ISNULL(ISNULL(i.PE_NME_Positor, i.Positor), 0),
        ISNULL(i.PE_NME_PositedAt, @now),
        ISNULL(i.PE_NME_Reliability,
        CASE
            WHEN i.Reliable = 0 THEN 0
            WHEN i.PE_NME_Reliable = 0 THEN 0
            ELSE 1
        END),
        i.PE_NME_People_Name
    FROM (
        SELECT
            Positor,
            Reliable,
            PE_ID,
            PE_NME_PE_ID,
            PE_NME_ChangedAt,
            PE_NME_Positor,
            PE_NME_PositedAt,
            PE_NME_Reliability,
            PE_NME_Reliable,
            PE_NME_People_Name,
            ROW_NUMBER() OVER (PARTITION BY PE_ID ORDER BY PE_ID) AS Row
        FROM
            inserted
    ) i
    LEFT JOIN
        @PE a
    ON
        a.Row = i.Row;

    INSERT INTO [dbo].[PE_NME_People_Name] (
        PE_NME_PE_ID,
        PE_NME_People_Name,
        PE_NME_ChangedAt,
        PE_NME_PositedAt,
        PE_NME_Positor,
        PE_NME_Reliability
    )
    SELECT
        i.PE_NME_PE_ID,
        i.PE_NME_People_Name,
        i.PE_NME_ChangedAt,
        i.PE_NME_PositedAt,
        i.PE_NME_Positor,
        i.PE_NME_Reliability
    FROM
        @inserted i
    WHERE
        i.PE_NME_People_Name is not null;
END
GO
-- UPDATE trigger -----------------------------------------------------------------------------------------------------
-- ut_lPE_People instead of UPDATE trigger on lPE_People
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[ut_lPE_People] ON [dbo].[lPE_People]
INSTEAD OF UPDATE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    IF(UPDATE(PE_ID))
        RAISERROR('The identity column PE_ID is not updatable.', 16, 1);
    IF(UPDATE(PE_NME_ID))
        RAISERROR('The identity column PE_NME_ID is not updatable.', 16, 1);
    IF(UPDATE(PE_NME_PE_ID))
        RAISERROR('The foreign key column PE_NME_PE_ID is not updatable.', 16, 1);
    IF(UPDATE(PE_NME_People_Name))
    BEGIN
        INSERT INTO [dbo].[PE_NME_People_Name] (
            PE_NME_PE_ID,
            PE_NME_People_Name,
            PE_NME_ChangedAt,
            PE_NME_PositedAt,
            PE_NME_Positor,
            PE_NME_Reliability
        )
        SELECT
            ISNULL(i.PE_NME_PE_ID, i.PE_ID),
            i.PE_NME_People_Name,
            cast(CASE
                WHEN i.PE_NME_People_Name is null THEN i.PE_NME_ChangedAt
                WHEN UPDATE(PE_NME_ChangedAt) THEN i.PE_NME_ChangedAt
                ELSE @now
            END as datetime),
            cast(CASE WHEN UPDATE(PE_NME_PositedAt) THEN i.PE_NME_PositedAt ELSE @now END as datetime),
            CASE WHEN UPDATE(Positor) THEN i.Positor ELSE ISNULL(i.PE_NME_Positor, 0) END,
            CASE
                WHEN i.PE_NME_People_Name is null THEN 0
                WHEN UPDATE(Reliable) THEN
                    CASE i.Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                WHEN UPDATE(PE_NME_Reliable) THEN
                    CASE i.PE_NME_Reliable
                        WHEN 0 THEN 0
                        ELSE 1
                    END
                ELSE ISNULL(i.PE_NME_Reliability, 1)
            END
        FROM
            inserted i
    END
END
GO
-- DELETE trigger -----------------------------------------------------------------------------------------------------
-- dt_lPE_People instead of DELETE trigger on lPE_People
-----------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[dt_lPE_People] ON [dbo].[lPE_People]
INSTEAD OF DELETE
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @now datetime2(7);
    SET @now = sysdatetime();
    INSERT INTO [dbo].[PE_NME_People_Name_Annex] (
        PE_NME_ID,
        PE_NME_Positor,
        PE_NME_PositedAt,
        PE_NME_Reliability
    )
    SELECT
        p.PE_NME_ID,
        p.PE_NME_Positor,
        @now,
        0
    FROM
        deleted d
    JOIN
        [dbo].[PE_NME_People_Name_Annex] p
    ON
        p.PE_NME_ID = d.PE_NME_ID;
END
GO
-- TIES ---------------------------------------------------------------------------------------------------------------
--
-- Ties are used to represent relationships between entities.
-- They come in four flavors: static, historized, knotted static, and knotted historized.
-- Ties have cardinality, constraining how members may participate in the relationship.
-- Every entity that is a member in a tie has a specified role in the relationship.
-- Ties must have at least two anchor roles and zero or more knot roles.
--
-- TIE ASSEMBLED VIEWS ------------------------------------------------------------------------------------------------
--
-- The assembled view of a tie combines the posit and annex table of the tie.
-- It can be used to maintain entity integrity through a primary key, which cannot be
-- defined elsewhere.
--
-- TIE REWINDERS AND FORWARDERS ---------------------------------------------------------------------------------------
--
-- These table valued functions rewind a tie posit table to the given
-- point in changing time, or a tie annex table to the given point
-- in positing time. It does not pick a temporal perspective and
-- instead shows all rows that have been in effect before that point
-- in time. The forwarder is the opposite of the rewinder, such that 
-- their union corresponds to all rows in the posit table.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to rewind to (defaults to End of Time, no rewind)
-- @positingTimepoint the point in positing time to rewind to (defaults to End of Time, no rewind)
--
-- TIE TEMPORAL PERSPECTIVES ------------------------------------------------------------------------------------------
--
-- These table valued functions simplify temporal querying by providing a temporal
-- perspective of each tie. There are four types of perspectives: latest,
-- point-in-time, difference, and now.
--
-- The time traveling perspective shows information as it was or will be based on a number
-- of input parameters.
--
-- @positor the view of which positor to adopt (defaults to 0)
-- @changingTimepoint the point in changing time to travel to (defaults to End of Time)
-- @positingTimepoint the point in positing time to travel to (defaults to End of Time)
-- @reliable whether to show reliable (1) or unreliable (0) results
--
-- The latest perspective shows the latest available information for each tie.
-- The now perspective shows the information as it is right now.
-- The point-in-time perspective lets you travel through the information to the given timepoint.
--
-- @changingTimepoint the point in changing time to travel to
--
-- The difference perspective shows changes between the two given timepoints.
--
-- @intervalStart the start of the interval for finding changes
-- @intervalEnd the end of the interval for finding changes
--
-- TIE TRIGGERS -------------------------------------------------------------------------------------------------------
--
-- The following triggers on the assembled and latest views make them behave like tables.
-- There are three different 'instead of' triggers: insert, update, and delete.
-- They will ensure that such operations are propagated to the underlying tables
-- in a consistent way. Default values are used for some columns if not provided
-- by the corresponding SQL statements.
--
-- For idempotent ties, only changes that represent values different from
-- the previous or following value are stored. Others are silently ignored in
-- order to avoid unnecessary temporal duplicates.
--
-- Attribute sheet stack ----------------------------------------------------------------------------------------------
-- ssPE_NME_People_Name procedure returning one temporal sheet per positor
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo.ssPE_NME_People_Name', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[ssPE_NME_People_Name];
GO
CREATE PROCEDURE [dbo].[ssPE_NME_People_Name] (
    @PE_ID int = null
)
AS
BEGIN
    DECLARE @pivot varchar(max);
    SET @pivot = REPLACE((
        SELECT DISTINCT
            '[' + convert(varchar(23), PE_NME_ChangedAt, 126) + ']' AS [text()]
        FROM
            [dbo].[PE_NME_People_Name]
        WHERE
            PE_NME_PE_ID = ISNULL(@PE_ID, PE_NME_PE_ID)
        FOR XML PATH('')
    ), '][', '], [');
    DECLARE positor CURSOR FOR
    SELECT DISTINCT
        PE_NME_Positor
    FROM
        [dbo].[PE_NME_People_Name]
    WHERE
        PE_NME_PE_ID = ISNULL(@PE_ID, PE_NME_PE_ID)
    AND
        PE_NME_People_Name is not null;
    DECLARE @positor varchar(20);
    OPEN positor;
    FETCH NEXT FROM positor INTO @positor;
    DECLARE @sql varchar(max);
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @sql = '
            SELECT
                b.PE_NME_Positor,
                x.PE_NME_PE_ID,
                x.PE_NME_PositedAt,
                b.PE_NME_Reliable,
                ' + @pivot + '
            FROM (
                SELECT DISTINCT
                    PE_NME_PE_ID,
                    PE_NME_PositedAt
                FROM
                    [dbo].[PE_NME_People_Name]
                WHERE
                    PE_NME_PE_ID = ISNULL(@PE_ID, PE_NME_PE_ID)
            ) x
            LEFT JOIN (
                SELECT
                    PE_NME_Positor,
                    PE_NME_PE_ID,
                    PE_NME_PositedAt,
                    PE_NME_Reliable,
                    ' + @pivot + '
                FROM
                    [dbo].[PE_NME_People_Name]
                PIVOT (
                    MIN(PE_NME_People_Name) FOR PE_NME_ChangedAt IN (' + @pivot + ')
                ) p
                WHERE
                    PE_NME_PE_ID = ISNULL(@PE_ID, PE_NME_PE_ID)
                AND
                    PE_NME_Positor = ' + @positor + '
            ) b
            ON
                b.PE_NME_PE_ID = x.PE_NME_PE_ID
            AND
                b.PE_NME_PositedAt = x.PE_NME_PositedAt
            ORDER BY
                PE_NME_PE_ID,
                PE_NME_PositedAt,
                PE_NME_Reliable
        ';
        EXEC(@sql);
        FETCH NEXT FROM positor INTO @positor;
    END
    CLOSE positor;
    DEALLOCATE positor;
END
GO
-- SCHEMA EVOLUTION ---------------------------------------------------------------------------------------------------
--
-- The following tables, views, and functions are used to track schema changes
-- over time, as well as providing every XML that has been 'executed' against
-- the database.
--
-- Schema table -------------------------------------------------------------------------------------------------------
-- The schema table holds every xml that has been executed against the database
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Schema', 'U') IS NULL
   CREATE TABLE [dbo].[_Schema] (
      [version] int identity(1, 1) not null primary key,
      [activation] datetime2(7) not null,
      [schema] xml not null
   );
GO
-- Insert the XML schema (as of now)
INSERT INTO [dbo].[_Schema] (
   [activation],
   [schema]
)
SELECT
   current_timestamp,
   N'<schema format="0.98" date="2015-04-20" time="16:38:49"><metadata changingRange="datetime" encapsulation="dbo" identity="int" metadataPrefix="Metadata" metadataType="int" metadataUsage="false" changingSuffix="ChangedAt" identitySuffix="ID" positIdentity="int" positGenerator="true" positingRange="datetime" positingSuffix="PositedAt" positorRange="tinyint" positorSuffix="Positor" reliabilityRange="tinyint" reliabilitySuffix="Reliability" reliableCutoff="1" deleteReliability="0" reliableSuffix="Reliable" partitioning="false" entityIntegrity="true" restatability="true" idempotency="false" assertiveness="true" naming="improved" positSuffix="Posit" annexSuffix="Annex" chronon="datetime2(7)" now="sysdatetime()" dummySuffix="Dummy" versionSuffix="Version" statementTypeSuffix="StatementType" checksumSuffix="Checksum" businessViews="false" equivalence="false" equivalentSuffix="EQ" equivalentRange="tinyint" databaseTarget="SQLServer" temporalization="crt"/><anchor mnemonic="PE" descriptor="People" identity="int"><metadata capsule="dbo" generator="true"/><attribute mnemonic="NME" descriptor="Name" identity="int" timeRange="datetime" dataRange="VARCHAR(24)"><metadata capsule="dbo" generator="false" assertive="true" restatable="true" idempotent="false"/><layout x="1012.65" y="578.27" fixed="false"/></attribute><layout x="960.00" y="549.50" fixed="false"/></anchor></schema>';
GO
-- Schema expanded view -----------------------------------------------------------------------------------------------
-- A view of the schema table that expands the XML attributes into columns
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Schema_Expanded', 'V') IS NOT NULL
DROP VIEW [dbo].[_Schema_Expanded]
GO
CREATE VIEW [dbo].[_Schema_Expanded]
AS
SELECT
	[version],
	[activation],
	[schema],
	[schema].value('schema[1]/@format', 'nvarchar(max)') as [format],
	[schema].value('schema[1]/@date', 'date') as [date],
	[schema].value('schema[1]/@time', 'time(0)') as [time],
	[schema].value('schema[1]/metadata[1]/@temporalization', 'nvarchar(max)') as [temporalization], 
	[schema].value('schema[1]/metadata[1]/@databaseTarget', 'nvarchar(max)') as [databaseTarget],
	[schema].value('schema[1]/metadata[1]/@changingRange', 'nvarchar(max)') as [changingRange],
	[schema].value('schema[1]/metadata[1]/@encapsulation', 'nvarchar(max)') as [encapsulation],
	[schema].value('schema[1]/metadata[1]/@identity', 'nvarchar(max)') as [identity],
	[schema].value('schema[1]/metadata[1]/@metadataPrefix', 'nvarchar(max)') as [metadataPrefix],
	[schema].value('schema[1]/metadata[1]/@metadataType', 'nvarchar(max)') as [metadataType],
	[schema].value('schema[1]/metadata[1]/@metadataUsage', 'nvarchar(max)') as [metadataUsage],
	[schema].value('schema[1]/metadata[1]/@changingSuffix', 'nvarchar(max)') as [changingSuffix],
	[schema].value('schema[1]/metadata[1]/@identitySuffix', 'nvarchar(max)') as [identitySuffix],
	[schema].value('schema[1]/metadata[1]/@positIdentity', 'nvarchar(max)') as [positIdentity],
	[schema].value('schema[1]/metadata[1]/@positGenerator', 'nvarchar(max)') as [positGenerator],
	[schema].value('schema[1]/metadata[1]/@positingRange', 'nvarchar(max)') as [positingRange],
	[schema].value('schema[1]/metadata[1]/@positingSuffix', 'nvarchar(max)') as [positingSuffix],
	[schema].value('schema[1]/metadata[1]/@positorRange', 'nvarchar(max)') as [positorRange],
	[schema].value('schema[1]/metadata[1]/@positorSuffix', 'nvarchar(max)') as [positorSuffix],
	[schema].value('schema[1]/metadata[1]/@reliabilityRange', 'nvarchar(max)') as [reliabilityRange],
	[schema].value('schema[1]/metadata[1]/@reliabilitySuffix', 'nvarchar(max)') as [reliabilitySuffix],
	[schema].value('schema[1]/metadata[1]/@reliableCutoff', 'nvarchar(max)') as [reliableCutoff],
	[schema].value('schema[1]/metadata[1]/@deleteReliability', 'nvarchar(max)') as [deleteReliability],
	[schema].value('schema[1]/metadata[1]/@reliableSuffix', 'nvarchar(max)') as [reliableSuffix],
	[schema].value('schema[1]/metadata[1]/@partitioning', 'nvarchar(max)') as [partitioning],
	[schema].value('schema[1]/metadata[1]/@entityIntegrity', 'nvarchar(max)') as [entityIntegrity],
	[schema].value('schema[1]/metadata[1]/@restatability', 'nvarchar(max)') as [restatability],
	[schema].value('schema[1]/metadata[1]/@idempotency', 'nvarchar(max)') as [idempotency],
	[schema].value('schema[1]/metadata[1]/@assertiveness', 'nvarchar(max)') as [assertiveness],
	[schema].value('schema[1]/metadata[1]/@naming', 'nvarchar(max)') as [naming],
	[schema].value('schema[1]/metadata[1]/@positSuffix', 'nvarchar(max)') as [positSuffix],
	[schema].value('schema[1]/metadata[1]/@annexSuffix', 'nvarchar(max)') as [annexSuffix],
	[schema].value('schema[1]/metadata[1]/@chronon', 'nvarchar(max)') as [chronon],
	[schema].value('schema[1]/metadata[1]/@now', 'nvarchar(max)') as [now],
	[schema].value('schema[1]/metadata[1]/@dummySuffix', 'nvarchar(max)') as [dummySuffix],
	[schema].value('schema[1]/metadata[1]/@statementTypeSuffix', 'nvarchar(max)') as [statementTypeSuffix],
	[schema].value('schema[1]/metadata[1]/@checksumSuffix', 'nvarchar(max)') as [checksumSuffix],
	[schema].value('schema[1]/metadata[1]/@businessViews', 'nvarchar(max)') as [businessViews],
	[schema].value('schema[1]/metadata[1]/@equivalence', 'nvarchar(max)') as [equivalence],
	[schema].value('schema[1]/metadata[1]/@equivalentSuffix', 'nvarchar(max)') as [equivalentSuffix],
	[schema].value('schema[1]/metadata[1]/@equivalentRange', 'nvarchar(max)') as [equivalentRange]
FROM 
	_Schema;
GO
-- Anchor view --------------------------------------------------------------------------------------------------------
-- The anchor view shows information about all the anchors in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Anchor', 'V') IS NOT NULL
DROP VIEW [dbo].[_Anchor]
GO
CREATE VIEW [dbo].[_Anchor]
AS
SELECT
   S.version,
   S.activation,
   Nodeset.anchor.value('concat(@mnemonic, "_", @descriptor)', 'nvarchar(max)') as [name],
   Nodeset.anchor.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.anchor.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.anchor.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.anchor.value('@identity', 'nvarchar(max)') as [identity],
   Nodeset.anchor.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.anchor.value('count(attribute)', 'int') as [numberOfAttributes]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/anchor') as Nodeset(anchor);
GO
-- Knot view ----------------------------------------------------------------------------------------------------------
-- The knot view shows information about all the knots in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Knot', 'V') IS NOT NULL
DROP VIEW [dbo].[_Knot]
GO
CREATE VIEW [dbo].[_Knot]
AS
SELECT
   S.version,
   S.activation,
   Nodeset.knot.value('concat(@mnemonic, "_", @descriptor)', 'nvarchar(max)') as [name],
   Nodeset.knot.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.knot.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.knot.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.knot.value('@identity', 'nvarchar(max)') as [identity],
   Nodeset.knot.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.knot.value('@dataRange', 'nvarchar(max)') as [dataRange],
   isnull(Nodeset.knot.value('metadata[1]/@checksum', 'nvarchar(max)'), 'false') as [checksum],
   isnull(Nodeset.knot.value('metadata[1]/@equivalent', 'nvarchar(max)'), 'false') as [equivalent]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/knot') as Nodeset(knot);
GO
-- Attribute view -----------------------------------------------------------------------------------------------------
-- The attribute view shows information about all the attributes in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Attribute', 'V') IS NOT NULL
DROP VIEW [dbo].[_Attribute]
GO
CREATE VIEW [dbo].[_Attribute]
AS
SELECT
   S.version,
   S.activation,
   ParentNodeset.anchor.value('concat(@mnemonic, "_")', 'nvarchar(max)') +
   Nodeset.attribute.value('concat(@mnemonic, "_")', 'nvarchar(max)') +
   ParentNodeset.anchor.value('concat(@descriptor, "_")', 'nvarchar(max)') +
   Nodeset.attribute.value('@descriptor', 'nvarchar(max)') as [name],
   Nodeset.attribute.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.attribute.value('@mnemonic', 'nvarchar(max)') as [mnemonic],
   Nodeset.attribute.value('@descriptor', 'nvarchar(max)') as [descriptor],
   Nodeset.attribute.value('@identity', 'nvarchar(max)') as [identity],
   isnull(Nodeset.attribute.value('metadata[1]/@equivalent', 'nvarchar(max)'), 'false') as [equivalent],
   Nodeset.attribute.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.attribute.value('metadata[1]/@assertive', 'nvarchar(max)') as [assertive],
   isnull(Nodeset.attribute.value('metadata[1]/@checksum', 'nvarchar(max)'), 'false') as [checksum],
   Nodeset.attribute.value('metadata[1]/@restatable', 'nvarchar(max)') as [restatable],
   Nodeset.attribute.value('metadata[1]/@idempotent', 'nvarchar(max)') as [idempotent],
   ParentNodeset.anchor.value('@mnemonic', 'nvarchar(max)') as [anchorMnemonic],
   ParentNodeset.anchor.value('@descriptor', 'nvarchar(max)') as [anchorDescriptor],
   ParentNodeset.anchor.value('@identity', 'nvarchar(max)') as [anchorIdentity],
   Nodeset.attribute.value('@dataRange', 'nvarchar(max)') as [dataRange],
   Nodeset.attribute.value('@knotRange', 'nvarchar(max)') as [knotRange],
   Nodeset.attribute.value('@timeRange', 'nvarchar(max)') as [timeRange]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/anchor') as ParentNodeset(anchor)
OUTER APPLY
   ParentNodeset.anchor.nodes('attribute') as Nodeset(attribute);
GO
-- Tie view -----------------------------------------------------------------------------------------------------------
-- The tie view shows information about all the ties in a schema
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Tie', 'V') IS NOT NULL
DROP VIEW [dbo].[_Tie]
GO
CREATE VIEW [dbo].[_Tie]
AS
SELECT
   S.version,
   S.activation,
   REPLACE(Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"]
      return concat($role/@type, "_", $role/@role)
   ').value('.', 'nvarchar(max)'), ' ', '_') as [name],
   Nodeset.tie.value('metadata[1]/@capsule', 'nvarchar(max)') as [capsule],
   Nodeset.tie.value('count(anchorRole) + count(knotRole)', 'int') as [numberOfRoles],
   Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"]
      return string($role/@role)
   ').value('.', 'nvarchar(max)') as [roles],
   Nodeset.tie.value('count(anchorRole)', 'int') as [numberOfAnchors],
   Nodeset.tie.query('
      for $role in anchorRole
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [anchors],
   Nodeset.tie.value('count(knotRole)', 'int') as [numberOfKnots],
   Nodeset.tie.query('
      for $role in knotRole
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [knots],
   Nodeset.tie.value('count(*[local-name() = "anchorRole" or local-name() = "knotRole"][@identifier = "true"])', 'int') as [numberOfIdentifiers],
   Nodeset.tie.query('
      for $role in *[local-name() = "anchorRole" or local-name() = "knotRole"][@identifier = "true"]
      return string($role/@type)
   ').value('.', 'nvarchar(max)') as [identifiers],
   Nodeset.tie.value('@timeRange', 'nvarchar(max)') as [timeRange],
   Nodeset.tie.value('metadata[1]/@generator', 'nvarchar(max)') as [generator],
   Nodeset.tie.value('metadata[1]/@assertive', 'nvarchar(max)') as [assertive],
   Nodeset.tie.value('metadata[1]/@restatable', 'nvarchar(max)') as [restatable],
   Nodeset.tie.value('metadata[1]/@idempotent', 'nvarchar(max)') as [idempotent]
FROM
   [dbo].[_Schema] S
CROSS APPLY
   S.[schema].nodes('/schema/tie') as Nodeset(tie);
GO
-- Evolution function -------------------------------------------------------------------------------------------------
-- The evolution function shows what the schema looked like at the given
-- point in time with additional information about missing or added
-- modeling components since that time.
--
-- @timepoint The point in time to which you would like to travel.
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._Evolution', 'IF') IS NOT NULL
DROP FUNCTION [dbo].[_Evolution];
GO
CREATE FUNCTION [dbo].[_Evolution] (
    @timepoint AS DATETIME2(7)
)
RETURNS TABLE
RETURN
SELECT
   V.[version],
   ISNULL(S.[name], T.[name]) AS [name],
   ISNULL(V.[activation], T.[create_date]) AS [activation],
   CASE
      WHEN S.[name] is null THEN
         CASE
            WHEN T.[create_date] > (
               SELECT
                  ISNULL(MAX([activation]), @timepoint)
               FROM
                  [dbo].[_Schema]
               WHERE
                  [activation] <= @timepoint
            ) THEN 'Future'
            ELSE 'Past'
         END
      WHEN T.[name] is null THEN 'Missing'
      ELSE 'Present'
   END AS Existence
FROM (
   SELECT
      MAX([version]) as [version],
      MAX([activation]) as [activation]
   FROM
      [dbo].[_Schema]
   WHERE
      [activation] <= @timepoint
) V
JOIN (
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Anchor] a
   UNION ALL
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Knot] k
   UNION ALL
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Attribute] b
   UNION ALL
   SELECT
      [name],
      [version]
   FROM
      [dbo].[_Tie] t
) S
ON
   S.[version] = V.[version]
FULL OUTER JOIN (
   SELECT
      [name],
      [create_date]
   FROM
      sys.tables
   WHERE
      [type] like '%U%'
   AND
      LEFT([name], 1) <> '_'
) T
ON
   S.[name] = T.[name];
GO
-- Drop Script Generator ----------------------------------------------------------------------------------------------
-- generates a drop script, that must be run separately, dropping everything in an Anchor Modeled database
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._GenerateDropScript', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[_GenerateDropScript];
GO
CREATE PROCEDURE [dbo]._GenerateDropScript (
   @exclusionPattern varchar(42) = '[_]%', -- exclude Metadata by default
   @inclusionPattern varchar(42) = '%' -- include everything by default
)
AS
BEGIN
   DECLARE @xml XML;
   WITH objects AS (
      SELECT
         'DROP ' + ft.[type] + ' ' + fn.[name] + '; -- ' + fn.[description] as [statement],
         row_number() OVER (
            ORDER BY
               -- restatement finders last
               CASE dc.[description]
                  WHEN 'restatement finder' THEN 1
                  ELSE 0
               END ASC,
               -- order based on type
               CASE ft.[type]
                  WHEN 'PROCEDURE' THEN 1
                  WHEN 'FUNCTION' THEN 2
                  WHEN 'VIEW' THEN 3
                  WHEN 'TABLE' THEN 4
                  ELSE 5
               END ASC,
               -- order within type
               CASE dc.[description]
                  WHEN 'key generator' THEN 1
                  WHEN 'latest perspective' THEN 2
                  WHEN 'current perspective' THEN 3
                  WHEN 'difference perspective' THEN 4
                  WHEN 'point-in-time perspective' THEN 5
                  WHEN 'time traveler' THEN 6
                  WHEN 'rewinder' THEN 7
                  WHEN 'assembled view' THEN 8
                  WHEN 'annex table' THEN 9
                  WHEN 'posit table' THEN 10
                  WHEN 'table' THEN 11
                  WHEN 'restatement finder' THEN 12
                  ELSE 13
               END,
               -- order within description
               CASE ft.[type]
                  WHEN 'TABLE' THEN
                     CASE cl.[class]
                        WHEN 'Attribute' THEN 1
                        WHEN 'Attribute Annex' THEN 2
                        WHEN 'Attribute Posit' THEN 3
                        WHEN 'Tie' THEN 4
                        WHEN 'Anchor' THEN 5
                        WHEN 'Knot' THEN 6
                        ELSE 7
                     END
                  ELSE
                     CASE cl.[class]
                        WHEN 'Anchor' THEN 1
                        WHEN 'Attribute' THEN 2
                        WHEN 'Attribute Annex' THEN 3
                        WHEN 'Attribute Posit' THEN 4
                        WHEN 'Tie' THEN 5
                        WHEN 'Knot' THEN 6
                        ELSE 7
                     END
               END,
               -- finally alphabetically
               o.[name] ASC
         ) AS [ordinal]
      FROM
         sys.objects o
      JOIN
         sys.schemas s
      ON
         s.[schema_id] = o.[schema_id]
      AND
         s.[name] = 'dbo'
      CROSS APPLY (
         SELECT
            CASE
               WHEN o.[name] LIKE '[_]%'
               COLLATE Latin1_General_BIN THEN 'Metadata'
               WHEN o.[name] LIKE '%[A-Z][A-Z][_][a-z]%[A-Z][A-Z][_][a-z]%'
               COLLATE Latin1_General_BIN THEN 'Tie'
               WHEN o.[name] LIKE '%[A-Z][A-Z][_][A-Z][A-Z][A-Z][_][A-Z]%[_]%'
               COLLATE Latin1_General_BIN THEN 'Attribute'
               WHEN o.[name] LIKE '%[A-Z][A-Z][A-Z][_][A-Z]%'
               COLLATE Latin1_General_BIN THEN 'Knot'
               WHEN o.[name] LIKE '%[A-Z][A-Z][_][A-Z]%'
               COLLATE Latin1_General_BIN THEN 'Anchor'
               ELSE 'Other'
            END
      ) cl ([class])
      CROSS APPLY (
         SELECT
            CASE o.[type]
               WHEN 'P' THEN 'PROCEDURE'
               WHEN 'IF' THEN 'FUNCTION'
               WHEN 'FN' THEN 'FUNCTION'
               WHEN 'V' THEN 'VIEW'
               WHEN 'U' THEN 'TABLE'
            END
      ) ft ([type])
      CROSS APPLY (
         SELECT
            CASE
               WHEN ft.[type] = 'PROCEDURE' AND cl.[class] = 'Anchor' AND o.[name] LIKE 'k%'
               COLLATE Latin1_General_BIN THEN 'key generator'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 't%'
               COLLATE Latin1_General_BIN THEN 'time traveler'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'rf%'
               COLLATE Latin1_General_BIN THEN 'restatement finder'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'r%'
               COLLATE Latin1_General_BIN THEN 'rewinder'
               WHEN ft.[type] = 'VIEW' AND o.[name] LIKE 'l%'
               COLLATE Latin1_General_BIN THEN 'latest perspective'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'p%'
               COLLATE Latin1_General_BIN THEN 'point-in-time perspective'
               WHEN ft.[type] = 'VIEW' AND o.[name] LIKE 'n%'
               COLLATE Latin1_General_BIN THEN 'current perspective'
               WHEN ft.[type] = 'FUNCTION' AND o.[name] LIKE 'd%'
               COLLATE Latin1_General_BIN THEN 'difference perspective'
               WHEN ft.[type] = 'VIEW' AND cl.[class] = 'Attribute'
               COLLATE Latin1_General_BIN THEN 'assembled view'
               WHEN ft.[type] = 'TABLE' AND o.[name] LIKE '%Annex'
               COLLATE Latin1_General_BIN THEN 'annex table'
               WHEN ft.[type] = 'TABLE' AND o.[name] LIKE '%Posit'
               COLLATE Latin1_General_BIN THEN 'posit table'
               WHEN ft.[type] = 'TABLE'
               COLLATE Latin1_General_BIN THEN 'table'
               ELSE 'other'
            END
      ) dc ([description])
      CROSS APPLY (
         SELECT
            s.[name] + '.' + o.[name],
            cl.[class] + ' ' + dc.[description]
      ) fn ([name], [description])
      WHERE
         o.[type] IN ('P', 'IF', 'FN', 'V', 'U')
      AND
         o.[name] NOT LIKE ISNULL(@exclusionPattern, '')
      AND
         o.[name] LIKE ISNULL(@inclusionPattern, '%')
   )
   SELECT @xml = (
       SELECT
          [statement] + CHAR(13) as [text()]
       FROM
          objects
       ORDER BY
          [ordinal]
       FOR XML PATH('')
   );
   SELECT @xml;
END
GO
-- Database Copy Script Generator -------------------------------------------------------------------------------------
-- generates a copy script, that must be run separately, copying all data between two identically modeled databases
-----------------------------------------------------------------------------------------------------------------------
IF Object_ID('dbo._GenerateCopyScript', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[_GenerateCopyScript];
GO
CREATE PROCEDURE [dbo]._GenerateCopyScript (
	@source varchar(123),
	@target varchar(123)
)
as 
begin
	declare @R char(1) = CHAR(13);
	-- stores the built SQL code
	declare @sql varchar(max) = 'USE ' + @target + ';' + @R;
	declare @xml xml;
	-- find which version of the schema that is in effect
	declare @version int;
	select 
		@version = max([version]) 
	from
		_Schema;
	-- declare and set other variables we need
	declare @equivalentSuffix varchar(42);
	declare @identitySuffix varchar(42);
	declare @annexSuffix varchar(42);
	declare @positSuffix varchar(42);
	declare @temporalization varchar(42);
	select
		@equivalentSuffix = equivalentSuffix,
		@identitySuffix = identitySuffix,
		@annexSuffix = annexSuffix,
		@positSuffix = positSuffix,
		@temporalization = temporalization
	from
		_Schema_Expanded 
	where
		[version] = @version;
	-- build non-equivalent knot copy
	set @xml = (
		select 
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' ON;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' OFF;' + @R 
			end
		from 
			_Knot x
		cross apply (
			select stuff((
				select 
					', ' + [name]
				from
					sys.columns 
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		and
			isnull(equivalent, 'false') = 'false'
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build equivalent knot copy
	set @xml = (
		select 
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @identitySuffix + ' ON;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @identitySuffix + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @identitySuffix + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @identitySuffix + ' OFF;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @equivalentSuffix + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @equivalentSuffix + ';' + @R
		from 
			_Knot x
		cross apply (
			select stuff((
				select 
					', ' + [name]
				from
					sys.columns 
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		and
			isnull(equivalent, 'false') = 'true'
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build anchor copy
	set @xml = (
		select 
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' ON;' + @R 
			end,
			'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
			'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R,
			case
				when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + ' OFF;' + @R 
			end
		from 
			_Anchor x
		cross apply (
			select stuff((
				select 
					', ' + [name]
				from
					sys.columns 
				where
					[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
				and
					is_computed = 0
				for xml path('')
			), 1, 2, '')
		) c ([columns])
		where
			[version] = @version
		for xml path('')
	);
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build attribute copy
	if (@temporalization = 'crt')
	begin
		set @xml = (
			select 
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' ON;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @positSuffix + '(' + [positColumns] + ')' + @R +
				'SELECT ' + [positColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @positSuffix + ';' + @R,
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' OFF;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @annexSuffix + '(' + [annexColumns] + ')' + @R +
				'SELECT ' + [annexColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @annexSuffix + ';' + @R
			from 
				_Attribute x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @positSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) pc ([positColumns])
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @annexSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) ac ([annexColumns])
			where
				[version] = @version
			for xml path('')
		);
	end
	else -- uni
	begin
		set @xml = (
			select 
				'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
				'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R
			from 
				_Attribute x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) c ([columns])
			where
				[version] = @version
			for xml path('')
		);
	end
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	-- build tie copy
	if (@temporalization = 'crt')
	begin
		set @xml = (
			select 
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' ON;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @positSuffix + '(' + [positColumns] + ')' + @R +
				'SELECT ' + [positColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @positSuffix + ';' + @R,
				case
					when [generator] = 'true' then 'SET IDENTITY_INSERT ' + [capsule] + '.' + [name] + '_' + @positSuffix + ' OFF;' + @R 
				end,
				'INSERT INTO ' + [capsule] + '.' + [name] + '_' + @annexSuffix + '(' + [annexColumns] + ')' + @R +
				'SELECT ' + [annexColumns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + '_' + @annexSuffix + ';' + @R
			from 
				_Tie x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @positSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) pc ([positColumns])
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name] + '_' + @annexSuffix)
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) ac ([annexColumns])
			where
				[version] = @version
			for xml path('')
		);
	end
	else -- uni
	begin
		set @xml = (
			select 
				'INSERT INTO ' + [capsule] + '.' + [name] + '(' + [columns] + ')' + @R +
				'SELECT ' + [columns] + ' FROM ' + @source + '.' + [capsule] + '.' + [name] + ';' + @R
			from 
				_Tie x
			cross apply (
				select stuff((
					select 
						', ' + [name]
					from
						sys.columns 
					where
						[object_Id] = object_Id(x.[capsule] + '.' + x.[name])
					and
						is_computed = 0
					for xml path('')
				), 1, 2, '')
			) c ([columns])
			where
				[version] = @version
			for xml path('')
		);
	end
	set @sql = @sql + isnull(@xml.value('.', 'varchar(max)'), '');
	select @sql for xml path('');
end
-- DESCRIPTIONS -------------------------------------------------------------------------------------------------------
