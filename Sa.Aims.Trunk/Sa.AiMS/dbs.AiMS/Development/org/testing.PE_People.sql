﻿DECLARE @Xaction CHAR(36) = NEWID();

BEGIN TRANSACTION @Xaction;

DECLARE @Reliability	INT = 50;
DECLARE @Now			DATETIMEOFFSET(7) = '2015-04-23 07:00:00.00 -05:00';
DECLARE @PositedBy		INT;
EXECUTE @PositedBy		= [posit].[Return_PositorId];

DECLARE @PE_ID			INT;
DECLARE @PE_NME_ID		INT;

-----------------------------------------------------------------------------------------
--	Inital Load (07:00)

INSERT INTO [org].[PE_People]([PE_PositedBy], [PE_PositedAt])
VALUES(@PositedBy, @Now);

SET @PE_ID		= IDENT_CURRENT('[org].[PE_People]');

INSERT INTO [org].[PE_NME_People_Name_Posit]([PE_NME_PE_ID], [PE_NME_NameFirst], [PE_NME_NameLast], [PE_NME_ChangedAt])
VALUES(@PE_ID, 'Initial', 'Load', @Now)

SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, @Reliability);

-----------------------------------------------------------------------------------------
--	First Change (08:00)

SET @Now		= DATEADD(hh, 1, @Now);

INSERT INTO [org].[PE_NME_People_Name_Posit]([PE_NME_PE_ID], [PE_NME_NameFirst], [PE_NME_NameLast], [PE_NME_ChangedAt])
VALUES(@PE_ID, 'First', 'Change', @Now)

SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, @Reliability);

-----------------------------------------------------------------------------------------
--	Inadvertant Delete (08:30)

SET @Now		= DATEADD(n, 30, @Now);

SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, 0);

-----------------------------------------------------------------------------------------
--	Restore (09:00)

SET @Now		= DATEADD(n, 30, @Now);
SET @Reliability  = 1;

SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, @Reliability);

-----------------------------------------------------------------------------------------
--	Second Change (10:00)

SET @Now		= DATEADD(hh, 1, @Now);

INSERT INTO [org].[PE_NME_People_Name_Posit]([PE_NME_PE_ID], [PE_NME_NameFirst], [PE_NME_NameLast], [PE_NME_ChangedAt])
VALUES(@PE_ID, 'Second', 'Alteration', @Now)

SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, @Reliability);

-----------------------------------------------------------------------------------------
--	Confidence Change (10:30)

SET @Now		= DATEADD(n, 30, @Now);
SET @Reliability  = 75;

SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, @Reliability);

-----------------------------------------------------------------------------------------
--	Third Change (11:30)

SET @Now		= DATEADD(hh, 1, @Now);

INSERT INTO [org].[PE_NME_People_Name_Posit]([PE_NME_PE_ID], [PE_NME_NameFirst], [PE_NME_NameLast], [PE_NME_ChangedAt])
VALUES(@PE_ID, 'Third', 'Alteration', @Now)

SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, @Reliability);

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DECLARE @Temporal	TABLE
(
	[ID]		INT					NOT	NULL	IDENTITY(1, 1),
	[Detail]	VARCHAR(48)			NOT	NULL	CHECK([Detail] <> ''),
	[Now]		DATETIMEOFFSET(7)	NOT	NULL,
	PRIMARY KEY([ID] ASC),
	UNIQUE ([Detail] ASC),
	UNIQUE ([Now] ASC)
);

INSERT INTO @Temporal([Detail], [Now])
SELECT *
FROM (VALUES
	('- Before Inital Load (pa)', '2015-04-23 06:45:00.00 -05:00'),
	('+ After Inital Load (pa)', '2015-04-23 07:15:00.00 -05:00'),
	('+ After First Change (pa)', '2015-04-23 08:15:00.00 -05:00'),
	('- After Inadvertant Delete (a)', '2015-04-23 08:45:00.00 -05:00'),
	('+ After Restore (a)', '2015-04-23 09:15:00.00 -05:00'),
	('+ After Second Alteration (pa)', '2015-04-23 10:15:00.00 -05:00'),
	('+ After Second Alteration Confidence (a)', '2015-04-23 10:45:00.00 -05:00'),
	('+ After Third Alteration (pa)', '2015-04-23 11:45:00.00 -05:00')
	) x([Detail], [Now])

SELECT
	[t].[Detail],
	[t].[Now],
	[r].[PE_NME_NameFirst],
	[r].[PE_NME_NameLast],
	[r].[PE_NME_PositReliability],
	[r].[PE_NME_ChangedAt],
	[r].[PE_NME_PositedAt]
FROM @Temporal	[t]
LEFT OUTER JOIN
	(SELECT
		[c].[ID],
		[r].[PE_NME_NameFirst],
		[r].[PE_NME_NameLast],
		[r].[PE_NME_PositReliability],
		[r].[PE_NME_ChangedAt],
		[r].[PE_NME_PositedAt]
	FROM @Temporal [c]
	CROSS APPLY [org].[PE_People_Rewind]([c].[Now], [c].[Now], DEFAULT) [r]
	) [r]
	ON	[r].ID = [t].[ID]
ORDER BY [t].[Now];

--SELECT * FROM [org].[PE_NME_People_Name_Posit]	[p] ORDER BY [p].[PE_NME_ChangedAt];;
--SELECT * FROM [org].[PE_NME_People_Name_Annex]	[a] ORDER BY [a].[PE_NME_PositedAt];

--SELECT
--	[Schema]		= [s].[name],
--	[Table]			= [t].[name],
--	[Partitions]	= COUNT([p].[rows]),
--	[RowCount]		= SUM([p].[rows])
--FROM
--	sys.schemas		[s]
--INNER JOIN
--	sys.tables		[t]
--		ON	[t].[schema_id]	= [s].[schema_id]
--INNER JOIN
--	sys.partitions	[p]
--		ON	[p].[object_id]	= [t].[object_id]
--		AND	[p].[index_id]	= 1
--WHERE	[s].[name] = 'org'
--	AND	[t].[name] IN ('PE_People', 'PE_NME_People_Name_Posit', 'PE_NME_People_Name_Annex')
--GROUP BY
--	[s].[name],
--	[t].[name]
--ORDER BY
--	SUM([p].[rows])	DESC;


ROLLBACK TRANSACTION @Xaction;
GO

DECLARE @Xaction CHAR(36) = NEWID();

BEGIN TRANSACTION @Xaction;

DECLARE @Reliability	INT = 50;
DECLARE @Now			DATETIMEOFFSET(7) = '2015-04-23 07:00:00.00 -05:00';
DECLARE @PositedBy		INT;
EXECUTE @PositedBy		= [posit].[Return_PositorId];

DECLARE @PE_ID			INT;
DECLARE @PE_NME_ID		INT;

-----------------------------------------------------------------------------------------
--	Inital Load (07:00)

INSERT INTO [org].[PE_People_Current]([PE_PositedAt], [PE_NME_NameFirst], [PE_NME_NameLast], [PE_NME_ChangedAt], [PE_NME_PositedAt])
VALUES(@Now, 'Inital', 'Load', @Now, @Now);

-----------------------------------------------------------------------------------------
--	First Change (08:00)
SET @Now		= DATEADD(hh, 1, @Now);

UPDATE [org].[PE_People_Current]
SET
	[PE_NME_NameFirst] = 'First',
	[PE_NME_NameLast] = 'Change',
	[PE_NME_ChangedAt] = @Now,
	[PE_NME_PositedAt]	= @Now;

UPDATE [org].[PE_People_Current]
SET
	[PE_NME_PositReliability]	= 23,
	[PE_NME_PositedAt] = DATEADD(YYYY, -23, SYSDATETIMEOFFSET());

-----------------------------------------------------------------------------------------
--	Inadvertant Delete (08:30)

SET @Now		= DATEADD(n, 30, @Now);

--DELETE FROM [org].[PE_People_Current];
SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');
INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, 0);

-----------------------------------------------------------------------------------------
--	Restore (09:00)

SET @Now		= DATEADD(n, 30, @Now);
SET @PE_NME_ID	= IDENT_CURRENT('[org].[PE_NME_People_Name_Posit]');

INSERT INTO [org].[PE_NME_People_Name_Annex]([PE_NME_ID], [PE_NME_PositedBy], [PE_NME_PositedAt], [PE_NME_PositReliability])
VALUES(@PE_NME_ID, @PositedBy, @Now, 1);

-----------------------------------------------------------------------------------------
--	Second Change (10:00)

SET @Now		= DATEADD(hh, 1, @Now);

UPDATE [org].[PE_People_Current]
SET
	[PE_NME_NameFirst] = 'Second',
	[PE_NME_NameLast] = 'Alteration',
	[PE_NME_ChangedAt] = @Now,
	[PE_NME_PositedAt]	= @Now;

-----------------------------------------------------------------------------------------
--	Confidence Change (10:30)

SET @Now		= DATEADD(n, 30, @Now);

UPDATE [org].[PE_People_Current]
SET
	[PE_NME_PositReliability] = 75,
	[PE_NME_ChangedAt] = @Now,
	[PE_NME_PositedAt]	= @Now;

-----------------------------------------------------------------------------------------
--	Third Change (11:30)

SET @Now		= DATEADD(hh, 1, @Now);

UPDATE [org].[PE_People_Current]
SET
	[PE_NME_NameFirst] = 'Third',
	[PE_NME_NameLast] = 'Alteration',
	[PE_NME_ChangedAt] = @Now,
	[PE_NME_PositedAt]	= @Now;

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

DECLARE @Temporal	TABLE
(
	[ID]		INT					NOT	NULL	IDENTITY(1, 1),
	[Detail]	VARCHAR(48)			NOT	NULL	CHECK([Detail] <> ''),
	[Now]		DATETIMEOFFSET(7)	NOT	NULL,
	PRIMARY KEY([ID] ASC),
	UNIQUE ([Detail] ASC),
	UNIQUE ([Now] ASC)
);

INSERT INTO @Temporal([Detail], [Now])
SELECT *
FROM (VALUES
	('- Before Inital Load (pa)', '2015-04-23 06:45:00.00 -05:00'),
	('+ After Inital Load (pa)', '2015-04-23 07:15:00.00 -05:00'),
	('+ After First Change (pa)', '2015-04-23 08:15:00.00 -05:00'),
	('- After Inadvertant Delete (a)', '2015-04-23 08:45:00.00 -05:00'),
	('+ After Restore (a)', '2015-04-23 09:15:00.00 -05:00'),
	('+ After Second Alteration (pa)', '2015-04-23 10:15:00.00 -05:00'),
	('+ After Second Alteration Confidence (a)', '2015-04-23 10:45:00.00 -05:00'),
	('+ After Third Alteration (pa)', '2015-04-23 11:45:00.00 -05:00')
	) x([Detail], [Now])

SELECT
	[t].[Detail],
	[t].[Now],
	[r].[PE_NME_NameFirst],
	[r].[PE_NME_NameLast],
	[r].[PE_NME_PositReliability],
	[r].[PE_NME_ChangedAt],
	[r].[PE_NME_PositedAt]
FROM @Temporal	[t]
LEFT OUTER JOIN
	(SELECT
		[c].[ID],
		[r].[PE_NME_NameFirst],
		[r].[PE_NME_NameLast],
		[r].[PE_NME_PositReliability],
		[r].[PE_NME_ChangedAt],
		[r].[PE_NME_PositedAt]
	FROM @Temporal [c]
	CROSS APPLY [org].[PE_People_Rewind]([c].[Now], [c].[Now], DEFAULT) [r]
	) [r]
	ON	[r].ID = [t].[ID]
ORDER BY [t].[Now];

--SELECT * FROM [org].[PE_NME_People_Name_Posit]	[p] ORDER BY [p].[PE_NME_ChangedAt];;
--SELECT * FROM [org].[PE_NME_People_Name_Annex]	[a] ORDER BY [a].[PE_NME_PositedAt];

--SELECT
--	[Schema]		= [s].[name],
--	[Table]			= [t].[name],
--	[Partitions]	= COUNT([p].[rows]),
--	[RowCount]		= SUM([p].[rows])
--FROM
--	sys.schemas		[s]
--INNER JOIN
--	sys.tables		[t]
--		ON	[t].[schema_id]	= [s].[schema_id]
--INNER JOIN
--	sys.partitions	[p]
--		ON	[p].[object_id]	= [t].[object_id]
--		AND	[p].[index_id]	= 1
--WHERE	[s].[name] = 'org'
--	AND	[t].[name] IN ('PE_People', 'PE_NME_People_Name_Posit', 'PE_NME_People_Name_Annex')
--GROUP BY
--	[s].[name],
--	[t].[name]
--ORDER BY
--	SUM([p].[rows])	DESC;

ROLLBACK TRANSACTION @Xaction;
GO

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

SELECT * FROM [org].[PE_People]					[z];
SELECT * FROM [org].[PE_NME_People_Name_Posit]	[p];
SELECT * FROM [org].[PE_NME_People_Name_Annex]	[a];

SELECT [p].[PE_NME_ID] FROM [org].[PE_NME_People_Name_Posit_RW](DEFAULT) [p];
SELECT [p].[PE_NME_ID] FROM [org].[PE_NME_People_Name_Annex_RW](DEFAULT) [p];

SELECT * FROM [org].[PE_NME_People_Name_RW](DEFAULT, DEFAULT);
SELECT * FROM [org].[PE_NME_People_Name_Rewind](DEFAULT, DEFAULT, DEFAULT);
SELECT * FROM [org].[PE_People_Rewind](DEFAULT, DEFAULT, DEFAULT);

SELECT * FROM [org].[PE_People_Current]			[c];

--DELETE FROM [org].[PE_People_Current]


----UPDATE [org].[PE_People_Current]
----SET [PE_NME_NameMiddle] = NULL;

----SELECT * FROM [org].[PE_People_Rewind](DEFAULT, DEFAULT, DEFAULT)

----INSERT INTO [org].[PE_People_Current]([PE_NME_NameFirst], [PE_NME_NameLast])
----VALUES('Jon', 'Bowen');

--DELETE FROM [org].[PE_NME_People_Name_Annex];
--DELETE FROM [org].[PE_NME_People_Name_Posit];
--DELETE FROM [org].[PE_People];
