﻿CREATE FUNCTION [org].[PE_NME_People_Name_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[PE_NME_ID],
	[a].[PE_NME_PositedBy],
	[a].[PE_NME_PositedAt],
	[a].[PE_NME_PositReliability],
	[a].[PE_NME_PositReliable]

FROM
	[org].[PE_NME_People_Name_Annex]		[a]
WHERE
	[a].[PE_NME_PositedAt] < @PositedBefore;