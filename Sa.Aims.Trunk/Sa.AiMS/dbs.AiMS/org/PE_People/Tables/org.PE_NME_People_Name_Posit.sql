﻿CREATE TABLE [org].[PE_NME_People_Name_Posit]
(
	[PE_NME_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[PE_NME_PE_ID]				INT					NOT	NULL	CONSTRAINT [FK__PE_NME_People_Name_Posit_PE_NME_PE_ID]			REFERENCES [org].[PE_People]([PE_ID]),

	[PE_NME_NamePrefix]			NVARCHAR(8)				NULL	CONSTRAINT [CL__PE_NME_People_Name_Posit_PE_NME_NamePrefix]		CHECK([PE_NME_NamePrefix] <> ''),
	[PE_NME_NameFirst]			NVARCHAR(24)		NOT	NULL	CONSTRAINT [CL__PE_NME_People_Name_Posit_PE_NME_NameFirst]		CHECK([PE_NME_NameFirst] <> ''),
	[PE_NME_NameMiddle]			NVARCHAR(24)			NULL	CONSTRAINT [CL__PE_NME_People_Name_Posit_PE_NME_NameMiddle]		CHECK([PE_NME_NameMiddle] <> ''),
	[PE_NME_NameLast]			NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__PE_NME_People_Name_Posit_PE_NME_NameLast]		CHECK([PE_NME_NameLast] <> ''),
	[PE_NME_NameSuffix]			NVARCHAR(8)				NULL	CONSTRAINT [CL__PE_NME_People_Name_Posit_PE_NME_NameSuffix]		CHECK([PE_NME_NameSuffix] <> ''),
	[PE_NME_Title]				NVARCHAR(72)			NULL	CONSTRAINT [CL__PE_NME_People_Name_Posit_PE_NME_Title]			CHECK([PE_NME_Title] <> ''),
	[PE_NME_Checksum]			AS BINARY_CHECKSUM([PE_NME_NamePrefix], [PE_NME_NameFirst], [PE_NME_NameMiddle], [PE_NME_NameLast], [PE_NME_NameSuffix], [PE_NME_Title])
								PERSISTED			NOT	NULL,
	[PE_NME_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PE_NME_People_Name_Posit_ChangedAt]				DEFAULT(SYSDATETIMEOFFSET()),
	[PE_NME_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PE_NME_People_Name_Posit_RowGuid]				DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__PE_NME_People_Name_Posit_RowGuid]				UNIQUE NONCLUSTERED([PE_NME_RowGuid]),
	CONSTRAINT [PK__PE_NME_People_Name_Posit]		PRIMARY KEY CLUSTERED([PE_NME_ID] ASC)
		WITH (FILLFACTOR = 95),
	CONSTRAINT [UK__PE_NME_People_Name_Posit]		UNIQUE NONCLUSTERED([PE_NME_PE_ID] ASC, [PE_NME_ChangedAt] DESC, [PE_NME_Checksum] ASC)
		WITH (FILLFACTOR = 95)
);