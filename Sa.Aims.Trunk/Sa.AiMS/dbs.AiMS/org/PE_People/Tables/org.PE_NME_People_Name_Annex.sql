﻿CREATE TABLE [org].[PE_NME_People_Name_Annex]
(
	[PE_NME_ID]					INT					NOT	NULL	CONSTRAINT [FK__PE_NME_People_Name_Annex_PE_NME_ID]		REFERENCES [org].[PE_NME_People_Name_Posit]([PE_NME_ID]),

	[PE_NME_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__PE_NME_People_Name_Annex_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__PE_NME_People_Name_Annex_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[PE_NME_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__PE_NME_People_Name_Annex_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[PE_NME_PositReliability]	TINYINT				NOT	NULL	CONSTRAINT [DF__PE_NME_People_Name_Annex_PositRel]		DEFAULT(50),
	[PE_NME_PositReliable]		AS CONVERT(BIT, CASE WHEN [PE_NME_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[PE_NME_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__PE_NME_People_Name_Annex_RowGuid]		DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__PE_NME_People_Name_Annex_RowGuid]		UNIQUE NONCLUSTERED([PE_NME_RowGuid]),
	CONSTRAINT [PK__PE_NME_People_Name_Annex]		PRIMARY KEY CLUSTERED([PE_NME_ID] ASC, [PE_NME_PositedAt] DESC, [PE_NME_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);