﻿CREATE TABLE [org].[CN_PHN_Contact_Phone_Annex]
(
	[CN_PHN_ID]					INT					NOT	NULL	CONSTRAINT [FK__CN_PHN_Contact_Phone_Annex_CN_PHN_ID]		REFERENCES [org].[CN_PHN_Contact_Phone_Posit]([CN_PHN_ID]),

	[CN_PHN_PositedBy]			INT					NOT	NULL	CONSTRAINT [DF__CN_PHN_Contact_Phone_Annex_PositedBy]		DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__CN_PHN_Contact_Phone_Annex_PositedBy]		REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[CN_PHN_PositedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__CN_PHN_Contact_Phone_Annex_PositedAt]		DEFAULT(SYSDATETIMEOFFSET()),
	[CN_PHN_PositReliability]	TINYINT				NOT	NULL	CONSTRAINT [DF__CN_PHN_Contact_Phone_Annex_PositRel]		DEFAULT(50),
	[CN_PHN_PositReliable]		AS CONVERT(BIT, CASE WHEN [CN_PHN_PositReliability] > 0 THEN 1 ELSE 0 END)
								PERSISTED			NOT	NULL,
	[CN_PHN_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__CN_PHN_Contact_Phone_Annex_RowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
																CONSTRAINT [UX__CN_PHN_Contact_Phone_Annex_RowGuid]			UNIQUE NONCLUSTERED([CN_PHN_RowGuid]),
	CONSTRAINT [PK__CN_PHN_Contact_Phone_Annex]	PRIMARY KEY CLUSTERED([CN_PHN_ID] ASC, [CN_PHN_PositedAt] DESC, [CN_PHN_PositedBy] ASC)
		WITH (FILLFACTOR = 95)
);