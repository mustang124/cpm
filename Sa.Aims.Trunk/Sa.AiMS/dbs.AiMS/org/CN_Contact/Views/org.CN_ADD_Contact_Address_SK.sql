﻿CREATE VIEW [org].[CN_ADD_Contact_Address_SK]
WITH SCHEMABINDING
AS
SELECT
	[p].[CN_ADD_ID],
	[p].[CN_ADD_CN_ID],

	[p].[CN_ADD_Type_ID],
	[p].[CN_ADD_Country_ID],
	[p].[CN_ADD_State],
	[p].[CN_ADD_City],
	[p].[CN_ADD_PostalCode],
	[p].[CN_ADD_PoBox],
	[p].[CN_ADD_Address1],
	[p].[CN_ADD_Address2],
	[p].[CN_ADD_Address3],

	[p].[CN_ADD_Checksum],
	[p].[CN_ADD_ChangedAt],
	[a].[CN_ADD_PositedBy],
	[a].[CN_ADD_PositedAt],
	[a].[CN_ADD_PositReliability],
	[a].[CN_ADD_PositReliable]

FROM
	[org].[CN_ADD_Contact_Address_Posit]	[p]
INNER JOIN
	[org].[CN_ADD_Contact_Address_Annex]	[a]
		ON	[a].[CN_ADD_ID]		= [p].[CN_ADD_ID];
GO

CREATE UNIQUE CLUSTERED INDEX [PK__CN_ADD_PricingValue_SK]
ON [org].[CN_ADD_Contact_Address_SK]
(
	[CN_ADD_CN_ID]				ASC,
	[CN_ADD_ChangedAt]			DESC,
	[CN_ADD_PositedAt]			DESC,
	[CN_ADD_PositedBy]			ASC,
	[CN_ADD_PositReliable]		ASC
)
WITH (FILLFACTOR = 95);