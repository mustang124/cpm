﻿CREATE FUNCTION [org].[CN_EML_Contact_EMail_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[CN_EML_ID],
	[a].[CN_EML_PositedBy],
	[a].[CN_EML_PositedAt],
	[a].[CN_EML_PositReliability],
	[a].[CN_EML_PositReliable]
FROM
	[org].[CN_EML_Contact_EMail_Annex]		[a]
WHERE
	[a].[CN_EML_PositedAt] < @PositedBefore;