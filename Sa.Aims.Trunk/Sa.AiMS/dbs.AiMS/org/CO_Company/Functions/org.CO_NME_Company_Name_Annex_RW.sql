﻿CREATE FUNCTION [org].[CO_NME_Company_Name_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[CO_NME_ID],
	[a].[CO_NME_PositedBy],
	[a].[CO_NME_PositedAt],
	[a].[CO_NME_PositReliability],
	[a].[CO_NME_PositReliable]

FROM
	[org].[CO_NME_Company_Name_Annex]		[a]
WHERE
	[a].[CO_NME_PositedAt] < @PositedBefore;