﻿CREATE FUNCTION [org].[AS_GEO_Assets_Location_Annex_RW]
(
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[a].[AS_GEO_ID],
	[a].[AS_GEO_PositedBy],
	[a].[AS_GEO_PositedAt],
	[a].[AS_GEO_PositReliability],
	[a].[AS_GEO_PositReliable]
FROM
	[org].[AS_GEO_Assets_Location_Annex]		[a]
WHERE
	[a].[AS_GEO_PositedAt] < @PositedBefore;