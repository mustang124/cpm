﻿CREATE FUNCTION [org].[AS_NME_Assets_Name_Rewind]
(
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@Reliable		BIT					= 1
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[NME].[AS_NME_ID],
	[NME].[AS_NME_AS_ID],

	[NME].[AS_NME_AssetTag],
	[NME].[AS_NME_AssetName],
	[NME].[AS_NME_AssetDetail],

	[NME].[AS_NME_Checksum],
	[NME].[AS_NME_ChangedAt],
	[NME].[AS_NME_PositedBy],
	[NME].[AS_NME_PositedAt],
	[NME].[AS_NME_PositReliability],
	[NME].[AS_NME_PositReliable]

FROM
	[org].[AS_NME_Assets_Name_RW](@ChangedBefore, @PositedBefore)			[NME]

WHERE	[NME].[AS_NME_PositReliable]	= @Reliable
	AND	[NME].[AS_NME_ID]				= (
		SELECT TOP 1
			[NME_s].[AS_NME_ID]
		FROM
			[org].[AS_NME_Assets_Name_RW](@ChangedBefore, @PositedBefore)	[NME_s]
		WHERE
			[NME_s].[AS_NME_AS_ID]		= [NME].[AS_NME_AS_ID]
		ORDER BY
			[NME_s].[AS_NME_ChangedAt]	DESC,
			[NME_s].[AS_NME_PositedAt]	DESC
		);