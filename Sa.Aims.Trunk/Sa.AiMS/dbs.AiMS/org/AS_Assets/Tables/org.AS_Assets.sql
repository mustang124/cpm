﻿CREATE TABLE [org].[AS_Assets]
(
	[AS_ID]						INT					NOT	NULL	IDENTITY(1, 1),

	[AS_PositedBy]				INT					NOT	NULL	CONSTRAINT [DF__AS_Assets_PositedBy]	DEFAULT([posit].[Get_PositorId]())
																CONSTRAINT [FK__AS_Assets_PositedBy]	REFERENCES [posit].[PO_Positor]([PO_PositorId]),
	[AS_PositedAt]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AS_Assets_PositedAt]	DEFAULT(SYSDATETIMEOFFSET()),
	[AS_RowGuid]				UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AS_Assets_RowGuid]		DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__AS_Assets_RowGuid]		UNIQUE NONCLUSTERED([AS_RowGuid]),
	CONSTRAINT [PK__AS_Assets]	PRIMARY KEY CLUSTERED([AS_ID] ASC)
		WITH (FILLFACTOR = 95)
);