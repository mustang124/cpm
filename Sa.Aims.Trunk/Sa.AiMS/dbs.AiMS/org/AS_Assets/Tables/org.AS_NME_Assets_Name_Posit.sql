﻿CREATE TABLE [org].[AS_NME_Assets_Name_Posit]
(
	[AS_NME_ID]					INT					NOT	NULL	IDENTITY(1, 1),
	[AS_NME_AS_ID]				INT					NOT	NULL	CONSTRAINT [FK__AS_NME_Assets_Name_Posit_AS_NME_AS_ID]			REFERENCES [org].[AS_Assets]([AS_ID]),

	[AS_NME_AssetTag]			VARCHAR(12)			NOT	NULL	CONSTRAINT [CL__AS_NME_Assets_Name_Posit_AS_NME_AssetTag]		CHECK([AS_NME_AssetTag] <> ''),
	[AS_NME_AssetName]			NVARCHAR(24)		NOT	NULL	CONSTRAINT [CL__AS_NME_Assets_Name_Posit_AS_NME_AssetName]		CHECK([AS_NME_AssetName] <> ''),
	[AS_NME_AssetDetail]		NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__AS_NME_Assets_Name_Posit_AS_NME_AssetDetail]	CHECK([AS_NME_AssetDetail] <> ''),
	[AS_NME_Checksum]			AS BINARY_CHECKSUM([AS_NME_AssetTag], [AS_NME_AssetName], [AS_NME_AssetDetail])
								PERSISTED			NOT	NULL,
	[AS_NME_ChangedAt]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF__AS_NME_Assets_Name_Posit_ChangedAt]				DEFAULT(SYSDATETIMEOFFSET()),
	[AS_NME_RowGuid]			UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF__AS_NME_Assets_Name_Posit_RowGuid]				DEFAULT(NEWSEQUENTIALID()) ROWGUIDCOL,
																CONSTRAINT [UX__AS_NME_Assets_Name_Posit_RowGuid]				UNIQUE NONCLUSTERED([AS_NME_RowGuid]),
	CONSTRAINT [PK__AS_NME_Assets_Name_Posit]		PRIMARY KEY CLUSTERED([AS_NME_ID] ASC)
		WITH (FILLFACTOR = 95),
	CONSTRAINT [UK__AS_NME_Assets_Name_Posit]		UNIQUE NONCLUSTERED([AS_NME_AS_ID] ASC, [AS_NME_ChangedAt] DESC, [AS_NME_Checksum] ASC)
		WITH (FILLFACTOR = 95)
);