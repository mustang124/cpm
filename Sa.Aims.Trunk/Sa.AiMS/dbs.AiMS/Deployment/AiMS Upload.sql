﻿DECLARE @Org TABLE
(
	[Id]						INT					NOT	NULL	IDENTITY(1, 1),

	[CO_NME_CompanyTag]			VARCHAR(12)			NOT	NULL	CHECK([CO_NME_CompanyTag] <> ''),
	[CO_NME_CompanyName]		NVARCHAR(24)		NOT	NULL	CHECK([CO_NME_CompanyName] <> ''),
	[CO_NME_CompanyDetail]		NVARCHAR(48)		NOT	NULL	CHECK([CO_NME_CompanyDetail] <> ''),

	[PE_NME_NamePrefix]			NVARCHAR(8)				NULL	CHECK([PE_NME_NamePrefix] <> ''),
	[PE_NME_NameFirst]			NVARCHAR(24)		NOT	NULL	CHECK([PE_NME_NameFirst] <> ''),
	[PE_NME_NameMiddle]			NVARCHAR(24)			NULL	CHECK([PE_NME_NameMiddle] <> ''),
	[PE_NME_NameLast]			NVARCHAR(48)		NOT	NULL	CHECK([PE_NME_NameLast] <> ''),
	[PE_NME_NameSuffix]			NVARCHAR(8)				NULL	CHECK([PE_NME_NameSuffix] <> ''),
	[PE_NME_NameTitle]			NVARCHAR(72)			NULL	CHECK([PE_NME_NameTitle] <> ''),

	[CN_PHN_Type_ID]			INT					NOT	NULL,
	[CN_PHN_Number]				VARCHAR(32)			NOT	NULL	CHECK([CN_PHN_Number] <> ''),

	[CN_EML_Type_ID]			INT					NOT	NULL,
	[CN_EML_Address]			NVARCHAR(254)		NOT	NULL	CHECK([CN_EML_Address] <> ''),

	[CN_ADD_Type_ID]			INT						NULL,
	[CN_ADD_Country_ID]			INT					NOT	NULL,
	[CN_ADD_State]				NVARCHAR(24)			NULL	CHECK([CN_ADD_State] <> ''),
	[CN_ADD_City]				NVARCHAR(176)			NULL	CHECK([CN_ADD_City] <> ''),
	[CN_ADD_PostalCode]			NVARCHAR(24)			NULL	CHECK([CN_ADD_PostalCode] <> ''),
	[CN_ADD_PoBox]				NVARCHAR(16)			NULL	CHECK([CN_ADD_PoBox] <> ''),
	[CN_ADD_Address1]			NVARCHAR(256)			NULL	CHECK([CN_ADD_Address1] <> ''),
	[CN_ADD_Address2]			NVARCHAR(256)			NULL	CHECK([CN_ADD_Address2] <> ''),
	[CN_ADD_Address3]			NVARCHAR(256)			NULL	CHECK([CN_ADD_Address3] <> ''),

	[contact_type_id]			INT						NULL,

	PRIMARY KEY([Id] ASC),
	UNIQUE CLUSTERED([CO_NME_CompanyTag] ASC, [PE_NME_NameFirst] ASC, [PE_NME_NameLast])
);

SELECT DISTINCT
	---------------------------------------------------------------------------
	-- Company

	--[CO_NME_CompanyTag]		= [c].[CompanyID],
	--[CO_NME_CompanyName]	= [c].[CompanyName],
	--[CO_NME_CompanyDetail]	= [c].[CompanyDetail],

	--'-'[-],

	---------------------------------------------------------------------------
	-- People

	--[PE_NME_NamePrefix]		= CASE WHEN LEN([tx].[NameTitle]) <= 4
	--							THEN
	--								REPLACE([tx].[NameTitle] + '.', '..', '.')
	--							END,
	--[PE_NME_NameFirst]		= CASE WHEN CHARINDEX(' ', [tx].[NameFull]) - 1 > 0
	--							THEN
	--								LEFT([tx].[NameFull], CHARINDEX(' ', [tx].[NameFull]) - 1)
	--							END,
	--[PE_NME_NameLast]		= CASE WHEN CHARINDEX(' ', [tx].[NameFull]) - 1 > 0
	--							THEN
	--								RIGHT([tx].[NameFull], LEN([tx].[NameFull]) - CHARINDEX(' ', [tx].[NameFull]))
	--							END,
	--[PE_NME_Title]			= CASE WHEN LEN([tx].[NameTitle]) > 4
	--							THEN
	--								[tx].[NameTitle]
	--							END,

	--[txp].[CN_PHN_Type_ID],
	--[txp].[CN_PHN_PhoneNumber],

	--[CN_EML_Type_ID]			= CASE WHEN [txe].[eMail] IS NOT NULL THEN 'Primary' END,
	--[CN_EML_Address]			= [txe].[eMail],
	
	--[CN_ADD_Type_ID]			= 'Primary',
	--[CN_ADD_Country_ID]			= RTRIM(LTRIM(UPPER([txa].[AddressCountryId]))),
	--[CN_ADD_State]				= RTRIM(LTRIM([txa].[AddressState])),
	--[CN_ADD_City]				= RTRIM(LTRIM([txa].[AddressCity])),
	--[CN_ADD_PostalCode]			= RTRIM(LTRIM(UPPER([txa].[AddressPostalCode]))),
	--[CN_ADD_PoBox]				= RTRIM(LTRIM([txa].[AddressPOBox])),
	--[CN_ADD_Address1]			= RTRIM(LTRIM([txa].[AddressStreet])),

	--'-'[-],

	---------------------------------------------------------------------------
	-- Asset

	[AS_NME_AssetTag]			= [a].[AssetID],
	[AS_NME_AssetName]			= [a].[AssetName],
	[AS_NME_AssetDetail]		= [a].[AssetDetail],

	[AS_GEO_Longitude_Deg]		= [a].[Longitude_Deg],
	[AS_GEO_Latitude_Deg]		= [a].[Latitude_Deg],

	[CN_ADD_Type_ID]			= 'Plant',
	[CN_ADD_Country_ID]			= RTRIM(LTRIM(UPPER([a].[CountryId]))),
	[CN_ADD_State]				= RTRIM(LTRIM([a].[StateName])),

	--'-'[-],

	---------------------------------------------------------------------------
	-- Agreement

	--[AG_AGR_StudyYear]			= [ts].[_StudyYear],
	--[AG_AGR_StudyType]			= 'PCH',
	--[AG_AGR_AgreementTag]		= '',
	--[AG_AGR_AgreementName]		= '',
	--[AG_AGR_AgreementDetail]	= '',

	--[AG_CMP_CompanyName]		= [sc].[SubscriberCompanyName],
	--[AG_CMP_CompanyDetail]		= '',

	--[AG_ASS_AssetName]			= [sa].[SubscriberAssetName],
	--[AG_ASS_AssetDetail]		= '',
	--[AG_ASS_RegionEcon]			= '',

	--[AG_PSN_PersonRole]			= [tx].[ContactTypeId],

	--[AG_GRP_GroupName]			= '',
	--[AG_GRP_GroupDetail]		= '',

	--[AG_RPT_Uom]				= '',
	--[AG_RPT_CurrencyRpt]		= '',

	'-'[-]

FROM
	[fact].[TSortClient]				[tc]

LEFT OUTER JOIN
	[cons].[TSortSolomon]				[ts]
		ON	[ts].[Refnum]		= [tc].[Refnum]

LEFT OUTER JOIN
	[fact].[TSortContact]				[tx]
		ON	[tx].[Refnum]		= [tc].[Refnum]

LEFT OUTER JOIN
	[fact].[TSortContact]				[txa]
		ON	[txa].[Refnum]		= [tc].[Refnum]
		AND	[txa].[NameFull]	= [tx].[NameFull]
		AND	[txa].[AddressCountryId] IS NOT NULL
		AND	NOT ([txa].[AddressState] IS NULL AND [txa].[AddressCity] IS NULL AND [txa].[AddressStreet] IS NULL)

LEFT OUTER JOIN
	[fact].[TSortContact]				[txe]
		ON	[txe].[Refnum]		= [tc].[Refnum]
		AND	[txe].[NameFull]	= [tx].[NameFull]
		AND	[txe].[eMail]		IS NOT NULL

LEFT OUTER JOIN(
	SELECT
		[txpi].[Refnum],
		[txpi].[ContactTypeId],
		[txpi].[NameFull],
		[Fax]		= [txpi].[NumberFax],
		[Voice]		= [txpi].[NumberVoice],
		[Mobile]	= [txpi].[NumberMobile]
	FROM 
		[fact].[TSortContact] [txpi]
	) p
	UNPIVOT(
		[CN_PHN_PhoneNumber] FOR [CN_PHN_Type_ID] IN ([Fax], [Voice], [Mobile])
	) [txp]
		ON	[txp].[Refnum]		= [tc].[Refnum]
		AND	[txp].[NameFull]	= [tx].[NameFull]

LEFT OUTER JOIN
	[cons].[SubscriptionsAssets]		[sa]
		ON	[sa].[Refnum]		= [tc].[Refnum]

LEFT OUTER JOIN
	[dim].[Company_LookUp]				[c]
		ON	[c].[CompanyID]		= [sa].[CompanyID]

LEFT OUTER JOIN
	[cons].[SubscriptionsCompanies]		[sc]
		ON	[sc].[CompanyID]	= [sa].[CompanyID]
		AND	[sc].[StudyID]		= [sa].[StudyID]
		AND	[sc].[CalDateKey]	= [sa].[CalDateKey]
LEFT OUTER JOIN
	[cons].[Assets]						[a]
		ON	[a].[AssetID]		= [sa].[AssetID]
LEFT OUTER JOIN
	[ante].[CountryEconRegion]			[r]
		ON	[r].[FactorSetID]	= [ts].[_StudyYear]
		AND	[r].[CountryID]		= [a].[CountryID]
LEFT OUTER JOIN
	[ante].[CountryRegion_Geo]			[g]
		ON	[g].[CountryId]		= [a].[CountryID]
LEFT OUTER JOIN
	[dim].[Country_LookUp]				[cl]
		ON	[cl].[CountryID]	= [a].[CountryID]

WHERE  ([tc].[Refnum] LIKE '20[0-9][0-9]PCH[0-9][0-9]%'
	OR	[tc].[Refnum] LIKE '20[0-9][0-9]SEEC[0-9][0-9]%');

