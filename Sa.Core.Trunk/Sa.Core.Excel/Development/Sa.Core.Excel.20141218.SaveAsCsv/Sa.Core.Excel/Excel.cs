﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace Sa.Core
{
	public partial class SaExcel
    {
		//	TODO: Create Excel objects (Application, Workbook, Worksheet) that implement IDisposable

		public static Excel.Application NewExcelApplication(bool ui = false)
		{
			Excel.Application xla = new Excel.Application();

			xla.AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable;
			xla.AskToUpdateLinks = ui;
			xla.DisplayAlerts = ui;
			xla.FileValidation = Microsoft.Office.Core.MsoFileValidationMode.msoFileValidationSkip;
			xla.Interactive = ui;
			xla.ScreenUpdating = ui;
			xla.Visible = ui;
			xla.EnableEvents = ui;

			return xla;
		}

		public static Excel.Application CurrentExcelApplication(bool ui = false)
		{
			Excel.Application xla;

			try
			{
				xla = (Excel.Application)Marshal.GetActiveObject("Excel.Application");
			}
			catch (COMException)
			{
				xla = NewExcelApplication(ui);
			}

			return xla;
		}

		public static Excel.Workbook OpenWorkbook_ReadOnly(Excel.Application xla, string PathFile)
		{
			PathFile = System.IO.Path.GetFullPath(PathFile);
			Excel.Workbook wkb = xla.Workbooks.Open(PathFile, Excel.XlUpdateLinks.xlUpdateLinksNever, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, false, Type.Missing, Excel.XlCorruptLoad.xlNormalLoad);
			wkb.EnableAutoRecover = false;

			return wkb;
		}

		public static void WorkbookSaveCopyAs(Excel.Workbook wkb, string path)
		{
			wkb.SaveAs(path);
		}

		#region Close and Release Objects

		public static void CloseWorkbook(ref Excel.Workbook wkb)
		{
			if (wkb != null)
			{
				try
				{
					wkb.Close(false);
					Marshal.FinalReleaseComObject(wkb);
				}
				finally
				{
					wkb = null;
				}
			}

			System.GC.WaitForPendingFinalizers();
			System.GC.Collect();
			System.GC.WaitForFullGCComplete();
		}

		public static void CloseApplication(ref Excel.Application xla)
		{
			if (xla != null)
			{
				try
				{
					xla.Quit();
					Marshal.FinalReleaseComObject(xla);
				}
				finally
				{
					xla = null;
				}
			}

			System.GC.WaitForPendingFinalizers();
			System.GC.Collect();
			System.GC.WaitForFullGCComplete();
		}

		public static void CloseExcel(ref Excel.Workbook wkb, ref Excel.Application xla)
		{
			CloseWorkbook(ref wkb);
			CloseApplication(ref xla);
		}

		#endregion
    }
}