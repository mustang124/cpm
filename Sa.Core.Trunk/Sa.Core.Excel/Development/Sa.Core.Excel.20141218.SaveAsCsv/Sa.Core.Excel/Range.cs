﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Core
{
	public partial class SaExcel
	{
		public static bool RangeHasValue(Excel.Range rng)
		{
			string s = string.Empty;

			foreach (Excel.Range r in rng.Cells)
			{
				s = Convert.ToString(r.Text).Trim();

				if (s.Length > 0) s = Convert.ToString(r.Value2).Trim();

				if (s.Length > 0) return true;
			}
			return false;
		}

		public static void CopyRangeValues(Excel.Workbook wkbSource, Excel.Workbook wkbTarget, List<string[]> range)
		{
			Excel.Worksheet wksSource;
			Excel.Worksheet wksTarget;

			Excel.Range rngSource;
			Excel.Range rngTarget;

			foreach (string[] r in range)
			{
				wksSource = wkbSource.Worksheets[r[0]];
				rngSource = wksSource.Range[r[1]];

				wksTarget = wkbTarget.Worksheets[r[0]];
				rngTarget = wksTarget.Range[r[1]];

				SaExcel.CopyRangeValues(rngSource, rngTarget);
			}

			wkbTarget.Parent.CalculateFullRebuild();
		}

		public static void CopyRangeValues(Excel.Range source, Excel.Range target)
		{
			target.Value2 = source.Value2;
		}

		#region Return Functions

		public static string ReturnAddress(Excel.Range rng)
		{
			return rng.AddressLocal.Replace("$", "");
		}

		public static string ReturnString(Excel.Range rng)
		{
			try
			{
				return Convert.ToString(rng.Value2).Trim();
			}
			catch
			{
				return string.Empty;
			}
		}

		public static string ReturnString(Excel.Range rng, UInt32 FieldLength)
		{
			try
			{
				string t = Convert.ToString(rng.Value2).Trim();
				return t.Substring(0, Math.Min(t.Length, (int)FieldLength));
			}
			catch
			{
				return string.Empty;
			}
		}

		public static Single ReturnSingle(Excel.Range rng)
		{
			try
			{
				return (Single)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return Single.Parse(s);
			}
		}

		public static Single ReturnSingle(Excel.Range rng, Single val)
		{
			if (Single.TryParse(rng.Value2, out val))
			{
				return val;
			}
			else
			{
				return val;
			}
		}

		public static double ReturnDouble(Excel.Range rng)
		{
			try
			{
				return (double)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return double.Parse(s);
			}
		}

		public static double ReturnDouble(Excel.Range rng, double val)
		{
			if (double.TryParse(rng.Value2, out val))
			{
				return val;
			}
			else
			{
				return val;
			}
		}

		public static DateTime ReturnDateTime(Excel.Range rng)
		{
			try
			{
				return (DateTime)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value).Trim();
				return DateTime.Parse(s);
			}
		}

		public static byte ReturnByte(Excel.Range rng, bool ConvertTrueFalse = false)
		{
			if (ConvertTrueFalse)
			{
				byte b = 0;

				string s = Convert.ToString(rng.Value2).Substring(0, 1).ToUpper();

				if (s == "T" || s == "X" || s == "Y" || s == "1" || s == "-") b = 1;

				return b;
			}
			else
			{
				try
				{
					return (byte)rng.Value2;
				}
				catch
				{
					string s = Convert.ToString(rng.Value2).Trim();
					return byte.Parse(s);
				}
			}
		}

		public static ushort ReturnUShort(Excel.Range rng)
		{
			try
			{
				return (ushort)rng.Value2;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return ushort.Parse(s);
			}
		}

		public static short ReturnInt16(Excel.Range rng)
		{
			try
			{
				return (short)rng.Value2;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return short.Parse(s);
			}
		}

		public static int ReturnInt32(Excel.Range rng)
		{
			try
			{
				return (int)rng.Value2;
			}
			catch
			{
				string s = Convert.ToString(rng.Value2).Trim();
				return int.Parse(s);
			}
		}

		#endregion
	}
}