﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Vbe.Interop;

namespace Sa.Core
{
	public partial class SaExcel
	{
		public static void RemoveVBProject(Excel.Workbook wkb)
		{
			VBProject project = wkb.VBProject;

			foreach (VBComponent component in project.VBComponents)
			{
				try
				{
					project.VBComponents.Remove(component);
				}
				catch (ArgumentException)
				{
					continue;
				}
			}

			foreach (VBComponent component in project.VBComponents)
			{
				component.CodeModule.DeleteLines(1, component.CodeModule.CountOfLines);
			}
		}

		[Obsolete("Does not function as expected, please manually delete the legacy code", false)]
		public static void RemoveExcel4MacroSheets(Excel.Workbook wkb)
		{
			foreach (Excel.Worksheet sht in wkb.Excel4MacroSheets)
			{
				sht.Delete();
			}

			foreach (Excel.Worksheet sht in wkb.Excel4IntlMacroSheets)
			{
				sht.Delete();
			}
		}
	}
}