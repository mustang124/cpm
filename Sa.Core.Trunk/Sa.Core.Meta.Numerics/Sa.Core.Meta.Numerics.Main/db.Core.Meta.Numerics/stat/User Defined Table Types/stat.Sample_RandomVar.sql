﻿CREATE TYPE [stat].[Sample_RandomVar] AS TABLE 
(
	[Id]				INT					NOT	NULL	IDENTITY(1, 1),
	[RowId]				INT					NOT	NULL,

	[x]					FLOAT				NOT	NULL,						--	Independant variable
	[w]					FLOAT					NULL,						--	Weighting factor

	UNIQUE CLUSTERED([x] ASC, [w] ASC, [Id] ASC),
	UNIQUE NONCLUSTERED([RowId] ASC),

	PRIMARY KEY NONCLUSTERED([Id] ASC)
);