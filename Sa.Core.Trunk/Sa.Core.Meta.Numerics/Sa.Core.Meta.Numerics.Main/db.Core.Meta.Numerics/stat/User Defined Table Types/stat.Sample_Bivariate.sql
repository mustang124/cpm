﻿CREATE TYPE [stat].[Sample_Bivariate] AS TABLE
(
	[Id]				INT					NOT	NULL	IDENTITY(1, 1),
	[RowId]				INT					NOT	NULL,

	[x]					FLOAT				NOT	NULL,						--	Independant variable
	[y]					FLOAT				NOT	NULL,						--	Dependant variable
	[w]					FLOAT					NULL,						--	Weighting factor

	UNIQUE CLUSTERED([x] ASC, [y] ASC, [w] ASC, [Id] ASC),
	UNIQUE NONCLUSTERED([RowId] ASC),

	PRIMARY KEY NONCLUSTERED([Id] ASC)
);