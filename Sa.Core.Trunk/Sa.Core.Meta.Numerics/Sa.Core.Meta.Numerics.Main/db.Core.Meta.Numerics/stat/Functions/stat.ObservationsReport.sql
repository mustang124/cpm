﻿CREATE FUNCTION [stat].[ObservationsReport]
(
	@SampleBasis			[stat].[Sample_RandomVar]				READONLY,					--	Sample population
	@SampleAudit			[stat].[Sample_RandomVar]				READONLY,					--	Data points to examine
	@ThresholdResidual		FLOAT = 2.0,
	@ThresholdPearson		FLOAT = 0.90
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT

	[o].[Id],
	[o].[x],
	
	[ResidualStdIn]		= CASE WHEN		ABS([o].[ResidualStandard]) < @ThresholdResidual	THEN [o].[x] END,
	[ResidualStdOut]	= CASE WHEN NOT(ABS([o].[ResidualStandard]) < @ThresholdResidual)	THEN [o].[x] END,
	
	[ConfidenceIn]		= CASE WHEN		[o].[x] > [d].[iConfidenceLower] AND [o].[x] < [d].[iConfidenceUpper]	THEN [o].[x] END,
	[ConfidenceOut]		= CASE WHEN NOT([o].[x] > [d].[iConfidenceLower] AND [o].[x] < [d].[iConfidenceUpper])	THEN [o].[x] END,
	
	[PredictionIn]		= CASE WHEN		[o].[x] > [d].[iPredictionLower] AND [o].[x] < [d].[iPredictionUpper]	THEN [o].[x] END,
	[PredictionOut]		= CASE WHEN NOT([o].[x] > [d].[iPredictionLower] AND [o].[x] < [d].[iPredictionUpper])	THEN [o].[x] END,
	
	[PearsonIn]			= CASE WHEN		[o].[ResidualAbsolute] < @ThresholdPearson AND [o].[ResidualRelative] < @ThresholdPearson	THEN [o].[x] END,
	[PearsonOut]		= CASE WHEN	NOT([o].[ResidualAbsolute] < @ThresholdPearson AND [o].[ResidualRelative] < @ThresholdPearson)	THEN [o].[x] END,

	[Q1]				= CASE WHEN [o].[xQuartile] = 1 THEN [o].[x] END,
	[Q2]				= CASE WHEN [o].[xQuartile] = 2 THEN [o].[x] END,
	[Q3]				= CASE WHEN [o].[xQuartile] = 3 THEN [o].[x] END,
	[Q4]				= CASE WHEN [o].[xQuartile] = 4 THEN [o].[x] END,

	[Z1]				= CASE WHEN [o].[zQuartile] = 1 THEN [o].[x] END,
	[Z2]				= CASE WHEN [o].[zQuartile] = 2 THEN [o].[x] END,
	[Z3]				= CASE WHEN [o].[zQuartile] = 3 THEN [o].[x] END,
	[Z4]				= CASE WHEN [o].[zQuartile] = 4 THEN [o].[x] END

FROM	[stat].[Observations](@SampleBasis, @SampleAudit)	[o]
CROSS JOIN [stat].[Descriptive](@SampleBasis)				[d]
);