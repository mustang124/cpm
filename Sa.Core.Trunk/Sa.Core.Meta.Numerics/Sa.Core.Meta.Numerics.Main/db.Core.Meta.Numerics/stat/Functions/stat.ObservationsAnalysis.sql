﻿CREATE FUNCTION [stat].[ObservationsAnalysis]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics

	@SampleBasis			[stat].[Sample_RandomVar]				READONLY,					--	Sample population
	@SampleAudit			[stat].[Sample_RandomVar]				READONLY,					--	Data points to examine

	@SortOrder				SMALLINT			= 1,											--	Sort order of data 1 (Ascending), -1 Descending
	@Tiles					TINYINT				= 4												--	Number of nTiles
)
RETURNS @ReturnTable TABLE
(
	[Id]					INT					NOT	NULL,
	[x]						FLOAT				NOT	NULL,										--	Raw value

	[xPcntTile]				FLOAT				NOT	NULL,										--	Item-tile							http://en.wikipedia.org/wiki/Percentile
	[xPcntRank]				FLOAT				NOT	NULL,										--	Percent Rank						http://en.wikipedia.org/wiki/Percentile
	[xCumeDist]				FLOAT				NOT	NULL,										--	Cumulative distribution				http://en.wikipedia.org/wiki/Percentile
	[xQuartile]				TINYINT				NOT	NULL	CHECK([xQuartile]	>= 1),			--	Quartile value						http://en.wikipedia.org/wiki/Quantile
	[xRank]					INT					NOT	NULL	CHECK([xRank]		>= 1),			--	Value Ranking						http://msdn.microsoft.com/en-us/library/ms176102.aspx
	[xRankDense]			INT					NOT	NULL	CHECK([xRankDense]	>= 1),			--	Value Ranking (Dense)				http://msdn.microsoft.com/en-us/library/ms173825.aspx

	[zPcntTile]				FLOAT				NOT	NULL,										--	Percentile							http://en.wikipedia.org/wiki/Percentile
	[zScore]				FLOAT				NOT	NULL,										--	Distance Away From Mean				http://en.wikipedia.org/wiki/Z-test
	[zQuartile]				TINYINT				NOT	NULL	CHECK([zQuartile]	>= 1),			--	Quartile for Z-Test
	[zHypothesis]			BIT					NOT	NULL,										--	Null Hypothesis 0 (Rejection), 1 (Acceptance)

	[ErrorAbsolute]			FLOAT				NOT	NULL,										--	Absolute approximation error		http://en.wikipedia.org/wiki/Approximation_error
	[ErrorRelative]			FLOAT					NULL,										--	Realitve approximation error		http://en.wikipedia.org/wiki/Approximation_error

	[ResidualStandard]		FLOAT					NULL,										--	Standard residual error				http://en.wikipedia.org/wiki/Errors_and_residuals_in_statistics
	[ResidualAbsolute]		FLOAT				NOT	NULL,
	[ResidualRelative]		FLOAT				NOT	NULL,

	PRIMARY KEY CLUSTERED ([x] ASC, [Id] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Order			FLOAT	= CAST(ABS(@SortOrder) / @SortOrder AS FLOAT);
	DECLARE	@nTiles			TINYINT	= COALESCE(@Tiles, 4);

	INSERT INTO @ReturnTable
	(
		[Id], [x],
		[xPcntTile], [xPcntRank], [xCumeDist], [xQuartile], [xRank], [xRankDense],
		[zPcntTile], [zScore], [zQuartile], [zHypothesis],
		[ErrorAbsolute], [ErrorRelative],
		[ResidualStandard], [ResidualAbsolute], [ResidualRelative]
	)
	SELECT
		[o].[Id],
		[o].[x],

		[xPcntTile]			= (SELECT [v]
								FROM
									(SELECT [u].[Id], [u].[Pop], [v] = (ROW_NUMBER()	OVER(ORDER BY [u].[x] * @Order ASC, [u].[Id] ASC) - 0.5) / CONVERT(FLOAT,  COUNT([u].[Id]) OVER())
										FROM (
											SELECT	ALL [Pop] = 'Basis', [b].[Id], [b].[x] FROM @SampleBasis [b]
											UNION	ALL
											SELECT	ALL [Pop] = 'Audit', [a].[Id], [a].[x] FROM @SampleAudit [a]
											) [u]
									) [s]
									WHERE [s].[Id] = [o].[Id] AND [s].[Pop] = 'Audit'
								),

		[xPcntRank]			= (SELECT [v]
								FROM
									(SELECT [u].[Id], [u].[Pop], [v] = PERCENT_RANK()	OVER(ORDER BY [u].[x] * @Order ASC, [u].[Id] ASC)
										FROM (
											SELECT	ALL [Pop] = 'Basis', [b].[Id], [b].[x] FROM @SampleBasis [b]
											UNION	ALL
											SELECT	ALL [Pop] = 'Audit', [a].[Id], [a].[x] FROM @SampleAudit [a]
											) [u]
									) [s]
									WHERE [s].[Id] = [o].[Id] AND [s].[Pop] = 'Audit'
								),

		[xCumeDist]			= (SELECT [v]
								FROM
									(SELECT [u].[Id], [u].[Pop], [v] = CUME_DIST()		OVER(ORDER BY [u].[x] * @Order ASC, [u].[Id] ASC)
										FROM (
											SELECT	ALL [Pop] = 'Basis', [b].[Id], [b].[x] FROM @SampleBasis [b]
											UNION	ALL
											SELECT	ALL [Pop] = 'Audit', [a].[Id], [a].[x] FROM @SampleAudit [a]
											) [u]
									) [s]
									WHERE [s].[Id] = [o].[Id] AND [s].[Pop] = 'Audit'
								),

		[xQuartile]			= (SELECT [v]
								FROM
									(SELECT [u].[Id], [u].[Pop], [v] = NTILE(@nTiles)	OVER(ORDER BY [u].[x] * @Order ASC, [u].[Id] ASC)
										FROM (
											SELECT	ALL [Pop] = 'Basis', [b].[Id], [b].[x] FROM @SampleBasis [b]
											UNION	ALL
											SELECT	ALL [Pop] = 'Audit', [o].[Id], [o].[x]
											) [u]
									) [s]
									WHERE [s].[Id] = [o].[Id] AND [s].[Pop] = 'Audit'
								),

		[xRank]				= (SELECT [v]
								FROM
									(SELECT [u].[Id], [u].[Pop], [v] = RANK()			OVER(ORDER BY [u].[x] * @Order ASC, [u].[Id] ASC)
										FROM (
											SELECT	ALL [Pop] = 'Basis', [b].[Id], [b].[x] FROM @SampleBasis [b]
											UNION	ALL
											SELECT	ALL [Pop] = 'Audit', [o].[Id], [o].[x]
											) [u]
									) [s]
									WHERE [s].[Id] = [o].[Id] AND [s].[Pop] = 'Audit'
								),

		[xRankDense]		= (SELECT [v]
								FROM
									(SELECT [u].[Id], [u].[Pop], [v] = DENSE_RANK()		OVER(ORDER BY [u].[x] * @Order ASC, [u].[Id] ASC)
										FROM (
											SELECT	ALL [Pop] = 'Basis', [b].[Id], [b].[x] FROM @SampleBasis [b]
											UNION	ALL
											SELECT	ALL [Pop] = 'Audit', [o].[Id], [o].[x]
											) [u]
									) [s]
									WHERE [s].[Id] = [o].[Id] AND [s].[Pop] = 'Audit'
								),

		[zPcntTile]			= [stat].[Student_Probability](CONVERT(FLOAT, [d].[xCount]), CASE WHEN ([d].[xStDev] <> 0.0) THEN ([o].[x] - [d].[xMean]) / [d].[xStDev] ELSE 0.0 END, 1),
		[zScore]			= CASE WHEN ([d].[xStDev] <> 0.0) THEN ([o].[x] - [d].[xMean]) / [d].[xStDev] ELSE 0.0 END,
		[zQuartile]			= (SELECT [v]
								FROM
									(SELECT [u].[Id], [u].[Pop], [v] = NTILE(4) OVER(ORDER BY ABS(CASE WHEN [d].[xStDev] <> 0.0 THEN ([u].[x] - [d].[xMean]) / [d].[xStDev] END) ASC)
										FROM (
											SELECT	ALL [Pop] = 'Basis', [b].[Id], [b].[x] FROM @SampleBasis [b]
											UNION	ALL
											SELECT	ALL [Pop] = 'Audit', [o].[Id], [o].[x]
											) [u]
									) [s]
									WHERE [s].[Id] = [o].[Id] AND [s].[Pop] = 'Audit'
								),

		[zHypothesis]		= CASE WHEN ((CASE WHEN ([d].[xStDev] <> 0.0) THEN ABS([o].[x] - [d].[xMean]) / [d].[xStDev] ELSE 0.0 END) < [d].[tStat])
								THEN 1
								ELSE 0
								END,

		[ErrorAbsolute]		= ([o].[x] - [d].[xMean]),
		[ErrorRelative]		= CASE WHEN ([d].[xMean] <> 0.0) THEN ([o].[x] - [d].[xMean]) / [d].[xMean] END,

		[ResidualStandard]	= CASE WHEN ([d].[xSumXiX] <> 0.0 AND SQRT(SQUARE([d].[xStDev]) * (1.0 - 1.0 / CONVERT(FLOAT, [d].[xCount]) - SQUARE([o].[x] - [d].[xMean]) / [d].[xSumXiX])) <> 0.0)
								THEN
									([o].[x] - [d].[xMean]) /
									SQRT(SQUARE([d].[xStDev]) * (1.0 - 1.0 / CONVERT(FLOAT, [d].[xCount]) - SQUARE([o].[x] - [d].[xMean]) / [d].[xSumXiX]))
								END,

		[ResidualAbsolute]	= (SELECT COUNT(1) FROM @SampleBasis [b]
									WHERE	SQUARE([b].[x] - [d].[xMean])
										<=	SQUARE([o].[x] - [d].[xMean])
									) / CONVERT(FLOAT, [d].[xCount]),

		[ResidualRelative]	= (SELECT COUNT(1) FROM @SampleBasis [b]
									WHERE	CASE WHEN [b].[x] <> 0.0 THEN SQUARE(([b].[x] - [d].[xMean]) / [b].[x]) ELSE 1.0 END
										<=	CASE WHEN [o].[x] <> 0.0 THEN SQUARE(([o].[x] - [d].[xMean]) / [o].[x]) ELSE 1.0 END
									) / CONVERT(FLOAT, [d].[xCount])

	FROM [stat].[Descriptive](@SampleBasis)	[d]
	CROSS JOIN @SampleAudit					[o];

	RETURN;

END;