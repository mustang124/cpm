﻿CREATE FUNCTION [stat].[DescriptiveArithmetic]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics

	@Sample					[stat].[Sample_RandomVar]	READONLY,								--	Data points to examine
	@Confidence_Pcnt		FLOAT				= 0.90,											--	Expected confidence/prediction interval percent	[0, 1)
	@SortOrder				SMALLINT			= 1,											--	Sort order
	@Tiles					TINYINT				= 4,											--	Number of nTiles
	@CapMin					FLOAT				= NULL,											--	Minimum reporting values to average
	@CapMax					FLOAT				= NULL,											--	Maximum reporting values to average
	@EndPointMean			TINYINT				= 2												--	Number of endpoints to average
)
RETURNS @ReturnTable TABLE
(
	[qTile]					TINYINT				NOT	NULL	CHECK([qTile]	>= 1),				--	Quartile value
	[SampleType]			VARCHAR(10)			NOT	NULL,

	[xCount]				SMALLINT			NOT	NULL	CHECK([xCount]	>= 1),				--	Item Count
	[xMin]					FLOAT				NOT	NULL,										--	Minimum
	[xMax]					FLOAT				NOT	NULL,										--	Maximum
	[xRange]				FLOAT				NOT	NULL,										--	Range

	[BreakPrevious]			FLOAT					NULL,
	[BreakNext]				FLOAT					NULL,

	[xMean]					FLOAT				NOT	NULL,										--	Mean								http://en.[w]ikipedia.org/wiki/Mean
	[xStDev]				FLOAT				NOT	NULL	CHECK([xStDev]	>= 0.0),			--	Standard Deviation			σ		http://en.[w]ikipedia.org/wiki/Standard_deviation
	[xVar]					FLOAT				NOT	NULL	CHECK([xVar]	>= 0.0),			--	Variance					σ²		http://en.[w]ikipedia.org/wiki/Variance

	[xErrorStandard]		FLOAT				NOT	NULL,										--	Standard Error						http://en.[w]ikipedia.org/wiki/Z-test
	[xCoeffVar]				FLOAT					NULL,										--  Coefficient of Variation			http://en.[w]ikipedia.org/wiki/Coefficient_of_variation
	[xIdxDisp]				FLOAT					NULL,										--  Index of Dispersion				http://en.[w]ikipedia.org/wiki/Variance-to-mean_ratio

	[xSum]					FLOAT				NOT	NULL,										--	Sum	of x					Σ(x)
	[xSumXiX]				FLOAT				NOT	NULL,										--	Sum of Error				Σ(xi-AVG(x))

	[wMean]					FLOAT					NULL,										--	Weighted Mean
	[xwDiv]					FLOAT					NULL,										--	SUM(x) / SUM(w)

	[xLimitLower]			FLOAT				NOT	NULL,										--	Minimum (Mean of smallest two items, >= @CapMin)
	[xLimitUpper]			FLOAT				NOT	NULL,										--	Minimum (Mean of largest two items, <= @CapMax)
	[xLimitRange]			FLOAT				NOT	NULL,

	[tTest]					FLOAT				NOT	NULL	CHECK([tTest]	>= 0.0),			--	Calcualted T Test
	[tConf]					FLOAT				NOT	NULL	CHECK([tConf]	>= 0.0),			--	Calculated Confidence
	[tAlpha]				FLOAT				NOT	NULL	CHECK([tAlpha]	>= 0.0),			--	Alpha' value						http://en.[w]ikipedia.org/wiki/Student's_t-test
	[tStat]					FLOAT				NOT	NULL	CHECK([tStat]	>= 0.0),			--	Student T Statistic
	[tHypo]					TINYINT				NOT	NULL,

	[iConfidence]			FLOAT				NOT	NULL	CHECK([iConfidence]	>= 0.0),		--	Confidence Interval					http://en.[w]ikipedia.org/wiki/Confidence_interval
	[iPrediction]			FLOAT				NOT	NULL	CHECK([iPrediction]	>= 0.0),		--	Prediction Interval					http://en.[w]ikipedia.org/wiki/Prediction_interval
	[iPredictionLower]		FLOAT				NOT	NULL,										--	Lower Prediction Limit
	[iConfidenceLower]		FLOAT				NOT	NULL,										--	Lower Confidence Limit
	[iConfidenceUpper]		FLOAT				NOT	NULL,										--	Upper Confidence Limit
	[iPredictionUpper]		FLOAT				NOT	NULL,										--	Upper Prediction Limit

	[BoxLowerExtreme]		FLOAT				NOT	NULL,
	[BoxLowerWhisker]		FLOAT				NOT	NULL,
	[BoxLower]				FLOAT				NOT	NULL,
	[BoxMedian]				FLOAT				NOT	NULL,
	[BoxUpper]				FLOAT				NOT	NULL,
	[BoxUpperWhisker]		FLOAT				NOT	NULL,
	[BoxUpperExtreme]		FLOAT				NOT	NULL,
	[BoxIQR]				FLOAT				NOT	NULL,
	[BoxWidth]				FLOAT				NOT	NULL,
	[BoxNotch]				FLOAT				NOT	NULL,

	[xKurtosis]				FLOAT					NULL,										--	Pearson's sample excess Kurtosis	http://en.[w]ikipedia.org/wiki/Kurtosis
	[xSkewness]				FLOAT					NULL,										--	Skewness (negative leans right)		http://en.[w]ikipedia.org/wiki/Skewness

	CHECK([xMin]			<= [xMean]),
	CHECK([xMean]			<= [xMax]),
	CHECK([xMin]			<= [xLimitLower]),
	CHECK([xMax]			>= [xLimitUpper]),
	CHECK([iConfidence]		<= [iPrediction]),

	CHECK([BoxLowerExtreme]	<= [BoxLowerWhisker]),
	CHECK([BoxLowerWhisker]	<= [BoxLower]),
	CHECK([BoxLower]		<= [BoxMedian]),
	CHECK([BoxMedian]		<= [BoxUpper]),
	CHECK([BoxUpper]		<= [BoxUpperWhisker]),
	CHECK([BoxUpperWhisker]	<= [BoxUpperExtreme])
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Confidence_Max FLOAT = 0.999999999999999;
	SET @Confidence_Pcnt = CASE WHEN @Confidence_Pcnt > @Confidence_Max THEN @Confidence_Max ELSE @Confidence_Pcnt END;

	DECLARE	@Order		FLOAT	= CAST(ABS(@SortOrder) / @SortOrder AS FLOAT);
	DECLARE @Alpha		FLOAT = 0.50 + (@Confidence_Pcnt / 2.0);	

	DECLARE @xCount		INT;
	SELECT	@xCount = COUNT(1)	FROM	@Sample;
	IF (@xCount / 2.0 < @Tiles) SET @Tiles = @xCount / 2.0;

	DECLARE	@nTiles		TINYINT	= COALESCE(@Tiles, 4);

	DECLARE @nTile TABLE
	(
		[Id]				INT					NOT	NULL,
		[RowId]				INT					NOT	NULL,
		[x]					FLOAT				NOT	NULL,
		[w]					FLOAT					NULL,
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile] >= 1),			--	Quartile value
		[rnMin]				TINYINT					NULL	CHECK([rnMin] >= 1),			--	Row Number
		[rnMax]				TINYINT					NULL	CHECK([rnMax] >= 1),			--	Row Number
		
		PRIMARY KEY CLUSTERED ([x] ASC, [RowId] ASC)
	);

	INSERT INTO @nTile([Id], [RowId], [x], [w], [qTile], [rnMin], [rnMax])
	SELECT
		[o].[Id],
		[o].[RowId],
		[o].[x],
		[o].[w],
		[o].[qTile],
		[rnMin]	= CASE WHEN [o].[x] >= COALESCE(@CapMin, [o].[x])
			THEN
				ROW_NUMBER() OVER(PARTITION BY CASE WHEN [o].[x] BETWEEN COALESCE(@CapMin, [o].[x]) AND COALESCE(@CapMax, [o].[x]) THEN [o].[qTile] END ORDER BY [o].[x] * @Order ASC, [o].[RowId] ASC)
			END,
		[rnMax]	= CASE WHEN [o].[x] <= COALESCE(@CapMax, [o].[x])
			THEN
				ROW_NUMBER() OVER(PARTITION BY CASE WHEN [o].[x] BETWEEN COALESCE(@CapMin, [o].[x]) AND COALESCE(@CapMax, [o].[x]) THEN [o].[qTile] END ORDER BY [o].[x] * @Order DESC, [o].[RowId] ASC)
			END	
	FROM (
		SELECT
			[o].[Id],
			[o].[RowId],
			[o].[x],
			[o].[w],
			[qTile] = NTILE(@nTiles) OVER(ORDER BY [o].[x] * @Order ASC, [o].[RowId] ASC)	
		FROM @Sample [o]
		) [o];

	DECLARE @Descriptive TABLE
	(
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile]	>= 1),						--	Quartile value
		[xCount]			SMALLINT			NOT	NULL,												--	Minimum
		[xMin]				FLOAT				NOT	NULL,												--	Minimum
		[xMax]				FLOAT				NOT	NULL,												--	Maximum
		[_xRange]			AS CAST([xMax] - [xMin]			AS FLOAT)									--	Range
							PERSISTED			NOT	NULL,
		[xSum]				FLOAT				NOT	NULL,												--	Sum	of x					Σ(x)
		[xMean]				FLOAT				NOT	NULL,												--	Mean								http://en.wikipedia.org/wiki/Mean
		[xStDev]			FLOAT				NOT	NULL	CHECK([xStDev]	>= 0.0),					--	Standard Deviation			σ		http://en.wikipedia.org/wiki/Standard_deviation
		[xVar]				FLOAT				NOT	NULL	CHECK([xVar]	>= 0.0),					--	Variance					σ²		http://en.wikipedia.org/wiki/Variance
		
		[_ErrorStandard]	AS [xStDev] / SQRT([xCount])												--	Standard Error						http://en.wikipedia.org/wiki/Z-test
							PERSISTED			NOT	NULL,
		[_xCoeffVarP]		AS CASE WHEN [xMean] <> 0.0 THEN [xStDev]	/ [xMean] END					--  Coefficient of Variation			http://en.wikipedia.org/wiki/Coefficient_of_variation
							PERSISTED					,
		[_xIdxDispP]		AS CASE WHEN [xMean] <> 0.0 THEN [xVar]		/ [xMean] END					--  Index of Dispersion					http://en.wikipedia.org/wiki/Variance-to-mean_ratio
							PERSISTED					,
		
		[wMean]				FLOAT					NULL,												--	Weighted Mean
		[xwDiv]				FLOAT					NULL,
		[xLimitLower]		FLOAT				NOT	NULL,
		[xLimitUpper]		FLOAT				NOT	NULL,
		[_xLimitRange]		AS ([xLimitUpper] - [xLimitLower])											--	Range
							PERSISTED			NOT	NULL,

		[_tTest]			AS CASE WHEN [xStDev] <> 0.0 THEN ABS([xMean]) / [xStDev] ELSE 0.0 END	--	Calcualted T Test
							PERSISTED			NOT	NULL,

		PRIMARY KEY CLUSTERED ([qTile] ASC)
	);

	INSERT INTO @Descriptive
	(
		[qTile],
		[xCount],
		[xMin],
		[xMax],
		[xSum],
		[xMean],
		[xStDev],
		[xVar],
		[wMean],
		[xwDiv],
		[xLimitLower],
		[xLimitUpper]
	)
	SELECT
		[n].[qTile],
		COUNT([n].[x]),
		MIN([n].[x]),
		MAX([n].[x]),
		SUM([n].[x]),
		AVG([n].[x]),
		[xStDev]			= CASE WHEN @nTiles	= 1 THEN STDEVP([n].[x])	ELSE STDEV([n].[x])	END,
		[xVar]				= CASE WHEN @nTiles	= 1 THEN VARP([n].[x])		ELSE VAR([n].[x])	END,
		[wMean]				= CASE WHEN SUM([n].[w]) <> 0.0 THEN SUM([n].[x] * [n].[w])	/ SUM([n].[w]) END,
		[xwDiv]				= CASE WHEN SUM([n].[w]) <> 0.0 THEN SUM([n].[x])			/ SUM([n].[w]) END,
		[xMeanLower]		= AVG(CASE WHEN [n].[rnMin] <= @EndPointMean THEN [n].[x] END),
		[xMeanUpper]		= AVG(CASE WHEN [n].[rnMax] <= @EndPointMean THEN [n].[x] END)
	FROM @nTile	[n]
	GROUP BY
		[n].[qTile];

	DECLARE @ComboStats TABLE
	(
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile]	>= 1),				--	Quartile value
		[xSumXiX]			FLOAT				NOT	NULL,										--	Sum of Error		Σ(xi-AVG(x))
		[xKurtosis]			FLOAT					NULL,										--	Pearson's sample excess Kurtosis	http://en.wikipedia.org/wiki/Kurtosis
		[xSkewness]			FLOAT					NULL,										--	Skewness (negative leans right)		http://en.wikipedia.org/wiki/Skewness
		[tAlpha]			FLOAT				NOT	NULL,										--	Alpha value							http://en.wikipedia.org/wiki/Student's_t-test
		[tConf]				FLOAT				NOT	NULL,										--	Calculated Confidence
		[tStat]				FLOAT				NOT	NULL,										--	Student T Statistic
		[iConfidence]		FLOAT				NOT	NULL,										--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
		[iPrediction]		FLOAT				NOT	NULL,										--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval
		PRIMARY KEY CLUSTERED ([qTile] ASC)
	);

	INSERT INTO @ComboStats
	(
		[qTile],
		[xSumXiX],
		[xKurtosis],
		[xSkewness],
		[tAlpha],
		[tConf],
		[tStat],
		[iConfidence],
		[iPrediction]
	)
	SELECT
		[d].[qTile],
		[xSumXiX]		= SUM(SQUARE([n].[x] - [d].[xMean])),
		[xKurtosis]		= CASE WHEN SUM(SQUARE([n].[x] - [d].[xMean])) <> 0.0
						THEN
							(1.0 / [d].[xCount]) * SUM(POWER([n].[x] - [d].[xMean], 4)) / SQUARE((1.0 / [d].[xCount]) * SUM(SQUARE([n].[x] - [d].[xMean]))) - 3.0
						END,
		[xSkewness]		= CASE WHEN SUM(SQUARE([n].[x] - [d].[xMean])) <> 0.0 AND ([d].[xCount] - 2.0) <> 0.0
						THEN 
							(1.0 / [d].[xCount]) * SUM(POWER([n].[x] - [d].[xMean], 3)) / POWER((1.0 / [d].[xCount]) * SUM(SQUARE([n].[x] - [d].[xMean])), 3.0 / 2.0)
							* SQRT([d].[xCount] * ([d].[xCount] - 1.0)) / ([d].[xCount] - 2.0)
						END,
		[tAlpha]		= @Alpha,
		[tConf]			= CASE WHEN [d].[_tTest] <> 0.0 THEN [stat].[Student_Probability]([d].[xCount] - 1, [d].[_tTest], 2) END,
		[tStat]			= [stat].[Student_Statistic]([d].[xCount] - 1, @Alpha, 2),

		[iConfidence]	= [stat].[Student_Statistic]([d].[xCount] - 1, @Alpha, 2) * [d].[xStDev] / SQRT([d].[xCount]),
		[iPrediction]	= [stat].[Student_Statistic]([d].[xCount] - 1, @Alpha, 2) * [d].[xStDev] / SQRT(1.0 + 1.0 / [d].[xCount])
	FROM	@Descriptive	[d]
	INNER JOIN @nTile		[n]
		ON	[n].[qTile]	= [d].[qTile]
	GROUP BY
		[d].[qTile],
		[d].[xCount],
		[d].[_tTest],
		[d].[xStDev];

	DECLARE @Box TABLE
	(
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile]	>= 1),						--	Quartile value
		[xCount]			SMALLINT			NOT	NULL	CHECK([xCount]	>= 1),		--	Item Count

		[_BoxLowerExtreme]	AS [BoxLower] - ([BoxUpper] - [BoxLower]) * 3.0
							PERSISTED			NOT	NULL,
		[_BoxLowerWhisker]	AS [BoxLower] - ([BoxUpper] - [BoxLower]) * 1.5
							PERSISTED			NOT	NULL,

		[BoxLower]			FLOAT				NOT	NULL,
		[BoxMedian]			FLOAT				NOT	NULL,
		[BoxUpper]			FLOAT				NOT	NULL,

		[_BoxUpperWhisker]	AS [BoxUpper] + ([BoxUpper] - [BoxLower]) * 1.5
							PERSISTED			NOT	NULL,
		[_BoxUpperExtreme]	AS [BoxUpper] + ([BoxUpper] - [BoxLower]) * 3.0
							PERSISTED			NOT	NULL,

		[_BoxIQR]			AS ([BoxUpper] - [BoxLower])
							PERSISTED			NOT	NULL,
		[_BoxWidth]			AS SQRT([xCount])
							PERSISTED			NOT	NULL,
		[_BoxNotch]			AS ([BoxUpper] - [BoxLower]) / SQRT([xCount])
							PERSISTED			NOT	NULL,

		CHECK([BoxLower]		<= [BoxMedian]),
		CHECK([BoxMedian]		<= [BoxUpper]),

		PRIMARY KEY CLUSTERED([qTile] ASC)
	);

	INSERT INTO @Box
	(
		[qTile],
		[xCount],
		[BoxLower],
		[BoxMedian],
		[BoxUpper]
	)
	SELECT
		[n].[qTile],
		[d].[xCount],

		[BoxLower] = AVG(CASE WHEN [n].[rnMin] IN 
			(
				ROUND([d].[xCount] / 4, 0),
				ROUND([d].[xCount] / 4, 0) + CASE WHEN [d].[xCount] % 2 = 0 THEN 0 ELSE 1 END
			)
			THEN
				[n].[x]
			END),

		[BoxMedian] = AVG(CASE WHEN [n].[rnMin] IN 
			(
				ROUND([d].[xCount] / 2, 0) + CASE WHEN [d].[xCount] % 2 = 0 THEN 0 ELSE 1 END,
				ROUND([d].[xCount] / 2, 0) + CASE WHEN [d].[xCount] % 2 = 0 THEN 1 ELSE NULL END
			)
			THEN
				[n].[x]
			END),

		[BoxUpper] = AVG(CASE WHEN [n].[rnMin] IN 
			(
				ROUND([d].[xCount] / 4, 0) * 3 + 1,
				ROUND([d].[xCount] / 4, 0) * 3 + CASE WHEN [d].[xCount] % 2 = 0 THEN 1 ELSE 2 END
			)
			THEN
				[n].[x]
			END)
		
	FROM	@Descriptive	[d]
	INNER JOIN @nTile		[n]
		ON	[n].[qTile]	= [d].[qTile]
	GROUP BY
		[n].[qTile],
		[d].[xCount];

	INSERT INTO @ReturnTable
	(
		[qTile],
		[SampleType],
		[xCount],
		[xMin],
		[xMax],
		[xRange],

		[BreakPrevious],
		[BreakNext],

		[xMean],
		[xStDev],
		[xVar],

		[xErrorStandard],
		[xCoeffVar],
		[xIdxDisp],

		[xSum],
		[xSumXiX],

		[wMean],
		[xwDiv],

		[xLimitLower],
		[xLimitUpper],
		[xLimitRange],

		[tTest],
		[tConf],
		[tAlpha],
		[tStat],
		[tHypo],

		[iConfidence],
		[iPrediction],
		[iPredictionLower],
		[iConfidenceLower],
		[iConfidenceUpper],
		[iPredictionUpper],

		[BoxLowerExtreme],
		[BoxLowerWhisker],
		[BoxLower],
		[BoxMedian],
		[BoxUpper],
		[BoxUpperWhisker],
		[BoxUpperExtreme],
		[BoxIQR],
		[BoxWidth],
		[BoxNotch],

		[xKurtosis],
		[xSkewness]
	)
	SELECT
		[d].[qTile],
		[SampleType]		= CASE WHEN @nTiles = 1 THEN 'Population' ELSE 'Sample' END,
		[d].[xCount],
		[d].[xMin],
		[d].[xMax],
		[d].[_xRange],

		[BreakPrevious]		= ([d].[xMin] + [p].[xMax]) / 2.0,
		[BreakNext]			= ([d].[xMax] + [n].[xMin]) / 2.0,

		[d].[xMean],
		[d].[xStDev],
		[d].[xVar],

		[d].[_ErrorStandard],
		[d].[_xCoeffVarP],
		[d].[_xIdxDispP],

		[d].[xSum],
		[c].[xSumXiX],
	
		[d].[wMean],
		[d].[xwDiv],
		[d].[xLimitLower],
		[d].[xLimitUpper],
		[d].[_xLimitRange],
	
		[d].[_tTest],
		[c].[tConf],
		[c].[tAlpha],
		[c].[tStat],

		[tHypo]				= CASE WHEN [c].[tStat] > [d].[_tTest] THEN 1 ELSE 0 END,	--	Null hypothesis test 1 (Accept) or 0 (Reject)

		[c].[iConfidence],
		[c].[iPrediction],

		[iPredictionLower]	= [d].[xMean] - [c].[iPrediction],							--	Lower Prediction Limit
		[iConfidenceLower]	= [d].[xMean] - [c].[iConfidence],							--	Lower Confidence Limit
		[iConfidenceUpper]	= [d].[xMean] + [c].[iConfidence],							--	Upper Confidence Limit
		[iPredictionUpper]	= [d].[xMean] + [c].[iPrediction],							--	Upper Prediction Limit

		[BoxLowerExtreme]	= [b].[_BoxLowerExtreme],
		[BoxLowerWhisker]	= [b].[_BoxLowerWhisker],
		[b].[BoxLower],
		[b].[BoxMedian],
		[b].[BoxUpper],
		[BoxUpperWhisker]	= [b].[_BoxUpperWhisker],
		[BoxUpperExtreme]	= [b].[_BoxUpperExtreme],
		[BoxIQR]			= [b].[_BoxIQR],
		[BoxWidth]			= [b].[_BoxWidth],
		[BoxNotch]			= [b].[_BoxNotch],

		[c].[xKurtosis],
		[c].[xSkewness]

	FROM @Descriptive				[d]
	LEFT OUTER JOIN @Descriptive	[p]
		ON	[p].[qTile]	= [d].[qTile] - 1
	LEFT OUTER JOIN @Descriptive	[n]
		ON	[n].[qTile]	= [d].[qTile] + 1
	INNER JOIN @ComboStats			[c]
		ON	[c].[qTile]	= [d].[qTile]
	INNER JOIN	@Box				[b]
		ON	[b].[qTile]	= [d].[qTile];

	RETURN;

END;