﻿using System;
using System.Data.SqlTypes;
using System.IO;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

[Serializable]
[SqlUserDefinedAggregate
	(
		Format.UserDefined,
		IsInvariantToNulls = true,
		IsInvariantToDuplicates = true,
		IsInvariantToOrder = true,
		IsNullIfEmpty = true,
		MaxByteSize = -1
	)
]
public class IsNormalDistribution_Statistic : MetaNumericsSample
{
	new public SqlDouble Terminate()
	{
		double mu = base.sampleDataSet.Mean;
		double s = base.sampleDataSet.StandardDeviation;

		TestResult tr = base.sampleDataSet.KolmogorovSmirnovTest(new NormalDistribution(mu, s));
		return tr.Statistic;
	}

	new public void Init()
	{
		base.Init();
	}

	new public void Accumulate(SqlDouble Observation)
	{
		base.Accumulate(Observation);
	}

	public void Merge(IsNormalDistribution_Statistic Group)
	{
		base.Merge(Group);
	}
}