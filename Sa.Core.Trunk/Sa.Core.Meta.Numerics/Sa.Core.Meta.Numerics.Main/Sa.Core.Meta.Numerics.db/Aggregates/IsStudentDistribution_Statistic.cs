﻿using System;
using System.Data.SqlTypes;
using System.IO;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

[Serializable]
[SqlUserDefinedAggregate
	(
		Format.UserDefined,
		IsInvariantToNulls = true,
		IsInvariantToDuplicates = true,
		IsInvariantToOrder = true,
		IsNullIfEmpty = true,
		MaxByteSize = -1
	)
]
public class IsStudentDistribution_Statistic : MetaNumericsSample
{
	private double mean;

	new public SqlDouble Terminate()
	{
		this.mean = base.sampleDataSet.Mean;
		base.sampleDataSet.Transform(ShiftMean);

		int nu = (base.sampleDataSet.Count < 1) ? 1 : base.sampleDataSet.Count - 1;

		TestResult tr = base.sampleDataSet.KolmogorovSmirnovTest(new StudentDistribution(nu));
		return tr.Statistic;
	}

	private double ShiftMean(double Value)
	{
		return Value - this.mean;
	}

	new public void Init()
	{
		base.Init();
	}

	new public void Accumulate(SqlDouble Observation)
	{
		base.Accumulate(Observation);
	}

	public void Merge(IsStudentDistribution_Statistic Group)
	{
		base.Merge(Group);
	}
}