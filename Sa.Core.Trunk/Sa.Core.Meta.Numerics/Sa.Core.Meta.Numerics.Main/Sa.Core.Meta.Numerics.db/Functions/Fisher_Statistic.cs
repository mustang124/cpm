﻿using System;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
	[SqlFunction]
	public static SqlDouble Fisher_Statistic(SqlDouble DegreesOfFreedomNumerator, SqlDouble DegreesOfFreedomDenominator, SqlDouble Probability)
	{
		FisherDistribution d = new FisherDistribution((double)DegreesOfFreedomNumerator, (double)DegreesOfFreedomDenominator);
		return d.InverseLeftProbability((double)Probability);
	}
}