﻿using System;
using System.Data.SqlTypes;

using Microsoft.SqlServer.Server;
using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
	[SqlFunction]
	public static SqlDouble Student_Probability(SqlDouble DegreesOfFreedom, SqlDouble TStatistic, SqlByte Tails)
	{
		StudentDistribution d = new StudentDistribution((double)DegreesOfFreedom);
		return (1.0 - (double)Tails * (1.0 - d.LeftProbability((double)TStatistic)));
	}
}