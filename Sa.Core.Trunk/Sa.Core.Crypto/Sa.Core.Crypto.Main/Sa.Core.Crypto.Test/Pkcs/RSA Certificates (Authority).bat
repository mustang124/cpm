rem		http://nick-howard.blogspot.com/2011/05/makecert-x509-certificates-and-CngEncr.html
		cd C:\"Program Files (x86)"\"Windows Kits"\8.0\bin\x64\
	
		makecert -pe -r -n "CN=Authority" -cy authority -a sha512 -len 4096 -sv "C:\Authority.pvk" "C:\Authority.cer"
rem		Private Key Password / Issuer Signature:	5DDD226A-8431-4D9B-8FEB-466F73A3	// Use only the 32 characters

		makecert -pe -n "CN=Confidentiality" -cy end -a sha512 -len 4096 -sky exchange -eku 1.3.6.1.5.5.7.3.1 -iv "C:\Authority.pvk" -ic "C:\Authority.cer" -sp "Microsoft RSA SChannel Cryptographic Provider" -sy 12 -sv "C:\Confidentiality.pvk" "C:\Confidentiality.cer"
rem		Private Key Password:						E3BD99F7-84B2-4E4E-ABF9-083D1C47
rem		Issuer Signature (Use Authority Password)

		makecert -pe -n "CN=Privacy" -cy end -a sha512 -len 4096 -sky exchange -eku 1.3.6.1.5.5.7.3.1 -iv "C:\Authority.pvk" -ic "C:\Authority.cer" -sp "Microsoft RSA SChannel Cryptographic Provider" -sy 12 -sv "C:\Privacy.pvk" "C:\Privacy.cer"
rem		Private Key Password:						A6CF91CB-58C6-4E92-A34C-0DF0584B
rem		Issuer Signature (Use Authority Password)

		makecert -pe -n "CN=Integrity" -cy end -a sha512 -len 4096 -sky exchange -eku 1.3.6.1.5.5.7.3.1 -iv "C:\Authority.pvk" -ic "C:\Authority.cer" -sp "Microsoft RSA SChannel Cryptographic Provider" -sy 12 -sv "C:\Integrity.pvk" "C:\Integrity.cer"
rem		Private Key Password:						3C0760F1-12B8-41AB-A231-86596C9E
rem		Issuer Signature (Use Authority Password)

		makecert -pe -n "CN=NonRepudiation" -cy end -a sha512 -len 4096 -sky exchange -eku 1.3.6.1.5.5.7.3.1 -iv "C:\Authority.pvk" -ic "C:\Authority.cer" -sp "Microsoft RSA SChannel Cryptographic Provider" -sy 12 -sv "C:\NonRepudiation.pvk" "C:\NonRepudiation.cer"
rem		Private Key Password:						7829ED80-E843-4CE2-8AFB-67EB5BC3
rem		Issuer Signature (Use Authority Password)

		pvk2pfx -pvk "C:\Confidentiality.pvk" -spc "C:\Confidentiality.cer" -pfx "C:\Confidentiality.pfx" -pi "E3BD99F7-84B2-4E4E-ABF9-083D1C47" -f
		pvk2pfx -pvk "C:\Privacy.pvk" -spc "C:\Privacy.cer" -pfx "C:\Privacy.pfx" -pi "A6CF91CB-58C6-4E92-A34C-0DF0584B" -f
		pvk2pfx -pvk "C:\Integrity.pvk" -spc "C:\Integrity.cer" -pfx "C:\Integrity.pfx" -pi "3C0760F1-12B8-41AB-A231-86596C9E" -f
		pvk2pfx -pvk "C:\NonRepudiation.pvk" -spc "C:\NonRepudiation.cer" -pfx "C:\NonRepudiation.pfx" -pi "7829ED80-E843-4CE2-8AFB-67EB5BC3" -f
