﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

using Microsoft.VisualStudio.TestTools.UnitTesting;

//	http://world.std.com/~dtd/sign_encrypt/sign_encrypt7.html

namespace Test.Core
{
	[TestClass]
	public partial class Crypto
	{
		const string sndMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

		[TestMethod]
		public void TotalChain()
		{
			byte[] aesSndPassword = File.ReadAllBytes("Rfc\\ConfidentialityKey.txt");
			byte[] aesSndSalt = File.ReadAllBytes("Rfc\\ConfidentialitySalt.txt");

			byte[] aesSndKey = new byte[32];
			Sa.Core.Crypto.Password.Derive(aesSndPassword, aesSndSalt, 32, out aesSndKey);

			byte[] hmacSndPassword = File.ReadAllBytes("Rfc\\IntegrityKey.txt");
			byte[] hmacSndSalt = File.ReadAllBytes("Rfc\\IntegritySalt.txt");

			byte[] hmacSndKey = new byte[64];
			Sa.Core.Crypto.Password.Derive(hmacSndPassword, hmacSndSalt, 64, out hmacSndKey);

			X509Certificate2 certSndPrivate;
			Sa.Core.Crypto.Certificate.Get("NonRepudiation", out certSndPrivate);

			X509Certificate2 certRecPublic;
			Sa.Core.Crypto.Certificate.Get("Privacy", out certRecPublic);

			//////////////////////////////////////////////////////////////////////////////
			//																			//
			//					Send Encrypted Secret Keys - Out of Band				//
			//																			//
			//////////////////////////////////////////////////////////////////////////////

			byte[] aesOutOfBand;
			X509Certificate2 certAesPublic;
			Sa.Core.Crypto.Certificate.Get("Confidentiality", out certAesPublic);
			Sa.Core.Crypto.Confidentiality.Asymmetric.Encrypt(aesSndKey, certAesPublic, out aesOutOfBand);

			byte[] hmacOutOfBand;
			X509Certificate2 certIntPublic;
			Sa.Core.Crypto.Certificate.Get("Integrity", out certIntPublic);
			Sa.Core.Crypto.Confidentiality.Asymmetric.Encrypt(hmacSndKey, certIntPublic, out hmacOutOfBand);

			//////////////////////////////////////////////////////////////////////////////

			byte[] sndBytes = Sa.Core.Serialization.ToByteArray(sndMessage);

			byte[] sndZip;
			Sa.Core.GZip.Compress(sndBytes, out sndZip);

			//	prevent plain-text tampering
			byte[] sndSigned;
			Sa.Core.Crypto.Integrity.Sign(sndZip, hmacSndKey, out sndSigned);

			//	encrypt pain-text
			byte[] sndEncrypted;
			Sa.Core.Crypto.Confidentiality.Symmetric.Encrypt(sndSigned, aesSndKey, out sndEncrypted);

			//	prevent cipher-text tampering, verfies specific encryption key was used
			byte[] sndEncryptedSigned;
			Sa.Core.Crypto.Integrity.Sign(sndEncrypted, hmacSndKey, out sndEncryptedSigned);

			//	encrypt for one recipient
			byte[] sndPrivatized;
			Sa.Core.Crypto.Privacy.Encode(sndEncryptedSigned, certRecPublic, out sndPrivatized);

			//	verify encryption was by signer
			byte[] sndAuthenticated;
			Sa.Core.Crypto.NonRepudiation.Sign(sndPrivatized, certSndPrivate, out sndAuthenticated);

			//	allow one recipeint to verify signature and signed encryption
			byte[] sndAuthenticatedPrivatized;
			Sa.Core.Crypto.Privacy.Encode(sndAuthenticated, certRecPublic, out sndAuthenticatedPrivatized);


			//////////////////////////////////////////////////////////////////////////////
			//																			//
			//								Send										//
			//																			//
			//////////////////////////////////////////////////////////////////////////////

			byte[] snd = sndAuthenticatedPrivatized;
			byte[] rec = snd;

			//////////////////////////////////////////////////////////////////////////////
			//																			//
			//								Receive										//
			//																			//
			//////////////////////////////////////////////////////////////////////////////

			byte[] aesRecKey;
			X509Certificate2 certAesPrivate;
			Sa.Core.Crypto.Certificate.Get("Confidentiality", out certAesPrivate);
			Sa.Core.Crypto.Confidentiality.Asymmetric.Decrypt(aesOutOfBand, certAesPrivate, out aesRecKey);

			byte[] hmacRecKey;
			X509Certificate2 certIntPrivate;
			Sa.Core.Crypto.Certificate.Get("Integrity", out certIntPrivate);
			Sa.Core.Crypto.Confidentiality.Asymmetric.Decrypt(hmacOutOfBand, certIntPrivate, out hmacRecKey);

			X509Certificate2 certSndPublic = new X509Certificate2("Pkcs\\" + "NonRepudiation" + ".cer");
			//X509Certificate2 certRecPrivate = new X509Certificate2("Pkcs\\" + "Privacy" + ".pfx", "A6CF91CB-58C6-4E92-A34C-0DF0584B");

			//////////////////////////////////////////////////////////////////////////////


			byte[] recAuthenticatedPrivatized;
			Sa.Core.Crypto.Privacy.Decode(rec, out recAuthenticatedPrivatized);

			byte[] recAuthenticated;
			Sa.Core.Crypto.NonRepudiation.Verify(recAuthenticatedPrivatized, certSndPublic, true, out recAuthenticated);

			byte[] recPrivatized;
			Sa.Core.Crypto.Privacy.Decode(recAuthenticated, out recPrivatized);

			byte[] recEncryptedSigned;
			Sa.Core.Crypto.Integrity.Verify(recPrivatized, hmacRecKey, out recEncryptedSigned);

			byte[] recEncrypted;
			Sa.Core.Crypto.Confidentiality.Symmetric.Decrypt(recEncryptedSigned, aesRecKey, out recEncrypted);

			byte[] recSigned;
			Sa.Core.Crypto.Integrity.Verify(recEncrypted, hmacRecKey, out recSigned);

			byte[] recZip;
			Sa.Core.GZip.Expand(recSigned, out recZip);

			byte[] recBytes = recZip;

			//////////////////////////////////////////////////////////////////////////////
			string recMessage = Sa.Core.Serialization.ToObject<string>(recBytes);

			Assert.IsTrue(sndBytes.SequenceEqual(recBytes));
			Assert.AreEqual(sndMessage, recMessage);
		}
	}
}