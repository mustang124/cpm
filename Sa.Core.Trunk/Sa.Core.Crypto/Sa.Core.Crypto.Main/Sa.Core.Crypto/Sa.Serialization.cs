﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Sa.Core
{
    public static class Serialization
    {
		/// <summary>
		/// Converts a byte array to the defined type.
		/// </summary>
		/// <typeparam name="returnType">Type to return</typeparam>
		/// <param name="byteArray"></param>
		/// <returns>type defined in returnType</returns>
		/// <example>string s = Deserialize<string>(new byte[0]);</example>
		public static returnType ToObject<returnType>(byte[] byteArray)
		{
			using (MemoryStream stream = new MemoryStream(byteArray))
			{
				BinaryFormatter formatter = new BinaryFormatter();
				return (returnType)formatter.Deserialize(stream);
			}
		}

		/// <summary>
		/// Converts an object to byte array.
		/// </summary>
		/// <param name="obj">The object to serialize to bytes</param>
		/// <returns>byte[]</returns>
		public static byte[] ToByteArray(object obj)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(stream, obj);
				return stream.ToArray();
			}
		}
	}
}