﻿using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;

namespace Sa.Core
{
	public static partial class Crypto
	{
		/// <summary>
		/// Signs a message from a specfic sender; increasing confidence that the message came from an expected sender.
		/// </summary>
		public static class NonRepudiation
		{
			/// <summary>
			/// Signs a message using a private certificate
			/// </summary>
			/// <param name="message"></param>
			/// <param name="certPrivate"></param>
			/// <param name="encodedSignedCms"></param>
			/// <returns></returns>
			public static bool Sign(byte[] message, X509Certificate2 certPrivate, out byte[] encodedSignedCms)
			{
				try
				{
					ContentInfo contentInfo = new ContentInfo(message);
					SignedCms signedCms = new SignedCms(contentInfo, false);
					CmsSigner cmsSigner = new CmsSigner(certPrivate);
					signedCms.ComputeSignature(cmsSigner);
					encodedSignedCms = signedCms.Encode();
					return true;
				}
				catch
				{
					encodedSignedCms = new byte[0];
					return false;
				}
			}

			/// <summary>
			/// Verifies a signed message using a public certificate; the certificate can be validated for revocation
			/// </summary>
			/// <param name="encodedSignedCms"></param>
			/// <param name="certPublic"></param>
			/// <param name="checkSignatureOnly">If true, checks only signature; if false, checks revocation.</param>
			/// <param name="origMsg"></param>
			/// <returns></returns>
			public static bool Verify(byte[] encodedSignedCms, X509Certificate2 certPublic, bool checkSignatureOnly, out byte[] origMsg)
			{
				try
				{
					SignedCms signedCms = new SignedCms();

					signedCms.Decode(encodedSignedCms);

					if (signedCms.Certificates.Contains(certPublic))
					{
						signedCms.CheckSignature(checkSignatureOnly);
						origMsg = signedCms.ContentInfo.Content;
						return true;
					}
					else
					{
						origMsg = new byte[0];
						return false;
					}
				}
				catch
				{
					origMsg = new byte[0];
					return false;
				}
			}
		}
	}
}