﻿using System.Linq;
using System.Security.Cryptography;

namespace Sa.Core
{
	/// <summary>
	/// Performs the encryption, decryption, and verification of a byte array.
	/// The byte array is signed during the encryption process. During decryption, the signature is verified.
	/// </summary>
	public static partial class Crypto
	{
		/// <summary>
		/// The message has not been tampered
		/// </summary>
		public static class Integrity
		{
			/// <summary>
			/// Appends the signature to the unsigned data.
			/// </summary>
			/// <param name="unsignedRaw">Data to be signed</param>
			/// <param name="signatureKey">Signature key</param>
			/// <returns></returns>
			public static bool Sign(byte[] unsigned, byte[] signatureKey, out byte[] signed)
			{
				using (HMACSHA512 hmac = new HMACSHA512(signatureKey))
				{
					byte[] signature = hmac.ComputeHash(unsigned);
					signed = signature.Concat(unsigned).ToArray();
					hmac.Clear();
				}
				return true;
			}

			/// <summary>
			/// Verifies the raw data signed by the signature key matches the signed raw data.
			/// </summary>
			/// <param name="unsigned"></param>
			/// <param name="signatureKey">Signature key</param>
			/// <param name="signed">Signed data</param>
			/// <returns></returns>
			public static bool Verify(byte[] signed, byte[] signatureKey, out byte[] unsigned)
			{
				const int HashLength = 64;

				byte[] uSigned = signed.Skip(HashLength).Take(signed.Length - HashLength).ToArray();

				if (VerifyHash(uSigned, signatureKey, signed))
				{
					unsigned = uSigned;
					return true;
				}
				else
				{
					unsigned = new byte[0];
					return false;
				}
			}

			private static bool VerifyHash(byte[] unsigned, byte[] signatureKey, byte[] signed)
			{
				byte[] sig;
				Integrity.Sign(unsigned, signatureKey, out sig);

				return sig.SequenceEqual(signed);
			}
		}
	}
}