﻿using System;
using System.Net.Mail;

[assembly: CLSCompliant(true)]
namespace Sa.Core.eMail
{
	/// <summary>
	/// E-mail address and host name verification.
	/// </summary>
	public static class Address
    {
		/// <summary>
		/// Verifies the e-mail address is well formed.
		/// </summary>
		/// <param name="recipientAddress">Well formed e-mail address.</param>
		/// <returns>bool</returns>
		public static bool ValidateAddress(string recipientAddress)
		{
			MailAddress mailAddress = new MailAddress(recipientAddress);
			return mailAddress.Address == recipientAddress;
		}

		/// <summary>
		/// Validates that the host exists using a DNS lookup function.
		/// </summary>
		/// <param name="hostName">String representing the e-mail server host name.</param>
		/// <returns>bool</returns>
		public static bool ValidateHost(string hostName)
		{
			System.Net.IPHostEntry Host = System.Net.Dns.GetHostEntry(hostName);
			return Host.HostName.Length > 0;
		}

		/// <summary>
		/// Verifies the e-mail address is well formed and the e-mail host exists.
		/// </summary>
		/// <param name="recipientAddress">Well formed e-mail address.</param>
		/// <returns>bool</returns>
		public static bool ValidateAddressHost(string recipientAddress)
		{
			MailAddress mailAddress = new MailAddress(recipientAddress);
			return Address.ValidateAddress(recipientAddress) && Address.ValidateHost(mailAddress.Host);
		}
    }
}