﻿using System;
using System.Net.Mail;

namespace Sa.Core.eMail
{
	class Client
	{
		internal static string SendMessage(MailMessage mailMessage)
		{
			string status = "E-mail message not yet sent.";

			string host = "mail.solomononline.com";
			int port = 25;

			using (SmtpClient client = new SmtpClient(host, port))
			{
				//client.UseDefaultCredentials = false;
				//client.Credentials = new System.Net.NetworkCredential("UserID", "Password");
				client.DeliveryFormat = SmtpDeliveryFormat.International;
				client.DeliveryMethod = SmtpDeliveryMethod.Network;
				client.EnableSsl = true;

				try
				{
					client.Send(mailMessage);
					status = "E-mail message sent.";
				}
				catch (Exception ex)
				{
					status = ex.Message;
				}
				return status;
			}
		}
	}
}