
Imports System
Imports System.IO
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Configuration


Public Class UserAuthentication

    Private Shared CONNSTR As String = ConfigurationManager.ConnectionStrings("ProfileFuelsConnectionString").ToString() + ";persist security info=False"
    '"user id=ProfileFuels;password=ProfileFuels;data source=""10.10.41.7"";persist security info=False;initial catalog=ProfileFuels"
    Private Shared CONNSTR12 As String = CONNSTR.Replace("initial catalog=ProfileFuels", "initial catalog=ProfileFuels12") ' "user id=ProfileFuels;password=ProfileFuels;data source=""10.10.41.7"";persist security info=False;initial catalog=ProfileFuels12"

    Private Is2012 As Boolean

    ' <summary>
    ' Constructs an instance of this security token manager.
    ' </summary>

    Public Sub New()

    End Sub

    Public Function CheckAuthenticationAndAuthorizationTable(refineryId As String,
                                                             ByRef active As Boolean,
                                                             ByRef certificateRequired As Boolean,
                                                             ByRef clientKeyRequired As Boolean,
                                                             ByRef clientKeyFromDb As String) As Boolean
        'Try
        '    'check to see if we are bypassing this set of checks
        '    Dim bypassSetting As String = ConfigurationManager.AppSettings("bypass").ToString()
        '    If ConvertStringToTrueOrFalse(bypassSetting) Then
        '        active = True
        '        certificateRequired = False
        '        clientKeyRequired = False
        '        'clientKeyFromDb 
        '        WriteLog("Bypassing checks in CheckAuthenticationAndAuthorizationTable()")
        '        Return True
        '    End If
        'Catch
        'End Try

        Try
            Dim conn As New SqlConnection(CONNSTR) '"user id=ProfileFuels;password=ProfileFuels;data source=""10.10.41.7"";persist security info=False;initial catalog=ProfileFuels")
            Dim reader As SqlDataReader
            Dim cmd As New SqlCommand()
            Try
                conn.Open()
                cmd.Connection = conn
                Dim sql As String = "SELECT RefineryGroupId,RefineryId,Active,CertificateRequired,ClientKeyRequired,ClientKey"
                sql += " FROM dbo.AuthenticationAndAuthorization where RefineryId =@RefineryId"
                cmd = New SqlCommand(sql, conn)
                'cmd.Parameters.Add("@RefineryId", refineryId)
                cmd.Parameters.Add(New SqlParameter("@RefineryID", refineryId))
                cmd.CommandType = CommandType.Text
                reader = cmd.ExecuteReader()
                If reader.HasRows() Then
                    reader.Read() 'expect only 1 record
                    active = TranslateSqlServerBitTypeFromNumbersToBoolean(reader.Item("Active"))
                    certificateRequired = TranslateSqlServerBitTypeFromNumbersToBoolean(reader.Item("CertificateRequired"))
                    clientKeyRequired = TranslateSqlServerBitTypeFromNumbersToBoolean(reader.Item("ClientKeyRequired"))
                    clientKeyFromDb = reader.Item("ClientKey").ToString()
                Else
                    WriteLog("No record for RefineryId " + refineryId + " found in dbo.AuthenticationAndAuthorization()")
                    'no match in db. 
                    Return False '
                End If
                Return True
            Catch Ex As Exception
                WriteLog("Error " + Ex.Message + " reading record for RefineryId " + refineryId + " in dbo.AuthenticationAndAuthorization()")
                Return False
            End Try
            conn.Close()
            Return True
        Catch ex As Exception
            WriteLog("Unexpectd Error " + ex.Message + " for RefineryId " + refineryId + " in dbo.AuthenticationAndAuthorization()")
            Return False 'error
        End Try
    End Function

    Private Function TranslateSqlServerBitTypeFromNumbersToBoolean(value As Byte) As Boolean
        '[AuthenticationAndAuthorization] table doesn't allow nulls so not need 'Boolean?'
        Select Case value
            Case 0
                Return False
            Case 255 'Even if it shows as a '1' in Sql Server, when it comes out of the DataReader, it is 255
                Return True
            Case Else
                Throw New Exception("Sql Server Bit datatype has value other than 0 or 255!")
        End Select
    End Function

    Private Shared Sub WriteLog(st As String)
        Try
            Dim sw As StreamWriter = New StreamWriter(ConfigurationManager.AppSettings.Item("LogFilePath").ToString(), True)
            sw.WriteLine(Now.ToString & ": " & st)
            sw.Flush()
            sw.Close()
        Catch ex As Exception
            'doesn't matter
        End Try
    End Sub

    ' <summary>
    ' Returns the password or password equivalent for the username provided.
    '</summary>
    ' <param name="token">The username token</param>
    '<returns>The password (or password equivalent) for the username</returns>
    Public Shared Function VerifyKey(ByVal token As String) As Boolean
        ' This is a very simple manager.
        ' In most production systems the following code
        ' typically consults an external database of (username,password) pairs where
        ' the password is often not the real password but a password equivalent
        ' (for example, the hash of the password). Provided that both client and
        ' server can generate the same value for a particular username, there is
        ' no requirement that the password be the actual password for the user.
        ' For this sample the password is simply the reverse of the username.

        'Return "password"
        Dim key, company, location As String
        Dim locIndex As Integer
        Dim pathToServerKeyFile As String = String.Empty
        Try
            company = Decrypt(token).Split("$".ToCharArray)(0)

            locIndex = Decrypt(token).Split("$".ToCharArray).Length - 2
            location = Decrypt(token).Split("$".ToCharArray)(locIndex)

            'Try new way 

            If File.Exists("C:\\ClientKeys\\" + company + "_" + location + ".key") Then
                pathToServerKeyFile = "C:\\ClientKeys\\" + company + "_" + location + ".key"
                Dim reader As New StreamReader(pathToServerKeyFile)
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return ServerKeyFileContainsUserKey(pathToServerKeyFile, key) 'Return True 'ceate mini key 
                Else
                    Return False
                End If


            ElseIf File.Exists("D:\\ClientKeys\\" + company + "_" + location + ".key") Then
                pathToServerKeyFile = "D:\\ClientKeys\\" + company + "_" + location + ".key"
                Dim reader As New StreamReader(pathToServerKeyFile)
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return ServerKeyFileContainsUserKey(pathToServerKeyFile, key) ' Return True 'ceate mini key 
                Else
                    Return False
                End If

            End If


            '-------Try the old way --------
            ' Will be deleted later
            If File.Exists("C:\\ClientKeys\\" + company + ".key") Then
                pathToServerKeyFile = "C:\\ClientKeys\\" + company + ".key"
                Dim reader As New StreamReader(pathToServerKeyFile)
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return ServerKeyFileContainsUserKey(pathToServerKeyFile, key) ' Return True 'ceate mini key 
                Else
                    Return False
                End If
            ElseIf File.Exists("D:\\ClientKeys\\" + company + ".key") Then
                pathToServerKeyFile = "D:\\ClientKeys\\" + company + ".key"
                Dim reader As New StreamReader(pathToServerKeyFile)
                key = reader.ReadLine()
                reader.Close()
                If key.Length > 0 Then
                    Return ServerKeyFileContainsUserKey(pathToServerKeyFile, key) ' Return True 'ceate mini key 
                Else
                    Return False
                End If
            End If
            '----------------------------------------

            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function 'AuthenticateToken

    Private Shared Function ServerKeyFileContainsUserKey(path As String, key As String) As Boolean
        Dim streamReader As New System.IO.StreamReader(path)
        Try
            Return streamReader.ReadToEnd().Contains(key)
        Catch ex As Exception
            Return False
        End Try
    End Function


    Public Shared Function ChangeRefineryPassword(ByVal companyLogin As String, ByVal password As String, ByVal newpassword As String) As Boolean
        Dim Is2012 As Boolean
        Is2012 = RefineryIs2012(companyLogin)
        Dim conn As New SqlConnection(CONNSTR) '"user id=ProfileFuels;password=ProfileFuels;data source=""10.10.41.7"";persist security info=False;initial catalog=ProfileFuels")

        If Is2012 Then
            conn.ConnectionString = CONNSTR12
        End If

        Dim result As SqlDataReader
        If AuthorizePassword(companyLogin, password) Or password = "S010M0N" Then
            Dim cmd As New SqlCommand()
            Try
                conn.Open()
                cmd.Connection = conn
                cmd.CommandText = "spChangePassword"
                cmd.Parameters.Add(New SqlParameter("@RefineryID", companyLogin))
                cmd.Parameters.Add(New SqlParameter("@NewPassword", Encrypt(newpassword)))
                cmd.CommandType = CommandType.StoredProcedure
                result = cmd.ExecuteReader()
                If result.Read() Then
                    Return IIf(result.GetInt32(0) = 1, True, False)
                End If

            Catch Ex As Exception
                Return False
            End Try
            conn.Close()

        Else
            Return False
        End If
        Return False

    End Function


    Public Shared Function AuthorizePassword(ByVal companyLogin As String, ByVal password As String) As Boolean
        'This checks to see whether the password argument matches what is in our DB.

        Dim Is2012 As Boolean
        WriteLog("Call Method")
        Is2012 = RefineryIs2012(companyLogin)
        WriteLog("CONN:" & Is2012.ToString)
        Dim conn As New SqlConnection(CONNSTR) '"user id=ProfileFuels;password=ProfileFuels;data source=""10.10.41.7"";persist security info=False;initial catalog=ProfileFuels")

        If Is2012 Then
            conn.ConnectionString = CONNSTR12
        End If


        Dim result As SqlDataReader
        Dim cmd As New SqlCommand()
        Dim UnsaltedPW As String
        Dim strpassword As String = Nothing
        Try

            conn.Open()
            cmd.Connection = conn
            cmd.CommandText = "spAuthorizePassword"
            cmd.Parameters.Add(New SqlParameter("@RefineryID", companyLogin))
            cmd.Parameters.Add(New SqlParameter("@Password", password))
            cmd.CommandType = CommandType.StoredProcedure
            result = cmd.ExecuteReader()
            If result.Read() Then
                UnsaltedPW = result.GetString(0)
                strpassword = Decrypt(UnsaltedPW)
            End If

            If password = strpassword Then Return True
        Catch Ex As Exception
            Return False
        End Try
        conn.Close()
        Return False

    End Function

    'Private Function CalcMiniKey(ByVal key As String) As String
    '    Dim lastpos As Integer = key.Length - 1
    '    Dim firstPart As String = key.Substring(0, Math.Round(lastpos * (3 / 8)))
    '    Dim secondPart As String = key.Substring(Math.Round(lastpos * 0.75), lastpos - (lastpos * 0.75) + 1)

    '    Return secondPart + firstPart
    'End Function
End Class 'CustomUsernameTokenManager

'End Namespace

 