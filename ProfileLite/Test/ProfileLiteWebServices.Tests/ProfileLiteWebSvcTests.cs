﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace ProfileLiteWebServices.Tests
{
    [TestClass]
    public class ProfileLiteWebServicesUnitTests
    {
        //[TestMethod]
        //public void FailCheckAuthenticationAndAuthorizationTable1()
        //{
        //    //<System.Web.Services.WebService(Namespace:="http://solomononline.com/ProfileLiteWS/")> _
        //    using (ProfileLiteWebServices.ProfileLiteWebServices webService = new ProfileLiteWebServices.ProfileLiteWebServices())
        //    {
        //        Assert.IsTrue(webService.RunTests());
        //    }
        //}
        [TestMethod]
        public void UserAuthenticationTests()
        {
            ProfileLiteService.Service svc = new ProfileLiteService.Service();
            /*u
             Dim returnValue As Boolean = False
            Dim refineryId As String = String.Empty
            Dim activeAuthenticationAndAuthorizationRecord As Boolean
            Dim certificateRequired As Boolean
            Dim clientKeyRequired As Boolean
            Dim clientKeyFromDb As String = String.Empty
                 * */
            //'Test1: Not in table
            string refineryId = "123ABC";
            bool activeAuthenticationAndAuthorizationRecord=false;
            bool certificateRequired=false;
            bool clientKeyRequired=false;
            string clientKeyFromDb = string.Empty;
            Assert.IsTrue(TestResults.Fail ==CallCheckAuthenticationAndAuthorizationTable(refineryId,
             ref activeAuthenticationAndAuthorizationRecord, ref certificateRequired, ref clientKeyRequired, ref clientKeyFromDb), 
             "Test1 (Not in table): Failed");

            //'Test2: in table but not active
            //' INSERT INTO [ProfileFuels12].[dbo].[AuthenticationAndAuthorization] 
            //'([RefineryGroupId],[RefineryId],[Active],[CertificateRequired],[ClientKeyRequired],[ClientKey]) VALUES
            //' ('NeedCertOnServer','MISING',1,1,0,'myOpSZ7UsAl6InmNs/KtTC6yeyQURMh9GZfzJMivNn8hHkxGoXWRLTqekZEQaHgAEPFiH9ihbzb3u90cMxCYDg==');
            //'refineryId = "MISING"
            //call db and set MISING to active 0, then run this test, then put back to 1 for next test.
            Assert.IsTrue(RunSql("UPdate AuthenticationAndAuthorization set Active = 0 where RefineryId = 'MISING'"), "Sql failed");
            
            //key for RefineryID 'MISING'
            string key = "myOpSZ7UsAl6InmNs/KtTC6yeyQURMh9GZfzJMivNn8hHkxGoXWRLTqekZEQaHgAEPFiH9ihbzb3u90cMxCYDg==";

            Byte[] encodedSignedCms = new Byte[] { };
            string errorResponse = string.Empty;
            Assert.IsFalse(svc.AuthenticateByCertificateAndClientId(ref errorResponse, encodedSignedCms, key, false),
                "Test2 (in table but not active - Part 2): Failed");
            //reset in db to Active for next test
            Assert.IsTrue(RunSql("UPdate AuthenticationAndAuthorization set Active = 1 where RefineryId = 'MISING'"), "Sql failed");

            //'Test3: Active but key mismatch
            //'There is no way this situation would occur. The key that is passed in from customer
            //' is the way we determine the refnum (even if it was spoofed), and having found it in the table,
            //' it should match the record we found. 


            //'Test4:active and cert req'd but missing cert on server


            Assert.IsFalse(svc.AuthenticateByCertificateAndClientId(ref errorResponse, encodedSignedCms, key, false),
                "Test4 (active and cert req'd but missing cert on server): Failed");
       
            //'Test5: active and cert req'd and we have cert on server BUT mismatch certificates
            errorResponse = string.Empty;
            key = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            ProfileLiteSecurity.Cryptography.Certificate certHelperMismatch = new ProfileLiteSecurity.Cryptography.Certificate();
            string profileLiteCertName = "PROFILE LITE (XXLPAC)";
            string yasrefKey = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            Byte[] certMismatch  = certHelperMismatch.GetEncodedSignedCmsFromNameWithRefnum(profileLiteCertName, yasrefKey);
            Assert.IsFalse(svc.AuthenticateByCertificateAndClientId(ref errorResponse, certMismatch, key, false), 
                "Test5 (active and cert req'd and we have cert on server BUT mismatch certificates): Failed");
            
            //'Test6: active and cert req'd and we have cert on server and certificates match
            key = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";
            string yasrefCertName = "YASREF (355EUR)";
            ProfileLiteSecurity.Cryptography.Certificate certHelperMatch = new ProfileLiteSecurity.Cryptography.Certificate();
            Byte[] certMatch  = certHelperMatch.GetEncodedSignedCmsFromNameWithRefnum(yasrefCertName, yasrefKey);
            string certRead = System.Text.Encoding.Default.GetString(certMatch);
            Assert.IsTrue(svc.AuthenticateByCertificateAndClientId(ref errorResponse, certMatch, key, false),
                "Test6 (active and cert req'd and we have cert on server and certificates match): Failed");
                
        }

        [TestMethod]
        public void DataDumpLite_Test()
        {
            ProfileLiteService.Service svc = new ProfileLiteService.Service();
            string sProc = "SPREPORTDEMOKPI";
            string ds = "ACTUAL";
            string scenario = "Client";
            string currency = "USD";
            int startYear=2015 ;
            int startMonth=12;
            string studyYEar = "2010";
            string UOM = "US";
            bool includeTarget = false;
            bool includeYTD = false;
            bool includeAVG = false;
            string clientKey = "v1LSbcg0ECZqNHgF5s8foOvb0l6pLC3LUlu1Eid76+I=";

            try
            {
                System.Data.DataSet dataSet =
                svc.DataDumpLiteFromDb(sProc, ds, scenario, currency, startYear, startMonth, studyYEar, UOM, includeTarget, includeYTD, includeAVG, clientKey);
                Assert.IsTrue(dataSet.Tables.Count > 0);
                Assert.IsTrue(dataSet.Tables[0].Rows.Count > 0);
            }
            catch (Exception ex)
            {
                Assert.Fail();
            }
        }


        private TestResults CallCheckAuthenticationAndAuthorizationTable(string refineryId ,
            ref bool activeAuthenticationAndAuthorizationRecord,
            ref bool certificateRequired ,
            ref bool clientKeyRequired ,
            ref string clientKeyFromDb ) 
        {
            try
            {
                ProfileLiteService.UserAuthentication userAuth = new ProfileLiteService.UserAuthentication();
                if(! userAuth.CheckAuthenticationAndAuthorizationTable(refineryId,
                                                            ref activeAuthenticationAndAuthorizationRecord,
                                                            ref certificateRequired,
                                                            ref clientKeyRequired,
                                                            ref clientKeyFromDb))
                {
                    return TestResults.Fail;
                }
                else
                {
                    return TestResults.Pass;
                }
            } catch(Exception ex)
            {
                return TestResults.Err;
            }
        }

        private bool RunSql(string sql)
        {
            System.Data.SqlClient.SqlConnection conx = null;
            try
            {
                //"Data Source=10.10.27.45;Initial Catalog=ProfileFuels12;User ID=ProfileFuels;Password=ProfileFuels"
                string id = DevConfigHelper.ReadConfig("TestDbId");
                string pw = DevConfigHelper.ReadConfig("TestDbPassword");

                string conxString = ConfigurationManager.ConnectionStrings["ProfileFuelsConnectionString"].ToString();
                conxString = conxString.Replace("User ID=ProfileFuels", "User ID=" + id);
                conxString = conxString.Replace("Password=ProfileFuels", "Password=" + pw);
                conx = new System.Data.SqlClient.SqlConnection(conxString);
                conx.Open();
                System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(sql, conx);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conx.Close();
            }
        }


    }
    enum TestResults
    {
        Pass = 0,
        Fail = 1,
        Err = 2
    }
}
