﻿Imports System.IO
Imports System.Configuration

Public Class Tests
    Public Function UserAuthenticationTest1() As Boolean
        Dim returnValue As Boolean = False

        Dim refineryId As String = String.Empty
        Dim activeAuthenticationAndAuthorizationRecord As Boolean
        Dim certificateRequired As Boolean
        Dim clientKeyRequired As Boolean
        Dim clientKeyFromDb As String = String.Empty

        'Test1: Not in table
        refineryId = "123ABC"
        If TestResults.Fail <> CallCheckAuthenticationAndAuthorizationTable(refineryId, _
         activeAuthenticationAndAuthorizationRecord, certificateRequired, clientKeyRequired, clientKeyFromDb) Then
            WriteLog("UserAuthenticationTest1 Test1 (Not in table): Failed")
            Return returnValue
        End If

        'Test2: in table but not active
        'INSERT INTO [ProfileFuels12].[dbo].[AuthenticationAndAuthorization] 
        '([RefineryGroupId],[RefineryId],[Active],[CertificateRequired],[ClientKeyRequired],[ClientKey]) VALUES
        ' ('INACTIVE','INACTIV',0,0,0,'KEY');
        refineryId = "INACTIV"
        If TestResults.Pass <> CallCheckAuthenticationAndAuthorizationTable(refineryId, _
            activeAuthenticationAndAuthorizationRecord, certificateRequired, clientKeyRequired, clientKeyFromDb) Then
            WriteLog("UserAuthenticationTest1 Test1 (in table but not active): Failed")
            Return returnValue
        End If
        If activeAuthenticationAndAuthorizationRecord <> False Then
            WriteLog("UserAuthenticationTest1 Test1 (in table but not active - Part 2): Failed")
            Return returnValue
        End If


        'Test3: Active but key mismatch
        'There is no way this situation would occur. The key that is passed in from customer
        ' is the way we determine the refnum (even if it was spoofed), and having found it in the table,
        ' it should match the record we found. 


        'Test4:active and cert req'd but missing cert on server
        ' INSERT INTO [ProfileFuels12].[dbo].[AuthenticationAndAuthorization] 
        '([RefineryGroupId],[RefineryId],[Active],[CertificateRequired],[ClientKeyRequired],[ClientKey]) VALUES
        ' ('NeedCertOnServer','MISING',1,1,0,'myOpSZ7UsAl6InmNs/KtTC6yeyQURMh9GZfzJMivNn8hHkxGoXWRLTqekZEQaHgAEPFiH9ihbzb3u90cMxCYDg==');
        'refineryId = "MISING"
        Dim errorResponse As String = String.Empty
        Dim key As String = "myOpSZ7UsAl6InmNs/KtTC6yeyQURMh9GZfzJMivNn8hHkxGoXWRLTqekZEQaHgAEPFiH9ihbzb3u90cMxCYDg=="
        Dim encodedSignedCms As Byte() = New Byte() {}
        Using profileLiteWebService4 As New ProfileLiteWebServices()
            If profileLiteWebService4.AuthenticateByCertificateAndClientId(errorResponse, encodedSignedCms, key, False) Then
                WriteLog("UserAuthenticationTest1 Test4 (active and cert req'd but missing cert on server): Failed")
                Return returnValue
            End If
        End Using

        'Test5: active and cert req'd and we have cert on server BUT mismatch certificates
        errorResponse = String.Empty
        key = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5"
        Dim certHelperMismatch As New ProfileLiteSecurity.Cryptography.Certificate()
        Dim profileLiteCertName As String = "PROFILE LITE (XXLPAC)"
        Dim yasrefKey As String = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5"
        Dim certMismatch As Byte() = certHelperMismatch.GetEncodedSignedCmsFromNameWithRefnum(profileLiteCertName, yasrefKey)
        Using profileLiteWebService5 As New ProfileLiteWebServices()
            If profileLiteWebService5.AuthenticateByCertificateAndClientId(errorResponse, certMismatch, key, False) Then
                WriteLog("UserAuthenticationTest1 Test5 (active and cert req'd and we have cert on server BUT mismatch certificates): Failed")
                Return returnValue
            End If
        End Using

        'Test6: active and cert req'd and we have cert on server and certificates match
        key = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5"
        Dim yasrefCertName As String = "YASREF (355EUR)"
        Dim certHelperMatch As New ProfileLiteSecurity.Cryptography.Certificate()
        Dim certMatch As Byte() = certHelperMatch.GetEncodedSignedCmsFromNameWithRefnum(yasrefCertName, yasrefKey)
        Dim certRead As String = System.Text.Encoding.Default.GetString(certMatch)
        Using profileLiteWebService6 As New ProfileLiteWebServices()
            If Not profileLiteWebService6.AuthenticateByCertificateAndClientId(errorResponse, certMatch, key, False) Then
                WriteLog("UserAuthenticationTest1 Test6 (active and cert req'd and we have cert on server and certificates match): Failed")
                Return returnValue
            End If
        End Using

        'default if passed all so far
        returnValue = True

        Return returnValue
    End Function




    Private Function CallCheckAuthenticationAndAuthorizationTable(refineryId As String,
                                                             ByRef activeAuthenticationAndAuthorizationRecord As Boolean,
                                                             ByRef certificateRequired As Boolean,
                                                             ByRef clientKeyRequired As Boolean,
                                                             ByRef clientKeyFromDb As String) As TestResults
        Try
            Dim userAuth As New UserAuthentication
            If Not userAuth.CheckAuthenticationAndAuthorizationTable(refineryId,
                                                              activeAuthenticationAndAuthorizationRecord,
                                                              certificateRequired,
                                                              clientKeyRequired,
                                                              clientKeyFromDb) Then
                Return TestResults.Fail
            Else
                Return TestResults.Pass
            End If
        Catch ex As Exception
            Return TestResults.Err
        End Try
    End Function

    Private Sub WriteLog(st As String)
        Try
            Dim sw As StreamWriter = New StreamWriter(ConfigurationManager.AppSettings.Item("LogFilePath").ToString(), True)
            sw.WriteLine(Now.ToString & ": " & st)
            sw.Flush()
            sw.Close()
        Catch
        End Try
    End Sub
End Class




Enum TestResults
    Pass = 0
    Fail = 1
    Err = 2
End Enum
