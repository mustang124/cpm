﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using xl =  Microsoft.Office.Interop.Excel;

namespace ProfileLiteAddinBase.Test
{
    [TestClass]
    public class ProfileLiteAddinBaseTest
    {
        ProfileLiteAddInBase.ProfileLiteAddinRoot _profileLiteUtilities = null;

        [TestInitialize]
        public void CreateObject()
        {
            _profileLiteUtilities = new ProfileLiteAddInBase.ProfileLiteAddinRoot();
        }


        [TestMethod]
        public void TestFuelsDbPopulation()
        {
            _profileLiteUtilities.AddSettings("CompanyName", "Location", "CoordName", "1", "1", "1", "1", "1", "1", false, 1, 2015,
                "Actual", "1", "1");
            _profileLiteUtilities.AddUserDefined("Loss", "Total Fuels Refinery Physical Hydrocarbon Loss",
                6109, 0, 0, 0, 0);
            _profileLiteUtilities.AddUserDefined("Flare", "Estimated Flare Losses Included in Above",
                 2993, 0, 0, 0, 0);
            _profileLiteUtilities.AddMaintRoutFull(11001, "CDU", 100, 0, 0, 0, 0, 0, 200, 0, 0, 300, 0, 0, 0, 0);
            PrivateObject obj = new PrivateObject(_profileLiteUtilities);
            var retVal = obj.Invoke("ReturnEntireDataset");
            System.Data.DataSet dsp = (DataSet)retVal;
            Assert.IsTrue(dsp.Tables["Settings"].Rows[0]["Company"].ToString() == "CompanyName");

            DataTable dt = dsp.Tables["UserDefined"];
            Assert.IsTrue(dt.Columns["DecPlaces"].DataType.ToString() == "System.Byte");


            //Yasref
            String CompanyLocationRefineryid = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";

            //"Kuwait National Petroleum Company$Shuaiba Refinery$
            //String CompanyLocationRefineryid = "QWsmTvZ7cjMFNmmGsPZmlP5/tQl8IAn7MZ+vg/76aRWIkq1CJLnMFjPwo9kHMAY+uC/iBaNsXEYsKBnr59iCOPxU7c5FNBkC";
            //_profileLiteUtilities.SubmitData( CompanyLocationRefineryid); //this doesn't return anything so would need to check db for records after this-or put break in web service.

        }

        [TestMethod]
        public void TestLubesDbPopulation()
        {
            _profileLiteUtilities.AddSettings("BLBOC", "SITRAH", "SUHA AHMED ALSHAFEI", "1", "1", "1", "1", "1", "1", false, 1, 2015, "TEST", "1", "1");
            _profileLiteUtilities.AddConfigRS("RSCRUDE", 1, 0, "0", 0, 0);
            _profileLiteUtilities.AddConfigRS("RSPROD", 2, 0, "0", 0, 0);
            _profileLiteUtilities.AddInventory("FDS", 0, 0, 0, 46, 199926);
            _profileLiteUtilities.AddInventory("INT", 0, 0, 0, 54, 333);
            _profileLiteUtilities.AddOpexAll(444, 16, 68, 1, 0, 920);
            _profileLiteUtilities.AddPersAll("OCCPO", 13796, 7562, 0);
            _profileLiteUtilities.AddYield_RM("MPROD", "L117", 297658, "", 0, 843, 39918);
            _profileLiteUtilities.AddEnergy("PUR", "NA", "FG_", 74567, 0);
            _profileLiteUtilities.AddElectric("SOL", "OTH", "ELE", 0, 1000, 0);

            _profileLiteUtilities.AddConfig(10002, "LBOU-RS", "CDWAX", "ISO", 10000, 84, 0, 0, 0, 0, 40);
            _profileLiteUtilities.AddConfig(90001, "", "STEAMGEN", "SFB", 19, 20, 0, 0,
            0, 0, 0, 0, "", "", 0, 0, 21);
            _profileLiteUtilities.AddConfig(0, "", "FTCOGEN", "AIR", 120, 0, 212, 50, 0, 0, 40,
            0, "", "", 0, 0, 100); //, Empty, Empty, Empty

            /*
             *this if failing - 20 args
            ProfileUtilities.AddConfig UnitID, UnitName, ProcessID, ProcessType, RptCap, UtilPcnt, RptStmCap, StmUtilPcnt, Empty, Empty, totalHoursStaffedPerWeek, _
            Empty, Empty, Empty, Empty, Empty, capacityAllocatedToLubePct, Empty, Empty, Empty
            */
            //interface:
            /*
            Sub AddConfig(ByVal UnitID As Integer, ByVal UnitName As String, ByVal ProcessID As String, ByVal ProcessType As String, _
                  ByVal RptCap As Single, ByVal UtilPcnt As Single, ByVal RptStmCap As Single, ByVal StmUtilPcnt As Single, _
                  Optional ByVal InServicePcnt As Decimal = 0, Optional ByVal YearsOper As Decimal = 0, Optional ByVal MHPerWeek As Single = 0, _
                  Optional ByVal PostPerShift As Decimal = 0, Optional ByVal BlockOp As String = "", Optional ByVal ControlType As String = "", _
                  Optional ByVal CapClass As Byte = Nothing, Optional ByVal UtilCap As Single = 0, Optional ByVal StmUtilCap As Single = 0, _
                  Optional ByVal AllocPcntOfTime As Decimal = 0, Optional ByVal AllocPcntOfCap As Decimal = 0, Optional ByVal AllocUtilPcnt As Decimal = 0)
            */
            _profileLiteUtilities.AddConfig(4000, "UnitName", "ProcessID", "ProcessType",
             0, 0, 0, 0, 0, 0, 0, 0, "BlockO", "CTyp", 0, 0, 0, 0, 0, 0);



            _profileLiteUtilities.AddProcessData(10001, "BC_OperPcnt", 100);
            _profileLiteUtilities.AddProcessData(10002, "NC_LN_OperPcnt", 63);
            //this causes error during merge in ws (0 (empty) Unit ID):
            //_profileLiteUtilities.AddProcessData(0, "FeedH2", 12);
            _profileLiteUtilities.AddProcessData(10003, "FeedRateGas", 13);

            _profileLiteUtilities.AddMaintTA(10001, 2, "RERUN", new DateTime(2015, 03, 01), 2, 3, 5, 6, new DateTime(2015, 02, 01), 0, 4);
            _profileLiteUtilities.AddMaintTA(100002, 1, "TEST", new DateTime(2015, 03, 01), 0, 0, 0, 0, new DateTime(2015, 03, 01), 0, 2);
            //ProfileUtilities.AddMaintRoutFull UnitID, ProcessID, RoutCostLocal, Empty, Empty, Empty, Empty, Empty, RegDown, Empty, Empty, MaintDown, Empty, Empty, Empty, Empty
            _profileLiteUtilities.AddMaintRoutFull(10001, "RERUN", 609, 0, 0, 0, 0, 0, 11, 0, 0, 10, 0, 0, 0, 0);
            _profileLiteUtilities.AddUserDefined("Loss", "Total Lube Refinery Physical Hydrocarbon Loss",
                238.8, 0, 0, 0, 0);
            _profileLiteUtilities.AddUserDefined("Flare", "Estimated Flare Losses Included in Above",
                 119, 0, 0, 0, 0);
            //---------------------------------------
            PrivateObject obj = new PrivateObject(_profileLiteUtilities);
            var retVal = obj.Invoke("ReturnEntireDataset");
            System.Data.DataSet dsp = (DataSet)retVal;
            Assert.IsTrue(dsp.Tables["Settings"].Rows[0]["Company"].ToString() == "BLBOC");
            int count = 0;
            foreach (DataRow dr in dsp.Tables["ConfigRS"].Rows)
            {
                if (dr["ProcessID"].ToString() == "RSCRUDE") { count++; }
                if (dr["ProcessID"].ToString() == "RSPROD") { count++; }
            }
            Assert.AreEqual(count, 2);
            foreach (DataRow dr in dsp.Tables["Inventory"].Rows)
            {
                if (dr["Inven"].ToString() == "199926") { count++; }
                if (dr["TankType"].ToString() == "INT") { count++; }
            }

            //Assert.IsTrue(dsp.Tables["OpexAll"].Rows[0][""].ToString() == "");
            Assert.IsTrue(dsp.Tables["Pers"].Rows[0]["Contract"].ToString() == "7562");
            Assert.IsTrue(dsp.Tables["Yield_RM"].Rows[0]["Category"].ToString() == "MPROD");
            Assert.IsTrue(dsp.Tables["Yield_RM"].Rows[0]["MT"].ToString() == "39918");
            Assert.IsTrue(dsp.Tables["Energy"].Rows[0]["TransType"].ToString() == "PUR");
            Assert.IsTrue(dsp.Tables["Electric"].Rows[0]["TransType"].ToString() == "SOL");

            //Assert.IsTrue(dsp.Tables["Config"].Rows.Count==4);
            Assert.IsTrue(dsp.Tables["Opexall"].Rows.Count == 5);
            Assert.IsTrue(dsp.Tables["Electric"].Rows.Count == 1);
            count = 0;
            foreach (DataRow dr in dsp.Tables["MaintTA"].Rows)
            {
                if (dr["TALaborCostLocal"].ToString() == "4") { count++; }
            }
            Assert.AreEqual(count, 1);
            Assert.IsTrue(dsp.Tables["MaintRout"].Rows.Count == 1);
            Assert.IsTrue(dsp.Tables["UserDefined"].Rows.Count == 2);
            Assert.IsTrue(dsp.Tables["MaintRout"].Rows.Count == 1);

            //DataTable dt = dsp.Tables["UserDefined"]; Assert.IsTrue(dt.Columns["DecPlaces"].DataType.ToString() == "System.Byte");


            //String CompanyLocationRefineryid = "RBvmBYi9YZu7o0CyY8VlFaY6Z+XfWjE2L94XKyCSW5A=";
            //_profileLiteUtilities.SubmitData(CompanyLocationRefineryid); //this doesn't return anything so would need to check db for records after this-or put break in web service.

            //############### also change to 2010 and add in a MaintTA record, ensure it works with and witout the new field TALaborCostLocal

        }

        [TestMethod]
        public void SubmitDataTest()
        {
            string addinTestresult = _profileLiteUtilities.TestAddIn();
            Assert.IsTrue(addinTestresult == "TestAddIn ProfileLiteAddin2010 = Success!");
            /*
            string wsTestResult = _profileLiteUtilities.TestWebService("XXPAC", "10.10.10.10");
            DateTime result = DateTime.Parse(wsTestResult);
            Assert.IsTrue(result > DateTime.Now.AddMinutes(-1));
                 */      
            _profileLiteUtilities.SubmitData("X3bBmEDEQQRI3BhJ5IKPGqbQHeT6y5nBDJcIwWBlvm8=");

        }

        [TestMethod]
        public void AddInAndWsTests()
        {
            string addinTestresult = _profileLiteUtilities.TestAddIn();
            Assert.IsTrue(addinTestresult == "TestAddIn ProfileLiteAddin2010 = Success!");
            
            
            //new way with only 2 non-optional:
            //NOTE:  If this errors, then try these steps:
            //1. Run the project, so the Web Service's page shows.
            //2. Open another instance of IE and pull up the web services' page.
            //3. Shut down the project, but leave the other instance of IE up.
            //4. Run the unit test.
            string wsTestResult = _profileLiteUtilities.TestWebService("XXPAC", "10.10.10.10");
            DateTime result = DateTime.Parse(wsTestResult);
            Assert.IsTrue(result > DateTime.Now.AddMinutes(-1));

            //old way with no optionals
            string wsTestResult2 = _profileLiteUtilities.TestWebService(
                "XXPAC", "10.10.10.10", "test", "test", "testpc", "ProfileLiteAddinBase test", "test",
                "", "", "", "", "");
            result = DateTime.Parse(wsTestResult2);
            Assert.IsTrue(result > DateTime.Now.AddMinutes(-1));
        }

        [TestMethod]
        public void TestThaiOilDownload()
        {
            string clientKey = "O9YsfgcCbV3TjlgESYzCV0/AfYrpbHHlvTqYUUcDYWR2G6fXfzINWg==";
            string addinTestresult = _profileLiteUtilities.TestAddIn();
            Assert.IsTrue(addinTestresult == "TestAddIn ProfileLiteAddin2010 = Success!");
            //Microsoft.Office.Interop.Excel.Worksheet ws;

            /*
            //this should err since I'm passing in null instead of a WorkSheet....
            try{
                _profileLiteUtilities.DownloadResults(clientKey,
                    "Actual", 2015, 1, "2008", "USD", "MET", "spReportThaiOilKPI", "Field", "", null);
                //ProfileUtilities.DownloadResults GetEncryptedKey, "Actual", PeriodYear, PeriodMonth, FactorSet, currencyCode, UOM, "spReportThaiOilKPI", "Field", "", Dashboard
            }
            catch(Exception exWebSvc)
            {
            
                string firstErr = exWebSvc.Message;
                */
            try
                {
                    ProfileLiteService.Service service = new ProfileLiteService.Service();
                    service.DataDumpLite("spReportThaiOilKPI", "Actual", "Client", "USD", 2015, 1, "2008", "MET",
                                     true, true, true, clientKey);
                }
                catch(Exception ex)
                {
                    string msg = ex.Message;
                }
            /*
            }
             * */
            Assert.Inconclusive("this should err since I'm passing in null instead of a WorkSheet....");
        }

        [TestMethod]
        public void TestBashneft_UfalDownload()
        {
            //"IvAl8g53JdmqbVDUAslRBzggsfop/7V6EcYv3hdaJ7c=","Actual",2016,4,"2012","RUB","MET","spReportBashneftKPI2", "Field", "", Dashboard
            string clientKey = "IvAl8g53JdmqbVDUAslRBzggsfop/7V6EcYv3hdaJ7c=";
            string addinTestresult = _profileLiteUtilities.TestAddIn();
            Assert.IsTrue(addinTestresult == "TestAddIn ProfileLiteAddin2010 = Success!");
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

            Microsoft.Office.Interop.Excel.Workbook wb = excelApp.Workbooks.Open(@"C:\Users\sfb.DC1\Desktop\UfaProblem2016-08-30\Solomon Profile Lite (Ufa) New 2016 апрель.xlsm");

            var ws = wb.Sheets["Dashboard"];
            excelApp.Visible = true;


            try
            {
                ProfileLiteService.Service service = new ProfileLiteService.Service();
                DataSet ds = service.DataDumpLite("spReportBashneftKPI2", "Actual", "Client", "RUB", 2016, 4, "2012", "MET",
                                 true, true, true, clientKey);
                WriteToXlsSheet(ds, "Field", "", ws);
                //WriteToXlsSheet(ds, "Dump", "ChartData", REF);

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            /*
            }
             * */
            Assert.Inconclusive("this should err since I'm passing in null instead of a WorkSheet....");
        }

        [TestMethod]
        public void TestIrvingOilDownload()
        {
            string clientKey = "ZV0zIiDpPEnDZEmZmhOOrRf2i0aHsbu8CPvesM0iH6DJcfvgQApY8A==";
            string addinTestresult = _profileLiteUtilities.TestAddIn();
            Assert.IsTrue(addinTestresult == "TestAddIn ProfileLiteAddin2010 = Success!");
            //Microsoft.Office.Interop.Excel.Worksheet ws;

            /*
            //this should err since I'm passing in null instead of a WorkSheet....
            try{
                _profileLiteUtilities.DownloadResults(clientKey,
                    "Actual", 2015, 1, "2008", "USD", "MET", "spReportThaiOilKPI", "Field", "", null);
                //ProfileUtilities.DownloadResults GetEncryptedKey, "Actual", PeriodYear, PeriodMonth, FactorSet, currencyCode, UOM, "spReportThaiOilKPI", "Field", "", Dashboard
            }
            catch(Exception exWebSvc)
            {
            
                string firstErr = exWebSvc.Message;
                */
            try
            {
                ProfileLiteService.Service service = new ProfileLiteService.Service();
                service.DataDumpLite("spReportIrvingKPI", "ACTUAL", "Client", "USD", 2017, 2, "2014", "US",
                                 true, true, true, clientKey);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            /*
            }
             * */
            Assert.Inconclusive("this should err since I'm passing in null instead of a WorkSheet....");
        }

        [TestMethod]
        public void TestDelekTylerDownload()
        {
            string xlsmPath = @"C:\tfs\CPA\ProfileLite\Dev\ProfileLiteAddins\ProfileLiteAddinBase.Test\Profile Lite - Delek Tyler 2017-05.xlsm";
            string clientKey = "v1LSbcg0ECZqNHgF5s8foOvb0l6pLC3LUlu1Eid76+I=";
            string addinTestresult = _profileLiteUtilities.TestAddIn();
            Assert.IsTrue(addinTestresult == "TestAddIn ProfileLiteAddin2010 = Success!");
            xl.Application xla = new xl.Application();
            xl.Workbook wb = xla.Workbooks.Open(xlsmPath);
            xla.Visible = true;
            xl.Worksheet ws = wb.Sheets["Dashboard"];
                
            try{
                _profileLiteUtilities.DownloadResults(clientKey,
                    "Actual", 2017, 5, "2010", "USD", "US", "spReportDemoKPI", "Field", "", ws);
                //ProfileUtilities.DownloadResults GetEncryptedKey, "Actual", PeriodYear, PeriodMonth, FactorSet, currencyCode, UOM, "spReportThaiOilKPI", "Field", "", Dashboard
            }
            catch(Exception exWebSvc)
            {
            
                string firstErr = exWebSvc.Message;
            }
            wb.Save();

        }


        private void WriteToXlsSheet(DataSet dsResults, string WriteMethod , string WriteRange, xl.Worksheet ws)
        {
            /*
                ProfileUtilities.DownloadResults ThisWorkbook.Names("EncryptedKey").RefersToRange.Value, dataSet, PeriodYear, PeriodMonth, FactorSet, KPICurrencyCode, UOM, "spReportBashneftKPI2", "Field", "", Dashboard
                ProfileUtilities.DownloadResults ThisWorkbook.Names("EncryptedKey").RefersToRange.Value, dataSet, PeriodYear, PeriodMonth, FactorSet, KPICurrencyCode, UOM, "spReportBashneftChartData2", "Dump", "ChartData", REF
            */
            
           if( WriteMethod == "Dump" )
           {
               /*
               //DataRow dr; 
               long r; Range rngRow;
                //Dim r As Long, dr As DataRow, rngRow As Excel.Range
                Range rng= ws.Range[WriteRange];
                System.Threading.Thread.CurrentThread.CurrentCulture =new System.Globalization.CultureInfo("en-US");
                rng.ClearContents();
                r = 1;
                foreach (DataRow dr in dsResults.Tables[0].Rows)
                {
                    try
                    {
                        //rngRow = rng.Rows(r)
                        rngRow = ws.Range[];


                        rngRow.Value2 = dr.ItemArray;
                        r = r + 1;
                    }
                    catch (Exception ex)
                    {                   
                  
                        //_notes = "Error with 'Dump' data in DownloadResults: Excel Row " + r.ToString()
                        //Try
                        //    _notes += ", value " + rngRow.Value2.ToString()
                        //Catch
                        //End Try
                        //uploadws.WriteActivityLog(String.Empty, _refineryId, _callerIP, _userID, _computerName, SERVICE_NAME, _methodName, _entityName, _periodStart, _periodEnd, _notes, _status)
                  
                        string err = ex.Message;
                        r = r + 1; // 'in case of error, won't increment above, so do it here.
                    }
                }
                System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture;
               */
           }        
           else if (WriteMethod == "Field")
           {
                //DataColumn fld; //, data As DataRow
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                DataRow data = dsResults.Tables[0].Rows[0];
                string tracking = string.Empty;
               string errors = string.Empty;
                foreach (DataColumn fld in dsResults.Tables[0].Columns)
                {
                    try
                    {
                        try
                        {
                            tracking += fld.ColumnName.ToString() + "|" + data[fld.ColumnName].ToString() + Environment.NewLine;
                        }
                        catch { }
                        ws.Range[fld.ColumnName].Value2 = data[fld.ColumnName];
                    }
                    catch (Exception ex)
                    {
                        errors += "Error with 'Field' data in DownloadResults: " + data[fld.ColumnName].ToString() + ", " + fld.ColumnName.ToString() + Environment.NewLine;
                        //uploadws.WriteActivityLog(String.Empty, _refineryId, _callerIP, _userID, _computerName, SERVICE_NAME, _methodName, _entityName, _periodStart, _periodEnd, _notes, _status)
                     //'Exit Sub 'removing Exit Sub at Richard's request
                    }
                }
                //System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture;
            }
               /*
            else
           {
                _notes = (vbObjectError + 5).ToString() + " , Download, Invalid write method specified"
                uploadws.WriteActivityLog(String.Empty, _refineryId, _callerIP, _userID, _computerName, SERVICE_NAME, _methodName, _entityName, _periodStart, _periodEnd, _notes, _status)
                Err.Raise(vbObjectError + 5, "Download", "Invalid write method specified")
            End If
            System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture
            dsResults = Nothing
            _status = "SUCCESS"
            }
            */
        }

        [TestMethod]
        public void WinformShow()
        {     
            /*
            string clientKey = "SUKEohFspIafM1KDHRhp3UuTcQAKR93l8+0iWGifV00=";
            String refineryID = "XXPAC";
            String location = "DALLAS";
            */
                        
            /*
            string clientKey = "RfLIRGOEX5ltaJ8wbAoDgYtZJaIKbg1VLmhhnRuoltd5ML80f3dzkMdWKArjcZDC";
                    Company	"PROFILE LITE"	String
                    Location	"SAMPLE REFINERY"	String
                    Password	"testtest"	String
                    RefNum	"XXLPAC"	String

            Need install .cer in LocalMachine/Trusted People
            Need install .pfx in LocalMachine/Personal
            */


            //YASREF (YASREF (355EUR))
            string clientKey = "V9UQixOjBFPRDQk6VYM0NnAryy+FvA/so2iqsYwtJZPfqQfexPjFcYaJS9Z7XlO5";


           // 'What about Thai Oil?????
            clientKey = "O9YsfgcCbV3TjlgESYzCV0/AfYrpbHHlvTqYUUcDYWR2G6fXfzINWg==";  //thaioil
            //           O9YsfgcCbV3TjlgESYzCV0/AfYrpbHHlvTqYUUcDYWR2G6fXfzINWg==

            

            String reportCode = string.Empty;
            DateTime periodStart = new DateTime(2016,01,01);
            String uOM = "Metric";
            String currencyCode = "USD"; // "THB";
            String studyYear = "2012"; // "2015";
            String dataSet = "ACTUAL";
            String scenario = string.Empty;
            Boolean includeTarget = false;
            Boolean includeAvg = true;
            Boolean includeYTD = true;
            //var z = String.Format("m/d/yyyy hh:mm:ss AMPM", DateTime.Now.ToString());
            //z = DateTime.Now.ToString("M/d/yyyy  hh:mm:ss tt");
            //ProfileLiteWinForms.WinformsManager formsManager = new ProfileLiteWinForms.WinformsManager("Profile Reporter");            
            
            //TO GET FORM TO SHOW FROM UNIT TEST:
            //-easiest way is to add this line of code (form.Load += (sender, e) => (sender as WebBrowser1).Visible = true;) after constructor:
            /*
            form = new WebBrowser1();
            form.Load += (sender, e) => (sender as WebBrowser1).Visible = true;   //only use this line for unit testing to show form.
            form.PrepForm(clientKey, refineryID, location, UOM, currencyCode, studyYear, dataSet, Scenario, includeTarget, includeAvg, includeYTD);
            form.ShowDialog();
            */
            /*
            string errors = formsManager.ShowReport(clientKey,refineryID,
                location,uOM,currencyCode,studyYear,dataSet,scenario,includeTarget,
                includeAvg,includeYTD);
            */

            //'test
            //studyYear = "";
            //scenario = "";
            /*
            //DELEK TYLER
            clientKey = "v1LSbcg0ECZqNHgF5s8foOvb0l6pLC3LUlu1Eid76+I=";
            reportCode = string.Empty;
            periodStart = new DateTime(2015, 12, 01);
            uOM = "US";
            currencyCode = "USD";
            studyYear = "2012"; 
            includeTarget = false;
            includeAvg = true;
            includeYTD = true;
            */
            //String dataSet = "ACTUAL";
            //tring scenario = string.Empty;

            /*
            //for troubleshooting NIS
            clientKey = "+/MHGpmhZWvQvGBCoT/XWQLF5EN8rc9cpPVSZ/qjWryU+k/xm73VHw==";
            uOM = "MET";
            currencyCode = "USD";
            studyYear = "2012";
            dataSet = "ACTUAL";
            scenario = string.Empty;
            includeTarget = false;
            includeAvg = true;
            includeYTD = true;
            */

            string errors = _profileLiteUtilities.ShowProfileReport(clientKey,uOM,
                currencyCode,studyYear,dataSet,scenario,includeTarget,
                includeAvg,includeYTD);

            Assert.IsFalse(errors.Length > 0);
        }

        [TestMethod]
        public void WinformShowIrvingOil()
        {
            string clientKey = "ZV0zIiDpPEnDZEmZmhOOrRf2i0aHsbu8CPvesM0iH6DJcfvgQApY8A==";

            String reportCode = string.Empty;
            DateTime periodStart = new DateTime(2016, 02, 01);
            String uOM = "Metric";
            String currencyCode = "USD";
            String studyYear = "2012"; 
            String dataSet = "ACTUAL";
            String scenario = string.Empty;
            Boolean includeTarget = false;
            Boolean includeAvg = true;
            Boolean includeYTD = true;
            //var z = String.Format("m/d/yyyy hh:mm:ss AMPM", DateTime.Now.ToString());
            //z = DateTime.Now.ToString("M/d/yyyy  hh:mm:ss tt");
            //ProfileLiteWinForms.WinformsManager formsManager = new ProfileLiteWinForms.WinformsManager("Profile Reporter");            

            //TO GET FORM TO SHOW FROM UNIT TEST:
            //-easiest way is to add this line of code (form.Load += (sender, e) => (sender as WebBrowser1).Visible = true;) after constructor:
            /*
            form = new WebBrowser1();
            form.Load += (sender, e) => (sender as WebBrowser1).Visible = true;   //only use this line for unit testing to show form.
            form.PrepForm(clientKey, refineryID, location, UOM, currencyCode, studyYear, dataSet, Scenario, includeTarget, includeAvg, includeYTD);
            form.ShowDialog();
            */

            string errors = _profileLiteUtilities.ShowProfileReport(clientKey, uOM,
                currencyCode, studyYear, dataSet, scenario, includeTarget,
                includeAvg, includeYTD);

            Assert.IsFalse(errors.Length > 0);
        }



        [TestCleanup]
        public void DisposeObject()
        {
            _profileLiteUtilities.Dispose();
        }
    }
}
