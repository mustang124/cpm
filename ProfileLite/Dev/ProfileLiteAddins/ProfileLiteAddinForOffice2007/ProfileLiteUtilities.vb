﻿'2007
Imports System.Data
Imports System.Runtime.InteropServices
Imports Excel = Microsoft.Office.Interop.Excel
Imports ExcelTools = Microsoft.Office.Tools.Excel
Imports System.Configuration

Imports System.Security.Cryptography.X509Certificates
Imports ProfileLiteAddinBase


<ComVisible(True)> _
<ClassInterface(ClassInterfaceType.None)> _
Public Class ProfileLiteUtilities
    Implements IProfileLiteUtilities, IDisposable

    'Private ds As New dsUpload
    Private base As New ProfileLiteAddInBase.ProfileLiteAddinRoot()
    Dim tracingInfo As New System.Text.StringBuilder()

    Function AuthorizeRefineryLogin(ByVal ClientId As String, ByVal Password As String) As Boolean Implements IProfileLiteUtilities.AuthorizeRefineryLogin
        Return base.AuthorizeRefineryLogin(ClientId, Password)
    End Function
    Function ChangeRefineryPassword(ByVal ClientId As String, ByVal Password As String, ByVal NewPassword As String, ByVal NewPassword2 As String) As Boolean Implements IProfileLiteUtilities.ChangeRefineryPassword
        Return base.ChangeRefineryPassword(ClientId, Password, NewPassword, NewPassword2)
    End Function
    Sub ClearAllData() Implements IProfileLiteUtilities.ClearAllData
        base.ClearAllData()
    End Sub

    Public Sub AddSettings(ByVal Company As String, ByVal Location As String, _
                        ByVal CoordName As String, ByVal CoordTitle As String, _
                        ByVal CoordPhone As String, ByVal CoordEmail As String, _
                        ByVal RptCurrency As String, ByVal RptCurrencyT15 As String, _
                        ByVal UOM As String, ByVal FuelsLubesCombo As Boolean, _
                        ByVal PeriodMonth As Integer, ByVal PeriodYear As Integer, _
                        ByVal Dataset As String, ByVal BridgeVersion As String, _
                        ByVal ClientVersion As String) Implements IProfileLiteUtilities.AddSettings

        base.AddSettings(Company, Location, _
                        CoordName, CoordTitle, _
                        CoordPhone, CoordEmail, _
                        RptCurrency, RptCurrencyT15, _
                        UOM, FuelsLubesCombo, _
                        PeriodMonth, PeriodYear, _
                        Dataset, BridgeVersion, _
                        ClientVersion)

    End Sub

    Public Sub AddConfig(ByVal UnitID As Integer, ByVal UnitName As String, ByVal ProcessID As String, ByVal ProcessType As String, _
                         ByVal RptCap As Single, ByVal UtilPcnt As Single, ByVal RptStmCap As Single, ByVal StmUtilPcnt As Single, _
                         Optional ByVal InServicePcnt As Decimal = 100, Optional ByVal YearsOper As Decimal = 0, Optional ByVal MHPerWeek As Single = 0, _
                  Optional ByVal PostPerShift As Decimal = 0, Optional ByVal BlockOp As String = "", Optional ByVal ControlType As String = "", _
                  Optional ByVal CapClass As Byte = Nothing, Optional ByVal UtilCap As Single = 0, Optional ByVal StmUtilCap As Single = 0, _
                  Optional ByVal AllocPcntOfTime As Decimal = 100, Optional ByVal AllocPcntOfCap As Decimal = 100, Optional ByVal AllocUtilPcnt As Decimal = 0) Implements IProfileLiteUtilities.AddConfig
        base.AddConfig(UnitID, UnitName, ProcessID, ProcessType, _
            RptCap, UtilPcnt, RptStmCap, StmUtilPcnt, _
            InServicePcnt, YearsOper, MHPerWeek, _
            PostPerShift, BlockOp, ControlType, _
            CapClass, UtilCap, StmUtilCap, _
            AllocPcntOfTime, AllocPcntOfCap, AllocUtilPcnt)
    End Sub

    Public Sub AddConfigRS(ByVal ProcessID As String, ByVal RailcarBBL As Single, ByVal TankTruckBBL As Single, ByVal TankerBerthBBL As String, ByVal OffshoreBuoy As Single, ByVal BargeBerthBBL As Single) Implements IProfileLiteUtilities.AddConfigRS
        base.AddConfigRS(ProcessID, RailcarBBL, TankTruckBBL, TankerBerthBBL, OffshoreBuoy, BargeBerthBBL)
    End Sub

    Public Sub AddInventory(ByVal TankType As String, ByVal SortKey As Byte, ByVal FuelsStorage As Double, ByVal NumTanks As Int16, ByVal AvgLevel As Single, Optional ByVal Inven As Single = 0, Optional ByVal TotStorage As Double = 0) Implements IProfileLiteUtilities.AddInventory
        base.AddInventory(TankType, SortKey, FuelsStorage, NumTanks, AvgLevel, Inven, TotStorage)
    End Sub

    Public Sub AddProcessData(ByVal UnitID As Integer, ByVal pProperty As String, ByVal RptValue As Single) Implements IProfileLiteUtilities.AddProcessData
        base.AddProcessData(UnitID, pProperty, RptValue)
    End Sub

    Public Sub AddOpexAll(ByVal OCCSal As Single, Optional ContMaintLabor As Single = 0, _
                          Optional ByVal OthCont As Single = 0, Optional ByVal GAPers As Single = 0, _
                          Optional ByVal STNonVol As Single = 0, Optional ByVal OthNonVol As Single = 0, _
                          Optional ByVal ContMaintLaborST As Single = 0) Implements IProfileLiteUtilities.AddOpexAll
        base.AddOpexAll(OCCSal, ContMaintLabor, _
                          OthCont, GAPers, _
                          STNonVol, OthNonVol, _
                          ContMaintLaborST)

    End Sub

    Public Sub AddOpexCategory(ByVal OpexCategory As String, ByVal CostLocal As Single) Implements IProfileLiteUtilities.AddOpexCategory
        base.AddOpexCategory(OpexCategory, CostLocal)
    End Sub

    Public Sub AddPersAll(ByVal PersID As String, ByVal STH As Single, ByVal Contract As Single, ByVal GA As Single) Implements IProfileLiteUtilities.AddPersAll
        base.AddPersAll(PersID, STH, Contract, GA)
    End Sub

    Sub AddPers(ByVal PersID As String, ByVal STH As Single, ByVal OVTHours As Single, ByVal OVTPcnt As Single, ByVal Contract As Single, ByVal GA As Single, ByVal AbsHrs As Single, ByVal MaintPcnt As Single) Implements IProfileLiteUtilities.AddPers
        base.AddPers(PersID, STH, OVTHours, OVTPcnt, Contract, GA, AbsHrs, MaintPcnt)
    End Sub

    Sub AddAbsence(ByVal CategoryID As String, ByVal SortKey As Int16, ByVal OCC As Single, ByVal MPS As Single) Implements IProfileLiteUtilities.AddAbsence
        base.AddAbsence(CategoryID, SortKey, OCC, MPS)
    End Sub

    Public Sub AddMaintTA(ByVal UnitID As Integer, ByVal TAID As Integer, _
                        ByVal ProcessID As String, ByVal TADate As Date, _
                        ByVal TAHrsDown As Single, ByVal TACostLocal As Single, _
                        ByVal TAOCCSTH As Single, ByVal TAContOCC As Single, _
                        ByVal PrevTADate As Date, ByVal TAException As Integer, _
                        Optional ByVal TALaborCostLocal As Single = 0) Implements IProfileLiteUtilities.AddMaintTA
        base.AddMaintTA(UnitID, TAID, _
            ProcessID, TADate, _
            TAHrsDown, TACostLocal, _
            TAOCCSTH, TAContOCC, _
            PrevTADate, TAException, _
            TALaborCostLocal)
    End Sub

    Public Sub AddMaintRout(ByVal UnitID As Integer, ByVal ProcessID As String, _
                        ByVal RoutCostLocal As Single, ByVal MaintDown As Single) Implements IProfileLiteUtilities.AddMaintRout
        base.AddMaintRout(UnitID, ProcessID, RoutCostLocal, MaintDown)
    End Sub

    Public Sub AddMaintRoutFull(ByVal UnitID As Integer, ByVal ProcessID As String, _
                    ByVal RoutCostLocal As Single, ByVal RoutMatlLocal As Single, ByVal RoutExpLocal As Single, ByVal RoutCptlLocal As Single, ByVal RoutOvhdLocal As Single, _
                    ByVal RegNum As Int16, ByVal RegDown As Single, ByVal RegSlow As Single, _
                    ByVal MaintNum As Int16, ByVal MaintDown As Single, ByVal MaintSlow As Single, _
                    ByVal OthNum As Int16, ByVal OthDown As Single, ByVal OthSlow As Single) Implements IProfileLiteUtilities.AddMaintRoutFull
        base.AddMaintRoutFull(UnitID, ProcessID, _
            RoutCostLocal, RoutMatlLocal, RoutExpLocal, RoutCptlLocal, RoutOvhdLocal, _
            RegNum, RegDown, RegSlow, _
            MaintNum, MaintDown, MaintSlow, _
            OthNum, OthDown, OthSlow)
    End Sub

    Public Sub AddCrude(ByVal CNum As String, ByVal BBL As Single, Optional ByVal CrudeName As String = "Crude", Optional ByVal PricePerBbl As Single = 0, Optional ByVal Gravity As Single = 0, Optional ByVal Sulfur As Single = 0, Optional ByVal MT As Double = 0) Implements IProfileLiteUtilities.AddCrude
        tracingInfo.AppendLine("Calling AddCrude with " + CNum & "," & BBL.ToString() & "," & CrudeName.ToString() & "," & PricePerBbl.ToString() & "," & Gravity.ToString() & "," & Sulfur.ToString() & "," & MT.ToString())
        base.AddCrude(CNum, BBL, CrudeName, PricePerBbl, Gravity, Sulfur, MT)
        tracingInfo.AppendLine("Successfully called AddCrude")
    End Sub

    Public Sub AddYield_RM(ByVal Category As String, ByVal MaterialID As String, ByVal BBL As Single, Optional ByVal MaterialName As String = "Other Raw Materials", Optional ByVal PriceLocal As Single = 0, Optional ByVal Density As Single = 0, Optional ByVal MT As Double = 0) Implements IProfileLiteUtilities.AddYield_RM
        base.AddYield_RM(Category, MaterialID, BBL, MaterialName, PriceLocal, Density, MT)
    End Sub

    Public Sub AddYield_Prod(ByVal Category As String, ByVal MaterialID As String, ByVal BBL As Single, Optional ByVal MaterialName As String = "Products", Optional ByVal PriceLocal As Single = 0, Optional ByVal Density As Single = 0, Optional ByVal MT As Double = 0) Implements IProfileLiteUtilities.AddYield_Prod
        base.AddYield_Prod(Category, MaterialID, BBL, MaterialName, PriceLocal, Density, MT)
    End Sub

    Public Sub AddUserDefined(ByVal HeaderText As String, ByVal VariableDesc As String, _
        ByVal RptValue As Double, ByVal DecPlaces As Byte, ByVal RptValue_Target As Double, _
        ByVal RptValue_Avg As Double, ByVal RptValue_YTD As Double) _
        Implements IProfileLiteUtilities.AddUserDefined
        base.AddUserDefined(HeaderText, VariableDesc, RptValue, DecPlaces, RptValue_Target, RptValue_Avg, RptValue_YTD)
    End Sub

    Public Sub AddEnergy(ByVal TransType As String, ByVal Transferto As String, _
                    ByVal EnergyType As String, ByVal RptSource As Single, _
                    ByVal RptPriceLocal As Single) Implements IProfileLiteUtilities.AddEnergy
        base.AddEnergy(TransType, Transferto, EnergyType, RptSource, RptPriceLocal)
    End Sub

    Public Sub AddElectric(ByVal TransType As String, ByVal Transferto As String, _
                ByVal EnergyType As String, ByVal RptGenEff As Single, _
                ByVal RptMWH As Single, ByVal PriceLocal As Single) Implements IProfileLiteUtilities.AddElectric
        base.AddElectric(TransType, Transferto, EnergyType, RptGenEff, RptMWH, PriceLocal)
    End Sub

    ' Private Function GetEncodedSignedCms(ByRef uploadws As ProfileLiteWS.ProfileLiteWebServices, _
    '                                                 ByVal key As String, ByRef encodedSignedCms() As Byte) As Boolean
    '    Try
    '        Return base.GetEncodedSignedCms(uploadws, key, encodedSignedCms)
    '    Catch ex As Exception
    '        tracingInfo.AppendLine("Error in AuthenticateByCertificateAndClientId(): " + ex.Message)
    '        If (ex.InnerException IsNot Nothing) Then tracingInfo.AppendLine(ex.InnerException.ToString())
    '        Return False
    '    End Try
    'End Function

    Public Sub SubmitData(ByVal key As String) Implements IProfileLiteUtilities.SubmitData
        Try
            SubmitData(key)
        Catch e As Exception
            MsgBox(e.Message)
        End Try
    End Sub

    Public Sub DownloadResults(ByVal key As String, ByVal DataSet As String, ByVal PeriodYear As Integer, ByVal PeriodMonth As Integer, ByVal FactorSet As String, ByVal Currency As String, ByVal UOM As String, ByVal proc As String, ByVal WriteMethod As String, ByVal WriteRange As String, ByVal ws As Excel.Worksheet) Implements IProfileLiteUtilities.DownloadResults
        base.DownloadResults(key, DataSet, PeriodYear, PeriodMonth, FactorSet, Currency, UOM, proc, WriteMethod, WriteRange, ws)
    End Sub

    'Private Function ReturnEntireDataset() As DataSet
    '    'for unit testing only
    '    Return ds
    'End Function

    Public Function TestAddIn() As String Implements IProfileLiteUtilities.TestAddIn
        Return base.TestAddIn()
    End Function

    Public Function TestWebService(RefineryID As String, CallerIP As String, Optional ByVal Scenario As String = "", _
                                   Optional ByVal UserID As String = "", Optional ByVal ComputerName As String = "", _
                                   Optional ByVal Service As String = "", Optional ByVal Method As String = "", _
                                   Optional ByVal EntityName As String = "", Optional ByVal PeriodStart As String = "", _
                                   Optional ByVal PeriodEnd As String = "", Optional ByVal Notes As String = "", _
                                   Optional ByVal Status As String = "") _
                               As String Implements IProfileLiteUtilities.TestWebService
        Try
            Return base.TestWebService(RefineryID, CallerIP, Scenario, UserID, ComputerName, Service, Method, EntityName, PeriodStart, PeriodEnd, Notes, Status)
        Catch ex As Exception
            If Not IsNothing(ex.InnerException) AndAlso Not IsNothing(ex.InnerException.Message) AndAlso ex.InnerException.Message.Length > 0 Then
                Return ex.InnerException.ToString()
            Else
                Return ex.Message
            End If
        End Try
    End Function

    Public Function ShowProfileReport(clientKey As String, _
                      uOM As String, currencyCode As String, studyYear As String, dataSet As String, scenario As String, _
                      includeTarget As Boolean, includeAvg As Boolean, includeYTD As Boolean) As String Implements IProfileLiteUtilities.ShowProfileReport
        Try
            Return base.ShowProfileReport(clientKey, uOM, currencyCode, studyYear, dataSet, scenario, includeTarget, includeAvg, includeYTD)
        Catch ex As Exception
            If Not IsNothing(ex.InnerException) AndAlso Not IsNothing(ex.InnerException.Message) AndAlso ex.InnerException.Message.Length > 0 Then
                Return ex.InnerException.ToString()
            Else
                Return ex.Message
            End If
        End Try
    End Function

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                'ds.Dispose()
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region




End Class
