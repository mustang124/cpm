﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Meep.Startup))]
namespace Meep
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
