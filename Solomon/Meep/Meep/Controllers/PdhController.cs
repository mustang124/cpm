﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MEEPVideos.Controllers
{
    [Authorize]
    public class PdhController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(string video)
        {
            switch (video)
            {
                case "first":
                    ViewBag.videoPath = "SEEC Intro.mp4";
                    ViewBag.imagePath = "SEEC_Intro_First_Frame.png";
                    break;
                case "second":
                    ViewBag.videoPath = "MEEP PDH Introduction.mp4";
                    ViewBag.imagePath = "second.png";
                    break;
                case "third":
                    ViewBag.videoPath = "pdh-table1.mp4";
                    ViewBag.imagePath = "third.png";
                    break;
                case "fourth":
                    ViewBag.videoPath = "pdh-table23-whole.mp4";
                    ViewBag.imagePath = "fourth.png";
                    break;
            }

            return View();
        }
        
    }
}