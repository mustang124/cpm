﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MEEPVideos.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(string video)
        {
            switch (video)
            {
                case "first":
                    ViewBag.videoPath = "SEEC Intro.mp4";
                    ViewBag.imagePath = "SEEC_Intro_First_Frame.png";
                    break;
                case "second":
                    ViewBag.videoPath = "MEEP Olefins Input form.mp4";
                    ViewBag.imagePath = "secondVideo.png";
                    break;
                case "third":
                    ViewBag.videoPath = "MEEP Olefins Table 1.mp4";
                    ViewBag.imagePath = "thirdVideo.png";
                    break;
                case "fourth":
                    ViewBag.videoPath = "Meep Olefins Table 2_3.mp4";
                    ViewBag.imagePath = "fourthVideo.png";
                    break;
                case "fifth":
                    ViewBag.videoPath = "MEEP Olefins Table 7.mp4";
                    ViewBag.imagePath = "fifthVideo.png";
                    break;
                case "sixth":
                    ViewBag.videoPath = "MEEP Olefins Wrapup.mp4";
                    ViewBag.imagePath = "sixthVideo.png";
                    break;
            }

            return View();
        }
        
    }
}