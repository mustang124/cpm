﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Solomon.CreditRiskModel;

namespace Presentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private Person _person = new Person();
        private DataHolder _dataHolder = new DataHolder();

        public MainWindow()
        {
            InitializeComponent();
            //_person.FirstName = "Josh";
            //_person.LastName = "Smith";
            _dataHolder.IntRateInsurer = .5;
            _dataHolder.LoanAmt = 100000;
            //_dataHolder.TestList.Add(2.1);
            _dataHolder.TestArr = new double[]{5, 6};
            BindInXaml();
            // ManuallyMoveData();
            //BindInCode();
        }


        private void BindInXaml()
        {
            //base.DataContext = new Person
            //{
            //    FirstName = "Josh",
            //    LastName = "Smith"
            //};
            //base.DataContext = _person;
            base.DataContext = _dataHolder; 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //_bindedObject.
            //_person.FirstName = "Ted";
            //firstNameTextBox.Text = "Ted";
            _dataHolder.IntRateInsurer += 0.1;
            _dataHolder.LoanAmt += 1000;

            double[] newArr = new double[2];
            newArr[0] = _dataHolder.TestArr[0] + 1;

            _dataHolder.TestArr = newArr;

            //_dataHolder.TestList[0] += 1;
            

        }
        /*
        private void BindInCode()
        {
            var person = new Person
            {
                FirstName = "Josh",
                LastName = "Smith"
            };

            Binding b = new Binding();
            b.Source = person;
            b.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            b.Path = new PropertyPath("FirstName");
            this.firstNameTextBox.SetBinding(TextBox.TextProperty, b);

            b = new Binding();
            b.Source = person;
            b.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            b.Path = new PropertyPath("LastName");
            this.lastNameTextBox.SetBinding(TextBox.TextProperty, b);

            b = new Binding();
            b.Source = person;
            b.Path = new PropertyPath("FullName");
            this.fullNameTextBlock.SetBinding(TextBlock.TextProperty, b);
        }
        private void ManuallyMoveData()
        {
            _person = new Person
            {
                FirstName = "Josh",
                LastName = "Smith"
            };

            this.firstNameTextBox.Text = _person.FirstName;
            this.lastNameTextBox.Text = _person.LastName;
            this.fullNameTextBlock.Text = _person.FullName;

            this.firstNameTextBox.TextChanged += firstNameTextBox_TextChanged;
            this.lastNameTextBox.TextChanged += lastNameTextBox_TextChanged;
        }
        */
        //void lastNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    _person.LastName = this.lastNameTextBox.Text;
        //    this.fullNameTextBlock.Text = _person.FullName;
        //}

        //void firstNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    _person.FirstName = this.firstNameTextBox.Text;
        //    this.fullNameTextBlock.Text = _person.FullName;
        //}
    }



    public class Person : INotifyPropertyChanged
    { //using System.ComponentModel;
        /*
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return String.Format("{0}, {1}",
                    this.LastName, this.FirstName);
            }
        }
         * */

        string _firstName;
        string _lastName;

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                this.OnPropertyChanged("FirstName");
                this.OnPropertyChanged("FullName");
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                this.OnPropertyChanged("LastName");
                this.OnPropertyChanged("FullName");
            }
        }

        public string FullName
        {
            get
            {
                return String.Format("{0}, {1}",
                    this.LastName, this.FirstName);
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(
                    this, new PropertyChangedEventArgs(propName));
        }

    }

}
