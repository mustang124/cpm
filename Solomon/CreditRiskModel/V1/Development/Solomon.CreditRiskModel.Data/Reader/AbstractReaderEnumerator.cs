﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Solomon.CreditRiskModel;

namespace Solomon.CreditRiskModel.Data.Reader
{
    public class AbstractReaderEnumerator : IEnumerator<ReaderDetails>
    {
        private List<ReaderDetails> _list = null;
        private ReaderDetails _current = null;
        int _position = -1;

        public AbstractReaderEnumerator(List<ReaderDetails> list)
        {
            _list = list;
        }

        object IEnumerator.Current
        {
            get
            {
                return _current;
            }
        }

        ReaderDetails IEnumerator<ReaderDetails>.Current
        {
            get
            {
                if (_list == null)
                {
                    throw new InvalidOperationException("AbstractReaderEnumerator _list is null.");
                }
                else
                {
                    return _list.ElementAt<ReaderDetails>(_position);
                }
            }
        }

        bool IEnumerator.MoveNext()
        {
            _position++;
            return _list == null ? false : _position < _list.Count();
        }

        void IEnumerator.Reset()
        {
            _list = null;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _current = null;
                }
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~AbstractReaderEnumerator() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
