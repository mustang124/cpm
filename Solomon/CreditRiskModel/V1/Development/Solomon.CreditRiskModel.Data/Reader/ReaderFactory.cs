﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Solomon.CreditRiskModel.Data.Reader
{
    public sealed class ReaderFactory
    {
        private AbstractReader _reader = null;
        public AbstractReader Create(string fileType, string versionType) //sealed throws error
        {
            switch (fileType.ToUpper().Trim())
            {
                case "TEXT":
                    _reader = new TxtReader();
                    break;
                case "KEY":
                    _reader = new TxtReader();
                    break;
                default:
                    throw new Exception("Unexpected reader type");
                    break;
            }
            return _reader;
        }
    }
}
