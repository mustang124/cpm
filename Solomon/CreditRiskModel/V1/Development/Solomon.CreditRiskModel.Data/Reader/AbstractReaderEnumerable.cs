﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.CreditRiskModel;
using System.Collections;
using System.Collections.Generic;


namespace Solomon.CreditRiskModel.Data.Reader
{
    public class AbstractReaderEnumerable : IEnumerable<ReaderDetails>
    {
        private List<ReaderDetails> _detailsIn = null;

        public AbstractReaderEnumerable(List<ReaderDetails> detailsIn)
        {
            _detailsIn = new List<ReaderDetails>();
            foreach (ReaderDetails detail in detailsIn)
            {
                _detailsIn.Add(detail);
            }
        }

        public IEnumerator<ReaderDetails> GetEnumerator()
        {
            return _detailsIn.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new AbstractReaderEnumerator(_detailsIn);
        }
    }
}