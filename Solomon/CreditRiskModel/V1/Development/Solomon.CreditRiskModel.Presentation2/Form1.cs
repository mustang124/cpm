﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.CreditRiskModel;
using System.IO;


namespace Solomon.CreditRiskModel.Presentation2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'testDataSet1.TestTable' table. You can move, or remove it, as needed.
            this.testTableTableAdapter.Fill(this.testDataSet1.TestTable);
            // TODO: This line of code loads data into the 'testDataSet.TestTable' table. You can move, or remove it, as needed.
            //this.testTableTableAdapter.Fill(this.testDataSet.TestTable);
         
        }

        private bool LoadDataset(ref FinanceData financeData, string filePath)
        {
            StreamReader reader = null;
            try
            {
                if(!File.Exists(filePath))
                {
                    ErrorsInfo.AddErrorInfo("LoadDataset","Cannot find file at " + filePath);
                    return false;
                }
                reader = new StreamReader(filePath);
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if(!LoadNextRecord(ref financeData,line))
                        return false;
                }                
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadDataset", ex.Message);
                return false;
            }
            finally
            {
                try{
                    reader.Close();
                }catch{}
            }
        }

        private bool LoadNextRecord(ref FinanceData financeData, string textRecord)
        {
            try
            {
                string[] dataForColumns = textRecord.Split(',');
                DataRow row = financeData.CumulativeDefaultProbabilities.NewRow();
                row[0] = financeData.CumulativeDefaultProbabilities.Rows.Count+1;
                for (int counter = 1; counter < 16; counter++)
                {
                    row[counter] = dataForColumns[counter - 1];
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadNextRecord", ex.Message);
                return false;
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            //var DSet = dataGridView1.DataSource;
           

            //Dim cmdbuilder As New MySqlCommandBuilder(SQLAdapter)
//    SQLAdapter.Update(myTable, "clinics")


            testTableTableAdapter.Update(testDataSet1);
            testDataSet1.AcceptChanges();
        }

        private void selectalllToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.testTableTableAdapter.selectalll(this.testDataSet1.TestTable);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

    }
}
