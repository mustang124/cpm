﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Solomon.CreditRiskModel.Services;
using Microsoft.Win32;

namespace Solomon.CreditRiskModel.Presentation2
{
    public partial class FinanceDataUI : Form
    {
        DataHolder _dataHolder = null;
        

        public FinanceDataUI()
        {
            InitializeComponent();
        }

        //private DataTable GetReadOnlyDataTable()
        //{
        //    DataTable dataTableReadOnly = new DataTable();
        //    dataTableReadOnly.Columns.Add(new DataColumn("B", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("B+", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("BB-", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("BB", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("BB+", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("BBB-", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("BBB", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("BBB+", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("A-", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("A", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("A+", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("AA-", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("AA", typeof(System.Double)));
        //    dataTableReadOnly.Columns.Add(new DataColumn("AA+", typeof(System.Double)));
        //    return dataTableReadOnly;
        //}

        private void GridBindToListTest()
        {

            List<double> testList = new List<double>();
            testList.Add(1);
            testList.Add(2);
            testList.Add(3);
            testList.Add(4);

            DataTable temp = Utilities.GetReadOnlyDataTableForBinding(testList,false);
            dataGridView2.DataSource = temp;

            //GridBindToList(dataGridView2, testList);       

        }

        //private void GridBindToList(DataGridView grid, List<double> list)
        //{
        //    DataTable dataTableReadOnly = Utilities.GetReadOnlyDataTable(); // GetReadOnlyDataTable();
        //    DataRow row = dataTableReadOnly.NewRow();
        //    for (int counter = 0; counter < list.Count; counter++)
        //    {
        //        row[counter] = list[counter];
        //    }
        //    dataTableReadOnly.Rows.Add(row);
        //    grid.DataSource = dataTableReadOnly;
        //}


        private void FinanceDataUI_Load(object sender, EventArgs e)
        {
            _dataHolder = new DataHolder();

            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance = new List<double?>();
            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance.Add(1);
            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance.Add(2);
            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance.Add(3);
        }

        /*
        private bool LoadDataset(ref FinanceData financeData, string filePath)
        {
            StreamReader reader = null;
            try
            {
                if (!File.Exists(filePath))
                {
                    ErrorsInfo.AddErrorInfo("LoadDataset", "Cannot find file at " + filePath);
                    return false;
                }
                reader = new StreamReader(filePath);
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (!LoadNextRecord(ref financeData, line))
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadDataset", ex.Message);
                return false;
            }
            finally
            {
                try
                {
                    reader.Close();
                }
                catch { }
            }
        }

        private bool LoadNextRecord(ref FinanceData financeData, string textRecord)
        {
            try
            {
                string[] dataForColumns = textRecord.Split(',');
                DataRow row = financeData.CumulativeDefaultProbabilities.NewRow();
                row[0] = financeData.CumulativeDefaultProbabilities.Rows.Count + 1;
                for (int counter = 1; counter < 16; counter++)
                {
                    row[counter] = dataForColumns[counter - 1];
                }
                financeData.CumulativeDefaultProbabilities.Rows.Add(row);
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadNextRecord", ex.Message);
                return false;
            }
        }
        */
        private void btnLoad_Click(object sender, EventArgs e)
        {
            FinanceData financeData = new FinanceData();
            ServiceManager serviceManager = new ServiceManager();


            //string path = ChooseFile(); // @"C:\Cumulative Default Probabilities Annual - Decrypted.csv";
            //string path = @"C:\Cumulative Default Probabilities Annual - Decrypted.csv";
            string path = @"C:\Cumulative Default Probabilities Month - Decrypted.csv";
            if (path.Length < 2)
                return;
            if (!File.Exists(path))
            {
                MessageBox.Show("Cannot find file at " + path);
                return;
            }
            if (!serviceManager.LoadFinanceData(ref _dataHolder, path))
            {
                MessageBox.Show(ErrorsInfo.GetAllErrors());
                return;
            }
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = financeData;
            dataGridView1.DataMember = "CumulativeDefaultProbabilities"; // table name you need to show
        }
        private string ChooseFile()
        {
            //http://www.wpf-tutorial.com/dialogs/the-openfiledialog/
            //openFileDialog1 dialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName.Length > 2)
            {
                return openFileDialog1.FileName;
            }
            else
            {
                return string.Empty;
            }
            /* if (openFileDialog1.ShowDialog() == true && dialog.FileName.Length > 2)
            {
                _dataHolder.ESDDataPath = dialog.FileName;
                string[] pathParts = dialog.FileName.Split('\\');
                ESDDataPath.Content = pathParts[pathParts.Length - 1];
            }
             * */
        }

        /*
        private bool SaveDataset(ref FinanceData financeData, string filePath)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(filePath,false);
                foreach (DataRow row in financeData.CumulativeDefaultProbabilities.Rows)
                {
                    if (!SaveNextRecord(row, ref writer))
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("SaveDataset", ex.Message);
                return false;
            }
            finally
            {
                try
                {
                    writer.Close();
                }
                catch { }
            }
        }

        private bool SaveNextRecord(DataRow row, ref StreamWriter writer)
        {
            try
            {
                string line = string.Empty;
                for (int counter = 1; counter < 16; counter++)
                {
                    line += row[counter].ToString() + ",";
                }
                writer.WriteLine(line);
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("SaveNextRecord", ex.Message);
                return false;
            }
        }
        */
        private void btnSave_Click(object sender, EventArgs e)
        {
            FinanceData financeData = (FinanceData)dataGridView1.DataSource;
            ServiceManager serviceManager = new ServiceManager();
            if (!serviceManager.SaveFinanceData(ref financeData, @"C:\TestWRite.csv"))
                MessageBox.Show(ErrorsInfo.GetAllErrors());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GridBindToListTest();
        }

    }
}
