﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.CreditRiskModel.Components.Data
{
    internal sealed class DataFactory
    {
        private AbstractData _dataObject = null;
        internal AbstractData Create(string dataType, Enums.PeriodTypes periodType, string keyFilePath,
            string keyEncryptionKey, byte[] rfcEncryptionKey, string keyEncryptionESD, byte[] rfcEncryptionESD, 
            string keyEncryptionInterestRates, byte[] rfcEncryptionInterestRates) //sealed throws error
        {
            switch (dataType.ToUpper().Trim())
            {
                case "ANALYSIS":
                    _dataObject = new Solomon.CreditRiskModel.Components.Data.AnalysisData(periodType, keyFilePath, 
                        keyEncryptionKey, rfcEncryptionKey, keyEncryptionESD, rfcEncryptionESD, keyEncryptionInterestRates, rfcEncryptionInterestRates);
                    break;
                default:
                    throw new Exception("Unexpected Data type: " + dataType.ToUpper().Trim());
                    break;
            }
            return _dataObject;
        }
    }
}
