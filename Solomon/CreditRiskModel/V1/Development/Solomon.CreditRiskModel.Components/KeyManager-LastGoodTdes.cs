﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace Solomon.CreditRiskModel.Components
{
    internal class KeyManager
    {
        public string BuildClientKey(string company,  string expirationDate,
            string effectiveDate)// password, string location, string refnum)
        {
            string retVal = string.Empty;

            //if (password.Length > 0)
            //{
            //    retVal = company.Trim() + "$" + password.Trim() + "$" + location.Trim() + "$" + refnum.ToUpper().Trim();
            //}
            //else
            //{
                //retVal = company.Trim() + "$" + location.Trim() + "$" + refnum.ToUpper().Trim();
            retVal = company.Trim() + "$" + expirationDate.Trim() + "$" + effectiveDate.Trim();
            //}

            return EncryptString(retVal);
        }

        internal string EncryptString(string theText)
        {
            return Cryptographer.Encrypt(theText);

        }

        internal string DecryptString(string theText)
        {
            return Cryptographer.Decrypt(theText);
        }

        internal bool DecryptKey(string keyText, ref string company, 
            ref string expirationDate, ref string effectiveDate)
        {
            try
            {
                string[] keyDetails = Cryptographer.Decrypt(keyText).Split('$');
                company = keyDetails[0];
                expirationDate = keyDetails[1];
                effectiveDate= keyDetails[2];
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal bool UserIsExpired(string keyFilePath)
        {
            System.IO.StreamReader reader = null;
            try
            {
                reader = new System.IO.StreamReader(keyFilePath);
                string key = reader.ReadLine();
                string company = string.Empty;
                string expirationDate = string.Empty;
                string effectiveDate = string.Empty;
                if (DecryptKey(key, ref company, ref expirationDate, ref effectiveDate))
                {
                    string[] dateParts = expirationDate.Split('/');
                    DateTime expired = new DateTime(Int32.Parse(dateParts[2]),
                        Int32.Parse(dateParts[0]),Int32.Parse(dateParts[1]));
                    if (DateTime.Now > expired)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }

        internal bool UserIsEffective(string keyFilePath)
        {
            System.IO.StreamReader reader = null;
            try
            {
                reader = new System.IO.StreamReader(keyFilePath);
                string key = reader.ReadLine();
                string company = string.Empty;
                string expirationDate = string.Empty;
                string effectiveDate = string.Empty;
                if (DecryptKey(key, ref company, ref expirationDate, ref effectiveDate))
                {
                    string[] dateParts = effectiveDate.Split('/');
                    DateTime effective = new DateTime(Int32.Parse(dateParts[2]),
                        Int32.Parse(dateParts[0]), Int32.Parse(dateParts[1]));
                    if (DateTime.Now >= effective)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }

        //internal bool DecryptKey(string keyText, ref string company, ref string password, ref string location, ref string refNum)
        //{
        //    try
        //    {
        //        Cryptographer crypto = new Cryptographer();
        //        string[] keyDetails = crypto.Decrypt(keyText).Split('$');
        //        company = keyDetails[0];
        //        password = keyDetails[1];
        //        location = keyDetails[2];
        //        refNum = keyDetails[3];
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
