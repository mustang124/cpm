﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.CreditRiskModel;


namespace Solomon.CreditRiskModel.Components.Reader
{
    public class AbstractReaderEnumerable : IEnumerable<ReaderDetails>
    {
        private string _filePath = null;
        private List<ReaderDetails> _detailsIn = null;

        public AbstractReaderEnumerable(List<ReaderDetails> detailsIn)
        {
            _detailsIn = new List<ReaderDetails>();
            foreach (ReaderDetails detail in detailsIn)
            {
                _detailsIn.Add(detail);
            }
        }

        public IEnumerator<ReaderDetails> GetEnumerator()
        {
            //return this.GetEnumerator();
            //return (IEnumerator<ReaderDetails>)this;
            return _detailsIn.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new AbstractReaderEnumerator(_detailsIn);
        }
    }
}
