﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.CreditRiskModel;

namespace Solomon.CreditRiskModel.Components
{
    public class CreditRiskManager : IAnalyze
    {
        private DataHolder _dataHolder = null;
        public CreditRiskManager(DataHolder dataHolder)
        {
            _dataHolder = dataHolder;
        }
  
        public bool Analyze(Enums.PeriodTypes periodType, string keyFilePath, 
            string keyEncryptionKey, byte[] rfcEncryptionKey, string keyEncryptionESD, 
            byte[] rfcEncryptionESD, string keyEncryptionInterestRates, byte[] rfcEncryptionInterestRates)
        {
            try
            {
                Data.DataFactory factory = new Data.DataFactory();
                Data.AnalysisData analysisData = (Data.AnalysisData)factory.Create("ANALYSIS", periodType, keyFilePath, 
                    keyEncryptionKey, rfcEncryptionKey, keyEncryptionESD, rfcEncryptionESD, 
                    keyEncryptionInterestRates, rfcEncryptionInterestRates);

                //Next we call PopulateBorrowerAndInsurerIntRateArrays()
                analysisData.PopulateUninsuredLoanPayment(ref _dataHolder);
                analysisData.PopulateInsuredEffectiveLoanPayment(ref _dataHolder);
                //next PopulateEnergySvgsDistArray()  gets called
                if (!analysisData.PopulateProjectSavingsDistribution(ref _dataHolder))
                    return false;
                if (!analysisData.PopulateLossCostsNoInsurance(ref _dataHolder))
                    return false;
                if (!analysisData.PopulateLossCostsWithInsurance(ref _dataHolder))
                    return false;

                if (!analysisData.PopulateSavingsFromLoanPaymentReduction(ref _dataHolder))
                    return false;
                return true;

            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("Analyze", ex.Message);
                return false;
            }

        }

    }
}
