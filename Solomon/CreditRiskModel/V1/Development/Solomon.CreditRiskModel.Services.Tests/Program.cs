﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Configuration;
using System.Security;
using System.IO;

namespace Solomon.CreditRiskModel.Services.Tests
{
    class Program
    {
        private static  DataHolder _dataHolder = null;
        private static string _keyFilePath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\SOLOMON.Key";
        private static string _keyEncryptionKey = "884587801A43486";
        private static byte[] _rfcEncryptionKey = new byte[] { 74, 40, 76, 81, 70, 35, 14, 86, 70, 37, 61, 87, 62 };
        private static string _keyEncryptionESD = "F545A7BA84FA4E8";
        private static byte[] _rfcEncryptionESD = new byte[] { 64, 52, 67, 50, 59, 48, 98, 22, 47, 26, 29, 29, 82 };
        private static string _keyEncryptionInterestRates = "DEE33F5D39414A3";
        private static byte[] _rfcEncryptionInterestRates = new byte[] { 98, 10, 226, 95, 90, 243, 33, 106, 99, 66, 70, 57, 100 };
        private static string _esdPath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\ESD-AnnualTestEncrypted.rdd";
        private static string _outputPath = @"C:\CreditRiskModel-TestResults.csv";

        private static long _loanAmtStart = 0;
        private static int _loanTermStart = 0;
        private static long _pctStart = 0;
        private static long _totalInsuredPrjSavingsStart = 0;
        private static int _borrCreditRatingStart = 0;
        private static int _insurerCreditRatingStart = 0;   
        private static string cumulativeDefaultProbabilitiesPath = string.Empty;
                
        static void Main(string[] args)
        {
            _loanAmtStart =GetLong("Starting Loan Amt");
            _loanTermStart = GetInt("Starting LoanTerm");
            _pctStart = GetLong("Starting Percent");
            _totalInsuredPrjSavingsStart = GetLong("Starting Insured Project Savings");
            _borrCreditRatingStart = 0; // GetInt("Borrower Credit Rating");
            _insurerCreditRatingStart = 0; // GetInt("Insurer Credit Rating");
           
            StartNewOutput(_outputPath);

            //test annual
            string financeDataInterestRatesFIlePath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\CreditRiskModelAnnualInterestRates.csv";
            string cumulativeDefaultProbabilitiesPath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\Cumulative Default Probabilities Annual.csv";
            double[] financeDataInterestRates = ReadFinanceDataInterestRates(financeDataInterestRatesFIlePath);
            KitchenSinkTest(1, financeDataInterestRates, Enums.PeriodTypes.YEAR,cumulativeDefaultProbabilitiesPath);


            //test monthly
            financeDataInterestRatesFIlePath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\CreditRiskModelMonthInterestRates.csv";
            cumulativeDefaultProbabilitiesPath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\Cumulative Default Probabilities Month.csv";
            financeDataInterestRates = ReadFinanceDataInterestRates(financeDataInterestRatesFIlePath);
            KitchenSinkTest(2, financeDataInterestRates, Enums.PeriodTypes.MONTH,cumulativeDefaultProbabilitiesPath);

            Console.WriteLine("Finished");
            Console.ReadLine();
        }

        private static int GetInt(string variableName)
        {
            Console.WriteLine("Enter " + variableName);
            string result = Console.ReadLine();
            return Int32.Parse(result);
        }
        private static long GetLong(string variableName)
        {
            Console.WriteLine("Enter " + variableName);
            string result = Console.ReadLine();
            return long.Parse(result);
        }


        static void KitchenSinkTest(int annual0Month1,double[] financeDataInterestRates, Enums.PeriodTypes perType, string cumulativeDefaultProbabilitiesPath)
        {
            ServiceManager svcMgr = new ServiceManager();
            long loanAmt = 0;
            int loanTerm = 0;
            long pct = 0;
            long totalInsuredPrjSavings = 0;
            int borrCreditRating = 0;
            int insurerCreditRating = 0;            

            //loop thru incrementing each input 0 to 10 digits and capture results and any error.
            for (loanAmt = _loanAmtStart; loanAmt <= 999999999; loanAmt++)
            {
                for (loanTerm = _loanTermStart; loanTerm <= 2147483647; loanTerm++)
                {
                    Console.WriteLine("Loan Amount: " + loanAmt.ToString() + ". " + DateTime.Now.ToString());
                    for (pct = _pctStart; pct < 101; pct++)
                    {
                        Console.WriteLine("   Percent: " + pct.ToString() + ". " + DateTime.Now.ToString());
                        for (totalInsuredPrjSavings = _totalInsuredPrjSavingsStart; totalInsuredPrjSavings <= 999999999; totalInsuredPrjSavings++)
                        {
                            Console.WriteLine("      TotalInsuredPrjSavings: " + totalInsuredPrjSavings.ToString() + ". " + DateTime.Now.ToString());
                            for (borrCreditRating = _borrCreditRatingStart; borrCreditRating < 15; borrCreditRating++)
                            {
                                for (insurerCreditRating = _insurerCreditRatingStart; insurerCreditRating < 15; insurerCreditRating++)
                                {
                                    _dataHolder = new DataHolder();
                                    _dataHolder.LoanAmt = loanAmt;
                                    _dataHolder.Nterm = loanTerm;
                                    _dataHolder.CoverageRatio = pct;
                                    _dataHolder.TotalInsuredProjectSavings = totalInsuredPrjSavings;


                                    _dataHolder.SelectedInsuredCreditRating = _dataHolder.SPRatings[borrCreditRating]; 
                                    _dataHolder.SelectedInsurerCreditRating = _dataHolder.SPRatings[insurerCreditRating ]; 

                                    _dataHolder.ESDDataPath = _esdPath;
                                    _dataHolder.FinanceDataInterestRates = financeDataInterestRates;
                                    
                                    if (!svcMgr.LoadFinanceData(ref _dataHolder, cumulativeDefaultProbabilitiesPath))
                                    {
                                        Console.WriteLine("###################################################################################");
                                        Console.WriteLine("Error loading cumulativeDefaultProbabilitiesPath: " + cumulativeDefaultProbabilitiesPath);
                                        Console.ReadLine();
                                    }

                                    string discard = string.Empty;
                                    if (!svcMgr.ValidateInput(_dataHolder, perType, pct.ToString(), ref discard))
                                    {
                                        Console.WriteLine("###################################################################################");
                                        Console.WriteLine("Error loading cumulativeDefaultProbabilitiesPath: " + cumulativeDefaultProbabilitiesPath);
                                        Console.ReadLine();
                                    }

                                    ErrorsInfo.Reset();
                                    try
                                    {
                                        //this gives 0.0 !!! _dataHolder.CoverageRatio = pct / 100; 
                                        _dataHolder.CoverageRatio = double.Parse(pct.ToString()) / 100; //convert to a real percent
                                        
                                        svcMgr.Analyze(_dataHolder, _keyFilePath, perType, _keyEncryptionKey,
                                            _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates,
                                            _rfcEncryptionInterestRates);

                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorsInfo.AddErrorInfo("test() method",ex.Message);
                                    }
                                    WriteResults(_outputPath, _dataHolder, perType);
                                }
                            }
                        }
                    }
                }
            }

        }

        private static double[] ReadFinanceDataInterestRates(string path)
        {
            StreamReader reader = new StreamReader(path);
            string line = reader.ReadLine();
            reader.Close();
            string[] lineParts = line.Split(',');
            double[] result = new double[lineParts.Length];
            for (int counter = 0; counter < lineParts.Length; counter++)
            {
                result[counter] = double.Parse(lineParts[counter]);
            }
            return result;
        }

        private static object BuildNullList<T>(int numberOfElements)
        {
            object list=null;
            if(typeof(T)==typeof(decimal))
            {
                List<decimal> decList = new List<decimal>();
                for (int counter = 1; counter <= numberOfElements; counter++)
                {
                    decList.Add(0);
                }
                list = decList;  
            }
            else if (typeof(T) == typeof(double?))
            {
                List<double?> dblList = new List<double?>();
                for (int counter = 1; counter <= numberOfElements; counter++)
                {
                    dblList.Add(0);
                }
                list = dblList;
            }
            return list;
        }

        private static void WriteResults(string path, DataHolder data, Enums.PeriodTypes perType)
        {
            if (data.ProjectSavingsDistribution == null)
                data.ProjectSavingsDistribution = (List<decimal>)BuildNullList<decimal>(9);
            if (data.ProjectSavingsCreditEnhancementNoInsurance == null)
                data.ProjectSavingsCreditEnhancementNoInsurance = (List<double?>)BuildNullList<double?>(14);
            if (data.ProjectSavingsCreditEnhancementWithInsurance == null)
                data.ProjectSavingsCreditEnhancementWithInsurance = (List<double?>)BuildNullList<double?>(14);
            if (data.CreditRiskSavings == null)
                data.CreditRiskSavings = (List<double?>)BuildNullList<double?>(14);
            if (data.LossReserves == null)
                data.LossReserves = (List<double?>)BuildNullList<double?>(15);

            StringBuilder line = new StringBuilder();
            StringBuilder lineParts = new StringBuilder();
            string errors = ErrorsInfo.GetAllErrors();

            if (errors.Length > 0)
            {
                errors.Replace(",", ";");
                errors.Replace(Environment.NewLine, "|");
            }
            lineParts.Append(
                (errors !=null ? errors : string.Empty) + ","
                + perType.ToString() + ","
                + (data.LoanAmt != null ? data.LoanAmt.ToString() : string.Empty) + ","
+ (data.Nterm != null ? data.Nterm.ToString() : string.Empty) + ","
+ (data.TotalInsuredProjectSavings != null ? data.TotalInsuredProjectSavings.ToString() : string.Empty) + ","
+ (data.CoverageRatio != null ? data.CoverageRatio.ToString() : string.Empty) + ","
+ (data.CreditRiskBorrower != null ? data.CreditRiskBorrower.ToString() : string.Empty) + ","
+ (data.CreditRiskInsurer != null ? data.CreditRiskInsurer.ToString() : string.Empty) + ","
+ (data.UninsuredLoanPayment != null ? data.UninsuredLoanPayment.ToString() : string.Empty) + ","
+ (data.InsuredEffectiveLoanPayment != null ? data.InsuredEffectiveLoanPayment.ToString() : string.Empty) + ","
+ (data.SavingsFromLoanPaymentReduction != null ? data.SavingsFromLoanPaymentReduction.ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[8] != null ? data.ProjectSavingsDistribution[8].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[7] != null ? data.ProjectSavingsDistribution[7].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[6] != null ? data.ProjectSavingsDistribution[6].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[5] != null ? data.ProjectSavingsDistribution[5].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[4] != null ? data.ProjectSavingsDistribution[4].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[3] != null ? data.ProjectSavingsDistribution[3].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[2] != null ? data.ProjectSavingsDistribution[2].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[1] != null ? data.ProjectSavingsDistribution[1].ToString() : string.Empty) + ","
+ (data.ProjectSavingsDistribution[0] != null ? data.ProjectSavingsDistribution[0].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[0] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[0].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[1] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[1].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[2] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[2].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[3] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[3].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[4] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[4].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[5] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[5].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[6] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[6].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[7] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[7].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[8] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[8].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[9] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[9].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[10] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[10].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[11] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[11].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[12] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[12].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementNoInsurance[13] != null ? data.ProjectSavingsCreditEnhancementNoInsurance[13].ToString() : string.Empty) + ","

+ (data.ProjectSavingsCreditEnhancementWithInsurance[0] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[0].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[1] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[1].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[2] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[2].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[3] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[3].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[4] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[4].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[5] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[5].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[6] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[6].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[7] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[7].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[8] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[8].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[9] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[9].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[10] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[10].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[11] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[11].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[12] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[12].ToString() : string.Empty) + ","
+ (data.ProjectSavingsCreditEnhancementWithInsurance[13] != null ? data.ProjectSavingsCreditEnhancementWithInsurance[13].ToString() : string.Empty) + ","

);
            int crsCounter = 0; 
            for(crsCounter = 0; crsCounter<14;crsCounter++)
            {
                try
                {
                    lineParts.Append((data.CreditRiskSavings[crsCounter] != null ? data.CreditRiskSavings[crsCounter].ToString() : string.Empty) + ",");
                }
                catch(ArgumentOutOfRangeException)
                { // no such element
                    lineParts.Append(string.Empty + ",");
                }
                catch(Exception)
                {
                    while(crsCounter < 14)
                    {
                        lineParts.Append(string.Empty + ",");
                        crsCounter++;
                    }
                    break;
                }
            }

            int lrCounter = 0;
            for (lrCounter = 0; lrCounter < 15; lrCounter++)
            {
                try
                {
                    lineParts.Append((data.LossReserves[lrCounter] != null ? data.LossReserves[lrCounter].ToString() : string.Empty) + ",");
                }
                catch (ArgumentOutOfRangeException)
                { // no such element
                    lineParts.Append(string.Empty + ",");
                }
                catch (Exception)
                {
                    while (lrCounter < 15)
                    {
                        lineParts.Append(string.Empty + ",");
                        lrCounter++;
                    }
                    break;
                }
            }

            
            //NaN value is probably caused by dividing by zero.
            line.AppendLine(lineParts.ToString());
            StreamWriter writer = new StreamWriter(path, true);
            writer.Write(line.ToString());
            writer.Close();
        }

        private static void StartNewOutput(string path)
        {
            StringBuilder line = new StringBuilder();
            line.AppendLine("Errors,"
                + "PeriodType,"
                + "LoanAmt,"
                + "Nterm,"
                + "TotalInsuredProjectSavings,"
                + "CoverageRatio,"
                + "CreditRiskBorrower,"
                + "CreditRiskInsurer,"
                + "UninsuredLoanPayment,"
                + "InsuredEffectiveLoanPayment,"
                + "SavingsFromLoanPaymentReduction,"
                + "ProjectSavingsDistribution[8],"
                + "ProjectSavingsDistribution[7],"
                + "ProjectSavingsDistribution[6],"
                + "ProjectSavingsDistribution[5],"
                + "ProjectSavingsDistribution[4],"
                + "ProjectSavingsDistribution[3],"
                + "ProjectSavingsDistribution[2],"
                + "ProjectSavingsDistribution[1],"
                + "ProjectSavingsDistribution[0],"

                + "ProjectSavingsCreditEnhancementNoInsurance[0],"
                + "ProjectSavingsCreditEnhancementNoInsurance[1],"
                + "ProjectSavingsCreditEnhancementNoInsurance[2],"
                + "ProjectSavingsCreditEnhancementNoInsurance[3],"
                + "ProjectSavingsCreditEnhancementNoInsurance[4],"
                + "ProjectSavingsCreditEnhancementNoInsurance[5],"
                + "ProjectSavingsCreditEnhancementNoInsurance[6],"
                + "ProjectSavingsCreditEnhancementNoInsurance[7],"
                + "ProjectSavingsCreditEnhancementNoInsurance[8],"
                + "ProjectSavingsCreditEnhancementNoInsurance[9],"
                + "ProjectSavingsCreditEnhancementNoInsurance[10],"
                + "ProjectSavingsCreditEnhancementNoInsurance[11],"
                + "ProjectSavingsCreditEnhancementNoInsurance[12],"
                + "ProjectSavingsCreditEnhancementNoInsurance[13],"


                + "ProjectSavingsCreditEnhancementWithInsurance[0],"
                + "ProjectSavingsCreditEnhancementWithInsurance[1],"
                + "ProjectSavingsCreditEnhancementWithInsurance[2],"
                + "ProjectSavingsCreditEnhancementWithInsurance[3],"
                + "ProjectSavingsCreditEnhancementWithInsurance[4],"
                + "ProjectSavingsCreditEnhancementWithInsurance[5],"
                + "ProjectSavingsCreditEnhancementWithInsurance[6],"
                + "ProjectSavingsCreditEnhancementWithInsurance[7],"
                + "ProjectSavingsCreditEnhancementWithInsurance[8],"
                + "ProjectSavingsCreditEnhancementWithInsurance[9],"
                + "ProjectSavingsCreditEnhancementWithInsurance[10],"
                + "ProjectSavingsCreditEnhancementWithInsurance[11],"
                + "ProjectSavingsCreditEnhancementWithInsurance[12],"
                + "ProjectSavingsCreditEnhancementWithInsurance[13],"

                + "CreditRiskSavings[0],"
                + "CreditRiskSavings[1],"
                + "CreditRiskSavings[2],"
                + "CreditRiskSavings[3],"
                + "CreditRiskSavings[4],"
                + "CreditRiskSavings[5],"
                + "CreditRiskSavings[6],"
                + "CreditRiskSavings[7],"
                + "CreditRiskSavings[8],"
                + "CreditRiskSavings[9],"
                + "CreditRiskSavings[10],"
                + "CreditRiskSavings[11],"
                + "CreditRiskSavings[12],"
                + "CreditRiskSavings[13],"

                + "LossReserves[0],"
                + "LossReserves[1],"
                + "LossReserves[2],"
                + "LossReserves[3],"
                + "LossReserves[4],"
                + "LossReserves[5],"
                + "LossReserves[6],"
                + "LossReserves[7],"
                + "LossReserves[8],"
                + "LossReserves[9],"
                + "LossReserves[10],"
                + "LossReserves[11],"
                + "LossReserves[12],"
                + "LossReserves[13],"
                + "LossReserves[14],");
                
            StreamWriter writer = new StreamWriter(path, false);
            writer.Write(line.ToString());

            writer.Close();
        }
    }
}

