﻿namespace Solomon.CreditRiskModel.KeyGenerator
{
    partial class KeyGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Effective = new System.Windows.Forms.DateTimePicker();
            this.Expiration = new System.Windows.Forms.DateTimePicker();
            this.CreateKey = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CompanyName = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnLoadKey = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnProcessEsdFiles = new System.Windows.Forms.Button();
            this.radDecryptEsd = new System.Windows.Forms.RadioButton();
            this.radEncryptEsd = new System.Windows.Forms.RadioButton();
            this.txtEncryptedEsdPath = new System.Windows.Forms.TextBox();
            this.btnSelectESDFile = new System.Windows.Forms.Button();
            this.EsdFilePath = new System.Windows.Forms.TextBox();
            this.btnCreateESD = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Effective
            // 
            this.Effective.Location = new System.Drawing.Point(16, 178);
            this.Effective.Name = "Effective";
            this.Effective.Size = new System.Drawing.Size(200, 20);
            this.Effective.TabIndex = 1;
            // 
            // Expiration
            // 
            this.Expiration.Location = new System.Drawing.Point(16, 226);
            this.Expiration.Name = "Expiration";
            this.Expiration.Size = new System.Drawing.Size(200, 20);
            this.Expiration.TabIndex = 2;
            // 
            // CreateKey
            // 
            this.CreateKey.Location = new System.Drawing.Point(16, 268);
            this.CreateKey.Name = "CreateKey";
            this.CreateKey.Size = new System.Drawing.Size(92, 23);
            this.CreateKey.TabIndex = 3;
            this.CreateKey.Text = "Write Key file";
            this.CreateKey.UseVisualStyleBackColor = true;
            this.CreateKey.Click += new System.EventHandler(this.CreateKey_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Effective Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 205);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Expiration Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Company Name";
            // 
            // CompanyName
            // 
            this.CompanyName.Location = new System.Drawing.Point(19, 127);
            this.CompanyName.MaxLength = 20;
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.Size = new System.Drawing.Size(194, 20);
            this.CompanyName.TabIndex = 8;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(374, 356);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnLoadKey);
            this.tabPage1.Controls.Add(this.Effective);
            this.tabPage1.Controls.Add(this.CompanyName);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.Expiration);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.CreateKey);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(366, 330);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Key File";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btnLoadKey
            // 
            this.btnLoadKey.Location = new System.Drawing.Point(16, 21);
            this.btnLoadKey.Name = "btnLoadKey";
            this.btnLoadKey.Size = new System.Drawing.Size(135, 23);
            this.btnLoadKey.TabIndex = 9;
            this.btnLoadKey.Text = "Load Existing Key File";
            this.btnLoadKey.UseVisualStyleBackColor = true;
            this.btnLoadKey.Click += new System.EventHandler(this.btnLoadKey_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnProcessEsdFiles);
            this.tabPage2.Controls.Add(this.radDecryptEsd);
            this.tabPage2.Controls.Add(this.radEncryptEsd);
            this.tabPage2.Controls.Add(this.txtEncryptedEsdPath);
            this.tabPage2.Controls.Add(this.btnSelectESDFile);
            this.tabPage2.Controls.Add(this.EsdFilePath);
            this.tabPage2.Controls.Add(this.btnCreateESD);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(366, 330);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ESD File";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnProcessEsdFiles
            // 
            this.btnProcessEsdFiles.Location = new System.Drawing.Point(9, 281);
            this.btnProcessEsdFiles.Name = "btnProcessEsdFiles";
            this.btnProcessEsdFiles.Size = new System.Drawing.Size(75, 23);
            this.btnProcessEsdFiles.TabIndex = 18;
            this.btnProcessEsdFiles.Text = "Start";
            this.btnProcessEsdFiles.UseVisualStyleBackColor = true;
            this.btnProcessEsdFiles.Click += new System.EventHandler(this.btnProcessEsdFiles_Click);
            // 
            // radDecryptEsd
            // 
            this.radDecryptEsd.AutoSize = true;
            this.radDecryptEsd.Location = new System.Drawing.Point(9, 40);
            this.radDecryptEsd.Name = "radDecryptEsd";
            this.radDecryptEsd.Size = new System.Drawing.Size(106, 17);
            this.radDecryptEsd.TabIndex = 17;
            this.radDecryptEsd.TabStop = true;
            this.radDecryptEsd.Text = "Decrypt ESD File";
            this.radDecryptEsd.UseVisualStyleBackColor = true;
            this.radDecryptEsd.Click += new System.EventHandler(this.radDecryptEsd_Click);
            // 
            // radEncryptEsd
            // 
            this.radEncryptEsd.AutoSize = true;
            this.radEncryptEsd.Location = new System.Drawing.Point(9, 16);
            this.radEncryptEsd.Name = "radEncryptEsd";
            this.radEncryptEsd.Size = new System.Drawing.Size(105, 17);
            this.radEncryptEsd.TabIndex = 16;
            this.radEncryptEsd.TabStop = true;
            this.radEncryptEsd.Text = "Encrypt ESD File";
            this.radEncryptEsd.UseVisualStyleBackColor = true;
            this.radEncryptEsd.CheckedChanged += new System.EventHandler(this.radEncryptEsd_CheckedChanged);
            this.radEncryptEsd.Click += new System.EventHandler(this.radEncryptEsd_Click);
            // 
            // txtEncryptedEsdPath
            // 
            this.txtEncryptedEsdPath.Location = new System.Drawing.Point(9, 239);
            this.txtEncryptedEsdPath.Name = "txtEncryptedEsdPath";
            this.txtEncryptedEsdPath.Size = new System.Drawing.Size(327, 20);
            this.txtEncryptedEsdPath.TabIndex = 15;
            // 
            // btnSelectESDFile
            // 
            this.btnSelectESDFile.Location = new System.Drawing.Point(5, 90);
            this.btnSelectESDFile.Name = "btnSelectESDFile";
            this.btnSelectESDFile.Size = new System.Drawing.Size(167, 23);
            this.btnSelectESDFile.TabIndex = 14;
            this.btnSelectESDFile.UseVisualStyleBackColor = true;
            this.btnSelectESDFile.Click += new System.EventHandler(this.btnSelectESDFile_Click);
            // 
            // EsdFilePath
            // 
            this.EsdFilePath.Location = new System.Drawing.Point(6, 152);
            this.EsdFilePath.Name = "EsdFilePath";
            this.EsdFilePath.Size = new System.Drawing.Size(327, 20);
            this.EsdFilePath.TabIndex = 9;
            // 
            // btnCreateESD
            // 
            this.btnCreateESD.Location = new System.Drawing.Point(5, 189);
            this.btnCreateESD.Name = "btnCreateESD";
            this.btnCreateESD.Size = new System.Drawing.Size(167, 23);
            this.btnCreateESD.TabIndex = 12;
            this.btnCreateESD.UseVisualStyleBackColor = true;
            this.btnCreateESD.Click += new System.EventHandler(this.btnCreateESD_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Path";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // KeyGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 362);
            this.Controls.Add(this.tabControl1);
            this.Name = "KeyGenerator";
            this.Text = "Credit Risk Model Files Generator";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker Effective;
        private System.Windows.Forms.DateTimePicker Expiration;
        private System.Windows.Forms.Button CreateKey;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CompanyName;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox EsdFilePath;
        private System.Windows.Forms.Button btnCreateESD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnLoadKey;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnSelectESDFile;
        private System.Windows.Forms.TextBox txtEncryptedEsdPath;
        private System.Windows.Forms.RadioButton radDecryptEsd;
        private System.Windows.Forms.RadioButton radEncryptEsd;
        private System.Windows.Forms.Button btnProcessEsdFiles;
    }
}

