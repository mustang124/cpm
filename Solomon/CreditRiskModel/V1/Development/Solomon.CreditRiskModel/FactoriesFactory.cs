﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Solomon.CreditRiskModel
{
    //suggested by Omar
    public class FactoriesFactory
    {
        public FactoriesFactory()
        {

        }

        public object Create(string factoryType)
        {
            switch (factoryType.ToUpper().Trim())
            {
                case "READER":
                    return new Solomon.CreditRiskModel.Reader.ReaderFactory();
                    break;
                //case "DATA":
                //    return new Solomon.CreditRiskModel.Components.Data.DataFactory();
                //    break;
                case "WRITER":
                    return new Solomon.CreditRiskModel.Writer.WriterFactory();
                    break;

            }
            return null;
        }
    }
}
