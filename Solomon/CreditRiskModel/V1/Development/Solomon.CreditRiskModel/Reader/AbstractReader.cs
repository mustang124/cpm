﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.CreditRiskModel.Reader
{
    public abstract class AbstractReader
    {
        public virtual bool Open()
        {
            return true;
        }

        public virtual bool Open(string filePath)
        {
            return true;
        }

        public virtual ReaderDetails Read()
        {
            return null;
        }

        public virtual AbstractReaderEnumerable ReadAll()
        {
            return null;
        }

        public virtual bool Close()
        {
            return true;
        }
    }
}
