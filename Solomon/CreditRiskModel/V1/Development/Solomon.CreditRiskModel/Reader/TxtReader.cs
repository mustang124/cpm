﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Solomon.CreditRiskModel;

namespace Solomon.CreditRiskModel.Reader
{
    //concrete
    public class TxtReader : AbstractReader
    {

        StreamReader _reader = null;
        List<ReaderDetails> _textLines = new List<ReaderDetails>();
        int _counter = 0;

        public bool EndOfStream 
        {
            get
            {
                return (!(_counter < _textLines.Count));
            }
        }


        public override ReaderDetails Read()
        {
            //only passing back 1, not all, so don't return AbstractReaderEnumerable
            ReaderDetails returnValue = null;
            if (_counter < _textLines.Count)
            {
                returnValue= _textLines[_counter];
                _counter++;
            }
            return returnValue;
        }

        public ReaderDetails ReadWithDecryption(string key, byte[] rfc)
        {
            //only passing back 1, not all, so don't return AbstractReaderEnumerable
            ReaderDetails returnValue = null;
            if (_counter < _textLines.Count)
            {
                Cryptographer crypto = new Cryptographer(key, rfc);
                returnValue = _textLines[_counter];
                returnValue.value = crypto.Decrypt(returnValue.value);
                _counter++;
            }
            return returnValue;
        }

        public void Reset()
        {
            _counter = 0;
        }

        public override AbstractReaderEnumerable ReadAll()
        {
            //ReaderDetails details = new ReaderDetails();

            //in prod, populae=te these value from text file instead\\
            //AbstractReaderEnumerable txtList = new AbstractReaderEnumerable(new List<ReaderDetails>());
            
            AbstractReaderEnumerable txtList = new AbstractReaderEnumerable(_textLines);

            return txtList;
        }


        public AbstractReaderEnumerable ReadAllWithDecryption(string key, byte[] rfc)
        {
            Cryptographer crypto = new Cryptographer(key, rfc);
            AbstractReaderEnumerable txtList = new AbstractReaderEnumerable(_textLines);
            foreach(ReaderDetails line in _textLines)
            {
                line.value = crypto.Decrypt(line.value);
            }
            return txtList;
        }

        public override bool Open(string filePath)
        {
            try
            {
                _reader = new StreamReader(filePath);
                while (!_reader.EndOfStream)
                {
                    string line = _reader.ReadLine();
                    if (line.Trim().Length > 0)
                    {
                        ReaderDetails detail = new ReaderDetails();
                        detail.value = line.Trim();
                        _textLines.Add(detail);
                    }
                }
                return true;
            }
            catch (IOException ex)
            {
                ErrorsInfo.AddErrorInfo("TxtReader.Open", ex.Message);
                return false;
            }
            finally
            {
                try
                {
                    _reader.Close();
                }catch{ }
            }
        }

        public override bool Close()
        {
            return true; //textreader already closed above.
        }
    }
}
