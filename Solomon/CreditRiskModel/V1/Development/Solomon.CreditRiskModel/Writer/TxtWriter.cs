﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Solomon.CreditRiskModel.Writer
{
    public class TxtWriter : AbstractWriter
    {
        public bool GenerateEncryptedCsvFile(string pathDecrypted, string pathEncrypted, string key, byte[] rfc)
        {
            try
            {
                if (File.Exists(pathEncrypted))
                    File.Delete(pathEncrypted);
                Reader.TxtReader txt = new Reader.TxtReader();
                StreamReader reader = new StreamReader(pathDecrypted);
                List<string> lines = new List<string>();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line.Trim().Length > 0)
                        lines.Add(line.Trim());
                }
                reader.Close();
                return GenerateEncryptedFile(lines, pathEncrypted, key, rfc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GenerateEncryptedFile(List<string> lines, string path, string key, byte[] rfc)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(path, false);
                foreach (string line in lines)
                {
                    Cryptographer crypto = new Cryptographer(key, rfc);
                    writer.WriteLine(crypto.Encrypt(line));
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Close();
            }
        }

        public bool GenerateDecryptedFile(List<string> lines, string path, string key, byte[] rfc)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(path, true);
                foreach (string line in lines)
                {
                    Cryptographer crypto = new Cryptographer(key, rfc);
                    writer.WriteLine(crypto.Decrypt(line));
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Close();
            }
        }

        public bool GenerateFile(List<string> lines, string path)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(path, false);
                foreach (string line in lines)
                {
                    writer.WriteLine(line);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Close();
            }
        }

        //public override void Close()
        //{ //IO writer closed already
        //    return;
        //}
    }
}
