﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.CreditRiskModel.Writer
{
    public abstract class AbstractWriter
    {
        public virtual bool Open()
        {
            return true;
        }
        public virtual bool Open(string filePath)
        {
            return true;
        }

        public virtual bool WriteAll(IList<object> records)
        {
            return true;
        }

        public virtual bool Close()
        {
            return true;
        }
    }
}
