﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.CreditRiskModel.Components;
using Solomon.CreditRiskModel.KeyComponents;


namespace Solomon.CreditRiskModel.Services
{
    public class ServiceManager
    {
        public bool Analyze(DataHolder dataHolder, string keyFilePath, Enums.PeriodTypes periodType, 
            string keyEncryptionKey, byte[] rfcEncryptionKey, string keyEncryptionESD, 
            byte[] rfcEncryptionESD, string keyEncryptionInterestRates, byte[] rfcEncryptionInterestRates)
        {
            IAnalyze creditRiskManager = new CreditRiskManager(dataHolder);
            return creditRiskManager.Analyze(periodType, keyFilePath, keyEncryptionKey, 
                rfcEncryptionKey, keyEncryptionESD, rfcEncryptionESD, keyEncryptionInterestRates, 
                rfcEncryptionInterestRates);
        }

        public bool SaveFinanceData(ref FinanceData financeData, string filePath)
        {
            IFinance financeDataManager = new FinanceDataManager();
            return financeDataManager.SaveFinanceData(ref financeData, filePath);
        }

        public bool LoadFinanceData(ref DataHolder dataHolder, string filePath)
        {
            IFinance financeDataManager = new FinanceDataManager();
            return financeDataManager.LoadFinanceData(ref dataHolder, filePath);
        }

        public bool LoadFinanceDataInterestRates(ref DataHolder dataHolder, string filePath)
        {
            IFinance financeDataManager = new FinanceDataManager();
            return financeDataManager.LoadFinanceDataInterestRates(ref dataHolder, filePath);
        }
        public bool SaveFinanceDataInterestRates(ref DataHolder dataHolder, string filePath)
        {
            IFinance financeDataManager = new FinanceDataManager();
            return financeDataManager.SaveFinanceDataInterestRates(ref dataHolder, filePath);
        }

        public string GenerateNewKey(string path, string companyName, string effectiveDate, string expirationDate, string key, byte[] rfc)
        {
            IFiles keyManager = new FileManager();
            return keyManager.GenerateNewKey(path, companyName, effectiveDate, expirationDate,key,rfc);
        }

        public string GenerateNewEsdFile(string pathDecrypted, string pathEncrypted,string key, byte[] rfc)
        {
            IFiles fileManager = new FileManager();
            return fileManager.GenerateNewEsdFile(pathDecrypted, pathEncrypted, key, rfc);
        }
        public string DecryptExistingEsdFile(string pathEncrypted, string pathDecrypted, string key, byte[] rfc)
        {
            IFiles fileManager = new FileManager();
            return fileManager.DecryptExistingEsdFile(pathEncrypted, pathDecrypted, key, rfc);
        }

        public bool UserIsEffective(string path, string key, byte[] rfc)
        {
            IFiles keyManager = new FileManager();
            return keyManager.UserIsEffective(path,key,rfc);
        }
        public bool UserIsExpired(string path, string key, byte[] rfc)
        {
            IFiles keyManager = new FileManager();
            return keyManager.UserIsExpired(path,key,rfc);
        }

        public bool GetKeyFileContents(string path, ref string company, ref string effectiveDate, ref string expiredDate,
            string key, byte[] rfc)
        {
            IFiles keyManager = new FileManager();
            return keyManager.GetKeyFileContents(path, ref company, ref effectiveDate, ref expiredDate, key, rfc);
        }

        public bool ValidateInput(DataHolder data, Enums.PeriodTypes period, string policyCoverageText, ref string errorMessage)
        {
            if (!ValidateNumberEntry(data.LoanAmt.ToString(), "Loan Amount", ref errorMessage))
                return false;
            if(Int64.Parse(data.LoanAmt.ToString())<1)
            {
                errorMessage = "Your Loan Amount must be greater than zero. ";
                return false;
            } 

            if (!ValidateNumberEntry(data.Nterm.ToString(), "Loan Term", ref errorMessage))
                return false;
            if (Int64.Parse(data.Nterm.ToString()) < 1)
            {
                errorMessage = "Your Loan Term Years must be greater than zero. ";
                return false;
            }


            if (!ValidateNumberEntry(data.TotalInsuredProjectSavings.ToString(), "Total Insured Project Savings", ref errorMessage))
                return false;
            if (!System.IO.File.Exists(data.ESDDataPath))
            {
                errorMessage="Please select a 'Project Savings Distribution File' and try again.";
                return false;
            }

            double testPct = 0;
            if (!double.TryParse(policyCoverageText, out testPct))
            {
                errorMessage = "The Policy Coverage Factor (" + policyCoverageText + ") is not a valid percent. Please enter a whole number between 0 and 100 (inclusive).";
                return false;
            }

            if (testPct < 0 || testPct > 100 || policyCoverageText.ToString().Contains('.'))
            {
                errorMessage = "The Policy Coverage Factor (" + policyCoverageText + ") is not a valid percent. Please enter a whole number between 0 and 100 (inclusive).";
                return false;
            }

            if (period == Enums.PeriodTypes.YEAR)
            {
                if (data.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.Rows.Count > 15)
                {
                    errorMessage = "Your Cumulative Default Probabilities has more than 15 rows. Please adjust to 15 rows or less, and try again.";
                    return false;
                }
                if (data.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.Rows.Count < 15)
                {
                    errorMessage = "Your Cumulative Default Probabilities has less than 15 rows. Please adjust to 15 rows, and try again.";
                    return false;
                }

            }



            return true;
        }


        private bool ValidateNumberEntry(string value, string controlDisplayName, ref string errorMessage)
        {
            value = value.Trim();
            string output = string.Empty;
            double discard = 0;
            //this is at UI:
            /*
            if (!Double.TryParse(value, out discard))
                output += value + " is not a valid number. Please try again." + Environment.NewLine;
            */
            //replace for commas-do before call this method.
            //olicyCoverageFactor.Text=olicyCoverageFactor.Text.Replace(",", string.Empty);

            //No negatives.
            if (discard < 0)
                output += value + " must not be less than zero. Please try again." + Environment.NewLine;

            //no decimal points
            if (value.Contains("."))
                output += value + " must not contain decimals. Please try again." + Environment.NewLine;

            //not > 10 digits
            if (value.Length > 10)
                output += value + " must not contain more than 10 digits. Please try again." + Environment.NewLine;

            if (output.Length > 0)
            {
                errorMessage = "There was a problem in '" + controlDisplayName + "': " + output;
                return false;
            }
            return true;
        }
    }
}
