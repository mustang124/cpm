﻿CREATE TABLE [dbo].[ProductPrices] (
    [PriceID]      INT          IDENTITY (1, 1) NOT NULL,
    [Source]       VARCHAR (10) NOT NULL,
    [FtpFileID]    INT          NULL,
    [TransType]    CHAR (1)     NULL,
    [ItemCode]     VARCHAR (10) NOT NULL,
    [PriceTypeID]  VARCHAR (2)  NULL,
    [ProductID]    AS           (concat(ltrim(rtrim([ItemCode])),ltrim(rtrim([PriceTypeID])))) PERSISTED NOT NULL,
    [PriceDate]    DATE         NULL,
    [PriceValue]   MONEY        NULL,
    [TimeStampID]  TINYINT      NULL,
    [FwdPeriod]    SMALLINT     NULL,
    [DiffBaseRoll] SMALLINT     NULL,
    [ContFwd]      TINYINT      NULL,
    [PriceYear]    AS           (datepart(year,[PriceDate])) PERSISTED,
    [PriceMonth]   AS           (datepart(month,[PriceDate])) PERSISTED,
    [RecCreated]   DATETIME     CONSTRAINT [DF_ProductPrices_RecCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ProductPrices] PRIMARY KEY CLUSTERED ([PriceID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idxProductPrices_ItemCode]
    ON [dbo].[ProductPrices]([ItemCode] DESC);

