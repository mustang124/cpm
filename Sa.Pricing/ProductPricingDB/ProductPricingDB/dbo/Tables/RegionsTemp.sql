﻿CREATE TABLE [dbo].[RegionsTemp] (
    [RegionCode] VARCHAR (5)  NOT NULL,
    [Region]     VARCHAR (50) NOT NULL,
    [tsModified] DATETIME     CONSTRAINT [DF__Regions__tsModif__1B9317B3] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Regions] PRIMARY KEY CLUSTERED ([RegionCode] ASC)
);

