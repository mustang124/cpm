﻿






CREATE VIEW [dbo].[vwProduct_Descriptions]
AS

	--selects all the detail information for each product code into a single place
	--note that it includes both Platts and Argus info, with a UNION between them
		


	SELECT DISTINCT Source = 'Argus', 
		aq.ItemCode, 
		PriceTypeId = CAST(aq.PriceTypeId AS varchar(10)),
		ParentCode = ISNULL(pl.ParentCode, aq.ItemCode), 
		ParentCodeOrder = ISNULL(pl.ParentCodeOrder, 1), 
		CNUM = ISNULL(cm.CNUM,''),
		CNUM2 = ISNULL(cm.CNUM2,''), 
		MNUM = ISNULL(cm.MNUM,''),
		MNUM2 = ISNULL(cm.MNUM2,''),
		ec.CatCommodity,
		amd.Module, 
		acat.Category,
		rl.Region,
		rl.Location, 
		ac.DisplayName,
		Curr = CASE WHEN CHARINDEX('/', ac.Unit) > 0 THEN LEFT(ac.Unit,CHARINDEX('/', ac.Unit) - 1) ELSE '' END,
		UOM = CASE WHEN CHARINDEX('/', ac.Unit) > 0 THEN RIGHT(ac.Unit, CHARINDEX('/', REVERSE(ac.Unit))-1) ELSE ac.Unit END,
		ac.Frequency
	FROM Argus_Quotes_RolledUp aq
		LEFT JOIN ref.Argus_Codes ac ON ac.ItemCode = aq.ItemCode
		LEFT JOIN solomon.ParentList pl ON pl.ItemCode = ac.ItemCode 
			AND ((pl.PriceTypeID = CAST(aq.PriceTypeId AS VARCHAR)) OR (pl.PriceTypeID = '1,2' AND aq.PriceTypeId IN (1,2)) OR (pl.PriceTypeID = '6,7' AND aq.PriceTypeId IN (6,7)))
		LEFT JOIN solomon.CandMNums cm ON cm.ItemCode = aq.ItemCode
		--LEFT JOIN ref.Argus_Category acat ON acat.ItemCode = ac.ItemCode
		--the below because there are itemcodes with multiple categories in the table, so we pick the lowest (alphabetically)
		LEFT JOIN (SELECT DISTINCT ItemCode, DisplayName, Category = MIN(Category) OVER (PARTITION BY ItemCode, DisplayName ORDER BY Category) FROM ref.Argus_Category) acat ON acat.ItemCode = ac.ItemCode
		LEFT JOIN ref.Argus_TimeStamp at ON at.TimeStampId = aq.TimeStampId
		LEFT JOIN ref.Argus_PriceType ap ON ap.PriceTypeId = aq.PriceTypeId
		LEFT JOIN ref.Argus_ModuleDetails amd ON amd.ItemCode = ac.ItemCode AND amd.TimeStampId = at.TimeStampId AND amd.PriceTypeId = ap.PriceTypeId AND amd.Module IN ( 'DAGM','DPC')
		LEFT JOIN ref.Argus_Modules am ON am.Module = amd.Module
		LEFT JOIN solomon.ExcelKeys ek ON ek.ItemCode = aq.ItemCode
		LEFT JOIN ref.Excel_Categories ec ON ec.CatKey = ek.ExcelKeyCode
		LEFT JOIN solomon.RegionLocation rl ON rl.ItemCode = aq.ItemCode
	WHERE aq.ItemCode IN (SELECT ItemCode FROM ProductPrices)

	UNION ALL

	SELECT Source = 'Platts', 
		ItemCode = aq.Symbol, 
		PriceTypeId = aq.Bates,
		ParentCode = ISNULL(pl.ParentCode, aq.Symbol), 
		ParentCodeOrder = ISNULL(pl.ParentCodeOrder, 1),
		CNUM = ISNULL(cm.CNUM,''),
		CNUM2 = ISNULL(cm.CNUM2,''), 
		MNUM = ISNULL(cm.MNUM,''),
		MNUM2 = ISNULL(cm.MNUM2,''),
		ec.CatCommodity,
		Module = aq.MDC, 
		Category = acat.Category + CASE WHEN acat.CategoryGroup IS NOT NULL THEN ' -> ' + acat.CategoryGroup ELSE '' END,
		rl.Region,
		rl.Location,
		aq.Description,
		aq.Curr,
		UOM,
		Freq
	--FROM ref.Platts_Symbols aq
	--why this? because it turns out that there are symbols with multiple MDCs, everything else being the same. So we just pick the lowest MDC (alphabetically)
	FROM (SELECT DISTINCT Trans, Symbol, Bates, Freq, Curr, UOM, Dec, Conv, StarSlash, To_UOM, Earliest, Latest, Description,
			MDC = MIN(MDC) OVER (PARTITION BY Trans, Symbol, Bates, Freq, Curr, UOM, Dec, Conv, StarSlash, To_UOM, Earliest, Latest, Description ORDER BY MDC)
			FROM ref.Platts_Symbols) aq

		LEFT JOIN solomon.ParentList pl ON pl.ItemCode = aq.Symbol 
		LEFT JOIN solomon.CandMNums cm ON cm.ItemCode = aq.Symbol
		LEFT JOIN ref.Platts_Categories acat ON acat.Module = aq.MDC
		LEFT JOIN solomon.ExcelKeys ek ON ek.ItemCode = aq.Symbol
		LEFT JOIN ref.Excel_Categories ec ON ec.CatKey = ek.ExcelKeyCode
		LEFT JOIN solomon.RegionLocation rl ON rl.ItemCode = aq.Symbol
	WHERE aq.Symbol IN (SELECT ItemCode FROM ProductPrices)
		AND (aq.Bates LIKE '%c%' OR aq.Bates LIKE '%hl%' OR aq.Bates LIKE '%u%')









