﻿

CREATE VIEW [dbo].[Argus_ProductPricesByYearConverted]
AS

	--uses UOM conversion rates to get new values
	SELECT p.ItemCode, 
		p.PriceTypeId, 
		p.PriceYear, 
		PriceValue = PriceValue * ISNULL(ConversionRate,1)
	FROM Argus_ProductPricesByYear p
		LEFT JOIN solomon.UOMConversions c ON c.ItemCode = p.ItemCode

	
	--SELECT b.ItemCode,b.PriceTypeId, PriceYear, PriceValue = AVG(PriceValue) * ISNULL(MIN(ConversionRate),1)
	--FROM (
	--	SELECT ItemCode, 
	--		PriceTypeId = CONVERT(varchar(3),CASE PriceTypeID WHEN 2 THEN 1 WHEN 7 THEN 6 ELSE PriceTypeID END),
	--		PriceYear = CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceYear 
	--			ELSE CASE WHEN PriceMonth <= 2 AND (PriceMonth - FwdPeriod) <= -10 THEN PriceYear - 1 
	--			ELSE CASE WHEN PriceMonth >= 8 AND (PriceMonth - FwdPeriod) >= 7 THEN PriceYear + 1 
	--			ELSE PriceYear END END END,
	--		--PriceMonth = CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceMonth ELSE FwdPeriod END,
	--		PriceValue,
	--		TransTypeRank = DENSE_RANK() OVER (PARTITION BY ItemCode, PriceTypeId, PriceDate ORDER BY ItemCode, PriceTypeId DESC, PriceDate, TransType ASC),
	--		ContFwdRank = DENSE_RANK() OVER (PARTITION BY ItemCode, PriceTypeId, PriceDate ORDER BY ContFwd ASC)
	--	FROM dbo.ProductPrices
	--	WHERE Source = 'Argus'
	--		AND PriceTypeID IN (1,2,3,4,5,6,7,8,20)
	--	GROUP BY ItemCode, PriceTypeId, 
	--		CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceYear 
	--			ELSE CASE WHEN PriceMonth <= 2 AND (PriceMonth - FwdPeriod) <= -10 THEN PriceYear - 1 
	--			ELSE CASE WHEN PriceMonth >= 8 AND (PriceMonth - FwdPeriod) >= 7 THEN PriceYear + 1 
	--			ELSE PriceYear END END END,
	--		--CASE WHEN ISNULL(FwdPeriod,0) = 0 THEN PriceMonth ELSE FwdPeriod END, 
	--		PriceDate, PriceValue, TransType, ContFwd
	--	) b
	--LEFT JOIN solomon.UOMConversions c ON c.ItemCode = b.ItemCode
	--WHERE TransTypeRank = 1 AND ContFwdRank = 1
	--GROUP BY b.ItemCode, b.PriceTypeId, PriceYear--, PriceMonth








