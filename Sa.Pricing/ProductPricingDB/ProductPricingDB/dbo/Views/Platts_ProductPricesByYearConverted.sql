﻿











CREATE VIEW [dbo].[Platts_ProductPricesByYearConverted]
AS

	--uses conversion rates to get new values
	SELECT p.ItemCode, 
		p.PriceTypeId, 
		p.PriceYear, 
		PriceValue = PriceValue * ISNULL(ConversionRate,1)
	FROM Platts_ProductPricesByYear p
		LEFT JOIN solomon.UOMConversions c ON c.ItemCode = p.ItemCode


	--	SELECT b.ItemCode, b.PriceTypeId, PriceYear, PriceValue = AVG(PriceValue) * ISNULL( MIN(ConversionRate),1)
	--FROM (
	--	SELECT ItemCode, 
	--		PriceTypeId = UPPER(CASE WHEN PriceTypeID = 'l' THEN 'h' ELSE PriceTypeID END),
	--		PriceYear,
	--		PriceValue,
	--		TransTypeRank = DENSE_RANK() OVER (PARTITION BY ItemCode, PriceTypeId, PriceDate ORDER BY ItemCode, PriceTypeId DESC, PriceDate, TransType ASC),
	--		ContFwdRank = DENSE_RANK() OVER (PARTITION BY ItemCode, PriceTypeId, PriceDate ORDER BY ContFwd ASC)
	--	FROM ProductPrices
	--	WHERE Source = 'Platts'
	--		--AND PriceTypeID IN (1,2,4,8)
	--	GROUP BY ItemCode, PriceTypeId, PriceYear, PriceDate, PriceValue, TransType, ContFwd
	--	) b
	--LEFT JOIN solomon.UOMConversions c ON c.ItemCode = b.ItemCode
	--WHERE TransTypeRank = 1 AND ContFwdRank = 1
	--GROUP BY b.ItemCode, b.PriceTypeId, PriceYear








