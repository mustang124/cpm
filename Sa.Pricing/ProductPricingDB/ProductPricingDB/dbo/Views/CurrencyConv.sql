﻿







CREATE VIEW [dbo].[CurrencyConv]
AS

	SELECT cc.ConversionID, 
		cc.FromCurrency, 
		cc.FromItemCode, 
		cc.ToCurrency, 
		cc.ToItemCode, 
		pp1.PriceYear, 
		pp1.PriceMonth, 
		ConversionRate = pp2.PriceValue/pp1.PriceValue
	FROM solomon.CurrencyConversions cc
		LEFT JOIN (SELECT ItemCode, PriceYear, PriceMonth, PriceValue = AVG(PriceValue) 
			FROM ProductPrices 
			WHERE PriceTypeID = 'c' 
			GROUP BY ItemCode, PriceYear, PriceMonth) pp1 ON pp1.ItemCode = cc.FromItemCode
		LEFT JOIN (SELECT ItemCode, PriceYear, PriceMonth, PriceValue = AVG(PriceValue) 
			FROM ProductPrices 
			WHERE PriceTypeID = 'c' 
			GROUP BY ItemCode, PriceYear, PriceMonth) pp2 ON pp2.ItemCode = cc.ToItemCode AND pp2.PriceYear = pp1.PriceYear and pp2.PriceMonth = pp1.PriceMonth



