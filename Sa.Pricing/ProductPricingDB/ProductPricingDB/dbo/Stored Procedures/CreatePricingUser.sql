﻿-- ---------------------------------------------------------------------------------------------
-- Author:		Solomon Associates
-- Programmer:	das
-- Create date: 2016.09.15
-- Description:	Create Application access user
/* Syntax:		

	EXEC dbo.[CreatePricingUser];

*/
-- ---------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[CreatePricingUser]
AS
BEGIN
	-- create login at server level
		CREATE LOGIN [master] WITH PASSWORD=N'PricingUser', DEFAULT_DATABASE=[Pricing], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
	
	-- create db ''user'' using server ''login''
		CREATE USER [PricingUser] FOR LOGIN [PricingUser];
	
	-- grant read/write
		ALTER ROLE [db_datareader] ADD MEMBER [PricingUser];

		ALTER ROLE [db_datawriter] ADD MEMBER [PricingUser];
	
	-- grant read/write/update/execute for common schemas
		GRANT SELECT, INSERT, UPDATE, EXECUTE TO [PricingUser];

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[dbo] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[etl] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[fn] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[ref] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[rpt] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[sp] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[sys] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[tmp] TO [PricingUser]; 

		GRANT SELECT, INSERT, UPDATE, EXECUTE ON SCHEMA::[vw] TO [PricingUser]; 


		GRANT CONNECT TO [PricingUser];
END

