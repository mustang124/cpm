﻿




CREATE  PROCEDURE [dbo].[All_Prices_Ralph]
AS
BEGIN

	DECLARE @query AS NVARCHAR(MAX),
		@cols AS NVARCHAR(MAX),
		@cols2 AS NVARCHAR(MAX),
		@cols3 AS NVARCHAR(MAX),
		@cols4 AS NVARCHAR(MAX),
		@cols5 AS NVARCHAR(MAX);

	WITH CTE AS
	(
		SELECT *, CAST(yr AS NVARCHAR(4))+RIGHT('00'+CAST(mt AS NVARCHAR(2)),2) YearMonth
		FROM MonthYears
	)

	--each of these gives a different definition of the list of columns in the data, to be used further down
	SELECT @cols = STUFF((SELECT DISTINCT ',' + QUOTENAME(YearMonth) 
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols2 = STUFF((SELECT DISTINCT ',ISNULL(' + QUOTENAME(YearMonth) + ',0) AS ' + QUOTENAME(YearMonth)
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols3 = STUFF((SELECT DISTINCT ',' + QUOTENAME(YearMonth) + ' real' 
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols4 = STUFF((SELECT DISTINCT ', a.' + QUOTENAME(YearMonth) + ' + CASE WHEN rp.ItemCode IS NULL THEN 0 ELSE b.'  + QUOTENAME(YearMonth) + ' END AS '  + QUOTENAME(YearMonth)
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,''),
		@cols5 = STUFF((SELECT DISTINCT ' update #tmp2 set ' + QUOTENAME(YearMonth) + ' = ' + QUOTENAME(YearMonth) 
				+ ' * CurrencyConv.ConversionRate from CurrencyConv 
					where #tmp2.Curr = CurrencyConv.FromCurrency and CurrencyConv.ToCurrency = ''USD'' and CurrencyConv.PriceYear = ' + LEFT(YearMonth, 4) + ' and CurrencyConv.PriceMonth = ' + RIGHT(YearMonth, 2) + ';'
			FROM CTE FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')
		
		--@cols5 = ''




	SET @query = 'CREATE TABLE #tmp (Source VARCHAR(10), ItemCode VARCHAR(10), ' + @cols3 + '); 

		--grabs the data from the UOM converted views, gets the values based on a partition of the ParentList (getting the lowest priority value each time)
		--then pivots them into the tmp table for further processing
		INSERT #tmp
		SELECT Source = ''Argus'', ItemCode = ParentCode, ' + @Cols2 + ' 
		FROM (
				SELECT ParentCode, ' + @cols2 + '
				FROM (  
						SELECT ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue 
						FROM (
								SELECT p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
									MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY PriceYear, PriceMonth, ParentCodeOrder)
							--	FROM Argus_ProductPricesByMonth p
								FROM Argus_ProductPricesByMonthConverted p
									INNER JOIN solomon.ParentList pl ON pl.ItemCode = p.ItemCode AND pl.PriceTypeID = p.PriceTypeId
						) a
						WHERE ParentCodeOrder = MinPriority AND ParentCodeOrder = 1
				) T
				PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c

		WHERE c.ParentCode NOT IN (SELECT ItemCode FROM solomon.NotNeeded)
		UNION ALL
		SELECT Source = ''Platts'', ItemCode = ParentCode, ' + @Cols2 + ' 
		FROM (
				SELECT ParentCode, ' + @cols2 + '
				FROM (  
						SELECT ParentCode, CAST([PriceYear] AS NVARCHAR(4))+RIGHT(''00''+CAST([PriceMonth] AS NVARCHAR(2)),2) YearMonth, PriceValue 
						FROM (
								SELECT p.PriceYear, p.PriceMonth, p.PriceTypeId, p.PriceValue, pl.ParentCode, pl.ParentCodeOrder, 
									MinPriority = MIN(ParentCodeOrder) OVER (PARTITION BY ParentCode, PriceYear, PriceMonth ORDER BY PriceYear, PriceMonth, ParentCodeOrder)
						--		FROM Platts_ProductPricesByMonth p
								FROM Platts_ProductPricesByMonthConverted p
									INNER JOIN solomon.ParentList pl ON pl.ItemCode = p.ItemCode AND pl.PriceTypeID = p.PriceTypeId
						) a
						WHERE ParentCodeOrder = MinPriority AND ParentCodeOrder = 1
		
				) T
				PIVOT(SUM(PriceValue) FOR YearMonth IN ('+@cols+')) PT
		) c
		WHERE c.ParentCode NOT IN (SELECT ItemCode FROM solomon.NotNeeded)

		--this next part adds on the Product Descriptions

		select * from #tmp where itemcode = ''PA5000760''
			'



	print @query


	--select @query
	EXEC(@Query)
	


END





