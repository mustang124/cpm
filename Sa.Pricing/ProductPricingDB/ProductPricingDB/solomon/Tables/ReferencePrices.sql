﻿CREATE TABLE [solomon].[ReferencePrices] (
    [ItemCode]       VARCHAR (10) NOT NULL,
    [RefCode]        VARCHAR (10) NOT NULL,
    [RefDescription] VARCHAR (50) NULL,
    [RefMonth]       INT          NULL,
    CONSTRAINT [PK_ReferencePrices] PRIMARY KEY CLUSTERED ([ItemCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idxReferencePrices_RefCode]
    ON [solomon].[ReferencePrices]([RefCode] ASC);

