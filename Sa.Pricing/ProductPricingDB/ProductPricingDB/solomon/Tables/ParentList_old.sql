﻿CREATE TABLE [solomon].[ParentList_old] (
    [ItemCode]        VARCHAR (10) NULL,
    [PriceTypeID]     VARCHAR (3)  NULL,
    [ParentCode]      VARCHAR (10) NULL,
    [ParentCodeOrder] INT          NULL,
    CONSTRAINT [ItemCodePriceTypeId] UNIQUE NONCLUSTERED ([ItemCode] ASC, [PriceTypeID] ASC),
    CONSTRAINT [ParentCodeParentCodeOrder] UNIQUE NONCLUSTERED ([ParentCode] ASC, [ParentCodeOrder] ASC)
);

