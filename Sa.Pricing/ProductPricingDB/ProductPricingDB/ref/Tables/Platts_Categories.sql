﻿CREATE TABLE [ref].[Platts_Categories] (
    [Module]        VARCHAR (20)  NOT NULL,
    [ImportedRow]   VARCHAR (128) NOT NULL,
    [Category]      VARCHAR (128) NULL,
    [CategoryGroup] VARCHAR (128) NULL
);

