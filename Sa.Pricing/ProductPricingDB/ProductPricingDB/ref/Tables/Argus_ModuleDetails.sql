﻿CREATE TABLE [ref].[Argus_ModuleDetails] (
    [Module]            VARCHAR (25) NOT NULL,
    [ItemCode]          CHAR (10)    NOT NULL,
    [TimeStampId]       TINYINT      NOT NULL,
    [PriceTypeId]       TINYINT      NOT NULL,
    [ContForwardPeriod] TINYINT      NOT NULL,
    [StartDate]         DATE         NOT NULL,
    [EndDate]           DATE         NOT NULL
);

