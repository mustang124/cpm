﻿CREATE TABLE [ref].[Argus_Quotes] (
    [ItemCode]                CHAR (9)      NOT NULL,
    [ContForwardPeriod]       INT           NOT NULL,
    [Timing]                  VARCHAR (12)  NOT NULL,
    [FwdPeriodDescription]    VARCHAR (128) NOT NULL,
    [TimeStampId]             INT           NOT NULL,
    [PriceTypeId]             INT           NOT NULL,
    [DifferentialBasis]       VARCHAR (56)  NOT NULL,
    [DifferentialBasisTiming] VARCHAR (5)   NULL,
    [StartDate]               DATE          NOT NULL,
    [EndDate]                 DATE          NULL,
    [OldCode]                 VARCHAR (9)   NULL,
    [DecimalPlaces]           INT           NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [ItemCode]
    ON [ref].[Argus_Quotes]([ItemCode] ASC);


GO
CREATE NONCLUSTERED INDEX [TimeStampId]
    ON [ref].[Argus_Quotes]([TimeStampId] ASC);


GO
CREATE NONCLUSTERED INDEX [PriceTypeId]
    ON [ref].[Argus_Quotes]([PriceTypeId] ASC);

