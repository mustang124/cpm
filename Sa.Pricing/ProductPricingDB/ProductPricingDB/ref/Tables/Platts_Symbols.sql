﻿CREATE TABLE [ref].[Platts_Symbols] (
    [MDC]         VARCHAR (3)   NULL,
    [Trans]       VARCHAR (10)  NULL,
    [Symbol]      CHAR (7)      NULL,
    [Bates]       CHAR (10)     NULL,
    [Freq]        CHAR (10)     NULL,
    [Curr]        CHAR (10)     NULL,
    [UOM]         VARCHAR (10)  NULL,
    [DEC]         VARCHAR (10)  NULL,
    [Conv]        VARCHAR (10)  NULL,
    [StarSlash]   VARCHAR (10)  NULL,
    [To_UOM]      VARCHAR (10)  NULL,
    [Earliest]    DATE          NULL,
    [Latest]      DATE          NULL,
    [Description] VARCHAR (192) NULL
);


GO
CREATE NONCLUSTERED INDEX [Symbol]
    ON [ref].[Platts_Symbols]([Symbol] ASC);

