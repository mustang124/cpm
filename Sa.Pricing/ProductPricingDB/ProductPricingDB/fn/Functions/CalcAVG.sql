﻿-- ------------------------------------------------------------------------------------------
-- Author:      Solomon & Associates, Inc.
-- Create date: 2015-09-01 by DAS
-- Description: Return calculated average price for passed in product (symbol) item
/*
	SELECT * FROM [fn].[CalcAVG](2015, 1, 'WA', 'AAAVR00');
	SELECT * FROM [fn].[CalcAVG](2015, 4, 'WA', 'AAAVQ00');
*/
-- ------------------------------------------------------------------------------------------
CREATE FUNCTION [fn].[CalcAVG](
	@symbol			varchar(10)
,	@priceTypeID	varchar(2)
,	@priceYear		smallint = 2015
,	@priceMonth		tinyint = 12
)
RETURNS @avgPrice TABLE (
		[Symbol]		varchar(10)
	,	[PriceTypeID]	varchar(2)
	,	[Year]			smallint
	,	[Month]			tinyint
	,	[Frequency]		char(2)		-- 'Mo', 'Wk', 'Da' typically converted to montly average
	,	[RecCount]		smallint
	,	[AvgPrice]		money
)
AS
BEGIN
	DECLARE @Source varchar(10)
		,	@Frequency varchar(4) = 'Wk';

	SELECT @Source = [Source], @Frequency = [Frequency] FROM [ProductPricing].[dbo].[Product_Items] WHERE [ItemCode] = @symbol AND [PriceTypeID] = @priceTypeID;
	
	DECLARE @tempItemPrices TABLE (
			[Symbol] varchar(10)
		,	[PriceTypeID] varchar(2)
		,	[CopyRank] tinyint
		,	[Rank] tinyint
		,	[PriceDate] datetime
		,	[PriceValue] money
		,	[RecCreated] datetime
	);

	IF (@Source = 'Platts')
		BEGIN
			INSERT INTO @tempItemPrices
			SELECT	'Symbol'		= [PP_ItemSymbol]
				,	'PriceTypeID'	= convert(char(2), [PP_ItemBateCode])
				,	'CopyRank'		= row_number() over (partition by convert(char(2), [PP_ItemBateCode]) order by convert(char(2), [PP_ItemBateCode]), [PP_ItemAssessment] desc)
				,	'Rank'			= rank() over (order by [PP_ItemBateCode])
				,	'PriceDate'		= [PP_ItemAssessment]
				,	'PriceValue'	= [PP_ItemPrice]
				,	'RecCreated'	= [PP_PositedAt]			
			FROM [pricing].[stage].[PP_PricingPlattsFtp_Posit]
			WHERE [PP_ItemSymbol]			= @symbol
			  AND YEAR([PP_ItemAssessment])	= @priceYear
			  AND MONTH([PP_ItemAssessment])= @priceMonth;			
		END		
	ELSE -- 'Argus'
		BEGIN
			INSERT INTO @tempItemPrices			
			SELECT	'Symbol'		= [ItemCode]
				,	'PriceTypeID'	= [PriceTypeID]
				,	'CopyRank'		= ROW_NUMBER() OVER (PARTITION BY [PriceTypeID], [PriceDate] ORDER BY [PriceTypeID], [PriceDate], [RecCreated] DESC)
				,	'Rank'			= RANK() OVER (ORDER BY [PriceTypeID])
				,	'PriceDate'		= [PriceDate]
				,	'PriceValue'	= [PriceValue]
				,	'RecCreated'	= [RecCreated]
			FROM [ProductPricing].[dbo].[ProductPrices]
			WHERE [ItemCode]			= @symbol
			  AND [PriceTypeID]			= @priceTypeID
			  AND YEAR([PriceDate])		= @priceYear
			  AND MONTH([PriceDate])	= @priceMonth;				
		END


	INSERT INTO @avgPrice ([Symbol],[PriceTypeID],[Year],[Month],[Frequency],[RecCount],[AvgPrice])
	SELECT	'Symbol'		= prtypes.[Symbol]
		,	'PriceTypeID'	= CASE WHEN prtypes.[PriceTypeID] IN ('1','2','6','7') THEN 'hl' ELSE prtypes.[PriceTypeID] END
		,	'Year'			= @priceYear
		,	'Month'			= @priceMonth
		,	'Frequency'		= CASE	WHEN @Frequency IN ('WA','Wkly','Wk','Weekly','prompt') THEN 'Wk'
									WHEN @Frequency IN ('DW','Da','Daily') THEN 'Da'
									WHEN @Frequency IN ('MA','Monthly','Mo') THEN 'Mo'
									WHEN @Frequency IN ('QR','Quarterly','QTR') THEN 'Qt'
									WHEN @Frequency IN ('YR','Yearly','Annual','Annually') THEN 'Yr'
									WHEN @Frequency IN ('BW','Bi-Weekly') THEN 'BW'
									WHEN @Frequency IN ('ID') THEN 'ID'
									WHEN @Frequency IN ('FN','Fortnightly') THEN 'FN'
								END
		,	'RecCount'		= prtypes.[RecCount]
		,	'AvgPrice'		= SUM(prtypes.[PriceTypeAvg])/COUNT(prtypes.[RecCount])
	FROM (
		select	'Symbol'		= ra.[Symbol]
			,	'PriceTypeID'	= ra.[PriceTypeID]
			,	'RecCount'		= count(PriceTypeID)
			,	'PriceTypeAvg'	= avg(ra.pricevalue)
		from (	SELECT * FROM @tempItemPrices WHERE [Rank] = 1 ) ra
		group by ra.[Symbol], ra.[PriceTypeID] ) prtypes
	GROUP BY [Symbol], CASE WHEN prtypes.[PriceTypeID] IN ('1','2','6','7') THEN 'hl' ELSE prtypes.[PriceTypeID] END, prtypes.[RecCount];

	RETURN;



/*
DECLARE @priceYear smallint = 2016,	@priceMonth tinyint = 1,	@Frequency varchar(4) = 'wkly',	@symbol varchar(10) = 'PA0003463'
	select
			'symbol'		= [pa_code]
		,	'pricetypeid'	= [pa_pricetypeid]
		,	'copyrank'		= row_number() over (partition by pa_pricetypeid, pa_date order by pa_pricetypeid, pa_date, pa_positedat desc)
		,	'pricedate'		= [pa_date]
		,	'pricevalue'	= [pa_value]
		,	'recchanged'	= [pa_changedat]
		,	'reccreated'	= [pa_positedat]
	from [pricing].[stage].[pa_pricingarguscsv_posit] 
	where [pa_code] = @symbol
	  and year([pa_date]) = @priceYear
	  and month([pa_date]) = @priceMonth
	order by pa_date, pa_pricetypeid, 'copyrank'

DECLARE @priceYear smallint = 2016,	@priceMonth tinyint = 1,	@Frequency varchar(4) = 'wkly',	@symbol varchar(10) = 'PA0003463'
select pr.*
from (
	--DECLARE @priceYear smallint = 2016,	@priceMonth tinyint = 1,	@Frequency varchar(4) = 'wkly',	@symbol varchar(10) = 'PA0003463'
	select	'symbol'		= [pa_code]
		,	'pricetypeid'	= [pa_pricetypeid]
		,	'copyrank'		= row_number() over (partition by pa_pricetypeid, pa_date order by pa_pricetypeid, pa_date, pa_positedat desc)
		,	'pricedate'		= [pa_date]
		,	'pricevalue'	= [pa_value]
		,	'recchanged'	= [pa_changedat]
		,	'reccreated'	= [pa_positedat]
	from [pricing].[stage].[pa_pricingarguscsv_posit] 
	where [pa_code] = @symbol
	  and year([pa_date]) = @priceYear
	  and month([pa_date]) = @priceMonth
	--order by pa_pricetypeid, pa_date, pa_positedat
	--order by pa_date, pa_pricetypeid, 'copyrank'
	) pr
where pr.copyrank = 1
order by pr.pricedate;
*/

END

