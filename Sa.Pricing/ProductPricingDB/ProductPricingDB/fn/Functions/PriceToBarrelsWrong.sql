﻿CREATE FUNCTION [fn].[PriceToBarrelsWrong]
(
	@Symbol		VARCHAR(10),
	@Price		MONEY = 0
)
RETURNS MONEY
AS
BEGIN

	DECLARE @UOM VARCHAR(3);

	SELECT @UOM = UOM FROM Product_Items WHERE [ItemCode] = @Symbol;

	SET @Price = CASE @UOM 
					WHEN 'MT' THEN CAST(@Price / 7.1475 as money)
					WHEN 'GAL' THEN CAST(@Price * 32.5 as money)
					ELSE --Assume already in Barrels
						@Price
				 END
	
	RETURN @Price;

END;