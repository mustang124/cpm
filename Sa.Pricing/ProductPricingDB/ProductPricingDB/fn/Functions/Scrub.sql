﻿
-- ------------------------------------------------------------------------------------------
-- Author:      Solomon & Associates, Inc.
-- Create date: 2015-09-01 by DAS
-- Description: Strip (scrib) text - clean from dirty extra characters.
-- ------------------------------------------------------------------------------------------
CREATE FUNCTION [fn].[Scrub](
	@passedValue NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @returnValue NVARCHAR(MAX) -- output string
	DECLARE @keepMe VARCHAR(50);

	SET @keepMe = '%[^a-z0-9]%'
	-- CHAR(160) ?
       
	WHILE PATINDEX(@keepMe, @passedValue) > 0
		SET @passedValue = STUFF(@passedValue, PATINDEX(@keepMe, @passedValue), 1, '');

	SET @returnValue = @passedValue;


	IF LEN(@returnValue) < 1
		SET @returnValue = NULL;

	RETURN @returnValue;
END


