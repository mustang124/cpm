﻿-- ----------------------------------------------------------------------------------------
-- Author:      Solomon & Associates, Inc.
-- Create date: 2015-09-01 by DAS
-- Description: Format text to mixed case
-- ----------------------------------------------------------------------------------------
CREATE FUNCTION [fn].[ToProperCase](
	@passedValue varchar(500)
)
RETURNS VARCHAR(500)
AS
BEGIN
    SET @passedValue = LTRIM(@passedValue);
    SET @passedValue = RTRIM(@passedValue);

	DECLARE @i int                     -- index
		,	@pv_length int             -- passed in value length
		,	@char nchar(1)             -- current char
		,	@firstLetterFlag tinyint   -- first letter flag (1/0)
		,	@returnValue varchar(500)  -- output string
		,	@whiteSpace varchar(10);   -- characters considered as white space

	SET @returnValue = ''
	SET @whiteSpace = '[' + CHAR(13) + CHAR(10) + CHAR(9) + CHAR(160) + ' ' + '-' + ']'
	SET @i = 0
	SET @pv_length = LEN(@passedValue)
	SET @firstLetterFlag = 1
       
	WHILE @i <= @pv_length
	BEGIN
		SET @char = SUBSTRING(@passedValue, @i, 1)
		IF @firstLetterFlag = 1 
			BEGIN
				SET @returnValue = @returnValue + @char
				SET @firstLetterFlag = 0
			END
		ELSE
			BEGIN
				SET @returnValue = @returnValue + LOWER(@char)
			END

		IF @char LIKE @whiteSpace 
			SET @firstLetterFlag = 1
			
		SET @i = @i + 1;

	END

	SET @returnValue = LTRIM(@returnValue);
	SET @returnValue = RTRIM(@returnValue);

	SET @returnValue = REPLACE(@returnValue, 'P O Box', 'PO BOX');
	SET @returnValue = REPLACE(@returnValue, 'P.o. Box', 'PO BOX');
	SET @returnValue = REPLACE(@returnValue, 'Po Box', 'PO BOX');
	SET @returnValue = REPLACE(@returnValue, 'Apt ', 'APT ');
	SET @returnValue = REPLACE(@returnValue, 'Apt. ', 'APT ');
	SET @returnValue = REPLACE(@returnValue, 'Apartment ', 'APT ');
	SET @returnValue = REPLACE(@returnValue, 'Apartment. ', 'APT ');
	SET @returnValue = REPLACE(@returnValue, 'Unit ', 'UNIT ');

	IF (@returnValue LIKE 'Mc%')
		SET @returnValue = 'Mc' + UPPER(SUBSTRING(@returnValue, 3, 1)) + SUBSTRING(@returnValue, 4, LEN(@returnValue));
       
	IF (@returnValue LIKE 'Mac%')
		SET @returnValue = 'Mac' + UPPER(SUBSTRING(@returnValue, 4, 1)) + SUBSTRING(@returnValue, 5, LEN(@returnValue));
       
	IF (@returnValue LIKE 'O''%')
		SET @returnValue = 'O''' + UPPER(SUBSTRING(@returnValue, 3, 1)) + SUBSTRING(@returnValue, 4, LEN(@returnValue));

	IF (@returnValue LIKE 'Iii')
		SET @returnValue = 'III' + UPPER(SUBSTRING(@returnValue, 3, 1)) + SUBSTRING(@returnValue, 4, LEN(@returnValue));

	IF LEN(@returnValue) < 1
		SET @returnValue = NULL;

	RETURN @returnValue;
END

