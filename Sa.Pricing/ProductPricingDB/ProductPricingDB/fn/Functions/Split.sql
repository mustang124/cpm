﻿-- ----------------------------------------------------------------------------
-- Author:      Solomon & Associates, Inc.
-- Create date: 2015-09-01 by DAS
-- Description: Adds common Split() funtionality.
-- SYNTAX: 
/*
       SELECT * FROM fn.Split('asdf,qwer,sfgh,68rthrhwt,sadvasdqewt', ',');
*/
-- ----------------------------------------------------------------------------
CREATE FUNCTION [fn].[Split] (@strArray nvarchar(4000), @delim nchar(1))
RETURNS @tParts TABLE (
	part nvarchar(2048)
)
AS
BEGIN
	IF (@strArray IS NULL)
		RETURN;

	DECLARE @rangeStart int
		,	@nextDelimPos int
		,	@rangeEnd int;
       
	IF (LEFT(@strArray, 1) = @delim)
		BEGIN
			SET @rangeStart = 2;
			INSERT INTO @tParts VALUES (null);
		END
	ELSE
		BEGIN
			SET @rangeStart = 1;
		END

	WHILE LEN(@strArray) > 0
		BEGIN
		SET @nextDelimPos = CHARINDEX(@delim, @strArray, @rangeStart);
		IF (@nextDelimPos = 0)
			SET @nextDelimPos = LEN(@strArray) + 1;
                     
		SET @rangeEnd = @nextDelimPos - @rangeStart;
		IF (@rangeEnd > 0)
			INSERT INTO @tParts VALUES (SUBSTRING(@strArray, @rangeStart, @rangeEnd));
		ELSE
			INSERT INTO @tParts VALUES(null);

		SET @rangeStart = @nextDelimPos + 1;
		IF (@rangeStart > len(@strArray))
			BREAK;
		END

	RETURN;
END


