﻿-- ---------------------------------------------------------------------------------------------
-- Author:		Solomon Associates
-- Programmer:	das
-- Create date: 2016.09.01
-- Description:	Report Query to return ARGUS Pricing Data
--				DROPs & CREATEs [rpt].[PricingMonthly_Argus] report table
-- Depenencies:	[dbo].[ProductItems] table
--				[dbo].[ProductPrices] table
--				[ref].[Watchlists] table
--				[ref].[Excel_Categories] table
--				[ref].[PriceTypes] table
--				[vw].[MaxPriceDates] view
--				[fn].[Scrub] function
/* Syntax:		

	EXEC [rpt].[PricingMonthly_ArgusPA];

*/
-- ------------------------------------------------------------------------------------------------------------ --
CREATE PROCEDURE [rpt].[PricingMonthly_ArgusPA]
	@CalendarYear smallint = 2015
,	@MaxPriceDate date = '2016-12-31'
,	@SymbolFilter varchar(10) = null
,	@OutputAs varchar(10) = 'datatable'				-- 'datatable' or 'confirmation'
AS
BEGIN
	SET NOCOUNT ON;

	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 1 - Set up local variables & objects
	-- -------------------------------------------------------------------------------------------------------- --
		
		IF (LEN(fn.Scrub(@SymbolFilter)) = 0)		-- account for zero-length string
			SET @SymbolFilter = NULL;

		IF OBJECT_ID('tempdb..#MonthRankArgus1') IS NOT NULL DROP TABLE #MonthRankArgus1;
		IF OBJECT_ID('tempdb..#MonthRankArgus2a') IS NOT NULL DROP TABLE #MonthRankArgus2a;
		IF OBJECT_ID('tempdb..#MonthRankArgus2b') IS NOT NULL DROP TABLE #MonthRankArgus2b;
		IF OBJECT_ID('tempdb..#MonthRankArgus3a') IS NOT NULL DROP TABLE #MonthRankArgus3a;
		IF OBJECT_ID('tempdb..#MonthRankArgus3b') IS NOT NULL DROP TABLE #MonthRankArgus3b;
		IF OBJECT_ID('tempdb..#SortedArgusAll') IS NOT NULL DROP TABLE #SortedArgusAll;
	
	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 2 - Gather 'detail' price-records into temp tables - BY 'price type' grouping
	-- -------------------------------------------------------------------------------------------------------- --
		
		-- #MonthRankArgus1 (1of1) -- All 'single' or 'stand-alone' PriceTypeID(s) ---------------------------- --
		-- ---------------------------------------------------------------------------------------------------- --
			
			SELECT	b.[ItemCode], b.[PriceTypeId], b.[PriceMonth], b.[PriceValue]
			INTO #MonthRankArgus1
			FROM (
					select	pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceTypeId] desc, pp.[PriceDate], pp.[TransType] asc)
						,	'ContFwdRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ContFwd] asc)
					from [dbo].[ProductPrices] pp
					where pp.[Source] = 'Argus'
					  and convert(varchar(2),pp.[PriceTypeId]) not in ('1','2','6','7')
					  and pp.[PriceYear] in (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					group by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType], pp.[ContFwd]
					) b
			WHERE b.[TransTypeRank] = 1 AND b.[ContFwdRank] = 1;


		-- #MonthRankArgus2a (1of2) -- PriceTypeID(s) -- Group (1,2) ------------------------------------------ --
		-- ---------------------------------------------------------------------------------------------------- --
			
			SELECT	b.[ItemCode], b.[PriceTypeId], b.[PriceMonth], b.[PriceValue]
			INTO #MonthRankArgus2a
			FROM (
					select	pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceTypeId] desc, pp.[PriceDate], pp.[TransType] asc)
						,	'ContFwdRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ContFwd] asc)
					from [dbo].[ProductPrices] pp
					where pp.[Source] = 'Argus'
					  and convert(varchar(2),pp.[PriceTypeId]) = '1'
					  and pp.[PriceYear] in (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					group by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType], pp.[ContFwd]
					) b
			WHERE b.[ItemCode] NOT IN (select distinct [ItemCode] from #MonthRankArgus1)
			  AND b.[TransTypeRank] = 1 AND b.[ContFwdRank] = 1;

		
		-- #MonthRankArgus2b (2of2) -- PriceTypeID(s) -- Group (1,2) ------------------------------------------ --
		-- ---------------------------------------------------------------------------------------------------- --
			
			SELECT	b.[ItemCode], b.[PriceTypeId], b.[PriceMonth], b.[PriceValue]
			INTO #MonthRankArgus2b
			FROM (
					select	pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceTypeId] desc, pp.[PriceDate], pp.[TransType] asc)
						,	'ContFwdRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ContFwd] asc)
					from [dbo].[ProductPrices] pp
					where pp.[Source] = 'Argus'
					  and convert(varchar(2),pp.[PriceTypeId]) = '2'
					  and pp.[PriceYear] in (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					group by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType], pp.[ContFwd]
					) b
			WHERE b.[ItemCode] NOT IN (select distinct [ItemCode] from #MonthRankArgus1)
			  AND b.[TransTypeRank] = 1 AND b.[ContFwdRank] = 1;


		-- #MonthRankArgus3a (1of2) -- PriceTypeID(s) -- Group (6,7) ------------------------------------------ --
		-- ---------------------------------------------------------------------------------------------------- --

			SELECT	b.[ItemCode], b.[PriceTypeId], b.[PriceMonth], b.[PriceValue]
			INTO #MonthRankArgus3a
			FROM (
					select	pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceTypeId] desc, pp.[PriceDate], pp.[TransType] asc)
						,	'ContFwdRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ContFwd] asc)
					from [dbo].[ProductPrices] pp
					where pp.[Source] = 'Argus'
					  and convert(varchar(2),pp.[PriceTypeId]) = '6'
					  and pp.[PriceYear] in (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					group by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType], pp.[ContFwd]
					) b
			WHERE b.[ItemCode] NOT IN (select distinct [ItemCode] from #MonthRankArgus1)
			  AND b.[TransTypeRank] = 1 AND b.[ContFwdRank] = 1;


		-- #MonthRankArgus3b (2of2) -- PriceTypeID(s) -- Group (6,7) ------------------------------------------ --
		-- ---------------------------------------------------------------------------------------------------- --

			SELECT	b.[ItemCode], b.[PriceTypeId], b.[PriceMonth], b.[PriceValue]
			INTO #MonthRankArgus3b
			FROM (
					select	pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear]
						,	'PriceMonth'	= (CASE pp.[PriceYear] WHEN 2015 THEN pp.[PriceMonth] WHEN 2016 THEN pp.[PriceMonth] + 12 END)
						,	'PriceDate'		= convert(varchar(10), pp.[PriceDate], 120)
						,	'PriceValue'	= pp.[PriceValue]
						,	'TransType'		= pp.[TransType]
						,	'TransTypeRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ItemCode], pp.[PriceTypeId] desc, pp.[PriceDate], pp.[TransType] asc)
						,	'ContFwdRank'	= dense_rank() over (partition by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceDate] order by pp.[ContFwd] asc)
					from [dbo].[ProductPrices] pp
					where pp.[Source] = 'Argus'
					  and convert(varchar(2),pp.[PriceTypeId]) = '7'
					  and pp.[PriceYear] in (2015,2016)
					  and convert(varchar(10), pp.[PriceDate], 120) <= @MaxPriceDate
					group by pp.[ItemCode], pp.[PriceTypeId], pp.[PriceYear], pp.[PriceMonth], pp.[PriceDate], pp.[PriceValue], pp.[TransType], pp.[ContFwd]
					) b
			WHERE b.[ItemCode] NOT IN (select distinct [ItemCode] from #MonthRankArgus1)
			  AND b.[TransTypeRank] = 1 AND b.[ContFwdRank] = 1;

		
		-- #SortedArgusAll (Aggregate All) -------------------------------------------------------------------- --
		-- ---------------------------------------------------------------------------------------------------- --
			
			SELECT *
			INTO #SortedArgusAll
			FROM (	
					SELECT *
					FROM  #MonthRankArgus1 src
					pivot ( 
						AVG([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
					) piv	
				
					UNION ALL
				
					SELECT [ItemCode], 'PriceTypeId' = '1'
						,	'1'=avg([1]),  '2'=avg([2]),  '3'=avg([3]),  '4'=avg([4]),  '5'=avg([5]),  '6'=avg([6]),  '7'=avg([7]),  '8'=avg([8]),  '9'=avg([9]),  '10'=avg([10]),'11'=avg([11]),'12'=avg([12])
						,	'13'=avg([13]),'14'=avg([14]),'15'=avg([15]),'16'=avg([16]),'17'=avg([17]),'18'=avg([18]),'19'=avg([19]),'20'=avg([20]),'21'=avg([21]),'22'=avg([22]),'23'=avg([23]),'24'=avg([24])
					FROM (
						SELECT	*
						FROM (
								SELECT *
								FROM  #MonthRankArgus2a pt
								pivot ( 
									AVG([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
								) piv
								
								UNION ALL

								SELECT *
								FROM  #MonthRankArgus2b pt
								pivot ( 
									AVG([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
								) piv
							) src
						) u
					GROUP BY [ItemCode]
				
					UNION ALL
				
					SELECT [ItemCode], 'PriceTypeId' = '6'
						,	'1'=avg([1]),  '2'=avg([2]),  '3'=avg([3]),  '4'=avg([4]),  '5'=avg([5]),  '6'=avg([6]),  '7'=avg([7]),  '8'=avg([8]),  '9'=avg([9]),  '10'=avg([10]),'11'=avg([11]),'12'=avg([12])
						,	'13'=avg([13]),'14'=avg([14]),'15'=avg([15]),'16'=avg([16]),'17'=avg([17]),'18'=avg([18]),'19'=avg([19]),'20'=avg([20]),'21'=avg([21]),'22'=avg([22]),'23'=avg([23]),'24'=avg([24])
					FROM (
						SELECT	*
						FROM (
								SELECT *
								FROM  #MonthRankArgus3a pt
								pivot ( 
									AVG([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
								) piv
								
								UNION ALL

								SELECT *
								FROM  #MonthRankArgus3b pt
								pivot ( 
									AVG([PriceValue]) FOR [PriceMonth] IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],[21],[22],[23],[24])
								) piv
							) src
						) u
					GROUP BY [ItemCode]
				) a;

		
		-- APPEND Watch-List ItemCodes where no price information was found for the period (2015-2016) -------- --
		-- ---------------------------------------------------------------------------------------------------- --

			INSERT INTO #SortedArgusAll ([ItemCode],[PriceTypeId])
				SELECT	'ItemCode'		= wl.[ItemCode], 
						'PriceTypeID'	= (SELECT MIN(PriceTypeID) FROM [ProductItems] WHERE [Source] = 'Argus' AND [ItemCode] = wl.[ItemCode])
				FROM	[ref].[Watchlists] wl
				WHERE	wl.[ItemCode] NOT IN (SELECT DISTINCT [ItemCode] FROM #SortedArgusAll);


	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 4 - MAIN REPORT QUERY - Place values in [rpt] table prior to return;
	-- -------------------------------------------------------------------------------------------------------- --
		
		-- ---------------------------------------------------------------------------------------------------- --
			IF OBJECT_ID('[rpt].[PricingMonthly_Argus]','U') IS NOT NULL
			BEGIN
				TRUNCATE TABLE [rpt].[PricingMonthly_Argus];
				DROP TABLE [rpt].[PricingMonthly_Argus];
			END

		-- ---------------------------------------------------------------------------------------------------- --
			SELECT	'Source'		= CONCAT(Items.[Source], (select ' WL*' from ref.Watchlists where itemcode = p.[ItemCode]) )
				,	'ItemCode'		= Items.[ItemCode]
				,	'PriceTypeID'	= CASE p.[PriceTypeId] WHEN '1' THEN '1,2' WHEN '6' THEN '6,7' ELSE p.[PriceTypeID] END
				,	'ParentCode'	= ISNULL(Items.[ParentCode],'')
				,	'PriorityRank'	= ISNULL(Items.[PriorityRank],'')
				,	'CNUM'			= CAST('' AS CHAR(5))
				,	'MNUM'			= CAST('' AS CHAR(5))				
				,	'PriceType'		= CASE p.[PriceTypeId] WHEN '1' THEN 'high-low' WHEN '6' THEN 'high-low' else (SELECT [AR_Description] FROM [ref].[PriceTypes] WHERE [AR_PriceTypeId] = p.[PriceTypeId]) END
				,	'ExcelKeyCode'	= ISNULL(Items.[ExcelKeyCode], '')
				,	'ExcelKey'		= ISNULL((SELECT [CatCommodity] FROM [ref].[Excel_Categories] WHERE [CatKey] = Items.[ExcelKeyCode]), '')
				,	'Module'		= ISNULL(Items.[Module], '')
				,	'Category'		= ISNULL(Items.[Category], '')
				,	'Region'		= ISNULL(Items.[Region], '')
				,	'Location'		= ISNULL(Items.[Location], '')				
				,	'DisplayName'	= ISNULL(Items.[DisplayName], '')
				,	'Detail'		= ISNULL(Items.[Detail], '')
				,	'CUR'			= ISNULL(Items.[Currency], '')
				,	'UOM'			= ISNULL(Items.[UOM], '')
				,	'FREQ'			= CASE	WHEN Items.[Frequency] IN ('Monthly','MA','mm') THEN 'Mo'
											WHEN Items.[Frequency] IN ('Weekly','WA','DW','wk') THEN 'Wk'
											WHEN Items.[Frequency] IN ('Daily','DA','DW','d5','d7','dd') THEN 'Da'
											WHEN Items.[Frequency] IN ('Fortnightly','fn') THEN 'Fn'
											WHEN Items.[Frequency] IN ('Quarterly','QR','qq') THEN 'Qr' 
											WHEN Items.[Frequency] IN ('Yearly','YR','Annual','Annually','yy') THEN 'Yr'
											WHEN Items.[Frequency] IN ('Bi-Weekly','BW') THEN 'Bw'
											WHEN Items.[Frequency] IN ('Bi-Monthly','Bm') THEN 'Bm'
											WHEN Items.[Frequency] IN ('Bi-Yearly','By') THEN 'By'
											WHEN Items.[Frequency] IN ('Semi-Weekly','sw') THEN 'Sw'
											WHEN Items.[Frequency] IN ('Semi-Monthly','sm') THEN 'Sm'
											WHEN Items.[Frequency] IN ('Semi-Yearly','sy') THEN 'Sy'
											WHEN Items.[Frequency] IN ('Hourly', 'hh') THEN 'Hr'
											WHEN Items.[Frequency] IN ('Intraday', 'ID') THEN 'Id'
											ELSE ISNULL(Items.[Frequency],'') END
				,	'Jan15'			= ROUND([1],2), 'Feb15'=ROUND([2],2), 'Mar15'=ROUND([3],2), 'Apr15'=ROUND([4],2), 'May15'=ROUND([5],2), 'Jun15'=ROUND([6],2), 'Jul15'=ROUND([7],2), 'Aug15'=ROUND([8],2), 'Sep15'=ROUND([9],2), 'Oct15'=ROUND([10],2),'Nov15'=ROUND([11],2),'Dec15'=ROUND([12],2)	
				,	'Jan16'			= ROUND([13],2),'Feb16'=ROUND([14],2),'Mar16'=ROUND([15],2),'Apr16'=ROUND([16],2),'May16'=ROUND([17],2),'Jun16'=ROUND([18],2),'Jul16'=ROUND([19],2),'Aug16'=ROUND([20],2),'Sep16'=ROUND([21],2),'Oct16'=ROUND([22],2),'Nov16'=ROUND([23],2),'Dec16'=ROUND([24],2)
				,	'Q1Cnt15'		= CAST(0 as int)
				,	'Q2Cnt15'		= CAST(0 as int)
				,	'Q3Cnt15'		= CAST(0 as int)
				,	'Q4Cnt15'		= CAST(0 as int)
				,	'Q1Avg15'		= CAST(0 as money)
				,	'Q2Avg15'		= CAST(0 as money)
				,	'Q3Avg15'		= CAST(0 as money)
				,	'Q4Avg15'		= CAST(0 as money)				
				,	'YTDCnt15'		= CAST(0 as int)
				,	'YTDAvg15'		= CAST(0.0000 as money)
				,	'Q1Cnt16'		= CAST(0 as int)
				,	'Q2Cnt16'		= CAST(0 as int)
				,	'Q3Cnt16'		= CAST(0 as int)
				,	'Q4Cnt16'		= CAST(0 as int)				
				,	'Q1Avg16'		= CAST(0 as money)
				,	'Q2Avg16'		= CAST(0 as money)
				,	'Q3Avg16'		= CAST(0 as money)
				,	'Q4Avg16'		= CAST(0 as money)				
				,	'YTDCnt16'		= CAST(0 as int)
				,	'YTDAvg16'		= CAST(0.0000 as money)
				,	'Note'			= ISNULL(Items.[Note],'')
				,	'NotNeeded'		= CASE Items.[NotNeeded] WHEN 'True' THEN 'x' ELSE '' END
				,	'Reason'		= ISNULL(Items.[Reason],'')
				,	'Primary'		= CASE Items.[IsPrimary] WHEN 'True' THEN 'x' ELSE '' END
				,	'Secondary'		= CASE Items.[IsSecondary] WHEN 'True' THEN 'x' ELSE '' END
				,	'Tertiary'		= CASE Items.[IsTertiary] WHEN 'True' THEN 'x' ELSE '' END
				,	'StartDate'		= ISNULL(CASE WHEN Items.[Earliest_DT] IS NULL THEN NULL
												WHEN YEAR(Items.[Earliest_DT]) >= 2015 THEN convert(varchar(10), Items.[Earliest_DT], 120) 
												ELSE 'x' END, '')
				,	'EndDate'		= ISNULL(CASE WHEN Items.[Latest_DT] IS NULL THEN NULL 
												WHEN m.[MAXPriceDate] > convert(varchar(10), Items.[Latest_DT], 120) THEN 'x'
												WHEN convert(varchar(10), dateadd(MM, -2, getdate()), 120) < convert(varchar(10), Items.[Latest_DT], 120) THEN 'x'
												ELSE convert(varchar(10), Items.[Latest_DT], 120) END, '')
				,	'Reviewed'		= CASE Items.[ExcelReviewed] WHEN 'True' THEN 'x' ELSE '' END
				,	'SortPrev'		= ISNULL([CSort2], '')
				,	'SortNew'		= ROW_NUMBER() OVER ( ORDER BY Items.[ItemCode], Items.[PriceTypeID] )
			INTO [rpt].[PricingMonthly_Argus]
			FROM [dbo].[ProductItems] Items
				INNER JOIN #SortedArgusAll p ON Items.[ItemCode] = p.[ItemCode] AND Items.[PriceTypeId] = p.[PriceTypeId]
				LEFT OUTER JOIN [vw].[MaxPriceDates] m ON Items.[ItemCode] = m.[ItemCode] AND Items.[Source] = m.[Source] AND Items.[PriceTypeId] = m.[PriceTypeId]
			WHERE Items.[Source] = 'Argus'
			ORDER BY p.[ItemCode], p.[PriceTypeID];


	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 5 - Create ROLLUP Records
	-- -------------------------------------------------------------------------------------------------------- --

			-- Clean out any "virtual" records & create new vars for dynamic sql
				DELETE FROM [rpt].[PricingMonthly_Argus] WHERE PriorityRank = -1;
				DECLARE @_iMonthNumber	int = 1
					,	@sql			nvarchar(max);

			-- MAIN Loop through 24 months of columns
				WHILE (@_iMonthNumber <= 24)
				BEGIN
					-- Clear temp table that is used to store the month's rollup value
						IF OBJECT_ID('tempdb..#PricingMonthly_Argus','U') IS NOT NULL DROP TABLE #PricingMonthly_Argus;
					
					-- Define the monthly average's column name
						DECLARE @MonthAvg varchar(5);

						SELECT @MonthAvg = CASE @_iMonthNumber	WHEN 1 THEN 'Jan15' WHEN 2 THEN 'Feb15' WHEN 3 THEN 'Mar15' WHEN 4 THEN 'Apr15' WHEN 5 THEN 'May15' WHEN 6 THEN 'Jun15'
																WHEN 7 THEN 'Jul15' WHEN 8 THEN 'Aug15' WHEN 9 THEN 'Sep15' WHEN 10 THEN 'Oct15' WHEN 11 THEN 'Nov15' WHEN 12 THEN 'Dec15'
																WHEN 13 THEN 'Jan16' WHEN 14 THEN 'Feb16' WHEN 15 THEN 'Mar16' WHEN 16 THEN 'Apr16' WHEN 17 THEN 'May16' WHEN 18 THEN 'Jun16'
																WHEN 19 THEN 'Jul16' WHEN 20 THEN 'Aug16' WHEN 21 THEN 'Sep16' WHEN 22 THEN 'Oct16' WHEN 23 THEN 'Nov16' WHEN 24 THEN 'Dec16'
																END
					
					-- Build temp Table Variable as the 1st statement in the dynamic sql code
						SET @sql = '';
						SET @sql = CONCAT(@sql, N'
						DECLARE @RollupCodes TABLE (
								[ItemCode]		varchar(10)
							,	[PriceTypeID]	varchar(3)
							,	[', @MonthAvg,']	money
							,	[PriorityRank]	int
							,	[OrderBy]		int
						);', char(10), char(10));

					-- Build and use CTE (common table expression) to "pull up" the first NON-ZERO value for the month
--required			-- (Modified to ROLLUP zeros instead of nulls incase 0.00 is the average)
					-- a.) 'allCodes'		= all codes in report table that contain a [ParentCode] value, linked by [PriceTypeID]
					-- b.) 'notNullCodes'	= subset of 'allCodes' where [PriceValue] is not null ordered by [PriorityRank] to determine the ROLLUP code to use next
					-- c.) 'nullCodes'		= subset of 'allCodes' where item NOT IN 'notNullCodes' UNION'd to be sure we show Zero averages where no ROLLUP price exists
						SET @sql = CONCAT(@sql, N'
						WITH allCodes AS
							(
								SELECT	[ItemCode]		= IsNull(fn.Scrub(allOfEm.[ParentCode]), fn.Scrub(allOfEm.[ItemCode]))
									,	[PriceTypeID]	= allOfEm.[PriceTypeID]
									,	[', @MonthAvg,']	= allOfEm.[', @MonthAvg, N']
									,	[PriorityRank]	= allOfEm.[PriorityRank]
								FROM [rpt].[PricingMonthly_Argus] allOfEm
									INNER JOIN [rpt].[PricingMonthly_Argus] ParentsOnly ON CONCAT(allOfEm.[ParentCode], allOfEm.[PriceTypeID]) = CONCAT(ParentsOnly.[ItemCode], ParentsOnly.[PriceTypeID])
								WHERE fn.Scrub(allOfEm.[ParentCode]) IS NOT NULL
								  AND ParentsOnly.[PriorityRank] = 0
							)
						, notNullCodes AS
							(
								SELECT [ItemCode], [PriceTypeID], [', @MonthAvg,'], [PriorityRank], [OrderBy] = ROW_NUMBER() OVER (PARTITION BY [ItemCode], [PriceTypeID] ORDER BY [PriorityRank])
								FROM allCodes
								WHERE [', @MonthAvg,'] IS NOT NULL
							)
						, nullCodes AS
							(
								SELECT [ItemCode], [PriceTypeID], [', @MonthAvg,'], [PriorityRank] = 0, [OrderBy] = 1
								FROM allCodes
								WHERE [', @MonthAvg,'] IS NULL
								 AND [ItemCode] NOT IN ( select ItemCode from notNullCodes )
							)
						INSERT INTO @RollupCodes
						SELECT *
						FROM notNullCodes nnc
						WHERE nnc.[OrderBy] = 1
						UNION ALL
						SELECT *
						FROM nullCodes nc
						GROUP BY [ItemCode],[PriceTypeID],[', @MonthAvg,'],[PriorityRank],[OrderBy]
						ORDER BY [ItemCode], [PriorityRank];', char(10), char(10));
	
				-- Create ROLLUP "Virtual" records
				-- -------------------------------------------------------------------------------------------- --

					-- Now that we have the ROLLUP value in @RollupCodes table variable, let's build a virtual record in #PricingMonthly_Argus to store the "rolled up" value in
						SET @sql = CONCAT(@sql, 'SELECT rpt.* INTO #PricingMonthly_Argus FROM [rpt].[PricingMonthly_Argus] rpt INNER JOIN @RollupCodes rollup ON rpt.[ItemCode] = rollup.[ItemCode] AND rpt.[PriceTypeID] = rollup.[PriceTypeID] AND rpt.[PriorityRank] = 0;', char(10));
					
					-- Update "virtual" record with a [PriorityRank] of '-1'
						SET @sql = CONCAT(@sql, 'UPDATE #PricingMonthly_Argus SET [PriorityRank] = -1;', char(10));
					
					-- Update "virtual" record with the rolled up monthy average value for the correct month
						SET @sql = CONCAT(@sql, 'UPDATE rpt SET rpt.[', @MonthAvg, '] = rollup.[', @MonthAvg,'] FROM #PricingMonthly_Argus rpt INNER JOIN @RollupCodes rollup ON rpt.[ItemCode] = rollup.[ItemCode] AND rpt.[PriceTypeID] = rollup.[PriceTypeID];', char(10));
		
					-- INSERT or UPDATE the report table's "virtual" record: [rpt].[PricingMontly_Argus] 
						IF (@_iMonthNumber = 1)
							SET @sql = CONCAT(@sql, 'INSERT INTO [rpt].[PricingMonthly_Argus] SELECT * FROM #PricingMonthly_Argus;', char(10));
						ELSE
							SET @sql = CONCAT(@sql, 'UPDATE rpt SET rpt.[', @MonthAvg, '] = rollup.[', @MonthAvg,'] FROM [rpt].[PricingMonthly_Argus] rpt INNER JOIN #PricingMonthly_Argus rollup ON rpt.[ItemCode] = rollup.[ItemCode] AND rpt.[PriceTypeID] = rollup.[PriceTypeID] AND rpt.[PriorityRank] = -1;', char(10));

					-- EXECUTE entire process as a single dynamic sql transaction
						EXEC (@sql);
		
					-- Move to next 'month' in loop
						SET @_iMonthNumber = @_iMonthNumber + 1;

				END --WHILE (@_iMonthNumber <= 24)

	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 6 - Calculate Averages
	-- -------------------------------------------------------------------------------------------------------- --

		-- Create Counts for Q1-Q4 2015 Averages Calc
		-- ---------------------------------------------------------------------------------------------------- --

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q1Cnt15] = IIF(ISNULL([Jan15],0)=0,0,1) + IIF(ISNULL([Feb15],0)=0,0,1) + IIF(ISNULL([Mar15],0)=0,0,1)
							 
		
			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q2Cnt15] = IIF(ISNULL([Apr15],0)=0,0,1) + IIF(ISNULL([May15],0)=0,0,1) + IIF(ISNULL([Jun15],0)=0,0,1)
							 

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q3Cnt15] = IIF(ISNULL([Jul15],0)=0,0,1) + IIF(ISNULL([Aug15],0)=0,0,1) + IIF(ISNULL([Sep15],0)=0,0,1)

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q4Cnt15] = IIF(ISNULL([Oct15],0)=0,0,1) + IIF(ISNULL([Nov15],0)=0,0,1) + IIF(ISNULL([Dec15],0)=0,0,1)
		
		-- Create Counts for Q1-Q4 2016 Averages Calc
		-- ---------------------------------------------------------------------------------------------------- --

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q1Cnt16] = IIF(ISNULL([Jan16],0)=0,0,1) + IIF(ISNULL([Feb16],0)=0,0,1) + IIF(ISNULL([Mar16],0)=0,0,1)
							 
		
			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q2Cnt16] = IIF(ISNULL([Apr16],0)=0,0,1) + IIF(ISNULL([May16],0)=0,0,1) + IIF(ISNULL([Jun16],0)=0,0,1)
							 

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q3Cnt16] = IIF(ISNULL([Jul16],0)=0,0,1) + IIF(ISNULL([Aug16],0)=0,0,1) + IIF(ISNULL([Sep16],0)=0,0,1)

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q4Cnt16] = IIF(ISNULL([Oct16],0)=0,0,1) + IIF(ISNULL([Nov16],0)=0,0,1) + IIF(ISNULL([Dec16],0)=0,0,1)

		-- Create Counts for YTD Averages Calc
		-- ---------------------------------------------------------------------------------------------------- --

			UPDATE [rpt].[PricingMonthly_Argus]
			SET [YTDCnt15] = IIF(ISNULL([Jan15],0)=0,0,1) + IIF(ISNULL([Feb15],0)=0,0,1) + IIF(ISNULL([Mar15],0)=0,0,1) + IIF(ISNULL([Apr15],0)=0,0,1) + IIF(ISNULL([May15],0)=0,0,1) + IIF(ISNULL([Jun15],0)=0,0,1)
								 + IIF(ISNULL([Jul15],0)=0,0,1) + IIF(ISNULL([Aug15],0)=0,0,1) + IIF(ISNULL([Sep15],0)=0,0,1) + IIF(ISNULL([Oct15],0)=0,0,1) + IIF(ISNULL([Nov15],0)=0,0,1) + IIF(ISNULL([Dec15],0)=0,0,1)
		
			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [YTDCnt16] = IIF(ISNULL([Jan16],0)=0,0,1) + IIF(ISNULL([Feb16],0)=0,0,1) + IIF(ISNULL([Mar16],0)=0,0,1) + IIF(ISNULL([Apr16],0)=0,0,1) + IIF(ISNULL([May16],0)=0,0,1) + IIF(ISNULL([Jun16],0)=0,0,1)
								 + IIF(ISNULL([Jul16],0)=0,0,1) + IIF(ISNULL([Aug16],0)=0,0,1) + IIF(ISNULL([Sep16],0)=0,0,1) + IIF(ISNULL([Oct16],0)=0,0,1) + IIF(ISNULL([Nov16],0)=0,0,1) + IIF(ISNULL([Dec16],0)=0,0,1)
	
		-- Calc Quarterly Averages
		-- ---------------------------------------------------------------------------------------------------- --
			DECLARE @zero Money = 0.0000;

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q1Avg15] = CASE WHEN ISNULL([Q1Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan15],@zero) + ISNULL([Feb15],@zero) + ISNULL([Mar15],@zero)) / [Q1Cnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q2Avg15] = CASE WHEN ISNULL([Q2Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Apr15],@zero) + ISNULL([May15],@zero) + ISNULL([Jun15],@zero)) / [Q2Cnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q3Avg15] = CASE WHEN ISNULL([Q3Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jul15],@zero) + ISNULL([Aug15],@zero) + ISNULL([Sep15],@zero)) / [Q3Cnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q4Avg15] = CASE WHEN ISNULL([Q4Cnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Oct15],@zero) + ISNULL([Nov15],@zero) + ISNULL([Dec15],@zero)) / [Q4Cnt15], 2) END


			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q1Avg16] = CASE WHEN ISNULL([Q1Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan16],@zero) + ISNULL([Feb16],@zero) + ISNULL([Mar16],@zero)) / [Q1Cnt16], 2) END

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q2Avg16] = CASE WHEN ISNULL([Q2Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Apr16],@zero) + ISNULL([May16],@zero) + ISNULL([Jun16],@zero)) / [Q2Cnt16], 2) END

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q3Avg16] = CASE WHEN ISNULL([Q3Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jul16],@zero) + ISNULL([Aug16],@zero) + ISNULL([Sep16],@zero)) / [Q3Cnt16], 2) END

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [Q4Avg16] = CASE WHEN ISNULL([Q4Cnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Oct16],@zero) + ISNULL([Nov16],@zero) + ISNULL([Dec16],@zero)) / [Q4Cnt16], 2) END

		-- Calc YTD Averages
		-- ---------------------------------------------------------------------------------------------------- --
		
			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [YTDAvg15] = CASE WHEN ISNULL([YTDCnt15],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan15],@zero) + ISNULL([Feb15],@zero) + ISNULL([Mar15],@zero) + ISNULL([Apr15],@zero) + ISNULL([May15],@zero) + ISNULL([Jun15],@zero)
																						+ ISNULL([Jul15],@zero) + ISNULL([Aug15],@zero) + ISNULL([Sep15],@zero) + ISNULL([Oct15],@zero) + ISNULL([Nov15],@zero) + ISNULL([Dec15],@zero)) / [YTDCnt15], 2) END

			UPDATE [rpt].[PricingMonthly_Argus] 
			SET [YTDAvg16] = CASE WHEN ISNULL([YTDCnt16],0) = 0 THEN 0 ELSE ROUND( (ISNULL([Jan16],@zero) + ISNULL([Feb16],@zero) + ISNULL([Mar16],@zero) + ISNULL([Apr16],@zero) + ISNULL([May16],@zero) + ISNULL([Jun16],@zero)
																						+ ISNULL([Jul16],@zero) + ISNULL([Aug16],@zero) + ISNULL([Sep16],@zero) + ISNULL([Oct16],@zero) + ISNULL([Nov16],@zero) + ISNULL([Dec16],@zero)) / [YTDCnt16], 2) END
	
	
	-- -------------------------------------------------------------------------------------------------------- --
	-- STEP 7 - Return full table or just a confirmation: based on input param @OutputAs
	-- -------------------------------------------------------------------------------------------------------- --	
	
		-- Show table: null = default = 'datatable'
		-- ---------------------------------------------------------------------------------------------------- --
			IF (@OutputAs = 'datatable')
				SELECT * FROM [rpt].[PricingMonthly_Argus] ORDER BY [ItemCode], [PriceTypeID];
			ELSE 
				SELECT 'Confirmation' = 1;

END

/*
	
	EXEC [rpt].[PricingMonthly_ArgusPA];

*/


