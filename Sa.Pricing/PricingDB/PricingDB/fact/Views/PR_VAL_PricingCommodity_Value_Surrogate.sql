﻿CREATE VIEW [fact].[PR_VAL_PricingCommodity_Value_Surrogate]
WITH SCHEMABINDING
AS
SELECT
	[p].[PR_VAL_ID],
	[p].[PR_VAL_PR_ID],
	[p].[PR_VAL_Open],
	[p].[PR_VAL_Close],
	[p].[PR_VAL_Low],
	[p].[PR_VAL_High],
	[p].[PR_VAL_Volume],
	[p].[PR_VAL_ChangedAt],

	[a].[PR_VAL_PositedBy],
	[a].[PR_VAL_PositedAt],
	[a].[PR_VAL_PositReliability],
	[a].[PR_VAL_PositReliable]
FROM
	[fact].[PR_VAL_PricingCommodity_Value_Posit]	[p]
INNER JOIN
	[fact].[PR_VAL_PricingCommodity_Value_Annex]	[a]
		ON	[a].[PR_VAL_ID]		= [p].[PR_VAL_ID];
GO
CREATE UNIQUE CLUSTERED INDEX [PK__PR_VAL_PricingValue_Surrogate]
    ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]([PR_VAL_PR_ID] ASC, [PR_VAL_ChangedAt] DESC, [PR_VAL_PositedAt] DESC, [PR_VAL_PositedBy] ASC, [PR_VAL_PositReliable] ASC) WITH (FILLFACTOR = 95, STATISTICS_NORECOMPUTE = ON);


GO
CREATE NONCLUSTERED INDEX [IX__PR_VAL_PricingValue_Surrogate]
    ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]([PR_VAL_ChangedAt] DESC, [PR_VAL_PositedAt] DESC)
    INCLUDE([PR_VAL_ID], [PR_VAL_PR_ID], [PR_VAL_Open], [PR_VAL_Close], [PR_VAL_Low], [PR_VAL_High], [PR_VAL_Volume], [PR_VAL_PositedBy], [PR_VAL_PositReliability], [PR_VAL_PositReliable]) WITH (FILLFACTOR = 95, STATISTICS_NORECOMPUTE = ON);


GO
CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_ChangedAt]
    ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]([PR_VAL_ChangedAt])
    WITH NORECOMPUTE;


GO
CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_ID]
    ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]([PR_VAL_ID])
    WITH NORECOMPUTE;


GO
CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_PositedAt]
    ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]([PR_VAL_PositedAt])
    WITH NORECOMPUTE;


GO
CREATE STATISTICS [ST__PR_VAL_PricingValue_Surrogate_PR_VAL_PositReliable]
    ON [fact].[PR_VAL_PricingCommodity_Value_Surrogate]([PR_VAL_PositReliable])
    WITH NORECOMPUTE;

