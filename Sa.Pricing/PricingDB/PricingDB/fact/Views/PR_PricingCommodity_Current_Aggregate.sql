﻿CREATE VIEW [fact].[PR_PricingCommodity_Current_Aggregate]
WITH SCHEMABINDING
AS
SELECT
	[p].[PR_CommodityId],
	[p].[PR_PriceDate_Year],
	[p].[PR_PriceDate_Semester],
	[p].[PR_PriceDate_Quarter],
	[p].[PR_PriceDate_Month],

	[p].[PR_FrequencyId],
	[p].[PR_CurrencyId],
	[p].[PR_UomId],
	[p].[PR_DataSourceId],

		[PR_VAL_Open_Avg]				= AVG([p].[PR_VAL_Open]),
		[PR_VAL_Close_Avg]				= AVG([p].[PR_VAL_Close]),
		[PR_VAL_Low_Avg]				= AVG([p].[PR_VAL_Low]),
		[PR_VAL_High_Avg]				= AVG([p].[PR_VAL_High]),
		[PR_VAL_Volume_Avg]				= AVG([p].[PR_VAL_Volume]),

		[PR_TRD_Mean_Avg]				= AVG([p].[PR_TRD_Mean]),
		[PR_TRD_Ask_Avg]				= AVG([p].[PR_TRD_Ask]),
		[PR_TRD_Bid_Avg]				= AVG([p].[PR_TRD_Bid]),
		[PR_TRD_PrevInterest_Avg]		= AVG([p].[PR_TRD_PrevInterest]),
		[PR_TRD_Rate_Avg]				= AVG([p].[PR_TRD_Rate]),
		[PR_TRD_Margin_Avg]				= AVG([p].[PR_TRD_Margin]),
		[PR_TRD_DiffLow_Avg]			= AVG([p].[PR_TRD_DiffLow]),
		[PR_TRD_DiffHigh_Avg]			= AVG([p].[PR_TRD_DiffHigh]),
		[PR_TRD_DiffIndex_Avg]			= AVG([p].[PR_TRD_DiffIndex]),
		[PR_TRD_DiffMidpoint_Avg]		= AVG([p].[PR_TRD_DiffMidpoint]),
		[PR_TRD_Midpoint_Avg]			= AVG([p].[PR_TRD_Midpoint]),
		[PR_TRD_Netback_Avg]			= AVG([p].[PR_TRD_Netback]),
		[PR_TRD_NetbackMargin_Avg]		= AVG([p].[PR_TRD_NetbackMargin]),
		[PR_TRD_RGV_Avg]				= AVG([p].[PR_TRD_RGV]),
		[PR_TRD_CumulativeIndex_Avg]	= AVG([p].[PR_TRD_CumulativeIndex]),
		[PR_TRD_CumulativeVolume_Avg]	= AVG([p].[PR_TRD_CumulativeVolume]),
		[PR_TRD_TransportCosts_Avg]		= AVG([p].[PR_TRD_TransportCosts]),

		[Items]							= COUNT(1)
FROM (
	SELECT
		[p].[PR_CommodityId],
		[p].[PR_PriceDate],
			[PR_PriceDate_Year]			= YEAR([p].[PR_PriceDate]),
			[PR_PriceDate_Semester]		= CASE WHEN (MONTH([p].[PR_PriceDate]) <= 6) THEN 1 ELSE 2 END,
			[PR_PriceDate_Quarter]		= DATEPART(QUARTER,[p].[PR_PriceDate]),
			[PR_PriceDate_Month]		= MONTH([p].[PR_PriceDate]),

		[p].[PR_FrequencyId],
		[p].[PR_CurrencyId],
		[p].[PR_UomId],
		[p].[PR_DataSourceId],

		[p].[PR_VAL_Open],
		[p].[PR_VAL_Close],
		[p].[PR_VAL_Low],
		[p].[PR_VAL_High],
		[p].[PR_VAL_Volume],

		[p].[PR_TRD_Mean],
		[p].[PR_TRD_Ask],
		[p].[PR_TRD_Bid],
		[p].[PR_TRD_PrevInterest],
		[p].[PR_TRD_Rate],
		[p].[PR_TRD_Margin],
		[p].[PR_TRD_DiffLow],
		[p].[PR_TRD_DiffHigh],
		[p].[PR_TRD_DiffIndex],
		[p].[PR_TRD_DiffMidpoint],
		[p].[PR_TRD_Midpoint],
		[p].[PR_TRD_Netback],
		[p].[PR_TRD_NetbackMargin],
		[p].[PR_TRD_RGV],
		[p].[PR_TRD_CumulativeIndex],
		[p].[PR_TRD_CumulativeVolume],
		[p].[PR_TRD_TransportCosts]
	FROM
		[fact].[PR_PricingCommodity_Current]	[p]
	) [p]
GROUP BY
	[p].[PR_CommodityId],
	[p].[PR_PriceDate_Year],
	[p].[PR_FrequencyId],
	[p].[PR_CurrencyId],
	[p].[PR_UomId],
	[p].[PR_DataSourceId],
	ROLLUP([p].[PR_PriceDate_Semester], [p].[PR_PriceDate_Quarter], [p].[PR_PriceDate_Month]);