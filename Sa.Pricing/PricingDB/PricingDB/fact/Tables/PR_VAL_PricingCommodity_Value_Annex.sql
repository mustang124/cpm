﻿CREATE TABLE [fact].[PR_VAL_PricingCommodity_Value_Annex] (
    [PR_VAL_ID]               INT                NOT NULL,
    [PR_VAL_PositedBy]        INT                CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [PR_VAL_PositedAt]        DATETIMEOFFSET (7) CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PR_VAL_PositReliability] DECIMAL (5, 2)     CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_Reliability] DEFAULT ((0.0)) NOT NULL,
    [PR_VAL_PositReliable]    AS                 (CONVERT([bit],case when [PR_VAL_PositReliability]>(0.0) then (1) else (0) end)) PERSISTED NOT NULL,
    [PR_VAL_PositAssertion]   AS                 (CONVERT([char](1),coalesce(case when [PR_VAL_PositReliability]>(0.0) then '+' when [PR_VAL_PositReliability]<(0.0) then '-' else '?' end,'?'))) PERSISTED NOT NULL,
    [PR_VAL_RowGuid]          UNIQUEIDENTIFIER   CONSTRAINT [DF__PR_VAL_PricingCommodity_Value_Annex_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__PR_VAL_PricingCommodity_Value_Annex] PRIMARY KEY CLUSTERED ([PR_VAL_ID] ASC, [PR_VAL_PositedBy] ASC, [PR_VAL_PositedAt] DESC) WITH (FILLFACTOR = 95),
    CONSTRAINT [FK__PR_VAL_PricingCommodity_Value_Annex_PositedBy] FOREIGN KEY ([PR_VAL_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [FK__PR_VAL_PricingCommodity_Value_Annex_PR_VAL_ID] FOREIGN KEY ([PR_VAL_ID]) REFERENCES [fact].[PR_VAL_PricingCommodity_Value_Posit] ([PR_VAL_ID]),
    CONSTRAINT [UX__PR_VAL_PricingCommodity_Value_Annex_RowGuid] UNIQUE NONCLUSTERED ([PR_VAL_RowGuid] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX__PR_VAL_PricingCommodity_Value_Annex]
    ON [fact].[PR_VAL_PricingCommodity_Value_Annex]([PR_VAL_PositedAt] DESC)
    INCLUDE([PR_VAL_ID], [PR_VAL_PositedBy], [PR_VAL_PositReliability], [PR_VAL_PositReliable]) WITH (FILLFACTOR = 95);


GO

CREATE TRIGGER [fact].[PR_VAL_PricingCommodity_Value_Annex_Delete]
ON [fact].[PR_VAL_PricingCommodity_Value_Annex]
INSTEAD OF DELETE
AS
BEGIN

	IF(@@ROWCOUNT > 0)
	BEGIN
		RAISERROR('Cannot delete from [fact].[PR_VAL_PricingCommodity_Value_Annex].', 16, 1);
		ROLLBACK;
	END;

END;