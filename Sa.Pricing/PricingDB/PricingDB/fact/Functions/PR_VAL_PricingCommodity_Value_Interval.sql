﻿CREATE FUNCTION [fact].[PR_VAL_PricingCommodity_Value_Interval]
(
	@ChangedAfter	DATETIMEOFFSET(7)	=   '01-01-01',
	@ChangedBefore	DATETIMEOFFSET(7)	= '9999-12-31',
	@PositedBefore	DATETIMEOFFSET(7)	= '9999-12-31'
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
SELECT
	[p].[PR_VAL_ID],
	[p].[PR_VAL_PR_ID],
	[p].[PR_VAL_Open],
	[p].[PR_VAL_Close],
	[p].[PR_VAL_Low],
	[p].[PR_VAL_High],
	[p].[PR_VAL_Volume],
	[p].[PR_VAL_ChangedAt],
	[a].[PR_VAL_PositedBy],
	[a].[PR_VAL_PositedAt],
	[a].[PR_VAL_PositReliability],
	[a].[PR_VAL_PositReliable]
FROM
	[fact].[PR_VAL_PricingCommodity_Value_Posit_IN](@ChangedAfter, @ChangedBefore)	[p]
INNER JOIN
	[fact].[PR_VAL_PricingCommodity_Value_Annex_RW](@PositedBefore)	[a]
		ON	[a].[PR_VAL_ID]			= [p].[PR_VAL_ID]
		AND	[a].[PR_VAL_PositedAt]	= (
			SELECT TOP 1
				[s].[PR_VAL_PositedAt]
			FROM
				[fact].[PR_VAL_PricingCommodity_Value_RW](@ChangedBefore, @PositedBefore)		[s]
			WHERE
				[s].[PR_VAL_PR_ID]	= [p].[PR_VAL_PR_ID]
			ORDER BY
				[s].[PR_VAL_ChangedAt]	DESC,
				[s].[PR_VAL_PositedAt]	DESC
			);