﻿CREATE TABLE [dim].[DS_DataSource_Bridge] (
    [DS_MethodologyId] INT                NOT NULL,
    [DS_DataSourceId]  INT                NOT NULL,
    [DS_DescendantId]  INT                NOT NULL,
    [DS_PositedBy]     INT                CONSTRAINT [DF__DS_DataSource_Bridge_DS_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [DS_PositedAt]     DATETIMEOFFSET (7) CONSTRAINT [DF__DS_DataSource_Bridge_DS_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [DS_RowGuid]       UNIQUEIDENTIFIER   CONSTRAINT [DF__DS_DataSource_Bridge_DS_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__DS_DataSource_Bridge] PRIMARY KEY CLUSTERED ([DS_MethodologyId] ASC, [DS_DataSourceId] ASC, [DS_DescendantId] ASC),
    CONSTRAINT [FK__DS_DataSource_Bridge_DS_DataSource_Parent_Ancestor] FOREIGN KEY ([DS_MethodologyId], [DS_DataSourceId]) REFERENCES [dim].[DS_DataSource_Parent] ([DS_MethodologyId], [DS_DataSourceId]),
    CONSTRAINT [FK__DS_DataSource_Bridge_DS_DataSource_Parent_Descendant] FOREIGN KEY ([DS_MethodologyId], [DS_DataSourceId]) REFERENCES [dim].[DS_DataSource_Parent] ([DS_MethodologyId], [DS_DataSourceId]),
    CONSTRAINT [FK__DS_DataSource_Bridge_DS_DataSourceId] FOREIGN KEY ([DS_DataSourceId]) REFERENCES [dim].[DS_DataSource_LookUp] ([DS_DataSourceId]),
    CONSTRAINT [FK__DS_DataSource_Bridge_DS_DescendantId] FOREIGN KEY ([DS_DescendantId]) REFERENCES [dim].[DS_DataSource_LookUp] ([DS_DataSourceId]),
    CONSTRAINT [FK__DS_DataSource_Bridge_DS_PositedBy] FOREIGN KEY ([DS_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UX__DS_DataSource_Bridge_DS_RowGuid] UNIQUE NONCLUSTERED ([DS_RowGuid] ASC)
);

