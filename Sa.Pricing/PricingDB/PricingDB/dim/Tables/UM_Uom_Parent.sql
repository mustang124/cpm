﻿CREATE TABLE [dim].[UM_Uom_Parent] (
    [UM_MethodologyId] INT                 NOT NULL,
    [UM_UomId]         INT                 NOT NULL,
    [UM_ParentId]      INT                 NOT NULL,
    [UM_Operator]      CHAR (1)            CONSTRAINT [DF__UM_Uom_Parent_UM_Operator] DEFAULT ('+') NOT NULL,
    [UM_SortKey]       INT                 NOT NULL,
    [UM_Hierarchy]     [sys].[hierarchyid] NOT NULL,
    [UM_PositedBy]     INT                 CONSTRAINT [DF__UM_Uom_Parent_UM_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [UM_PositedAt]     DATETIMEOFFSET (7)  CONSTRAINT [DF__UM_Uom_Parent_UM_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [UM_RowGuid]       UNIQUEIDENTIFIER    CONSTRAINT [DF__UM_Uom_Parent_UM_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__UM_Uom_Parent] PRIMARY KEY CLUSTERED ([UM_MethodologyId] DESC, [UM_UomId] ASC),
    CONSTRAINT [FK__UM_Uom_Parent_UM_Operator] FOREIGN KEY ([UM_Operator]) REFERENCES [dim].[OP_Operator_LookUp] ([OP_OperatorId]),
    CONSTRAINT [FK__UM_Uom_Parent_UM_Parent] FOREIGN KEY ([UM_ParentId]) REFERENCES [dim].[UM_Uom_LookUp] ([UM_UomId]),
    CONSTRAINT [FK__UM_Uom_Parent_UM_ParentParent] FOREIGN KEY ([UM_MethodologyId], [UM_UomId]) REFERENCES [dim].[UM_Uom_Parent] ([UM_MethodologyId], [UM_UomId]),
    CONSTRAINT [FK__UM_Uom_Parent_UM_PositedBy] FOREIGN KEY ([UM_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [FK__UM_Uom_Parent_UM_UomId] FOREIGN KEY ([UM_UomId]) REFERENCES [dim].[UM_Uom_LookUp] ([UM_UomId]),
    CONSTRAINT [UX__UM_Uom_Parent_UM_RowGuid] UNIQUE NONCLUSTERED ([UM_RowGuid] ASC)
);

