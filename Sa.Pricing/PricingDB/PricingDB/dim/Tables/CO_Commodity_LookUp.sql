﻿CREATE TABLE [dim].[CO_Commodity_LookUp] (
    [CO_CommodityId]  INT                IDENTITY (1, 1) NOT NULL,
    [CO_DataSourceId] INT                NOT NULL,
    [CO_Tag]          VARCHAR (12)       NOT NULL,
    [CO_Name]         NVARCHAR (256)     NOT NULL,
    [CO_Detail]       AS                 ((([CO_Name]+N' (')+CONVERT([nvarchar](12),[CO_Tag]))+N')') PERSISTED NOT NULL,
    [CO_PositedBy]    INT                CONSTRAINT [DF__CO_Commodity_LookUp_CO_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [CO_PositedAt]    DATETIMEOFFSET (7) CONSTRAINT [DF__CO_Commodity_LookUp_CO_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [CO_RowGuid]      UNIQUEIDENTIFIER   CONSTRAINT [DF__CO_Commodity_LookUp_CO_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__CO_Commodity_LookUp] PRIMARY KEY NONCLUSTERED ([CO_CommodityId] ASC),
    CONSTRAINT [CL__CO_Commodity_LookUp_CO_Name] CHECK ([CO_Name]<>''),
    CONSTRAINT [CL__CO_Commodity_LookUp_CO_Tag] CHECK ([CO_Tag]<>''),
    CONSTRAINT [FK__CO_Commodity_LookUp_CO_DataSourceId] FOREIGN KEY ([CO_DataSourceId]) REFERENCES [dim].[DS_DataSource_LookUp] ([DS_DataSourceId]),
    CONSTRAINT [FK__CO_Commodity_LookUp_CO_PositedBy] FOREIGN KEY ([CO_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__CO_Commodity_LookUp_CO_Tag] UNIQUE CLUSTERED ([CO_Tag] ASC),
    CONSTRAINT [UX__CO_Commodity_LookUp_CO_Detail] UNIQUE NONCLUSTERED ([CO_Detail] ASC),
    CONSTRAINT [UX__CO_Commodity_LookUp_CO_RowGuid] UNIQUE NONCLUSTERED ([CO_RowGuid] ASC)
);

