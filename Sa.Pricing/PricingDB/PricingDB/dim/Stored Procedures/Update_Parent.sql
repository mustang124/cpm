﻿CREATE PROCEDURE [dim].[Update_Parent]
(
	@SchemaName				VARCHAR(48)	= 'dim',
	@TableName				VARCHAR(48),

	@Prefix					VARCHAR(2),
	@NamePrime				VARCHAR(48),

	@NameMethodology		VARCHAR(48)	= 'MethodologyId',
	@NameParent				VARCHAR(48)	= 'ParentId',
	@NameSortKey			VARCHAR(48)	= 'SortKey',
	@NameHierarchy			VARCHAR(48)	= 'Hierarchy'
)
AS
BEGIN

SET NOCOUNT ON;

	SET @NamePrime			= @Prefix + '_' + @NamePrime;
	SET @NameMethodology	= @Prefix + '_' + @NameMethodology;
	SET @NameParent			= @Prefix + '_' + @NameParent;
	SET @NameSortKey		= @Prefix + '_' + @NameSortKey;
	SET @NameHierarchy		= @Prefix + '_' + @NameHierarchy;

	DECLARE @SQL	VARCHAR(MAX) =
	'WITH cte([MethodologyId], [PrimeId], [ParentId], [Hierarchy]) AS
	(
		SELECT
			a.[' + @NameMethodology + '],
			a.[' + @NamePrime + '],
			a.[' + @NameParent + '],
			SYS.HIERARCHYID::Parse(''/'' + CAST(ROW_NUMBER() OVER(PARTITION BY a.[' + @NameMethodology + '] ORDER BY a.[' + @NameSortKey + ']) AS VARCHAR) + ''/'')
		FROM [' + @SchemaName + '].[' + @TableName + '] a
		WHERE a.[' + @NamePrime + '] = a.[' + @NameParent + ']
		UNION ALL
		SELECT
			a.[' + @NameMethodology + '],
			a.[' + @NamePrime + '],
			a.[' + @NameParent + '],
			SYS.HIERARCHYID::Parse(c.Hierarchy.ToString() + CAST(ROW_NUMBER() OVER(PARTITION BY a.[' + @NameMethodology + '] ORDER BY a.[' + @NameSortKey + ']) AS VARCHAR) + ''/'')
		FROM [' + @SchemaName + '].[' + @TableName + '] a
		INNER JOIN cte c
			ON	c.[MethodologyId] = a.[' + @NameMethodology + ']
			AND	c.[PrimeId] = a.[' + @NameParent + ']
		WHERE a.[' + @NamePrime + '] <> a.[' + @NameParent + ']
	)
	UPDATE a
	SET a.[' + @NameHierarchy + '] = c.[Hierarchy]
	FROM [' + @SchemaName + '].[' + @TableName + '] a
	INNER JOIN cte c
		ON	c.[MethodologyId] = a.[' + @NameMethodology + ']
		AND	c.[PrimeId] = a.[' + @NamePrime + '];';

	EXECUTE(@SQL);

END;

/*
EXECUTE dim.Update_Parents 'dim', 'LO_Location_Parent', 'LO', 'MethodologyId', 'StreamId', 'ParentId', 'SortKey', 'Hierarchy';
*/