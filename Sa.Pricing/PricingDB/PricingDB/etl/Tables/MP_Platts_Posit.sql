﻿CREATE TABLE [etl].[MP_Platts_Posit] (
    [MP_MapId]              INT                IDENTITY (1, 1) NOT NULL,
    [MP_ItemSymbol]         CHAR (7)           NOT NULL,
    [MP_FrequencyId]        INT                NOT NULL,
    [MP_UomId]              INT                NOT NULL,
    [MP_UomMultiplier]      FLOAT (53)         NOT NULL,
    [MP_CurrencyId]         INT                NOT NULL,
    [MP_CurrencyMultiplier] FLOAT (53)         NOT NULL,
    [MP_PositedBy]          INT                CONSTRAINT [DF__MP_Platts_Posit_MP_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [MP_PositedAt]          DATETIMEOFFSET (7) CONSTRAINT [DF__MP_Platts_Posit_MP_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [MP_RowGuid]            UNIQUEIDENTIFIER   CONSTRAINT [DF__MP_Platts_Posit_MP_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    CONSTRAINT [PK__MP_Platts_Posit] PRIMARY KEY NONCLUSTERED ([MP_MapId] ASC),
    CONSTRAINT [CL__MP_Platts_Posit_MP_ItemSymbol] CHECK ([MP_ItemSymbol]<>''),
    CONSTRAINT [FK__MP_Platts_Posit_MP_CurrencyId] FOREIGN KEY ([MP_CurrencyId]) REFERENCES [dim].[CU_Currency_LookUp] ([CU_CurrencyId]),
    CONSTRAINT [FK__MP_Platts_Posit_MP_FrequencyId] FOREIGN KEY ([MP_FrequencyId]) REFERENCES [dim].[FQ_Frequency_LookUp] ([FQ_FrequencyId]),
    CONSTRAINT [FK__MP_Platts_Posit_MP_PositedBy] FOREIGN KEY ([MP_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [FK__MP_Platts_Posit_MP_UomId] FOREIGN KEY ([MP_UomId]) REFERENCES [dim].[UM_Uom_LookUp] ([UM_UomId]),
    CONSTRAINT [UK__MP_Platts_Posit] UNIQUE CLUSTERED ([MP_ItemSymbol] ASC),
    CONSTRAINT [UX__MP_Platts_Posit_MP_RowGuid] UNIQUE NONCLUSTERED ([MP_RowGuid] ASC)
);

