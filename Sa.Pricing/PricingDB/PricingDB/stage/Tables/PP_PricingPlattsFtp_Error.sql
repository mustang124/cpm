﻿CREATE TABLE [stage].[PP_PricingPlattsFtp_Error] (
    [PP_ID]                   INT                IDENTITY (1, 1) NOT NULL,
    [PP_FilePath]             VARCHAR (260)      NULL,
    [PP_FileCopyright]        VARCHAR (48)       NULL,
    [PP_FileIdentifier]       VARCHAR (16)       NULL,
    [PP_FileCreated_Stamp]    CHAR (12)          NULL,
    [PP_FileCreated]          DATETIMEOFFSET (0) NULL,
    [PP_FileItemCount]        INT                NULL,
    [PP_FileLabel]            VARCHAR (9)        NULL,
    [PP_FileCategoryCode]     VARCHAR (3)        NULL,
    [PP_FileAsOf_Stamp]       CHAR (8)           NULL,
    [PP_FileAsOf]             DATETIMEOFFSET (0) NULL,
    [PP_ItemTrans]            VARCHAR (2)        NULL,
    [PP_ItemSymbol]           CHAR (7)           NULL,
    [PP_ItemBateCode]         CHAR (1)           NULL,
    [PP_ItemAssessment_Stamp] CHAR (12)          NULL,
    [PP_ItemAssessment]       DATETIMEOFFSET (0) NULL,
    [PP_ItemPrice]            DECIMAL (14, 4)    NULL,
    [PP_ItemXAction_Stamp]    CHAR (12)          NULL,
    [PP_ItemXAction]          DATETIMEOFFSET (0) NULL,
    [PP_ChangedAt]            DATETIMEOFFSET (7) NULL,
    [PP_PositedBy]            INT                CONSTRAINT [DF__PP_PricingPlattsFtp_Error_PP_PositedBy] DEFAULT ([posit].[Get_PositorId]()) NOT NULL,
    [PP_PositedAt]            DATETIMEOFFSET (7) CONSTRAINT [DF__PP_PricingPlattsFtp_Error_PP_PositedAt] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [PP_PositReliability]     TINYINT            CONSTRAINT [DF__PP_PricingPlattsFtp_Error_PP_Reliability] DEFAULT ((50)) NOT NULL,
    [PP_PositReliable]        AS                 (CONVERT([bit],case when [PP_PositReliability]>(0) then (1) else (0) end)) PERSISTED NOT NULL,
    [PP_RowGuid]              UNIQUEIDENTIFIER   CONSTRAINT [DF__PP_PricingPlattsFtp_Error_PP_RowGuid] DEFAULT (newsequentialid()) ROWGUIDCOL NOT NULL,
    [PP_ErrorCode]            INT                NULL,
    [PP_ErrorColumn]          INT                NULL,
    CONSTRAINT [PK__PP_PricingPlattsFtp_Error] PRIMARY KEY NONCLUSTERED ([PP_ID] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [FK__PP_PricingPlattsFtp_Error_PP_PositedBy] FOREIGN KEY ([PP_PositedBy]) REFERENCES [posit].[PO_Positor] ([PO_PositorId]),
    CONSTRAINT [UK__PP_PricingPlattsFtp_Error] UNIQUE CLUSTERED ([PP_ItemSymbol] ASC, [PP_ItemBateCode] ASC, [PP_ItemAssessment] ASC, [PP_ChangedAt] ASC, [PP_PositedAt] ASC) WITH (FILLFACTOR = 95),
    CONSTRAINT [UX__PP_PricingPlattsFtp_Error_PP_RowGuid] UNIQUE NONCLUSTERED ([PP_RowGuid] ASC)
);

