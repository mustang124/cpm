﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;


namespace PetroChemLibrary
{
    //Complex-Level Input Tabl
    public class PetroChem_CompanyInfo
    {
        public string CompanyName { get; set; }
        public int CompanyID { get; set; }
        public string Location { get; set; }
        public string CoordName { get; set; }
        public string CoordTitle { get; set; }
        public string CoordPhone { get; set; }
        public string CoordEmail { get; set; }
    }



    public class PetroChem_Products
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? MethanolPipe { get; set; }
        public int? MethanolTruck { get; set; }
        public int? AmmoniaPipe { get; set; }
        public int? BulkUrea { get; set; }
        public int? PackageUrea { get; set; }


    }


    public class PetroChem_Costs
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? PNGFeedCost { get; set; }
        public int? PNGFuelCost { get; set; }
        public int? POtherFuelsCost { get; set; }
        public int? PElectCost { get; set; }
        public int? SElectCost { get; set; }
        public int? PSteamCost { get; set; }
        public int? SSteamCost { get; set; }
        public int? ChemCatCost { get; set; }
        public int? SWBNonMaintCost { get; set; }
        public int? SWBMaintCost { get; set; }
        public int? ContMaintCost { get; set; }
        public int? MaintMatCost { get; set; }
        public int? MaintEquipRental { get; set; }
        public int? NonMaintContCost { get; set; }
        public int? NonTAOpEx { get; set; }
        public int? GAPersCost { get; set; }
        public int? OtherGACost { get; set; }
        public int? GTNonTACost { get; set; }
        public string AUMNotes { get; set; }
    }


    public class PetroChem_ThermalEnergy
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? NatGas { get; set; }
        public int? POtherFuels { get; set; }
        public int? PSteam { get; set; }
        public int? SSteam { get; set; }
    }

    public class PetroChem_Electricity
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? PElect { get; set; }
        public int? ProdElect { get; set; }
        public int? SoldElect { get; set; }
        public int? ProdElectEff { get; set; }
    }


    public class PetroChem_Personnel
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? NonTAEmp { get; set; }
        public int? NonTACont { get; set; }
        public string NonTAGA { get; set; }
    }


    //Process-Level Tab
    public class PetroChem_Units
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public string ProcessID { get; set; }
        public string ProcessType { get; set; }
        public string ProcessSubType { get; set; }

    }

    public class PetroChem_ProcessData
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public int? FeedQty { get; set; }
        public int? H2 { get; set; }
        public int? CH4 { get; set; }
        public int? Ethane { get; set; }
        public int? Propane { get; set; }
        public int? Butane { get; set; }
        public int? CO { get; set; }
        public int? CO2 { get; set; }
        public int? H2S { get; set; }
        public int? N2 { get; set; }
        public int? Argon { get; set; }
        public int? Total { get; set; }

    }

    public class PetroChem_Capacity
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public int? Capacity { get; set; }
        public int? CapacityUtil { get; set; }
        public int? SteamCap { get; set; }
        public int? SteamCapUtil { get; set; }
    }

    public class PetroChem_FeedQuantity
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public int? FeedQty { get; set; }
       
    }

    public class PetroChem_SteamReforming
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? UnitID { get; set; }
        public int? Steam2C { get; set; }
        public int? ProcessGas { get; set; }
        public int? PercH2 { get; set; }

    }

    public class PetroChem_SynGasComposition
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? UnitID { get; set; }
        public int? H2 { get; set; }
        public int? CH4 { get; set; }
        public int? Ethane { get; set; }
        public int? Propane { get; set; }
        public int? Butane { get; set; }
        public int? CO { get; set; }
        public int? CO2 { get; set; }
        public int? H2S { get; set; }
        public int? N2 { get; set; }
        public int? Argon { get; set; }
        public int? Total { get; set; }

    }

    public class PetroChem_PSAH2Purification
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? UnitID { get; set; }
        public int? H2Recovered { get; set; }
        public int? H2Content { get; set; }
        public int? H2Lost { get; set; }

    }
    public class PetroChem_UreaFeed
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? UnitID { get; set; }
        public int? NH3 { get; set; }
        public int? CO2 { get; set; }
        public string ProductType { get; set; }


    }
    public class PetroChem_MajorCompressors
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public int? ProcessAir { get; set; }
        public int? SynGas { get; set; }
        public int? NH3 { get; set; }
        public int? CO2 { get; set; }

    }


    public class PetroChem_UreaConveyors
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public string Type { get; set; }
        public int? SynGas { get; set; }
        public int? NoLines { get; set; }
        public int? BedLength { get; set; }
        public int? BedWidth { get; set; }

    }
    public class PetroChem_TAMaintenance
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public string RecentTADate { get; set; }
        public string PrevTADate { get; set; }
        public int? TotalHrsDown { get; set; }
        public int? TotalCost { get; set; }
        public int? CompanyHrs { get; set; }
        public int? ContractorHrs { get; set; }

    }

    public class PetroChem_NonTADowntime
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public int? TotalRegHrsDown { get; set; }
        public int? TotalMechHrsDown { get; set; }

    }

    public class PetroChem_RawMaterials
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public string MaterialName { get; set; }
        public int? KNM3 { get; set; }
        public int? Density { get; set; }
        public int? Tonnes { get; set; }

    }

    public class PetroChem_ProductYield
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public string ProductName { get; set; }
        public int? Tonnes { get; set; }

    }

    public class PetroChem_HydroCarbonLoss
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? GTHydrocarbon { get; set; }
        public int? FlareLosses { get; set; }

    }




    public class PetroChem
    {

        public PetroChem(string cs)
        {
            CurrentLogFile = cs;
        }

        string CurrentLogFile = null;
        public string Error = null;


        public int CompanyID = 0;
        public int SubmissionID = 0;


        //Open Excel Application
        Excel.Application excel;
        Excel.Workbook excelWorkbook;

        //Initialize NumberShips Variable
     

        //Initialize Connection and Command
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();


        string CONNECTSTR = Properties.Settings.Default.CONN;


        public string ImportPetroChem(string PetroChemPath)
        {
            string rtn = "";
            cn.ConnectionString = CONNECTSTR;
            cn.Open();

            //Open Workbook
            excel = new Excel.Application();

            excelWorkbook = excel.Workbooks.Open(PetroChemPath,
                0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
                true, false, 0, true, false, false);




            //Initialize the Objects
            PetroChem_CompanyInfo Company = new PetroChem_CompanyInfo();
            PetroChem_ThermalEnergy TE = new PetroChem_ThermalEnergy();
            PetroChem_Products Products = new PetroChem_Products();
            PetroChem_Costs Costs = new PetroChem_Costs();
            PetroChem_Electricity Electricity = new PetroChem_Electricity();
            PetroChem_Personnel Personnel = new PetroChem_Personnel();
            List<PetroChem_ProcessData> PD = new List<PetroChem_ProcessData>();
            List<PetroChem_FeedQuantity> FeedQty = new List<PetroChem_FeedQuantity>();
            List<PetroChem_Capacity> Capacity = new List<PetroChem_Capacity>();
            List<PetroChem_SteamReforming> SF = new List<PetroChem_SteamReforming>();
            List<PetroChem_SynGasComposition> SGC = new List<PetroChem_SynGasComposition>();

            List<PetroChem_PSAH2Purification> PSAH2 = new List<PetroChem_PSAH2Purification>();
            List<PetroChem_UreaFeed> UreaFeed = new List<PetroChem_UreaFeed>();
            List<PetroChem_MajorCompressors> MC = new List<PetroChem_MajorCompressors>();
            List<PetroChem_UreaConveyors> UC = new List<PetroChem_UreaConveyors>();
            List<PetroChem_TAMaintenance> TAM = new List<PetroChem_TAMaintenance>();
            List<PetroChem_NonTADowntime> NonTAD = new List<PetroChem_NonTADowntime>();
            List<PetroChem_Units> Units = new List<PetroChem_Units>();

            List<PetroChem_RawMaterials> RM = new List<PetroChem_RawMaterials>();
            List<PetroChem_ProductYield> PY = new List<PetroChem_ProductYield>();
            List<PetroChem_HydroCarbonLoss> HL = new List<PetroChem_HydroCarbonLoss>();

            Helper.WriteLog("--------------------------------------------------------------------------------------------------------------------------------------------", @"PetroChem.log");
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": PetroChem Import Started", CurrentLogFile);


            Company = GetCompanyInfo();
            CompanyID = WriteCompanyInfo(Company);

            //Get Next SubmissionID
            SubmissionID = Helper.GetNextSubmissionID(CONNECTSTR,CompanyID);
            Helper.WriteSubmission(CONNECTSTR, SubmissionID, CompanyID);
            //Read Data from Excel to Objects
            Units = GetUnits();
            WriteUnits(Units);

            Products = GetProducts();
            WriteProducts(Products);

            Costs = GetCosts();
            WriteCosts(Costs);

            TE = GetThermalEnergy();
            WriteThermalEnergy(TE);

            Electricity = GetElectricity();
            WriteElectricity(Electricity);

            Personnel = GetPersonnel();
            WritePersonnel(Personnel);

            FeedQty = GetFeedQuantity();
            WriteFeedQuantity(FeedQty);

            PD = GetProcessData();
            WriteProcessData(PD);

            Capacity = GetCapacity();
            WriteCapacity(Capacity);

            SF = GetSteamReforming();
            WriteSteamReforming(SF);

            SGC = GetSynGasComposition();
            WriteSynGasComposition(SGC);

            PSAH2 = GetPSAH2Purification();
            WritePSAH2Purification(PSAH2);

            UreaFeed = GetUreaFeed();
            WriteUreaFeed(UreaFeed);

            MC = GetMajorCompressors();
            WriteMajorCompressors(MC);

            UC = GetUreaConveyors();
            WriteUreaConveyors(UC);

            TAM = GetTAMaintenance();
            WriteTAMaintenance(TAM);

            NonTAD = GetNonTADowntime();
            WriteNonTADowntime(NonTAD);

            RM = GetRawMaterials();
            WriteRawMaterials(RM);

            PY = GetProductYield();
            WriteProductYield(PY);

            HL = GetHydroCarbonLoss();
            WriteHydroCarbonLoss(HL);

            rtn = "Complete";

            //Close Connection
            cn.Close();

            //Close and Destroy Excel Object
            excelWorkbook.Close(SaveChanges: false);
            excelWorkbook = null;

            excel = null;
            return rtn;
        }

        private PetroChem_CompanyInfo GetCompanyInfo()
        {


            PetroChem_CompanyInfo comp = new PetroChem_CompanyInfo();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Complex-Level Input"];
            try
            {

                comp.CompanyName = xlWorkSheet.get_Range("Company").Value2;
                comp.Location = xlWorkSheet.get_Range("Location").Value2;
                comp.CoordName = xlWorkSheet.get_Range("CoordName").Value2;
                comp.CoordTitle = xlWorkSheet.get_Range("CoordTitle").Value2;
                comp.CoordPhone = xlWorkSheet.get_Range("CoordPhone").Value2;
                comp.CoordEmail = xlWorkSheet.get_Range("CoordEmail").Value2;



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetCompanyInfo";
            }

            return comp;

        }


        private int WriteCompanyInfo(PetroChem_CompanyInfo contact)
        {
            int rtn = 0;
            cmd.CommandText = "PetroChem_InsertCompanyInformation";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@Company", Write(contact.CompanyName)));
            cmd.Parameters.Add(new SqlParameter("@Location", Write(contact.Location)));
            cmd.Parameters.Add(new SqlParameter("@CoordName", Write(contact.CoordName)));
            cmd.Parameters.Add(new SqlParameter("@CoordTitle", Write(contact.CoordTitle)));
            cmd.Parameters.Add(new SqlParameter("@CoordPhone", Write(contact.CoordPhone)));
            cmd.Parameters.Add(new SqlParameter("@CoordEmail", Write(contact.CoordEmail)));


            try
            {
                rtn = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                 Error = "SubmissionID:" + SubmissionID + ": WriteCompanyInfo" + ": ex.Message";

            }

            return rtn;

        }


        private PetroChem_Products GetProducts()
        {


            PetroChem_Products comp = new PetroChem_Products();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Complex-Level Input"];
            try
            {

                comp.MethanolPipe = xlWorkSheet.get_Range("MethanolPipe").Value2;
                comp.MethanolTruck = xlWorkSheet.get_Range("MethanolTruck").Value2;
                comp.AmmoniaPipe = xlWorkSheet.get_Range("AmmoniaPipe").Value2;
                comp.BulkUrea = xlWorkSheet.get_Range("BulkUrea").Value2;
                comp.PackageUrea = xlWorkSheet.get_Range("PackageUrea").Value2;




            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetProducts";
            }

            return comp;

        }


        private void WriteProducts(PetroChem_Products products)
        {
            
            cmd.CommandText = "PetroChem_InsertProducts";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@MethanolPipe", Write(products.MethanolPipe)));
            cmd.Parameters.Add(new SqlParameter("@MethanolTruck", Write(products.MethanolTruck)));
            cmd.Parameters.Add(new SqlParameter("@AmmoniaPipe", Write(products.AmmoniaPipe)));
            cmd.Parameters.Add(new SqlParameter("@BulkUrea", Write(products.BulkUrea)));
            cmd.Parameters.Add(new SqlParameter("@PackageUrea", Write(products.PackageUrea)));



            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteProducts";

            }

            

        }


        private PetroChem_Costs GetCosts()
        {


            PetroChem_Costs comp = new PetroChem_Costs();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Complex-Level Input"];
            try
            {

                comp.PNGFeedCost = xlWorkSheet.get_Range("PNGFeedCost").Value2;
                comp.PNGFuelCost = xlWorkSheet.get_Range("PNGFuelCost").Value2;
                comp.POtherFuelsCost = xlWorkSheet.get_Range("POtherFuelsCost").Value2;
                comp.PElectCost = xlWorkSheet.get_Range("PElectCost").Value2;
                comp.SElectCost = xlWorkSheet.get_Range("SElectCost").Value2;

                comp.PSteamCost = xlWorkSheet.get_Range("PSteamCost").Value2;
                comp.SSteamCost = xlWorkSheet.get_Range("SSteamCost").Value2;
                comp.ChemCatCost = xlWorkSheet.get_Range("ChemCatCost").Value2;
                comp.SWBNonMaintCost = xlWorkSheet.get_Range("SWBNonMaintCost").Value2;
                comp.SWBMaintCost = xlWorkSheet.get_Range("SWBMaintCost").Value2;

                comp.ContMaintCost = xlWorkSheet.get_Range("ContMaintCost").Value2;
                comp.MaintMatCost = xlWorkSheet.get_Range("MaintMatCost").Value2;
                comp.MaintEquipRental = xlWorkSheet.get_Range("MaintEquipRental").Value2;
                comp.NonMaintContCost = xlWorkSheet.get_Range("NonMaintContCost").Value2;
                comp.NonTAOpEx = xlWorkSheet.get_Range("NonTAOpEx").Value2;

                comp.GAPersCost = xlWorkSheet.get_Range("GAPersCost").Value2;
                comp.OtherGACost = xlWorkSheet.get_Range("OtherGACost").Value2;
                comp.GTNonTACost = xlWorkSheet.get_Range("GTNonTACost").Value2;
                comp.AUMNotes = xlWorkSheet.get_Range("AUMNotes").Value2;
                

            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetCosts";
            }

            return comp;

        }


        private void WriteCosts(PetroChem_Costs costs)
        {
            
            cmd.CommandText = "PetroChem_InsertCosts";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@PNGFeedCost", Write(costs.PNGFeedCost)));
            cmd.Parameters.Add(new SqlParameter("@PNGFuelCost", Write(costs.PNGFuelCost)));
            cmd.Parameters.Add(new SqlParameter("@POtherFuelsCost", Write(costs.POtherFuelsCost)));
            cmd.Parameters.Add(new SqlParameter("@PElectCost", Write(costs.PElectCost)));
            cmd.Parameters.Add(new SqlParameter("@SElectCost", Write(costs.SElectCost)));
            cmd.Parameters.Add(new SqlParameter("@PSteamCost", Write(costs.PSteamCost)));
            cmd.Parameters.Add(new SqlParameter("@SSteamCost", Write(costs.SSteamCost)));
            cmd.Parameters.Add(new SqlParameter("@ChemCatCost", Write(costs.ChemCatCost)));
            cmd.Parameters.Add(new SqlParameter("@SWBNonMaintCost", Write(costs.SWBNonMaintCost)));
            cmd.Parameters.Add(new SqlParameter("@SWBMaintCost", Write(costs.SWBMaintCost)));
            cmd.Parameters.Add(new SqlParameter("@ContMaintCost", Write(costs.ContMaintCost)));
            cmd.Parameters.Add(new SqlParameter("@MaintMatCost", Write(costs.MaintMatCost)));
            cmd.Parameters.Add(new SqlParameter("@MaintEquipRental", Write(costs.MaintEquipRental)));
            cmd.Parameters.Add(new SqlParameter("@NonMaintContCost", Write(costs.NonMaintContCost)));
            cmd.Parameters.Add(new SqlParameter("@NonTAOpEx", Write(costs.NonTAOpEx)));
            cmd.Parameters.Add(new SqlParameter("@GAPersCost", Write(costs.GAPersCost)));
            cmd.Parameters.Add(new SqlParameter("@OtherGACost", Write(costs.OtherGACost)));
            cmd.Parameters.Add(new SqlParameter("@GTNonTACost", Write(costs.GTNonTACost)));
            cmd.Parameters.Add(new SqlParameter("@AUMNotes", Write(costs.AUMNotes)));


            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteCosts";

            }

            

        }


        private PetroChem_ThermalEnergy GetThermalEnergy()
        {


            PetroChem_ThermalEnergy TE = new PetroChem_ThermalEnergy();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Complex-Level Input"];
            try
            {

                TE.NatGas = xlWorkSheet.get_Range("NatGas").Value2;
                TE.POtherFuels = xlWorkSheet.get_Range("POtherFuels").Value2;
                TE.PSteam = xlWorkSheet.get_Range("PSteam").Value2;
                TE.SSteam = xlWorkSheet.get_Range("SSteam").Value2;
                
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetThermalEnergy";
            }

            return TE;

        }


        private void WriteThermalEnergy(PetroChem_ThermalEnergy te)
        {

            cmd.CommandText = "PetroChem_InsertThermalEnergy";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@NatGas", Write(te.NatGas)));
            cmd.Parameters.Add(new SqlParameter("@POtherFuels", Write(te.POtherFuels)));
            cmd.Parameters.Add(new SqlParameter("@PSteam", Write(te.PSteam)));
            cmd.Parameters.Add(new SqlParameter("@SSteam", Write(te.SSteam)));

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteThermalEnergy";

            }



        }




        private PetroChem_Electricity GetElectricity()
        {


            PetroChem_Electricity Elect = new PetroChem_Electricity();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Complex-Level Input"];
            try
            {

                Elect.PElect = xlWorkSheet.get_Range("PElect").Value2;
                Elect.ProdElect = xlWorkSheet.get_Range("ProdElect").Value2;
                Elect.SoldElect = xlWorkSheet.get_Range("SElect").Value2;
                Elect.ProdElectEff = xlWorkSheet.get_Range("ProdElectEff").Value2;

            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetElectricity";
            }

            return Elect;

        }


        private void WriteElectricity(PetroChem_Electricity e)
        {

            cmd.CommandText = "PetroChem_InsertElectricity";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@PElect", Write(e.PElect)));
            cmd.Parameters.Add(new SqlParameter("@ProdElect", Write(e.ProdElect)));
            cmd.Parameters.Add(new SqlParameter("@SoldElect", Write(e.SoldElect)));
            cmd.Parameters.Add(new SqlParameter("@ProdElectEff", Write(e.ProdElectEff)));

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteElectricity";

            }



        }


        private PetroChem_Personnel GetPersonnel()
        {


            PetroChem_Personnel pers = new PetroChem_Personnel();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Complex-Level Input"];
            try
            {

                pers.NonTAEmp = xlWorkSheet.get_Range("PElect").Value2;
                pers.NonTACont = xlWorkSheet.get_Range("ProdElect").Value2;
                pers.NonTAGA = xlWorkSheet.get_Range("SElect").Value2;
                

            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetPersonnel";
            }

            return pers;

        }


        private void WritePersonnel(PetroChem_Personnel p)
        {

            cmd.CommandText = "PetroChem_InsertPersonnel";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@NonTAEmp", Write(p.NonTAEmp)));
            cmd.Parameters.Add(new SqlParameter("@NonTACont", Write(p.NonTACont)));
            cmd.Parameters.Add(new SqlParameter("@NonTAGA", Write(p.NonTAGA)));
            

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WritePersonnel";

            }



        }




        public List<PetroChem_Capacity> GetCapacity()
        {
            int i = 1;
            List<PetroChem_Capacity> scfg = new List<PetroChem_Capacity>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("Capacity").Columns.Count;
                Excel.Range capacity = xlWorkSheet.get_Range("Capacity");

                for (i = 1; i <= NumUnits; i++)
                {

                    if (capacity.Cells[1, i].Value != null && capacity.Cells[1, i].Value.ToString().Length > 1)
                    {

                        PetroChem_Capacity config = new PetroChem_Capacity();
                        config.UnitID = i;
                        config.Capacity = Convert.ToInt32(capacity.Cells[1, i].Value);
                        config.CapacityUtil = Convert.ToInt32(capacity.Cells[2, i].Value);
                        if (i == 16)
                        {
                            config.SteamCap = Convert.ToInt32(xlWorkSheet.get_Range("SteamCap").Value);
                            config.SteamCapUtil = Convert.ToInt32(xlWorkSheet.get_Range("SteamCapUtil").Value);
                        }
                        scfg.Add(config);
                    }
                }
            }
                
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetCapacity(" + i + ")";
            }

            return scfg;


        }


        private void WriteCapacity(List<PetroChem_Capacity> cap)
        {

            cmd.CommandText = "PetroChem_InsertCapacity";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_Capacity c in cap)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.AddWithValue("@UnitID", Write(c.UnitID));
                cmd.Parameters.Add(new SqlParameter("@Capacity", Write(c.Capacity)));
                cmd.Parameters.Add(new SqlParameter("@CapacityUtil", Write(c.CapacityUtil)));
                cmd.Parameters.Add(new SqlParameter("@SteamCap", Write(c.SteamCap)));
                cmd.Parameters.Add(new SqlParameter("@SteamCapUtil", Write(c.SteamCapUtil)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteCapacity";

                }
            }
        }


        public List<PetroChem_FeedQuantity> GetFeedQuantity()
        {
            int i = 1;
            List<PetroChem_FeedQuantity> scfg = new List<PetroChem_FeedQuantity>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("FeedQty").Columns.Count;
                Excel.Range capacity = xlWorkSheet.get_Range("FeedQty");

                for (i = 1; i <= NumUnits; i++)
                {

                    if (capacity.Cells[1, i].Value != null && capacity.Cells[1, i].Value.ToString().Length > 1)
                    {

                        PetroChem_FeedQuantity config = new PetroChem_FeedQuantity();
                        config.UnitID = Convert.ToInt32(xlWorkSheet.Cells[9,i+1].Value);
                        config.FeedQty = Convert.ToInt32(capacity.Cells[1, i].Value);
                        
                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetFeedQuantity(" + i + ")";
            }

            return scfg;


        }





        private void WriteFeedQuantity(List<PetroChem_FeedQuantity> fq)
        {

            cmd.CommandText = "PetroChem_InsertFeedQuantity";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_FeedQuantity f in fq)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", f.UnitID));
                cmd.Parameters.Add(new SqlParameter("@FeedQty", Write(f.FeedQty)));
                
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteFeedQuantity";

                }
            }
        }



        public List<PetroChem_Units> GetUnits()
        {
            int i = 1;
            List<PetroChem_Units> scfg = new List<PetroChem_Units>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("Units").Columns.Count;

                for (i = 1; i <= NumUnits; i++)
                {
                    Excel.Range fc = xlWorkSheet.get_Range("Units");

                    if (fc.Cells[3, i].Value != null)
                    {
                        PetroChem_Units config = new PetroChem_Units();
                        config.UnitID = Convert.ToInt32(fc.Cells[1, i].Value);
                        config.SubmissionID = SubmissionID;
                        config.CompanyID = CompanyID;
                        config.UnitName = fc.Cells[3, i].Value;
                        config.ProcessID = fc.Cells[4, i].Value;
                        config.ProcessType = fc.Cells[5, i].Value;
                        config.ProcessSubType = fc.Cells[6, i].Value;
                        

                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetUnits(" + i + ")";
            }

            return scfg;


        }

        private void WriteUnits(List<PetroChem_Units> units)
        {

            cmd.CommandText = "PetroChem_InsertUnit";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_Units u in units)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", u.UnitID));
                cmd.Parameters.Add(new SqlParameter("@UnitName", u.UnitName));
                cmd.Parameters.Add(new SqlParameter("@ProcessID", Write(u.ProcessID)));
                cmd.Parameters.Add(new SqlParameter("@ProcessType", Write(u.ProcessType)));
                cmd.Parameters.Add(new SqlParameter("@ProcessSubType", Write(u.ProcessSubType)));
                


                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteUnits";

                }
            }
        }


        public List<PetroChem_ProcessData> GetProcessData()
        {
            int i = 1;
            List<PetroChem_ProcessData> scfg = new List<PetroChem_ProcessData>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("AvgFeedComp").Columns.Count;

                for (i = 1; i <= NumUnits; i++)
                {
                    Excel.Range fc = xlWorkSheet.get_Range("AvgFeedComp");

                    if (fc.Cells[1, i].Value != null)
                    {
                        PetroChem_ProcessData config = new PetroChem_ProcessData();
                        config.UnitID = i;
                        config.H2 = Convert.ToInt32(fc.Cells[1, i].Value);
                        config.CH4 = Convert.ToInt32(fc.Cells[2, i].Value);
                        config.Ethane = Convert.ToInt32(fc.Cells[3, i].Value);
                        config.Propane = Convert.ToInt32(fc.Cells[4, i].Value);
                        config.Butane = Convert.ToInt32(fc.Cells[5, i].Value);
                        config.CO = Convert.ToInt32(fc.Cells[6, i].Value);
                        config.CO2 = Convert.ToInt32(fc.Cells[7, i].Value);
                        config.H2S = Convert.ToInt32(fc.Cells[8, i].Value);
                        config.N2 = Convert.ToInt32(fc.Cells[9, i].Value);
                        config.Argon = Convert.ToInt32(fc.Cells[10, i].Value);
                        config.Total = Convert.ToInt32(fc.Cells[11, i].Value);

                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetProcessData(" + i + ")";
            }

            return scfg;


        }

        private void WriteProcessData(List<PetroChem_ProcessData> pd)
        {

            cmd.CommandText = "PetroChem_InsertProcessData";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_ProcessData p in pd)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", p.UnitID));
                cmd.Parameters.Add(new SqlParameter("@FeedQty", Write(p.FeedQty)));
                cmd.Parameters.Add(new SqlParameter("@H2", Write(p.H2)));
                cmd.Parameters.Add(new SqlParameter("@CH4", Write(p.CH4)));
                cmd.Parameters.Add(new SqlParameter("@Ethane", Write(p.Ethane)));
                cmd.Parameters.Add(new SqlParameter("@Propane", Write(p.Propane)));
                cmd.Parameters.Add(new SqlParameter("@Butane", Write(p.Butane)));
                cmd.Parameters.Add(new SqlParameter("@CO", Write(p.CO)));
                cmd.Parameters.Add(new SqlParameter("@CO2", Write(p.CO2)));
                cmd.Parameters.Add(new SqlParameter("@H2S", Write(p.H2S)));
                cmd.Parameters.Add(new SqlParameter("@N2", Write(p.N2)));
                cmd.Parameters.Add(new SqlParameter("@Argon", Write(p.Argon)));
                cmd.Parameters.Add(new SqlParameter("@Total", Write(p.Total)));
                

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteProcessData";

                }
            }
        }


        public List<PetroChem_SynGasComposition> GetSynGasComposition()
        {
            int i = 1;
            List<PetroChem_SynGasComposition> scfg = new List<PetroChem_SynGasComposition>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("SynGas").Columns.Count;

                for (i = 1; i <= NumUnits; i++)
                {
                    Excel.Range sg = xlWorkSheet.get_Range("SynGas");

                    if (sg.Cells[1, i].Value != null)
                    {
                        PetroChem_SynGasComposition config = new PetroChem_SynGasComposition();
                        config.UnitID = i;
                        config.H2 = Convert.ToInt32(sg.Cells[1, i].Value);
                        config.CH4 = Convert.ToInt32(sg.Cells[2, i].Value);
                        config.Ethane = Convert.ToInt32(sg.Cells[3, i].Value);
                        config.Propane = Convert.ToInt32(sg.Cells[4, i].Value);
                        config.Butane = Convert.ToInt32(sg.Cells[5, i].Value);
                        config.CO = Convert.ToInt32(sg.Cells[6, i].Value);
                        config.CO2 = Convert.ToInt32(sg.Cells[7, i].Value);
                        config.H2S = Convert.ToInt32(sg.Cells[8, i].Value);
                        config.N2 = Convert.ToInt32(sg.Cells[9, i].Value);
                        config.Argon = Convert.ToInt32(sg.Cells[10, i].Value);
                        config.Total = Convert.ToInt32(sg.Cells[11, i].Value);

                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetSynGasComposition(" + i + ")";
            }

            return scfg;


        }

        private void WriteSynGasComposition(List<PetroChem_SynGasComposition> sgc)
        {

            cmd.CommandText = "PetroChem_InsertSynGasComposition";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_SynGasComposition sg in sgc)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", sg.UnitID));
                cmd.Parameters.Add(new SqlParameter("@H2", Write(sg.H2)));
                cmd.Parameters.Add(new SqlParameter("@CH4", Write(sg.CH4)));
                cmd.Parameters.Add(new SqlParameter("@Ethane", Write(sg.Ethane)));
                cmd.Parameters.Add(new SqlParameter("@Propane", Write(sg.Propane)));
                cmd.Parameters.Add(new SqlParameter("@Butane", Write(sg.Butane)));
                cmd.Parameters.Add(new SqlParameter("@CO", Write(sg.CO)));
                cmd.Parameters.Add(new SqlParameter("@CO2", Write(sg.CO2)));
                cmd.Parameters.Add(new SqlParameter("@H2S", Write(sg.H2S)));
                cmd.Parameters.Add(new SqlParameter("@N2", Write(sg.N2)));
                cmd.Parameters.Add(new SqlParameter("@Argon", Write(sg.Argon)));
                cmd.Parameters.Add(new SqlParameter("@Total", Write(sg.Total)));


                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteSynGasComposition";

                }
            }
        }



        public List<PetroChem_SteamReforming> GetSteamReforming()
        {
            int i = 1;
            List<PetroChem_SteamReforming> scfg = new List<PetroChem_SteamReforming>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("SteamReforming").Columns.Count;
                Excel.Range capacity = xlWorkSheet.get_Range("SteamReforming");

                for (i = 1; i <= NumUnits; i++)
                {

                    if (capacity.Cells[1, i].Value != null)
                    {

                        PetroChem_SteamReforming config = new PetroChem_SteamReforming();
                        config.UnitID = i;
                        config.Steam2C = Convert.ToInt32(capacity.Cells[1, i].Value);
                        config.ProcessGas = Convert.ToInt32(capacity.Cells[2, i].Value);
                        config.PercH2 = Convert.ToInt32(capacity.Cells[3, i].Value);
                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetSteamReforming(" + i + ")";
            }

            return scfg;


        }





        private void WriteSteamReforming(List<PetroChem_SteamReforming> sr)
        {

            cmd.CommandText = "PetroChem_InsertSteamReforming";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_SteamReforming s in sr)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", s.UnitID));
                cmd.Parameters.Add(new SqlParameter("@Steam2C", Write(s.Steam2C)));
                cmd.Parameters.Add(new SqlParameter("@ProcessGas", Write(s.ProcessGas)));
                cmd.Parameters.Add(new SqlParameter("@PercH2", Write(s.PercH2)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteSteamReforming";

                }
            }
        }


        public List<PetroChem_PSAH2Purification> GetPSAH2Purification()
        {
            int i = 1;
            List<PetroChem_PSAH2Purification> scfg = new List<PetroChem_PSAH2Purification>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                
                Excel.Range PSA = xlWorkSheet.get_Range("PSAH2Pur");


                    

                        PetroChem_PSAH2Purification config = new PetroChem_PSAH2Purification();
                        config.UnitID = 6;
                        config.H2Recovered = Convert.ToInt32(PSA.Cells[1, 1].Value);
                        config.H2Content = Convert.ToInt32(PSA.Cells[2, 1].Value);
                        config.H2Lost = Convert.ToInt32(PSA.Cells[3, 1].Value);
                        scfg.Add(config);
                   
                
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetPSAH2Purification";
            }

            return scfg;


        }





        private void WritePSAH2Purification(List<PetroChem_PSAH2Purification> psa)
        {

            cmd.CommandText = "PetroChem_InsertPSAH2Purification";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_PSAH2Purification p in psa)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", p.UnitID));
                cmd.Parameters.Add(new SqlParameter("@H2Recovered", Write(p.H2Recovered)));
                cmd.Parameters.Add(new SqlParameter("@H2Content", Write(p.H2Content)));
                cmd.Parameters.Add(new SqlParameter("@H2Lost", Write(p.H2Lost)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WritePSAH2Purification";

                }
            }
        }


        public List<PetroChem_UreaFeed> GetUreaFeed()
        {
            int i = 1;
            List<PetroChem_UreaFeed> scfg = new List<PetroChem_UreaFeed>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("UreaFeed").Columns.Count;
                Excel.Range PSA = xlWorkSheet.get_Range("UreaFeed");

               


                        PetroChem_UreaFeed config = new PetroChem_UreaFeed();
                        config.UnitID = 4;
                        config.NH3 = Convert.ToInt32(PSA.Cells[1, 1].Value);
                        config.CO2 = Convert.ToInt32(PSA.Cells[2, 1].Value);
                        config.ProductType = PSA.Cells[3, 1].Value;
                        scfg.Add(config);
                    
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetUreaFeed";
            }

            return scfg;


        }





        private void WriteUreaFeed(List<PetroChem_UreaFeed> uf)
        {

            cmd.CommandText = "PetroChem_InsertUreaFeed";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_UreaFeed u in uf)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", u.UnitID));
                cmd.Parameters.Add(new SqlParameter("@NH3", Write(u.NH3)));
                cmd.Parameters.Add(new SqlParameter("@CO2", Write(u.CO2)));
                cmd.Parameters.Add(new SqlParameter("@ProductType", Write(u.ProductType)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteUreaFeed";

                }
            }
        }


        public List<PetroChem_MajorCompressors> GetMajorCompressors()
        {
            int i = 1;
            List<PetroChem_MajorCompressors> scfg = new List<PetroChem_MajorCompressors>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("MajorComps").Columns.Count;
                Excel.Range MC = xlWorkSheet.get_Range("MajorComps");

                for (i = 1; i <= NumUnits; i++)
                {

                    if (i != 5)
                    {

                        PetroChem_MajorCompressors config = new PetroChem_MajorCompressors();
                        config.UnitID = Convert.ToInt32(xlWorkSheet.Cells[9,i+2].Value);
                        config.ProcessAir = Convert.ToInt32(MC.Cells[1, i].Value);
                        config.SynGas = Convert.ToInt32(MC.Cells[2, i].Value);
                        config.NH3 = Convert.ToInt32(MC.Cells[3, i].Value);
                        config.CO2 = Convert.ToInt32(MC.Cells[4, i].Value);
                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetMajorCompressors(" + i + ")";
            }

            return scfg;


        }





        private void WriteMajorCompressors(List<PetroChem_MajorCompressors> mc)
        {

            cmd.CommandText = "PetroChem_InsertMajorCompressors";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_MajorCompressors m in mc)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", m.UnitID));
                cmd.Parameters.Add(new SqlParameter("@ProcessAir", Write(m.ProcessAir)));
                cmd.Parameters.Add(new SqlParameter("@SynGas", Write(m.SynGas)));
                cmd.Parameters.Add(new SqlParameter("@NH3", Write(m.NH3)));
                cmd.Parameters.Add(new SqlParameter("@CO2", Write(m.CO2)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteMajorCompressors";

                }
            }
        }


        public List<PetroChem_UreaConveyors> GetUreaConveyors()
        {
            int i = 1;
            List<PetroChem_UreaConveyors> scfg = new List<PetroChem_UreaConveyors>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("UreaConveyors").Columns.Count;
                Excel.Range UC = xlWorkSheet.get_Range("UreaConveyors");

                

                        PetroChem_UreaConveyors config = new PetroChem_UreaConveyors();
                        config.UnitID = 22;
                        config.Type = (UC.Cells[1, 1].Value == null) ? "" : UC.Cells[1, 1].Value.ToString();
                        config.NoLines = Convert.ToInt32(UC.Cells[2, 1].Value);
                        config.BedLength = Convert.ToInt32(UC.Cells[3, 1].Value);
                        config.BedWidth = Convert.ToInt32(UC.Cells[4, 1].Value);
                        scfg.Add(config);
                    
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetUreaConveyors(" + i + ")";
            }

            return scfg;


        }





        private void WriteUreaConveyors(List<PetroChem_UreaConveyors> uc)
        {

            cmd.CommandText = "PetroChem_InsertUreaConveyors";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_UreaConveyors u in uc)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", u.UnitID));
                cmd.Parameters.Add(new SqlParameter("@Type", Write(u.Type)));
                cmd.Parameters.Add(new SqlParameter("@NoLines", Write(u.NoLines)));
                cmd.Parameters.Add(new SqlParameter("@BedLength", Write(u.BedLength)));
                cmd.Parameters.Add(new SqlParameter("@BedWidth", Write(u.BedWidth)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteUreaConveyors";

                }
            }
        }


        public List<PetroChem_TAMaintenance> GetTAMaintenance()
        {
            int i = 1;
            List<PetroChem_TAMaintenance> scfg = new List<PetroChem_TAMaintenance>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("TAMaintHist").Columns.Count;
                Excel.Range TAM = xlWorkSheet.get_Range("TAMaintHist");

                for (i = 1; i <= NumUnits; i++)
                {

                    if (TAM.Cells[1, i].Value != null)
                    {

                        PetroChem_TAMaintenance config = new PetroChem_TAMaintenance();
                        config.UnitID = i;
                        config.RecentTADate = (TAM.Cells[1, i].Value==null)? "" : TAM.Cells[1, i].Value.ToString();
                        config.PrevTADate = (TAM.Cells[2, i].Value == null) ? "" : TAM.Cells[2, i].Value.ToString(); ;
                        config.TotalHrsDown = Convert.ToInt32(TAM.Cells[3, i].Value);
                        config.TotalCost = Convert.ToInt32(TAM.Cells[4, i].Value);
                        config.CompanyHrs = Convert.ToInt32(TAM.Cells[5, i].Value);
                        config.ContractorHrs = Convert.ToInt32(TAM.Cells[6, i].Value);
                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetTAMaintenance(" + i + ")";
            }

            return scfg;


        }





        private void WriteTAMaintenance(List<PetroChem_TAMaintenance> TAM)
        {

            cmd.CommandText = "PetroChem_InsertTAMaintenance";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_TAMaintenance t in TAM)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", t.UnitID));
                cmd.Parameters.Add(new SqlParameter("@RecentTADate", Write(t.RecentTADate)));
                cmd.Parameters.Add(new SqlParameter("@PrevTADate", Write(t.PrevTADate)));
                cmd.Parameters.Add(new SqlParameter("@TotalHrsDown", Write(t.TotalHrsDown)));
                cmd.Parameters.Add(new SqlParameter("@TotalCost", Write(t.TotalCost)));
                cmd.Parameters.Add(new SqlParameter("@CompanyHrs", Write(t.CompanyHrs)));
                cmd.Parameters.Add(new SqlParameter("@ContractorHrs", Write(t.ContractorHrs)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteTAMainenance";

                }
            }
        }


        public List<PetroChem_NonTADowntime> GetNonTADowntime()
        {
            int i = 1;
            List<PetroChem_NonTADowntime> scfg = new List<PetroChem_NonTADowntime>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Process-Level Input");
            try
            {

                int NumUnits = xlWorkSheet.get_Range("NonTADowntime").Columns.Count;
                Excel.Range NTD = xlWorkSheet.get_Range("NonTADowntime");

                for (i = 1; i <= NumUnits; i++)
                {

                    if (NTD.Cells[1, i].Value != null)
                    {

                        PetroChem_NonTADowntime config = new PetroChem_NonTADowntime();
                        config.UnitID = i;
                        config.TotalRegHrsDown = Convert.ToInt32(NTD.Cells[1, i].Value);
                        config.TotalMechHrsDown = Convert.ToInt32(NTD.Cells[2, i].Value);
                        
                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetNonTADowntime(" + i + ")";
            }

            return scfg;


        }


        private void WriteNonTADowntime(List<PetroChem_NonTADowntime> NTD)
        {

            cmd.CommandText = "PetroChem_InsertNonTADowntime";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_NonTADowntime n in NTD)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", n.UnitID));
                cmd.Parameters.Add(new SqlParameter("@TotalRegHrsDown", Write(n.TotalRegHrsDown)));
                cmd.Parameters.Add(new SqlParameter("@TotalMechHrsDown", Write(n.TotalMechHrsDown)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteNonTADowntime";

                }
            }
        }



        public List<PetroChem_RawMaterials> GetRawMaterials()
        {
            int i = 1;
            List<PetroChem_RawMaterials> scfg = new List<PetroChem_RawMaterials>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Raw Material Input");
            try
            {

                int NumRows = xlWorkSheet.get_Range("RawMaterials").Rows.Count;
                Excel.Range RM = xlWorkSheet.get_Range("RawMaterials");

                for (i = 1; i <= NumRows; i++)
                {

                    if (RM.Cells[i,1].Value != null)
                    {

                        PetroChem_RawMaterials config = new PetroChem_RawMaterials();
                        
                        config.MaterialName = RM.Cells[i,1].Value.ToString();
                        config.KNM3 = Convert.ToInt32(RM.Cells[i,2].Value);
                        config.Density = Convert.ToInt32(RM.Cells[i,3].Value);
                        config.Tonnes = Convert.ToInt32(RM.Cells[i,4].Value);

                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetRawMaterials(" + i + ")";
            }

            return scfg;


        }





        private void WriteRawMaterials(List<PetroChem_RawMaterials> RM)
        {

            cmd.CommandText = "PetroChem_InsertRawMaterials";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_RawMaterials r in RM)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@MaterialName", Write(r.MaterialName)));
                cmd.Parameters.Add(new SqlParameter("@KNM3", Write(r.KNM3)));
                cmd.Parameters.Add(new SqlParameter("@Density", Write(r.Density)));
                cmd.Parameters.Add(new SqlParameter("@Tonnes", Write(r.Tonnes)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteRawMaterials";

                }
            }
        }




        public List<PetroChem_ProductYield> GetProductYield()
        {
            int i = 1;
            List<PetroChem_ProductYield> scfg = new List<PetroChem_ProductYield>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Product Yield");
            try
            {

                int NumRows = xlWorkSheet.get_Range("FinishedPY").Rows.Count;
                Excel.Range PY = xlWorkSheet.get_Range("FinishedPY");

                for (i = 1; i <= NumRows; i++)
                {

                    if (PY.Cells[i,1].Value != null)
                    {

                        PetroChem_ProductYield config = new PetroChem_ProductYield();
                        config.ProductName = (PY.Cells[i, 1].Value == null) ? "" : PY.Cells[i, 1].Value.ToString();
                        config.Tonnes = Convert.ToInt32(PY.Cells[i,2].Value);
                        
                        scfg.Add(config);
                    }
                }

                NumRows = xlWorkSheet.get_Range("OtherPY").Rows.Count;
                Excel.Range OPY = xlWorkSheet.get_Range("OtherPY");

                for (i = 1; i <= NumRows; i++)
                {

                    if (OPY.Cells[i, 1].Value != null)
                    {

                        PetroChem_ProductYield config = new PetroChem_ProductYield();
                        config.ProductName = (OPY.Cells[i, 1].Value == null) ? "" : OPY.Cells[i, 1].Value.ToString();
                        config.Tonnes = Convert.ToInt32(OPY.Cells[i, 2].Value);

                        scfg.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetProductYield(" + i + ")";
            }

            return scfg;


        }





        private void WriteProductYield(List<PetroChem_ProductYield> PY)
        {

            cmd.CommandText = "PetroChem_InsertProductYield";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_ProductYield p in PY)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@ProductName", Write(p.ProductName)));
                cmd.Parameters.Add(new SqlParameter("@Tonnes", Write(p.Tonnes)));
                
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteProductYield";

                }
            }
        }



        public List<PetroChem_HydroCarbonLoss> GetHydroCarbonLoss()
        {
            int i = 1;
            List<PetroChem_HydroCarbonLoss> scfg = new List<PetroChem_HydroCarbonLoss>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item("Hydrocarbon Loss");
            try
            {

                int NumRows = xlWorkSheet.get_Range("HydrocarbonLoss").Rows.Count;
                Excel.Range HC = xlWorkSheet.get_Range("HydrocarbonLoss");

                

                    

                        PetroChem_HydroCarbonLoss config = new PetroChem_HydroCarbonLoss();
                        config.GTHydrocarbon = Convert.ToInt32(HC.Cells[1, 1].Value);
                        config.FlareLosses = Convert.ToInt32(HC.Cells[2, 1].Value);

                        scfg.Add(config);
                    
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetHydroCarbonLoss(" + i + ")";
            }

            return scfg;


        }





        private void WriteHydroCarbonLoss(List<PetroChem_HydroCarbonLoss> HC)
        {

            cmd.CommandText = "PetroChem_InsertHydrocarbonLoss";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            foreach (PetroChem_HydroCarbonLoss h in HC)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@GTHydrocarbon", Write(h.GTHydrocarbon)));
                cmd.Parameters.Add(new SqlParameter("@FlareLosses", Write(h.FlareLosses)));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteHydroCarbonLoss";

                }
            }
        }

        public int? Write(int? str)
        {
            if (str == null)

                return 0;
            else
                return Convert.ToInt32(str);
        }

        public string Write(string str)
        {
            if (str == null)

                return "";
            else
                return str.ToString();
        }


    }
}
