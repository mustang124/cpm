﻿namespace ISS
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnImport = new System.Windows.Forms.Button();
            this.chkLPGShipping = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPetroChemPath = new System.Windows.Forms.TextBox();
            this.chkPetroChem = new System.Windows.Forms.CheckBox();
            this.txtAirRefuelingPath = new System.Windows.Forms.TextBox();
            this.chkAirRefueling = new System.Windows.Forms.CheckBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.txtLPGShippingPath = new System.Windows.Forms.TextBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRefiningPath = new System.Windows.Forms.TextBox();
            this.chkRefining = new System.Windows.Forms.CheckBox();
            this.txtLubesPath = new System.Windows.Forms.TextBox();
            this.chkLubes = new System.Windows.Forms.CheckBox();
            this.txtLPGPath = new System.Windows.Forms.TextBox();
            this.chkLPG = new System.Windows.Forms.CheckBox();
            this.txtTermPath = new System.Windows.Forms.TextBox();
            this.chkTerminals = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.chkUpstream = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(559, 315);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 0;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // chkLPGShipping
            // 
            this.chkLPGShipping.AutoSize = true;
            this.chkLPGShipping.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLPGShipping.Location = new System.Drawing.Point(28, 32);
            this.chkLPGShipping.Name = "chkLPGShipping";
            this.chkLPGShipping.Size = new System.Drawing.Size(103, 17);
            this.chkLPGShipping.TabIndex = 3;
            this.chkLPGShipping.Text = "LPG Shipping";
            this.chkLPGShipping.UseVisualStyleBackColor = true;
            this.chkLPGShipping.CheckedChanged += new System.EventHandler(this.chkLPGShipping_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.chkUpstream);
            this.panel1.Controls.Add(this.txtTermPath);
            this.panel1.Controls.Add(this.chkTerminals);
            this.panel1.Controls.Add(this.txtLPGPath);
            this.panel1.Controls.Add(this.chkLPG);
            this.panel1.Controls.Add(this.txtLubesPath);
            this.panel1.Controls.Add(this.chkLubes);
            this.panel1.Controls.Add(this.txtRefiningPath);
            this.panel1.Controls.Add(this.chkRefining);
            this.panel1.Controls.Add(this.txtPetroChemPath);
            this.panel1.Controls.Add(this.chkPetroChem);
            this.panel1.Controls.Add(this.txtAirRefuelingPath);
            this.panel1.Controls.Add(this.chkAirRefueling);
            this.panel1.Controls.Add(this.txtLPGShippingPath);
            this.panel1.Controls.Add(this.chkLPGShipping);
            this.panel1.Location = new System.Drawing.Point(15, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(701, 269);
            this.panel1.TabIndex = 4;
            // 
            // txtPetroChemPath
            // 
            this.txtPetroChemPath.Location = new System.Drawing.Point(215, 82);
            this.txtPetroChemPath.Name = "txtPetroChemPath";
            this.txtPetroChemPath.Size = new System.Drawing.Size(422, 20);
            this.txtPetroChemPath.TabIndex = 9;
            // 
            // chkPetroChem
            // 
            this.chkPetroChem.AutoSize = true;
            this.chkPetroChem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPetroChem.Location = new System.Drawing.Point(28, 84);
            this.chkPetroChem.Name = "chkPetroChem";
            this.chkPetroChem.Size = new System.Drawing.Size(111, 17);
            this.chkPetroChem.TabIndex = 8;
            this.chkPetroChem.Text = "Petro Chemical";
            this.chkPetroChem.UseVisualStyleBackColor = true;
            this.chkPetroChem.CheckedChanged += new System.EventHandler(this.chkPetroChem_CheckedChanged);
            // 
            // txtAirRefuelingPath
            // 
            this.txtAirRefuelingPath.Location = new System.Drawing.Point(215, 56);
            this.txtAirRefuelingPath.Name = "txtAirRefuelingPath";
            this.txtAirRefuelingPath.Size = new System.Drawing.Size(422, 20);
            this.txtAirRefuelingPath.TabIndex = 7;
            // 
            // chkAirRefueling
            // 
            this.chkAirRefueling.AutoSize = true;
            this.chkAirRefueling.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAirRefueling.Location = new System.Drawing.Point(28, 58);
            this.chkAirRefueling.Name = "chkAirRefueling";
            this.chkAirRefueling.Size = new System.Drawing.Size(99, 17);
            this.chkAirRefueling.TabIndex = 6;
            this.chkAirRefueling.Text = "Air Refueling";
            this.chkAirRefueling.UseVisualStyleBackColor = true;
            this.chkAirRefueling.CheckedChanged += new System.EventHandler(this.chkAirRefueling_CheckedChanged);
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(641, 315);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(75, 23);
            this.btnQuit.TabIndex = 5;
            this.btnQuit.Text = "Quit";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // txtLPGShippingPath
            // 
            this.txtLPGShippingPath.Location = new System.Drawing.Point(215, 30);
            this.txtLPGShippingPath.Name = "txtLPGShippingPath";
            this.txtLPGShippingPath.Size = new System.Drawing.Size(422, 20);
            this.txtLPGShippingPath.TabIndex = 4;
            this.txtLPGShippingPath.Enter += new System.EventHandler(this.txtLPGShippingPath_Enter);
            this.txtLPGShippingPath.Leave += new System.EventHandler(this.txtLPGShippingPath_Leave);
            // 
            // btnProcess
            // 
            this.btnProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.Location = new System.Drawing.Point(586, 6);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(24, 20);
            this.btnProcess.TabIndex = 10;
            this.btnProcess.Text = "X";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click_1);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.Color.Green;
            this.lblStatus.Location = new System.Drawing.Point(12, 320);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(47, 13);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "Status:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(616, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Excel Processes";
            // 
            // txtRefiningPath
            // 
            this.txtRefiningPath.Location = new System.Drawing.Point(215, 108);
            this.txtRefiningPath.Name = "txtRefiningPath";
            this.txtRefiningPath.Size = new System.Drawing.Size(422, 20);
            this.txtRefiningPath.TabIndex = 11;
            // 
            // chkRefining
            // 
            this.chkRefining.AutoSize = true;
            this.chkRefining.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRefining.Location = new System.Drawing.Point(28, 110);
            this.chkRefining.Name = "chkRefining";
            this.chkRefining.Size = new System.Drawing.Size(73, 17);
            this.chkRefining.TabIndex = 10;
            this.chkRefining.Text = "Refining";
            this.chkRefining.UseVisualStyleBackColor = true;
            this.chkRefining.CheckedChanged += new System.EventHandler(this.chkRefining_CheckedChanged);
            // 
            // txtLubesPath
            // 
            this.txtLubesPath.Location = new System.Drawing.Point(215, 134);
            this.txtLubesPath.Name = "txtLubesPath";
            this.txtLubesPath.Size = new System.Drawing.Size(422, 20);
            this.txtLubesPath.TabIndex = 13;
            // 
            // chkLubes
            // 
            this.chkLubes.AutoSize = true;
            this.chkLubes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLubes.Location = new System.Drawing.Point(28, 136);
            this.chkLubes.Name = "chkLubes";
            this.chkLubes.Size = new System.Drawing.Size(60, 17);
            this.chkLubes.TabIndex = 12;
            this.chkLubes.Text = "Lubes";
            this.chkLubes.UseVisualStyleBackColor = true;
            // 
            // txtLPGPath
            // 
            this.txtLPGPath.Location = new System.Drawing.Point(215, 160);
            this.txtLPGPath.Name = "txtLPGPath";
            this.txtLPGPath.Size = new System.Drawing.Size(422, 20);
            this.txtLPGPath.TabIndex = 15;
            // 
            // chkLPG
            // 
            this.chkLPG.AutoSize = true;
            this.chkLPG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLPG.Location = new System.Drawing.Point(28, 162);
            this.chkLPG.Name = "chkLPG";
            this.chkLPG.Size = new System.Drawing.Size(50, 17);
            this.chkLPG.TabIndex = 14;
            this.chkLPG.Text = "LPG";
            this.chkLPG.UseVisualStyleBackColor = true;
            // 
            // txtTermPath
            // 
            this.txtTermPath.Location = new System.Drawing.Point(215, 187);
            this.txtTermPath.Name = "txtTermPath";
            this.txtTermPath.Size = new System.Drawing.Size(422, 20);
            this.txtTermPath.TabIndex = 17;
            // 
            // chkTerminals
            // 
            this.chkTerminals.AutoSize = true;
            this.chkTerminals.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTerminals.Location = new System.Drawing.Point(28, 189);
            this.chkTerminals.Name = "chkTerminals";
            this.chkTerminals.Size = new System.Drawing.Size(80, 17);
            this.chkTerminals.TabIndex = 16;
            this.chkTerminals.Text = "Terminals";
            this.chkTerminals.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(215, 213);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(422, 20);
            this.textBox1.TabIndex = 19;
            // 
            // chkUpstream
            // 
            this.chkUpstream.AutoSize = true;
            this.chkUpstream.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkUpstream.Location = new System.Drawing.Point(28, 215);
            this.chkUpstream.Name = "chkUpstream";
            this.chkUpstream.Size = new System.Drawing.Size(79, 17);
            this.chkUpstream.TabIndex = 18;
            this.chkUpstream.Text = "Upstream";
            this.chkUpstream.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 347);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnImport);
            this.Name = "frmMain";
            this.Text = "ISS Short Form Import";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.CheckBox chkLPGShipping;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtLPGShippingPath;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.TextBox txtAirRefuelingPath;
        private System.Windows.Forms.CheckBox chkAirRefueling;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox txtPetroChemPath;
        private System.Windows.Forms.CheckBox chkPetroChem;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox chkUpstream;
        private System.Windows.Forms.TextBox txtTermPath;
        private System.Windows.Forms.CheckBox chkTerminals;
        private System.Windows.Forms.TextBox txtLPGPath;
        private System.Windows.Forms.CheckBox chkLPG;
        private System.Windows.Forms.TextBox txtLubesPath;
        private System.Windows.Forms.CheckBox chkLubes;
        private System.Windows.Forms.TextBox txtRefiningPath;
        private System.Windows.Forms.CheckBox chkRefining;
    }
}

