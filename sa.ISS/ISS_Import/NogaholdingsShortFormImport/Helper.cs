﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace LPGShipping
{
    public static class Helper
    {

        

        public static string EraseAll(string conn, string companyID, string submissionID)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(conn);
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "LPGShipping.ClearAllSubmissions";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@CompanyID", companyID));
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", submissionID));
            int rtn = cmd.ExecuteNonQuery();
            cn.Close();
            if (rtn != 0)
                return "Cleared";
            else
                return "Not Cleared";


        }


        public static int WriteSubmission(string conn, int SubmissionID, int companyID)
        {

            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(conn);
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "ISS_WriteSubmission";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@CompanyID", companyID));
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));

            int sid = cmd.ExecuteNonQuery();
            cn.Close();
            return sid;

        }

        public static int GetNextSubmissionID(string conn)
        {

            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(conn);
            cn.Open();
            cmd.Connection = cn;
            cmd.CommandText = "ISS_GetNextSubmissionID";
            cmd.CommandType = CommandType.StoredProcedure;
            int sid = Convert.ToInt32(cmd.ExecuteScalar());
            cn.Close();
            return sid;
        }

        public static void WriteLog(string str, string CurrentLogFile)
        {
            //Write to Log
            StreamWriter fs = new StreamWriter(CurrentLogFile, true);
            fs.WriteLine(DateTime.Now.ToString() + ": " + str);
            fs.Close();
        }

        public static bool WorkSheetExist(string s, Excel.Workbook wb )
        {

            bool exist = false;
            foreach (Excel.Worksheet ws in wb.Worksheets)
            {
                if (ws.Name == s) exist = true;

            }
            return exist;
        }

    }
}
