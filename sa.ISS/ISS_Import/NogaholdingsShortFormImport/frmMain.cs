﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Shipping = LPGShipping;
using Air = AirRefueling;
using PetroChem = PetroChemLibrary;
using Ref = RefiningLibrary;

namespace ISS
{
    
    public partial class frmMain : Form
    {
        //READ SETTINGS
        string CONNECTSTR = ISS.Properties.Settings.Default.CONN;

        string CurrentLogFile = null;
        string PetroChemPath = null;
        string LPGShippingPath = null;
        string RefiningPath = null;
        
        
        Shipping.LPGShipping ship;
        AirRefueling.AirRefueling air;
        PetroChemLibrary.PetroChem petrochem;
        RefiningLibrary.Refinery refining;

        

        
        //Open Excel Application
        Excel.Application excel;
        
        

        //Initialize Connection and Command
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();

        


        public frmMain()
            
        {
            
            InitializeComponent();
            btnProcess.Text = NumberOfProcesses().ToString();
        }


        private string OpenFileDialog()
        {
            string file=null;
            
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                file = openFileDialog1.FileName;
                
            }
            
            return file;
        }


        private void chkLPGShipping_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLPGShipping.Checked)
                txtLPGShippingPath.Text = OpenFileDialog();
            else
                txtLPGShippingPath.Text = "";
 
        }

 
        private void txtLPGShippingPath_Leave(object sender, EventArgs e)
        {
            if (!File.Exists(txtLPGShippingPath.Text))
            {
                MessageBox.Show("File Does Not Exist");
                txtLPGShippingPath.Text = LPGShippingPath;
            }
        }

        private void txtLPGShippingPath_Enter(object sender, EventArgs e)
        {
            LPGShippingPath = txtLPGShippingPath.Text;
        }
        
        
        
        private void btnImport_Click(object sender, EventArgs e)
        {
            string msg = "";
            CurrentLogFile = "";
            try
            {
                if (chkLPGShipping.Checked)
                {
                    ship = new Shipping.LPGShipping(@"LPGShipping.log");
                    msg = ship.ImportLPGShipping(txtLPGShippingPath.Text);
                    
                }

                if (chkAirRefueling.Checked)
                {
                    air = new Air.AirRefueling(@"AirRefueling.log");
                    msg = air.ImportAirRefueling(txtAirRefuelingPath.Text);
                    
                }

                if (chkPetroChem.Checked)
                {
                    petrochem = new PetroChem.PetroChem(@"PetroChem.log");
                    msg = petrochem.ImportPetroChem(txtPetroChemPath.Text);
                    
                }

                if (chkRefining.Checked)
                {
                    refining = new RefiningLibrary.Refinery(@"Refining.log");
                    msg = refining.ImportRefining(txtRefiningPath.Text);
                    
                }

                lblStatus.Text = msg;
            }
            
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        
        private void btnQuit_Click(object sender, EventArgs e)
        {
            if (cn.State == ConnectionState.Open) cn.Close();
            if (excel != null) excel = null;
            System.Windows.Forms.Application.Exit();
        }

        private void chkAirRefueling_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAirRefueling.Checked)
                txtAirRefuelingPath.Text = OpenFileDialog();
            else
                txtAirRefuelingPath.Text = "";
        }

        private void chkPetroChem_CheckedChanged(object sender, EventArgs e)
        {

            if (chkPetroChem.Checked)
                txtPetroChemPath.Text = OpenFileDialog();
            else
                txtPetroChemPath.Text = "";
        
        }

        private void txtPetroChemPath_Leave(object sender, EventArgs e)
        {
            if (!File.Exists(txtPetroChemPath.Text))
            {
                MessageBox.Show("File Does Not Exist");
                txtPetroChemPath.Text = PetroChemPath;
            }
        }

        private void txtPetroChem_Enter(object sender, EventArgs e)
        {
            PetroChemPath = txtPetroChemPath.Text;
        }

                
        private void KillProcesses()
        {
            DialogResult dr = new DialogResult();
            dr = MessageBox.Show("This will kill all OPEN Excel.  Please <SAVE> any Excel documents you have open and then click OK when ready", "Warning", MessageBoxButtons.OKCancel);
            if (dr == DialogResult.OK)
            {
                 System.Diagnostics.Process[] pProcess = System.Diagnostics.Process.GetProcessesByName("Excel");

                foreach(System.Diagnostics.Process ep in pProcess)
                {
                    try
                    {
                        ep.Kill();
                    }
                    catch
                    {
                    }
                }
       
            }
        
        }

        private int NumberOfProcesses()
        {
            int Count = 0;
            foreach(System.Diagnostics.Process mProcess in System.Diagnostics.Process.GetProcesses())
            {
                if (mProcess.ProcessName.Contains("EXCEL"))
                    Count++;
                
            }
            return Count;
        }

        private void btnProcess_Click_1(object sender, EventArgs e)
        {
            KillProcesses();
            btnProcess.Text = "0";
        }

        private void chkRefining_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRefining.Checked)
                txtRefiningPath.Text = OpenFileDialog();
            else
                txtRefiningPath.Text = "";
        }   
        
    }
}
