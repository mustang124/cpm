﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessManager;
using System.Collections.Generic;
using Common;
using System.Configuration;
using Excel = Microsoft.Office.Interop.Excel;

namespace BusinessManagerTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void QuartilesResultsTest()
        {
            QuartileResults results = new QuartileResults();
            Queries routines = new Queries(DateTime.Today, "XXPAC", "Actual", 2014, "2014", "USD", "US");
            //List<string> fieldNames = new List<string>();
            //string refListName = "TEST";
            //results = routines.GetQuartiles(68.9f, 83.8f, 88.9f, 100f, 226.2f, 132f); //"DBS7-DEV", fieldNames, refListName);
            List<Single> last24EIIs = new List<float>() { 125.8f, 129.1f, 134.3f, 133.8f, 128.9f, 133.6f, 125.7f, 128.1f, 133.9f, 131.5f, 166.8f, 140.4f, 123.7f, 137.3f, 130.9f, 129.0f, 126.1f, 131.3f, 131.4f, 129.1f, 127.4f, 132.1f, 132.2f };
            results = routines.GetQuartilesForKpi(68.9f, 83.8f, 88.9f, 100f, 226.2f, 132f, "EII", last24EIIs);
            Assert.IsTrue(results.currentMonth > 39 && results.currentMonth < 41);

            List<Single> last24PEIs = new List<float>() { 208, 250, 223, 210, 202, 205, 205, 210, 238, 277, 462, 319, 201, 228, 220, 250, 219, 234, 238, 231, 225, 247, 447 };
            results = routines.GetQuartilesForKpi(46f, 72, 85.6f, 135.1f, 577.4f, 446.6f, "PEI", last24PEIs);
            Assert.IsTrue(results.currentMonth > 96 && results.currentMonth < 98);
        }

        [TestMethod]
        public void AdjustQuartilesForDisplayTest()
        {
            List<QuartileResults> list = new List<QuartileResults>();
            QuartileResults results = new QuartileResults();
            Queries routines = new Queries(DateTime.Today, "XXPAC", "Actual", 2014, "2014", "USD", "MET");
            //List<string> fieldNames = new List<string>();
            //string refListName = "TEST";
            //results = routines.GetQuartiles(68.9f, 83.8f, 88.9f, 100f, 226.2f, 132f); //"DBS7-DEV", fieldNames, refListName);
            List<Single> last24EIIs = new List<float>() { 125.8f, 129.1f, 134.3f, 133.8f, 128.9f, 133.6f, 125.7f, 128.1f, 133.9f, 131.5f, 166.8f, 140.4f, 123.7f, 137.3f, 130.9f, 129.0f, 126.1f, 131.3f, 131.4f, 129.1f, 127.4f, 132.1f, 132.2f };
            results = routines.GetQuartilesForKpi(68.9f, 83.8f, 88.9f, 100f, 226.2f, 132f, "EII", last24EIIs);
            list.Add(results);
            List<Single> last24NEIs = new List<float>() { 147, 151, 151, 135, 148, 172, 152, 157, 155, 164, 177, 294, 108, 168, 148, 162, 152, 164, 174, 183, 166, 204, 186 };
            results = routines.GetQuartilesForKpi(74.9f, 114.6f, 138.4f, 169.6f, 290f, 185.6f, "NEI", last24NEIs);
            list.Add(results);
            List<QuartileDisplayResults> displayResults = routines.AdjustQuartilesForDisplay(list);
            Assert.IsTrue(displayResults[0].CurrentMonth > 56 && displayResults[0].CurrentMonth < 58);
            Assert.IsTrue(displayResults[1].CurrentMonth > 50 && displayResults[1].CurrentMonth < 51);
        }


        public void TestUtilLossValueOBSOLETE()
        {
            List<float> last24MonthsOfSomething = new List<float>() { 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 };
            DateTime submissionMonth = new DateTime(2017, 11, 1);
            List<float> result = Util.LastJanuaryAndForward(last24MonthsOfSomething, submissionMonth);
            Assert.IsTrue(result.Count == 11);
            result = Util.LastCalendarYear(last24MonthsOfSomething, submissionMonth);
            Assert.IsTrue(result.Count == 12);
            result = Util.Last12Months(last24MonthsOfSomething, submissionMonth);
            Assert.IsTrue(result.Count == 12);
            result = Util.Last24Months(last24MonthsOfSomething, submissionMonth);
            Assert.IsTrue(result.Count == 24);
            List<float> numsToAverage = new List<float>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
            result = Util.AvgFromList(numsToAverage);
            Assert.IsTrue(result.Count == 24);
            result = Util.GetLast24TargetsFromUserDefined("LossValue", ConfigurationManager.ConnectionStrings["ProfileFuelsConnectionString"].ToString(),
                submissionMonth, "161EUR", "Actual", 2014, "2014", "USD", "MET");
            Assert.IsTrue(result.Count == 24);
        }

        [TestMethod]
        public void TestUtilLossValue()
        {
            DateTime startDate = new DateTime(2017, 11, 1);
            string refNum = "161EUR";
            string dataSet = "Actual";
            int factorSet = 2014;
            string scenario = "2014";
            string currency = "USD";
            string uOM = "MET";
            DateTime endDate = startDate;

            Excel.Application xl = null;
            //Excel.Workbook _wb = null;
            xl = new Microsoft.Office.Interop.Excel.Application();
            Excel.Workbook wb = xl.Workbooks.Open(@"C:\tfs\CPA\sa.ISS\ExcelReportTempate\Test.xlsx",
                            Type.Missing, false, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                            Type.Missing, Type.Missing);
            xl.Visible = true;
            Excel.Worksheet net = (Excel.Worksheet)wb.Sheets["Sheet1"];

            //Excel.Worksheet net = _xlFuels.SetSheet(ref xl, ref wb, "Sheet1");

            string conx = ConfigurationManager.ConnectionStrings["ProfileFuelsConnectionString"].ToString();
            DBHelper dbHelper = new DBHelper(conx);
            //Queries queries = new Queries(startDate, refNum, dataSet, factorSet,scenario,currency,uOM);
            int startRow = 1;
            Util.WriteLostTime(ref net,
                ref startRow, dbHelper, "Safety Lost Time Incidents",
                startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            startRow = 15;
            Util.WriteEnviroIncidents(ref net,
                ref startRow, dbHelper, "Reportable Environmental Incidents",
                startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
        }

        [TestMethod]
        public void AverageOfYtdAnnualizedLossValueForMonthTest()
        {
            List<float> lossValuesForMonth = new List<float>() { 500, 600 };
            int year = 2018;
            List<float> results = Util.AverageOfYtdAnnualizedLossValueForMonth(lossValuesForMonth, year);
            Assert.IsTrue(results[0] > 5999 && results[0] < 6001);
            Assert.IsTrue(results[1] > 6569 && results[1] < 6570);
        }

        [TestMethod]
        public void LastJanuaryAndForwardTest()
        {
            //public static List<float> LastJanuaryAndForward(List<float> list, DateTime submissionMonthAndYear)
            List<float> twoYears = new List<float>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
            List<float> result = Util.LastJanuaryAndForward(twoYears, new DateTime(2017, 12, 1));
            Assert.IsTrue(result[0] == 13);
        }

        [TestMethod]
        public void SumPRoductTest()
        {
            DateTime startDate = new DateTime(2018, 3, 1);
            List<float> last24MonthsOfSomething = new List<float> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
            //=============================
            List<float> yTDs = new List<float>();
            float sumOATimesDays = 0;
            int sumDays = 0;
            int monthCount = 1;
            //find the most recent January in last24MonthsOfSomething
            for (int count = 23 - startDate.Month + 1; count < 24; count++)
            {
                DateTime tempDate = new DateTime(startDate.Year, monthCount, 1);
                int numDays = numberOfDaysInThisMonth(monthCount, startDate.Year);
                //multiply it by # days in that month
                float temp = last24MonthsOfSomething[count] * numDays;
                //add it to sumOATimesDays
                sumOATimesDays += temp;
                //add # days in that month to sumDays
                sumDays += numDays;
                //divide sumOATimesDays by sumDays, store it for January YTD in yTDs.
                yTDs.Add(sumOATimesDays / sumDays);
                //repeat above for Feb, etc.
                monthCount++;
            }

        }

        [TestMethod]
        public void LossValueVsTargetKDollarsTest()
        {
            int yearToUse = 2017;
            float lossValue = 0.541f;
            float opAvail = 98.39f;
            float netInputBPD = 240.5f;
            float utilPcnt = 68.95f;
            float lossTarget = 0.42f;
            //float totalNetRawMaterialInputWithFinProdAddit = 97480080.43f;
            //float finishedProdAddit = 2750;
            //float subtotalVolumeRelatedExpenses = 149685.26f;
            float variableCashOpex = 1.54f;
            float rawMaterialCostUsdPerBblNetInput = 40.44f;
            float grossMarginUsdPerBblNetInput = 7.39f;
            float result = Util.LossValueVsTargetKDollars__2018_08_27(yearToUse, lossValue,
                opAvail, netInputBPD, utilPcnt, lossTarget, rawMaterialCostUsdPerBblNetInput,
                grossMarginUsdPerBblNetInput, variableCashOpex);
            Assert.IsTrue(result < 4458.99 && result > 4458.97);
        }

        [TestMethod]
        public void oaLostOpportunityValueTest()
        {
            int yearToUse = 2017;
            float lossValue = 0.541f;
            float opAvail = 98.39f;
            float netInputBPD = 240.5f;
            float utilPcnt = 68.95f;
            float lossTarget = 0.42f;
            float variableCashOpex = 1.54f;
            float rawMaterialCostUsdPerBblNetInput = 40.44f;
            float grossMarginUsdPerBblNetInput = 7.39f;
            float oaTarget = 97.0f;
            float result = Util.OALostOpportunityValue__2018_08_27(yearToUse, lossValue,
                opAvail, netInputBPD, utilPcnt, lossTarget, rawMaterialCostUsdPerBblNetInput,
                grossMarginUsdPerBblNetInput, oaTarget, variableCashOpex);
            Assert.IsTrue(result < 10352.46 && result > 10352.48);
        }

        [TestMethod]
        public void Discard()
        {
            List<float> targets = new List<float>() { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };
            // 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 };
            int ordinal = 11; // startDate.Month;
            int col = 2;
            for (int month = targets.Count - ordinal; month < targets.Count; month++)
            {
                System.Diagnostics.Debug.Write( targets[month].ToString() + ",");
                col++;
            }
        }

        private static int numberOfDaysInThisMonth(int month, int year)
        {
            int result = 0;
            int count = 1;
            try
            {
                while (count <= 31)
                {
                    DateTime temp = new DateTime(year, month, count);
                    result = count;
                    count++;
                }
            }
            catch { }
            return result;
        }
    }

}
