﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelService
{
    public class Refinery
    {

        public string errorMessage { get; set; }
        public Refinery()
        {
        }

        public bool Open(ref Excel.Application xl, ref Excel.Workbook wb,  string xlPath)
        {
            errorMessage = string.Empty;
            try
            {
             wb = xl.Workbooks.Open(xlPath,
                Type.Missing, false, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing);
                xl.Visible = true;
                return true;
           }
            catch(Exception ex)
            {
                errorMessage = "Open(): " + ex.Message;
                return false;
            }
        }

        public Excel.Worksheet SetSheet(ref Excel.Application xl, ref Excel.Workbook wb, string sheetName)
        {
            try
            {
                Excel.Worksheet ws = (Excel.Worksheet)wb.Sheets[sheetName];
                return ws;
            }
            catch (Exception ex)
            {
                throw new Exception("SetSheet(): " + ex.Message);
            }
        }

        public string Read(ref Excel.Worksheet sheet, int row, string col)
        {
            errorMessage = string.Empty;
            try
            {

                return sheet.Cells[row, col].Value.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Read(): " + ex.Message);
            }
        }
        public string Read(ref Excel.Worksheet sheet, int row, int col)
        {
            errorMessage = string.Empty;
            try
            {
                if (sheet.Cells[row, col].Value != null)
                {
                    return sheet.Cells[row, col].Value.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Read(): " + ex.Message);
            }
        }
        /*
        public bool Write(ref Excel.Worksheet sheet, int row, string col, string val)
        {
            errorMessage = string.Empty;
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }
        */
        public bool Write(ref Excel.Worksheet sheet, int row, int col, string val)
        {
            errorMessage = string.Empty;
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }
        public bool Write(ref Excel.Worksheet sheet, int row, string col, Single val)
        {
            errorMessage = string.Empty;
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }
        public bool Write(ref Excel.Worksheet sheet, int row, int col, Single val)
        {
            errorMessage = string.Empty;
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }

        public bool Save(ref Excel.Workbook wb)
        {
            errorMessage = string.Empty;
            try
            {
                wb.Save();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }

        public bool ClearSheet(ref Excel.Worksheet sheet)
        {
            errorMessage = string.Empty;
            try
            {
                sheet.Range["A1:CZ1000"].Select();
                sheet.Range["A1:CZ1000"].Clear();
                sheet.Range["A1"].Select();
                sheet.Range["B2"].Value = "Jan";
                sheet.Range["C2"].Value = "Feb";
                sheet.Range["D2"].Value = "Mar";
                sheet.Range["E2"].Value = "Apr";
                sheet.Range["F2"].Value = "May";
                sheet.Range["G2"].Value = "Jun";
                sheet.Range["H2"].Value = "Jul";
                sheet.Range["I2"].Value = "Aug";
                sheet.Range["J2"].Value = "Sep";
                sheet.Range["K2"].Value = "Oct";
                sheet.Range["L2"].Value = "Nov";
                sheet.Range["M2"].Value = "Dec";

                sheet.Range["N2"].Value = "Jan";
                sheet.Range["O2"].Value = "Feb";
                sheet.Range["P2"].Value = "Mar";
                sheet.Range["Q2"].Value = "Apr";
                sheet.Range["R2"].Value = "May";
                sheet.Range["S2"].Value = "Jun";
                sheet.Range["T2"].Value = "Jul";
                sheet.Range["U2"].Value = "Aug";
                sheet.Range["V2"].Value = "Sep";
                sheet.Range["W2"].Value = "Oct";
                sheet.Range["X2"].Value = "Nov";
                sheet.Range["Y2"].Value = "Dec";

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }

    }
}
