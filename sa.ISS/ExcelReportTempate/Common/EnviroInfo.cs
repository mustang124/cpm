﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class EnviroInfo
    {
        public float TotalEnviroEventsForMonth { get; set; }
        public float TotalEnviroEventsYTD { get; set; }
        public float TotalPersonnelWorkhoursForMonth { get; set; }
        public float TotalPersonnelWorkhoursYTD { get; set; }
        public float EnviroIncidentRateForMonth { get; set; }
        public float EnviroIncidentRateYTD { get; set; }
        public int MonthDataIsFor { get; set; }
        public int YearDataIsFor { get; set; }


    }
}
