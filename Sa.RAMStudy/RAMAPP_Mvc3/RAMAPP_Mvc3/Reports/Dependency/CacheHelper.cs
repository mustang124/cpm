﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RAMAPP_Mvc3.Dependency
{
    public class CacheHelper
    {
        internal static string GetHelperKeyName(string key)
        {
            return key + ":RAM:CacheDependency";
        }

        public static void Insert(string key, object keyValue,
                                  Dependency.CacheDependency dep)
        {
            // Create the array of cache keys for the CacheDependency ctor
            string storageKey = GetHelperKeyName(key);

            // Create a helper cache key
            HttpContext.Current.Cache.Insert(storageKey, dep);

            // Create a standard CacheDependency object and 
            // link it to the previously created 
            string[] rgDepKeys = new string[1];
            rgDepKeys[0] = storageKey;
            System.Web.Caching.CacheDependency keyDep;
            keyDep = new System.Web.Caching.CacheDependency(null, rgDepKeys);

            // Create a new entry in the cache and make it dependent on a 
            // helper key
            HttpContext.Current.Cache.Insert(key, keyValue, keyDep);
        }
    }
}