﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using RAMAPP_Mvc3.Helpers;

namespace RAMAPP_Mvc3.Reports
{
    public partial class ODRReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var cp = SessionUtil.AdminKey();
            var mode = Session["Mode"];
            var IsSAISession = mode != null ? (mode.ToString() == cp) : false;
            var userinfo = ((RAMAPP_Mvc3.Entity.LogIn)Session["UserInfo"]);
            if (IsSAISession || userinfo.Email == "brian.franze@solomononline.com")
            {
                if (!Page.IsPostBack)
                {
                    FillReport(null);
                }
            }
        }

        public DataSet GetReportDataSet()
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMConnectionStringSQLClient"].ToString());
            using (conn)
            {
                ControlParameter cpYear = new ControlParameter();
                //SqlCommand command = new SqlCommand("SELECT	DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex, NULL AS 'RAMEIPerformance', NULL AS 'MAIPerformance', NULL AS 'MCIPerformance', DSLUCo.FacilityID, NULL AS 'TotalOpp', CAST(NULL AS varchar(200)) AS 'WorstGap', CAST(NULL AS varchar(200)) AS 'BestGap' FROM Dataset_LU DSLU JOIN RankData ON RankData.DatasetID = DSLU.DatasetID JOIN Output.SiteSummary AS OSS ON OSS.SubGroupID = CAST(DSLU.DatasetID as varchar(4)) JOIN Dataset_LU DSLUCo ON DSLUCo.DatasetID = DSLU.ParentID WHERE DSLU.DataLevel = 'Site' AND DSLU.StudyYear > 2010 AND DSLU.Deleted = 0 AND DSLU.ParentID != 3 AND DSLU.ParentID != 6 AND DSLU.FacilityID NOT LIKE '%test%' AND	RankData.PlantType IS NOT NULL GROUP BY DSLUCo.FacilityID, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex ORDER BY DSLUCo.FacilityID, DSLU.StudyYear DESC", conn);
                //SqlCommand command = new SqlCommand("SELECT	DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex, NULL AS 'RAMEIPerformance', NULL AS 'MAIPerformance', NULL AS 'MCIPerformance', dbo.GetCompanyID(DSLU.DatasetID) AS FacilityID, NULL AS 'TotalOpp', CAST(NULL AS varchar(200)) AS 'WorstGap', CAST(NULL AS varchar(200)) AS 'BestGap' FROM Dataset_LU DSLU JOIN RankData ON RankData.DatasetID = DSLU.DatasetID JOIN Output.SiteSummary AS OSS ON OSS.SubGroupID = CAST(DSLU.DatasetID as varchar(4)) WHERE DSLU.DataLevel = 'Site' AND DSLU.StudyYear > 2010 AND DSLU.Deleted = 0 AND DSLU.ParentID != 3 AND DSLU.ParentID != 6 AND DSLU.FacilityID NOT LIKE '%test%' AND	RankData.PlantType IS NOT NULL GROUP BY FacilityID, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex ORDER BY FacilityID, DSLU.StudyYear DESC", conn);
                //SqlCommand command = new SqlCommand("SELECT vgiBest.Item AS BestGapItem, vGapMUSD.BestGap AS BestGapMUSD, vgiWorst.Item AS WorstGapItem, vGapMUSD.WorstGap AS WorstGapMUSD, so.TotOppMUSD, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex, NULL AS 'RAMEIPerformance', NULL AS 'MAIPerformance', NULL AS 'MCIPerformance', DSLUCo.FacilityID FROM Dataset_LU DSLU JOIN RankData ON RankData.DatasetID = DSLU.DatasetID JOIN Output.SiteSummary AS OSS ON OSS.SubGroupID = CAST(DSLU.DatasetID as varchar(4)) JOIN Dataset_LU DSLUCo ON DSLUCo.DatasetID = DSLU.ParentID LEFT OUTER JOIN View_BestWorstGapMUSD vGapMUSD on vGapMUSD.DatasetID = DSLU.DatasetID LEFT OUTER JOIN View_GapItems vgiBest ON vgiBest.DatasetID = vGapMUSD.DatasetID AND vgiBest.GapMUSD = vGapMUSD.BestGap LEFT OUTER JOIN View_GapItems vgiWorst ON vgiWorst.DatasetID = vGapMUSD.DatasetID AND vgiWorst.GapMUSD = vGapMUSD.WorstGap LEFT OUTER JOIN Gaps.SiteOpportunities so on so.DataSetID = DSLU.DatasetID WHERE DSLU.DataLevel = 'Site' AND DSLU.StudyYear > 2010 AND DSLU.Deleted = 0 AND DSLU.ParentID != 3 AND DSLU.ParentID != 6 AND DSLU.FacilityID NOT LIKE '%test%' AND	RankData.PlantType IS NOT NULL GROUP BY DSLUCo.FacilityID, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex, vGapMUSD.BestGap, vGapMUSD.WorstGap, vgiBest.Item, vgiWorst.Item, so.TotOppMUSD ORDER BY DSLUCo.FacilityID, DSLU.StudyYear DESC", conn);
                //SqlCommand command = new SqlCommand("SELECT vGapMUSD.BestGap, vGapMUSD.WorstGap, REPLACE(CAST(ROUND(vGapMUSD.BestGap,1) AS nvarchar(100)) + '/' + vgiBest.Item,'GapMUSD','') AS BestGapItem, REPLACE(CAST(ROUND(vGapMUSD.WorstGap,1) AS varchar(100)) + '/' + vgiWorst.Item,'GapMUSD','') AS WorstGapItem, ROUND(so.TotOppMUSD,1) AS TotalOpp, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex, NULL AS 'RAMEIPerformance', NULL AS 'MAIPerformance', NULL AS 'MCIPerformance', DSLUCo.FacilityID FROM Dataset_LU DSLU JOIN RankData ON RankData.DatasetID = DSLU.DatasetID JOIN Output.SiteSummary AS OSS ON OSS.SubGroupID = CAST(DSLU.DatasetID as varchar(4)) JOIN Dataset_LU DSLUCo ON DSLUCo.DatasetID = DSLU.ParentID LEFT OUTER JOIN View_BestWorstGapMUSD vGapMUSD on vGapMUSD.DatasetID = DSLU.DatasetID LEFT OUTER JOIN View_GapItems vgiBest ON vgiBest.DatasetID = vGapMUSD.DatasetID AND vgiBest.GapMUSD = vGapMUSD.BestGap LEFT OUTER JOIN View_GapItems vgiWorst ON vgiWorst.DatasetID = vGapMUSD.DatasetID AND vgiWorst.GapMUSD = vGapMUSD.WorstGap LEFT OUTER JOIN Gaps.SiteOpportunities so on so.DataSetID = DSLU.DatasetID WHERE DSLU.DataLevel = 'Site' AND DSLU.StudyYear > 2010 AND DSLU.Deleted = 0 AND DSLU.ParentID != 3 AND DSLU.ParentID != 6 AND DSLU.FacilityID NOT LIKE '%test%' AND	RankData.PlantType IS NOT NULL GROUP BY DSLUCo.FacilityID, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex, vGapMUSD.BestGap, vGapMUSD.WorstGap, vgiBest.Item, vgiWorst.Item, so.TotOppMUSD ORDER BY DSLUCo.FacilityID, DSLU.StudyYear DESC", conn);
                SqlCommand command = new SqlCommand("SELECT vGapMUSD.BestGap, vGapMUSD.WorstGap, REPLACE(CAST(ROUND(vGapMUSD.BestGap,1) AS nvarchar(100)) + '/' + vgiBest.Item,'GapMUSD','') AS BestGapItem, REPLACE(CAST(ROUND(vGapMUSD.WorstGap,1) AS varchar(100)) + '/' + vgiWorst.Item,'GapMUSD','') AS WorstGapItem, ROUND(so.TotOppMUSD,1) AS TotalOpp, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, UD.MechAvailSlow AS LossPcntPRV_Total, OSS.MaintEffectIndex, NULL AS 'RAMEIPerformance', NULL AS 'MAIPerformance', NULL AS 'MCIPerformance', DSLUCo.FacilityID FROM Dataset_LU DSLU JOIN RankData ON RankData.DatasetID = DSLU.DatasetID JOIN Output.UnitDowntime AS UD ON UD.SubgroupID = CAST(DSLU.DatasetID as varchar(4)) JOIN Output.SiteSummary AS OSS ON OSS.SubGroupID = CAST(DSLU.DatasetID as varchar(4)) JOIN Dataset_LU DSLUCo ON DSLUCo.DatasetID = DSLU.ParentID LEFT OUTER JOIN View_BestWorstGapMUSD vGapMUSD on vGapMUSD.DatasetID = DSLU.DatasetID LEFT OUTER JOIN View_GapItems vgiBest ON vgiBest.DatasetID = vGapMUSD.DatasetID AND vgiBest.GapMUSD = vGapMUSD.BestGap LEFT OUTER JOIN View_GapItems vgiWorst ON vgiWorst.DatasetID = vGapMUSD.DatasetID AND vgiWorst.GapMUSD = vGapMUSD.WorstGap LEFT OUTER JOIN Gaps.SiteOpportunities so on so.DataSetID = DSLU.DatasetID WHERE DSLU.DataLevel = 'Site' AND DSLU.StudyYear > 2010 AND DSLU.Deleted = 0 AND DSLU.ParentID != 3 AND DSLU.ParentID != 6 AND DSLU.FacilityID NOT LIKE '%test%' AND	RankData.PlantType IS NOT NULL GROUP BY DSLUCo.FacilityID, DSLU.StudyYear, DSLU.DatasetID, DSLU.FacilityName, RankData.PlantType, OSS.TotMaintCost, OSS.LossPcntPRV_Total, OSS.MaintEffectIndex, vGapMUSD.BestGap, vGapMUSD.WorstGap, vgiBest.Item, vgiWorst.Item, so.TotOppMUSD, UD.MechAvailSlow ORDER BY DSLUCo.FacilityID, DSLU.StudyYear DESC", conn);
                command.CommandType = CommandType.Text;
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsSites = new DataSet();
                sda.Fill(dsSites);
                DataSet dsQuartiles = GetQuartiles();
                foreach (DataRow dr in dsSites.Tables[0].Rows)
                {
                    if (dr["PlantType"].ToString() == "Chem" && dr["StudyYear"].ToString() == "2011")
                    {
                        if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MEIQ1"].ToString()))
                            dr["RAMEIPerformance"] = 1;
                        else if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MEIQ2"].ToString()))
                            dr["RAMEIPerformance"] = 2;
                        else if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MEIQ3"].ToString()))
                            dr["RAMEIPerformance"] = 3;
                        else
                            dr["RAMEIPerformance"] = 4;

                        if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MechAvailSlow_Q1"].ToString()))
                            dr["MAIPerformance"] = 1;
                        else if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MechAvailSlow_Q2"].ToString()))
                            dr["MAIPerformance"] = 2;
                        else if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MechAvailSlow_Q3"].ToString()))
                            dr["MAIPerformance"] = 3;
                        else
                            dr["MAIPerformance"] = 4;

                        if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MCIQ1"].ToString()))
                            dr["MCIPerformance"] = 1;
                        else if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MCIQ2"].ToString()))
                            dr["MCIPerformance"] = 2;
                        else if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem11_MCIQ3"].ToString()))
                            dr["MCIPerformance"] = 3;
                        else
                            dr["MCIPerformance"] = 4;

                    }
                    if (dr["PlantType"].ToString() == "Chem" && dr["StudyYear"].ToString() == "2012")
                    {
                        if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MEIQ1"].ToString()))
                            dr["RAMEIPerformance"] = 1;
                        else if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MEIQ2"].ToString()))
                            dr["RAMEIPerformance"] = 2;
                        else if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MEIQ3"].ToString()))
                            dr["RAMEIPerformance"] = 3;
                        else
                            dr["RAMEIPerformance"] = 4;

                        if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MechAvailSlow_Q1"].ToString()))
                            dr["MAIPerformance"] = 1;
                        else if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MechAvailSlow_Q2"].ToString()))
                            dr["MAIPerformance"] = 2;
                        else if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MechAvailSlow_Q3"].ToString()))
                            dr["MAIPerformance"] = 3;
                        else
                            dr["MAIPerformance"] = 4;

                        if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MCIQ1"].ToString()))
                            dr["MCIPerformance"] = 1;
                        else if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MCIQ2"].ToString()))
                            dr["MCIPerformance"] = 2;
                        else if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem12_MCIQ3"].ToString()))
                            dr["MCIPerformance"] = 3;
                        else
                            dr["MCIPerformance"] = 4;
                    }
                    if (dr["PlantType"].ToString() == "Chem" && dr["StudyYear"].ToString() == "2013")
                    {
                        if (dsQuartiles.Tables[0].Rows[0]["Chem13_MEIQ1"].ToString() != "" && (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MEIQ1"].ToString())))
                            dr["RAMEIPerformance"] = 1;
                        else if (dsQuartiles.Tables[0].Rows[0]["Chem13_MEIQ2"].ToString() != "" && (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MEIQ2"].ToString())))
                            dr["RAMEIPerformance"] = 2;
                        else if (dsQuartiles.Tables[0].Rows[0]["Chem13_MEIQ3"].ToString() != "" && (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MEIQ3"].ToString())))
                            dr["RAMEIPerformance"] = 3;
                        else
                            dr["RAMEIPerformance"] = 4;

                        if (dsQuartiles.Tables[0].Rows[0]["Chem13_MechAvailSlow_Q1"].ToString() != "" && (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MechAvailSlow_Q1"].ToString())))
                            dr["MAIPerformance"] = 1;
                        else if (dsQuartiles.Tables[0].Rows[0]["Chem13_MechAvailSlow_Q2"].ToString() != "" && (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MechAvailSlow_Q2"].ToString())))
                            dr["MAIPerformance"] = 2;
                        else if (dsQuartiles.Tables[0].Rows[0]["Chem13_MechAvailSlow_Q3"].ToString() != "" && (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MechAvailSlow_Q3"].ToString())))
                            dr["MAIPerformance"] = 3;
                        else
                            dr["MAIPerformance"] = 4;

                        if (dsQuartiles.Tables[0].Rows[0]["Chem13_MCIQ1"].ToString() != "" && (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MCIQ1"].ToString())))
                            dr["MCIPerformance"] = 1;
                        else if (dsQuartiles.Tables[0].Rows[0]["Chem13_MCIQ2"].ToString() != "" && (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MCIQ2"].ToString())))
                            dr["MCIPerformance"] = 2;
                        else if (dsQuartiles.Tables[0].Rows[0]["Chem13_MCIQ3"].ToString() != "" && (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Chem13_MCIQ3"].ToString())))
                            dr["MCIPerformance"] = 3;
                        else
                            dr["MCIPerformance"] = 4;
                    }

                    if (dr["PlantType"].ToString() == "Fuels" && dr["StudyYear"].ToString() == "2011")
                    {
                        if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels11_MEIQ1"].ToString()))
                            dr["RAMEIPerformance"] = 1;
                        else
                            dr["RAMEIPerformance"] = 2;

                        if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels11_MechAvailSlow_Q1"].ToString()))
                            dr["MAIPerformance"] = 1;
                        else 
                            dr["MAIPerformance"] = 2;

                        if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels11_MCIQ1"].ToString()))
                            dr["MCIPerformance"] = 1;
                        else
                            dr["MCIPerformance"] = 2;

                    }
                    if (dr["PlantType"].ToString() == "Fuels" && dr["StudyYear"].ToString() == "2012")
                    {
                        if (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels12_MEIQ1"].ToString()))
                            dr["RAMEIPerformance"] = 1;
                        else
                            dr["RAMEIPerformance"] = 2;

                        if (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels12_MechAvailSlow_Q1"].ToString()))
                            dr["MAIPerformance"] = 1;
                        else
                            dr["MAIPerformance"] = 2;

                        if (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels12_MCIQ1"].ToString()))
                            dr["MCIPerformance"] = 1;
                        else
                            dr["MCIPerformance"] = 2;
                    }
                    if (dr["PlantType"].ToString() == "Fuels" && dr["StudyYear"].ToString() == "2013")
                    {
                        if (dsQuartiles.Tables[0].Rows[0]["Fuels13_MEIQ1"].ToString() != "" && (Convert.ToDecimal(dr["MaintEffectIndex"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels13_MEIQ1"].ToString())))
                            dr["RAMEIPerformance"] = 1;
                        else
                            dr["RAMEIPerformance"] = 2;

                        if (dsQuartiles.Tables[0].Rows[0]["Fuels13_MechAvailSlow_Q1"].ToString() != "" && (Convert.ToDecimal(dr["LossPcntPRV_Total"].ToString()) >= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels13_MechAvailSlow_Q1"].ToString())))
                            dr["MAIPerformance"] = 1;
                        else
                            dr["MAIPerformance"] = 2;

                        if (dsQuartiles.Tables[0].Rows[0]["Fuels13_MCIQ1"].ToString() != "" && (Convert.ToDecimal(dr["TotMaintCost"].ToString()) <= Convert.ToDecimal(dsQuartiles.Tables[0].Rows[0]["Fuels13_MCIQ1"].ToString())))
                            dr["MCIPerformance"] = 1;
                        else
                            dr["MCIPerformance"] = 2;
                    }
                }
                Session["dvGlobal"] = dsSites.Tables[0];
                return dsSites;
            }
        }

        public void FillReport(string sort)
        {
            DataSet dsSites = GetReportDataSet();
            DataView dv = new DataView(dsSites.Tables[0]);
            dv.RowFilter = string.Format("RAMEIPerformance + '' LIKE '{0}' AND MAIPerformance + '' LIKE '{1}' AND MCIPerformance + '' LIKE '{2}' AND StudyYear + '' LIKE '{3}'", ddlRAMEI.SelectedItem.Value, ddlMAI.SelectedItem.Value, ddlMCI.SelectedItem.Value, ddlYear.SelectedItem.Value);

            if (sort != null)
                dv.Sort = (sort.ToString() + " " + ViewState["SortDirection"]);

            lblRecordCount.Text = "Displaying " + dv.Count.ToString() + " of " + dsSites.Tables[0].Rows.Count.ToString() + " records";
            
            lvODR.DataSource = dv;
            lvODR.DataBind();
        }

        public void FilterReport(string sort = null)
        {
            DataView dv = new DataView((DataTable)Session["dvGlobal"]);
            dv.RowFilter = string.Format("RAMEIPerformance + '' LIKE '{0}' AND MAIPerformance + '' LIKE '{1}' AND MCIPerformance + '' LIKE '{2}' AND StudyYear + '' LIKE '{3}'", ddlRAMEI.SelectedItem.Value, ddlMAI.SelectedItem.Value, ddlMCI.SelectedItem.Value, ddlYear.SelectedItem.Value);

            if (sort != null)
                dv.Sort = (sort.ToString() + " " + ViewState["SortDirection"]);
            
            lblRecordCount.Text = "Displaying " + dv.Count.ToString() + " of " + dv.Table.Rows.Count.ToString() + " records";

            lvODR.DataSource = dv;
            lvODR.DataBind();
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillReport(null);
            FilterReport();
        }

        protected void ddlRAMEI_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillReport(null);
            FilterReport();
        }

        protected void ddlMAI_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillReport(null);
            FilterReport();
        }

        protected void ddlMCI_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillReport(null);
            FilterReport();
        }

        protected void lnkbtnSortCompanyID_Click(object sender, EventArgs e)
        {
            //FillReport("FacilityID");
            FilterReport("FacilityID");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
            
        }

        protected void lnkbtnSortStudyYear_Click(object sender, EventArgs e)
        {
            //FillReport("StudyYear");
            FilterReport("StudyYear");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortDatasetID_Click(object sender, EventArgs e)
        {
            //FillReport("DatasetID");
            FilterReport("DatasetID");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortFacilityName_Click(object sender, EventArgs e)
        {
            //FillReport("FacilityName");
            FilterReport("FacilityName");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortRAMEI_Click(object sender, EventArgs e)
        {
            //FillReport("RAMEIPerformance");
            FilterReport("RAMEIPerformance");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortMAI_Click(object sender, EventArgs e)
        {
            //FillReport("MAIPerformance");
            FilterReport("MAIPerformance");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortMCI_Click(object sender, EventArgs e)
        {
            //FillReport("MCIPerformance");
            FilterReport("MCIPerformance");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortTotalOpp_Click(object sender, EventArgs e)
        {
            //FillReport("TotalOpp");
            FilterReport("TotalOpp");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortWorstGap_Click(object sender, EventArgs e)
        {
            //FillReport("WorstGap");
            FilterReport("WorstGap");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        protected void lnkbtnSortBestGap_Click(object sender, EventArgs e)
        {
            //FillReport("BestGap");
            FilterReport("BestGap");
            string direction = "";
            if (ViewState["SortDirection"] != null)
                direction = ViewState["SortDirection"].ToString();

            if (direction == "DESC")
                direction = "ASC";
            else
                direction = "DESC";

            ViewState["SortDirection"] = direction;
        }

        

        public DataSet GetQuartiles()
        {
            //SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMDevConnection"].ToString());
            //using (conn)
            //{
            //    conn.Open();
            //    SqlCommand command = new SqlCommand("SELECT MaintEffectIndex FROM Output.SiteSummary WHERE GroupID = 'Fuels11S Quartiles' AND SubGroupID = 'MEI Q1'", conn);
            //    command.CommandType = CommandType.Text;
            //    SqlDataReader dr;
            //    dr = command.ExecuteReader();
            //    dr.Read();
            //    if (dr.HasRows)
            //        Fuels11_MEQ1 = Convert.ToDecimal(dr["MaintEffectIndex"].ToString());
            //    dr.Close();

            //    command.CommandText = "SELECT MaintEffectIndex FROM Output.SiteSummary WHERE GroupID = 'Fuels11S Quartiles' AND SubGroupID = 'MEI Q2'";
            //    dr = command.ExecuteReader();
            //    dr.Read();
            //    if (dr.HasRows)
            //        Fuels11_MEQ2 = Convert.ToDecimal(dr["MaintEffectIndex"].ToString());
            //    dr.Close();



            //}

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["RAMConnectionStringSQLClient"].ToString());
            SqlCommand command = new SqlCommand("[dbo].[GetQuartileValues]", conn);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaQuartiles = new SqlDataAdapter(command);
            DataSet dsQuartiles = new DataSet();
            using (conn)
            {
                sdaQuartiles.Fill(dsQuartiles);
            }
            return dsQuartiles;
        }


    }
}