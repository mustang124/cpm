﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Models;
using System.Linq;
using Microsoft.Reporting.WebForms;

namespace RAMAPP_Mvc3.Reports
{
    public partial class InputReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack) {
                decimal nm;
                using (var db = new RAMEntities())
                {
                    this.ReportViewer1.LocalReport.ReportEmbeddedResource = "Report1.rdlc";
                  
                    var companySID = SessionUtil.GetCompanySID();
                    var isAdmin = SessionUtil.IsAdmin();
                    var sitedata = from rpt in db.View_SitesAnsInSiteSurveyRpt
                                   where (isAdmin || db.SitePermissions.Any(sp => rpt.DatasetID == sp.DatasetID)) &&
                                         rpt.CompanySID == companySID
                                   select  rpt;



                    var unitdata = from rpt in db.View_UnitAnsInSiteSurveyRpt
                                   where (isAdmin || db.SitePermissions.Any(sp => rpt.SiteDatasetID == sp.DatasetID)) &&
                                         rpt.CompanySID == companySID
                                   select rpt;

                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("AnswersInSiteSection",
                                                                                        sitedata.ToList()));

                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("AnswersInUnitSection",
                                                                                       unitdata.ToList()));
                    //this.DataBind();
                }
            }
        }
    }
}