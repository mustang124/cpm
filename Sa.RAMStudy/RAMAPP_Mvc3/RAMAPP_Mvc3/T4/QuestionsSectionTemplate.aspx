﻿ 
 
  


<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UT_CHARModel>" %>
<%@ Import Namespace="System.Globalization" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<%//------------------------------------------------------
  // Date Created : 02/05/2015 15:30:28  
  //-----------------------------------------------------%>
	 <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
	 <script type="text/javascript">
	    $(document).ready(function () {
	        // Smart Wizard 	
	        $("#wizard_UT_CHAR").smartTab({ transitionEffect: 'fade', keyNavigation: false });

	       /* $('li .stepDesc small').each(function (index) {
	            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

	            $(this).text($(elementTitleId).text());

	        });*/
    
	    });
		</script>
	  <% Html.EnableClientValidation(); %>			   
	  <% using (Ajax.BeginForm("UT_CHAR",new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" })) { 
	     bool uomPref = true;
	  %>
	  	<div id="wizard_UT_CHAR" class="stContainer">
		    <ul class="tabContainer">
			<li><a href='#step-1'><label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br /><small>Recordable Injuries</small> </span></a></li><li><a href='#step-2'><label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br /><small>Unit Characteristics</small> </span></a></li><li><a href='#step-3'><label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br /><small>Unit Process Characteristics</small> </span></a></li><li><a href='#step-4'><label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br /><small>Rotating Equipment Data</small> </span></a></li><li><a href='#step-5'><label class='stepNumber'>5</label><span class='stepDesc'>Section 5<br /><small>Fixed Equipment Data</small> </span></a></li><li><a href='#step-6'><label class='stepNumber'>6</label><span class='stepDesc'>Section 6<br /><small>Instrument and Electrical Equipment Data</small> </span></a></li>
			</ul>
		
			    				  
				  
				  <div class="SubSectionTab"  id="step-1">
				   
					<p class="StepTitle"><a href="#" tooltip="An OSHA recordable injury is an occupational injury or illness that requires medical treatment more than simple first aid and must be reported. You must consider an injury or illness to meet the general recording criteria, and therefore to be recordable, if it results in any of the following: death, days away from work, restricted work or transfer to another job, medical treatment beyond first aid, or loss of consciousness. You must also consider a case to meet the general recording criteria if it involves a significant injury or illness diagnosed by a physician or other licensed health care professional, even if it does not result in death, days away from work, restricted work or job transfer, medical treatment beyond first aid, or loss of consciousness.">Recordable Injuries</a></p>
					<p><i>Please indicate the number of <a href="#" tooltip="An OSHA recordable injury is an occupational injury or illness that requires medical treatment more than simple first aid and must be reported. You must consider an injury or illness to meet the general recording criteria, and therefore to be recordable, if it results in any of the following: death, days away from work, restricted work or transfer to another job, medical treatment beyond first aid, or loss of consciousness. You must also consider a case to meet the general recording criteria if it involves a significant injury or illness diagnosed by a physician or other licensed health care professional, even if it does not result in death, days away from work, restricted work or job transfer, medical treatment beyond first aid, or loss of consciousness.">recordable injuries</a> in the maintenance function that occurred while performing <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">routine maintenance</a> for the year reported excluding injuries that occurred during <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A's</a>.  Include the following as recordable incidents: workplace-related death, workplace injury resulting in time lost from work, and work-related injuries or illnesses requiring a physician's care.</i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_UCH_110.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_110.1' /></span> <span class='numbering'>1.</span> Company
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitSafetyCurrInjuryCntComp_RT,"IntTmpl",new {size="10" ,CalcTag="sumUTINJ" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.UnitSafetyCurrInjuryCntComp_RT)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Count </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_110.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_110.2' /></span> <span class='numbering'>2.</span> Contractor
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitSafetyCurrInjuryCntCont_RT,"IntTmpl",new {size="10" ,CalcTag="sumUTINJ" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.UnitSafetyCurrInjuryCntCont_RT)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Count </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_110.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_110.3' /></span> Total
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitSafetyCurrInjuryCntTot_RT,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.UnitSafetyCurrInjuryCntTot_RT)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  				  
				  </div>
				  <div class="SubSectionTab"  id="step-2">
				   
					<p class="StepTitle">Unit Characteristics</p>
					<p><i></i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_UCH_120.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_120.1' /></span> <span class='numbering'>1.</span> <a href="#" tooltip="Plant age is the number of years since the plant was commissioned. If the plant has been modified extensively, the age is adjusted down pro rata to the investment date. For example, if the plant was doubled in size, the new age would be the average of the original age and the age of the investment. However, if the original unit has been modified or expanded at a cost exceeding 50% of the replacement value, the date of the startup after the expansion would be used to determine the unit age.">Effective age</a> of the process unit?
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitInfoPlantAge,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.UnitInfoPlantAge)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Years </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_120.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_120.2' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="The plant replacement value (PRV) is the original gross asset value of the plant facilities for which maintenance costs are reported, adjusted by the country’s industry sector engineering plant cost index change between the original construction date and the current date. The plant replacement value includes capitalized additions and excludes removed or demolished assets. The plant replacement values of leased assets that are maintained by the company are included. Included in the value are the value of improvements to the land upon which these assets are located and Capitalized Engineering costs. Excluded from the value are value of the land upon which the assets are located, Spare Parts and capitalized interest. Also known as asset replacement value (ARV), replacement asset value (RAV), or estimated replacement value (ERV).">Plant replacement value (PRV)</a> of the process unit?
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitInfoPRV,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0"})%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Local Currency </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_120.2USD">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_120.2USD' /></span> <a href="#" tooltip="The plant replacement value (PRV) is the original gross asset value of the plant facilities for which maintenance costs are reported, adjusted by the country’s industry sector engineering plant cost index change between the original construction date and the current date. The plant replacement value includes capitalized additions and excludes removed or demolished assets. The plant replacement values of leased assets that are maintained by the company are included. Included in the value are the value of improvements to the land upon which these assets are located and Capitalized Engineering costs. Excluded from the value are value of the land upon which the assets are located, Spare Parts and capitalized interest. Also known as asset replacement value (ARV), replacement asset value (RAV), or estimated replacement value (ERV).">Plant replacement value (PRV)</a> of the process unit in US$
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitInfoPRVUSD,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.UnitInfoPRVUSD)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > US$ </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_120.3UOM">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_120.3UOM' /></span> <span class='numbering'>3.</span> Unit of measure for <a href="#" tooltip="The maximum average daily production rate attained for the predominant feed and product composition under normal operating conditions sustained over the best 30-day demonstrated period.">Maximum Daily Production Rate</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<select id="UnitInfoProdRateUOM" name="UnitInfoProdRateUOM"  width="30px"><option value='-1'>SELECT ONE</option> <option value='LB/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("LB/D")? "SELECTED" : String.Empty):String.Empty %> >Pounds/Day</option> <option value='MT/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("MT/D")? "SELECTED" : String.Empty):String.Empty %> >Metric Tons/Day</option> <option value='ST/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("ST/D")? "SELECTED" : String.Empty):String.Empty %> >Short Tons/Day</option> <option value='LT/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("LT/D")? "SELECTED" : String.Empty):String.Empty %> >Long Tons/Day</option> <option value='B/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("B/D")? "SELECTED" : String.Empty):String.Empty %> >Barrels/Day</option> <option value='M3/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("M3/D")? "SELECTED" : String.Empty):String.Empty %> >Cubic Meters/Day</option> <option value='KSCF/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("KSCF/D")? "SELECTED" : String.Empty):String.Empty %> >k SCF/Day</option> <option value='KNM3/D' <%= Model.UnitInfoProdRateUOM != null ?(Model.UnitInfoProdRateUOM.Equals("KNM3/D")? "SELECTED" : String.Empty):String.Empty %> >k normal cubic meters/Day</option> </select> <%: Html.ValidationMessageFor(model => model.UnitInfoProdRateUOM)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_120.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_120.3' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="The maximum average daily production rate attained for the predominant feed and product composition under normal operating conditions sustained over the best 30-day demonstrated period.">Maximum daily production rate</a> based on best demonstrated 30-day period?
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitInfoMaxDailyProdRate,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.UnitInfoMaxDailyProdRate)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_120.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_120.4' /></span> <span class='numbering'>5.</span> Is this a continuous or batch operation?
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Continuous</td> <td>Batch</td></tr><tr><td><%= Html.RadioButtonFor(model => model.UnitInfoContinuosOrBatch, "C") %></td> <td><%= Html.RadioButtonFor(model => model.UnitInfoContinuosOrBatch, "B") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.UnitInfoContinuosOrBatch)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_120.5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_120.5' /></span> <span class='numbering'>6.</span> Number of <a href="#" tooltip="Refining and chemical manufacturing processes are engineered as trains of equipment that together are used to manufacture the final product. Process units can be configured as single train operations whereby all process materials go through a single line of connected equipment. Or they can be configured as multiple trains whereby process materials can go through any one of a set of parallel trains to produce the final product. Generally speaking, a single train operation with large equipment can have the same capacity as a multiple train operation with smaller equipment.">process trains</a> (single-train versus multiple-train facilities)?
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.UnitInfoTrainCnt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.UnitInfoTrainCnt)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  style="display: none;"  class="questionblock" qid="QSTID_UCH_ExchRate">
			 				
				
														<%= Html.HiddenFor(model => model._UnitInfoExchangeRate, new {id="_UnitInfoExchangeRate"}) %>
								
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  				  
				  </div>
				  <div class="SubSectionTab"  id="step-3">
				   
					<p class="StepTitle">Unit Process Characteristics</p>
					<p><i></i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.1' /></span> <span class='numbering'>1.</span> <a href="#" tooltip="The physical state of being gas, liquid, or solid.">Phase</a> of predominant feedstock
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Gas</td> <td>Liquid</td> <td>Solid</td></tr><tr><td><%= Html.RadioButtonFor(model => model.UnitInfoPhasePredominentFeedstock, "G") %></td> <td><%= Html.RadioButtonFor(model => model.UnitInfoPhasePredominentFeedstock, "L") %></td> <td><%= Html.RadioButtonFor(model => model.UnitInfoPhasePredominentFeedstock, "S") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.UnitInfoPhasePredominentFeedstock)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.2' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="The physical state of being gas, liquid, or solid.">Phase</a> of predominant product
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Gas</td> <td>Liquid</td> <td>Solid</td></tr><tr><td><%= Html.RadioButtonFor(model => model.UnitInfoPhasePredominentProduct, "G") %></td> <td><%= Html.RadioButtonFor(model => model.UnitInfoPhasePredominentProduct, "L") %></td> <td><%= Html.RadioButtonFor(model => model.UnitInfoPhasePredominentProduct, "S") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.UnitInfoPhasePredominentProduct)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.3' /></span> <span class='numbering'>3.</span> <a href="#" tooltip="Process chemicals’ principal toxicity is classified in three categories in the study: Low = harmless; Medium = may cause injury; and High = hazardous.">Toxicity</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityToxicity, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityToxicity, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityToxicity, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityToxicity)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.4' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="The predominant process pressure: Low = 150 psi or 10.5 kg/cm; Medium > 150 psi or 10.5 kg/cm and < 600 psi or 42.2 kg/cm; and High = 600 psi or 42.2 kg/cm.">Pressure</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityPressure, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityPressure, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityPressure, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityPressure)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.5' /></span> <span class='numbering'>5.</span> <a href="#" tooltip="Process principal temperature is classified in three categories in the study: Low = 40 °F (5 °C); Medium = 40–160 °F (5–70 °C); and High = 160 °F (70 °C).">Temperature</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityTemperature, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityTemperature, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityTemperature, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityTemperature)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.6">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.6' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="The probability that a material will combust and be consumed by fire: Low = non-flammable; Medium = moderately flammable; and High = highly flammable.">Flammability</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityFlammability, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityFlammability, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityFlammability, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityFlammability)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.7">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.7' /></span> <span class='numbering'>7.</span> <a href="#" tooltip="The tendency to undergo a violent or explosive decomposition resulting in catastrophic failure: Low = non-explosive, Medium = moderately explosive, and High = highly explosive">Explosivity</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityExplosivity, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityExplosivity, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityExplosivity, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityExplosivity)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.8">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.8' /></span> <span class='numbering'>8.</span> <a href="#" tooltip="The propensity to cause irreversible destruction of a surface or substance with which it comes into contact through the act of oxidation or corrosion often associated with a strong acid or base.">Corrosivity</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityCorrosivity, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityCorrosivity, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityCorrosivity, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityCorrosivity)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_130.9">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_130.9' /></span> <span class='numbering'>9.</span> <a href="#" tooltip="The propensity to cause irreversible destruction of a surface or substance with which it comes into contact through the act of abrading or eroding, most often associated with granular materials.">Erosivity</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityErosivity, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityErosivity, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityErosivity, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityErosivity)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_131">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_131' /></span> <span class='numbering'>10.</span> <a href="#" tooltip="Process complexity is classified into the following categories: Low = easy to control with little or no heat recovery or other process interactivity; Medium = moderately difficult to control with moderate heat recovery and process interactivity; High = very tight control parameters with multiple heat recovery and process interactivity.">Process Complexity</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityProcessComplexity, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityProcessComplexity, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityProcessComplexity, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityProcessComplexity)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_131.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_131.1' /></span> <span class='numbering'>11.</span> <a href="#" tooltip="A measure of a fluid’s resistance to flow: Low = flows easily; Medium = flows moderately well with some resistance; and High = exhibit a high resistance to flow.">Viscosity</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityViscosity, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityViscosity, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityViscosity, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityViscosity)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_131.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_131.2' /></span> <span class='numbering'>12.</span> <a href="#" tooltip="The probability that a material will freeze or solidify due to a drop in temperature: Low < 32 °F (0 °C); Medium = 32–212 °F (0–100 °C); and High > 212 °F (100 °C).">Freeze Point</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SeverityFreezePt, "L") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityFreezePt, "M") %></td> <td><%= Html.RadioButtonFor(model => model.SeverityFreezePt, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SeverityFreezePt)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_131.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_131.3' /></span> <span class='numbering'>13.</span> Primary <a href="#" tooltip="The predominate material from which the process equipment is made.">material of construction</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<select id="SeverityConstMaterials" name="SeverityConstMaterials"  width="30px"><option value='-1'>SELECT ONE</option> <option value='CS' <%= Model.SeverityConstMaterials != null ?(Model.SeverityConstMaterials.Equals("CS")? "SELECTED" : String.Empty):String.Empty %> >Carbon steel</option> <option value='SS' <%= Model.SeverityConstMaterials != null ?(Model.SeverityConstMaterials.Equals("SS")? "SELECTED" : String.Empty):String.Empty %> >Stainless steel</option> <option value='A' <%= Model.SeverityConstMaterials != null ?(Model.SeverityConstMaterials.Equals("A")? "SELECTED" : String.Empty):String.Empty %> >Alloys</option> <option value='P' <%= Model.SeverityConstMaterials != null ?(Model.SeverityConstMaterials.Equals("P")? "SELECTED" : String.Empty):String.Empty %> >Plastic</option> <option value='FG' <%= Model.SeverityConstMaterials != null ?(Model.SeverityConstMaterials.Equals("FG")? "SELECTED" : String.Empty):String.Empty %> >Fiberglass</option> <option value='Oth' <%= Model.SeverityConstMaterials != null ?(Model.SeverityConstMaterials.Equals("Oth")? "SELECTED" : String.Empty):String.Empty %> >Other</option> </select> <%: Html.ValidationMessageFor(model => model.SeverityConstMaterials)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  				  
				  </div>
				  <div class="SubSectionTab"  id="step-4">
				   
					<p class="StepTitle"><a href="#" tooltip="Equipment that turns on an axis and is designed to impart kinetic energy on a process material. Common examples include centrifugal pumps and compressors, electric motors, steam turbines, etc. Rotating equipment craft include machinery mechanics such as millwrights and rotating equipment condition monitoring technicians. Include process equipment and exclude non-process equipment.">Rotating Equipment</a> Data</p>
					<p><i>What are the equipment types and counts for the process unit?</i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.1' /></span> <span class='numbering'>1.</span> <a href="#" tooltip="A rotary engine that extracts energy from a fluid flow and converts it into useful work. Most common are steam and gas turbines installed to drive process and ancillary equipment.">Turbines</a> (Main only)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntTurbines,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntTurbines)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_141.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_141.1' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="A rotary engine that extracts energy from a fluid flow and converts it into useful work. Most common are steam and gas turbines installed to drive process and ancillary equipment.">Turbines</a> (<a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntTurbinesSpares,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntTurbinesSpares)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.2' /></span> <span class='numbering'>3.</span> <a href="#" tooltip="Reciprocating compressors generally include a piston that moves forward and backward alternately within a cylinder to compress a fluid and increase pressure. Reciprocating compressors integrated into other equipment are excluded.">Compressors – Reciprocating</a> (Main)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntCompressorsRecip,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntCompressorsRecip)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_141.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_141.2' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="Reciprocating compressors generally include a piston that moves forward and backward alternately within a cylinder to compress a fluid and increase pressure. Reciprocating compressors integrated into other equipment are excluded.">Compressors – Reciprocating</a> (<a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntCompressorsRecipSpares,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntCompressorsRecipSpares)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.3' /></span> <span class='numbering'>5.</span> <a href="#" tooltip="Rotating compressors (sometimes referred to as centrifugal, axial, or radial compressors) turn on an axis to impart kinetic energy and/or velocity to a continuous flowing fluid through a rotor or impeller. Rotating compressors integrated into other equipment are excluded.">Compressors – Rotating</a> (Main)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntCompressorsRotating,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntCompressorsRotating)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_141.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_141.3' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="Rotating compressors (sometimes referred to as centrifugal, axial, or radial compressors) turn on an axis to impart kinetic energy and/or velocity to a continuous flowing fluid through a rotor or impeller. Rotating compressors integrated into other equipment are excluded.">Compressors – Rotating</a> (<a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntCompressorsRotatingSpares,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntCompressorsRotatingSpares)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.4' /></span> <span class='numbering'>7.</span> <a href="#" tooltip="A centrifugal pump uses centrifugal force to move a fluid through the use of an impeller that imparts kinetic energy into the fluid. Most common in the process industry is the horizontal centrifugal pump. Low-horsepower pumps are used primarily for injection, lubrication, analyzers, and similar services that are excluded from the equipment count.">Process Pumps – Centrifugal</a> (Main)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntPumpsCentrifugal,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntPumpsCentrifugal)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_141.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_141.4' /></span> <span class='numbering'>8.</span> <a href="#" tooltip="A centrifugal pump uses centrifugal force to move a fluid through the use of an impeller that imparts kinetic energy into the fluid. Most common in the process industry is the horizontal centrifugal pump. Low-horsepower pumps are used primarily for injection, lubrication, analyzers, and similar services that are excluded from the equipment count.">Process Pumps – Centrifugal</a> (<a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntPumpsCentrifugalSpares,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntPumpsCentrifugalSpares)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.5' /></span> <span class='numbering'>9.</span> <a href="#" tooltip="A positive displacement pump causes a fluid to move by trapping a fixed amount of it then forcing (displacing) that trapped volume into the discharge pipe. Common examples include rotary, diaphragm, gear and reciprocating pumps. Smaller pumps that are part of auxiliary systems such as process analyzers, lube oil circuits on compressors, or additive pumps should not be included in the count. See Motors.">Process Pumps – Positive Displacement</a> (Main and <a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntPumpsPosDisp,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntPumpsPosDisp)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.6">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.6' /></span> <span class='numbering'>10.</span> <a href="#" tooltip="A blower is an air moving device that can be either positive displacement or rotating impeller with a straight lobe, screw, sliding vane, and similar configuration. A fan is an air moving device with rotating blades or vanes. Blowers or fans that are incorporated as an integral part of equipment such as dryers and compressors are excluded; however, fin fans are included.">Blowers/Fans</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntBlowersFans,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntBlowersFans)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.7">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.7' /></span> <span class='numbering'>11.</span> Rotary Dryers/Rotary <a href="#" tooltip="A vessel designed to filter process media. Examples include cartridge-type filters with elements or sand filters, both of which are housed in pressure vessels and would be classified as such. Additionally, strainers that are engineered within pressure vessels such as duplex strainers would be included. Strainers used for general duty in the suction of pumps and installed between flanges such as cone or basket types would not be included.">Filters</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntRotaryDryers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntRotaryDryers)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.8">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.8' /></span> <span class='numbering'>12.</span> <a href="#" tooltip="Industrial equipment designed to reduce the particle size of a material or to create objects of a fixed cross sectional profile.">Crushers/Mills/Rotating Drums/Extruders</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntCrushers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntCrushers)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_140.9">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_140.9' /></span> <span class='numbering'>13.</span> <a href="#" tooltip="A rotating piece of process equipment that spins around a fixed axis and is designed to apply a force perpendicular to the axis to separate more dense components along the radial direction. A centrifuge is typically used to separate solids from liquids.">Process Centrifuges</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntCentrifugesMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntCentrifugesMain)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_141">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_141' /></span> <span class='numbering'>14.</span> <a href="#" tooltip="Motorized agitators, blenders, and mixers typically in storage tanks and process vessels designed to blend or mix materials. This category includes in-line mixers.">Agitators/Blenders/Mixers</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntAgitatorsInVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntAgitatorsInVessels)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  				  
				  </div>
				  <div class="SubSectionTab"  id="step-5">
				   
					<p class="StepTitle"><a href="#" tooltip="Process equipment that is fixed versus rotating equipment, reciprocating equipment, or other moving equipment. Examples of fixed equipment include pipe, heat exchangers, process vessels, distillation towers, tanks, etc. Fixed equipment craft workers include pipefitters, welders, boilermakers, carpenters, painters, equipment operators, insulators, riggers, and other civil crafts. Include process equipment and exclude non-process equipment.">Fixed Equipment</a> Data</p>
					<p><i>What are the equipment types and counts for the process unit?</i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.1' /></span> <span class='numbering'>1.</span> <a href="#" tooltip="A fired heat induction device used to transfer heat into water or a process fluid. This typically involves a fired chamber (i.e., boiler) with tubes containing the fluid to be heated.">Fired Furnaces/Boilers</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntFurnaceBoilers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntFurnaceBoilers)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.2' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="Equipment used to complete a process such as a chemical reaction in a reactor.">Process Vessels</a> (e.g., Reactors)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntVessels)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.3' /></span> <span class='numbering'>3.</span> <a href="#" tooltip="Columnar equipment used to separate liquid mixtures based on the differences of boiling point. Includes both vacuum and atmospheric units that typically have internal trays or related separation devices.">Distillation Towers</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntDistTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntDistTowers)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.4' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="Heat removal device used to transfer process waste heat to the atmosphere through the cooling and evaporation of water cascading down inside a tower with a large fan blowing ambient air through the tower. Each cooling tower cell counts as one piece of equipment. For those cases where the cooling tower has multiple cells that support different production units, each cell would be counted in the equipment count for the unit it supports. Cooling towers located outside the battery limits of the production unit should be included with unit equipment.">Cooling Towers</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntCoolingTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntCoolingTowers)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.5' /></span> <span class='numbering'>5.</span> <a href="#" tooltip="All raw material, intermediate, or finished product storage tanks inside the process unit battery limits (ISBL). Storage tanks in the tank farm and/or outside the unit battery limits are included. Smaller chemical storage tanks used for chemical injections and additives are not included.">Storage Tanks</a> (ISBL)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntStorageTanksISBL,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntStorageTanksISBL)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.6">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.6' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="Heat exchange devices comprised of a shell and tubes with fluids of differing temperatures flowing through each with the intention of transferring the heat energy from one fluid to another. Heat exchangers that are part of other equipment such as refrigeration units and boilers are excluded.">Heat Exchangers – Shell and Tube</a> (Main)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntHeatExch,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntHeatExch)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_141.5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_141.5' /></span> <span class='numbering'>7.</span> <a href="#" tooltip="Heat exchange devices comprised of a shell and tubes with fluids of differing temperatures flowing through each with the intention of transferring the heat energy from one fluid to another. Heat exchangers that are part of other equipment such as refrigeration units and boilers are excluded.">Heat Exchangers – Shell and Tube</a> (<a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntHeatExchSpares,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntHeatExchSpares)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.7">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.7' /></span> <span class='numbering'>8.</span> <a href="#" tooltip="Heat exchange devices other than shell and tube heat exchangers. Examples include plate and frame, double pipe, compact, spiral and other non-shell and tube type heat removal or recovery exchangers. Heat exchangers that are part of other equipment such as refrigeration units and boilers are excluded.">Heat Exchangers – Other</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntHeatExchOth,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntHeatExchOth)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.8">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.8' /></span> <span class='numbering'>9.</span> <a href="#" tooltip="A heat removal device used to transfer process waste heat directly to the atmosphere by use of large fans that blow ambient air over a heat transfer surface (usually tubes with heat radiation fins) containing the fluid to be heated or cooled.">Fin Fan Heat Exchangers</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntHeatExchFinFan,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntHeatExchFinFan)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_150.9">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_150.9' /></span> <span class='numbering'>10.</span> <a href="#" tooltip="All process refrigeration and chiller units excluding air conditioning units.">Process Refrigeration/Chiller</a> Units
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntRefrigUnits,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntRefrigUnits)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_151">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_151' /></span> <span class='numbering'>11.</span> <a href="#" tooltip="An industrial storage vessel typically used to store solid granular material. May be elevated or recessed into the ground with a conical or similarly shaped bottom to funnel materials for transfer.">Silos/Bins</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntSilosISBL,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntSilosISBL)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_151.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_151.1' /></span> <span class='numbering'>12.</span> <a href="#" tooltip="A pressure safety valve installed in pressurized systems designed to relieve pressure and avoid a catastrophic failure. Exclude rupture diaphragms and disks, and safety valves installed on specific equipment items such as turbines, boilers, etc.">Process Pressure Relief/Safety Valves</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntSafetyValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntSafetyValves)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  				  
				  </div>
				  <div class="SubSectionTab"  id="step-6">
				   
					<p class="StepTitle">Instrument and Electrical Equipment Data</p>
					<p><i>What are the equipment types and counts for the process unit?</i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.1' /></span> <span class='numbering'>1.</span> <a href="#" tooltip="Electrical motors that drive process equipment such as pumps and compressors. Small motors that are part of auxiliary systems such as process analyzers, lube oil circuits, or additive pumps are not included. See Pumps.">Motors</a> (Excludes Motors for VSD's)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntMotorsMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntMotorsMain)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.2' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="An electric motor with the capability of varying the speed by controlling the frequency of the electrical power supply.">Variable Speed Drives</a> (Includes <a href="#" tooltip="Electrical motors that drive process equipment such as pumps and compressors. Small motors that are part of auxiliary systems such as process analyzers, lube oil circuits, or additive pumps are not included. See Pumps.">Motor</a>)
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntVarSpeedDrives,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntVarSpeedDrives)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.3' /></span> <span class='numbering'>3.</span> Substation Transformers/Capacitors
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntISBLSubStationTransformer,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntISBLSubStationTransformer)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.4' /></span> <span class='numbering'>4.</span> <a href="#" tooltip="The electrical voltage distributed to electrically energized equipment through a combination of electrical disconnects such circuit breakers and fuses: Low = 5 kV, Medium >5 kV and = 15 kV, and High > 15 kV.">Distribution Voltage to Switchgear</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Low</td> <td>Medium</td> <td>High</td></tr><tr><td><%= Html.RadioButtonFor(model => model.EquipCntDistVoltage, "L") %></td> <td><%= Html.RadioButtonFor(model => model.EquipCntDistVoltage, "M") %></td> <td><%= Html.RadioButtonFor(model => model.EquipCntDistVoltage, "H") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.EquipCntDistVoltage)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.5' /></span> <span class='numbering'>5.</span> <a href="#" tooltip="A valve integrated into a process control loop used to control flows within the process unit. Include automated block valves with a positioner. Do not include control valves that are integrated into other equipment (e.g., boilers). Include automated block valves with a positioner and automatic shut-off valves.">Control Valves</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntControlValves)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.6">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.6' /></span> <span class='numbering'>6.</span> <a href="#" tooltip="Analyzers used to automatically adjust the process either as an element of a closed loop control scheme or by reporting analysis data to an advanced control algorithm.">On-Stream Analyzers – Process Control</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntAnalyzerProcCtrl)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.7">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.7' /></span> <span class='numbering'>7.</span> <a href="#" tooltip="Analyzers used to measure blending operations and used to adjust the process.">On-Stream Analyzers – Blending</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntAnalyzerBlending,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntAnalyzerBlending)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_UCH_160.8">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.8' /></span> <span class='numbering'>8.</span> <a href="#" tooltip="Analyzers used in a Continuous Emissions Monitoring System (CEMS) to meet environmental regulations for byproducts such as SO2 and NOX.">On-Stream Analyzers – Continuous Emissions Monitoring</a>
					 					 <span><img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.EquipCntAnalyzerEmissions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2"})%> <%: Html.ValidationMessageFor(model => model.EquipCntAnalyzerEmissions)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  style="display: none;"  class="questionblock" qid="QSTID_UCH_160.9X">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UCH_160.9X' /></span> <a href="#" tooltip="A form of process control designed to maintain a safe state of operation when hazardous process conditions are encountered.">Safety Instrumented Systems</a>
					 				
				
														<%= Html.HiddenFor(model => model.EquipCntSafetyInstrSys, new {id="EquipCntSafetyInstrSys"}) %>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			    <%: Html.HiddenFor(model=>model.CompanySID) %>
  </div> <!-- for last step --> 
  </div>  <!-- for wizard container -->
<%
} //for form using clause 
%>
 


<script type="text/javascript">  function loadHelpReferences(){var  objHelpRefs = Section_HelpLst;for (i = objHelpRefs.length -1; i >= 0; i--) {      var qidNm = objHelpRefs[i]['HelpID'];      if ($('li[qid^="' + qidNm + '"]').length) {        var $elm = $('li[qid^="' + qidNm + '"]');        $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');        $elm.find('img.tooltipBtn').css('visibility', 'visible');     }     else if ($('div[qid^="' + qidNm + '"]').length) {       $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');    }}} function formatResult(el,value){ var format='N'; if(el.attr('NumFormat')=='Int' )  format= 'n0'; if (el.attr('DecPlaces') != null) {      if (el.attr('NumFormat') == 'Number')          format = 'n' + el.attr('DecPlaces');}   if(value!=undefined && isNaN(value)) value = 0 ;    var tempval = Globalize.parseFloat(value.toString(),'en');  if(isFinite(tempval) ) { el.val(Globalize.format(tempval, format));    var idnm = '#_' + el.attr('id');     var $f= $(idnm);      if ($f.length){$f.val(el.val());}}else{ el.val('');}  }$("#UnitInfoPRV,#_UnitInfoExchangeRate ").bind("keyup",function(){var $result =$("#UnitInfoPRVUSD");$result.calc("v1/v2", { v1: $("#UnitInfoPRV"), v2:$("#_UnitInfoExchangeRate")}); formatResult($result, $result.val()); /*$("#UnitInfoPRVUSD").trigger('blur');*/      });if ($("#UnitInfoPRV").val()!= null && !isNaN($("#UnitInfoPRV").val()) )if ($("#_UnitInfoExchangeRate").val()!= null && !isNaN($("#_UnitInfoExchangeRate").val()) ){$("#UnitInfoPRV").trigger('keyup');}$("input[CalcTag*='sumUTINJ']").sum({bind: "keyup", selector: "#UnitSafetyCurrInjuryCntTot_RT",oncalc: function(value,settings){  if(value != undefined && !isNaN(value)) { formatResult($(settings.selector),value); /*$(settings.selector).trigger('blur');*/  }}});$('#UnitSafetyCurrInjuryCntTot_RT').attr('readonly','readonly');$('#UnitSafetyCurrInjuryCntTot_RT').change(function(){$(this).validate();});var secComments =  Section_Comments;var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';for(i = secComments.length-1;i >=0; i-- ){		var qidNm = secComments[i]['CommentID'];		if($('img[qid="' + qidNm + '"]').length){    		$('img[qid="' + qidNm + '"]').attr('src', redCmtImg);		}	}<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(  function (index, html) {   var gridId = $(this).closest('.grid').attr('id');   if ($.trim(html).indexOf('nbsp', 0) == -1) {        var newHTML = '<span> ' +       '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' +       ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html +      '</span>';      }    $(this).html(newHTML);   }  ); $('img[suid]').click(function(){ var suid = $(this).attr('suid'); var qid = $(this).attr('qid') ; recordComment(suid,qid); });</script>

</asp:Content> 
