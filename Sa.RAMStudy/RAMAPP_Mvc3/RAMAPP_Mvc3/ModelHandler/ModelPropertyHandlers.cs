﻿



 
  

using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Entity;
using System.Globalization;
using System;





namespace  RAMAPP_Mvc3.ModelPropertyHandlers
{
 struct SqlResults
 { 
   public string retVal1;
   public string retVal2;
 }
	
		
    public  class ST_CHARModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_CHARModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_CHARModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_CHARModel>(qry, new object[]{datasetId,"ST_CHAR"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_CHAR",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_CHARModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_CHARModel
   	
	
	
	           public bool Save(ST_CHARModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_CHAR|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.ExchangeRate!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ExchangeRate',NULL," + m.ExchangeRate.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ExchangeRate',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LocalCurrency!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LocalCurrency',NULL,NULL,'"+ m.LocalCurrency +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LocalCurrency',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.PRVCalcMethod!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PRVCalcMethod',NULL,NULL,'"+ m.PRVCalcMethod +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PRVCalcMethod',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteAreaTotal!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteAreaTotal',NULL," + m.SiteAreaTotal.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteAreaTotal',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteAreaUnit!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteAreaUnit',NULL," + m.SiteAreaUnit.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteAreaUnit',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteAreaUOM!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteAreaUOM',NULL,NULL,'"+ m.SiteAreaUOM +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteAreaUOM',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteNumPers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteNumPers',NULL,NULL,'"+ m.SiteNumPers +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteNumPers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteNumPersInt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteNumPersInt'," + m.SiteNumPersInt + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteNumPersInt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePRV!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePRV',NULL," + m.SitePRV.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePRV',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePRVUSD!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePRVUSD',NULL," + m.SitePRVUSD.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePRVUSD',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSafetyCurrInjuryCntComp_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyCurrInjuryCntComp_RT'," + m.SiteSafetyCurrInjuryCntComp_RT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyCurrInjuryCntComp_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSafetyCurrInjuryCntCont_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyCurrInjuryCntCont_RT'," + m.SiteSafetyCurrInjuryCntCont_RT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyCurrInjuryCntCont_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSafetyCurrInjuryCntTot_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyCurrInjuryCntTot_RT'," + m.SiteSafetyCurrInjuryCntTot_RT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyCurrInjuryCntTot_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSafetyPreTaskAnalysis!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyPreTaskAnalysis',NULL,NULL,'"+ m.SiteSafetyPreTaskAnalysis +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyPreTaskAnalysis',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSafetyReturnToOperation!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyReturnToOperation',NULL,NULL,'"+ m.SiteSafetyReturnToOperation +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetyReturnToOperation',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSafetySafeWorkPermit!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetySafeWorkPermit',NULL,NULL,'"+ m.SiteSafetySafeWorkPermit +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSafetySafeWorkPermit',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyBadActorList!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyBadActorList',NULL,NULL,'"+ m.SiteStrategyBadActorList +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyBadActorList',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyEquipRelDocPcnt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyEquipRelDocPcnt',NULL," + m.SiteStrategyEquipRelDocPcnt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyEquipRelDocPcnt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyGoalsEngTech!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsEngTech',NULL,NULL,'"+ m.SiteStrategyGoalsEngTech +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsEngTech',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyGoalsProdStaff!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsProdStaff',NULL,NULL,'"+ m.SiteStrategyGoalsProdStaff +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsProdStaff',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyGoalsProdTechs!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsProdTechs',NULL,NULL,'"+ m.SiteStrategyGoalsProdTechs +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsProdTechs',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyGoalsRamCraft!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsRamCraft',NULL,NULL,'"+ m.SiteStrategyGoalsRamCraft +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsRamCraft',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyGoalsRamStaff!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsRamStaff',NULL,NULL,'"+ m.SiteStrategyGoalsRamStaff +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsRamStaff',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyGoalsSuppProc!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsSuppProc',NULL,NULL,'"+ m.SiteStrategyGoalsSuppProc +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyGoalsSuppProc',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyLCCA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyLCCA',NULL," + m.SiteStrategyLCCA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyLCCA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyMgrObjectives!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyMgrObjectives',NULL,NULL,'"+ m.SiteStrategyMgrObjectives +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyMgrObjectives',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyMOC!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyMOC',NULL,NULL,'"+ m.SiteStrategyMOC +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyMOC',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyBadActorPct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyBadActorPct',NULL,NULL,'" + m.SiteStrategyBadActorPct + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyBadActorPct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteStrategyNumKPI!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPI',NULL," + m.SiteStrategyNumKPI.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPI',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyNumKPIDaily!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPIDaily',NULL," + m.SiteStrategyNumKPIDaily.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPIDaily',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyNumKPIMonthly!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPIMonthly',NULL," + m.SiteStrategyNumKPIMonthly.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPIMonthly',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyNumKPIWeekly!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPIWeekly',NULL," + m.SiteStrategyNumKPIWeekly.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyNumKPIWeekly',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyObjectivesDoc!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyObjectivesDoc',NULL,NULL,'"+ m.SiteStrategyObjectivesDoc +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyObjectivesDoc',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyPerfIndicators!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPerfIndicators',NULL,NULL,'"+ m.SiteStrategyPerfIndicators +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPerfIndicators',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyPersonnelCert!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPersonnelCert',NULL,NULL,'"+ m.SiteStrategyPersonnelCert +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPersonnelCert',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyPlanDoc!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPlanDoc',NULL,NULL,'"+ m.SiteStrategyPlanDoc +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPlanDoc',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyPlanDocYrs!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPlanDocYrs',NULL," + m.SiteStrategyPlanDocYrs.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyPlanDocYrs',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteStrategyReliModeling!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyReliModeling',NULL," + m.SiteStrategyReliModeling.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteStrategyReliModeling',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_CHARModel
   	         
	} // class ST_CHARModelHandler 
	
		
    public  class ST_COSTModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_COSTModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_COSTModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_COSTModel>(qry, new object[]{datasetId,"ST_COST"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_COST",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_COSTModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_COSTModel
   	
	
	
	           public bool Save(ST_COSTModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_COST|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.SiteCostsCompLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompLabor_RT',NULL," + m.SiteCostsCompLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsCompLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompLabor_TA',NULL," + m.SiteCostsCompLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsCompMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompMatl_RT',NULL," + m.SiteCostsCompMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsCompMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompMatl_TA',NULL," + m.SiteCostsCompMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsCompMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsContLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContLabor_RT',NULL," + m.SiteCostsContLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsContLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContLabor_TA',NULL," + m.SiteCostsContLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsContMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContMatl_RT',NULL," + m.SiteCostsContMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsContMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContMatl_TA',NULL," + m.SiteCostsContMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsContMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsIndirectCompSupport_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectCompSupport_RT',NULL," + m.SiteCostsIndirectCompSupport_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectCompSupport_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsIndirectCompSupport_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectCompSupport_TA',NULL," + m.SiteCostsIndirectCompSupport_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectCompSupport_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsIndirectContSupport_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectContSupport_RT',NULL," + m.SiteCostsIndirectContSupport_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectContSupport_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsIndirectContSupport_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectContSupport_TA',NULL," + m.SiteCostsIndirectContSupport_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsIndirectContSupport_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotDirect_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotDirect_RT',NULL," + m.SiteCostsTotDirect_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotDirect_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotDirect_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotDirect_TA',NULL," + m.SiteCostsTotDirect_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotDirect_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotIndirect_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotIndirect_RT',NULL," + m.SiteCostsTotIndirect_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotIndirect_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotIndirect_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotIndirect_TA',NULL," + m.SiteCostsTotIndirect_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotIndirect_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotLabor_RT',NULL," + m.SiteCostsTotLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotLabor_TA',NULL," + m.SiteCostsTotLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotMaint_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMaint_RT',NULL," + m.SiteCostsTotMaint_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMaint_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotMaint_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMaint_TA',NULL," + m.SiteCostsTotMaint_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMaint_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMatl_RT',NULL," + m.SiteCostsTotMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCostsTotMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMatl_TA',NULL," + m.SiteCostsTotMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCostsTotMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpAvgWageRate_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRate_RT',NULL," + m.SiteExpAvgWageRate_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRate_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpAvgWageRate_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRate_TA',NULL," + m.SiteExpAvgWageRate_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRate_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpAvgWageRateUSD_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRateUSD_RT',NULL," + m.SiteExpAvgWageRateUSD_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRateUSD_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpAvgWageRateUSD_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRateUSD_TA',NULL," + m.SiteExpAvgWageRateUSD_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpAvgWageRateUSD_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompLabor_RT',NULL," + m.SiteExpCompLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompLabor_TA',NULL," + m.SiteExpCompLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompManHrs_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompManHrs_RT',NULL," + m.SiteExpCompManHrs_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompManHrs_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompManHrs_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompManHrs_TA',NULL," + m.SiteExpCompManHrs_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompManHrs_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompMatl_RT',NULL," + m.SiteExpCompMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompMatl_TA',NULL," + m.SiteExpCompMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompWageRate_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRate_RT',NULL," + m.SiteExpCompWageRate_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRate_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompWageRate_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRate_TA',NULL," + m.SiteExpCompWageRate_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRate_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompWageRateUSD_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRateUSD_RT',NULL," + m.SiteExpCompWageRateUSD_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRateUSD_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpCompWageRateUSD_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRateUSD_TA',NULL," + m.SiteExpCompWageRateUSD_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpCompWageRateUSD_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContLabor_RT',NULL," + m.SiteExpContLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContLabor_TA',NULL," + m.SiteExpContLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContManHrs_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContManHrs_RT',NULL," + m.SiteExpContManHrs_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContManHrs_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContManHrs_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContManHrs_TA',NULL," + m.SiteExpContManHrs_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContManHrs_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContMatl_RT',NULL," + m.SiteExpContMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContMatl_TA',NULL," + m.SiteExpContMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContWageRate_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRate_RT',NULL," + m.SiteExpContWageRate_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRate_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContWageRate_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRate_TA',NULL," + m.SiteExpContWageRate_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRate_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContWageRateUSD_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRateUSD_RT',NULL," + m.SiteExpContWageRateUSD_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRateUSD_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpContWageRateUSD_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRateUSD_TA',NULL," + m.SiteExpContWageRateUSD_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpContWageRateUSD_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpIndirectCompSupport_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectCompSupport_RT',NULL," + m.SiteExpIndirectCompSupport_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectCompSupport_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpIndirectCompSupport_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectCompSupport_TA',NULL," + m.SiteExpIndirectCompSupport_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectCompSupport_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpIndirectContSupport_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectContSupport_RT',NULL," + m.SiteExpIndirectContSupport_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectContSupport_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpIndirectContSupport_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectContSupport_TA',NULL," + m.SiteExpIndirectContSupport_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpIndirectContSupport_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotDirect_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotDirect_RT',NULL," + m.SiteExpTotDirect_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotDirect_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotDirect_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotDirect_TA',NULL," + m.SiteExpTotDirect_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotDirect_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotIndirect_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotIndirect_RT',NULL," + m.SiteExpTotIndirect_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotIndirect_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotIndirect_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotIndirect_TA',NULL," + m.SiteExpTotIndirect_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotIndirect_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotLabor_RT',NULL," + m.SiteExpTotLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotLabor_TA',NULL," + m.SiteExpTotLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotMaint_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMaint_RT',NULL," + m.SiteExpTotMaint_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMaint_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotMaint_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMaint_TA',NULL," + m.SiteExpTotMaint_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMaint_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotManHrs_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotManHrs_RT',NULL," + m.SiteExpTotManHrs_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotManHrs_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotManHrs_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotManHrs_TA',NULL," + m.SiteExpTotManHrs_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotManHrs_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMatl_RT',NULL," + m.SiteExpTotMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteExpTotMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMatl_TA',NULL," + m.SiteExpTotMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteExpTotMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlAvgWageRate_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRate_RT',NULL," + m.SiteMCptlAvgWageRate_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRate_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlAvgWageRate_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRate_TA',NULL," + m.SiteMCptlAvgWageRate_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRate_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlAvgWageRateUSD_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRateUSD_RT',NULL," + m.SiteMCptlAvgWageRateUSD_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRateUSD_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlAvgWageRateUSD_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRateUSD_TA',NULL," + m.SiteMCptlAvgWageRateUSD_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlAvgWageRateUSD_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompIndirectCost_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompIndirectCost_RT',NULL," + m.SiteMCptlCompIndirectCost_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompIndirectCost_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompIndirectCost_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompIndirectCost_TA',NULL," + m.SiteMCptlCompIndirectCost_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompIndirectCost_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompLabor_RT',NULL," + m.SiteMCptlCompLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompLabor_TA',NULL," + m.SiteMCptlCompLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompManHrs_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompManHrs_RT',NULL," + m.SiteMCptlCompManHrs_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompManHrs_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlMetricLabor_RT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricLabor_RT',NULL," + m.SiteMCptlMetricLabor_RT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricLabor_RT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteMCptlMetricManHrs_RT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricManHrs_RT',NULL," + m.SiteMCptlMetricManHrs_RT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricManHrs_RT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteMCptlMetricWageRate_RT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricWageRate_RT',NULL," + m.SiteMCptlMetricWageRate_RT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricWageRate_RT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteMCptlMetricWageRateUSD_RT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricWageRateUSD_RT',NULL," + m.SiteMCptlMetricWageRateUSD_RT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlMetricWageRateUSD_RT',NULL,NULL,NULL," + userNo + "; ");
}

if (m.SiteMCptlCompManHrs_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompManHrs_TA',NULL," + m.SiteMCptlCompManHrs_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompManHrs_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompMatl_RT',NULL," + m.SiteMCptlCompMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompMatl_TA',NULL," + m.SiteMCptlCompMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompWageRate_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRate_RT',NULL," + m.SiteMCptlCompWageRate_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRate_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompWageRate_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRate_TA',NULL," + m.SiteMCptlCompWageRate_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRate_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompWageRateUSD_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRateUSD_RT',NULL," + m.SiteMCptlCompWageRateUSD_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRateUSD_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlCompWageRateUSD_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRateUSD_TA',NULL," + m.SiteMCptlCompWageRateUSD_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlCompWageRateUSD_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContIndirectCost_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContIndirectCost_RT',NULL," + m.SiteMCptlContIndirectCost_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContIndirectCost_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContIndirectCost_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContIndirectCost_TA',NULL," + m.SiteMCptlContIndirectCost_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContIndirectCost_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContLabor_RT',NULL," + m.SiteMCptlContLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContLabor_TA',NULL," + m.SiteMCptlContLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContManHrs_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContManHrs_RT',NULL," + m.SiteMCptlContManHrs_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContManHrs_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContManHrs_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContManHrs_TA',NULL," + m.SiteMCptlContManHrs_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContManHrs_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContMatl_RT',NULL," + m.SiteMCptlContMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContMatl_TA',NULL," + m.SiteMCptlContMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContWageRate_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRate_RT',NULL," + m.SiteMCptlContWageRate_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRate_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContWageRate_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRate_TA',NULL," + m.SiteMCptlContWageRate_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRate_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContWageRateUSD_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRateUSD_RT',NULL," + m.SiteMCptlContWageRateUSD_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRateUSD_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlContWageRateUSD_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRateUSD_TA',NULL," + m.SiteMCptlContWageRateUSD_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlContWageRateUSD_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotCost_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotCost_RT',NULL," + m.SiteMCptlTotCost_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotCost_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotCost_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotCost_TA',NULL," + m.SiteMCptlTotCost_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotCost_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotDirectCost_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotDirectCost_RT',NULL," + m.SiteMCptlTotDirectCost_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotDirectCost_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotDirectCost_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotDirectCost_TA',NULL," + m.SiteMCptlTotDirectCost_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotDirectCost_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotIndirectCost_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotIndirectCost_RT',NULL," + m.SiteMCptlTotIndirectCost_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotIndirectCost_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotIndirectCost_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotIndirectCost_TA',NULL," + m.SiteMCptlTotIndirectCost_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotIndirectCost_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotLabor_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotLabor_RT',NULL," + m.SiteMCptlTotLabor_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotLabor_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotLabor_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotLabor_TA',NULL," + m.SiteMCptlTotLabor_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotLabor_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotManHrs_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotManHrs_RT',NULL," + m.SiteMCptlTotManHrs_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotManHrs_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotManHrs_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotManHrs_TA',NULL," + m.SiteMCptlTotManHrs_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotManHrs_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotMatl_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotMatl_RT',NULL," + m.SiteMCptlTotMatl_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotMatl_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlTotMatl_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotMatl_TA',NULL," + m.SiteMCptlTotMatl_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlTotMatl_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMCptlPctFTEContractors != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlPctFTEContractors',NULL," + m.SiteMCptlPctFTEContractors.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMCptlPctFTEContractors',NULL,NULL,NULL," + userNo + "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_COSTModel
   	         
	} // class ST_COSTModelHandler 
	
		
    public  class ST_CRAFTCHARModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_CRAFTCHARModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_CRAFTCHARModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_CRAFTCHARModel>(qry, new object[]{datasetId,"ST_CRAFTCHAR"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_CRAFTCHAR",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_CRAFTCHARModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_CRAFTCHARModel
   	
	
	
	           public bool Save(ST_CRAFTCHARModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_CRAFTCHAR|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
if (m.SiteWorkEnvirAvgTrainHrs_COFP!=null){
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_COFP',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_COFP + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_COFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_COIE!=null){
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_COIE',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_COIE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_COIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CORE!=null){
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CORE',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CORE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CORE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CRFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRFP',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CRFP + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CRIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRIE',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CRIE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CRRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRRE',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CRRE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_COFP!=null){
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_COFP',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_COFP + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_COFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_COIE!=null){
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_COIE',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_COIE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_COIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CORE!=null){
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CORE',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CORE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CORE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CRFP!=null){
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRFP',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CRFP + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CRIE!=null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRIE',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CRIE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CRRE!=null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRRE',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CRRE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirLaborUnion_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirLaborUnion_CO',NULL," + m.SiteWorkEnvirLaborUnion_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirLaborUnion_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirLaborUnion_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirLaborUnion_CR',NULL," + m.SiteWorkEnvirLaborUnion_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirLaborUnion_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirOTPcnt_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTPcnt_CO',NULL," + m.SiteWorkEnvirOTPcnt_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTPcnt_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirOTPcnt_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTPcnt_CR',NULL," + m.SiteWorkEnvirOTPcnt_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTPcnt_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirOTTarget_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTTarget_CO',NULL," + m.SiteWorkEnvirOTTarget_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTTarget_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirOTTarget_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTTarget_CR',NULL," + m.SiteWorkEnvirOTTarget_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirOTTarget_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirPlanTrainHrs_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_COFP',NULL," + m.SiteWorkEnvirPlanTrainHrs_COFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirPlanTrainHrs_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_COIE',NULL," + m.SiteWorkEnvirPlanTrainHrs_COIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirPlanTrainHrs_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CORE',NULL," + m.SiteWorkEnvirPlanTrainHrs_CORE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirPlanTrainHrs_CRFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CRFP',NULL," + m.SiteWorkEnvirPlanTrainHrs_CRFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CRFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirPlanTrainHrs_CRIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CRIE',NULL," + m.SiteWorkEnvirPlanTrainHrs_CRIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CRIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirPlanTrainHrs_CRRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CRRE',NULL," + m.SiteWorkEnvirPlanTrainHrs_CRRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_CRRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirRCARCFA_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CO',NULL," + m.SiteWorkEnvirRCARCFA_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirRCARCFA_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CR',NULL," + m.SiteWorkEnvirRCARCFA_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirRegsCert_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRegsCert_CO',NULL,NULL,'"+ m.SiteWorkEnvirRegsCert_CO +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRegsCert_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirRegsCert_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRegsCert_CR',NULL,NULL,'"+ m.SiteWorkEnvirRegsCert_CR +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRegsCert_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirReqDocFail_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirReqDocFail_CO',NULL,NULL,'"+ m.SiteWorkEnvirReqDocFail_CO +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirReqDocFail_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirReqDocFail_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirReqDocFail_CR',NULL,NULL,'"+ m.SiteWorkEnvirReqDocFail_CR +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirReqDocFail_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirSector_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_COFP',NULL," + m.SiteWorkEnvirSector_COFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirSector_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_COIE',NULL," + m.SiteWorkEnvirSector_COIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirSector_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CORE',NULL," + m.SiteWorkEnvirSector_CORE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CORE_Range != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CORE_Range',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CORE_Range + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CORE_Range',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CORE_Range != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CORE_Range',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CORE_Range + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CORE_Range',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CRRE_Range != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRRE_Range',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CRRE_Range + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRRE_Range',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CRRE_Range != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRRE_Range',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CRRE_Range + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRRE_Range',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirSector_COFP_Range != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_COFP_Range',NULL,NULL,'" + m.SiteWorkEnvirSector_COFP_Range + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_COFP_Range',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirPlanTrainHrs_COFP_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_COFP_Rg',NULL,NULL,'" + m.SiteWorkEnvirPlanTrainHrs_COFP_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirPlanTrainHrs_COFP_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CRFP_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRFP_Rg',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CRFP_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRFP_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CRFP_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRFP_Rg',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CRFP_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRFP_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_COIE_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_COIE_Rg',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_COIE_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_COIE_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_COIE_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_COIE_Rg',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_COIE_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_COIE_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirAvgTrainHrs_CRIE_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRIE_Rg',NULL,NULL,'" + m.SiteWorkEnvirAvgTrainHrs_CRIE_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirAvgTrainHrs_CRIE_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirExperInJob_CRIE_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRIE_Rg',NULL,NULL,'" + m.SiteWorkEnvirExperInJob_CRIE_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirExperInJob_CRIE_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARCFA_CO_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CO_Rg',NULL,NULL,'" + m.SiteWorkEnvirRCARCFA_CO_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CO_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARCFA_CR_Rg != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CR_Rg',NULL,NULL,'" + m.SiteWorkEnvirRCARCFA_CR_Rg + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCFA_CR_Rg',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARCPQ_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPQ_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARCPQ_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPQ_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARMWPO_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARMWPO_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARMWPO_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARMWPO_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARACTB_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARACTB_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARACTB_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARACTB_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARWTSP_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWTSP_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARWTSP_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWTSP_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARWPA_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWPA_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARWPA_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWPA_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARCPQ_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPQ_CR',NULL,NULL,'" + m.SiteWorkEnvirRCARCPQ_CR + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPQ_CR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARMWPO_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARMWPO_CR',NULL,NULL,'" + m.SiteWorkEnvirRCARMWPO_CR + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARMWPO_CR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARACTB_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARACTB_CR',NULL,NULL,'" + m.SiteWorkEnvirRCARACTB_CR + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARACTB_CR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARWTSP_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWTSP_CR',NULL,NULL,'" + m.SiteWorkEnvirRCARWTSP_CR + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWTSP_CR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARWPA_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWPA_CR',NULL,NULL,'" + m.SiteWorkEnvirRCARWPA_CR + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARWPA_CR',NULL,NULL,NULL," + userNo + "; ");
}

if (m.SiteWorkEnvirRCARCPA_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPA_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARCPA_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPA_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARCPA_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPA_CR'," + m.SiteWorkEnvirRCARCPA_CR + ",NULL,NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCPA_CR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARCWQA_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCWQA_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARCWQA_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCWQA_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARCWQA_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCWQA_CR'," + m.SiteWorkEnvirRCARCWQA_CR + ",NULL,NULL," +  userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARCWQA_CR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARRPRM_CO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARRPRM_CO',NULL,NULL,'" + m.SiteWorkEnvirRCARRPRM_CO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARRPRM_CO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirRCARRPRM_CR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARRPRM_CR'," + m.SiteWorkEnvirRCARRPRM_CR + ",NULL,NULL," + userNo + " ;");

}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirRCARRPRM_CR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkEnvirSector_CRFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CRFP',NULL," + m.SiteWorkEnvirSector_CRFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CRFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirSector_CRIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CRIE',NULL," + m.SiteWorkEnvirSector_CRIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CRIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkEnvirSector_CRRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CRRE',NULL," + m.SiteWorkEnvirSector_CRRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkEnvirSector_CRRE',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_CRAFTCHARModel
   	         
	} // class ST_CRAFTCHARModelHandler 
	
		
    public  class ST_IDENTModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_IDENTModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_IDENTModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_IDENTModel>(qry, new object[]{datasetId,"ST_IDENT"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_IDENT",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_IDENTModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_IDENTModel
   	
	
	
	           public bool Save(ST_IDENTModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_IDENT|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.CompanyName!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CompanyName',NULL,NULL,'"+ m.CompanyName +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CompanyName',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MailAddr1!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MailAddr1',NULL,NULL,'"+ m.MailAddr1 +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MailAddr1',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MaintMgr!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MaintMgr',NULL,NULL,'"+ m.MaintMgr +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MaintMgr',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MaintMgrEmail!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MaintMgrEmail',NULL,NULL,'"+ m.MaintMgrEmail +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MaintMgrEmail',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteCountry!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCountry',NULL,NULL,'"+ m.SiteCountry +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteCountry',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMgr!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMgr',NULL,NULL,'"+ m.SiteMgr +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMgr',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteMgrEmail!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMgrEmail',NULL,NULL,'"+ m.SiteMgrEmail +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteMgrEmail',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteName!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteName',NULL,NULL,'"+ m.SiteName +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteName',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StreetAddr1!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StreetAddr1',NULL,NULL,'"+ m.StreetAddr1 +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StreetAddr1',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_IDENTModel
   	         
	} // class ST_IDENTModelHandler 
	
		
    public  class ST_INVENModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_INVENModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_INVENModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_INVENModel>(qry, new object[]{datasetId,"ST_INVEN"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_INVEN",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_INVENModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_INVENModel
   	
	
	
	           public bool Save(ST_INVENModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_INVEN|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.SiteSparesCatalogLineItemCnt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesCatalogLineItemCnt',NULL," + m.SiteSparesCatalogLineItemCnt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesCatalogLineItemCnt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSparesCosignOrVendorPcnt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesCosignOrVendorPcnt',NULL," + m.SiteSparesCosignOrVendorPcnt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesCosignOrVendorPcnt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSparesInventory!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesInventory',NULL," + m.SiteSparesInventory.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesInventory',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSparesInventoryUSD!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesInventoryUSD',NULL," + m.SiteSparesInventoryUSD.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesInventoryUSD',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSparesPlantStoresIssues!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesPlantStoresIssues',NULL," + m.SiteSparesPlantStoresIssues.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesPlantStoresIssues',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteSparesPlantStoresIssuesUSD!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesPlantStoresIssuesUSD',NULL," + m.SiteSparesPlantStoresIssuesUSD.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteSparesPlantStoresIssuesUSD',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgAvgStockOutPcnt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgAvgStockOutPcnt',NULL," + m.SiteWhsOrgAvgStockOutPcnt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgAvgStockOutPcnt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgClosedStoredPolicy!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgClosedStoredPolicy',NULL,NULL,'"+ m.SiteWhsOrgClosedStoredPolicy +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgClosedStoredPolicy',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgLCCAStrRmItems!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgLCCAStrRmItems',NULL,NULL,'"+ m.SiteWhsOrgLCCAStrRmItems +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgLCCAStrRmItems',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgLowCostFreeIssue!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgLowCostFreeIssue',NULL,NULL,'"+ m.SiteWhsOrgLowCostFreeIssue +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgLowCostFreeIssue',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgOpCost!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgOpCost',NULL," + m.SiteWhsOrgOpCost.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgOpCost',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgOpCostUSD!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgOpCostUSD',NULL," + m.SiteWhsOrgOpCostUSD.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgOpCostUSD',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgOutsourced!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgOutsourced',NULL,NULL,'"+ m.SiteWhsOrgOutsourced +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgOutsourced',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgPcntCriticalRank!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPcntCriticalRank',NULL," + m.SiteWhsOrgPcntCriticalRank.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPcntCriticalRank',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgPcntDelivered!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPcntDelivered',NULL," + m.SiteWhsOrgPcntDelivered.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPcntDelivered',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgPcntKitted!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPcntKitted',NULL," + m.SiteWhsOrgPcntKitted.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPcntKitted',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWhsOrgPrevMaintStrRmItems!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPrevMaintStrRmItems',NULL,NULL,'"+ m.SiteWhsOrgPrevMaintStrRmItems +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWhsOrgPrevMaintStrRmItems',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_INVENModel
   	         
	} // class ST_INVENModelHandler 
	
		
    public  class ST_MAINTModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_MAINTModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_MAINTModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_MAINTModel>(qry, new object[]{datasetId,"ST_MAINT"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_MAINT",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_MAINTModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_MAINTModel
   	
	
	
	           public bool Save(ST_MAINTModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_MAINT|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
if (m.CostCatInsulation_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatInsulation_RT',NULL," + m.CostCatInsulation_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatInsulation_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CostCatInsulation_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatInsulation_TA',NULL," + m.CostCatInsulation_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatInsulation_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CostCatMaterialCost_RT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatMaterialCost_RT',NULL," + m.CostCatMaterialCost_RT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatMaterialCost_RT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostCatMaterialCost_TA != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatMaterialCost_TA',NULL," + m.CostCatMaterialCost_TA.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatMaterialCost_TA',NULL,NULL,NULL," + userNo + "; ");
}

if (m.CostCatPainting_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatPainting_RT',NULL," + m.CostCatPainting_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatPainting_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CostCatPainting_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatPainting_TA',NULL," + m.CostCatPainting_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatPainting_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CostCatScaffolding_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatScaffolding_RT',NULL," + m.CostCatScaffolding_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatScaffolding_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CostCatScaffolding_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatScaffolding_TA',NULL," + m.CostCatScaffolding_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatScaffolding_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CostCatTotGeneral_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatTotGeneral_RT',NULL," + m.CostCatTotGeneral_RT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatTotGeneral_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CostCatTotGeneral_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatTotGeneral_TA',NULL," + m.CostCatTotGeneral_TA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostCatTotGeneral_TA',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_MAINTModel
   	         
	} // class ST_MAINTModelHandler 
	
		
    public  class ST_MT_TAModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_MT_TAModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_MT_TAModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_MT_TAModel>(qry, new object[]{datasetId,"ST_MT_TA"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_MT_TA",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_MT_TAModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_MT_TAModel
   	
	
	
	           public bool Save(ST_MT_TAModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_MT_TA|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.SiteTA10YearCnt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteTA10YearCnt',NULL," + m.SiteTA10YearCnt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteTA10YearCnt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteTAAvgDaysDown!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteTAAvgDaysDown',NULL," + m.SiteTAAvgDaysDown.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteTAAvgDaysDown',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteTATaken!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteTATaken',NULL,NULL,'"+ m.SiteTATaken +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteTATaken',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_MT_TAModel
   	         
	} // class ST_MT_TAModelHandler 
	
		
    public  class ST_ORGModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_ORGModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_ORGModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_ORGModel>(qry, new object[]{datasetId,"ST_ORG"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_ORG",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_ORGModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_ORGModel
   	
	
	
	           public bool Save(ST_ORGModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_ORG|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.CorpPersMaintEngr_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintEngr_CO',NULL," + m.CorpPersMaintEngr_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintEngr_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersMaintEngr_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintEngr_CR',NULL," + m.CorpPersMaintEngr_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintEngr_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersMaintEngr_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintEngr_Tot',NULL," + m.CorpPersMaintEngr_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintEngr_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersMaintMgr_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintMgr_CO',NULL," + m.CorpPersMaintMgr_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintMgr_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersMaintMgr_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintMgr_CR',NULL," + m.CorpPersMaintMgr_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintMgr_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersMaintMgr_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintMgr_Tot',NULL," + m.CorpPersMaintMgr_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersMaintMgr_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersRelEngr_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngr_CO',NULL," + m.CorpPersRelEngr_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngr_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersRelEngr_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngr_CR',NULL," + m.CorpPersRelEngr_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngr_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersRelEngr_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngr_Tot',NULL," + m.CorpPersRelEngr_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngr_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteOrgCompMaintOrg!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgCompMaintOrg',NULL,NULL,'"+ m.SiteOrgCompMaintOrg +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgCompMaintOrg',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteOrgCompMaintOrgReportsTo!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgCompMaintOrgReportsTo',NULL,NULL,'"+ m.SiteOrgCompMaintOrgReportsTo +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgCompMaintOrgReportsTo',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteOrgLevelsCEOtoCraft!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgLevelsCEOtoCraft'," + m.SiteOrgLevelsCEOtoCraft + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgLevelsCEOtoCraft',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteOrgLevelsGMtoCraft!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgLevelsGMtoCraft'," + m.SiteOrgLevelsGMtoCraft + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgLevelsGMtoCraft',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteOrgOverallPlant!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgOverallPlant',NULL,NULL,'"+ m.SiteOrgOverallPlant +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgOverallPlant',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteOrgPrimaryLeader!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgPrimaryLeader',NULL,NULL,'"+ m.SiteOrgPrimaryLeader +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgPrimaryLeader',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteOrgUpperLevels!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgUpperLevels'," + m.SiteOrgUpperLevels + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteOrgUpperLevels',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintAdmin_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintAdmin_CO',NULL," + m.SitePersMaintAdmin_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintAdmin_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintAdmin_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintAdmin_CR',NULL," + m.SitePersMaintAdmin_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintAdmin_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintAdmin_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintAdmin_Tot',NULL," + m.SitePersMaintAdmin_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintAdmin_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintEngr_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintEngr_CO',NULL," + m.SitePersMaintEngr_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintEngr_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintEngr_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintEngr_CR',NULL," + m.SitePersMaintEngr_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintEngr_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintEngr_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintEngr_Tot',NULL," + m.SitePersMaintEngr_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintEngr_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintMgr_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMgr_CO',NULL," + m.SitePersMaintMgr_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMgr_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintMgr_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMgr_CR',NULL," + m.SitePersMaintMgr_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMgr_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintMgr_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMgr_Tot',NULL," + m.SitePersMaintMgr_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMgr_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintMSPMStaff_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMSPMStaff_CO',NULL," + m.SitePersMaintMSPMStaff_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMSPMStaff_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintMSPMStaff_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMSPMStaff_CR',NULL," + m.SitePersMaintMSPMStaff_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMSPMStaff_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintMSPMStaff_TOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMSPMStaff_TOT',NULL," + m.SitePersMaintMSPMStaff_TOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintMSPMStaff_TOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintPlanner_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintPlanner_CO',NULL," + m.SitePersMaintPlanner_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintPlanner_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintPlanner_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintPlanner_CR',NULL," + m.SitePersMaintPlanner_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintPlanner_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintPlanner_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintPlanner_Tot',NULL," + m.SitePersMaintPlanner_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintPlanner_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintSched_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSched_CO',NULL," + m.SitePersMaintSched_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSched_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintSched_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSched_CR',NULL," + m.SitePersMaintSched_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSched_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintSched_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSched_Tot',NULL," + m.SitePersMaintSched_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSched_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintStoreroomMgrs_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintStoreroomMgrs_CO',NULL," + m.SitePersMaintStoreroomMgrs_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintStoreroomMgrs_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintStoreroomMgrs_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintStoreroomMgrs_CR',NULL," + m.SitePersMaintStoreroomMgrs_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintStoreroomMgrs_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintStoreroomMgrs_TOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintStoreroomMgrs_TOT',NULL," + m.SitePersMaintStoreroomMgrs_TOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintStoreroomMgrs_TOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintSupv_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSupv_CO',NULL," + m.SitePersMaintSupv_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSupv_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintSupv_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSupv_CR',NULL," + m.SitePersMaintSupv_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSupv_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersMaintSupv_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSupv_Tot',NULL," + m.SitePersMaintSupv_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersMaintSupv_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersTechnical_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTechnical_CO',NULL," + m.SitePersTechnical_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTechnical_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersTechnical_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTechnical_CR',NULL," + m.SitePersTechnical_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTechnical_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersTechnical_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTechnical_Tot',NULL," + m.SitePersTechnical_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTechnical_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersTotSupport_CO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTotSupport_CO',NULL," + m.SitePersTotSupport_CO.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTotSupport_CO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersTotSupport_CR!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTotSupport_CR',NULL," + m.SitePersTotSupport_CR.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTotSupport_CR',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SitePersTotSupport_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTotSupport_Tot',NULL," + m.SitePersTotSupport_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SitePersTotSupport_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.CorpPersRelEngrExpr != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngrExpr',NULL,NULL, '" + m.CorpPersRelEngrExpr + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CorpPersRelEngrExpr',NULL,NULL,NULL," + userNo + "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_ORGModel
   	         
	} // class ST_ORGModelHandler 
	
		
    public  class ST_PROCESSModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_PROCESSModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_PROCESSModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_PROCESSModel>(qry, new object[]{datasetId,"ST_PROCESS"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_PROCESS",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_PROCESSModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_PROCESSModel
   	
	
	
	           public bool Save(ST_PROCESSModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_PROCESS|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
if (m.SiteDocDefinedWorkflow_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow_RT',NULL,NULL,'"+ m.SiteDocDefinedWorkflow_RT +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteDocDefinedWorkflow_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow_TA',NULL,NULL,'"+ m.SiteDocDefinedWorkflow_TA +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteDocDefinedWorkflow1_RT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow1_RT',NULL,NULL,'" + m.SiteDocDefinedWorkflow1_RT + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow1_RT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocDefinedWorkflow1_TA != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow1_TA',NULL,NULL,'" + m.SiteDocDefinedWorkflow1_TA + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocDefinedWorkflow_TA1',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowPcntTM != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntTM',NULL," + m.SiteDocWorkflowPcntTM.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntTM',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowPcntMR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntMR',NULL," + m.SiteDocWorkflowPcntMR.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntMR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowPcntFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntFP',NULL," + m.SiteDocWorkflowPcntFP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowPcntAE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntAE',NULL," + m.SiteDocWorkflowPcntAE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntAE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowPcntOT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntOT',NULL," + m.SiteDocWorkflowPcntOT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntOT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowPcntTT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntTT',NULL," + m.SiteDocWorkflowPcntTT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowPcntTT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowWorkPlan != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowWorkPlan',NULL,NULL,'" + m.SiteDocWorkflowWorkPlan + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowWorkPlan',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowWorkSchedule != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowWorkSchedule',NULL,NULL,'" + m.SiteDocWorkflowWorkSchedule + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowWorkSchedule',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersJobPlan != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersJobPlan',NULL,NULL,'" + m.SiteWorkOrdersJobPlan + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersJobPlan',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteDocWorkflowExtAssessed_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowExtAssessed_RT',NULL,NULL,'"+ m.SiteDocWorkflowExtAssessed_RT +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowExtAssessed_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteDocWorkflowExtAssessed_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowExtAssessed_TA',NULL,NULL,'"+ m.SiteDocWorkflowExtAssessed_TA +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowExtAssessed_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteDocWorkflowFollowed_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowFollowed_RT',NULL,NULL,'"+ m.SiteDocWorkflowFollowed_RT +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowFollowed_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteDocWorkflowFollowed_TA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowFollowed_TA',NULL,NULL,'"+ m.SiteDocWorkflowFollowed_TA +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteDocWorkflowFollowed_TA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_COFP',NULL," + m.SiteWorkOrdersAvgPlanBacklog_COFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_COIE',NULL," + m.SiteWorkOrdersAvgPlanBacklog_COIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_CORE',NULL," + m.SiteWorkOrdersAvgPlanBacklog_CORE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_PREFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PREFP',NULL," + m.SiteWorkOrdersAvgPlanBacklog_PREFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PREFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_PREIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PREIE',NULL," + m.SiteWorkOrdersAvgPlanBacklog_PREIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PREIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_PRERE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PRERE',NULL," + m.SiteWorkOrdersAvgPlanBacklog_PRERE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PRERE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_PROFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PROFP',NULL," + m.SiteWorkOrdersAvgPlanBacklog_PROFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PROFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_PROIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PROIE',NULL," + m.SiteWorkOrdersAvgPlanBacklog_PROIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PROIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgPlanBacklog_PRORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PRORE',NULL," + m.SiteWorkOrdersAvgPlanBacklog_PRORE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgPlanBacklog_PRORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_COFP',NULL," + m.SiteWorkOrdersAvgReadyBacklog_COFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_COIE',NULL," + m.SiteWorkOrdersAvgReadyBacklog_COIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_CORE',NULL," + m.SiteWorkOrdersAvgReadyBacklog_CORE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_PREFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PREFP',NULL," + m.SiteWorkOrdersAvgReadyBacklog_PREFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PREFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_PREIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PREIE',NULL," + m.SiteWorkOrdersAvgReadyBacklog_PREIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PREIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_PRERE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PRERE',NULL," + m.SiteWorkOrdersAvgReadyBacklog_PRERE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PRERE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_PROFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PROFP',NULL," + m.SiteWorkOrdersAvgReadyBacklog_PROFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PROFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_PROIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PROIE',NULL," + m.SiteWorkOrdersAvgReadyBacklog_PROIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PROIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersAvgReadyBacklog_PRORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PRORE',NULL," + m.SiteWorkOrdersAvgReadyBacklog_PRORE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersAvgReadyBacklog_PRORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersEmergencyOrders_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_FP',NULL," + m.SiteWorkOrdersEmergencyOrders_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersEmergencyOrders_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_IE',NULL," + m.SiteWorkOrdersEmergencyOrders_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersEmergencyOrders_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_RE',NULL," + m.SiteWorkOrdersEmergencyOrders_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersEmergencyOrders_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_Tot',NULL," + m.SiteWorkOrdersEmergencyOrders_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersEmergencyOrders_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersFreqMeasureCmpl!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersFreqMeasureCmpl',NULL,NULL,'"+ m.SiteWorkOrdersFreqMeasureCmpl +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersFreqMeasureCmpl',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersLibraryReport_COTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryReport_COTOT',NULL," + m.SiteWorkOrdersLibraryReport_COTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryReport_COTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersLibraryReport_PRETOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryReport_PRETOT',NULL," + m.SiteWorkOrdersLibraryReport_PRETOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryReport_PRETOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersLibraryReport_PROTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryReport_PROTOT',NULL," + m.SiteWorkOrdersLibraryReport_PROTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryReport_PROTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersLibraryStorage_COTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryStorage_COTOT',NULL," + m.SiteWorkOrdersLibraryStorage_COTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryStorage_COTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersLibraryStorage_PRETOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryStorage_PRETOT',NULL," + m.SiteWorkOrdersLibraryStorage_PRETOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryStorage_PRETOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersLibraryStorage_PROTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryStorage_PROTOT',NULL," + m.SiteWorkOrdersLibraryStorage_PROTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersLibraryStorage_PROTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersPcntCapacitySched!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersPcntCapacitySched',NULL," + m.SiteWorkOrdersPcntCapacitySched.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersPcntCapacitySched',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_CAPFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPFP'," + m.SiteWorkOrdersTotalOrders_CAPFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_CAPIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPIE'," + m.SiteWorkOrdersTotalOrders_CAPIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_CAPRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPRE'," + m.SiteWorkOrdersTotalOrders_CAPRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_CAPTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPTOT'," + m.SiteWorkOrdersTotalOrders_CAPTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CAPTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_COFP'," + m.SiteWorkOrdersTotalOrders_COFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_COIE'," + m.SiteWorkOrdersTotalOrders_COIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CORE'," + m.SiteWorkOrdersTotalOrders_CORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_COTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_COTOT'," + m.SiteWorkOrdersTotalOrders_COTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_COTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PREFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PREFP'," + m.SiteWorkOrdersTotalOrders_PREFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PREFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PREIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PREIE'," + m.SiteWorkOrdersTotalOrders_PREIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PREIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PRERE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PRERE'," + m.SiteWorkOrdersTotalOrders_PRERE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PRERE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PRETOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PRETOT'," + m.SiteWorkOrdersTotalOrders_PRETOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PRETOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PROFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PROFP'," + m.SiteWorkOrdersTotalOrders_PROFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PROFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PROIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PROIE'," + m.SiteWorkOrdersTotalOrders_PROIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PROIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PRORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PRORE'," + m.SiteWorkOrdersTotalOrders_PRORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PRORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_PROTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PROTOT'," + m.SiteWorkOrdersTotalOrders_PROTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_PROTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_TTLFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLFP'," + m.SiteWorkOrdersTotalOrders_TTLFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_TTLIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLIE'," + m.SiteWorkOrdersTotalOrders_TTLIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_TTLRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLRE'," + m.SiteWorkOrdersTotalOrders_TTLRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalOrders_RM != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RM'," + "NULL,NULL,'" + m.SiteWorkOrdersTotalOrders_RM + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RM',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersTotalOrders_RWRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWRE'," + m.SiteWorkOrdersTotalOrders_RWRE + ",NULL,NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWRE',NULL,NULL,NULL," + userNo + "; ");
}

if (m.SiteWorkOrdersTotalOrders_RWFW != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWFW'," + m.SiteWorkOrdersTotalOrders_RWFW + ",NULL,NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWFW',NULL,NULL,NULL," + userNo + "; ");
}

if (m.SiteWorkOrdersTotalOrders_RWIEE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWIEE'," + m.SiteWorkOrdersTotalOrders_RWIEE + ",NULL,NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWIEE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersTotalOrders_RWTT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWTT'," + m.SiteWorkOrdersTotalOrders_RWTT + ",NULL,NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWTT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersTotalOrders_RWRETT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWRETT',NULL," + m.SiteWorkOrdersTotalOrders_RWRETT + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWRETT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersTotalOrders_RWFWTT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWFWTT',NULL," + m.SiteWorkOrdersTotalOrders_RWFWTT + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWFWTT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersTotalOrders_RWIEETT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWIEETT',NULL," + m.SiteWorkOrdersTotalOrders_RWIEETT + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWIEETT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersTotalOrders_RWPCTTT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWPCTTT',NULL," + m.SiteWorkOrdersTotalOrders_RWPCTTT + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_RWPCTTT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteWorkOrdersTotalOrders_TTLTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLTOT'," + m.SiteWorkOrdersTotalOrders_TTLTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalOrders_TTLTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_CAPFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPFP'," + m.SiteWorkOrdersTotalPlanned_CAPFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_CAPIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPIE'," + m.SiteWorkOrdersTotalPlanned_CAPIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_CAPRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPRE'," + m.SiteWorkOrdersTotalPlanned_CAPRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_CAPTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPTOT'," + m.SiteWorkOrdersTotalPlanned_CAPTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CAPTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_COFP'," + m.SiteWorkOrdersTotalPlanned_COFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_COIE'," + m.SiteWorkOrdersTotalPlanned_COIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CORE'," + m.SiteWorkOrdersTotalPlanned_CORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_COTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_COTOT'," + m.SiteWorkOrdersTotalPlanned_COTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_COTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PREFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PREFP'," + m.SiteWorkOrdersTotalPlanned_PREFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PREFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PREIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PREIE'," + m.SiteWorkOrdersTotalPlanned_PREIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PREIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PRERE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PRERE'," + m.SiteWorkOrdersTotalPlanned_PRERE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PRERE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PRETOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PRETOT'," + m.SiteWorkOrdersTotalPlanned_PRETOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PRETOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PROFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PROFP'," + m.SiteWorkOrdersTotalPlanned_PROFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PROFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PROIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PROIE'," + m.SiteWorkOrdersTotalPlanned_PROIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PROIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PRORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PRORE'," + m.SiteWorkOrdersTotalPlanned_PRORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PRORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_PROTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PROTOT'," + m.SiteWorkOrdersTotalPlanned_PROTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_PROTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_TTLFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLFP'," + m.SiteWorkOrdersTotalPlanned_TTLFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_TTLIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLIE'," + m.SiteWorkOrdersTotalPlanned_TTLIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_TTLRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLRE'," + m.SiteWorkOrdersTotalPlanned_TTLRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalPlanned_TTLTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLTOT'," + m.SiteWorkOrdersTotalPlanned_TTLTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalPlanned_TTLTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_CAPFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPFP'," + m.SiteWorkOrdersTotalSched_CAPFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_CAPIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPIE'," + m.SiteWorkOrdersTotalSched_CAPIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_CAPRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPRE'," + m.SiteWorkOrdersTotalSched_CAPRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_CAPTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPTOT'," + m.SiteWorkOrdersTotalSched_CAPTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CAPTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_COFP'," + m.SiteWorkOrdersTotalSched_COFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_COIE'," + m.SiteWorkOrdersTotalSched_COIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CORE'," + m.SiteWorkOrdersTotalSched_CORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_COTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_COTOT'," + m.SiteWorkOrdersTotalSched_COTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_COTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PREFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PREFP'," + m.SiteWorkOrdersTotalSched_PREFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PREFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PREIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PREIE'," + m.SiteWorkOrdersTotalSched_PREIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PREIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PRERE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PRERE'," + m.SiteWorkOrdersTotalSched_PRERE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PRERE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PRETOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PRETOT'," + m.SiteWorkOrdersTotalSched_PRETOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PRETOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PROFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PROFP'," + m.SiteWorkOrdersTotalSched_PROFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PROFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PROIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PROIE'," + m.SiteWorkOrdersTotalSched_PROIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PROIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PRORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PRORE'," + m.SiteWorkOrdersTotalSched_PRORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PRORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_PROTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PROTOT'," + m.SiteWorkOrdersTotalSched_PROTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_PROTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_TTLFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLFP'," + m.SiteWorkOrdersTotalSched_TTLFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_TTLIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLIE'," + m.SiteWorkOrdersTotalSched_TTLIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_TTLRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLRE'," + m.SiteWorkOrdersTotalSched_TTLRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSched_TTLTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLTOT'," + m.SiteWorkOrdersTotalSched_TTLTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSched_TTLTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_CAPFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPFP'," + m.SiteWorkOrdersTotalSchedCmpl_CAPFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_CAPIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPIE'," + m.SiteWorkOrdersTotalSchedCmpl_CAPIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_CAPRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPRE'," + m.SiteWorkOrdersTotalSchedCmpl_CAPRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_CAPTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPTOT'," + m.SiteWorkOrdersTotalSchedCmpl_CAPTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CAPTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_COFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_COFP'," + m.SiteWorkOrdersTotalSchedCmpl_COFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_COFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_COIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_COIE'," + m.SiteWorkOrdersTotalSchedCmpl_COIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_COIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_CORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CORE'," + m.SiteWorkOrdersTotalSchedCmpl_CORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_CORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_COTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_COTOT'," + m.SiteWorkOrdersTotalSchedCmpl_COTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_COTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PREFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PREFP'," + m.SiteWorkOrdersTotalSchedCmpl_PREFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PREFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PREIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PREIE'," + m.SiteWorkOrdersTotalSchedCmpl_PREIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PREIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PRERE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PRERE'," + m.SiteWorkOrdersTotalSchedCmpl_PRERE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PRERE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PRETOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PRETOT'," + m.SiteWorkOrdersTotalSchedCmpl_PRETOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PRETOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PROFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PROFP'," + m.SiteWorkOrdersTotalSchedCmpl_PROFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PROFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PROIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PROIE'," + m.SiteWorkOrdersTotalSchedCmpl_PROIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PROIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PRORE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PRORE'," + m.SiteWorkOrdersTotalSchedCmpl_PRORE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PRORE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_PROTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PROTOT'," + m.SiteWorkOrdersTotalSchedCmpl_PROTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_PROTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_TTLFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLFP'," + m.SiteWorkOrdersTotalSchedCmpl_TTLFP + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_TTLIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLIE'," + m.SiteWorkOrdersTotalSchedCmpl_TTLIE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_TTLRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLRE'," + m.SiteWorkOrdersTotalSchedCmpl_TTLRE + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteWorkOrdersTotalSchedCmpl_TTLTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLTOT'," + m.SiteWorkOrdersTotalSchedCmpl_TTLTOT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteWorkOrdersTotalSchedCmpl_TTLTOT',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_PROCESSModel
   	         
	} // class ST_PROCESSModelHandler 
	
		
    public  class ST_RELModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_RELModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_RELModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_RELModel>(qry, new object[]{datasetId,"ST_REL"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_REL",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_RELModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_RELModel
   	
	
	
	           public bool Save(ST_RELModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_REL|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.SiteRelCMMSSuppMgmtChng!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppMgmtChng',NULL,NULL,'"+ m.SiteRelCMMSSuppMgmtChng +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppMgmtChng',NULL,NULL,NULL," + userNo+ "; ");
}
                                if (m.SiteRelCMMSReworkMeasure != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSReworkMeasure',NULL,NULL,'" + m.SiteRelCMMSReworkMeasure + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSReworkMeasure',NULL,NULL,NULL," + userNo + "; ");
}

if (m.SiteRelCMMSSuppPlanning!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppPlanning',NULL,NULL,'"+ m.SiteRelCMMSSuppPlanning +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppPlanning',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelCMMSSuppRAMProcess!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppRAMProcess',NULL,NULL,'"+ m.SiteRelCMMSSuppRAMProcess +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppRAMProcess',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelCMMSSuppScheduling!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppScheduling',NULL,NULL,'"+ m.SiteRelCMMSSuppScheduling +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSSuppScheduling',NULL,NULL,NULL," + userNo+ "; ");
}

if (m.SiteRelCritEquipMeth_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCritEquipMeth_FP',NULL,NULL,'"+ m.SiteRelCritEquipMeth_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCritEquipMeth_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelCritEquipMeth_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCritEquipMeth_IE',NULL,NULL,'"+ m.SiteRelCritEquipMeth_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCritEquipMeth_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelCritEquipMeth_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCritEquipMeth_RE',NULL,NULL,'"+ m.SiteRelCritEquipMeth_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCritEquipMeth_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelDowntimeAutoRec_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelDowntimeAutoRec_FP',NULL,NULL,'"+ m.SiteRelDowntimeAutoRec_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelDowntimeAutoRec_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelDowntimeAutoRec_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelDowntimeAutoRec_IE',NULL,NULL,'"+ m.SiteRelDowntimeAutoRec_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelDowntimeAutoRec_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelDowntimeAutoRec_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelDowntimeAutoRec_RE',NULL,NULL,'"+ m.SiteRelDowntimeAutoRec_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelDowntimeAutoRec_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelEquipEngrStd_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelEquipEngrStd_FP',NULL,NULL,'"+ m.SiteRelEquipEngrStd_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelEquipEngrStd_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelEquipEngrStd_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelEquipEngrStd_IE',NULL,NULL,'"+ m.SiteRelEquipEngrStd_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelEquipEngrStd_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelEquipEngrStd_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelEquipEngrStd_RE',NULL,NULL,'"+ m.SiteRelEquipEngrStd_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelEquipEngrStd_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelFailureAnalysis_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelFailureAnalysis_FP',NULL,NULL,'"+ m.SiteRelFailureAnalysis_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelFailureAnalysis_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelFailureAnalysis_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelFailureAnalysis_IE',NULL,NULL,'"+ m.SiteRelFailureAnalysis_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelFailureAnalysis_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelFailureAnalysis_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelFailureAnalysis_RE',NULL,NULL,'"+ m.SiteRelFailureAnalysis_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelFailureAnalysis_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelInstalledSpares_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelInstalledSpares_FP',NULL,NULL,'"+ m.SiteRelInstalledSpares_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelInstalledSpares_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelInstalledSpares_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelInstalledSpares_IE',NULL,NULL,'"+ m.SiteRelInstalledSpares_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelInstalledSpares_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelInstalledSpares_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelInstalledSpares_RE',NULL,NULL,'"+ m.SiteRelInstalledSpares_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelInstalledSpares_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelKPIs_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelKPIs_FP',NULL,NULL,'"+ m.SiteRelKPIs_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelKPIs_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelKPIs_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelKPIs_IE',NULL,NULL,'"+ m.SiteRelKPIs_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelKPIs_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelKPIs_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelKPIs_RE',NULL,NULL,'"+ m.SiteRelKPIs_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelKPIs_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelLCCA_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelLCCA_FP',NULL,NULL,'"+ m.SiteRelLCCA_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelLCCA_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelLCCA_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelLCCA_IE',NULL,NULL,'"+ m.SiteRelLCCA_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelLCCA_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelLCCA_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelLCCA_RE',NULL,NULL,'"+ m.SiteRelLCCA_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelLCCA_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPIP5S!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIP5S',NULL,NULL,'"+ m.SiteRelPIP5S +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIP5S',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPIP6SigmaBlackBelt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIP6SigmaBlackBelt',NULL," + m.SiteRelPIP6SigmaBlackBelt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIP6SigmaBlackBelt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPIP6SigmaGreenBelt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIP6SigmaGreenBelt',NULL," + m.SiteRelPIP6SigmaGreenBelt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIP6SigmaGreenBelt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPIPMonteCarloSim!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPMonteCarloSim',NULL,NULL,'"+ m.SiteRelPIPMonteCarloSim +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPMonteCarloSim',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPIPPoissonDist!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPPoissonDist',NULL,NULL,'"+ m.SiteRelPIPPoissonDist +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPPoissonDist',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPIPTPM!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPTPM',NULL,NULL,'"+ m.SiteRelPIPTPM +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPTPM',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPIPRBM != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPRBM',NULL,NULL,'" + m.SiteRelPIPRBM + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPRBM',NULL,NULL,NULL," + userNo + "; ");
}
if (m.SiteRelPIPWeibullAnalysis!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPWeibullAnalysis',NULL,NULL,'"+ m.SiteRelPIPWeibullAnalysis +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPIPWeibullAnalysis',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPredictive_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPredictive_FP',NULL,NULL,'"+ m.SiteRelPredictive_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPredictive_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPredictive_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPredictive_IE',NULL,NULL,'"+ m.SiteRelPredictive_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPredictive_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPredictive_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPredictive_RE',NULL,NULL,'"+ m.SiteRelPredictive_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPredictive_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPreventive_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPreventive_FP',NULL,NULL,'"+ m.SiteRelPreventive_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPreventive_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPreventive_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPreventive_IE',NULL,NULL,'"+ m.SiteRelPreventive_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPreventive_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelPreventive_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPreventive_RE',NULL,NULL,'"+ m.SiteRelPreventive_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelPreventive_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRCM_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRCM_FP',NULL,NULL,'"+ m.SiteRelRCM_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRCM_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRCM_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRCM_IE',NULL,NULL,'"+ m.SiteRelRCM_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRCM_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRCM_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRCM_RE',NULL,NULL,'"+ m.SiteRelRCM_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRCM_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRepairProc_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRepairProc_FP',NULL,NULL,'"+ m.SiteRelRepairProc_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRepairProc_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRepairProc_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRepairProc_IE',NULL,NULL,'"+ m.SiteRelRepairProc_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRepairProc_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRepairProc_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRepairProc_RE',NULL,NULL,'"+ m.SiteRelRepairProc_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRepairProc_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRiskBasedInsp_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRiskBasedInsp_FP',NULL,NULL,'"+ m.SiteRelRiskBasedInsp_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRiskBasedInsp_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRiskBasedInsp_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRiskBasedInsp_IE',NULL,NULL,'"+ m.SiteRelRiskBasedInsp_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRiskBasedInsp_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRiskBasedInsp_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRiskBasedInsp_RE',NULL,NULL,'"+ m.SiteRelRiskBasedInsp_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRiskBasedInsp_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRootCause_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRootCause_FP',NULL,NULL,'"+ m.SiteRelRootCause_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRootCause_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRootCause_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRootCause_IE',NULL,NULL,'"+ m.SiteRelRootCause_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRootCause_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelRootCause_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRootCause_RE',NULL,NULL,'"+ m.SiteRelRootCause_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelRootCause_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelStrategicPlan_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelStrategicPlan_FP',NULL,NULL,'"+ m.SiteRelStrategicPlan_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelStrategicPlan_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelStrategicPlan_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelStrategicPlan_IE',NULL,NULL,'"+ m.SiteRelStrategicPlan_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelStrategicPlan_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SiteRelStrategicPlan_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelStrategicPlan_RE',NULL,NULL,'"+ m.SiteRelStrategicPlan_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelStrategicPlan_RE',NULL,NULL,NULL," + userNo+ "; ");
}
//if (m.SiteRelCMMSMaintRework != null)
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSMaintRework',NULL," + m.SiteRelCMMSMaintRework.Value.ToString(cl) + ",NULL," + userNo + " ;");
//}
//else
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSMaintRework',NULL,NULL,NULL," + userNo + "; ");
//}
//if (m.SiteRelCMMSPctRework != null)
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSPctRework',NULL," + m.SiteRelCMMSPctRework.Value.ToString(cl) + ",NULL," + userNo + " ;");
//}
//else
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SiteRelCMMSPctRework',NULL,NULL,NULL," + userNo + "; ");
//}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_RELModel
   	         
	} // class ST_RELModelHandler 
	
		
    public  class ST_STUDYModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public ST_STUDYModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  ST_STUDYModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<ST_STUDYModel>(qry, new object[]{datasetId,"ST_STUDY"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"ST_STUDY",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< ST_STUDYModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load ST_STUDYModel
   	
	
	
	           public bool Save(ST_STUDYModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|ST_STUDY|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.StudyDataCollectMethod_Char!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_Char',NULL,NULL,'"+ m.StudyDataCollectMethod_Char +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_Char',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_EquipData!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_EquipData',NULL,NULL,'"+ m.StudyDataCollectMethod_EquipData +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_EquipData',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_MaintOrg!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MaintOrg',NULL,NULL,'"+ m.StudyDataCollectMethod_MaintOrg +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MaintOrg',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_MCptl!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MCptl',NULL,NULL,'"+ m.StudyDataCollectMethod_MCptl +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MCptl',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_MHrs!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MHrs',NULL,NULL,'"+ m.StudyDataCollectMethod_MHrs +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MHrs',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_MRO!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MRO',NULL,NULL,'"+ m.StudyDataCollectMethod_MRO +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_MRO',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_ProcessChar!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_ProcessChar',NULL,NULL,'"+ m.StudyDataCollectMethod_ProcessChar +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_ProcessChar',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_Reliability!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_Reliability',NULL,NULL,'"+ m.StudyDataCollectMethod_Reliability +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_Reliability',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_RTCost!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_RTCost',NULL,NULL,'"+ m.StudyDataCollectMethod_RTCost +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_RTCost',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_TACost!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_TACost',NULL,NULL,'"+ m.StudyDataCollectMethod_TACost +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_TACost',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_WorkProcess!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_WorkProcess',NULL,NULL,'"+ m.StudyDataCollectMethod_WorkProcess +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_WorkProcess',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyDataCollectMethod_WorkTypes!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_WorkTypes',NULL,NULL,'"+ m.StudyDataCollectMethod_WorkTypes +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyDataCollectMethod_WorkTypes',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyEffortDataCollectHrs!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataCollectHrs'," + m.StudyEffortDataCollectHrs + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataCollectHrs',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyEffortDataCollectHrs_Person!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataCollectHrs_Person',NULL," + m.StudyEffortDataCollectHrs_Person.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataCollectHrs_Person',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyEffortDataCollectNumPers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataCollectNumPers'," + m.StudyEffortDataCollectNumPers + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataCollectNumPers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyEffortDataEntryHrs!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataEntryHrs'," + m.StudyEffortDataEntryHrs + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataEntryHrs',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyEffortDataEntryHrs_Person!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataEntryHrs_Person',NULL," + m.StudyEffortDataEntryHrs_Person.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataEntryHrs_Person',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StudyEffortDataEntryNumPers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataEntryNumPers'," + m.StudyEffortDataEntryNumPers + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StudyEffortDataEntryNumPers',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save ST_STUDYModel
   	         
	} // class ST_STUDYModelHandler 
	
		
    public  class UT_CHARModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public UT_CHARModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  UT_CHARModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<UT_CHARModel>(qry, new object[]{datasetId,"UT_CHAR"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"UT_CHAR",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< UT_CHARModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load UT_CHARModel
   	
	
	
	           public bool Save(UT_CHARModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|UT_CHAR|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.EquipCntAgitatorsInVessels!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAgitatorsInVessels',NULL," + m.EquipCntAgitatorsInVessels.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAgitatorsInVessels',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntAnalyzerBlending!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerBlending',NULL," + m.EquipCntAnalyzerBlending.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerBlending',NULL,NULL,NULL," + userNo+ "; ");
}
//if (m.EquipCntAnalyzerEmissions != null)
//{
//bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerEmissions',NULL," + m.EquipCntAnalyzerEmissions.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
//}else { 
//bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerEmissions',NULL,NULL,NULL," + userNo+ "; ");
//}
if (m.EquipCntAnylyzerEnv != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnylyzerEnv',NULL," + m.EquipCntAnylyzerEnv.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnylyzerEnv',NULL,NULL,NULL," + userNo + "; ");
}
if (m.EquipCntMotorsOthers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntMotorsOthers',NULL," + m.EquipCntMotorsOthers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntMotorsOthers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.EquipCntAnalyzerSafty != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerSafty',NULL," + m.EquipCntAnalyzerSafty.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerSafty',NULL,NULL,NULL," + userNo + "; ");
}
if (m.EquipCntAnalyzerProcCtrl!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerProcCtrl',NULL," + m.EquipCntAnalyzerProcCtrl.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntAnalyzerProcCtrl',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntBlowersFans!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntBlowersFans',NULL," + m.EquipCntBlowersFans.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntBlowersFans',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCentrifugesMain!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCentrifugesMain',NULL," + m.EquipCntCentrifugesMain.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCentrifugesMain',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCompressorsRecip!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRecip',NULL," + m.EquipCntCompressorsRecip.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRecip',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCompressorsRecipSpares!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRecipSpares',NULL," + m.EquipCntCompressorsRecipSpares.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRecipSpares',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCompressorsRotating!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRotating',NULL," + m.EquipCntCompressorsRotating.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRotating',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCompressorsRotatingSpares!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRotatingSpares',NULL," + m.EquipCntCompressorsRotatingSpares.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRotatingSpares',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntControlValves!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntControlValves',NULL," + m.EquipCntControlValves.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntControlValves',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCoolingTowers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCoolingTowers',NULL," + m.EquipCntCoolingTowers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCoolingTowers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCrushers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCrushers',NULL," + m.EquipCntCrushers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCrushers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntDistTowers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntDistTowers',NULL," + m.EquipCntDistTowers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntDistTowers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntDistVoltage!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntDistVoltage',NULL,NULL,'"+ m.EquipCntDistVoltage +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntDistVoltage',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntFurnaceBoilers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntFurnaceBoilers',NULL," + m.EquipCntFurnaceBoilers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntFurnaceBoilers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntHeatExch!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExch',NULL," + m.EquipCntHeatExch.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExch',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntHeatExchFinFan!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchFinFan',NULL," + m.EquipCntHeatExchFinFan.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchFinFan',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntHeatExchOth!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchOth',NULL," + m.EquipCntHeatExchOth.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchOth',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntHeatExchSpares!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchSpares',NULL," + m.EquipCntHeatExchSpares.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchSpares',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntISBLSubStationTransformer!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntISBLSubStationTransformer',NULL," + m.EquipCntISBLSubStationTransformer.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntISBLSubStationTransformer',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntMotorsMain!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntMotorsMain',NULL," + m.EquipCntMotorsMain.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntMotorsMain',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntPumpsCentrifugal!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsCentrifugal',NULL," + m.EquipCntPumpsCentrifugal.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsCentrifugal',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntPumpsCentrifugalSpares!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsCentrifugalSpares',NULL," + m.EquipCntPumpsCentrifugalSpares.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsCentrifugalSpares',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntPumpsPosDisp!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsPosDisp',NULL," + m.EquipCntPumpsPosDisp.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsPosDisp',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntRefrigUnits!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntRefrigUnits',NULL," + m.EquipCntRefrigUnits.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntRefrigUnits',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntRotaryDryers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntRotaryDryers',NULL," + m.EquipCntRotaryDryers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntRotaryDryers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntSafetyInstrSys!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntSafetyInstrSys',NULL," + m.EquipCntSafetyInstrSys.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntSafetyInstrSys',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntSafetyValves!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntSafetyValves',NULL," + m.EquipCntSafetyValves.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntSafetyValves',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntSilosISBL!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntSilosISBL',NULL," + m.EquipCntSilosISBL.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntSilosISBL',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntStorageTanksISBL!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntStorageTanksISBL',NULL," + m.EquipCntStorageTanksISBL.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntStorageTanksISBL',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntTurbines!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntTurbines',NULL," + m.EquipCntTurbines.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntTurbines',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntTurbinesSpares!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntTurbinesSpares',NULL," + m.EquipCntTurbinesSpares.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntTurbinesSpares',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntVarSpeedDrives!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntVarSpeedDrives',NULL," + m.EquipCntVarSpeedDrives.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntVarSpeedDrives',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntVessels!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntVessels',NULL," + m.EquipCntVessels.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntVessels',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityConstMaterials!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityConstMaterials',NULL,NULL,'"+ m.SeverityConstMaterials +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityConstMaterials',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityCorrosivity!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityCorrosivity',NULL,NULL,'"+ m.SeverityCorrosivity +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityCorrosivity',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityErosivity!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityErosivity',NULL,NULL,'"+ m.SeverityErosivity +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityErosivity',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityExplosivity!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityExplosivity',NULL,NULL,'"+ m.SeverityExplosivity +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityExplosivity',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityFlammability!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityFlammability',NULL,NULL,'"+ m.SeverityFlammability +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityFlammability',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityFreezePt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityFreezePt',NULL,NULL,'"+ m.SeverityFreezePt +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityFreezePt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityPressure!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityPressure',NULL,NULL,'"+ m.SeverityPressure +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityPressure',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityProcessComplexity!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityProcessComplexity',NULL,NULL,'"+ m.SeverityProcessComplexity +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityProcessComplexity',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityTemperature!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityTemperature',NULL,NULL,'"+ m.SeverityTemperature +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityTemperature',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityToxicity!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityToxicity',NULL,NULL,'"+ m.SeverityToxicity +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityToxicity',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.SeverityViscosity!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityViscosity',NULL,NULL,'"+ m.SeverityViscosity +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'SeverityViscosity',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoContinuosOrBatch!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoContinuosOrBatch',NULL,NULL,'"+ m.UnitInfoContinuosOrBatch +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoContinuosOrBatch',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoMaxDailyProdRate!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoMaxDailyProdRate',NULL," + m.UnitInfoMaxDailyProdRate.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoMaxDailyProdRate',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoPhasePredominentFeedstock!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPhasePredominentFeedstock',NULL,NULL,'"+ m.UnitInfoPhasePredominentFeedstock +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPhasePredominentFeedstock',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoPhasePredominentProduct!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPhasePredominentProduct',NULL,NULL,'"+ m.UnitInfoPhasePredominentProduct +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPhasePredominentProduct',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoPlantAge!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPlantAge',NULL," + m.UnitInfoPlantAge.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPlantAge',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoProdRateUOM!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoProdRateUOM',NULL,NULL,'"+ m.UnitInfoProdRateUOM +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoProdRateUOM',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoPRV!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPRV',NULL," + m.UnitInfoPRV.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPRV',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoPRVUSD!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPRVUSD',NULL," + m.UnitInfoPRVUSD.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoPRVUSD',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitInfoTrainCnt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoTrainCnt',NULL," + m.UnitInfoTrainCnt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitInfoTrainCnt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitSafetyCurrInjuryCntComp_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitSafetyCurrInjuryCntComp_RT'," + m.UnitSafetyCurrInjuryCntComp_RT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitSafetyCurrInjuryCntComp_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitSafetyCurrInjuryCntCont_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitSafetyCurrInjuryCntCont_RT'," + m.UnitSafetyCurrInjuryCntCont_RT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitSafetyCurrInjuryCntCont_RT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.UnitSafetyCurrInjuryCntTot_RT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitSafetyCurrInjuryCntTot_RT'," + m.UnitSafetyCurrInjuryCntTot_RT + ",NULL,NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitSafetyCurrInjuryCntTot_RT',NULL,NULL,NULL," + userNo+ "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save UT_CHARModel
   	         
	} // class UT_CHARModelHandler 
	
		
    public  class UT_COSTModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public UT_COSTModel Load( int datasetId)
				{
                    try
                    {
                        using (var db = new RAMEntities())
                        {
                            var m = new UT_COSTModel();
                            //var qry = "exec dbo.GetQuestionAnswers {0},{1};";
                            var qry1 = "select dbo.GetQuestionAnswers_A({0},{1})";
                            var qry2 = "select dbo.GetQuestionAnswers_B({0},{1})";
                            var result1 = db.ExecuteStoreQuery<string>(qry1, new object[] { "UT_COST", datasetId }).FirstOrDefault();
                            var result2 = db.ExecuteStoreQuery<string>(qry2, new object[] { "UT_COST", datasetId }).FirstOrDefault();
                            m = db.ExecuteStoreQuery<UT_COSTModel>(result1 + result2).FirstOrDefault();
                            return m;
                        }//using
                    }
                    catch (Exception ex)
                    {
                        throw new System.Exception("Errors on getting answers for Cost: " + ex.Message);
                    }

				}// Load UT_COSTModel
   	
	
	
	           public bool Save(UT_COSTModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|UT_COST|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.AnnTAMatlFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlFP',NULL," + m.AnnTAMatlFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlIE',NULL," + m.AnnTAMatlIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlMtCapFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapFP',NULL," + m.AnnTAMatlMtCapFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlMtCapIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapIE',NULL," + m.AnnTAMatlMtCapIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlMtCapRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapRE',NULL," + m.AnnTAMatlMtCapRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlMtCapTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapTOT',NULL," + m.AnnTAMatlMtCapTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlMtCapTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlRE',NULL," + m.AnnTAMatlRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTOT',NULL," + m.AnnTAMatlTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlTotFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotFP',NULL," + m.AnnTAMatlTotFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlTotIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotIE',NULL," + m.AnnTAMatlTotIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlTotRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotRE',NULL," + m.AnnTAMatlTotRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.AnnTAMatlTotTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotTOT',NULL," + m.AnnTAMatlTotTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTotTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanMtCapWhrFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrFP',NULL," + m.RoutCraftmanMtCapWhrFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanMtCapWhrIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrIE',NULL," + m.RoutCraftmanMtCapWhrIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanMtCapWhrRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrRE',NULL," + m.RoutCraftmanMtCapWhrRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanMtCapWhrTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrTOT',NULL," + m.RoutCraftmanMtCapWhrTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanMtCapWhrTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanTotWhrFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrFP',NULL," + m.RoutCraftmanTotWhrFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanTotWhrIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrIE',NULL," + m.RoutCraftmanTotWhrIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanTotWhrRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrRE',NULL," + m.RoutCraftmanTotWhrRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanTotWhrTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrTOT',NULL," + m.RoutCraftmanTotWhrTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTotWhrTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanWhrFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrFP',NULL," + m.RoutCraftmanWhrFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanWhrIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrIE',NULL," + m.RoutCraftmanWhrIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanWhrRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrRE',NULL," + m.RoutCraftmanWhrRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanWhrTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrTOT',NULL," + m.RoutCraftmanWhrTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanWhrTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutCraftmanTltHrsRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsRE',NULL," + m.RoutCraftmanTltHrsRE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutCraftmanTltHrsFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsFP',NULL," + m.RoutCraftmanTltHrsFP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutCraftmanTltHrsIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsIE',NULL," + m.RoutCraftmanTltHrsIE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutCraftmanTltHrsTOT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsTOT',NULL," + m.RoutCraftmanTltHrsTOT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutCraftmanTltHrsTOT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutMatlFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlFP',NULL," + m.RoutMatlFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlIE',NULL," + m.RoutMatlIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlMtCapFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapFP',NULL," + m.RoutMatlMtCapFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlMtCapIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapIE',NULL," + m.RoutMatlMtCapIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlMtCapRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapRE',NULL," + m.RoutMatlMtCapRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlMtCapTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapTOT',NULL," + m.RoutMatlMtCapTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlMtCapTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlRE',NULL," + m.RoutMatlRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTOT',NULL," + m.RoutMatlTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlTotFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotFP',NULL," + m.RoutMatlTotFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlTotIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotIE',NULL," + m.RoutMatlTotIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlTotRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotRE',NULL," + m.RoutMatlTotRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RoutMatlTotTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotTOT',NULL," + m.RoutMatlTotTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTotTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanMtCapWhrFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrFP',NULL," + m.TACraftmanMtCapWhrFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanMtCapWhrIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrIE',NULL," + m.TACraftmanMtCapWhrIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanMtCapWhrRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrRE',NULL," + m.TACraftmanMtCapWhrRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanMtCapWhrTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrTOT',NULL," + m.TACraftmanMtCapWhrTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanMtCapWhrTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanTotWhrFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrFP',NULL," + m.TACraftmanTotWhrFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanTotWhrIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrIE',NULL," + m.TACraftmanTotWhrIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanTotWhrRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrRE',NULL," + m.TACraftmanTotWhrRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanTotWhrTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrTOT',NULL," + m.TACraftmanTotWhrTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanTotWhrTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanWhrFP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrFP',NULL," + m.TACraftmanWhrFP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrFP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanWhrIE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrIE',NULL," + m.TACraftmanWhrIE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrIE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanWhrRE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrRE',NULL," + m.TACraftmanWhrRE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrRE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanWhrTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTOT',NULL," + m.TACraftmanWhrTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACraftmanWhrTltHrsRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsRE',NULL," + m.TACraftmanWhrTltHrsRE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TACraftmanWhrTltHrsFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsFP',NULL," + m.TACraftmanWhrTltHrsFP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TACraftmanWhrTltHrsIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsIE',NULL," + m.TACraftmanWhrTltHrsIE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TACraftmanWhrTltHrsTOT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsTOT',NULL," + m.TACraftmanWhrTltHrsTOT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACraftmanWhrTltHrsTOT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutMatlTltHrsTOT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsTOT',NULL," + m.RoutMatlTltHrsTOT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsTOT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutMatlTltHrsRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsRE',NULL," + m.RoutMatlTltHrsRE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutMatlTltHrsFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsFP',NULL," + m.RoutMatlTltHrsFP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutMatlTltHrsIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsIE',NULL," + m.RoutMatlTltHrsIE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RoutMatlTltHrsIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RoutMatlTltHrsIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsRE',NULL," + m.AnnTAMatlTltHrsRE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AnnTAMatlTltHrsFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsFP',NULL," + m.AnnTAMatlTltHrsFP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AnnTAMatlTltHrsIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsIE',NULL," + m.AnnTAMatlTltHrsIE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AnnTAMatlTltHrsTOT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsTOT',NULL," + m.AnnTAMatlTltHrsTOT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AnnTAMatlTltHrsTOT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.UnitMaintCostRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostRE',NULL," + m.UnitMaintCostRE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostRE',NULL,NULL,NULL," + userNo + "; ");
}
 if (m.UnitMaintCostFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostFP',NULL," + m.UnitMaintCostFP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.UnitMaintCostIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostIE',NULL," + m.UnitMaintCostIE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.UnitMaintCostTOT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostTOT',NULL," + m.UnitMaintCostTOT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitMaintCostTOT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.UnitTAMaintCostRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostRE',NULL," + m.UnitTAMaintCostRE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.UnitTAMaintCostFP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostFP',NULL," + m.UnitTAMaintCostFP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostFP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.UnitTAMaintCostIE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostIE',NULL," + m.UnitTAMaintCostIE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostIE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.UnitTAMaintCostTOT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostTOT',NULL," + m.UnitTAMaintCostTOT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'UnitTAMaintCostTOT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcCLWRCOHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCOHrs',NULL," + m.NRProcCLWRCOHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCOHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcCLWRCTHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCTHrs',NULL," + m.NRProcCLWRCTHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCTHrs',NULL,NULL,NULL," + userNo + "; ");
}

if (m.NRProcCLWRCOMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCOMatl',NULL," + m.NRProcCLWRCOMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCOMatl',NULL,NULL,NULL," + userNo + "; ");
}

if (m.NRProcCLWRCTMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCTMatl',NULL," + m.NRProcCLWRCTMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWRCTMatl',NULL,NULL,NULL," + userNo + "; ");
}

if (m.NRProcCLWDCOHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCOHrs',NULL," + m.NRProcCLWDCOHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCOHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcCLWDCTHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCTHrs',NULL," + m.NRProcCLWDCTHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCTHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcCLWDCOMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCOMatl',NULL," + m.NRProcCLWDCOMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCOMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcCLWDCTMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCTMatl',NULL," + m.NRProcCLWDCTMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcCLWDCTMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWRCOHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCOHrs',NULL," + m.NRProcRLWRCOHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCOHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWRCTHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCTHrs',NULL," + m.NRProcRLWRCTHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCTHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWRCOMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCOMatl',NULL," + m.NRProcRLWRCOMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCOMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWRCTMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCTMatl',NULL," + m.NRProcRLWRCTMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWRCTMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWDCOHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCOHrs',NULL," + m.NRProcRLWDCOHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCOHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWDCTHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCTHrs',NULL," + m.NRProcRLWDCTHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCTHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWDCOMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCOMatl',NULL," + m.NRProcRLWDCOMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCOMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRProcRLWDCTMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCTMatl',NULL," + m.NRProcRLWDCTMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRProcRLWDCTMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRUnitRLMTCOHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCOHrs',NULL," + m.NRUnitRLMTCOHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCOHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRUnitRLMTCTHrs != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCTHrs',NULL," + m.NRUnitRLMTCTHrs.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCTHrs',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRUnitRLMTCOMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCOMatl',NULL," + m.NRUnitRLMTCOMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCOMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRUnitRLMTCTMatl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCTMatl',NULL," + m.NRUnitRLMTCTMatl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitRLMTCTMatl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostTAWKEXEPCT_CT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CT',NULL," + m.CostTAWKEXEPCT_CT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostTAWKEXEPCT_CRC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CRC',NULL," + m.CostTAWKEXEPCT_CRC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CRC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostTAWKEXEPCT_CMC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CMC',NULL," + m.CostTAWKEXEPCT_CMC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CMC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostTAWKEXEPCT_CPC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CPC',NULL," + m.CostTAWKEXEPCT_CPC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CPC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostTAWKEXEPCT_CAC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CAC',NULL," + m.CostTAWKEXEPCT_CAC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_CAC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostTAWKEXEPCT_TT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_TT',NULL," + m.CostTAWKEXEPCT_TT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXEPCT_TT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.CostTAWKEXE_RM != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXE_RM',NULL,NULL,'" + m.CostTAWKEXE_RM + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'CostTAWKEXE_RM',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RTWKRoutCT_CT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutCT_CT',NULL," + m.RTWKRoutCT_CT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutCT_CT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RTWKRoutC_CRC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CRC',NULL," + m.RTWKRoutC_CRC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CRC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RTWKRoutC_CMC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CMC',NULL," + m.RTWKRoutC_CMC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CMC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RTWKRoutC_CPC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CPC',NULL," + m.RTWKRoutC_CPC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CPC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RTWKRoutC_CAC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CAC',NULL," + m.RTWKRoutC_CAC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_CAC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RTWKRoutC_TT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_TT',NULL," + m.RTWKRoutC_TT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RTWKRoutC_TT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRUnitTotal_HR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitTotal_HR',NULL," + m.NRUnitTotal_HR.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitTotal_HR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NRUnitTotal_SP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitTotal_SP',NULL," + m.NRUnitTotal_SP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NRUnitTotal_SP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RAMNRUnitTotal_HR != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMNRUnitTotal_HR',NULL," + m.RAMNRUnitTotal_HR.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMNRUnitTotal_HR',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RAMNRUnitTotal_SP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMNRUnitTotal_SP',NULL," + m.RAMNRUnitTotal_SP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMNRUnitTotal_SP',NULL,NULL,NULL," + userNo + "; ");
}


								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save UT_COSTModel
   	         
	} // class UT_COSTModelHandler 
	
		
    public  class UT_DTModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public UT_DTModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  UT_DTModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<UT_DTModel>(qry, new object[]{datasetId,"UT_DT"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"UT_DT",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< UT_DTModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load UT_DTModel
   	
	
	
	           public bool Save(UT_DTModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|UT_DT|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.LossHrsForAnnTA!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForAnnTA',NULL," + m.LossHrsForAnnTA.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForAnnTA',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LossHrsForProcessReliability!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForProcessReliability',NULL," + m.LossHrsForProcessReliability.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForProcessReliability',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LossHrsForShortOH!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForShortOH',NULL," + m.LossHrsForShortOH.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForShortOH',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LossHrsForTotalProdLoss!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForTotalProdLoss',NULL," + m.LossHrsForTotalProdLoss.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForTotalProdLoss',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LossHrsForUnschedMaint!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForUnschedMaint',NULL," + m.LossHrsForUnschedMaint.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsForUnschedMaint',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LossHrsTotalAnnProdLoss!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsTotalAnnProdLoss',NULL," + m.LossHrsTotalAnnProdLoss.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LossHrsTotalAnnProdLoss',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LostProductionOutages!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LostProductionOutages',NULL," + m.LostProductionOutages.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LostProductionOutages',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.LostProductionRateReductions!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LostProductionRateReductions',NULL," + m.LostProductionRateReductions.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'LostProductionRateReductions',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.NonMaintOutagesMaintEquivHours!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonMaintOutagesMaintEquivHours',NULL," + m.NonMaintOutagesMaintEquivHours.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonMaintOutagesMaintEquivHours',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RAMCausePcnt_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_FP',NULL," + m.RAMCausePcnt_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RAMCausePcnt_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_IE',NULL," + m.RAMCausePcnt_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RAMCausePcnt_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_RE',NULL," + m.RAMCausePcnt_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RAMCausePcnt_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_Tot',NULL," + m.RAMCausePcnt_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RAMCausePcnt_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ShortOH2YrCnt!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOH2YrCnt',NULL," + m.ShortOH2YrCnt.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOH2YrCnt',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ShortOHAnnHrsDown!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHAnnHrsDown',NULL," + m.ShortOHAnnHrsDown.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHAnnHrsDown',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ShortOHAvgHrsDown!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHAvgHrsDown',NULL," + m.ShortOHAvgHrsDown.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHAvgHrsDown',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ShortOHCausePcnt_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_FP',NULL," + m.ShortOHCausePcnt_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ShortOHCausePcnt_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_IE',NULL," + m.ShortOHCausePcnt_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ShortOHCausePcnt_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_RE',NULL," + m.ShortOHCausePcnt_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ShortOHCausePcnt_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_Tot',NULL," + m.ShortOHCausePcnt_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ShortOHCausePcnt_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StoppagePcnt_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_FP',NULL," + m.StoppagePcnt_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StoppagePcnt_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_IE',NULL," + m.StoppagePcnt_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StoppagePcnt_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_RE',NULL," + m.StoppagePcnt_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.StoppagePcnt_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_Tot',NULL," + m.StoppagePcnt_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'StoppagePcnt_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAAvgAnnHrsDown!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgAnnHrsDown',NULL," + m.TAAvgAnnHrsDown.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgAnnHrsDown',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAAvgHrsDown!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsDown',NULL," + m.TAAvgHrsDown.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsDown',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAAvgHrsExecution!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsExecution',NULL," + m.TAAvgHrsExecution.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsExecution',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAAvgHrsShutdown!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsShutdown',NULL," + m.TAAvgHrsShutdown.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsShutdown',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAAvgHrsStartup!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsStartup',NULL," + m.TAAvgHrsStartup.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgHrsStartup',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAAvgInterval!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgInterval',NULL," + m.TAAvgInterval.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgInterval',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAAvgPlanHrsDown!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgPlanHrsDown',NULL," + m.TAAvgPlanHrsDown.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAAvgPlanHrsDown',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACausePcnt_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_FP',NULL," + m.TACausePcnt_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACausePcnt_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_IE',NULL," + m.TACausePcnt_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACausePcnt_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_RE',NULL," + m.TACausePcnt_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACausePcnt_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_Tot',NULL," + m.TACausePcnt_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACausePcnt_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACompressors!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACompressors',NULL," + m.TACompressors.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACompressors',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAContAvail!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAContAvail',NULL,NULL,'"+ m.TAContAvail +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAContAvail',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAControlValves!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAControlValves',NULL," + m.TAControlValves.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAControlValves',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACptlProject!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACptlProject',NULL,NULL,'"+ m.TACptlProject +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACptlProject',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACritPath!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACritPath',NULL,NULL,'"+ m.TACritPath +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACritPath',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TACritPathOth!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACritPathOth',NULL,NULL,'"+ m.TACritPathOth +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TACritPathOth',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TADeterioration!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TADeterioration',NULL,NULL,'"+ m.TADeterioration +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TADeterioration',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TADiscretionaryInspect!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TADiscretionaryInspect',NULL,NULL,'"+ m.TADiscretionaryInspect +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TADiscretionaryInspect',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TADistTowersOpened!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TADistTowersOpened',NULL," + m.TADistTowersOpened.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TADistTowersOpened',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAFiredFurnaces!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAFiredFurnaces',NULL," + m.TAFiredFurnaces.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAFiredFurnaces',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAFixedSchedule!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAFixedSchedule',NULL,NULL,'"+ m.TAFixedSchedule +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAFixedSchedule',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAHeatExchOpened!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAHeatExchOpened',NULL," + m.TAHeatExchOpened.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAHeatExchOpened',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TALowDemand!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TALowDemand',NULL,NULL,'"+ m.TALowDemand +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TALowDemand',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAPumps!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAPumps',NULL," + m.TAPumps.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAPumps',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TARegulatoryInspect!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TARegulatoryInspect',NULL,NULL,'"+ m.TARegulatoryInspect +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TARegulatoryInspect',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAUnplannedFailure!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAUnplannedFailure',NULL,NULL,'"+ m.TAUnplannedFailure +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAUnplannedFailure',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAValvesCalibrated!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAValvesCalibrated',NULL," + m.TAValvesCalibrated.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAValvesCalibrated',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAValvesOpened!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAValvesOpened',NULL," + m.TAValvesOpened.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAValvesOpened',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAWeather!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWeather',NULL,NULL,'"+ m.TAWeather +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWeather',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAManagement!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAManagement',NULL,NULL,'"+ m.TAManagement +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAManagement',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TAManagementOth != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAManagementOth',NULL,NULL,'" + m.TAManagementOth + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAManagementOth',NULL,NULL,NULL," + userNo + "; ");
}

if (m.TAStrategyWP_RFW != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyWP_RFW',NULL,NULL,'" + m.TAStrategyWP_RFW + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyWP_RFW',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyWP_MCW != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyWP_MCW',NULL," + m.TAStrategyWP_MCW.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyWP_MCW',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_TSD != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_TSD',NULL," + m.TAStrategyMon_TSD.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_TSD',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_SSF != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_SSF',NULL," + m.TAStrategyMon_SSF.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_SSF',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_ASF != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_ASF',NULL," + m.TAStrategyMon_ASF.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_ASF',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_MCC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_MCC',NULL," + m.TAStrategyMon_MCC.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_MCC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_BDC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_BDC',NULL," + m.TAStrategyMon_BDC.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_BDC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_IDSP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_IDSP',NULL," + m.TAStrategyMon_IDSP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_IDSP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_IDSA != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_IDSA',NULL," + m.TAStrategyMon_IDSA.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_IDSA',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_MMP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_MMP',NULL," + m.TAStrategyMon_MMP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_MMP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_NDW != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_NDW',NULL," + m.TAStrategyMon_NDW.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_NDW',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAStrategyMon_DWA != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_DWA',NULL," + m.TAStrategyMon_DWA.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAStrategyMon_DWA',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKEXEPCT_CT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CT',NULL," + m.TAWKEXEPCT_CT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKEXEPCT_CRC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CRC',NULL," + m.TAWKEXEPCT_CRC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CRC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKEXEPCT_CMC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CMC',NULL," + m.TAWKEXEPCT_CMC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CMC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKEXEPCT_CPC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CPC',NULL," + m.TAWKEXEPCT_CPC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CPC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKEXEPCT_CAC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CAC',NULL," + m.TAWKEXEPCT_CAC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_CAC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKEXEPCT_TT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_TT',NULL," + m.TAWKEXEPCT_TT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXEPCT_TT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKEXE_RM != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXE_RM',NULL,NULL,'" + m.TAWKEXE_RM + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKEXE_RM',NULL,NULL,NULL," + userNo + "; ");
}

if (m.TAWKRoutCT_CT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CT',NULL," + m.TAWKRoutCT_CT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CT',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKRoutCT_CRC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CRC',NULL," + m.TAWKRoutCT_CRC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CRC',NULL,NULL,NULL," + userNo + "; ");
}

if (m.TAWKRoutCT_CMC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CMC',NULL," + m.TAWKRoutCT_CMC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CMC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKRoutCT_CPC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CPC',NULL," + m.TAWKRoutCT_CPC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CPC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKRoutCT_CAC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CAC',NULL," + m.TAWKRoutCT_CAC.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_CAC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.TAWKRoutCT_TT != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_TT',NULL," + m.TAWKRoutCT_TT.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TAWKRoutCT_TT',NULL,NULL,NULL," + userNo + "; ");
}




								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save UT_DTModel
   	         
	} // class UT_DTModelHandler 
	
		
    public  class UT_PROCESSModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public UT_PROCESSModel Load( int datasetId)
				{
				   using (var db = new RAMEntities())
                   {
				     var m = new  UT_PROCESSModel();
					 var qry = "exec dbo.GetQuestionAnswers {0},{1};";
					//var qry1= "select dbo.GetQuestionAnswers_A({0},{1})";
					// var qry2= "select dbo.GetQuestionAnswers_B({0},{1})";
                     m =   db.ExecuteStoreQuery<UT_PROCESSModel>(qry, new object[]{datasetId,"UT_PROCESS"}).FirstOrDefault();
					 //var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"UT_PROCESS",datasetId}).FirstOrDefault();
					 //m =  db.ExecuteStoreQuery< UT_PROCESSModel>(result1 +result2 ).FirstOrDefault();
					 return m;
				   }//using
				}// Load UT_PROCESSModel
   	
	
	
	           public bool Save(UT_PROCESSModel m, int datasetID, int userNo)
			   {
				    
					bool result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|UT_PROCESS|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.TaskConditionMonitor_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_FP',NULL," + m.TaskConditionMonitor_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskConditionMonitor_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_IE',NULL," + m.TaskConditionMonitor_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskConditionMonitor_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_RE',NULL," + m.TaskConditionMonitor_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskConditionMonitor_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_Tot',NULL," + m.TaskConditionMonitor_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskConditionMonitor_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskEmergency_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_FP',NULL," + m.TaskEmergency_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskEmergency_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_IE',NULL," + m.TaskEmergency_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskEmergency_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_RE',NULL," + m.TaskEmergency_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskEmergency_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_Tot',NULL," + m.TaskEmergency_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskEmergency_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskPreventive_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_FP',NULL," + m.TaskPreventive_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskPreventive_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_IE',NULL," + m.TaskPreventive_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskPreventive_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_RE',NULL," + m.TaskPreventive_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskPreventive_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_Tot',NULL," + m.TaskPreventive_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskPreventive_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskRoutCorrective_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_FP',NULL," + m.TaskRoutCorrective_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskRoutCorrective_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_IE',NULL," + m.TaskRoutCorrective_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskRoutCorrective_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_RE',NULL," + m.TaskRoutCorrective_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskRoutCorrective_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_Tot',NULL," + m.TaskRoutCorrective_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskRoutCorrective_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskTotal_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_FP',NULL," + m.TaskTotal_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskTotal_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_IE',NULL," + m.TaskTotal_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskTotal_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_RE',NULL," + m.TaskTotal_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.TaskTotal_Tot!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_Tot',NULL," + m.TaskTotal_Tot.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'TaskTotal_Tot',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.NonEmergencyPCT_RE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_RE',NULL," + m.NonEmergencyPCT_RE.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_RE',NULL,NULL,NULL," + userNo + "; ");
}

if (m.NonEmergencyPCT_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_FP',NULL," + m.NonEmergencyPCT_FP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NonEmergencyPCT_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_IE',NULL," + m.NonEmergencyPCT_IE.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NonEmergencyPCT_Tot != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_Tot',NULL," + m.NonEmergencyPCT_Tot.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NonEmergencyPCT_Tot',NULL,NULL,NULL," + userNo + "; ");
}
//if (m.Rework_RE != null)
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_RE',NULL," + m.Rework_RE.Value + ",NULL," + userNo + " ;");
//}
//else
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_RE',NULL,NULL,NULL," + userNo + "; ");
//}

//if (m.Rework_FP != null)
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_FP',NULL," + m.Rework_FP.Value + ",NULL," + userNo + " ;");
//}
//else
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_FP',NULL,NULL,NULL," + userNo + "; ");
//}
//if (m.Rework_IE != null)
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_IE',NULL," + m.Rework_IE.Value + ",NULL," + userNo + " ;");
//}
//else
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_IE',NULL,NULL,NULL," + userNo + "; ");
//}
//if (m.Rework_Tot != null)
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_Tot',NULL," + m.Rework_Tot.Value + ",NULL," + userNo + " ;");
//}
//else
//{
//    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'Rework_Tot',NULL,NULL,NULL," + userNo + "; ");
//}



								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save UT_PROCESSModel
   	         
	} // class UT_PROCESSModelHandler 
	
		
    public  class UT_RELModelPropertyHandler 
	{
			    
				private static object CacheLockObject = new object();
			
				public UT_RELModel Load( int datasetId)
				{
                    try
                    {
                        var db = new RAMEntities();
                        using (db)
                        {
                            var m = new  UT_RELModel();
                            db.CommandTimeout = 0;
                            var qry1= "select dbo.GetQuestionAnswers_C({0},{1})";
                            var qry2= "select dbo.GetQuestionAnswers_D({0},{1})";
                            var qry3 = "select dbo.GetQuestionAnswers_E({0},{1})";
                            var qry4 = "select dbo.GetQuestionAnswers_F({0},{1})";
                            var qry5 = "select dbo.GetQuestionAnswers_G({0},{1})";
                            var result1 = db.ExecuteStoreQuery<string>(qry1, new object[] { "UT_REL", datasetId }).FirstOrDefault();
                            var result2 =   db.ExecuteStoreQuery<string>(qry2, new object[]{"UT_REL",datasetId}).FirstOrDefault();
                            var result3 = db.ExecuteStoreQuery<string>(qry3, new object[] { "UT_REL", datasetId }).FirstOrDefault();
                            var result4 = db.ExecuteStoreQuery<string>(qry4, new object[] { "UT_REL", datasetId }).FirstOrDefault();
                            var result5 = db.ExecuteStoreQuery<string>(qry5, new object[] { "UT_REL", datasetId }).FirstOrDefault();
                            m = db.ExecuteStoreQuery<UT_RELModel>(result1 + result2 + result3 + result4 + result5).FirstOrDefault();
                            db.Dispose();
                            return m;
				        }//using
                    }
                    catch (Exception ex)
                    {
                        throw new System.Exception("Errors on getting answers for Reliability: " + ex.Message);
                    }
				}// Load UT_RELModel
   	
	
	
	           public bool Save(UT_RELModel m, int datasetID, int userNo)
			   {
				    
					var result = false;
		            var cl  = CultureInfo.CreateSpecificCulture("en-US");
					
					if ( m != null && m.CompanySID.Length > 0)
		            {
		                string cacheKey = m.CompanySID + "|UT_REL|" + datasetID;
		                try
		                {

		                    using (var ramDb = new RAMEntities())
		                    {
		                        var bd = new StringBuilder();
								if (m.EquipCntCompressorsRecipTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRecipTOT',NULL," + m.EquipCntCompressorsRecipTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRecipTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntCompressorsRotatingTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRotatingTOT',NULL," + m.EquipCntCompressorsRotatingTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntCompressorsRotatingTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntHeatExchTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchTOT',NULL," + m.EquipCntHeatExchTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntHeatExchTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntPumpsCentrifugalTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsCentrifugalTOT',NULL," + m.EquipCntPumpsCentrifugalTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntPumpsCentrifugalTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCntTurbinesTOT!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntTurbinesTOT',NULL," + m.EquipCntTurbinesTOT.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCntTurbinesTOT',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCriticalityRanking_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCriticalityRanking_FP',NULL," + m.EquipCriticalityRanking_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCriticalityRanking_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCriticalityRanking_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCriticalityRanking_IE',NULL," + m.EquipCriticalityRanking_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCriticalityRanking_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EquipCriticalityRanking_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCriticalityRanking_RE',NULL," + m.EquipCriticalityRanking_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipCriticalityRanking_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsAnalyzerBlending!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsAnalyzerBlending',NULL," + m.EventsAnalyzerBlending.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsAnalyzerBlending',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsAnalyzerEmissions!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsAnalyzerEmissions',NULL," + m.EventsAnalyzerEmissions.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsAnalyzerEmissions',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsAnalyzerProcCtrl!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsAnalyzerProcCtrl',NULL," + m.EventsAnalyzerProcCtrl.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsAnalyzerProcCtrl',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsCompressorsRecip!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsCompressorsRecip',NULL," + m.EventsCompressorsRecip.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsCompressorsRecip',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsCompressorsRotating!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsCompressorsRotating',NULL," + m.EventsCompressorsRotating.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsCompressorsRotating',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsControlValves!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsControlValves',NULL," + m.EventsControlValves.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsControlValves',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsDistTowers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsDistTowers',NULL," + m.EventsDistTowers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsDistTowers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsFurnaceBoilers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsFurnaceBoilers',NULL," + m.EventsFurnaceBoilers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsFurnaceBoilers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsHeatExch!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsHeatExch',NULL," + m.EventsHeatExch.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsHeatExch',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsMotorsMain!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsMotorsMain',NULL," + m.EventsMotorsMain.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsMotorsMain',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsPumpsCentrifugal!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsPumpsCentrifugal',NULL," + m.EventsPumpsCentrifugal.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsPumpsCentrifugal',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsPumpsPosDisp!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsPumpsPosDisp',NULL," + m.EventsPumpsPosDisp.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsPumpsPosDisp',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsSafetyInstrSys!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsSafetyInstrSys',NULL," + m.EventsSafetyInstrSys.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsSafetyInstrSys',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsTurbines!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsTurbines',NULL," + m.EventsTurbines.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsTurbines',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsVarSpeedDrives!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsVarSpeedDrives',NULL," + m.EventsVarSpeedDrives.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsVarSpeedDrives',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.EventsVessels!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsVessels',NULL," + m.EventsVessels.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EventsVessels',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFAnalyzerBlending!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFAnalyzerBlending',NULL," + m.MTBFAnalyzerBlending.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFAnalyzerBlending',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFAnalyzerEmissions!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFAnalyzerEmissions',NULL," + m.MTBFAnalyzerEmissions.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFAnalyzerEmissions',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFAnalyzerProcCtrl!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFAnalyzerProcCtrl',NULL," + m.MTBFAnalyzerProcCtrl.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFAnalyzerProcCtrl',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFCompressorsRecip!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFCompressorsRecip',NULL," + m.MTBFCompressorsRecip.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFCompressorsRecip',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFCompressorsRotating!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFCompressorsRotating',NULL," + m.MTBFCompressorsRotating.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFCompressorsRotating',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFControlValves!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFControlValves',NULL," + m.MTBFControlValves.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFControlValves',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFDistTowers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFDistTowers',NULL," + m.MTBFDistTowers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFDistTowers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFFurnaceBoilers!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFFurnaceBoilers',NULL," + m.MTBFFurnaceBoilers.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFFurnaceBoilers',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFHeatExch!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFHeatExch',NULL," + m.MTBFHeatExch.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFHeatExch',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFMotorsMain!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFMotorsMain',NULL," + m.MTBFMotorsMain.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFMotorsMain',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFPumpsCentrifugal!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFPumpsCentrifugal',NULL," + m.MTBFPumpsCentrifugal.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFPumpsCentrifugal',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFPumpsPosDisp!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFPumpsPosDisp',NULL," + m.MTBFPumpsPosDisp.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFPumpsPosDisp',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFSafetyInstrSys!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFSafetyInstrSys',NULL," + m.MTBFSafetyInstrSys.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFSafetyInstrSys',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFTurbines!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFTurbines',NULL," + m.MTBFTurbines.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFTurbines',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFVarSpeedDrives!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFVarSpeedDrives',NULL," + m.MTBFVarSpeedDrives.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFVarSpeedDrives',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MTBFVessels!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFVessels',NULL," + m.MTBFVessels.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MTBFVessels',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramAcousticEmission_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramAcousticEmission_FP',NULL," + m.ProgramAcousticEmission_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramAcousticEmission_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramAcousticEmission_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramAcousticEmission_IE',NULL," + m.ProgramAcousticEmission_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramAcousticEmission_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramAcousticEmission_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramAcousticEmission_RE',NULL," + m.ProgramAcousticEmission_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramAcousticEmission_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramCBM_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCBM_FP',NULL," + m.ProgramCBM_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCBM_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramCBM_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCBM_IE',NULL," + m.ProgramCBM_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCBM_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramCBM_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCBM_RE',NULL," + m.ProgramCBM_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCBM_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramCUI_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCUI_FP',NULL," + m.ProgramCUI_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCUI_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramCUI_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCUI_IE',NULL," + m.ProgramCUI_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCUI_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramCUI_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCUI_RE',NULL," + m.ProgramCUI_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramCUI_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramIRThermography_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramIRThermography_FP',NULL," + m.ProgramIRThermography_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramIRThermography_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramIRThermography_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramIRThermography_IE',NULL," + m.ProgramIRThermography_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramIRThermography_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramIRThermography_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramIRThermography_RE',NULL," + m.ProgramIRThermography_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramIRThermography_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramOilAnalysis_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramOilAnalysis_FP',NULL," + m.ProgramOilAnalysis_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramOilAnalysis_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramOilAnalysis_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramOilAnalysis_IE',NULL," + m.ProgramOilAnalysis_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramOilAnalysis_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramOilAnalysis_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramOilAnalysis_RE',NULL," + m.ProgramOilAnalysis_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramOilAnalysis_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramRCM_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramRCM_FP',NULL," + m.ProgramRCM_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramRCM_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramRCM_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramRCM_IE',NULL," + m.ProgramRCM_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramRCM_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramRCM_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramRCM_RE',NULL," + m.ProgramRCM_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramRCM_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramTimeBased_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramTimeBased_FP',NULL," + m.ProgramTimeBased_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramTimeBased_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramTimeBased_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramTimeBased_IE',NULL," + m.ProgramTimeBased_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramTimeBased_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramTimeBased_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramTimeBased_RE',NULL," + m.ProgramTimeBased_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramTimeBased_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramVibrationAnalysis_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramVibrationAnalysis_FP',NULL," + m.ProgramVibrationAnalysis_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramVibrationAnalysis_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramVibrationAnalysis_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramVibrationAnalysis_IE',NULL," + m.ProgramVibrationAnalysis_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramVibrationAnalysis_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.ProgramVibrationAnalysis_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramVibrationAnalysis_RE',NULL," + m.ProgramVibrationAnalysis_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'ProgramVibrationAnalysis_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RelProcessImproveProg_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RelProcessImproveProg_FP',NULL,NULL,'"+ m.RelProcessImproveProg_FP +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RelProcessImproveProg_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RelProcessImproveProg_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RelProcessImproveProg_IE',NULL,NULL,'"+ m.RelProcessImproveProg_IE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RelProcessImproveProg_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RelProcessImproveProg_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RelProcessImproveProg_RE',NULL,NULL,'"+ m.RelProcessImproveProg_RE +"',"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RelProcessImproveProg_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RootCauseAls_ARI != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_ARI',NULL,NULL,'" + m.RootCauseAls_ARI + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_ARI',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RootCauseAls_Pct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_Pct',NULL,NULL,'" + m.RootCauseAls_Pct + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_Pct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RootCauseAls_No != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_No',NULL,NULL,'" + m.RootCauseAls_No + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_No',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RootCauseAls_AFNO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_AFNO',NULL,NULL,'" + m.RootCauseAls_AFNO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_AFNO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RootCauseAls_AFO != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_AFO',NULL,NULL,'" + m.RootCauseAls_AFO + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_AFO',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RootCauseAls_QOF != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_QOF',NULL,NULL,'" + m.RootCauseAls_QOF + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_QOF',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RootCauseAls_BAL != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_BAL',NULL,NULL,'" + m.RootCauseAls_BAL + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RootCauseAls_BAL',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RiskMgmtEquipRBI_FP!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RiskMgmtEquipRBI_FP',NULL," + m.RiskMgmtEquipRBI_FP.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RiskMgmtEquipRBI_FP',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RiskMgmtEquipRBI_IE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RiskMgmtEquipRBI_IE',NULL," + m.RiskMgmtEquipRBI_IE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RiskMgmtEquipRBI_IE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.RiskMgmtEquipRBI_RE!=null){
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RiskMgmtEquipRBI_RE',NULL," + m.RiskMgmtEquipRBI_RE.Value.ToString(cl) + ",NULL,"+ userNo + " ;");
}else { 
bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RiskMgmtEquipRBI_RE',NULL,NULL,NULL," + userNo+ "; ");
}
if (m.MachineToolPct_RE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_RE',NULL," + m.MachineToolPct_RE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_RE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.MachineToolPct_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_FP',NULL," + m.MachineToolPct_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.MachineToolPct_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_IE',NULL," + m.MachineToolPct_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.MachineToolMVal_RE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolMVal_RE',NULL,NULL,'" + m.MachineToolMVal_RE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolMVal_RE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.MachineToolMVal_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolMVal_FP',NULL,NULL,'" + m.MachineToolMVal_FP + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolMVal_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.MachineToolMVal_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolMVal_IE',NULL,NULL,'" + m.MachineToolMVal_IE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolMVal_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.EquipmentRelStg_RE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipmentRelStg_RE',NULL,NULL,'" + m.EquipmentRelStg_RE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipmentRelStg_RE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.EquipmentRelStg_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipmentRelStg_FP',NULL,NULL,'" + m.EquipmentRelStg_FP + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipmentRelStg_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.EquipmentRelStg_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipmentRelStg_IE',NULL,NULL,'" + m.EquipmentRelStg_IE + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'EquipmentRelStg_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataTurbines != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataTurbines',NULL,NULL,'" + m.PDataTurbines + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataTurbines',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataCompressorsRecip != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataCompressorsRecip',NULL,NULL,'" + m.PDataCompressorsRecip + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataCompressorsRecip',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataCompressorsRotating != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataCompressorsRotating',NULL,NULL,'" + m.PDataCompressorsRotating + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataCompressorsRotating',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataPumpsCentrifugal != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataPumpsCentrifugal',NULL,NULL,'" + m.PDataPumpsCentrifugal + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataPumpsCentrifugal',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataPumpsPosDisp != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataPumpsPosDisp',NULL,NULL,'" + m.PDataPumpsPosDisp + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataPumpsPosDisp',NULL,NULL,NULL," + userNo + "; ");
}
if (m.MachineToolPct_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_IE',NULL," + m.MachineToolPct_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'MachineToolPct_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsTurbines != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsTurbines',NULL," + m.PEventsTurbines.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsTurbines',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsCompressorsRecip != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRecip',NULL," + m.PEventsCompressorsRecip.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRecip',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsCompressorsRotating != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRotating',NULL," + m.PEventsCompressorsRotating.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRotating',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsPumpsCentrifugal != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsCentrifugal',NULL," + m.PEventsPumpsCentrifugal.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsCentrifugal',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsPumpsPosDisp != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsPosDisp',NULL," + m.PEventsPumpsPosDisp.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsPosDisp',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsTurbines != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsTurbines',NULL," + m.REventsTurbines.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsTurbines',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsCompressorsRecip != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsCompressorsRecip',NULL," + m.REventsCompressorsRecip.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsCompressorsRecip',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsCompressorsRotating != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsCompressorsRotating',NULL," + m.REventsCompressorsRotating.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsCompressorsRotating',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsPumpsCentrifugal != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsPumpsCentrifugal',NULL," + m.REventsPumpsCentrifugal.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsPumpsCentrifugal',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsPumpsPosDisp != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsPumpsPosDisp',NULL," + m.REventsPumpsPosDisp.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsPumpsPosDisp',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsTurbinesMTBE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsTurbinesMTBE',NULL," + m.PEventsTurbinesMTBE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsTurbinesMTBE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsCompressorsRecipMTBE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRecipMTBE',NULL," + m.PEventsCompressorsRecipMTBE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRecipMTBE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsCompressorsRotatingMTBE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRotatingMTBE',NULL," + m.PEventsCompressorsRotatingMTBE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRotatingMTBE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsPumpsCentrifugalMTBE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsCentrifugalMTBE',NULL," + m.PEventsPumpsCentrifugalMTBE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsCentrifugalMTBE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsPumpsPosDispMTBE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsPosDispMTBE',NULL," + m.PEventsPumpsPosDispMTBE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsPosDispMTBE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsTurbinesMTBE_Rct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsTurbinesMTBE_Rct',NULL," + m.PEventsTurbinesMTBE_Rct.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsTurbinesMTBE_Rct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsCompressorsRecipMTBE_Rct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRecipMTBE_Rct',NULL," + m.PEventsCompressorsRecipMTBE_Rct.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRecipMTBE_Rct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsCompressorsRotatingMTBE_Rct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRotatingMTBE_Rct',NULL," + m.PEventsCompressorsRotatingMTBE_Rct.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsCompressorsRotatingMTBE_Rct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsPumpsCentrifugalMTBE_Rct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsCentrifugalMTBE_Rct',NULL," + m.PEventsPumpsCentrifugalMTBE_Rct.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsCentrifugalMTBE_Rct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsPumpsPosDispMTBE_Rct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsPosDispMTBE_Rct',NULL," + m.PEventsPumpsPosDispMTBE_Rct.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsPumpsPosDispMTBE_Rct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsDsgIsu != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu',NULL," + m.PctEventsDsgIsu.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsConsIsu != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu',NULL," + m.PctEventsConsIsu.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsMaintIsu != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu',NULL," + m.PctEventsMaintIsu.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOprtIsu != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu',NULL," + m.PctEventsOprtIsu.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsNmlEndLife != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife',NULL," + m.PctEventsNmlEndLife.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsTotalRE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotalRE',NULL," + m.PctEventsTotalRE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotalRE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOther != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther',NULL," + m.PctEventsOther.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsFurnaceBoilers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsFurnaceBoilers',NULL,NULL,'" + m.PDataEventsFurnaceBoilers + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsFurnaceBoilers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsVessels != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsVessels',NULL,NULL,'" + m.PDataEventsVessels + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsVessels',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsDistTowers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsDistTowers',NULL,NULL,'" + m.PDataEventsDistTowers + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsDistTowers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsHeatExch != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsHeatExch',NULL,NULL,'" + m.PDataEventsHeatExch + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsHeatExch',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsFurnaceBoilers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsFurnaceBoilers',NULL," + m.PEventsFurnaceBoilers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsFurnaceBoilers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsVessels != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsVessels',NULL," + m.PEventsVessels.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsVessels',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsDistTowers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsDistTowers',NULL," + m.PEventsDistTowers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsDistTowers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsHeatExch != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsHeatExch',NULL," + m.PEventsHeatExch.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsHeatExch',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsFurnaceBoilers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsFurnaceBoilers',NULL," + m.REventsFurnaceBoilers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsFurnaceBoilers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsVessels != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsVessels',NULL," + m.REventsVessels.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsVessels',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsDistTowers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsDistTowers',NULL," + m.REventsDistTowers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsDistTowers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsHeatExch != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsHeatExch',NULL," + m.REventsHeatExch.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsHeatExch',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsMTBEFurnaceBoilers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEFurnaceBoilers',NULL," + m.PEventsMTBEFurnaceBoilers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEFurnaceBoilers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsMTBEVessels != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEVessels',NULL," + m.PEventsMTBEVessels.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEVessels',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsMTBEDistTowers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEDistTowers',NULL," + m.PEventsMTBEDistTowers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEDistTowers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsMTBEHeatExch != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEHeatExch',NULL," + m.PEventsMTBEHeatExch.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMTBEHeatExch',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsMTBEFurnaceBoilers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEFurnaceBoilers',NULL," + m.REventsMTBEFurnaceBoilers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEFurnaceBoilers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsMTBEVessels != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEVessels',NULL," + m.REventsMTBEVessels.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEVessels',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsMTBEDistTowers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEDistTowers',NULL," + m.REventsMTBEDistTowers.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEDistTowers',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsMTBEHeatExch != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEHeatExch',NULL," + m.REventsMTBEHeatExch.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMTBEHeatExch',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsDsgIsu_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_FP',NULL," + m.PctEventsDsgIsu_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsConsIsu_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_FP',NULL," + m.PctEventsConsIsu_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsMaintIsu_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_FP',NULL," + m.PctEventsMaintIsu_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOprtIsu_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_FP',NULL," + m.PctEventsOprtIsu_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsNmlEndLife_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_FP',NULL," + m.PctEventsNmlEndLife_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOther_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_FP',NULL," + m.PctEventsOther_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsTotalRE_FP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotalRE_FP',NULL," + m.PctEventsTotalRE_FP.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotalRE_FP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsDsgIsu_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_IE',NULL," + m.PctEventsDsgIsu_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsConsIsu_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_IE',NULL," + m.PctEventsConsIsu_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsMaintIsu_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_IE',NULL," + m.PctEventsMaintIsu_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOprtIsu_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_IE',NULL," + m.PctEventsOprtIsu_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsNmlEndLife_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_IE',NULL," + m.PctEventsNmlEndLife_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOther_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_IE',NULL," + m.PctEventsOther_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsTotal_IE != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotal_IE',NULL," + m.PctEventsTotal_IE.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotal_IE',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsMotorsMain != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsMotorsMain',NULL,NULL,'" + m.PDataEventsMotorsMain + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsMotorsMain',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsVarSpeedDrives != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsVarSpeedDrives',NULL,NULL,'" + m.PDataEventsVarSpeedDrives + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsVarSpeedDrives',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsControlValves != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsControlValves',NULL,NULL,'" + m.PDataEventsControlValves + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsControlValves',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsAnalyzerProcCtrl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsAnalyzerProcCtrl',NULL,NULL,'" + m.PDataEventsAnalyzerProcCtrl + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsAnalyzerProcCtrl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsAnalyzerBlending != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsAnalyzerBlending',NULL,NULL,'" + m.PDataEventsAnalyzerBlending + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsAnalyzerBlending',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PDataEventsAnalyzerEmissions != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsAnalyzerEmissions',NULL,NULL,'" + m.PDataEventsAnalyzerEmissions + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PDataEventsAnalyzerEmissions',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsMotorsMain != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMotorsMain',NULL," + m.PEventsMotorsMain.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsMotorsMain',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsVarSpeedDrives != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsVarSpeedDrives',NULL," + m.PEventsVarSpeedDrives.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsVarSpeedDrives',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsControlValves != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsControlValves',NULL," + m.PEventsControlValves.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsControlValves',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsAnalyzerProcCtrl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsAnalyzerProcCtrl',NULL," + m.PEventsAnalyzerProcCtrl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsAnalyzerProcCtrl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsAnalyzerBlending != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsAnalyzerBlending',NULL," + m.PEventsAnalyzerBlending.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsAnalyzerBlending',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PEventsAnalyzerEmissions != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsAnalyzerEmissions',NULL," + m.PEventsAnalyzerEmissions.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PEventsAnalyzerEmissions',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsMotorsMain != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMotorsMain',NULL," + m.REventsMotorsMain.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsMotorsMain',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsVarSpeedDrives != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsVarSpeedDrives',NULL," + m.REventsVarSpeedDrives.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsVarSpeedDrives',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsControlValves != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsControlValves',NULL," + m.REventsControlValves.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsControlValves',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsAnalyzerProcCtrl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsAnalyzerProcCtrl',NULL," + m.REventsAnalyzerProcCtrl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsAnalyzerProcCtrl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsAnalyzerBlending != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsAnalyzerBlending',NULL," + m.REventsAnalyzerBlending.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsAnalyzerBlending',NULL,NULL,NULL," + userNo + "; ");
}
if (m.REventsAnalyzerEmissions != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsAnalyzerEmissions',NULL," + m.REventsAnalyzerEmissions.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'REventsAnalyzerEmissions',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PMTBEMotorsMain != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEMotorsMain',NULL," + m.PMTBEMotorsMain.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEMotorsMain',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PMTBEVarSpeedDrives != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEVarSpeedDrives',NULL," + m.PMTBEVarSpeedDrives.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEVarSpeedDrives',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PMTBEControlValves != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEControlValves',NULL," + m.PMTBEControlValves.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEControlValves',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PMTBEAnalyzerProcCtrl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEAnalyzerProcCtrl',NULL," + m.PMTBEAnalyzerProcCtrl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEAnalyzerProcCtrl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PMTBEAnalyzerBlending != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEAnalyzerBlending',NULL," + m.PMTBEAnalyzerBlending.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEAnalyzerBlending',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PMTBEAnalyzerEmissions != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEAnalyzerEmissions',NULL," + m.PMTBEAnalyzerEmissions.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PMTBEAnalyzerEmissions',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RMTBEMotorsMain != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEMotorsMain',NULL," + m.RMTBEMotorsMain.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEMotorsMain',NULL,NULL,NULL," + userNo + "; ");
}

if (m.RMTBEVarSpeedDrives != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEVarSpeedDrives',NULL," + m.RMTBEVarSpeedDrives.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEVarSpeedDrives',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RMTBEControlValves != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEControlValves',NULL," + m.RMTBEControlValves.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEControlValves',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RMTBEAnalyzerProcCtrl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEAnalyzerProcCtrl',NULL," + m.RMTBEAnalyzerProcCtrl.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEAnalyzerProcCtrl',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RMTBEAnalyzerBlending != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEAnalyzerBlending',NULL," + m.RMTBEAnalyzerBlending.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEAnalyzerBlending',NULL,NULL,NULL," + userNo + "; ");
}
if (m.RMTBEAnalyzerEmissions != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEAnalyzerEmissions',NULL," + m.RMTBEAnalyzerEmissions.Value.ToString(cl) + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'RMTBEAnalyzerEmissions',NULL,NULL,NULL," + userNo + "; ");
}

if (m.NumCauseShutDownPU_EY != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPU_EY',NULL," + m.NumCauseShutDownPU_EY.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPU_EY',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumRsltGovSpillPU_EY != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPU_EY',NULL," + m.NumRsltGovSpillPU_EY.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPU_EY',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumExPipeAstLocPU_EY != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPU_EY',NULL," + m.NumExPipeAstLocPU_EY.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPU_EY',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PipeConnectionPU_Pct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPU_Pct',NULL," + m.PipeConnectionPU_Pct.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPU_Pct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumCauseShutDownPU_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPU_CC',NULL,NULL,'" + m.NumCauseShutDownPU_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPU_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumRsltGovSpillPU_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPU_CC',NULL,NULL,'" + m.NumRsltGovSpillPU_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPU_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumExPipeAstLocPU_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPU_CC',NULL,NULL,'" + m.NumExPipeAstLocPU_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPU_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PipeConnectionPU_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPU_CC',NULL,NULL,'" + m.PipeConnectionPU_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPU_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsDsgIsu_PU != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_PU',NULL," + m.PctEventsDsgIsu_PU.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_PU',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsConsIsu_PU != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_PU',NULL," + m.PctEventsConsIsu_PU.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_PU',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsMaintIsu_PU != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_PU',NULL," + m.PctEventsMaintIsu_PU.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_PU',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOprtIsu_PU != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_PU',NULL," + m.PctEventsOprtIsu_PU.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_PU',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsNmlEndLife_PU != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_PU',NULL," + m.PctEventsNmlEndLife_PU.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_PU',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOther_PU != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_PU',NULL," + m.PctEventsOther_PU.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_PU',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsTotal_PU != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotal_PU',NULL," + m.PctEventsTotal_PU.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotal_PU',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumCauseShutDownPP_EY != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPP_EY',NULL," + m.NumCauseShutDownPP_EY.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPP_EY',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumRsltGovSpillPP_EY != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPP_EY',NULL," + m.NumRsltGovSpillPP_EY.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPP_EY',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumExPipeAstLocPP_EY != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPP_EY',NULL," + m.NumExPipeAstLocPP_EY.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPP_EY',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PipeConnectionPP_Pct != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPP_Pct',NULL," + m.PipeConnectionPP_Pct.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPP_Pct',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumCauseShutDownPP_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPP_CC',NULL,NULL,'" + m.NumCauseShutDownPP_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumCauseShutDownPP_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumRsltGovSpillPP_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPP_CC',NULL,NULL,'" + m.NumRsltGovSpillPP_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumRsltGovSpillPP_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.NumExPipeAstLocPP_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPP_CC',NULL,NULL,'" + m.NumExPipeAstLocPP_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'NumExPipeAstLocPP_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PipeConnectionPP_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPP_CC',NULL,NULL,'" + m.PipeConnectionPP_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PipeConnectionPP_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsDsgIsu_PP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_PP',NULL," + m.PctEventsDsgIsu_PP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsDsgIsu_PP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsConsIsu_PP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_PP',NULL," + m.PctEventsConsIsu_PP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsConsIsu_PP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsMaintIsu_PP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_PP',NULL," + m.PctEventsMaintIsu_PP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsMaintIsu_PP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOprtIsu_PP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_PP',NULL," + m.PctEventsOprtIsu_PP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOprtIsu_PP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsNmlEndLife_PP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_PP',NULL," + m.PctEventsNmlEndLife_PP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsNmlEndLife_PP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsOther_PP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_PP',NULL," + m.PctEventsOther_PP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsOther_PP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.PctEventsTotal_PP != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotal_PP',NULL," + m.PctEventsTotal_PP.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'PctEventsTotal_PP',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspPresVes_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_2Y',NULL," + m.AssetInspPresVes_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspDistCol_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_2Y',NULL," + m.AssetInspDistCol_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspHeatExg_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_2Y',NULL," + m.AssetInspHeatExg_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspProcPipe_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_2Y',NULL," + m.AssetInspProcPipe_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspUtilPipe_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_2Y',NULL," + m.AssetInspUtilPipe_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspReliefVal_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_2Y',NULL," + m.AssetInspReliefVal_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspOthProDev_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_2Y',NULL," + m.AssetInspOthProDev_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspMachines_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_2Y',NULL," + m.AssetInspMachines_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspStructures_2Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_2Y',NULL," + m.AssetInspStructures_2Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_2Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspPresVes_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_5Y',NULL," + m.AssetInspPresVes_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspDistCol_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_5Y',NULL," + m.AssetInspDistCol_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspHeatExg_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_5Y',NULL," + m.AssetInspHeatExg_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspProcPipe_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_5Y',NULL," + m.AssetInspProcPipe_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspUtilPipe_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_5Y',NULL," + m.AssetInspUtilPipe_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspReliefVal_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_5Y',NULL," + m.AssetInspReliefVal_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspOthProDev_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_5Y',NULL," + m.AssetInspOthProDev_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspMachines_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_5Y',NULL," + m.AssetInspMachines_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspStructures_5Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_5Y',NULL," + m.AssetInspStructures_5Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_5Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspPresVes_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_9Y',NULL," + m.AssetInspPresVes_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspDistCol_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_9Y',NULL," + m.AssetInspDistCol_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspHeatExg_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_9Y',NULL," + m.AssetInspHeatExg_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspProcPipe_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_9Y',NULL," + m.AssetInspProcPipe_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspUtilPipe_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_9Y',NULL," + m.AssetInspUtilPipe_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspReliefVal_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_9Y',NULL," + m.AssetInspReliefVal_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspOthProDev_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_9Y',NULL," + m.AssetInspOthProDev_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspMachines_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_9Y',NULL," + m.AssetInspMachines_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspStructures_9Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_9Y',NULL," + m.AssetInspStructures_9Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_9Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspPresVes_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_10Y',NULL," + m.AssetInspPresVes_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspDistCol_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_10Y',NULL," + m.AssetInspDistCol_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspHeatExg_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_10Y',NULL," + m.AssetInspHeatExg_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspProcPipe_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_10Y',NULL," + m.AssetInspProcPipe_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspUtilPipe_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_10Y',NULL," + m.AssetInspUtilPipe_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspReliefVal_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_10Y',NULL," + m.AssetInspReliefVal_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspOthProDev_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_10Y',NULL," + m.AssetInspOthProDev_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspMachines_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_10Y',NULL," + m.AssetInspMachines_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspStructures_10Y != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_10Y',NULL," + m.AssetInspStructures_10Y.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_10Y',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspPresVes_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_CC',NULL,NULL,'" + m.AssetInspPresVes_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspPresVes_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspDistCol_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_CC',NULL,NULL,'" + m.AssetInspDistCol_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspDistCol_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspHeatExg_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_CC',NULL,NULL,'" + m.AssetInspHeatExg_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspHeatExg_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspProcPipe_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_CC',NULL,NULL,'" + m.AssetInspProcPipe_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspProcPipe_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspUtilPipe_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_CC',NULL,NULL,'" + m.AssetInspUtilPipe_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspUtilPipe_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspReliefVal_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_CC',NULL,NULL,'" + m.AssetInspReliefVal_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspReliefVal_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspOthProDev_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_CC',NULL,NULL,'" + m.AssetInspOthProDev_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspOthProDev_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspMachines_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_CC',NULL,NULL,'" + m.AssetInspMachines_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspMachines_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m.AssetInspStructures_CC != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_CC',NULL,NULL,'" + m.AssetInspStructures_CC + "'," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'AssetInspStructures_CC',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntAnalyzerBlending != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntAnalyzerBlending',NULL," + m._EquipCntAnalyzerBlending.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntAnalyzerBlending',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntAnylyzerEnv != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntAnylyzerEnv',NULL," + m._EquipCntAnylyzerEnv.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntAnylyzerEnv',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntAnalyzerProcCtrl != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntAnalyzerProcCtrl',NULL," + m._EquipCntAnalyzerProcCtrl.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntAnalyzerProcCtrl',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntControlValves != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntControlValves',NULL," + m._EquipCntControlValves.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntControlValves',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntDistTowers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntDistTowers',NULL," + m._EquipCntDistTowers.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntDistTowers',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntFurnaceBoilers != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntFurnaceBoilers',NULL," + m._EquipCntFurnaceBoilers.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntFurnaceBoilers',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntMotorsMain != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntMotorsMain',NULL," + m._EquipCntMotorsMain.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntMotorsMain',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntPumpsPosDisp != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntPumpsPosDisp',NULL," + m._EquipCntPumpsPosDisp.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntPumpsPosDisp',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntVarSpeedDrives != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntVarSpeedDrives',NULL," + m._EquipCntVarSpeedDrives.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntVarSpeedDrives',NULL,NULL,NULL," + userNo + "; ");
}
if (m._EquipCntVessels != null)
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntVessels',NULL," + m._EquipCntVessels.Value + ",NULL," + userNo + " ;");
}
else
{
    bd.Append("exec dbo.spSaveAnswer " + datasetID + ",'_EquipCntVessels',NULL,NULL,NULL," + userNo + "; ");
}

								if(bd.Length >0)
								{
		                        ramDb.ExecuteStoreCommand(bd.ToString());
								//ramDb.SaveChanges();
								
								lock(CacheLockObject)
								{
		                        //Cache the model
		                        HttpRuntime.Cache.Remove(cacheKey);
								}
								}
		                        result = true;
		                    }
		                }
		                catch
		                {
		                   throw;
		                }
                        finally
						{
												
						}
						
		            }	
					return result;	
			   }// Save UT_RELModel
   	         
	} // class UT_RELModelHandler 
	
	
		
 } // namespace model handlers
 
 
 