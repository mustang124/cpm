﻿using System;
using System.Web.Mvc;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Models;

namespace RAMAPP_Mvc3.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        public ActionResult ChangePassword(string newPassword)
        {
            var memProvider = new RAMMembershipProvider();
            var httpSessionStateBase = this.HttpContext.Session;
            if (httpSessionStateBase != null && httpSessionStateBase["UserInfo"] != null)
            {
                var userinfo = (LogIn)httpSessionStateBase["UserInfo"];
                string email = userinfo.Email;
               
                bool result = memProvider.ChangePassword(email, "", newPassword);

                TempData["ResposeMessage"] = result ? "Password has been changed" : "Password has not been changed";
            }

            return View();
        }

        public ActionResult Index()
        {
            var httpSessionStateBase = this.HttpContext.Session;
            if (httpSessionStateBase != null)
            {
                var userinfo = (LogIn)httpSessionStateBase["UserInfo"];

                if (userinfo != null)
                {
                    // string userId = userinfo.UserID;
                    //var provider = new RAMMembershipProvider();
                    try
                    {
                        //LogIn ln = provider.GetUser(userId, userinfo.CompanyID);
                        var ud = new UserDetails
                        {
                            UserNo = userinfo.UserNo,
                            CompanyID = userinfo.CompanyID,
                            UserID = userinfo.UserID,
                            Firstname = userinfo.FirstName,
                            Lastname = userinfo.LastName,
                            Password = userinfo.Password,
                            EmailAddress = userinfo.Email,
                            Culture = userinfo.UserLanguage,
                            JobTile = userinfo.JobTitle,
                            ScreenName = userinfo.ScreenName
                        };
                        return View(ud);
                    }
                    catch
                    {
                        TempData["UserCreateMsg"] = "Error occurred. Unable to retrieve the user information.";
                    }
                }
            }
            //else
            return View();
        }

        //Modifies a user information
        [HttpPost]
        public ActionResult Index(UserDetails um)
        {
            var provider = new RAMMembershipProvider();

            var httpSessionStateBase = this.HttpContext.Session;

            if (httpSessionStateBase != null)
            {
                if ((um.Culture == null || um.Culture.Trim().Length == 0))
                    um.Culture = "";

                var userinfo = (LogIn)httpSessionStateBase["UserInfo"];

                try
                {
                    if (um.EmailAddress != null && um.EmailAddress != String.Empty)
                        userinfo.Email = um.EmailAddress;

                    if (um.Culture != null && um.Culture != String.Empty && um.Culture != "-1")
                    {
                        userinfo.UserLanguage = um.Culture;
                        Session["lang"] = null; //Causes the the culture to reset
                    }

                    if (um.Firstname != null && um.Firstname != String.Empty)
                        userinfo.FirstName = um.Firstname;

                    if (um.Lastname != null && um.Lastname != String.Empty)
                        userinfo.LastName = um.Lastname;

                    if (um.JobTile != null && um.JobTile != String.Empty)
                        userinfo.JobTitle = um.JobTile;

                    provider.UpdateUser(userinfo);

                    httpSessionStateBase["UserInfo"] = userinfo;
                    TempData["UserCreateMsg"] = "User " + um.Firstname + " " + um.Lastname + " was sucessfully updated";
                }
                catch
                {
                    TempData["UserCreateMsg"] = "Error occurred." + um.Firstname + " " + um.Lastname + " was not updated";
                }
            }
            return View(um);
        }
    }
}