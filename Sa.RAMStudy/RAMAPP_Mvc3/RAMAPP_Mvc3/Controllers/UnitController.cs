﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAMAPP_Mvc3.ActionFilter;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Localization;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Repositories.Cache;
using RAMAPP_Mvc3.Navigation;
using System.Web.UI;

namespace RAMAPP_Mvc3.Controllers
{
    [Authorize]
    [ReplaceCurrencyAndBreadcrumb]
    public class UnitController : Controller
    {
        //
        // GET: /Unit/

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UT_CHAR(string id)
        {
            return LoadView<UT_CHARModel>(id);
        }

        [HttpPost]
        [OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult UT_CHAR(UT_CHARModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        public ActionResult UT_COST(string id)
        {
            return LoadView<UT_COSTModel>(id);
        }

        [HttpPost]
        [OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult UT_COST(UT_COSTModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        public ActionResult UT_DT(string id)
        {
           
            return LoadView<UT_DTModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult UT_DT(UT_DTModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        public ActionResult UT_PROCESS(string id)
        {
           
            return LoadView<UT_PROCESSModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult UT_PROCESS(UT_PROCESSModel m, string id)
        {
            return SaveView(m, id);
        }

      
        [HttpGet]
        public ActionResult UT_REL(string id)
        {
            return LoadView<UT_RELModel>(id);
        }

        [HttpPost]
        [OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult UT_REL(UT_RELModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        public ActionResult UnitDetails(int id)
        {
            using (var db = new RAMEntities())
            {
                var cacheNav = new CachedNavigationRepository();

                var validUser =
                    cacheNav.GetSiteUnitList().Any(c => c.Role >= 1 && c.UnitsOfSite.Select(u => u.DatasetID).Contains(id));
                if (!validUser)
                    return RedirectToAction("NotAllowed","Shared");

                string companySID = SessionUtil.GetCompanySID();

                //var unit = (from u in db.UnitInfo
                //            where u.DatasetID == id && u.CompanySID == companySID
                //            select new UnitDetails
                //            {
                //                CompanySID = u.CompanySID,
                //                SiteDatasetID = u.SiteDatasetID,
                //                DatasetID = u.DatasetID,
                //                UnitName = u.UnitName,
                //                ProcessFamily = u.ProcessType,
                //                PrimaryProductName = u.PrimaryProductName,
                //                GenericProductName = (u.ProcessType.Trim() == "OthRef" || u.ProcessType.Trim() == "OthChem") ? "" : u.GenericProductName,
                //                OtherGenericProductName = (u.ProcessType.Trim() == "OthRef" || u.ProcessType.Trim() == "OthChem") ? u.GenericProductName : ""
                //            }).First();
                var unit = Linqs.CompiledLinqs.UnitDetailsQuery(db, id, companySID).First();
                return View(unit);
            }
        }
        [HttpGet]
        public ActionResult CreateUnit(int id)
        {
            int siteDatasetId = id;

            return View("CreateUnit",
                        new UnitDetails { CompanySID = SessionUtil.GetCompanySID(), SiteDatasetID = siteDatasetId });
        }

        [HttpPost]
        public JsonResult CreateUnit(UnitDetails m)
        {
            bool result = false;
          
            try
            {
                if (!SessionUtil.IsSessionExpired())
                {
                    var userinfo = (LogIn)HttpContext.Session["UserInfo"];
                    var GenProductName = (m.ProcessFamily == "OthRef" || m.ProcessFamily == "OthChem") ? m.OtherGenericProductName : m.GenericProductName;
                    using (var db = new RAMEntities())
                    {
                        int? ret =
                            db.AddUnit(m.SiteDatasetID, m.UnitName, null, null, m.ProcessFamily, m.ProcessCategory,
                            GenProductName, m.PrimaryProductName, userinfo.UserNo, userinfo.UserID).
                                FirstOrDefault();

                        if (ret != null && ret.HasValue && ret.Value != -1)
                        {
                            int unitDatasetID = ret.Value;

                            var navRepos = new CachedNavigationRepository();
                           // var navTree = new NavigationTree();
                            navRepos.ResetList();

                            result = true;
                            return
                                Json(
                                    new
                                    {
                                        Status = result,
                                        Unit =
                                    new
                                    {
                                        DatasetID = unitDatasetID,
                                        m.SiteDatasetID,
                                        m.UnitName,
                                        ProcessType = m.ProcessFamily,
                                        ProductGenericName = GenProductName,
                                        ProductPrimaryName = m.PrimaryProductName
                                    },
                                        SitesUnits = new NavigationTree().GetNavigation(NavigationTree.NavigationType.Administration)
                                    }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch
            {
            }

            return Json(new { Status = result });
        }
        [HttpPost]
        public ActionResult UpdateUnit(UnitDetails m)
        {
            var userinfo = (LogIn)HttpContext.Session["UserInfo"];
            string message = "The unit has been updated";

            if (userinfo != null)
            {
                using (var db = new RAMEntities())
                {
                    try
                    {
                        Dataset_LU unit =
                            db.Dataset_LU.FirstOrDefault(u => u.DatasetID == m.DatasetID && u.DataLevel == "unit");
                       
                        unit.FacilityName = m.UnitName;

                        db.SaveAnswer(m.DatasetID, "UnitInfoProcessType", null, null, m.ProcessFamily, userinfo.UserNo);
                        db.SaveAnswer(m.DatasetID, "UnitInfoProcessST", null, null, m.ProcessCategory, userinfo.UserNo);

                        db.SaveAnswer(m.DatasetID, "UnitInfoProductGenericName", null, null, (m.ProcessFamily == "OthRef" || m.ProcessFamily == "OthChem") ? m.OtherGenericProductName : m.GenericProductName,
                                      userinfo.UserNo);

                        db.SaveAnswer(m.DatasetID, "UnitInfoPrimaryProductName", null, null, m.PrimaryProductName,
                                      userinfo.UserNo);

                        db.SaveChanges();
                    }
                    catch (Exception)
                    {
                        ViewData["resultclass"] = "ErrMsg";
                        message = "The unit was not updated.";
                    }
                }
            }
            ViewData["resultclass"] = "GoodMsg";
            ViewData["result"] = message;

            return View("UnitDetails", m);
        }

        [HttpDelete]
        public JsonResult DeleteUnit(string id, string name)
        {
            var userinfo = (LogIn)HttpContext.Session["UserInfo"];
            
            bool result = false;
            short unitId = Int16.Parse(id);
            string companySID = SessionUtil.GetCompanySID();
           

            if (userinfo != null)
            {
                using (var db = new RAMEntities())
                {
                    try
                    {
                        int? ret = db.DeleteUnit(unitId, companySID, name, userinfo.UserNo, userinfo.UserID);

                        result = (ret.HasValue && ret.Value != -1);
                        if (result)
                        {
                            var navRepos = new CachedNavigationRepository();
                            navRepos.ResetList();
                        }
                    }
                    catch (Exception)
                    {
                        //do nothing
                    }
                }
            }

            return Json(new
            {
                ElementID = unitId,
                Success = result,
                Message = result
                ? "The unit has been deleted from this site."
                : "A system error occurred. The unit was not deleted."
                ,
                SitesUnits = new NavigationTree().GetNavigation(NavigationTree.NavigationType.Administration)
            });
        }
        public int? GetFacilityDataQuality(int datasetId)
        {
            int? grade;
            using (var db = new RAMEntities())
            {
                grade = (int?)(db.PropertyGrades.Select(k => k.DatasetID).Contains(datasetId)
                                    ? db.PropertyGrades.Where(k => k.DatasetID == datasetId).Select(
                                        s => s.Grade).
                                          Average()
                                    : null);
            }

            return grade;
        }
        protected ActionResult LoadView<TModel>(string id) where TModel : class
        {
            if (!SessionUtil.IsSessionExpired() && id != String.Empty)
            {
                short siteOrUnitId = Int16.Parse(id);

                //Called from the repository to make that the object is pulled from the cache
                var repository =
                    (ICachedSiteUnitModelsRepository)new CachedSiteUnitModelsRepository();
              
                TModel siteOrUnitModel;
                repository.GetModel(out siteOrUnitModel, SessionUtil.GetCompanySID(), siteOrUnitId);

                return View(siteOrUnitModel);
            }

            return View();
        }

        protected JsonResult SaveView<TModel>(TModel m, string id) where TModel : class
        {
            bool result = false;
            string statusMsg = "Data has been saved";
            string companySID;
            int userNo;

            userNo = (Session["UserInfo"] as LogIn).UserNo;

            if (id != null)
            {
                companySID = SessionUtil.GetCompanySID();

                try
                {
                    Type type =
                        Type.GetType("RAMAPP_Mvc3.ModelPropertyHandlers." + typeof(TModel).Name + "PropertyHandler",
                                     true);
                    dynamic hnd = Activator.CreateInstance(type);

                    result = hnd.Save(m, Int32.Parse(id), userNo);
                }
                catch (Exception e)
                {
                    result = false;
                    statusMsg = e.Message;
                }
            }

            return Json(new { Status = result, Msg = statusMsg });
        }
      
    }
}
