﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI.DataVisualization.Charting;
using RAMAPP_Mvc3.ActionFilter;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Localization;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Repositories.Cache;

/*using AutoMapper;*/

namespace RAMAPP_Mvc3.Controllers
{
    [Authorize]
    [ReplaceCurrencyAndBreadcrumb]
    public class AdminController : Controller
    {
        public static Func<RAMEntities, int, IQueryable<SiteUnits>, IQueryable<DataValidationSummaryModel>> DataValidationInnerResult =
         System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, IQueryable<SiteUnits>, IQueryable<DataValidationSummaryModel>>(
         (db, id, mylist) =>
             (
              from ct in mylist
              let cm = ct.UnitsOfSite.AsQueryable()
              from dq in db.DatasetQuality
              where ct.DatasetID == dq.DatasetID || cm.Any(a => a.DatasetID == dq.DatasetID)
              let qs = db.QuestionSection
              select new DataValidationSummaryModel
              {
                  FacilityName = ((ct.DatasetID == ct.DatasetID) ? ct.SiteName : cm.First(b => b.DatasetID == dq.DatasetID).UnitName),
                  DatasetID = dq.DatasetID,
                  Grade = dq.Grade ?? 0,
                  PcntComplete = dq.PcntComplete ?? 0,
                  ChecksNC = dq.ChecksNC ?? 0,
                  ChecksNA = dq.ChecksNA ?? 0
              }
              ));

         //public class DataValidationSummaryModelNEW
         //{
         //    public string pct { set; get; }
         //}
         //public static Func<RAMEntities, int, List<decimal>> DataValidationPcntComplete =
         //System.Data.Objects.CompiledQuery.Compile<RAMEntities, int,  List<decimal>>(
         //(db, id) =>
         //    (
         //     //from ct in mylist
         //     //let cm = ct.UnitsOfSite.AsQueryable()
         //     from dq in db.DatasetQuality
         //     where dq.DatasetID == id
         //     select new List<decimal>
         //     {
         //         dq.PcntComplete.Value
         //     }
         //     ).FirstOrDefault());

        [HttpGet]
        public ActionResult ApproveData()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ApproveData(string cbApproval)
        {
            if (Request["cbApproval"] == null)
            {
                TempData["ReturnMsg"] = "You must check a site first before clicking the Approve button. ";

                //##### Between these lines is just various tests, do not even uncomment this #####
                //var glob = (RAMAPP_Mvc3.Localization.ClientCultureInfo)Session["globObj"];
                //glob.SetClientCultureInfo();
                //var cachedNavigation = new CachedNavigationRepository();
                //List<SiteUnits> sites = cachedNavigation.GetSiteUnitList();
                //using (var db = new RAMEntities())
                //{
                //    var siteUnitInfo = (from h in sites
                //                        join d in db.DatasetQualitySiteSummary on h.DatasetID equals d.DatasetID
                //                        orderby d.Grade descending
                //                        let units = h.UnitsOfSite
                //                        select new DataValidationSummaryModel
                //                        {
                //                            DatasetID = h.DatasetID,
                //                            FacilityName = h.SiteName,
                //                            PcntComplete = d.PcntComplete ?? 0,
                //                            Grade = d.Grade ?? 0,
                //                            ChecksNC = d.ChecksNC ?? 0,
                //                            ChecksNA = d.ChecksNA ?? 0,
                //                            ChecksIN = d.ChecksIN ?? 0,
                //                            MissingData = d.MissingData ?? 0,
                //                            LockedImg = h.LockedImg
                //                        }).OrderByDescending(o => o.Grade).ToArray();

                //    var sLen = siteUnitInfo.Length;
                //}

                //Smtp.SendMailMessage("Solomon RAM Study Coordinator", "RAM@solomononline.com",
                //        "jbf@solomononline.com",
                //        "",
                //        "",
                //         "Solomon RAM Coordinator",
                //         "Solomon RAM coordinator was just notified that you have submitted an approval. The coordinator will begin reviewing your data and contact you if there are questions." + System.Environment.NewLine + System.Environment.NewLine + "Thank you for participating in the RAM Study," + System.Environment.NewLine + "Solomon Associates"
                //         );
                //using (var db = new RAMEntities())
                //{
                //    var cachedNavigation2 = new CachedNavigationRepository();
                //    int s = new int();
                //    s = 5406;
                //    var sites2 = cachedNavigation2.GetSiteUnitList().AsQueryable();
                //    var siteUnitInfo = DataValidationPcntComplete(db, s);

                //    List<RAMAPP_Mvc3.Repositories.Cache.SiteUnits> mylist3 = new List<RAMAPP_Mvc3.Repositories.Cache.SiteUnits>();
                //    mylist3 = sites2.ToList<RAMAPP_Mvc3.Repositories.Cache.SiteUnits>();

                //    List<decimal> mylist4 = new List<decimal>();

                //    foreach (RAMAPP_Mvc3.Repositories.Cache.SiteUnits su in mylist3)
                //    {
                //        mylist4.Add(DataValidationPcntComplete(db, su.DatasetID).FirstOrDefault());
                //    }

                //    var sit = siteUnitInfo.FirstOrDefault();
                //}
                //##### Between these lines is just various tests, do not even uncomment this #####

                return View();
            }
            bool result = true; 

            List<string> approvalList = Request["cbApproval"].Split(',').ToList();
            var cacheNav = new CachedNavigationRepository();
            var siteNames = (from ch in cacheNav.GetSiteUnitList()
                             join ap in approvalList on ch.DatasetID equals Convert.ToInt32(ap)
                             select "\u0009\u2022 " + ch.SiteName).ToArray();

            var compName = "";
            var sid = SessionUtil.GetCompanySID();
            try
            {
                using (var db = new RAMEntities())
                {
                    compName = db.Company_LU.FirstOrDefault(f => f.CompanySID == sid).CompanyName;
                    approvalList.ForEach(
                        datasetId =>
                        {
                            db.AddToReviewable(new Reviewable
                                                   {
                                                       CompanySID = sid,
                                                       DataLevel = "Si",
                                                       SiteDatsetID = Convert.ToInt32(datasetId)
                                                   });
                        }
                            );
                    db.SaveChanges();
                }
            }
            catch
            {
                result = false;
            }

            TempData["ReturnMsg"] = result
                                        ? "Site(s) has been submitted for review."
                                        : "Error. Site(s) was not submitted.";
            if (result)
            {
                var login = (LogIn)Session["UserInfo"];

                Smtp.SendMailMessage(login.FirstName != null && login.LastName != null ? login.FirstName + " " + login.LastName : login.ScreenName, 
                                      System.Configuration.ConfigurationManager.AppSettings["RAMEMailFromAddress"].ToString(),
                                      System.Configuration.ConfigurationManager.AppSettings["RAMEMailToAddress"].ToString(),
                                      "",
                                      "",
                                       compName + " submitted their data",
                                       login.FirstName + " " + login.LastName + " from " + compName + " approved their data at " + DateTime.Now.ToString("f", new CultureInfo("en")) + ". " +
                                                      System.Environment.NewLine +
                                                      System.Environment.NewLine + "The following site(s) were approved: " +
                                                      System.Environment.NewLine + String.Join(System.Environment.NewLine, siteNames)
                                       );
                //Smtp.SendMailMessage(login.FirstName != null && login.LastName != null ? login.FirstName + " " + login.LastName : login.ScreenName, login.Email,
                //          System.Configuration.ConfigurationManager.AppSettings["RAMEMailToAddress"].ToString(),
                //          "",
                //          "",
                //           compName + " submitted their data",
                //           login.FirstName + " " + login.LastName + " from " + compName + " approved their data at " + DateTime.Now.ToString("f", new CultureInfo("en")) + ". " +
                //                          System.Environment.NewLine +
                //                          System.Environment.NewLine + "The following site(s) were approved: " +
                //                          System.Environment.NewLine + String.Join(System.Environment.NewLine, siteNames)
                //           );

                Smtp.SendMailMessage("Solomon RAM Study Coordinator", System.Configuration.ConfigurationManager.AppSettings["RAMEMailFromAddress"].ToString(),
                        login.Email,
                        "",
                        "",
                         "Solomon RAM Coordinator",
                         "Dear " + login.FirstName + " " + login.LastName + "," + System.Environment.NewLine + System.Environment.NewLine + "Solomon RAM coordinator was just notified that you have submitted an approval. The coordinator will begin reviewing your data and contact you if there are questions." + System.Environment.NewLine + System.Environment.NewLine + "Thank you for participating in the RAM Study," + System.Environment.NewLine + "Solomon Associates"
                         );
            }
            return View();
        }



        [HttpGet]
        public ActionResult DataDetails(int id)
        {
            return View("DataDetails");
        }


        public JsonResult DisplayLocale(string locale)
        {
            CultureInfo ci;
            try
            {
                ci = new CultureInfo(locale);
            }
            catch
            {
                ci = new CultureInfo("en");
            }
            return Json(new
                            {
                                localeNumber = ((Decimal)1234567.90).ToString("n", ci),
                                localeDate = DateTime.Now.ToString("D", ci)
                            }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public int? GetFacilityDataQuality(int datasetId)
        {
            int? grade;
            using (var db = new RAMEntities())
            {
                grade = (int?)(db.PropertyGrades.Select(k=>k.DatasetID).Contains(datasetId)
                                    ? db.PropertyGrades.Where(k => k.DatasetID == datasetId).Select(
                                        s => s.Grade).
                                          Average()
                                    : null);
            }

            return grade;
        }

        [HttpGet]
        public ActionResult GetStatusImage(string code)
        {
            var imgStatusCode = "";
            if (code == "GreenCheck" || code == "OK")
                imgStatusCode = Server.MapPath("~/images/Green_Checkmark_sm.png");

            if (code == "RedX" || code == "NC")
                imgStatusCode = Server.MapPath("~/images/Error_sm.png");

            if (code == "YellowCircle" || code == "NA")
                imgStatusCode = Server.MapPath("~/images/Warning_sm.png");

            if (code == "BlackBar" || code == "IN")
                imgStatusCode = Server.MapPath("~/images/BlackBar_sm.png");

            return File(imgStatusCode, "image/png");
        }

        // GET: /SiteAdmin/SiteAdmin/
        public ActionResult Index()
        {
            using (var db = new RAMEntities())
            {
                if (!SessionUtil.IsAdmin() && !SessionUtil.IsSiteAdmin())
                    return View("NotAllowed");
            }

            return View();
        }



        [HttpGet]
        public ActionResult NotAllowed()
        {
            return View();
        }



        public void ResetPassword(string usr, string email)
        {
            int passwordLength = 12;
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            var chars = new char[passwordLength];
            var rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            var randpwd = new string(chars);
            using (var db = new RAMEntities())
            {
                db.AddToPasswordChangeRequests(new PasswordChangeRequests { UserID = usr, ID = randpwd, RequestTime = DateTime.Now });
                db.SaveChanges();
            }

            if (email != null)
            {
                Smtp.SendMailMessage("Solomon Associates Administrator", System.Configuration.ConfigurationManager.AppSettings["RAMEMailFromAddress"].ToString(),
                                     email,
                                     "",
                                     "",
                                     "Reset RAM Study Password",
                                     "Please click this link to reset your password https://webservices.solomononline.com/RAMStudy/Account/ResetPassword/" +
                                     randpwd + Environment.NewLine + Environment.NewLine +
                                     "Solomon RAM Study Administrator");
            }
        }

      

        protected FileResult GenerateChart(string xTitle, string yTitle, List<string> xPoints, List<int?> yPoints,
                                                   int height = 525, int width = 650)
        {
            var key = new Chart();
            key.Compression = 1;
            key.Height = height;
            key.Width = width;

            key.ChartAreas.Add(new ChartArea("MainArea"));
            key.ChartAreas[0].AxisX.LabelStyle.Angle = 0;
            key.ChartAreas[0].AxisY.Title = yTitle;
            key.ChartAreas[0].AxisX.Title = xTitle;
            key.ChartAreas[0].AxisY.Maximum = 100;
            key.ChartAreas[0].AxisY.Minimum = 0;
            key.ChartAreas[0].Area3DStyle.Enable3D = true;
            key.ChartAreas[0].Area3DStyle.IsRightAngleAxes = false;
            key.ChartAreas[0].Area3DStyle.IsClustered = false;

            key.ChartAreas[0].Area3DStyle.Inclination = 0;
            key.ChartAreas[0].Area3DStyle.WallWidth = 0;
            key.ChartAreas[0].Area3DStyle.Rotation = 0;

            key.BorderlineDashStyle = ChartDashStyle.Solid;
            key.BackSecondaryColor = Color.White;
            key.BackGradientStyle = GradientStyle.TopBottom;
            key.BorderlineWidth = 1;
            key.Palette = ChartColorPalette.BrightPastel;
            key.BorderlineColor = Color.FromArgb(26, 59, 105);
            key.RenderType = RenderType.BinaryStreaming;
            key.BorderSkin.SkinStyle = BorderSkinStyle.None;
            key.AntiAliasing = AntiAliasingStyles.All;
            key.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal;

            key.Series.Add(new Series());

            key.Series[0]["DrawingStyle"] = "Cylinder";
            key.Series[0].BackGradientStyle = GradientStyle.LeftRight;
            key.Series[0].Points.DataBindXY(xPoints, yPoints);
            key.Series[0].ToolTip = "% Completed: #VALY{n0}";
            key.Series[0].ShadowColor = Color.Gray;
            key.Series[0].ShadowOffset = 2;

            key.Legends.Add(new Legend("Series1"));

            var ms = new MemoryStream();
            key.SaveImage(ms);
            return File(ms.GetBuffer(), @"image/png");
        }

        protected UnitInfo GetUnit(RAMEntities db, int siteDatasetId, string unitName)
        {
            return db.UnitInfo.FirstOrDefault(u => u.UnitName == unitName && u.SiteDatasetID == siteDatasetId);
        }

        private string BuildTableView(string header, StringBuilder rows)
        {
            string table = null;
            ////rows.Insert()
            //rows.Insert(0, "<div class=\"gridHorizontal\">" + header);

            //rows.Add("</div>");

            //table= string.Join(" ", rows.ToArray());

            return table;
        }

        private byte DataCheckStatus(RAMEntities db, int datasetID)
        {
            var qry = "SELECT dbo.DatasetProcessStatus({0})";

            var status = db.ExecuteStoreQuery<byte>(qry, datasetID).Single();
            return status;
        }

        

        private List<DataCheckTables> GetDataCheckDetails(int id, int datacheckid, out string table1, out string table2,
                    out string dataCheckName,
          out string datacheckText,
          out string statusCode,
          out string statusText,
          out string statusImage,
          out string statusMessage,
          out decimal checkValue,
          out decimal compareValue,
          out decimal rangeMin,
          out decimal rangeMax,
          out int decimalPlaces)
        {
            table1 = "";
            table2 = "";

            string qry = "exec dbo.GetDataCheckDetails @p0,@p1,@p2 out ,@p3 out,@p4 out,@p5 out,@p6 out,@p7 out, @p8 out,@p9 out,@p10 out,@p11 out,@p12 out";
            var pId = new SqlParameter { ParameterName = "p0", Value = id, Direction = ParameterDirection.InputOutput };
            var pCheckId = new SqlParameter { ParameterName = "p1", Value = datacheckid, Direction = ParameterDirection.InputOutput };
            var pCheckName = new SqlParameter { ParameterName = "p2", SqlDbType = SqlDbType.VarChar, Size = 150, Direction = ParameterDirection.Output };
            var pCheckText = new SqlParameter { ParameterName = "p3", SqlDbType = SqlDbType.VarChar, Size = 4000, Direction = ParameterDirection.Output };
            var pStatusCode = new SqlParameter { ParameterName = "p4", SqlDbType = SqlDbType.VarChar, Size = 20, Direction = ParameterDirection.Output };
            var pStatusText = new SqlParameter { ParameterName = "p5", SqlDbType = SqlDbType.VarChar, Size = 50, Direction = ParameterDirection.Output };
            var pStatusImage = new SqlParameter { ParameterName = "p6", SqlDbType = SqlDbType.VarChar, Size = 35, Direction = ParameterDirection.Output };
            var pStatusMessage = new SqlParameter { ParameterName = "p7", SqlDbType = SqlDbType.NVarChar, Size = 225, Direction = ParameterDirection.Output };
            var pCheckValue = new SqlParameter { ParameterName = "p8", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pCompareValue = new SqlParameter { ParameterName = "p9", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pRangeMin = new SqlParameter { ParameterName = "p10", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pRangeMax = new SqlParameter { ParameterName = "p11", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pDecimalPlaces = new SqlParameter { ParameterName = "p12", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };
            //var myList = new StringBuilder();

            using (var db = new RAMEntities())
            {
                //var results = null as DataPropertyDetailModel;
                List<DataCheckTables> resultsCol = db.ExecuteStoreQuery<DataCheckTables>(qry,
                                                                new object[]
                                                                      {
                                                                         pId, pCheckId , pCheckName ,
                                                                         pCheckText ,pStatusCode ,
                                                                         pStatusText ,pStatusImage ,
                                                                         pStatusMessage ,pCheckValue  ,
                                                                         pCompareValue ,pRangeMin ,pRangeMax,pDecimalPlaces
                                                                      }).ToList();

                dataCheckName = pCheckName.Value != DBNull.Value ? (string)pCheckName.Value : "";
                datacheckText = pCheckText.Value != DBNull.Value ? (string)pCheckText.Value : "";
                statusCode = pStatusCode.Value != DBNull.Value ? (string)pStatusCode.Value : "";
                statusText = pStatusText.Value != DBNull.Value ? (string)pStatusText.Value : "";
                statusImage = pStatusImage.Value != DBNull.Value ? (string)pStatusImage.Value : "";
                statusMessage = pStatusMessage.Value != DBNull.Value ? (string)pStatusMessage.Value : "";
                checkValue = pCheckValue.Value != DBNull.Value ? Convert.ToDecimal(pCheckValue.Value) : 0;
                compareValue = pCompareValue.Value != DBNull.Value ? Convert.ToDecimal(pCompareValue.Value) : 0;
                rangeMin = pRangeMin.Value != DBNull.Value ? Convert.ToDecimal(pRangeMin.Value) : 0;
                rangeMax = pRangeMax.Value != DBNull.Value ? Convert.ToDecimal(pRangeMax.Value) : 0;
                decimalPlaces = pDecimalPlaces.Value != DBNull.Value ? (int)pDecimalPlaces.Value : 0;

                return resultsCol;
            }
        }

        private List<DataValidationGradeModel> GetGrades(int id, out int total, int start = -1, int size = -1)
        {
            const string qry = "exec dbo.GetPropertyGrades {0},99;";
            var myList = new StringBuilder();
            var results = null as List<DataValidationGradeModel>;
            total = 0;

            if (Session["grades" + id] == null)
            {
                using (var db = new RAMEntities())
                {
                    results = db.ExecuteStoreQuery<DataValidationGradeModel>(qry, new object[] { id }).ToList();
                    Session["grades" + id] = results;
                }
            }
            else
            {
                results = Session["grades" + id] as List<DataValidationGradeModel>;
            }

            if (results != null)
            {
                total = results.Count();

                if (start > -1 && size > -1)
                {
                    if (size > total)
                        size = total;

                    if (size > (total - start))
                        size = (total - start);

                    return results.GetRange(start, size);
                }
            }
            return results;
        }

        private string GetPagingLinks(int datasetID, int total, int start, int size)
        {
            double NumOfPg = Math.Ceiling((double)total / size);
            string templt = "<span class='pgLinks'><a href='{0}?start={1}&size={2}'>{3}</a> </span>&nbsp;";
            var links = new StringBuilder();

            links.AppendFormat(templt, Url.Action("ViewGradeDetails", "Admin", new { id = datasetID }), 1, size, "First");

            for (int g = 1; g < NumOfPg + 1; g++)
            {
                links.AppendFormat(templt, Url.Action("ViewGradeDetails", "Admin", new { id = datasetID }),
                                   ((g * size) - size) + 1, size, g);
            }

            links.AppendFormat(templt, Url.Action("ViewGradeDetails", "Admin", new { id = datasetID }),
                               ((NumOfPg * size) - size) + 1, size, "Last");
            return links.ToString();
        }

        private List<DataPropertyDetailModel> GetPropertyValidationDetails(int id, int propertyNo, out string propertyName,
                    out string friendlyName,
                    out string dataType,
                    out decimal numberValue,
                    out string strValue,
                    out string inputField,
                    out string sectionName,
                    out string subSectionName,
                    out string subSectionDesc,
                    out string UOM,
                    out short decimalPlaces,
                    int datacheckID = 0)
        {
            string qry = "exec dbo.GetPropertyDataChecks @p0,@p1,@p2 out ,@p3 out,@p4 out,@p5 out,@p6 out,@p7 out, @p8 out,@p9 out,@p10 out,@p11 out,@p12 out";
            //string qry = "exec dbo.GetPropertyDataChecks {0},{1},{2} OUTPUT,{3} OUTPUT,{4} OUTPUT,{5} OUTPUT,{6} OUTPUT,{7} OUTPUT;";
            var pId = new SqlParameter { ParameterName = "p0", Value = id, Direction = ParameterDirection.InputOutput };
            var pPropNo = new SqlParameter { ParameterName = "p1", Value = propertyNo, Direction = ParameterDirection.InputOutput };
            var pPropName = new SqlParameter { ParameterName = "p2", SqlDbType = SqlDbType.VarChar, Size = 35, Direction = ParameterDirection.Output };
            var pFriend = new SqlParameter { ParameterName = "p3", SqlDbType = SqlDbType.VarChar, Size = 300, Direction = ParameterDirection.Output };
            var pType = new SqlParameter { ParameterName = "p4", SqlDbType = SqlDbType.Char, Size = 10, Direction = ParameterDirection.Output };
            var pNmVal = new SqlParameter { ParameterName = "p5", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Output };
            var pStrVal = new SqlParameter { ParameterName = "p6", SqlDbType = SqlDbType.NVarChar, Size = 225, Direction = ParameterDirection.Output };
            var pFieldHtml = new SqlParameter { ParameterName = "p7", SqlDbType = SqlDbType.VarChar, Size = 8000, Direction = ParameterDirection.Output };
            var pSection = new SqlParameter { ParameterName = "p8", SqlDbType = SqlDbType.VarChar, Size = 45, Direction = ParameterDirection.Output };
            var pSubSection = new SqlParameter { ParameterName = "p9", SqlDbType = SqlDbType.VarChar, Size = 45, Direction = ParameterDirection.Output };
            var pSubSectionDesc = new SqlParameter { ParameterName = "p10", SqlDbType = SqlDbType.VarChar, Size = 8000, Direction = ParameterDirection.Output };
            var pUOM = new SqlParameter { ParameterName = "p11", SqlDbType = SqlDbType.VarChar, Size = 80, Direction = ParameterDirection.Output };
            var pDecimalPlaces = new SqlParameter { ParameterName = "p12", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Output };
            //var myList = new StringBuilder();

            using (var db = new RAMEntities())
            {
                //var results = null as DataPropertyDetailModel;
                List<DataPropertyDetailModel> resultsCol =
                  db.ExecuteStoreQuery<DataPropertyDetailModel>(qry,
                                                                new object[]
                                                                      {
                                                                          pId, pPropNo, pPropName, pFriend,
                                                                          pType, pNmVal, pStrVal, pFieldHtml,pSection,pSubSection,
                                                                          pSubSectionDesc,pUOM,pDecimalPlaces
                                                                      }).
                        ToList();

                propertyName = (string)pPropName.Value;
                friendlyName = (string)pFriend.Value;
                dataType = (string)pType.Value;
                numberValue = pNmVal.Value != DBNull.Value ? (decimal)pNmVal.Value : 0;
                strValue = pStrVal.Value != DBNull.Value ? (string)pStrVal.Value : "";
                inputField = pFieldHtml.Value != DBNull.Value ? (string)pFieldHtml.Value : "";
                sectionName = pSection.Value != DBNull.Value ? (string)pSection.Value : "";
                subSectionName = pSubSection.Value != DBNull.Value ? (string)pSubSection.Value : "";
                subSectionDesc = pSubSectionDesc.Value != DBNull.Value ? (string)pSubSectionDesc.Value : "";
                UOM = pUOM.Value != DBNull.Value ? (string)pUOM.Value : "";
                decimalPlaces = pDecimalPlaces.Value != DBNull.Value ? (short)pDecimalPlaces.Value : (short)0;

                return resultsCol;
            }
        }

        private SiteInfo GetSite(RAMEntities db, int siteDatasetId)
        {
            return db.SiteInfo.FirstOrDefault(s => s.DatasetID == siteDatasetId);
        }

        private void ReplaceKeywords(ref DataCheckDetailsModel results, decimal rangeMin, decimal rangeMax, decimal compareVal, decimal checkVal, int decimalPlaces)
        {
            if (results == null)
                return;
           
            var cult = new ClientCultureInfo();
            CultureInfo cultProvider = Thread.CurrentThread.CurrentCulture;

            results.DatacheckText = results.DatacheckText.Replace("@RangeMin",
                                                                  String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                 results.RangeMin));
            results.DatacheckText = results.DatacheckText.Replace("@RangeMax",
                                                                  String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                                rangeMax));

            results.DatacheckText = results.DatacheckText.Replace("@CompareValue",
                                                                  String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                                compareVal));

            results.DatacheckText = results.DatacheckText.Replace("@CheckValue",
                                                                  String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                                checkVal));
        }

        private void ReplaceKeywords(ref DataPropertyDetailModel results, int decimalPlaces)
        {
            if (results == null)
                return;
            if (decimalPlaces == 0)
                decimalPlaces = 2;

            var cult = new ClientCultureInfo();
            CultureInfo cultProvider = Thread.CurrentThread.CurrentCulture;
            if (results.RangeMin != null)
            {
               
                results.DataCheckText = results.DataCheckText.Replace("@RangeMin",
                                                                      String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                                    results.RangeMin));
            }

            if (results.RangeMax != null)
            {
       
                results.DataCheckText = results.DataCheckText.Replace("@RangeMax",
                                                                      String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                                    results.RangeMax));
            }

            if (results.CompareValue != null)
            {
                results.DataCheckText = results.DataCheckText.Replace("@CompareValue",
                                                                      String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                                    results.CompareValue));
            }

            if (results.CheckValue != null)
            {
                results.DataCheckText = results.DataCheckText.Replace("@CheckValue",
                                                                      String.Format(cultProvider, "{0:N" + decimalPlaces + "}",
                                                                                    results.CheckValue));
            }
        }
    }
}