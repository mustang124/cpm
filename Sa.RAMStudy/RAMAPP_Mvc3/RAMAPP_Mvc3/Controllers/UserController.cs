﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using RAMAPP_Mvc3.ActionFilter;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Linqs;
using RAMAPP_Mvc3.Localization;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Repositories.Cache;

namespace RAMAPP_Mvc3.Controllers
{
    [Authorize]
    [ReplaceCurrencyAndBreadcrumb]
    public class UserController : Controller
    {
        //
        // GET: /User/

        [HttpPost]
        public JsonResult AddSiteUsrs()
        {
            var userinfo = (LogIn)HttpContext.Session["UserInfo"];

            bool result = true;
            var avalUsersLst = new List<object>();

            if (userinfo != null && RouteData.Values["id"] != null)
            {
                int siteDatasetId = Int32.Parse(RouteData.GetRequiredString("id"));
                List<string> newusr = Request["cbLstAvalUsrs"].Split(',').ToList();

                using (var db = new RAMEntities())
                {
                    try
                    {
                        List<LogIn> newusers = (from h in db.LogIn
                                                where newusr.Contains(h.UserID) &&
                                                h.CompanyID == userinfo.CompanyID
                                                select h).ToList();

                        newusers.ForEach(l =>
                        {
                            avalUsersLst.Add(
                                new
                                {
                                    l.UserNo,
                                    UserId = l.UserID,
                                    ScreenName = (l.ScreenName ?? " "),
                                    FirstName = (l.FirstName ?? " "),
                                    LastName = (l.LastName ?? " ")
                                });
                            db.AddToSitePermissions(new SitePermissions
                            {
                                UserID = l.UserID,
                                DatasetID = siteDatasetId
                            });
                        });

                        db.SaveChanges();
                    }
                    catch
                    {
                        result = false;
                    }
                }
            }

            return Json(new { Status = result, UsersList = avalUsersLst }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateUser()
        {
            if (!SessionUtil.IsSessionExpired())
            {
                var userinfo = (LogIn)Session["UserInfo"];
                return View(new UserDetails { CompanyID = userinfo.CompanyID });
            }
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(UserDetails um)
        {
            using (var db = new RAMEntities())
            {
                TempData["UserCreateMsgClass"] = "GoodMsg";
                TempData["UserCreateMsg"] = "The user " + um.Firstname + " " + um.Lastname + " has been created";
                var memProvider = new RAMMembershipProvider();
                MembershipCreateStatus status;

                try
                {
                    um.ScreenName = um.EmailAddress.Split("@".ToCharArray())[0].Trim();
                    um.UserID = um.ScreenName + "/" + SessionUtil.GetCompanySID().Trim();
                    string theCID = RouteData.Values["id"].ToString().Trim();
                    int theNum = 0;
                    bool hasNum = int.TryParse(theCID.Substring(theCID.Length-2), out theNum);
                    if (hasNum)
                    {
                        theCID = theCID.Substring(0, theCID.Length - 2);
                    }
                    memProvider.CreateUser(theCID, um.UserID, um.Password.Trim(), um.Role, um.Firstname.Trim(),
                        um.Lastname.Trim(), um.ScreenName, (um.Culture ?? "en"), um.EmailAddress.Trim(), um.JobTile != null ? um.JobTile.Trim() : "", um.Telephone != null ? um.Telephone : "",
                                           out status);

                    foreach (
                        string key in
                            HttpContext.Request.Form.AllKeys.Where(k => k.Contains("sltPerm_") && Request[k] != "-1").ToArray())
                    {
                        var perm = new SitePermissions
                        {
                            DatasetID = Convert.ToInt32(key.Replace("sltPerm_", "")),
                            SecurityLevel = Convert.ToInt16(HttpContext.Request.Form[key]),
                            UserID = um.UserID
                        };
                        db.AddToSitePermissions(perm);
                    }

                    db.SaveChanges();
                }
                catch
                {
                    TempData["UserCreateMsgClass"] = "ErrMsg";
                    TempData["UserCreateMsg"] = "A system error occurred." + um.Firstname + " " + um.Lastname +
                                                " was not created";
                }
            }

            return View();
        }

        [HttpPost]
        public JsonResult DeleteLogin(int userNo, string userId)
        {
            int ret;
            using (var db = new RAMEntities())
            {
                ret = db.DeleteLogin(userNo, userId);
               // db.SaveChanges();
            }

            return Json(new { result = (ret != -1) ? "The user has been deleted." : "The user was not deleted." }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditUser(string usr)
        {
            if (!SessionUtil.IsSessionExpired())
            {
                var userinfo = (LogIn)Session["UserInfo"];

                if (userinfo != null && usr != null)
                {
                    var provider = new RAMMembershipProvider();

                    try
                    {
                        using (var db = new RAMEntities())
                        {
                         
                            LogIn user = CompiledQryLogin.LoginByID(db, usr.Trim()).FirstOrDefault();
                            if (Session["Mode"] == null && user.CompanyID != userinfo.CompanyID)
                                return View("NotAllowed");

                            var ud = new UserDetails
                            {
                                CompanyID = user.CompanyID,
                                UserNo = user.UserNo,
                                UserID = user.UserID,
                                Firstname = user.FirstName,
                                Lastname = user.LastName,
                                Password = user.Password,
                                EmailAddress = user.Email,
                                Telephone = user.PhoneNo,
                                Culture = user.UserLanguage,
                                JobTile = user.JobTitle,
                                ScreenName = user.ScreenName
                            };

                            return View(ud);
                        }
                    }
                    catch
                    {
                        TempData["UserCreateMsgClass"] = "ErrMsg";
                        TempData["UserCreateMsg"] = "Unable to find user's information.";
                    }
                }
            }

            return View(new UserDetails());
        }

        [HttpGet]
        public JsonResult GetAvailUsrs(short siteId, List<UserDetails> users)
        {
            //Change this  section---------
            var userinfo = (LogIn)Session["UserInfo"];
            var currentUserSecurityLevel = (short)userinfo.SecurityLevel;
            bool isAdmin = SessionUtil.IsAdmin();
            var selectAvalLst = new List<SelectListItem>();

            using (var db = new RAMEntities())
            {
                List<string> usrLst = users.Select(u => u.UserID).ToList();

                selectAvalLst = (from l in db.LogIn
                                 where l.CompanyID == userinfo.CompanyID && l.Active &&
                                       l.SecurityLevel != 2 &&
                                       !db.SitePermissions.Any(p => p.UserID == l.UserID && p.DatasetID == siteId) &&
                                       !usrLst.Contains(l.UserID) &&
                                       (isAdmin || currentUserSecurityLevel >= l.SecurityLevel || l.SecurityLevel == 0) &&
                                       l.UserID != userinfo.UserID
                                 select
                                     new SelectListItem
                                     {
                                         Text =
                                             (l.FirstName != null && l.LastName != null)
                                                 ? l.LastName + " , " + l.FirstName
                                                 : l.ScreenName,
                                         Value = l.UserID
                                     }
                                ).ToList();
            }

            return Json(new { usrAvalLst = selectAvalLst }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ModifyUsrPriv(short siteId, string user, short privOpt)
        {
            string message = "User privileges has been changed.";

            using (var db = new RAMEntities())
            {
                try
                {
                    if (privOpt == 1 || privOpt == 0) //Not allowing  admin priv?
                    {
                        int uNo = Int32.Parse(user);
                        string userID = db.LogIn.FirstOrDefault(l => l.UserNo == uNo).UserID;

                        if (!db.SitePermissions.Any(sp => sp.DatasetID == siteId && sp.UserID == userID.Trim()))
                        {
                            db.AddToSitePermissions(new SitePermissions { DatasetID = siteId, UserID = userID, SecurityLevel = privOpt });
                        }
                        else
                        {
                            db.SitePermissions.FirstOrDefault(p => p.DatasetID == siteId && p.UserID == userID).
                                SecurityLevel = privOpt;
                        }

                        db.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    message = "User site privileges was not changed.";
                }
            }

            return Json(message);
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public JsonResult RemoveSiteUsrs(int id, string usr)
        {
            var userinfo = (LogIn)HttpContext.Session["UserInfo"];
            bool result = false;
            string userName = " ";
            if (userinfo != null)
            {
                using (var db = new RAMEntities())
                {
                    try
                    {
                        int uNo = Int32.Parse(usr);//?UserNo
                        
                        var usrlog = CompiledQryLogin.LoginByUserNo(db,uNo).FirstOrDefault();  
                        userName = (usrlog.FirstName != null && usrlog.LastName != null ? usrlog.FirstName + "  " + usrlog.LastName : usrlog.ScreenName).Trim();
                        SitePermissions removeuser = CompiledQryPermission.GetUserPermissionBySiteID(db, usrlog.UserID.Trim(), id);


                        if (removeuser != null)
                        {
                            db.SitePermissions.DeleteObject(removeuser);
                            db.SaveChanges();
                        }
                        result = true;
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return Json(new
            {
                ElementID = usr,
                Success = result,
                Message = result ? userName + " has been removed from this site." : userName + " was not removed."
            });
        }

        public void ResetPassword(string usr, string email)
        {
            int passwordLength = 12;
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            var chars = new char[passwordLength];
            var rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            var randpwd = new string(chars);
            using (var db = new RAMEntities())
            {
                db.AddToPasswordChangeRequests(new PasswordChangeRequests { UserID = usr, ID = randpwd, RequestTime = DateTime.Now });
                db.SaveChanges();
            }

            if (email != null)
            {
                Smtp.SendMailMessage("Solomon Associates Administrator", System.Configuration.ConfigurationManager.AppSettings["RAMEMailFromAddress"].ToString(),
                                     email,
                                     "",
                                     "",
                                     "Reset RAM Study Password",
                                     "Please click this link to reset your password https://webservices.solomononline.com/RAMStudy/Account/ResetPassword/" +
                                     randpwd + Environment.NewLine + Environment.NewLine +
                                     "Solomon RAM Study Administrator");
            }
        }

        //Update user information
        [HttpPost]
        public ActionResult UpdateUser(UserDetails um)
        {
            var provider = new RAMMembershipProvider();
            try
            {
                var ln = new LogIn
                {
                    CompanyID = um.CompanyID,
                    UserID = um.UserID,
                    UserNo = um.UserNo,
                    SecurityLevel = um.Role,
                    Email = um.EmailAddress,
                    PhoneNo = um.Telephone,
                    UserLanguage = um.Culture,
                    FirstName = um.Firstname,
                    LastName = um.Lastname,
                    JobTitle = um.JobTile,
                    ScreenName = um.ScreenName
                };

                provider.UpdateUser(ln);
                TempData["UserCreateMsgClass"] = "GoodMsg";
                TempData["UserCreateMsg"] = "User " + um.Firstname + " " + um.Lastname +
                                            " has been updated.";
            }
            catch
            {
                TempData["UserCreateMsgClass"] = "ErrMsg";
                TempData["UserCreateMsg"] = "A system error occurred." + um.Firstname + " " + um.Lastname +
                                            " could  not updated";
            }

            return View("EditUser", um);
        }
    }
}
