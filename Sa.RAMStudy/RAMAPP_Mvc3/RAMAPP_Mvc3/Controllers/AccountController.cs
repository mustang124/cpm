﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using RAMAPP_Mvc3.Models;

namespace RAMAPP_Mvc3.Controllers
{
    [HandleError]
    public class AccountController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }

        public IMembershipService MembershipService { get; set; }

        [Authorize]
        public ActionResult ChangePassword()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                string email = ((RAMAPP_Mvc3.Entity.LogIn)Session["UserInfo"]).Email.Trim();
                if (MembershipService.ChangePassword(email, model.OldPassword, model.NewPassword))
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The old password  entered is incorrect. Please try again.");
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }
        // **************************************
        // URL: /Account/ChangePasswordSuccess
        // **************************************
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginPartial(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ValidateUser(model.EmailAddress, model.Password))
                {
                    FormsService.SignIn(model.EmailAddress, model.RememberMe);
                    var authTicket = new FormsAuthenticationTicket(model.EmailAddress, true, 2880); //persists cookie for 2 days
                    var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                    var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                    if (model.RememberMe)
                        authCookie.Expires = authTicket.Expiration;

                    Response.Cookies.Add(authCookie);
                    
                    return RedirectToAction("LoggedIn", "App", new { email = model.EmailAddress });
                }
                else
                {
                    ModelState.AddModelError("", "The username or password is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View("LogOn", model);
        }
        // **************************************
        // URL: /Account/LogOff
        // **************************************
        public ActionResult LogOff()
        {
            FormsService.SignOut();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOn()
        {
            var cookieAspxFormsAuth = HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
            var m = new LogOnModel();
            if (cookieAspxFormsAuth != null)
            {
                var fat = FormsAuthentication.Decrypt(cookieAspxFormsAuth.Value);
                m.EmailAddress = fat.Name;
                m.RememberMe = fat.IsPersistent;
            }

            return View(m);
        }

        // **************************************
        // URL: /Account/LogOn
        // **************************************
        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (MembershipService.ValidateUser(model.EmailAddress, model.Password))
                {
                    FormsService.SignIn(model.EmailAddress, model.RememberMe);
                    var authTicket = new FormsAuthenticationTicket(model.EmailAddress, true, 2880); //persists cookie for 2 days
                    var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                    var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                    if (model.RememberMe)
                        authCookie.Expires = authTicket.Expiration;

                    Response.Cookies.Add(authCookie);

                   return RedirectToAction("LoggedIn", "App", new { email = model.EmailAddress });
                }
                else
                {
                    ModelState.AddModelError("", "The username or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult Register()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

      
        // **************************************
        // URL: /Account/Register
        // **************************************
        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userinfo = (RAMAPP_Mvc3.Entity.LogIn)this.HttpContext.Session["UserInfo"];
                // Attempt to register the user
                MembershipCreateStatus createStatus = MembershipService.CreateUser(userinfo.CompanyID, model.Email, model.Password, model.FirstName, model.LastName);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsService.SignIn(model.Email, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View(model);
        }

        // **************************************
        // URL: /Account/ChangePassword
        // **************************************
    
        public ActionResult ResetPassword()
        {
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(string id)
        {
            string emailAddress;
            try
            {
                using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
                {
                    var rp = (from h in db.PasswordChangeRequests
                              join l in db.LogIn on h.UserID equals l.UserID
                              where h.ID == id && l.Active
                              select l).FirstOrDefault();

                    if (rp == null)
                        throw new Exception();

                    var crypt = new Encryption.Crypt();
                    var salt = "dog" + rp.ScreenName.Trim() + "butt" + rp.CompanyID.Trim() + "steelers";
                    var oldPswd = crypt.Decrypt(rp.Password.Trim(), salt);
                    emailAddress = rp.Email.Trim();

                    if (MembershipService.ChangePassword(emailAddress, oldPswd, HttpContext.Request.Form["NewPassword"]))
                    {
                        db.DeleteObject(db.PasswordChangeRequests.Single(h => h.ID == id));
                        db.SaveChanges();
                        ViewData["ResetMessageClass"] = "GoodMsg"; //css class
                        ViewData["ResetMessage"] = "Password is now changed. <a style='text-decoration:underline;' href=\"" + Url.Action("LogOn", "Account") + "\">Click here</a> to login.";
                        return View();
                    }
                    else
                    {
                         ViewData["ResetMessageClass"] = "ErrMsg"; //css class
                         ViewData["ResetMessage"] = "Password change was unsuccessful. Please correct the errors and try again.";
                       
                        ModelState.AddModelError("", "Failed to reset your password. Please try again.");
                    }
                }
            }
            catch
            {
                ViewData["ResetMessageClass"] = "ErrMsg";
                ViewData["ResetMessage"] = "Password change was unsuccessful. Please correct the errors and try again.";
                ModelState.AddModelError("", "Failed to reset your password. Please try again.");
            }

            // If we got this far, something failed, redisplay form
            ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
            return View();
        }

        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        
    }
}