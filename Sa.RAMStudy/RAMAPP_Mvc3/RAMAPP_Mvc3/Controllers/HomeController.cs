﻿using System.Web.Mvc;
using RAMAPP_Mvc3.Models;

namespace RAMAPP_Mvc3.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        [OutputCache(Duration = 120, VaryByParam = "None")]
        public ActionResult About()
        {
            return View();
        }

        [OutputCache(Duration = 120, VaryByParam = "None")]
        public ActionResult ContactUs()
        {
            ContactUsModel c = new ContactUsModel();
            return View(c);
        }

        [HttpPost]
        public ActionResult ContactUs(ContactUsModel c)
        {
            var newLine = System.Environment.NewLine;
            if (ModelState.IsValid)
            {
                if (c.emailAddress != null)
                {
                    Smtp.SendMailMessage(c.authorsName, c.emailAddress,
                        System.Configuration.ConfigurationManager.AppSettings["RAMEMailToAddress"].ToString(),
                        "",
                        "",
                        "RAM Inquiry -  " + c.emailSubject,
                        "Company - " + c.companyName + newLine +
                        "Location - " + c.location + newLine +
                        "Question" + newLine +
                        "".PadLeft(100, '-') + newLine +
                        c.emailBody);

                    TempData["EmailStatus"] = "Your email was sent.";
                }
                //return View(c);
            }

            return View(c);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowVideo()
        {
            return View();
        }

        [OutputCache(Duration = 120, VaryByParam = "None")]
        public ActionResult SystemRequirements()
        {
            return View();
        }

        public ActionResult VideoPres()
        {
            return View();
        }

        public ActionResult Webinars()
        {
            return View();
        }
    }
}