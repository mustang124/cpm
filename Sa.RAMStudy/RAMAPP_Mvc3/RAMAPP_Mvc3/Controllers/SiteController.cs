﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RAMAPP_Mvc3.ActionFilter;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Localization;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Repositories.Cache;
using RAMAPP_Mvc3.Navigation;
using System.Web.UI;

namespace RAMAPP_Mvc3.Controllers
{
    [Authorize]
    [ReplaceCurrencyAndBreadcrumb]
    public class SiteController : Controller
    {
        //
        // GET: /Site/

        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CreateSite()
        {
            var m = new SiteModel();
            m.CompanySID = SessionUtil.GetCompanySID();
            return View(m);
        }


        [HttpPost]
        public JsonResult ChangeSiteName(int SiteDatasetID, string Name)
        {
            var userinfo = (LogIn)HttpContext.Session["UserInfo"];
            bool result = false;

            if (userinfo != null)
            {
                using (var db = new RAMEntities())
                {
                    try
                    {
                        db.Dataset_LU.FirstOrDefault(d => d.DatasetID == SiteDatasetID && d.DataLevel == "Site").
                            FacilityName = Name;

                        db.SaveChanges();
                        var suList = new CachedNavigationRepository();
                        suList.ResetList();
                        result = true;
                    }
                    catch (Exception)
                    {
                        // Nothing
                    }
                }
            }

            return Json(new
            {
                Message = result ? "Data has been saved" : "Unable to save data cause of an error.",
                SitesUnits = new NavigationTree().GetNavigation( NavigationTree.NavigationType.Administration)
            });
        }

        [HttpPost]
        public ActionResult CreateSite(SiteModel m)
        {
            bool result = false;
            TempData["CreateSiteMsg"] = "Site " + m.Name + " has been created";

            if (!SessionUtil.IsSessionExpired())
            {
                using (var db = new RAMEntities())
                {
                    try
                    {
                        var userinfo = (LogIn)Session["UserInfo"];

                        int compDataSetId =
                            db.Dataset_LU.Single(c => c.FacilitySID == m.CompanySID && c.DataLevel == "Comp").
                                DatasetID;
                        int? ret =
                            db.AddSite(m.CompanySID, compDataSetId, m.Name, null, null, m.Location, m.Currency,
                                       m.ExchangeRate, userinfo.UserNo, userinfo.UserID, null).Single();

                        result = (ret.HasValue && ret.Value > 0);

                        if (!result)
                            TempData["CreateSiteMsg"] = "Site " + m.Name + " was not created";

                        if (result)
                        {
                            var navRepos = new CachedNavigationRepository();
                            navRepos.ResetList();
                        }
                    }
                    catch
                    {
                        TempData["CreateSiteMsg"] = "System error. Site " + m.Name + " was not created";
                    }
                }
            }

            return View();
        }


        [HttpDelete]
        public JsonResult DeleteSite()
        {
            var userinfo = (LogIn)HttpContext.Session["UserInfo"];
            bool result = false;
            short dataSetId = Int16.Parse(RouteData.GetRequiredString("id"));
            string siteName = Request["sitename"] != null ? Request["sitename"].Trim() : "";

            if (userinfo != null)
            {
                using (var db = new RAMEntities())
                {
                    try
                    {
                        int ret = db.DeleteSite(dataSetId, SessionUtil.GetCompanySID(), siteName, userinfo.UserNo,
                                                userinfo.UserID);

                        result = ret != -1;
                        if (result)
                        {
                            var navRepos = new CachedNavigationRepository();
                            navRepos.ResetList();
                        }
                    }
                    catch (Exception)
                    {
                        //do nothing
                    }
                }
            }

            return Json(new
            {
                Success = result,
                Message = result ? "The site has been deleted." : "The site was not deleted."
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SiteDetail()
        {
            var userinfo = (LogIn)HttpContext.Session["UserInfo"];

            if (userinfo != null && RouteData.Values["id"] != null)
            {
                bool isAdmin = SessionUtil.IsAdmin();
                bool isSiteAdmin = SessionUtil.IsSiteAdmin();
                ViewData["AccessLevel"] = (short)(isAdmin ? 2 : isSiteAdmin ? 1 : 0);

                int siteDatasetId = int.Parse(RouteData.GetRequiredString("id"));
                ViewData["UnitSiteID"] = siteDatasetId;
                string companySID = SessionUtil.GetCompanySID();

                using (var db = new RAMEntities())
                {
                    var cacheNav = new CachedNavigationRepository();
                    var validUser = cacheNav.GetSiteUnitList().Any(c => c.Role >= 1 && c.DatasetID == siteDatasetId);
                    if (!validUser)
                        return View("NotAllowed");

                    //IQueryable<SiteModel> sitem = from s in db.SiteInfo
                    //                              where s.CompanySID == companySID &&
                    //                                    s.DatasetID == siteDatasetId
                    //                              select new SiteModel
                    //                              {
                    //                                  CompanySID = s.CompanySID.Trim(),
                    //                                  DatasetID = s.DatasetID,
                    //                                  Name = s.SiteName.Trim(),
                    //                                  Location = s.Country,
                    //                                  Currency = s.LocalCurrency,
                    //                                  PercentCompleted = 0,
                    //                                  IsActive =
                    //                                      !(db.Reviewable.Any(
                    //                                          r => (r.CompanySID == s.CompanySID) &&
                    //                                               ((r.SiteDatsetID == s.DatasetID &&
                    //                                                 r.DataLevel == "Si") ||
                    //                                                (r.DataLevel == "Co")))),
                    //                                  Users = (from l in db.LogIn
                    //                                           where l.CompanyID == userinfo.CompanyID &&
                    //                                                 ((isAdmin && l.SecurityLevel == 2) ||
                    //                                                  db.SitePermissions.Any(
                    //                                                      p =>
                    //                                                      p.UserID == l.UserID &&
                    //                                                      p.DatasetID == s.DatasetID)) &&
                    //                                                 db.CurrentStudyForUsers.Any(
                    //                                                     cs =>
                    //                                                     cs.CompanySID == companySID &&
                    //                                                     cs.UserID == l.UserID)
                    //                                           select new UserDetails
                    //                                           {
                    //                                               UserNo = l.UserNo,
                    //                                               UserID = l.UserID,
                    //                                               CompanyID = l.CompanyID,
                    //                                               ScreenName = l.ScreenName,
                    //                                               Firstname = l.FirstName,
                    //                                               Lastname = l.LastName,
                    //                                               Password = l.Password,
                    //                                               Role =
                    //                                                   (short)
                    //                                                   ((isAdmin &&
                    //                                                     l.SecurityLevel == 2)
                    //                                                        ? 2
                    //                                                        : db.SitePermissions.
                    //                                                              Any(
                    //                                                                  p1 =>
                    //                                                                  p1.DatasetID ==
                    //                                                                  s.DatasetID &&
                    //                                                                  p1.UserID.
                    //                                                                      Contains
                    //                                                                      (l.
                    //                                                                           UserID
                    //                                                                           .
                    //                                                                           Trim
                    //                                                                           ()) &&
                    //                                                                  p1.
                    //                                                                      SecurityLevel ==
                    //                                                                  1)
                    //                                                              ? 1
                    //                                                              : 0)
                    //                                           }
                    //                                          ),
                    //                                  Units = (from u in db.UnitInfo
                    //                                           where
                    //                                               u.SiteDatasetID == s.DatasetID &&
                    //                                               u.CompanySID == s.CompanySID
                    //                                           select new UnitDetails
                    //                                           {
                    //                                               CompanySID =
                    //                                                   s.CompanySID.Trim(),
                    //                                               UnitName = u.UnitName.Trim(),
                    //                                               PrimaryProductName =
                    //                                                   u.PrimaryProductName.Trim(),
                    //                                               GenericProductName =
                    //                                                   u.GenericProductName.Trim(),
                    //                                               OtherGenericProductName =
                    //                                                    u.GenericProductName.Trim(),
                    //                                               ProcessFamily =
                    //                                                   u.ProcessType.Trim(),
                    //                                               DatasetID = u.DatasetID,
                    //                                               SiteDatasetID = s.DatasetID
                    //                                           })
                    //                              };


                    var sitem = Linqs.CompiledLinqs.SiteDetailsQuery(db, siteDatasetId, companySID,userinfo.CompanyID,isAdmin);
                    return View(sitem.FirstOrDefault());
                }
            }

            return View();
        }

        [HttpGet]
        // [ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_CHAR(string id)
        {
            return LoadView<ST_CHARModel>(id);
        }

        [HttpPost, OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_CHAR(ST_CHARModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        //[ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_COST(string id)
        {
            return LoadView<ST_COSTModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_COST(ST_COSTModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        // [ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_CRAFTCHAR(string id)
        {
            return LoadView<ST_CRAFTCHARModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_CRAFTCHAR(ST_CRAFTCHARModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        //[ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_IDENT(string id)
        {
            return LoadView<ST_IDENTModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_IDENT(ST_IDENTModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        // [ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_INVEN(string id)
        {
            return LoadView<ST_INVENModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_INVEN(ST_INVENModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        //[ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_MAINT(string id)
        {
            return LoadView<ST_MAINTModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_MAINT(ST_MAINTModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        //[ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_MT_TA(string id)
        {
            return LoadView<ST_MT_TAModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_MT_TA(ST_MT_TAModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        //[ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_ORG(string id)
        {
            return LoadView<ST_ORGModel>(id);
        }

        [HttpPost]
        [OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_ORG(ST_ORGModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        //[ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_PROCESS(string id)
        {
            return LoadView<ST_PROCESSModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_PROCESS(ST_PROCESSModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        // [ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_REL(string id)
        {
            return LoadView<ST_RELModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_REL(ST_RELModel m, string id)
        {
            return SaveView(m, id);
        }

        [HttpGet]
        // [ReplaceCurrencyAndBreadcrumb]
        public ActionResult ST_STUDY(string id)
        {
            return LoadView<ST_STUDYModel>(id);
        }

        [HttpPost, OutputCache(Duration = 0, Location = OutputCacheLocation.None, NoStore = true)]
        public JsonResult ST_STUDY(ST_STUDYModel m, string id)
        {
            return SaveView(m, id);
        }

        public int? GetFacilityDataQuality(int datasetId)
        {
            int? grade;
            using (var db = new RAMEntities())
            {
                grade = (int?)(db.PropertyGrades.Select(k => k.DatasetID).Contains(datasetId)
                                    ? db.PropertyGrades.Where(k => k.DatasetID == datasetId).Select(
                                        s => s.Grade).
                                          Average()
                                    : null);
            }

            return grade;
        }


        protected ActionResult LoadView<TModel>(string id) where TModel : class
        {
            if (!SessionUtil.IsSessionExpired() && id != String.Empty)
            {
                short siteOrUnitId = Int16.Parse(id);

                //Called from the repository to make that the object is pulled from the cache
                var repository =
                    (ICachedSiteUnitModelsRepository)new CachedSiteUnitModelsRepository();

                TModel siteOrUnitModel;
                repository.GetModel(out siteOrUnitModel, SessionUtil.GetCompanySID(), siteOrUnitId);

                return View(siteOrUnitModel);
            }

            return View();
        }

        protected JsonResult SaveView<TModel>(TModel m, string id) where TModel : class
        {
            bool result = false;
            string statusMsg = "Sucessfully saved data";
            string companySID;
            int userNo;

            userNo = (Session["UserInfo"] as LogIn).UserNo;

            if (id != null)
            {
                companySID = SessionUtil.GetCompanySID();

                try
                {
                    Type type =
                        Type.GetType("RAMAPP_Mvc3.ModelPropertyHandlers." + typeof(TModel).Name + "PropertyHandler",
                                     true);
                    dynamic hnd = Activator.CreateInstance(type);

                    result = hnd.Save(m, Int32.Parse(id), userNo);
                }
                catch (Exception e)
                {
                    result = false;
                    statusMsg = e.Message;
                }
            }

            return Json(new { Status = result, Msg = statusMsg });
        }

        [HttpGet]
        //public JsonResult GetAvailUsrsNEW(short siteId, List<UserDetails> users)
        public List<SelectListItem> GetAvailUsrsNEW(int siteId, List<UserDetails> users)
        {
            //Change this  section---------
            var userinfo = (LogIn)Session["UserInfo"];
            var currentUserSecurityLevel = (short)userinfo.SecurityLevel;
            bool isAdmin = SessionUtil.IsAdmin();
            var selectAvalLst = new List<SelectListItem>();

            using (var db = new RAMEntities())
            {
                List<string> usrLst = users.Select(u => u.UserID).ToList();

                selectAvalLst = (from l in db.LogIn
                                 where l.CompanyID == userinfo.CompanyID && l.Active &&
                                       l.SecurityLevel != 2 &&
                                       !db.SitePermissions.Any(p => p.UserID == l.UserID && p.DatasetID == siteId) &&
                                       !usrLst.Contains(l.UserID) &&
                                       (isAdmin || currentUserSecurityLevel >= l.SecurityLevel || l.SecurityLevel == 0) &&
                                       l.UserID != userinfo.UserID
                                 select
                                     new SelectListItem
                                     {
                                         Text =
                                             (l.FirstName != null && l.LastName != null)
                                                 ? l.LastName + " , " + l.FirstName
                                                 : l.ScreenName,
                                         Value = l.UserID
                                     }
                                ).ToList();
            }

            //return Json(new { usrAvalLst = selectAvalLst }, JsonRequestBehavior.AllowGet);
            return selectAvalLst;
        }


        
    }
}
