﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.UI.DataVisualization.Charting;
using RAMAPP_Mvc3.ActionFilter;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Helpers;
using RAMAPP_Mvc3.Localization;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Repositories.Cache;
using RAMAPP_Mvc3.Localization;
using NHunspell;

namespace RAMAPP_Mvc3.Controllers
{
    [Authorize]
    [ReplaceCurrencyAndBreadcrumb]
    public class ReviewController : Controller
    {
        //
        // GET: /Validation/

        static SpellEngine spellEngine;

        static public SpellEngine SpellEngine { get { return spellEngine; } }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void ChangeCommentStatus(int id, int dcid, string status)
        {
            using (var db = new RAMEntities())
            {
                var executedQuery = new Linqs.ExecuteStoreQuery();
                executedQuery.ChangeDataCheckStatusQuery(db, id, dcid, status);
            }
        }

        [HttpGet]
        public ActionResult CommentsforDatacheck()
        {
            return View();
        }


        [HttpPost]
        //[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SubmitDataCheckComment(int id, int datacheckID, string add)
        {
            if (add == "Spell Check")
            {
                return Content(SpellCheckComment(Request.Form["DataCheckComment"].Trim()));
            }
            else
            {
                if (SessionUtil.IsSessionExpired() != null)
                {
                    var userinfo = (LogIn)Session["UserInfo"];

                    if (userinfo != null && datacheckID != 0 && id != 0)
                    {
                        string newComment = Request.Form["DataCheckComment"].Trim();

                        if (newComment.Length > 350)
                            newComment = newComment.Substring(0, 350);

                        var cm = new DatacheckComments
                        {
                            UserNo = userinfo.UserNo,
                            DatasetID = id,
                            DatacheckID = datacheckID,
                            Posted = DateTime.Now,
                            CommentText = newComment
                        };

                        using (var db = new RAMEntities())
                        {
                            db.AddToDatacheckComments(cm);
                            db.SaveChanges();
                        }
                    }
                }
                return View();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        [HttpPost]
        public void DeleteDatacheckComment(int dataCheckComentId)
        {
            var user = (LogIn)Session["UserInfo"];
            using (var db = new RAMEntities())
            {
                DatacheckComments o = (from g in db.DatacheckComments
                                       where g.DatacheckCommentID == dataCheckComentId
                                       select g).Single();
                db.DeleteObject(o);
                db.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult DataDetails(int id)
        {
            return View("DataDetails");
        }

        [HttpGet]
        public ActionResult DataSectionSummary(int id, string sectionID)
        {

            const string overallTemplate =
              "<ul  class=\"gridbody\" scale=\"{1}" +
              "\"  pctcm='{2:N1}'><li class=\"gridrowtext site \">{0}" +
              "</li><li class=\"gridrow\"><span class='bar' style='width:{5}%;'>{2:N1}%</span>" +
              "</li><li class=\"gridrow\"><span class=\"bigfont\">{1}" +
              "</span></li> <li class=\"gridrow\">{3}" +
              "</li><li class=\"gridrow\">{4}</li>" +
              "</ul>";
            const string missingTemplate =
            "<ul  class=\"gridbody\" ><li class=\"gridrowtext site \">{0}" +
            "</li><li class=\"gridrowtextlonger\">{1}</li>" +
            "</ul>";

            const string missingheader =
                 "<ul class=\"gridbody\"><li class=\" columndesc gridrowtext\"> Section(s) </li><li class=\"gridrowtextlonger columndesc\"> Questions  </li></ul>";

            const string dchecksTemplate =
              "<ul  class=\"gridbody\" ><li class=\"gridrowtext site \">{0}" +
              "</li><li title=\"{4}\" class=\"gridrow {1}Status\">&nbsp;</li><li class=\"gridrowtextlong\">{2}</li><li class=\"gridrow ViewIcon\"><a href=\"{3}\">&nbsp;</a></li>" +
              "</ul>";

            const string dchecksheader =
                 "<ul class=\"gridbody\"><li class=\" columndesc gridrowtext\"> Section(s) </li><li class=\"gridrow columndesc\"> Status  </li><li class=\"gridrowtextlong columndesc\"> Data Check Description  </li><li class=\"gridrow columndesc\"> Action  </li></ul>";

            const string overallheader =
              "<ul class=\"gridbody\"><li class=\" columndesc gridrowtext\"> Name </li><li class=\"gridrow columndesc\"> Percent Complete </li><li class=\"gridrow columndesc\"> Data Quality </li><li class=\"gridrow columndesc\">Data Quality Issues</li><li class=\"gridrow columndesc\">Issues Pending Review </li></ul><ul>";

            var model = new DataSectionSummaryModel();

            var cachedNavigation = new CachedNavigationRepository();

            var sites = cachedNavigation.GetSiteUnitList().Where(st => st.DatasetID == id || st.UnitsOfSite.Select(ut => ut.DatasetID).Contains(id));

            if (!String.IsNullOrEmpty(sectionID))
                sectionID = sectionID.Trim();

         
            using (var db = new RAMEntities())
            {
               
                var siteUnitInfo = Linqs.CompiledLinqs.DataSectionSummaryQuery(db,id,sites);
                var sit = siteUnitInfo.First();
                var usum = sit.unitsummary.FirstOrDefault();
                var overallRows = new StringBuilder();
                var enClt = ClientCultureInfo.GetEnglishCulture();

                overallRows.Append("<div class=\"gridHorizontal\">" + overallheader);

                if (usum == null)
                {
                    var rlt = sit;
                    Session["ValFacility"] = rlt.SiteName;

                    overallRows.AppendFormat(overallTemplate,
                                                  rlt.SiteName,
                                                  rlt.Grade,
                                                  rlt.PcntComplete,
                                                  rlt.ChecksNC,
                                                  rlt.ChecksNA,
                                                  String.Format(enClt, "{0:N1}", rlt.PcntComplete)
                                        );

                    if (!String.IsNullOrEmpty(sectionID))
                    {
                        var sect = db.DatasetQualityBySection.FirstOrDefault(t => t.DatasetID == id && t.SectionID == sectionID);
                        if (sect != null)
                        {
                            overallRows.AppendFormat(overallTemplate,
                                                          sect.Section,
                                                          sect.Grade,
                                                          sect.PcntComplete,
                                                          sect.ChecksNC,
                                                          sect.ChecksNA,
                                                          String.Format(enClt, "{0:N1}", sect.PcntComplete)
                                                );
                        }
                    }
                }
                else // (siteUnitInfo.Any(o => o.unitsummary.Any(u => u.DatasetID == id)))
                {
                    //var st = siteUnitInfo.First();
                    var rlt = usum;

                    Session["ValFacility"] = sit.SiteName + "&nbsp;&raquo;&nbsp;" + rlt.UnitName;
                    overallRows.AppendFormat(overallTemplate,
                                                  rlt.UnitName,
                                                  rlt.Grade,
                                                  rlt.PcntComplete,
                                                  rlt.ChecksNC,
                                                  rlt.ChecksNA,
                                                  String.Format(enClt, "{0:N1}", rlt.PcntComplete)
                                        );

                    if (!String.IsNullOrEmpty(sectionID))
                    {
                        var sect = db.DatasetQualityBySection.FirstOrDefault(t => t.DatasetID == id && t.SectionID == sectionID);
                        if (sect != null)
                        {
                            overallRows.AppendFormat(overallTemplate,
                                                          sect.Section,
                                                          sect.Grade,
                                                          sect.PcntComplete,
                                                          sect.ChecksNC,
                                                          sect.ChecksNA,
                                                          String.Format(enClt, "{0:N1}", sect.PcntComplete)
                                                );
                        }
                    }
                }

                overallRows.Append("</div>");

                model.Overall = overallRows.ToString();
                overallRows.Clear();

                var missSb = new StringBuilder();
                var dchkSb = new StringBuilder();
                var sectype = usum == null ? "site" : "unit";
                var executedQuery = new Linqs.ExecuteStoreQuery();
                var miss = executedQuery.GetMissingDataQuery(db, id, sectionID).ToArray();//db.ExecuteStoreQuery<MissDataModel>("SELECT * FROM dbo.GetMissingData({0},{1})  ORDER BY SortKey", id, sectionID).ToArray();
                var mlen = miss.Length;

                var dchks = executedQuery.GetDataChecksQuery(db, id, sectionID).ToArray(); // db.ExecuteStoreQuery<DataPropertyDetailModel>("SELECT gd.*,lu.StatusText FROM dbo.GetDatachecks({0},{1},0) gd join dbo.DatacheckStatus_LU lu on lu.StatusCode=gd.StatusCode Order By RowNumber", id, sectionID).ToArray();
                var dclen = dchks.Length;
                var maxlen = Math.Max(mlen, dclen);


                var path = Url.RouteUrl("DataCheckDetail", new RouteValueDictionary(new Dictionary<string, object> { { "id", id } }));
                for (var b = 0; b < maxlen; b++)
                {
                    //Build missing grid
                    if (b < mlen)
                    {
                        if (b == 0)
                            missSb.Append("<div class=\"gridHorizontal\">" + missingheader);
                        var u = miss[b];

                        missSb.Append("<ul  class=\"gridbody\" ><li class=\"gridrowtext site \">" + u.Section.Trim() +
               "</li><li class=\"gridrowtextlonger\">" + u.FriendlyName.Trim() + "</li>" +
               "</ul>");
                        if (b == mlen - 1)
                            missSb.Append("</div>");
                    }

                    //Build datacheck grid
                    if (b < dclen)
                    {
                        if (b == 0)
                            dchkSb.Append("<div class=\"gridHorizontal\">" + dchecksheader);
                        var u = dchks[b];

                        dchkSb.Append("<ul  class=\"gridbody\" ><li class=\"gridrowtext site \">" + u.SectionName.Trim() +
               "</li><li title=\"" + u.StatusText.Trim() + "\" class=\"gridrow " + u.StatusCode.Trim() + "Status\">&nbsp;</li><li class=\"gridrowtextlong\">" + u.DataCheckName.Trim() + "</li><li class=\"gridrow ViewIcon\"><a href=\"" + path + "?datacheckId=" + u.DatacheckID + "&sectiontype=" + sectype + "\">&nbsp;</a></li>" +
               "</ul>");

                        if (b == dclen - 1)
                            dchkSb.Append("</div>");
                    }
                }

                if (mlen > 0)
                    model.Missing = missSb.ToString();

                if (dclen > 0)
                    model.Datachecks = dchkSb.ToString();
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult DataSectionSummary2(int id, string sectionID)
        {
            var model = new DataSectionSummaryModel2();
            using (var db = new RAMEntities())
            {
                var cachedNavigation = new CachedNavigationRepository();
                var overallCol = new List<OverallModel>();
                //var siteUnitInfo = (from h in db.Dataset_LU
                //                    join h2 in db.DatasetQuality on h.DatasetID equals h2.DatasetID
                //                    where h.DatasetID == id
                //                    select new
                //                    {
                //                        h.DatasetID,
                //                        h.FacilityName,
                //                        h.ParentID,
                //                        h.DataLevel,
                //                        h2.Grade,
                //                        h2.PcntComplete,
                //                        h2.ChecksNC,
                //                        h2.ChecksNA
                //                    });
                var siteUnitInfo = Linqs.CompiledLinqs.DataSectionSummary2Query(db, id);
                var sit = siteUnitInfo.First();
                
                var facname = (sit.ParentID != null ? db.Dataset_LU.First(j => j.DatasetID == sit.ParentID).FacilityName + "&nbsp;&raquo;&nbsp;" : "") + sit.FacilityName;

                Session["ValFacility"] = facname;
                overallCol.Add(new OverallModel
                {
                    name = facname,
                    Grade = sit.Grade ,
                    PcntComplete = sit.PcntComplete ,
                    ChecksNC = sit.ChecksNC ,
                    ChecksNA = sit.ChecksNA
                });

                if (!String.IsNullOrEmpty(sectionID))
                {
                    var sect = db.DatasetQualityBySection.FirstOrDefault(t => t.DatasetID == id && t.SectionID == sectionID);
                    if (sect != null)
                    {
                        overallCol.Add(new OverallModel
                        {
                            name = sect.Section,
                            Grade = sect.Grade ?? 0,
                            PcntComplete = sect.PcntComplete ?? 0,
                            ChecksNC = sect.ChecksNC ?? 0,
                            ChecksNA = sect.ChecksNA ?? 0
                        });
                    }
                }

                model.Overall = overallCol;
                var executedQuery = new Linqs.ExecuteStoreQuery();
                var missingCol = executedQuery.GetMissingDataQuery(db, id, sectionID);//db.ExecuteStoreQuery<MissDataModel>("SELECT * FROM dbo.GetMissingData({0},{1})  ORDER BY SortKey", id, sectionID).ToList();
                var mlen = missingCol.Count();
                model.Missing = missingCol;

                var dchksCol = executedQuery.GetDataChecksQuery(db, id, sectionID);//db.ExecuteStoreQuery<DataPropertyDetailModel>("SELECT gd.*,lu.StatusText FROM dbo.GetDatachecks({0},{1},0) gd join dbo.DatacheckStatus_LU lu on lu.StatusCode=gd.StatusCode Order By RowNumber", id, sectionID).ToList();
                Session["saNAList" + id] = dchksCol.Where(gh => (gh.StatusCode.Trim() == "NA") || (gh.StatusCode.Trim() == "NC")).ToList();
                var dclen = dchksCol.Count();
                model.Datachecks = dchksCol;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult DataSummaryFurtherDetails(int id, string level)
        {
            var enClt = ClientCultureInfo.GetEnglishCulture();
            const string innerTemplate2 = "<li class='nocursor' ><ul  class=\"gridbody\" scale=\"{1}" +
                                "\"  pctcm='{2:N1}'><li class=\"gridrowtext\"  style=\"text-indent:60px;\">{0}</li><li class=\"gridrow\">" + "<span class='bar' style='width:{7}%;'>{2:N1}%</span>" +
                                "</li><li class=\"gridrow\"><span class=\"bigfont\">{1}" +
                                "</span></li> <li class=\"gridrow\">{3}" +
                                "</li><li class=\"gridrow\">{4}</li><li class=\"gridrow ViewIcon \">{5}</li>" +
                                "</ul></li>";

            var sbldInr = new StringBuilder();

            using (var db = new RAMEntities())
            {
                //var sectionSummary = (
                //                       from us in db.DatasetQualityBySection
                //                       join sc in db.QuestionSection on us.Section.Trim() equals sc.Name.Trim()
                //                       where us.DatasetID == id && sc.DataLevel.ToLower().Equals(level)
                //                       orderby us.SectionSortKey descending
                //                       select new DataValidationSectionModel
                //                       {
                //                           Section = us.Section,
                //                           QuestionNo = us.QuestionCount ?? 0,
                //                           PcntComplete = us.PcntComplete ?? 0,
                //                           MissingData = us.MissingData ?? 0,
                //                           Grade = us.Grade ?? 0,
                //                           DatasetID = us.DatasetID,
                //                           ChecksOK = us.ChecksOK ?? 0,
                //                           ChecksNC = us.ChecksNC ?? 0,
                //                           ChecksNA = us.ChecksNA ?? 0,
                //                           ChecksIN = us.ChecksIN ?? 0,
                //                           SectionID = us.SectionID.Trim()
                //                       }
                //                         );
                var sectionSummary = Linqs.CompiledLinqs.DataSummaryFurtherDetailsQuery(db, id, level);
                var arr = sectionSummary.ToArray();
                var inlen = arr.Length;
                sbldInr.Append("<ul>");
                if (inlen == 0)
                {
                    sbldInr.Append("There are not any units. ");
                }
                for (var z = inlen - 1; z >= 0; z--)
                {
                    var st = arr[z];
                    var executedQuery = new Linqs.ExecuteStoreQuery();
                    var dchks = executedQuery.GetDataChecksQuery(db, st.DatasetID, st.SectionID).ToArray();
                    sbldInr.AppendFormat(innerTemplate2, st.Section, st.Grade, st.PcntComplete, st.ChecksNC, st.ChecksNA, "<a href='" +
                    //sbldInr.AppendFormat(innerTemplate2, st.Section, st.Grade, st.PcntComplete, dchks.Count(), st.ChecksNA, "<a href='" +
                                        Url.Action("DataSectionSummary2", "Review", new { id = st.DatasetID, sectionID = st.SectionID.Trim() }) +
                                         "'>&nbsp;</a>", st.DatasetID, String.Format(enClt, "{0:N1}", st.PcntComplete));
                }

                sbldInr.Append("</ul>");
            }
            return Content(sbldInr.ToString());
        }

        [HttpGet]
        //public ActionResult DataValidationSummary()
        public ActionResult DataInputReviewSummary()
        {
            var glob = (RAMAPP_Mvc3.Localization.ClientCultureInfo)Session["globObj"];
            glob.SetClientCultureInfo();

            const string overallTemplate = "<li class=\"rightPointerNav level1\" dSetId={6}><ul  class=\"gridbody\" scale=\"{1}" +
                                 "\"  pctcm='{2:N1}' ><li class=\"gridrowtext site \">{8}{0}" +
                                 "</li><li class=\"gridrow\"><span class='bar' style='width:{7:N1}%;'>{2:N1}%</span>" +
                                 "</li><li class=\"gridrow\"><span class=\"bigfont\">{1}" +
                                 "</span></li> <li class=\"gridrow\">{3}" +
                                 "</li><li class=\"gridrow\">{4}</li>" +
                                 "<li class=\"gridrow ViewIcon\">" +
                                 "{5}" + "</li> </ul><div class='unitsContainer'><div style=\"margin:0 auto;\"> Loading Site Overall and Units Data ..</div>InnerTable{6}</div></li>";

            var innerTable = new StringBuilder();
            var cachedNavigation = new CachedNavigationRepository();
            var lockimg = "<img alt=\"locked\" src=\"" + Url.Content("~/images") + "/lockedsym.png\" />";
            List<SiteUnits> sites = cachedNavigation.GetSiteUnitList();
            var myList = new StringBuilder();
            myList.Append("<div class=\"gridHorizontal\">");
            myList.Append(
                "<ul class=\"gridbody\"><li class=\" columndesc gridrowtext\"> Site </li><li class=\"gridrow columndesc\"> Percent Complete </li><li class=\"gridrow columndesc\"> Data Quality </li><li class=\"gridrow columndesc\">Data Quality Issues</li><li class=\"gridrow columndesc\">Issues Pending Review</li><li class=\"gridrow columndesc\"> Action</li></ul><ul>");

            using (var db = new RAMEntities())
            {
                var siteUnitInfo = (from h in sites
                                    join d in db.DatasetQualitySiteSummary on h.DatasetID equals d.DatasetID
                                    orderby d.Grade descending
                                    let units = h.UnitsOfSite
                                    select new DataValidationSummaryModel
                                    {
                                        DatasetID = h.DatasetID,
                                        FacilityName = h.SiteName,
                                        PcntComplete = d.PcntComplete ?? 0,
                                        Grade = d.Grade ?? 0,
                                        ChecksNC = d.ChecksNC ?? 0,
                                        ChecksNA = d.ChecksNA ?? 0,
                                        ChecksIN = d.ChecksIN ?? 0,
                                        MissingData = d.MissingData ?? 0,
                                        LockedImg = h.LockedImg 
                                    }).OrderByDescending(o => o.Grade).ToArray();

                var sLen = siteUnitInfo.Length;
                var enClt = ClientCultureInfo.GetEnglishCulture();
                for (var n = sLen - 1; n >= 0; n--)
                {
                    var g = siteUnitInfo[n];

                    myList.AppendFormat(overallTemplate,
                                  g.FacilityName,
                                  g.Grade,
                                  g.PcntComplete,
                                  g.ChecksNC,
                                  g.ChecksNA,
                                   ((true)
                                  ? "<a  href='" +
                                      Url.Action("DataSectionSummary2", "Review", new { id = g.DatasetID, sectionID = "" }) +
                                    "?start=1&size=20&name=" + g.FacilityName + "'>&nbsp;</a>"
                                   : ""), g.DatasetID, String.Format(enClt, "{0:N1}", g.PcntComplete), g.LockedImg );

                    innerTable.Append("<ul>");
                    innerTable.Append("</ul>");
                    myList = myList.Replace("InnerTable" + g.DatasetID, innerTable.ToString());
                    innerTable.Clear();
                }

                myList.Append("</ul></div>");
                TempData["CompanySummary"] = myList.ToString().Replace("#lockimg#", lockimg);
            }

            return View();
        }

        [HttpGet]
        public ActionResult DataValidationSummaryInner(int id)
        {
           
            const string innerTemplate = "<li class='rightPointerNav level2' dsid={6} level='{8}'><ul  class=\"gridbody\" scale=\"{1}" +
                             "\"  pctcm='{2:N1}'><li class=\"gridrowtext\" style=\"text-indent:30px;\">{0}</li><li class=\"gridrow\">" + "<span class='bar' style='width:{7}%;'>{2:N1}%</span>" +
                             "</li><li class=\"gridrow\"><span class=\"bigfont\">{1}" +
                             "</span></li> <li class=\"gridrow\">{3}" +
                             "</li><li class=\"gridrow\">{4}</li>" +
                             "<li class=\"gridrow ViewIcon\">" +
                             "{5}</li> </ul><div class='unitsContainer'></div></li>";



            var sbld = new StringBuilder();
            //var cachedNavigation = new CachedNavigationRepository();

            using (var db = new RAMEntities())
            {
                //var myLst = new CachedNavigationRepository().GetSiteUnitList().Where(f => f.DatasetID == id).AsQueryable();

          //      var dataQuality = (
          //from dq in db.DatasetQuality
          //join dlu in db.Dataset_LU on dq.DatasetID equals dlu.DatasetID
          //where (dq.DatasetID == id || dlu.ParentID == id) && dlu.Deleted == false
          //select new DataValidationSummaryModel
          //{
          //    FacilityName = dlu.FacilityName,
          //    DatasetID = dq.DatasetID,
          //    Grade = dq.Grade ?? 0,
          //    PcntComplete = dq.PcntComplete ?? 0,
          //    ChecksNC = dq.ChecksNC ?? 0,
          //    ChecksNA = dq.ChecksNA ?? 0
          //}
          //);
                var dataQuality = Linqs.CompiledLinqs.DataValidationSummaryInnerQuery(db, id);
                var enClt = ClientCultureInfo.GetEnglishCulture();

                ////Append Units and Unit Sections
                var dqrs = dataQuality.ToList();

                foreach (var unt in dqrs)
                {
                    sbld.AppendFormat(innerTemplate, unt.DatasetID == id ? "Site Level Data" : unt.FacilityName,
                                                     unt.Grade,
                                                     unt.PcntComplete,
                                                     unt.ChecksNC,
                                                     unt.ChecksNA,
                                                     "<a href='" + Url.Action("DataSectionSummary2", "Review", new { id = unt.DatasetID, sectionID = "" }) + "'>&nbsp;</a>",
                                                     unt.DatasetID,
                                                     String.Format(enClt, "{0:N1}", unt.PcntComplete),
                                                      unt.DatasetID == id ? "site" : "unit"
                                                     );
                }
                sbld.Append("</ul>");

                return Content(sbld.ToString());
            }
        }



        [HttpGet]
        public ActionResult ViewDataCheckDetail(int id, int datacheckId)
        {
            //var relatedSql = "SELECT * FROM dbo.GetRelatedDataChecks({0},{1}) Order By RowNumber";

            var dcModel = new DataCheckDetailsModel();
            string table1 = "";
            string table2 = "";
            string dataCheckName;
            string datacheckText;
            string statusCode;
            string statusText;
            string statusImage;
            string statusMessage;
            decimal checkValue;
            decimal compareValue;
            decimal rangeMin;
            decimal rangeMax;
            int decimalPlaces;

            using (var db = new RAMEntities())
            {
                var executedQuery = new Linqs.ExecuteStoreQuery();
                var dataRelatedChecks = executedQuery.GetRelatedDataChecksQuery(db, id, datacheckId); //db.ExecuteStoreQuery<RelatedDataChecks>(relatedSql, id, datacheckId).ToList();
                ViewData["DataCheckLists"] = dataRelatedChecks;

                var results = GetDataCheckDetails(id, datacheckId, out table1, out table2, out dataCheckName, out datacheckText, out statusCode, out statusText, out statusImage, out statusMessage, out checkValue, out compareValue, out rangeMin, out rangeMax, out decimalPlaces);

                dcModel.CheckValue = checkValue;
                dcModel.CompareValue = compareValue;
                dcModel.DataCheckName = dataCheckName;
                dcModel.DatacheckText = datacheckText;
                dcModel.RangeMax = rangeMax;
                dcModel.RangeMin = rangeMin;
                dcModel.StatusCode = statusCode;
                dcModel.StatusImage = statusImage;
                dcModel.StatusMessage = statusMessage;
                dcModel.Tables = results;
                ReplaceKeywords(ref dcModel, rangeMin, rangeMax, compareValue, checkValue, decimalPlaces);
            }
            return View(dcModel);
        }

        [HttpGet]
        public ActionResult GetStatusImage(string code)
        {
            var imgStatusCode = "";
            if (code == "GreenCheck" || code == "OK")
                imgStatusCode = Server.MapPath("~/images/Green_Checkmark_sm.png");

            if (code == "RedX" || code == "NC")
                imgStatusCode = Server.MapPath("~/images/Error_sm.png");

            if (code == "YellowCircle" || code == "NA")
                imgStatusCode = Server.MapPath("~/images/Warning_sm.png");

            if (code == "BlackBar" || code == "IN")
                imgStatusCode = Server.MapPath("~/images/BlackBar_sm.png");

            return File(imgStatusCode, "image/png");
        }


        [HttpPost]
        public ActionResult ViewDataCheckDetail(int id)
        {
            string lang = (string)HttpContext.Session["lang"];
            int userNo = ((LogIn)Session["UserInfo"]).UserNo;
            using (var db = new RAMEntities())
            {
                string qry = "";
                HttpContext.Request.Form.AllKeys.Where(k => !k.EndsWith("_Format") && !k.Equals("datacheckId") && Request[k] != null).
                                ToList().ForEach(key =>
                                {
                                    string val = Request[key];
                                    string propName = key.Trim();
                                    string numFormat = Request[propName + "_Format"].Trim();
                                    var enClt = ClientCultureInfo.GetEnglishCulture();
                                    switch (numFormat)
                                    {
                                        //case "String": db.SaveAnswer(id, propName, null, null, val, userNo);
                                        //    break;
                                        case "Number": 
                                            if (val == "")
                                            {
                                                qry += "exec dbo.spSaveAnswer " + id + ",'" + propName + "', null," + " null, null, " + userNo + ";";
                                            }
                                            else
                                            {
                                                if ((lang == "ru") || (lang == "de") || (lang == "it"))
                                                {
                                                    qry += "exec dbo.spSaveAnswer " + id + ",'" + propName + "', null," + String.Format(enClt, "{0:F4}", Convert.ToDecimal((val.Replace(".", "")).Replace(" ", ""))) + ", null," + userNo + ";";
                                                }
                                                else
                                                {
                                                    qry += "exec dbo.spSaveAnswer " + id + ",'" + propName + "', null," + val.Replace(",", "") + ", null," + userNo + ";";
                                                }
                                            }
                                            break;
                                        case "Int":
                                            if ((lang == "ru") || (lang == "de") || (lang == "it"))
                                            {
                                                qry += "exec dbo.spSaveAnswer " + id + ",'" + propName + "'," + String.Format(enClt, "{0:d}", Convert.ToInt32((val.Replace(".", "")).Replace(" ", ""))) + ", null, null," + userNo + ";";
                                            }
                                            else 
                                            {
                                                qry += "exec dbo.spSaveAnswer " + id + ",'" + propName + "'," + val.Replace(",", "") + ", null, null," + userNo + ";";
                                            }
                                            break;
                                        default: db.SaveAnswer(id, propName, null, null, val, userNo);
                                            break;

                                    }
                                });
                db.ExecuteStoreCommand(qry);
                while (!DataCheckStatus(db, id).Equals(0))
                {
                    //nothing
                }
            }

            return RedirectToAction("ViewDataCheckDetail", new { id = id, datacheckId = Convert.ToInt32(Request["datacheckId"]), retmsg = "Validation has been updated." });
        }

        [HttpGet]
        public ActionResult ViewGradeDetails(int id, int start = -1, int size = -1)
        {
            var myList = new StringBuilder();
            string propertyName;
            string friendlyName;
            string dataType;
            decimal numberValue;
            string strValue;
            string inputField;
            string sectionName;
            string subSectionName;
            string subSectionDesc;
            string UOM;
            short decimalPlaces;
            //var cult = new ClientCultureInfo();

            CultureInfo cultProvider = Thread.CurrentThread.CurrentCulture;
            int total = 0;

            myList.Append("<div class=\"gridHorizontal\">");
            myList.Append(
                "<ul class=\"gridbody\"><li class=\"gridrowtext columndesc\">  Question </li><li class=\"gridrow columndesc\"> Section </li><li class=\"gridrow columndesc\"> Value </li><li class=\"gridrow columndesc\"> Quality of Data </li><li class=\"gridrow columndesc\"> No. of Errors </li><li class=\"gridrow columndesc\">Needs Comment </li><li class=\"gridrow columndesc\"> Action</li></ul><ul>");

            if (Request["name"] != null)
                Session["ValidateFacility"] = Request["name"];

            if (Session["PageSize"] != null || Request["size"] != null)
            {
                if (Request["size"] != null)
                {
                    size = Convert.ToInt32(Request["size"]);
                    Session["PageSize"] = size;
                }
                else
                    size = (int)Session["PageSize"];
            }

            //using (var db = new RAMEntities())
            //{
            var results = null as List<DataValidationGradeModel>;

            results = GetGrades(id, out total);

            if (start > 0 && size > 0)
                results = GetGrades(id, out total, start, size);

            int idx = start;

            if (idx == -1)
                idx = 1;

            foreach (DataValidationGradeModel detail in results)
            {
                List<DataPropertyDetailModel> errDetails = GetPropertyValidationDetails(id, detail.PropertyNo, out propertyName,
        out friendlyName,
        out  dataType,
        out numberValue,
        out strValue,
        out inputField,
        out sectionName,
        out subSectionName,
        out subSectionDesc,
        out UOM,
        out decimalPlaces);

                if (detail.ChecksNC > 0 || detail.ChecksNA > 0)
                    myList.Append("<li id='rowDesc" + (idx) + "' class='rightPointerNav'>");
                else
                    myList.Append("<li>");

                myList.Append("<ul class=\"gridbody\" scale=\"" + detail.Grade +
                              "\"> <li class=\"gridrowtext\"><span class='numbering'>" + (idx) + ". </span> &nbsp;" +
                              (detail.MissingData > 0 ? "<font color='#99100'>MISSING: </font>" : "") +
                              detail.FriendlyName + "</li><li class=\"gridrow\">" + detail.Section +
                              "</li><li class=\"gridrow\">" +
                              (detail.strValue == null
                                   ? String.Format(cultProvider, "{0:N}", detail.numValue)
                                   : detail.strValue) + "</li><li class=\"gridrow\"><span class=\"bigfont\">" +
                              detail.Grade + "</span></li><li class=\"gridrow\">" + (detail.ChecksNC) +
                              "</li><li class=\"gridrow\">" + (detail.ChecksNA > 0 ? " Yes" : "") +
                              "</li><li class=\"gridrow\">" +
                              ((detail.ChecksNC > 0 || detail.ChecksNA > 0)
                                   ? "<a href='" +
                                     Url.Action("ViewPropertyDataCheck", "Admin",
                                                new { id, detail.PropertyNo }) + "'>View</a>"
                                   : "") + "</li> </ul>");

                if (detail.ChecksNC > 0 || detail.ChecksNA > 0)
                {
                    myList.Append("<div class='unitsContainer summaryContainer'><ul class='gridbody summaryDesc'>");

                    foreach (DataPropertyDetailModel m in errDetails)
                    {
                        DataPropertyDetailModel temp = m;
                        ReplaceKeywords(ref temp, (int)decimalPlaces);

                        myList.Append("<li><strong style='text-indent:2;'>" + temp.StatusImage + "&nbsp;" + temp.DataCheckName +
                                      "</strong></br><p class='checkMsg'>" + temp.DataCheckText + "<br><a href='" +
                                      Url.Action("ViewPropertyDataCheck", "Admin",
                                                 new
                                                 {
                                                     id,
                                                     detail.PropertyNo,
                                                     datacheckId = m.DatacheckID
                                                 }) + "'>Read more</a></p></li>");
                    }
                    myList.Append("</ul></div>");
                }

                myList.Append("</li>");
                idx++;
            }
            //}

            myList.Append("</ul></div>");

            myList = myList.Replace("GreenCheck", "<img src='" + Url.Content("~/images/Green_Checkmark_sm.png") + "' runat='server' align='middle'/>");
            myList = myList.Replace("RedX", "<img src='" + Url.Content("~/images/Error_sm.png") + "' runat='server'  align='middle'/>");
            myList = myList.Replace("YellowCircle", "<img src='" + Url.Content("~/images/Warning_sm.png") + "' runat='server'  align='middle'/>");
            myList = myList.Replace("BlackBar", "<img src='" + Url.Content("~/images/BlackBar_sm.png") + "' runat='server'  align='middle'/>");

            TempData["GradeDetails"] = myList.ToString();
            if (total > 0 && size > 0)
                TempData["PageNav"] = GetPagingLinks(id, total, start, size);
            return View();
        }

        [HttpGet]
        public ActionResult ViewPropertyDataCheck(int id, int PropertyNo, int datacheckID = 0)
        {
            var myList = new StringBuilder();

            string propertyName;
            string friendlyName;
            string dataType;
            decimal numberValue;
            string strValue;
            string inputField;
            string sectionName;
            string subSectionName;
            string subSectionDesc;
            string UOM;
            short decimalPlaces;
            //using (var db = new RAMEntities())
            //{
            var results = null as DataPropertyDetailModel;

            List<DataPropertyDetailModel> resultsCol = GetPropertyValidationDetails(id, PropertyNo, out propertyName, out friendlyName, out dataType,
                                         out numberValue, out strValue, out inputField, out sectionName,
                                         out subSectionName, out subSectionDesc, out UOM, out decimalPlaces);

            // QuestionProperties pr = db.QuestionProperties.FirstOrDefault(q => q.PropertyNo == (short)PropertyNo);

            ViewData["Section"] = sectionName.Trim();
            ViewData["SubSection"] = subSectionName.Trim();
            ViewData["SubSectionDesc"] = subSectionDesc.Trim();
            ViewData["Question"] = friendlyName.Trim();
            ViewData["NumberFormat"] = dataType.Trim();
            ViewData["DecimalPlaces"] = decimalPlaces;
            ViewData["PropertyName"] = propertyName.Trim();
            ViewData["PropertyUOM"] = UOM.Trim();
            ViewData["InputField"] = inputField.Trim();

            resultsCol.ToList().ForEach(i =>
            {
                i.DatasetID = id;
                i.PropertyNo = (short)PropertyNo;
            });

            ViewData["DataCheckLists"] = resultsCol.ToList();

            if (datacheckID > 0)
                results = resultsCol.SingleOrDefault(c => c.DatacheckID == datacheckID);
            else
                results = resultsCol.FirstOrDefault();

            ReplaceKeywords(ref results, (int)decimalPlaces);
            if (Request["fromPost"] == null)
                Session["MessageDC"] = "";
            return View(results);
            //}
        }

        [HttpPost]
        public ActionResult ViewPropertyDataCheck(int id, short prpNo)
        {
            try
            {
                using (var db = new RAMEntities())
                {
                    string val = Request["tbAnswer"];
                    string propName = Request["PropertyName"].Trim();
                    string numFormat = Request["NumberFormat"].Trim();
                    int userNo = ((LogIn)Session["UserInfo"]).UserNo;

                    switch (numFormat)
                    {
                        case "String": db.SaveAnswer(id, propName, null, null, val, userNo);
                            break;
                        case "Number": db.SaveAnswer(id, propName, null, Convert.ToDecimal(val), null, userNo);
                            break;
                        case "Int": db.SaveAnswer(id, propName, Convert.ToInt32(val), null, null, userNo);
                            break;
                    }

                    Session["MessageDC"] = "The value has been updated.";
                    //wait til checks have been updated
                    while (!DataCheckStatus(db, id).Equals(0))
                    {
                    }
                }

                //return RedirectToAction("ViewGradeDetails", new { id = id,start=1,size=20,name="WHAT" });
            }
            catch
            {
                Session["MessageDC"] = "The value was not updated.";
            }
            return RedirectToAction("ViewPropertyDataCheck", new { id = id, PropertyNo = prpNo, fromPost = true });
        }

        private string DataValidationSectionalView(IEnumerable<dynamic> siteUnitInfo)
        {
            var myList = new StringBuilder();
            myList.Append("<div class=\"gridHorizontal\">");
            myList.Append(
                "<ul class=\"gridbody\"><li class=\" columndesc gridrowtext\"> Site </li><li class=\"gridrow columndesc\"> % Complete </li><li class=\"gridrow columndesc\"> Quality of Data </li><li class=\"gridrow columndesc\">No. of Data Quality Issues</li><li class=\"gridrow columndesc\">No. of Pending Review Comment </li><li class=\"gridrow columndesc\"> Action</li><li class=\"gridrow columndesc\"> Submit Data</li></ul><ul>");
            foreach (var grades in siteUnitInfo)
            {
                if (((IEnumerable<dynamic>)grades.unitsummary).Any())
                    myList.Append("<li class='rightPointerNav'>");
                else
                    myList.Append("<li>");

                if (grades.Grade != null)
                    myList.Append("<ul  class=\"gridbody\" scale=\"" + grades.Grade +
                                  "\"><li class=\"gridrowtext site\">" + grades.SiteName +
                                  "</li><li class=\"gridrow\">" + grades.PcntComplete +
                                  "</li><li class=\"gridrow\"><span class=\"bigfont\">" + grades.Grade +
                                  "</span></li> <li class=\"gridrow\">" + grades.ChecksNC +
                                  "</li><li class=\"gridrow\">" + grades.ChecksNA + "</li>" +
                                  "<li class=\"gridrow\">" +
                                  ((true)
                                       ? "<a href='" +
                                         Url.Action("ViewGradeDetails", "Admin", new { id = grades.DatasetID }) +
                                         "?start=1&size=20&name=" + grades.SiteName + "'>View</a>"
                                       : "") + "</li><li class=\"gridrow\">" +
                                  (grades.Grade > 95 && grades.unitsummary.Any()
                                       ? "<a href='" +
                                         Url.Action("ApproveData", "Admin",
                                                    new { id = grades.DatasetID, name = grades.SiteName }) +
                                         "'> Submit</a>"
                                       : "") + "</li> </ul>");
                else
                    myList.Append("<ul  class=\"gridbody\"><li class=\" site gridrowtext\"> " + grades.SiteName +
                                  " </li><li class=\"gridrow\"> - </li><li class=\"gridrow\"> - </li> <li class=\"gridrow\"> - </li><li class=\"gridrow\"> - </li><li class=\"gridrow\"> </li><li class=\"gridrow\"> </li> </ul>");

                myList.Append("<div class='unitsContainer'>");

                if (!((IEnumerable<dynamic>)grades.unitsummary).Any())
                {
                    myList.Append("Please add units.");
                }
                else
                {
                    foreach (var child in grades.unitsummary)
                    {
                        if (child.Grade != null)
                        {
                            myList.Append("<ul class=\"gridbody\" scale=\"" + child.Grade +
                                          "\"><li class=\" unit gridrowtext\"> &nbsp;&nbsp;&nbsp;" +
                                          child.UnitName + "</li>" +
                                          "<li class=\"gridrow\">" + child.PcntComplete +
                                          "</li> <li class=\"gridrow\"><span class=\"bigfont\">" + child.Grade +
                                          "</span></li><li class=\"gridrow\">" + child.ChecksNC +
                                          "</li><li class=\"gridrow\">" + child.ChecksNA +
                                          "</li>" +
                                          "<li class=\"gridrow\">" +
                                          ((true)
                                               ? "<a href='" +
                                                 Url.Action("ViewGradeDetails", "Admin", new { id = child.DatasetID }) +
                                                 "?start=1&size=20&name=" + child.UnitName + "'>View</a>"
                                               : "") + "</li></ul>");
                        }
                    }
                }

                myList.Append("</div></li> ");
            }

            myList.Append("</ul></div>");
            return myList.ToString();
        }

        #region "Validation Helpers"
        private byte DataCheckStatus(RAMEntities db, int datasetID)
        {
            var qry = "SELECT dbo.DatasetProcessStatus({0})";

            var status = db.ExecuteStoreQuery<byte>(qry, datasetID).Single();
            return status;
        }

        //private SiteInfo GetSite(RAMEntities db, int siteDatasetId)
        //{
        //    return db.SiteInfo.FirstOrDefault(s => s.DatasetID == siteDatasetId);
        //}


        private List<DataValidationGradeModel> GetGrades(int id, out int total, int start = -1, int size = -1)
        {
            const string qry = "exec dbo.GetPropertyGrades {0},99;";
            var myList = new StringBuilder();
            var results = null as List<DataValidationGradeModel>;
            var gradesKey = "grades" + id;
            total = 0;

            if (Session[gradesKey] == null)
            {
                using (var db = new RAMEntities())
                {
                    results = db.ExecuteStoreQuery<DataValidationGradeModel>(qry, new object[] { id }).ToList();
                    Session[gradesKey] = results;
                }
            }
            else
            {
                results = Session[gradesKey] as List<DataValidationGradeModel>;
            }

            if (results != null)
            {
                total = results.Count();

                if (start > -1 && size > -1)
                {
                    if (size > total)
                        size = total;

                    if (size > (total - start))
                        size = (total - start);

                    return results.GetRange(start, size);
                }
            }
            return results;
        }

        private List<DataCheckTables> GetDataCheckDetails(int id, int datacheckid, out string table1, out string table2,
            out string dataCheckName,
  out string datacheckText,
  out string statusCode,
  out string statusText,
  out string statusImage,
  out string statusMessage,
  out decimal checkValue,
  out decimal compareValue,
  out decimal rangeMin,
  out decimal rangeMax,
  out int decimalPlaces)
        {
            table1 = "";
            table2 = "";

            string qry = "exec dbo.GetDataCheckDetails @p0,@p1,@p2 out ,@p3 out,@p4 out,@p5 out,@p6 out,@p7 out, @p8 out,@p9 out,@p10 out,@p11 out,@p12 out";
            var pId = new SqlParameter { ParameterName = "p0", Value = id, Direction = ParameterDirection.InputOutput };
            var pCheckId = new SqlParameter { ParameterName = "p1", Value = datacheckid, Direction = ParameterDirection.InputOutput };
            var pCheckName = new SqlParameter { ParameterName = "p2", SqlDbType = SqlDbType.VarChar, Size = 150, Direction = ParameterDirection.Output };
            var pCheckText = new SqlParameter { ParameterName = "p3", SqlDbType = SqlDbType.VarChar, Size = 4000, Direction = ParameterDirection.Output };
            var pStatusCode = new SqlParameter { ParameterName = "p4", SqlDbType = SqlDbType.VarChar, Size = 20, Direction = ParameterDirection.Output };
            var pStatusText = new SqlParameter { ParameterName = "p5", SqlDbType = SqlDbType.VarChar, Size = 50, Direction = ParameterDirection.Output };
            var pStatusImage = new SqlParameter { ParameterName = "p6", SqlDbType = SqlDbType.VarChar, Size = 35, Direction = ParameterDirection.Output };
            var pStatusMessage = new SqlParameter { ParameterName = "p7", SqlDbType = SqlDbType.NVarChar, Size = 225, Direction = ParameterDirection.Output };
            var pCheckValue = new SqlParameter { ParameterName = "p8", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pCompareValue = new SqlParameter { ParameterName = "p9", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pRangeMin = new SqlParameter { ParameterName = "p10", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pRangeMax = new SqlParameter { ParameterName = "p11", SqlDbType = SqlDbType.Float, Direction = ParameterDirection.Output };
            var pDecimalPlaces = new SqlParameter { ParameterName = "p12", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };
            //var myList = new StringBuilder();

            using (var db = new RAMEntities())
            {
                //var results = null as DataPropertyDetailModel;
                List<DataCheckTables> resultsCol = db.ExecuteStoreQuery<DataCheckTables>(qry,
                                                                new object[]
                                                                      {
                                                                         pId, pCheckId , pCheckName ,
                                                                         pCheckText ,pStatusCode ,
                                                                         pStatusText ,pStatusImage ,
                                                                         pStatusMessage ,pCheckValue  ,
                                                                         pCompareValue ,pRangeMin ,pRangeMax,pDecimalPlaces
                                                                      }).ToList();

                dataCheckName = pCheckName.Value != DBNull.Value ? (string)pCheckName.Value : "";
                datacheckText = pCheckText.Value != DBNull.Value ? (string)pCheckText.Value : "";
                statusCode = pStatusCode.Value != DBNull.Value ? (string)pStatusCode.Value : "";
                statusText = pStatusText.Value != DBNull.Value ? (string)pStatusText.Value : "";
                statusImage = pStatusImage.Value != DBNull.Value ? (string)pStatusImage.Value : "";
                statusMessage = pStatusMessage.Value != DBNull.Value ? (string)pStatusMessage.Value : "";
                checkValue = pCheckValue.Value != DBNull.Value ? Convert.ToDecimal(pCheckValue.Value) : 0;
                compareValue = pCompareValue.Value != DBNull.Value ? Convert.ToDecimal(pCompareValue.Value) : 0;
                rangeMin = pRangeMin.Value != DBNull.Value ? Convert.ToDecimal(pRangeMin.Value) : 0;
                rangeMax = pRangeMax.Value != DBNull.Value ? Convert.ToDecimal(pRangeMax.Value) : 0;
                decimalPlaces = pDecimalPlaces.Value != DBNull.Value ? (int)pDecimalPlaces.Value : 0;

                return resultsCol;
            }
        }


        private string GetPagingLinks(int datasetID, int total, int start, int size)
        {
            double NumOfPg = Math.Ceiling((double)total / size);
            string templt = "<span class='pgLinks'><a href='{0}?start={1}&size={2}'>{3}</a> </span>&nbsp;";
            var links = new StringBuilder();

            links.AppendFormat(templt, Url.Action("ViewGradeDetails", "Review", new { id = datasetID }), 1, size, "First");

            for (int g = 1; g < NumOfPg + 1; g++)
            {
                links.AppendFormat(templt, Url.Action("ViewGradeDetails", "Review", new { id = datasetID }),
                                   ((g * size) - size) + 1, size, g);
            }

            links.AppendFormat(templt, Url.Action("ViewGradeDetails", "Review", new { id = datasetID }),
                               ((NumOfPg * size) - size) + 1, size, "Last");
            return links.ToString();
        }

        private List<DataPropertyDetailModel> GetPropertyValidationDetails(int id, int propertyNo, out string propertyName,
                    out string friendlyName,
                    out string dataType,
                    out decimal numberValue,
                    out string strValue,
                    out string inputField,
                    out string sectionName,
                    out string subSectionName,
                    out string subSectionDesc,
                    out string UOM,
                    out short decimalPlaces,
                    int datacheckID = 0)
        {
            string qry = "exec dbo.GetPropertyDataChecks @p0,@p1,@p2 out ,@p3 out,@p4 out,@p5 out,@p6 out,@p7 out, @p8 out,@p9 out,@p10 out,@p11 out,@p12 out";
            //string qry = "exec dbo.GetPropertyDataChecks {0},{1},{2} OUTPUT,{3} OUTPUT,{4} OUTPUT,{5} OUTPUT,{6} OUTPUT,{7} OUTPUT;";
            var pId = new SqlParameter { ParameterName = "p0", Value = id, Direction = ParameterDirection.InputOutput };
            var pPropNo = new SqlParameter { ParameterName = "p1", Value = propertyNo, Direction = ParameterDirection.InputOutput };
            var pPropName = new SqlParameter { ParameterName = "p2", SqlDbType = SqlDbType.VarChar, Size = 35, Direction = ParameterDirection.Output };
            var pFriend = new SqlParameter { ParameterName = "p3", SqlDbType = SqlDbType.VarChar, Size = 300, Direction = ParameterDirection.Output };
            var pType = new SqlParameter { ParameterName = "p4", SqlDbType = SqlDbType.Char, Size = 10, Direction = ParameterDirection.Output };
            var pNmVal = new SqlParameter { ParameterName = "p5", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Output };
            var pStrVal = new SqlParameter { ParameterName = "p6", SqlDbType = SqlDbType.NVarChar, Size = 225, Direction = ParameterDirection.Output };
            var pFieldHtml = new SqlParameter { ParameterName = "p7", SqlDbType = SqlDbType.VarChar, Size = 8000, Direction = ParameterDirection.Output };
            var pSection = new SqlParameter { ParameterName = "p8", SqlDbType = SqlDbType.VarChar, Size = 45, Direction = ParameterDirection.Output };
            var pSubSection = new SqlParameter { ParameterName = "p9", SqlDbType = SqlDbType.VarChar, Size = 45, Direction = ParameterDirection.Output };
            var pSubSectionDesc = new SqlParameter { ParameterName = "p10", SqlDbType = SqlDbType.VarChar, Size = 8000, Direction = ParameterDirection.Output };
            var pUOM = new SqlParameter { ParameterName = "p11", SqlDbType = SqlDbType.VarChar, Size = 80, Direction = ParameterDirection.Output };
            var pDecimalPlaces = new SqlParameter { ParameterName = "p12", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Output };
            //var myList = new StringBuilder();

            using (var db = new RAMEntities())
            {
                //var results = null as DataPropertyDetailModel;
                List<DataPropertyDetailModel> resultsCol =
                  db.ExecuteStoreQuery<DataPropertyDetailModel>(qry,
                                                                new object[]
                                                                      {
                                                                          pId, pPropNo, pPropName, pFriend,
                                                                          pType, pNmVal, pStrVal, pFieldHtml,pSection,pSubSection,
                                                                          pSubSectionDesc,pUOM,pDecimalPlaces
                                                                      }).
                        ToList();

                propertyName = (string)pPropName.Value;
                friendlyName = (string)pFriend.Value;
                dataType = (string)pType.Value;
                numberValue = pNmVal.Value != DBNull.Value ? (decimal)pNmVal.Value : 0;
                strValue = pStrVal.Value != DBNull.Value ? (string)pStrVal.Value : "";
                inputField = pFieldHtml.Value != DBNull.Value ? (string)pFieldHtml.Value : "";
                sectionName = pSection.Value != DBNull.Value ? (string)pSection.Value : "";
                subSectionName = pSubSection.Value != DBNull.Value ? (string)pSubSection.Value : "";
                subSectionDesc = pSubSectionDesc.Value != DBNull.Value ? (string)pSubSectionDesc.Value : "";
                UOM = pUOM.Value != DBNull.Value ? (string)pUOM.Value : "";
                decimalPlaces = pDecimalPlaces.Value != DBNull.Value ? (short)pDecimalPlaces.Value : (short)0;

                return resultsCol;
            }
        }



        private void ReplaceKeywords(ref DataCheckDetailsModel results, decimal? rangeMin, decimal? rangeMax, decimal? compareVal, decimal? checkVal, int decimalPlaces)
        {
            if (results == null)
                return;

            var cult = new ClientCultureInfo();
            CultureInfo cultProvider = Thread.CurrentThread.CurrentCulture;
            results.DatacheckText = ReplaceKeyword(results.DatacheckText, rangeMin, rangeMax, compareVal, checkVal, cultProvider, decimalPlaces);
           
        }

        private void ReplaceKeywords(ref DataPropertyDetailModel results, int decimalPlaces)
        {
            if (results == null)
                return;

            if (decimalPlaces == 0)
                decimalPlaces = 2;



            var cult = new ClientCultureInfo();
            CultureInfo cultProvider = Thread.CurrentThread.CurrentCulture;
            results.DataCheckText = ReplaceKeyword(results.DataCheckText, results.RangeMin, results.RangeMax, results.CompareValue, results.CheckValue, cultProvider, decimalPlaces);
           
        }


        private string ReplaceKeyword(string vtext, decimal? rangeMin, decimal? rangeMax, decimal? compareVal, decimal? checkVal, CultureInfo culture, int decimalPlaces)
        {

            var decimalPlacesText = "{0:N" + decimalPlaces + "}";
            if (rangeMin != null)
                vtext = vtext.Replace("@RangeMin", String.Format(culture, decimalPlacesText, rangeMin));
            

            if (rangeMax != null)
                vtext = vtext.Replace("@RangeMax", String.Format(culture, decimalPlacesText, rangeMax));
            

            if (compareVal != null)
                vtext = vtext.Replace("@CompareValue", String.Format(culture, decimalPlacesText, compareVal));
            

            if (checkVal != null)
                vtext = vtext.Replace("@CheckValue", String.Format(culture, decimalPlacesText, checkVal));
            

            return vtext;
        }

        [HttpPost]
        public string SpellCheckComment(string queryText)
        {
            //bool correct = SpellEngine["en"].Spell(queryText);

            string result = "<br />";

            //if (correct)
            //{
            //    result += Server.HtmlEncode(queryText) + " is correct.<br />Synonyms:<br />";
            //    ThesResult meanings = SpellEngine["en"].LookupSynonyms(queryText, true);
            //    if (meanings != null)
            //    {
            //        foreach (ThesMeaning meaning in meanings.Meanings)
            //        {
            //            result += "<b>Meaning: " + Server.HtmlEncode(meaning.Description) + "</b><br />";
            //            int number = 1;
            //            foreach (string synonym in meaning.Synonyms)
            //            {
            //                result += number.ToString() + ": " + Server.HtmlEncode(synonym) + "<br />";
            //                ++number;
            //            }
            //        }
            //    }

            //}
            //else
            //{

            result += "<div style=\"width: 400px; border: 1px solid #000000; padding: 3px; background-color: #cccccc;\"><div style=\"float: left; width: 365px;\">Spelling: English (U.S.)</div><div><a href=\"javascript:hideShowSpellcheck('none');\">Close</a></div><div style=\"width: 392px; border: 1px solid #000000; padding: 3px; background-color: #eaeaea;\">";

            //queryText = queryText.Replace(",", " ");
            //queryText = queryText.Replace(";", " ");
            //queryText = queryText.Replace("$", " ");
            //queryText = queryText.Replace(")", " ");
            //queryText = queryText.Replace("(", " ");

            string nextMistake = string.Empty;
            int initialMistake = 1;
            int mistakeNumber = 0;
            string[] output = queryText.Split(' ');
            List<string> outputMistakes = new List<string>();

            foreach (string mistakeWord in output)
            {
                if (!mistakeWord.EndsWith(";") && !mistakeWord.EndsWith(",") && !mistakeWord.EndsWith(".") && !mistakeWord.EndsWith("\"") && !mistakeWord.EndsWith(")") && !mistakeWord.StartsWith("$") && !mistakeWord.StartsWith("(") && !mistakeWord.StartsWith("\""))
                    if (!MvcApplication.spellEngine["en"].Spell(mistakeWord))
                        outputMistakes.Add(mistakeWord);
            }
            
            foreach (string word in outputMistakes)
            {
                if (initialMistake == 1)
                {
                    result += "<div id=\"ss" + mistakeNumber.ToString() + "\"><div style=\"float: left;\">Not in Dictionary:&nbsp;</div><div id=\"mistake\">" + Server.HtmlEncode(word) + "</div>Suggestions:<br /><div style=\"width: 390px; border: 1px solid #000000; height: 100px; overflow-y: scroll; overflow-x: hidden;\">";
                }
                else
                {
                    result += "<div id=\"ss" + mistakeNumber.ToString() + "\" style=\"display: none;\"><div style=\"float: left;\">Not in Dictionary:&nbsp;</div><div id=\"mistake\">" + Server.HtmlEncode(word) + "</div>Suggestions:<br /><div style=\"width: 390px; border: 1px solid #000000; height: 100px; overflow-y: scroll; overflow-x: hidden;\">";
                }
                List<string> suggestions = MvcApplication.SpellEngine["en"].Suggest(word);
                int number = 1;
                if (suggestions.Count == 0)
                    result += "(No Spelling Suggestions)";
                foreach (string suggestion in suggestions)
                {
                    result += number.ToString() + ": <a href=\"javascript:replaceSelectedText('DataCheckComment','" + suggestion + "');\">" + Server.HtmlEncode(suggestion) + "</a><br />";
                    ++number;
                }
                if (mistakeNumber < outputMistakes.Count() - 1)
                {
                    nextMistake = outputMistakes[mistakeNumber + 1];

                    result += "</div><div style=\"width: 392px; height: 30px;\"><div style=\"width: 60px; border: 1px solid #000000; float: right; text-align: center; margin-top: 4px;\"><a href=\"javascript:hideShowSuggestions('ss" + mistakeNumber.ToString() + "', 'ss" + (mistakeNumber + 1).ToString() + "', '" + nextMistake + "');\">Next</a></div></div></div>";
                }

                ++initialMistake;

                ++mistakeNumber;
            }

            if (outputMistakes.Count == 0)
                result = "<script type=\"text/javascript\">alert('The spelling check is complete.');</script>";

            return result;
        }

        [HttpGet]
        public ActionResult GetSpellCheckSuggestions(DataCheckDetailsModel result)
        {
            ViewData["Suggestions"] = result;
            //var results = new DataCheckDetailsModel();
            //results.SpellingSuggestions = result;
            return View(result);
        }

        #endregion
    }
}
