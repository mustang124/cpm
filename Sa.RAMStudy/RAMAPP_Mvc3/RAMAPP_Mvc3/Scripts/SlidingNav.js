﻿
//Left Side Navigation
$(".btnNavWindow").click(function () {
    var wth = $('ul.outer').not('#wrapper ul.outer').length * 300;
    /*alert($('ul.outer').not('#wrapper ul.outer').outerWidth());*/
    var slidingDistance =  500 + 'px';
    if ($(this).hasClass('leftPointer')) {
        $(this).removeClass('leftPointer').addClass('rightPointer');
        $(this).animate({ "left": "-=" + slidingDistance }, "slow");
        $('.navWindow').animate({ "left": "-=" + slidingDistance }, "slow");
        $(this).html('SHOW&nbsp;MENU');
    }
    else {
        $(this).removeClass('rightPointer').addClass('leftPointer');
        $(this).animate({ "left": "+=" + slidingDistance }, "slow");
        $('.navWindow').animate({ "left": "+=" + slidingDistance }, "slow");
        $(this).html('HIDE&nbsp;MENU');
    }
});

//Quick Link Navigation
$('.navWindow2 div.unitsContainer').toggle('fast');
$('.navWindow2 li').click(function (event) {
   
    if ($(event.target).is('a')) {
        return;
    }
    if ($(this).hasClass('rightPointerNav')) {
        $(this).removeClass('rightPointerNav').addClass('downPointerNav');
        $(this).children('div.unitsContainer').slideDown('slow');
    }
    else if ($(this).hasClass('downPointerNav')) {
        $(this).removeClass('downPointerNav').addClass('rightPointerNav');
        $(this).children('div.unitsContainer').slideUp('slow');
    }
});

//Tree Navigation
var docheight = $('.navWindow').height();
$('.navWindow div.unitsContainer').toggle('fast');
$(document).delegate('.navWindow li', 'click', function (event) {

    if ($(event.target).is('a')) {
        return;
    }
    var $navWindow = $('.navWindow');

    if ($(this).hasClass('rightPointerNav')) {
        $(this).removeClass('rightPointerNav').addClass('downPointerNav');
        
        var len = $(this).children('div.unitsContainer').find('li').length * 35;
        $(this).children('div.unitsContainer').slideDown(500, function () {
            //alert($navWindow.height() - 125);
            //alert($('#ulOuter').height());
           // if (($navWindow.height() - 125) < $('#ulOuter').height())
            //    $navWindow.animate({ height: $('#ulOuter').height() + 125 + 'px' },200);/*.css('height', $('#ulOuter').height() + 125 + 'px');*/
        });
        if (($navWindow.height() - 125) < $('#ulOuter').height() + len )
            $navWindow.animate({ height: $('#ulOuter').height() + 125 + len + 'px' }, 200);
    }
    else if ($(this).hasClass('downPointerNav')) {
        $(this).removeClass('downPointerNav').addClass('rightPointerNav');
        $(this).children('div.unitsContainer').slideUp('slow');
        //alert($('#ulOuter').height() + 125);
        //alert($docheight);
        var newHeight = $navWindow.height() - $(this).children('div.unitsContainer').height();
        if (newHeight > docheight)
            $navWindow.animate({ height: newHeight + 'px' }, 400);
            //$navWindow.css('height', newHeight + 'px');
    }

    //    alert($.getDocHeight());
    //    $('.navWindow').css('height', $(document).height() + 'px');
});

//Drilldown Grid
$('.gridHorizontal div.unitsContainer').toggle('fast');
$('.gridHorizontal li ul.gridbody').not('.unitsContainer').click(function (event) {

    if ($(event.target).parent().is('li.ViewIcon')) {
        return;
    }
    if ($(this).parent().hasClass('rightPointerNav')) {
        $(this).parent().removeClass('rightPointerNav').addClass('downPointerNav');
        $(this).parent().children('div.unitsContainer').slideDown('fast');
    } else if ($(this).parent().hasClass('downPointerNav')) {
        $(this).parent().removeClass('downPointerNav').addClass('rightPointerNav');
        $(this).parent().children('div.unitsContainer').slideUp('fast');
    }

   
});