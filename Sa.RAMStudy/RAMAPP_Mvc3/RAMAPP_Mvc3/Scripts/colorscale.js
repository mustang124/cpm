﻿

var min = Math.min;
var max = Math.max;
var round = Math.round;

function get_color_for_height(startColor, endColor, height, row) {
    var scale = row / height;
    var r = startColor['red'] + scale * (endColor['red'] - startColor['red']);
    var b = startColor['blue'] + scale * (endColor['blue'] - startColor['blue']);
    var g = startColor['green'] + scale * (endColor['green'] - startColor['green']);

    return {
        r: round(min(255, max(0, r))),
        g: round(min(255, max(0, g))),
        b: round(min(255, max(0, b)))
    }
}

function changeColor(obj, scale)
{
    var color = null;
    if (scale != undefined) 
    {
        
        if (obj.hasClass('bar')) {
            if (scale >= 97 )
                color = get_color_for_height({ red: 235, green: 255, blue: 100 }, { red: 0, green: 255, blue: 60 }, 100, scale);  // green range
            if (scale < 97 && scale >= 90)
                color = get_color_for_height({ red: 255, green: 255, blue: 0 }, { red: 255, green: 255, blue: 100 }, 100, scale); //yellow range
            if (scale < 90 && scale >= 80)
                color = get_color_for_height({ red: 255, green: 69, blue: 0 }, { red: 255, green: 255, blue: 0 }, 100, scale);  //orange range
            if (scale < 80)
                color = get_color_for_height({ red: 255, green: 98, blue: 98 }, { red: 255, green: 69, blue: 0 }, 100, scale); // red range
        }
        else {

            if (scale >=95)
                color = get_color_for_height({ red: 235, green: 255, blue: 100 }, { red: 0, green: 255, blue: 60 }, 100, scale);   // green range
            if (scale < 95 && scale >= 85)
                color = get_color_for_height({ red: 255, green: 255, blue: 0 }, { red: 255, green: 255, blue: 100 }, 100, scale); //yellow range
            if (scale < 85 && scale >= 75)
                color = get_color_for_height({ red: 255, green: 69, blue: 0 }, { red: 255, green: 255, blue: 0 }, 100, scale); //orange range
            if (scale < 75)
                color = get_color_for_height({ red: 255, green: 98, blue: 98 }, { red: 255, green: 69, blue: 0 }, 100, scale); // red range
        }

        obj.css('background-color', 'rgb(' + color['r'] + ',' + color['g'] + ',' + color['b'] + ')', 'background-color', 'rgba(' + color['r'] + ',' + color['g'] + ',' + color['b'] + ',0.60)');
    }
}
