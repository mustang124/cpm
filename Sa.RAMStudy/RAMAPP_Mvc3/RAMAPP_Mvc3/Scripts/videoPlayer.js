/*********************************************************
                  
Loads flash video into the flash player region. This 
gets called whenever a video is selected in list.

**********************************************************/
function loadVideo(video) {
    //var videoName = video.folder + "/" + video.name;
    var videoName = video.videoName;
    var videoDesc = "<span class='headerIntro2'>" + video.title + "</span>";
    var videoLink = "<br><a href='http://webservices.solomononline.com/dbap/video/" +
                                         video.folder + "/" + video.videoName +
                                         "'>Download: Right-click mouse button and select 'Save As Target'</a>";

//    var playlist = [
//              {
//           0: { src: '"'+ videoName.toString() +'"' , type: "video/mp4" }
//              }];

    //              alert(videoName.toString());

//    flowplayer("#vMedia", "http://releases.flowplayer.org/swf/flowplayer-3.2.7.swf", {
//        clip: {
//            url: '' + videoName.toString() + '',
//            provider: 'h264streaming'
//        },
//        plugins: {
//            h264streaming: {
//                url: 'flowplayer.h264streaming-3.0.5.swf'
//            },
//            controls: {
//                url: 'flowplayer.controls-3.0.3.swf',

//                // which buttons are visible and which not ?
//                play: false,
//                fullscreen: true,

//                // scrubber is a well known nickname to the timeline/playhead combination
//                scrubber: true
//            }
//        }
//    });


//          projekktor('#vMedia', {
//           debug: true,
//           poster: 'intro.png',
//           useYTIframeAPI: false,
//           width: 640,
//           height: 385,
//           controls: true,
//           playlist: [{ 0: { src: ''+ videoName.toString() + '', type: 'video/mp4'}}]
//       });
//var player = new MediaElementPlayer('#vMedia');
//    var player = MediaElement('vMedia');
//player.pause();
//player.setSrc(videoName);
    //player.play();
    var player = new MediaElementPlayer('#vMedia', {
    
       defaultVideoWidth: 640,
    // if the <video height> is not specified, this is the default
    defaultVideoHeight: 385
    });
   
    player.pause();
    player.media.src = videoName.toString();
    player.play();
//    MediaElement('vMedia', { success: function (me) {


//        me.play();

//        /*me.addEventListener('timeupdate', function () {
//        document.getElementById('time').innerHTML = me.currentTime;
//        }, false);*/

//        document.getElementById('pp')['onclick'] = function () {
//            if (me.paused)
//                me.play();
//            else
//                me.pause();
//        };

//    },
//        error: function (e) {
//            alert(e);
//        }


//    });




//       projekktor('#vMedia').setFile(playlist);
//       projekktor('#vMedia').setPlay();
  //  $("#vMedia").setFile( videoName,false);

    /*$("#vMedia").flashembed(videoName, { "autostart": true
        ,
        "thumb": video.folder + "/" + video.image,
        "thumbscale": 65
    });*/

    $("#vDesc").html(videoDesc);
    // $("#vLink").html(videoLink);
    $(document).scrollTop(0);
}


/*******************************************************
                 
Load video information from config.js into the list 
                
*******************************************************/
function loadVideoList() {
    
    var content = "";

    for (i = 0; i < videoConfig.videos.length; i++) {
        content += "<li id='vid" + i + "' class='videoitem' onclick='loadVideo(videoConfig.videos[" + i + "]);'> " +
                               "<img height=108 width=144 src='" + videoConfig.videos[i].folder + "/thumb.png'/>" +
                               "<span class='vidNumber'>" + (i + 1) + " </span><span class='vidTitle'> " + videoConfig.videos[i].title + "</span> " +
                               "</li>";
    }

    $("#videoGallery").html(content);
}
 