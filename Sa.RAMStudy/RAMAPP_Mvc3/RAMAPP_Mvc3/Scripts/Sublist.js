﻿
/***
* Creates dependency list
*/ 
function makeSublist(parent, child, isSubselectOptional, childVal) {
    $("body").append("<select style='display:none' id='" + parent + child + "'></select>");
    $('#' + parent + child).html($("#" + child + " option"));

    var parentValue = $('#' + parent).attr('value');
    $('#' + child).html($("#" + parent + child + " .sub_" + parentValue).clone());

    childVal = (typeof childVal == "undefined") ? "" : childVal;
    $("#" + child + ' option[@value="' + childVal + '"]').attr('selected', 'selected');

    $('#' + parent).change(
		function () {
		    var parentValue = $('#' + parent).attr('value');
		    $('#' + child).html($("#" + parent + child + " .sub_" + parentValue).clone());
		    if (isSubselectOptional) $('#' + child).prepend("<option value='none'> -- Select -- </option>");
		    $('#' + child).trigger("change");
		    $('#' + child).focus();
		}
	);
}

function HighlightListTooltip(){
    $(document).ready(function(){
        $("select").each(function(){
            var i = 0;
            var s = this;
            for(i = 0; i < s.length; i ++ )
                s.options[i].title = s.options[i].text;
            if(s.selectedIndex > -1)
                s.onmousemove = function(){
                s.title = s.options[s.selectedIndex].text;
            };
        });
    });
}