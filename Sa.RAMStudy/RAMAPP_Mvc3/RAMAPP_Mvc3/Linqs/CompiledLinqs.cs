﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RAMAPP_Mvc3.Models;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Repositories.Cache;
namespace RAMAPP_Mvc3.Linqs
{
    public class CompiledQryLogin
    {
        public static readonly Func<RAMEntities, string, IQueryable<LogIn>>
            LoginByEmail =
            System.Data.Objects.CompiledQuery.Compile<RAMEntities, string, IQueryable<LogIn>>(
            (RAMEntities db, string emailAddress) =>
            from l in db.LogIn
            where l.Email == emailAddress && l.Active == true
            select l
            );

        public static readonly Func<RAMEntities, string, IQueryable<LogIn>>
           LoginByID =
           System.Data.Objects.CompiledQuery.Compile<RAMEntities, string, IQueryable<LogIn>>(
           (RAMEntities db, string userID) =>
           from l in db.LogIn
           where l.UserID == userID && l.Active == true
           select l
           );

        public static readonly Func<RAMEntities, int, IQueryable<LogIn>>
          LoginByUserNo =
          System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, IQueryable<LogIn>>(
          (RAMEntities db, int userNo) =>
          from l in db.LogIn
          where l.UserNo == userNo && l.Active == true
          select l
          );

        public static readonly Func<RAMEntities, string, string, IQueryable<LogIn>>
           LoginByUserAndCompany =
           System.Data.Objects.CompiledQuery.Compile<RAMEntities, string, string, IQueryable<LogIn>>(
           (RAMEntities db, string userID, string companyID) =>
           from l in db.LogIn
           where l.UserID == userID && l.CompanyID == companyID && l.Active == true
           select l
           );
    }


    public class CompiledQryPermission
    {

        public static readonly Func<RAMEntities, string, int, SitePermissions>
        GetUserPermissionBySiteID =
        System.Data.Objects.CompiledQuery.Compile<RAMEntities, string, int, SitePermissions>(
        (RAMEntities db, string userID, int siteID) =>
        (from h in db.SitePermissions
         where h.UserID == userID &&
               h.DatasetID == siteID
         select h).FirstOrDefault()
        );

    }

    public class CompiledLinqs
    {
        public static Func<RAMEntities, int, IQueryable<SiteUnits>, IQueryable<DataValidationSummaryModel>> DataValidationInnerResult =
       System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, IQueryable<SiteUnits>, IQueryable<DataValidationSummaryModel>>(
       (db, id, mylist) =>
           (
            from ct in mylist
            let cm = ct.UnitsOfSite.AsQueryable()
            from dq in db.DatasetQuality
            where ct.DatasetID == dq.DatasetID || cm.Any(a => a.DatasetID == dq.DatasetID)
            let qs = db.QuestionSection
            select new DataValidationSummaryModel
            {
                FacilityName = ((ct.DatasetID == ct.DatasetID) ? ct.SiteName : cm.First(b => b.DatasetID == dq.DatasetID).UnitName),
                DatasetID = dq.DatasetID,
                Grade = dq.Grade ?? 0,
                PcntComplete = dq.PcntComplete ?? 0,
                ChecksNC = dq.ChecksNC ?? 0,
                ChecksNA = dq.ChecksNA ?? 0
            }
            ));





        #region "Validation Queries"

        public static Func<RAMEntities, int, IEnumerable<SiteUnits>, dynamic> DataSectionSummaryQuery =
System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, IEnumerable<SiteUnits>, dynamic>(
(db, id, mylist) => (
                     from h in mylist
                     from h2 in db.DatasetQuality
                     where h.DatasetID == h2.DatasetID
                     select new
                     {
                         h.DatasetID,
                         h.SiteName,
                         h2.Grade,
                         h2.PcntComplete,
                         h2.ChecksNC,
                         h2.ChecksNA,
                         unitsummary = (
                                        from k in h.UnitsOfSite
                                        from k2 in db.DatasetQuality
                                        where k.DatasetID == id && k.DatasetID == k2.DatasetID
                                        select
                                            new
                                            {
                                                SiteDatasetID = h.DatasetID,
                                                k.DatasetID,
                                                k.UnitName,
                                                k2.Grade,
                                                k2.PcntComplete,
                                                k2.ChecksNC,
                                                k2.ChecksNA
                                            })
                     }));



        public static Func<RAMEntities, int, IQueryable<DataValidationSummaryModel>> DataSectionSummary2Query =
System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, IQueryable<DataValidationSummaryModel>>(
(db, id) => (
    from h in db.Dataset_LU
    join h2 in db.DatasetQuality on h.DatasetID equals h2.DatasetID
    where h.DatasetID == id
    select new DataValidationSummaryModel
    {
        DatasetID = h.DatasetID,
        FacilityName = h.FacilityName,
        ParentID = h.ParentID ?? 0,
        DataLevel = h.DataLevel,
        Grade = h2.Grade ?? 0,
        PcntComplete = h2.PcntComplete ?? 0,
        ChecksNC = h2.ChecksNC ?? 0,
        ChecksNA = h2.ChecksNA ?? 0
    }));



        public static Func<RAMEntities, int, string, IQueryable<DataValidationSectionModel>> DataSummaryFurtherDetailsQuery =
System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, string, IQueryable<DataValidationSectionModel>>(
(db, id, level) => (
                    from us in db.DatasetQualityBySection
                    join sc in db.QuestionSection on us.Section.Trim() equals sc.Name.Trim()
                    where us.DatasetID == id && sc.DataLevel.ToLower().Equals(level)
                    orderby us.SectionSortKey descending
                    select new DataValidationSectionModel
                    {
                        Section = us.Section,
                        QuestionNo = us.QuestionCount ?? 0,
                        PcntComplete = us.PcntComplete ?? 0,
                        MissingData = us.MissingData ?? 0,
                        Grade = us.Grade ?? 0,
                        DatasetID = us.DatasetID,
                        ChecksOK = us.ChecksOK ?? 0,
                        ChecksNC = us.ChecksNC ?? 0,
                        ChecksNA = us.ChecksNA ?? 0,
                        ChecksIN = us.ChecksIN ?? 0,
                        SectionID = us.SectionID.Trim()
                    }
                                         ));


        public static Func<RAMEntities, int, IQueryable<DataValidationSummaryModel>> DataValidationSummaryInnerQuery =
System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, IQueryable<DataValidationSummaryModel>>(
(db, id) => (
              from dq in db.DatasetQuality
              join dlu in db.Dataset_LU on dq.DatasetID equals dlu.DatasetID
              where (dq.DatasetID == id || dlu.ParentID == id) && dlu.Deleted == false
              select new DataValidationSummaryModel
              {
                  FacilityName = dlu.FacilityName,
                  DatasetID = dq.DatasetID,
                  Grade = dq.Grade ?? 0,
                  PcntComplete = dq.PcntComplete ?? 0,
                  ChecksNC = dq.ChecksNC ?? 0,
                  ChecksNA = dq.ChecksNA ?? 0
              }
          ));

        #endregion

        #region "Unit Queries"

        public static Func<RAMEntities, int, string, IQueryable<UnitDetails>> UnitDetailsQuery =
System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, string, IQueryable<UnitDetails>>(
(db, id, companySID) => (from u in db.UnitInfo
                         where u.DatasetID == id && u.CompanySID == companySID
                         select new UnitDetails
                         {
                             CompanySID = u.CompanySID,
                             SiteDatasetID = u.SiteDatasetID,
                             DatasetID = u.DatasetID,
                             UnitName = u.UnitName,
                             ProcessFamily = u.ProcessType,
                             PrimaryProductName = u.PrimaryProductName,
                             GenericProductName = (u.ProcessType.Trim() == "OthRef" || u.ProcessType.Trim() == "OthChem") ? "" : u.GenericProductName,
                             OtherGenericProductName = (u.ProcessType.Trim() == "OthRef" || u.ProcessType.Trim() == "OthChem") ? u.GenericProductName : ""
                         }));

        #endregion

        #region "Site Queries"

        public static Func<RAMEntities, int, string, string, bool, IQueryable<SiteModel>> SiteDetailsQuery =
System.Data.Objects.CompiledQuery.Compile<RAMEntities, int, string, string, bool, IQueryable<SiteModel>>(
(db, siteDatasetId, companySID, companyID, isAdmin) => (from s in db.SiteInfo
                                                        where s.CompanySID == companySID &&
                                                              s.DatasetID == siteDatasetId
                                                        select new SiteModel
                                                        {
                                                            CompanySID = s.CompanySID.Trim(),
                                                            DatasetID = s.DatasetID,
                                                            Name = s.SiteName.Trim(),
                                                            Location = s.Country,
                                                            Currency = s.LocalCurrency,
                                                            PercentCompleted = 0,
                                                            IsActive =
                                                                !(db.Reviewable.Any(
                                                                    r => (r.CompanySID == s.CompanySID) &&
                                                                         ((r.SiteDatsetID == s.DatasetID &&
                                                                           r.DataLevel == "Si") ||
                                                                          (r.DataLevel == "Co")))),
                                                            Users = (from l in db.LogIn
                                                                     where l.CompanyID == companyID &&
                                                                           ((isAdmin && l.SecurityLevel == 2) ||
                                                                            db.SitePermissions.Any(
                                                                                p =>
                                                                                p.UserID == l.UserID &&
                                                                                p.DatasetID == s.DatasetID)) &&
                                                                           db.CurrentStudyForUsers.Any(
                                                                               cs =>
                                                                               cs.CompanySID == companySID &&
                                                                               cs.UserID == l.UserID)
                                                                     select new UserDetails
                                                                     {
                                                                         UserNo = l.UserNo,
                                                                         UserID = l.UserID,
                                                                         CompanyID = l.CompanyID,
                                                                         ScreenName = l.ScreenName,
                                                                         Firstname = l.FirstName,
                                                                         Lastname = l.LastName,
                                                                         Password = l.Password,
                                                                         Role =
                                                                             (short)
                                                                             ((isAdmin &&
                                                                               l.SecurityLevel == 2)
                                                                                  ? 2
                                                                                  : db.SitePermissions.
                                                                                        Any(
                                                                                            p1 =>
                                                                                            p1.DatasetID ==
                                                                                            s.DatasetID &&
                                                                                            p1.UserID.
                                                                                                Contains
                                                                                                (l.UserID.Trim()) &&
                                                                                            p1.SecurityLevel == 1)
                                                                                        ? 1
                                                                                        : 0)
                                                                     }
                                                                    ),
                                                            Units = (from u in db.UnitInfo
                                                                     where
                                                                         u.SiteDatasetID == s.DatasetID &&
                                                                         u.CompanySID == s.CompanySID
                                                                     select new UnitDetails
                                                                     {
                                                                         CompanySID =
                                                                             s.CompanySID.Trim(),
                                                                         UnitName = u.UnitName.Trim(),
                                                                         PrimaryProductName =
                                                                             u.PrimaryProductName.Trim(),
                                                                         GenericProductName =
                                                                             u.GenericProductName.Trim(),
                                                                         OtherGenericProductName =
                                                                              u.GenericProductName.Trim(),
                                                                         ProcessFamily =
                                                                             u.ProcessType.Trim(),
                                                                         DatasetID = u.DatasetID,
                                                                         SiteDatasetID = s.DatasetID
                                                                     })
                                                        }));
        #endregion

        #region "Navigation Queries"
        public static Func<RAMEntities, bool, string, string, IQueryable<SiteUnits>> GetSiteUnitsQuery =
System.Data.Objects.CompiledQuery.Compile<RAMEntities, bool, string, string, IQueryable<SiteUnits>>(
(db, isAdmin, companySID, userID) => (from s in db.SiteInfo
                                      where s.CompanySID == companySID &&
                                     (isAdmin || db.SitePermissions.Any(p => p.UserID == userID && (p.SecurityLevel == 1 || p.SecurityLevel == 0) && p.DatasetID == s.DatasetID))
                                      select new SiteUnits
                                      {
                                          SiteName = s.SiteName,
                                          Role = (isAdmin ? (short)2 : db.SitePermissions.FirstOrDefault(p => p.UserID == userID && p.DatasetID == s.DatasetID).SecurityLevel),
                                          DatasetID = s.DatasetID,
                                          LockedImg = db.Reviewable.Any(r => r.CompanySID == s.CompanySID && (((r.DataLevel.Trim() == "Co") || (r.DataLevel.Trim() == "Si" && r.SiteDatsetID == s.DatasetID)))) ? "#lockimg#" : "",
                                          UnitsOfSite = from u in db.UnitInfo
                                                        where u.SiteDatasetID == s.DatasetID
                                                        select new Units { UnitName = u.UnitName, DatasetID = u.DatasetID, GenericProductName = u.GenericProductName, ProcessType = u.ProcessType }
                                      }));

        #endregion
    }
}