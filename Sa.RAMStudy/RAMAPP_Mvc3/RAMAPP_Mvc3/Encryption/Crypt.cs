﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace RAMAPP_Mvc3.Encryption
{
    public sealed class Crypt
    {
        private const string EncryptionIV = "SACTW0S1G2L3C4D5B6R7M8F9";
        private const string PartialKey = "SOALCDMSCCHOOL1996DUSTYRHODES";

        public string Decrypt(string aString, string salt)
        {
            // Note that the key and IV must be the same for the encrypt and decript calls.
            string results;
            aString = aString.Replace(" ", "+");
            TripleDESCryptoServiceProvider tdesEngine = new TripleDESCryptoServiceProvider();
            string key = salt + PartialKey;
            byte[] keyBytes = Encoding.ASCII.GetBytes(key.Substring(0, 24));
            byte[] ivBytes = Encoding.ASCII.GetBytes(EncryptionIV);
            ICryptoTransform transform = tdesEngine.CreateDecryptor(keyBytes, ivBytes);

            byte[] mesg = Convert.FromBase64String(aString);//Encoding.ASCII.GetBytes(aString);

            byte[] ecn = transform.TransformFinalBlock(mesg, 0, mesg.Length);

            results = Encoding.ASCII.GetString(ecn);
            // results = Convert.ToBase64String(ecn);

            return (results);
        }

        public string Encrypt(string aString, string salt)
        {
            // Note that the key and IV must be the same for the encrypt and decrypt calls.
            string results;
            TripleDESCryptoServiceProvider tdesEngine = new TripleDESCryptoServiceProvider();
            string key = salt + PartialKey;
            byte[] keyBytes = Encoding.ASCII.GetBytes(key.Substring(0, 24));
            byte[] ivBytes = Encoding.ASCII.GetBytes(EncryptionIV);

            ICryptoTransform transform = tdesEngine.CreateEncryptor(keyBytes, ivBytes);
            byte[] mesg = Encoding.ASCII.GetBytes(aString);
            byte[] ecn = transform.TransformFinalBlock(mesg, 0, mesg.Length);

            results = Convert.ToBase64String(ecn);

            return (results);
        }
    }
}