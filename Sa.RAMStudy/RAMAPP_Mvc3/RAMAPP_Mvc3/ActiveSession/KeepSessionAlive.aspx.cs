﻿using System;
using System.Web.UI.HtmlControls;

namespace RAMAPP_Mvc3.ActiveSession
{
    public partial class KeepSessionAlive : System.Web.UI.Page
    {
        protected string WindowStatusText = "";
        protected HtmlMeta MetaRefresh;

        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            this.MetaRefresh = new HtmlMeta();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //string RefreshValue = Convert.ToString((Session.Timeout * 60) - 60);

            string RefreshValue = Convert.ToString((Session.Timeout * 60) - 90);

            // Refresh this page 60 seconds before session timeout, effectively resetting the session timeout counter.
            MetaRefresh.Attributes["content"] = RefreshValue + ";url=./KeepSessionAlive.aspx?q=" + DateTime.Now.Ticks;

            WindowStatusText = "Last refresh " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
        }
    }
}