﻿using System;
using Dimebrain.Web.Extensions;

namespace ShrinkWrap.Minifier
{
    public class CssMinifier : IMinifier
    {
        /// <summary>
        /// Minify the input using a C# regex minifier
        /// </summary>
        /// <param name="input">Css string for minification</param>
        /// <returns>Minified version of the input</returns>
        public String Minify(String input)
        {
            String output = CssMin.CssMinify(input);
            return output;
        }
    }
}
