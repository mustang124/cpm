﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace ShrinkWrap.HttpHandler
{
    public class HttpRequestWrapper : IHttpRequest
    {
        public Uri Uri {get; set;}
        public string Verb { get; set; }
        public NameValueCollection Headers { get; set; }
        public HttpRequestWrapper(Uri uri, NameValueCollection headers, string verb)
        {
            Uri = uri;
            Verb = verb;
            Headers = headers;
        }
    }
}
