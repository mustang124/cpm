using System;
using ShrinkWrap.Minifier;

namespace ShrinkWrap.HttpHandler
{
    /// <summary>
    /// Handles CSS stylesheets
    /// </summary>
    public class CssHandler : MinifyingHandler
    {
        IMinifier minifier = new CssMinifier();
        public override IMinifier Minifier {
            get
            {
                return minifier;
            }
        }
        public override string ContentType
        {
            get
            {
                return "text/css";
            }
        }
    }
}