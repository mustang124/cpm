﻿using System;
using System.IO;

namespace JavaScriptSupport
{
    partial class JavaScriptMinifier
    {
        /// <summary>
        /// Overload for Minify() so that the arguments don't have to come
        /// from file.
        /// </summary>
        /// <param name="sr">A stream the un-minified js can be read from.</param>
        /// <param name="sw">A stream to write the minified js to.</param>
        public void Minify(StreamReader sr, StreamWriter sw)
        {
            this.sr = sr;
            this.sw = sw;
            jsmin();
        }
    }
}
