﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;

namespace RAMAPP_Mvc3.Helpers
{
    

    /// <summary>
    ///  This class is a timer that removes from the list whenever they have been inactive for a certain time.
    ///  This used in the who is online feature.
    /// </summary>
    public class GlobalTimer : IDisposable
    {
        private static Timer timer;
        private static int interval = 5 * 60000;


        public static void StartGlobalTimer()
        {
            if (null == timer)
            {
                SessionChecker.CreatSessionDictionary();
                timer = new Timer(new TimerCallback(DropUsers), HttpContext.Current, 0, interval);

            }
        }

        private static void DropUsers(object sender)
        {
            HttpContext context = (HttpContext)sender;
            var dict = HttpRuntime.Cache["UsersCountSession"] as Dictionary<int, DateTime>;
            if (dict != null)
            {
                if (dict.Count > 0)
                {

                    try
                    {
                        var q = (from p in dict.AsQueryable()
                                 where (DateTime.Now - p.Value).Minutes >= 20
                                 select p).ToList();
                        if (q.Count > 0 )
                        {
                            q.ForEach(p => dict.Remove(p.Key));
                            HttpRuntime.Cache["UsersCountSession"] = dict;
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLog.WriteEntry("Application", "exception at DropUsers: " + ex.Message, EventLogEntryType.Error);
                    }
                }
            }
            else
                SessionChecker.CreatSessionDictionary(); // for some resone if the cache is not filled.
        }

        #region IDisposable Members

        public void Dispose()
        {
            timer = null;
        }

        #endregion

    }


}