﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RAMAPP_Mvc3.Encryption;
using RAMAPP_Mvc3.Entity;

namespace RAMAPP_Mvc3.Helpers
{
    public class SessionUtil
    {
        /// <summary>
        ///  Checks if the session is expired or not
        /// </summary>
        /// <returns></returns>
        public static bool IsSessionExpired()
        {
            if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session.IsNewSession)
                {
                    string CookieHeaders = HttpContext.Current.Request.Headers["Cookie"];

                    if ((null != CookieHeaders) && (CookieHeaders.IndexOf("ASP.NET_SessionId") >= 0))
                    {
                        // IsNewSession is true, but session cookie exists,
                        // so, ASP.NET session is expired
                        return true;
                    }
                }
            }

            // Session is not expired and function will return false,
            // could be new session, or existing active session
            return false;
        }

        
       /// <summary>
       /// Returns the current user company SID
       /// </summary>
       /// <returns></returns>
        public static string GetCompanySID()
        {
            //  var salt =  DateTime.Now.AddDays(-3).ToShortDateString()+"00SAI";


            if (!IsSessionExpired())
            {
                //string csid = String.Empty;
                //if (HttpContext.Current.Session["Mode"] == null)
                string  csid = HttpContext.Current.Session["CompanySID"].ToString();
                //else
               // {
                    var cp = HttpContext.Current.Session["Mode"] as string;
                    if (cp != null && HttpContext.Current.Request[cp] != null)
                    {
                        csid = HttpContext.Current.Request[cp].ToString();
                        HttpContext.Current.Session[cp] = csid;
                    }
                    else if (cp != null && HttpContext.Current.Session[cp] != null)
                    {
                        csid = HttpContext.Current.Session[cp].ToString();
                       // HttpContext.Current.Request[cp] = csid;
                    }
                //}
               
                if (csid != String.Empty)
                    return csid;
            }
            return String.Empty;
        }

        /// <summary>
        /// Returns if the current user is a data coordinator
        /// </summary>
        /// <returns></returns>
        public static bool IsAdmin()
        {
            if (!IsSessionExpired())
            {
                var csid = (bool)HttpContext.Current.Session["IsAdmin"];
                return csid;
            }
            return false;
        }

        /// <summary>
        /// Returns if the current user is a site coordinator
        /// </summary>
        /// <returns></returns>
        public static bool IsSiteAdmin()
        {
            if (!IsSessionExpired())
            {
                var csid = (bool)HttpContext.Current.Session["IsSiteAdmin"];
                return csid;
            }
            return false;
        }

        /// <summary>
        /// Returns an admin key. It used in solomon administrator page
        /// </summary>
        /// <returns></returns>
        public static string AdminKey()
        {
            var salt = "CONF00SAISARG";
            var cp = new Crypt().Encrypt("00SAI", salt).Replace("=", "").Replace("+", "");

            return cp;
        }

        /// <summary>
        /// Returns current user culture
        /// </summary>
        /// <returns></returns>
        public static RAMAPP_Mvc3.Localization.ClientCultureInfo GetCultureInfo()
        {
            // Sets client thread cultureinfo.
            if (HttpContext.Current.Session["globObj"] == null)
            {
                var local = new RAMAPP_Mvc3.Localization.ClientCultureInfo();
                HttpContext.Current.Session["globObj"] = local;
                return local;
            }
            else
                return HttpContext.Current.Session["globObj"] as RAMAPP_Mvc3.Localization.ClientCultureInfo;
        }
    }
}