﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web.Security;
using RAMAPP_Mvc3.Encryption;
using RAMAPP_Mvc3.Entity;
using RAMAPP_Mvc3.Linqs;
using System.Data.SqlClient;

namespace RAMAPP_Mvc3.Helpers
{

  

    #region RAMMembershipProvider
    public class RAMMembershipProvider : MembershipProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string emailAddress, string oldPassword, string newPassword)
        {

            try
            {
                var user = this.GetUser(emailAddress);
                var crypt = new Crypt();
                var salt = "dog" + user.ScreenName.Trim() + "butt" + user.CompanyID.Trim() + "steelers";

                var encOldPswd = crypt.Encrypt(oldPassword, salt);
                if (encOldPswd == user.Password.Trim())
                {
                    string encPswd = crypt.Encrypt(newPassword, salt);
                    user.Password = encPswd;
                    this.UpdateUser(user);
                }
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

     

       /* public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {

            throw new NotImplementedException();
        }*/

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public RAMMembershipUser CreateUser(string companyID, string username, string password, short role, string firstname, string lastname,string screenname, string culture, string email,string  jobTitle,string phoneNo, out MembershipCreateStatus status)
        {
            using (var ramDb = new RAMEntities())
            {
                //string encPwd;
                var crypt = new Crypt();
                
                string salt = "dog" + screenname + "butt" + companyID + "steelers";
                string encPwd = crypt.Encrypt(password, salt);

                bool userExist = ramDb.LogIn.Any(l => l.UserID == username);
                if (!userExist)
                {
                    
                    var newuser = new LogIn
                    {
                        CompanyID = companyID,
                        UserID = username,
                        Email = email,
                        Password = encPwd,
                        SecurityLevel = role,
                        FirstName = firstname,
                        LastName = lastname,
                        ScreenName = screenname,
                        UserLanguage = culture,
                        JobTitle = jobTitle,
                        PhoneNo=phoneNo,
                        Active = true
                    };

                    ramDb.AddToLogIn(newuser);
                    ramDb.SaveChanges();
                    
                    status = MembershipCreateStatus.Success;
                    return newuser as RAMMembershipUser;
                }
                else
                    status = MembershipCreateStatus.DuplicateUserName;


                return  null as  RAMMembershipUser ;
            }

        }

        public MembershipUser CreateUser(string companyID, string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

       /* public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {

            throw new NotImplementedException();
        }*/

        public override  bool DeleteUser( string userID, bool deleteAllRelatedData)
        {
            bool result = false;
            using (RAMEntities db = new RAMEntities())
            {

                try
                {
                   
                    SqlParameter pUserId = new SqlParameter("UserID", userID);
                    int status = db.ExecuteStoreCommand("exec sp_DeleteAppUser {0}", pUserId.Value);

                    result = true;
                }
                catch (Exception)
                {
                    //do nothing
                }



            }
            return result;
        }

        public override bool EnablePasswordReset
        {
            get { return true; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return true; }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public List<LogIn> GetAllCompanyUsers(string companyID, int pageIndex, int pageSize, out int totalRecords)
        {
            using (RAMEntities ramDb = new RAMEntities())
            {
                var users = (from l in ramDb.LogIn
                             where l.CompanyID == companyID
                             select l);

                totalRecords = users.Count();

                return users.ToList();

            }
           
        }


        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public LogIn GetUser(string emailAddress)
        {
            LogIn user;

            using (var db = new RAMEntities())
            {

                user = CompiledQryLogin.LoginByEmail(db, emailAddress).FirstOrDefault();
                //user =( from l in db.LogIn
                //       where l.Email == emailAddress && l.Active == true
                //       select l).SingleOrDefault();

            }

            return user;
        }

        //public LogIn GetUserByID(string userID)
        //{
        //    LogIn user;

        //    using (var db = new RAMEntities())
        //    {
        //        user = CompiledQryLogin.LoginByEmail(db, emailAddress).FirstOrDefault();
        //        //user =( from l in db.LogIn
        //        //       where l.Email == emailAddress && l.Active == true
        //        //       select l).SingleOrDefault();
        //    }

        //    return user;
        //}

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return 3; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return false; }
        }

        public override string ResetPassword(string username, string answer)
        {

            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(LogIn user)
        {
           
            using (var ramDb = new RAMEntities())
            {
                LogIn updateUsr = CompiledQryLogin.LoginByUserAndCompany(ramDb, user.UserID, user.CompanyID).First();// ramDb.LogIn.First(l => l.CompanyID == user.CompanyID && l.UserID == user.UserID);
                updateUsr.JobTitle = user.JobTitle;
                updateUsr.UserLanguage = user.UserLanguage;
                updateUsr.SecurityLevel = user.SecurityLevel;
                updateUsr.PhoneNo = user.PhoneNo;
                updateUsr.LastName = user.LastName;
                updateUsr.FirstName = user.FirstName;
                updateUsr.Email = user.Email;

                if (user.Password != null)
                    updateUsr.Password = user.Password;
              
                ramDb.SaveChanges();

            }
           
        }

        public override bool ValidateUser(string emailAddress, string password)
        {

            LogIn secData = this.GetUser(emailAddress);

            if (secData != null )
            {
                Crypt crypt = new Crypt();
                string   salt = "dog" + secData.ScreenName + "butt" + secData.CompanyID.Trim() + "steelers";
               // string saltPwd = crypt.Encrypt(password, salt);
                return crypt.Encrypt(password, salt).Equals(secData.Password);
            }

            return false;

        }

      
    }
    #endregion


    #region RAMMembershipUser
    public class RAMMembershipUser : LogIn
    {
        //No implementation

    }
    #endregion
}

