﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace RAMAPP_Mvc3.ModelBinder
{
    /// <summary>
    /// Formats the data to the proper culture when read from the presentation  to database 
    /// </summary>
     public class FloatModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(float) || bindingContext.ModelType == typeof(float?))
            {
                return BindNumber(bindingContext);
            }
            else
            {
                return base.BindModel(controllerContext, bindingContext);
            }
        }

        private object BindNumber(ModelBindingContext bindingContext)
        {
            ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueProviderResult == null)
            {
                return null;
            }
            try
            {
                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);
                object value = null;
                value = Convert.ChangeType(valueProviderResult.AttemptedValue.Trim(), bindingContext.ModelType,
                                           Thread.CurrentThread.CurrentCulture);
                return value;
            }
            catch
            {
                var ex = new InvalidOperationException("Invalid value", new Exception("Invalid value", new FormatException("Invalid value")));
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                return null;
            }
           
        }
    }
}
