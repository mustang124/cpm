﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RAMAPP_Mvc3.Repositories.Cache;
using RAMAPP_Mvc3.Helpers;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RAMAPP_Mvc3.Navigation
{
    
    ///
    /// Class used for building treelist for survey and admin section.
    /// 
    public class NavigationTree
    {


        private List<SiteUnits> sitesAndUnits;

        public NavigationTree()
        {
            var cachedNavigation = new CachedNavigationRepository();
            this.sitesAndUnits = cachedNavigation.GetSiteUnitList();
        }

        public enum NavigationType { Survey = 1, Administration = 2 }

        public string GetNavigation(NavigationType nav)
        {

            return GetTree(nav);

        }


        private string GetTree(NavigationType nav)
        {
            var myList = new StringBuilder();
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            bool isAdmin = SessionUtil.IsAdmin();
            bool isSiteAdmin = SessionUtil.IsSiteAdmin();
            string siteLink = "", title = "", unitLink = "";

            myList.Append("<p><ul id='ulOuter' class='outer' style=\"padding-left:15px;color: #991000;\">");

            if (this.sitesAndUnits.Count == 0)
                myList.Append("<li> Please add some sites </li>");

            foreach (SiteUnits parent in this.sitesAndUnits.Where(w => (nav == NavigationType.Administration && w.Role>0)||(nav == NavigationType.Survey)))
            {
                //Skip site where site administrators are users
                //if (nav == NavigationType.Administration)
                // if (parent.Role == 0)
                //    continue;

                if (nav == NavigationType.Administration)
                {
                    siteLink = urlHelper.Action("SiteDetail", "Site", new { id = parent.DatasetID });
                    title = "Modify Site";
                }
                else if (nav == NavigationType.Survey)
                {
                    siteLink = urlHelper.Action("Index", "App", new { id = parent.DatasetID, sectiontype = "site" });
                    title = "Site Study Questions";
                }

                myList.Append("<li class=' rightPointerNav'><strong>" +
                              "<a href='" + siteLink + "' title='" + title + "'>" + parent.LockedImg + parent.SiteName +
                              "</a>");

                myList.Append("</strong><div class='sep'>&nbsp;</div><div class='unitsContainer'><ul>");

                var siteUnits = (List<Units>)parent.UnitsOfSite;

                if (siteUnits.Count == 0)
                {
                    myList.Append("<li>Please add units.</li>");
                }
                else
                {
                    myList.Append("<li class='subHeader'> UNITS </li>");
                    foreach (Units child in siteUnits)
                    {
                        
                        if (nav == NavigationType.Administration)
                        {
                            unitLink = urlHelper.Action("UnitDetails", "Unit", new { id = child.DatasetID });
                            title = "Modify Unit";
                        }
                        else if (nav == NavigationType.Survey)
                        {
                            unitLink = urlHelper.Action("Index", "App", new { id = child.DatasetID, sectiontype = "unit" });
                            title = "Unit Study Questions";
                        }

                        myList.Append("<li><a href='" + unitLink + "' title='" + title + "' >" + parent.LockedImg +
                                      child.UnitName + "</a></li>");

                    }
                }
                myList.Append("</ul></div></li> ");
            }

            myList.Append("</ul>");
            myList.Append("</p>");

            myList = myList.Replace("#lockimg#",
                                    "<img border=0 src=\"" + urlHelper.Content("~/images/lockedsym.png") + "\" alt='' />");

            return myList.ToString();
        }

    }
}