﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Repositories.Cache" %>
<%@ Import Namespace="RAMAPP_Mvc3.Models" %>
<%@ Import Namespace="System.Globalization" %>
<%
 
    const string innerTemplate = "<li class='rightPointerNav' ><ul  class=\"gridbody\" scale=\"{1}" +
                             "\"  pctcm='{2:N1}'><li class=\"gridrowtext\" style=\"text-indent:30px;\">{0}</li><li class=\"gridrow\">" + "<span class='bar' style='width:{7}%;'>{2:N1}%</span>" +
                             "</li><li class=\"gridrow\"><span class=\"bigfont\">{1}" +
                             "</span></li> <li class=\"gridrow\">{3}" +
                             "</li><li class=\"gridrow\">{4}</li>" +
                             "<li class=\"gridrow ViewIcon\">" +
                             "{5}</li> </ul><div class='unitsContainer'>InnerTable2{6}</div></li>";

            const string innerTemplate2 = "<li class='nocursor' ><ul  class=\"gridbody\" scale=\"{1}" +
                              "\"  pctcm='{2:N1}'><li class=\"gridrowtext\"  style=\"text-indent:60px;\">{0}</li><li class=\"gridrow\">" + "<span class='bar' style='width:{7}%;'>{2:N1}%</span>" +
                              "</li><li class=\"gridrow\"><span class=\"bigfont\">{1}" +
                              "</span></li> <li class=\"gridrow\">{3}" +
                              "</li><li class=\"gridrow\">{4}</li><li class=\"gridrow ViewIcon \">{5}</li>" +
                              "</ul></li>";

            var sbld = new StringBuilder();
            var sbldInr = new StringBuilder();
            var cachedNavigation = new CachedNavigationRepository();

            var sites = cachedNavigation.GetSiteUnitList().FirstOrDefault(s => s.DatasetID == id);
            if (sites == null)
                return Content(String.Empty);

            //var glob = (RAMAPP_Mvc3.Localization.ClientCultureInfo)Session["globObj"];
            // glob.SetClientCultureInfo();
            using (var db = new RAMEntities())
            {
                var dataQuality =
                  (from dq in db.DatasetQuality.AsParallel()
                   where sites.DatasetID == dq.DatasetID
                   select new DataValidationSummaryModel
                    {
                        DatasetID = dq.DatasetID,
                        Grade = dq.Grade ?? 0,
                        PcntComplete = dq.PcntComplete ?? 0,
                        ChecksNC = dq.ChecksNC ?? 0,
                        ChecksNA = dq.ChecksNA ?? 0,
                        SectionSummary = (from us in db.DatasetQualityBySection
                                          join sc in db.QuestionSection on us.Section.Trim() equals sc.Name.Trim()
                                          where us.DatasetID == dq.DatasetID && sc.DataLevel.Equals("Site")
                                          orderby us.SectionSortKey descending
                                          select new DataValidationSectionModel
                                          {
                                              Section = us.Section,
                                              QuestionNo = us.QuestionCount ?? 0,
                                              PcntComplete = us.PcntComplete ?? 0,
                                              MissingData = us.MissingData ?? 0,
                                              Grade = us.Grade ?? 0,
                                              DatasetID = us.DatasetID,
                                              ChecksOK = us.ChecksOK ?? 0,
                                              ChecksNC = us.ChecksNC ?? 0,
                                              ChecksNA = us.ChecksNA ?? 0,
                                              ChecksIN = us.ChecksIN ?? 0,
                                              SectionID = sc.QuestionSectionID
                                          }
                                            )
                    }).ToArray();

                var dataQualityUnits = (from k in sites.UnitsOfSite.AsParallel()
                                        join k2 in db.DatasetQuality.AsParallel() on k.DatasetID equals k2.DatasetID
                                        orderby k2.Grade descending
                                        select new DataValidationSummaryModel
                                        {
                                            FacilityName = k.UnitName,
                                            DatasetID = k.DatasetID,
                                            Grade = k2.Grade ?? 0,
                                            PcntComplete = k2.PcntComplete ?? 0,
                                            ChecksNC = k2.ChecksNC ?? 0,
                                            ChecksNA = k2.ChecksNA ?? 0,
                                            SectionSummary = (from us in db.DatasetQualityBySection
                                                              join sc in db.QuestionSection on us.Section.Trim() equals sc.Name.Trim()
                                                              where us.DatasetID == k.DatasetID && sc.DataLevel.Equals("Unit")
                                                              orderby us.SectionSortKey descending
                                                              select new DataValidationSectionModel
                                                              {
                                                                  Section = us.Section,
                                                                  QuestionNo = us.QuestionCount ?? 0,
                                                                  PcntComplete = us.PcntComplete ?? 0,
                                                                  MissingData = us.MissingData ?? 0,
                                                                  Grade = us.Grade ?? 0,
                                                                  DatasetID = us.DatasetID,
                                                                  ChecksOK = us.ChecksOK ?? 0,
                                                                  ChecksNC = us.ChecksNC ?? 0,
                                                                  ChecksNA = us.ChecksNA ?? 0,
                                                                  ChecksIN = us.ChecksIN ?? 0,
                                                                  SectionID = sc.QuestionSectionID
                                                              }
                                             )
                                        }).ToArray();





                var dqr = dataQuality.First();
                var enClt = new CultureInfo("en-US");
               
                var len = dataQualityUnits.Length;
                var t = 0;
    do
    {
         var pctCm = String.Format(enClt,"{0:N1}", dqr.PcntComplete);
         var arr = dqr.SectionSummary.ToArray();
         var inlen = arr.Count();
%>
<ul>
  
    
   <li class='rightPointerNav'>
   <ul  class="gridbody" scale="<%: dqr.Grade %>" pctcm='<%:pctCm %>'>
   <li class="gridrowtext" style="text-indent:30px;">Site Level Data</li>
   <li class="gridrow"> <span class='bar' style='width<%:pctcm %>%;'><%: String.Format("{0:N1}",dqr.PcntComplete)%>%</span> </li>
   <li class="gridrow"><span class="bigfont"><%: dqr.Grade %></span></li> 
   <li class="gridrow"><%: dqr.ChecksNC%></li>
   <li class="gridrow"><%: dqr.ChecksNA%></li>
   <li class="gridrow ViewIcon"><%: "<a href='" +
                                                Url.Action("DataSectionSummary", "Review", new { id = dqr.DatasetID, sectionID = "" }) +
                                         "'>&nbsp;</a>"%> </li> 
   </ul>
   <div class='unitsContainer'>
   <ul>
<% for (var z = inlen - 1; z >= 0; z--)
                {
                    var st = arr[z];
                    var upctCm = String.Format(enClt, "{0:N1}", dqr.PcntComplete);    
        %>



                    <li class='nocursor' >
                    <ul  class="gridbody" scale="<%:st.Grade %>" pctcm='<%:upctCm %>'>
                    <li class="gridrowtext"  style="text-indent:60px;">{0}</li>
                    <li class="gridrow"><span class='bar' style='width:<%:upctCm %>%;'><%: String.Format("{0:N1}",st.PcntComplete)%>%</span></li>
                    <li class="gridrow"><span class="bigfont"><%: st.Grade %></span></li> 
                    <li class="gridrow"><%: st.ChecksNC%></li>
                    <li class="gridrow"><%: st.ChecksNA%></li>
                    <li class="gridrow ViewIcon"><%: "<a href='" +
                                                            Url.Action("DataSectionSummary", "Review", new { id = dqr.DatasetID, sectionID = st.SectionID.Trim() }) +
                                         "'>&nbsp;</a>"%></li>
                    </ul>
                    </li>



<%} %>
</ul>
   </div>
   </li>
    
  
</ul>

<% 
     dqr = dataQualityUnits[t++];
    }while( t <= len );
    } %>