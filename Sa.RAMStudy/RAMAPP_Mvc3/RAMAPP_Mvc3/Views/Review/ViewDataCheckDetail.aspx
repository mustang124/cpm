﻿<%@ Page Title="Input Review Description" Language="C#" MasterPageFile="~/Views/Shared/RAM_Validation.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.DataCheckDetailsModel>" %>

<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%@ Import Namespace="RAMAPP_Mvc3.Models" %>
<asp:Content ID="Content3" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% var scriptUrl = Page.ResolveUrl("~/Scripts"); %>
    <script src="<%=  scriptUrl + "/globalize.min.js" %>" type="text/javascript"></script>
    <script src="<%=  scriptUrl + "/cultures/globalize.cultures.js" %>" type="text/javascript"></script>
    <script src="<%=  scriptUrl + "/jquery.qtip.min.js" %>" type="text/javascript"></script>
    <script src="<%=  scriptUrl + "/jquery.dd.js"%>" type="text/javascript"></script>
    <%
        var cp = SessionUtil.AdminKey();
        var mode = Session["Mode"];
        var IsSAISession = mode != null ? (mode.ToString() == cp) : false;
        
        //Set thread culture
        var glob = (RAMAPP_Mvc3.Localization.ClientCultureInfo)Session["globObj"];
        glob.SetClientCultureInfo();

        var secID = "";

        if (Request.UrlReferrer != null)
        {
            if (Request["ovsec"] == null)
            {
                var parseResult = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);
                secID = parseResult["sectionID"];
                Session["OrgOvSec"] = secID;
            }
            else if (Request["ovsec"].Equals("939") && Session["OrgOvSec"] != null)
            {
                secID = Session["OrgOvSec"].ToString();
            }
        }

    %>
    <script type="text/javascript">
        var selectedCulture = '<%= System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
        Globalize.culture(selectedCulture);

        //Configure validator
        $.validator.methods.number = function (value, element) {
            return this.optional(element) || !isNaN(Globalize.parseFloat(value, selectedCulture));
        }

        $(function () {
            Globalize.culture(selectedCulture);
        });
    </script>
    <div style="position: absolute; display: block; left: 75%; top: 30px; color: #ccc;
        font-size: .85em;">
        <%:Html.ActionLink("Return to Input Review Summary", "DataSectionSummary2", "Review", new { id = this.RouteData.Values["id"], sectionID = secID },null)%>
        <%--  <%:Html.ActionLink("Return to List", "ViewGradeDetails", "Admin", new { id = this.RouteData.Values["id"], start = pgStart, size = pgSize, name = facName }, null)%>--%>
    </div>
    <div id="currentSec" style="font-size: .85em; margin: 5px 0;">
        <font color="#666666">Facility:
            <%=  Session["ValFacility"]%></font>
        <br />
        <%--   Section:
        <%= ViewData["Section"]%>&nbsp; &raquo;&nbsp;<%= ViewData["SubSection"]%>
        <% if (ViewData["SubSectionDesc"].ToString().Trim().Length > 0)
           {%>
            <a href="#" tooltip="<%= ViewData["SubSectionDesc"] %>"><img align="middle" border="0" alt='information' src="<%= Page.ResolveUrl("~/images/info.gif") %>" /></a>
        <% } %>--%>
        <div style="height: 1px; min-width: 400px; border-bottom: thin wheat solid;">
            &nbsp;</div>
    </div>
   

    <%--<div>
        <%=  ViewData["Question"] %>
        &nbsp;
        <% using (Html.BeginForm("ViewPropertyDataCheck", "Admin", new { id = this.RouteData.Values["id"], prpNo = 0 }))
           {%>
        <br />

        Reported Value: &nbsp;<%= ViewData["InputField"] %> <%--<%: Html.TextBox("tbAnswer", null, new { size = "15", NumFormat = ViewData["NumberFormat"], DecPlaces = ViewData["DecimalPlaces"] })%>--%>
    <%--  &nbsp;<%: ViewData["PropertyUOM"] %>&nbsp;
        <%: Html.Hidden("NumberFormat", ViewData["NumberFormat"])%>
        <%: Html.Hidden("PropertyName", ViewData["PropertyName"])%>
        <input type="button" name="btnUpdate" value="Update" onclick="this.form.submit();"/>
        <% } %>

       <span style="color:#991000;"><%: Session["MessageDC"].ToString() %></span>
    </div>--%>
    <%-- <div>
        <div class="headerIntro">
       <strong > #<%: Request["datacheckId"]%>- <%: Html.DisplayFor(model =>model.DataCheckName)  %></strong>
    </div>--%>
    
    <% if (Request["retmsg"] != null)
       {%>
 <%--   <div style="color: #991000;">
        <%= Request["retmsg"] != null ? Request["retmsg"] : ""%>
    </div>--%>
    <br />
          <div id="ReturnMsg" class="GoodMsg">
                      <span class="ResponseMsg"><%:   Request["retmsg"] %></span>
           </div>
    <%} %>
    <table style="width: 100%;" cellspacing="8" align="left">
        <tr>
            <td width="50%">
                 <%
        var dsId = this.RouteData.Values["id"];

        //var naList = db.ExecuteStoreQuery<DataPropertyDetailModel>("SELECT gd.*,lu.StatusText FROM dbo.GetDatachecks({0},{1},0) gd join dbo.DatacheckStatus_LU lu on lu.StatusCode=gd.StatusCode gd='NA' Order By RowNumber", id, sectionID).ToArray();
        var naList = Session["saNAList" + dsId] as List<DataPropertyDetailModel>;
        
        if (naList != null && naList.Count() > 0)
        {
    %>
   <div id="CommentedNav">
    <label style="font-family:Arial;font-size:15px;color:#991000">Other Datachecks</label><br />
    <select name="sltNaList" id="sltNaList">
   
        <% 
            foreach (var m in naList)
           { %>
           <option class="<%: m.StatusCode%>Code" 
                   value="<%: m.DatacheckID %>" <%:  m.DatacheckID == Convert.ToInt32( Request["datacheckId"]) ? "selected=selected" :" " %>
                   title="{image :'<%: Url.Action("GetStatusImage", "Review",new {code=m.StatusCode})%>'}">
                   <%: m.DataCheckName %>
        </option>
        <%} %>
    </select>
    </div>

   <script type="text/javascript">

       $(document).ready(function () {
                     //  $("#sltNaList").msDropDown({ visibleRows: 5, rowHeight: 23, mainCSS: 'dd' });

        }).delegate('#sltNaList', 'change', function(){
                       var action = '<%: Url.Action("ViewDataCheckDetail", "Review", new { id = this.RouteData.Values["id"]})%>?datacheckId=' + $(this).val();

                       document.location.href = action;
        });

    </script>
   
    <% }%>
            </td>
            <td width="50%">

                &nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" width="50%">
                <div id="errSec">
                    <div class="headerIntro">
                        <img alt="<%: Model.StatusCode %>" style="position: relative; top: 0; padding-bottom: 4px;
                            margin-top: 0; vertical-align: top;" src="<%: Url.Action("GetStatusImage", "Review",new {code=Model.StatusImage})%>" />
                        <span class="dcTitleName">DC<%: Request["datacheckId"]%>.&nbsp;
                            <%: Model.DataCheckName  %></span>
                    </div>
                    <p>
                        <%: Model.DatacheckText %></p>
                    <div style="margin: 0 10px;">
                        <% using (Html.BeginForm("ViewDataCheckDetail", "Review", new { id = this.RouteData.Values["id"] }))
                           {%>
                        <% foreach (var table in Model.Tables)
                           {
                        %>
                        <%=  "<p>" + table.TableHtml + "</p>"%>
                        <% } %>
                        <br />
                        <strong>Message:</strong><br />
                        <div style="display: block; width: 85%;">
                            <font color='<%: Model.StatusCode =="OK"? "green":"red"%>'>
                                <%: Model.StatusMessage %></font></div>
                        <%: Html.Hidden("datacheckId",Request["datacheckId"].Trim()) %>
                        <br />
                        <input type="submit" value="Update" style='visibility: <%= Model.Tables.Count() > 0 ? "visible":"hidden" %>;' />
                        <%

                           } %>
                    </div>
                </div>
                <%
                    // var salt = DateTime.Now.AddDays(-3).ToShortDateString() + "00SAI";
                    //var cp = SessionUtil.AdminKey();//new Crypt().Encrypt("00SAI", salt).Replace("=", "");
                    //var mode = Session["Mode"];
                    //var IsSAISession = mode != null ? (mode.ToString() == cp) : false;
                    if (IsSAISession)
                    {  %>
                <div style="font-size: 12px; width: 100%; background: #ccc; height: 28px;">
                    <div style="padding: auto 10; width:auto; margin: auto 10;">
                        &nbsp;Change status code to: &nbsp;
                        <img title="Data acceptable" align="middle" style="margin-bottom:2px" alt="<%: Model.StatusCode %>" width="16px" height="16px"
                             src="<%: Url.Action("GetStatusImage", "Review",new {code="OK"})%>" />&nbsp;<label>Data
                        acceptable </label>&nbsp;
                        <input style="border: 0; margin: 5px 0;height:16px" name="toggleStatus" type="radio" value="OK"
                            onclick="changeCommentStatus('<%:this.RouteData.Values["id"]%>','<%:Request["datacheckId"] %>','OK')" />
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img title="Needs correction or comment" style="margin-bottom:2px" align="middle" alt="<%: Model.StatusCode %>"
                            width="16px" height="16px"  src="<%: Url.Action("GetStatusImage", "Review",new {code="NC"})%>" />&nbsp;<label>Needs
                        correction or comment</label>&nbsp;
                        <input style="border: 0; margin: 5px 0;height:16px" name="toggleStatus" type="radio" value="NC"
                            onclick="changeCommentStatus('<%:this.RouteData.Values["id"]%>','<%:Request["datacheckId"] %>','NC')" />
                    </div>
                </div>
                <%} %>
            </td>
            <td valign="top" align="left" width="50%">
                <strong>Enter Comment Below</strong>
                <% using (Ajax.BeginForm("SubmitDataCheckComment", new { id = this.RouteData.Values["id"], datacheckID = Request["datacheckId"] }, new AjaxOptions { UpdateTargetId = "suggs", HttpMethod = "POST", OnComplete = "refreshDatacheckComment('" + this.RouteData.Values["id"] + "','" + Request["datacheckId"] + "'); changeCommentStatus('" + this.RouteData.Values["id"] + "','" + Request["datacheckId"] + "','NA');" }))
                   { %>
                <p>
                <% if (IsSAISession)
                   { 
                       using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
                       {
                           var dcID = Convert.ToInt32(Request["datacheckId"]);
                           var cmt = (from dcc in db.DatacheckCommentStarters
                                      where dcc.DatacheckID == dcID
                                      select new SelectListItem() { Text = dcc.CommentText.Trim().Substring(0, 75) + "... (" + dcc.SuggestedUse.Trim() + ")", Value = dcc.CommentText.Trim() }).ToList();
                       %>
                       Select predefined comment:&nbsp;
                       <%: Html.DropDownListFor(m => m.DataCheckName, cmt, "-- Select --", new Dictionary<string, object> { {"style", "width: 70%;"},{ "onchange", "insertDatacheckComment();" } })%>
                       <%} %>
                 
                <% } %>
                    <%: Html.TextArea("DataCheckComment", new {@style="width:100%;min-width:350;", cols = "40", rows = "5" })%>
                    <br />
                    <% 
                       using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
                       {
                           object ll = null;
                           var dsID = Convert.ToInt32(this.RouteData.Values["id"]);
                           var comps = (from m in db.UnitInfo
                                        //where m.StudyYear == 2011 && !m.CompanySID.ToUpper().Contains("00SAI") && !m.CompanySID.ToUpper().Contains("TEST")
                                        where m.DatasetID == dsID
                                        select new
                                        {
                                            LockedLevel = db.Reviewable.Where(g => (g.CompanySID == m.CompanySID && g.DataLevel == "Co") ||
                                                                                  (g.DataLevel == "Si" && g.CompanySID == m.CompanySID && db.SiteInfo.Any(s => s.CompanySID == g.CompanySID && g.SiteDatsetID == s.DatasetID))).Select(r => r.DataLevel).FirstOrDefault()
                                        });

                           foreach (var b in comps)
                           {
                               ll = b.LockedLevel;
                           }
                           if (ll == null)
                           {
                              %> <input id="commentBtn" type="submit" name="add" value="Add Comment" /></p> 
                              <%
                           }
                           

                       }
                         %>
                    
                    <input id="spellcheckBtn" type="submit" name="add" value="Spell Check" onclick="hideShowSpellcheck('');" />
                    <div id="suggs" style="width: 100%; min-height: 0px; max-height: 300px; overflow: hidden;"></div>
                <%} %>

                <strong>Comments</strong>
                <div id="commentList" style="width: 100%; min-height: 0px; max-height: 300px; overflow-y: scroll;
                    overflow-x: hidden;">
                    There are no comments
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <%
                    var pgStart = "1";
                    var facName = "";
                    var pgSize = "20";
                    var DatasetID = RouteData.Values["id"] as int?;
                    if (Request.UrlReferrer != null)
                    {
                        var parseResult = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);

                        pgStart = parseResult["start"];
                        pgSize = parseResult["size"];
                        facName = parseResult["name"];

                    }

                    var list = ViewData["DataCheckLists"] as List<RAMAPP_Mvc3.Models.RelatedDataChecks>;
                    if (list.Count() > 0)
                    {
                %>
                <div id="sideBar">
                    <fieldset style="border: none;">
                        <legend>Related Data Checks</legend>
                        <ul style="font-size: .90em; padding-left: 0; list-style: none;">
                            <%

                        foreach (var item in list)
                        {

                            if (Convert.ToInt32(Request["datacheckID"].Trim()).Equals(item.DatacheckID))
                            {
                            %>
                            <li class="currentCheck">
                                <% }
                            else
                            { %>
                                <li class="otherCheck">
                                    <% }%>
                                    <img align="middle" alt="<%: item.StatusCode %>" width="16px" src="<%: Url.Action("GetStatusImage", "Review",new {code=item.StatusCode})%>" />
                                    <span style="display: inline-block; margin-left: 2px; width: 85%;">
                                        <%: Html.ActionLink(item.DatacheckName, "ViewDataCheckDetail", "Review", new { id = this.RouteData.Values["id"], datacheckID = item.DatacheckID,ovsec=939 }, null)%></span>
                                </li>
                                <%} %>
                        </ul>
                    </fieldset>
                </div>
                <%} %>
            </td>
            <td>
            </td>
        </tr>
    </table>
    <script type="text/javascript">

        var baseUrl = '<%: Page.ResolveUrl("~/Review") %>';
        var imageBaseUrl = '<%: Page.ResolveUrl("~/images") %>';
        var statuscode = '<%:Model.StatusCode%>';
        var dsetid = '<%: RouteData.Values["id"] %>';
        var dcid = '<%: Request["datacheckId"] %>';
        // $.ajaxSetup({ cache: false }); function deleteDatacheckComment(c, b, a) { $.post(baseUrl + "/DeleteDatacheckComment", { dataCheckComentId: a }, function (d) { refreshDatacheckComment(c, b) }, "json") } function changeCommentStatus(c, b, a) { $.post(baseUrl + "/ChangeCommentStatus", { id: c, dcid: b, status: a }, function (d) { $img = $("div.headerIntro img"); if (a == "NA") { $img.attr("src", imageBaseUrl + "/Warning_sm.png") } if (a == "NC") { $img.attr("src", imageBaseUrl + "/Error_sm.png") } if (a == "OK") { $img.attr("src", imageBaseUrl + "/Green_Checkmark_sm.png") } }, "json") } function refreshDatacheckComment(d, b) { var c = baseUrl + "/CommentsForDatacheck/" + d + "/" + b; var a = $("#commentList"); a.html("Loading Responses..."); a.load(c, function (f, e, g) { if (e == "error") { $(this).html(f) } }); $("#DataCheckComment").val("") } function formatResult(a, d) { var e = "N"; if (a.attr("NumFormat") == "Int") { e = "N0" } if (a.attr("NumFormat") == "Number" && a.attr("DecPlaces") != null) { if (a.attr("NumFormat") == "Number") { e = "n" + a.attr("DecPlaces") } } if (d != undefined && isNaN(d)) { return } var b = Globalize.parseFloat(d.toString(), "en"); if (isFinite(b)) { var c = Globalize.format(b, e); if (a.is("span")) { a.text(c) } else { if (a.is("input")) { a.val(c) } } } } $(window).load(function () { refreshDatacheckComment(dsetid, dcid); var a = $("span[NumFormat],input[NumFormat]"); for (h = a.length - 1; h >= 0; h--) { $numCell = $(a[h]); if ($numCell.text() != "") { formatResult($numCell, $numCell.text()) } else { formatResult($numCell, $numCell.val()) } } });

        $.ajaxSetup({
            cache: false
        });

       
        function insertDatacheckComment() {

            //$('#DataCheckComment').val($("#ddlDataCheckComments option:selected").text());
            $('#DataCheckComment').val($("#DataCheckName option:selected").val());
       
        }

        function deleteDatacheckComment(id, dcid, commentID) {

            $.post(baseUrl + '/DeleteDatacheckComment',
            {
                "dataCheckComentId": commentID
            }, function (e) {
                refreshDatacheckComment(id, dcid);
            }, 'json');

        }

        function changeCommentStatus(id, dcid, status) {
            //if ( statuscode=='NC')
            //{
            $.post(baseUrl + '/ChangeCommentStatus',
                {
                    "id": id,
                    "dcid": dcid,
                    "status": status
                }, function (e) {
                    $img = $('div.headerIntro img');
                    switch (status) {
                        case 'NA': $img.attr('src', imageBaseUrl + '/Warning_sm.png');
                            break;
                        case 'NC': $img.attr('src', imageBaseUrl + '/Error_sm.png');
                            break;
                        case 'OK': $img.attr('src', imageBaseUrl + '/Green_Checkmark_sm.png');
                            break;

                    }
                  /*  if (status == 'NA')
                        $img.attr('src', imageBaseUrl + '/Warning_sm.png');
                    if (status == 'NC')
                        $img.attr('src', imageBaseUrl + '/Error_sm.png');
                    if (status == 'OK')
                        $img.attr('src', imageBaseUrl + '/Green_Checkmark_sm.png');*/
                }, 'json');
            //}
        }

        function refreshDatacheckComment(id, dcid) {

            var path = baseUrl + '/CommentsForDatacheck/' + id + '/' + dcid;
            var $commentList = $("#commentList");

            $commentList.html('Loading Responses...');
            $commentList.load(path, function (response, status, xhr) {
                if (status == "error")
                    $(this).html(response);
            });
            
            //$('#DataCheckComment').val('');

            var myMistake = $("#mistake").html();
            var myString = document.getElementById('DataCheckComment').value;
            myString.replace(myMistake, function (m, key, value) {
                var oRange = document.getElementById('DataCheckComment').createTextRange();
                oRange.findText(myMistake);
                oRange.select();
                return "Some function of match";
            });

            

        }

        function formatResult(el, value) {
            var format = 'N';

            if (el.attr('NumFormat') == 'Int')
                format = 'N0';

            if (el.attr('NumFormat') == 'Number' && el.attr('DecPlaces') != null) {
                if (el.attr('NumFormat') == 'Number')
                    format = 'n' + el.attr('DecPlaces');
            }

            if (value != undefined && isNaN(value))
                return;

            var tempval = Globalize.parseFloat(value.toString(), 'en');
            if (isFinite(tempval)) {
                var newval = Globalize.format(tempval, format);

                if (el.is('span'))
                    el.text(newval);
                else if (el.is('input'))
                    el.val(newval);
            }

        }

        $(window).load(function () {

            refreshDatacheckComment(dsetid, dcid);
            // $("#websites2").msDropDown({ visibleRows: 5, rowHeight: 23 });
            var $allDecPlaces = $("span[NumFormat],input[NumFormat]");
            for (h = $allDecPlaces.length - 1; h >= 0; h--) {
                $numCell = $($allDecPlaces[h]);
                if ($numCell.text() != "")
                    formatResult($numCell, $numCell.text());
                else
                    formatResult($numCell, $numCell.val());
            }
            /* $allDecPlaces.each(function (t) {

            if ($(this).text() != "")
            formatResult($(this), $(this).text());
            else
            formatResult($(this), $(this).val());
            });*/
        });


        function getInputSelection(el) {
            var start = 0, end = 0, normalizedValue, range, textInputRange, len, endRange;

            if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
                start = el.selectionStart;
                end = el.selectionEnd;
            } else {
                range = document.selection.createRange();

                if (range && range.parentElement() == el) {
                    len = el.value.length;
                    normalizedValue = el.value.replace(/\r\n/g, "\n");

                    // Create a working TextRange that lives only in the input
                    textInputRange = el.createTextRange();
                    textInputRange.moveToBookmark(range.getBookmark());

                    // Check if the start and end of the selection are at the very end
                    // of the input, since moveStart/moveEnd doesn't return what we want
                    // in those cases
                    endRange = el.createTextRange();
                    endRange.collapse(false);

                    if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                        start = end = len;
                    } else {
                        start = -textInputRange.moveStart("character", -len);
                        start += normalizedValue.slice(0, start).split("\n").length - 1;

                        if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                            end = len;
                        } else {
                            end = -textInputRange.moveEnd("character", -len);
                            end += normalizedValue.slice(0, end).split("\n").length - 1;
                        }
                    }
                }
            }

            return {
                start: start,
                end: end
            };

        }

        function replaceSelectedText(elx, text) {
            var el = document.getElementById(elx);
            var sel = getInputSelection(el), val = el.value;
            el.value = val.slice(0, sel.start) + text + val.slice(sel.end);
            document.selection.empty();
        }

        function hideShowSuggestions(suggestion1, suggestion2, mistake) {
            var el = document.getElementById(suggestion1);
            el.style.display = 'none';
            var el2 = document.getElementById(suggestion2);
            el2.style.display = '';

            var oRange = document.getElementById('DataCheckComment').createTextRange();
            oRange.findText(mistake);
            oRange.select();
        }

        function hideShowSpellcheck(hs) {
            document.getElementById('suggs').style.display = hs;
            if (hs == 'none') {
                $("#suggs").html('');
                
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="LeftContent" runat="server">
    <%--<%
        var pgStart = "1";
        var facName = "";
        var pgSize = "20";
        var DatasetID = RouteData.Values["id"] as int?;
        if (Request.UrlReferrer != null)
        {
            var parseResult = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);

            pgStart = parseResult["start"];
            pgSize = parseResult["size"];
            facName = parseResult["name"];

        }

        var list = ViewData["DataCheckLists"] as List<RAMAPP_Mvc3.Models.RelatedDataChecks>;
        if (list.Count() > 0)
        {
    %>
    <div id="sideBar">
        <fieldset style="border: none; position: relative; top: -12px;">
            <legend>Related Data Checks</legend>
            <ul style="font-size: .90em; padding-left: 0; list-style: none;">
                <%

            foreach (var item in list)
            {

                if (Convert.ToInt32(Request["datacheckID"].Trim()).Equals(item.DatacheckID))
                {
                %>
                <li class="currentCheck">
                    <% }
                 else
                 { %>
                    <li class="otherCheck">
                        <% }%>
                        <img align="middle" alt="<%: item.StatusCode %>" style="margin-bottom: 25%;" width="16px"
                            src="<%: Url.Action("GetStatusImage", "Admin",new {code=item.StatusCode})%>" />
                        <span style="display: inline-block; margin-left: 2px; width: 85%;">
                            <%: Html.ActionLink(item.DatacheckName, "ViewDataCheckDetail", "Admin", new { id = this.RouteData.Values["id"], datacheckID = item.DatacheckID }, null)%></span>
                    </li>
                    <%} %>
            </ul>
        </fieldset>
    </div>
    <%} %>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
     <%
         var imageBase = Page.ResolveUrl("~/images");
     %>
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <link href="<%= Url.Content("~/Content/jquery.qtip.min.css")%>" rel="stylesheet"
        type="text/css" />
    <link href="<%= Url.Content("~/Content/dd.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        
        option.OKCode
        {
            background: url(<%= imageBase %>/Green_Checkmark_sm.png) no-repeat;
        }
        option.INCode
        {
            background: url(<%= imageBase %>/BlackBar_sm.png) no-repeat;
        }
        option.NACode
        {
            background: 	#FFFACD ;
        }
        option.NCCode
        {
            background: 	#FA8072 ;
        }
        #tbAnswer
        {
            text-align: right;
        }
        .dcTitleName
        {
            font-size: 90%;
            display: inline-block;
            margin-right: 5px;
            width: 85%;
        }
        #commentBtn
        {
            position: relative;
            margin-left: auto;
            margin-right: auto;
            margin-top: 5px;
        }
        table
        {
            font-size: .95em;
            border: none;
        }
        table thead th
        {
            background: transparent !important;
            border-bottom: thin #999 solid;
            color: #333;
            line-height: 1;
        }
        table tr th
        {
            background: transparent !important;
            color: #991000;
        }
        td > span, th > span
        {
            margin-right: 5px;
            float: right;
        }
        td, th
        {
            border: 0;
        }
        #sideBar
        {
            font-size: .95em;
            width: 550px;
            min-height: 250px;
        }
        #sideBar legend
        {
            font-size: 1.2em;
            color: #991000;
            padding: 1px;
        }
        #errSec
        {
            background-color: #FFF8DC;
            height: 100%;
            width: 95%;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
            min-height: 300px;
            border: #F5DEB3 2px solid;
            margin-top: 10px;
            margin-bottom: 10px;
            behavior: url(PIE.htc);
            padding: 10px;
        }
        #sideBar li.currentCheck
        {
            border-bottom: 1px solid #F5DEB3;
            background-color: #F5DEB3;
        }
        #sideBarli.otherCheck a:hover
        {
            color: #D2691E;
        }
        #sideBar li.otherCheck
        {
            border-bottom: 1px solid #F5DEB3;
        }
        .headerIntro
        {
            font-family: Arial;
            font-size: 14px;
            font-weight: 600;
            color: #991000;
            border-bottom: 1px solid #ccc;
            min-width: 200px;
            max-width: 600px;
            padding-left: 0;
            padding-bottom: 2px;
            margin-top: 0;
            text-align: left;
            height: auto;
            vertical-align: bottom;
        }
        caption
        {
            color: #8B3626;
            font-weight: 700;
        }
        input[type=submit]
        {
            font-size: 14px;
            color: #FFF;
            border: none;
            background: #991000;
        }
        input[type=submit]:hover
        {
            cursor: pointer;
            font-size: 14px;
            color: #FFF;
            border: none;
            background: #660b00;
        }
        input[NumFormat=Number], input[NumFormat=Int]
        {
            float: right;
            text-align: right;
            width: 80%;
        }
        .KeyText
        {
            font-weight: 600;
            color: #991000;
        }
        .DataConnect2
        {
            color: green;
        }
        .DataConnect1
        {
            color: blue;
        }
        tr:nth-child(even), tr:nth-child(odd)
        {
            background: transparent !important;
        }
        #sideBar li.otherCheck a, .CheckedValue
        {
            color: #991000;
        }
      /*  #CommentedNav{ background:#dede; width:auto; }*/
        #sltNaList {font-size:12px; border:solid wheat 1px; height:24px; }
        #sltNaList option.NA { background-image:url(<%: Url.Action("GetStatusImage", "Review",new {code="NA"})%>); padding-left:15px; }
    </style>
    <%--
    <style type="text/css">
        #tbAnswer
        {
            text-align: right;
        }
        .dcTitleName
        {
            font-size: 90%;
            display: inline-block;
            margin-right: 5px;
            width: 85%;
        }
        #commentBtn
        {
            position: relative;
            margin-left: auto;
            margin-right: auto;
            margin-top: 5px;
        }

        table
        {
            font-size: .95em;
            border: none;
        }

        table thead th
        {
            background: transparent !important;
            border-bottom: thin #999 solid;
            color: #333; /*text-align: right;*/
            line-height: 1;
        }

        table tr th
        {
            background: transparent !important;
            color: #991000;
        }
        td > span, th > span
        {
            margin-right: 5px;
            float: right;
        }
        td, th
        {
            border: 0;
        }

        tr:nth-child(even)
        {
            background: transparent !important;
        }

        tr:nth-child(odd)
        {
            background: transparent !important;
        }

        #sideBar
        {
            /*margin-top: 40px;*/
            font-size: .95em; /*padding-right: 5px;*/
            width: 550px; /*background-color: cornsilk;*/
            min-height: 250px; /* border: wheat 2px solid;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
           border-radius: 12px;
            behavior: url(PIE.htc);*/
        }
        #sideBar legend
        {
            padding: 1px;
            font-size: 1.2em;
            color: #991000;
        }
        #errSec
        {
            background-color: cornsilk;
            height: 100%;
            width: 95%;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
            min-height: 300px;
            border: wheat 2px solid;
            margin-top: 10px;
            margin-bottom: 10px;
            padding: 10px;
            behavior: url(PIE.htc);
        }

        #sideBar li.currentCheck
        {
            border-bottom: 1px solid wheat;
            background-color: wheat;
        }
        #sideBar li.otherCheck a
        {
            color: #991000;
        }
        #sideBarli.otherCheck a:hover
        {
            color: chocolate;
        }
        #sideBar li.otherCheck
        {
            border-bottom: 1px solid wheat;
        }

        .headerIntro
        {
            font-family: 'Arial';
            font-size: 14px;
            font-weight: 600;
            color: #991000;
            border-bottom: 1px solid #ccc;
            min-width: 200px;
            max-width: 600px;
            padding-left: 0px;
            padding-bottom: 2px;
            margin-top: 0px;
            text-align: left;
            height: auto;
            vertical-align: bottom;
        }
        caption
        {
            color: #8B3626;
            font-weight: bold;
        }

        input[type="submit"]
        {
            font-size: 14px;
            color: white;
            border: none;
            background: #991000;
        }

        input[type="submit"]:hover
        {
            cursor: pointer;
            font-size: 14px;
            color: white;
            border: none;
            background: #660b00;
        }
        input[NumFormat="Number"], input[NumFormat="Int"]
        {
            float: right;
            text-align: right;
            width: 80%;
        }

        .KeyText
        {
            font-weight: 600;
            color: #991000;
        }
        .CheckedValue
        {
            color: #991000;
        }
        .DataConnect2
        {
            color: green;
        }
        .DataConnect1
        {
            color: blue;
        }
    </style>
    --%>
</asp:Content>