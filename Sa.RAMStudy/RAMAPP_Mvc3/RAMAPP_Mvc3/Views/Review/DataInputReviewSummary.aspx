﻿<%@ Page Title="Input Review Summary" Language="C#" MasterPageFile="~/Views/Shared/RAM_Validation.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Src="~/Views/Shared/AdminMenu.ascx" TagPrefix="uc" TagName="AdminMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%-- <script src="<%= Page.ResolveUrl("~/Scripts/jquery.idTabs.min.js") %>" type="text/javascript"></script>--%>
    <br />
    <p class="headerIntro">
        Input Data Review Summary</p>
    <p>
        &nbsp;</p>
    <div style="margin: 0 auto; font-weight: bold;">
        Company Summary
    </div>
    <%: Html.Raw(TempData["CompanySummary"].ToString()) %>
    <p>
        &nbsp;</p>
    <script src="<%= Page.ResolveUrl("~/Scripts/colorscale.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript">
        var baseUrl = '<%= Page.ResolveUrl("~/Review") %>';
        // $(document).ready(function () { $.cookie("lastrowclicked", null); var a = $("div.gridHorizontal ul.gridbody[scale|pctcm]"); for (gb = a.length; $gridbody = $(a[gb]), gb >= 0; gb--) { var b = $gridbody.attr("scale"); var c = $gridbody.attr("pctcm"); if (b != undefined && c != undefined) { c = Globalize.parseFloat(c, selectedCulture); changeColor($gridbody.find("li:nth-child(3)"), b); changeColor($gridbody.find("li:nth-child(2)>span.bar"), c) } } $(this).delegate("div.gridHorizontal li.downPointerNav ul.gridbody", "click", function (d) { if ($(d.target).parent().is("li.ViewIcon")) { return } var g = $(this).parent(); var e = g.attr("dSetId"); var f = g.find("div.unitsContainer"); if (e != undefined && e != null) { f.hide().load(baseUrl + "/DataValidationSummaryInner/" + e, function () { a = f.find("ul.gridbody[scale|pctcm]"); for (gb = a.length; $gridbody = $(a[gb]), gb >= 0; gb--) { var h = $gridbody.attr("scale"); var i = $gridbody.attr("pctcm"); if (h != undefined && i != undefined) { i = Globalize.parseFloat(i, selectedCulture); changeColor($gridbody.find("li:nth-child(3)"), h); changeColor($gridbody.find("li:nth-child(2)>span.bar"), i) } } f.find("div.unitsContainer").toggle(0) }).show(); } else { f.toggle(0) } return false;  }) });
        $(document).ready(function () { $.cookie("lastrowclicked", null); var $gridbodies = $("div.gridHorizontal ul.gridbody[scale|pctcm]"); for (gb = $gridbodies.length; $gridbody = $($gridbodies[gb]), gb >= 0; gb--) { var scale = $gridbody.attr("scale"); var pctcm = $gridbody.attr("pctcm"); if (scale != undefined && pctcm != undefined) { pctcm = Globalize.parseFloat(pctcm, selectedCulture); changeColor($gridbody.find("li:nth-child(3)"), scale); changeColor($gridbody.find("li:nth-child(2)>span.bar"), pctcm) } } $(this).delegate("div.gridHorizontal  li.level1.downPointerNav  ul.gridbody:not(.ViewIcon a)", "click", function (event) { if ($(event.target).parent().is("li.ViewIcon")) { return; } var $obj = $(this).parent(); var dSetId = $obj.attr("dSetId"); var $inner = $obj.find("div.unitsContainer:nth(0)"); if (dSetId != undefined && dSetId != null && !$obj.hasClass("level2")) { $inner.load(baseUrl + "/DataValidationSummaryInner/" + dSetId, function () { $gridbodies = $inner.find("ul.gridbody[scale|pctcm]"); for (gb = $gridbodies.length; $gridbody = $($gridbodies[gb]), gb >= 0; gb--) { var scale = $gridbody.attr("scale"); var pctcm = $gridbody.attr("pctcm"); if (scale != undefined && pctcm != undefined) { pctcm = Globalize.parseFloat(pctcm, selectedCulture); changeColor($gridbody.find("li:nth-child(3)"), scale); changeColor($gridbody.find("li:nth-child(2)>span.bar"), pctcm) } } $inner.find("div.unitsContainer").toggle(0) }) } else { $inner.toggle(0) } }); $(this).delegate("div.gridHorizontal  li.level2.rightPointerNav  ul.gridbody", "click", function () { var $obj = $(this).parent(); var dSetId = $obj.attr("dsid"); var level = $obj.attr("level"); var $inner = $obj.find("div.unitsContainer:nth(0)"); $inner.load(baseUrl + "/DataSummaryFurtherDetails/" + dSetId + "?level=" + level, function (response, status, xhr) { $gridbodies = $inner.find("ul.gridbody[scale|pctcm]"); for (gb = $gridbodies.length; $gridbody = $($gridbodies[gb]), gb >= 0; gb--) { var scale = $gridbody.attr("scale"); var pctcm = $gridbody.attr("pctcm"); if (scale != undefined && pctcm != undefined) { pctcm = Globalize.parseFloat(pctcm, selectedCulture); changeColor($gridbody.find("li:nth-child(3)"), scale); changeColor($gridbody.find("li:nth-child(2)>span.bar"), pctcm) } } $inner.find("div.unitsContainer").toggle(0) }) }) });
<%-- 
        $(document).ready(function () {

            $.cookie('lastrowclicked', null);

            var $gridbodies = $("div.gridHorizontal ul.gridbody[scale|pctcm]");

            for (gb = $gridbodies.length; $gridbody = $($gridbodies[gb]), gb >= 0; gb--) {

                var scale = $gridbody.attr("scale");
                var pctcm = $gridbody.attr("pctcm");
                if (scale != undefined && pctcm != undefined) {
                    pctcm = Globalize.parseFloat(pctcm, selectedCulture);
                    changeColor($gridbody.find('li:nth-child(3)'), scale);
                    changeColor($gridbody.find('li:nth-child(2)>span.bar'), pctcm);
                }

            }
            //li.downPointerNav
            $(this).delegate("div.gridHorizontal  li.level1.downPointerNav  ul.gridbody", "click", function (event) {

                alert('in');
                if ($(event.target).parent().is('li.ViewIcon')) {
                    alert(2);
                    return;
                }
                var $obj = $(this).parent();
                var dSetId = $obj.attr('dSetId');
                var $inner = $obj.find('div.unitsContainer:nth(0)');

                if (dSetId != undefined && dSetId != null && !$obj.hasClass('level2')) {

                    //  $inner.load('<%= Page.ResolveUrl("~/Admin/DataValidationSummaryInner") %>/' + dSetId, function () {
                    $inner.load(baseUrl + '/DataValidationSummaryInner/' + dSetId, function () {

                        $gridbodies = $inner.find("ul.gridbody[scale|pctcm]");
                        for (gb = $gridbodies.length; $gridbody = $($gridbodies[gb]), gb >= 0; gb--) {
                            var scale = $gridbody.attr("scale");
                            // var pctcm = $gridbody.attr("pctcm");
                            var pctcm = $gridbody.attr("pctcm");

                            if (scale != undefined && pctcm != undefined) {
                                pctcm = Globalize.parseFloat(pctcm, selectedCulture);
                                changeColor($gridbody.find('li:nth-child(3)'), scale);
                                changeColor($gridbody.find('li:nth-child(2)>span.bar'), pctcm);
                            }
                        }
                        $inner.find('div.unitsContainer').toggle(0);
                    });
                }
                else {
                    $inner.toggle(0);
                }
            });

            $(this).delegate('div.gridHorizontal  li.level2.rightPointerNav  ul.gridbody', 'click', function () {
                var $obj = $(this).parent();
                var dSetId = $obj.attr('dsid');
                var level = $obj.attr('level');
                var $inner = $obj.find('div.unitsContainer:nth(0)');
                
                $inner.load(baseUrl + '/DataSummaryFurtherDetails/' + dSetId + "?level=" + level, function (response, status, xhr) {
                    $gridbodies = $inner.find("ul.gridbody[scale|pctcm]");
                    for (gb = $gridbodies.length; $gridbody = $($gridbodies[gb]), gb >= 0; gb--) {
                        var scale = $gridbody.attr("scale");
                        // var pctcm = $gridbody.attr("pctcm");
                        var pctcm = $gridbody.attr("pctcm");

                        if (scale != undefined && pctcm != undefined) {
                            pctcm = Globalize.parseFloat(pctcm, selectedCulture);
                            changeColor($gridbody.find('li:nth-child(3)'), scale);
                            changeColor($gridbody.find('li:nth-child(2)>span.bar'), pctcm);
                        }
                    }
                    $inner.find('div.unitsContainer').toggle(0);
                });
            });
        });--%>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
      .OKStatus,.GreenCheckStatus{background:url(<%:Page.ResolveUrl("~/images/Green_Checkmark_sm.png") %>) no-repeat 50% 50%}
.BlackBarStatus,.INStatus{background-image:url(<%:Page.ResolveUrl("~/images/BlackBar_sm.png") %>)}
.NAStatus,.YellowCircleStatus{background-image:url(<%:Page.ResolveUrl("~/images/Warning_sm.png") %>)}
.NCStatus,.RedXStatus{background-image:url(<%:Page.ResolveUrl("~/images/Error_sm.png") %>)}
li.ViewIcon{background:url(<%:Page.ResolveUrl("~/images/viewIcon.png")%>) no-repeat 50% 50%}
li.ViewIcon a{position:relative;z-index:20;display:block;width:24px;height:28px;margin:0 auto}
li.ViewIcon a:hover{border:thin solid #87CEEB}
.usual{width:850px}
.usual li{list-style:none;float:left}
.usual ul a{display:block;text-decoration:none!important;color:#FFF;background:#991000;margin:1px 1px 1px 0;padding:6px 10px}
.usual ul a:hover{color:#FFF;background:#111}
.usual ul a.selected{margin-bottom:0;color:#000;background:#FFFAFA;border-bottom:1px solid #FFFAFA;cursor:default}
.usual div{margin-top:-15px;clear:left;border-top:thin 2px #ccc;padding:3px 10px 8px}
.usual div a{color:#000;font-weight:700}
.gridHorizontal{min-width:500px}
.gridHorizontal ul.gridbody{display:table;font-size:.90em}
.gridHorizontal li div.unitsContainer{display:block;background:#FFF8DC;min-width:738px;border-right:1px solid #ddd;border-bottom:1px solid #F33;height:auto;margin:0 auto}
.gridHorizontal li div.unitsContainer div.unitsContainer{background-color:#EEE8CD}
.gridHorizontal li div.unitsContainer ul.gridbody{margin:0}
.gridHorizontal ul li{min-height:28px}
.gridHorizontal li.gridrow{display:inline-block;float:left;border-right:#ddd 1px solid;width:100px}
.headerIntro{font-family:Arial;font-size:16px;color:#991000;border-bottom:1px solid #ccc;min-width:200px;max-width:600px;padding-left:0;margin-top:0;text-align:left;height:20px;vertical-align:bottom;text-transform:capitalize}
.gridbody:hover{background-color:#FFFBCC!important;border:thin solid #F0E68C}
.gridHorizontal li.gridrowtext{display:inline-block;text-align:left;width:225px;padding-left:10px;border-right:#ddd 1px solid;font-family:Arial,Verdana;font-weight:500}
.gridHorizontal li.overall{font-weight:600;color:#8B0000}
.nocursor{cursor:default!important}
.bar{float:left;display:inline-block;height:12px;text-align:center;border:thin ridge #333;text-shadow:1px 1px 1px rgba(0,0,0,0.2);-moz-border-radius:2px;-webkit-border-radius:2px;border-radius:2px;max-width:97.5%}
    </style>
    <%--
 <style type="text/css">
        .OKStatus,.GreenCheckStatus
        {
        	background : url(<%:Page.ResolveUrl("~/images/Green_Checkmark_sm.png") %>) no-repeat 50% 50%;

        }
        .BlackBarStatus,.INStatus
        {
        	background-image : url(<%:Page.ResolveUrl("~/images/BlackBar_sm.png") %>);
        	}
        .NAStatus,.YellowCircleStatus
        {
        	background-image : url(<%:Page.ResolveUrl("~/images/Warning_sm.png") %>);
        }
        .NCStatus,.RedXStatus
        {
        	background-image : url(<%:Page.ResolveUrl("~/images/Error_sm.png") %>);

        }
        li.ViewIcon
        {
        	background: url(<%:Page.ResolveUrl("~/images/search-sm.png")%>)  no-repeat 50% 50%;
        }
         li.ViewIcon a
         {
         	margin: 0 auto;
         	display:block;
         	width:24px;
         	height:28px;
         }

         li.ViewIcon a:hover
         {
         	border: thin solid skyblue;

         }

    	.usual
        {
	        width: 850px;

        }
.usual li { list-style:none; float:left; }
.usual ul a {
  display:block;
  padding:6px 10px;
  text-decoration:none!important;
  margin:1px;
  margin-left:0;

  color:#FFF;
  background:#991000;
}

.usual ul a:hover {
  color:#FFF;
  background:#111;
  }
.usual ul a.selected {
  margin-bottom:0;
  color:#000;
  background:snow;
  border-bottom:1px solid snow;
  cursor:default;
  }
.usual div {
 padding:10px 10px 8px 10px;
   *padding-top:3px;
  *margin-top:-15px;
  clear:left;
	border-top: thin 2px #ccc;

}
.usual div a { color:#000; font-weight:bold; }
        .gridHorizontal
        {
            min-width: 500px;
        }

        .gridHorizontal ul.gridbody
        {
            display: table;
            font-size: .90em;
        }

        .gridHorizontal li div.unitsContainer
        {
            display: block;
            background: cornsilk;
            min-width: 738px;
            margin: 0 auto;
            border-right: 1px solid #ddd;
            border-bottom: 1px solid #FF3333;
        	height: auto;
        }
         .gridHorizontal li div.unitsContainer div.unitsContainer
        {
        	 background-color: #EEE8CD /*wheat*/;
        }

        .gridHorizontal li div.unitsContainer ul.gridbody
        {
            /*background-color: white ;*/
            margin: 0;
        }

       .gridHorizontal ul li
       {
            min-height: 28px;
       }

        .gridHorizontal li.gridrow
        {
            display: inline-block;
            float: left;
            border-right: #ddd 1px solid;
            width: 100px;

        }
        .headerIntro
        {
            font-family: 'Arial';
            font-size: 16px;
            color: #991000;
            text-transform: capitalize;
            border-bottom: 1px solid #ccc;
            min-width: 200px;
            max-width: 600px;
            padding-left: 0px;
            margin-top: 0px;
            text-align: left;
            height: 20px;
            vertical-align: bottom;
            text-transform: capitalize;
        }
     .gridbody:hover {
        	background-color: #FFFBCC!important;
        	border: thin solid khaki;

        }
        .gridHorizontal li.gridrowtext
        {
            display: inline-block;
            text-align: left;
            width: 225px;
            padding-left: 10px;
            border-right: #ddd 1px solid;
        	font-family: Arial,Verdana;
        	font-weight: 500;

        }

        /*.gridHorizontal .unitsContainer li.gridrowtext
        {

            width: 150px!important;

        }*/
    .gridHorizontal li.overall {
    	font-weight: 600;
    	color: darkred;
    }
    .nocursor
    {
    	cursor: default !important;
    }
    .bar {

    	float:left;
    	display:inline-block;
    	height:12px;
    	text-align:center;

    	border: thin ridge #333;
    	text-shadow:1px 1px 1px rgba(0,0,0,0.2);
        -moz-border-radius:2px;
        -webkit-border-radius:2px;
        border-radius:2px;
       max-width: 97.5%;
    }

    </style>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>