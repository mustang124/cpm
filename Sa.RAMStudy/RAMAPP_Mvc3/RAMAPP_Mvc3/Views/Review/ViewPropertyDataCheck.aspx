﻿<%@ Page Title="Input Review Description" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.DataPropertyDetailModel>" %>

<%@ Import Namespace="System.Web" %>
  
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="<%= Page.ResolveUrl("~/Scripts/globalize.js")%>" type="text/javascript"></script>
    <script src="<%= Page.ResolveUrl("~/Scripts/cultures/globalize.cultures.js")%>" type="text/javascript"></script>
    <script src="<%= Page.ResolveUrl("~/Scripts/jquery.qtip.min.js")%>" type="text/javascript"></script>
  <%
        var pgStart = "1";
        var facName = "";
        var pgSize = "20";
        
        if (Request.UrlReferrer != null)
        {
           var parseResult=  HttpUtility.ParseQueryString(Request.UrlReferrer.Query);

           pgStart = parseResult["start"];
           pgSize = parseResult["size"];
           facName = parseResult["name"];
           
        }

        var glob = (RAMAPP_Mvc3.Localization.ClientCultureInfo)Session["globObj"];
        glob.SetClientCultureInfo();
    %>

    <script type="text/javascript">
        var selectedCulture = '<%= System.Threading.Thread.CurrentThread.CurrentCulture.Name %>';
        Globalize.culture(selectedCulture);

        //Configure validator
        $.validator.methods.number = function (value, element) {
            return this.optional(element) || !isNaN(Globalize.parseFloat(value, selectedCulture));
        }

        $(function () {
            Globalize.culture(selectedCulture);
        });
    </script>
  
    <div style="display: block; position: absolute; left: 520px; top:50px;color: #ccc; font-size: .85em;">
      <%:Html.ActionLink("Return to List", "ViewGradeDetails", "Review", new { id = this.RouteData.Values["id"], start = pgStart, size = pgSize, name = facName }, null)%>
    </div>
  
    <div id="currentSec" style="font-size: .85em; margin: 5px 0;">
      <font color="#666666"> Facility:
        <%: Session["ValidateFacility"]%></font> <br />
         Section:
        <%= ViewData["Section"]%>&nbsp; &raquo;&nbsp;<%= ViewData["SubSection"]%>
        <% if (ViewData["SubSectionDesc"].ToString().Trim().Length > 0)
           {%>
            <a href="#" tooltip="<%= ViewData["SubSectionDesc"] %>"><img align="middle" border="0" alt='information' src="<%= Page.ResolveUrl("~/images/info.gif") %>" /></a>
        <% } %>
       
        <div style="height: 1px; width: 100%; border-bottom: thin wheat solid;">
            &nbsp;</div>
    </div>
    <div>
        <%=  ViewData["Question"] %>
        &nbsp;
        <% using (Html.BeginForm("ViewPropertyDataCheck", "Review", new { id = this.RouteData.Values["id"], prpNo = Model.PropertyNo }))
           {%>
        <br />
        
        Reported Value: &nbsp;<%= ViewData["InputField"] %> <%--<%: Html.TextBox("tbAnswer", null, new { size = "15", NumFormat = ViewData["NumberFormat"], DecPlaces = ViewData["DecimalPlaces"] })%>--%>
        &nbsp;<%: ViewData["PropertyUOM"] %>&nbsp;
        <%: Html.Hidden("NumberFormat", ViewData["NumberFormat"])%>
        <%: Html.Hidden("PropertyName", ViewData["PropertyName"])%>
        <input type="button" name="btnUpdate" value="Update" onclick="this.form.submit();"/>
        <% } %>
        
       <span style="color:#991000;"><%: Session["MessageDC"].ToString() %></span> 
    </div>
    <div>
        <!--div class="headerIntro">
       <strong > #<%: Html.DisplayFor(model =>model.DatacheckID)  %> - <%: Html.DisplayFor(model =>model.DataCheckName)  %></strong>
    </div-->
        <div style="color: #991000;">
            <%= ViewData["Message"] %></div>
        <table style="width: 600px">
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="errSec">
                        
                        
                        <div class="headerIntro">
                            <img alt="<%: Model.StatusCode %>" style="position:relative;top:0; padding-bottom: 4px;margin-top:0; vertical-align: top;"
                                src="<%: Url.Action("GetStatusImage", "Admin",new {code=Model.StatusImage})%>" />
                          <span class="dcTitleName">  DC<%: Html.DisplayFor(model =>model.DatacheckID)%>.&nbsp;
                            <%: Html.DisplayFor(model =>model.DataCheckName)  %></span>
                        </div>
                        <p>
                            <%: Html.DisplayFor(model => model.DataCheckText)%></p>
                        <div style="margin: 0 10px;">
                            <%="<p>" + Model.Table1 + "</p>" %>
                            <%= "<p>" + Model.Table2 + "</p>" %>
                            <%= "<p>" + Model.Table3 + "</p>" %>
                            <br/>
                              Message: <font color='<%: Model.StatusCode =="OK"? "green":"red"%>'>
                                <%: Html.DisplayFor(model => model.StatusMessage )%></font>
                        </div>
                        
                        <!--p>
                        Value Reported : <font color='#991000'>
                            <%: Html.DisplayFor(model => model.CheckValue )%></font></p-->
                        <!--h5>
                        Relating to Question
                    </h5>
                    <div>
                        <%--<%if (ViewData["SubSectionDesc"] != null)
                          {%>
                        <%= ViewData["SubSectionDesc"] %>
                        <% } %>--%>
                    </div -->
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Responses</strong>
                    <div id="commentList" style="width: 100%; min-height: 0px; max-height: 300px; overflow-y: scroll;overflow-x: hidden;">
                        There are no comments
                    </div>
                    <% using (Ajax.BeginForm("SubmitDataCheckComment", new { id = this.RouteData.Values["id"], datacheckID = Model.DatacheckID }, new AjaxOptions { HttpMethod = "POST", OnComplete = "refreshDatacheckComment('" + this.RouteData.Values["id"] + "','" + Model.DatacheckID + "')" }))
                       { %>
                    <p>
                        <%: Html.TextArea("DataCheckComment", new {@style="width:100%", cols = "25", rows = "5" })%></p>
                    <p style="float:left;">
                        <input  type="button" value="Add Comment" onclick="$(this).closest('form').submit();" /></p>
                    <%} %>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            cache: false
        });


        function deleteDatacheckComment(id, dcid, commentID) {


            $.post('<%: Page.ResolveUrl("~/Review/DeleteDatacheckComment") %>',
            {
                "dataCheckComentId": commentID
            }, function (e) {

                refreshDatacheckComment(id, dcid);
            }, 'json');

        }

        function refreshDatacheckComment(id, dcid) {

            var path = '<%:Page.ResolveUrl("~/Review/CommentsForDatacheck")%>/' + id + '/' + dcid;
            var $commentList = $("#commentList");

            $commentList.html('Loading Responses...');
            $commentList.load(path, function (response, status, xhr) {

                if (status == "error")
                    $(this).html(response);
                //                else
                //                    $(this).css('background-color', '#eee');


            });


            $('#DataCheckComment').val('');
            //$('#DataCheckComment').focus();

        }

        function formatResult(el, value) {
            var format = 'N';
         
            if (el.attr('NumFormat') == 'Int')
                format = 'n0';

            if (el.attr('DecPlaces') != null) {
                if (el.attr('NumFormat') == 'Number')
                    format = 'n' + el.attr('DecPlaces');
            }

            if (value != undefined && isNaN(value))
                value = 0;
            
            var tempval = Globalize.parseFloat(value.toString(), 'en');
            if (isFinite(tempval)) {
                el.text(Globalize.format(tempval, format));

                if (el.attr('id') == 'tbAnswer')
                    el.val(Globalize.format(tempval, format));
            }

        }

        $(document).ready(function () {


            refreshDatacheckComment('<%: this.RouteData.Values["id"]%>', '<%: Model.DatacheckID %>');

            var $allDecPlaces = $('span[DecPlaces]');

            $allDecPlaces.each(function (t) {
                formatResult($(this), $(this).text());
            });

            
            formatResult($('#tbAnswer'), $('#tbAnswer').val());

            //Setup tooltip style
            var $toolTipLinks = $('a[tooltip]');
            for (var c = 0; c < $toolTipLinks.length; c++) {
                var $tipLink = $toolTipLinks[c];

                $($tipLink).qtip({
                    content: $($tipLink).attr('tooltip'), // Use the tooltip attribute of the element for the content
                    style: { classes: 'ui-tooltip-dark', textAlign: 'justify'}// Give it a crea mstyle to make it stand out
                });
            }

        });
       
        $(document).delegate('#tbAnswer', 'change', function() {
             formatResult($('#tbAnswer'), $('#tbAnswer').val());
        });
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="LeftContent" runat="server">
     <%
         var pgStart = "1";
         var facName = "";
         var pgSize = "20";
         
        if (Request.UrlReferrer != null)
        {
           var parseResult=  HttpUtility.ParseQueryString(Request.UrlReferrer.Query);

           pgStart = parseResult["start"];
           pgSize = parseResult["size"];
           facName = parseResult["name"];
           
        }

       
    %>
    <div id="sideBar">
        <fieldset style=" border: none;position:relative;top:-12px;">
            <legend>Related Data Checks</legend>
            <ul style="font-size: .90em; padding-left: 0; list-style: none;">
                <%
                    var list = ViewData["DataCheckLists"] as List<RAMAPP_Mvc3.Models.DataPropertyDetailModel>;
                    foreach (var item in list) 
                    {

                        if (Model.DatacheckID == item.DatacheckID)
                        {
                %>
                         <li class="currentCheck">
                    <% }
                        else
                        { %>
                        <li class="otherCheck">
                        <% }%>
                        <img align="middle" alt="<%: item.StatusCode %>" style="margin-bottom:25%;" width="16px" 
                            src="<%: Url.Action("GetStatusImage", "Admin",new {code=item.StatusImage})%>" />
                        <span style="display: inline-block; margin-left: 2px; width: 85%;">
                            <%: Html.ActionLink(item.DataCheckName, "ViewPropertyDataCheck", "Review", new { id = item.DatasetID ,datacheckID = item.DatacheckID , PropertyNo = item.PropertyNo, start=pgStart, size=pgSize, name=facName },null)%></span>
                    </li>
              <%} %>
            </ul>
        </fieldset>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <link href="<%= Url.Content("~/Content/jquery.qtip.min.css")%>" rel="stylesheet"
        type="text/css" />
    <style type="text/css">
    	
    	#tbAnswer {
    		text-align: right;
    	}
    	.dcTitleName {
    		font-size: 90%;
    		display: inline-block;
    		margin-right: 5px;
    		width: 85%;
    		
    	}
       input[type='button'] {
       	position: relative;
       	left: 200px;	
       } 
       
        table
        {
            font-size: .95em;
            border: none;
        }
        
        
        table thead th
        {
            border-bottom: thin #999 solid;
            color: #991000;
        	/*text-align: right;*/
        	line-height: 1;
        	
        }
        
        table tr th
        {
            color: #991000;
        }
        td>span,th>span {
        	margin-right: 5px;
        	float: right;
        }
        td, th
        {
            border: 0;
        	
        }
        
        tr:nth-child(even)
        {
            background: #FFF;
        }
        
        tr:nth-child(odd)
        {
            background: #FFF;
        }
        
        #sideBar {
        	
        	 margin-top: 40px; 
        	 font-size: .90em; 
        	 padding-right: 5px;
        width: 200px;
        background-color: cornsilk; 
        min-height: 250px; 
        border: wheat 2px solid;
            -moz-border-radius: 12px; -webkit-border-radius: 12px; border-radius: 12px;
        	behavior: url(PIE.htc);
        }
        #sideBar legend 
        {
        	padding: 1px;
        	
        	font-size: 1em; color: #991000;
        }
        #errSec
        {
            background-color: cornsilk;
            height: 100%;
            width: 100%;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
            min-height: 300px;
            border: wheat 2px solid;
            margin-top: 10px;
            margin-bottom: 10px;
            padding: 10px;
        	behavior: url(PIE.htc);
        }
        
        li.currentCheck
        {
            border-bottom: 1px solid wheat;
        	background-color: wheat;
        }
        li.otherCheck a
        {
           
            color: burlywood;
        }
        li.otherCheck a:hover
        {
        	
            color: chocolate;
        }
        li.otherCheck
        {
            border-bottom: 1px solid wheat;
        }
        
        .headerIntro
        {
            font-family: 'Arial';
            font-size: 14px;
            font-weight: 600;
            color: #991000;
           
            border-bottom: 1px solid #ccc;
            min-width: 200px;
            max-width: 600px;
            padding-left: 0px;
            padding-bottom: 2px;
            margin-top: 0px;
            text-align: left;
            height: auto;
            vertical-align: bottom;
           
        }
        caption {
        	font-weight: bold;
        }
    </style>
</asp:Content>
