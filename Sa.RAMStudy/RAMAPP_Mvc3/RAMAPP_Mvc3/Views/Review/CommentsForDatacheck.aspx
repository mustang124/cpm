﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>


<%
    
    var companySID = SessionUtil.GetCompanySID();
    var id = Convert.ToInt32(ViewContext.RouteData.Values["id"].ToString().Trim());
    var dcid = Convert.ToInt32(ViewContext.RouteData.Values["usr"].ToString().Trim());
    var userNo = ((LogIn)Session["UserInfo"]).UserNo;

    
    if (id == null || dcid == null)
    {

        Response.End();
    }

    using (var db = new RAMEntities())
    {
        var cm = (from h in db.DatacheckComments
                  join l in db.LogIn on h.UserNo equals l.UserNo
                  where h.DatasetID == id && h.DatacheckID == dcid
                  orderby h.Posted descending
                  select new { h.UserNo, h.DatasetID, h.DatacheckID, h.DatacheckCommentID, h.Posted, h.CommentText, l.ScreenName ,l.FirstName ,l.LastName ,SecurityLevel= l.SecurityLevel>=2 ?l.SecurityLevel : 1 }).ToList();

        
        int f = 0;

        if (cm.Count() == 0)
            Response.Write("There are not any comments.");
        else
        foreach (var item in cm)
        {

            var textcolor = db.SecurityLevels.FirstOrDefault(sl => sl.SecurityLevel == item.SecurityLevel).DCCommentColor; 
          %>
<div style="background: <%= (f++%2 == 0) ?  "white" : "white" %>; border: solid 1px #ddd;
    min-width: 255px; max-width: 98%; margin: 10px 2px;">
    <div style="font-size: .80em; color: #333; margin: 0px 5px; width: 98%;">
      
        <%=  item.FirstName != null && item.LastName != null ? item.FirstName + " " + item.LastName : item.ScreenName%> &nbsp; <%= item.Posted.ToLongDateString() %> <%= item.Posted.ToLongTimeString() %>
      
        <%if (item.UserNo == userNo )
          {
                 
         %>
        <div style="position: relative; top: 2px; float: right; display: inline;">
            <img height="12px"; style="cursor: pointer; <%= (userNo == item.UserNo) ? "display:hidden;" : "" %>"
                alt="delete" src="<%= Page.ResolveUrl("~/images/comment_btn_delete.jpg") %>"
                align="middle" border="0" onclick="deleteDatacheckComment(<%= item.DatasetID %>,<%= item.DatacheckID %>,<%= item.DatacheckCommentID %>);" />
        </div>
        <% } %>
    </div>
    <div style="margin: 0px 10px; font-size: .80em; color:<%= textcolor %>; border-top: 1px #666 solid;
        min-width: 90%; min-height: 35px;">
        <%= item.CommentText %>
    </div>
    <!-- div style="position: relative; bottom: 0px; font-size: .80em; color: #333; min-width: 95%;
        height: auto; margin: 0px 5px;">
        <span>
            <%= item.Posted%></span>
        <%--<span style="float: right; cursor: pointer"><a href="javascript:ReplyToComment('')">
                REPLY</a>&nbsp;>
               
            </span--%>
    </div-->
</div>
<%      }
  }
  
  %>