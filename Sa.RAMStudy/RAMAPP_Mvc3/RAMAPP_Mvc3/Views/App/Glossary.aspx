<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_App.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style>
        .downloadBtn
        {
            position: absolute;
            bottom: 50px;
            border-top: 1px solid #96d1f8;
            background: #65a9d7;
            background: -webkit-gradient(linear, left top, left bottom, from(#3e779d), to(#65a9d7));
            background: -webkit-linear-gradient(top, #3e779d, #65a9d7);
            background: -moz-linear-gradient(top, #3e779d, #65a9d7);
            background: -ms-linear-gradient(top, #3e779d, #65a9d7);
            background: -o-linear-gradient(top, #3e779d, #65a9d7);
            padding: 6px 6px;
            -webkit-border-radius: 13px;
            -moz-border-radius: 13px;
            border-radius: 13px;
            -webkit-box-shadow: rgba(0,0,0,1) 0 1px 0;
            -moz-box-shadow: rgba(0,0,0,1) 0 1px 0;
            box-shadow: rgba(0,0,0,1) 0 1px 0;
            text-shadow: rgba(0,0,0,.4) 0 1px 0;
            color: white;
            font: 12px Arial, Sans-Serif;
            font-weight: bold;
            text-decoration: none;
            vertical-align: middle;
            width: 125px;
            text-align: center;
        }
        .downloadBtn a
        {
            color: White;
        }
        .downloadBtn:hover, .downloadBtn a:Hover
        {
            border-top-color: #28597a;
            background: #28597a;
            color: #ccc;
        }
        .downloadBtn:active
        {
            border-top-color: #1b435e;
            background: #1b435e;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="grid_4 noshow" >
        <fieldset style="height: 300px; width: 175px; padding: 20px 10px;">
            <legend>Download</legend>
            <p>
                &nbsp;</p>
            <center>
                <p class="tinylink">
                    <img alt="" src="../../images/doc_pdf.png" border="0" align="middle" />&nbsp; Download
                    the definitions in PDF format
                </p>
                <div class="downloadBtn">
                    <a href="../../Documents/RAM_Study_Glossary.pdf" target="_blank">Click To Download</a>
                </div>
            </center>
        </fieldset>
    </div>
   
        <div class="grid_12 showPrint" style="max-width:785px;"> 
            <%:Html.Raw(ViewData["Glossary"] as string)%>
    </div>
</asp:Content>
