<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_App.Master" Inherits="System.Web.Mvc.ViewPage" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="../../Content/dd_menu_style.css" type="text/css" rel="stylesheet" />
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">	
			<div align=center>
    <IMG  alt="" src="../../images/pdf-icon-transparent.png"  border="0">
	<A style="BACKGROUND-COLOR: #ffffcc" href="RAM Web Data Input Guide.pdf" target="_blank"></A>
	<div style="display:block;width:150px;font-size: xx-small;" class="ui-button ui-icon-arrow-1-e">Download Glossary</div>
	</div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

            <H4>User Guide</H4>
				
			
			<P class="bodyright" align="left">Please review the introduction, instructions, 
				questions, and glossary carefully before entering data. Provide all individuals 
				engaged in this effort with the complete file so they have the necessary 
				infomation. Address your questions to Gary Fuller&nbsp;via e-mail at <A href="mailto:Gary.Fuller@SolomonOnline.com">
					Gary.Fuller@SolomonOnline.com</A>, by phone at (972) 739-1731, or by fax at 
				972-774-1119 Fax.</P>
			<P class="bodyright" align="left">All studies of this type require some allocation 
				of costs or craft labor, based on best judgement and readily available 
				information. We do not intend for these allocations to be unduly 
				time-consuming. Feel free to contact us to discuss allocation methods used by 
				other participants.</P>
			<P class="bodyright" align="left">Enter all percentage data as whole parts of 100, 
				not as decimal fractions. For example, enter 32.5% as 32.5 rather than 0.325. 
				Units should be entered as complete numbers with all the zeros. For example, a 
				plant replacement value of twenty-five million US dollars should be entered as 
				25000000-not 25.</P>

</asp:Content>


