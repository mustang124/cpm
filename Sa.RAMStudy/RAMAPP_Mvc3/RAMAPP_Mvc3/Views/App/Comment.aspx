<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.CommentsModel>" %>

<html>
<head>
    <title>Comment</title>

   
    <!--link href="../../Content/jquery-ui-1.8.15.custom.css" rel="stylesheet" type="text/css" /-->
    <!--script src="../../Scripts/jquery-ui-1.8.15.custom.min.js" type="text/javascript"></script-->
    <!--script src="<%= Page.ResolveUrl("~/Scripts/jquery.unobtrusive-ajax.min.js") %>" type="text/javascript"></script-->
    <style type="text/css">
    
.comment-meta { padding: 3px 5px; background: #f7f7f7; margin: 0 0 15px 0; }

.comment-meta a { font-size: 10px; color: #999; font-weight: normal;
    </style>
</head>
<body bgcolor="white">
    <div id="commentList" style="border: slategray 1px dotted; position: absolute; overflow-x: hidden; overflow-y: visible; width: 425px; height: 275px;top: 10px; left: 35px;">
        <span id="loading"  style="position:relative;top:35%;left:25%; "> Loading comments...</span> 
      
        <!--div class="dragger_container">
    		<div class="dragger">&#9618;</div>
		</div-->
    </div>
    <div style="position: absolute; top: 315px; left: 35px; width: 425px;">
        <font size="1" face="Verdana">You have <b><span id="myCounter">250</span></b> characters
            remaining</font>
        <% using (Ajax.BeginForm("Comment",new { qid = this.RouteData.Values["qid"], rep="comment" }, new AjaxOptions { HttpMethod = "POST",  OnComplete = "refreshComment(" + this.RouteData.Values["id"] + ",'" + this.RouteData.Values["qid"].ToString().Trim() +"')" }))
           {%>
        <%= Html.EditorFor(m => m.newComment.Comment, new { onkeyup = "taCount();", rows = 3, cols = 25, MaxLength = 250, style = "height:48px;width:425px;  border: 1px dotted;" })%>
        
        <%} %>
    </div>
    <script language="Javascript" type="text/javascript">
        function refresh() { window.location.reload() } function taLimit() { var taObj = event.srcElement; if (taObj.value.length == taObj.maxLength * 1) return false } function taCount() { var visCnt = document.getElementById("myCounter"); var taObj = event.srcElement; if (taObj.value.length > taObj.maxLength * 1) taObj.value = taObj.value.substring(0, taObj.maxLength * 1); if (visCnt) visCnt.innerText = taObj.maxLength - taObj.value.length } taObj = document.getElementById("newComment_Comment"); taObj.maxLength = 250; document.getElementById("myCounter").innerText = taObj.maxLength - taObj.value.length;
        <%-- function refresh() {
            window.location.reload();
        }
        function taLimit() {
            var taObj = event.srcElement;
            if (taObj.value.length == taObj.maxLength * 1) return false;
        }

        function taCount() {

            var visCnt = document.getElementById("myCounter");
            var taObj = event.srcElement;

            if (taObj.value.length > taObj.maxLength * 1) taObj.value = taObj.value.substring(0, taObj.maxLength * 1);
            if (visCnt) visCnt.innerText = taObj.maxLength - taObj.value.length;
        }

        taObj = document.getElementById("newComment_Comment");
        taObj.maxLength = 250;
        document.getElementById("myCounter").innerText = taObj.maxLength - taObj.value.length;--%>
    </script>
</body>
</html>
