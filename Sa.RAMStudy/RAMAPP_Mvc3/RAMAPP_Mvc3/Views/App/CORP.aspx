﻿ 
 
  


<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.CORPModel>" %>
<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
		
	 <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
	<script type="text/javascript">
	    $(document).ready(function () {
	        // Smart Wizard 	
	        $("#wizard_CORP").smartTab({ transitionEffect: 'fade', keyNavigation: false });

	        /* $('li .stepDesc small').each(function (index) {
	        var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

	        $(this).text($(elementTitleId).text());

	        });*/

	    });
		</script>
	  <% Html.EnableClientValidation(); %>			   
	  <% using (Ajax.BeginForm("CORP",new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" })) { 
	     bool uomPref = true;
	  %>
	  	<div id="wizard_CORP" class="stContainer">
		    <ul class="tabContainer">
			<li><a href='#step-1'><label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br /><small>Company/Site Support Personnel – Category</small> </span></a></li>
			</ul>
		
			    				  
				  
				  <div class="SubSectionTab"  id="step-1">
				   
					<p class="StepTitle"> COMPANY/SITE SUPPORT PERSONNEL – CATEGORY</p>
					<p><i>Report in the table below the total number of corporate and site reliability and maintenance personnel for both company and contractor for the complete site.</i></p> 
				 
		              			   <div class="questionblock" qid="">
								
								        
						<div id='CORP_PERS' class='grid editor-field' ><ul class="rowdesc"><li>&nbsp;</li> <li class='gridrowtext'><span class='numbering'>1.</span> Corporate <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Manager, Director, or Executive</li> <li class='gridrowtext'><span class='numbering'>2.</span> Corporate <a href="#" tooltip="A technical staff professional employee responsible for applying engineering concepts through the study, evaluation, and implementation of life-cycle management of equipment with the expressed intention of increasing plant availability. Reliability engineers are well versed in the usage of contempo... See Glossary">Reliability Engineer</a>s or Paraprofessionals</li> <li class='gridrowtext'><span class='numbering'>3.</span> Corporate <a href="#" tooltip="A maintenance technical staff professional employee responsible for applying engineering concepts to the optimization of equipment, maintenance resources and maintenance expenses to achieve improved maintainability, resource efficiency, and cost effectiveness.">Maintenance Engineer</a>s or Paraprofessionals</li> <li class='gridrowtext'>Total Corporate Reliability and Maintenance Support</li></ul><ul class='gridbody'>
						<li class='columndesc' style='width:50px;'>Company</li><li class="gridrow"><%: Html.EditorFor(model => model.CorpPersMaintMgr_CO,"IntTmpl",new {size="10" ,CalcTag="sumCorpAll sumCorpCoSpt sumCorpCRCO_A" ,NumFormat="Int"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersRelEngr_CO,"IntTmpl",new {size="10" ,CalcTag="sumCorpAll sumCorpCoSpt sumCorpCRCO_B" ,NumFormat="Int"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersMaintEngr_CO,"IntTmpl",new {size="10" ,CalcTag="sumCorpAll sumCorpCoSpt sumCorpCRCO_C" ,NumFormat="Int"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersTot_CO,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,@class="ttlfld" ,ReadOnly="True"})%></li> </ul><ul class='gridbody'><li class='columndesc' style='width:50px;'>Contractor</li><li class="gridrow"><%: Html.EditorFor(model => model.CorpPersMaintMgr_CR,"IntTmpl",new {size="10" ,CalcTag="sumCorpAll sumCorpCRCO_A sumCorpCRSpt" ,NumFormat="Int"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersRelEngr_CR,"IntTmpl",new {size="10" ,CalcTag="sumCorpAll sumCorpCRCO_B sumCorpCRSpt" ,NumFormat="Int"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersMaintEngr_CR,"IntTmpl",new {size="10" ,CalcTag="sumCorpAll sumCorpCRCO_C sumCorpCRSpt" ,NumFormat="Int"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersTot_CR,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,@class="ttlfld" ,ReadOnly="True"})%></li> </ul><ul class='gridbody'><li class='columndesc' style='width:50px;'>Total</li><li class="gridrow"><%: Html.EditorFor(model => model.CorpPersMaintMgr_Tot,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersRelEngr_Tot,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersMaintEngr_Tot,"IntTmpl",new {size="10" ,NumFormat="Int" ,@class="ttlfld" ,ReadOnly="True"})%></li> <li class="gridrow"><%: Html.EditorFor(model => model.CorpPersTot_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,@class="ttlfld" ,ReadOnly="True"})%></li></ul></div>
						<%: Html.ValidationMessageFor(model => model.CorpPersMaintMgr_CO)%> <%: Html.ValidationMessageFor(model => model.CorpPersRelEngr_CO)%> <%: Html.ValidationMessageFor(model => model.CorpPersMaintEngr_CO)%> <%: Html.ValidationMessageFor(model => model.CorpPersTot_CO)%> <%: Html.ValidationMessageFor(model => model.CorpPersMaintMgr_CR)%> <%: Html.ValidationMessageFor(model => model.CorpPersRelEngr_CR)%> <%: Html.ValidationMessageFor(model => model.CorpPersMaintEngr_CR)%> <%: Html.ValidationMessageFor(model => model.CorpPersTot_CR)%> <%: Html.ValidationMessageFor(model => model.CorpPersMaintMgr_Tot)%> <%: Html.ValidationMessageFor(model => model.CorpPersRelEngr_Tot)%> <%: Html.ValidationMessageFor(model => model.CorpPersMaintEngr_Tot)%> <%: Html.ValidationMessageFor(model => model.CorpPersTot_Tot)%>
								
												
								<div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset>
			    			  </div>
			    <%: Html.HiddenFor(model=>model.CompanySID) %>
  </div> <!-- for last step --> 
  </div>  <!-- for wizard container -->
<%
} //for form using clause 
%>
 


<script type="text/javascript"> function formatResult(el,value){ var format='N'; if(el.attr('NumFormat')=='Int' )  format= 'n0';  if(value!=undefined && isNaN(value)) value = 0 ;    var tempval = Globalize.parseFloat(value.toString().trim(),selectedCulture); el.val(Globalize.format(tempval, format)); }$("input[CalcTag*='sumCorpAll']").sum({bind: "keyup", selector: "#CorpPersTot_Tot",oncalc: function(value,settings){  if(value != undefined && isNaN(value)) formatResult($(settings.selector),value);  }});$('#CorpPersTot_Tot').attr('readonly','readonly');$('#CorpPersTot_Tot').change(function(){$(this).validate();});$("input[CalcTag*='sumCorpCoSpt']").sum({bind: "keyup", selector: "#CorpPersTot_CO",oncalc: function(value,settings){  if(value != undefined && isNaN(value)) formatResult($(settings.selector),value);  }});$('#CorpPersTot_CO').attr('readonly','readonly');$('#CorpPersTot_CO').change(function(){$(this).validate();});$("input[CalcTag*='sumCorpCRCO_A']").sum({bind: "keyup", selector: "#CorpPersMaintMgr_Tot",oncalc: function(value,settings){  if(value != undefined && isNaN(value)) formatResult($(settings.selector),value);  }});$('#CorpPersMaintMgr_Tot').attr('readonly','readonly');$('#CorpPersMaintMgr_Tot').change(function(){$(this).validate();});$("input[CalcTag*='sumCorpCRCO_B']").sum({bind: "keyup", selector: "#CorpPersRelEngr_Tot",oncalc: function(value,settings){  if(value != undefined && isNaN(value)) formatResult($(settings.selector),value);  }});$('#CorpPersRelEngr_Tot').attr('readonly','readonly');$('#CorpPersRelEngr_Tot').change(function(){$(this).validate();});$("input[CalcTag*='sumCorpCRCO_C']").sum({bind: "keyup", selector: "#CorpPersMaintEngr_Tot",oncalc: function(value,settings){  if(value != undefined && isNaN(value)) formatResult($(settings.selector),value);  }});$('#CorpPersMaintEngr_Tot').attr('readonly','readonly');$('#CorpPersMaintEngr_Tot').change(function(){$(this).validate();});$("input[CalcTag*='sumCorpCRSpt']").sum({bind: "keyup", selector: "#CorpPersTot_CR",oncalc: function(value,settings){  if(value != undefined && isNaN(value)) formatResult($(settings.selector),value);  }});$('#CorpPersTot_CR').attr('readonly','readonly');$('#CorpPersTot_CR').change(function(){$(this).validate();});$.getJSON('<%= Url.Action("GetComments","App")%>',{ 'id' : '<%: ViewContext.RouteData.Values["id"] %>' ,  'section' : 'CORP'}, function(data){if(data!=null){	var comments = $.parseJSON(data);	for(i = 0;comments!=null && i < comments.length; i ++ ){		var qidNm = comments[i]['CommentID'];		if($('img[qid="' + qidNm + '"]')){    		$('img[qid="' + qidNm + '"]').attr('src', '<%=Url.Content("~/images/comment_icon_red.png")%>');		}	}}});<%= Model.isUnderReview ? "$('input, select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(  function (index, html) {   var gridId = $(this).closest('.grid').attr('id');   if ($.trim(html).indexOf('nbsp', 0) == -1) {        var newHTML = '<span> ' +       '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' +       ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html +      '</span>';      }    $(this).html(newHTML);   }  ); $('img[suid]').click(function(){ var suid = $(this).attr('suid'); var qid = $(this).attr('qid') ; recordComment(suid,qid); });</script>

</asp:Content> 
