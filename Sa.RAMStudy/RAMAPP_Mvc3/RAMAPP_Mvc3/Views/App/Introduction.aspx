<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_App.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
</asp:Content>


<asp:Content  ContentPlaceHolderID="MainContent" runat="server">

   <h4 id="header"> Study Overview</h4>
 
  
       <P class=bodyright align=justify>The objective of this study of the factors of plant 
      reliability and maintenance effectiveness is to provide participants with 
      realistic performance improvement priorities and with an understanding of 
      their plant's characteristics and maintenance practices that could be 
      contributory to negative performance gaps. The performance indices that 
      together define reliability and maintenance effectiveness are:</P>
  
       <P class=bodyright align=justify><STRONG>Reliability Loss 
      Index</STRONG>     ,<font class="font5"> as measured by the value of productive capacity    loss due to unplanned downtime, annualized planned shutdowns/turnarounds and    slowdowns due to maintenance reasons.&nbsp;</font></P>
  
       <P class=bodyright align=justify><STRONG>Maintenance Direct Cost 
      Index</STRONG>      ,<font class="font5"> as measured by the ratio of direct    maintenance expenses to plant replacement value.</font></P>
 
       <P class=bodyright align=justify><STRONG>Maintenance Overheads Cost 
      Index</STRONG>      ,<font class="font5"> measured by the ratio of maintenance    overhead costs to plant replacement value.</font></P>
  
       <P class=bodyright align=justify><STRONG>Inventory Holding Cost 
      Index,</STRONG>      <font class="font5"> measured as the holding cost of spares    inventory as a percentage of plant replacement value.</font></P>
 
       <P class=bodyright align=justify><STRONG>Operator Maintenance Cost 
      Index</STRONG>      ,<font class="font5"> measured as the cost of operators'    time on maintenance activities as a percentage of plant replacement value.</font></P>
 
       <P class=bodyright align=justify>The <STRONG>Maintenance 
      Effectiveness Index</STRONG>      <font class="font5"> combines the above indices    into a single index representing the global cost attributable to    unreliability and maintenance work.</font></P>
 
       <P class=bodyright align=justify>For the purpose of this study, maintenance is defined as 
      "all the work necessary to conserve plant facilities at their designed 
      performance level." For this study, maintenance work includes:</P>
 
      <UL>
        <LI>
        <DIV align=justify>Emergency urgent repairs and replacement of equipment 
        or parts with or without plant shutdown</DIV>
        <LI>
        <DIV align=justify>Routine non-urgent repairs and replacement 
        of failed parts and equipment</DIV>
        <LI>
        <DIV align=justify>Condition monitoring and inspection of plant and 
        equipment</DIV>
        <LI>
        <DIV align=justify>Cleaning necessary due to fouling, plugging, 
        polymerization and freezing and systematic preventive cleaning, 
        adjustments and lubrication of equipment</DIV>
        <LI>
        <DIV align=justify>Planned and scheduled equipment overhauls, 
        turnarounds and replacements</DIV>
        <LI>
        <DIV align=justify>Catalyst changes (excluding the cost of the catalyst 
        itself)</DIV>
        <LI>
        <DIV align=justify>Maintenance work planning and scheduling</DIV>
        <LI>
        <DIV align=justify>Reliability and maintainability engineering and 
        maintenance "projects"</DIV>
        <LI>
        <DIV align=justify>Collecting, recording, and analyzing equipment 
        reliability, maintenance, and maintenance work performance 
        data</DIV>
        <LI>
        <DIV align=justify>Craft training</DIV></LI></UL>

       <P class=bodyright align=justify>This study specifically excludes certain work which may 
      normally be assigned to the site’s maintenance function, but which is 
      outside this study’s definition of maintenance:</P>
      <UL>
        <LI>
        <DIV align=justify>    Investment/capital works for production, environmental and safety reasons and    installation, and commissioning <font class="font6"> </font><font class="font5">support    to projects</font></DIV>
        <LI>
        <DIV align=justify>Modification projects for production, environmental, 
        and safety reasons</DIV>
        <LI>
        <DIV align=justify>Catastrophic damage repairs (fire, explosion, 
        flooding, etc.) whether or not covered by insurance</DIV>
        <LI>
        <DIV align=justify>Maintenance of facilities and assets not included in 
        this site and process unit inside battery limits (ISBL) study <FONT 
        class=font6></FONT><FONT class=font5>(vehicles, mobile plant, head 
        office premises, research and development laboratories, 
        etc.)</FONT></DIV>
        <LI>
        <DIV align=justify>Work for third parties (local community, separate 
        company, etc.)</DIV>
        <LI>
        <DIV align=justify>Production work (cleanups, transport, packaging, 
        etc.)</DIV>
        <LI>
        <DIV align=justify>Utility production work (steam, electricity, 
        etc.)</DIV></LI></UL>
        
       <P class=bodyright align=justify>The maintenance cost components include:</P>
 
      <UL>
        <LI>
        <DIV align=justify>Direct Costs</DIV>
        <UL>
          <LI>
          <DIV align=justify>Company craft and semi-skilled labor</DIV>
          <LI>
          <DIV align=justify>Work by contractors’ craft and semi-skilled 
          labor</DIV>
          <LI>
          <DIV align=justify>Stores issues (company and vendors)</DIV>
          <LI>
          <DIV align=justify>Material purchases (company and 
          contractors)</DIV></LI></UL></LI></UL>
      <UL>
        <LI>
        <DIV align=justify>Indirect Costs</DIV>
        <UL>
          <LI>
          <DIV align=justify>Supervision, management and support staff (company 
          and contractors)</DIV>
          <LI>
          <DIV align=justify>Consumable workshop and maintenance materials 
          (safety gear, fastenings, etc.)</DIV></LI></UL></LI></UL>
          
       <P class=bodyright align=justify>This study's objective is to specifically confirm the 
      interrelationships of inside battery limits (ISBL) process unit 
      characteristics and organization features with the components of 
      maintenance effectiveness. The study also sets out to quantify 
      correlations so site management can better prioritize the fruitful areas 
      for performance improvements.</P>
 
       <P class=bodyright align=justify>The process units reported can be chosen for convenience 
      of data collection by the participant and may correspond to the site's 
      business lines, process units, etc. The following outside battery limit 
      (OSBL) assets are excluded from process unit facilities:</P>
      
      <UL>
        <LI>
        <DIV align=justify>    Utilities (water/effluent/solid waste treatment, steam/electricity/compressed    air generation). However these assets <font class="font6">  </font><font class="font5">may be separately analyzed in the study as process units in their    own right.</font></DIV>
        <LI>
        <DIV align=justify>Off-sites (OSBL feed and product storage, receiving 
        and loading facilities, and site grounds)</DIV>
        <LI>
        <DIV align=justify>Vehicles and mobile plant</DIV>
        <LI>
        <DIV align=justify>Office facilities not dedicated to process unit 
        operations purposes (e.g., marketing, etc.)</DIV>
        <LI>
        <DIV align=justify>Sports and club facilities</DIV>
        <LI>
        <DIV align=justify>Living accommodation for employees and/or 
        contractors</DIV>
        <LI>
        <DIV align=justify>Laboratories other than plant laboratories</DIV>
        <LI>
        <DIV align=justify>Research and development and pilot 
      plants</DIV></LI></UL>
       <P class=bodyright align=justify>The site data reported should cover all industrial assets 
      on the site. Utilities and off-sites must be included. The on-site assets 
      that should be excluded are:</P>
      <UL>
        <LI>
        <DIV align=justify>Vehicles and mobile plant</DIV>
        <LI>
        <DIV align=justify>Office facilities not dedicated to process unit 
        operations purposes (e.g., marketing, etc.)</DIV>
        <LI>
        <DIV align=justify>Sports and club facilities</DIV>
        <LI>
        <DIV align=justify>Living accommodation for employees and/or 
        contractors</DIV>
        <LI>
        <DIV align=justify>Laboratories other than site laboratories</DIV>
        <LI>
        <DIV align=justify>Research and development and pilot 
      plants</DIV></LI></UL>
       <P class=bodyright align=justify>The study includes analysis for the following OSBL assets 
      when participants supply data:</P>
      <UL>
        <LI>
        <DIV align=justify>Water/effluent/solid waste treatment</DIV>
        <LI>
        <DIV align=justify>Steam/electricity/generation</DIV>
        <LI>
        <DIV align=justify>Storage Tanks</DIV></LI></UL>
       <P class=bodyright align=justify>The questions relate to either process unit data, or to 
      site data. When the question concerns the overall site, answers should 
      give the data or the majority practice for the whole site.&nbsp;</P>
       <P class=bodyright align=justify>All data given should be for the year 2008 unless 
      otherwise specified. Currency data should be given in local 
       currency.</P>
       <P class=bodyright align=justify> The data should be entered through this 
      web site. Refer to the Instructions for information.</P>


</asp:Content>



