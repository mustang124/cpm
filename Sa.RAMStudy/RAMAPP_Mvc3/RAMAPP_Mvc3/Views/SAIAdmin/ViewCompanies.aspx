﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Validation.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Register Src="~/Views/Shared/SAIAdminMenuUserControl.ascx" TagName="SAIMenu"
    TagPrefix="uc" %>
<%@ Import Namespace=" RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Encryption" %>
<%@ Import Namespace="System.Data.Objects.SqlClient" %>
<%@ Register Src="~/Views/Shared/AdminMenu.ascx" TagPrefix="uc" TagName="AdminMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <p id="head" class="headerIntro">
        Companies in Study
    </p>
    <p>
        &nbsp;</p>
     <% using (Html.BeginForm("ViewCompanies", "SAIAdmin",FormMethod.Get))
         { %>
        <div id="gridHorizontal" align="center">
            <% 
                using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
                {
                    int? curStudyYear  = (Request["StudyYear"] != null ? Convert.ToInt32(Request["StudyYear"]) : db.GetCurrentStudy().Single().Value.Year);
                    var studyYearDropDownList = (from g in db.StudyYear
                                                 select new SelectListItem() { Text = SqlFunctions.StringConvert((double)g.StudyYear1.Year), Value = SqlFunctions.StringConvert((double)g.StudyYear1.Year), Selected = g.StudyYear1.Year == curStudyYear }).OrderByDescending(o => o.Value).ToList();


           %>
            <div class="editor-field">Study Year : 
                <%: Html.DropDownList("StudyYear", studyYearDropDownList, new { onchange = "this.form.submit();" }) %>
                <% } %>
            </div>
        </div>
        <%}%>
        <div id="coLst" class="gridHorizontal">
            <ul class="gridbody">
                <li class=" columndesc gridrowtext">Company </li>
                <li class="gridrow columndesc">Company ID </li>
                <li class="gridrow columndesc">Input Dump </li>
                <li class="gridrow columndesc">Site Form </li>
                <li class="gridrow columndesc">Status</li></ul>
            <ul>
                <%
                    using (var db = new RAMEntities())
                    {
                        int? curStudyYear  = (Request["StudyYear"] != null ? Convert.ToInt32(Request["StudyYear"]) : db.GetCurrentStudy().Single().Value.Year);
                        var cp = Session["Mode"];
                        var comps = (from m in db.Company_LU
                                     //where m.StudyYear == 2011 && !m.CompanySID.ToUpper().Contains("00SAI") && !m.CompanySID.ToUpper().Contains("TEST")
                                     where m.StudyYear == curStudyYear && !m.CompanySID.ToUpper().Contains("00SAI") && !m.CompanySID.ToUpper().Contains("TEST")
                                     select new
                                     {
                                         m.CompanyName,
                                         m.CompanySID,
                                         m.DatasetID,
                                         LockedLevel = db.Reviewable.Where(g => (g.CompanySID == m.CompanySID && g.DataLevel == "Co") ||
                                                                               (g.DataLevel == "Si" && g.CompanySID == m.CompanySID && db.SiteInfo.Any(s => s.CompanySID == g.CompanySID && g.SiteDatsetID == s.DatasetID))).Select(r=>r.DataLevel).FirstOrDefault(),
                                         HasSubmitted = db.Reviewable.Any(g => (g.CompanySID == m.CompanySID && g.DataLevel == "Co") ||
                                                                               (g.DataLevel == "Si" && g.CompanySID == m.CompanySID && db.SiteInfo.Any(s => s.CompanySID == g.CompanySID && g.SiteDatsetID == s.DatasetID))),
                                         m.StudyYear
                                     }).
                                     OrderBy(o => o.CompanyName);

                        foreach (var b in comps)
                        {
                            var compSID = b.CompanySID.Trim();
                %>
                <li class="rightPointerNav" sid="<%: compSID %>">
                    <ul class="gridbody">
                        <li class="gridrowtext">
                            <%: b.CompanyName%>
                        </li>
                        <li class="gridrow">
                            <%:compSID%></li>
                        <li class="gridrow"><a href="<%=Page.ResolveUrl("~/Reports")%>/InputReport.aspx?<%:cp + "=" + compSID%>"
                            target="_blank">View</a></li>
                        <li class="gridrow"><a href="<%=Page.ResolveUrl("~/App")%>?<%:cp + "="+ b.CompanySID%>"
                            target="_blank">View</a></li>
                        <li class="gridrow">
                            <img class="lockbtn" level="Co" sid="<%: compSID %>" eid="<%: b.DatasetID %>" hassubmitted="<%:b.HasSubmitted%>"
                                alt="<%= (b.HasSubmitted ? "Ready" : "Not Ready") %>" src="<%= Page.ResolveUrl("~/images") +(b.HasSubmitted ? (b.LockedLevel=="Si" ? "/disable.gif" : "/company-disabled.png")  : "/enable.gif") %>" /></li>
                    </ul>
                    <div class="unitsContainer">
                        <div style="margin: 0 auto;">
                            Loading Site Overall and Units Data ..</div>
                    </div>
                </li>
                <%}%>
            </ul>
        </div>
        <%} %>

    <script src="<%= Page.ResolveUrl("~/Scripts/colorscale.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({ cache: false });

            var baseUrl = '<%= Page.ResolveUrl("~/SAIAdmin") %>';
            var baseImg = '<%= Page.ResolveUrl("~/images")%>';
            $(this).delegate("div.gridHorizontal li.downPointerNav ul.gridbody", "click", function (event) {
                if ($(event.target).parent().is('li.ViewIcon'))
                    return;

                var $obj = $(this).parent();
                var sid = $obj.attr('sid');
                var $inner = $obj.find('div.unitsContainer');
                if (sid != undefined && sid != null) {

                    //  $inner.load('<%= Page.ResolveUrl("~/Admin/DataValidationSummaryInner") %>/' + dSetId, function () {
                    $inner.load(baseUrl + '/ViewCompaniesInner/' + sid, function (response, status, xhr) {

                        $inner.html(response);
                        $gridbodies = $inner.find("ul.gridbody[scale|pctcm]");
                        for (gb = $gridbodies.length; $gridbody = $($gridbodies[gb]), gb >= 0; gb--) {
                            var scale = $gridbody.attr("scale");
                            var pctcm = $gridbody.attr("pctcm");

                            if (scale != undefined && pctcm != undefined) {
                                pctcm = Globalize.parseFloat(pctcm, selectedCulture);

                                changeColor($gridbody.find('li:nth-child(3)'), scale);
                                changeColor($gridbody.find('li:nth-child(2)>span.bar'), pctcm);
                            }
                        }
                        // $inner.toggle(0);
                    });
                }
                else {
                    // $inner.toggle(0);
                }
            }).delegate("img.lockbtn", "click", function () {
                var $img = $(this);
                var level = $img.attr("level");
                var sid = $img.attr("sid");
                var eid = $img.attr("eid");
                var hasSubmitted = $img.attr("hasSubmitted");

                $.post(baseUrl + '/ToggleSubmit', { "sid": sid, "level": level, "id": eid },
                function (ctx) {

                    if (ctx.ret) {
                        var $totLckCnt = $img.parent().closest("ul.gridbody").siblings().find("img[hasSubmitted='True']").length;
                        var $coImg = $img.parent().closest('div.unitsContainer').parent().find('img[level="Co"]');
                        var coHasSubmitted = $coImg.attr("hasSubmitted");

                        var image = baseImg + (hasSubmitted == 'True' ? '/enable.gif' : (level == 'Si' ? '/disable.gif' : '/company-disabled.png'));
                        $img.attr("src", image);

                        $img.attr("hasSubmitted", hasSubmitted == 'True' ? 'False' : 'True');
                        hasSubmitted = $img.attr("hasSubmitted");

                        $totLckCnt += (hasSubmitted == 'False') ? 0 : 1;
                        //alert(level);
                        var coImage = baseImg + ($totLckCnt ? (level == 'Si' ? '/disable.gif' : '/company-disabled.png') : '/enable.gif');
                        $coImg.attr("src", coImage);

                    }
                });

            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Page.ResolveUrl("~/Content/menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        #coLst, #head
        {
            margin: auto auto;
        }

        .gridHorizontal
        {
            min-width: 500px;
        }

        .gridHorizontal ul.gridbody
        {
            display: table;
            font-size: .90em;
        }

        .gridHorizontal li div.unitsContainer
        {
            display: block;
            background: cornsilk;
            min-width: 100%;
            margin: 0 auto;
            border-right: 1px solid #ddd;
            border-bottom: 1px solid #FF3333;
        }
        .gridHorizontal li div.unitsContainer div.unitsContainer
        {
            background: #EEE8CD;
        }

        .gridHorizontal li div.unitsContainer ul.gridbody
        {
            
            margin: 0;
        }

        .gridHorizontal ul li
        {
            min-height: 28px;
        }

        .gridHorizontal li.gridrow
        {
            display: inline-block;
            float: left;
            border-right: #ddd 1px solid;
            width: 100px;
        }
        .headerIntro
        {
            font-family: 'Arial';
            font-size: 16px;
            color: #991000;
            border-bottom: 1px solid wheat;
            min-width: 200px;
            max-width: 600px;
            padding-left: 0px;
            margin-top: 0px;
            text-align: left;
            height: 20px;
            vertical-align: bottom;
        }
        .gridbody:hover
        {
            background-color: #FFFBCC !important;
            border: thin solid khaki;
        }
        .gridHorizontal li.gridrowtext
        {
            display: inline-block;
            text-align: left;
            width: 225px;
            padding-left: 10px;
            border-right: #ddd 1px solid;
            font-family: "Lucida Sans Unicode";
            font-weight: 500;
        }

      
        .gridHorizontal li.overall
        {
            font-weight: 600;
            color: darkred;
        }
        .nocursor
        {
            cursor: default !important;
        }
        .bar
        {
            float: left;
            display: inline-block;
            height: 12px;
            text-align: center;
            border: thin ridge #777;
            text-shadow: 1px 1px 1px rgba(0,0,0,0.2);
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
            max-width: 95.5%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc:SAIMenu ID="saiMenu" runat="server" />
</asp:Content>