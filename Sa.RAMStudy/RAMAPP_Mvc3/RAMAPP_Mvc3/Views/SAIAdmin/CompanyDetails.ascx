﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<RAMAPP_Mvc3.Models.CompanyModel>" %>

<fieldset>
        <legend>Company Information</legend>
        
        <div class="grid_4">
           Company Name:
        </div>
        <div class="grid_12">
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.Name) %>
            &nbsp;
        </div>
        </div>
        <div class="grid_4">
           Company Identification Number:
        </div>
        <div class="grid_12">
        
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.CompanyID)%>
            &nbsp;
        </div>
        </div>
        
        
         <div class="grid_4">
           Maximum Number of Sites:
        </div>
        <div class="grid_12">
        
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.SiteLimit)%>
            &nbsp;
        </div>
        </div>
       
    </fieldset>

