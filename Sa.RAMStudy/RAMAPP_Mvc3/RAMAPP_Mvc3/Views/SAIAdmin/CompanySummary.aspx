﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Import Namespace="RAMAPP_Mvc3.Models" %>
<%@ Import Namespace="StructureMap.Query" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<% Html.RenderPartial("CompanyDetails", new CompanyModel { CompanyID = (ViewData["DCcompanyid"] == null) ? "" : ViewData["DCcompanyid"].ToString(), Name = (ViewData["DCcompanyname"] == null) ? "" : ViewData["DCcompanyname"].ToString() }); %>
<%--<% Html.Partial("CompanyDetails", new CompanyModel { CompanyID = ViewData["companyid"].ToString() }); %>--%>
<%--<% Html.Partial("CompanyDetails", new { id = ViewData["companyid"] }); %>--%>

<% Html.RenderPartial("DataCoordinatorDetails", new UserDetails { Firstname = (ViewData["DCfirstname"] == null) ? "" : ViewData["DCfirstname"].ToString(), Lastname = (ViewData["DClastname"] == null) ? "" : ViewData["DClastname"].ToString(), JobTile = (ViewData["DCjobtitle"] == null) ? "" : ViewData["DCjobtitle"].ToString(), Telephone = (ViewData["DCphone"] == null) ? "" : ViewData["DCphone"].ToString(), EmailAddress = (ViewData["DCemail"] == null) ? "" : ViewData["DCemail"].ToString() }); %>
<%--<% Html.Partial("DataCoordinatorDetails", new UserDetails { EmailAddress = ViewData["email"].ToString() }); %>--%>
<%--<% Html.Partial("DataCoordinatorDetails", new { email = ViewData["email"] }); %>--%>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
