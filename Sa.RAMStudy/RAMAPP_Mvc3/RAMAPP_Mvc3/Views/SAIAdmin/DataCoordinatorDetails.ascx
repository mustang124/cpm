﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<RAMAPP_Mvc3.Models.UserDetails>" %>
<%@ Import Namespace="System.Globalization" %>

<fieldset>
        <legend>Data coordinator </legend>

        
        <div class="grid_4">
           Firstname:
        </div>
        <div class="grid_12">
       
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.Firstname)%>
            &nbsp;
        </div>
        </div>
        
         <div class="grid_4">
           Lastname:
        </div>
        <div class="grid_12">
       
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.Lastname)%>
            &nbsp;
        </div>
        </div>
       

        <div class="grid_4">
          Job Title:
        </div>
        <div class="grid_12">
        
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.JobTile)%>
            &nbsp;
        </div>
        </div>
        <div class="grid_4">
          Email Address:
        </div>
        <div class="grid_12">
        
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.EmailAddress)%>
            &nbsp;
        </div>
        </div>
        
         <div class="grid_4">
           Telephone:
        </div>
        <div class="grid_12">
       
        <div class="editor-field">
            <%: Html.DisplayFor(model => model.Telephone)%>
            &nbsp;
        </div>
        </div>
     
        <div class="grid_16">
            <h5>
                Culture and Language</h5>
        </div>
         <br /><br/>
        <div class="grid_4">
            Language preference:<br />
            <font color="#990000" size="1px">(Optional) Default is U.S. English</font>
        </div>
        <div class="grid_12">
            <span class="editor-label">&nbsp; </span><span class="editor-field">
                <%: Html.DropDownListFor(model => model.Culture, CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                                                                        .OrderBy(ci=>ci.DisplayName)
                                                                        .Select(ci =>new SelectListItem() {Text=ci.EnglishName,Value=ci.Name }).Distinct()
                                    ,"Select One")%>
            </span>
        </div>
      
    </fieldset>
    
    <script type="text/javascript">
        $('#Culture').val('<%: Model.Culture != null ? Model.Culture.Trim() : String.Empty %>');
    </script>
