﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.CompanyModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create Company</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<%:TempData["CreateMsg"]%>
<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Enter Company Information</legend>
        
        
        <div class="grid_4">
           Company Name:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Name) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Name) %>
            <%: Html.ValidationMessageFor(model => model.Name) %>
        </div>
        </div>
        
        <div class="grid_4">
           Company Identification:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.CompanyID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CompanyID) %>
            <%: Html.ValidationMessageFor(model => model.CompanyID) %>
        </div>
        </div>
        
        
         <div class="grid_4">
           Maximum Number of Sites:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.SiteLimit) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.SiteLimit) %>
            <%: Html.ValidationMessageFor(model => model.SiteLimit) %>
        </div>
        </div>
        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
