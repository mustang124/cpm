﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<h5>Add Company Coordinator</h5>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<%:TempData["UserCreateMsg"]%> 

<% using (Html.BeginForm("CreateCompanyCoordinator","SAIAdmin"))
   { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Create a data coordinator<%-- for <%: TempData["CompanyName"] %>--%> </legend>

        <%--<div class="editor-field">
            <%: Html.HiddenFor(model => model.CompanyID) %>
        </div>--%>
        
       <% using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
           {
               DateTime? curStudyYear = db.GetCurrentStudy().Single();
               var slt = (from g in db.Company_LU
                          //where g.StudyYear == 2011
                          where g.StudyYear == curStudyYear.Value.Year
                          select new SelectListItem() { Text = g.CompanyName.Trim(), Value = g.CompanyID.Trim() }).ToList();
       %>
    
        <div class="grid_4">
           Company:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%--<%: Html.ValidationSummary(true) %>--%>
            <%--<%:Html.LabelFor(model => model.CompanyID) %>--%>
        </div>
        <div class="editor-field">
            <%: Html.DropDownListFor(model => model.CompanyID ,slt,"--Select--") %>
            <%: Html.ValidationMessageFor(model => model.CompanyID) %>
            <% } %>
        </div>
        </div>

        <div class="grid_4">
           Firstname:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Firstname) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Firstname) %>
            <%: Html.ValidationMessageFor(model => model.Firstname) %>
        </div>
        </div>
        
         <div class="grid_4">
           Lastname:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Lastname) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Lastname) %>
            <%: Html.ValidationMessageFor(model => model.Lastname) %>
        </div>
        </div>
        <div class="grid_4">
           Password:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>
        </div>

        <div class="grid_4">
          Job Title:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.JobTile) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.JobTile) %>
            <%: Html.ValidationMessageFor(model => model.JobTile) %>
        </div>
        </div>
        <div class="grid_4">
          Email Address:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.EmailAddress) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.EmailAddress) %>
            <%: Html.ValidationMessageFor(model => model.EmailAddress) %>
        </div>
        </div>
        
         <div class="grid_4">
           Telephone:
        </div>
        <div class="grid_12">
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Telephone) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Telephone) %>
            <%: Html.ValidationMessageFor(model => model.Telephone) %>
        </div>
        </div>
     
        <div class="grid_16">
            <h5>
                Culture and Language</h5>
        </div>
         <br /><br/>
        <div class="grid_4">
            Language preference:<br />
            <font color="#990000" size="1px">(Optional) Default is U.S. English</font>
        </div>
        <div class="grid_12">
            <span class="editor-label">&nbsp; </span><span class="editor-field">
                <%: Html.DropDownListFor(model => model.Culture, CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                                                                        .OrderBy(ci=>ci.DisplayName)
                                                                        .Select(ci =>new SelectListItem() {Text=ci.EnglishName,Value=ci.Name }).Distinct()
                                    ,"Select One")%>
            </span>
        </div>
        <p>
            &nbsp;</p>
        
        <div class="editor-field">
            <%: Html.HiddenFor(model => model.Role) %>
            
        </div>

       

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
