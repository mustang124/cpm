﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<h2>UserInfo</h2>

<fieldset>
    <legend>UserDetails</legend>

    <div class="display-label">CompanyID</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.CompanyID) %>
    </div>

    <div class="display-label">Firstname</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Firstname) %>
    </div>

    <div class="display-label">Lastname</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Lastname) %>
    </div>

    <div class="display-label">Fullname</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Fullname) %>
    </div>

    <div class="display-label">UserID</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.UserID) %>
    </div>

    <div class="display-label">Password</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Password) %>
    </div>

    <div class="display-label">JobTile</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.JobTile) %>
    </div>

    <div class="display-label">EmailAddress</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.EmailAddress) %>
    </div>

    <div class="display-label">Telephone</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Telephone) %>
    </div>

    <div class="display-label">Culture</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Culture) %>
    </div>

    <div class="display-label">Role</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Role) %>
    </div>

    <div class="display-label">ScreenName</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.ScreenName) %>
    </div>
</fieldset>
<p>
    <%: Html.ActionLink("Edit", "Edit", new { /* id=Model.PrimaryKey */ }) %> |
    <%: Html.ActionLink("Back to List", "Index") %>
</p>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
