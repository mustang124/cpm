﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
    Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Src="~/Views/Shared/ApprovalControl.ascx" TagPrefix="uc" TagName="ApprovalControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="margin: 25px; width:750px;" class="grid_16">
        <p>
            Check the boxes by the site(s) that are ready for review&nbsp;and click the <b>APPROVE</b>
        </p>
        <p> 
        <div style="color: #991000;">
        <%= TempData["ReturnMsg"] != null ?  TempData["ReturnMsg"].ToString() :""  %>
       </div>
      
        <% Html.EnableClientValidation();
        using (Html.BeginForm())
           {%>
        <fieldset>
            <legend>Submit for Review</legend>
            <uc:ApprovalControl ID="Approval" runat="server" />
         </fieldset>
        <input type="submit" value="Approve" />
        <%} %>
       
        <br></p>
        <div style="padding:10px;border:1px #333 solid; background-color: #eee;font-size:.95em;">
            After you have submitted your data, the site form will be disabled. A RAM coordinator will review your data. If
            there are any concerns or questions with your data, we will contact you. <br /><br /><i>Thank
            you for participating.</i> </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="<%=Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
    &nbsp;
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
    &nbsp;
</asp:Content>
