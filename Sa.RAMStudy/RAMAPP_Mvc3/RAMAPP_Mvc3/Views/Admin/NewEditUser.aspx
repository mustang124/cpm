﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<h2>EditUser</h2>

<%--<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>--%>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>UserDetails</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CompanyID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CompanyID) %>
            <%: Html.ValidationMessageFor(model => model.CompanyID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Firstname) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Firstname) %>
            <%: Html.ValidationMessageFor(model => model.Firstname) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Lastname) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Lastname) %>
            <%: Html.ValidationMessageFor(model => model.Lastname) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.UserID) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.UserID) %>
            <%: Html.ValidationMessageFor(model => model.UserID) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.JobTile) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.JobTile) %>
            <%: Html.ValidationMessageFor(model => model.JobTile) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.EmailAddress) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.EmailAddress) %>
            <%: Html.ValidationMessageFor(model => model.EmailAddress) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Telephone) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Telephone) %>
            <%: Html.ValidationMessageFor(model => model.Telephone) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Culture) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Culture) %>
            <%: Html.ValidationMessageFor(model => model.Culture) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Role) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Role) %>
            <%: Html.ValidationMessageFor(model => model.Role) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ScreenName) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.ScreenName) %>
            <%: Html.ValidationMessageFor(model => model.ScreenName) %>
        </div>

        <p>
            <input type="submit" value="Save" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="RightContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
