﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared//RAM_Validation.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.DataSectionSummaryModel>" %>

<%@ Register Src="~/Views/Shared/AdminMenu.ascx" TagPrefix="uc" TagName="AdminMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="currentSec" style="font-size: .85em; margin: 5px 0;">
        <font color="#666666">Facility:
            <%=  Session["ValFacility"]%></font>
        <br />
        <div style="height: 1px; min-width: 400px; border-bottom: thin wheat solid;">
            &nbsp;</div>
    </div>
    <div style="display: block; height: 40px; position: absolute; left: 650px; color: #ccc;
        font-size: .85em;">
        <%: Html.ActionLink("Return to Grade Summaries", "DataInputReviewSummary", "Admin")%>
        <br />
    </div>
    <br />
    <br />
    <div class="headerIntro">
        Data Summary</div>
    <p>
        <%=  Model.Overall%></p>
    <%if (!String.IsNullOrEmpty(Model.Missing))
      {%>
    <div class="headerIntro">
        Missing Data</div>
    <p>
        <%=  Model.Missing%></p>
    <% } %>
    <%if (!String.IsNullOrEmpty(Model.Datachecks))
      {%>
    <div class="headerIntro">
        Data Checks</div>
    <p>
        <%=  (Model.Datachecks)%></p>
    <% } %>
    <script src="<%= Page.ResolveUrl("~/Scripts/colorscale.min.js") %>" type="text/javascript"></script>
  
    <script type="text/javascript">
        $(document).ready(function () { $rows = $("div.gridHorizontal ul.gridbody[scale|pctcm]"); for (gb = $rows.length; $gridbody = $($rows[gb]), gb >= 0; gb--) { var a = $gridbody.attr("scale"); var b = $gridbody.attr("pctcm"); if (a != undefined && b != undefined) { b = Globalize.parseFloat(b, selectedCulture); changeColor($gridbody.find("li:nth-child(3)"), a); changeColor($gridbody.find("li:nth-child(2)>span.bar"), b) } } });
        <%-- 
//        $(document).ready(function () {
//            $rows = $("div.gridHorizontal ul.gridbody[scale|pctcm]");
//            for (gb = $rows.length; $gridbody = $($rows[gb]), gb >= 0; gb--) {
//                var scale = $gridbody.attr("scale");
//                var pctcm = $gridbody.attr("pctcm");
//                if (scale != undefined && pctcm != undefined) {
//                    pctcm = Globalize.parseFloat(pctcm, selectedCulture);
//                    changeColor($gridbody.find('li:nth-child(3)'), scale);
//                    changeColor($gridbody.find('li:nth-child(2)>span.bar'), pctcm);
//                }
//            }


//        });

--%>
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        li.OKStatus, li.GreenCheckStatus
        {
            background: url(<%:Page.ResolveUrl("~/images/Green_Checkmark_sm.png") %>) no-repeat 50% 50%;
        }
        li.BlackBarStatus, li.INStatus
        {
            background: url(<%:Page.ResolveUrl("~/images/BlackBar_sm.png") %>) no-repeat 50% 50%;
        }
        li.NAStatus, li.YellowCircleStatus
        {
            background: url(<%:Page.ResolveUrl("~/images/Warning_sm.png") %>) no-repeat 50% 50%;
        }
        li.NCStatus, li.RedXStatus
        {
            background: url(<%:Page.ResolveUrl("~/images/Error_sm.png") %>) no-repeat 50% 50%;
        }
        li.ViewIcon
        {
            background: url(<%:Page.ResolveUrl("~/images/search-sm.png")%>) no-repeat 50% 50%;
        }
        li.ViewIcon a
        {
            margin: 0 auto;
            display: block;
            width: 50px;
            height: 28px;
        }
        .gridHorizontal
        {
            min-width: 500px;
        }
        
        .gridHorizontal ul.gridbody
        {
            display: table;
            font-size: .90em;
        }
        
        .gridHorizontal li div.unitsContainer
        {
            display: block;
            background: cornsilk;
            min-width: 738px;
            margin: 0 auto;
            border-right: 1px solid #ddd;
            border-bottom: 1px solid #FF3333;
            height: auto;
        }
        
        .gridHorizontal li div.unitsContainer ul.gridbody
        {
            background-color: whitesmoke;
            margin: 0;
        }
        
        .gridHorizontal ul li
        {
            font-size: .90em !important;
            min-height: 28px;
        }
        
        .gridHorizontal li.gridrow
        {
            display: inline-block;
            float: left;
            border-right: #ddd 1px solid;
            width: 100px;
        }
        .headerIntro
        {
            font-family: 'Arial';
            font-size: 16px;
            color: #991000;
            text-transform: capitalize;
            border-bottom: 1px solid wheat;
            min-width: 200px;
            max-width: 600px;
            padding-left: 0px;
            margin-top: 0px;
            text-align: left;
            height: 20px;
            vertical-align: bottom;
            text-transform: capitalize;
        }
        .gridbody:hover
        {
            background-color: #FFFBCC !important;
            border: thin solid khaki;
        }
        .gridHorizontal li.gridrowtext
        {
            display: inline-block;
            text-align: left;
            width: 225px;
            padding-left: 10px;
            border-right: #ddd 1px solid;
            font-family: "Lucida Sans Unicode";
            font-weight: 500;
        }
        .gridHorizontal li.gridrowtextlong
        {
            display: inline-block; /*text-align: left;*/
            padding-left: 10px;
            border-right: #ddd 1px solid;
            line-height: 1.2;
            font-family: "Lucida Sans Unicode";
            font-weight: 500;
            float: left;
        }
        .gridHorizontal li.gridrowtextlonger
        {
            display: inline-block; /*text-align: left;*/
            padding-left: 10px;
            border-right: #ddd 1px solid;
            line-height: 1.2;
            font-family: "Lucida Sans Unicode";
            font-weight: 500;
            float: left;
        }
        .gridHorizontal li.overall
        {
            font-weight: 600;
            color: darkred;
        }
        
        .bar
        {
         float:left;
    	display:inline-block;
    	height:12px;
    	text-align:center;
    	border: thin ridge #777;
    	text-shadow:1px 1px 1px rgba(0,0,0,0.2);
        -moz-border-radius:2px;
        -webkit-border-radius:2px;
        border-radius:2px;
       max-width: 97.5%;
        }
    </style>
</asp:Content>
