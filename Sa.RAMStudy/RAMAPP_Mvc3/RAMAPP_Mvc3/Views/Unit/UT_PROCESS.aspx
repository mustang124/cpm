﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UT_PROCESSModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:42:35  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_UT_PROCESS").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("UT_PROCESS", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_UT_PROCESS" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Maintenance Work-Type Category</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">Maintenance Work-Type Category</p>
            <p><i>Report the hours of routine (non-T/A) maintenance work hours spent by each <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> (company and contractor) in the following work-type categories. Include only <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance.  Excludes all maintenance capital work and all maintenance work executed during a plant or unit turnaround.">Routine Maintenance Expense</a> work hours.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_PRC' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UPRC_100.1'><span class='numbering'>1.</span> <a href="#" tooltip="Maintenance work that occurs within 72 hours of the equipment issue and/or disrupts the pre-planned (weekly) schedule.">Emergency Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_UPRC_100.2'><span class='numbering'>2.</span> <a href="#" tooltip="Maintenance performed to restore the function of an asset based on the discovery of an issue (or fault).  The issue may be related to a failure, partial failure, or imminent failure of an asset. Typically, this work would be a onetime repair of an asset that can be planned, scheduled and completed as scheduled.  It can also be performed outside of this work process but does not involve performing the work on an emergency corrective basis.  If the discovery of the fault is through a formal Preventive Maintenance (PM) or Predictive Maintenance (PdM) program defined in the Reliability Strategy, then the repair work should be classified under PM or PdM respectively.">Non-Emergency Corrective Maintenance</a></li>
                        <li class='gridrowtext' qid='QSTID_UPRC_100.3'><span class='numbering'>3.</span> <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies or units of use with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Includes systematic maintenance activities performed at predetermined frequencies with established methods, tools, and equipment.">Preventive Maintenance (PM)</a></li>
                        <li class='gridrowtext' qid='QSTID_UPRC_100.4'><span class='numbering'>4.</span> <a href="#" tooltip="Includes inspection and condition monitoring focused on predicting potential equipment failures through the use of non-destructive testing.  Includes maintenance carried out following a forecast derived from the analysis of the significant parameters of the degradation of the item. The term Condition-Based Maintenance (CBM) and Predictive Maintenance (PdM) can be used interchangeably.  The condition of this asset will be monitored on an ongoing basis and based on the condition of the asset work will be performed.">Predictive Maintenance (PdM)</li>
                        <li class='gridrowtext calctext' qid='QSTID_UPRC_100.5'>Total <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Labor Hours</li>
                        <li class='gridrowtext' qid='QSTID_UPRC_100.6'><span class='numbering'>6.</span> % of Non Emergency Corrective Maintenance Considered Improvement Maintenance (see definition)</li>
<%--                        <li class='gridrowtext' qid='QSTID_UPRC_100.7'><span class='numbering'>7.</span> Rework %</li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_100.1" /><%: Html.EditorFor(model => model.TaskEmergency_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftRE sumUProcTaskEmer sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_100.2" /><%: Html.EditorFor(model => model.TaskRoutCorrective_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftRE sumUProcTaskCorr sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_100.3" /><%: Html.EditorFor(model => model.TaskPreventive_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftRE sumUProcTaskPrev sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_100.4" /><%: Html.EditorFor(model => model.TaskConditionMonitor_RE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftRE sumUProcTaskPred sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_100.5" /><%: Html.EditorFor(model => model.TaskTotal_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_100.6" /><%: Html.EditorFor(model => model.NonEmergencyPCT_RE,"DoubleTmpl",new {size="10"  ,CalcTag="pctNonEmergency" ,NumFormat="Number" ,DecPlaces="2"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_100.7" /><%: Html.EditorFor(model => model.Rework_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_110.1" /><%: Html.EditorFor(model => model.TaskEmergency_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftFP sumUProcTaskEmer sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_110.2" /><%: Html.EditorFor(model => model.TaskRoutCorrective_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftFP sumUProcTaskCorr sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_110.3" /><%: Html.EditorFor(model => model.TaskPreventive_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftFP sumUProcTaskPrev sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_110.4" /><%: Html.EditorFor(model => model.TaskConditionMonitor_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftFP sumUProcTaskPred sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_110.5" /><%: Html.EditorFor(model => model.TaskTotal_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_110.6" /><%: Html.EditorFor(model => model.NonEmergencyPCT_FP,"DoubleTmpl",new {size="10" ,CalcTag="pctNonEmergency" ,NumFormat="Number" ,DecPlaces="2"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_110.7" /><%: Html.EditorFor(model => model.Rework_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_120.1" /><%: Html.EditorFor(model => model.TaskEmergency_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftIE sumUProcTaskEmer sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_120.2" /><%: Html.EditorFor(model => model.TaskRoutCorrective_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftIE sumUProcTaskCorr sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_120.3" /><%: Html.EditorFor(model => model.TaskPreventive_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftIE sumUProcTaskPrev sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_120.4" /><%: Html.EditorFor(model => model.TaskConditionMonitor_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumUProcCraftIE sumUProcTaskPred sumUProcHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_120.5" /><%: Html.EditorFor(model => model.TaskTotal_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_120.6" /><%: Html.EditorFor(model => model.NonEmergencyPCT_IE,"DoubleTmpl",new {size="10" ,CalcTag="pctNonEmergency" ,NumFormat="Number" ,DecPlaces="2"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_120.7" /><%: Html.EditorFor(model => model.Rework_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_130.1" /><%: Html.EditorFor(model => model.TaskEmergency_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_130.2" /><%: Html.EditorFor(model => model.TaskRoutCorrective_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_130.3" /><%: Html.EditorFor(model => model.TaskPreventive_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_130.4" /><%: Html.EditorFor(model => model.TaskConditionMonitor_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_130.5" /><%: Html.EditorFor(model => model.TaskTotal_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_130.6" /><%: Html.EditorFor(model => model.NonEmergencyPCT_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="ttlfld" ,ReadOnly="True"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UPRC_130.7" /><%: Html.EditorFor(model => model.Rework_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2",@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.TaskEmergency_RE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskRoutCorrective_RE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskPreventive_RE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskConditionMonitor_RE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskTotal_RE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskEmergency_FP)%> 
                <%: Html.ValidationMessageFor(model => model.TaskRoutCorrective_FP)%> 
                <%: Html.ValidationMessageFor(model => model.TaskPreventive_FP)%> 
                <%: Html.ValidationMessageFor(model => model.TaskConditionMonitor_FP)%> 
                <%: Html.ValidationMessageFor(model => model.TaskTotal_FP)%> 
                <%: Html.ValidationMessageFor(model => model.TaskEmergency_IE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskRoutCorrective_IE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskPreventive_IE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskConditionMonitor_IE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskTotal_IE)%> 
                <%: Html.ValidationMessageFor(model => model.TaskEmergency_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.TaskRoutCorrective_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.TaskPreventive_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.TaskConditionMonitor_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.TaskTotal_Tot)%>

                <%: Html.ValidationMessageFor(model => model.NonEmergencyPCT_RE)%> 
                <%: Html.ValidationMessageFor(model => model.NonEmergencyPCT_FP)%> 
                <%: Html.ValidationMessageFor(model => model.NonEmergencyPCT_IE)%> 
                <%: Html.ValidationMessageFor(model => model.NonEmergencyPCT_Tot)%>
                
<%--                <%: Html.ValidationMessageFor(model => model.Rework_RE)%> --%>
<%--                <%: Html.ValidationMessageFor(model => model.Rework_FP)%> --%>
<%--                <%: Html.ValidationMessageFor(model => model.Rework_IE)%> --%>
<%--                <%: Html.ValidationMessageFor(model => model.Rework_Tot)%>--%>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



<script type = "text/javascript" >
   $(window).load(function() {
       init();
   });


function loadHelpReferences() {
    var objHelpRefs = Section_HelpLst;
    for (i = objHelpRefs.length - 1; i >= 0; i--) {
        var qidNm = objHelpRefs[i]['HelpID'];
        if ($('li[qid^="' + qidNm + '"]').length) {
            var $elm = $('li[qid^="' + qidNm + '"]');
            $elm.append('<img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
            $elm.find('img.tooltipBtn').css('visibility', 'visible');
        } else if ($('div[qid^="' + qidNm + '"]').length) {
            $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
        }
    }
}

function formatResult(el, value) {
    var format = 'N';
    if (el.attr('NumFormat') == 'Int') format = 'n0';
    if (el.attr('DecPlaces') != null) {
        if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
    }
    if (value != undefined && isNaN(value)) value = 0;
    var tempval = Globalize.parseFloat(value.toString(), 'en');
    if (isFinite(tempval)) {
        el.val(Globalize.format(tempval, format));
        var idnm = '#_' + el.attr('id');
        var $f = $(idnm);
        if ($f.length) {
            $f.val(el.val());
        }
    } else {
        el.val('');
    }
}
$("input[CalcTag*='sumUProcCraftIE']").sum({
    bind: "keyup",
    selector: "#TaskTotal_IE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TaskTotal_IE').attr('readonly', 'readonly');
$('#TaskTotal_IE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUProcCraftFP']").sum({
    bind: "keyup",
    selector: "#TaskTotal_FP",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TaskTotal_FP').attr('readonly', 'readonly');
$('#TaskTotal_FP').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUProcCraftRE']").sum({
    bind: "keyup",
    selector: "#TaskTotal_RE",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TaskTotal_RE').attr('readonly', 'readonly');
$('#TaskTotal_RE').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUProcTaskEmer']").sum({
    bind: "keyup",
    selector: "#TaskEmergency_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TaskEmergency_Tot').attr('readonly', 'readonly');
$('#TaskEmergency_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUProcTaskCorr']").sum({
    bind: "keyup",
    selector: "#TaskRoutCorrective_Tot",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
            var $result = $("#NonEmergencyPCT_Tot");
            $result.calc("(v1*v2+v3*v4+v5*v6)/v7", {
                v1: $("#TaskRoutCorrective_RE"),
                v2: $("#NonEmergencyPCT_RE"),
                v3: $("#TaskRoutCorrective_FP"),
                v4: $("#NonEmergencyPCT_FP"),
                v5: $("#TaskRoutCorrective_IE"),
                v6: $("#NonEmergencyPCT_IE"),
                v7: $("#TaskRoutCorrective_Tot")
            });
            formatResult($result, $result.val());
        }
    }
});
$('#TaskRoutCorrective_Tot').attr('readonly', 'readonly');
$('#TaskRoutCorrective_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUProcTaskPrev']").sum({
    bind: "keyup",
    selector: "#TaskPreventive_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TaskPreventive_Tot').attr('readonly', 'readonly');
$('#TaskPreventive_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUProcTaskPred']").sum({
    bind: "keyup",
    selector: "#TaskConditionMonitor_Tot",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#TaskConditionMonitor_Tot').attr('readonly', 'readonly');
$('#TaskConditionMonitor_Tot').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumUProcHrs']").sum({
    bind: "keyup",
    selector: "#TaskTotal_Tot",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$("input[CalcTag*='pctNonEmergency']").sum({
    bind: "keyup",
    selector: "#NonEmergencyPCT_Tot",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
            var $result = $("#NonEmergencyPCT_Tot");
            $result.calc("(v1*v2+v3*v4+v5*v6)/v7", {
                v1: $("#TaskRoutCorrective_RE"),
                v2: $("#NonEmergencyPCT_RE"),
                v3: $("#TaskRoutCorrective_FP"),
                v4: $("#NonEmergencyPCT_FP"),
                v5: $("#TaskRoutCorrective_IE"),
                v6: $("#NonEmergencyPCT_IE"),
                v7: $("#TaskRoutCorrective_Tot")
            });
            formatResult($result, $result.val());
        }
    }
});
var secComments = Section_Comments;
var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
for (i = secComments.length - 1; i >= 0; i--) {
    var qidNm = secComments[i]['CommentID'];
    if ($('img[qid="' + qidNm + '"]').length) {
        $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
    }
} <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function(index, html) {
    var gridId = $(this).closest('.grid').attr('id');
    if ($.trim(html).indexOf('nbsp', 0) == -1) {
        var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
    }
    $(this).html(newHTML);
});
$('img[suid]').click(function() {
    var suid = $(this).attr('suid');
    var qid = $(this).attr('qid');
    recordComment(suid, qid);
}); 
</script>
</asp:Content>
