﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UT_RELModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/13/2013 09:58:00  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_UT_REL").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("UT_REL", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_UT_REL" class="stContainer" style="width:1300px;">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Reliability Processes</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Root Cause Analysis</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Mean Time Between Events (MTBE) - Rotating Equipment</small> </span></a></li>
            <li><a href='#step-4'>
                <label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br />
                    <small>Mean Time Between Events (MTBE) - Fixed Equipment</small> </span></a></li>
            <li><a href='#step-5'>
                <label class='stepNumber'>5</label><span class='stepDesc'>Section 5<br />
                    <small>Mean Time Between Events (MTBE) - Instrument and Electrical Equipment</small> </span></a></li>
            <li><a href='#step-6'>
                <label class='stepNumber'>6</label><span class='stepDesc'>Section 6<br />
                    <small>Piping systems (Utilities)</small> </span></a></li>
            <li><a href='#step-7'>
                <label class='stepNumber'>7</label><span class='stepDesc'>Section 7<br />
                    <small>Piping systems (Process)</small> </span></a></li>
            <li><a href='#step-8'>
                <label class='stepNumber'>8</label><span class='stepDesc'>Section 8<br />
                    <small>Asset Inspection</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle"><a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> Processes</p>
            <p><i></i></p>



            <div class="questionblock" qid="">


                <div id='UT_REL_PROC' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_150.1'><span class='numbering'>1.</span> Is there a formal documented <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> improvement program?</li>
                        <li class='gridrowtext' qid='QSTID_UREL_150.2'><span class='numbering'>2.</span> What percentage of process equipment is covered by an ongoing <a href="#" tooltip="Time-based maintenance activities performed at predetermined frequencies with established methods, tools, and equipment designed to avoid failure or extend equipment or asset life. Examples include lubrication or periodic adjustments. Tasks such as equipment overhauls scheduled via PM are to be considered Maintenance Events and included in statistics as a failure.">preventive (time-based) maintenance</a> program?</li>
                        <li class='gridrowtext' qid='QSTID_UREL_150.3'><span class='numbering'>3.</span> What percentage of process equipment is covered by an ongoing predictive maintenance (CBM) program?</li>
                        <li class='gridrowtext' qid='QSTID_UREL_150.4'><span class='numbering'>4.</span> What percentage of process equipment is covered by a <a href="#" tooltip="A robust methodology (compliant with SAE JA-1011) designed to ensure that equipment meets its intended function through the analysis of potential failure mechanisms and the implementation of proactive measures designed to avoid failure which provides a structured framework for analyzing the potential failures of a defined group of physical assets in order to develop a functional failure management strategy.  This strategy will include scheduled proactive activities designed to provide an acceptable level of reliability, with an acceptable level of risk, in an efficient and cost-effective manner.  The goal of RCM is to avoid or reduce asset failure consequences, not necessarily to avoid asset failures.">reliability-centered maintenance (RCM)</a> program?</li>
                        <li class='gridrowtext' qid='QSTID_UREL_151.31'><span class='numbering'>5.</span> What percentage of process equipment is covered by sanctioned (e.g., API RP-580, API-691, etc.) <a href="#" tooltip="The application of a systematic risk of failure evaluation procedure for equipment employing inspection techniques to assess the probability and consequence of failure. An example can be found in API RP-580 that describes the application of Risk-Based Inspection for hydrocarbon process facilities. Government regulations that require systematic inspections of pressure vessels and equipment on a risk basis can be considered as equivalent to API RP-580 or other equivalent Risk-Based Inspection methodologies.">risk-based inspection (RBI)</a> methodologies?</li>
                        <li class='gridrowtext' qid='QSTID_UREL_240.1'><span class='numbering'>6.</span> Percent of process equipment which have been assigned a criticality ranking.</li>
                        <li class='gridrowtext' qid='QSTID_UREL_240.4'><span class='numbering'>7.</span> Percent of process equipment included in defined CUI program</li>
                        <li class='gridrowtext' qid='QSTID_UREL_240.7'><span class='numbering'>8.</span> Percent of process equipment included in defined Infrared Thermography monitoring program</li>
                        <li class='gridrowtext' qid='QSTID_UREL_250'><span class='numbering'>9.</span> Percent of process equipment included in defined vibration analysis</li>
                        <li class='gridrowtext' qid='QSTID_UREL_250.3'><span class='numbering'>10.</span> Percent of process equipment included in defined acoustic emission monitoring program</li>
                        <li class='gridrowtext' qid='QSTID_UREL_250.6'><span class='numbering'>11.</span> Percent of process equipment included in defined oil analysis monitoring program</li>
                        <li class='gridrowtext' qid='QSTID_UREL_260'><span class='numbering'>12.</span> Percent of equipment using predictive analytics/machine learning tools</li>
                        <li class='gridrowtext' qid='QSTID_UREL_260.3'><span class='numbering'>13.</span> Are the unit predictive analytics/machine learning tools in place with measured value</li>
                        <li class='gridrowtext' qid='QSTID_UREL_260.6'><span class='numbering'>14.</span> Unit Equipment Reliability Strategy</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.1" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.RelProcessImproveProg_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.RelProcessImproveProg_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.1" /><br />
                            <select id="RelProcessImproveProg_RE" name="RelProcessImproveProg_RE" ><option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.RelProcessImproveProg_RE != null ?(Model.RelProcessImproveProg_RE.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.RelProcessImproveProg_RE != null ?(Model.RelProcessImproveProg_RE.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but...</option>
                                <option title='exists and documented' value='exists and documented' <%= Model.RelProcessImproveProg_RE != null ?(Model.RelProcessImproveProg_RE.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and...</option>
                                <option title='documented and training provided' value='documented and training provided' <%= Model.RelProcessImproveProg_RE != null ?(Model.RelProcessImproveProg_RE.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented...</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.2" /><%: Html.EditorFor(model => model.ProgramTimeBased_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.3" /><%: Html.EditorFor(model => model.ProgramCBM_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.4" /><%: Html.EditorFor(model => model.ProgramRCM_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_151.31" /><%: Html.EditorFor(model => model.RiskMgmtEquipRBI_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.1" /><%: Html.EditorFor(model => model.EquipCriticalityRanking_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.4" /><%: Html.EditorFor(model => model.ProgramCUI_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.7" /><%: Html.EditorFor(model => model.ProgramIRThermography_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250" /><%: Html.EditorFor(model => model.ProgramVibrationAnalysis_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.3" /><%: Html.EditorFor(model => model.ProgramAcousticEmission_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.6" /><%: Html.EditorFor(model => model.ProgramOilAnalysis_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260" /><%: Html.EditorFor(model => model.MachineToolPct_RE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.3" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.MachineToolMVal_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.MachineToolMVal_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.6" /><br />
                            <select id="EquipmentRelStg_RE" name="EquipmentRelStg_RE" ><option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.EquipmentRelStg_RE != null ?(Model.EquipmentRelStg_RE.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.EquipmentRelStg_RE != null ?(Model.EquipmentRelStg_RE.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but...</option>
                                <option title='exists and documented' value='exists and documented' <%= Model.EquipmentRelStg_RE != null ?(Model.EquipmentRelStg_RE.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and...</option>
                                <option title='documented and training provided' value='documented and training provided' <%= Model.EquipmentRelStg_RE != null ?(Model.EquipmentRelStg_RE.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented...</option>
                                <option title='documented, training provided, and internally audited' value='documented, training provided, and internally audited' <%= Model.EquipmentRelStg_RE != null ?(Model.EquipmentRelStg_RE.Equals("documented, training provided, and internally audited")? "SELECTED" : String.Empty):String.Empty %>>documented,...</option>
                            </select>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.5" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.RelProcessImproveProg_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.RelProcessImproveProg_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.5" /><br />
                            <select id="RelProcessImproveProg_FP" name="RelProcessImproveProg_FP" ><option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.RelProcessImproveProg_FP != null ?(Model.RelProcessImproveProg_FP.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.RelProcessImproveProg_FP != null ?(Model.RelProcessImproveProg_FP.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but...</option>
                                <option title='exists and documented' value='exists and documented' <%= Model.RelProcessImproveProg_FP != null ?(Model.RelProcessImproveProg_FP.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and...</option>
                                <option title='documented and training provided' value='documented and training provided' <%= Model.RelProcessImproveProg_FP != null ?(Model.RelProcessImproveProg_FP.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented...</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.6" /><%: Html.EditorFor(model => model.ProgramTimeBased_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.7" /><%: Html.EditorFor(model => model.ProgramCBM_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.8" /><%: Html.EditorFor(model => model.ProgramRCM_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_151.32" /><%: Html.EditorFor(model => model.RiskMgmtEquipRBI_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.2" /><%: Html.EditorFor(model => model.EquipCriticalityRanking_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.5" /><%: Html.EditorFor(model => model.ProgramCUI_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.8" /><%: Html.EditorFor(model => model.ProgramIRThermography_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
<%--                        <li class="gridrow">
                            <img style="visibility: hidden;" src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.1" /><%= Html.HiddenFor(model => model.ProgramVibrationAnalysis_FP, new {id="ProgramVibrationAnalysis_FP"}) %></li>--%>
                        <li class="gridrow"></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.4" /><%: Html.EditorFor(model => model.ProgramAcousticEmission_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow"></li>
<%--                        <li class="gridrow">
                            <img style="visibility: hidden;" src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.7" /><%= Html.HiddenFor(model => model.ProgramOilAnalysis_FP, new {id="ProgramOilAnalysis_FP"}) %></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.1" /><%: Html.EditorFor(model => model.MachineToolPct_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.4" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.MachineToolMVal_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.MachineToolMVal_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.7" /><br />
                            <select id="EquipmentRelStg_FP" name="EquipmentRelStg_FP" ><option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.EquipmentRelStg_FP != null ?(Model.EquipmentRelStg_FP.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.EquipmentRelStg_FP != null ?(Model.EquipmentRelStg_FP.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but...</option>
                                <option title='exists and documented' value='exists and documented' <%= Model.EquipmentRelStg_FP != null ?(Model.EquipmentRelStg_FP.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and...</option>
                                <option title='documented and training provided' value='documented and training provided' <%= Model.EquipmentRelStg_FP != null ?(Model.EquipmentRelStg_FP.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented...</option>
                                <option title='documented, training provided, and internally audited' value='documented, training provided, and internally audited' <%= Model.EquipmentRelStg_FP != null ?(Model.EquipmentRelStg_FP.Equals("documented, training provided, and internally audited")? "SELECTED" : String.Empty):String.Empty %>>documented,...</option>
                            </select>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.9" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.RelProcessImproveProg_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.RelProcessImproveProg_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_150.9" /><br />
                            <select id="RelProcessImproveProg_IE" name="RelProcessImproveProg_IE" ><option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.RelProcessImproveProg_IE != null ?(Model.RelProcessImproveProg_IE.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.RelProcessImproveProg_IE != null ?(Model.RelProcessImproveProg_IE.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but...</option>
                                <option title='exists and documented' value='exists and documented' <%= Model.RelProcessImproveProg_IE != null ?(Model.RelProcessImproveProg_IE.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and...</option>
                                <option title='documented and training provided' value='documented and training provided' <%= Model.RelProcessImproveProg_IE != null ?(Model.RelProcessImproveProg_IE.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented...</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_151" /><%: Html.EditorFor(model => model.ProgramTimeBased_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_151.1" /><%: Html.EditorFor(model => model.ProgramCBM_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_151.2" /><%: Html.EditorFor(model => model.ProgramRCM_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_151.33" /><%: Html.EditorFor(model => model.RiskMgmtEquipRBI_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.3" /><%: Html.EditorFor(model => model.EquipCriticalityRanking_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.6" /><%: Html.EditorFor(model => model.ProgramCUI_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow"></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_240.9" /><%: Html.EditorFor(model => model.ProgramIRThermography_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
<%--                        <li class="gridrow">
                            <img style="visibility: hidden;" src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.2" /><%= Html.HiddenFor(model => model.ProgramVibrationAnalysis_IE, new {id="ProgramVibrationAnalysis_IE"}) %></li>--%>
                        <li class="gridrow"></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.5" /><%: Html.EditorFor(model => model.ProgramAcousticEmission_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_250.8" /><%: Html.EditorFor(model => model.ProgramOilAnalysis_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <%--<li class="gridrow"></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.2" /><%: Html.EditorFor(model => model.MachineToolPct_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.5" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.MachineToolMVal_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.MachineToolMVal_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_260.8" /><br />
                            <select id="EquipmentRelStg_IE" name="EquipmentRelStg_IE" ><option value='-1'>SELECT ONE</option>
                                <option value='does not exist' <%= Model.EquipmentRelStg_IE != null ?(Model.EquipmentRelStg_IE.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                <option title="exists but not documented" value='exists but not documented' <%= Model.EquipmentRelStg_IE != null ?(Model.EquipmentRelStg_IE.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but...</option>
                                <option title='exists and documented' value='exists and documented' <%= Model.EquipmentRelStg_IE != null ?(Model.EquipmentRelStg_IE.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and...</option>
                                <option title='documented and training provided' value='documented and training provided' <%= Model.EquipmentRelStg_IE != null ?(Model.EquipmentRelStg_IE.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented...</option>
                                <option title='documented, training provided, and internally audited' value='documented, training provided, and internally audited' <%= Model.EquipmentRelStg_IE != null ?(Model.EquipmentRelStg_IE.Equals("documented, training provided, and internally audited")? "SELECTED" : String.Empty):String.Empty %>>documented,...</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.ProgramOilAnalysis_IE)%>
                <%: Html.ValidationMessageFor(model => model.MachineToolPct_RE)%>
                <%: Html.ValidationMessageFor(model => model.MachineToolPct_FP)%>
                <%: Html.ValidationMessageFor(model => model.MachineToolPct_IE)%>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2" >
            <p class="StepTitle">Root Cause Analysis</p>
            <p><i></i></p>
            <div class="questionblock" qid="QSTID_UREL_270">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_270' /></span> <span class='numbering'>1.</span> Do you perform RCA's on Asset Related Issues?
					 					 <span>
                                              <img id="Img28" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.RootCauseAls_ARI, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.RootCauseAls_ARI, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.RootCauseAls_ARI)%>
                </div>
            </div>
            <div class="questionblock" qid="QSTID_UREL_270.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_270.1' /></span> <span class='numbering'>2.</span> What % of the action items from RCA's were closed in the study year?
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="RootCauseAls_Pct" name="RootCauseAls_Pct">
                        <option value='-1'>SELECT ONE</option>
                        <option value='0%' <%= Model.RootCauseAls_Pct != null ?(Model.RootCauseAls_Pct.Equals("0%")? "SELECTED" : String.Empty):String.Empty %>>0%</option>
                        <option value='>0% to 25%' <%= Model.RootCauseAls_Pct != null ?(Model.RootCauseAls_Pct.Equals(">0% to 25%")? "SELECTED" : String.Empty):String.Empty %>>>0% to 25%</option>
                        <option value='>25% to 50%' <%= Model.RootCauseAls_Pct != null ?(Model.RootCauseAls_Pct.Equals(">25% to 50%")? "SELECTED" : String.Empty):String.Empty %>>>25% to 50%</option>
                        <option value='>50%' <%= Model.RootCauseAls_Pct != null ?(Model.RootCauseAls_Pct.Equals(">50%")? "SELECTED" : String.Empty):String.Empty %>>>50%</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.RootCauseAls_Pct)%>
                </div>
            </div>
            <div class="questionblock" qid="QSTID_UREL_270.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_270.2' /></span> <span class='numbering'>3.</span> # of Asset Related Near Miss Incidents with RCA's Conducted
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="RootCauseAls_No" name="RootCauseAls_No">
                        <option value='-1'>SELECT ONE</option>
                        <option value='Not Done' <%= Model.RootCauseAls_No != null ?(Model.RootCauseAls_No.Equals("Not Done")? "SELECTED" : String.Empty):String.Empty %>>Not Done</option>
                        <option value='0-9' <%= Model.RootCauseAls_No != null ?(Model.RootCauseAls_No.Equals("0-9")? "SELECTED" : String.Empty):String.Empty %>>0-9</option>
                        <option value='10-19' <%= Model.RootCauseAls_No != null ?(Model.RootCauseAls_No.Equals("10-19")? "SELECTED" : String.Empty):String.Empty %>>10-19</option>
                        <option value='20 or more' <%= Model.RootCauseAls_No != null ?(Model.RootCauseAls_No.Equals("20 or more")? "SELECTED" : String.Empty):String.Empty %>>20 or more</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.RootCauseAls_No)%>
                </div>
            </div>
            <p><i>Indicate How RCA's are Facilitated:</i></p>
            <div class="questionblock" qid="QSTID_UREL_270.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_270.3' /></span> <span class='numbering'>4.</span> Asset failures that do not lead to unit outage or reduced rate operation
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="RootCauseAls_AFNO" name="RootCauseAls_AFNO">
                        <option value='-1'>SELECT ONE</option>
                        <option value='Not Done' <%= Model.RootCauseAls_AFNO != null ?(Model.RootCauseAls_AFNO.Equals("Not Done")? "SELECTED" : String.Empty):String.Empty %>>Not Done</option>
                        <option value='Facilitated by Unit Team with No Site Support' <%= Model.RootCauseAls_AFNO != null ?(Model.RootCauseAls_AFNO.Equals("Facilitated by Unit Team with No Site Support")? "SELECTED" : String.Empty):String.Empty %>>Facilitated by Unit Team with No Site Support</option>
                        <option value='Facilitated by Unit team with Site support as needed' <%= Model.RootCauseAls_AFNO != null ?(Model.RootCauseAls_AFNO.Equals("Facilitated by Unit team with Site support as needed")? "SELECTED" : String.Empty):String.Empty %>>Facilitated by Unit team with Site support as needed</option>
                        <option value='Facilitated by Site' <%= Model.RootCauseAls_AFNO != null ?(Model.RootCauseAls_AFNO.Equals("Facilitated by Site")? "SELECTED" : String.Empty):String.Empty %>>Facilitated by Site</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.RootCauseAls_AFNO)%>
                </div>

            </div>
            <div class="questionblock" qid="QSTID_UREL_270.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_270.4' /></span> <span class='numbering'>5.</span> Asset failures that do lead to unit outage or reduced rate operation
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="RootCauseAls_AFO" name="RootCauseAls_AFO">
                        <option value='-1'>SELECT ONE</option>
                        <option value='Not Done' <%= Model.RootCauseAls_AFO != null ?(Model.RootCauseAls_AFO.Equals("Not Done")? "SELECTED" : String.Empty):String.Empty %>>Not Done</option>
                        <option value='Facilitated by Unit Team with No Site Support' <%= Model.RootCauseAls_AFO != null ?(Model.RootCauseAls_AFO.Equals("Facilitated by Unit Team with No Site Support")? "SELECTED" : String.Empty):String.Empty %>>Facilitated by Unit Team with No Site Support</option>
                        <option value='Facilitated by Unit team with Site support as needed' <%= Model.RootCauseAls_AFO != null ?(Model.RootCauseAls_AFO.Equals("Facilitated by Unit team with Site support as needed")? "SELECTED" : String.Empty):String.Empty %>>Facilitated by Unit team with Site support as needed</option>
                        <option value='Facilitated by Site' <%= Model.RootCauseAls_AFO != null ?(Model.RootCauseAls_AFO.Equals("Facilitated by Site")? "SELECTED" : String.Empty):String.Empty %>>Facilitated by Site</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.RootCauseAls_AFO)%>
                </div>

            </div>
            <div class="questionblock" qid="QSTID_UREL_270.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_270.5' /></span> <span class='numbering'>6.</span> Indicate the qualification of the facilitator
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="RootCauseAls_QOF" name="RootCauseAls_QOF">
                        <option value='-1'>SELECT ONE</option>
                        <option value='No Training Required' <%= Model.RootCauseAls_QOF != null ?(Model.RootCauseAls_QOF.Equals("No Training Required")? "SELECTED" : String.Empty):String.Empty %>>No Training Required</option>
                        <option value='Appointed Based on Experience' <%= Model.RootCauseAls_QOF != null ?(Model.RootCauseAls_QOF.Equals("Appointed Based on Experience")? "SELECTED" : String.Empty):String.Empty %>>Appointed Based on Experience</option>
                        <option value='Appointed After Training' <%= Model.RootCauseAls_QOF != null ?(Model.RootCauseAls_QOF.Equals("Appointed After Training")? "SELECTED" : String.Empty):String.Empty %>>Appointed After Training</option>
                        <%--<option value='Facilitated by Site' <%= Model.RootCauseAls_QOF != null ?(Model.RootCauseAls_QOF.Equals("Facilitated by Site")? "SELECTED" : String.Empty):String.Empty %>>Facilitated by Site</option>--%>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.RootCauseAls_QOF)%>
                </div>

            </div>
            <div class="questionblock" qid="QSTID_UREL_270.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_UREL_270.6' /></span> <span class='numbering'>7.</span> Bad Actor List - What percentage of the Top Ten issues on the list are resolved every year
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">
                    <select id="RootCauseAls_BAL" name="RootCauseAls_BAL">
                        <option value='-1'>SELECT ONE</option>
                        <option value='0%' <%= Model.RootCauseAls_BAL != null ?(Model.RootCauseAls_BAL.Equals("0%")? "SELECTED" : String.Empty):String.Empty %>>0%</option>
                        <option value='>0% to 50%' <%= Model.RootCauseAls_BAL != null ?(Model.RootCauseAls_BAL.Equals(">0% to 50%")? "SELECTED" : String.Empty):String.Empty %>>>0% to 50%</option>
                        <option value='> 50%' <%= Model.RootCauseAls_BAL != null ?(Model.RootCauseAls_BAL.Equals("> 50%")? "SELECTED" : String.Empty):String.Empty %>>> 50%</option>
                    </select>
                    <%: Html.ValidationMessageFor(model => model.RootCauseAls_BAL)%>
                </div>
            </div>
        </div>
        <div class="SubSectionTab" id="step-3" style="display:inline; width:1050px;">

            <p class="StepTitle">Mean Time Between <a href="#" tooltip="&lt;b&gt;Event&lt;br&gt;An event is one of the following: 1. Failure of the asset, 2. The discovery of a fault, 3. The identification of a defect leading to the need for repair. An inspection of an asset with no discovery is not an event.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Proactive&lt;br&gt;This is an event that is discovered through inspection, condition monitoring or other means of discovery in which action is taken to plan, schedule and complete as scheduled the addressing of the discovered issue to bring the condition of the asset back to normal. Items 2 and 3 in Event definition.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Reactive&lt;br&gt;This is an event that occurs because of the failure of the asset or the stop of production. Item 1 in Event definition.">Events</a> (MTBE) - <a href="#" tooltip="Equipment that turns on an axis and is designed to impart kinetic energy on a process material. Common examples include centrifugal pumps and compressors, electric motors, steam turbines, etc. Rotating equipment craft include machinery mechanics such as millwrights and rotating equipment condition monitoring technicians. Include process equipment and exclude non-process equipment.">Rotating Equipment</a></p>
            <p><i>Provide the number of <a href="#" tooltip="&lt;b&gt;Event&lt;br&gt;An event is one of the following: 1. Failure of the asset, 2. The discovery of a fault, 3. The identification of a defect leading to the need for repair. An inspection of an asset with no discovery is not an event.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Proactive&lt;br&gt;This is an event that is discovered through inspection, condition monitoring or other means of discovery in which action is taken to plan, schedule and complete as scheduled the addressing of the discovered issue to bring the condition of the asset back to normal. Items 2 and 3 in Event definition.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Reactive&lt;br&gt;This is an event that occurs because of the failure of the asset or the stop of production. Item 1 in Event definition.">maintenance events</a> per year that cause equipment to be taken out of service including <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnarounds</a>.  Please provide an average number per year over the past 2-5 years.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_REL_MTBF_RE' class='grid editor-field'>
                    <ul class="rowdesc" style="width:180px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.1'><span class='numbering'>1.</span> <a href="#" tooltip="A rotary engine that extracts energy from a fluid flow and converts it into useful work. Most common are steam and gas turbines installed to drive process and ancillary equipment.">Turbines</a> (Main and <a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)</li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.2'><span class='numbering'>2.</span> <a href="#" tooltip="Reciprocating compressors generally include a piston that moves forward and backward alternately within a cylinder to compress a fluid and increase pressure. Reciprocating compressors integrated into other equipment are excluded.">Compressors – Reciprocating</a> (Main and <a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)</li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.3'><span class='numbering'>3.</span> <a href="#" tooltip="Rotating compressors (sometimes referred to as centrifugal, axial, or radial compressors) turn on an axis to impart kinetic energy and/or velocity to a continuous flowing fluid through a rotor or impeller. Rotating compressors integrated into other equipment are excluded.">Compressors – Rotating</a> (Main and <a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)</li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.4'><span class='numbering'>4.</span> <a href="#" tooltip="A centrifugal pump uses centrifugal force to move a fluid through the use of an impeller that imparts kinetic energy into the fluid. Most common in the process industry is the horizontal centrifugal pump. Low-horsepower pumps are used primarily for injection, lubrication, analyzers, and similar services that are excluded from the equipment count.">Process Pumps – Centrifugal</a> (Main and <a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)</li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.5'><span class='numbering'>5.</span> <a href="#" tooltip="A positive displacement pump causes a fluid to move by trapping a fixed amount of it then forcing (displacing) that trapped volume into the discharge pipe. Common examples include rotary, diaphragm, gear and reciprocating pumps. Smaller pumps that are part of auxiliary systems such as process analyzers, lube oil circuits on compressors, or additive pumps should not be included in the count. See Motors.">Process Pumps – Positive Displacement</a> (Main and <a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance<br /> Events per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.1" /><%: Html.EditorFor(model => model.EventsTurbines,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.2" /><%: Html.EditorFor(model => model.EventsCompressorsRecip,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.3" /><%: Html.EditorFor(model => model.EventsCompressorsRotating,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.4" /><%: Html.EditorFor(model => model.EventsPumpsCentrifugal,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.5" /><%: Html.EditorFor(model => model.EventsPumpsPosDisp,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Equipment Count</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.1" /><%: Html.EditorFor(model => model.EquipCntTurbinesTOT,"DoubleTmpl",new {size="10" ,id="EquipCntTurbinesTOT" ,NumFormat="Number" ,DecPlaces="2",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.2" /><%: Html.EditorFor(model => model.EquipCntCompressorsRecipTOT,"DoubleTmpl",new {size="10" ,id="EquipCntCompressorsRecipTOT" ,NumFormat="Number" ,DecPlaces="2",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.3" /><%: Html.EditorFor(model => model.EquipCntCompressorsRotatingTOT,"DoubleTmpl",new {size="10" ,id="EquipCntCompressorsRotatingTOT" ,NumFormat="Number" ,DecPlaces="2",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.4" /><%: Html.EditorFor(model => model.EquipCntPumpsCentrifugalTOT,"DoubleTmpl",new {size="10" ,id="EquipCntPumpsCentrifugalTOT" ,NumFormat="Number" ,DecPlaces="2",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.5" /><%: Html.EditorFor(model => model._EquipCntPumpsPosDisp,"DoubleTmpl",new {size="10" ,id="_EquipCntPumpsPosDisp" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">MTBE (Months)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.1" /><%: Html.EditorFor(model => model.MTBFTurbines,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.2" /><%: Html.EditorFor(model => model.MTBFCompressorsRecip,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.3" /><%: Html.EditorFor(model => model.MTBFCompressorsRotating,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.4" /><%: Html.EditorFor(model => model.MTBFPumpsCentrifugal,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.5" /><%: Html.EditorFor(model => model.MTBFPumpsPosDisp,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Are you<br />currently capturing<br />Reactive/Proactive<br /> data</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.1" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataTurbines, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataTurbines, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.2" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataCompressorsRecip, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataCompressorsRecip, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_166.3" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataCompressorsRotating, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataCompressorsRotating, "N") %></td>
                                </tr>
                            </table>

                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.4" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataPumpsCentrifugal, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataPumpsCentrifugal, "N") %></td>
                                </tr>
                            </table>
                        </li>
                       <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.5" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataPumpsPosDisp, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataPumpsPosDisp, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Proactive Events<br /> per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.6" /><%: Html.EditorFor(model => model.PEventsTurbines,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.7" /><%: Html.EditorFor(model => model.PEventsCompressorsRecip,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.8" /><%: Html.EditorFor(model => model.PEventsCompressorsRotating,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166.9" /><%: Html.EditorFor(model => model.PEventsPumpsCentrifugal,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_166" /><%: Html.EditorFor(model => model.PEventsPumpsPosDisp,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Reactive Events<br /> per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.1" /><%: Html.EditorFor(model => model.REventsTurbines,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.2" /><%: Html.EditorFor(model => model.REventsCompressorsRecip,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.3" /><%: Html.EditorFor(model => model.REventsCompressorsRotating,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.4" /><%: Html.EditorFor(model => model.REventsPumpsCentrifugal,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.5" /><%: Html.EditorFor(model => model.REventsPumpsPosDisp,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc"> % Proactive Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.6" /><%: Html.EditorFor(model => model.PEventsTurbinesMTBE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.7" /><%: Html.EditorFor(model => model.PEventsCompressorsRecipMTBE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.8" /><%: Html.EditorFor(model => model.PEventsCompressorsRotatingMTBE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167.9" /><%: Html.EditorFor(model => model.PEventsPumpsCentrifugalMTBE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_167" /><%: Html.EditorFor(model => model.PEventsPumpsPosDispMTBE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% Reactive Events<br /> </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_168.1" /><%: Html.EditorFor(model => model.PEventsTurbinesMTBE_Rct,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_168.2" /><%: Html.EditorFor(model => model.PEventsCompressorsRecipMTBE_Rct,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_168.3" /><%: Html.EditorFor(model => model.PEventsCompressorsRotatingMTBE_Rct,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_168.4" /><%: Html.EditorFor(model => model.PEventsPumpsCentrifugalMTBE_Rct,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_168.5" /><%: Html.EditorFor(model => model.PEventsPumpsPosDispMTBE_Rct,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.EventsTurbines)%> 
                <%: Html.ValidationMessageFor(model => model.EventsCompressorsRecip)%> 
                <%: Html.ValidationMessageFor(model => model.EventsCompressorsRotating)%> 
                <%: Html.ValidationMessageFor(model => model.EventsPumpsCentrifugal)%> 
                <%: Html.ValidationMessageFor(model => model.EventsPumpsPosDisp)%> 
                <%: Html.ValidationMessageFor(model => model.EquipCntTurbinesTOT)%> 
                <%: Html.ValidationMessageFor(model => model.EquipCntCompressorsRecipTOT)%> 
                <%: Html.ValidationMessageFor(model => model.EquipCntCompressorsRotatingTOT)%> 
                <%: Html.ValidationMessageFor(model => model.EquipCntPumpsCentrifugalTOT)%> 
                <%: Html.ValidationMessageFor(model => model._EquipCntPumpsPosDisp)%>
                <%: Html.ValidationMessageFor(model => model.MTBFTurbines)%> 
                <%: Html.ValidationMessageFor(model => model.MTBFCompressorsRecip)%>
                <%: Html.ValidationMessageFor(model => model.MTBFCompressorsRotating)%> 
                <%: Html.ValidationMessageFor(model => model.MTBFPumpsCentrifugal)%> 
                <%: Html.ValidationMessageFor(model => model.MTBFPumpsPosDisp)%>

                <%: Html.ValidationMessageFor(model => model.PEventsTurbines)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsCompressorsRecip)%>
                <%: Html.ValidationMessageFor(model => model.PEventsCompressorsRotating)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsPumpsCentrifugal)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsPumpsPosDisp)%>

                <%: Html.ValidationMessageFor(model => model.REventsTurbines)%> 
                <%: Html.ValidationMessageFor(model => model.REventsCompressorsRecip)%>
                <%: Html.ValidationMessageFor(model => model.REventsCompressorsRotating)%> 
                <%: Html.ValidationMessageFor(model => model.REventsPumpsCentrifugal)%> 
                <%: Html.ValidationMessageFor(model => model.REventsPumpsPosDisp)%>

                <%: Html.ValidationMessageFor(model => model.PEventsTurbinesMTBE)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsCompressorsRecipMTBE)%>
                <%: Html.ValidationMessageFor(model => model.PEventsCompressorsRotatingMTBE)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsPumpsCentrifugalMTBE)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsPumpsPosDispMTBE)%>

                <%: Html.ValidationMessageFor(model => model.PEventsTurbinesMTBE_Rct)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsCompressorsRecipMTBE_Rct)%>
                <%: Html.ValidationMessageFor(model => model.PEventsCompressorsRotatingMTBE_Rct)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsPumpsCentrifugalMTBE_Rct)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsPumpsPosDispMTBE_Rct)%>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

            <p><i>Primary Causes of Rotating Equipment Events (%)</i></p>
            <div class="questionblock" qid="">
                <div id='UT_REL_MTBF_RE_Pct' class='grid editor-field'>
                    <ul class="rowdesc" style="width: 200px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.1'><span class='numbering'>6.</span> Design Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.2'><span class='numbering'>7.</span> Construction/Installation Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.3'><span class='numbering'>8.</span> Maintenance Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>9.</span> Operational Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>10.</span> Normal End of Life</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>11.</span> Other</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>12.</span> <a href="#" tooltip="total Percentage should be 100%">Total Percentage</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% of Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_169.1" /><%: Html.EditorFor(model => model.PctEventsDsgIsu,"DoubleTmpl",new {size="10" ,CalcTag="sumPctRE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_169.2" /><%: Html.EditorFor(model => model.PctEventsConsIsu,"DoubleTmpl",new {size="10" ,CalcTag="sumPctRE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_169.3" /><%: Html.EditorFor(model => model.PctEventsMaintIsu,"DoubleTmpl",new {size="10" ,CalcTag="sumPctRE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_169.4" /><%: Html.EditorFor(model => model.PctEventsOprtIsu,"DoubleTmpl",new {size="10" ,CalcTag="sumPctRE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_169.5" /><%: Html.EditorFor(model => model.PctEventsNmlEndLife,"DoubleTmpl",new {size="10" ,CalcTag="sumPctRE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_169.6" /><%: Html.EditorFor(model => model.PctEventsOther,"DoubleTmpl",new {size="10" ,CalcTag="sumPctRE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_169.7" /><%: Html.EditorFor(model => model.PctEventsTotalRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.PctEventsDsgIsu)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsConsIsu)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsMaintIsu)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOprtIsu)%>            
                <%: Html.ValidationMessageFor(model => model.PctEventsNmlEndLife)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsTotalRE)%>
                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
        </div>
        <div class="SubSectionTab" id="step-4" style="display:inline; width:1050px;">

            <p class="StepTitle">Mean Time Between <a href="#" tooltip="&lt;b&gt;Event&lt;br&gt;An event is one of the following: 1. Failure of the asset, 2. The discovery of a fault, 3. The identification of a defect leading to the need for repair. An inspection of an asset with no discovery is not an event.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Proactive&lt;br&gt;This is an event that is discovered through inspection, condition monitoring or other means of discovery in which action is taken to plan, schedule and complete as scheduled the addressing of the discovered issue to bring the condition of the asset back to normal. Items 2 and 3 in Event definition.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Reactive&lt;br&gt;This is an event that occurs because of the failure of the asset or the stop of production. Item 1 in Event definition.">Events</a> (MTBE) - <a href="#" tooltip="Process equipment that is fixed versus rotating equipment, reciprocating equipment, or other moving equipment. Examples of fixed equipment include pipe, heat exchangers, process vessels, distillation towers, tanks, etc. Fixed equipment craft workers include pipefitters, welders, boilermakers, carpenters, painters, equipment operators, insulators, riggers, and other civil crafts. Include process equipment and exclude non-process equipment.">Fixed Equipment</a></p>
            <p><i>Provide the number of <a href="#" tooltip="&lt;b&gt;Event&lt;br&gt;An event is one of the following: 1. Failure of the asset, 2. The discovery of a fault, 3. The identification of a defect leading to the need for repair. An inspection of an asset with no discovery is not an event.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Proactive&lt;br&gt;This is an event that is discovered through inspection, condition monitoring or other means of discovery in which action is taken to plan, schedule and complete as scheduled the addressing of the discovered issue to bring the condition of the asset back to normal. Items 2 and 3 in Event definition.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Reactive&lt;br&gt;This is an event that occurs because of the failure of the asset or the stop of production. Item 1 in Event definition.">maintenance events</a> per year that cause equipment to be taken out of service including <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnarounds</a>.  Please provide an average number per year over the past 2-5 years.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_REL_MTBF_FP' class='grid editor-field'>
                    <ul class="rowdesc" style="width:160px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.6'><span class='numbering'>1.</span> <a href="#" tooltip="A fired heat induction device used to transfer heat into water or a process fluid. This typically involves a fired chamber (i.e., boiler) with tubes containing the fluid to be heated.">Fired Furnaces/Boilers</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.7'><span class='numbering'>2.</span> <a href="#" tooltip="Equipment used to complete a process such as a chemical reaction in a reactor.">Process Vessels</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.8'><span class='numbering'>3.</span> <a href="#" tooltip="Columnar equipment used to separate liquid mixtures based on the differences of boiling point. Includes both vacuum and atmospheric units that typically have internal trays or related separation devices.">Distillation Towers</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_160.9'><span class='numbering'>4.</span> Heat Exchangers (Shell and Tube, both Main and <a href="#" tooltip="Spare equipment that is installed in the process unit to ensure the continuity of the production process if the primary equipment fails. Installed spares require maintenance to ensure that they are operational if/when needed. Highly reliable operations generally do not require installed spares, whereas unreliable operations generally do require installed spares to minimize downtime.">Installed Spares</a>)</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Events per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.6" /><%: Html.EditorFor(model => model.EventsFurnaceBoilers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.7" /><%: Html.EditorFor(model => model.EventsVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.8" /><%: Html.EditorFor(model => model.EventsDistTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_160.9" /><%: Html.EditorFor(model => model.EventsHeatExch,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Equipment Count</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.6    " /><%: Html.EditorFor(model => model._EquipCntFurnaceBoilers,"DoubleTmpl",new {size="10" ,id="_EquipCntFurnaceBoilers" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.7    " /><%: Html.EditorFor(model => model._EquipCntVessels,"DoubleTmpl",new {size="10" ,id="_EquipCntVessels" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.8    " /><%: Html.EditorFor(model => model._EquipCntDistTowers,"DoubleTmpl",new {size="10" ,id="_EquipCntDistTowers" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_162.9    " /><%: Html.EditorFor(model => model.EquipCntHeatExchTOT,"DoubleTmpl",new {size="10" ,id="EquipCntHeatExchTOT" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True" ,@class="formfld"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">MTBE (Months)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.6    " /><%: Html.EditorFor(model => model.MTBFFurnaceBoilers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.7    " /><%: Html.EditorFor(model => model.MTBFVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.8    " /><%: Html.EditorFor(model => model.MTBFDistTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_164.9    " /><%: Html.EditorFor(model => model.MTBFHeatExch,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Are you<br />currently capturing<br />Reactive/Proactive<br /> data</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_171.1" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsFurnaceBoilers, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsFurnaceBoilers, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_171.2" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsVessels, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsVessels, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_171.3" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsDistTowers, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsDistTowers, "N") %></td>
                                </tr>
                            </table>

                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_171.4" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsHeatExch, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsHeatExch, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                   <ul class="gridbody">
                        <li class="columndesc">Proactive Events<br />per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_171.5" /><%: Html.EditorFor(model => model.PEventsFurnaceBoilers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_171.7" /><%: Html.EditorFor(model => model.PEventsVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_171.8" /><%: Html.EditorFor(model => model.PEventsDistTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_171.9" /><%: Html.EditorFor(model => model.PEventsHeatExch,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Reactive Events<br />per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.1" /><%: Html.EditorFor(model => model.REventsFurnaceBoilers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.2" /><%: Html.EditorFor(model => model.REventsVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.3" /><%: Html.EditorFor(model => model.REventsDistTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.4" /><%: Html.EditorFor(model => model.REventsHeatExch,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% Proactive Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.5" /><%: Html.EditorFor(model => model.PEventsMTBEFurnaceBoilers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.6" /><%: Html.EditorFor(model => model.PEventsMTBEVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.7" /><%: Html.EditorFor(model => model.PEventsMTBEDistTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_172.8" /><%: Html.EditorFor(model => model.PEventsMTBEHeatExch,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% Reactive Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.1" /><%: Html.EditorFor(model => model.REventsMTBEFurnaceBoilers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.2" /><%: Html.EditorFor(model => model.REventsMTBEVessels,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.3" /><%: Html.EditorFor(model => model.REventsMTBEDistTowers,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.4" /><%: Html.EditorFor(model => model.REventsMTBEHeatExch,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.EventsFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model.EventsVessels)%> 
                <%: Html.ValidationMessageFor(model => model.EventsDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.EventsHeatExch)%> 
                <%: Html.ValidationMessageFor(model => model._EquipCntFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model._EquipCntVessels)%> 
                <%: Html.ValidationMessageFor(model => model._EquipCntDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.EquipCntHeatExchTOT)%> 
                <%: Html.ValidationMessageFor(model => model.MTBFFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model.MTBFVessels)%> 
                <%: Html.ValidationMessageFor(model => model.MTBFDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.MTBFHeatExch)%>

                <%: Html.ValidationMessageFor(model => model.PDataEventsFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model.PDataEventsVessels)%> 
                <%: Html.ValidationMessageFor(model => model.PDataEventsDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.PDataEventsHeatExch)%>

                <%: Html.ValidationMessageFor(model => model.PEventsFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsVessels)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsHeatExch)%>

                <%: Html.ValidationMessageFor(model => model.REventsFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model.REventsVessels)%> 
                <%: Html.ValidationMessageFor(model => model.REventsDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.REventsHeatExch)%>

                <%: Html.ValidationMessageFor(model => model.PEventsMTBEFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsMTBEVessels)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsMTBEDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.PEventsMTBEHeatExch)%>

                <%: Html.ValidationMessageFor(model => model.REventsMTBEFurnaceBoilers)%> 
                <%: Html.ValidationMessageFor(model => model.REventsMTBEVessels)%> 
                <%: Html.ValidationMessageFor(model => model.REventsMTBEDistTowers)%> 
                <%: Html.ValidationMessageFor(model => model.REventsMTBEHeatExch)%>
            <p><i>Primary Causes of Fixed Equipment Events (%)</i></p>
            <div class="questionblock" qid="">
                <div id='UT_REL_MTBF_FP_Pct' class='grid editor-field'>
                    <ul class="rowdesc" style="width: 200px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.1'><span class='numbering'>5.</span> Design Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.2'><span class='numbering'>6.</span> Construction/Installation Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.3'><span class='numbering'>7.</span> Maintenance Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>8.</span> Operational Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>9.</span> Normal End of Life</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>10.</span> Other</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>11.</span> <a href="#" tooltip="total Percentage should be 100%">Total Percentage</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% of Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.5" /><%: Html.EditorFor(model => model.PctEventsDsgIsu_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctFP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.6" /><%: Html.EditorFor(model => model.PctEventsConsIsu_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctFP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.7" /><%: Html.EditorFor(model => model.PctEventsMaintIsu_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctFP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.8" /><%: Html.EditorFor(model => model.PctEventsOprtIsu_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctFP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173.9" /><%: Html.EditorFor(model => model.PctEventsNmlEndLife_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctFP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_173" /><%: Html.EditorFor(model => model.PctEventsOther_FP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctFP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_174.1" /><%: Html.EditorFor(model => model.PctEventsTotalRE_FP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.PctEventsDsgIsu_FP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsConsIsu_FP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsMaintIsu_FP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOprtIsu_FP)%>            
                <%: Html.ValidationMessageFor(model => model.PctEventsNmlEndLife_FP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOther_FP)%>
                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
    </div>
        <div class="SubSectionTab" id="step-5" style="display:inline; width:1050px;">

            <p class="StepTitle">Mean Time Between <a href="#" tooltip="&lt;b&gt;Event&lt;br&gt;An event is one of the following: 1. Failure of the asset, 2. The discovery of a fault, 3. The identification of a defect leading to the need for repair. An inspection of an asset with no discovery is not an event.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Proactive&lt;br&gt;This is an event that is discovered through inspection, condition monitoring or other means of discovery in which action is taken to plan, schedule and complete as scheduled the addressing of the discovered issue to bring the condition of the asset back to normal. Items 2 and 3 in Event definition.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Reactive&lt;br&gt;This is an event that occurs because of the failure of the asset or the stop of production. Item 1 in Event definition.">Events</a> (MTBE) - Instrument and Electrical Equipment</p>
            <p><i>Provide the number of <a href="#" tooltip="&lt;b&gt;Event&lt;br&gt;An event is one of the following: 1. Failure of the asset, 2. The discovery of a fault, 3. The identification of a defect leading to the need for repair. An inspection of an asset with no discovery is not an event.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Proactive&lt;br&gt;This is an event that is discovered through inspection, condition monitoring or other means of discovery in which action is taken to plan, schedule and complete as scheduled the addressing of the discovered issue to bring the condition of the asset back to normal. Items 2 and 3 in Event definition.
&lt;br&gt;&lt;br&gt;&lt;b&gt;Event – Reactive&lt;br&gt;This is an event that occurs because of the failure of the asset or the stop of production. Item 1 in Event definition.">maintenance events</a> per year that cause equipment to be taken out of service including <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">turnarounds</a>.  Please provide an average number per year over the past 2-5 years.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_REL_MTBF_IE' class='grid editor-field'>
                    <ul class="rowdesc" style="width:160px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161'><span class='numbering'>1.</span> <a href="#" tooltip="Electrical motors that drive process equipment such as pumps and compressors. Small motors that are part of auxiliary systems such as process analyzers, lube oil circuits, or additive pumps are not included. See Pumps.">Motors</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.1'><span class='numbering'>2.</span> <a href="#" tooltip="An electric motor with the capability of varying the speed by controlling the frequency of the electrical power supply.">Variable Speed Drives</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.2'><span class='numbering'>3.</span> <a href="#" tooltip="A valve integrated into a process control loop used to control flows within the process unit. Include automated block valves with a positioner. Do not include control valves that are integrated into other equipment (e.g., boilers). Include automated block valves with a positioner and automatic shut-off valves.">Control Valves</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.3'><span class='numbering'>4.</span> <a href="#" tooltip="Analyzers used to automatically adjust the process either as an element of a closed loop control scheme or by reporting analysis data to an advanced control algorithm.">On-Stream Analyzers – Process Control</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.4'><span class='numbering'>5.</span> <a href="#" tooltip="Analyzers used to measure blending operations and used to adjust the process.">On-Stream Analyzers – Blending</a></li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.5'><span class='numbering'>6.</span> <a href="#" tooltip="Analyzers used in a Continuous Emissions Monitoring System (CEMS) to meet environmental regulations for byproducts such as SO2 and NOX.">On-Stream Analyzers – Continuous Emissions Monitoring</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Events per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_161" /><%: Html.EditorFor(model => model.EventsMotorsMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_161.1" /><%: Html.EditorFor(model => model.EventsVarSpeedDrives,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_161.2" /><%: Html.EditorFor(model => model.EventsControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_161.3" /><%: Html.EditorFor(model => model.EventsAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_161.4" /><%: Html.EditorFor(model => model.EventsAnalyzerBlending,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_161.5" /><%: Html.EditorFor(model => model.EventsAnalyzerEmissions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Equipment Count</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_163      " /><%: Html.EditorFor(model => model._EquipCntMotorsMain,"DoubleTmpl",new {size="10" ,id="_EquipCntMotorsMain" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_163.1    " /><%: Html.EditorFor(model => model._EquipCntVarSpeedDrives,"DoubleTmpl",new {size="10" ,id="_EquipCntVarSpeedDrives" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_163.2    " /><%: Html.EditorFor(model => model._EquipCntControlValves,"DoubleTmpl",new {size="10" ,id="_EquipCntControlValves" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_163.3    " /><%: Html.EditorFor(model => model._EquipCntAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,id="_EquipCntAnalyzerProcCtrl" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_163.4    " /><%: Html.EditorFor(model => model._EquipCntAnalyzerBlending,"DoubleTmpl",new {size="10" ,id="_EquipCntAnalyzerBlending" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_163.5    " /><%: Html.EditorFor(model => model._EquipCntAnylyzerEnv,"DoubleTmpl",new {size="10" ,id="_EquipCntAnylyzerEnv" ,NumFormat="Number" ,DecPlaces="2" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">MTBE (Months)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_165      " /><%: Html.EditorFor(model => model.MTBFMotorsMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_165.1    " /><%: Html.EditorFor(model => model.MTBFVarSpeedDrives,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_165.2    " /><%: Html.EditorFor(model => model.MTBFControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_165.3    " /><%: Html.EditorFor(model => model.MTBFAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_165.4    " /><%: Html.EditorFor(model => model.MTBFAnalyzerBlending,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_165.5    " /><%: Html.EditorFor(model => model.MTBFAnalyzerEmissions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Are you<br />currently capturing<br />Reactive/Proactive<br /> data</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_174.2" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsMotorsMain, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsMotorsMain, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_174.3" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsVarSpeedDrives, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsVarSpeedDrives, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_174.4" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsControlValves, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsControlValves, "N") %></td>
                                </tr>
                            </table>

                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_174.5" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsAnalyzerProcCtrl, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsAnalyzerProcCtrl, "N") %></td>
                                </tr>
                            </table>
                        </li>
                            <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_174.6" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsAnalyzerBlending, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsAnalyzerBlending, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_174.7" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsAnalyzerEmissions, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PDataEventsAnalyzerEmissions, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Proactive Events<br />per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_174.8" /><%: Html.EditorFor(model => model.PEventsMotorsMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_174.9" /><%: Html.EditorFor(model => model.PEventsVarSpeedDrives,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_1749" /><%: Html.EditorFor(model => model.PEventsControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.1" /><%: Html.EditorFor(model => model.PEventsAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.2" /><%: Html.EditorFor(model => model.PEventsAnalyzerBlending,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.3" /><%: Html.EditorFor(model => model.PEventsAnalyzerEmissions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Reactive Events<br />per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.4" /><%: Html.EditorFor(model => model.REventsMotorsMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.5" /><%: Html.EditorFor(model => model.REventsVarSpeedDrives,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.6" /><%: Html.EditorFor(model => model.REventsControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.7" /><%: Html.EditorFor(model => model.REventsAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.8" /><%: Html.EditorFor(model => model.REventsAnalyzerBlending,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_175.9" /><%: Html.EditorFor(model => model.REventsAnalyzerEmissions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% Proactive Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176" /><%: Html.EditorFor(model => model.PMTBEMotorsMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.1" /><%: Html.EditorFor(model => model.PMTBEVarSpeedDrives,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.2" /><%: Html.EditorFor(model => model.PMTBEControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.3" /><%: Html.EditorFor(model => model.PMTBEAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.4" /><%: Html.EditorFor(model => model.PMTBEAnalyzerBlending,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.5" /><%: Html.EditorFor(model => model.PMTBEAnalyzerEmissions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% Reactive Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.7" /><%: Html.EditorFor(model => model.RMTBEMotorsMain,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.8" /><%: Html.EditorFor(model => model.RMTBEVarSpeedDrives,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_176.9" /><%: Html.EditorFor(model => model.RMTBEControlValves,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177" /><%: Html.EditorFor(model => model.RMTBEAnalyzerProcCtrl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.1" /><%: Html.EditorFor(model => model.RMTBEAnalyzerBlending,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.2" /><%: Html.EditorFor(model => model.RMTBEAnalyzerEmissions,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>

                </div>
                            <p><i>Primary Causes of Instrument and Electrical Events (%)</i></p>
            <div class="questionblock" qid="">
                <div id='UT_REL_MTBF_IE_Pct' class='grid editor-field'>
                    <ul class="rowdesc" style="width: 200px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.1'><span class='numbering'>7.</span> Design Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.2'><span class='numbering'>8.</span> Construction/Installation Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.3'><span class='numbering'>9.</span> Maintenance Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>10.</span> Operational Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>11.</span> Normal End of Life</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>12.</span> Other</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>13.</span> <a href="#" tooltip="total Percentage should be 100%">Total Percentage</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% of Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.3" /><%: Html.EditorFor(model => model.PctEventsDsgIsu_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumPctIE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.4" /><%: Html.EditorFor(model => model.PctEventsConsIsu_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumPctIE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.5" /><%: Html.EditorFor(model => model.PctEventsMaintIsu_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumPctIE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.6" /><%: Html.EditorFor(model => model.PctEventsOprtIsu_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumPctIE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.7" /><%: Html.EditorFor(model => model.PctEventsNmlEndLife_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumPctIE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.8" /><%: Html.EditorFor(model => model.PctEventsOther_IE,"DoubleTmpl",new {size="10" ,CalcTag="sumPctIE",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_177.9" /><%: Html.EditorFor(model => model.PctEventsTotal_IE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.PctEventsDsgIsu_IE)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsConsIsu_IE)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsMaintIsu_IE)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOprtIsu_IE)%>            
                <%: Html.ValidationMessageFor(model => model.PctEventsNmlEndLife_IE)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOther_IE)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsTotal_IE)%>
                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>



                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <div class="SubSectionTab" id="step-6" style="display:inline; width:1050px;">

            <p class="StepTitle">Piping Systems (Utilities)</p>
            <p><i</i></p>
            <div class="questionblock" qid="">
                <div id='UT_REL_MTBF_PU' class='grid editor-field'>
                    <ul class="rowdesc" style="width:300px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161'><span class='numbering'>1.</span> Number of <a href="#" tooltip="The unplanned release of materials arising from failure of the primary containment.  This includes all process and utility fluids, gases, and solids.  Primary containment can include pipes, vessels, encasements, silos, basins, valves, tubing, etc.">Loss of Primary Containment Events</a> that caused assets to be shut down</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.1'><span class='numbering'>2.</span> Number of <a href="#" tooltip="The unplanned release of materials arising from failure of the primary containment.  This includes all process and utility fluids, gases, and solids.  Primary containment can include pipes, vessels, encasements, silos, basins, valves, tubing, etc.">Loss of Primary Containment Events</a> that resulted  in a Government Reportable Spill</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.2'><span class='numbering'>3.</span> Number of <a href="#" tooltip="The unplanned release of materials arising from failure of the primary containment.  This includes all process and utility fluids, gases, and solids.  Primary containment can include pipes, vessels, encasements, silos, basins, valves, tubing, etc.">Loss of Primary Containment Events</a> not included in the above 2 questions</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Events per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178" /><%: Html.EditorFor(model => model.NumCauseShutDownPU_EY,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.1" /><%: Html.EditorFor(model => model.NumRsltGovSpillPU_EY,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.2" /><%: Html.EditorFor(model => model.NumExPipeAstLocPU_EY,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Currently Captured</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.4" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumCauseShutDownPU_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumCauseShutDownPU_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.5" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumRsltGovSpillPU_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumRsltGovSpillPU_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_178.6" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumExPipeAstLocPU_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumExPipeAstLocPU_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
               
                <div class="questionblock" qid="">
                <div id='UT_REL_MTBF_PU1' class='grid editor-field'>
                   <ul class="rowdesc" style="width:300px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161'><span class='numbering'>4.</span> What percentage of these are at pipe connections</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Percent</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.3" /><%: Html.EditorFor(model => model.PipeConnectionPU_Pct,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Currently Captured</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.7" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PipeConnectionPU_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PipeConnectionPU_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
                </div>
            <p><i>Primary Causes of Piping Systems (Utilities) Events (%)</i></p>
            <div class="questionblock" qid="">
                <div id='Div3' class='grid editor-field'>
                    <ul class="rowdesc" style="width: 300px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.1'><span class='numbering'>5.</span> Design Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.2'><span class='numbering'>6.</span> Construction/Installation Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.3'><span class='numbering'>7.</span> Maintenance Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>8.</span> Operational Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>9.</span> Normal End of Life</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>10.</span> Other</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>11.</span> <a href="#" tooltip="total Percentage should be 100%">Total Percentage</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% of Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.8" /><%: Html.EditorFor(model => model.PctEventsDsgIsu_PU,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPU",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_178.9" /><%: Html.EditorFor(model => model.PctEventsConsIsu_PU,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPU",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179" /><%: Html.EditorFor(model => model.PctEventsMaintIsu_PU,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPU",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.1" /><%: Html.EditorFor(model => model.PctEventsOprtIsu_PU,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPU",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.2" /><%: Html.EditorFor(model => model.PctEventsNmlEndLife_PU,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPU",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.3" /><%: Html.EditorFor(model => model.PctEventsOther_PU,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPU",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.4" /><%: Html.EditorFor(model => model.PctEventsTotal_PU,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.PctEventsDsgIsu_PU)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsConsIsu_PU)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsMaintIsu_PU)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOprtIsu_PU)%>            
                <%: Html.ValidationMessageFor(model => model.PctEventsNmlEndLife_PU)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOther_PU)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsTotal_PU)%>
                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>



                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <div class="SubSectionTab" id="step-7" style="display:inline; width:1050px;">

            <p class="StepTitle">Piping Systems (Process)</p>
            <p><i</i></p>
            <div class="questionblock" qid="">
                <div id='UT_REL_MTBF_PP' class='grid editor-field'>
                    <ul class="rowdesc" style="width:300px;">
                        <li>&nbsp;</li> 
                        <li class='gridrowtext' qid='QSTID_UREL_161'><span class='numbering'>1.</span> Number of <a href="#" tooltip="The unplanned release of materials arising from failure of the primary containment.  This includes all process and utility fluids, gases, and solids.  Primary containment can include pipes, vessels, encasements, silos, basins, valves, tubing, etc.">Loss of Primary Containment Events</a> that caused assets to be shut down</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.1'><span class='numbering'>2.</span> Number of <a href="#" tooltip="The unplanned release of materials arising from failure of the primary containment.  This includes all process and utility fluids, gases, and solids.  Primary containment can include pipes, vessels, encasements, silos, basins, valves, tubing, etc.">Loss of Primary Containment Events</a> that resulted  in a Government Reportable Spill</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.2'><span class='numbering'>3.</span> Number of <a href="#" tooltip="The unplanned release of materials arising from failure of the primary containment.  This includes all process and utility fluids, gases, and solids.  Primary containment can include pipes, vessels, encasements, silos, basins, valves, tubing, etc.">Loss of Primary Containment Events</a> not included in the above 2 questions</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Events per Year</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.5" /><%: Html.EditorFor(model => model.NumCauseShutDownPP_EY,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.6" /><%: Html.EditorFor(model => model.NumRsltGovSpillPP_EY,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.7" /><%: Html.EditorFor(model => model.NumExPipeAstLocPP_EY,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Currently Captured</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.9" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumCauseShutDownPP_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumCauseShutDownPP_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumRsltGovSpillPP_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumRsltGovSpillPP_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.1" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumExPipeAstLocPP_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.NumExPipeAstLocPP_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
               
                <div class="questionblock" qid="">
                <div id='Div4' class='grid editor-field'>
                   <ul class="rowdesc" style="width:300px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161'><span class='numbering'>4.</span> What percentage of these are at pipe connections</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Percent</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_179.8" /><%: Html.EditorFor(model => model.PipeConnectionPP_Pct,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Currently Captured</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.2" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.PipeConnectionPP_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.PipeConnectionPP_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
                </div>
            <p><i>Primary Causes of Piping Systems (Process) Events (%)</i></p>
            <div class="questionblock" qid="">
                <div id='Div5' class='grid editor-field'>
                    <ul class="rowdesc" style="width: 300px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.1'><span class='numbering'>5.</span> Design Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.2'><span class='numbering'>6.</span> Construction/Installation Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.3'><span class='numbering'>7.</span> Maintenance Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>8.</span> Operational Issues</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>9.</span> Normal End of Life</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.4'><span class='numbering'>10.</span> Other</li>
                        <li class='gridrowtext' qid='QSTID_UREL_169.5'><span class='numbering'>11.</span> <a href="#" tooltip="total Percentage should be 100%">Total Percentage</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">% of Events</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.3" /><%: Html.EditorFor(model => model.PctEventsDsgIsu_PP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.4" /><%: Html.EditorFor(model => model.PctEventsConsIsu_PP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.5" /><%: Html.EditorFor(model => model.PctEventsMaintIsu_PP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.6" /><%: Html.EditorFor(model => model.PctEventsOprtIsu_PP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.7" /><%: Html.EditorFor(model => model.PctEventsNmlEndLife_PP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.8" /><%: Html.EditorFor(model => model.PctEventsOther_PP,"DoubleTmpl",new {size="10" ,CalcTag="sumPctPP",NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_181.9" /><%: Html.EditorFor(model => model.PctEventsTotal_PP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1",@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.PctEventsDsgIsu_PP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsConsIsu_PP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsMaintIsu_PP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOprtIsu_PP)%>            
                <%: Html.ValidationMessageFor(model => model.PctEventsNmlEndLife_PP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsOther_PP)%>
                <%: Html.ValidationMessageFor(model => model.PctEventsTotal_PP)%>
                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>



                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
     <div class="SubSectionTab" id="step-8" style="display:inline; width:1050px;">

            <p class="StepTitle">Asset Inspection</p>
            <p><i>Enter percentage of equipment at inspection interval (years)<br />
Input Clarification:  In this section we are looking at the frequency of inspection for various types of equipment.  Indicate how often inspections take place for each type of Equipment.  All equipment in the category may not be inspected and therefore the total percent does not need to equal 100%.<br />
Note:  A machine is equipment with moving parts contained in a housing. (e.g. a compressor, pump, centrifuge, 
etc.).  A structure is a support mechanism for equipment. (e.g. process structure, pipe racks, etc.) 
</i></p>


            <div class="questionblock" qid="">


                <div id='UT_REL_MTBF_AI' class='grid editor-field'>
                    <ul class="rowdesc" style="width:180px;">
                        <li class='gridrowtext'><b>&nbsp;&nbsp;&nbsp;&nbsp;Equipment</b></li>
                        <li class='gridrowtext' qid='QSTID_UREL_161'><span class='numbering'>1.</span> Pressure vessels</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.1'><span class='numbering'>2.</span> Distillation columns</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.2'><span class='numbering'>3.</span> Heat exchangers</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.3'><span class='numbering'>4.</span> Process piping systems</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.4'><span class='numbering'>5.</span> Utilities piping systems</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.5'><span class='numbering'>6.</span> Relief valves</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.3'><span class='numbering'>7.</span> Other protective devices</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.4'><span class='numbering'>8.</span> Machines</li>
                        <li class='gridrowtext' qid='QSTID_UREL_161.5'><span class='numbering'>9.</span> Structures</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">less than 2 years<br />(percentage)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.1" /><%: Html.EditorFor(model => model.AssetInspPresVes_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.2" /><%: Html.EditorFor(model => model.AssetInspDistCol_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.3" /><%: Html.EditorFor(model => model.AssetInspHeatExg_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.4" /><%: Html.EditorFor(model => model.AssetInspProcPipe_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.5" /><%: Html.EditorFor(model => model.AssetInspUtilPipe_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.6" /><%: Html.EditorFor(model => model.AssetInspReliefVal_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.7" /><%: Html.EditorFor(model => model.AssetInspOthProDev_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.8" /><%: Html.EditorFor(model => model.AssetInspMachines_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_182.9" /><%: Html.EditorFor(model => model.AssetInspStructures_2Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">2-5 years<br />(percentage)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.1" /><%: Html.EditorFor(model => model.AssetInspPresVes_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.2" /><%: Html.EditorFor(model => model.AssetInspDistCol_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.3" /><%: Html.EditorFor(model => model.AssetInspHeatExg_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.4" /><%: Html.EditorFor(model => model.AssetInspProcPipe_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.5" /><%: Html.EditorFor(model => model.AssetInspUtilPipe_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.6" /><%: Html.EditorFor(model => model.AssetInspReliefVal_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.7" /><%: Html.EditorFor(model => model.AssetInspOthProDev_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.8" /><%: Html.EditorFor(model => model.AssetInspMachines_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_183.9" /><%: Html.EditorFor(model => model.AssetInspStructures_5Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">6-9 years<br />(percentage)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.1" /><%: Html.EditorFor(model => model.AssetInspPresVes_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.2" /><%: Html.EditorFor(model => model.AssetInspDistCol_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.3" /><%: Html.EditorFor(model => model.AssetInspHeatExg_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.4" /><%: Html.EditorFor(model => model.AssetInspProcPipe_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.5" /><%: Html.EditorFor(model => model.AssetInspUtilPipe_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.6" /><%: Html.EditorFor(model => model.AssetInspReliefVal_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.7" /><%: Html.EditorFor(model => model.AssetInspOthProDev_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.8" /><%: Html.EditorFor(model => model.AssetInspMachines_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_184.9" /><%: Html.EditorFor(model => model.AssetInspStructures_9Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">10+ years<br />(percentage)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.1" /><%: Html.EditorFor(model => model.AssetInspPresVes_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.2" /><%: Html.EditorFor(model => model.AssetInspDistCol_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.3" /><%: Html.EditorFor(model => model.AssetInspHeatExg_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.4" /><%: Html.EditorFor(model => model.AssetInspProcPipe_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.5" /><%: Html.EditorFor(model => model.AssetInspUtilPipe_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.6" /><%: Html.EditorFor(model => model.AssetInspReliefVal_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.7" /><%: Html.EditorFor(model => model.AssetInspOthProDev_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.8" /><%: Html.EditorFor(model => model.AssetInspMachines_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_185.9" /><%: Html.EditorFor(model => model.AssetInspStructures_10Y,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                    </ul>
                      <ul class="gridbody">
                        <li class="columndesc">currently captured</li>
                        <li class="gridrow">
                             <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_186.1" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspPresVes_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspPresVes_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_186.2" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspDistCol_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspDistCol_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_186.3" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspHeatExg_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspHeatExg_CC, "N") %></td>
                                </tr>
                            </table>

                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_186.4" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspProcPipe_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspProcPipe_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                            <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_186.5" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspUtilPipe_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspUtilPipe_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_186.6" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspReliefVal_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspReliefVal_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                                                  <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_186.7" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspOthProDev_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspOthProDev_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                            <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid=QSTID_UREL_186.8" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspMachines_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspMachines_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UREL_186.9" /></td>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspStructures_CC, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.AssetInspStructures_CC, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
                            



                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
        <!-- for wizard container -->
    <%
       } //for form using clause 
    %>
        </div>


<script type = "text/javascript" > function loadHelpReferences() {
     var objHelpRefs = Section_HelpLst;
     for (i = objHelpRefs.length - 1; i >= 0; i--) {
         var qidNm = objHelpRefs[i]['HelpID'];
         if ($('li[qid^="' + qidNm + '"]').length) {
             var $elm = $('li[qid^="' + qidNm + '"]');
             $elm.append('<img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
             $elm.find('img.tooltipBtn').css('visibility', 'visible');
         } else if ($('div[qid^="' + qidNm + '"]').length) {
             $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
         }
     }
 }

    function formatResult(el, value) {
        var format = 'N';
        if (el.attr('NumFormat') == 'Int') format = 'n0';
        if (el.attr('DecPlaces') != null) {
            if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
        }
        if (value != undefined && isNaN(value)) value = 0;
        var tempval = Globalize.parseFloat(value.toString(), 'en');
        if (isFinite(tempval)) {
            el.val(Globalize.format(tempval, format));
            var idnm = '#_' + el.attr('id');
            var $f = $(idnm);
            if ($f.length) {
                $f.val(el.val());
            }
        } else {
            el.val('');
        }
    }
    if ($("#EquipCntMotorsMain").val() == null || $("#EquipCntMotorsMain").val().trim() == "") {
        $("#_EquipCntMotorsMain").val("");
    } else {
        var $result = $("#_EquipCntMotorsMain");
        $result.calc("v1", {
            v1: $("#EquipCntMotorsMain")
        });
        formatResult($result, $result.val());
    }
    if ($("#EquipCntVarSpeedDrives").val() == null || $("#EquipCntVarSpeedDrives").val().trim() == "") {
        $("#_EquipCntVarSpeedDrives").val("");
    } else {
        var $result = $("#_EquipCntVarSpeedDrives");
        $result.calc("v1", {
            v1: $("#EquipCntVarSpeedDrives")
        });
        formatResult($result, $result.val());
    }
    if ($("#EquipCntControlValves").val() == null || $("#EquipCntControlValves").val().trim() == "") {
        $("#_EquipCntControlValves").val("");
    } else {
        var $result = $("#_EquipCntControlValves");
        $result.calc("v1", {
            v1: $("#EquipCntControlValves")
        });
        formatResult($result, $result.val());
    }
    if ($("#EquipCntAnalyzerProcCtrl").val() == null || $("#EquipCntAnalyzerProcCtrl").val().trim() == "") {
        $("#_EquipCntAnalyzerProcCtrl").val("");
    } else {
        var $result = $("#_EquipCntAnalyzerProcCtrl");
        $result.calc("v1", {
            v1: $("#EquipCntAnalyzerProcCtrl")
        });
        formatResult($result, $result.val());
    }
    if ($("#EquipCntAnalyzerBlending").val() == null || $("#EquipCntAnalyzerBlending").val().trim() == "") {
        $("#_EquipCntAnalyzerBlending").val("");
    } else {
        var $result = $("#_EquipCntAnalyzerBlending");
        $result.calc("v1", {
            v1: $("#EquipCntAnalyzerBlending")
        });
        formatResult($result, $result.val());
    }
    if ($("#EquipCntAnylyzerEnv").val() == null || $("#EquipCntAnylyzerEnv").val().trim() == "") {
        $("#_EquipCntAnylyzerEnv").val("");
    } else {
        var $result = $("#_EquipCntAnylyzerEnv");
        $result.calc("v1", {
            v1: $("#EquipCntAnylyzerEnv")
        });
        formatResult($result, $result.val());
    }
    $("#EquipCntAnalyzerBlending,#EventsAnalyzerBlending").bind("keyup", function () {
        var $result = $("#MTBFAnalyzerBlending");
        $result.calc("v1/v2*12", {
            v1: $("#EquipCntAnalyzerBlending"),
            v2: $("#EventsAnalyzerBlending")
        });
        formatResult($result, $result.val()); /*$("#MTBFAnalyzerBlending").trigger('blur');*/
    });
    if ($("#EquipCntAnalyzerBlending").val() != null && !isNaN($("#EquipCntAnalyzerBlending").val()))
        if ($("#EventsAnalyzerBlending").val() != null && !isNaN($("#EventsAnalyzerBlending").val())) {
            $("#EquipCntAnalyzerBlending").trigger('keyup');
        } $("#EquipCntAnylyzerEnv,#EventsAnalyzerEmissions ").bind("keyup", function () {
            var $result = $("#MTBFAnalyzerEmissions");
            $result.calc("v1/v2*12", {
                v1: $("#EquipCntAnylyzerEnv"),
                v2: $("#EventsAnalyzerEmissions")
            });
            formatResult($result, $result.val()); /*$("#MTBFAnalyzerEmissions").trigger('blur');*/
        });
        if ($("#EquipCntAnylyzerEnv").val() != null && !isNaN($("#EquipCntAnylyzerEnv").val()))
        if ($("#EventsAnalyzerEmissions").val() != null && !isNaN($("#EventsAnalyzerEmissions").val())) {
            $("#EquipCntAnylyzerEnv").trigger('keyup');
        } $("#EquipCntAnalyzerProcCtrl,#EventsAnalyzerProcCtrl ").bind("keyup", function () {
            var $result = $("#MTBFAnalyzerProcCtrl");
            $result.calc("v1/v2*12", {
                v1: $("#EquipCntAnalyzerProcCtrl"),
                v2: $("#EventsAnalyzerProcCtrl")
            });
            formatResult($result, $result.val()); /*$("#MTBFAnalyzerProcCtrl").trigger('blur');*/
        });
    if ($("#EquipCntAnalyzerProcCtrl").val() != null && !isNaN($("#EquipCntAnalyzerProcCtrl").val()))
        if ($("#EventsAnalyzerProcCtrl").val() != null && !isNaN($("#EventsAnalyzerProcCtrl").val())) {
            $("#EquipCntAnalyzerProcCtrl").trigger('keyup');
        } $("#EquipCntCompressorsRecip,#EventsCompressorsRecip,#EquipCntCompressorsRecipSpares ").bind("keyup", function () {
            var $result = $("#MTBFCompressorsRecip");
            $result.calc("(v1+v3)/v2*12", {
                v1: $("#EquipCntCompressorsRecip"),
                v2: $("#EventsCompressorsRecip"),
                v3: $("#EquipCntCompressorsRecipSpares")
            });
            formatResult($result, $result.val()); /*$("#MTBFCompressorsRecip").trigger('blur');*/
        });
    if ($("#EquipCntCompressorsRecip").val() != null && !isNaN($("#EquipCntCompressorsRecip").val()))
        if ($("#EventsCompressorsRecip").val() != null && !isNaN($("#EventsCompressorsRecip").val()))
            if ($("#EquipCntCompressorsRecipSpares").val() != null && !isNaN($("#EquipCntCompressorsRecipSpares").val())) {
                $("#EquipCntCompressorsRecip").trigger('keyup');
            } $("#EquipCntCompressorsRotating,#EventsCompressorsRotating,#EquipCntCompressorsRotatingSpares ").bind("keyup", function () {
                var $result = $("#MTBFCompressorsRotating");
                $result.calc("(v1+v3)/v2*12", {
                    v1: $("#EquipCntCompressorsRotating"),
                    v2: $("#EventsCompressorsRotating"),
                    v3: $("#EquipCntCompressorsRotatingSpares")
                });
                formatResult($result, $result.val()); /*$("#MTBFCompressorsRotating").trigger('blur');*/
            });
    if ($("#EquipCntCompressorsRotating").val() != null && !isNaN($("#EquipCntCompressorsRotating").val()))
        if ($("#EventsCompressorsRotating").val() != null && !isNaN($("#EventsCompressorsRotating").val()))
            if ($("#EquipCntCompressorsRotatingSpares").val() != null && !isNaN($("#EquipCntCompressorsRotatingSpares").val())) {
                $("#EquipCntCompressorsRotating").trigger('keyup');
            } $("#EquipCntControlValves,#EventsControlValves ").bind("keyup", function () {
                var $result = $("#MTBFControlValves");
                $result.calc("v1/v2*12", {
                    v1: $("#EquipCntControlValves"),
                    v2: $("#EventsControlValves")
                });
                formatResult($result, $result.val()); /*$("#MTBFControlValves").trigger('blur');*/
            });
    if ($("#EquipCntControlValves").val() != null && !isNaN($("#EquipCntControlValves").val()))
        if ($("#EventsControlValves").val() != null && !isNaN($("#EventsControlValves").val())) {
            $("#EquipCntControlValves").trigger('keyup');
        } $("#EquipCntDistTowers,#EventsDistTowers ").bind("keyup", function () {
            var $result = $("#MTBFDistTowers");
            $result.calc("v1/v2*12", {
                v1: $("#EquipCntDistTowers"),
                v2: $("#EventsDistTowers")
            });
            formatResult($result, $result.val()); /*$("#MTBFDistTowers").trigger('blur');*/
        });
    if ($("#EquipCntDistTowers").val() != null && !isNaN($("#EquipCntDistTowers").val()))
        if ($("#EventsDistTowers").val() != null && !isNaN($("#EventsDistTowers").val())) {
            $("#EquipCntDistTowers").trigger('keyup');
        } $("#EquipCntFurnaceBoilers,#EventsFurnaceBoilers ").bind("keyup", function () {
            var $result = $("#MTBFFurnaceBoilers");
            $result.calc("v1/v2*12", {
                v1: $("#EquipCntFurnaceBoilers"),
                v2: $("#EventsFurnaceBoilers")
            });
            formatResult($result, $result.val()); /*$("#MTBFFurnaceBoilers").trigger('blur');*/
        });
    if ($("#EquipCntFurnaceBoilers").val() != null && !isNaN($("#EquipCntFurnaceBoilers").val()))
        if ($("#EventsFurnaceBoilers").val() != null && !isNaN($("#EventsFurnaceBoilers").val())) {
            $("#EquipCntFurnaceBoilers").trigger('keyup');
        } $("#EquipCntHeatExch,#EventsHeatExch,#EquipCntHeatExchSpares ").bind("keyup", function () {
            var $result = $("#MTBFHeatExch");
            $result.calc("(v1+v3)/v2*12", {
                v1: $("#EquipCntHeatExch"),
                v2: $("#EventsHeatExch"),
                v3: $("#EquipCntHeatExchSpares")
            });
            formatResult($result, $result.val()); /*$("#MTBFHeatExch").trigger('blur');*/
        });
    if ($("#EquipCntHeatExch").val() != null && !isNaN($("#EquipCntHeatExch").val()))
        if ($("#EventsHeatExch").val() != null && !isNaN($("#EventsHeatExch").val()))
            if ($("#EquipCntHeatExchSpares").val() != null && !isNaN($("#EquipCntHeatExchSpares").val())) {
                $("#EquipCntHeatExch").trigger('keyup');
            } $("#EquipCntMotorsMain,#EventsMotorsMain ").bind("keyup", function () {
                var $result = $("#MTBFMotorsMain");
                $result.calc("v1/v2*12", {
                    v1: $("#EquipCntMotorsMain"),
                    v2: $("#EventsMotorsMain")
                });
                formatResult($result, $result.val()); /*$("#MTBFMotorsMain").trigger('blur');*/
            });
    if ($("#EquipCntMotorsMain").val() != null && !isNaN($("#EquipCntMotorsMain").val()))
        if ($("#EventsMotorsMain").val() != null && !isNaN($("#EventsMotorsMain").val())) {
            $("#EquipCntMotorsMain").trigger('keyup');
        } $("#EquipCntPumpsCentrifugal,#EventsPumpsCentrifugal,#EquipCntPumpsCentrifugalSpares ").bind("keyup", function () {
            var $result = $("#MTBFPumpsCentrifugal");
            $result.calc("(v1+v3)/v2*12", {
                v1: $("#EquipCntPumpsCentrifugal"),
                v2: $("#EventsPumpsCentrifugal"),
                v3: $("#EquipCntPumpsCentrifugalSpares")
            });
            formatResult($result, $result.val()); /*$("#MTBFPumpsCentrifugal").trigger('blur');*/
        });
    if ($("#EquipCntPumpsCentrifugal").val() != null && !isNaN($("#EquipCntPumpsCentrifugal").val()))
        if ($("#EventsPumpsCentrifugal").val() != null && !isNaN($("#EventsPumpsCentrifugal").val()))
            if ($("#EquipCntPumpsCentrifugalSpares").val() != null && !isNaN($("#EquipCntPumpsCentrifugalSpares").val())) {
                $("#EquipCntPumpsCentrifugal").trigger('keyup');
            } $("#EquipCntPumpsPosDisp,#EventsPumpsPosDisp ").bind("keyup", function () {
                var $result = $("#MTBFPumpsPosDisp");
                $result.calc("v1/v2*12", {
                    v1: $("#EquipCntPumpsPosDisp"),
                    v2: $("#EventsPumpsPosDisp")
                });
                formatResult($result, $result.val()); /*$("#MTBFPumpsPosDisp").trigger('blur');*/
            });
    if ($("#EquipCntPumpsPosDisp").val() != null && !isNaN($("#EquipCntPumpsPosDisp").val()))
        if ($("#EventsPumpsPosDisp").val() != null && !isNaN($("#EventsPumpsPosDisp").val())) {
            $("#EquipCntPumpsPosDisp").trigger('keyup');
        } $("#EquipCntTurbines,#EventsTurbines,#EquipCntTurbinesSpares ").bind("keyup", function () {
            var $result = $("#MTBFTurbines");
            $result.calc("(v1+v3)/v2*12", {
                v1: $("#EquipCntTurbines"),
                v2: $("#EventsTurbines"),
                v3: $("#EquipCntTurbinesSpares")
            });
            formatResult($result, $result.val()); /*$("#MTBFTurbines").trigger('blur');*/
        });
    if ($("#EquipCntTurbines").val() != null && !isNaN($("#EquipCntTurbines").val()))
        if ($("#EventsTurbines").val() != null && !isNaN($("#EventsTurbines").val()))
            if ($("#EquipCntTurbinesSpares").val() != null && !isNaN($("#EquipCntTurbinesSpares").val())) {
                $("#EquipCntTurbines").trigger('keyup');
            } $("#EquipCntVarSpeedDrives,#EventsVarSpeedDrives ").bind("keyup", function () {
                var $result = $("#MTBFVarSpeedDrives");
                $result.calc("v1/v2*12", {
                    v1: $("#EquipCntVarSpeedDrives"),
                    v2: $("#EventsVarSpeedDrives")
                });
                formatResult($result, $result.val()); /*$("#MTBFVarSpeedDrives").trigger('blur');*/
            });
    if ($("#EquipCntVarSpeedDrives").val() != null && !isNaN($("#EquipCntVarSpeedDrives").val()))
        if ($("#EventsVarSpeedDrives").val() != null && !isNaN($("#EventsVarSpeedDrives").val())) {
            $("#EquipCntVarSpeedDrives").trigger('keyup');
        } $("#EquipCntVessels,#EventsVessels ").bind("keyup", function () {
            var $result = $("#MTBFVessels");
            $result.calc("v1/v2*12", {
                v1: $("#EquipCntVessels"),
                v2: $("#EventsVessels")
            });
            formatResult($result, $result.val()); /*$("#MTBFVessels").trigger('blur');*/
        });
    if ($("#EquipCntVessels").val() != null && !isNaN($("#EquipCntVessels").val()))
        if ($("#EventsVessels").val() != null && !isNaN($("#EventsVessels").val())) {
            $("#EquipCntVessels").trigger('keyup');
        } var secComments = Section_Comments;
    var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
        for (i = secComments.length - 1; i >= 0; i--) {
            var qidNm = secComments[i]['CommentID'];
            if ($('img[qid="' + qidNm + '"]').length) {
                $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
            }
        } <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function (index, html) {
        var gridId = $(this).closest('.grid').attr('id');
        if ($.trim(html).indexOf('nbsp', 0) == -1) {
            var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
    }
        $(this).html(newHTML);
    });
if ($("#EquipCntTurbines").val() != null && !isNaN($("#EquipCntTurbines").val()))
    if ($("#EquipCntTurbinesSpares").val() != null && !isNaN($("#EquipCntTurbinesSpares").val())) {
        var $result = $("#EquipCntTurbinesTOT");
        $result.calc("v1+v2", {
            v1: $("#EquipCntTurbines"),
            v2: $("#EquipCntTurbinesSpares")
        });
        formatResult($result, $result.val()); /*$("#SiteMCptlContWageRateUSD_RT").trigger('blur');*/
    };
if ($("#EquipCntCompressorsRecip").val() != null && !isNaN($("#EquipCntCompressorsRecip").val()))
    if ($("#EquipCntCompressorsRecipSpares").val() != null && !isNaN($("#EquipCntCompressorsRecipSpares").val())) {
        var $result = $("#EquipCntCompressorsRecipTOT");
        $result.calc("v1+v2", {
            v1: $("#EquipCntCompressorsRecip"),
            v2: $("#EquipCntCompressorsRecipSpares"),
        });
        formatResult($result, $result.val());
    };
if ($("#EquipCntCompressorsRotating").val() != null && !isNaN($("#EquipCntCompressorsRotating").val()))
    if ($("#EquipCntCompressorsRotatingSpares").val() != null && !isNaN($("#EquipCntCompressorsRotatingSpares").val())) {
        var $result = $("#EquipCntCompressorsRotatingTOT");
        $result.calc("v1+v2", {
            v1: $("#EquipCntCompressorsRotating"),
            v2: $("#EquipCntCompressorsRotatingSpares")
        });
        formatResult($result, $result.val());
    };

if ($("#EquipCntPumpsCentrifugal").val() != null && !isNaN($("#EquipCntPumpsCentrifugal").val()))
    if ($("#EquipCntPumpsCentrifugalSpares").val() != null && !isNaN($("#EquipCntPumpsCentrifugalSpares").val())) {
        var $result = $("#EquipCntPumpsCentrifugalTOT");
        $result.calc("v1+v2", {
            v1: $("#EquipCntPumpsCentrifugal"),
            v2: $("#EquipCntPumpsCentrifugalSpares")
        });
        formatResult($result, $result.val());
    };
if ($("#EquipCntPumpsPosDisp").val() != null && !isNaN($("#EquipCntPumpsPosDisp").val())) {
    var $result = $("#_EquipCntPumpsPosDisp");
    $result.calc("v1", {
        v1: $("#EquipCntPumpsPosDisp")
    });
    formatResult($result, $result.val());
};
if ($("#EquipCntFurnaceBoilers").val() != null && !isNaN($("#EquipCntFurnaceBoilers").val())) {
    var $result = $("#_EquipCntFurnaceBoilers");
    $result.calc("v1", {
        v1: $("#EquipCntFurnaceBoilers")
    });
    formatResult($result, $result.val());
};
if ($("#EquipCntVessels").val() != null && !isNaN($("#EquipCntVessels").val())) {
    var $result = $("#_EquipCntVessels");
    $result.calc("v1", {
        v1: $("#EquipCntVessels")
    });
    formatResult($result, $result.val());
};

if ($("#EquipCntDistTowers").val() != null && !isNaN($("#EquipCntDistTowers").val())) {
    var $result = $("#_EquipCntDistTowers");
    $result.calc("v1", {
        v1: $("#EquipCntDistTowers")
    });
    formatResult($result, $result.val());
};
if ($("#PEventsTurbines").val() != null && !isNaN($("#PEventsTurbines").val()))
    if ($("#EventsTurbines").val() != null && !isNaN($("#EventsTurbines").val())) {
        if ($("#EventsTurbines").val != 0) {
            var $result = $("#PEventsTurbinesMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsTurbines"),
                v2: $("#EventsTurbines")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsTurbines").val() != null && !isNaN($("#PEventsTurbines").val()))
    if ($("#EventsTurbines").val() != null && !isNaN($("#EventsTurbines").val())) {
        if ($("#EventsTurbines").val != 0) {
            $("#PEventsTurbines").trigger('keyup');
        } $("#PEventsTurbines,#PEventsTurbinesMTBE,#EventsTurbines,#PEventsTurbinesMTBE_Rct").bind("keyup", function () {
            var $result = $("#PEventsTurbinesMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsTurbines"),
                v2: $("#EventsTurbines")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsCompressorsRecip").val() != null && !isNaN($("#PEventsCompressorsRecip").val()))
    if ($("#EventsCompressorsRecip").val() != null && !isNaN($("#EventsCompressorsRecip").val())) {
        if ($("#EventsCompressorsRecip").val != 0) {
            var $result = $("#PEventsCompressorsRecipMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsCompressorsRecip"),
                v2: $("#EventsCompressorsRecip")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsCompressorsRecip").val() != null && !isNaN($("#PEventsCompressorsRecip").val()))
    if ($("#EventsCompressorsRecip").val() != null && !isNaN($("#EventsCompressorsRecip").val())) {
        if ($("#EventsCompressorsRecip").val != 0) {
            $("#PEventsCompressorsRecip").trigger('keyup');
        } $("#PEventsCompressorsRecip,#PEventsCompressorsRecipMTBE,#EventsCompressorsRecip,#PEventsCompressorsRecipMTBE_Rct").bind("keyup", function () {
            var $result = $("#PEventsCompressorsRecipMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsCompressorsRecip"),
                v2: $("#EventsCompressorsRecip")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsCompressorsRotating").val() != null && !isNaN($("#PEventsCompressorsRotating").val()))
    if ($("#EventsCompressorsRotating").val() != null && !isNaN($("#EventsCompressorsRotating").val())) {
        if ($("#EventsCompressorsRotating").val != 0) {
            var $result = $("#PEventsCompressorsRotatingMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsCompressorsRotating"),
                v2: $("#EventsCompressorsRotating")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsCompressorsRotating").val() != null && !isNaN($("#PEventsCompressorsRotating").val()))
    if ($("#EventsCompressorsRotating").val() != null && !isNaN($("#EventsCompressorsRotating").val())) {
        if ($("#EventsCompressorsRotating").val != 0) {
            $("#PEventsCompressorsRotating").trigger('keyup');
        } $("#PEventsCompressorsRotating,#PEventsCompressorsRotatingMTBE,#EventsCompressorsRotating,#PEventsCompressorsRotatingMTBE_Rct").bind("keyup", function () {
            var $result = $("#PEventsCompressorsRotatingMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsCompressorsRotating"),
                v2: $("#EventsCompressorsRotating")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsPumpsCentrifugal").val() != null && !isNaN($("#PEventsPumpsCentrifugal").val()))
    if ($("#EventsPumpsCentrifugal").val() != null && !isNaN($("#EventsPumpsCentrifugal").val())) {
        if ($("#EventsPumpsCentrifugal").val != 0) {
            var $result = $("#PEventsPumpsCentrifugalMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsPumpsCentrifugal"),
                v2: $("#EventsPumpsCentrifugal")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsPumpsCentrifugal").val() != null && !isNaN($("#PEventsPumpsCentrifugal").val()))
    if ($("#EventsPumpsCentrifugal").val() != null && !isNaN($("#EventsPumpsCentrifugal").val())) {
        if ($("#EventsPumpsCentrifugal").val != 0) {
            $("#PEventsPumpsCentrifugal").trigger('keyup');
        } $("#PEventsPumpsCentrifugal,#PEventsPumpsCentrifugalMTBE,#EventsPumpsCentrifugal,#PEventsPumpsCentrifugalMTBE_Rct").bind("keyup", function () {
            var $result = $("#PEventsPumpsCentrifugalMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsPumpsCentrifugal"),
                v2: $("#EventsPumpsCentrifugal")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsPumpsPosDisp").val() != null && !isNaN($("#PEventsPumpsPosDisp").val()))
    if ($("#EventsPumpsPosDisp").val() != null && !isNaN($("#EventsPumpsPosDisp").val())) {
        if ($("#EventsPumpsPosDisp").val != 0) {
            var $result = $("#PEventsPumpsPosDispMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsPumpsPosDisp"),
                v2: $("#EventsPumpsPosDisp")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsPumpsPosDisp").val() != null && !isNaN($("#PEventsPumpsPosDisp").val()))
    if ($("#EventsPumpsPosDisp").val() != null && !isNaN($("#EventsPumpsPosDisp").val())) {
        if ($("#EventsPumpsPosDisp").val != 0) {
            $("#PEventsPumpsPosDisp").trigger('keyup');
        } $("#PEventsPumpsPosDisp,#PEventsPumpsPosDispMTBE,#EventsPumpsPosDisp,#PEventsPumpsPosDispMTBE_Rct").bind("keyup", function () {
            var $result = $("#PEventsPumpsPosDispMTBE");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsPumpsPosDisp"),
                v2: $("#EventsPumpsPosDisp")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsTurbines").val() != null && !isNaN($("#REventsTurbines").val()))
    if ($("#EventsTurbines").val() != null && !isNaN($("#EventsTurbines").val())) {
        if ($("#EventsTurbines").val != 0) {
            var $result = $("#PEventsTurbinesMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsTurbines"),
                v2: $("#EventsTurbines")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsTurbines").val() != null && !isNaN($("#REventsTurbines").val()))
    if ($("#EventsTurbines").val() != null && !isNaN($("#EventsTurbines").val())) {
        if ($("#EventsTurbines").val != 0) {
            $("#REventsTurbines").trigger('keyup');
        } $("#REventsTurbines,#PEventsTurbinesMTBE_Rct,#EventsTurbines,#PEventsTurbines").bind("keyup", function () {
            var $result = $("#PEventsTurbinesMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsTurbines"),
                v2: $("#EventsTurbines")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsCompressorsRecip").val() != null && !isNaN($("#REventsCompressorsRecip").val()))
    if ($("#EventsCompressorsRecip").val() != null && !isNaN($("#EventsCompressorsRecip").val())) {
        if ($("#EventsCompressorsRecip").val != 0) {
            var $result = $("#PEventsCompressorsRecipMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsCompressorsRecip"),
                v2: $("#EventsCompressorsRecip")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsCompressorsRecip").val() != null && !isNaN($("#REventsCompressorsRecip").val()))
    if ($("#EventsCompressorsRecip").val() != null && !isNaN($("#EventsCompressorsRecip").val())) {
        if ($("#EventsCompressorsRecip").val != 0) {
            $("#REventsCompressorsRecip").trigger('keyup');
        } $("#REventsCompressorsRecip,#PEventsCompressorsRecipMTBE_Rct,#EventsCompressorsRecip,#PEventsCompressorsRecip").bind("keyup", function () {
            var $result = $("#PEventsCompressorsRecipMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsCompressorsRecip"),
                v2: $("#EventsCompressorsRecip")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsCompressorsRotating").val() != null && !isNaN($("#REventsCompressorsRotating").val()))
    if ($("#EventsCompressorsRotating").val() != null && !isNaN($("#EventsCompressorsRotating").val())) {
        if ($("#EventsCompressorsRotating").val != 0) {
            var $result = $("#PEventsCompressorsRotatingMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsCompressorsRotating"),
                v2: $("#EventsCompressorsRotating")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsCompressorsRotating").val() != null && !isNaN($("#REventsCompressorsRotating").val()))
    if ($("#EventsCompressorsRotating").val() != null && !isNaN($("#EventsCompressorsRotating").val())) {
        if ($("#EventsCompressorsRotating").val != 0) {
            $("#REventsCompressorsRotating").trigger('keyup');
        } $("#REventsCompressorsRotating,#PEventsCompressorsRotatingMTBE_Rct,#EventsCompressorsRotating,#PEventsCompressorsRotating").bind("keyup", function () {
            var $result = $("#PEventsCompressorsRotatingMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsCompressorsRotating"),
                v2: $("#EventsCompressorsRotating")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsPumpsCentrifugal").val() != null && !isNaN($("#REventsPumpsCentrifugal").val()))
    if ($("#EventsPumpsCentrifugal").val() != null && !isNaN($("#EventsPumpsCentrifugal").val())) {
        if ($("#EventsPumpsCentrifugal").val != 0) {
            var $result = $("#PEventsPumpsCentrifugalMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsPumpsCentrifugal"),
                v2: $("#EventsPumpsCentrifugal")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsPumpsCentrifugal").val() != null && !isNaN($("#REventsPumpsCentrifugal").val()))
    if ($("#EventsPumpsCentrifugal").val() != null && !isNaN($("#EventsPumpsCentrifugal").val())) {
        if ($("#EventsPumpsCentrifugal").val != 0) {
            $("#REventsPumpsCentrifugal").trigger('keyup');
        } $("#REventsPumpsCentrifugal,#PEventsPumpsCentrifugalMTBE_Rct,#EventsPumpsCentrifugal,#PEventsPumpsCentrifugal").bind("keyup", function () {
            var $result = $("#PEventsPumpsCentrifugalMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsPumpsCentrifugal"),
                v2: $("#EventsPumpsCentrifugal")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsPumpsPosDisp").val() != null && !isNaN($("#REventsPumpsPosDisp").val()))
    if ($("#EventsPumpsPosDisp").val() != null && !isNaN($("#EventsPumpsPosDisp").val())) {
        if ($("#REventsPumpsPosDisp").val != 0) {
            var $result = $("#PEventsPumpsPosDispMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsPumpsPosDisp"),
                v2: $("#EventsPumpsPosDisp")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsPumpsPosDisp").val() != null && !isNaN($("#REventsPumpsPosDisp").val()))
    if ($("#EventsPumpsPosDisp").val() != null && !isNaN($("#EventsPumpsPosDisp").val())) {
        if ($("#EventsPumpsPosDisp").val != 0) {
            $("#REventsPumpsPosDisp").trigger('keyup');
        } $("#REventsPumpsPosDisp,#PEventsPumpsPosDispMTBE_Rct,#EventsPumpsPosDisp,#PEventsPumpsPosDisp").bind("keyup", function () {
            var $result = $("#PEventsPumpsPosDispMTBE_Rct");
            $result.calc("v1*100/v2", {
                v1: $("#REventsPumpsPosDisp"),
                v2: $("#EventsPumpsPosDisp")
            });
            formatResult($result, $result.val());
        })
    };
$("input[CalcTag*='sumPctRE']").sum({
    bind: "keyup",
    selector: "#PctEventsTotalRE",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value);
        }
    }
});
$('#PctEventsTotalRE').attr('readonly', 'readonly');
$('#PctEventsTotalRE').change(function () {
    $(this).validate();
});
if ($("#EquipCntHeatExch").val() != null && !isNaN($("#EquipCntHeatExch").val()))
    if ($("#EquipCntHeatExchSpares").val() != null && !isNaN($("#EquipCntHeatExchSpares").val())) {
        var $result = $("#EquipCntHeatExchTOT");
        $result.calc("v1+v2", {
            v1: $("#EquipCntHeatExch"),
            v2: $("#EquipCntHeatExchSpares")
        });
        formatResult($result, $result.val());
    };
if ($("#PEventsFurnaceBoilers").val() != null && !isNaN($("#PEventsFurnaceBoilers").val()))
    if ($("#EventsFurnaceBoilers").val() != null && !isNaN($("#EventsFurnaceBoilers").val())) {
        if ($("#EventsFurnaceBoilers").val != 0) {
            var $result = $("#PEventsMTBEFurnaceBoilers");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsFurnaceBoilers"),
                v2: $("#EventsFurnaceBoilers")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsFurnaceBoilers").val() != null && !isNaN($("#PEventsFurnaceBoilers").val()))
    if ($("#EventsFurnaceBoilers").val() != null && !isNaN($("#EventsFurnaceBoilers").val())) {
        if ($("#EventsFurnaceBoilers").val != 0) {
            $("#PEventsFurnaceBoilers").trigger('keyup');
        } $("#PEventsFurnaceBoilers,#PEventsMTBEFurnaceBoilers,#EventsFurnaceBoilers,#REventsFurnaceBoilers").bind("keyup", function () {
            var $result = $("#PEventsMTBEFurnaceBoilers");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsFurnaceBoilers"),
                v2: $("#EventsFurnaceBoilers")
            });
            formatResult($result, $result.val());
        })
    };

if ($("#PEventsVessels").val() != null && !isNaN($("#PEventsVessels").val()))
    if ($("#EventsVessels").val() != null && !isNaN($("#EventsVessels").val())) {
        if ($("#EventsVessels").val != 0) {
            var $result = $("#PEventsMTBEVessels");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsVessels"),
                v2: $("#EventsVessels")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsVessels").val() != null && !isNaN($("#PEventsVessels").val()))
    if ($("#EventsVessels").val() != null && !isNaN($("#EventsVessels").val())) {
        if ($("#EventsVessels").val != 0) {
            $("#PEventsVessels").trigger('keyup');
        } $("#PEventsVessels,#PEventsMTBEVessels,#EventsVessels,#REventsVessels").bind("keyup", function () {
            var $result = $("#PEventsMTBEVessels");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsVessels"),
                v2: $("#EventsVessels")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsDistTowers").val() != null && !isNaN($("#PEventsDistTowers").val()))
    if ($("#EventsDistTowers").val() != null && !isNaN($("#EventsDistTowers").val())) {
        if ($("#EventsDistTowers").val != 0) {
            var $result = $("#PEventsMTBEDistTowers");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsDistTowers"),
                v2: $("#EventsDistTowers")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsDistTowers").val() != null && !isNaN($("#PEventsDistTowers").val()))
    if ($("#EventsDistTowers").val() != null && !isNaN($("#EventsDistTowers").val())) {
        if ($("#EventsDistTowers").val != 0) {
            $("#PEventsDistTowers").trigger('keyup');
        } $("#PEventsDistTowers,#PEventsMTBEDistTowers,#EventsDistTowers,#REventsDistTowers").bind("keyup", function () {
            var $result = $("#PEventsMTBEDistTowers");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsDistTowers"),
                v2: $("#EventsDistTowers")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsHeatExch").val() != null && !isNaN($("#PEventsHeatExch").val()))
    if ($("#EventsHeatExch").val() != null && !isNaN($("#EventsHeatExch").val())) {
        if ($("#EventsHeatExch").val != 0) {
            var $result = $("#PEventsMTBEHeatExch");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsHeatExch"),
                v2: $("#EventsHeatExch")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsHeatExch").val() != null && !isNaN($("#PEventsHeatExch").val()))
    if ($("#EventsHeatExch").val() != null && !isNaN($("#EventsHeatExch").val())) {
        if ($("#EventsHeatExch").val != 0) {
            $("#PEventsHeatExch").trigger('keyup');
        } $("#PEventsHeatExch,#PEventsMTBEHeatExch,#EventsHeatExch,#REventsHeatExch").bind("keyup", function () {
            var $result = $("#PEventsMTBEHeatExch");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsHeatExch"),
                v2: $("#EventsHeatExch")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsFurnaceBoilers").val() != null && !isNaN($("#REventsFurnaceBoilers").val()))
    if ($("#EventsFurnaceBoilers").val() != null && !isNaN($("#EventsFurnaceBoilers").val())) {
        if ($("#EventsFurnaceBoilers").val != 0) {
            var $result = $("#REventsMTBEFurnaceBoilers");
            $result.calc("v1*100/v2", {
                v1: $("#REventsFurnaceBoilers"),
                v2: $("#EventsFurnaceBoilers")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsFurnaceBoilers").val() != null && !isNaN($("#REventsFurnaceBoilers").val()))
    if ($("#EventsFurnaceBoilers").val() != null && !isNaN($("#EventsFurnaceBoilers").val())) {
        if ($("#EventsFurnaceBoilers").val != 0) {
            $("#REventsFurnaceBoilers").trigger('keyup');
        } $("#REventsFurnaceBoilers,#REventsMTBEFurnaceBoilers,#EventsFurnaceBoilers,#PEventsFurnaceBoilers").bind("keyup", function () {
            var $result = $("#REventsMTBEFurnaceBoilers");
            $result.calc("v1*100/v2", {
                v1: $("#REventsFurnaceBoilers"),
                v2: $("#EventsFurnaceBoilers")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsVessels").val() != null && !isNaN($("#REventsVessels").val()))
    if ($("#EventsVessels").val() != null && !isNaN($("#EventsVessels").val())) {
        if ($("#EventsVessels").val != 0) {
            var $result = $("#REventsMTBEVessels");
            $result.calc("v1*100/v2", {
                v1: $("#REventsVessels"),
                v2: $("#EventsVessels")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsVessels").val() != null && !isNaN($("#REventsVessels").val()))
    if ($("#EventsVessels").val() != null && !isNaN($("#EventsVessels").val())) {
        if ($("#EventsVessels").val != 0) {
            $("#REventsVessels").trigger('keyup');
        } $("#REventsVessels,#REventsMTBEVessels,#EventsVessels").bind("keyup", function () {
            var $result = $("#REventsMTBEVessels");
            $result.calc("v1*100/v2", {
                v1: $("#REventsVessels"),
                v2: $("#EventsVessels")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsDistTowers").val() != null && !isNaN($("#REventsDistTowers").val()))
    if ($("#EventsDistTowers").val() != null && !isNaN($("#EventsDistTowers").val())) {
        if ($("#EventsDistTowers").val != 0) {
            var $result = $("#REventsMTBEDistTowers");
            $result.calc("v1*100/v2", {
                v1: $("#REventsDistTowers"),
                v2: $("#EventsDistTowers")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsDistTowers").val() != null && !isNaN($("#REventsDistTowers").val()))
    if ($("#EventsDistTowers").val() != null && !isNaN($("#EventsDistTowers").val())) {
        if ($("#EventsDistTowers").val != 0) {
            $("#REventsDistTowers").trigger('keyup');
        } $("#REventsDistTowers,#REventsMTBEDistTowers,#EventsDistTowers,#PEventsDistTowers").bind("keyup", function () {
            var $result = $("#REventsMTBEDistTowers");
            $result.calc("v1*100/v2", {
                v1: $("#REventsDistTowers"),
                v2: $("#EventsDistTowers")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsHeatExch").val() != null && !isNaN($("#REventsHeatExch").val()))
    if ($("#EventsHeatExch").val() != null && !isNaN($("#EventsHeatExch").val())) {
        if ($("#EventsHeatExch").val != 0) {
            var $result = $("#REventsMTBEHeatExch");
            $result.calc("v1*100/v2", {
                v1: $("#REventsHeatExch"),
                v2: $("#EventsHeatExch")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsHeatExch").val() != null && !isNaN($("#REventsHeatExch").val()))
    if ($("#EventsHeatExch").val() != null && !isNaN($("#EventsHeatExch").val())) {
        if ($("#EventsHeatExch").val != 0) {
            $("#REventsHeatExch").trigger('keyup');
        } $("#REventsHeatExch,#REventsMTBEHeatExch,#EventsHeatExch,#PEventsHeatExch").bind("keyup", function () {
            var $result = $("#REventsMTBEHeatExch");
            $result.calc("v1*100/v2", {
                v1: $("#REventsHeatExch"),
                v2: $("#EventsHeatExch")
            });
            formatResult($result, $result.val());
        })
    };
$("input[CalcTag*='sumPctFP']").sum({
    bind: "keyup",
    selector: "#PctEventsTotalRE_FP",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value);
        }
    }
});
$('#PctEventsTotalRE_FP').attr('readonly', 'readonly');
$('#PctEventsTotalRE_FP').change(function () {
    $(this).validate();
});
if ($("#PEventsMotorsMain").val() != null && !isNaN($("#PEventsMotorsMain").val()))
    if ($("#EventsMotorsMain").val() != null && !isNaN($("#EventsMotorsMain").val())) {
        if ($("#EventsMotorsMain").val != 0) {
            var $result = $("#PMTBEMotorsMain");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsMotorsMain"),
                v2: $("#EventsMotorsMain")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsMotorsMain").val() != null && !isNaN($("#PEventsMotorsMain").val()))
    if ($("#EventsMotorsMain").val() != null && !isNaN($("#EventsMotorsMain").val())) {
        if ($("#EventsMotorsMain").val != 0) {
            $("#PEventsMotorsMain").trigger('keyup');
        } $("#PEventsMotorsMain,#PMTBEMotorsMain,#EventsMotorsMain,#REventsMotorsMain").bind("keyup", function () {
            var $result = $("#PMTBEMotorsMain");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsMotorsMain"),
                v2: $("#EventsMotorsMain")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsVarSpeedDrives").val() != null && !isNaN($("#PEventsVarSpeedDrives").val()))
    if ($("#EventsVarSpeedDrives").val() != null && !isNaN($("#EventsVarSpeedDrives").val())) {
        if ($("#EventsVarSpeedDrives").val != 0) {
            var $result = $("#PMTBEVarSpeedDrives");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsVarSpeedDrives"),
                v2: $("#EventsVarSpeedDrives")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsVarSpeedDrives").val() != null && !isNaN($("#PEventsVarSpeedDrives").val()))
    if ($("#EventsVarSpeedDrives").val() != null && !isNaN($("#EventsVarSpeedDrives").val())) {
        if ($("#EventsVarSpeedDrives").val != 0) {
            $("#PEventsVarSpeedDrives").trigger('keyup');
        } $("#PEventsVarSpeedDrives,#PMTBEVarSpeedDrives,#EventsVarSpeedDrives,#REventsVarSpeedDrives").bind("keyup", function () {
            var $result = $("#PMTBEVarSpeedDrives");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsVarSpeedDrives"),
                v2: $("#EventsVarSpeedDrives")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsControlValves").val() != null && !isNaN($("#PEventsControlValves").val()))
    if ($("#EventsControlValves").val() != null && !isNaN($("#EventsControlValves").val())) {
        if ($("#EventsControlValves").val != 0) {
            var $result = $("#PMTBEControlValves");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsControlValves"),
                v2: $("#EventsControlValves")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsControlValves").val() != null && !isNaN($("#PEventsControlValves").val()))
    if ($("#EventsControlValves").val() != null && !isNaN($("#EventsControlValves").val())) {
        if ($("#EventsControlValves").val != 0) {
            $("#PEventsControlValves").trigger('keyup');
        } $("#PEventsControlValves,#PMTBEControlValves,#EventsControlValves,#REventsControlValves").bind("keyup", function () {
            var $result = $("#PMTBEControlValves");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsControlValves"),
                v2: $("#EventsControlValves")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsAnalyzerProcCtrl").val() != null && !isNaN($("#PEventsAnalyzerProcCtrl").val()))
    if ($("#EventsAnalyzerProcCtrl").val() != null && !isNaN($("#EventsAnalyzerProcCtrl").val())) {
        if ($("#PEventsAnalyzerProcCtrl").val != 0) {
            var $result = $("#PMTBEAnalyzerProcCtrl");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsAnalyzerProcCtrl"),
                v2: $("#EventsAnalyzerProcCtrl")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsAnalyzerProcCtrl").val() != null && !isNaN($("#PEventsAnalyzerProcCtrl").val()))
    if ($("#EventsAnalyzerProcCtrl").val() != null && !isNaN($("#EventsAnalyzerProcCtrl").val())) {
        if ($("#PEventsAnalyzerProcCtrl").val != 0) {
            $("#PEventsAnalyzerProcCtrl").trigger('keyup');
        } $("#PEventsAnalyzerProcCtrl,#PMTBEAnalyzerProcCtrl,#EventsAnalyzerProcCtrl,#REventsAnalyzerProcCtrl").bind("keyup", function () {
            var $result = $("#PMTBEAnalyzerProcCtrl");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsAnalyzerProcCtrl"),
                v2: $("#EventsAnalyzerProcCtrl")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsAnalyzerBlending").val() != null && !isNaN($("#PEventsAnalyzerBlending").val()))
    if ($("#EventsAnalyzerBlending").val() != null && !isNaN($("#EventsAnalyzerBlending").val())) {
        if ($("#EventsAnalyzerBlending").val != 0) {
            var $result = $("#PMTBEAnalyzerBlending");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsAnalyzerBlending"),
                v2: $("#EventsAnalyzerBlending")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsAnalyzerBlending").val() != null && !isNaN($("#PEventsAnalyzerBlending").val()))
    if ($("#EventsAnalyzerBlending").val() != null && !isNaN($("#EventsAnalyzerBlending").val())) {
        if ($("#PEventsAnalyzerBlending").val != 0) {
            $("#PEventsAnalyzerBlending").trigger('keyup');
        } $("#PEventsAnalyzerBlending,#PMTBEAnalyzerBlending,#EventsAnalyzerBlending,#REventsAnalyzerBlending").bind("keyup", function () {
            var $result = $("#PMTBEAnalyzerBlending");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsAnalyzerBlending"),
                v2: $("#EventsAnalyzerBlending")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#PEventsAnalyzerEmissions").val() != null && !isNaN($("#PEventsAnalyzerEmissions").val()))
    if ($("#EventsAnalyzerEmissions").val() != null && !isNaN($("#EventsAnalyzerEmissions").val())) {
        if ($("#EventsAnalyzerEmissions").val != 0) {
            var $result = $("#PMTBEAnalyzerEmissions");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsAnalyzerEmissions"),
                v2: $("#EventsAnalyzerEmissions")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#PEventsAnalyzerEmissions").val() != null && !isNaN($("#PEventsAnalyzerEmissions").val()))
    if ($("#EventsAnalyzerEmissions").val() != null && !isNaN($("#EventsAnalyzerEmissions").val())) {
        if ($("#EventsAnalyzerEmissions").val != 0) {
            $("#PEventsAnalyzerEmissions").trigger('keyup');
        } $("#PEventsAnalyzerEmissions,#PMTBEAnalyzerEmissions,#EventsAnalyzerEmissions,#REventsAnalyzerEmissions").bind("keyup", function () {
            var $result = $("#PMTBEAnalyzerEmissions");
            $result.calc("v1*100/v2", {
                v1: $("#PEventsAnalyzerEmissions"),
                v2: $("#EventsAnalyzerEmissions")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsMotorsMain").val() != null && !isNaN($("#REventsMotorsMain").val()))
    if ($("#EventsMotorsMain").val() != null && !isNaN($("#EventsMotorsMain").val())) {
        if ($("#EventsMotorsMain").val != 0) {
            var $result = $("#RMTBEMotorsMain");
            $result.calc("v1*100/v2", {
                v1: $("#REventsMotorsMain"),
                v2: $("#EventsMotorsMain")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsMotorsMain").val() != null && !isNaN($("#REventsMotorsMain").val()))
    if ($("#EventsMotorsMain").val() != null && !isNaN($("#EventsMotorsMain").val())) {
        if ($("#EventsMotorsMain").val != 0) {
            $("#REventsMotorsMain").trigger('keyup');
        } $("#REventsMotorsMain,#RMTBEMotorsMain,#EventsMotorsMain,#PEventsMotorsMain").bind("keyup", function () {
            var $result = $("#RMTBEMotorsMain");
            $result.calc("v1*100/v2", {
                v1: $("#REventsMotorsMain"),
                v2: $("#EventsMotorsMain")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsVarSpeedDrives").val() != null && !isNaN($("#REventsVarSpeedDrives").val()))
    if ($("#EventsVarSpeedDrives").val() != null && !isNaN($("#EventsVarSpeedDrives").val())) {
        if ($("#EventsVarSpeedDrives").val != 0) {
            var $result = $("#RMTBEVarSpeedDrives");
            $result.calc("v1*100/v2", {
                v1: $("#REventsVarSpeedDrives"),
                v2: $("#EventsVarSpeedDrives")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsVarSpeedDrives").val() != null && !isNaN($("#REventsVarSpeedDrives").val()))
    if ($("#EventsVarSpeedDrives").val() != null && !isNaN($("#EventsVarSpeedDrives").val())) {
        if ($("#EventsVarSpeedDrives").val != 0) {
            $("#REventsVarSpeedDrives").trigger('keyup');
        } $("#REventsVarSpeedDrives,#RMTBEVarSpeedDrives,#EventsVarSpeedDrives,#PEventsVarSpeedDrives").bind("keyup", function () {
            var $result = $("#RMTBEVarSpeedDrives");
            $result.calc("v1*100/v2", {
                v1: $("#REventsVarSpeedDrives"),
                v2: $("#EventsVarSpeedDrives")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsControlValves").val() != null && !isNaN($("#REventsControlValves").val()))
    if ($("#EventsControlValves").val() != null && !isNaN($("#EventsControlValves").val())) {
        if ($("#EventsControlValves").val != 0) {
            var $result = $("#RMTBEControlValves");
            $result.calc("v1*10/v2", {
                v1: $("#REventsControlValves"),
                v2: $("#EventsControlValves")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsControlValves").val() != null && !isNaN($("#REventsControlValves").val()))
    if ($("#EventsControlValves").val() != null && !isNaN($("#EventsControlValves").val())) {
        if ($("#EventsControlValves").val != 0) {
            $("#REventsControlValves").trigger('keyup');
        } $("#REventsControlValves,#RMTBEControlValves,#EventsControlValves,#PEventsControlValves").bind("keyup", function () {
            var $result = $("#RMTBEControlValves");
            $result.calc("v1*100/v2", {
                v1: $("#REventsControlValves"),
                v2: $("#EventsControlValves")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsAnalyzerProcCtrl").val() != null && !isNaN($("#REventsAnalyzerProcCtrl").val()))
    if ($("#EventsAnalyzerProcCtrl").val() != null && !isNaN($("#EventsAnalyzerProcCtrl").val())) {
        if ($("#EventsAnalyzerProcCtrl").val != 0) {
            var $result = $("#RMTBEAnalyzerProcCtrl");
            $result.calc("v1*100/v2", {
                v1: $("#REventsAnalyzerProcCtrl"),
                v2: $("#EventsAnalyzerProcCtrl")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsAnalyzerProcCtrl").val() != null && !isNaN($("#REventsAnalyzerProcCtrl").val()))
    if ($("#EventsAnalyzerProcCtrl").val() != null && !isNaN($("#EventsAnalyzerProcCtrl").val())) {
        if ($("#EventsAnalyzerProcCtrl").val != 0) {
            $("#REventsAnalyzerProcCtrl").trigger('keyup');
        } $("#REventsAnalyzerProcCtrl,#RMTBEAnalyzerProcCtrl,#EventsAnalyzerProcCtrl,#PEventsAnalyzerProcCtrl").bind("keyup", function () {
            var $result = $("#RMTBEAnalyzerProcCtrl");
            $result.calc("v1*100/v2", {
                v1: $("#REventsAnalyzerProcCtrl"),
                v2: $("#EventsAnalyzerProcCtrl")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsAnalyzerBlending").val() != null && !isNaN($("#REventsAnalyzerBlending").val()))
    if ($("#EventsAnalyzerBlending").val() != null && !isNaN($("#EventsAnalyzerBlending").val())) {
        if ($("#EventsAnalyzerBlending").val != 0) {
            var $result = $("#RMTBEAnalyzerBlending");
            $result.calc("v1*100/v2", {
                v1: $("#REventsAnalyzerBlending"),
                v2: $("#EventsAnalyzerBlending")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsAnalyzerBlending").val() != null && !isNaN($("#REventsAnalyzerBlending").val()))
    if ($("#EventsAnalyzerBlending").val() != null && !isNaN($("#EventsAnalyzerBlending").val())) {
        if ($("#EventsAnalyzerBlending").val != 0) {
            $("#REventsAnalyzerBlending").trigger('keyup');
        } $("#REventsAnalyzerBlending,#RMTBEAnalyzerBlending,#EventsAnalyzerBlending,#PEventsAnalyzerBlending").bind("keyup", function () {
            var $result = $("#RMTBEAnalyzerBlending");
            $result.calc("v1*100/v2", {
                v1: $("#REventsAnalyzerBlending"),
                v2: $("#EventsAnalyzerBlending")
            });
            formatResult($result, $result.val());
        })
    };
if ($("#REventsAnalyzerEmissions").val() != null && !isNaN($("#REventsAnalyzerEmissions").val()))
    if ($("#EventsAnalyzerEmissions").val() != null && !isNaN($("#EventsAnalyzerEmissions").val())) {
        if ($("#EventsAnalyzerEmissions").val != 0) {
            var $result = $("#RMTBEAnalyzerEmissions");
            $result.calc("v1*100/v2", {
                v1: $("#REventsAnalyzerEmissions"),
                v2: $("#EventsAnalyzerEmissions")
            });
            formatResult($result, $result.val());
        }
    };
if ($("#REventsAnalyzerEmissions").val() != null && !isNaN($("#REventsAnalyzerEmissions").val()))
    if ($("#EventsAnalyzerEmissions").val() != null && !isNaN($("#EventsAnalyzerEmissions").val())) {
        if ($("#EventsAnalyzerEmissions").val != 0) {
            $("#REventsAnalyzerEmissions").trigger('keyup');
        } $("#REventsAnalyzerEmissions,#RMTBEAnalyzerEmissions,#EventsAnalyzerEmissions,#PEventsAnalyzerEmissions").bind("keyup", function () {
            var $result = $("#RMTBEAnalyzerEmissions");
            $result.calc("v1*100/v2", {
                v1: $("#REventsAnalyzerEmissions"),
                v2: $("#EventsAnalyzerEmissions")
            });
            formatResult($result, $result.val());
        })
    };
$("input[CalcTag*='sumPctIE']").sum({
    bind: "keyup",
    selector: "#PctEventsTotal_IE",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value);
        }
    }
});
$('#PctEventsTotal_IE').attr('readonly', 'readonly');
$('#PctEventsTotal_IE').change(function () {
    $(this).validate();
});

$("input[CalcTag*='sumPctPU']").sum({
    bind: "keyup",
    selector: "#PctEventsTotal_PU",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value);
        }
    }
});
$('#PctEventsTotal_PU').attr('readonly', 'readonly');
$('#PctEventsTotal_PU').change(function () {
    $(this).validate();
});


$("input[CalcTag*='sumPctPP']").sum({
    bind: "keyup",
    selector: "#PctEventsTotal_PP",
    oncalc: function (value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value);
        }
    }
});
$('#PctEventsTotal_PP').attr('readonly', 'readonly');
$('#PctEventsTotal_PP').change(function () {
    $(this).validate();
});
$('img[suid]').click(function () {
    var suid = $(this).attr('suid');
    var qid = $(this).attr('qid');
    recordComment(suid, qid);
}); </script>
</asp:Content>
