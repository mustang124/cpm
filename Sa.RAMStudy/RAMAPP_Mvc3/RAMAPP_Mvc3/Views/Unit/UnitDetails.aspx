﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UnitDetails>" %>

<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="MvcContrib.UI.Grid.ActionSyntax" %>
<%@ Import Namespace="System.Web.UI.DataVisualization.Charting" %>
<%@ Import Namespace="RAMAPP_Mvc3.Controllers" %>
<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <link href="<%= Url.Content("~/Content/jquery-ui-1.8.15.custom.min.css")%>" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/mvc/3.0/jquery.unobtrusive-ajax.min.js" type="text/javascript"></script>
    <script src="<%: Url.Content("~/Scripts/jquery-ui-1.8.15.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%: Url.Content("~/Scripts/jquery.ui.selectmenu.js") %>" type="text/javascript"></script>

    <style type="text/css">
        #OtherGenericProoductName {
            font-family: Arial,Verdana;
        }

        .scorebox {
            display: inline-block;
            text-align: center;
        }

            .scorebox .innerbox {
                -webkit-border-radius: 12px;
                -moz-border-radius: 12px;
                border-radius: 12px;
                /*height: 25px;*/
                width: 24px;
                margin: 1px;
                background: #ccc;
            }

            .scorebox .score {
                color: #6699ff;
                text-shadow: 0px 0px 10px #99ccff padding: 1px;
                font-family: Helvetica, Arial;
                font-size: 14px;
                /*color: #333;*/
            }

        .score a {
            font-weight: 500;
            color: #333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="float: left; <%= !SessionUtil.GetCompanySID().Contains("00TEST") ? "display:none;": "" %>">
        Data Quality:
        <div class="scorebox">
            <div class="innerbox">
                <div class="score">
                    <a title="See Details" href="<%=Url.RouteUrl(
                                      new  { action="ViewGradeDetails",controller="Review",id=Model.DatasetID
                                       }
) %>?start=1&size=20&name=<%=Model.UnitName %>">
                        <%= ((UnitController)ViewContext.Controller). GetFacilityDataQuality(Model.DatasetID)%>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <p>
        &nbsp;
    </p>
    <fieldset style="width: 700px;">
        <legend>Unit Description</legend>
        <div class="container_16">

            <div class="grid_16">
                <span style="float: left; font-size: .85em;">
                    <img alt="return" align="middle" src="<%=Page.ResolveUrl("~/images")%>/icon_back.gif" />
                    <%: Html.ActionLink("Return", "SiteDetail","Site", new { id = Model.SiteDatasetID },null)%>
                </span>
                <span style="float: right; font-size: .85em;">
                    <% if ((ViewContext.RouteData.Values["action"].ToString() != "CreateUnit"))
                       {%>
                    <img alt="delete" align="middle" src="<%=Page.ResolveUrl("~/images")%>/x-delete-icon.png" />
                    <%:Html.Raw(Ajax.ActionLink("imagelink",
                                                "DeleteUnit",
                                                new {id = Model.DatasetID,name=Model.UnitName},
                                                new AjaxOptions
                                                    {
                                                        Confirm =
                                                            "Are you sure you want to remove this unit from using this site?",
                                                        HttpMethod = "Delete",
                                                        OnComplete = "JsonDeleteUnit_OnComplete"

                                                    },
                                                new {id = "delLink_" + Model.DatasetID})
                                    .ToHtmlString()
                                    .Replace("imagelink",
                                             " Delete Unit"))%>
                    <% } %>
                </span>
            </div>
            <div style="border-bottom: #CCC thin solid; width: 100%; margin: 10px;">
                &nbsp;
            </div>

            <div class="grid_16">
                <div id="ReturnMsg" class="<%:  ViewData["resultClass"] ?? String.Empty%>">
                    <span class="ResponseMsg"><%:  ViewData["result"] ?? String.Empty%></span>
                </div>

            </div>

            <p>
                &nbsp;
            </p>
            <div class="grid_16">
                <div>

                    <% Html.EnableClientValidation(); %>
                    <% using (Html.BeginForm("UpdateUnit", "Unit", new { id = Model.DatasetID }))
                       { %>

                    <span class="display-label grid_5">Unit Name :</span> <span class="display-field grid_8">
                        <%: Html.EditorFor(model => model.UnitName,"TextTmpl", new { placeholder = "Unit Name" ,size = "38" })%>
                    </span>
                    <br />
                    <br />
                    <span class="display-label grid_5">Primary Product Name : </span><span class="display-field grid_8">
                        <%: Html.EditorFor(model => model.PrimaryProductName, "TextTmpl", new { placeholder = "Primary Product Name", size = "38" })%>
                    </span>
                    <br />
                    <br />
                    <span class="display-label grid_5">Process Category : </span><span class="display-field grid_8">
                        <select id="ProcessCategory" name="ProcessCategory" style="width: 235px">
                            <option value="-1">SELECT ONE</option>
                            <option value="Fuels">FUELS REFINERIES</option>
                            <option value="Chem">CHEMICALS </option>
                        </select>
                    </span>
                    <br />
                    <br />
                    <span class="display-label grid_5">Process Family : </span><span class="display-field grid_8">
                        <select id="ProcessFamily" name="ProcessFamily" style="width: 235px">
                            <option class="sub_Chem sub_Fuels" value="-1">SELECT ONE</option>
                            <option class="sub_Chem" value="COAT">Adhesives, Paints, and Coatings</option>
                            <option class="sub_Chem" value="Cl_ALK">Chlor-Alkali Products</option>
                            <option class="sub_Chem" value="Cl_HC">Chlorinated Hydrocarbons</option>
                            <option class="sub_Chem" value="C_PLAST">Commodity Thermoplastics</option>
                            <option class="sub_Chem" value="E_PLAST">Engineering Thermoplastics</option>
                            <option class="sub_Chem" value="FFF">Fibers, Films, and Fabrics</option>
                            <option class="sub_Chem" value="GLOX">Glycols and Oxides</option>
                            <option class="sub_Chem" value="IND_GAS">Industrial Gases</option>
                            <option class="sub_Chem" value="IORG">Inorganics</option>
                            <option class="sub_Chem" value="INT_ARM">Intermediates from Aromatics</option>
                            <option class="sub_Chem" value="INT_OLE">Intermediates from Olefins </option>
                            <option class="sub_Chem" value="OthChem">Other Chemical Process</option>
                            <option class="sub_Chem" value="AGR_PHAR"> Pharmaceuticals-Agricultural_Active_Ingredient</option>
                            <option class="sub_Chem" value="PPPE">Polypropylene and Polyethylene</option>
                            <option class="sub_Chem" value="PM_ARM">Primary Aromatics</option>
                            <option class="sub_Chem" value="PM_OLE">Primary Olefins</option>
                            <option class="sub_Chem" value="S_ORG">Solid Organics</option>
                            <option class="sub_Chem" value="THMSETS">Thermosets</option>
                            <option class="sub_Chem" value="Utilities">Utilities</option>
                            <%--<option class="sub_Chem" value="ELASM">Elastomers & Rubbers</option>
                            <option class="sub_Chem" value="LIQ_IORG">Liquid Inorganics</option>
                            <option class="sub_Chem" value="S_IORG">Solid Inorganics</option>
                            <option class="sub_Chem" value="PHARM_ACT">Pharmaceuticals- Active Ingredient</option>
                            <option class="sub_Chem" value="PHARM_FORM">Pharmaceuticals- Formulation</option>
                            <option class="sub_Chem" value="AGR_ACT">Agricultural- Active Ingredient</option>
                            <option class="sub_Chem" value="AGR_FORM">Agricultural- Formulation</option>--%>
                            
                            <option class="sub_Fuels" value="ALKY">Alkylation Unit</option>
                            <option class="sub_Fuels" value="CDU">Atmospheric Crude Distillation Unit</option>
                            <option class="sub_Fuels" value="REF">Catalytic Reforming Unit</option>
                            <option class="sub_Fuels" value="COK">Coking Unit</option>
                            <option class="sub_Fuels" value="VHYT">Cracking Feed or Vacuum Gas Oil Desulfurization</option>
                            <option class="sub_Fuels" value="DHYT">Distillate/Light Gas Oil Desulfurization and
                                Treating</option>
                            <option class="sub_Fuels" value="FCC">Fluid Catalytic Cracking Unit</option>
                            <option class="sub_Fuels" value="HYC">Hydrocracking Unit</option>
                            <option class="sub_Fuels" value="HYG">Hydrogen Generation Unit</option>
                            <option class="sub_Fuels" value="H2PURE">Hydrogen Purification Unit</option>
                            <option class="sub_Fuels" value="KHYT">Kerosene Desulfurization and Treating</option>
                            <option class="sub_Fuels" value="NHYT">Naphtha/Gasoline Desulfurization and Treating</option>
                            <option class="sub_Fuels" value="OTH">Other Refining Process Unit- please provide a
                                description</option>
                            <option class="sub_Fuels" value="RHYT">Residual Desulfurization</option>
                            <option class="sub_Fuels" value="SHYT">Selective Hydrotreating</option>
                            <option class="sub_Fuels" value="SRU">Sulfur Recovery Unit</option>
                            <option class="sub_Fuels" value="TRU">Tail Gas Recovery Unit</option>
                            <option class="sub_Fuels" value="VAC">Vacuum Crude Distillation Unit</option>
                            <option class="sub_Fuels" value="Utilities">Utilities</option>
                            <option class="sub_Fuels" value="OthRef">Other Refining Process</option>
                        </select>
                    </span>
                    <br />
                    <br />
                    <span class="display-label grid_5 genprod">Generic Product Name : </span><span class="display-field grid_8 genprod">
                        <select id="GenericProductName" name="GenericProductName" style="width: 235px">
                            <option class="sub_E_PLAST sub_COAT sub_PHARM_ACT sub_Utilities sub_PHARM_FORM sub_AGR_ACT sub_GLOX sub_PPPE sub_AGR_FORM sub_AGR_PHAR sub_PM_ARM sub_ELASM sub_INT_OLE sub_PM_OLE sub_C_PLAST sub_E_PLAST sub_INT_ARM sub_Cl_HC sub_Cl_ALK sub_IND_GAS sub_IORG sub_S_IORG sub_LIQ_IORG sub_FFF sub_COAT sub_THMSETS"
                                value="-1">SELECT ONE</option>
                            <option class="sub_E_PLAST" value="POM-H">Acetal Homopolymer or Copolymer</option>
                            <option class="sub_INT_OLE" value="Acetaldehyde">Acetaldehyde</option>
                            <option class="sub_INT_OLE" value="Acetate Esters">Acetate Esters</option>
                            <option class="sub_INT_OLE" value="Acetates">Acetates</option>
                            <option class="sub_INT_OLE" value="Acetic Acid">Acetic Acid</option>
                            <option class="sub_INT_OLE" value="Acetone">Acetone</option>
                            <option class="sub_PM_OLE" value="Acetylene">Acetylene</option>
                            <option class="sub_INT_OLE" value="Acrylates">Acrylates</option>
                            <option class="sub_C_PLAST" value="Acrylic-Plast">Acrylic</option>
                            <option class="sub_INT_OLE" value="AA">Acrylic Acid (AA)</option>
                            <option class="sub_INT_OLE" value="Acrolein">Acrylic Aldehyde (Acrolein)</option>
                            <option class="sub_INT_OLE" value="Acrylonitrile">Acrylonitrile (AN)</option>
                            <option class="sub_C_PLAST" value="ABS">Acrylonitrile butadiene styrene (ABS)</option>
                            <option class="sub_COAT" value="Adhesive">Adhesive/Glue</option>
                            <option class="sub_INT_ARM" value="ADA-ARM">Adipic Acid</option>
                            <option class="sub_INT_OLE" value="ADA-OLE">Adipic Acid</option>
                            <option class="sub_INT_ARM" value="ADN-ADA">Adiponitrile (ADN) via Adiptic Acid</option>
                            <option class="sub_INT_OLE" value="ADN-C3/C4">Adiponitrile (ADN) via C3/C4 Process</option>
                            <option class="sub_IND_GAS" value="Air">Air</option>
                            <option class="sub_IND_GAS" value="Air Gases">Air Gases (N2, O2)</option>
                            <option class="sub_INT_OLE" value="Aldehydes">Aldehydes</option>
                            <option class="sub_Cl_HC" value="Allyl Bromine">Allyl Bromine</option>
                            <option class="sub_Cl_HC" value="Allyl Chloride">Allyl Chloride</option>
                            <option class="sub_INT_ARM" value="Amines-ARM">Amines</option>
                            <option class="sub_INT_OLE" value="Amines-OLE">Amines</option>
                            <option class="sub_IND_GAS" value="NH3">Ammonia (NH3)</option>
                            <option class="sub_IORG" value="AN">Ammonium Nitrate</option>
                            <option class="sub_INT_ARM" value="Aniline">Aniline</option>
                            <option class="sub_THMSETS" value="Bakelite">Bakelite</option>
                            <option class="sub_PM_ARM" value="Benzene-Arm">Benzene</option>
                            <option class="sub_PM_OLE" value="Benzene-Ole">Benzene</option>
                            <option class="sub_PPPE" value="Biopolymers">Biopolymers (PLA, Polyethylene via Biomass)</option>
                            <option class="sub_PPPE" value="HDPE">High Density Polyethylene (HDPE)</option>
                            <option class="sub_PPPE" value="HIPS">High Impact Polystyrene (HIPS)</option>
                            <option class="sub_PPPE" value="LLDPE">Linear Low Density Polyethylene (LLDPE)</option>
                            <option class="sub_PPPE" value="LDPE">Low Density Polyethylene (LDPE)</option>
                            <option class="sub_PPPE" value="PE">Polyethylene (PE)</option>
                            <option class="sub_PPPE" value="PET">Polyethylene terephthalate (PET)</option>
                            <option class="sub_PPPE" value="PP">Polypropylene (PP)</option>
                            <option class="sub_INT_ARM" value="BPA">Bisphenol A (BPA)</option>
                            <option class="sub_IORG" value="Boric Acid">Boric Acid</option>
                            <option class="sub_PM_OLE" value="Butadiene">Butadiene</option>
                            <option class="sub_INT_OLE" value="BDO">Butane Diol (BDO)</option>
                            <option class="sub_PM_OLE" value="Butanes">Butanes</option>
                            <option class="sub_INT_OLE" value="BUOH">Butanol (BUOH)</option>
                            <option class="sub_PM_OLE" value="Butenes">Butenes</option>
                            <option class="sub_E_PLAST" value="PIB">Butyl Rubber (PIB)</option>
                            <option class="sub_IORG" value="CaCO3">Calcium Carbonate</option>
                            <option class="sub_INT_ARM" value="Caprolactam">Caprolactam</option>
                            <option class="sub_IORG" value="Carbon Black">Carbon Black (PBk7)</option>
                            <option class="sub_IND_GAS" value="CO2">Carbon Dioxide (CO2)</option>
                            <option class="sub_IND_GAS" value="CO">Carbon Monoxide (CO)</option>
                            <option class="sub_S_ORG" value="Paper">Cardboard/Paper</option>
                            <option class="sub_IORG" value="Catalyst">Catalyst (Zeolites)</option>
                            <option class="sub_COAT" value="Caulk">Caulk</option>
                            <option class="sub_IORG" value="Caustic">Caustic (NaOH; Lye) </option>
                            <option class="sub_Cl_ALK" value="Cell Effluent">Cell Effluent</option>
                            <option class="sub_FFF" value="CAF">Cellulose Acetate Films or Fibers</option>
                            <option class="sub_S_ORG" value="Cellulose Esters">Cellulose Esters</option>
                            <option class="sub_E_PLAST" value="Cellulosics">Cellulosics- Acetate, Butyrate, or Propionate</option>
                            <option class="sub_Cl_ALK" value="Chlor-Alkali by-prod">Chlor-Alkali by-products</option>
                            <option class="sub_Cl_ALK" value="Chlorine">Chlorine</option>
                            <option class="sub_Cl_HC" value="Chlorobenzene">Chlorobenzene</option>
                            <option class="sub_Cl_HC" value="Chloroethane">Chloroethane</option>
                            <option class="sub_Cl_HC" value="CFC">Chlorofluorocarbon (CFC)</option>
                            <option class="sub_Cl_HC" value="Chloromethane">Chloromethane</option>
                            <option class="sub_Cl_HC" value="Chloropropylene">Chloropropylene</option>
                            <option class="sub_INT_ARM" value="Cumene-IntArm">Cumene</option>
                            <option class="sub_PM_ARM" value="Cumene-PriArm">Cumene</option>
                            <option class="sub_THMSETS" value="Cyanate Esters">Cyanate Esters</option>
                            <option class="sub_INT_ARM" value="Cyclohexane">Cyclohexane</option>
                            <option class="sub_INT_ARM" value="Cyclohexanone">Cyclohexanone</option>
                            <option class="sub_INT_ARM" value="Cyclohexanone Oxime">Cyclohexanone Oxime</option>
                            <option class="sub_INT_ARM" value="DMC">Dimethyl Carbonate</option>
                            <option class="sub_INT_ARM" value="DMT">Dimethyl Terephthalate (DMT)</option>
                            <option class="sub_INT_ARM" value="DNT">Dinitrotoluene (DNT)</option>
                            <option class="sub_INT_ARM" value="DOP">Dioctyl Phthalate (DOP or DHEP)</option>
                            <option class="sub_INT_OLE" value="Diols">Diols</option>
                            <option class="sub_INT_ARM" value="DPC">Diphenyl Carbonate</option>
                            <option class="sub_E_PLAST" value="TPE-v">Elastomeric Alloys (TPE-v or TPV)</option>
                            <option class="sub_E_PLAST" value="ETPV">Engineering thermoplastic vulcanizates (ETPV)</option>
                            <option class="sub_THMSETS" value="Epoxy resin">Epoxy resin</option>
                            <option class="sub_INT_OLE" value="Esters">Esters</option>
                            <option class="sub_INT_OLE" value="Ethanol">Ethanol</option>
                            <option class="sub_INT_OLE" value="Ethers">Ethers</option>
                            <option class="sub_INT_OLE" value="Ethoxylated Products">Ethoxylated Products</option>
                            <option class="sub_INT_OLE" value="Ethoxylates">Ethoxylates</option>
                            <option class="sub_INT_OLE" value="EA">Ethyl Acetate (EA)</option>
                            <option class="sub_INT_ARM" value="EB">Ethylbenzene (EB)</option>
                            <option class="sub_PM_OLE" value="Ethylene">Ethylene</option>
                            <option class="sub_Cl_HC" value="EC">Ethylene Chloride</option>
                            <option class="sub_Cl_HC" value="EDC">Ethylene Dichloride (EDC)</option>
                            <option class="sub_E_PLAST" value="EPDM">Ethylene propylene diene monomer (EPDM)</option>
                            <option class="sub_E_PLAST" value="EP">Ethylene-Propylene Rubber (EP)</option>
                            <option class="sub_E_PLAST" value="EVA">Ethylene-vinyl acetate (EVA)</option>
                            <option class="sub_C_PLAST" value="EPS">Expandable Polystyrene (EPS)</option>
                            <option class="sub_FFF" value="Fabric">Fabric/ Yarn</option>
                            <option class="sub_FFF" value="Films">Films- Excruded, Blown, or Molded </option>
                            <option class="sub_E_PLAST" value="Fluoroelastomer">Fluoroelastomer (FKM and FPEM)</option>
                            <option class="sub_S_ORG" value="Food">Food</option>
                            <option class="sub_INT_OLE" value="Formaldehyde">Formaldehyde (Methanal)</option>
                            <option class="sub_INT_OLE" value="Glycerin">Glycerin (Glycerol)</option>
                            <option class="sub_INT_OLE" value="Glycol Ethers">Glycol Ethers</option>
                            <option class="sub_IND_GAS" value="Halogen Elements">Halogen Elements (Cl2, F2)</option>
                            <option class="sub_FFF" value="Housewrap">Housewrap (Tyvek®, WeathermateTM)</option>
                            <option class="sub_IND_GAS" value="HC Gases">Hydrocarbon Gases </option>
                            <option class="sub_IORG" value="HCl-LiqInorg">Hydrochloric Acid</option>
                            <option class="sub_IND_GAS" value="H2-IND">Hydrogen (H2)</option>
                            <option class="sub_Cl_ALK" value="H2-Electrolysis">Hydrogen via Electrolysis</option>
                            <option class="sub_PM_OLE" value="H2-Pyrolysis">Hydrogen via Pyrolysis</option>
                            <option class="sub_PM_ARM" value="H2-Reforming">Hydrogen via Reforming</option>
                            <option class="sub_IND_GAS" value="HCl-IND">Hydrogen Chloride (HCl)</option>
                            <option class="sub_INT_OLE" value="HCN">Hydrogen Cyanide (Formonitrile; Prussic Acid)</option>
                            <option class="sub_IND_GAS" value="H2S">Hydrogen Sulfide (H2S)</option>
                            <option class="sub_INT_ARM" value="Hydroquinone">Hydroquinone</option>
                            <option class="sub_IORG" value="HydroxylamineL">Hydroxylamine (Liquid)</option>
                            <option class="sub_IORG" value="HydroxylamineS">Hydroxylamine (Solid)</option>
                            <option class="sub_IORG" value="Iodine">Iodine</option>
                            <option class="sub_INT_OLE" value="Isobutylene">Isobutylene</option>
                            <option class="sub_INT_ARM" value="IP Acid">Isophthalic Acid</option>
                            <option class="sub_INT_OLE" value="Isoprene">Isoprene</option>
                            <option class="sub_INT_OLE" value="IPA">Isopropyl alcohol (IPA)</option>
                            <option class="sub_INT_OLE" value="Ketones">Ketones</option>
                            <option class="sub_FFF" value="Latex">Latex</option>
                            <option class="sub_COAT" value="Latex-Emulsion">Latex (emulsion)</option>
                            <option class="sub_IORG" value="NH3-Liq">Liquid Ammonia</option>
                            <option class="sub_E_PLAST" value="LCP">Liquid Crystal Polymers</option>
                            <option class="sub_IORG" value="SO2-Liq">Liquid Sulfur Dioxide</option>
                            <option class="sub_INT_OLE" value="MA">Maleic Anhydride (MA)</option>
                            <option class="sub_THMSETS" value="MMC">Melamine Molding Compound</option>
                            <option class="sub_IORG" value="Metals">Metals and Alloys</option>
                            <option class="sub_PM_OLE" value="Methane">Methane</option>
                            <option class="sub_INT_OLE" value="MEOH">Methanol (MEOH)</option>
                            <option class="sub_INT_OLE" value="MEK">Methyl ethyl ketone (MEK)</option>
                            <option class="sub_INT_OLE" value="MIBK">Methyl isobutyl ketone (MIBK)</option>
                            <option class="sub_INT_OLE" value="MTBE">Methyl Tertiary Butyl Ether (MTBE)</option>
                            <option class="sub_INT_ARM" value="MDI">Methylene Diphenyl Diisocyanate (MDI)</option>
                            <option class="sub_PM_OLE" value="Mixed C4s">Mixed C4s</option>
                            <option class="sub_PM_OLE" value="Mixed C5s">Mixed C5s</option>
                            <option class="sub_E_PLAST" value="CR">Neoprene polychloroprene</option>
                            <option class="sub_IORG" value="Nitric Acid">Nitric Acid</option>
                            <option class="sub_E_PLAST" value="NBR">Nitrile Butadiene Rubber (NBR)</option>
                            <option class="sub_INT_ARM" value="Nitrotoluene">Nitrotoluene (or Methylnitrobenzene)</option>
                            <option class="sub_IND_GAS" value="N2O">Nitrous Oxide (N2O)</option>
                            <option class="sub_IND_GAS" value="Noble Gases">Noble Gases (He, Ar, Kr, Ne, Xe)</option>
                            <option class="sub_FFF" value="Non-wovens">Non-wovens</option>
                            <option class="sub_E_PLAST" value="Nylon 6">Nylon 6, Nylon 6/6</option>
                            <option class="sub_INT_ARM" value="Nylon6FromArom">Nylon 6</option>
                            <option class="sub_INT_OLE" value="Organic Acids">Organic Acids</option>
                            <option class="sub_INT_OLE" value="Other Alcohols">Other Alcohols</option>
                            <option class="sub_INT_ARM" value="Other AA">Other Aromatic Amines</option>
                            <option class="sub_E_PLAST" value="Other FP">Other Fluoropolymers (ECTFE, PVDF)</option>
                            <option class="sub_COAT" value="Paint   ">Paint </option>
                            <option class="sub_S_ORG" value="Paraffin Wax">Paraffin Wax</option>
                            <option class="sub_AGR_PHAR" value="Pharm-Act">Pharmaceuticals–Active Ingredient</option>
                            <option class="sub_AGR_PHAR" value="Pharm-Form">Pharmaceuticals–Formulation</option>
                            <option class="sub_AGR_PHAR" value="Pesticides-Act">Pesticides (herbicides, insecticides, fungicides)–Active Ingredient</option>
                            <option class="sub_AGR_PHAR" value="Pesticides-Form">Pesticides (herbicides, insecticides, fungicides)–Formulation</option>
                            <option class="sub_AGR_PHAR" value="SynFertilizer-Act">Synthetic fertilizers, harmones, and other growth agents–Active Ingredient</option>
                            <option class="sub_AGR_PHAR" value="SynFertilizer-Form">Synthetic fertilizers, harmones,  and other growth agents–Formulation</option>
                            <option class="sub_PHARM_ACT" value="Pharm-Act">Pharmaceuticals</option>
                            <option class="sub_AGR_FORM" value="Pesticides-Form">Pesticides (herbicides, insecticides, fungicides)</option>
                            <option class="sub_PHARM_FORM" value="Pharm-Form">Pharmaceuticals</option>
                            <option class="sub_INT_ARM" value="Phenol">Phenol</option>
                            <option class="sub_INT_ARM" value="Phenol-Acetone">Phenol-Acetone</option>
                            <option class="sub_THMSETS" value="Phenolic">Phenolic</option>
                            <option class="sub_IORG" value="Phosphates">Phosphates</option>
                            <option class="sub_INT_ARM" value="Phthalic Anhydride">Phthalic Anhydride</option>
                            <option class="sub_INT_OLE" value="Piperylene">Piperylene</option>
                            <option class="sub_E_PLAST" value="Polyamide">Polyamide</option>
                            <option class="sub_E_PLAST" value="Polyarylate">Polyarylate</option>
                            <option class="sub_E_PLAST" value="Polybutadiene">Polybutadiene (BR; Butadiene Rubber)</option>
                            <option class="sub_E_PLAST" value="PBT">Polybutylene Terephthalate (PBT)</option>
                            <option class="sub_C_PLAST" value="PCL">Polycaprolactone (PCL)</option>
                            <option class="sub_E_PLAST" value="PC">Polycarbonate (PC)</option>
                            <option class="sub_E_PLAST" value="Polyester-Plast">Polyester</option>
                            <option class="sub_THMSETS" value="Polyester-Unsat">Polyester Unsaturated</option>
                            <option class="sub_E_PLAST" value="PEEK">Polyetherketone (PEEK)</option>
                            <option class="sub_INT_OLE" value="Polyethers">Polyethers</option>
                            <option class="sub_E_PLAST" value="PET Composite">Polyetheylene terephthalate (PET)
                                Composite</option>
                            <option class="sub_E_PLAST" value="Polyimide">Polyimide/ Polyetherimide</option>
                            <option class="sub_C_PLAST" value="PMMA">Polymethyl methacrylate (PMMA)</option>
                            <option class="sub_E_PLAST" value="PMP">Polymethylpentene (PMP, TPX)</option>
                            <option class="sub_INT_OLE" value="Polyol">Polyol</option>
                            <option class="sub_E_PLAST" value="Polyolefin Blends">Polyolefin Blends</option>
                            <option class="sub_E_PLAST" value="PPE">Polyphenyl Ether (PPE) </option>
                            <option class="sub_E_PLAST" value="PPO">Polyphenylene Oxide (PPO)</option>
                            <option class="sub_E_PLAST" value="PPS">Polyphenylene Sulfide (PPS)</option>
                            <option class="sub_E_PLAST" value="PPA">Polyphthalamide (PPA)</option>
                            <option class="sub_C_PLAST" value="PS">Polystyrene (PS)</option>
                            <option class="sub_E_PLAST" value="PSU">Polysulfone (PSU)</option>
                            <option class="sub_E_PLAST" value="PTFE">Polytetrafluoroethylene (PTFE)/ Teflon®</option>
                            <option class="sub_E_PLAST" value="PTT">Polytrimethylene terephthalate (PTT)</option>
                            <option class="sub_E_PLAST" value="PUR-Plast">Polyurethane (PU)</option>
                            <option class="sub_THMSETS" value="PUR Isocyanates">Polyurethane Isocyanates</option>
                            <option class="sub_E_PLAST" value="PUR-Elasm">Polyurethanes</option>
                            <option class="sub_C_PLAST" value="PVA">Polyvinyl Alcohol (PVA; PVOH)</option>
                            <option class="sub_C_PLAST" value="PVC">Polyvinyl Chloride (PVC)</option>
                            <option class="sub_IORG" value="KNO3">Potasium Nitrate</option>
                            <option class="sub_INT_OLE" value="PROH">Propanol (PROH)</option>
                            <option class="sub_PM_OLE" value="Propylene">Propylene</option>
                            <option class="sub_GLOX" value="EG">Ethylene Glycol</option>
                            <option class="sub_GLOX" value="EO">Ethylene Oxide (EO)</option>
                            <option class="sub_GLOX" value="PG">Propylene Glycol (PG)</option>
                            <option class="sub_GLOX" value="PO">Propylene Oxide (PO)</option>
                            <option class="sub_PM_OLE" value="PyGas">Pyrolysis Gasoline</option>
                            <option class="sub_PM_OLE" value="PyNap">Pyrolysis Naphtha/Gas Oil</option>
                            <option class="sub_INT_OLE" value="Raffinate">Raffinate</option>
                            <option class="sub_INT_OLE" value="Recovered C4/C5">Recovered C4s or C5s</option>
                            <option class="sub_E_PLAST" value="Rubber">Rubber (natural or synthetic polyisoprene)</option>
                            <option class="sub_IORG" value="Salt">Salt (NaCl)</option>
                            <option class="sub_COAT" value="Sealant">Sealant</option>
                            <option class="sub_E_PLAST" value="Si Rubber">Silicone/Fluorosilicone Rubber</option>
                            <option class="sub_IORG" value="Solid Explosives">Solid Explosives</option>
                            <option class="sub_INT_ARM" value="Styrene">Styrene</option>
                            <option class="sub_E_PLAST" value="SMA">Styrene maleic anhydride (SMA)</option>
                            <option class="sub_E_PLAST" value="SAN">Styrene-acrylonitrile resin (SAN)</option>
                            <option class="sub_E_PLAST" value="SBR">Styrene-Butadiene Rubber (SBR)</option>
                            <option class="sub_E_PLAST" value="SBS">Styrenic Block Copolymers (SBS, SIS)</option>
                            <option class="sub_IORG" value="Sulfur">Sulfur</option>
                            <option class="sub_IORG" value="Sulfuric Acid">Sulfuric Acid</option>
                            <option class="sub_IORG" value="Sulfuryl Chloride">Sulfuryl Chloride</option>
                            <option class="sub_C_PLAST" value="SAP">Superabsorbent Polymers </option>
                            <option class="sub_COAT" value="Surfactant">Surfactant/Detergent</option>
                            <option class="sub_AGR_ACT" value="SynFertilizer-Act">Synthetic fertilizers, harmones,
                                and other growth agents</option>
                            <option class="sub_AGR_FORM" value="SynFertilizer-Form">Synthetic fertilizers, harmones,
                                and other growth agents</option>
                            <option class="sub_Utilities" value="Electrical-Gen">Electrical Generation</option>
                            <option class="sub_Utilities" value="Electrical-Gen-Cogen">Electrical Generation - Cogeneration</option>
                            <option class="sub_Utilities" value="Steam-Boilers">Steam Boilers</option>
                            <option class="sub_Utilities" value="Cooling-Water">Cooling Water</option>
                            <option class="sub_Utilities" value="Waste-Water-Treatment">Waste Water Treatment</option>
                            <option class="sub_Utilities" value="Others-Combinations">Others (Combinations)</option>
                            <option class="sub_FFF" value="Acrylic-Fiber">Synthetic Fibers- Acrylic</option>
                            <option class="sub_FFF" value="Nylon">Synthetic Fibers- Nylon</option>
                            <option class="sub_FFF" value="Polyester-Fiber">Synthetic Fibers- Polyester</option>
                            <option class="sub_FFF" value="Polyolefin">Synthetic Fibers- Polyolefin</option>
                            <option class="sub_INT_ARM" value="TPA">Terephthalic Acid (TPA)</option>
                            <option class="sub_INT_OLE" value="TBA">Tertiary Butyl Ether (TBA)</option>
                            <option class="sub_Cl_HC" value="TCE">Tetrachloroethylene</option>
                            <option class="sub_E_PLAST" value="TPCP">Thermoplastic copolyester</option>
                            <option class="sub_E_PLAST" value="TPPA">Thermoplastic polyamides</option>
                            <option class="sub_IORG" value="TiO2">Titanium Dioxide (TiO2)</option>
                            <option class="sub_PM_ARM" value="Toluene-ARM">Toluene</option>
                            <option class="sub_PM_OLE" value="Toluene-OLE">Toluene</option>
                            <option class="sub_INT_ARM" value="TDI">Toluene Diisocyanate (TDI)</option>
                            <option class="sub_Cl_HC" value="Chloroform">Trichloromethane (Chloroform)</option>
                            <option class="sub_INT_ARM" value="TNT">Trinitrotoluene (TNT)</option>
                            <option class="sub_E_PLAST" tip="" value="UHMW PE">Ultra-high-molecular-weight Polyethylene
                                (UHMW PE; HPPE)</option>
                            <option class="sub_S_ORG" value="Urea">Urea (or Carbamide)</option>
                            <option class="sub_THMSETS" value="Urea Molding">Urea Molding Compound</option>
                            <option class="sub_COAT" value="Varnish">Varnish</option>
                            <option class="sub_INT_OLE" value="VA">Vinyl Acetate (VA)</option>
                            <option class="sub_INT_OLE" value="VAM">Vinyl Acetate Monomer (VAM)</option>
                            <option class="sub_Cl_HC" value="VCM">Vinyl Chloride Monomer (VCM)</option>
                            <option class="sub_THMSETS" value="Vinyl Ester">Vinyl Ester</option>
                            <option class="sub_E_PLAST" value="Vulc Rubber">Vulcanized Rubber </option>
                            <option class="sub_PM_ARM" value="Xylenes-ARM">Xylenes</option>
                            <option class="sub_PM_OLE" value="Xylenes-OLE">Xylenes</option>
                        </select>
                    </span>
                    <div id="OthBox">
                        <br />
                        <span class="display-label grid_5">Please provide a description of the process :
                        </span><span class="display-field grid_8">
                            <textarea id="OtherGenericProductName" name="OtherGenericProductName" cols="38" rows="4"></textarea>
                        </span>
                    </div>
                    <%: Html.HiddenFor(model=>model.CompanySID) %>
                    <%: Html.HiddenFor(model=>model.SiteDatasetID) %>
                    <%: Html.HiddenFor(model=>model.DatasetID) %>
                    <div class="grid_16">
                        <p>
                            &nbsp;
                        </p>
                        <div style="border: 1px wheat solid; background-color: cornsilk; font-size: .85em; border: 1px #888 solid;">
                            <p align="justify">
                                <img src="<%: Url.Content("~/images/info.png")%>" align="top" style="margin: 0 5px; display: inline-block;" />
                                <span style="position: relative; display: inline-block; width: 545px; text-align: justify;">If your chemical product is not in the list above, please send a description of
                                    the process (e.g. MSDS sheet or equivalent) including the primary raw materials
                                    and products to <a href="mailto:RAM@SolomonInsight.com">RAM@SolomonInsight.com</a></span>
                            </p>
                        </div>
                    </div>


                    <div class="grid_8">
                        <p>
                            &nbsp;
                        </p>
                        <input type="submit" name="Update" title="Update" value="Update" onclick="this.value = 'Updating..'" />
                    </div>

                    <% } 
                           
                           
                    %>

                    <script src="<%= Url.Content("~/Scripts/Sublist.js")%>" type="text/javascript"></script>
                    <script src="<%= Page.ResolveUrl("~/Scripts/colorscale.min.js")%>" type="text/javascript"></script>
                    <script type="text/javascript">

                        var addressFormatting = function (text) {
                            var newText = text;
                            //array of find replaces
                            var findreps = [
            { find: /^([^\-]+) \- /g, rep: '<span class="ui-selectmenu-item-header">$1</span>' },
            { find: /([^\|><]+) \| /g, rep: '<span class="ui-selectmenu-item-content">$1</span>' },
            { find: /([^\|><\(\)]+) (\()/g, rep: '<span class="ui-selectmenu-item-content">$1</span>$2' },
            { find: /([^\|><\(\)]+)$/g, rep: '<span class="ui-selectmenu-item-content">$1</span>' },
            { find: /(\([^\|><]+\))$/g, rep: '<span class="ui-selectmenu-item-footer">$1</span>' }
                            ];

                            for (var i in findreps) {
                                newText = newText.replace(findreps[i].find, findreps[i].rep);
                            }
                            return newText;
                        }

                        $(document).ready(function () {


                            var proctype = $("#ProcessFamily option[value='<%: Model.ProcessFamily %>']").attr("class");
                            $(".genprod").hide();
                            $("#OthBox").hide();

                            $(this).delegate("#ProcessCategory", "change", function () {

                                if ($("#ProcessCategory ").val() == "Chem") {
                                    $(".genprod").show();
                                }
                                else {
                                    $(".genprod").hide();
                                }
                            }).delegate("#ProcessFamily", "change", function () {
                                if (($(this).val() == "OthChem") || ($(this).val() == "OthRef")) {
                                    $("#OthBox").show();
                                    $(".genprod").hide();
                                }
                                else {
                                    $("#OthBox").hide();
                                    if ($("#ProcessCategory ").val() == "Chem")
                                        $(".genprod").show();
                                }
                            });

                            makeSublist('ProcessCategory', 'ProcessFamily', false, '<%: Model.ProcessFamily %>');
                            makeSublist('ProcessFamily', 'GenericProductName', false, '');
                            // HighlightListTooltip();


                            if (proctype) {
                                $("#ProcessCategory").val(proctype.split(' ')[0].replace("sub_", ""));
                                $("#ProcessCategory").trigger("change");


                                $("#ProcessCategory ").val(proctype.replace("sub_", ""));
                                $("#ProcessFamily").val("<%: Model.ProcessFamily %>");
                                $("#ProcessFamily").trigger("change");
                                $("#GenericProductName").val("<%: Model.GenericProductName%>");
                                $("#OtherGenericProductName").val("<%: Model.OtherGenericProductName%>");
                            }


                            changeColor($('.innerbox'), $('.score').text());
                        });





                    </script>

                </div>

            </div>
            <%--
            <img src="<%: Url.Content("~/images/arrow-return-180.png")%>" border="0" alt="return"
                align="middle" />
            &nbsp;
            <%: Html.ActionLink("Return To Site", "SiteDetail","Site", new { id = Model.SiteDatasetID },new {@style="font-size:.90em;"})%>
            --%>
        </div>
        <div id="alertmsg">
        </div>
        <script type="text/javascript">
            function JsonDeleteUnit_OnComplete(response) {
                ;
                window.location.href = '<%: Url.RouteUrl(new {action="SiteDetail",controller="Site",id=Model.SiteDatasetID}) %>';
            }
        </script>
    </fieldset>
</asp:Content>
