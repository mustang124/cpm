﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UT_COSTModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:05:58  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_UT_COST").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("UT_COST", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_UT_COST" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Routine (Non-T/A) Maintenance Craft Hours</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Annualized T/A Maintenance Craft Hours</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Routine (Non-T/A) Maintenance Material Cost</small> </span></a></li>
            <li><a href='#step-4'>
                <label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br />
                    <small>Annualized T/A Maintenance Material Cost</small> </span></a></li>
            <li><a href='#step-5'>
                <label class='stepNumber'>5</label><span class='stepDesc'>Section 5<br />
                    <small>Additional Non-RAM Maintenance Hours and Cost</small> </span></a></li>
                        <li><a href='#step-6'>
                <label class='stepNumber'>6</label><span class='stepDesc'>Section 6<br />
                    <small>Turnaround Work Execution</small> </span></a></li>
            <li><a href='#step-7'>
                <label class='stepNumber'>7</label><span class='stepDesc'>Section 7<br />
                    <small>Routine Work Execution</small> </span></a></li>

        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">Routine (Non-T/A) Maintenance <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Hours</p>
            <p><i>Provide a breakdown of the total direct <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> maintenance hours (company and contractor) by each equipment category.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_CST_RT_CR_HR' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
<%--                        <li class='gridrowtext' qid='QSTID_UCST_110.1'><span class='numbering'>1.</span> <a href="#" tooltip="Total Maintenance Hours including all types of labor hours charged to maintenance.">Total Maintenance</a> Hours</li>--%>
                        <li class='gridrowtext' qid='QSTID_UCST_100.1'><span class='numbering'>1.</span> <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance.  Excludes all maintenance capital work and all maintenance work executed during a plant or unit turnaround.">Routine Maintenance Expense</a> Work Hours</li>
                        <li class='gridrowtext' qid='QSTID_UCST_100.2'><span class='numbering'>2.</span> <a href="#" tooltip="All Maintenance Capital work performed during normal run and maintain periods.  Excludes all maintenance capital work executed during a plant or unit turnaround.  Maintenance Capital:  Expenditures to replace equipment considered to be at the end of its useful life, without change of function (like for like replacement) or expenditure aimed at increasing the intrinsic reliability of the asset, again without changing its function. See also Solomon guide, illustration, and decision tree on Maintenance Capital.  See also Targeted Spending.">Routine Maintenance Capital</a> Hours</li>
                        <li class='gridrowtext calctext' qid='QSTID_UCST_100.3'>Total <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Labor Hours</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_110.1" /><%: Html.EditorFor(model => model.RoutCraftmanTltHrsRE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftTltHr sumRTTotHr sumRTTotHrA" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.1" /><%: Html.EditorFor(model => model.RoutCraftmanWhrRE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftHr sumRTTotHr sumRTTotHrA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.2" /><%: Html.EditorFor(model => model.RoutCraftmanMtCapWhrRE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTMtCapCftHr sumRTTotHr sumRTTotHrA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.3" /><%: Html.EditorFor(model => model.RoutCraftmanTotWhrRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
<%--                         <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_110.2" /><%: Html.EditorFor(model => model.RoutCraftmanTltHrsFP,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftTltHr sumRTTotHr sumRTTotHrB" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.4" /><%: Html.EditorFor(model => model.RoutCraftmanWhrFP,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftHr sumRTTotHr sumRTTotHrB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.5" /><%: Html.EditorFor(model => model.RoutCraftmanMtCapWhrFP,"DoubleTmpl",new {size="10" ,CalcTag="sumRTMtCapCftHr sumRTTotHr sumRTTotHrB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.6" /><%: Html.EditorFor(model => model.RoutCraftmanTotWhrFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_110.3" /><%: Html.EditorFor(model => model.RoutCraftmanTltHrsIE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftTltHr sumRTTotHr sumRTTotHrC" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.7" /><%: Html.EditorFor(model => model.RoutCraftmanWhrIE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftHr sumRTTotHr sumRTTotHrC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.8" /><%: Html.EditorFor(model => model.RoutCraftmanMtCapWhrIE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTMtCapCftHr sumRTTotHr sumRTTotHrC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_100.9" /><%: Html.EditorFor(model => model.RoutCraftmanTotWhrIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_110.4" /><%: Html.EditorFor(model => model.RoutCraftmanTltHrsTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_101" /><%: Html.EditorFor(model => model.RoutCraftmanWhrTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_101.1" /><%: Html.EditorFor(model => model.RoutCraftmanMtCapWhrTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_101.2" /><%: Html.EditorFor(model => model.RoutCraftmanTotWhrTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanWhrRE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanMtCapWhrRE)%>
                 <%: Html.ValidationMessageFor(model => model.RoutCraftmanTotWhrRE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanWhrFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanMtCapWhrFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanTotWhrFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanWhrIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanMtCapWhrIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanTotWhrIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanWhrTOT)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanMtCapWhrTOT)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanTotWhrTOT)%>

                <%: Html.ValidationMessageFor(model => model.RoutCraftmanTltHrsRE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanTltHrsFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanTltHrsIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutCraftmanTltHrsTOT)%>



                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle"><a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5 years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Maintenance <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Hours</p>
            <p><i>Provide a breakdown of the total direct <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> maintenance hours (company and contractor) by each equipment category.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_CST_AnnTA_CR_HR' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
<%--                        <li class='gridrowtext' qid='QSTID_UCST_130.1'><span class='numbering'>1.</span> <a href="#" tooltip="Total Maintenance Hours including all types of labor hours charged to maintenance.">Total Maintenance</a> Hours</li>--%>
                        <li class='gridrowtext' qid='QSTID_UCST_120.1'><span class='numbering'>1.</span> <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during turnaround periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance.  Excludes all maintenance capital work executed during a unit turnaround.">Turnaround Maintenance Expense</a> Work Hours</li>
                        <li class='gridrowtext' qid='QSTID_UCST_120.2'><span class='numbering'>2.</span> <a href="#" tooltip="Maintenance Capital work performed through the execution of a maintenance turnaround.">Turnaround Maintenance Capital</a> Hours</li>
                        <li class='gridrowtext calctext' qid='QSTID_UCST_120.3'>Total <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Labor Hours</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_130.1" /><%: Html.EditorFor(model => model.TACraftmanWhrTltHrsRE, "DoubleTmpl", new {size="10" ,CalcTag="sumTACftTltHr sumTATotHr sumTATotHrA" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.1" /><%: Html.EditorFor(model => model.TACraftmanWhrRE,"DoubleTmpl",new {size="10" ,CalcTag="sumTACftHr sumTATotHr sumTATotHrA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.2" /><%: Html.EditorFor(model => model.TACraftmanMtCapWhrRE,"DoubleTmpl",new {size="10" ,CalcTag="sumTAMtCapCftHr sumTATotHr sumTATotHrA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.3" /><%: Html.EditorFor(model => model.TACraftmanTotWhrRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_130.2" /><%: Html.EditorFor(model => model.TACraftmanWhrTltHrsFP,"DoubleTmpl",new {size="10" ,CalcTag="sumTACftTltHr sumTATotHr sumTATotHrB" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.4" /><%: Html.EditorFor(model => model.TACraftmanWhrFP,"DoubleTmpl",new {size="10" ,CalcTag="sumTACftHr sumTATotHr sumTATotHrB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.5" /><%: Html.EditorFor(model => model.TACraftmanMtCapWhrFP,"DoubleTmpl",new {size="10" ,CalcTag="sumTAMtCapCftHr sumTATotHr sumTATotHrB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.6" /><%: Html.EditorFor(model => model.TACraftmanTotWhrFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_130.3" /><%: Html.EditorFor(model => model.TACraftmanWhrTltHrsIE,"DoubleTmpl",new {size="10" ,CalcTag="sumTACftTltHr sumTATotHr sumTATotHrC" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.7" /><%: Html.EditorFor(model => model.TACraftmanWhrIE,"DoubleTmpl",new {size="10" ,CalcTag="sumTACftHr sumTATotHr sumTATotHrC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.8" /><%: Html.EditorFor(model => model.TACraftmanMtCapWhrIE,"DoubleTmpl",new {size="10" ,CalcTag="sumTAMtCapCftHr sumTATotHr sumTATotHrC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_120.9" /><%: Html.EditorFor(model => model.TACraftmanTotWhrIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_130.4" /><%: Html.EditorFor(model => model.TACraftmanWhrTltHrsTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_121" /><%: Html.EditorFor(model => model.TACraftmanWhrTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_121.1" /><%: Html.EditorFor(model => model.TACraftmanMtCapWhrTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_121.2" /><%: Html.EditorFor(model => model.TACraftmanTotWhrTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrRE)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanMtCapWhrRE)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanTotWhrRE)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrFP)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanMtCapWhrFP)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanTotWhrFP)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrIE)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanMtCapWhrIE)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanTotWhrIE)%>
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrTOT)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanMtCapWhrTOT)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanTotWhrTOT)%>
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrTltHrsRE)%>
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrTltHrsFP)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrTltHrsIE)%> 
                <%: Html.ValidationMessageFor(model => model.TACraftmanWhrTltHrsTOT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle">Routine (Non-T/A) <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Material</a> Cost</p>
            <p><i>Provide a breakdown of the total <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">routine maintenance</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">material </a>(company and contractor) by each equipment category in local currency.</i></p>



            <div class="questionblock" qid="">


                <div id='UT_CST_RT_MTL_CST' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
<%--                        <li class='gridrowtext' qid='QSTID_UCST_142.1'><span class='numbering'>1.</span> <a href="#" tooltip="Total Maintenance Materials including all types of materials charged to maintenance.">Total Maintenance</a> Materials</li>--%>
                        <li class='gridrowtext' qid='QSTID_UCST_140.1'><span class='numbering'>1.</span> <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Material </a>Expense</li>
                        <li class='gridrowtext' qid='QSTID_UCST_140.2'><span class='numbering'>2.</span> <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Capital </a>Materials</li>
                        <li class='gridrowtext calctext' qid='QSTID_UCST_140.3'>Total <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Material </a>Cost</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_142.1" /><%: Html.EditorFor(model => model.RoutMatlTltHrsRE, "DoubleTmpl", new {size="10" ,CalcTag="sumMatlTltHr sumRTTotCst sumRTTotCstA" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.1" /><%: Html.EditorFor(model => model.RoutMatlRE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftCst sumRTTotCst sumRTTotCstA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.2" /><%: Html.EditorFor(model => model.RoutMatlMtCapRE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTMtCapCftCst sumRTTotCst sumRTTotCstA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.3" /><%: Html.EditorFor(model => model.RoutMatlTotRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_142.2" /><%: Html.EditorFor(model => model.RoutMatlTltHrsFP,"DoubleTmpl",new {size="10" ,CalcTag="sumMatlTltHr sumRTTotCst sumRTTotCstB" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.4" /><%: Html.EditorFor(model => model.RoutMatlFP,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftCst sumRTTotCst sumRTTotCstB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.5" /><%: Html.EditorFor(model => model.RoutMatlMtCapFP,"DoubleTmpl",new {size="10" ,CalcTag="sumRTMtCapCftCst sumRTTotCst sumRTTotCstB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.6" /><%: Html.EditorFor(model => model.RoutMatlTotFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_142.3" /><%: Html.EditorFor(model => model.RoutMatlTltHrsIE,"DoubleTmpl",new {size="10" ,CalcTag="sumMatlTltHr sumRTTotCst sumRTTotCstC" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.7" /><%: Html.EditorFor(model => model.RoutMatlIE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCftCst sumRTTotCst sumRTTotCstC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.8" /><%: Html.EditorFor(model => model.RoutMatlMtCapIE,"DoubleTmpl",new {size="10" ,CalcTag="sumRTMtCapCftCst sumRTTotCst sumRTTotCstC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_140.9" /><%: Html.EditorFor(model => model.RoutMatlTotIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_142.4" /><%: Html.EditorFor(model => model.RoutMatlTltHrsTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_141" /><%: Html.EditorFor(model => model.RoutMatlTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_141.1" /><%: Html.EditorFor(model => model.RoutMatlMtCapTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_141.2" /><%: Html.EditorFor(model => model.RoutMatlTotTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.RoutMatlRE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlMtCapRE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTotRE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlMtCapFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTotFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlMtCapIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTotIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTOT)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlMtCapTOT)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTotTOT)%>

                <%: Html.ValidationMessageFor(model => model.RoutMatlTltHrsRE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTltHrsFP)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTltHrsIE)%> 
                <%: Html.ValidationMessageFor(model => model.RoutMatlTltHrsTOT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-4">

            <p class="StepTitle"><a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5 years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Material</a> Cost</p>
            <p><i>Provide a breakdown of the total <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">maintenance material</a> (company and contractor) by each equipment category in local currency.</i></p>



            <div class="questionblock" qid="">

                <div id='UT_CST_AnnTA_MTL_CST' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
<%--                        <li class='gridrowtext' qid='QSTID_UCST_152.1'><span class='numbering'>1.</span> <a href="#" tooltip="Total Maintenance Materials including all types of materials charged to maintenance.">Total Maintenance</a> Materials</li>--%>
                        <li class='gridrowtext' qid='QSTID_UCST_150.1'><span class='numbering'>1.</span> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Material</a> Expense</li>
                        <li class='gridrowtext' qid='QSTID_UCST_150.2'><span class='numbering'>2.</span> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Maintenance Capital</a> Materials</li>
                        <li class='gridrowtext calctext' qid='QSTID_UCST_150.3'>Total <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5 years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Material</a> Cost</li>
<%--                        <li class='gridrowtext' qid='QSTID_UCST_153.1'><span class='numbering'>3.</span> Total Unit Maintenance Cost for Routine Work (includes cleaning, etc)</li>
                        <li class='gridrowtext' qid='QSTID_UCST_153.5'><span class='numbering'>4.</span> Total Annual  Unit Maintenance Costs for Turnarounds (includes cleaning, etc)</li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_152.1" /><%: Html.EditorFor(model => model.AnnTAMatlTltHrsRE, "DoubleTmpl", new {size="10" ,CalcTag="sumAnnTATltHr sumAnnTATotCst sumAnnTATotCstA" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.1" /><%: Html.EditorFor(model => model.AnnTAMatlRE,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTACftCst sumAnnTATotCst sumAnnTATotCstA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.2" /><%: Html.EditorFor(model => model.AnnTAMatlMtCapRE,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTAMtCapCftCst sumAnnTATotCst sumAnnTATotCstA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.3" /><%: Html.EditorFor(model => model.AnnTAMatlTotRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.1" /><%: Html.EditorFor(model => model.UnitMaintCostRE,"DoubleTmpl",new {size="10" ,CalcTag="sumUnitCost",NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.5" /><%: Html.EditorFor(model => model.UnitTAMaintCostRE,"DoubleTmpl",new {size="10" ,CalcTag="sumTACost",NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_152.2" /><%: Html.EditorFor(model => model.AnnTAMatlTltHrsFP,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTATltHr sumAnnTATotCst sumAnnTATotCstB" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.4" /><%: Html.EditorFor(model => model.AnnTAMatlFP,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTACftCst sumAnnTATotCst sumAnnTATotCstB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.5" /><%: Html.EditorFor(model => model.AnnTAMatlMtCapFP,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTAMtCapCftCst sumAnnTATotCst sumAnnTATotCstB" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.6" /><%: Html.EditorFor(model => model.AnnTAMatlTotFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.2" /><%: Html.EditorFor(model => model.UnitMaintCostFP,"DoubleTmpl",new {size="10" ,CalcTag="sumUnitCost",NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.6" /><%: Html.EditorFor(model => model.UnitTAMaintCostFP,"DoubleTmpl",new {size="10" ,CalcTag="sumTACost",NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_152.3" /><%: Html.EditorFor(model => model.AnnTAMatlTltHrsIE,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTATltHr sumAnnTATotCst sumAnnTATotCstC" ,NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.7" /><%: Html.EditorFor(model => model.AnnTAMatlIE,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTACftCst sumAnnTATotCst sumAnnTATotCstC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.8" /><%: Html.EditorFor(model => model.AnnTAMatlMtCapIE,"DoubleTmpl",new {size="10" ,CalcTag="sumAnnTAMtCapCftCst sumAnnTATotCst sumAnnTATotCstC" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_150.9" /><%: Html.EditorFor(model => model.AnnTAMatlTotIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.3" /><%: Html.EditorFor(model => model.UnitMaintCostIE,"DoubleTmpl",new {size="10" ,CalcTag="sumUnitCost",NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.7" /><%: Html.EditorFor(model => model.UnitTAMaintCostIE,"DoubleTmpl",new {size="10" ,CalcTag="sumTACost",NumFormat="Number" ,DecPlaces="0"})%></li>--%>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_152.4" /><%: Html.EditorFor(model => model.AnnTAMatlTltHrsTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_151" /><%: Html.EditorFor(model => model.AnnTAMatlTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_151.1" /><%: Html.EditorFor(model => model.AnnTAMatlMtCapTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_151.2" /><%: Html.EditorFor(model => model.AnnTAMatlTotTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.4" /><%: Html.EditorFor(model => model.UnitMaintCostTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_153.8" /><%: Html.EditorFor(model => model.UnitTAMaintCostTOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>--%>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlRE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlMtCapRE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTotRE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlFP)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlMtCapFP)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTotFP)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlIE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlMtCapIE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTotIE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTOT)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlMtCapTOT)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTotTOT)%>

                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTltHrsRE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTltHrsFP)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTltHrsIE)%> 
                <%: Html.ValidationMessageFor(model => model.AnnTAMatlTltHrsTOT)%>

                <%: Html.ValidationMessageFor(model => model.UnitMaintCostRE)%> 
                <%: Html.ValidationMessageFor(model => model.UnitMaintCostFP)%> 
                <%: Html.ValidationMessageFor(model => model.UnitMaintCostIE)%> 
                <%: Html.ValidationMessageFor(model => model.UnitMaintCostTOT)%>

                <%: Html.ValidationMessageFor(model => model.UnitTAMaintCostRE)%> 
                <%: Html.ValidationMessageFor(model => model.UnitTAMaintCostFP)%> 
                <%: Html.ValidationMessageFor(model => model.UnitTAMaintCostIE)%> 
                <%: Html.ValidationMessageFor(model => model.UnitTAMaintCostTOT)%>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
<%--            <%: Html.HiddenFor(model=>model.CompanySID) %>--%>
        </div>
        <div class="SubSectionTab" id="step-5">
            <p class="StepTitle">Additional Non-RAM Maintenance Hours and Cost </p>
            <p><i>For non-RAM activities</i></p>
            <div class="questionblock" qid="">
                <div id='UT_CST_NonRam_MT_HRS' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_UCST_154.1'><span class='numbering'>1.</span> Process Cleaning while running</li>
                        <li class='gridrowtext' qid='QSTID_UCST_154.5'><span class='numbering'>2.</span> Process Cleaning while down</li>
                        <li class='gridrowtext' qid='QSTID_UCST_154.9'><span class='numbering'>3.</span> Process Related while running (catalyst change, membrane change, etc.)</li>
                        <li class='gridrowtext' qid='QSTID_UCST_155.3'><span class='numbering'>4.</span> Process Related while down (catalyst change, membrane change, etc.)</li>
                        <li class='gridrowtext' qid='QSTID_UCST_155.7'><span class='numbering'>5.</span> General Unit Related Maintenance (unit cleaning, building, grounds, etc)</li>
                        <li class='gridrowtext' qid='QSTID_UCST_156.1'><span class='numbering'>6.</span> Total Non-RAM Hours and Spending</li>
                        <li class='gridrowtext' qid='QSTID_UCST_156.1'><span class='numbering'>7.</span> Total RAM and Non-RAM Hours and Spending</li>

                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Company and Contractor Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.1" /><%: Html.EditorFor(model => model.NRProcCLWRCOHrs, "DoubleTmpl", new {size="10" ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMHrs" })%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.5" /><%: Html.EditorFor(model => model.NRProcCLWDCOHrs,"DoubleTmpl",new {size="10" , NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMHrs" })%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.9" /><%: Html.EditorFor(model => model.NRProcRLWRCOHrs,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMHrs" })%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.3" /><%: Html.EditorFor(model => model.NRProcRLWDCOHrs,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMHrs" })%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.7" /><%: Html.EditorFor(model => model.NRUnitRLMTCOHrs,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMHrs" })%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_156.1" /><%: Html.EditorFor(model => model.NRUnitTotal_HR,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_156.3" /><%: Html.EditorFor(model => model.RAMNRUnitTotal_HR,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
             <%--       <ul class="gridbody">
                        <li class="columndesc">Contractor Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.2" /><%: Html.EditorFor(model => model.NRProcCLWRCTHrs,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.6" /><%: Html.EditorFor(model => model.NRProcCLWDCTHrs,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155" /><%: Html.EditorFor(model => model.NRProcRLWRCTHrs,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.4" /><%: Html.EditorFor(model => model.NRProcRLWDCTHrs,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.8" /><%: Html.EditorFor(model => model.NRUnitRLMTCTHrs,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                    </ul>--%>
                    <ul class="gridbody">
                        <li class="columndesc">Company and Contractor Materials Cost (local currency)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.3" /><%: Html.EditorFor(model => model.NRProcCLWRCOMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMSP"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.7" /><%: Html.EditorFor(model => model.NRProcCLWDCOMatl,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMSP"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.1" /><%: Html.EditorFor(model => model.NRProcRLWRCOMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMSP"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.5" /><%: Html.EditorFor(model => model.NRProcRLWDCOMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMSP"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.9" /><%: Html.EditorFor(model => model.NRUnitRLMTCOMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",CalcTag="sumNonRAMSP"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_156.2" /><%: Html.EditorFor(model => model.NRUnitTotal_SP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                         <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_156.4" /><%: Html.EditorFor(model => model.RAMNRUnitTotal_SP,"DoubleTmpl",new {size="10"  ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
<%--                    <ul class="gridbody">
                        <li class="columndesc">Contractor Materials</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.4" /><%: Html.EditorFor(model => model.NRProcCLWRCTMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_154.8" /><%: Html.EditorFor(model => model.NRProcCLWDCTMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.2" /><%: Html.EditorFor(model => model.NRProcRLWRCTMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_155.6" /><%: Html.EditorFor(model => model.NRProcRLWDCTMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_156" /><%: Html.EditorFor(model => model.NRUnitRLMTCTMatl,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" })%></li>
                    </ul>--%>
                </div>

                <%: Html.ValidationMessageFor(model => model.NRProcCLWRCOHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcCLWRCTHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcCLWRCOMatl)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcCLWRCTMatl)%>

                <%: Html.ValidationMessageFor(model => model.NRProcCLWDCOHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcCLWDCTHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcCLWDCOMatl)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcCLWDCTMatl)%>

                 <%: Html.ValidationMessageFor(model => model.NRProcRLWRCOHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcRLWRCTHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcRLWRCOMatl)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcRLWRCTMatl)%>

                <%: Html.ValidationMessageFor(model => model.NRProcRLWDCOHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcRLWDCTHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcRLWDCOMatl)%> 
                <%: Html.ValidationMessageFor(model => model.NRProcRLWDCTMatl)%>

                <%: Html.ValidationMessageFor(model => model.NRUnitRLMTCOHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRUnitRLMTCTHrs)%> 
                <%: Html.ValidationMessageFor(model => model.NRUnitRLMTCOMatl)%> 
                <%: Html.ValidationMessageFor(model => model.NRUnitRLMTCTMatl)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <div class="SubSectionTab" id="step-6">
            <p class="StepTitle">Turnaround Work Execution</p>
            <p><i>How is the Turnaround work executed during  the TAR?  (Percent of hours, must total to 100%)</i></p>
            <div class="questionblock" qid="">
                <div id='UT_CST_TA_Wrk_Exe' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>Delivery</li>
                        <li class='gridrowtext' qid='QSTID_UCST_157'><span class='numbering'>1.</span> Company technicians </li>
                        <li class='gridrowtext' qid='QSTID_UCST_157.1'><span class='numbering'>2.</span> Contractors on reimbursable contract</li>
                        <li class='gridrowtext' qid='QSTID_UCST_157.2'><span class='numbering'>3.</span> Contractors on measured (Unit Rate) contract</li>
                        <li class='gridrowtext' qid='QSTID_UCST_157.3'><span class='numbering'>4.</span> Contractors on fixed price contract</li>
                        <li class='gridrowtext' qid='QSTID_UCST_157.4'><span class='numbering'>5.</span> Contractors on alliance or incentivised contract</li>
                        <li class='gridrowtext calctext' qid='QSTID_UCST_157.5'><span class='numbering'>6.</span> Total</li>
                        <li class='gridrowtext' qid='QSTID_UCST_157.6'><span class='numbering'>7.</span> If the Turnaround is performed by contractors, do you use the same contractor as for routine maintenance?</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc" style="text-align:center">Percent of Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_157" /><%: Html.EditorFor(model => model.CostTAWKEXEPCT_CT,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_157.1" /><%: Html.EditorFor(model => model.CostTAWKEXEPCT_CRC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_157.2" /><%: Html.EditorFor(model => model.CostTAWKEXEPCT_CMC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_157.3" /><%: Html.EditorFor(model => model.CostTAWKEXEPCT_CPC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_157.4" /><%: Html.EditorFor(model => model.CostTAWKEXEPCT_CAC,"DoubleTmpl",new {size="10" ,CalcTag="sumDvryHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_157.5" /><%: Html.EditorFor(model => model.CostTAWKEXEPCT_TT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_157.6" /><br />
                            <select id="CostTAWKEXE_RM" name="CostTAWKEXE_RM" width="30px">
                                <option value='-1'>SELECT ONE</option>
                                <option value='No' <%= Model.CostTAWKEXE_RM != null ?(Model.CostTAWKEXE_RM.Equals("No")? "SELECTED" : String.Empty):String.Empty %>>No</option>
                                <option value='Yes' <%= Model.CostTAWKEXE_RM != null ?(Model.CostTAWKEXE_RM.Equals("Yes")? "SELECTED" : String.Empty):String.Empty %>>Yes</option>
                                <option value='Mixed' <%= Model.CostTAWKEXE_RM != null ?(Model.CostTAWKEXE_RM.Equals("Mixed")? "SELECTED" : String.Empty):String.Empty %>>Mixed</option>
                            </select></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.CostTAWKEXEPCT_CT)%> 
                <%: Html.ValidationMessageFor(model => model.CostTAWKEXEPCT_CRC)%> 
                <%: Html.ValidationMessageFor(model => model.CostTAWKEXEPCT_CMC)%> 
                <%: Html.ValidationMessageFor(model => model.CostTAWKEXEPCT_CPC)%> 
                <%: Html.ValidationMessageFor(model => model.CostTAWKEXEPCT_CAC)%> 
                <%: Html.ValidationMessageFor(model => model.CostTAWKEXEPCT_TT)%> 
                <%: Html.ValidationMessageFor(model => model.CostTAWKEXE_RM)%> 

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-7">
            <p class="StepTitle">Routine Work Execution</p>
            <p><i>How is routine work executed  (Percent of hours, must total to 100%)</i></p>
            <div class="questionblock" qid="">
                <div id='UT_CST_RW_EXE' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>Delivery</li>
                        <li class='gridrowtext' qid='QSTID_UCST_158'><span class='numbering'>1.</span> Company technicians </li>
                        <li class='gridrowtext' qid='QSTID_UCST_158.1'><span class='numbering'>2.</span> Contractors on reimbursable contract</li>
                        <li class='gridrowtext' qid='QSTID_UCST_158.2'><span class='numbering'>3.</span> Contractors on measured (Unit Rate) contract</li>
                        <li class='gridrowtext' qid='QSTID_UCST_158.3'><span class='numbering'>4.</span> Contractors on fixed price contract</li>
                        <li class='gridrowtext' qid='QSTID_UCST_158.4'><span class='numbering'>5.</span> Contractors on alliance or incentivised contract</li>
                        <li class='gridrowtext calctext' qid='QSTID_UCST_158.5'><span class='numbering'>6.</span> Total</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc" style="text-align:center">Percent of Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_158" /><%: Html.EditorFor(model => model.RTWKRoutCT_CT,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_158.1" /><%: Html.EditorFor(model => model.RTWKRoutC_CRC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_158.2" /><%: Html.EditorFor(model => model.RTWKRoutC_CMC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_158.3" /><%: Html.EditorFor(model => model.RTWKRoutC_CPC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_158.4" /><%: Html.EditorFor(model => model.RTWKRoutC_CAC,"DoubleTmpl",new {size="10" ,CalcTag="sumRoutineHrs" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_UCST_158.5" /><%: Html.EditorFor(model => model.RTWKRoutC_TT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0",@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.RTWKRoutCT_CT)%> 
                <%: Html.ValidationMessageFor(model => model.RTWKRoutC_CRC)%> 
                <%: Html.ValidationMessageFor(model => model.RTWKRoutC_CMC)%> 
                <%: Html.ValidationMessageFor(model => model.RTWKRoutC_CPC)%> 
                <%: Html.ValidationMessageFor(model => model.RTWKRoutC_CAC)%> 
                <%: Html.ValidationMessageFor(model => model.RTWKRoutC_TT)%> 

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
                        <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



    <script type="text/javascript">
        function loadHelpReferences() {
            var objHelpRefs = Section_HelpLst;
            for (i = objHelpRefs.length - 1; i >= 0; i--) {
                var qidNm = objHelpRefs[i]['HelpID'];
                if ($('li[qid^="' + qidNm + '"]').length) {
                    var $elm = $('li[qid^="' + qidNm + '"]');
                    $elm.append('<img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
                    $elm.find('img.tooltipBtn').css('visibility', 'visible');
                } else if ($('div[qid^="' + qidNm + '"]').length) {
                    $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
                }
            }
        }

        function formatResult(el, value) {
            var format = 'N';
            if (el.attr('NumFormat') == 'Int') format = 'n0';
            if (el.attr('DecPlaces') != null) {
                if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
            }
            if (value != undefined && isNaN(value)) value = 0;
            var tempval = Globalize.parseFloat(value.toString(), 'en');
            if (isFinite(tempval)) {
                el.val(Globalize.format(tempval, format));
                var idnm = '#_' + el.attr('id');
                var $f = $(idnm);
                if ($f.length) {
                    $f.val(el.val());
                }
            } else {
                el.val('');
            }
        }
        $("input[CalcTag*='sumAnnTACftCst']").sum({
            bind: "keyup",
            selector: "#AnnTAMatlTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#AnnTAMatlTOT').attr('readonly', 'readonly');
        $('#AnnTAMatlTOT').change(function () {
            $(this).validate();
        });
        
        $("input[CalcTag*='sumAnnTATltHr']").sum({
            bind: "keyup",
            selector: "#AnnTAMatlTltHrsTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#AnnTAMatlTltHrsTOT').attr('readonly', 'readonly');
        $('#AnnTAMatlTltHrsTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumAnnTAMtCapCftCst']").sum({
            bind: "keyup",
            selector: "#AnnTAMatlMtCapTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#AnnTAMatlMtCapTOT').attr('readonly', 'readonly');
        $('#AnnTAMatlMtCapTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumAnnTATotCst']").sum({
            bind: "keyup",
            selector: "#AnnTAMatlTotTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#AnnTAMatlTotTOT').attr('readonly', 'readonly');
        $('#AnnTAMatlTotTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumUnitCost']").sum({
            bind: "keyup",
            selector: "#UnitMaintCostTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#UnitMaintCostTOT').attr('readonly', 'readonly');
        $('#UnitMaintCostTOT').change(function () {
            $(this).validate();
        });  
        $("input[CalcTag*='sumTACost']").sum({
            bind: "keyup",
            selector: "#UnitTAMaintCostTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#UnitTAMaintCostTOT').attr('readonly', 'readonly');
        $('#UnitTAMaintCostTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumAnnTATotCstA']").sum({
            bind: "keyup",
            selector: "#AnnTAMatlTotRE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#AnnTAMatlTotRE').attr('readonly', 'readonly');
        $('#AnnTAMatlTotRE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumAnnTATotCstB']").sum({
            bind: "keyup",
            selector: "#AnnTAMatlTotFP",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#AnnTAMatlTotFP').attr('readonly', 'readonly');
        $('#AnnTAMatlTotFP').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumAnnTATotCstC']").sum({
            bind: "keyup",
            selector: "#AnnTAMatlTotIE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#AnnTAMatlTotIE').attr('readonly', 'readonly');
        $('#AnnTAMatlTotIE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTCftCst']").sum({
            bind: "keyup",
            selector: "#RoutMatlTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutMatlTOT').attr('readonly', 'readonly');
        $('#RoutMatlTOT').change(function () {
            $(this).validate();
        });     
        $("input[CalcTag*='sumRTCftHr']").sum({
            bind: "keyup",
            selector: "#RoutCraftmanWhrTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutCraftmanWhrTOT').attr('readonly', 'readonly');
        $('#RoutCraftmanWhrTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTCftTltHr']").sum({
            bind: "keyup",
            selector: "#RoutCraftmanTltHrsTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutCraftmanTltHrsTOT').attr('readonly', 'readonly');
        $('#RoutCraftmanTltHrsTOT').change(function () {
            $(this).validate();
        });
        
        $("input[CalcTag*='sumRTMtCapCftCst']").sum({
            bind: "keyup",
            selector: "#RoutMatlMtCapTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutMatlMtCapTOT').attr('readonly', 'readonly');
        $('#RoutMatlMtCapTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTMtCapCftHr']").sum({
            bind: "keyup",
            selector: "#RoutCraftmanMtCapWhrTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutCraftmanMtCapWhrTOT').attr('readonly', 'readonly');
        $('#RoutCraftmanMtCapWhrTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotCst']").sum({
            bind: "keyup",
            selector: "#RoutMatlTotTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutMatlTotTOT').attr('readonly', 'readonly');
        $('#RoutMatlTotTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotCstA']").sum({
            bind: "keyup",
            selector: "#RoutMatlTotRE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutMatlTotRE').attr('readonly', 'readonly');
        $('#RoutMatlTotRE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotCstB']").sum({
            bind: "keyup",
            selector: "#RoutMatlTotFP",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutMatlTotFP').attr('readonly', 'readonly');
        $('#RoutMatlTotFP').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotCstC']").sum({
            bind: "keyup",
            selector: "#RoutMatlTotIE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutMatlTotIE').attr('readonly', 'readonly');
        $('#RoutMatlTotIE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotHr']").sum({
            bind: "keyup",
            selector: "#RoutCraftmanTotWhrTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutCraftmanTotWhrTOT').attr('readonly', 'readonly');
        $('#RoutCraftmanTotWhrTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotHrA']").sum({
            bind: "keyup",
            selector: "#RoutCraftmanTotWhrRE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutCraftmanTotWhrRE').attr('readonly', 'readonly');
        $('#RoutCraftmanTotWhrRE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotHrB']").sum({
            bind: "keyup",
            selector: "#RoutCraftmanTotWhrFP",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutCraftmanTotWhrFP').attr('readonly', 'readonly');
        $('#RoutCraftmanTotWhrFP').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRTTotHrC']").sum({
            bind: "keyup",
            selector: "#RoutCraftmanTotWhrIE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutCraftmanTotWhrIE').attr('readonly', 'readonly');
        $('#RoutCraftmanTotWhrIE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTACftHr']").sum({
            bind: "keyup",
            selector: "#TACraftmanWhrTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TACraftmanWhrTOT').attr('readonly', 'readonly');
        $('#TACraftmanWhrTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTACftTltHr']").sum({
            bind: "keyup",
            selector: "#TACraftmanWhrTltHrsTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TACraftmanWhrTltHrsTOT').attr('readonly', 'readonly');
        $('#TACraftmanWhrTltHrsTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTAMtCapCftHr']").sum({
            bind: "keyup",
            selector: "#TACraftmanMtCapWhrTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TACraftmanMtCapWhrTOT').attr('readonly', 'readonly');
        $('#TACraftmanMtCapWhrTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTATotHr']").sum({
            bind: "keyup",
            selector: "#TACraftmanTotWhrTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TACraftmanTotWhrTOT').attr('readonly', 'readonly');
        $('#TACraftmanTotWhrTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTATotHrA']").sum({
            bind: "keyup",
            selector: "#TACraftmanTotWhrRE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TACraftmanTotWhrRE').attr('readonly', 'readonly');
        $('#TACraftmanTotWhrRE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTATotHrB']").sum({
            bind: "keyup",
            selector: "#TACraftmanTotWhrFP",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TACraftmanTotWhrFP').attr('readonly', 'readonly');
        $('#TACraftmanTotWhrFP').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTATotHrC']").sum({
            bind: "keyup",
            selector: "#TACraftmanTotWhrIE",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TACraftmanTotWhrIE').attr('readonly', 'readonly');
        $('#TACraftmanTotWhrIE').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumMatlTltHr']").sum({
            bind: "keyup",
            selector: "#RoutMatlTltHrsTOT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#RoutMatlTltHrsTOT').attr('readonly', 'readonly');
        $('#RoutMatlTltHrsTOT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumDvryHrs']").sum({
            bind: "keyup",
            selector: "#CostTAWKEXEPCT_TT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#CostTAWKEXEPCT_TT').attr('readonly', 'readonly');
        $('#CostTAWKEXEPCT_TT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumRoutineHrs']").sum({
            bind: "keyup",
            selector: "#RTWKRoutC_TT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#TAWKRoutCT_TT').attr('readonly', 'readonly');
        $('#TAWKRoutCT_TT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumNonRAMHrs']").sum({
            bind: "keyup",
            selector: "#NRUnitTotal_HR",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                    var $result = $("#RAMNRUnitTotal_HR");
                    $result.calc("v1+v2+v3", {
                        v1: $("#NRUnitTotal_HR"),
                        v2: $("#RoutCraftmanTotWhrTOT"),
                        v3: $("#TACraftmanTotWhrTOT")
                    });
                    formatResult($result, $result.val());
                }
            }
        });
        $('#NRUnitTotal_HR').attr('readonly', 'readonly');
        $('#NRUnitTotal_HR').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumNonRAMSP']").sum({
            bind: "keyup",
            selector: "#NRUnitTotal_SP",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                    var $result = $("#RAMNRUnitTotal_SP");
                    $result.calc("v1+v2+v3", {
                        v1: $("#NRUnitTotal_SP"),
                        v2: $("#AnnTAMatlTotTOT"),
                        v3: $("#RoutMatlTotTOT")
                    });
                    formatResult($result, $result.val());
                }
            }
        });
        $('#NRUnitTotal_SP').attr('readonly', 'readonly');
        $('#NRUnitTotal_SP').change(function () {
            $(this).validate();
        });
        if ($("#NRUnitTotal_HR").val() != null && !isNaN($("#NRUnitTotal_HR").val()))
            if ($("#RoutCraftmanTotWhrTOT").val() != null && !isNaN($("#RoutCraftmanTotWhrTOT").val())) {
                if ($("#TACraftmanTotWhrTOT").val() != 0 && !isNaN($("#TACraftmanTotWhrTOT").val())) {
                    $("#NRUnitTotal_HR").trigger('keyup');
                } $("#NRProcCLWRCOHrs, #NRProcCLWDCOHrs, #NRProcRLWRCOHrs, #NRProcRLWDCOHrs, #NRUnitRLMTCOHrs, #RoutCraftmanWhrRE, #RoutCraftmanMtCapWhrRE, #RoutCraftmanWhrFP, #RoutCraftmanMtCapWhrFP, #RoutCraftmanWhrIE, #RoutCraftmanMtCapWhrIE, #TACraftmanWhrRE, #TACraftmanMtCapWhrRE, #TACraftmanWhrFP, #TACraftmanMtCapWhrFP, #TACraftmanWhrIE, #TACraftmanMtCapWhrIE").bind("keyup", function () {
                    var $result = $("#RAMNRUnitTotal_HR");
                    $result.calc("v1+v2+v3", {
                        v1: $("#NRUnitTotal_HR"),
                        v2: $("#RoutCraftmanTotWhrTOT"),
                        v3: $("#TACraftmanTotWhrTOT")
                    });
                    formatResult($result, $result.val());
                })
            };
        if ($("#NRUnitTotal_SP").val() != null && !isNaN($("#NRUnitTotal_SP").val()))
            if ($("#AnnTAMatlTotTOT").val() != null && !isNaN($("#AnnTAMatlTotTOT").val())) {
                if ($("#RoutMatlTotTOT").val() != 0 && !isNaN($("#RoutMatlTotTOT").val())) {
                    $("#NRUnitTotal_SP").trigger('keyup');
                } $("#NRProcCLWRCOMatl, #NRProcCLWDCOMatl, #NRProcRLWRCOMatl, #NRProcRLWDCOMatl, #NRUnitRLMTCOMatl, #RoutMatlRE, #RoutMatlMtCapRE, #RoutMatlFP, #RoutMatlMtCapFP, #RoutMatlIE, #RoutMatlMtCapIE, #AnnTAMatlRE, #AnnTAMatlMtCapRE, #AnnTAMatlFP, #AnnTAMatlMtCapFP, #AnnTAMatlIE, #AnnTAMatlMtCapIE").bind("keyup", function () {
                    var $result = $("#RAMNRUnitTotal_SP");
                    $result.calc("v1+v2+v3", {
                        v1: $("#NRUnitTotal_SP"),
                        v2: $("#AnnTAMatlTotTOT"),
                        v3: $("#RoutMatlTotTOT")
                    });
                    formatResult($result, $result.val());
                })
            };

        var secComments = Section_Comments;
        var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
for (i = secComments.length - 1; i >= 0; i--) {
    var qidNm = secComments[i]['CommentID'];
    if ($('img[qid="' + qidNm + '"]').length) {
        $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
    }
} <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function (index, html) {
    var gridId = $(this).closest('.grid').attr('id');
    if ($.trim(html).indexOf('nbsp', 0) == -1) {
        var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
    }
    $(this).html(newHTML);
});
$('img[suid]').click(function () {
    var suid = $(this).attr('suid');
    var qid = $(this).attr('qid');
    recordComment(suid, qid);
});
    </script>
</asp:Content>
