﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Profile.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="<%= Url.Content("~/Content/dd_menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        #CultureFormat
        {
            font-style: italic;
            font-size: x-small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!--script src="../../Scripts/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.unobtrusive-ajax.js" type="text/javascript"></script-->
    <h4>My Profile</h4>
    <p>
    <i>
        Use the form below to update your profile. 
    </i>
   </p>
   <div style="color: #991000;">
                <%:  TempData["UserCreateMsg"]  %></div>
    <fieldset style="width: 650px;">
        <legend>Edit Profile</legend>
        
        <% Html.EnableClientValidation(); %>

        <% Html.BeginForm("Index", "Profile");
           { %>
        <div class="grid_16">
            <span style="float: right;">&nbsp; </span>
        </div>
        <div class="grid_5">
            Name :
        </div>
        <div class="grid_11">
            <span class="editor-label">
                <%: Html.LabelFor(model=>model.Firstname) %></span> <span class="editor-field">
                    <%: Html.EditorFor(model => model.Firstname, "TextTmpl", new { style = "width:130px" })%>
                </span><span class="editor-label">
                    <%: Html.LabelFor(model=>model.Lastname) %></span> <span class="editor-field">
                        <%: Html.EditorFor(model => model.Lastname, "TextTmpl", new { style = "width:130px" })%>
                        
                    </span>
               
                     <%: Html.ValidationMessageFor( model=>model.Firstname ) %>
                   
                    <%: Html.ValidationMessageFor( model=>model.Lastname ) %>
        </div>

        <p>
            &nbsp;</p>
        <div class="grid_5">
            Email :
        </div>
        <div class="grid_11">
            <span class="editor-field">
                <%: Html.LabelFor(model => model.EmailAddress,"Email Address")%>
                <%: Html.EditorFor(model => model.EmailAddress, "TextTmpl", new { size=35 })%>
            </span>
               <%: Html.ValidationMessageFor(model => model.EmailAddress)%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_5">
            Job Title:
        </div>
        <div class="grid_11">
            <span class="editor-field">
                <%: Html.LabelFor(model => model.JobTile)%>
                <%: Html.EditorFor(model => model.JobTile, "TextTmpl", new { size = 35 })%>
            </span>
            <%: Html.ValidationMessageFor(model => model.JobTile)%>
        </div>
        <p>
            &nbsp;</p>
        <div class="grid_5">
            <span class="editor-label">Select culture: </span><br />
            <font color="#990000" size="1px">(optional) default is US English</font> 
        </div>
        <div class="grid_11">
            <%--<span class="editor-field">--%>
                <%: Html.DropDownListFor(model => model.Culture, CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                                                                        .OrderBy(ci=>ci.DisplayName)
                                                                        .Where(ci=> ci.Name.Trim().Length > 0)
                                                                        .Select(ci =>new SelectListItem() {Text=ci.EnglishName,Value=ci.Name }).Distinct()
                                    ,"Select Culture")%>
                <br />
                
            <div id="CultureFormat">
                Number format :<span id="NumberFormat"> 1,234,567.90</span><br />
                Date format : <span id="DateFormat">
                    <%: DateTime.Now.ToLongDateString() %></span>
            </div>
        </div>
        <%: Html.HiddenFor(model=>model.UserNo) %>
        <%: Html.HiddenFor(model=>model.UserID) %>
        <%: Html.HiddenFor(model=>model.CompanyID) %>
      
        <% }%>
        
            
        
        <div id="alertBox">
        </div>
        <input type="submit" title="Update" name="Update" value="Save" <% var userinfo = (RAMAPP_Mvc3.Entity.LogIn)HttpContext.Current.Session["UserInfo"]; if (userinfo.UserID.Trim() == "demo/04TEST11") { %> disabled="disabled" <% }; %> />
    </fieldset>
    <script type="text/javascript">
       
        $('#Culture').val('<%: Model.Culture != null ? Model.Culture.Trim() : String.Empty %>');
       
        $('#Culture').click(function () {
            var cultureSlt = $('#Culture').val();
            $.post("<%= Page.ResolveUrl("~/Admin/DisplayLocale")%>",
				 { "locale": cultureSlt },
				  function (response) {
				      try {
				          $('#NumberFormat').html(response.localeNumber);
				          $('#DateFormat').html(response.localeDate);
				      }
				      catch (error) {
				          $('#NumberFormat').html("Unable to display format ");
				      }
				  }, 'json');
        });

        $(document).ready(function () {
            var cultureSlt = $('#Culture').val();
       
            $.post("<%= Page.ResolveUrl("~/Admin/DisplayLocale")%>",
				 { "locale": cultureSlt },
				  function (response) {
				      try {
				          $('#NumberFormat').html(response.localeNumber);
				          $('#DateFormat').html(response.localeDate);
				      }
				      catch (error) {
				          $('#NumberFormat').html("Unable to display format ");
				      }
				  }, 'json');
        });
    </script>
</asp:Content>
