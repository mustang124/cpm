﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Src="~/Views/Shared/HomeMenuUserControl.ascx" TagName="HomeMenu" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

 <h2>Reset  Password</h2>
    <p>
        Use the form below to reset your password. 
    </p>
    <p>
        New passwords are required to be a minimum of <%= Html.Encode(ViewData["PasswordLength"]) %> characters in length.
    </p>
     
  
     <div id="ReturnMsg" class="<%: ViewData["ResetMessageClass"] ?? String.Empty%>">
                      <span class="ResponseMsg"><%:  ViewData["ResetMessage"] ?? String.Empty%></span>
              </div>
             
    <% using (Html.BeginForm()) { %>
       <%-- <%= Html.ValidationSummary(true, "Password change was unsuccessful. Please correct the errors and try again.") %>--%>
        <div>
            <fieldset style="width:650px;">
                <legend> Enter New Password</legend>
                
               
                <div class="editor-label">
                    <%= Html.Label("NewPassword","Enter New Password") %>
                </div>
                <div class="editor-field">
                    <%= Html.Password("NewPassword", "", new { size = 30 })%>
                    <%= Html.ValidationMessage("NewPassword")%>
                </div>
                
                <div class="editor-label">
                    <%= Html.Label("ConfirmPassword","Re-enter New Password ") %>
                </div>
                <div class="editor-field">
                    <%= Html.Password("ConfirmPassword", "", new { size = 30})%>
                    <%= Html.ValidationMessage("ConfirmPassword") %>
                </div>
                
                <p>
                    <input type="submit" value="Reset Password" />
                </p>
            </fieldset>
        </div>
    <% } %>
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
<link href="<%= Page.ResolveUrl("~/Content/menu_style.css")%>" type="text/css" rel="stylesheet" />
</asp:Content>



<asp:Content ID="Content9" ContentPlaceHolderID="MenuContent" runat="server">
  <uc:HomeMenu ID="homeMenu" runat="server" />
</asp:Content>
