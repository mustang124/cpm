﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.LogOnModel>" %>
<%@ Register Src="~/Views/Shared/HomeMenuUserControl.ascx" TagName="HomeMenu" TagPrefix="uc" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
<link href="<%= Page.ResolveUrl("~/Content/menu_style.css")%>" type="text/css" rel="stylesheet" />
<style>
	.ui-tooltip-rounded 
	
	
	
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="MenuContent" runat="server">
    <uc:HomeMenu ID="homeMenu" runat="server" />
</asp:Content>

<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
    <div style="margin: 30px 30px; width: 500px;" >

        <%
            
          
            using (Html.BeginForm())
            { %>
        <%= Html.ValidationSummary(true, "Login was unsuccessful. Please try again.") %>
        
        <fieldset>
            <legend>Login</legend>
            <br/>
            
            <div class="editor-label">
                <%= Html.LabelFor(m => m.EmailAddress) %>
            </div>
            <div class="editor-field">
                <%= Html.TextBoxFor(m => m.EmailAddress)%>
                <%= Html.ValidationMessageFor(m => m.EmailAddress)%>
            </div>
            <div class="editor-label">
                <%= Html.LabelFor(m => m.Password) %>
            </div>
            <div class="editor-field">
                <%= Html.PasswordFor(m => m.Password) %>
                <%= Html.ValidationMessageFor(m => m.Password) %>
            </div>
           <%-- <div class="editor-label">
                <%= Html.CheckBoxFor(m => m.RememberMe)%>
                <%= Html.LabelFor(m => m.RememberMe) %>
            </div>--%>
             <div class="editor-label">
               <%-- <%= Html.ActionLink("Forgot Password?","ForgotPassword","Account",new {onclick="alert('This feature is not ready');return false;"}) %>--%>
               
            </div>
            <p>
             <img src="~/images/login.png"   runat="server" align="middle"/> &nbsp; 
             
             <span style="float:right;"><input type="submit" value="Login" onclick="this.value='Verifying...'"/></span> 
            </p>
        </fieldset>
        <% } %>
    </div>
    
</asp:Content>
