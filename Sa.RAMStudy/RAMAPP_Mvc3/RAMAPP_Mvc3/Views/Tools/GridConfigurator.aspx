﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <style>
        .grid
        {
            display: inline-table;
            margin: 10px 10px;
            border: 1px solid #ddd;
            width: 100%;
        }
        .gridrowtext
        {
            min-width: 275px;
            max-width: 400px;
            display: inline-block;
            margin: 5px 0;
        }
        .gridrow
        {
            display: inline-block;
            margin: 5px 0;
        }
        .grid ul
        {
            margin-left: 0;
            padding-left: 5px;
            padding-right: 5px;
            display: block;
            margin: 0px 0px;
            float: left;
            max-width: 325px;
            list-style: none;
            line-height: normal;
            list-style-position: outside;
        }
        .grid ul li
        {
            align: left;
            valign: middle;
            height: 55px;
            display: block;
            border-bottom: #EEE 1px solid;
        }
        .grid .gridbody
        {
            border-left: #EEE 1px solid;
        }
        div[qid]
        {
            background: #EEE;
            border: 1px #000 solid;
            font-size: x-small;
        }
        
        .drop, #sundisplay, #formulaGroup
        {
            width: 94%;
            height: 50px;
            padding: 0.5em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="RightContent" runat="server">
    <h4> Grid Configurator</h4>
    <%
        using (var db = new RAMEntities())
        {

            var listItem = (from f in db.View_QuestionsInStudy
                            join d in db.QuestionSubSection on f.QuestionGridID equals d.QuestionSubSectionID
                            where f.QuestionGridID != null
                            select new SelectListItem {Text = d.Name, Value = f.QuestionGridID}).ToList();
            listItem.Insert(0, new SelectListItem {Text = "Select Sub Section", Value = "-1"});

            //var result1= db.ExecuteStoreQuery<string>( "select dbo.CreateGridList_A({0})");
            //var result2= db.ExecuteStoreQuery<string>( "select dbo.EditGridList_B({0}) ");

            //var tablebldr = result1.Single() + result2.Single();
%>
    <%:Html.DropDownList("Grids", listItem)%>
    <% } %>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftContent"  runat="server">
</asp:Content>
<asp:Content ID="Main" ContentPlaceHolderID="MainContent"  runat="server">
    <div id="tbl">
    </div>
    <div class="drop ui-widget-content">Drag Elements here</div>

    <span  id="group" 
        style="border: thin solid #000000; width: 50px; font-size: x-small;" > </span>&nbsp; = &nbsp;
        <span  id="groupElements" style="border: thin solid #000000;width: 350px; font-size: x-small;"></span>
    <input type="hidden" id="formulaGroup" />
    <input type="hidden" id="sumdisplay" />

    <% using(Html.BeginForm("AddCalcGroup"))
       {%>
    <input type="submit" value="Add Group" />
    &nbsp;
    <input type="reset" value="Clear" onclick="clearFormula()" />
    <% } %>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.8.15.custom.min.js" type="text/javascript"></script>
    <%-- ReSharper disable Asp.DeadCode --%>
    <script type="text/javascript" language="javascript">
        var formulaGroup;
        var formulaChild;

        function clearFormula() {

            $('#sumdisplay').html('');
            $('#formulaGroup').html('');
            $('div[qid]').css("background-color", "#EEE");

        }

        $(document).ready(function () {

            $("div[qid]").draggable({ helper: 'clone' });

            $(".drop").droppable({
                accept: 'div[qid]',
                activeClass: 'droppable-active',
                hoverClass: 'droppable-hover',
                drop: function (ev, ui) {


                    if ($(ui.draggable).attr('qtype') == 'TtlFld' || $(ui.draggable).attr('qtype') == 'FormulaFld') {
                        alert(1);
                        $('#formulaGroup').val($(ui.draggable).text());
                        $('#group').html($('#formulaGroup').val());
                        $(ui.draggable).css("background-color", "#B5E3F7");
                    }
                    else {
                        alert(3);
                        var formula = $("#sumdisplay").val() + $(ui.draggable).text() + '+';
                        $(ui.draggable).css("background-color", "yellow");
                        $('#groupElements').html(formula);
                        $("#sumdisplay").val(formula);
                    }


                }
            });

        });







        $('#Grids').change(function () {
            $.getJSON("/Tools/GetGrid", { "gridId": $(this).val() }, function (data) {

                $('#tbl').html(data);
                $("div[qid]").draggable({ helper: 'clone' });
            });
        });
     
     
     
     
    </script>
    <%-- ReSharper restore Asp.DeadCode --%>
</asp:Content>
