﻿ 
 
  


<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_INVENModel>" %>
<%@ Import Namespace="System.Globalization" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<%//------------------------------------------------------
  // Date Created : 12/12/2013 09:59:27  
  //-----------------------------------------------------%>
	 <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
	 <script type="text/javascript">
	     $(document).ready(function () {
	         // Smart Wizard 	
	         $("#wizard_ST_INVEN").smartTab({ transitionEffect: 'fade', keyNavigation: false });

	         /* $('li .stepDesc small').each(function (index) {
	         var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

	         $(this).text($(elementTitleId).text());

	         });*/

	     });
		</script>
	  <% Html.EnableClientValidation(); %>			   
	  <% using (Ajax.BeginForm("ST_INVEN",new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" })) { 
	     bool uomPref = true;
	  %>
	  	<div id="wizard_ST_INVEN" class="stContainer">
		    <ul class="tabContainer">
			<li><a href='#step-1'><label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br /><small>MRO Inventory</small> </span></a></li><li><a href='#step-2'><label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br /><small>MRO Practices</small> </span></a></li>
			</ul>
		
			    				  
				  
				  <div class="SubSectionTab"  id="step-1">
				   
					<p class="StepTitle"><a href="#" tooltip="Maintenance, repair, and operating materials (MRO) and spare parts. This excludes capital equipment staged for installation and consumable operating materials such as catalysts, chemicals, etc.">MRO</a> Inventory</p>
					<p><i>The purpose of this section is to collect information on the <a href="#" tooltip="Maintenance, repair, and operating materials (MRO) and spare parts. This excludes capital equipment staged for installation and consumable operating materials such as catalysts, chemicals, etc.">MRO</a> (maintenance, repair, and operating supplies) inventory required to support maintenance work.</i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_IVN_100.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_100.1' /></span> <span class='numbering'>1.</span> How many <a href="#" tooltip="Maintenance, repair, and operating materials (MRO) and spare parts. This excludes capital equipment staged for installation and consumable operating materials such as catalysts, chemicals, etc.">MRO</a> inventory (cataloged) line items?
					 					 <span><img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteSparesCatalogLineItemCnt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteSparesCatalogLineItemCnt)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Number </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_100.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_100.2' /></span> <span class='numbering'>2.</span> What is the total stocked <a href="#" tooltip="Maintenance, repair, and operating materials (MRO) and spare parts. This excludes capital equipment staged for installation and consumable operating materials such as catalysts, chemicals, etc.">MRO</a> inventory value?
					 					 <span><img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteSparesInventory,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteSparesInventory)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Local Currency </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_100.2USD">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_100.2USD' /></span> Total stocked <a href="#" tooltip="Maintenance, repair, and operating materials (MRO) and spare parts. This excludes capital equipment staged for installation and consumable operating materials such as catalysts, chemicals, etc.">MRO</a> inventory value in US$
					 					 <span><img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteSparesInventoryUSD,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.SiteSparesInventoryUSD)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > US$ </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_100.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_100.3' /></span> <span class='numbering'>3.</span> What percent of the <a href="#" tooltip="Maintenance, repair, and operating materials (MRO) and spare parts. This excludes capital equipment staged for installation and consumable operating materials such as catalysts, chemicals, etc.">MRO</a> inventory is <a href="#" tooltip="Inventoried items that are physically stored in a storeroom warehouse but are owned by the vendor or supplier until issued or consumed. The basis would be calculated against total stocked MRO inventory value.">consignment</a> or vendor owned?
					 					 <span><img id="Img4" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteSparesCosignOrVendorPcnt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.SiteSparesCosignOrVendorPcnt)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Percent </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_100.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_100.4' /></span> <span class='numbering'>4.</span> What is the total annual <a href="#" tooltip="Storeroom warehouse for MRO materials.">MRO storeroom</a> issues value?
					 					 <span><img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteSparesPlantStoresIssues,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteSparesPlantStoresIssues)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Local Currency </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_100.4USD">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_100.4USD' /></span> Total annual value of <a href="#" tooltip="Storeroom warehouse for MRO materials.">MRO storeroom</a> issues in US$
					 					 <span><img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteSparesPlantStoresIssuesUSD,"DoubleTmpl",new {size="15" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.SiteSparesPlantStoresIssuesUSD)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > US$ </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.3' /></span> <span class='numbering'>5.</span> What is the total annual operating cost (wages and benefits) for the <a href="#" tooltip="Storeroom warehouse for MRO materials.">MRO storeroom</a>?
					 					 <span><img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteWhsOrgOpCost,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgOpCost)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Local Currency </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.3USD">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.3USD' /></span> Total annual operating cost (wages and benefits) for the <a href="#" tooltip="Storeroom warehouse for MRO materials.">MRO storeroom</a>  in US$
					 					 <span><img id="Img8" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteWhsOrgOpCostUSD,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgOpCostUSD)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > US$ </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  				  
				  </div>
				  <div class="SubSectionTab"  id="step-2">
				   
					<p class="StepTitle">MRO Practices</p>
					<p><i>The purpose of this section is to collect information on the practices of the <a href="#" tooltip="Storeroom warehouse for MRO materials.">MRO storeroom</a>/organization in support maintenance work.</i></p> 
				 
		              
					 
			    <div  class="questionblock" qid="QSTID_IVN_119.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_119.1' /></span> <span class='numbering'>1.</span> What percent of MRO materials have an assigned criticality ranking?
					 					 <span><img id="Img9" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteWhsOrgPcntCriticalRank,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgPcntCriticalRank)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Percent </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_119.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_119.2' /></span> <span class='numbering'>2.</span> Is preventative maintenance performed on <a href="#" tooltip="A storage warehouse or facility where maintenance and repair materials are stored. This includes equipment spare parts, maintenance consumables, and spare equipment.">maintenance storeroom</a> items (e.g. rotating <a href="#" tooltip="Electrical motors that drive process equipment such as pumps and compressors. Small motors that are part of auxiliary systems such as process analyzers, lube oil circuits, or additive pumps are not included. See Pumps.">motor</a> shafts)?
					 					 <span><img id="Img10" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Yes</td> <td>&nbsp;&nbsp;No</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SiteWhsOrgPrevMaintStrRmItems, "Y") %></td> <td><%= Html.RadioButtonFor(model => model.SiteWhsOrgPrevMaintStrRmItems, "N") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgPrevMaintStrRmItems)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_119.3">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_119.3' /></span> <span class='numbering'>3.</span> Is <a href="#" tooltip="An analysis of all costs associated with the life cycle of equipment or assets including design, manufacture, operation, maintenance, and disposal. This analysis is used to determine the best equipment or material based on the total life cycle cost, not just the original purchase price.">life cycle cost analysis</a> (LCCA) an inherent part of the <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">maintenance material</a> procurement decision making process?
					 					 <span><img id="Img11" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Yes</td> <td>&nbsp;&nbsp;No</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SiteWhsOrgLCCAStrRmItems, "Y") %></td> <td><%= Html.RadioButtonFor(model => model.SiteWhsOrgLCCAStrRmItems, "N") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgLCCAStrRmItems)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.1">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.1' /></span> <span class='numbering'>4.</span> What percent of MRO materials are <a href="#" tooltip="The assemblage of maintenance materials and tools in preparation for the execution of a maintenance job. These items are typically placed in a container or kit and delivered to the job site prior to the commencement of the maintenance work. The percentage delivered is calculated as a percent of the total material delivered to the job sites.">kitted</a> prior to work execution?
					 					 <span><img id="Img12" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteWhsOrgPcntKitted,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgPcntKitted)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Percent </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.2">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.2' /></span> <span class='numbering'>5.</span> What percent of MRO materials are delivered to the maintenance job <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a> (as opposed to <a href="#" tooltip="A first-line leader who provides direction to maintenance craft personnel. Also referred to as foreman, team leader, or group leader.">supervisor</a>y or <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel going to the <a href="#" tooltip="Storeroom warehouse for MRO materials.">MRO storeroom</a> to pick up materials)?
					 					 <span><img id="Img13" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteWhsOrgPcntDelivered,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgPcntDelivered)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Percent </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.4">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.4' /></span> <span class='numbering'>6.</span> What is the annual average stock-out (stock request unsatisfied at first request) percentage for the MRO inventory?
					 					 <span><img id="Img14" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<%: Html.EditorFor(model => model.SiteWhsOrgAvgStockOutPcnt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgAvgStockOutPcnt)%>
								
					
				 </div>
									
					<div class="ReportInUnit" > Percent </div>
								
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.5">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.5' /></span> <span class='numbering'>7.</span> Does the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a> apply a <a href="#" tooltip="A closed maintenance storeroom warehouse has controlled access. Parts and materials are issued to the requester by a storeroom clerk or attendant using a stores issue or similar request. The storeroom warehouse is physically secure so that it can be entered only through controlled access entry points.">closed storeroom</a> policy (storeroom is secure and only stores personnel allowed in)?
					 					 <span><img id="Img15" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Yes</td> <td>&nbsp;&nbsp;No</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SiteWhsOrgClosedStoredPolicy, "Y") %></td> <td><%= Html.RadioButtonFor(model => model.SiteWhsOrgClosedStoredPolicy, "N") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgClosedStoredPolicy)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.6">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.6' /></span> <span class='numbering'>8.</span> Are low cost/high volume MRO materials readily available without a stock issue request (free issue)?
					 					 <span><img id="Img16" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Yes</td> <td>&nbsp;&nbsp;No</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SiteWhsOrgLowCostFreeIssue, "Y") %></td> <td><%= Html.RadioButtonFor(model => model.SiteWhsOrgLowCostFreeIssue, "N") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgLowCostFreeIssue)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			  
					 
			    <div  class="questionblock" qid="QSTID_IVN_120.7">
			 					<div style="margin: 10px 5px;"><span  ><img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_IVN_120.7' /></span> <span class='numbering'>9.</span> Is the storeroom <a href="#" tooltip="The process of turning over the management and execution of a function to a third party. Examples of functions commonly outsourced include plant maintenance and the storeroom warehouse.">outsourced</a> to an MRO vendor or other similar third party?
					 					 <span><img id="Img17" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/></span>
					</div>
					<div style="display:inline-block;margin: 5px 5px;" class="editor-field">
								
				
														<table align="center" border=0 cellspacing=4><tr><td>Yes</td> <td>&nbsp;&nbsp;No</td></tr><tr><td><%= Html.RadioButtonFor(model => model.SiteWhsOrgOutsourced, "Y") %></td> <td><%= Html.RadioButtonFor(model => model.SiteWhsOrgOutsourced, "N") %></td></tr></table> <%: Html.ValidationMessageFor(model => model.SiteWhsOrgOutsourced)%>
								
					
				 </div>
												
								<!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
			  			  </div>  
			    <%: Html.HiddenFor(model=>model.CompanySID) %>
  </div> <!-- for last step --> 
  </div>  <!-- for wizard container -->
<%
} //for form using clause 
%>
 


<script type="text/javascript">  function loadHelpReferences(){var  objHelpRefs = Section_HelpLst;for (i = objHelpRefs.length -1; i >= 0; i--) {      var qidNm = objHelpRefs[i]['HelpID'];      if ($('li[qid^="' + qidNm + '"]').length) {        var $elm = $('li[qid^="' + qidNm + '"]');        $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');        $elm.find('img.tooltipBtn').css('visibility', 'visible');     }     else if ($('div[qid^="' + qidNm + '"]').length) {       $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');    }}} function formatResult(el,value){ var format='N'; if(el.attr('NumFormat')=='Int' )  format= 'n0'; if (el.attr('DecPlaces') != null) {      if (el.attr('NumFormat') == 'Number')          format = 'n' + el.attr('DecPlaces');}   if(value!=undefined && isNaN(value)) value = 0 ;    var tempval = Globalize.parseFloat(value.toString(),'en');  if(isFinite(tempval) ) { el.val(Globalize.format(tempval, format));    var idnm = '#_' + el.attr('id');     var $f= $(idnm);      if ($f.length){$f.val(el.val());}}else{ el.val('');}  }$("#SiteSparesInventory,#ExchangeRate ").bind("keyup",function(){var $result =$("#SiteSparesInventoryUSD");$result.calc("v1/v2", { v1: $("#SiteSparesInventory"), v2:$("#ExchangeRate")}); formatResult($result, $result.val()); /*$("#SiteSparesInventoryUSD").trigger('blur');*/      });if ($("#SiteSparesInventory").val()!= null && !isNaN($("#SiteSparesInventory").val()) )if ($("#ExchangeRate").val()!= null && !isNaN($("#ExchangeRate").val()) ){$("#SiteSparesInventory").trigger('keyup');}$("#SiteSparesPlantStoresIssues,#ExchangeRate ").bind("keyup",function(){var $result =$("#SiteSparesPlantStoresIssuesUSD");$result.calc("v1/v2", { v1: $("#SiteSparesPlantStoresIssues"), v2:$("#ExchangeRate")}); formatResult($result, $result.val()); /*$("#SiteSparesPlantStoresIssuesUSD").trigger('blur');*/      });if ($("#SiteSparesPlantStoresIssues").val()!= null && !isNaN($("#SiteSparesPlantStoresIssues").val()) )if ($("#ExchangeRate").val()!= null && !isNaN($("#ExchangeRate").val()) ){$("#SiteSparesPlantStoresIssues").trigger('keyup');}$("#SiteWhsOrgOpCost,#ExchangeRate ").bind("keyup",function(){var $result =$("#SiteWhsOrgOpCostUSD");$result.calc("v1/v2", { v1: $("#SiteWhsOrgOpCost"), v2:$("#ExchangeRate")}); formatResult($result, $result.val()); /*$("#SiteWhsOrgOpCostUSD").trigger('blur');*/      });if ($("#SiteWhsOrgOpCost").val()!= null && !isNaN($("#SiteWhsOrgOpCost").val()) )if ($("#ExchangeRate").val()!= null && !isNaN($("#ExchangeRate").val()) ){$("#SiteWhsOrgOpCost").trigger('keyup');}var secComments =  Section_Comments;var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';for(i = secComments.length-1;i >=0; i-- ){		var qidNm = secComments[i]['CommentID'];		if($('img[qid="' + qidNm + '"]').length){    		$('img[qid="' + qidNm + '"]').attr('src', redCmtImg);		}	}<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(  function (index, html) {   var gridId = $(this).closest('.grid').attr('id');   if ($.trim(html).indexOf('nbsp', 0) == -1) {        var newHTML = '<span> ' +       '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' +       ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html +      '</span>';      }    $(this).html(newHTML);   }  ); $('img[suid]').click(function(){ var suid = $(this).attr('suid'); var qid = $(this).attr('qid') ; recordComment(suid,qid); });</script>

</asp:Content> 
