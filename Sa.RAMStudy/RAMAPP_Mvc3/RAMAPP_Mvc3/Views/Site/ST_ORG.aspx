﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_ORGModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:01:35  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_ORG").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_ORG", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_ORG" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Management of the Reliability and Maintenance Function</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Site Support Personnel</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Organization Levels</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">Management of the <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Function</p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_ORG_100.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_100.1' /></span> <span class='numbering'>1.</span> Is there a corporate <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> and/or maintenance organization?
					 					 <span>
                                              <img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteOrgCompMaintOrg, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteOrgCompMaintOrg, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteOrgCompMaintOrg)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_ORG_100.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_100.2' /></span> <span class='numbering'>2.</span> If yes, to whom does the corporate <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> and maintenance organization report administratively (check the closest description)?
					 					 <span>
                                              <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.SiteOrgCompMaintOrgReportsTo, "VPMfg") %>&nbsp;Vice President Manufacturing</br> <%= Html.RadioButtonFor(model => model.SiteOrgCompMaintOrgReportsTo, "VPEng") %>&nbsp;Vice President Engineering</br> <%= Html.RadioButtonFor(model => model.SiteOrgCompMaintOrgReportsTo, "OthExec") %>&nbsp;Other Executive</br> <%: Html.ValidationMessageFor(model => model.SiteOrgCompMaintOrgReportsTo)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_ORG_100.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_100.3' /></span> <span class='numbering'>3.</span> Is there a single site maintenance manager/superintendent who has primary responsibility for maintenance?
					 					 <span>
                                              <img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteOrgOverallPlant, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteOrgOverallPlant, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteOrgOverallPlant)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_ORG_100.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_100.4' /></span> <span class='numbering'>4.</span> Who has primary responsibility for <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a> <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> administratively (check the closest function description)?
					 					 <span>
                                              <img id="Img4" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%= Html.RadioButtonFor(model => model.SiteOrgPrimaryLeader, "MM") %>&nbsp;Site Maintenance Manager</br> <%= Html.RadioButtonFor(model => model.SiteOrgPrimaryLeader, "RM") %>&nbsp;Site Reliability Manager</br> <%= Html.RadioButtonFor(model => model.SiteOrgPrimaryLeader, "PM") %>&nbsp;Site Operations/Production Manager</br> <%= Html.RadioButtonFor(model => model.SiteOrgPrimaryLeader, "TM") %>&nbsp;Site Engineering/Technical Manager</br> <%: Html.ValidationMessageFor(model => model.SiteOrgPrimaryLeader)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle"><a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> Support Personnel</p>
            <p><i>Report in the table below the total number of corporate and <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a> <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> and maintenance personnel for both company and contractor for the complete site. Enter 0 (zero) if no support personnel for a category. (Annualized Turnaround headcount included).</i></p>



            <div class="questionblock" qid="">


                <div id='ST_ORG_PERS' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CORP_120.2'><span class='numbering'>1.</span> <a href="#" tooltip="To allocate is to apportion for a specific purpose. For example, corporate RAM resources allocate their time to the plant sites they support. If six corporate reliability engineers are utilized to support three plants and their time is equally divided among the plant sites, then effectively two corporate reliability engineers support each plant.">Allocated</a> Corporate <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Manager, Director, or Executive</li>
                        <li class='gridrowtext' qid='QSTID_CORP_120.3'><span class='numbering'>2.</span> <a href="#" tooltip="To allocate is to apportion for a specific purpose. For example, corporate RAM resources allocate their time to the plant sites they support. If six corporate reliability engineers are utilized to support three plants and their time is equally divided among the plant sites, then effectively two corporate reliability engineers support each plant.">Allocated</a> Corporate <a href="#" tooltip="A technical staff professional employee with a degree from an accredited university responsible for applying engineering concepts through the study, evaluation, and implementation of life-cycle management of equipment with the expressed intention of increasing plant availability. Reliability engineers are well-versed in the usage of contemporary predictive methods used in condition based maintenance.">Reliability Engineers</a> or <a href="#" tooltip="A salaried non-degreed employee or contractor who has gained expertise through experience, is recognized for his/her reliability and maintenance knowledge, and is used in a similar capacity as an engineer.">Paraprofessionals</a></li>
                        <li class='gridrowtext' qid='QSTID_CORP_120.4'><span class='numbering'>3.</span> <a href="#" tooltip="To allocate is to apportion for a specific purpose. For example, corporate RAM resources allocate their time to the plant sites they support. If six corporate reliability engineers are utilized to support three plants and their time is equally divided among the plant sites, then effectively two corporate reliability engineers support each plant.">Allocated</a> Corporate <a href="#" tooltip="A maintenance technical staff professional employee with a degree from an accredited university responsible for applying engineering concepts to the optimization of equipment, maintenance resources, and maintenance expenses to achieve improved maintainability, resource efficiency, and cost effectiveness.">Maintenance Engineers</a> or <a href="#" tooltip="A salaried non-degreed employee or contractor who has gained expertise through experience, is recognized for his/her reliability and maintenance knowledge, and is used in a similar capacity as an engineer.">Paraprofessionals</a></li>
                        <li class='gridrowtext' qid='QSTID_ORG_120.1'><span class='numbering'>4.</span> Site Maintenance Managers and Superintendents</li>
                        <li class='gridrowtext' qid='QSTID_ORG_120.2'><span class='numbering'>5.</span> Site Maintenance <a href="#" tooltip="A first-line leader who provides direction to maintenance craft personnel. Also referred to as foreman, team leader, or group leader.">Supervisors</a>, Foremen, or Team Leaders</li>
                        <li class='gridrowtext' qid='QSTID_ORG_120.3'><span class='numbering'>6.</span> Site <a href="#" tooltip="A formally trained maintenance professional who identifies labor, materials, tools, and safety requirements for maintenance work orders. The planner assembles this information into a job plan package and communicates it to the maintenance supervisor and/or craft workers prior to the start of the work.">Maintenance Planners</a></li>
                        <li class='gridrowtext' qid='QSTID_ORG_120.4'><span class='numbering'>7.</span> Site <a href="#" tooltip="A designated position within the maintenance work flow process that acts as a liaison between Operations and Maintenance to schedule maintenance activities. The scheduler schedules maintenance work based on plant priorities and communicates the schedule to all the stakeholders. A core metric for maintenance scheduling is schedule compliance measured on a daily and/or weekly basis.">Maintenance Schedulers</a> and/or Coordinators</li>
                        <li class='gridrowtext' qid='QSTID_ORG_120.5'><span class='numbering'>8.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> <a href="#" tooltip="A technical staff professional employee with a degree from an accredited university responsible for applying engineering concepts through the study, evaluation, and implementation of life-cycle management of equipment with the expressed intention of increasing plant availability. Reliability engineers are well-versed in the usage of contemporary predictive methods used in condition based maintenance.">Reliability Engineers</a> or <a href="#" tooltip="A salaried non-degreed employee or contractor who has gained expertise through experience, is recognized for his/her reliability and maintenance knowledge, and is used in a similar capacity as an engineer.">Paraprofessionals</a> (include test and inspection personnel)</li>
                        <li class='gridrowtext' qid='QSTID_ORG_120.6'><span class='numbering'>9.</span> Site <a href="#" tooltip="A maintenance technical staff professional employee with a degree from an accredited university responsible for applying engineering concepts to the optimization of equipment, maintenance resources, and maintenance expenses to achieve improved maintainability, resource efficiency, and cost effectiveness.">Maintenance Engineers</a> or <a href="#" tooltip="A salaried non-degreed employee or contractor who has gained expertise through experience, is recognized for his/her reliability and maintenance knowledge, and is used in a similar capacity as an engineer.">Paraprofessionals</a></li>
                        <li class='gridrowtext' qid='QSTID_ORG_120.7'><span class='numbering'>10.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Administrative Assistants, Secretaries or Clerks</li>
                        <li class='gridrowtext' qid='QSTID_ORG_122.5'><span class='numbering'>11.</span> <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> Material and Services Procurement Managers and Staff</li>
                        <li class='gridrowtext' qid='QSTID_ORG_122.8'><span class='numbering'>12.</span> Site <a href="#" tooltip="A storage warehouse or facility where maintenance and repair materials are stored. This includes equipment spare parts, maintenance consumables, and spare equipment.">Maintenance Storeroom</a>/Warehouse Managers or <a href="#" tooltip="A first-line leader who provides direction to maintenance craft personnel. Also referred to as foreman, team leader, or group leader.">Supervisors</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_ORG_120.8'>Total <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a> and Maintenance Support Personnel</li>
                        <li class='gridrowtext' qid='QSTID_ORG_122.8'><span class='numbering'>13.</span> Average Years of Experience of Reliability Engineers or Reliability Paraprofessionals</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Company</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_120.2" /><%: Html.EditorFor(model => model.CorpPersMaintMgr_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumSitePersSupTot sumSitePersSupCO sumCorpCRCO_A" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_120.3" /><%: Html.EditorFor(model => model.CorpPersRelEngr_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumSitePersSupTot sumSitePersSupCO sumCorpCRCO_B" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_120.4" /><%: Html.EditorFor(model => model.CorpPersMaintEngr_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumSitePersSupTot sumSitePersSupCO sumCorpCRCO_C" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.1" /><%: Html.EditorFor(model => model.SitePersMaintMgr_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumCRCO_D sumSitePersSupTot sumSitePersSupCO" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.2" /><%: Html.EditorFor(model => model.SitePersMaintSupv_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumCRCO_E sumSitePersSupTot sumSitePersSupCO" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.3" /><%: Html.EditorFor(model => model.SitePersMaintPlanner_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumCRCO_F sumSitePersSupTot sumSitePersSupCO" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.4" /><%: Html.EditorFor(model => model.SitePersMaintSched_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumCRCO_G sumSitePersSupTot sumSitePersSupCO" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.5" /><%: Html.EditorFor(model => model.SitePersTechnical_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumCRCO_H sumSitePersSupTot sumSitePersSupCO" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.6" /><%: Html.EditorFor(model => model.SitePersMaintEngr_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumCRCO_I sumSitePersSupCO sumSitePersSupTot" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.7" /><%: Html.EditorFor(model => model.SitePersMaintAdmin_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumCRCO_J sumSitePersSupTot sumSitePersSupCO" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.5" /><%: Html.EditorFor(model => model.SitePersMaintMSPMStaff_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumSitePersSupTot sumSitePersSupCO sumCRCO_K" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.8" /><%: Html.EditorFor(model => model.SitePersMaintStoreroomMgrs_CO,"DoubleTmpl",new {size="10" ,CalcTag="sumCoSpt sumSitePersSupTot sumSitePersSupCO sumCRCO_L" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.8" /><%: Html.EditorFor(model => model.SitePersTotSupport_CO,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_124' /></span><br />
                            <select id="CorpPersRelEngrExpr" name="CorpPersRelEngrExpr" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0-5 years' <%= Model.CorpPersRelEngrExpr != null ?(Model.CorpPersRelEngrExpr.Equals("0-5 years")? "SELECTED" : String.Empty):String.Empty %>>0-5 years</option>
                                <option value='6-10 years' <%= Model.CorpPersRelEngrExpr != null ?(Model.CorpPersRelEngrExpr.Equals("6-10 years")? "SELECTED" : String.Empty):String.Empty %>>6-10 years</option>
                                <option value='11-15 years' <%= Model.CorpPersRelEngrExpr != null ?(Model.CorpPersRelEngrExpr.Equals("11-15 years")? "SELECTED" : String.Empty):String.Empty %>>11-15 years</option>
                                <option value='16-20 years' <%= Model.CorpPersRelEngrExpr != null ?(Model.CorpPersRelEngrExpr.Equals("16-20 years")? "SELECTED" : String.Empty):String.Empty %>>16-20 years</option>
                                <option value='>20 years' <%= Model.CorpPersRelEngrExpr != null ?(Model.CorpPersRelEngrExpr.Equals(">20 years")? "SELECTED" : String.Empty):String.Empty %>>>20 years</option>
                            </select>
                        </li>

                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Contractor</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_130.2" /><%: Html.EditorFor(model => model.CorpPersMaintMgr_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumSitePersSupTot sumCorpCRCO_A sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_130.3" /><%: Html.EditorFor(model => model.CorpPersRelEngr_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumSitePersSupTot sumCorpCRCO_B sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_130.4" /><%: Html.EditorFor(model => model.CorpPersMaintEngr_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumSitePersSupTot sumCorpCRCO_C sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_120.9" /><%: Html.EditorFor(model => model.SitePersMaintMgr_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRCO_D sumCRSpt sumSitePersSupTot sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121" /><%: Html.EditorFor(model => model.SitePersMaintSupv_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRCO_E sumCRSpt sumSitePersSupTot sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.1" /><%: Html.EditorFor(model => model.SitePersMaintPlanner_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRCO_F sumCRSpt sumSitePersSupTot sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.2" /><%: Html.EditorFor(model => model.SitePersMaintSched_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRCO_G sumCRSpt sumSitePersSupTot sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.3" /><%: Html.EditorFor(model => model.SitePersTechnical_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRCO_H sumCRSpt sumSitePersSupTot sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.4" /><%: Html.EditorFor(model => model.SitePersMaintEngr_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRCO_I sumCRSpt sumSitePersSupTot sumSitePersSupCR" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.5" /><%: Html.EditorFor(model => model.SitePersMaintAdmin_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRCO_J sumCRSpt sumSitePersSupCR sumSitePersSupTot" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.6" /><%: Html.EditorFor(model => model.SitePersMaintMSPMStaff_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRSpt sumSitePersSupTot sumSitePersSupCR sumCRCO_K" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.9" /><%: Html.EditorFor(model => model.SitePersMaintStoreroomMgrs_CR,"DoubleTmpl",new {size="10" ,CalcTag="sumCRSpt sumSitePersSupTot sumSitePersSupCR sumCRCO_L" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.6" /><%: Html.EditorFor(model => model.SitePersTotSupport_CR,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_150.2" /><%: Html.EditorFor(model => model.CorpPersMaintMgr_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_150.3" /><%: Html.EditorFor(model => model.CorpPersRelEngr_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CORP_150.4" /><%: Html.EditorFor(model => model.CorpPersMaintEngr_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.7" /><%: Html.EditorFor(model => model.SitePersMaintMgr_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.8" /><%: Html.EditorFor(model => model.SitePersMaintSupv_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_121.9" /><%: Html.EditorFor(model => model.SitePersMaintPlanner_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122" /><%: Html.EditorFor(model => model.SitePersMaintSched_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.1" /><%: Html.EditorFor(model => model.SitePersTechnical_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.2" /><%: Html.EditorFor(model => model.SitePersMaintEngr_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.3" /><%: Html.EditorFor(model => model.SitePersMaintAdmin_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.7" /><%: Html.EditorFor(model => model.SitePersMaintMSPMStaff_TOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_123" /><%: Html.EditorFor(model => model.SitePersMaintStoreroomMgrs_TOT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_ORG_122.4" /><%: Html.EditorFor(model => model.SitePersTotSupport_Tot,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.CorpPersMaintMgr_CO)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersRelEngr_CO)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersMaintEngr_CO)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersMaintMgr_CR)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersRelEngr_CR)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersMaintEngr_CR)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersMaintMgr_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersRelEngr_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.CorpPersMaintEngr_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintMgr_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintSupv_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintPlanner_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintSched_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersTechnical_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintEngr_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintAdmin_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersTotSupport_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintMgr_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintSupv_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintPlanner_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintSched_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersTechnical_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintEngr_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintAdmin_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersTotSupport_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintMgr_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintSupv_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintPlanner_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintSched_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersTechnical_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintEngr_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintAdmin_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersTotSupport_Tot)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintMSPMStaff_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintMSPMStaff_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintMSPMStaff_TOT)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintStoreroomMgrs_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintStoreroomMgrs_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SitePersMaintStoreroomMgrs_TOT)%>
                <%: Html.ValidationMessageFor(model => model.CorpPersRelEngrExpr)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle">Organization Levels</p>
            <p><i>Example: <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> General Manager, Technical Manager, Maintenance Manager, Maintenance Superintendent, <a href="#" tooltip="A first-line leader who provides direction to maintenance craft personnel. Also referred to as foreman, team leader, or group leader.">Supervisor</a>, and <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> worker are six levels.</i></p>



            <div class="questionblock" qid="QSTID_ORG_129.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_129.2' /></span> <span class='numbering'>1.</span> Report the number of organization levels from the President/CEO to the Maintenance Manager including these two positions.
					 					 <span>
                                              <img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteOrgUpperLevels,"IntTmpl",new {size="10" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.SiteOrgUpperLevels)%>
                </div>

                <div class="ReportInUnit">Number </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_ORG_129.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_129.3' /></span> <span class='numbering'>2.</span> Report the number of organization levels from the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a> General Manager to the <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> worker including these two positions.
					 					 <span>
                                              <img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteOrgLevelsGMtoCraft,"IntTmpl",new {size="10" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.SiteOrgLevelsGMtoCraft)%>
                </div>

                <div class="ReportInUnit">Number </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_ORG_129.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_ORG_129.4' /></span> <span class='numbering'>3.</span> Total Number of Levels Between CEO and <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a>
                    <span>
                        <img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteOrgLevelsCEOtoCraft,"IntTmpl",new {size="10" ,NumFormat="Int"})%> <%: Html.ValidationMessageFor(model => model.SiteOrgLevelsCEOtoCraft)%>
                </div>

                <div class="ReportInUnit">Number </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



    <script type="text/javascript">  function loadHelpReferences() { var objHelpRefs = Section_HelpLst; for (i = objHelpRefs.length - 1; i >= 0; i--) { var qidNm = objHelpRefs[i]['HelpID']; if ($('li[qid^="' + qidNm + '"]').length) { var $elm = $('li[qid^="' + qidNm + '"]'); $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>'); $elm.find('img.tooltipBtn').css('visibility', 'visible'); } else if ($('div[qid^="' + qidNm + '"]').length) { $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible'); } } } function formatResult(el, value) { var format = 'N'; if (el.attr('NumFormat') == 'Int') format = 'n0'; if (el.attr('DecPlaces') != null) { if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces'); } if (value != undefined && isNaN(value)) value = 0; var tempval = Globalize.parseFloat(value.toString(), 'en'); if (isFinite(tempval)) { el.val(Globalize.format(tempval, format)); var idnm = '#_' + el.attr('id'); var $f = $(idnm); if ($f.length) { $f.val(el.val()); } } else { el.val(''); } } $("input[CalcTag*='sumCorpCRCO_A']").sum({ bind: "keyup", selector: "#CorpPersMaintMgr_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#CorpPersMaintMgr_Tot').attr('readonly', 'readonly'); $('#CorpPersMaintMgr_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCorpCRCO_B']").sum({ bind: "keyup", selector: "#CorpPersRelEngr_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#CorpPersRelEngr_Tot').attr('readonly', 'readonly'); $('#CorpPersRelEngr_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCorpCRCO_C']").sum({ bind: "keyup", selector: "#CorpPersMaintEngr_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#CorpPersMaintEngr_Tot').attr('readonly', 'readonly'); $('#CorpPersMaintEngr_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_D']").sum({ bind: "keyup", selector: "#SitePersMaintMgr_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintMgr_Tot').attr('readonly', 'readonly'); $('#SitePersMaintMgr_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_E']").sum({ bind: "keyup", selector: "#SitePersMaintSupv_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintSupv_Tot').attr('readonly', 'readonly'); $('#SitePersMaintSupv_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_F']").sum({ bind: "keyup", selector: "#SitePersMaintPlanner_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintPlanner_Tot').attr('readonly', 'readonly'); $('#SitePersMaintPlanner_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_G']").sum({ bind: "keyup", selector: "#SitePersMaintSched_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintSched_Tot').attr('readonly', 'readonly'); $('#SitePersMaintSched_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_H']").sum({ bind: "keyup", selector: "#SitePersTechnical_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersTechnical_Tot').attr('readonly', 'readonly'); $('#SitePersTechnical_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_I']").sum({ bind: "keyup", selector: "#SitePersMaintEngr_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintEngr_Tot').attr('readonly', 'readonly'); $('#SitePersMaintEngr_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_J']").sum({ bind: "keyup", selector: "#SitePersMaintAdmin_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintAdmin_Tot').attr('readonly', 'readonly'); $('#SitePersMaintAdmin_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumSitePersSupCO']").sum({ bind: "keyup", selector: "#SitePersTotSupport_CO", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersTotSupport_CO').attr('readonly', 'readonly'); $('#SitePersTotSupport_CO').change(function () { $(this).validate(); }); $("input[CalcTag*='sumSitePersSupCR']").sum({ bind: "keyup", selector: "#SitePersTotSupport_CR", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersTotSupport_CR').attr('readonly', 'readonly'); $('#SitePersTotSupport_CR').change(function () { $(this).validate(); }); $("input[CalcTag*='sumSitePersSupTot']").sum({ bind: "keyup", selector: "#SitePersTotSupport_Tot", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersTotSupport_Tot').attr('readonly', 'readonly'); $('#SitePersTotSupport_Tot').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_K']").sum({ bind: "keyup", selector: "#SitePersMaintMSPMStaff_TOT", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintMSPMStaff_TOT').attr('readonly', 'readonly'); $('#SitePersMaintMSPMStaff_TOT').change(function () { $(this).validate(); }); $("input[CalcTag*='sumCRCO_L']").sum({ bind: "keyup", selector: "#SitePersMaintStoreroomMgrs_TOT", oncalc: function (value, settings) { if (value != undefined && !isNaN(value)) { formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/ } } }); $('#SitePersMaintStoreroomMgrs_TOT').attr('readonly', 'readonly'); $('#SitePersMaintStoreroomMgrs_TOT').change(function () { $(this).validate(); }); var secComments = Section_Comments; var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>'; for (i = secComments.length - 1; i >= 0; i--) { var qidNm = secComments[i]['CommentID']; if ($('img[qid="' + qidNm + '"]').length) { $('img[qid="' + qidNm + '"]').attr('src', redCmtImg); } }<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(function (index, html) { var gridId = $(this).closest('.grid').attr('id'); if ($.trim(html).indexOf('nbsp', 0) == -1) { var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>'; } $(this).html(newHTML); }); $('img[suid]').click(function () { var suid = $(this).attr('suid'); var qid = $(this).attr('qid'); recordComment(suid, qid); });</script>

</asp:Content>
