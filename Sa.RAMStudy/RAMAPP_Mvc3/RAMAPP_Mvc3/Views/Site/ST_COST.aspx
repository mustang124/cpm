<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_COSTModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 09:57:52  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_COST").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_COST", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_COST" class="stContainer" style="width: 1200px;">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Direct Routine Maintenance Costs</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Indirect Routine Maintenance Costs</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Routine Maintenance Expense Labor Hours</small> </span></a></li>
            <li><a href='#step-4'>
                <label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br />
                    <small>Routine Maintenance Capital Labor Hours</small> </span></a></li>
            <li><a href='#step-5'>
                <label class='stepNumber'>5</label><span class='stepDesc'>Section 5<br />
                    <small>Direct Annualized T/A Maintenance Costs</small> </span></a></li>
            <li><a href='#step-6'>
                <label class='stepNumber'>6</label><span class='stepDesc'>Section 6<br />
                    <small>Indirect Annualized T/A Maintenance Costs</small> </span></a></li>
            <li><a href='#step-7'>
                <label class='stepNumber'>7</label><span class='stepDesc'>Section 7<br />
                    <small>T/A Maintenance Expense Labor Hours</small> </span></a></li>
            <li><a href='#step-8'>
                <label class='stepNumber'>8</label><span class='stepDesc'>Section 8<br />
                    <small>T/A Maintenance Capital Labor Hours</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">Direct <a href="#" tooltip="The cost of maintenance craft labor, repair material, and overhead incurred through the execution of routine maintenance.">Routine Maintenance Costs</a></p>
            <p><i>Report all values in local currency.</i></p>



            <div style="display: block;" class="questionblock" qid="QSTID_CST_Hide1">


                <%= Html.HiddenFor(model => model._ExchangeRate, new {id="_ExchangeRate"}) %>
                <div id='ST_RT_DR' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CST_120.2'><span class='numbering'>1.</span> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Company <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Employees</li>
                        <li class='gridrowtext' qid='QSTID_CST_120.3'><span class='numbering'>2.</span> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Contractor <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_120.4'>Total Direct <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Labor </a>Costs</li>
                        <li class='gridrowtext' qid='QSTID_CST_120.5'><span class='numbering'>3.</span> Direct <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Materials</a> � Supplied by the Company</li>
                        <li class='gridrowtext' qid='QSTID_CST_120.6'><span class='numbering'>4.</span> Direct <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Materials</a> � Supplied by Contractors</li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_120.7'>Total Direct <a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Material </a>Costs</li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_120.8'>Total Direct <a href="#" tooltip="The cost of maintenance craft labor, repair material, and overhead incurred through the execution of routine maintenance.">Routine Maintenance Costs</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Expense</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.2" /><%: Html.EditorFor(model => model.SiteExpCompLabor_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumLbrRTExp sumSiteTotExpRT sumSiteTotCostRT sumLbrCORTTot sumLbrRTTot sumExpDirTot sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.3" /><%: Html.EditorFor(model => model.SiteExpContLabor_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteTotExpRT sumSiteTotCostRT sumLbrRTExp sumLbrCTRTTot sumLbrRTTot sumExpDirTot sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.4" /><%: Html.EditorFor(model => model.SiteExpTotLabor_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.5" /><%: Html.EditorFor(model => model.SiteExpCompMatl_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumMtlRTExp sumSiteTotExpRT sumSiteTotCostRT sumMtlRTTot sumExpDirTot sumMatlCompRT sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.6" /><%: Html.EditorFor(model => model.SiteExpContMatl_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumMtlRTExp sumSiteTotExpRT sumSiteTotCostRT sumMtlRTTot sumExpDirTot sumMatlContRT sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.7" /><%: Html.EditorFor(model => model.SiteExpTotMatl_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.8" /><%: Html.EditorFor(model => model.SiteExpTotDirect_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Capital</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_120.9" /><%: Html.EditorFor(model => model.SiteMCptlCompLabor_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteTotCostRT sumSiteTotMCptlRT sumLbrRTCap sumLbrCORTTot sumLbrRTTot sumMtCapDirTot sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121" /><%: Html.EditorFor(model => model.SiteMCptlContLabor_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteTotCostRT sumSiteTotMCptlRT sumLbrRTCap sumLbrCTRTTot sumLbrRTTot sumMtCapDirTot sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.1" /><%: Html.EditorFor(model => model.SiteMCptlTotLabor_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.2" /><%: Html.EditorFor(model => model.SiteMCptlCompMatl_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteTotCostRT sumSiteTotMCptlRT sumMtlRTCap sumMtlRTTot sumMtCapDirTot sumMatlCompRT sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.3" /><%: Html.EditorFor(model => model.SiteMCptlContMatl_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteTotCostRT sumSiteTotMCptlRT sumMtlRTCap sumMtlRTTot sumMtCapDirTot sumMatlContRT sumTotRTDir" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.4" /><%: Html.EditorFor(model => model.SiteMCptlTotMatl_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.5" /><%: Html.EditorFor(model => model.SiteMCptlTotDirectCost_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.6" /><%: Html.EditorFor(model => model.SiteCostsCompLabor_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.7" /><%: Html.EditorFor(model => model.SiteCostsContLabor_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.8" /><%: Html.EditorFor(model => model.SiteCostsTotLabor_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_121.9" /><%: Html.EditorFor(model => model.SiteCostsCompMatl_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_122" /><%: Html.EditorFor(model => model.SiteCostsContMatl_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_122.1" /><%: Html.EditorFor(model => model.SiteCostsTotMatl_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_122.2" /><%: Html.EditorFor(model => model.SiteCostsTotDirect_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteExpCompLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpContLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpCompMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpContMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotDirect_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotDirectCost_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsCompLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsContLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotLabor_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsCompMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsContMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotMatl_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotDirect_RT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle"><a href="#" tooltip="Maintenance supervision and staff costs, both company and contractor including all employee benefits such as paid time off for vacation, holidays, sickness, union representation, workman or personal leave, hospitalization, life and other insurance, retirement or pension program costs, recreational or educational program costs, payroll social security taxes paid by company, company uniforms, safety glasses or shoes, sports or social clubs, overtime or subsidized meals, subsidized transportation, company stock or profit sharing programs, company cars, subsidized loans, accommodations, and any other employee benefit program.">Indirect Routine Maintenance Costs</a></p>
            <p><i>Report all values in local currency.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_RT_IN' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CST_140.2'><span class='numbering'>1.</span> Maintenance Support/Overhead � Company</li>
                        <li class='gridrowtext' qid='QSTID_CST_140.3'><span class='numbering'>2.</span> Maintenance Support/Overhead � Contractor</li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_140.4'>Total <a href="#" tooltip="Maintenance supervision and staff costs, both company and contractor including all employee benefits such as paid time off for vacation, holidays, sickness, union representation, workman or personal leave, hospitalization, life and other insurance, retirement or pension program costs, recreational or educational program costs, payroll social security taxes paid by company, company uniforms, safety glasses or shoes, sports or social clubs, overtime or subsidized meals, subsidized transportation, company stock or profit sharing programs, company cars, subsidized loans, accommodations, and any other employee benefit program.">Indirect Routine Maintenance Costs</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_141.1'>Total Direct and <a href="#" tooltip="Maintenance supervision and staff costs, both company and contractor including all employee benefits such as paid time off for vacation, holidays, sickness, union representation, workman or personal leave, hospitalization, life and other insurance, retirement or pension program costs, recreational or educational program costs, payroll social security taxes paid by company, company uniforms, safety glasses or shoes, sports or social clubs, overtime or subsidized meals, subsidized transportation, company stock or profit sharing programs, company cars, subsidized loans, accommodations, and any other employee benefit program.">Indirect Routine Maintenance Costs</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Expense</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.2" /><%: Html.EditorFor(model => model.SiteExpIndirectCompSupport_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSpptRTExp sumSiteTotExpRT sumSiteTotCostRT sumCOSpptRTExp sumTotIndrRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.3" /><%: Html.EditorFor(model => model.SiteExpIndirectContSupport_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSpptRTExp sumSiteTotExpRT sumSiteTotCostRT sumCTSpptRTExp sumTotIndrRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.4" /><%: Html.EditorFor(model => model.SiteExpTotIndirect_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_141.1" /><%: Html.EditorFor(model => model.SiteExpTotMaint_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Capital</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.5" /><%: Html.EditorFor(model => model.SiteMCptlCompIndirectCost_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSpptRTCap sumSiteTotCostRT sumSiteTotMCptlRT sumCOSpptRTExp sumTotIndrRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.6" /><%: Html.EditorFor(model => model.SiteMCptlContIndirectCost_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSpptRTCap sumSiteTotCostRT sumSiteTotMCptlRT sumCTSpptRTExp sumTotIndrRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.7" /><%: Html.EditorFor(model => model.SiteMCptlTotIndirectCost_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_141.2" /><%: Html.EditorFor(model => model.SiteMCptlTotCost_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.8" /><%: Html.EditorFor(model => model.SiteCostsIndirectCompSupport_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_140.9" /><%: Html.EditorFor(model => model.SiteCostsIndirectContSupport_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_141" /><%: Html.EditorFor(model => model.SiteCostsTotIndirect_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_141.3" /><%: Html.EditorFor(model => model.SiteCostsTotMaint_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteExpIndirectCompSupport_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpIndirectContSupport_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotIndirect_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompIndirectCost_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContIndirectCost_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotIndirectCost_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsIndirectCompSupport_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsIndirectContSupport_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotIndirect_RT)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotMaint_RT)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotCost_RT)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotMaint_RT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle"><a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> Expense Labor Hours</p>
            <p><i></i></p>



            <div class="questionblock" qid="">


                <div id='ST_RT_EXP_LH' class='grid editor-field'>
                    <ul class="rowdesc" style="width: 270px">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CST_160.2'><span class='numbering'>1.</span> Routine Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Company <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Employees</li>
                        <li class='gridrowtext' qid='QSTID_CST_160.3'><span class='numbering'>2.</span> Routine Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Contractor <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></li>
                        <li class='gridrowtext' qid='QSTID_CST_160.4'><span class='numbering'>3.</span> Calculated combined (Company plus Contractor)</li>
                        <li class='gridrowtext' qid='QSTID_CST_181.4'><span class='numbering'>4.</span> % of FTE Contractors on site for routine expense maintenance</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Reported Labor Cost</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.2" /><%: Html.EditorFor(model => model._SiteExpCompLabor_RT,"DoubleTmpl",new {size="10" ,id="_SiteExpCompLabor_RT" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.3" /><%: Html.EditorFor(model => model._SiteExpContLabor_RT,"DoubleTmpl",new {size="10" ,id="_SiteExpContLabor_RT" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.4" /><%: Html.EditorFor(model => model._SiteExpTotLabor_RT,"DoubleTmpl",new {size="10" ,id="_SiteExpTotLabor_RT" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Labor Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.5" /><%: Html.EditorFor(model => model.SiteExpCompManHrs_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteExpManHrsRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.6" /><%: Html.EditorFor(model => model.SiteExpContManHrs_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteExpManHrsRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.7" /><%: Html.EditorFor(model => model.SiteExpTotManHrs_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181.4" /><%: Html.EditorFor(model => model.SiteMCptlPctFTEContractors,"DoubleTmpl",new {size="10", id="SiteMCptlPctFTEContractors" ,NumFormat="Number", DecPlaces="2" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Wage Rate - Local Currency/Hour</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.8" /><%: Html.EditorFor(model => model.SiteExpCompWageRate_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_160.9" /><%: Html.EditorFor(model => model.SiteExpContWageRate_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_161" /><%: Html.EditorFor(model => model.SiteExpAvgWageRate_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Wage Rate - USD/Hour</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_161.1" /><%: Html.EditorFor(model => model.SiteExpCompWageRateUSD_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_161.2" /><%: Html.EditorFor(model => model.SiteExpContWageRateUSD_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_161.3" /><%: Html.EditorFor(model => model.SiteExpAvgWageRateUSD_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model._SiteExpCompLabor_RT)%>
                <%: Html.ValidationMessageFor(model => model._SiteExpContLabor_RT)%>
                <%: Html.ValidationMessageFor(model => model._SiteExpTotLabor_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpCompManHrs_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpContManHrs_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpTotManHrs_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpCompWageRate_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpContWageRate_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpAvgWageRate_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpCompWageRateUSD_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpContWageRateUSD_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteExpAvgWageRateUSD_RT)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-4">

            <p class="StepTitle"><a href="#" tooltip="All maintenance work, both planned and unplanned, performed during normal run and maintain periods such as corrective repairs and replacements, preventive maintenance, and predictive (condition-based) maintenance. Includes maintenance capital work performed during normal run and maintain periods. Excludes all maintenance work executed during a plant or unit turnaround.">Routine Maintenance</a> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Capital </a>Labor Hours</p>
            <p><i></i></p>



            <div class="questionblock" qid="">
                <div id='ST_RT_CP_LH' class='grid editor-field'>
                    <table>
                        <tr>
                            <td>
                                <ul class="rowdesc">
                                    <li>&nbsp;</li>
                                    <li class='gridrowtext' qid='QSTID_CST_180.2'><span class='numbering'>1.</span> Routine Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Company <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Employees</li>
                                    <li class='gridrowtext' qid='QSTID_CST_180.3'><span class='numbering'>2.</span> Routine Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Contractor <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></li>
                                    <li class='gridrowtext' qid='QSTID_CST_180.4'><span class='numbering'>3.</span> Calculated combined (Company plus Contractor)</li>
                                    <li class='gridrowtext' qid='QSTID_CST_180'><span class='numbering'>4.</span> % of FTE Contractors on site for routine capital maintenance</li>
                                </ul>
                            </td>
                            <td>
                                <ul class="gridbody">
                                    <li class="columndesc">Reported Labor Cost</li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.2" /><%: Html.EditorFor(model => model._SiteMCptlCompLabor_RT,"DoubleTmpl",new {size="10" ,id="_SiteMCptlCompLabor_RT" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.3" /><%: Html.EditorFor(model => model._SiteMCptlContLabor_RT,"DoubleTmpl",new {size="10" ,id="_SiteMCptlContLabor_RT" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.4" /><%: Html.EditorFor(model => model._SiteMCptlTotLabor_RT,"DoubleTmpl",new {size="10" ,id="_SiteMCptlTotLabor_RT" ,NumFormat="Number" ,DecPlaces="0"  ,@class="formfld",ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <%--                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180" /><%: Html.EditorFor(model => model.SiteMCptlMetricLabor_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%>--%>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="gridbody">
                                    <li class="columndesc">Labor Hours</li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.5" /><%: Html.EditorFor(model => model.SiteMCptlCompManHrs_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCptlManHrsRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.6" /><%: Html.EditorFor(model => model.SiteMCptlContManHrs_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCptlManHrsRT" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.7" /><%: Html.EditorFor(model => model.SiteMCptlTotManHrs_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181.5" /><%: Html.EditorFor(model => model.SiteMCptlMetricManHrs_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%></li>
                                </ul>
                            </td>
                            <td>
                                <ul class="gridbody">
                                    <li class="columndesc">Wage Rate - Local Currency/Hour</li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.8" /><%: Html.EditorFor(model => model.SiteMCptlCompWageRate_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_180.9" /><%: Html.EditorFor(model => model.SiteMCptlContWageRate_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181" /><%: Html.EditorFor(model => model.SiteMCptlAvgWageRate_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <%--                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181.6" /><%: Html.EditorFor(model => model.SiteMCptlMetricWageRate_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%>--%>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="gridbody">
                                    <li class="columndesc">Wage Rate - USD/Hour</li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181.1" /><%: Html.EditorFor(model => model.SiteMCptlCompWageRateUSD_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181.2" /><%: Html.EditorFor(model => model.SiteMCptlContWageRateUSD_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181.3" /><%: Html.EditorFor(model => model.SiteMCptlAvgWageRateUSD_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                                    <li class="gridrow">
                                        <%--                                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_181.7" /><%: Html.EditorFor(model => model.SiteMCptlMetricWageRateUSD_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="formfld" ,ReadOnly="True"})%>--%>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
                <%: Html.ValidationMessageFor(model => model._SiteMCptlCompLabor_RT)%>
                <%: Html.ValidationMessageFor(model => model._SiteMCptlContLabor_RT)%>
                <%: Html.ValidationMessageFor(model => model._SiteMCptlTotLabor_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlCompManHrs_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlContManHrs_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlTotManHrs_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlCompWageRate_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlContWageRate_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlAvgWageRate_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlCompWageRateUSD_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlContWageRateUSD_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlAvgWageRateUSD_RT)%>
                <%: Html.ValidationMessageFor(model => model.SiteMCptlPctFTEContractors)%>

                <%--  <div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset >--%>
            </div>

        </div>

        <div class="SubSectionTab" id="step-5">

            <p class="StepTitle">Direct <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Maintenance Costs</p>
            <p><i>Report all values in local currency.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_TA_DR' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CST_200.1'><span class='numbering'>1.</span> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Company <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Employees</li>
                        <li class='gridrowtext' qid='QSTID_CST_200.2'><span class='numbering'>2.</span> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Contractor <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_200.3'>Total Direct <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> Costs</li>
                        <li class='gridrowtext' qid='QSTID_CST_200.4'><span class='numbering'>3.</span> Direct <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Materials</a> � Supplied by the Company</li>
                        <li class='gridrowtext' qid='QSTID_CST_200.5'><span class='numbering'>4.</span> Direct <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Materials</a> � Supplied by Contractors</li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_200.6'>Total Direct <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Materials used for maintenance and repair of equipment regardless of the source. This includes stocked maintenance inventory usage, outside purchased materials, supplies, consumables, and the costs to repair spare components. Also includes materials used for capital expenditures directly related to end-of-life machinery replacement (this is necessary so that excessive replacement versus proper maintenance is not masked). Do not include material used for capital expenditures for plant expansions or improvements.">Maintenance Material</a> Costs</li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_200.7'>Total Direct <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Maintenance Costs</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Expense</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.1" /><%: Html.EditorFor(model => model.SiteExpCompLabor_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteExpLaborTA sumSiteCompLaborTA sumSiteTotLaborTA sumSiteExpDirTA sumSiteCostDirTA sumSiteExpTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.2" /><%: Html.EditorFor(model => model.SiteExpContLabor_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteExpLaborTA sumSiteContLaborTA sumSiteTotLaborTA sumSiteExpDirTA sumSiteCostDirTA sumSiteExpTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.3" /><%: Html.EditorFor(model => model.SiteExpTotLabor_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.4" /><%: Html.EditorFor(model => model.SiteExpCompMatl_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCompMatlTA sumSiteExpMatlTA sumSiteTotMatlTA sumSiteExpDirTA sumSiteCostDirTA sumSiteExpTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.5" /><%: Html.EditorFor(model => model.SiteExpContMatl_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteContMatlTA sumSiteExpMatlTA sumSiteTotMatlTA sumSiteExpDirTA sumSiteCostDirTA sumSiteExpTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.6" /><%: Html.EditorFor(model => model.SiteExpTotMatl_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.7" /><%: Html.EditorFor(model => model.SiteExpTotDirect_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Capital</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.8" /><%: Html.EditorFor(model => model.SiteMCptlCompLabor_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCptlLaborTA sumSiteCompLaborTA sumSiteTotLaborTA sumSiteCostDirTA sumSiteCptlDirTA sumSiteCptlTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_200.9" /><%: Html.EditorFor(model => model.SiteMCptlContLabor_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCptlLaborTA sumSiteContLaborTA sumSiteTotLaborTA sumSiteCostDirTA sumSiteCptlDirTA sumSiteCptlTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201" /><%: Html.EditorFor(model => model.SiteMCptlTotLabor_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.1" /><%: Html.EditorFor(model => model.SiteMCptlCompMatl_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCompMatlTA sumSiteCptlMatlTA sumSiteTotMatlTA sumSiteCostDirTA sumSiteCptlDirTA sumSiteCptlTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.2" /><%: Html.EditorFor(model => model.SiteMCptlContMatl_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteContMatlTA sumSiteCptlMatlTA sumSiteTotMatlTA sumSiteCostDirTA sumSiteCptlDirTA sumSiteCptlTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.3" /><%: Html.EditorFor(model => model.SiteMCptlTotMatl_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.4" /><%: Html.EditorFor(model => model.SiteMCptlTotDirectCost_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.5" /><%: Html.EditorFor(model => model.SiteCostsCompLabor_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.6" /><%: Html.EditorFor(model => model.SiteCostsContLabor_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.7" /><%: Html.EditorFor(model => model.SiteCostsTotLabor_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.8" /><%: Html.EditorFor(model => model.SiteCostsCompMatl_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_201.9" /><%: Html.EditorFor(model => model.SiteCostsContMatl_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_202" /><%: Html.EditorFor(model => model.SiteCostsTotMatl_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_202.1" /><%: Html.EditorFor(model => model.SiteCostsTotDirect_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteExpCompLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpContLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpCompMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpContMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotDirect_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotDirectCost_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsCompLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsContLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsCompMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsContMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotMatl_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotDirect_TA)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-6">

            <p class="StepTitle"><a href="#" tooltip="Maintenance supervision and staff costs, both company and contractor including all employee benefits such as paid time off for vacation, holidays, sickness, union representation, workman or personal leave, hospitalization, life and other insurance, retirement or pension program costs, recreational or educational program costs, payroll social security taxes paid by company, company uniforms, safety glasses or shoes, sports or social clubs, overtime or subsidized meals, subsidized transportation, company stock or profit sharing programs, company cars, subsidized loans, accommodations, and any other employee benefit program.">Indirect Annualized T/A Maintenance Costs</a></p>
            <p><i>Report all values in local currency.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_TA_IN' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CST_220.2'><span class='numbering'>1.</span> Maintenance Support/Overhead � Company</li>
                        <li class='gridrowtext' qid='QSTID_CST_220.3'><span class='numbering'>2.</span> Maintenance Support/Overhead � Contractor</li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_220.4'>Total <a href="#" tooltip="Maintenance supervision and staff costs, both company and contractor including all employee benefits such as paid time off for vacation, holidays, sickness, union representation, workman or personal leave, hospitalization, life and other insurance, retirement or pension program costs, recreational or educational program costs, payroll social security taxes paid by company, company uniforms, safety glasses or shoes, sports or social clubs, overtime or subsidized meals, subsidized transportation, company stock or profit sharing programs, company cars, subsidized loans, accommodations, and any other employee benefit program.">Indirect Annualized T/A Maintenance Costs</a></li>
                        <li class='gridrowtext calctext' qid='QSTID_CST_221.1'>Total Direct and <a href="#" tooltip="Maintenance supervision and staff costs, both company and contractor including all employee benefits such as paid time off for vacation, holidays, sickness, union representation, workman or personal leave, hospitalization, life and other insurance, retirement or pension program costs, recreational or educational program costs, payroll social security taxes paid by company, company uniforms, safety glasses or shoes, sports or social clubs, overtime or subsidized meals, subsidized transportation, company stock or profit sharing programs, company cars, subsidized loans, accommodations, and any other employee benefit program.">Indirect Annualized T/A Maintenance Costs</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Expense</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.2" /><%: Html.EditorFor(model => model.SiteExpIndirectCompSupport_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCompIndTA sumSiteExpIndTA sumSiteCostIndTA sumSiteExpTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.3" /><%: Html.EditorFor(model => model.SiteExpIndirectContSupport_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteContIndTA sumSiteExpIndTA sumSiteCostIndTA sumSiteExpTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.4" /><%: Html.EditorFor(model => model.SiteExpTotIndirect_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_221.1" /><%: Html.EditorFor(model => model.SiteExpTotMaint_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Maintenance Capital</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.5" /><%: Html.EditorFor(model => model.SiteMCptlCompIndirectCost_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCompIndTA sumSiteCptlIndTA sumSiteCostIndTA sumSiteCptlTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.6" /><%: Html.EditorFor(model => model.SiteMCptlContIndirectCost_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteContIndTA sumSiteCptlIndTA sumSiteCostIndTA sumSiteCptlTotTA sumSiteCostTotTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.7" /><%: Html.EditorFor(model => model.SiteMCptlTotIndirectCost_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_221.2" /><%: Html.EditorFor(model => model.SiteMCptlTotCost_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Total</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.8" /><%: Html.EditorFor(model => model.SiteCostsIndirectCompSupport_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_220.9" /><%: Html.EditorFor(model => model.SiteCostsIndirectContSupport_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_221" /><%: Html.EditorFor(model => model.SiteCostsTotIndirect_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_221.3" /><%: Html.EditorFor(model => model.SiteCostsTotMaint_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteExpIndirectCompSupport_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpIndirectContSupport_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotIndirect_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompIndirectCost_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContIndirectCost_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotIndirectCost_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsIndirectCompSupport_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsIndirectContSupport_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotIndirect_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotMaint_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotCost_TA)%> <%: Html.ValidationMessageFor(model => model.SiteCostsTotMaint_TA)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-7" style="display: inline; width: 900px;">

            <p class="StepTitle"><a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Maintenance Expense Labor Hours</p>
            <p><i></i></p>



            <div class="questionblock" qid="">


                <div id='ST_TA_EXP_LH' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CST_240.2'><span class='numbering'>1.</span> <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Company <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Employees</li>
                        <li class='gridrowtext' qid='QSTID_CST_240.3'><span class='numbering'>2.</span> <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Contractor <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></li>
                        <li class='gridrowtext' qid='QSTID_CST_240.4'><span class='numbering'>3.</span> Calculated combined (Company plus Contractor)</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Reported Labor Cost</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.2" /><%: Html.EditorFor(model => model._SiteExpCompLabor_TA,"DoubleTmpl",new {size="10" ,id="_SiteExpCompLabor_TA" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.3" /><%: Html.EditorFor(model => model._SiteExpContLabor_TA,"DoubleTmpl",new {size="10" ,id="_SiteExpContLabor_TA" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.4" /><%: Html.EditorFor(model => model._SiteExpTotLabor_TA,"DoubleTmpl",new {size="10" ,id="_SiteExpTotLabor_TA" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Labor Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.5" /><%: Html.EditorFor(model => model.SiteExpCompManHrs_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteExpManHrsTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.6" /><%: Html.EditorFor(model => model.SiteExpContManHrs_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteExpManHrsTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.7" /><%: Html.EditorFor(model => model.SiteExpTotManHrs_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Wage Rate - Local Currency/Hour</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.8" /><%: Html.EditorFor(model => model.SiteExpCompWageRate_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_240.9" /><%: Html.EditorFor(model => model.SiteExpContWageRate_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_241" /><%: Html.EditorFor(model => model.SiteExpAvgWageRate_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Wage Rate - USD/Hour</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_241.1" /><%: Html.EditorFor(model => model.SiteExpCompWageRateUSD_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_241.2" /><%: Html.EditorFor(model => model.SiteExpContWageRateUSD_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_241.3" /><%: Html.EditorFor(model => model.SiteExpAvgWageRateUSD_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model._SiteExpCompLabor_TA)%> <%: Html.ValidationMessageFor(model => model._SiteExpContLabor_TA)%> <%: Html.ValidationMessageFor(model => model._SiteExpTotLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpCompManHrs_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpContManHrs_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpTotManHrs_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpCompWageRate_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpContWageRate_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpAvgWageRate_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpCompWageRateUSD_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpContWageRateUSD_TA)%> <%: Html.ValidationMessageFor(model => model.SiteExpAvgWageRateUSD_TA)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-8" style="display: inline; width: 900px;">

            <p class="StepTitle"><a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">Maintenance Capital</a> Labor Hours</p>
            <p><i></i></p>



            <div class="questionblock" qid="">


                <div id='ST_TA_CAP_LH' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_CST_260.2'><span class='numbering'>1.</span> <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Company <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> Employees</li>
                        <li class='gridrowtext' qid='QSTID_CST_260.3'><span class='numbering'>2.</span> <a href="#" tooltip="To calculate or adjust a variable to determine the annual value even though the value reported was not for a fixed 12-month period. For example, the cost of a maintenance turnaround that occurs once every 5�years is divided by five to get the annualized cost of the turnaround.">Annualized</a> <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> Direct <a href="#" tooltip="Personnel in the reliability and/or maintenance function (company or contractor personnel) that spend more than 50% of their time working with tools and repair materials. Examples include maintenance technicians and journeymen.">Maintenance Labor</a> � Contractor <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></li>
                        <li class='gridrowtext' qid='QSTID_CST_260.4'><span class='numbering'>3.</span> Calculated combined (Company plus Contractor)</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Reported Labor Cost</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.2" /><%: Html.EditorFor(model => model._SiteMCptlCompLabor_TA,"DoubleTmpl",new {size="10" ,id="_SiteMCptlCompLabor_TA" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.3" /><%: Html.EditorFor(model => model._SiteMCptlContLabor_TA,"DoubleTmpl",new {size="10" ,id="_SiteMCptlContLabor_TA" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.4" /><%: Html.EditorFor(model => model._SiteMCptlTotLabor_TA,"DoubleTmpl",new {size="10" ,id="_SiteMCptlTotLabor_TA" ,NumFormat="Number" ,DecPlaces="0" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Labor Hours</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.5" /><%: Html.EditorFor(model => model.SiteMCptlCompManHrs_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCptlManHrsTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.6" /><%: Html.EditorFor(model => model.SiteMCptlContManHrs_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumSiteCptlManHrsTA" ,NumFormat="Number" ,DecPlaces="0"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.7" /><%: Html.EditorFor(model => model.SiteMCptlTotManHrs_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Wage Rate - Local Currency/Hour</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.8" /><%: Html.EditorFor(model => model.SiteMCptlCompWageRate_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_260.9" /><%: Html.EditorFor(model => model.SiteMCptlContWageRate_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_261" /><%: Html.EditorFor(model => model.SiteMCptlAvgWageRate_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Wage Rate - USD/Hour</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_261.1" /><%: Html.EditorFor(model => model.SiteMCptlCompWageRateUSD_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_261.2" /><%: Html.EditorFor(model => model.SiteMCptlContWageRateUSD_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_CST_261.3" /><%: Html.EditorFor(model => model.SiteMCptlAvgWageRateUSD_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="2" ,@class="formfld" ,ReadOnly="True"})%></li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model._SiteMCptlCompLabor_TA)%> <%: Html.ValidationMessageFor(model => model._SiteMCptlContLabor_TA)%> <%: Html.ValidationMessageFor(model => model._SiteMCptlTotLabor_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompManHrs_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContManHrs_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlTotManHrs_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompWageRate_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContWageRate_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlAvgWageRate_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlCompWageRateUSD_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlContWageRateUSD_TA)%> <%: Html.ValidationMessageFor(model => model.SiteMCptlAvgWageRateUSD_TA)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



    <script type="text/javascript">
        function loadHelpReferences() {
            var objHelpRefs = Section_HelpLst;
            for (i = objHelpRefs.length - 1; i >= 0; i--) {
                var qidNm = objHelpRefs[i]['HelpID'];
                if ($('li[qid^="' + qidNm + '"]').length) {
                    var $elm = $('li[qid^="' + qidNm + '"]');
                    $elm.append('<img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
                    $elm.find('img.tooltipBtn').css('visibility', 'visible');
                } else if ($('div[qid^="' + qidNm + '"]').length) {
                    $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
                }
            }
        }

        function formatResult(el, value) {
            var format = 'N';
            if (el.attr('NumFormat') == 'Int') format = 'n0';
            if (el.attr('DecPlaces') != null) {
                if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
            }
            if (value != undefined && isNaN(value)) value = 0;
            var tempval = Globalize.parseFloat(value.toString(), 'en');
            if (isFinite(tempval)) {
                el.val(Globalize.format(tempval, format));
                var idnm = '#_' + el.attr('id');
                var $f = $(idnm);
                if ($f.length) {
                    $f.val(el.val());
                }
            } else {
                el.val('');
            }
        }
        $("input[CalcTag*='sumLbrRTExp']").sum({
            bind: "keyup",
            selector: "#SiteExpTotLabor_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotLabor_RT').attr('readonly', 'readonly');
        $('#SiteExpTotLabor_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumMtlRTExp']").sum({
            bind: "keyup",
            selector: "#SiteExpTotMatl_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotMatl_RT').attr('readonly', 'readonly');
        $('#SiteExpTotMatl_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumLbrRTCap']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotLabor_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotLabor_RT').attr('readonly', 'readonly');
        $('#SiteMCptlTotLabor_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumMtlRTCap']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotMatl_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotMatl_RT').attr('readonly', 'readonly');
        $('#SiteMCptlTotMatl_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumLbrCORTTot']").sum({
            bind: "keyup",
            selector: "#SiteCostsCompLabor_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsCompLabor_RT').attr('readonly', 'readonly');
        $('#SiteCostsCompLabor_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumLbrCTRTTot']").sum({
            bind: "keyup",
            selector: "#SiteCostsContLabor_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsContLabor_RT').attr('readonly', 'readonly');
        $('#SiteCostsContLabor_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSpptRTExp']").sum({
            bind: "keyup",
            selector: "#SiteExpTotIndirect_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotIndirect_RT').attr('readonly', 'readonly');
        $('#SiteExpTotIndirect_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSpptRTCap']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotIndirectCost_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotIndirectCost_RT').attr('readonly', 'readonly');
        $('#SiteMCptlTotIndirectCost_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumExpDirTot']").sum({
            bind: "keyup",
            selector: "#SiteExpTotDirect_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotDirect_RT').attr('readonly', 'readonly');
        $('#SiteExpTotDirect_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumMtCapDirTot']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotDirectCost_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotDirectCost_RT').attr('readonly', 'readonly');
        $('#SiteMCptlTotDirectCost_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumLbrRTTot']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotLabor_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotLabor_RT').attr('readonly', 'readonly');
        $('#SiteCostsTotLabor_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumMatlCompRT']").sum({
            bind: "keyup",
            selector: "#SiteCostsCompMatl_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsCompMatl_RT').attr('readonly', 'readonly');
        $('#SiteCostsCompMatl_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumMatlContRT']").sum({
            bind: "keyup",
            selector: "#SiteCostsContMatl_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsContMatl_RT').attr('readonly', 'readonly');
        $('#SiteCostsContMatl_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumMtlRTTot']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotMatl_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotMatl_RT').attr('readonly', 'readonly');
        $('#SiteCostsTotMatl_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTotRTDir']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotDirect_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotDirect_RT').attr('readonly', 'readonly');
        $('#SiteCostsTotDirect_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumCOSpptRTExp']").sum({
            bind: "keyup",
            selector: "#SiteCostsIndirectCompSupport_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsIndirectCompSupport_RT').attr('readonly', 'readonly');
        $('#SiteCostsIndirectCompSupport_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumCTSpptRTExp']").sum({
            bind: "keyup",
            selector: "#SiteCostsIndirectContSupport_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsIndirectContSupport_RT').attr('readonly', 'readonly');
        $('#SiteCostsIndirectContSupport_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumTotIndrRT']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotIndirect_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotIndirect_RT').attr('readonly', 'readonly');
        $('#SiteCostsTotIndirect_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteTotExpRT']").sum({
            bind: "keyup",
            selector: "#SiteExpTotMaint_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotMaint_RT').attr('readonly', 'readonly');
        $('#SiteExpTotMaint_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteTotMCptlRT']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotCost_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotCost_RT').attr('readonly', 'readonly');
        $('#SiteMCptlTotCost_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteTotCostRT']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotMaint_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotMaint_RT').attr('readonly', 'readonly');
        $('#SiteCostsTotMaint_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteExpManHrsRT']").sum({
            bind: "keyup",
            selector: "#SiteExpTotManHrs_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotManHrs_RT').attr('readonly', 'readonly');
        $('#SiteExpTotManHrs_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCptlManHrsRT']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotManHrs_RT",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotManHrs_RT').attr('readonly', 'readonly');
        $('#SiteMCptlTotManHrs_RT').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteExpLaborTA']").sum({
            bind: "keyup",
            selector: "#SiteExpTotLabor_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotLabor_TA').attr('readonly', 'readonly');
        $('#SiteExpTotLabor_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCptlLaborTA']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotLabor_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotLabor_TA').attr('readonly', 'readonly');
        $('#SiteMCptlTotLabor_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCompLaborTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsCompLabor_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsCompLabor_TA').attr('readonly', 'readonly');
        $('#SiteCostsCompLabor_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteContLaborTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsContLabor_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsContLabor_TA').attr('readonly', 'readonly');
        $('#SiteCostsContLabor_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCompMatlTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsCompMatl_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsCompMatl_TA').attr('readonly', 'readonly');
        $('#SiteCostsCompMatl_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteContMatlTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsContMatl_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsContMatl_TA').attr('readonly', 'readonly');
        $('#SiteCostsContMatl_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteExpMatlTA']").sum({
            bind: "keyup",
            selector: "#SiteExpTotMatl_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotMatl_TA').attr('readonly', 'readonly');
        $('#SiteExpTotMatl_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCptlMatlTA']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotMatl_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotMatl_TA').attr('readonly', 'readonly');
        $('#SiteMCptlTotMatl_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCompIndTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsIndirectCompSupport_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsIndirectCompSupport_TA').attr('readonly', 'readonly');
        $('#SiteCostsIndirectCompSupport_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteContIndTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsIndirectContSupport_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsIndirectContSupport_TA').attr('readonly', 'readonly');
        $('#SiteCostsIndirectContSupport_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteExpIndTA']").sum({
            bind: "keyup",
            selector: "#SiteExpTotIndirect_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotIndirect_TA').attr('readonly', 'readonly');
        $('#SiteExpTotIndirect_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCptlIndTA']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotIndirectCost_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotIndirectCost_TA').attr('readonly', 'readonly');
        $('#SiteMCptlTotIndirectCost_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteExpManHrsTA']").sum({
            bind: "keyup",
            selector: "#SiteExpTotManHrs_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotManHrs_TA').attr('readonly', 'readonly');
        $('#SiteExpTotManHrs_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCptlManHrsTA']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotManHrs_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotManHrs_TA').attr('readonly', 'readonly');
        $('#SiteMCptlTotManHrs_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteTotLaborTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotLabor_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotLabor_TA').attr('readonly', 'readonly');
        $('#SiteCostsTotLabor_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteTotMatlTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotMatl_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotMatl_TA').attr('readonly', 'readonly');
        $('#SiteCostsTotMatl_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteExpDirTA']").sum({
            bind: "keyup",
            selector: "#SiteExpTotDirect_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotDirect_TA').attr('readonly', 'readonly');
        $('#SiteExpTotDirect_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCostDirTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotDirect_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotDirect_TA').attr('readonly', 'readonly');
        $('#SiteCostsTotDirect_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCptlDirTA']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotDirectCost_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotDirectCost_TA').attr('readonly', 'readonly');
        $('#SiteMCptlTotDirectCost_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCostIndTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotIndirect_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotIndirect_TA').attr('readonly', 'readonly');
        $('#SiteCostsTotIndirect_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteExpTotTA']").sum({
            bind: "keyup",
            selector: "#SiteExpTotMaint_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteExpTotMaint_TA').attr('readonly', 'readonly');
        $('#SiteExpTotMaint_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCptlTotTA']").sum({
            bind: "keyup",
            selector: "#SiteMCptlTotCost_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteMCptlTotCost_TA').attr('readonly', 'readonly');
        $('#SiteMCptlTotCost_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumSiteCostTotTA']").sum({
            bind: "keyup",
            selector: "#SiteCostsTotMaint_TA",
            oncalc: function (value, settings) {
                if (value != undefined && !isNaN(value)) {
                    formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
                }
            }
        });
        $('#SiteCostsTotMaint_TA').attr('readonly', 'readonly');
        $('#SiteCostsTotMaint_TA').change(function () {
            $(this).validate();
        });
        $("input[CalcTag*='sumLbrRTExp'],input[CalcTag*='sumSiteExpManHrsRT'] ").bind("keyup", function () {
            var $result = $("#SiteExpAvgWageRate_RT");
            $result.calc("v1/v2", {
                v1: $("#SiteExpTotLabor_RT"),
                v2: $("#SiteExpTotManHrs_RT")
            });
            formatResult($result, $result.val()); /*$("#SiteExpAvgWageRate_RT").trigger('blur');*/
        });
        if ($("#SiteExpTotLabor_RT").val() != null && !isNaN($("#SiteExpTotLabor_RT").val()))
            if ($("#SiteExpTotManHrs_RT").val() != null && !isNaN($("#SiteExpTotManHrs_RT").val())) {
                $("#SiteExpTotLabor_RT").trigger('keyup');
            } $("input[CalcTag*='sumLbrRTExp'],input[CalcTag*='sumSiteExpManHrsRT'],#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteExpAvgWageRateUSD_RT");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteExpTotLabor_RT"),
                    v2: $("#SiteExpTotManHrs_RT"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteExpAvgWageRateUSD_RT").trigger('blur');*/
            });
        if ($("#SiteExpTotLabor_RT").val() != null && !isNaN($("#SiteExpTotLabor_RT").val()))
            if ($("#SiteExpTotManHrs_RT").val() != null && !isNaN($("#SiteExpTotManHrs_RT").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteExpTotLabor_RT").trigger('keyup');
                } $("input[CalcTag*='sumLbrRTCap'],input[CalcTag*='sumSiteCptlManHrsRT'] ").bind("keyup", function () {
                    var $result = $("#SiteMCptlAvgWageRate_RT");
                    $result.calc("v1/v2", {
                        v1: $("#SiteMCptlTotLabor_RT"),
                        v2: $("#SiteMCptlTotManHrs_RT")
                    });
                    formatResult($result, $result.val()); /*$("#SiteMCptlAvgWageRate_RT").trigger('blur');*/
                });
        if ($("#SiteMCptlTotLabor_RT").val() != null && !isNaN($("#SiteMCptlTotLabor_RT").val()))
            if ($("#SiteMCptlTotManHrs_RT").val() != null && !isNaN($("#SiteMCptlTotManHrs_RT").val())) {
                $("#SiteMCptlTotLabor_RT").trigger('keyup');
            } $("input[CalcTag*='sumLbrRTCap'],input[CalcTag*='sumSiteCptlManHrsRT'],#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteMCptlAvgWageRateUSD_RT");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteMCptlTotLabor_RT"),
                    v2: $("#SiteMCptlTotManHrs_RT"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteMCptlAvgWageRateUSD_RT").trigger('blur');*/
            });
        if ($("#SiteMCptlTotLabor_RT").val() != null && !isNaN($("#SiteMCptlTotLabor_RT").val()))
            if ($("#SiteMCptlTotManHrs_RT").val() != null && !isNaN($("#SiteMCptlTotManHrs_RT").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteMCptlTotLabor_RT").trigger('keyup');
                } $("input[CalcTag*='sumSiteCptlLaborTA'],input[CalcTag*='sumSiteCptlManHrsTA'] ").bind("keyup", function () {
                    var $result = $("#SiteMCptlAvgWageRate_TA");
                    $result.calc("v1/v2", {
                        v1: $("#SiteMCptlTotLabor_TA"),
                        v2: $("#SiteMCptlTotManHrs_TA")
                    });
                    formatResult($result, $result.val()); /*$("#SiteMCptlAvgWageRate_TA").trigger('blur');*/
                });
        if ($("#SiteMCptlTotLabor_TA").val() != null && !isNaN($("#SiteMCptlTotLabor_TA").val()))
            if ($("#SiteMCptlTotManHrs_TA").val() != null && !isNaN($("#SiteMCptlTotManHrs_TA").val())) {
                $("#SiteMCptlTotLabor_TA").trigger('keyup');
            } $("input[CalcTag*='sumSiteCptlLaborTA'],input[CalcTag*='sumSiteCptlManHrsTA'],#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteMCptlAvgWageRateUSD_TA");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteMCptlTotLabor_TA"),
                    v2: $("#SiteMCptlTotManHrs_TA"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteMCptlAvgWageRateUSD_TA").trigger('blur');*/
            });
        if ($("#SiteMCptlTotLabor_TA").val() != null && !isNaN($("#SiteMCptlTotLabor_TA").val()))
            if ($("#SiteMCptlTotManHrs_TA").val() != null && !isNaN($("#SiteMCptlTotManHrs_TA").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteMCptlTotLabor_TA").trigger('keyup');
                } $("input[CalcTag*='sumSiteExpLaborTA'],input[CalcTag*='sumSiteExpManHrsTA'] ").bind("keyup", function () {
                    var $result = $("#SiteExpAvgWageRate_TA");
                    $result.calc("v1/v2", {
                        v1: $("#SiteExpTotLabor_TA"),
                        v2: $("#SiteExpTotManHrs_TA")
                    });
                    formatResult($result, $result.val()); /*$("#SiteExpAvgWageRate_TA").trigger('blur');*/
                });
        if ($("#SiteExpTotLabor_TA").val() != null && !isNaN($("#SiteExpTotLabor_TA").val()))
            if ($("#SiteExpTotManHrs_TA").val() != null && !isNaN($("#SiteExpTotManHrs_TA").val())) {
                $("#SiteExpTotLabor_TA").trigger('keyup');
            } $("input[CalcTag*='sumSiteExpLaborTA'],input[CalcTag*='sumSiteExpManHrsTA'],#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteExpAvgWageRateUSD_TA");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteExpTotLabor_TA"),
                    v2: $("#SiteExpTotManHrs_TA"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteExpAvgWageRateUSD_TA").trigger('blur');*/
            });

        if ($("#SiteExpTotLabor_TA").val() != null && !isNaN($("#SiteExpTotLabor_TA").val()))
            if ($("#SiteExpTotManHrs_TA").val() != null && !isNaN($("#SiteExpTotManHrs_TA").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteExpTotLabor_TA").trigger('keyup');
                } $("#SiteExpCompLabor_RT,#SiteExpCompManHrs_RT ").bind("keyup", function () {
                    var $result = $("#SiteExpCompWageRate_RT");
                    $result.calc("v1/v2", {
                        v1: $("#SiteExpCompLabor_RT"),
                        v2: $("#SiteExpCompManHrs_RT")
                    });
                    formatResult($result, $result.val()); /*$("#SiteExpCompWageRate_RT").trigger('blur');*/
                });
        if ($("#SiteExpCompLabor_RT").val() != null && !isNaN($("#SiteExpCompLabor_RT").val()))
            if ($("#SiteExpCompManHrs_RT").val() != null && !isNaN($("#SiteExpCompManHrs_RT").val())) {
                $("#SiteExpCompLabor_RT").trigger('keyup');
            } $("#SiteExpCompLabor_RT,#SiteExpCompManHrs_RT,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteExpCompWageRateUSD_RT");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteExpCompLabor_RT"),
                    v2: $("#SiteExpCompManHrs_RT"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteExpCompWageRateUSD_RT").trigger('blur');*/
            });
        if ($("#SiteExpCompLabor_RT").val() != null && !isNaN($("#SiteExpCompLabor_RT").val()))
            if ($("#SiteExpCompManHrs_RT").val() != null && !isNaN($("#SiteExpCompManHrs_RT").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteExpCompLabor_RT").trigger('keyup');
                } $("#SiteExpContLabor_RT,#SiteExpContManHrs_RT ").bind("keyup", function () {
                    var $result = $("#SiteExpContWageRate_RT");
                    $result.calc("v1/v2", {
                        v1: $("#SiteExpContLabor_RT"),
                        v2: $("#SiteExpContManHrs_RT")
                    });
                    formatResult($result, $result.val()); /*$("#SiteExpContWageRate_RT").trigger('blur');*/
                });
        if ($("#SiteExpContLabor_RT").val() != null && !isNaN($("#SiteExpContLabor_RT").val()))
            if ($("#SiteExpContManHrs_RT").val() != null && !isNaN($("#SiteExpContManHrs_RT").val())) {
                $("#SiteExpContLabor_RT").trigger('keyup');
            } $("#SiteExpContLabor_RT,#SiteExpContManHrs_RT,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteExpContWageRateUSD_RT");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteExpContLabor_RT"),
                    v2: $("#SiteExpContManHrs_RT"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteExpContWageRateUSD_RT").trigger('blur');*/
            });
        if ($("#SiteExpContLabor_RT").val() != null && !isNaN($("#SiteExpContLabor_RT").val()))
            if ($("#SiteExpContManHrs_RT").val() != null && !isNaN($("#SiteExpContManHrs_RT").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteExpContLabor_RT").trigger('keyup');
                } $("#SiteExpCompLabor_TA,#SiteExpCompManHrs_TA ").bind("keyup", function () {
                    var $result = $("#SiteExpCompWageRate_TA");
                    $result.calc("v1/v2", {
                        v1: $("#SiteExpCompLabor_TA"),
                        v2: $("#SiteExpCompManHrs_TA")
                    });
                    formatResult($result, $result.val()); /*$("#SiteExpCompWageRate_TA").trigger('blur');*/
                });
        if ($("#SiteExpContManHrs_RT").val() != null)
            if ($("#SiteExpContManHrs_RT").val() != null && !isNaN($("#SiteExpContManHrs_RT").val()))
                if ($("#SiteExpTotManHrs_RT").val() != null && !isNaN($("#SiteExpTotManHrs_RT").val())) {
                    $("#SiteExpContManHrs_RT").trigger('keyup');
                } $("#SiteExpContManHrs_RT, #SiteExpCompManHrs_RT").bind("keyup", function () {
                    var $result = $("#SiteMCptlPctFTEContractors");
                    $result.calc("v1/v2*v3", {
                        v1: $("#SiteExpContManHrs_RT"),
                        v2: $("#SiteExpTotManHrs_RT"),
                        v3: 100
                    });
                    formatResult($result, $result.val()); /*$("#SiteExpCompWageRate_TA").trigger('blur');*/
                });
        if ($("#SiteExpCompLabor_TA").val() != null && !isNaN($("#SiteExpCompLabor_TA").val()))
            if ($("#SiteExpCompManHrs_TA").val() != null && !isNaN($("#SiteExpCompManHrs_TA").val())) {
                $("#SiteExpCompLabor_TA").trigger('keyup');
            } $("#SiteExpCompLabor_TA,#SiteExpCompManHrs_TA,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteExpCompWageRateUSD_TA");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteExpCompLabor_TA"),
                    v2: $("#SiteExpCompManHrs_TA"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteExpCompWageRateUSD_TA").trigger('blur');*/
            });
        if ($("#SiteExpCompLabor_TA").val() != null && !isNaN($("#SiteExpCompLabor_TA").val()))
            if ($("#SiteExpCompManHrs_TA").val() != null && !isNaN($("#SiteExpCompManHrs_TA").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteExpCompLabor_TA").trigger('keyup');
                } $("#SiteExpContLabor_TA,#SiteExpContManHrs_TA ").bind("keyup", function () {
                    var $result = $("#SiteExpContWageRate_TA");
                    $result.calc("v1/v2", {
                        v1: $("#SiteExpContLabor_TA"),
                        v2: $("#SiteExpContManHrs_TA")
                    });
                    formatResult($result, $result.val()); /*$("#SiteExpContWageRate_TA").trigger('blur');*/
                });
        if ($("#SiteExpContLabor_TA").val() != null && !isNaN($("#SiteExpContLabor_TA").val()))
            if ($("#SiteExpContManHrs_TA").val() != null && !isNaN($("#SiteExpContManHrs_TA").val())) {
                $("#SiteExpContLabor_TA").trigger('keyup');
            } $("#SiteExpContLabor_TA,#SiteExpContManHrs_TA,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteExpContWageRateUSD_TA");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteExpContLabor_TA"),
                    v2: $("#SiteExpContManHrs_TA"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteExpContWageRateUSD_TA").trigger('blur');*/
            });
        if ($("#SiteExpContLabor_TA").val() != null && !isNaN($("#SiteExpContLabor_TA").val()))
            if ($("#SiteExpContManHrs_TA").val() != null && !isNaN($("#SiteExpContManHrs_TA").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteExpContLabor_TA").trigger('keyup');
                } $("#SiteMCptlCompLabor_TA,#SiteMCptlCompManHrs_TA ").bind("keyup", function () {
                    var $result = $("#SiteMCptlCompWageRate_TA");
                    $result.calc("v1/v2", {
                        v1: $("#SiteMCptlCompLabor_TA"),
                        v2: $("#SiteMCptlCompManHrs_TA")
                    });
                    formatResult($result, $result.val()); /*$("#SiteMCptlCompWageRate_TA").trigger('blur');*/
                });
        if ($("#SiteMCptlCompLabor_TA").val() != null && !isNaN($("#SiteMCptlCompLabor_TA").val()))
            if ($("#SiteMCptlCompManHrs_TA").val() != null && !isNaN($("#SiteMCptlCompManHrs_TA").val())) {
                $("#SiteMCptlCompLabor_TA").trigger('keyup');
            } $("#SiteMCptlCompLabor_TA,#SiteMCptlCompManHrs_TA,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteMCptlCompWageRateUSD_TA");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteMCptlCompLabor_TA"),
                    v2: $("#SiteMCptlCompManHrs_TA"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteMCptlCompWageRateUSD_TA").trigger('blur');*/
            });
        if ($("#SiteMCptlCompLabor_TA").val() != null && !isNaN($("#SiteMCptlCompLabor_TA").val()))
            if ($("#SiteMCptlCompManHrs_TA").val() != null && !isNaN($("#SiteMCptlCompManHrs_TA").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteMCptlCompLabor_TA").trigger('keyup');
                } $("#SiteMCptlContLabor_TA,#SiteMCptlContManHrs_TA ").bind("keyup", function () {
                    var $result = $("#SiteMCptlContWageRate_TA");
                    $result.calc("v1/v2", {
                        v1: $("#SiteMCptlContLabor_TA"),
                        v2: $("#SiteMCptlContManHrs_TA")
                    });
                    formatResult($result, $result.val()); /*$("#SiteMCptlContWageRate_TA").trigger('blur');*/
                });
        if ($("#SiteMCptlContLabor_TA").val() != null && !isNaN($("#SiteMCptlContLabor_TA").val()))
            if ($("#SiteMCptlContManHrs_TA").val() != null && !isNaN($("#SiteMCptlContManHrs_TA").val())) {
                $("#SiteMCptlContLabor_TA").trigger('keyup');
            } $("#SiteMCptlContLabor_TA,#SiteMCptlContManHrs_TA,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteMCptlContWageRateUSD_TA");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteMCptlContLabor_TA"),
                    v2: $("#SiteMCptlContManHrs_TA"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteMCptlContWageRateUSD_TA").trigger('blur');*/
            });
        if ($("#SiteMCptlContLabor_TA").val() != null && !isNaN($("#SiteMCptlContLabor_TA").val()))
            if ($("#SiteMCptlContManHrs_TA").val() != null && !isNaN($("#SiteMCptlContManHrs_TA").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteMCptlContLabor_TA").trigger('keyup');
                } $("#SiteMCptlCompLabor_RT,#SiteMCptlCompManHrs_RT ").bind("keyup", function () {
                    var $result = $("#SiteMCptlCompWageRate_RT");
                    $result.calc("v1/v2", {
                        v1: $("#SiteMCptlCompLabor_RT"),
                        v2: $("#SiteMCptlCompManHrs_RT")
                    });
                    formatResult($result, $result.val()); /*$("#SiteMCptlCompWageRate_RT").trigger('blur');*/
                });
        if ($("#SiteMCptlCompLabor_RT").val() != null && !isNaN($("#SiteMCptlCompLabor_RT").val()))
            if ($("#SiteMCptlCompManHrs_RT").val() != null && !isNaN($("#SiteMCptlCompManHrs_RT").val())) {
                $("#SiteMCptlCompLabor_RT").trigger('keyup');
            } $("#SiteMCptlCompLabor_RT,#SiteMCptlCompManHrs_RT,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteMCptlCompWageRateUSD_RT");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteMCptlCompLabor_RT"),
                    v2: $("#SiteMCptlCompManHrs_RT"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteMCptlCompWageRateUSD_RT").trigger('blur');*/
            });
        if ($("#SiteMCptlCompLabor_RT").val() != null && !isNaN($("#SiteMCptlCompLabor_RT").val()))
            if ($("#SiteMCptlCompManHrs_RT").val() != null && !isNaN($("#SiteMCptlCompManHrs_RT").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteMCptlCompLabor_RT").trigger('keyup');
                } $("#SiteMCptlContLabor_RT,#SiteMCptlContManHrs_RT ").bind("keyup", function () {
                    var $result = $("#SiteMCptlContWageRate_RT");
                    $result.calc("v1/v2", {
                        v1: $("#SiteMCptlContLabor_RT"),
                        v2: $("#SiteMCptlContManHrs_RT")
                    });
                    formatResult($result, $result.val()); /*$("#SiteMCptlContWageRate_RT").trigger('blur');*/
                });
        if ($("#SiteMCptlContLabor_RT").val() != null && !isNaN($("#SiteMCptlContLabor_RT").val()))
            if ($("#SiteMCptlContManHrs_RT").val() != null && !isNaN($("#SiteMCptlContManHrs_RT").val())) {
                $("#SiteMCptlContLabor_RT").trigger('keyup');
            } $("#SiteMCptlContLabor_RT,#SiteMCptlContManHrs_RT,#_ExchangeRate ").bind("keyup", function () {
                var $result = $("#SiteMCptlContWageRateUSD_RT");
                $result.calc("v1/v2/v3", {
                    v1: $("#SiteMCptlContLabor_RT"),
                    v2: $("#SiteMCptlContManHrs_RT"),
                    v3: $("#_ExchangeRate")
                });
                formatResult($result, $result.val()); /*$("#SiteMCptlContWageRateUSD_RT").trigger('blur');*/
            });
        if ($("#SiteMCptlContLabor_RT").val() != null && !isNaN($("#SiteMCptlContLabor_RT").val()))
            if ($("#SiteMCptlContManHrs_RT").val() != null && !isNaN($("#SiteMCptlContManHrs_RT").val()))
                if ($("#_ExchangeRate").val() != null && !isNaN($("#_ExchangeRate").val())) {
                    $("#SiteMCptlContLabor_RT").trigger('keyup');
                } var secComments = Section_Comments;
        var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
                    for (i = secComments.length - 1; i >= 0; i--) {
                        var qidNm = secComments[i]['CommentID'];
                        if ($('img[qid="' + qidNm + '"]').length) {
                            $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
                        }
                    } <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function (index, html) {
                        var gridId = $(this).closest('.grid').attr('id');
                        if ($.trim(html).indexOf('nbsp', 0) == -1) {
                            var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
            }
                        $(this).html(newHTML);
                    });
        if ($("#_SiteMCptlCompLabor_RT").val() != null && !isNaN($("#_SiteMCptlCompLabor_RT").val()))
            if ($("#_SiteMCptlContLabor_RT").val() != null && !isNaN($("#_SiteMCptlContLabor_RT").val())) {
                $("#_SiteMCptlContLabor_RT").trigger('keyup');
            } $("#_SiteMCptlContLabor_RT,#_SiteMCptlCompLabor_RT,#SiteMCptlMetricLabor_RT").bind("keyup", function () {
                var $result = $("#SiteMCptlMetricLabor_RT");
                $result.calc("v1/(v1+v2)*100", {
                    v1: $("#_SiteMCptlContLabor_RT"),
                    v2: $("#_SiteMCptlCompLabor_RT"),
                });
                formatResult($result, $result.val());
            });
        if ($("#SiteMCptlContManHrs_RT").val() != null && !isNaN($("#SiteMCptlContManHrs_RT").val()))
            if ($("#SiteMCptlCompManHrs_RT").val() != null && !isNaN($("#SiteMCptlCompManHrs_RT").val())) {
                $("#SiteMCptlCompManHrs_RT").trigger('keyup');
            } $("#SiteMCptlContManHrs_RT,#SiteMCptlCompManHrs_RT,#SiteMCptlMetricManHrs_RT").bind("keyup", function () {
                var $result = $("#SiteMCptlMetricManHrs_RT");
                $result.calc("v1/(v1+v2)*100", {
                    v1: $("#SiteMCptlContManHrs_RT"),
                    v2: $("#SiteMCptlCompManHrs_RT"),
                });
                formatResult($result, $result.val());
            });

        if ($("#SiteMCptlCompWageRate_RT").val() != null && !isNaN($("#SiteMCptlCompWageRate_RT").val()))
            if ($("#SiteMCptlContWageRate_RT").val() != null && !isNaN($("#SiteMCptlContWageRate_RT").val())) {
                $("#SiteMCptlMetricWageRate_RT").trigger('keyup');
            } $("#SiteMCptlContManHrs_RT,#SiteMCptlCompManHrs_RT,#SiteMCptlCompWageRate_RT, #SiteMCptlContWageRate_RT, #SiteMCptlMetricWageRate_RT").bind("keyup", function () {
                var $result = $("#SiteMCptlMetricWageRate_RT");
                $result.calc("v1/(v1+v2)*100", {
                    v1: $("#SiteMCptlContWageRate_RT"),
                    v2: $("#SiteMCptlCompWageRate_RT"),
                });
                formatResult($result, $result.val());
            });

        if ($("#SiteMCptlCompWageRateUSD_RT").val() != null && !isNaN($("#SiteMCptlCompWageRateUSD_RT").val()))
            if ($("#SiteMCptlContWageRateUSD_RT").val() != null && !isNaN($("#SiteMCptlContWageRateUSD_RT").val())) {
                $("#SiteMCptlMetricWageRateUSD_RT").trigger('keyup');
            } $("#SiteMCptlContManHrs_RT,#SiteMCptlCompManHrs_RT,#SiteMCptlCompWageRateUSD_RT, #SiteMCptlContWageRateUSD_RT, #SiteMCptlMetricWageRateUSD_RT").bind("keyup", function () {
                var $result = $("#SiteMCptlMetricWageRateUSD_RT");
                $result.calc("v1/(v1+v2)*100", {
                    v1: $("#SiteMCptlContWageRateUSD_RT"),
                    v2: $("#SiteMCptlCompWageRateUSD_RT"),
                });
                formatResult($result, $result.val());
            });



        $(document).ready(function () {
            if ($("#SiteExpContManHrs_RT").val() != null && !isNaN($("#SiteExpContManHrs_RT").val()))
                if ($("#SiteExpTotManHrs_RT").val() != null && !isNaN($("#SiteExpTotManHrs_RT").val())) {
                    var $result = $("#SiteMCptlPctFTEContractors");
                    $result.calc("v1/v2*v3", {
                        v1: $("#SiteExpContManHrs_RT"),
                        v2: $("#SiteExpTotManHrs_RT"),
                        v3: 100
                    });
                    formatResult($result, $result.val()); /*$("#SiteMCptlCompWageRateUSD_TA").trigger('blur');*/
                }
            if ($("#SiteMCptlContManHrs_RT").val() != null && !isNaN($("#SiteMCptlContManHrs_RT").val()))
                if ($("#SiteMCptlCompManHrs_RT").val() != null && !isNaN($("#SiteMCptlCompManHrs_RT").val())) {
                    var $result = $("#SiteMCptlMetricManHrs_RT");
                    $result.calc("v1/(v1+v2)*100", {
                        v1: $("#SiteMCptlContManHrs_RT"),
                        v2: $("#SiteMCptlCompManHrs_RT"),
                    });
                    formatResult($result, $result.val());
                }
        });
        $('img[suid]').click(function () {
            var suid = $(this).attr('suid');
            var qid = $(this).attr('qid');
            recordComment(suid, qid);
        });
    </script>
</asp:Content>
