﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
    Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.SiteModel>" %>

<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>
<%@ Import Namespace="RAMAPP_Mvc3.Controllers" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="<%= Url.Content("~/Content/dd_menu_style.css") %>" type="text/css" rel="stylesheet" />
    <style type="text/css">
      
      /*  #usrmessage, #unitmessage
        {
            color: #990000;
        }*/
   
    	.basictab {
    		border-bottom: 1px solid gray;
    		font: bold 12px Verdana;
    		list-style-type: none;
    		margin-left: 0;
    		padding: 3px 0;
    		text-align: left; /*set to left, center, or right to align the menu as desired*/
    	}

    	.basictab li {
    		display: inline;
    		margin: 0;
    	}

    	.basictab li a {
    		background-color: #f6ffd5;
    		border: 1px solid gray;
    		border-bottom: none;
    		color: #2d2b2b;
    		margin-right: 3px;
    		padding: 3px 7px;
    		text-decoration: none;
    	}

    	.basictab li a:visited { color: #2d2b2b; }

    	.basictab li a:hover {
    		background-color: #333;
    		color: white;
    	}

    	.basictab li a:active { color: black; }

    	.basictab li.selected a {
    		background-color: #991000;
    		color: white;
    		padding-top: 4px;
    		/*selected tab effect*/
    		position: relative;
    		top: 1px;
    	}

    .scorebox
        {
        	display: inline-block;
          
           
         <%--   width: 26px;
 	 	-webkit-border-radius: 15px;
 	 	-moz-border-radius: 15px;
 	 	border-radius: 15px;--%>
            text-align: center;
 	 	<%--box-shadow:rgba(0,0,0,0.2) 0px 0px 14px; border-radius:12px;
            --%>
        }
        .scorebox .innerbox
        {
        	-webkit-border-radius: 12px;
 	 	-moz-border-radius: 12px;
 	 	border-radius: 12px;
        	/*height: 25px;*/
            width: 24px;
        	margin: 1px;
            background: #ccc;
            
        }
     
        .scorebox .score 
        {
        	color:#6699ff; text-shadow:0px 0px 10px #99ccff
        	padding: 1px;
            font-family: Helvetica, Arial;
            font-size: 14px;
            /*color: #333;*/
        }
        .score a 
        {
        	font-weight: 500;	
        	color: #333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%
        var usrAccessLvl = (short?)ViewData["AccessLevel"];
        var ProcFamily = new Dictionary<string,string>()
{ 
{"PM_OLE","Primary Olefins"},
{"INT_OLE","Intermediates from Olefins  "},
{"PM_ARM","Primary Aromatics"},
{"INT_ARM","Intermediates from Aromatics"},
{"Cl_HC","Chlorinated Hydrocarbons"},
{"Cl_ALK","Chlor-Alkali Products"},
{"C_PLAST","Commodity Thermoplastics"},
{"E_PLAST","Engineering Thermoplastics"},
{"ELASM","Elastomers & Rubbers"},
{"THMSETS","Thermosets"},
{"FFF","Fibers, Films, and Fabrics"},
{"IND_GAS","Industrial Gases"},
{"LIQ_IORG","Liquid Inorganics"},
{"S_IORG","Solid Inorganics"},
{"S_ORG","Solid Organics"},
{"PHARM_ACT","Pharmaceuticals- Active Ingredient"},
{"PHARM_FORM","Pharmaceuticals- Formulation"},
{"AGR_ACT","Agricultural- Active Ingredient"},
{"AGR_FORM","Agricultural- Formulation"},
{"COAT","Adhesives, Paints, and Coatings"},
{"ALKY","Alkylation Unit"},
{"CDU","Atmospheric Crude Distillation Unit"},
{"REF","Catalytic Reforming Unit"},
{"COK","Coking Unit"},
{"VHYT","Cracking Feed or Vacuum Gas Oil Desulfurization"},
{"DHYT","Distillate/Light Gas Oil Desulfurization and Treating"},
{"FCC","Fluid Catalytic Cracking Unit"},
{"HYC","Hydrocracking Unit"},
{"HYG","Hydrogen Generation Unit (partial oxidation, steam naphtha reforming, or steam methane through LPG reforming)"},
{"H2PURE","Hydrogen Purification Unit (cryogenic, membrane separation, or PSA)"},
{"KHYT","Kerosene Desulfurization and Treating"},
{"NHYT","Naphtha/Gasoline Desulfurization and Treating"},
{"OTH","Other Refining Process Unit- please provide a description"},
{"RHYT","Residual Desulfurization"},
{"SHYT","Selective Hydrotreating"},
{"SRU","Sulfur Recovery Unit"},
{"TRU","Tail Gas Recovery Unit"},
{"VAC","Vacuum Crude Distillation Unit"},
{"OthChem","Other Chemical Process"},
{"OthRef","Other Refining Process"}
};
    %>
    <div id="wizard" class="stContainer">
        <div style="float: left;">
            Active/Inactive: &nbsp;
            <img align="middle" alt="Active Or Inactive" title="<%: Model.IsActive ? "Data input is enabled" : "Data input is disabled" %>"
                src="<%= Url.Content("~/images/" + ((Model.IsActive) ? "enable" : "disable") + ".gif") %>"
                border="no" />
            <% if (SessionUtil.GetCompanySID().Contains("00TEST"))
               {%>
            &nbsp; Data Quality:
            <div class="scorebox">
                <div class="innerbox">
                    <div class="score">
                        <a title="See details on data problems" href="<%= Url.RouteUrl(
                                                                                   new
                                                                                       {
                                                                                           action = "ViewGradeDetails",
                                                                                           controller = "Admin",
                                                                                           id = Model.DatasetID
                                                                                       }
                            ) %>?start=1&size=20&name=<%= Model.Name %>">
                            <%= ((SiteController) ViewContext.Controller).GetFacilityDataQuality(Model.DatasetID) %>
                        </a>
                    </div>
                </div>
            </div>
            <% } %>
        </div>
        <p>
            &nbsp;</p>
        <fieldset style="width: 750px;">
            <legend>Site Description</legend>
            <div class="container_16">
                <div class="grid_16">
                    <span style="float: right; font-size: .85em;"><img  alt="delete" align="middle"  src="<%=Page.ResolveUrl("~/images")%>/buildings.png"  /><a href="javascript:openCreateUnitDialog()">
                        Create Unit </a>&nbsp;&nbsp;<img  alt="delete" align="middle"  src="<%=Page.ResolveUrl("~/images")%>/user.png"  /> <a href="javascript:openAddUsrDialog()">Add User </a>&nbsp;&nbsp;
                        <% if (Model.DatasetID != null)
                           { %>
                        <img  alt="delete" align="middle"  src="<%=Page.ResolveUrl("~/images")%>/x-delete-icon.png"  />
                        <%: Html.Raw(Ajax.ActionLink("imagelink",
                                                 "DeleteSite",
                                                 new {controller="Site" ,id = Model.DatasetID, sitename = Model.Name},
                                                 new AjaxOptions
                                                     {
                                                         Confirm = "Are you sure you want to delete this site?",
                                                         HttpMethod = "Delete",
                                                         OnComplete = "JsonDeleteSite_OnComplete"
                                                     },
                                                 new {id = "delLink_" + Model.DatasetID})
                                     .ToHtmlString()
                                     .Replace("imagelink", "Delete Site")) %>
                        <% }%>
                    </span>
                </div>
                <div style="border-bottom: #CCC thin solid; margin: 10px; width: 100%;">
                    &nbsp;</div>
                <div class="grid_16">
                  
                </div>
                <div class="grid_16">
                    <div id="step-1">
                        <br />
                        <!--fieldset >
                            <legend>Information</legend-->
                        <span class="display-label grid_2">Name :</span> <span class="display-field grid_4">
                            <%: Html.TextBoxFor(model => model.Name, new
                                                                        {
                                                                            title =
                                                                        "Name will be saved after if changed",
                                                                            placeholder = "Site Name",
                                                                            onchange =
                                                                        "recordChange()",
                                                                            size = "35"
                                                                        }) %>
                        </span>
                        <br />
                        <%-- <span class="display-label grid_2">Location : </span><span class="display-field grid_4">
                                <%: Html.DropDownListFor(model => model.Location, CultureInfo.GetCultures(CultureTypes.SpecificCultures & ~CultureTypes.NeutralCultures)
                                                                                                            .Where(ci => ci.Name.Trim().Length > 0)
                                                                        .OrderBy(ci=>new RegionInfo(ci.LCID).EnglishName )
                                                                        .Select(ci =>{
                                                                                          var r = new RegionInfo(ci.LCID);
                                                                                     
                                                                                          return new SelectListItem() { Text = r.DisplayName, Value = r.TwoLetterISORegionName };
                                                                                   
                                                                                               
                                                                        }).Distinct(new RAMAPP_Mvc3.Models.SelectListItemComparer()),"Select Location",
                                                                        new {onchange = "recordChange()" })%>
                              
                            </span>
                            <br /><br />
                            <%
                                RegionInfo rg = null;
                                try
                                {
                                    rg = new RegionInfo(Model.Location ?? "");
                                }
                                catch
                                {

                                    rg = null;
                                }      
                            
                            %>--%>
                        <%--   <span class="display-label grid_2">Currency : </span><span class="display-field grid_4">
                               <%: Html.TextBoxFor(model => model.Currency, new { placeholder = "Site Name", onchange = "recordChange()" })%>--%>
                        <%-- <%: Html.DropDownListFor(model => model.Currency, CultureInfo.GetCultures(CultureTypes.SpecificCultures & ~CultureTypes.NeutralCultures)
                                                                      
                                                                        .Select(ci =>{
                                                                                      var r = new RegionInfo(ci.LCID);

                                                                                      return new SelectListItem() { Text = r.CurrencyEnglishName, Value = r.ISOCurrencySymbol, Selected = r.ISOCurrencySymbol.Equals(Model != null && Model.Currency != null ? Model.Currency.Trim()  : "") };
                                                                                         
                                                                        }).OrderBy(ci=> ci.Text).Distinct(new RAMAPP_Mvc3.Models.SelectListItemComparer()),"Select Currency",
                                                                        new { onchange = "recordChange()" 
                          })%>
                             
                            </span>--%>
                        <!--/fieldset-->
                    </div>
                    <div id="step-2">
                        <%-- <fieldset>
                            <legend>Input Configuration</legend><span class="display-label">
                            
                                <%
                                    bool? isMetric = false;
                                    try
                                    {
                                        isMetric = rg != null && rg.IsMetric;
                                    }
                                    catch
                                    {
                                        //Do nothing   
                                    }
                                %>
                            
                                <%: Html.LabelFor(model => model.UOMPref)%>:&nbsp; </span><span class="display-field">
                                    Metric
                                    <%:  Html.RadioButtonFor(x => x.UOMPref, 1,new { onclick = "recordChange()", Checked =isMetric})%>
                                    US
                                    <%: Html.RadioButtonFor(x => x.UOMPref, 2, new { onclick = "recordChange()" , Checked = !isMetric})%>
                                </span>
                        </fieldset>--%>
                        <%: Html.HiddenFor(model => model.CompanySID) %>
                        <%: Html.HiddenFor(model => model.DatasetID) %>
                    </div>
                    <div class="grid_16" id="step-3">
                        <%-- <fieldset>
                            <legend>% of Questions Completed</legend>
                              <div class="grid_16">
                                <img src="<%= Url.Content("~/Admin/GetSiteSectionChart/" + Model.DatasetID) %>" alt="sitechart" />
                                </div>
                                <div class="grid_16">
                                <img src="<%= Url.Content("~/Admin/GetSiteUnitsChart/" + Model.DatasetID) %>" alt="unitchart" />  
                                </div>  --%>
                        <%-- <asp:Chart ID="chtSiteQuestions" runat="server" Width="532px">
                                <Series>
                                    <asp:Series Name="Section" YValueType="Int32" ChartType="Column" ChartArea="MainChartArea"
                                        LabelAngle="45">
                                        <Points>
                                            <asp:DataPoint AxisLabel="Identification" YValues="80" />
                                            <asp:DataPoint AxisLabel="Characteristics" YValues="60" />
                                            <asp:DataPoint AxisLabel="Costs" YValues="15" />
                                            <asp:DataPoint AxisLabel="Personnel" YValues="68" />
                                            <asp:DataPoint AxisLabel="Inventory" YValues="45" />
                                            <asp:DataPoint AxisLabel="Processes" YValues="70" />
                                        </Points>
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="MainChartArea">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </fieldset>--%>
                    </div>
                    <div style="border-bottom: #CCC thin solid; margin: 10px; width: 100%;">
                        &nbsp;</div>
                    <div id="step-4">
                        <div class="display-field">
                            <h5>
                                Units</h5>
                            <br />
                             <div id="unitmessage" align="center">
                        </div>
                            <%: Html.Grid(Model.Units).Columns(column =>
                                                           {
                                                               column.For(
                                                                   u => Server.HtmlDecode(Ajax.ActionLink("<img  alt=\"delete unit\" title=\"Delete Unit\" align=\"middle\" border=0  src=\"" + Page.ResolveUrl("~/images") + "/delete_red.png\" />",
                                                                                                          "DeleteUnit",
                                                                                                          new
                                                                                                              {
                                                                                                                  controller="Unit",
                                                                                                                  id =
                                                                                                              u.
                                                                                                              DatasetID,
                                                                                                                  name =
                                                                                                              u.UnitName
                                                                                                              },
                                                                                                          new AjaxOptions
                                                                                                              {
                                                                                                                  Confirm
                                                                                                                      =
                                                                                                                      "Are you sure you want to remove this unit from using the site?",
                                                                                                                  HttpMethod
                                                                                                                      =
                                                                                                                      "Delete",
                                                                                                                  OnComplete
                                                                                                                      =
                                                                                                                      "JsonDeleteUnit_OnComplete"
                                                                                                              },
                                                                                                          new
                                                                                                              {
                                                                                                                  id =
                                                                                                              "delLink_" +
                                                                                                              u.
                                                                                                                  DatasetID
                                                                                                              }).
                                                                                              ToHtmlString())).Named(
                                                                                                  "Delete").Encode(
                                                                                                      false);

                                                               column.For(u => u.UnitName).Named("Name");

                                                               column.For(u => (u.ProcessFamily.Trim() == "OthRef" || u.ProcessFamily.Trim() == "OthChem") ? u.OtherGenericProductName : u.GenericProductName).Named("Generic Name");

                                                               //column.For(u => ProcFamily.Keys.Contains(u.ProcessFamily.Trim()) ? ProcFamily[u.ProcessFamily.Trim()]: "").Named("Process Family");
                                                               column.For(u => u.ProcessFamily).Named("Process Family");

                                                               column.For(
                                                                   u =>
                                                                   "<a href=" +
                                                                   Url.Action("UnitDetails","Unit", new {id = u.DatasetID}) +
                                                                   "> <img  alt=\"edit unit\" title=\"Edit Unit\" align=\"middle\" border=0  src=\"" + Page.ResolveUrl("~/images") + "/edit.gif\" /></a>").Attributes(@class => "left").Named(
                                                                       "Edit").Encode(false);
                                                               ;
                                                           })
                            .Empty("Please add units")
                            .Attributes(@width => "100%", @cellpadding => "5", @cellspacing => "5",
                                        @bordercolor => "#999999", @rules => "groups") %>
                        </div>
                       
                     
                    </div>
                </div>
                <div class="grid_16" id="step-5">
                    <div style="border-bottom: #CCC thin solid; margin: 10px; width: 100%;">
                        &nbsp;</div>
                    <div class="display-field">
                        <h5>
                            Users</h5>
                        <br />
                         <div id="usrmessage" align="center">
                        </div>
                        <% bool isAdmin = SessionUtil.IsAdmin();%>
                        <%:
    Html.Grid(Model.Users).Columns(column =>
                                       {
                                           column.For(u => u.Role != 2
                                                               ? Server.HtmlDecode(Ajax.ActionLink("<img  alt=\"delete unit\" title=\"Remove User\" align=\"middle\" border=0  src=\"" + Page.ResolveUrl("~/images") + "/User-red-icon.png\" />",
                                                                                                   "RemoveSiteUsrs",
                                                                                                   new
                                                                                                       {
                                                                                                           controller="User",
                                                                                                           id =
                                                                                                       Model.DatasetID,
                                                                                                           usr =
                                                                                                       u.UserNo
                                                                                                       },
                                                                                                   new AjaxOptions
                                                                                                       {
                                                                                                           Confirm =
                                                                                                               "You are removing this user from site. Are you sure?",
                                                                                                           HttpMethod =
                                                                                                               "Delete",
                                                                                                           OnComplete =
                                                                                                               "JsonDeleteUser_OnComplete"
                                                                                                       },
                                                                                                   new
                                                                                                       {
                                                                                                           id =
                                                                                                       "delLink_" +
                                                                                                       u.UserNo
                                                                                                       }).
                                                                                       ToHtmlString())
                                                               : "")
                                               .Named("Remove")
                                               .Encode(false);


                                           column.For(u => u.ScreenName);

                                           column.For(u => u.Firstname);

                                           column.For(u => u.Lastname);


                                           column.For(u =>
                                                      /*(isAdmin) ? Html.DropDownList("ddAdminLst_" + u.ScreenName, new List<SelectListItem>{  
														  new SelectListItem {Text="User",Value="0",Selected=(u.Role==0)},
														  new SelectListItem {Text="Site Coordinator",Value="1",Selected=(u.Role==1)},
														  new SelectListItem {Text="Study Coordinator",Value="2",Selected=(u.Role==2)} 
															   },
														  new
														  {
															  onchange = "changeSitePriv('" + u.UserID + "','" + Model.DatasetID + "', $('#ddAdminLst_" + u.ScreenName + "').val())"
															
														  })
			
			:*/
                                                      (u.Role != 2)
                                                          ? Html.DropDownList("ddAdminLst_" + u.UserNo,
                                                                              new List<SelectListItem>
                                                                                  {
                                                                                      new SelectListItem
                                                                                          {
                                                                                              Text = "User",
                                                                                              Value = "0",
                                                                                              Selected = (u.Role == 0)
                                                                                          },
                                                                                      new SelectListItem
                                                                                          {
                                                                                              Text = "Site Coordinator",
                                                                                              Value = "1",
                                                                                              Selected = (u.Role == 1)
                                                                                          }
                                                                                  },
                                                                              new
                                                                                  {
                                                                                      onchange =
                                                                                  "changeSitePriv('" + u.UserNo + "','" +
                                                                                  Model.DatasetID + "', $('#ddAdminLst_" +
                                                                                  u.UserNo + "').val())",
                                                                                      enabled = (usrAccessLvl != -1)
                                                                                  })
                                                          : Html.Label("Data Coordinator")
                                               ).Named("Access Level");
                                       })
        .Empty("No items found.")
        .Attributes(@width => "100%", @cellpadding => "5", @cellspacing => "5", @bordercolor => "#999999",
                    @rules => "groups", @align => "left") %>
                   

                      
                    </div>
                        
                </div>
            </div>
        </fieldset>
        <%
                //ChartArea chartArea = chtSiteQuestions.ChartAreas[0];
                //chartArea.AxisY.Maximum = 100;
                //chartArea.AxisY.Minimum = 0;
        %>

        <div id="addUsrDiv" title="Add Users" class="dialog">
            <%
                using (
                    Ajax.BeginForm("AddSiteUsrs", new {controller="User", id = Model.DatasetID },
                                   new AjaxOptions { HttpMethod = "POST", OnSuccess = "RefreshSiteUsers" }))
                {
            %>
            To add a user , click the checkbox by the name and click <b>"ADD"</b> to assign
            the user to this site.

             <div id="divadduserstemp">
            <% var ml = ((SiteController)ViewContext.Controller).GetAvailUsrsNEW(Model.DatasetID, Model.Users.ToList());
            var avalUsers = ml;
            var items = "";
            for (var i = 0; i < avalUsers.Count; i++) {
            var user = avalUsers[i];
            items += @"<input type=""checkbox"" name=""cbLstAvalUsrs"" id=""cbLstAvalUsrs"" value=""" + user.Value + @""" />&nbsp;&nbsp;" + user.Text + "</br>";
            }
           %>              
             </div>

            <div id="lstAvalUsr">
            <%: Html.Raw(items) %>
            <script type="text/javascript">$("#addUsrDiv input[type='button']").button();</script>
            </div>
            <% } %>
        </div>
    </div>
    <div id="unitDialogBox" title="Add Unit" class="dialog">
    </div>


    <!--div id="alertBox" title="Notice" class="dialog">
    </div-->
    <%--    <script type="text/javascript" src="../../Scripts/jquery.smartTab.js"></script>--%>
    <%-- <script src="<%= Page.ResolveUrl("~/Scripts/jquery-ui-1.8.15.custom.min.js")%>" type="text/javascript"></script>--%>
    <script src="<%= Page.ResolveUrl("~/Scripts/colorscale.min.js") %>" type="text/javascript"> </script>
    <script type="text/javascript">
        $(document).ready(function() {

            var usertitle = '<div style="text-align:left;height:40px;border-bottom:1px solid #911000; font-size:medium;font-weight: 500;">Add a user</div>';

            $("#addUsrDiv").dialog({
                modal: true,
                autoOpen: false,
                height: 425,
                width: 450,
                buttons: {
                    "Add": {
                        text: 'Add',
                        click: function() {
                            $('#addUsrDiv form').first().submit();
                            $(this).dialog('close');
                            return false;
                        }
                    },
                    "Cancel": { text: 'Cancel', className: 'cancel', click: function() { $(this).dialog('close'); } }
                }
            }).siblings('.ui-dialog-titlebar').remove();
            $("#addUsrDiv").prepend(usertitle);

            // var unittitle = '<div  style="text-align:left;height:40px;border-bottom:1px solid #911000; font-size:medium;font-weight: 500;">Add a unit</div>';

            $("#unitDialogBox").dialog({
                modal: true,
                autoOpen: false,
                height: 625,
                width: 700,
                buttons: {
                    "Add": {
                        text: 'Add',
                        click: function() {
                            if ($("#ProcessCategory ").val() == "" || $("#ProcessCategory ").val() == "-1")
                            {
                                $("#lblProcessCategoryValMsg").html("The Process Category field is required");
                                return false;
                            }
                            if ($(".genprod").is(':visible') && (document.getElementById('GenericProductName').value == "-1" || document.getElementById('GenericProductName').value == ""))
                            {
                                $("#lblGenericValMsg").html("The Product Generic Name field is required");
                                return false;
                            }
                            $('#unitDialogBox form').first().submit();
                            $(this).dialog('close');
                            return false;
                        }
                    },
                    "Cancel": { text: 'Cancel', className: 'cancel', click: function() { $(this).dialog('close'); } }
                }
            }).siblings('.ui-dialog-titlebar').remove();
            // $("#unitDialogBox").prepend(unittitle);


            $(document).delegate('#delLink', 'click', function() {
                var link = $(this);
                $.ajax({
                    url: '@Url.Action("Delete", "ControllerName")',
                    success: function(result) {
                        //do something with the result here 
                        //you also have the source in the variable link 
                    }
                });
            });
//            $('#delLink').live('click', function () {
//                var link = $(this);
//                $.ajax({
//                    url: '@Url.Action("Delete", "ControllerName")',
//                    success: function (result) {
//                        //do something with the result here 
//                        //you also have the source in the variable link 
//                    }
//                });
//            });
            	
    
	
//	        // Smart Wizard 	
//	        $('#wizard').smartTab({ transitionEffect: 'fade' });

//	        $('li .stepDesc small').each(function (index) {
//	            var elementTitleId = $(this).closest('a').attr('href') + ' legend';
//	            $(this).text($(elementTitleId).text());
//	        });

            changeColor($('.innerbox'), $('.score').text());
//var scale = $('.score').text();
//            var color = null;
//            if (scale != undefined) {

//                if (scale <= 100 && scale > 75)
//                    color = get_color_for_height({ red: 235, green: 255, blue: 100 }, { red: 0, green: 255, blue: 60 }, 100, scale);
//                if (scale <= 75 && scale > 50)
//                    color = get_color_for_height({ red: 255, green: 255, blue: 0 }, { red: 255, green: 255, blue: 100 }, 100, scale);
//                if (scale <= 50 && scale > 25)
//                    color = get_color_for_height({ red: 255, green: 69, blue: 0 }, { red: 255, green: 255, blue: 0 }, 100, scale);
//                if (scale <= 25)
//                    color = get_color_for_height({ red: 255, green: 98, blue: 98 }, { red: 255, green: 69, blue: 0 }, 100, scale);

//                $('.innerbox').css('background-color', 'rgb(' + color['r'] + ',' + color['g'] + ',' + color['b'] + ')', 'background-color', 'rgba(' + color['r'] + ',' + color['g'] + ',' + color['b'] + ',0.60)');
//            }


        });

//	  var min = Math.min;
//        var max = Math.max;
//        var round = Math.round;

//        function get_color_for_height(startColor, endColor, height, row) {
//            var scale = row / height;
//            var r = startColor['red'] + scale * (endColor['red'] - startColor['red']);
//            var b = startColor['blue'] + scale * (endColor['blue'] - startColor['blue']);
//            var g = startColor['green'] + scale * (endColor['green'] - startColor['green']);

//            return {
//                r: round(min(255, max(0, r))),
//                g: round(min(255, max(0, g))),
//                b: round(min(255, max(0, b)))
//            }
//        }
//	
            
          

        function JsonDeleteSite_OnComplete(response) {

            $('#alertBox').html(response.Message);
            $('#alertBox').dialog('open');
            window.location.href = '<%: Url.RouteUrl(new {action = "Index", controller = "Admin"}) %>';
        }


        function JsonDeleteUser_OnComplete(context) {
            var response = $.parseJSON(context.responseText);

            //remove row
            var linkId = '#delLink_' + response.ElementID;
            $(linkId).closest('tr').remove();

            $("#usrmessage").html('<div class="GoodMsg"><span  id="usrmessage" class="ResponseMsg">' + response.Message + '</span></div>');
        }


        function JsonDeleteUnit_OnComplete(context) {
            var response = $.parseJSON(context.responseText);

            //remove row
            var linkId = '#delLink_' + response.ElementID;
            $(linkId).closest('tr').remove();

            $("#unitmessage").html('<div class="GoodMsg"><span  id="usrmessage" class="ResponseMsg">' + response.Message + '</span></div>');
            //alert(context.SitesUnits);
            $("#SiteUnitSection").html(context.SitesUnits);
            $('.navWindow div.unitsContainer').toggle('fast');
        }

        function doTooltip(event, element) {
            //do nothing
        }

        function hideTip() {
            //do nothing
        }

        function recordChange(e) {
           
           
            var name = $('#Name').val();
            
            var siteDatasetID = <%= Model.DatasetID %>;
            var target = window.event ? window.event.srcElement : e ? e.target : null;
         

           
            $.post('<%= Page.ResolveUrl("~/Site/ChangeSiteName") %>',
                { "SiteDatasetID": siteDatasetID,  "Name": name },
                function(response) {
                    
                    if(response) {
                        $("#SiteUnitSection").html(response.SitesUnits);
                        $('.navWindow div.unitsContainer').toggle('fast');
                        
                        $(target).closest('.display-field').after('<span id="blinker"  text-align="center" style="margin-left:40px;color:slategrey;">' + response.Message + '</span>');
                        $("#blinker").animate({ opacity: .3 },
                            1200,
                            function() {
                                $(this).animate({ opacity: 1 }, 1400, function() {
                                    $(this).remove();
                                });
                            });
                    }

                }, 'json');
        }


        function changeSitePriv(userNo, sitenbr, privOpt) {
           // debugger;
            // Send the request
            $.post('<%: Page.ResolveUrl("~/User/ModifyUsrPriv") %>',
                { "user": userNo, "siteId": sitenbr, "privOpt": privOpt },
                function(data) {
                    $('#usrmessage').html('<div class="GoodMsg"><span  id="usrmessage" class="ResponseMsg">' + data  + '</span></div>');
                }, 'json');

        }

        function openCreateUnitDialog() {
            var $unitBox = $("#unitDialogBox");
            $unitBox.fadeIn(2000).dialog('open').css("border", "1px solid #991000");
            $unitBox.load('<%: Url.RouteUrl(new {action = "CreateUnit", controller = "Unit", id = Model.DatasetID}) %>',
                function(response, status, xhr) {
                    if (status != "error") {
                        var unitTitle = '<div style="text-align:left;height:40px;border-bottom:1px solid #911000; font-size:medium;font-weight: 500;">Add a Unit</div>';
                        $unitBox.prepend(unitTitle);
                    }
                });

        }

        function RefreshSiteUnits(context) {

            if (context.Status) {

                var unit = context.Unit;
                var item = "<tr class='gridrow'><td><a data-ajax='true' data-ajax-complete='JsonDeleteUnit_OnComplete' data-ajax-confirm='Are you sure you want to remove this unit from using this site?' data-ajax-method='Delete' href=<%: Page.ResolveUrl("~/Unit/DeleteUnit") %>/@UnitNbr?name=@UnitNameURI id='delLink_@UnitNbr'>Delete</a></td><td>@UnitName</td><td>@ProductName</td><td>@ProcessType</td><td class='left'><a href='<%: Page.ResolveUrl("~/Unit/UnitDetails") %>/@UnitNbr'> Edit</a></td></tr>";
                item = item.replace( /@UnitNbr/gi , unit.DatasetID);
                item = item.replace( /@UnitNameURI/gi , encodeURIComponent(unit.UnitName));
                item = item.replace( /@UnitName/gi , unit.UnitName);
                item = item.replace( /@ProductName/gi , unit.ProductGenericName);
                item = item.replace( /@ProcessType/gi , unit.ProcessType);

                $("table.grid:first > tbody").prepend(item);

                $("#unitmessage").html('<div class="GoodMsg"><span  id="usrmessage" class="ResponseMsg">' + unit.UnitName + ' has been added.</span></div>');
               // debugger;
                //alert($("#SiteUnitSection").length);
                $("#SiteUnitSection").html(context.SitesUnits);
                $('.navWindow div.unitsContainer').toggle('fast');


            }

        }

        function RefreshSiteUsers(context) {

       
            if (context.Status) {

                var newUsers = context.UsersList;
                var items = "";
                $.each(newUsers, function(i, user) {

                    items += " <tr class='gridrow'><td><a data-ajax='true' data-ajax-complete='JsonDeleteUser_OnComplete' data-ajax-confirm='Are you sure you want to remove this user from using this site?' data-ajax-method='Delete' href='<%: Page.ResolveUrl("~/User/RemoveSiteUsrs") %>/<%: Model.DatasetID %>?usr=" + user.UserNo + "' id='delLink_" + user.UserNo + "'>Remove</a></td>";
                    items += " <td>" + user.ScreenName  + "</td>";
                    items += " <td>" + user.FirstName + "</td>";
                    items += " <td>" + user.LastName + "</td>";
                    items += " <td><select id='ddAdminLst_" + user.UserNo + "' name='ddAdminLst_" + user.UserNo + "' onchange=\"changeSitePriv(&#39;" + user.UserNo + "&#39;,&#39;<%: Model.DatasetID %>&#39;, $(&#39;#ddAdminLst_" + user.UserNo + "&#39;).val())\">";
                    items += " <option value='0'>User</option>";
                    items += " <option value='1'>Site Coordinator</option>";
                    items += " </select></td></tr>";

                   
                });


                $("table.grid:last > tbody").prepend(items);
            }
        }

        function openAddUsrDialog() {
        /*    
            var jsonModel = <%: Html.Raw(new JavaScriptSerializer().Serialize(Model)) %>;
            var users = jsonModel.Users;
            var siteid = jsonModel.DatasetID;
            var avalUsers = "";   
            
            $.getJSON('<%: Page.ResolveUrl("~/User/GetAvailUsrs") %>',
            //$.getJSON('<%= Url.Action("GetAvailUsrs","User") %>',
            //$.getJSON('<%= Url.Action("GetAvailUsrsNEW","Site") %>',
            //$.getJSON('<%=  Url.Action("GetAvailUsrsNEW","Site") %>',
                { "siteId": siteid, "users": users },
                function(data) {
                    try {

                        avalUsers = data.usrAvalLst;
                        var items = "";

                        for (var i = 0; i < avalUsers.length; i++) {
                            var user = avalUsers[i];
                            items += "<input type='checkbox' name='cbLstAvalUsrs' id='cbLstAvalUsrs' value='" + user.Value + "' />&nbsp;&nbsp;" + user.Text + "</br>";
                        }

                        $("#addUsrDiv #lstAvalUsr").html('');
                        $("#addUsrDiv #lstAvalUsr").html(items);
                        $("#addUsrDiv input[type='button']").button();
                    } catch(error) {
                       
                    }
                })
                .error(function(XMLHttpRequest, textStatus, errorThrown) {

                    start = XMLHttpRequest.responseText.search("<title>") + 7;
                    end = XMLHttpRequest.responseText.search("</title>");


                    if (start > 0 && end > 0)
                        alert(XMLHttpRequest.responseText.substring(start, end));

                });
                */
            $("#addUsrDiv").fadeIn(2000).dialog('open').css("border", "1px solid #991000");
        }

    </script>
</asp:Content>
