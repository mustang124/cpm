﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_MAINTModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:00:37  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_MAINT").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_MAINT", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_MAINT" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>General Maintenance Categories</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle">General Maintenance Categories</p>
            <p><i>Please provide the percentage of site Direct Maintenance cost (expense only - exclude <a href="#" tooltip="Equipment that is properly maintained will operate with minimal interruption reaching their full potential life cycle. In contrast, poorly maintained assets fail frequently and reach the end of their useful life prematurely. Consequently, it is necessary to capture and report in-kind replacement of equipment as maintenance cost to avoid masking poor maintenance practices. If this cost is capitalized in the accounting system, report this amount as maintenance cost (either Routine Maintenance Cost or Turnaround Maintenance Cost as applicable).     See also Solomon guide, illustration, and decision tree on Maintenance Capital.">maintenance capital</a> costs) reported in the 'Site Maintenance Cost' section above spent on the following categories of general maintenance.  If the data are unavailable, please estimate.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_MAINT_CTG' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_MT_100.1'><span class='numbering'>1.</span> <a href="#" tooltip="A temporary structure typically erected out of metal poles and wooden planks to enable workers to access elevated equipment or work areas.">Scaffolding</a>, %</li>
                        <li class='gridrowtext' qid='QSTID_MT_100.2'><span class='numbering'>2.</span> <a href="#" tooltip="Painting of the plant physical assets to preserve or extend the life of the asset.">Painting</a>, %</li>
                        <li class='gridrowtext' qid='QSTID_MT_100.3'><span class='numbering'>3.</span> <a href="#" tooltip="Insulation is a non-conductive material wrapped around process piping or vessels to maintain the temperature of the process fluid. Tracing is typically electrical heat tracing or steam tracing used to keep a process fluid from reaching its freeze point.">Insulation and Tracing</a>, %</li>
                        <li class='gridrowtext' qid='QSTID_MT_100.9'><span class='numbering'>4.</span> <a href="#" tooltip="Maintenance Rental/Lease Cost that is hired equipment from 3rd party providers (such as maintenance equipment, cranes, rigs, etc)  Not including scaffolding.">Maintenance Rental/Lease Cost (Not including scaffolding)</a>, %</li>
                        <li class='gridrowtext calctext' qid='QSTID_MT_100.4'>Total "General" Maintenance, %</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Routine</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.1" /><%: Html.EditorFor(model => model.CostCatScaffolding_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.2" /><%: Html.EditorFor(model => model.CostCatPainting_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.3" /><%: Html.EditorFor(model => model.CostCatInsulation_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.9" /><%: Html.EditorFor(model => model.CostCatMaterialCost_RT,"DoubleTmpl",new {size="10" ,CalcTag="sumRTCtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.4" /><%: Html.EditorFor(model => model.CostCatTotGeneral_RT,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Turnaround</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.5" /><%: Html.EditorFor(model => model.CostCatScaffolding_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumTACtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.6" /><%: Html.EditorFor(model => model.CostCatPainting_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumTACtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.7" /><%: Html.EditorFor(model => model.CostCatInsulation_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumTACtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_101.1" /><%: Html.EditorFor(model => model.CostCatMaterialCost_TA,"DoubleTmpl",new {size="10" ,CalcTag="sumTACtg" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_100.8" /><%: Html.EditorFor(model => model.CostCatTotGeneral_TA,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1" ,@class="ttlfld" ,ReadOnly="True"})%></li>
                    </ul>

                </div>
                <%: Html.ValidationMessageFor(model => model.CostCatScaffolding_RT)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatPainting_RT)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatInsulation_RT)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatMaterialCost_RT)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatTotGeneral_RT)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatScaffolding_TA)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatPainting_TA)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatInsulation_TA)%> 
                <%: Html.ValidationMessageFor(model => model.CostCatMaterialCost_TA)%>
                <%: Html.ValidationMessageFor(model => model.CostCatTotGeneral_TA)%>
                


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>
        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>


    
    <script type = "text/javascript" > function loadHelpReferences() {
    var objHelpRefs = Section_HelpLst;
    for (i = objHelpRefs.length - 1; i >= 0; i--) {
        var qidNm = objHelpRefs[i]['HelpID'];
        if ($('li[qid^="' + qidNm + '"]').length) {
            var $elm = $('li[qid^="' + qidNm + '"]');
            $elm.append('<img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>');
            $elm.find('img.tooltipBtn').css('visibility', 'visible');
        } else if ($('div[qid^="' + qidNm + '"]').length) {
            $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible');
        }
    }
}

function formatResult(el, value) {
    var format = 'N';
    if (el.attr('NumFormat') == 'Int') format = 'n0';
    if (el.attr('DecPlaces') != null) {
        if (el.attr('NumFormat') == 'Number') format = 'n' + el.attr('DecPlaces');
    }
    if (value != undefined && isNaN(value)) value = 0;
    var tempval = Globalize.parseFloat(value.toString(), 'en');
    if (isFinite(tempval)) {
        el.val(Globalize.format(tempval, format));
        var idnm = '#_' + el.attr('id');
        var $f = $(idnm);
        if ($f.length) {
            $f.val(el.val());
        }
    } else {
        el.val('');
    }
}
$("input[CalcTag*='sumRTCtg']").sum({
    bind: "keyup",
    selector: "#CostCatTotGeneral_RT",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#CostCatTotGeneral_RT').attr('readonly', 'readonly');
$('#CostCatTotGeneral_RT').change(function() {
    $(this).validate();
});
$("input[CalcTag*='sumTACtg']").sum({
    bind: "keyup",
    selector: "#CostCatTotGeneral_TA",
    oncalc: function(value, settings) {
        if (value != undefined && !isNaN(value)) {
            formatResult($(settings.selector), value); /*$(settings.selector).trigger('blur');*/
        }
    }
});
$('#CostCatTotGeneral_TA').attr('readonly', 'readonly');
$('#CostCatTotGeneral_TA').change(function() {
    $(this).validate();
});
var secComments = Section_Comments;
var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>';
for (i = secComments.length - 1; i >= 0; i--) {
    var qidNm = secComments[i]['CommentID'];
    if ($('img[qid="' + qidNm + '"]').length) {
        $('img[qid="' + qidNm + '"]').attr('src', redCmtImg);
    }
} <%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %> $('.rowdesc li div').prepend(function(index, html) {
    var gridId = $(this).closest('.grid').attr('id');
    if ($.trim(html).indexOf('nbsp', 0) == -1) {
        var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>';
    }
    $(this).html(newHTML);
});
$('img[suid]').click(function() {
    var suid = $(this).attr('suid');
    var qid = $(this).attr('qid');
    recordComment(suid, qid);
}); 
</script>

</asp:Content>
