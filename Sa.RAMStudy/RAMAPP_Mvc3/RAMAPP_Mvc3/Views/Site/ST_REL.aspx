﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_RELModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 12/12/2013 10:02:45  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_REL").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });
    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_REL", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_REL" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Reliability</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Site RAM Performance Improvement Process</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Computerized Maintenance Management System (CMMS)</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle"><a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">Reliability</a></p>
            <p><i>Check those factors that are included in your written procedures and/or standards for <a href="#" tooltip="Equipment that has been evaluated and classified as critical due to its potential impact on safety, environment, quality, production, or cost.">critical equipment</a>. A yes answer must comply with any related standards/requirements listed in the Solomon Glossary.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_REL_GEN' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_REL_100.1'><span class='numbering'>1.</span> Well defined methodology for identifying <a href="#" tooltip="Equipment that has been evaluated and classified as critical due to its potential impact on safety, environment, quality, production, or cost.">critical equipment</a> per Glossary definition based on risk.</li>
                        <%--                        <li class='gridrowtext' qid='QSTID_REL_100.4'><span class='numbering'>2.</span> <a href="#" tooltip="To automatically capture and record downtime by means of an automatic data recording device such as a distributive control system (DCS), programmable logic controller (PLC), or other automated means.">Automatic recording of downtime</a></li>--%>
                        <li class='gridrowtext' qid='QSTID_REL_100.7'><span class='numbering'>2.</span> <a href="#" tooltip="An analysis of all costs associated with the life cycle of equipment or assets including design, manufacture, operation, maintenance, and disposal. This analysis is used to determine the best equipment or material based on the total life cycle cost, not just the original purchase price.">Life Cycle Cost Analysis</a> (LCCA)</li>
                        <li class='gridrowtext' qid='QSTID_REL_100.8'><span class='numbering'>3.</span> <a href="#" tooltip="A robust methodology (compliant with SAE JA-1011) designed to ensure that equipment meets its intended function through the analysis of potential failure mechanisms and the implementation of proactive measures designed to avoid failure which provides a structured framework for analyzing the potential failures of a defined group of physical assets in order to develop a functional failure management strategy.  This strategy will include scheduled proactive activities designed to provide an acceptable level of reliability, with an acceptable level of risk, in an efficient and cost-effective manner.  The goal of RCM is to avoid or reduce asset failure consequences, not necessarily to avoid asset failures.">Reliability-Centered Maintenance (RCM)</a></li>
                        <li class='gridrowtext' qid='QSTID_REL_100.9'><span class='numbering'>4.</span> Equipment <a href="#" tooltip="A documented standard for a specified asset type such as horizontal centrifugal pump and is used to ensure the quality and fitness-for-purpose requirements when installed in a process application. These standards may be internal or may come from a recognized standards body (e.g., ASME, ANSI, API, etc.). These standards should include specifications related to the reliability and maintainability of the specified equipment.">Engineering Standards</a> (well defined and mandatory)</li>
                        <li class='gridrowtext' qid='QSTID_REL_101'><span class='numbering'>5.</span> <a href="#" tooltip="The application of a systematic risk of failure evaluation procedure for equipment employing inspection techniques to assess the probability and consequence of failure. An example can be found in API RP-580 that describes the application of Risk-Based Inspection for hydrocarbon process facilities. Government regulations that require systematic inspections of pressure vessels and equipment on a risk basis can be considered as equivalent to API RP-580 or other equivalent Risk-Based Inspection methodologies.">Risk-Based Inspection</a></li>
                        <li class='gridrowtext' qid='QSTID_REL_101.3'><span class='numbering'>6.</span> <a href="#" tooltip="A process (compliant with IEC 60812 / SAE J1739 or similar standard) in which each potential failure mode in every component of an asset is analyzed to determine its effect on other components and on the required function of the asset.">Failure Mode and Effect Analysis</a></li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Rotating Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_100.1" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelCritEquipMeth_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelCritEquipMeth_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_100.4" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelDowntimeAutoRec_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelDowntimeAutoRec_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_100.7" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelLCCA_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelLCCA_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_100.8" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRCM_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRCM_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_100.9" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelEquipEngrStd_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelEquipEngrStd_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_101" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRiskBasedInsp_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRiskBasedInsp_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_101.3" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelFailureAnalysis_RE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelFailureAnalysis_RE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Fixed Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_101.4" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelCritEquipMeth_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelCritEquipMeth_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_101.7" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelDowntimeAutoRec_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelDowntimeAutoRec_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_102" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelLCCA_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelLCCA_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_102.1" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRCM_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRCM_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_102.2" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelEquipEngrStd_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelEquipEngrStd_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_102.3" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRiskBasedInsp_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRiskBasedInsp_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_102.6" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelFailureAnalysis_FP, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelFailureAnalysis_FP, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Instrument & Electrical Equipment</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_102.7" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelCritEquipMeth_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelCritEquipMeth_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_103" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelDowntimeAutoRec_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelDowntimeAutoRec_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>--%>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_103.3" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelLCCA_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelLCCA_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_103.4" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRCM_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRCM_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_103.5" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelEquipEngrStd_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelEquipEngrStd_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_103.6" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRiskBasedInsp_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelRiskBasedInsp_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_REL_103.9" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelFailureAnalysis_IE, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteRelFailureAnalysis_IE, "N") %></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteRelFailureAnalysis_IE)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle"><a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">Site</a> <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> Performance Improvement Process</p>
            <p><i>Which of the following improvement processes are utilized (including a documented procedure)?</i></p>



            <div class="questionblock" qid="QSTID_REL_110">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110' /></span> <span class='numbering'>1.</span> <a href="#" tooltip="5S is the name of a workplace organization method that uses a list of five Japanese words: seiri, seiton, seiso, seiketsu, and shitsuke. Transliterated or translated into English, the words are Sort, Straighten, Sweep, Standardize and Sustain. The list describes how to organize a work space for efficiency and effectiveness by identifying and storing the items used, maintaining the area and items, and sustaining the new order.">5S</a> (workplace organzational method)
					 					 <span>
                                              <img id="Img1" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIP5S, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIP5S, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelPIP5S)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


<%--            <div class="questionblock" qid="QSTID_REL_110.1">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110.1' /></span> <span class='numbering'>2.</span> <a href="#" tooltip="A proactive approach that essentially aims to identify potential asset performance problems as soon as possible and plan to prevent the problem from adversely impacting asset capability.  Operations personnel are trained and required to perform basic RAM tasks including asset condition monitoring, basic maintenance task completion and potential problem identification/reporting.">Total Productive Maintenance (TPM)</a>
                    <span>
                        <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPTPM, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPTPM, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelPIPTPM)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>--%>

               <div class="questionblock" qid="QSTID_REL_110.8">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110.8' /></span> <span class='numbering'>2.</span>Risk Based Maintenance  (RBM)
                    <span>
                        <img id="Img2" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPRBM, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPRBM, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelPIPRBM)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <div class="questionblock" qid="QSTID_REL_110.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110.2' /></span> <span class='numbering'>3.</span> Number of Six Sigma/DMAIC certified Green Belt personnel
					 					 <span>
                                              <img id="Img3" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteRelPIP6SigmaGreenBelt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteRelPIP6SigmaGreenBelt)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_110.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110.3' /></span> <span class='numbering'>4.</span> Number of Six Sigma/DMAIC certified Black Belt personnel
					 					 <span>
                                              <img id="Img4" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteRelPIP6SigmaBlackBelt,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteRelPIP6SigmaBlackBelt)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <%--            <div class="questionblock" qid="QSTID_REL_110.4">

                <span class='labelDesc'>Which of the following statistical analysis tools are utilized in <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> applications?</span>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_110.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110.5' /></span> <span class='numbering'>5.</span> Monte Carlo simulation
					 					 <span>
                                              <img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPMonteCarloSim, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPMonteCarloSim, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelPIPMonteCarloSim)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_110.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110.6' /></span> <span class='numbering'>6.</span> Poisson distribution
					 					 <span>
                                              <img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPPoissonDist, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPPoissonDist, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelPIPPoissonDist)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_110.7">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_110.7' /></span> <span class='numbering'>7.</span> Weibull analysis
					 					 <span>
                                              <img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPWeibullAnalysis, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelPIPWeibullAnalysis, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelPIPWeibullAnalysis)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>--%>
        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle"><a href="#" tooltip="Software system enabling electronic management of asset maintenance information.  The software will typically include asset master data, work order management, planning, scheduling, spare parts and reporting.">Computerized Maintenance Management System (CMMS)</a></p>
            <p><i></i></p>



            <div class="questionblock" qid="QSTID_REL_120">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_120' /></span> <span class='numbering'>1.</span> Is the <a href="#" tooltip="An acronym for Reliability and Maintenance.">RAM</a> CMMS configured to support <a href="#" tooltip="The probability that equipment will perform as designed when operated under design conditions.">reliability</a> and maintenance work processes?
					 					 <span>
                                              <img id="Img8" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppRAMProcess, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppRAMProcess, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelCMMSSuppRAMProcess)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_120.1">

                <span class='labelDesc'>Is an additional (i.e. bolt on) system utilized to supplement the CMMS in these areas?</span>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_120.2">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_120.2' /></span> <span class='numbering'>2.</span> Planning
					 					 <span>
                                              <img id="Img9" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppPlanning, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppPlanning, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelCMMSSuppPlanning)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_120.3">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_120.3' /></span> <span class='numbering'>3.</span> Scheduling
					 					 <span>
                                              <img id="Img10" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppScheduling, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppScheduling, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelCMMSSuppScheduling)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>


            <div class="questionblock" qid="QSTID_REL_120.4">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_120.4' /></span> <span class='numbering'>4.</span> Management of Change
					 					 <span>
                                              <img id="Img11" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppMgmtChng, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSSuppMgmtChng, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelCMMSSuppMgmtChng)%>
                </div>

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
<%--            <div class="questionblock" qid="QSTID_REL_120.5">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_120.5' /></span> <span class='numbering'>5.</span> Is maintenance rework measured?
					 		<span>
                                 <img id="Img5" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <table align="center" border="0" cellspacing="4">
                        <tr>
                            <td>Yes</td>
                            <td>&nbsp;&nbsp;No</td>
                        </tr>
                        <tr>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSReworkMeasure, "Y") %></td>
                            <td><%= Html.RadioButtonFor(model => model.SiteRelCMMSReworkMeasure, "N") %></td>
                        </tr>
                    </table>
                    <%: Html.ValidationMessageFor(model => model.SiteRelCMMSReworkMeasure)%>
                </div>
            </div>--%>
<%--            <div class="questionblock" qid="QSTID_REL_120.6">
                <div style="margin: 10px 5px;">
                    <span>
                        <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_120.6' /></span> <span class='numbering'>6.</span>  # of Maintenance Rework Work Orders
					 					 <span>
                                              <img id="Img6" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                </div>
                <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                    <%: Html.EditorFor(model => model.SiteRelCMMSMaintRework,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0"})%> <%: Html.ValidationMessageFor(model => model.SiteRelCMMSMaintRework)%>
                </div>
                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
                <div class="questionblock" qid="QSTID_REL_120.7">
                    <div style="margin: 10px 5px;">
                        <span>
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_REL_120.7' /></span> <span class='numbering'>7.</span> % Rework
					 					 <span>
                                              <img id="Img7" src="~/images/help-icon-red.png" runat="server" class="tooltipBtn" /></span>
                    </div>
                    <div style="display: inline-block; margin: 5px 5px;" class="editor-field">


                        <%: Html.EditorFor(model => model.SiteRelCMMSPctRework,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="0" ,@class="formfld",ReadOnly="True"})%> <%: Html.ValidationMessageFor(model => model.SiteRelCMMSPctRework)%>
                    </div>
                    <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
                </div>--%>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>


        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



    <script type="text/javascript">  function loadHelpReferences() { var objHelpRefs = Section_HelpLst; for (i = objHelpRefs.length - 1; i >= 0; i--) { var qidNm = objHelpRefs[i]['HelpID']; if ($('li[qid^="' + qidNm + '"]').length) { var $elm = $('li[qid^="' + qidNm + '"]'); $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>'); $elm.find('img.tooltipBtn').css('visibility', 'visible'); } else if ($('div[qid^="' + qidNm + '"]').length) { $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible'); } } } var secComments = Section_Comments; var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>'; for (i = secComments.length - 1; i >= 0; i--) { var qidNm = secComments[i]['CommentID']; if ($('img[qid="' + qidNm + '"]').length) { $('img[qid="' + qidNm + '"]').attr('src', redCmtImg); } }<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(function (index, html) { var gridId = $(this).closest('.grid').attr('id'); if ($.trim(html).indexOf('nbsp', 0) == -1) { var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>'; } $(this).html(newHTML); }); $('img[suid]').click(function () { var suid = $(this).attr('suid'); var qid = $(this).attr('qid'); recordComment(suid, qid); });</script>

</asp:Content>
