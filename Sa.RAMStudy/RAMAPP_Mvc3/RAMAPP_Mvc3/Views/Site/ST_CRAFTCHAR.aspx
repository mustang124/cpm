﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Question.Master" Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.ST_CRAFTCHARModel>" %>

<%@ Import Namespace="System.Globalization" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%//------------------------------------------------------
  // Date Created : 02/04/2015 12:40:25  
  //-----------------------------------------------------%>
    <script type="text/javascript" src="<%=Url.Content("~/Scripts/jquery.smartTab.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard 	
            $("#wizard_ST_CRAFTCHAR").smartTab({ transitionEffect: 'fade', keyNavigation: false });

            /* $('li .stepDesc small').each(function (index) {
            var elementTitleId = $(this).closest('a').attr('href') + ' .StepTitle';

            $(this).text($(elementTitleId).text());

            });*/

        });

    </script>
    <% Html.EnableClientValidation(); %>
    <% using (Ajax.BeginForm("ST_CRAFTCHAR", new AjaxOptions { HttpMethod = "Post", OnComplete = "SaveResult" }))
       {
           bool uomPref = true;
    %>
    <div id="wizard_ST_CRAFTCHAR" class="stContainer">
        <ul class="tabContainer">
            <li><a href='#step-1'>
                <label class='stepNumber'>1</label><span class='stepDesc'>Section 1<br />
                    <small>Rotating Equipment Craft</small> </span></a></li>
            <li><a href='#step-2'>
                <label class='stepNumber'>2</label><span class='stepDesc'>Section 2<br />
                    <small>Fixed Equipment Craft</small> </span></a></li>
            <li><a href='#step-3'>
                <label class='stepNumber'>3</label><span class='stepDesc'>Section 3<br />
                    <small>Instrument and Electrical Equipment Craft</small> </span></a></li>
            <li><a href='#step-4'>
                <label class='stepNumber'>4</label><span class='stepDesc'>Section 4<br />
                    <small>All Crafts</small> </span></a></li>
        </ul>



        <div class="SubSectionTab" id="step-1">

            <p class="StepTitle"><a href="#" tooltip="Equipment that turns on an axis and is designed to impart kinetic energy on a process material. Common examples include centrifugal pumps and compressors, electric motors, steam turbines, etc. Rotating equipment craft include machinery mechanics such as millwrights and rotating equipment condition monitoring technicians. Include process equipment and exclude non-process equipment.">Rotating Equipment</a> <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></p>
            <p><i>Report the characteristics below for the Routine Maintenance <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> (exclude <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> and maintenance capital) work for each <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> category. Report Company and Contractor personnel separately. If personnel are multi-craft skilled, please prorate equivalent numbers between <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">crafts</a>.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_CRAFTCHAR_RE' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <%--                        <li class='gridrowtext' qid='QSTID_MT_CHAR_300.1'><span class='numbering'>1.</span> What is the planned number of hours of skills and/or technical training per <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> work per year?</li>--%>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.3'><span class='numbering'>1.</span> What is the average number of hours of skills and/or technical training provided per <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> worker per year?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.1'><span class='numbering'>2.</span> What percentage of <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel are permanently assigned to a unit or area and only exceptionally work in another unit or area?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.2'><span class='numbering'>3.</span> For <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel, what is the average number of years of experience in their present role on the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a>?</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Company</li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_300.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirPlanTrainHrs_CORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_100.3" /><%: Html.EditorFor(model => model.SiteWorkEnvirAvgTrainHrs_CORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_100.3' /></span><br />
                            <select id="SiteWorkEnvirAvgTrainHrs_CORE" name="SiteWorkEnvirAvgTrainHrs_CORE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='<40 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CORE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CORE.Equals("<40 hours")? "SELECTED" : String.Empty):String.Empty %>><40 hours</option>
                                <option value='40-80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CORE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CORE.Equals("40-80 hours")? "SELECTED" : String.Empty):String.Empty %>>40-80 hours</option>
                                <option value='>80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CORE!= null ?(Model.SiteWorkEnvirAvgTrainHrs_CORE.Equals(">80 hours")? "SELECTED" : String.Empty):String.Empty %>>>80 hours</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_100.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirSector_CORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_100.2" /><%: Html.EditorFor(model => model.SiteWorkEnvirExperInJob_CORE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_100.2' /></span><br />
                            <select id="SiteWorkEnvirExperInJob_CORE" name="SiteWorkEnvirExperInJob_CORE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0-3 years' <%= Model.SiteWorkEnvirExperInJob_CORE != null ?(Model.SiteWorkEnvirExperInJob_CORE.Equals("0-3 years")? "SELECTED" : String.Empty):String.Empty %>>0-3 years</option>
                                <option value='4-7 years' <%= Model.SiteWorkEnvirExperInJob_CORE != null ?(Model.SiteWorkEnvirExperInJob_CORE.Equals("4-7 years")? "SELECTED" : String.Empty):String.Empty %>>4-7 years</option>
                                <option value='>7 years' <%= Model.SiteWorkEnvirExperInJob_CORE != null ?(Model.SiteWorkEnvirExperInJob_CORE.Equals(">7 years")? "SELECTED" : String.Empty):String.Empty %>>>7 years</option>
                            </select>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Contractor</li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_400.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirPlanTrainHrs_CRRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_200.3" /><%: Html.EditorFor(model => model.SiteWorkEnvirAvgTrainHrs_CRRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_200.3' /></span><br />
                            <select id="SiteWorkEnvirAvgTrainHrs_CRRE" name="SiteWorkEnvirAvgTrainHrs_CRRE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='<40 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRRE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRRE.Equals("<40 hours")? "SELECTED" : String.Empty):String.Empty %>><40 hours</option>
                                <option value='40-80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRRE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRRE.Equals("40-80 hours")? "SELECTED" : String.Empty):String.Empty %>>40-80 hours</option>
                                <option value='>80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRRE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRRE.Equals(">80 hours")? "SELECTED" : String.Empty):String.Empty %>>>80 hours</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_200.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirSector_CRRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_200.2" /><%: Html.EditorFor(model => model.SiteWorkEnvirExperInJob_CRRE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_200.2' /></span><br />
                            <select id="SiteWorkEnvirExperInJob_CRRE" name="SiteWorkEnvirExperInJob_CRRE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0-3 years' <%= Model.SiteWorkEnvirExperInJob_CRRE != null ?(Model.SiteWorkEnvirExperInJob_CRRE.Equals("0-3 years")? "SELECTED" : String.Empty):String.Empty %>>0-3 years</option>
                                <option value='4-7 years' <%= Model.SiteWorkEnvirExperInJob_CRRE != null ?(Model.SiteWorkEnvirExperInJob_CRRE.Equals("4-7 years")? "SELECTED" : String.Empty):String.Empty %>>4-7 years</option>
                                <option value='>7 years' <%= Model.SiteWorkEnvirExperInJob_CRRE != null ?(Model.SiteWorkEnvirExperInJob_CRRE.Equals(">7 years")? "SELECTED" : String.Empty):String.Empty %>>>7 years</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CORE_Range)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CORE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_CORE_Range)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirSector_CORE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirSector_CRRE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_CRRE_Range)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CRRE_Range)%>
                 <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_CRRE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CRRE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirPlanTrainHrs_CORE)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirPlanTrainHrs_CRRE)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-2">

            <p class="StepTitle"><a href="#" tooltip="Process equipment that is fixed versus rotating equipment, reciprocating equipment, or other moving equipment. Examples of fixed equipment include pipe, heat exchangers, process vessels, distillation towers, tanks, etc. Fixed equipment craft workers include pipefitters, welders, boilermakers, carpenters, painters, equipment operators, insulators, riggers, and other civil crafts. Include process equipment and exclude non-process equipment.">Fixed Equipment</a> <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></p>
            <p><i>Report the characteristics below for the Routine Maintenance <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> (exclude <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> and maintenance capital) work for each <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> category. Report Company and Contractor personnel separately. If personnel are multi-craft skilled, please prorate equivalent numbers between <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">crafts</a>.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_CRAFTCHAR_FP' class='grid editor-field'>
                   <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <%--                        <li class='gridrowtext' qid='QSTID_MT_CHAR_300.1'><span class='numbering'>1.</span> What is the planned number of hours of skills and/or technical training per <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> work per year?</li>--%>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.3'><span class='numbering'>1.</span> What is the average number of hours of skills and/or technical training provided per <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> worker per year?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.1'><span class='numbering'>2.</span> What percentage of <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel are permanently assigned to a unit or area and only exceptionally work in another unit or area?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.2'><span class='numbering'>3.</span> For <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel, what is the average number of years of experience in their present role on the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a>?</li>
                    </ul>                    
                    <ul class="gridbody">
                        <li class="columndesc">Company</li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_110.3" /><%: Html.EditorFor(model => model.SiteWorkEnvirAvgTrainHrs_COFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_110.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirSector_COFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_110.3' /></span><br />
                            <select id="SiteWorkEnvirAvgTrainHrs_COFP" name="SiteWorkEnvirAvgTrainHrs_COFP" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='<40 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_COFP != null ?(Model.SiteWorkEnvirAvgTrainHrs_COFP.Equals("<40 hours")? "SELECTED" : String.Empty):String.Empty %>><40 hours</option>
                                <option value='40-80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_COFP != null ?(Model.SiteWorkEnvirAvgTrainHrs_COFP.Equals("40-80 hours")? "SELECTED" : String.Empty):String.Empty %>>40-80 hours</option>
                                <option value='>80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_COFP != null ?(Model.SiteWorkEnvirAvgTrainHrs_COFP.Equals(">80 hours")? "SELECTED" : String.Empty):String.Empty %>>>80 hours</option>
                            </select>
                        </li>
                           <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_110.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirSector_COFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%>
                           </li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_110.2" /><%: Html.EditorFor(model => model.SiteWorkEnvirExperInJob_COFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_500.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirPlanTrainHrs_COFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_110.2' /></span><br />
                            <select id="SiteWorkEnvirExperInJob_COFP" name="SiteWorkEnvirExperInJob_COFP" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0-3 years' <%= Model.SiteWorkEnvirExperInJob_COFP != null ?(Model.SiteWorkEnvirExperInJob_COFP.Equals("0-3 years")? "SELECTED" : String.Empty):String.Empty %>>0-3 years</option>
                                <option value='4-7 years' <%= Model.SiteWorkEnvirExperInJob_COFP != null ?(Model.SiteWorkEnvirExperInJob_COFP.Equals("4-7 years")? "SELECTED" : String.Empty):String.Empty %>>4-7 years</option>
                                <option value='>7 years' <%= Model.SiteWorkEnvirExperInJob_COFP != null ?(Model.SiteWorkEnvirExperInJob_COFP.Equals(">7 years")? "SELECTED" : String.Empty):String.Empty %>>>7 years</option>
                            </select>
                        </li>

                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Contractor</li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_600.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirPlanTrainHrs_CRFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_210.3" /><%: Html.EditorFor(model => model.SiteWorkEnvirAvgTrainHrs_CRFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_210.3' /></span><br />
                            <select id="SiteWorkEnvirAvgTrainHrs_CRFP" name="SiteWorkEnvirAvgTrainHrs_CRFP"style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='<40 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRFP != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRFP.Equals("<40 hours")? "SELECTED" : String.Empty):String.Empty %>><40 hours</option>
                                <option value='40-80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRFP != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRFP.Equals("40-80 hours")? "SELECTED" : String.Empty):String.Empty %>>40-80 hours</option>
                                <option value='>80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRFP != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRFP.Equals(">80 hours")? "SELECTED" : String.Empty):String.Empty %>>>80 hours</option>
                            </select>
                        </li>

                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_210.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirSector_CRFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_210.2" /><%: Html.EditorFor(model => model.SiteWorkEnvirExperInJob_CRFP,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_210.2' /></span><br />
                            <select id="SiteWorkEnvirExperInJob_CRFP" name="SiteWorkEnvirExperInJob_CRFP" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0-3 years' <%= Model.SiteWorkEnvirExperInJob_CRFP != null ?(Model.SiteWorkEnvirExperInJob_CRFP.Equals("0-3 years")? "SELECTED" : String.Empty):String.Empty %>>0-3 years</option>
                                <option value='4-7 years' <%= Model.SiteWorkEnvirExperInJob_CRFP != null ?(Model.SiteWorkEnvirExperInJob_CRFP.Equals("4-7 years")? "SELECTED" : String.Empty):String.Empty %>>4-7 years</option>
                                <option value='>7 years' <%= Model.SiteWorkEnvirExperInJob_CRFP != null ?(Model.SiteWorkEnvirExperInJob_CRFP.Equals(">7 years")? "SELECTED" : String.Empty):String.Empty %>>>7 years</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirSector_COFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_COFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_COFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirSector_CRFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_CRFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CRFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirPlanTrainHrs_COFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirPlanTrainHrs_CRFP)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirSector_COFP_Range)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirPlanTrainHrs_COFP_Rg)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CRFP_Rg)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_CRFP_Rg)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-3">

            <p class="StepTitle">Instrument and Electrical Equipment <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a></p>
            <p><i>Report the characteristics below for the Routine Maintenance <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> (exclude <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> and maintenance capital) work for each <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> category. Report Company and Contractor personnel separately. If personnel are multi-craft skilled, please prorate equivalent numbers between <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">crafts</a>.</i></p>



            <div class="questionblock" qid="">


                <div id='ST_CRAFTCHAR_IE' class='grid editor-field'>
                   <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <%--                        <li class='gridrowtext' qid='QSTID_MT_CHAR_300.1'><span class='numbering'>1.</span> What is the planned number of hours of skills and/or technical training per <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> work per year?</li>--%>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.3'><span class='numbering'>1.</span> What is the average number of hours of skills and/or technical training provided per <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> worker per year?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.1'><span class='numbering'>2.</span> What percentage of <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel are permanently assigned to a unit or area and only exceptionally work in another unit or area?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_100.2'><span class='numbering'>3.</span> For <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel, what is the average number of years of experience in their present role on the <a href="#" tooltip="Includes all manufacturing assets on the site such as process units, utilities, fire fighting and safety, raw materials storage, intermediates and products storage, terminals, waste treatment, grounds, and manufacturing buildings assets. Exclude head office facilities, research and development, pilot plant, accommodations, and social assets that are considered non-industrial. Integrated sites with both chemicals and refining operations are classified as a “site” if it shares a common site management team and related site resources.">site</a>?</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Company</li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_700.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirPlanTrainHrs_COIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <%--<li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_120.3" /><%: Html.EditorFor(model => model.SiteWorkEnvirAvgTrainHrs_COIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_120.3' /></span><br />
                            <select id="SiteWorkEnvirAvgTrainHrs_COIE" name="SiteWorkEnvirAvgTrainHrs_COIE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='<40 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_COIE != null ?(Model.SiteWorkEnvirAvgTrainHrs_COIE.Equals("<40 hours")? "SELECTED" : String.Empty):String.Empty %>><40 hours</option>
                                <option value='40-80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_COIE != null ?(Model.SiteWorkEnvirAvgTrainHrs_COIE.Equals("40-80 hours")? "SELECTED" : String.Empty):String.Empty %>>40-80 hours</option>
                                <option value='>80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_COIE != null ?(Model.SiteWorkEnvirAvgTrainHrs_COIE.Equals(">80 hours")? "SELECTED" : String.Empty):String.Empty %>>>80 hours</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_120.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirSector_COIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_120.2" /><%: Html.EditorFor(model => model.SiteWorkEnvirExperInJob_COIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_120.2' /></span><br />
                            <select id="SiteWorkEnvirExperInJob_COIE" name="SiteWorkEnvirExperInJob_COIE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0-3 years' <%= Model.SiteWorkEnvirExperInJob_COIE != null ?(Model.SiteWorkEnvirExperInJob_COIE.Equals("0-3 years")? "SELECTED" : String.Empty):String.Empty %>>0-3 years</option>
                                <option value='4-7 years' <%= Model.SiteWorkEnvirExperInJob_COIE != null ?(Model.SiteWorkEnvirExperInJob_COIE.Equals("4-7 years")? "SELECTED" : String.Empty):String.Empty %>>4-7 years</option>
                                <option value='>7 years' <%= Model.SiteWorkEnvirExperInJob_COIE != null ?(Model.SiteWorkEnvirExperInJob_COIE.Equals(">7 years")? "SELECTED" : String.Empty):String.Empty %>>>7 years</option>
                            </select>
                        </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Contractor</li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_800.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirPlanTrainHrs_CRIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_220.3" /><%: Html.EditorFor(model => model.SiteWorkEnvirAvgTrainHrs_CRIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_220.4' /></span><br />
                            <select id="SiteWorkEnvirAvgTrainHrs_CRIE" name="SiteWorkEnvirAvgTrainHrs_CRIE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='<40 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRIE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRIE.Equals("<40 hours")? "SELECTED" : String.Empty):String.Empty %>><40 hours</option>
                                <option value='40-80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRIE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRIE.Equals("40-80 hours")? "SELECTED" : String.Empty):String.Empty %>>40-80 hours</option>
                                <option value='>80 hours' <%= Model.SiteWorkEnvirAvgTrainHrs_CRIE != null ?(Model.SiteWorkEnvirAvgTrainHrs_CRIE.Equals(">80 hours")? "SELECTED" : String.Empty):String.Empty %>>>80 hours</option>
                            </select>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_220.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirSector_CRIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_220.2" /><%: Html.EditorFor(model => model.SiteWorkEnvirExperInJob_CRIE,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_220.2' /></span><br />
                            <select id="SiteWorkEnvirExperInJob_CRIE" name="SiteWorkEnvirExperInJob_CRIE" style="width:100px;">
                                <option value='-1'>SELECT ONE</option>
                                <option value='0-3 years' <%= Model.SiteWorkEnvirExperInJob_CRIE != null ?(Model.SiteWorkEnvirExperInJob_CRIE.Equals("0-3 years")? "SELECTED" : String.Empty):String.Empty %>>0-3 years</option>
                                <option value='4-7 years' <%= Model.SiteWorkEnvirExperInJob_CRIE != null ?(Model.SiteWorkEnvirExperInJob_CRIE.Equals("4-7 years")? "SELECTED" : String.Empty):String.Empty %>>4-7 years</option>
                                <option value='>7 years' <%= Model.SiteWorkEnvirExperInJob_CRIE != null ?(Model.SiteWorkEnvirExperInJob_CRIE.Equals(">7 years")? "SELECTED" : String.Empty):String.Empty %>>>7 years</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirSector_COIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_COIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_COIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirSector_CRIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_CRIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CRIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirPlanTrainHrs_COIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirPlanTrainHrs_CRIE)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_COIE_Rg)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_COIE_Rg)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirAvgTrainHrs_CRIE_Rg)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirExperInJob_CRIE_Rg)%>


                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>

        </div>
        <div class="SubSectionTab" id="step-4">

            <p class="StepTitle">All <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Crafts</a></p>
            <p><i>Report the characteristics below for the Routine Maintenance <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">Craft</a> (exclude <a href="#" tooltip="A turnaround is a complete unit outage (usually costing more than 0.5% of plant replacement value) planned and scheduled well in advance of the actual stoppage (usually more than 12 months) for the purpose of significant unit inspection, overhaul, and repair. Turnaround expenses and production losses are computed on an annualized basis for the last two turnarounds. (Sum of the last two turnaround expenses and losses divided by two times the average number of years between turnarounds.) It may be necessary to go back more than two turnarounds to account for the full pattern of costs. For example, a plant may be taken down annually, but two of the turnarounds are very small, and every third year a turnaround with an extensive scope of work is executed. The expenses are reported in local currency values for the study year.">T/A</a> and maintenance capital) work for each <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> category. Report Company and Contractor personnel separately. If personnel are multi-craft skilled, please prorate equivalent numbers between <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">crafts</a>.</i></p>
            <div class="questionblock" qid="">


                <div id='ST_CRAFTCHAR_ALL' class='grid editor-field'>
                    <ul class="rowdesc">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_250.1'><span class='numbering'>1.</span> What percentage of <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel belong to a recognized <a href="#" tooltip="A legally recognized representative who is authorized to negotiate wages and benefits on behalf of the worker.">labor union</a>?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_250.2'><span class='numbering'>2.</span> What is the average actual <a href="#" tooltip="Hours worked beyond a standard workweek as a percent of a standard workweek.">overtime</a> percent for the year reported?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_250.6'><span class='numbering'>3.</span> Is there a testing, qualification or certification process to confirm the knowledge and skills of <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> workers in compliance with local regulations (such as OSHA 1910) and applicable to the craft of the individual?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_250.8'><span class='numbering'>4.</span> Percent of <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel formally trained in RCA / RCFA process</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_251.1'><span class='numbering'>5.</span> Are <a href="#" tooltip="Maintenance skilled labor with specialized skills such as millwright, pipefitter, boilermaker, instrument technician, electrician, etc. or with general skills such as a general mechanic or maintenance technician.">craft</a> personnel required to document <a href="#" tooltip="When an asset is unable to perform its required function even if a reduced operating state is still possible. PM/PdM tasks are not considered failures. However if through execution of a PM/PdM task the need for a Maintenance Event is identified, this work is to be considered a failure.">failure</a> causes, defects found and Root Cause opinion in work order history or in a related system that supports analysis?</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_252.1'><span class='numbering'>6.</span> Are maintenance craft personnel qualified/certified in more than one of the following craft types: Fixed Equipment, Rotating Equipment or Instrument & Electrical? </li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_252.2'><span class='numbering'>7.</span> First line maintenance work performed by process operators </li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_252.3'><span class='numbering'>8.</span> Accounting of Craft time on a weekly/monthly basis </li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_252.4'><span class='numbering'>9.</span> Wrench time studies performed </li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_252.5'><span class='numbering'>10.</span> Work orders audited to ensure completeness and accuracy </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Company</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.1" /><%: Html.EditorFor(model => model.SiteWorkEnvirLaborUnion_CO,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.2" /><%: Html.EditorFor(model => model.SiteWorkEnvirOTPcnt_CO,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.6" /><%--<table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRegsCert_CO, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRegsCert_CO, "N") %></td>
                                </tr>
                            </table>--%>
                            <br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRegsCert_CO" name="SiteWorkEnvirRegsCert_CO" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRegsCert_CO != null ?(Model.SiteWorkEnvirRegsCert_CO.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option title="Trade test by company" value='Trade test by company' <%= Model.SiteWorkEnvirRegsCert_CO != null ?(Model.SiteWorkEnvirRegsCert_CO.Equals("Trade test by company")? "SELECTED" : String.Empty):String.Empty %>>Trade test by company</option>
                                    <option title="Full documented evidence of technician competence" value='Full documented evidence of technician competence' <%= Model.SiteWorkEnvirRegsCert_CO != null ?(Model.SiteWorkEnvirRegsCert_CO.Equals("Full documented evidence of technician competence")? "SELECTED" : String.Empty):String.Empty %>>Full documented evidence of technician competence</option>
                                </select>
                            </div>
                        </li>
                        <%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.8" /><%: Html.EditorFor(model => model.SiteWorkEnvirRCARCFA_CO,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_250.81' /></span><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARCFA_CO_Rg" name="SiteWorkEnvirRCARCFA_CO_Rg" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARCFA_CO_Rg != null ?(Model.SiteWorkEnvirRCARCFA_CO_Rg.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option value='1-50%' <%= Model.SiteWorkEnvirRCARCFA_CO_Rg != null ?(Model.SiteWorkEnvirRCARCFA_CO_Rg.Equals("1-50%")? "SELECTED" : String.Empty):String.Empty %>>1-50%</option>
                                    <option value='>50%' <%= Model.SiteWorkEnvirRCARCFA_CO_Rg != null ?(Model.SiteWorkEnvirRCARCFA_CO_Rg.Equals(">50%")? "SELECTED" : String.Empty):String.Empty %>>>50%</option>
                                </select>
                            </div>
                        </li>

                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_251.1" /><%--<table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirReqDocFail_CO, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirReqDocFail_CO, "N") %></td>
                                </tr>
                            </table>--%>
                            <br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirReqDocFail_CO" name="SiteWorkEnvirReqDocFail_CO" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='does not exist' <%= Model.SiteWorkEnvirReqDocFail_CO != null ?(Model.SiteWorkEnvirReqDocFail_CO.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                    <option title="exists but not documented" value='exists but not documented' <%= Model.SiteWorkEnvirReqDocFail_CO != null ?(Model.SiteWorkEnvirReqDocFail_CO.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but not documented</option>
                                    <option title="exists and documented" value='exists and documented' <%= Model.SiteWorkEnvirReqDocFail_CO != null ?(Model.SiteWorkEnvirReqDocFail_CO.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and documented</option>
                                    <option title="documented and training provided" value='documented and training provided' <%= Model.SiteWorkEnvirReqDocFail_CO != null ?(Model.SiteWorkEnvirReqDocFail_CO.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented and training provided</option>
                                    <option title="Documented, training provided, and internally audited" value='Documented, training provided, and internally audited' <%= Model.SiteWorkEnvirReqDocFail_CO != null ?(Model.SiteWorkEnvirReqDocFail_CO.Equals("Documented, training provided, and internally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and internally audited</option>
                                    <option title="Documented, training provided, and both internally and externally audited" value='Documented, training provided, and both internally and externally audited' <%= Model.SiteWorkEnvirReqDocFail_CO != null ?(Model.SiteWorkEnvirReqDocFail_CO.Equals("Documented, training provided, and both internally and externally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and both internally and externally audited</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.1" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARCPQ_CO" name="SiteWorkEnvirRCARCPQ_CO" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option title="none, core craft only" value='none, core craft only' <%= Model.SiteWorkEnvirRCARCPQ_CO != null ?(Model.SiteWorkEnvirRCARCPQ_CO.Equals("none, core craft only")? "SELECTED" : String.Empty):String.Empty %>>none, core craft only</option>
                                    <option title="minor extension beyond core craft" value='minor extension beyond core craft' <%= Model.SiteWorkEnvirRCARCPQ_CO != null ?(Model.SiteWorkEnvirRCARCPQ_CO.Equals("minor extension beyond core craft")? "SELECTED" : String.Empty):String.Empty %>>minor extension beyond core craft</option>
                                    <option title="technicians trained and validated in non core activities focused on completion of whole task" value='technicians trained and validated in non core activities focused on completion of whole task' <%= Model.SiteWorkEnvirRCARCPQ_CO != null ?(Model.SiteWorkEnvirRCARCPQ_CO.Equals("technicians trained and validated in non core activities focused on completion of whole task")? "SELECTED" : String.Empty):String.Empty %>>technicians trained and validated in non core activities focused on completion of whole task</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.2" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARMWPO_CO" name="SiteWorkEnvirRCARMWPO_CO" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARMWPO_CO != null ?(Model.SiteWorkEnvirRCARMWPO_CO.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option value='informal' <%= Model.SiteWorkEnvirRCARMWPO_CO != null ?(Model.SiteWorkEnvirRCARMWPO_CO.Equals("informal")? "SELECTED" : String.Empty):String.Empty %>>informal</option>
                                    <option title="formal list of work to be done by operators, training and validation in place" value='formal list of work to be done by operators, training and validation in place' <%= Model.SiteWorkEnvirRCARMWPO_CO != null ?(Model.SiteWorkEnvirRCARMWPO_CO.Equals("formal list of work to be done by operators, training and validation in place")? "SELECTED" : String.Empty):String.Empty %>>formal list of work to be done by operators, training and validation in place</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.3" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARACTB_CO" name="SiteWorkEnvirRCARACTB_CO" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARACTB_CO != null ?(Model.SiteWorkEnvirRCARACTB_CO.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option title="times allocated to work order" value='times allocated to work order' <%= Model.SiteWorkEnvirRCARACTB_CO != null ?(Model.SiteWorkEnvirRCARACTB_CO.Equals("times allocated to work order")? "SELECTED" : String.Empty):String.Empty %>>times allocated to work order</option>
                                    <option title="times allocated to work order, with improvement plans and efficiency targets" value='times allocated to work order, with improvement plans and efficiency targets'<%= Model.SiteWorkEnvirRCARACTB_CO != null ?(Model.SiteWorkEnvirRCARACTB_CO.Equals("times allocated to work order, with improvement plans and efficiency targets")? "SELECTED" : String.Empty):String.Empty %>>times allocated to work order, with improvement plans and efficiency targets</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.4" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARWTSP_CO" name="SiteWorkEnvirRCARWTSP_CO" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARWTSP_CO != null ?(Model.SiteWorkEnvirRCARWTSP_CO.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option  value='occasional' <%= Model.SiteWorkEnvirRCARWTSP_CO != null ?(Model.SiteWorkEnvirRCARWTSP_CO.Equals("occasional")? "SELECTED" : String.Empty):String.Empty %>>occasional</option>
                                    <option title="Regularly undertaken, with improvement plans and targets" value='Regularly undertaken, with improvement plans and targets'<%= Model.SiteWorkEnvirRCARWTSP_CO != null ?(Model.SiteWorkEnvirRCARWTSP_CO.Equals("Regularly undertaken, with improvement plans and targets")? "SELECTED" : String.Empty):String.Empty %>>Regularly undertaken, with improvement plans and targets</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.5" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARWPA_CO" name="SiteWorkEnvirRCARWPA_CO" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='not performed' <%= Model.SiteWorkEnvirRCARWPA_CO != null ?(Model.SiteWorkEnvirRCARWPA_CO.Equals("not performed")? "SELECTED" : String.Empty):String.Empty %>>not performed</option>
                                    <option  value='occasional' <%= Model.SiteWorkEnvirRCARWPA_CO != null ?(Model.SiteWorkEnvirRCARWPA_CO.Equals("occasional")? "SELECTED" : String.Empty):String.Empty %>>occasional</option>
                                    <option title="on a tracked  systematic basis with feedback given to crafts for improvement" value='on a tracked  systematic basis with feedback given to crafts for improvement'<%= Model.SiteWorkEnvirRCARWPA_CO != null ?(Model.SiteWorkEnvirRCARWPA_CO.Equals("on a tracked  systematic basis with feedback given to crafts for improvement")? "SELECTED" : String.Empty):String.Empty %>>on a tracked  systematic basis with feedback given to crafts for improvement</option>
                                </select>
                            </div>
                        </li>

                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Contractor</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.4" /><%: Html.EditorFor(model => model.SiteWorkEnvirLaborUnion_CR,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.5" /><%: Html.EditorFor(model => model.SiteWorkEnvirOTPcnt_CR,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.7" /><%--<table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRegsCert_CR, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRegsCert_CR, "N") %></td>
                                </tr>
                            </table>--%>
                            <br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRegsCert_CR" name="SiteWorkEnvirRegsCert_CR" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRegsCert_CR != null ?(Model.SiteWorkEnvirRegsCert_CR.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option title="Trade test by company" value='Trade test by company' <%= Model.SiteWorkEnvirRegsCert_CR != null ?(Model.SiteWorkEnvirRegsCert_CR.Equals("Trade test by company")? "SELECTED" : String.Empty):String.Empty %>>Trade test by company</option>
                                    <option title="Full documented evidence of technician competence" value='Full documented evidence of technician competence' <%= Model.SiteWorkEnvirRegsCert_CR != null ?(Model.SiteWorkEnvirRegsCert_CR.Equals("Full documented evidence of technician competence")? "SELECTED" : String.Empty):String.Empty %>>Full documented evidence of technician competence</option>
                                </select>
                            </div>
                        </li>
<%--                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_250.9" /><%: Html.EditorFor(model => model.SiteWorkEnvirRCARCFA_CR,"DoubleTmpl",new {size="10" ,NumFormat="Number" ,DecPlaces="1"})%></li>--%>
                        <li class="gridrow">
                            <span>
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid='QSTID_MT_CHAR_250.91' /></span><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARCFA_CR_Rg" name="SiteWorkEnvirRCARCFA_CR_Rg" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARCFA_CR_Rg != null ?(Model.SiteWorkEnvirRCARCFA_CR_Rg.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option value='1-50%' <%= Model.SiteWorkEnvirRCARCFA_CR_Rg != null ?(Model.SiteWorkEnvirRCARCFA_CR_Rg.Equals("1-50%")? "SELECTED" : String.Empty):String.Empty %>>1-50%</option>
                                    <option value='>50%' <%= Model.SiteWorkEnvirRCARCFA_CR_Rg != null ?(Model.SiteWorkEnvirRCARCFA_CR_Rg.Equals(">50%")? "SELECTED" : String.Empty):String.Empty %>>>50%</option>
                                </select>
                            </div>
                        </li>

                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_251.2" /><%--<table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirReqDocFail_CR, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirReqDocFail_CR, "N") %></td>
                                </tr>
                            </table>--%>
                            <br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirReqDocFail_CR" name="SiteWorkEnvirReqDocFail_CR" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='does not exist' <%= Model.SiteWorkEnvirReqDocFail_CR != null ?(Model.SiteWorkEnvirReqDocFail_CR.Equals("does not exist")? "SELECTED" : String.Empty):String.Empty %>>does not exist</option>
                                    <option title="exists but not documented" value='exists but not documented' <%= Model.SiteWorkEnvirReqDocFail_CR != null ?(Model.SiteWorkEnvirReqDocFail_CR.Equals("exists but not documented")? "SELECTED" : String.Empty):String.Empty %>>exists but not documented</option>
                                    <option title="exists and documented" value='exists and documented' <%= Model.SiteWorkEnvirReqDocFail_CR != null ?(Model.SiteWorkEnvirReqDocFail_CR.Equals("exists and documented")? "SELECTED" : String.Empty):String.Empty %>>exists and documented</option>
                                    <option title="documented and training provided" value='documented and training provided' <%= Model.SiteWorkEnvirReqDocFail_CR != null ?(Model.SiteWorkEnvirReqDocFail_CR.Equals("documented and training provided")? "SELECTED" : String.Empty):String.Empty %>>documented and training provided</option>
                                    <option title="Documented, training provided, and internally audited" value='Documented, training provided, and internally audited' <%= Model.SiteWorkEnvirReqDocFail_CR != null ?(Model.SiteWorkEnvirReqDocFail_CR.Equals("Documented, training provided, and internally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and internally audited</option>
                                    <option title="Documented, training provided, and both internally and externally audited" value='Documented, training provided, and both internally and externally audited' <%= Model.SiteWorkEnvirReqDocFail_CR != null ?(Model.SiteWorkEnvirReqDocFail_CR.Equals("Documented, training provided, and both internally and externally audited")? "SELECTED" : String.Empty):String.Empty %>>Documented, training provided, and both internally and externally audited</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.6" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARCPQ_CR" name="SiteWorkEnvirRCARCPQ_CR" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option title="none, core craft only" value='none, core craft only' <%= Model.SiteWorkEnvirRCARCPQ_CR != null ?(Model.SiteWorkEnvirRCARCPQ_CR.Equals("none, core craft only")? "SELECTED" : String.Empty):String.Empty %>>none, core craft only</option>
                                    <option title="minor extension beyond core craft" value='minor extension beyond core craft' <%= Model.SiteWorkEnvirRCARCPQ_CR != null ?(Model.SiteWorkEnvirRCARCPQ_CR.Equals("minor extension beyond core craft")? "SELECTED" : String.Empty):String.Empty %>>minor extension beyond core craft</option>
                                    <option title="technicians trained and validated in non core activities focused on completion of whole task" value='technicians trained and validated in non core activities focused on completion of whole task' <%= Model.SiteWorkEnvirRCARCPQ_CR != null ?(Model.SiteWorkEnvirRCARCPQ_CR.Equals("technicians trained and validated in non core activities focused on completion of whole task")? "SELECTED" : String.Empty):String.Empty %>>technicians trained and validated in non core activities focused on completion of whole task</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.7" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARMWPO_CR" name="SiteWorkEnvirRCARMWPO_CR" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARMWPO_CR != null ?(Model.SiteWorkEnvirRCARMWPO_CR.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option value='informal' <%= Model.SiteWorkEnvirRCARMWPO_CR != null ?(Model.SiteWorkEnvirRCARMWPO_CR.Equals("informal")? "SELECTED" : String.Empty):String.Empty %>>informal</option>
                                    <option title="formal list of work to be done by operators, training and validation in place" value='formal list of work to be done by operators, training and validation in place' <%= Model.SiteWorkEnvirRCARMWPO_CR != null ?(Model.SiteWorkEnvirRCARMWPO_CR.Equals("formal list of work to be done by operators, training and validation in place")? "SELECTED" : String.Empty):String.Empty %>>formal list of work to be done by operators, training and validation in place</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.8" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARACTB_CR" name="SiteWorkEnvirRCARACTB_CR" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARACTB_CR != null ?(Model.SiteWorkEnvirRCARACTB_CR.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option title="times allocated to work order" value='times allocated to work order' <%= Model.SiteWorkEnvirRCARACTB_CR != null ?(Model.SiteWorkEnvirRCARACTB_CR.Equals("times allocated to work order")? "SELECTED" : String.Empty):String.Empty %>>times allocated to work order</option>
                                    <option title="times allocated to work order, with improvement plans and efficiency targets" value='times allocated to work order, with improvement plans and efficiency targets'<%= Model.SiteWorkEnvirRCARACTB_CR != null ?(Model.SiteWorkEnvirRCARACTB_CR.Equals("times allocated to work order, with improvement plans and efficiency targets")? "SELECTED" : String.Empty):String.Empty %>>times allocated to work order, with improvement plans and efficiency targets</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_252.9" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARWTSP_CR" name="SiteWorkEnvirRCARWTSP_CR" style="width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='none' <%= Model.SiteWorkEnvirRCARWTSP_CR != null ?(Model.SiteWorkEnvirRCARWTSP_CO.Equals("none")? "SELECTED" : String.Empty):String.Empty %>>none</option>
                                    <option  value='occasional' <%= Model.SiteWorkEnvirRCARWTSP_CR != null ?(Model.SiteWorkEnvirRCARWTSP_CR.Equals("occasional")? "SELECTED" : String.Empty):String.Empty %>>occasional</option>
                                    <option title="regularly undertaken, with improvement plans and targets" value='regularly undertaken, with improvement plans and targets'<%= Model.SiteWorkEnvirRCARWTSP_CR != null ?(Model.SiteWorkEnvirRCARWTSP_CR.Equals("regularly undertaken, with improvement plans and targets")? "SELECTED" : String.Empty):String.Empty %>>regularly undertaken, with improvement plans and targets</option>
                                </select>
                            </div>
                        </li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_253.1" /><br />
                            <div style="font-size: x-small; font-family: Verdana, Arial, Helvetica, sans-serif; color: #5a5655;">
                                <select id="SiteWorkEnvirRCARWPA_CR" name="SiteWorkEnvirRCARWPA_CR" style="max-width:100px;">
                                    <option value='-1'>SELECT ONE</option>
                                    <option value='not performed' <%= Model.SiteWorkEnvirRCARWPA_CR != null ?(Model.SiteWorkEnvirRCARWPA_CR.Equals("not performed")? "SELECTED" : String.Empty):String.Empty %>>not performed</option>
                                    <option  value='occasional' <%= Model.SiteWorkEnvirRCARWPA_CR != null ?(Model.SiteWorkEnvirRCARWPA_CR.Equals("occasional")? "SELECTED" : String.Empty):String.Empty %>>occasional</option>
                                    <option title="on a tracked  systematic basis with feedback given to crafts for improvement" value='on a tracked  systematic basis with feedback given to crafts for improvement'<%= Model.SiteWorkEnvirRCARWPA_CR != null ?(Model.SiteWorkEnvirRCARWPA_CR.Equals("on a tracked  systematic basis with feedback given to crafts for improvement")? "SELECTED" : String.Empty):String.Empty %>>on a tracked  systematic basis with feedback given to crafts for improvement</option>
                                </select>
                            </div>
                        </li>

                    </ul>
                </div>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRegsCert_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCFA_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCFA_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirReqDocFail_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirReqDocFail_CR)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCFA_CO_Rg)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCFA_CR_Rg)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCPQ_CO)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARMWPO_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARACTB_CO)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARWTSP_CO)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARWPA_CO)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCPQ_CR)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARMWPO_CR)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARACTB_CR)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARWTSP_CR)%>
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARWPA_CR)%> 





                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <p><i>Indicate which of the following activities is undertaken for contract work and how often it is performed.</i></p>



            <div class="questionblock" qid="">


                <div id='Div1' class='grid editor-field'>
                    <ul class="rowdesc" style="width:500px;">
                        <li>&nbsp;</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_250.6'><span class='numbering'>11.</span> Regular performance review meetings</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_250.8'><span class='numbering'>12.</span> Contractor performance audits</li>
                        <li class='gridrowtext' qid='QSTID_MT_CHAR_251.1'><span class='numbering'>13.</span> Contractor work Quality Assurance</li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Undertaken (Y/N)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_253.2" /><table align="center" border="0" cellspacing="4">
                                <tr>
                                    <td>Yes</td>
                                    <td>&nbsp;&nbsp;No</td>
                                </tr>
                                <tr>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRCARRPRM_CO, "Y") %></td>
                                    <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRCARRPRM_CO, "N") %></td>
                                </tr>
                            </table>
                            <li class="gridrow">
                                <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_253.3" /><table align="center" border="0" cellspacing="4">
                                    <tr>
                                        <td>Yes</td>
                                        <td>&nbsp;&nbsp;No</td>
                                    </tr>
                                    <tr>
                                        <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRCARCPA_CO, "Y") %></td>
                                        <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRCARCPA_CO, "N") %></td>
                                    </tr>
                                </table>
                                <li class="gridrow">
                                    <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_253.4" /><table align="center" border="0" cellspacing="4">
                                        <tr>
                                            <td>Yes</td>
                                            <td>&nbsp;&nbsp;No</td>
                                        </tr>
                                        <tr>
                                            <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRCARCWQA_CO, "Y") %></td>
                                            <td><%= Html.RadioButtonFor(model => model.SiteWorkEnvirRCARCWQA_CO, "N") %></td>
                                        </tr>
                                    </table>
                                </li>
                    </ul>
                    <ul class="gridbody">
                        <li class="columndesc">Frequency (months)</li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_253.5" /><%: Html.EditorFor(model => model.SiteWorkEnvirRCARRPRM_CR,"IntTmpl",new {size="10" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_253.6" /><%: Html.EditorFor(model => model.SiteWorkEnvirRCARCPA_CR,"IntTmpl",new {size="10" ,NumFormat="Int"})%></li>
                        <li class="gridrow">
                            <img src="<%= Url.Content("~/images/comment_icon.gif") %>" suid='<%= ViewContext.RouteData.Values["id"] %>' qid="QSTID_MT_CHAR_253.7" /><%: Html.EditorFor(model => model.SiteWorkEnvirRCARCWQA_CR,"IntTmpl",new {size="10" ,NumFormat="Int"})%></li>
                    </ul>
                </div>

                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCPA_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCPA_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCWQA_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARCWQA_CR)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARRPRM_CO)%> 
                <%: Html.ValidationMessageFor(model => model.SiteWorkEnvirRCARRPRM_CR)%> 

                <!--div class="tooltipIndicator downPointer"> Show Help </div>
                <fieldset class="tooltip">&nbsp;</fieldset -->
            </div>
            <%: Html.HiddenFor(model=>model.CompanySID) %>
        </div>

        <!-- for last step -->
    </div>
    <!-- for wizard container -->
    <%
       } //for form using clause 
    %>



    <script type="text/javascript">  function loadHelpReferences() { var objHelpRefs = Section_HelpLst; for (i = objHelpRefs.length - 1; i >= 0; i--) { var qidNm = objHelpRefs[i]['HelpID']; if ($('li[qid^="' + qidNm + '"]').length) { var $elm = $('li[qid^="' + qidNm + '"]'); $elm.append('<img src="~/images/help-icon-red.png" runat="server" class="tooltipBtn"/>'); $elm.find('img.tooltipBtn').css('visibility', 'visible'); } else if ($('div[qid^="' + qidNm + '"]').length) { $('div[qid^="' + qidNm + '"]').find('img.tooltipBtn').css('visibility', 'visible'); } } } var secComments = Section_Comments; var redCmtImg = '<%=Url.Content("~/images/comment_icon_red.png")%>'; for (i = secComments.length - 1; i >= 0; i--) { var qidNm = secComments[i]['CommentID']; if ($('img[qid="' + qidNm + '"]').length) { $('img[qid="' + qidNm + '"]').attr('src', redCmtImg); } }<%= Model.isUnderReview ? "$('input, textarea ,select').attr('disabled', 'disabled'); " : "" %>$('.rowdesc li div').prepend(function (index, html) { var gridId = $(this).closest('.grid').attr('id'); if ($.trim(html).indexOf('nbsp', 0) == -1) { var newHTML = '<span> ' + '<img src=" <%= Url.Content("~/images/comment_icon.gif")%>" suid="<%= ViewContext.RouteData.Values["id"].ToString() %>"' + ' qid= "' + gridId + '_' + index + '"  /></span> <span> ' + html + '</span>'; } $(this).html(newHTML); }); $('img[suid]').click(function () { var suid = $(this).attr('suid'); var qid = $(this).attr('qid'); recordComment(suid, qid); });</script>

</asp:Content>
