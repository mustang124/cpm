﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Register Src="~/Views/Shared/HomeMenuUserControl.ascx" TagName="HomeMenu" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


<p>&nbsp;</p>
<p class="headerIntro" >Supported Browsers </p>

<p >You can access the application via a PC, Mac, or Linux
computer.</p>

<p>We recommend using a supported
browser with application:</p>

<ul type=disc style="line-height:1.5">
 <li>Google
     Chrome (current stable version): download: <a
     href="http://www.google.com/chrome">Windows</a>
     <a href="http://www.google.com/chrome?platform=mac">Mac</a> <a
     href="http://www.google.com/chrome?platform=linux">Linux</a></li>
 <li>Firefox
     3.6+: download: <a href="http://www.mozilla.org/products/firefox/">Windows</a> <a
     href="http://www.mozilla.org/products/firefox/">Mac</a>
     <a href="http://www.mozilla.org/products/firefox/">Linux</a></li>
 <li>Internet
     Explorer 8.0+: download: <a
     href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie">Windows</a> </li>
 <li>Safari
     4.0+ download: <a href="http://www.apple.com/safari/download/">Mac</a> <a
     href="http://www.apple.com/safari/download/">Windows</a>
     </li>
 <li>Opera:
     We don't test Opera, but believe it works with this application: download:
     <a href="http://www.opera.com/browser">Windows</a>
     <a href="http://www.opera.com/browser">Mac</a>
     <a href="http://www.opera.com/browser">Linux</a>
     </li>
</ul>

<p >You must have cookies and JavaScript enabled to use with
this application</p>
<p ><strong>Note: </strong> If you are using Internet Explorer, the browser maybe in compatability mode. To disable this feature,  please do the following: 
<span style="display:block;width:85%; margin:5px 10px;font-style: italic; color: #000;">Internet Options> Advanced> Browsing> uncheck "Automatically recover from
page layout errors with Compatibility View."</span>
</p>
<p><span style="text-decoration:underline;">Browser</span>: A browser is an application you can use to access the Internet and visit
websites. Google Chrome, Internet Explorer, Firefox, and Safari are all
examples of browsers.</p>

<p><span style="text-decoration:underline;">Cookies</span>: Cookies are small files stored on your computer by your browser when you
visit a website, so the website can recall something about you the next time
you visit it. For example, a cookie might be used to remember your username or
pages on a website you’ve already looked at</p>



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Page.ResolveUrl("~/Content/menu_style.css")%>" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .style1
        {
            font-size: small;
        }
          p
        {
             font-size: .95em;
            font-family:Arial,Tahoma,"Lucida san", Tahoma;
            line-height:1.5;
            color:#333;
        }
        a {
        	text-decoration: underline;
        }
    </style>
    <!--[if lte IE 7 ]> 
        <script type="text/javascript">
            
            alert('Detected a web browser that is not compatible with this site. Recommend Internet Explorer 9 (compatible with 8 but have some minor issues) , Chrome 8.0 ,or FireFox 3.6+ with this website.');
         </script>
      <![endif]--> 
</asp:Content>




<asp:Content ID="Content9" ContentPlaceHolderID="MenuContent" runat="server">
    <uc:HomeMenu ID="homeMenu" runat="server" />
</asp:Content>

