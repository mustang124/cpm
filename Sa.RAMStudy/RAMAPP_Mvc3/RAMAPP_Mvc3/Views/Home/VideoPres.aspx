﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<%@ Import Namespace="SquishIt.Framework" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Video</title>
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/playertheme/style.css") %>"
        type="text/css" media="screen" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js" type="text/javascript"></script>
    <script src="<%= Page.ResolveUrl("~/Scripts") %>/video.config.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts") %>/projekktor.min.js"></script>
</head>
<body>
    <video id="player_a" class="projekktor" poster="<%= Page.ResolveUrl("~/images/solomon-logo.gif") %>"
          width="640" height="480" controls>  
        
    </video>
    <script type="text/javascript">
    var scriptPath = '<%= Page.ResolveUrl("~/Scripts") %>';
    var vidName = '<%: HttpContext.Current.Request["vid"] %>';
    if (vidName.length < 3)
    {
        $(window).load(function () { var b = $("#player_a"); var a = videoConfig.videos[vidName]; b.attr("title", a.title); projekktor("#player_a", { autoplay: true, debug: false, height: 550, width: 800, videoScaling: "fill", playerFlashMP4: scriptPath + "/jarisplayer.swf", playlist: [{ 1: { src: a.videoFlv, type: "video/x-flv" }, 0: { src: a.videoName, type: "video/mp4"}}] }) }).unload(function () { try { projekktor("#player_a").setStop(0); projekktor("#player_a").selfDestruct() } catch (a) { $("#player_a").remove() } });
    }
<%-- 
    $(window).load(function(){
        
        var $player = $('#player_a');
        var video = videoConfig.videos[vidName];
        $player.attr("title", video['title']);
        
        projekktor('#player_a', 
        {
            autoplay : true,
            debug : false,
            height : 550,
            width : 800,
            videoScaling : "fill",
            playerFlashMP4 : scriptPath + '/jarisplayer.swf',
            playlist : [{
                1 : 
                {
                    src : video['videoFlv'],
                    type : 'video/x-flv'
                },
                0 : 
                {
                    src : video['videoName'],
                    type : 'video/mp4'
                }
                
            }]
        });
        
    }).unload(function(){
        
        try{
            
            projekktor('#player_a').setStop(0);
            projekktor('#player_a').selfDestruct();
            
        }
        catch(e){
            $('#player_a').remove();
        }
        
    });--%>
     
    </script>
</body>
</html>
