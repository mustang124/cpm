﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/mediaelement-and-player.min.js") %>"></script>
     <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery.tools.min.js")  %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/toolbox.flashembed.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/scrollable.navigator.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/scrollable.autoscroll.min.js") %>"></script>
    <script src="<%= Page.ResolveUrl("~/Scripts/jquery-ui-1.8.15.custom.min.js")%>" type="text/javascript"></script>
    <%--<script type="text/javascript" src="<%= Page.ResolveUrl("") %>~/scripts/jquery-ui.min.js"></script>--%>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery.easing.1.3.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery.mousewheel.min.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/swfobject.js") %>"></script>
    <script type="text/javascript">
        swfobject.registerObject("csSWF", "9.0.115", "<%= Page.ResolveUrl("~/Content/expressInstall.swf") %>");
    </script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/video.config.js") %>"></script>
    <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/videoPlayer.js") %>"></script>
     <script type="text/javascript" src="<%= Page.ResolveUrl("~/Scripts/jquery.mCustomScrollbar.js") %>"></script>
    <div id="main">
        <div id="actions">
            <br>
            <p class="headerIntro">Study Webinars (<script type="text/javascript">document.write(videoConfig.videos.length); </script>)</p>
        </div>
        <div class="scrollable5 vertical" style="display: inline; float: left;">
            <div id="mcs4_container" style="border-top: 1px inset #EEE;">
                <div class="customScrollBox">
                    <div class="container">
                        <!-- div class="content"-->
                        <ul id="videoGallery" class="items content">
                        </ul>
                        <!-- /div -->
                    </div>
                    <div class="dragger_container">
                        <div class="dragger">
                        </div>
                    </div>
                </div>
                <!-- mcs4_container -->
            </div>
        </div>
        <!-- scrollable vertical -->
        <div id="videoPlayer">
            <video id="vMedia" type="video/mp4"  controls>
            <object width="540" height="304" type="application/x-shockwave-flash" data="/js/flashmediaelement.swf">
								<param name="movie" value="/js/flashmediaelement.swf" />
								<param name="flashvars" value="controls=true&amp;file=/media/echo-hereweare.mp4" />

								<img src="/media/echo-hereweare.jpg" width="540" height="303" alt="No video playback capabilities" />
							</object>
            </video>
            <!--div id="vMedia">
            </div-->
            <div id="vDesc">
            </div>
            <div id="vLink">
            </div>
        </div>
    </div>
    <!-- main -->




     <script type="text/javascript">

         $(document).ready(function () {

            
             // projekktor('#vMedia', {});
             loadVideoList();

             $("#mcs4_container").mCustomScrollbar("vertical", 200, "easeOutCirc", 1.05, "fixed", "yes", "yes", 10);
         });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <title>Webinars</title>
   
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="<%= Page.ResolveUrl("~/Content/app.mCustomScrollbar.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%= Page.ResolveUrl("~/Content/flashplayer.css")%>" rel="stylesheet" type="text/css" />
  
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-21195743-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    

   
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LoginContent" runat="server">
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="footer1" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="footer2" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="footer3" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="MenuContent" runat="server">
   
</asp:Content>
