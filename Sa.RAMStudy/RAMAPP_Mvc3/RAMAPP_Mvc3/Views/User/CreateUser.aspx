﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RAM_Admin.Master"
         Inherits="System.Web.Mvc.ViewPage<RAMAPP_Mvc3.Models.UserDetails>" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%@ Import Namespace="RAMAPP_Mvc3.Repositories.Cache" %>
<%@ Import Namespace="System.Globalization" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="<%= Url.Content("~/Content/dd_menu_style.css") %>" type="text/css" rel="stylesheet" />
    <style type="text/css">
    	#CultureFormat {
    		font-size: x-small;
    		font-style: italic;
    	}

    	select { border: #ccc solid 1px; }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% using (Html.BeginForm("CreateUser", "User", new {id = RouteData.Values["id"]}))
       { %>
        <%: Html.ValidationSummary(true) %>
        
             <div id="ReturnMsg" class="<%: TempData["UserCreateMsgClass"] ?? String.Empty%>">
                      <span class="ResponseMsg"><%:   TempData["UserCreateMsg"] ?? String.Empty%></span>
              </div>
        <fieldset style="width: 700px">
            <legend>Create User</legend>
            <div class="grid_4">
                Name:</div>
            <div class="grid_12">
                <span style="display: inline-block; margin-right: 20px;">
                    <%: Html.LabelFor(model => model.Firstname) %>
                    <%: Html.EditorFor(model => model.Firstname, "TextTmpl", new {size = "15"}) %> </span>
                
                <span style="display: inline-block;">
                    <%: Html.LabelFor(model => model.Lastname) %>
           
                    <%: Html.EditorFor(model => model.Lastname, "TextTmpl", new {size = "15"}) %></span>
               
           
            </div>
            <%: Html.ValidationMessageFor(model => model.Firstname) %>
            <%: Html.ValidationMessageFor(model => model.Lastname) %>
            <p>
                &nbsp;</p>
            <div class="grid_4">
                Email:</div>
            <div class="grid_12">
           
                <%: Html.LabelFor(model => model.EmailAddress, "ex. user@email.com") %>
                <%: Html.EditorFor(model => model.EmailAddress, "TextTmpl", new {size = "35"}) %>
                <%: Html.ValidationMessageFor(model => model.EmailAddress) %>
           
            </div>
            <br/><br/>
            <div class="grid_4">
                Password:</div>
            <div class="grid_12">
           
       
                <%: Html.LabelFor(model => model.Password) %>
                <%: Html.EditorFor(model => model.Password, "TextTmpl", new {size = "35", @type = "password"}) %>
                <%: Html.ValidationMessageFor(model => model.Password) %>
       
            </div>
            <br/><br/>
            <div class="grid_4">
                Telephone Number:</div>
            <div class="grid_12">
           
                <%: Html.LabelFor(model => model.Telephone, "ex. 922-299-8992") %>
                <%: Html.EditorFor(model => model.Telephone, "TextTmpl", new {size = "35"}) %>
                <%: Html.ValidationMessageFor(model => model.Telephone) %>
            
            </div>
            <br/><br/>
            <div class="grid_4">
                Title:</div>
            <div class="grid_12">
            
                <%: Html.LabelFor(model => model.JobTile, "ex. Coordinator") %>
                <%: Html.EditorFor(model => model.JobTile, "TextTmpl", new {size = "35"}) %>
                <%: Html.ValidationMessageFor(model => model.JobTile) %>
            
            </div>
            <p>&nbsp;</p>
            <div style="border-bottom: #CCC thin solid; margin: 10px; width: 100%;">
                &nbsp;</div>
            <div class="grid_16">
                <h5>
                    User Permissions</h5>
            </div>
            <br /><br/>
            <div class="grid_16">
                <% if (SessionUtil.IsAdmin())
                   {%>
                    <span class="editor-label">Is this user a data coordinator?</span> <span class="editor-field">
                                                                                           Yes
                                                                                           <%: Html.RadioButtonFor(model => model.Role, 2) %>
                                                                                           No
                                                                                           <%: Html.RadioButtonFor(model => model.Role, 0) %>
                                                                                           <%: Html.ValidationMessageFor(model => model.Role) %>
                                                                                       </span>
                <% } %>
                <br /><br/>
                <div id="ctnSitePermissions">
                    <table border="0" cellspacing="4" cellpadding="4" width="100%">
                        <thead>
                            <th>
                                Site Name
                            </th>
                            <th>
                                Permission Level
                            </th>
                        </thead>
                        <%
                            var cacheNav = new CachedNavigationRepository();
                            List<SiteUnits> avalSitesUnits = cacheNav.GetSiteUnitList();
                            var tblContent = new StringBuilder();

                            foreach (SiteUnits sites in avalSitesUnits)
                            {
                                //Exclude the sites where the user is not an admin
                                if (sites.Role == 0)
                                    continue;

                                tblContent.Append("<tr><td> " + sites.SiteName + "</td>");
                                tblContent.Append("<td><select name='sltPerm_" + sites.DatasetID +
                                                  "'><option value='-1'> Not Allowed </option><option value='0'> User</option> <option value='1'> Site Administrator </option></select></td></tr>");
                            }
                            Response.Write(tblContent.ToString());
%>
                    </table>
                </div>
            </div>
            <div style="border-bottom: #CCC thin solid; margin: 10px; width: 100%;">
                &nbsp;</div>
            <div class="grid_16">
                <h5>
                    Culture and Language</h5>
            </div>
            <br /><br/>
            <div class="grid_4">
                Language preference:<br />
                <font color="#990000" size="1px">(Optional) Default is U.S. English</font>
            </div>
            <div class="grid_12">
                <span class="editor-label">&nbsp; </span><span class="editor-field">
                                                             <%: Html.DropDownListFor(model => model.Culture,
                                             CultureInfo.GetCultures(CultureTypes.NeutralCultures)
                                                 .OrderBy(ci => ci.DisplayName)
                                                 .Select(
                                                     ci => new SelectListItem {Text = ci.EnglishName, Value = ci.Name})
                                                 .Distinct()
                                             , "Select One") %>
                                                         </span>
            </div>
            <p>
                &nbsp;</p>
            <div class="grid_5">
                Preview
            </div>
            <div id="CultureFormat" class="grid_5">
                <div style="width: 150px;">
                    Number format :<span id="NumberFormat"> 1,234,567.90</span><br />
                    Date format : <span id="DateFormat">
                                      <%: DateTime.Now.ToLongDateString() %></span>
                </div>
            </div>
            <p class="grid_16">
                <%: Html.HiddenFor(model => model.CompanyID) %>
                <input type="submit" value="Create User" />
            </p>
        </fieldset>
    <% } %>
   
    <script src="<%= Page.ResolveUrl("~/Scripts/Watermark.js") %>" type="text/javascript"> </script>
    <script src="<%= Page.ResolveUrl("~/Scripts/jquery.countdown.min.js") %>" type="text/javascript"> </script>
    <script type="text/javascript">

        $('#Culture').click(function() {
            var cultureSlt = $('#Culture').val();
            $.post('<%= Page.ResolveUrl("~/Admin/DisplayLocale") %>',
                { "locale": cultureSlt },
                function(response) {
                    try {
                        $('#NumberFormat').html(response.localeNumber);
                        $('#DateFormat').html(response.localeDate);
                    } catch(error) {
                        $('#NumberFormat').html("Unable to display format ");
                    }
                }, 'json');
        });


        $('input[id="Role"]').bind('change',
            function() {
                var val = $(this).val();

                if (val == 0)
                    $('#ctnSitePermissions').css('display', 'inline');
                else if (val == 2)
                    $('#ctnSitePermissions').css('display', 'none');
            }
        );
    </script>
</asp:Content>