using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Mail;

namespace RAM_MvcApp.Views.Shared
{
	/// <summary>
	/// Summary description for Approve.
	/// </summary>
    public class Approve : System.Web.UI.Page
    {
//        protected System.Web.UI.WebControls.CheckBoxList CheckBoxList1;
//        protected System.Data.SqlClient.SqlConnection sqlConnection1;
//        protected System.Web.UI.WebControls.Button btnApprove;
//        protected RAMStudy.RAMDataSet ramDataSet1;
//        protected System.Data.SqlClient.SqlCommand sqlSelectCommand2;
//        protected System.Data.SqlClient.SqlCommand sqlInsertCommand2;
//        protected System.Data.SqlClient.SqlCommand sqlUpdateCommand2;
//        protected System.Data.SqlClient.SqlCommand sqlDeleteCommand2;
//        protected System.Data.SqlClient.SqlDataAdapter sdSitePermissions;
//        protected System.Web.UI.WebControls.Literal Literal1;
//        protected System.Data.SqlClient.SqlCommand sqlSelectCommand1;
//        protected System.Data.SqlClient.SqlCommand sqlInsertCommand1;
//        protected System.Data.SqlClient.SqlDataAdapter sdSiteInfo;
//        protected System.Data.SqlClient.SqlCommand sqlSelectCommand3;
//        protected System.Data.SqlClient.SqlCommand sqlInsertCommand3;
//        protected System.Data.SqlClient.SqlCommand sqlUpdateCommand1;
//        protected System.Data.SqlClient.SqlCommand sqlDeleteCommand1;
//        protected System.Data.SqlClient.SqlDataAdapter sdReviewable;
	
//        private void Page_Load(object sender, System.EventArgs e)
//        {
//            // Put user code to initialize the page here

//            if (!IsPostBack)
//            {
//                int i=0 ;
//                string siteStmt = "SELECT SiteNbr,SiteName FROM SiteInfo WHERE CompanyID='"+HttpContext.Current.Session["CompanyID"]+"'";
//                string permStmt = "SELECT * FROM  SitePermisions WHERE companyid='"+HttpContext.Current.Session["CompanyID"]+"' AND userid='"+HttpContext.Current.Session["AppUser"]+ "'";
//                string erStmt = "SELECT * FROM Reviewable WHERE companyid='"+HttpContext.Current.Session["CompanyID"]+"'";
//                DBHelper dbh = new DBHelper();

//                DataSet dsSite = dbh.QueryDb(siteStmt,0,4,"");
//                DataSet dsPerms = dbh.QueryDb(permStmt,0,4,"");
//                DataSet dsReview = dbh.QueryDb(erStmt,0,4,"");
				
//                if (dsSite.Tables.Count  > 0 ) 
//                {
//                    for (i=0 ; i< dsSite.Tables[0].Rows.Count; i++)
//                    {
//                        string siteName = (string)dsSite.Tables[0].Rows[i]["SiteName"];
//                        string siteNbr = dsSite.Tables[0].Rows[i]["SiteNbr"].ToString();
//                        DataRow[] rows = dsReview.Tables[0].Select("SiteNbr=" + siteNbr);
//                        if (dsPerms.Tables[0].Rows.Count > 0)
//                        {
//                            DataRow[] permRows = dsPerms.Tables[0].Select("CompanyID='"+HttpContext.Current.Session["CompanyID"]+
//                                                                          "' and UserID='"+HttpContext.Current.Session["AppUser"]+
//                                                                          "' and SiteNbr='"+siteNbr+"'"); 
//                            if (permRows.Length ==0)
//                                continue;
//                        }
//                        ListItem item = new ListItem(siteName,siteNbr);
//                        CheckBoxList1.Items.Add(item);
						
//                        if ((dsReview.Tables[0].Rows.Count ==1) && i ==0 &&
//                            (dsReview.Tables[0].Rows[i]["SiteNbr"]==DBNull.Value)) 
//                        {
//                                item.Selected =true;
//                        }
//                        else if ((dsReview.Tables[0].Rows.Count >0) &&
//                                (rows.Length >0) )
//                        {	
							
//                                item.Selected =true;
//                        }
						

//                    }
//                }
//            }
//        }

//        #region Web Form Designer generated code
//        override protected void OnInit(EventArgs e)
//        {
//            //
//            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
//            //
//            InitializeComponent();
//            base.OnInit(e);
//        }
		
//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {    
//            this.sqlConnection1 = new System.Data.SqlClient.SqlConnection();
//            this.sdReviewable = new System.Data.SqlClient.SqlDataAdapter();
//            this.sqlInsertCommand1 = new System.Data.SqlClient.SqlCommand();
//            this.sqlSelectCommand1 = new System.Data.SqlClient.SqlCommand();
//            this.ramDataSet1 = new RAMStudy.RAMDataSet();
//            this.sqlSelectCommand2 = new System.Data.SqlClient.SqlCommand();
//            this.sqlInsertCommand2 = new System.Data.SqlClient.SqlCommand();
//            this.sqlUpdateCommand2 = new System.Data.SqlClient.SqlCommand();
//            this.sqlDeleteCommand2 = new System.Data.SqlClient.SqlCommand();
//            this.sdSitePermissions = new System.Data.SqlClient.SqlDataAdapter();
//            this.sdSiteInfo = new System.Data.SqlClient.SqlDataAdapter();
//            this.sqlDeleteCommand1 = new System.Data.SqlClient.SqlCommand();
//            this.sqlInsertCommand3 = new System.Data.SqlClient.SqlCommand();
//            this.sqlSelectCommand3 = new System.Data.SqlClient.SqlCommand();
//            this.sqlUpdateCommand1 = new System.Data.SqlClient.SqlCommand();
//            ((System.ComponentModel.ISupportInitialize)(this.ramDataSet1)).BeginInit();
//            this.btnApprove.Click += new System.EventHandler(this.Approve_Click);
//            // 
//            // sqlConnection1
//            // 
//            this.sqlConnection1.ConnectionString = "packet size=4096;user id=saroy;password=carter;data source=\"10.10.41.13\";persist " +
//                "security info=False;initial catalog=RAM";
//            // 
//            // sdReviewable
//            // 
//            this.sdReviewable.InsertCommand = this.sqlInsertCommand1;
//            this.sdReviewable.SelectCommand = this.sqlSelectCommand1;
//            this.sdReviewable.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
//                                                                                                   new System.Data.Common.DataTableMapping("Table", "Reviewable", new System.Data.Common.DataColumnMapping[] {
//                                                                                                                                                                                                                 new System.Data.Common.DataColumnMapping("CompanyID", "CompanyID"),
//                                                                                                                                                                                                                 new System.Data.Common.DataColumnMapping("SiteNbr", "SiteNbr"),
//                                                                                                                                                                                                                 new System.Data.Common.DataColumnMapping("Rank", "Rank")})});
//            // 
//            // sqlInsertCommand1
//            // 
//            this.sqlInsertCommand1.CommandText = "INSERT INTO dbo.Reviewable(CompanyID, SiteNbr, Rank) VALUES (@CompanyID, @SiteNbr" +
//                ", @Rank); SELECT CompanyID, SiteNbr, Rank FROM dbo.Reviewable rw";
//            this.sqlInsertCommand1.Connection = this.sqlConnection1;
//            this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.NVarChar, 9, "CompanyID"));
//            this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SiteNbr", System.Data.SqlDbType.Int, 4, "SiteNbr"));
//            this.sqlInsertCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Rank", System.Data.SqlDbType.NVarChar, 2, "Rank"));
//            // 
//            // sqlSelectCommand1
//            // 
//            this.sqlSelectCommand1.CommandText = "SELECT CompanyID, SiteNbr, Rank FROM dbo.Reviewable rw";
//            this.sqlSelectCommand1.Connection = this.sqlConnection1;
//            // 
//            // ramDataSet1
//            // 
//            this.ramDataSet1.DataSetName = "RAMDataSet";
//            this.ramDataSet1.Locale = new System.Globalization.CultureInfo("en-US");
//            // 
//            // sqlSelectCommand2
//            // 
//            this.sqlSelectCommand2.CommandText = "SELECT CompanyID, UserID, SiteNbr FROM dbo.SitePermisions";
//            this.sqlSelectCommand2.Connection = this.sqlConnection1;
//            // 
//            // sqlInsertCommand2
//            // 
//            this.sqlInsertCommand2.CommandText = "INSERT INTO dbo.SitePermisions(CompanyID, UserID, SiteNbr) VALUES (@CompanyID, @U" +
//                "serID, @SiteNbr); SELECT CompanyID, UserID, SiteNbr FROM dbo.SitePermisions WHER" +
//                "E (CompanyID = @CompanyID) AND (SiteNbr = @SiteNbr) AND (UserID = @UserID)";
//            this.sqlInsertCommand2.Connection = this.sqlConnection1;
//            this.sqlInsertCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.NVarChar, 9, "CompanyID"));
//            this.sqlInsertCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UserID", System.Data.SqlDbType.NVarChar, 9, "UserID"));
//            this.sqlInsertCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SiteNbr", System.Data.SqlDbType.Int, 4, "SiteNbr"));
//            // 
//            // sqlUpdateCommand2
//            // 
//            this.sqlUpdateCommand2.CommandText = @"UPDATE dbo.SitePermisions SET CompanyID = @CompanyID, UserID = @UserID, SiteNbr = @SiteNbr WHERE (CompanyID = @Original_CompanyID) AND (SiteNbr = @Original_SiteNbr) AND (UserID = @Original_UserID); SELECT CompanyID, UserID, SiteNbr FROM dbo.SitePermisions WHERE (CompanyID = @CompanyID) AND (SiteNbr = @SiteNbr) AND (UserID = @UserID)";
//            this.sqlUpdateCommand2.Connection = this.sqlConnection1;
//            this.sqlUpdateCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.NVarChar, 9, "CompanyID"));
//            this.sqlUpdateCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@UserID", System.Data.SqlDbType.NVarChar, 9, "UserID"));
//            this.sqlUpdateCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SiteNbr", System.Data.SqlDbType.Int, 4, "SiteNbr"));
//            this.sqlUpdateCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CompanyID", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_SiteNbr", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "SiteNbr", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_UserID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "UserID", System.Data.DataRowVersion.Original, null));
//            // 
//            // sqlDeleteCommand2
//            // 
//            this.sqlDeleteCommand2.CommandText = "DELETE FROM dbo.SitePermisions WHERE (CompanyID = @Original_CompanyID) AND (SiteN" +
//                "br = @Original_SiteNbr) AND (UserID = @Original_UserID)";
//            this.sqlDeleteCommand2.Connection = this.sqlConnection1;
//            this.sqlDeleteCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CompanyID", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_SiteNbr", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "SiteNbr", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand2.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_UserID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "UserID", System.Data.DataRowVersion.Original, null));
//            // 
//            // sdSitePermissions
//            // 
//            this.sdSitePermissions.DeleteCommand = this.sqlDeleteCommand2;
//            this.sdSitePermissions.InsertCommand = this.sqlInsertCommand2;
//            this.sdSitePermissions.SelectCommand = this.sqlSelectCommand2;
//            this.sdSitePermissions.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
//                                                                                                        new System.Data.Common.DataTableMapping("Table", "SitePermisions", new System.Data.Common.DataColumnMapping[] {
//                                                                                                                                                                                                                          new System.Data.Common.DataColumnMapping("CompanyID", "CompanyID"),
//                                                                                                                                                                                                                          new System.Data.Common.DataColumnMapping("UserID", "UserID"),
//                                                                                                                                                                                                                          new System.Data.Common.DataColumnMapping("SiteNbr", "SiteNbr")})});
//            this.sdSitePermissions.UpdateCommand = this.sqlUpdateCommand2;
//            // 
//            // sdSiteInfo
//            // 
//            this.sdSiteInfo.DeleteCommand = this.sqlDeleteCommand1;
//            this.sdSiteInfo.InsertCommand = this.sqlInsertCommand3;
//            this.sdSiteInfo.SelectCommand = this.sqlSelectCommand3;
//            this.sdSiteInfo.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
//                                                                                                 new System.Data.Common.DataTableMapping("Table", "SiteInfo", new System.Data.Common.DataColumnMapping[] {
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("CompanyID", "CompanyID"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("SiteNbr", "SiteNbr"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("CoName", "CoName"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("Country", "Country"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("SiteName", "SiteName"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("Area", "Area"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("Refnum", "Refnum"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("CurrencyType", "CurrencyType"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("Nationalized", "Nationalized"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("ExtraQs", "ExtraQs"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("DataCompleted", "DataCompleted"),
//                                                                                                                                                                                                             new System.Data.Common.DataColumnMapping("StudyYear", "StudyYear")})});
//            this.sdSiteInfo.UpdateCommand = this.sqlUpdateCommand1;
//            // 
//            // sqlDeleteCommand1
//            // 
//            this.sqlDeleteCommand1.CommandText = @"DELETE FROM dbo.SiteInfo WHERE (CompanyID = @Original_CompanyID) AND (SiteNbr = @Original_SiteNbr) AND (Area = @Original_Area OR @Original_Area IS NULL AND Area IS NULL) AND (CoName = @Original_CoName OR @Original_CoName IS NULL AND CoName IS NULL) AND (Country = @Original_Country OR @Original_Country IS NULL AND Country IS NULL) AND (CurrencyType = @Original_CurrencyType OR @Original_CurrencyType IS NULL AND CurrencyType IS NULL) AND (DataCompleted = @Original_DataCompleted OR @Original_DataCompleted IS NULL AND DataCompleted IS NULL) AND (ExtraQs = @Original_ExtraQs OR @Original_ExtraQs IS NULL AND ExtraQs IS NULL) AND (Nationalized = @Original_Nationalized OR @Original_Nationalized IS NULL AND Nationalized IS NULL) AND (Refnum = @Original_Refnum OR @Original_Refnum IS NULL AND Refnum IS NULL) AND (SiteName = @Original_SiteName OR @Original_SiteName IS NULL AND SiteName IS NULL) AND (StudyYear = @Original_StudyYear OR @Original_StudyYear IS NULL AND StudyYear IS NULL)";
//            this.sqlDeleteCommand1.Connection = this.sqlConnection1;
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CompanyID", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_SiteNbr", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "SiteNbr", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Area", System.Data.SqlDbType.NVarChar, 10, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Area", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CoName", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CoName", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Country", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Country", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CurrencyType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CurrencyType", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_DataCompleted", System.Data.SqlDbType.VarBinary, 1, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "DataCompleted", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_ExtraQs", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "ExtraQs", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Nationalized", System.Data.SqlDbType.NVarChar, 1, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Nationalized", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Refnum", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Refnum", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_SiteName", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "SiteName", System.Data.DataRowVersion.Original, null));
//            this.sqlDeleteCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_StudyYear", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "StudyYear", System.Data.DataRowVersion.Original, null));
//            // 
//            // sqlInsertCommand3
//            // 
//            this.sqlInsertCommand3.CommandText = @"INSERT INTO dbo.SiteInfo(CompanyID, SiteNbr, CoName, Country, SiteName, Area, Refnum, CurrencyType, Nationalized, ExtraQs, DataCompleted, StudyYear) VALUES (@CompanyID, @SiteNbr, @CoName, @Country, @SiteName, @Area, @Refnum, @CurrencyType, @Nationalized, @ExtraQs, @DataCompleted, @StudyYear); SELECT CompanyID, SiteNbr, CoName, Country, SiteName, Area, Refnum, CurrencyType, Nationalized, ExtraQs, DataCompleted, StudyYear FROM dbo.SiteInfo WHERE (CompanyID = @CompanyID) AND (SiteNbr = @SiteNbr)";
//            this.sqlInsertCommand3.Connection = this.sqlConnection1;
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.NVarChar, 9, "CompanyID"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SiteNbr", System.Data.SqlDbType.Int, 4, "SiteNbr"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CoName", System.Data.SqlDbType.NVarChar, 50, "CoName"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Country", System.Data.SqlDbType.NVarChar, 50, "Country"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SiteName", System.Data.SqlDbType.NVarChar, 50, "SiteName"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Area", System.Data.SqlDbType.NVarChar, 10, "Area"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Refnum", System.Data.SqlDbType.NVarChar, 9, "Refnum"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CurrencyType", System.Data.SqlDbType.Int, 4, "CurrencyType"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Nationalized", System.Data.SqlDbType.NVarChar, 1, "Nationalized"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtraQs", System.Data.SqlDbType.NVarChar, 50, "ExtraQs"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@DataCompleted", System.Data.SqlDbType.VarBinary, 1, "DataCompleted"));
//            this.sqlInsertCommand3.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StudyYear", System.Data.SqlDbType.SmallInt, 2, "StudyYear"));
//            // 
//            // sqlSelectCommand3
//            // 
//            this.sqlSelectCommand3.CommandText = "SELECT CompanyID, SiteNbr, CoName, Country, SiteName, Area, Refnum, CurrencyType," +
//                " Nationalized, ExtraQs, DataCompleted, StudyYear FROM dbo.SiteInfo";
//            this.sqlSelectCommand3.Connection = this.sqlConnection1;
//            // 
//            // sqlUpdateCommand1
//            // 
//            this.sqlUpdateCommand1.CommandText = @"UPDATE dbo.SiteInfo SET CompanyID = @CompanyID, SiteNbr = @SiteNbr, CoName = @CoName, Country = @Country, SiteName = @SiteName, Area = @Area, Refnum = @Refnum, CurrencyType = @CurrencyType, Nationalized = @Nationalized, ExtraQs = @ExtraQs, DataCompleted = @DataCompleted, StudyYear = @StudyYear WHERE (CompanyID = @Original_CompanyID) AND (SiteNbr = @Original_SiteNbr) AND (Area = @Original_Area OR @Original_Area IS NULL AND Area IS NULL) AND (CoName = @Original_CoName OR @Original_CoName IS NULL AND CoName IS NULL) AND (Country = @Original_Country OR @Original_Country IS NULL AND Country IS NULL) AND (CurrencyType = @Original_CurrencyType OR @Original_CurrencyType IS NULL AND CurrencyType IS NULL) AND (DataCompleted = @Original_DataCompleted OR @Original_DataCompleted IS NULL AND DataCompleted IS NULL) AND (ExtraQs = @Original_ExtraQs OR @Original_ExtraQs IS NULL AND ExtraQs IS NULL) AND (Nationalized = @Original_Nationalized OR @Original_Nationalized IS NULL AND Nationalized IS NULL) AND (Refnum = @Original_Refnum OR @Original_Refnum IS NULL AND Refnum IS NULL) AND (SiteName = @Original_SiteName OR @Original_SiteName IS NULL AND SiteName IS NULL) AND (StudyYear = @Original_StudyYear OR @Original_StudyYear IS NULL AND StudyYear IS NULL); SELECT CompanyID, SiteNbr, CoName, Country, SiteName, Area, Refnum, CurrencyType, Nationalized, ExtraQs, DataCompleted, StudyYear FROM dbo.SiteInfo WHERE (CompanyID = @CompanyID) AND (SiteNbr = @SiteNbr)";
//            this.sqlUpdateCommand1.Connection = this.sqlConnection1;
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CompanyID", System.Data.SqlDbType.NVarChar, 9, "CompanyID"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SiteNbr", System.Data.SqlDbType.Int, 4, "SiteNbr"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CoName", System.Data.SqlDbType.NVarChar, 50, "CoName"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Country", System.Data.SqlDbType.NVarChar, 50, "Country"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SiteName", System.Data.SqlDbType.NVarChar, 50, "SiteName"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Area", System.Data.SqlDbType.NVarChar, 10, "Area"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Refnum", System.Data.SqlDbType.NVarChar, 9, "Refnum"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@CurrencyType", System.Data.SqlDbType.Int, 4, "CurrencyType"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Nationalized", System.Data.SqlDbType.NVarChar, 1, "Nationalized"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@ExtraQs", System.Data.SqlDbType.NVarChar, 50, "ExtraQs"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@DataCompleted", System.Data.SqlDbType.VarBinary, 1, "DataCompleted"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@StudyYear", System.Data.SqlDbType.SmallInt, 2, "StudyYear"));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CompanyID", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CompanyID", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_SiteNbr", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "SiteNbr", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Area", System.Data.SqlDbType.NVarChar, 10, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Area", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CoName", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CoName", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Country", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Country", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_CurrencyType", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "CurrencyType", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_DataCompleted", System.Data.SqlDbType.VarBinary, 1, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "DataCompleted", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_ExtraQs", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "ExtraQs", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Nationalized", System.Data.SqlDbType.NVarChar, 1, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Nationalized", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_Refnum", System.Data.SqlDbType.NVarChar, 9, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "Refnum", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_SiteName", System.Data.SqlDbType.NVarChar, 50, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "SiteName", System.Data.DataRowVersion.Original, null));
//            this.sqlUpdateCommand1.Parameters.Add(new System.Data.SqlClient.SqlParameter("@Original_StudyYear", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, false, ((System.Byte)(0)), ((System.Byte)(0)), "StudyYear", System.Data.DataRowVersion.Original, null));
//            this.Load += new System.EventHandler(this.Page_Load);
//            ((System.ComponentModel.ISupportInitialize)(this.ramDataSet1)).EndInit();

//        }
//        #endregion

//        private void Approve_Click(object sender, System.EventArgs e)
//        {
//            sdReviewable.Fill(ramDataSet1);
//            sdSitePermissions.Fill(ramDataSet1);
			
//            //DataRow[] rows = ramDataSet1.SitePermisions.Select("CompanyID='"+HttpContext.Current.Session["CompanyID"]+"' and UserID='"+HttpContext.Current.Session["AppUser"]+"'");
//            string level;
//            DataRow reviewableRow;
//            DataRow[] reviewableRows;
//            //Check its a site manager 
//            //if (rows.Length >0)
//            //{
//                level ="Site";
//                for(int y =0 ; y < CheckBoxList1.Items.Count ; y++)
//                {
					
//                    if (CheckBoxList1.Items[y].Selected )
//                    {
//                        reviewableRows = 
//                            ramDataSet1.Reviewable.Select("CompanyID='" + HttpContext.Current.Session["CompanyID"].ToString() + 
//                            "' and SiteNbr=" + Convert.ToInt32 (CheckBoxList1.Items[y].Value ) + 
//                            " and Rank='"+ level + "'");
						
//                        bool isNew =(reviewableRows.Length ==0);
//                        if (isNew)
//                        {
//                            reviewableRow = ramDataSet1.Reviewable.NewReviewableRow(); 
	  					
//                            reviewableRow["CompanyID"]=HttpContext.Current.Session["CompanyID"].ToString();
//                            reviewableRow["SiteNbr"]=Convert.ToInt32(CheckBoxList1.Items[y].Value);
//                            reviewableRow["Rank"] = level;
						
//                           ramDataSet1.Reviewable.Rows.Add (reviewableRow);
//                           MailApprovalNotice(HttpContext.Current.Session["CompanyID"].ToString(),Convert.ToInt32(CheckBoxList1.Items[y].Value));
//                        }
//                        sdReviewable.Update(ramDataSet1);
//                    }
//                }
////	        }
////			else
////			{
////				level="Company";
////				DataRow[] rows1 = ramDataSet1.Reviewable.Select("CompanyID='"+ HttpContext.Current.Session["CompanyID"] +"'");
////				for(int p=0; p < rows1.Length; p++)
////					rows1[p].Delete();
////
////				RAMStudy.RAMDataSet.ReviewableRow reviewableRow 
////					= ramDataSet1.Reviewable.NewReviewableRow(); 
////				
////				reviewableRow.CompanyID=HttpContext.Current.Session["CompanyID"].ToString();
////				reviewableRow.Rank = level;
////
////				ramDataSet1.Reviewable.AddReviewableRow(reviewableRow);
////				sdReviewable.Update(ramDataSet1);
////				MailApprovalNotice(HttpContext.Current.Session["CompanyID"].ToString(),-1);
////			}


//            //Close window
//            Literal1.Text="<script> window.close(); </script>";

			
//        }
		

//        private void MailApprovalNotice(string companyid , int sitenbr)
//        {	
//            string bodytext;
//            DataRow[] rows ;
//            sdSiteInfo.Fill(ramDataSet1);
//            if (sitenbr > -1)
//                rows = ramDataSet1.SiteInfo.Select("CompanyID='"+HttpContext.Current.Session["CompanyID"]+"'");
//            else
//                rows = ramDataSet1.SiteInfo.Select("CompanyID='"+HttpContext.Current.Session["CompanyID"]+"' and SiteNbr='"+ sitenbr +"'");
			
//            if (rows.Length > 0)
//            {
//                if  (sitenbr == -1)
//                {
//                    bodytext =  rows[0]["CoName"] +","+ HttpContext.Current.Session["CompanyID"] + 
//                        ", has been approved by "  + HttpContext.Current.Session["AppUser"] ;
//                }
//                else
//                    bodytext= bodytext =  "The site named " + rows[0]["SiteName"] + " from the company " + rows[0]["CoName"]   +","+ HttpContext.Current.Session["CompanyID"] + 
//                        ", has been approved by " + HttpContext.Current.Session["AppUser"] ;
 
			
					
//                Smtp.SendMailMessage("RAM@solomononline.com",
//                    "Saroy@solomononline.com;AJP@solomononline.com",
//                    "",
//                    "",
//                    "RAM Approval Notice from " + rows[0]["CoName"]  ,
//                    bodytext );
//            }
//        }

    }
}
