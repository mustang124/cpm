﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<int?>" %>
<% this.ViewData.Add("style","text-align:right"); %>
<%: Html.TextBox("", String.Format(System.Threading.Thread.CurrentThread.CurrentCulture, "{0:N0}", Model), this.ViewData.ToDictionary(c => c.Key, c => c.Value))%> 
<%--<%: Html.TextBox("", String.Format(System.Globalization.CultureInfo.CurrentCulture,"{0:d}", Model),  this.ViewData.ToDictionary(c=>c.Key,c=>c.Value))%> --%>
