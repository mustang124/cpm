<%@ Page language="c#" Codebehind="Approve.aspx.cs" AutoEventWireup="false" Inherits="RAMStudy.Approve" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Approve</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body onblur="focus();" MS_POSITIONING="GridLayout" bgColor="#ffffff">
		<form id="Form1" method="post" runat="server">
			<TABLE cellSpacing="1" cellPadding="1" width="100%" align="left" border="0" height="100%"
				background="#FFFFFF">
				<TR>
					<TD>
						<div align="left" style="MARGIN: 25px">
							<P>
								<FONT size="2" face="Tahoma"><b>Directions:</b> Check the site(s) that are ready 
									for review&nbsp;and click the <b>Approve</b> </FONT>
							</P>
							<fieldset title="Check site and click APPROVE">
								<asp:checkboxlist id="CheckBoxList1" runat="server" Font-Names="Tahoma" Font-Size="XX-Small" RepeatColumns="4"></asp:checkboxlist>
							</fieldset>
							<br>
							<br>
							<FONT size="2" face="Tahoma">Following approval of your site data submission, your 
								input form will be disabled and&nbsp;a Solomon RAM project coordinator will 
								begin analyzing your data. If there are any concerns or questions, we will be 
								in contact.</FONT>
						</div>
						<P></P>
					</TD>
				</TR>
				<TR>
					<TD align="center" vAlign="bottom" bgColor="#ffffff">
						<asp:Literal id="Literal1" runat="server"></asp:Literal><BR>
						<BR>
						<asp:button id="btnApprove" runat="server" Height="24px" Width="64px" BorderStyle="None" BackColor="White"></asp:button></TD>
				</TR>
			</TABLE>
		</form>
		<script language="javascript">	
		/*******************************************************************
		This is the javascript function that is invoked when the checkboxlist
		is clicked.

		Function:   disableListItems.
		Inputs:     checkBoxListId - The id of the checkbox list.

					checkBoxIndex - The index of the checkbox to verify.
					i.e If the 4th checkbox is clicked and 
					you want the other checkboxes to be
					disabled the index would be 3.
		            
					numOfItems - The number of checkboxes in the list.
		Purpose:  Disables all the checkboxes when the checkbox to verify is
					checked.  The checkbox to verify is never disabled.
		********************************************************************/
		function disableListItems(checkBoxListId, numOfItems)
		{
			// Get the checkboxlist object.
			objCtrl = document.getElementById(checkBoxListId);
		    
			// Does the checkboxlist not exist?
			if(objCtrl == null)
			{
				return;
			}

			var i = 0;
			var objItem = null;
			
			// Loop through the checkboxes in the list.
			for(i = 0; i < numOfItems; i++)
			{
				objItem = document.getElementById(checkBoxListId + '_' + i);

				if(objItem == null)
				{
					continue;
				}
				
				var isChecked = objItem.checked;
				objItem.disabled = isChecked;
			}
		}
		
		disableListItems('<%=CheckBoxList1.ClientID %>', '<%=CheckBoxList1.Items.Count %>');
		</script>
	</body>
</HTML>
