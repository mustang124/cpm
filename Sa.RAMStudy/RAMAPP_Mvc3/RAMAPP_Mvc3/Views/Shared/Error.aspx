﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/RAM.Master" Inherits="System.Web.Mvc.ViewPage<System.Web.Mvc.HandleErrorInfo>" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
              .style1
              {
                  text-align: right;
              }
              .style2
              {
                  font-family: "Century Gothic";
                  color: #991000;
                  font-weight: 400;
                  font-size: 14pt;
              }
              #content
              {
                  /* background: url('<%: Page.ResolveUrl("~/images/Background-fade.jpg") %>') left no-repeat;*/
                  background: url('<%: Page.ResolveUrl("~/images/Background-rht.jpg") %>') right no-repeat;
                  position: relative;
                  top: 6px;
              }
          </style>
</asp:Content>
<asp:Content ID="errorContent" ContentPlaceHolderID="MainContent" runat="server">
    <div style="position: absolute; margin: auto auto; padding: 10px 10px; display: block;
        width: 500px; height: 400px;">
        <div style="margin: auto auto;">
            <h5>
                Sorry an error just happened.</h5>
            <span class="style2">
                <br />
                <br />
                Please use the browser's back button and try again.</span>
        </div>
    </div>
</asp:Content>
