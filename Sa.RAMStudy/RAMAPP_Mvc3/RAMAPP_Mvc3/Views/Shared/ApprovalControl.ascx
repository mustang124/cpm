﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%
    //Brian F: Changes made to get percent complete and new field percent complete required (to submit data).
   
    const string template = "<input name='cbApproval' type='checkbox' value='{1}' />&nbsp;{0}<br />{2}% Complete/{3}% Required<br />Data Quality: {4}/{5} Required</br></br>";
    const string dtemplate = "<input name='cbApproval' type='checkbox' value='{1}' disabled checked />&nbsp;<span style='color:#999;'>{0}<br />{2}% Complete/{3}% Required<br />Data Quality: {4}/{5} Required</span></br></br>";
    const string ictemplate = "<input name='cbApproval' type='checkbox' value='{1}' disabled />&nbsp;<span style='color:#999;'>{0}<br />{2}% Complete/{3}% Required<br />Data Quality: {4}/{5} Required</span></br></br>";
    var bld = new StringBuilder();
    var idx = 1;
    var userinfo = ((RAMAPP_Mvc3.Entity.LogIn)Session["UserInfo"]);
    var companySID = SessionUtil.GetCompanySID();
     
    using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
    {

        var sites = (from s in db.SiteInfo
                     where s.CompanySID == companySID &&
                     (userinfo.SecurityLevel == 2 || db.SitePermissions.Any(p => p.UserID == userinfo.UserID &&
                                                                                 p.DatasetID == s.DatasetID &&
                                                                                 p.SecurityLevel == 1))
                     join d in db.DatasetQualitySiteSummary on s.DatasetID equals d.DatasetID
                     select
                     new
                     {
                         PcntComplete = d.PcntComplete ?? 0,
                         s.SiteName,
                         s.DatasetID,
                         PcntCompleteReq = s.PcntCompleteReq ?? 0,
                         Grade = d.Grade ?? 0,
                         GradeReq = s.GradeReq ?? 0
                     }).ToList();


        sites.ForEach(l =>
           {

               //var grade = (int?)(db.PropertyGrades.Select(k => k.DatasetID).Contains(l.DatasetID)
               //                         ? db.PropertyGrades.Where(k => k.DatasetID == l.DatasetID).Select(
               //                             s => s.Grade).
               //                               Average()
               //                         : null);
               
               
               
               if ((idx % 4) == 1)
                   bld.Append("<div class='grid_6' style='font-size:.95em;'>");
               
               if (l.PcntComplete < l.PcntCompleteReq || l.Grade < l.GradeReq)
                   bld.Append(String.Format(ictemplate, l.SiteName, l.DatasetID, Math.Round(l.PcntComplete), Math.Round(l.PcntCompleteReq), l.Grade, l.GradeReq));
               else if (!db.Reviewable.Any(r => r.CompanySID == companySID && (((r.DataLevel.Trim() == "Co") || (r.DataLevel.Trim() == "Si" && r.SiteDatsetID == l.DatasetID)))))
                   bld.Append(String.Format(template, l.SiteName, l.DatasetID, Math.Round(l.PcntComplete), Math.Round(l.PcntCompleteReq), l.Grade, l.GradeReq));
               else
                   bld.Append(String.Format(dtemplate, l.SiteName, l.DatasetID, Math.Round(l.PcntComplete), Math.Round(l.PcntCompleteReq), l.Grade, l.GradeReq));

               if ((idx % 4) == 0)
                   bld.Append("</div>");
               idx++;
           });

        if ((idx-- % 4) == 1)
            bld.Append("</div>");
                        
                

      
%>
<%: Html.Raw( bld.ToString())%>
<%
            
        } %>