﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%
    var isAdmin = SessionUtil.IsAdmin(); 
    var isSiteAdmin = SessionUtil.IsSiteAdmin();
%>
<div class="dd_menu">
    <ul>
        <li><a href="<%=Url.RouteUrl(new { action="Index",controller="App" })%>">
          Home</a></li>
          <li><a href="<%=Url.RouteUrl(new { action="Index",controller="Admin" })%>">
          Administration</a></li>
        <li><a href="#">
            Tools </a>
            <ul>
                <% if (isAdmin)
                   {  %>
                <li>
                    <%: Html.ActionLink("Create Site", "CreateSite","Site") %></li>
                <% } %>
                <% if (isAdmin || isSiteAdmin)
                   {%>
                <li>
                    <%: Html.ActionLink("Create User", "CreateUser", "User", new { id = SessionUtil.GetCompanySID() },null)%></li>
                <% } %>
            </ul>
        </li>
        <li> <%-- <a href="#">
          <img border="0" src="~/images/list-accept.png" runat="server" />
           &nbsp; Reports </a>--%>
          <%--  <ul>
                <li><a href=" <%=  Url.Content("~/Reports/InputReport.aspx") %>">Data Report</a></li>
            </ul>--%>
        </li>
        <li> <a href="<%=Url.RouteUrl(new {action="ApproveData",controller="Admin"})%>">
          
              Submit for Review</a>   </li>
      
            
         
    </ul>
</div>
