﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>


<div class="container_16">
<div id="SessionAlert" style="background-color: #FBFBE8;border: 1px  solid; width:100%;margin:0 0 0 0; padding: 0 0 0 0;" >
   
            <div class="grid_4">&nbsp;</div>
            <div id="sessionmsg" class="grid_12"><img src="http://cdn1.iconfinder.com/data/icons/sketchdock-ecommerce-icons/alert-triangle-red.png" border="0" alt="alert" align="middle" />
            Session is will timeout soon.  <a href="javascript:window.location.reload(true);" style="color:Blue">Click here</a> to reset your session. Time remaining <span id="timeremaining" ></span>  </div>
    
 </div>
</div>

 <script src="../../../../Scripts/jquery-1.5.1.min.js" type="text/javascript"></script>
 <script src="../../../../Scripts/jquery.countdown.min.js" type="text/javascript"></script>
 <script type="text/javascript">
        function sessionClosed()
        {
      
           $("#sessionmsg").html('<img src="http://cdn1.iconfinder.com/data/icons/sketchdock-ecommerce-icons/alert-triangle-red.png" border="0" alt="alert" align="middle" /> Your session has expired. Please logout and re-login to continue.');
        }

        $(document).ready(function () {

            timeout = <%: (Session.Timeout-2)*60*1000 %>;
            $("#SessionAlert").fadeOut(10).delay(1000).fadeIn(2000,
                                function () {
                                    var shortly = new Date();
                                    shortly.setSeconds(shortly.getSeconds() + 117);
                                    $('#timeremaining').countdown({ until: shortly, format: 'MS',compact:true , onExpiry: sessionClosed});
                                    
                                }); 
        });

       
    </script>