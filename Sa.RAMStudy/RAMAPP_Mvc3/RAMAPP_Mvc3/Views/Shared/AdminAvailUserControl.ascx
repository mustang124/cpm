﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%@ Import Namespace="RAMAPP_Mvc3.Entity" %>
<%
    string usertemplate = "<li style='border-bottom: 1px #ccc solid;'><a href=\"" + Url.Content("~/User/UserDetails/") + "{0}\">{1}</a></li> ";
    var usermenu = new StringBuilder();

    string companyId = "";
    var userinfo = (RAMAPP_Mvc3.Entity.LogIn)HttpContext.Current.Session["UserInfo"];
    var companySID = SessionUtil.GetCompanySID();
    var isAdmin = SessionUtil.IsAdmin();
    var isSiteAdmin = SessionUtil.IsSiteAdmin();
    var cp = SessionUtil.AdminKey();
    var mode = Session["Mode"];
    var IsSAISession = mode != null ? (mode.ToString() == cp) : false;

    using (var db = new RAMAPP_Mvc3.Entity.RAMEntities())
    {
        companyId = (IsSAISession ? db.Company_LU.FirstOrDefault(c => c.CompanySID == companySID).CompanyID : userinfo.CompanyID).Trim();
        string userid = IsSAISession ? (db.LogIn.Any(l => l.CompanyID == companyId && l.SecurityLevel == 2 && l.Active == true) ? db.LogIn.FirstOrDefault(l => l.CompanyID == companyId && l.SecurityLevel == 2 && l.Active == true).UserID : String.Empty) : userinfo.UserID;

        if ((isAdmin || isSiteAdmin) && userid != String.Empty)
        {
            if (userinfo != null)
            {

                //Return all users that are in company if administrator 
                //but if not return all users that present user is permitted to see
                var usrperm = (from sp in db.SitePermissions
                               join si in db.SiteInfo on sp.DatasetID equals si.DatasetID
                               where userid == sp.UserID && sp.SecurityLevel == 1 && si.CompanySID == companySID
                               select sp.DatasetID).Distinct();

                var users = (from usr in db.LogIn
                             join sp in db.SitePermissions on usr.UserID equals sp.UserID into permGroup
                             where usr.Active && usr.CompanyID == companyId && (isAdmin || (permGroup.Any(u => u.SecurityLevel <= 1 && usrperm.Contains(u.DatasetID))))
                             select
                             new
                             {
                                 usr.UserID,
                                 usr.FirstName,
                                 usr.LastName

                             }).ToList();

            
%>
<div style="text-align: center; background: #991000; height: 35px;width:98%;">
    <h5 style="color: white; padding-top: 6px;">
        USERS</h5>
</div>
<ul style="list-style:disc; line-height: 2; color: #333; padding-left: 25px;color: #991000;" class="outer">
    <% if (users != null)
       {
           foreach (var parent in users)
           {
               if (parent.FirstName == null || parent.LastName == null)
               {%>
    <li>
        <%: Html.ActionLink(parent.UserID, "EditUser", "User", new { usr = parent.UserID }, null)%>
    </li>
    <%   }
               else
               { %>
    <li>
        <%: Html.ActionLink(parent.LastName + " , " + parent.FirstName, "EditUser", "User", new { usr = parent.UserID }, null)%></li>
    <% }
           }
       }
       else
       {%>
    <li>Please add users</li>
    <%} %>
</ul>
<%   }
        }
    }

  
%>
