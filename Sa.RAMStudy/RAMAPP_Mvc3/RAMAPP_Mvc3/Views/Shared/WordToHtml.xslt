﻿<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/3/main">

  <xsl:output method="html" />

  <xsl:template match="/">

    <xsl:apply-templates select="//w:body" />

  </xsl:template>

  <xsl:template match="w:body">



        <pre>

          <xsl:apply-templates />

        </pre>


  </xsl:template>

  <xsl:template match="w:p">

    <div>

      <xsl:apply-templates select="w:r" />

    </div>

  </xsl:template>

  <xsl:template match="w:r">

    <xsl:apply-templates select="w:t" />

  </xsl:template>

  <xsl:template match="w:t">

    <span>

      <xsl:apply-templates select="../w:rPr" />

      <xsl:value-of select="." />

    </span>

  </xsl:template>

  <xsl:template match="w:rPr">

    <xsl:attribute name="style">

      <xsl:apply-templates />

    </xsl:attribute>

  </xsl:template>

  <xsl:template match="w:u">text-decoration:underline;</xsl:template>

  <xsl:template match="w:b">font-weight:bold;</xsl:template>

  <xsl:template match="w:i">font-style:italic;</xsl:template>

</xsl:stylesheet>

