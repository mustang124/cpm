﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>

<%@ Import Namespace="RAMAPP_Mvc3.Repositories.Cache" %>
<%@ Import Namespace="RAMAPP_Mvc3.Helpers" %>
<%@ Import Namespace="RAMAPP_Mvc3.Navigation" %>
<!-- start navigation -->
<%
     var nav = new NavigationTree();
     var adminTree = nav.GetNavigation(NavigationTree.NavigationType.Administration);
    /*var myList = new StringBuilder();
    var isAdmin = SessionUtil.IsAdmin();
    var isSiteAdmin = SessionUtil.IsSiteAdmin();
    
    

        var cachedNavigation = new CachedNavigationRepository();
        
        var sites = cachedNavigation.GetSiteUnitList();
        myList.Append("<ul id='ulOuter' style=\"padding-left:20px;\">");


        foreach (var parent in sites)
        {
            //Skip site where site administrators are users 
            if (parent.Role == 0)
                continue;
            
            myList.Append("<li class=' rightPointerNav'><strong>" +
                Html.ActionLink(parent.LockedImg as string + parent.SiteName as string, "SiteDetail", new { id = parent.DatasetID },new {title = "Change Site Characteristics"}));
            myList.Append("</strong><div class='sep'>&nbsp;</div><div class='unitsContainer'><ul>");

            List<Units> siteUnits = (List<Units>)parent.UnitsOfSite;
            if (siteUnits.Count == 0)
            {
                myList.Append("<li>Please add units.</li>");
            }
            else
            {
                myList.Append("<li class='subHeader'> UNITS </li>");
                foreach (var child in siteUnits)
                {
                    //if (child != null)
                   // {

                        myList.Append("<li> " + Html.ActionLink(parent.LockedImg as string + child.UnitName as string, "UnitDetails", new { id = child.DatasetID, onclick = "sideMenuClick();" },new {title="Change Unit Characteristics"}) + "</li>");
                    //}

                }

            }
            myList.Append("</ul></div></li> ");
        }

        myList.Append("</ul>");




   // }   
      */
%>



<div style="text-align:center;background:#991000;height:35px;width:98%">
<h5 style="color:white;padding-top:6px;">SITES AND UNITS</h5>
</div>
<p>
 
 <%: Html.Raw(adminTree.Length == 0 ? " No sites were found." : adminTree.Replace("#lockimg#", "<img border=0 src=\"" + Url.Content("~/images/lockedsym.png") + "\" alt='' />"))%>
</p>


<!-- end navigation -->

