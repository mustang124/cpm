﻿

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using RAMAPP_Mvc3.Entity;

namespace RAMAPP_Mvc3.Models
{
    public class CompanyModel
    {

        [ScaffoldColumn(false)]
        public int DatasetID { get; set; } 
        
        [DisplayName("Company Name")]
        public string Name { get; set; }
        
        [DisplayName("Assign Company Identification Number")]
        public string CompanyID { get; set; }
        
        [DisplayName("Maximum number of sites")]
        public int SiteLimit { get; set; }
        
        [DisplayName("Company Coordinator")]
        public UserDetails  DataCoordinator { get; set; }
    }

}