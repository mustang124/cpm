﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace RAMAPP_Mvc3.Models
{
    [Table("Comments")]
    public class CommentModel
    {
        [ScaffoldColumn(true)]
        public string CompanyID { get; set; }
        
        [ScaffoldColumn(true)]
        public string NoteID { get; set; }

        [StringLength(250)]
        [DataType(DataType.MultilineText)]
        [UIHint("TextAreaTmpl")]
        public string Comment { get; set; }

        public DateTime Posted { get; set; }
    }


    public class CommentsModel
    {
        public List<CommentModel> CommentCollection { get; set; }
        public CommentModel newComment { get; set; }
    }
}