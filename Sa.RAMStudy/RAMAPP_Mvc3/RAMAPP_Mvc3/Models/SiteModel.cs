﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using RAMAPP_Mvc3.Entity;

namespace RAMAPP_Mvc3.Models
{
    public enum UOM { METRIC = 1, US = 2 }

    public class SelectListItemComparer : EqualityComparer<SelectListItem>
    {
        public override bool Equals(SelectListItem x, SelectListItem y)
        {
            return x.Value.Equals(y.Value);
        }

        public override int GetHashCode(SelectListItem obj)
        {
            return obj.Value.GetHashCode();
        }
    }

    public class SiteModel
    {
        public string CompanySID { get; set; }

        [DisplayName("Currency")]
        public string Currency { get; set; }

        public int DatasetID { get; set; }

        [DisplayName("Exchange Rate")]
        public float ExchangeRate { get; set; }

        [ScaffoldColumn(true)]
        [DefaultValue(true)]
        public bool IsActive { get; set; }

        [DisplayName("Location")]
        public string Location { get; set; }

        [DisplayName("Name")]
        [Required]
        public string Name { get; set; }

        [DisplayName("Number of Errors")]
        [ScaffoldColumn(true)]
        [DefaultValue(0)]
        public int NoOfErrs { get; set; }

        [DisplayName("Completed, %")]
        [ScaffoldColumn(true)]
        [DefaultValue(0)]
        public int? PercentCompleted { get; set; }

        [DisplayName("Unit(s)")]
        public IEnumerable<UnitDetails> Units { get; set; }

        [DisplayName("Unit of Measurement")]
        [DefaultValue((short)UOM.US)]
        public short UOMPref { get; set; }

        [DisplayName("User(s)")]
        public IEnumerable<UserDetails> Users { get; set; }
    }

    public class UnitDetails
    {
        public string CompanySID { get; set; }

        public int DatasetID { get; set; }

        [DisplayName("Generic Product Name")]
        public string GenericProductName { get; set; }

        public string OtherGenericProductName { get; set; }

        [DisplayName("Primary Product Name")]
        public string PrimaryProductName { get; set; }

        [DisplayName("Process Family")]
        public string ProcessFamily { get; set; }

        [DisplayName("Process Category")]
        public string ProcessCategory { get; set; }


        public int SiteDatasetID { get; set; }

        [DisplayName("Unit Name")]
        public string UnitName { get; set; }
    }

    [Table("LogIn")]
    public class UserDetails
    {
        [ScaffoldColumn(true)]
        public string CompanyID { get; set; }

        [Column("UserLanguage")]
        [DisplayName("Culture Setting")]
        [DefaultValue("en")]
        public string Culture { get; set; }

        [Required]
        [Column("Email")]
        [Display(Prompt = "Enter Email Address")]
        [DataType(DataType.EmailAddress)]
        // [RegularExpression(@"\b[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b",ErrorMessage="Invalid Email Address")]
        [RegularExpression(@"(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})", ErrorMessage = "Invalid Email Address")]
        public string EmailAddress { get; set; }

        [DisplayName("First")]
        [Display(Prompt = "Enter First Name")]
        [Required(ErrorMessage = "Firstname is required.")]
        public string Firstname { get; set; }

        public string Fullname
        {
            get { return string.Format("{0} {1}", Firstname == null ? "" : Firstname, Lastname == null ? "" : Lastname); }
        }

        [DisplayName("Job Title")]
        [Display(Prompt = "Enter Job Title")]
        public string JobTile { get; set; }

        [DisplayName("Last")]
        [Display(Prompt = "Enter Last Name")]
        [Required(ErrorMessage = "Lastname is required.")]
        public string Lastname { get; set; }

        [Required]
        [DisplayName("Password")]
        [Display(Prompt = "Enter Password")]
        [DataType(DataType.Password)]
        [StringLength(25)]
        public string Password { get; set; }

        [Column("SecurityLevel")]
        [DisplayName("Security Level")]
        public short Role { get; set; }

        [Column("ScreenName")]
        [DisplayName("Screen Name")]
        public string ScreenName { get; set; }

        [Column("PhoneNo")]
        [Display(Prompt = "Enter Telephone Number")]
        [DataType(DataType.PhoneNumber)]
        public string Telephone { get; set; }

        [StringLength(35)]
        public string UserID { get; set; }

        public int UserNo { get; set; }
    }
}