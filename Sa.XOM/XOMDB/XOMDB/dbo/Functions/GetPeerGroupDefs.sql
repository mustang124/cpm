﻿
CREATE FUNCTION [dbo].[GetPeerGroupDefs](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS @results TABLE (PGKey int NOT NULL, Study varchar(10) NOT NULL, StudyYear smallint NOT NULL, Methodology varchar(10) NOT NULL
	, RegionCode varchar(5) NOT NULL, MajorGroup varchar(50) NOT NULL, SubGroup varchar(50) NOT NULL, PricingScenario varchar(50) NOT NULL
	, LongText nvarchar(MAX) NOT NULL, PublishEuro bit NOT NULL, PGType varchar(10) NOT NULL, tsModified datetime NOT NULL)
AS 
BEGIN
	INSERT @results (PGKey, Study, StudyYear, Methodology, RegionCode, MajorGroup, SubGroup, PricingScenario, LongText, PublishEuro, PGType, tsModified)
	SELECT DISTINCT pgd.PGKey, pgd.Study, pgd.StudyYear, pgd.Methodology, pgd.RegionCode, pgd.MajorGroup, pgd.SubGroup, pgd.PricingScenario, pgd.LongText, pgd.PublishEuro, PGType = 'StudyPeer', pgd.tsModified
	FROM dbo.CompanyStudies s INNER JOIN dbo.PeerGroupDef pgd ON pgd.Study = s.Study AND pgd.StudyYear = s.StudyYear
	INNER JOIN dbo.StudyGroups sg ON sg.Study = pgd.Study AND sg.StudyYear = pgd.StudyYear AND sg.RegionCode = pgd.RegionCode AND sg.MajorGroup = pgd.MajorGroup
	WHERE s.CompanyID = @CompanyID AND pgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')

	INSERT @results (PGKey, Study, StudyYear, Methodology, RegionCode, MajorGroup, SubGroup, PricingScenario, LongText, PublishEuro, PGType, tsModified)
	SELECT DISTINCT pgd.PGKey, pgd.Study, pgd.StudyYear, pgd.Methodology, pgd.RegionCode, pgd.MajorGroup, pgd.SubGroup, pgd.PricingScenario, pgd.LongText, pgd.PublishEuro, PGType = 'Company', pgd.tsModified
	FROM dbo.CompanyRefnums s INNER JOIN dbo.PeerGroupDef pgd ON pgd.StudyRefnum = s.Refnum
	WHERE s.CompanyID = @CompanyID AND pgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
	AND pgd.PGKey NOT IN (SELECT PGKey FROM @results)

	INSERT @results (PGKey, Study, StudyYear, Methodology, RegionCode, MajorGroup, SubGroup, PricingScenario, LongText, PublishEuro, PGType, tsModified)
	SELECT DISTINCT rpd.PGKey, rpd.Study, rpd.StudyYear, rpd.Methodology, rpd.RegionCode, rpd.MajorGroup, rpd.SubGroup, rpd.PricingScenario, rpd.LongText, rpd.PublishEuro, PGType = 'CoPeer', rpd.tsModified
	FROM dbo.RefineryPeers rp INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = rp.FacilityPGKey
	INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
	INNER JOIN dbo.PeerGroupDef rpd ON rpd.PGKey = rp.TargetPGKey
	WHERE cr.CompanyID = @CompanyID AND (pgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR rpd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR rp.tsModified  > ISNULL(@dtModifiedSince, '1/1/2000'))
	AND rpd.PGKey NOT IN (SELECT PGKey FROM @results)

	INSERT @results (PGKey, Study, StudyYear, Methodology, RegionCode, MajorGroup, SubGroup, PricingScenario, LongText, PublishEuro, PGType, tsModified)
	SELECT DISTINCT rpd.PGKey, rpd.Study, rpd.StudyYear, rpd.Methodology, rpd.RegionCode, rpd.MajorGroup, rpd.SubGroup, rpd.PricingScenario, rpd.LongText, rpd.PublishEuro, PGType = 'CoPeer', rpd.tsModified
	FROM dbo.ProcessPeers pp INNER JOIN dbo.ProcessPeerGroupDef ppgd ON ppgd.PPGKey = pp.FacilityPPGKey
	INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = ppgd.PGKey
	INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
	 INNER JOIN dbo.ProcessPeerGroupDef tppgd ON tppgd.PPGKey = pp.TargetPPGKey
	INNER JOIN dbo.PeerGroupDef rpd ON rpd.PGKey = tppgd.PGKey
	WHERE cr.CompanyID = @CompanyID AND (pgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR rpd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR pp.tsModified  > ISNULL(@dtModifiedSince, '1/1/2000'))
	AND rpd.PGKey NOT IN (SELECT PGKey FROM @results)
	
	RETURN
END