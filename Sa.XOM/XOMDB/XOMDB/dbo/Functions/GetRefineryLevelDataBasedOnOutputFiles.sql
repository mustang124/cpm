﻿
CREATE FUNCTION [dbo].[GetRefineryLevelDataBasedOnOutputFiles](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS TABLE AS
RETURN (
	WITH OutputTableRows AS 
	(
	SELECT f.StudyYear, f.Study, f.Filename, f.FileType, f.SheetNo, f.SheetName, pgd.PGKey, f.Refnum, f.Currency, f.RowNo, f.RowText, f.DBTableName, f.DBColumnName, f.DBFilter, f.VKey
	FROM dbo.CompanyOutputFiles cof
	INNER JOIN dbo.OutputFileSheetCells f ON f.FKey = cof.FKey AND f.FileType IN ('Output','Trend')
	INNER JOIN dbo.PeerGroupDef pgd ON pgd.StudyRefnum = f.Refnum
	INNER JOIN dbo.VarDef vd ON vd.VKey = f.VKey
	WHERE f.VKey IS NOT NULL AND f.RowHidden = 0 AND f.Study NOT LIKE 'cs%'
	AND cof.CompanyID = @CompanyID
	AND NOT EXISTS (SELECT * FROM CompanyOutputExclusions x WHERE x.CompanyID = cof.CompanyID AND x.FKey = cof.FKey AND (x.VKey = f.VKey OR x.SectionCode = vd.SectionCode))
	)
	--SELECT otr.*, d.Currency, d.numberValue, d.textValue, d.dateValue
	SELECT d.PGKey, d.VKey, d.Currency, d.numberValue, d.textValue, d.dateValue, d.tsModified
	FROM dbo.RefineryData d
	WHERE EXISTS (SELECT 1 FROM OutputTableRows otr WHERE d.VKey = otr.VKey AND d.PGKey = otr.PGKey AND d.Currency = otr.Currency)
	AND d.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
	UNION
	SELECT d.PGKey, d.VKey, d.Currency, d.numberValue, d.textValue, d.dateValue, d.tsModified
	FROM dbo.RefineryData d
	WHERE PGKey IN (SELECT PGKey FROM dbo.PeerGroupDef WHERE StudyRefnum IN (SELECT Refnum FROM dbo.CompanyRefnums WHERE CompanyID = @CompanyID))
	AND d.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
)

