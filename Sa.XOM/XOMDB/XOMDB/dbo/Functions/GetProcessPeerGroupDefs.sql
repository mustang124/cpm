﻿






CREATE FUNCTION [dbo].[GetProcessPeerGroupDefs](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS TABLE
AS
RETURN (
	SELECT DISTINCT pgd.PPGKey, pgd.PGKey, pgd.ProcessID, pgd.ProcessType, pgd.LongText, pgd.Class, pgd.SubClass, pgd.PeerType, pgd.RamGroup, pgd.tsModified
	FROM dbo.GetPeerGroupDefs(@CompanyID, NULL) r
	INNER JOIN dbo.ProcessPeerGroupDef pgd ON r.PGKey = pgd.PGKey
	INNER JOIN dbo.CompanyStudyProcesses csp ON (csp.Study = r.Study OR (csp.Study = 'LIV' AND r.Study = 'LIVAll'))AND csp.StudyYear = r.StudyYear AND csp.ProcessID = pgd.ProcessID
	WHERE csp.CompanyID = @CompanyID AND pgd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
	UNION
	SELECT DISTINCT tpg.PPGKey, tpg.PGKey, tpg.ProcessID, tpg.ProcessType, tpg.LongText, tpg.Class, tpg.SubClass, tpg.PeerType, tpg.RamGroup, tpg.tsModified
	FROM dbo.GetPeerGroupDefs(@CompanyID, NULL) r INNER JOIN dbo.ProcessPeerGroupDef pgd ON r.PGKey = pgd.PGKey
	INNER JOIN dbo.ProcessPeers pp ON pp.FacilityPPGKey = pgd.PPGKey
	INNER JOIN dbo.ProcessPeerGroupDef tpg ON tpg.PPGKey = pp.TargetPPGKey
	WHERE tpg.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR pp.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') 
)


