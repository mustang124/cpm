﻿
CREATE FUNCTION [dbo].[GetProcessLevelData](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS TABLE AS
RETURN (
	SELECT pd.PPGKey, pd.VKey, pd.Currency, pd.numberValue, pd.textValue, pd.dateValue, tsModified = CASE WHEN k.tsModified > pd.tsModified THEN k.tsModified ELSE pd.tsModified END
	FROM (SELECT DISTINCT pgd.Study, pgd.StudyYear, ppgd.ProcessID, ppgd.PPGKey, s.SectionCode, pgd.PublishEuro, ppgd.tsModified
		FROM dbo.GetProcessPeerGroupDefsNew(@CompanyID, NULL) ppgd
		INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = ppgd.PGKey 
		INNER JOIN dbo.ProcessPeerGroupSections s ON s.Study = pgd.Study AND s.ProcessID = ppgd.ProcessID AND s.ProcessType = ppgd.ProcessType AND s.RegionCode = pgd.RegionCode AND s.MajorGroup = pgd.MajorGroup AND ISNULL(s.Class,'N/A') = ISNULL(ppgd.Class,'N/A')
		
		UNION
		
		SELECT DISTINCT pgd.Study, pgd.StudyYear, ppgd.ProcessID, ppgd.PPGKey, s.SectionCode, pgd.PublishEuro, ppgd.tsModified
		FROM dbo.GetProcessPeerGroupDefsNew(@CompanyID, NULL) ppgd INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = ppgd.PGKey
		INNER JOIN dbo.Sections s ON 1=1
		WHERE s.SectionCode IN (SELECT SectionCode FROM dbo.ProcessPeerGroupSections ps WHERE ps.Study = pgd.Study) --AND s.ProcessID = ppgd.ProcessID
		OR pgd.StudyRefnum IN (SELECT Refnum FROM dbo.CompanyRefnums WHERE CompanyID = @CompanyID)
	) k
	INNER JOIN (SELECT vd.VKey, vd.SectionCode, vd.tsModified FROM dbo.CompanyVarDef vd WHERE vd.CompanyID = @CompanyID AND vd.SuppressData = 0) v ON v.SectionCode = k.SectionCode
	INNER JOIN dbo.ProcessData pd ON pd.VKey = v.VKey AND pd.PPGKey = k.PPGKey AND (pd.Currency = 'USD' OR (pd.Currency = 'EUR' AND k.PublishEuro = 1)) 
	WHERE pd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR k.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR v.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')
)

