﻿CREATE FUNCTION [dbo].[GetRefineryLevelData](@CompanyID varchar(15), @dtModifiedSince datetime)
RETURNS @data TABLE (PGKey int NOT NULL, VKey int NOT NULL, Currency varchar(4) NOT NULL, numberValue float NULL, textValue nvarchar(max) NULL, dateValue smalldatetime NULL, tsModified datetime NOT NULL)
AS BEGIN
	DECLARE @PGSections TABLE (PGKey int NOT NULL, SectionCode varchar(255) NOT NULL, PublishEuro bit NOT NULL, tsModified datetime NOT NULL, PRIMARY KEY (PGKey, SectionCode))

	INSERT @PGSections(PGKey, SectionCode, PublishEuro, tsModified)
	SELECT DISTINCT pgd.PGKey, pgs.SectionCode, pgd.PublishEuro, tsModified = CASE WHEN s.tsModified > pgd.tsModified THEN s.tsModified ELSE pgd.tsModified END
	FROM dbo.CompanyRefnums s INNER JOIN dbo.PeerGroupDef pgd ON pgd.StudyRefnum = s.Refnum
	INNER JOIN dbo.Sections pgs ON 1=1 --pgs.Study = pgd.Study AND pgs.RegionCode = pgd.RegionCode
	WHERE s.CompanyID = @CompanyID AND pgd.PGKey NOT IN (SELECT PGKey FROM @PGSections)

	INSERT @PGSections(PGKey, SectionCode, PublishEuro, tsModified)
	SELECT DISTINCT pgd.PGKey, pgs.SectionCode, pgd.PublishEuro, tsModified = CASE WHEN s.tsModified > pgd.tsModified THEN s.tsModified ELSE pgd.tsModified END
	FROM dbo.CompanyStudies s INNER JOIN dbo.PeerGroupDef pgd ON pgd.Study = s.Study AND pgd.StudyYear = s.StudyYear
	INNER JOIN dbo.PeerGroupSections pgs ON pgs.Study = pgd.Study AND pgs.RegionCode = pgd.RegionCode AND pgs.MajorGroup = pgd.MajorGroup
	WHERE s.CompanyID = @CompanyID AND pgd.PGKey NOT IN (SELECT PGKey FROM @PGSections)

	INSERT @PGSections(PGKey, SectionCode, PublishEuro, tsModified)
	SELECT rp.TargetPGKey, pgs.SectionCode, PublishEuro = CASE WHEN MAX(CASE WHEN pgd.PublishEuro = 1 THEN 1 ELSE 0 END) = 1 AND ppgd.PublishEuro = 1 THEN 1 ELSE 0 END
		, tsModified = CASE WHEN MAX(rp.tsModified) >= MAX(pgd.tsModified) AND MAX(rp.tsModified) >= MAX(cr.tsModified) THEN MAX(rp.tsModified)
							WHEN MAX(cr.tsModified) >= MAX(rp.tsModified) AND MAX(cr.tsModified) >= MAX(pgd.tsModified) THEN MAX(cr.tsModified) 
							ELSE MAX(pgd.tsModified) 
						END
	FROM dbo.RefineryPeers rp INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = rp.FacilityPGKey
	INNER JOIN dbo.PeerGroupDef ppgd ON ppgd.PGKey = rp.TargetPGKey
	INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
	INNER JOIN dbo.PeerGroupSections pgs ON pgs.Study = pgd.Study AND pgs.RegionCode = pgd.RegionCode
	WHERE cr.CompanyID = @CompanyID AND rp.TargetPGKey NOT IN (SELECT PGKey FROM @PGSections)
	GROUP BY rp.TargetPGKey, pgs.SectionCode, ppgd.PublishEuro

	INSERT @data (PGKey, VKey, Currency, numberValue, textValue, dateValue, tsModified)
	SELECT rd.PGKey, rd.VKey, rd.Currency, rd.numberValue, rd.textValue, rd.dateValue, tsModified = CASE WHEN k.tsModified > rd.tsModified THEN k.tsModified ELSE rd.tsModified END
	FROM @PGSections k
	INNER JOIN (SELECT VKey, SectionCode, tsModified FROM dbo.CompanyVarDef vd WHERE vd.SuppressData = 0 AND vd.CompanyID = @CompanyID) v ON v.SectionCode = k.SectionCode
	INNER JOIN dbo.RefineryData rd ON rd.VKey = v.VKey AND rd.PGKey = k.PGKey AND (rd.Currency = 'USD' OR (rd.Currency = 'EUR' AND k.PublishEuro = 1)) 
	WHERE rd.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR k.tsModified > ISNULL(@dtModifiedSince, '1/1/2000') OR v.tsModified > ISNULL(@dtModifiedSince, '1/1/2000')

	RETURN
END

