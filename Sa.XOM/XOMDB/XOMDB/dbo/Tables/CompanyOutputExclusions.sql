﻿CREATE TABLE [dbo].[CompanyOutputExclusions] (
    [CompanyID]   VARCHAR (10)  NOT NULL,
    [FKey]        INT           NOT NULL,
    [SectionCode] VARCHAR (255) NULL,
    [VKey]        INT           NULL,
    [tsModified]  DATETIME      CONSTRAINT [DF_CompanyOutputExclusions_tsModified] DEFAULT (getdate()) NOT NULL
);

