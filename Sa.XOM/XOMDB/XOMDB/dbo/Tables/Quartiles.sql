﻿CREATE TABLE [dbo].[Quartiles] (
    [PGKey]      INT        NOT NULL,
    [VKey]       INT        NOT NULL,
    [DataCount]  SMALLINT   NOT NULL,
    [NumTiles]   TINYINT    NOT NULL,
    [Top2]       FLOAT (53) NOT NULL,
    [Tile1Break] FLOAT (53) NULL,
    [Tile2Break] FLOAT (53) NULL,
    [Tile3Break] FLOAT (53) NULL,
    [Bottom2]    FLOAT (53) NOT NULL,
    [tsModified] DATETIME   CONSTRAINT [DF_Quartiles_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified] DATETIME   CONSTRAINT [DF_Quartiles_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Quartiles] PRIMARY KEY CLUSTERED ([PGKey] ASC, [VKey] ASC)
);


GO

CREATE TRIGGER dbo.SetQuartilesModifiedAfterUpdate
   ON  dbo.Quartiles
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Quartiles
	SET tsVerified = getdate(), tsModified = CASE WHEN chk.Changed = 1 THEN getdate() ELSE d.tsModified END
	FROM dbo.Quartiles Quartiles
	INNER JOIN deleted d ON d.PGKey = Quartiles.PGKey AND d.VKey = Quartiles.VKey
	CROSS APPLY (VALUES (CASE WHEN 
			d.DataCount <> Quartiles.DataCount OR d.NumTiles <> Quartiles.NumTiles OR d.Top2 <>Quartiles.Top2 OR d.Bottom2 <>Quartiles.Bottom2
			OR d.Tile1Break <> Quartiles.Tile1Break OR (d.Tile1Break IS NULL AND Quartiles.Tile1Break IS NOT NULL) OR (d.Tile1Break IS NOT NULL AND Quartiles.Tile1Break IS NULL)
			OR d.Tile2Break <> Quartiles.Tile2Break OR (d.Tile2Break IS NULL AND Quartiles.Tile2Break IS NOT NULL) OR (d.Tile2Break IS NOT NULL AND Quartiles.Tile2Break IS NULL)
			OR d.Tile3Break <> Quartiles.Tile3Break OR (d.Tile3Break IS NULL AND Quartiles.Tile3Break IS NOT NULL) OR (d.Tile3Break IS NOT NULL AND Quartiles.Tile3Break IS NULL) 
		THEN 1 ELSE 0 END)) chk(Changed)
	
END

GO

CREATE TRIGGER dbo.SetQuartilesModifiedAfterInsert
   ON  dbo.Quartiles
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Quartiles
	SET tsModified = getdate(), tsVerified = getdate()
	FROM dbo.Quartiles Quartiles
	INNER JOIN inserted i ON i.PGKey = Quartiles.PGKey AND i.VKey = Quartiles.VKey
	
END
