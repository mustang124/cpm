﻿CREATE TABLE [dbo].[CompanyRefnums] (
    [CompanyID]  VARCHAR (10) NOT NULL,
    [Refnum]     VARCHAR (25) NOT NULL,
    [tsModified] DATETIME     CONSTRAINT [DF_CompanyRefnums_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CompanyRefnum] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [Refnum] ASC)
);

