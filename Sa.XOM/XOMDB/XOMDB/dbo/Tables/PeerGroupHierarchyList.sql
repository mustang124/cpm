﻿CREATE TABLE [dbo].[PeerGroupHierarchyList] (
    [PGHKey]           INT           IDENTITY (1, 1) NOT NULL,
    [PGHName]          VARCHAR (100) NOT NULL,
    [GroupType]        VARCHAR (50)  NOT NULL,
    [RegionCode]       VARCHAR (5)   NOT NULL,
    [MajorGroup]       VARCHAR (15)  NOT NULL,
    [ParentPGHItemKey] INT           CONSTRAINT [DF_PeerGroupHierarchyList_ParentPGHItemKey] DEFAULT ((0)) NOT NULL,
    [tsModified]       DATETIME      CONSTRAINT [DF_PeerGroupHierarchyList_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PeerGroupHierarchyList] PRIMARY KEY CLUSTERED ([PGHKey] ASC),
    CONSTRAINT [FK_PeerGroupHierarchyList_PeerGroupHierarchyGroupTypes] FOREIGN KEY ([GroupType]) REFERENCES [dbo].[PeerGroupHierarchyGroupTypes] ([GroupType]),
    CONSTRAINT [FK_PeerGroupHierarchyList_PeerGroupHierarchyMajorGroups] FOREIGN KEY ([MajorGroup]) REFERENCES [dbo].[PeerGroupHierarchyMajorGroups] ([MajorGroup]),
    CONSTRAINT [FK_PeerGroupHierarchyList_PeerGroupHierarchyRegions] FOREIGN KEY ([RegionCode]) REFERENCES [dbo].[Regions] ([RegionCode]),
    CONSTRAINT [IX_PeerGroupHierarchyList_PGHName] UNIQUE NONCLUSTERED ([PGHName] ASC)
);


GO

CREATE TRIGGER dbo.SetPeerGroupHierarchyListModifiedAfterUpdate
   ON  dbo.PeerGroupHierarchyList
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupHierarchyList
	SET tsModified = getdate()
	FROM dbo.PeerGroupHierarchyList PeerGroupHierarchyList
	INNER JOIN deleted d ON d.PGHKey = PeerGroupHierarchyList.PGHKey
	WHERE d.PGHName <> PeerGroupHierarchyList.PGHName
	OR d.GroupType <> PeerGroupHierarchyList.GroupType
	OR d.Region <> PeerGroupHierarchyList.Region
	OR d.MajorGroup <> PeerGroupHierarchyList.MajorGroup
	OR d.ParentPGHItemKey <> PeerGroupHierarchyList.ParentPGHItemKey
	
END

GO

CREATE TRIGGER dbo.SetPeerGroupHierarchyListModifiedAfterInsert
   ON  dbo.PeerGroupHierarchyList
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupHierarchyList
	SET tsModified = getdate()
	FROM dbo.PeerGroupHierarchyList PeerGroupHierarchyList
	INNER JOIN inserted i ON i.PGHKey = PeerGroupHierarchyList.PGHKey
	
END
