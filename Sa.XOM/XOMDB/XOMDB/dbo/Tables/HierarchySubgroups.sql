﻿CREATE TABLE [dbo].[HierarchySubgroups] (
    [GroupCode] VARCHAR (10) NOT NULL,
    [GroupName] VARCHAR (50) NOT NULL,
    [GroupType] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_HierarchySubgroups] PRIMARY KEY CLUSTERED ([GroupCode] ASC)
);

