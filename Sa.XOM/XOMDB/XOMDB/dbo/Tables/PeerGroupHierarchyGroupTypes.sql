﻿CREATE TABLE [dbo].[PeerGroupHierarchyGroupTypes] (
    [GroupType]     VARCHAR (50)  NOT NULL,
    [GroupTypeDesc] VARCHAR (100) NOT NULL,
    [tsModified]    DATETIME      CONSTRAINT [DF_PeerGroupHierarchyGroupTypes_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PeerGroupHierarchyGroupTypes] PRIMARY KEY CLUSTERED ([GroupType] ASC)
);


GO


CREATE TRIGGER [dbo].[SetPeerGroupHierarchyGroupTypesModifiedAfterInsert]
   ON  [dbo].[PeerGroupHierarchyGroupTypes]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupHierarchyGroupTypes
	SET tsModified = getdate()
	FROM dbo.PeerGroupHierarchyGroupTypes PeerGroupHierarchyGroupTypes
	INNER JOIN inserted i ON i.GroupType = PeerGroupHierarchyGroupTypes.GroupType
	
END


GO

CREATE TRIGGER [dbo].[SetPeerGroupHierarchyGroupTypesModifiedAfterUpdate]
   ON  [dbo].[PeerGroupHierarchyGroupTypes]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (GroupType) OR UPDATE (GroupTypeDesc) 
		UPDATE PeerGroupHierarchyGroupTypes
		SET tsModified =GETDATE()
		FROM dbo.PeerGroupHierarchyGroupTypes PeerGroupHierarchyGroupTypes 
		INNER JOIN deleted d ON d.GroupType = PeerGroupHierarchyGroupTypes.GroupType
	
END

