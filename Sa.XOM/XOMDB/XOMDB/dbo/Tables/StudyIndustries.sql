﻿CREATE TABLE [dbo].[StudyIndustries] (
    [IndustryCode] VARCHAR (10)  NOT NULL,
    [IndustryName] VARCHAR (255) NULL,
    [tsModified]   DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Studies] PRIMARY KEY CLUSTERED ([IndustryCode] ASC)
);


GO

CREATE TRIGGER [dbo].[SetStudyIndustriesModifiedAfterInsert]
   ON  [dbo].[StudyIndustries]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE StudyIndustries
	SET tsModified = getdate()
	FROM dbo.StudyIndustries StudyIndustries
	INNER JOIN inserted i ON i.IndustryCode = StudyIndustries.IndustryCode
	
END


GO

CREATE TRIGGER [dbo].[SetStudyIndustriesModifiedAfterUpdate]
   ON  [dbo].[StudyIndustries]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (IndustryCode) OR UPDATE (IndustryName)
		UPDATE StudyIndustries
		SET tsModified =GETDATE()
		FROM dbo.StudyIndustries StudyIndustries 
		INNER JOIN deleted d ON d.IndustryCode = StudyIndustries.IndustryCode
	
END

