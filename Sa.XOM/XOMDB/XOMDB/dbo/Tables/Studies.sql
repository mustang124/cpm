﻿CREATE TABLE [dbo].[Studies] (
    [StudyCode]    VARCHAR (10)  NOT NULL,
    [IndustryCode] VARCHAR (10)  NOT NULL,
    [StudyName]    VARCHAR (255) NOT NULL,
    [tsModified]   DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_Studies_1] PRIMARY KEY CLUSTERED ([StudyCode] ASC),
    CONSTRAINT [FK_Studies_StudyIndustries] FOREIGN KEY ([IndustryCode]) REFERENCES [dbo].[StudyIndustries] ([IndustryCode])
);


GO

CREATE TRIGGER [dbo].[SetStudiesModifiedAfterUpdate]
   ON  [dbo].[Studies]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (StudyCode) OR UPDATE (IndustryCode) OR UPDATE(StudyName)
		UPDATE Studies
		SET tsModified =GETDATE()
		FROM dbo.Studies Studies 
		INNER JOIN deleted d ON d.StudyCode = Studies.StudyCode
	
END


GO

CREATE TRIGGER [dbo].[SetStudiesModifiedAfterInsert]
   ON  [dbo].[Studies]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE Studies
	SET tsModified = getdate()
	FROM dbo.Studies Studies
	INNER JOIN inserted i ON i.StudyCode = Studies.StudyCode
	
END

