﻿CREATE TABLE [dbo].[UOM_UStoMetric] (
    [UOMCode_US]         VARCHAR (50) NOT NULL,
    [UOMCode_MET]        VARCHAR (50) NOT NULL,
    [ConversionConstant] REAL         NULL,
    [tsModified]         DATETIME     DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_UOM_UStoMetric] PRIMARY KEY CLUSTERED ([UOMCode_US] ASC)
);


GO

CREATE TRIGGER [dbo].[SetUOM_UStoMetricModifiedAfterInsert]
   ON  [dbo].[UOM_UStoMetric]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE UOM_UStoMetric
	SET tsModified = getdate()
	FROM dbo.UOM_UStoMetric UOM_UStoMetric
	INNER JOIN inserted i ON i.UOMCode_US = UOM_UStoMetric.UOMCode_US
	
END


GO

CREATE TRIGGER [dbo].[SetUOM_UStoMetricModifiedAfterUpdate]
   ON  [dbo].[UOM_UStoMetric]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (UOMCode_US) OR UPDATE (UOMCode_MET) OR UPDATE(ConversionConstant)
		UPDATE UOM_UStoMetric
		SET tsModified =GETDATE()
		FROM dbo.UOM_UStoMetric UOM_UStoMetric 
		INNER JOIN deleted d ON d.UOMCode_US = UOM_UStoMetric.UOMCode_US
	
END

