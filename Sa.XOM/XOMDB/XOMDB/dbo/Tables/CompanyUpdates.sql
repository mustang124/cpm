﻿CREATE TABLE [dbo].[CompanyUpdates] (
    [CompanyID]  VARCHAR (10)  NOT NULL,
    [UpdateDate] DATETIME      NOT NULL,
    [Notes]      VARCHAR (MAX) NULL,
    CONSTRAINT [PK_CompanyUpdates] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [UpdateDate] ASC)
);

