﻿CREATE TABLE [dbo].[StudyGroups] (
    [Study]      VARCHAR (10) NOT NULL,
    [StudyYear]  SMALLINT     NOT NULL,
    [RegionCode] VARCHAR (5)  NOT NULL,
    [MajorGroup] VARCHAR (50) NOT NULL,
    [tsModified] DATETIME     DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_StudyGroups] PRIMARY KEY CLUSTERED ([Study] ASC, [StudyYear] ASC, [RegionCode] ASC, [MajorGroup] ASC)
);


GO

CREATE TRIGGER [dbo].[SetStudyGroupsModifiedAfterInsert]
   ON  [dbo].[StudyGroups]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE StudyGroups
	SET tsModified = getdate()
	FROM dbo.StudyGroups StudyGroups
	INNER JOIN inserted i ON i.Study = StudyGroups.Study AND i.StudyYear = StudyGroups.StudyYear AND i.RegionCode = StudyGroups.RegionCode AND i.MajorGroup = StudyGroups.MajorGroup
	
END


GO

CREATE TRIGGER [dbo].[SetStudyGroupsModifiedAfterUpdate]
   ON  [dbo].[StudyGroups]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (Study) OR UPDATE (StudyYear) OR UPDATE(RegionCode) OR UPDATE(MajorGroup)
		UPDATE StudyGroups
		SET tsModified =GETDATE()
		FROM dbo.StudyGroups StudyGroups 
		INNER JOIN deleted d ON d.Study = StudyGroups.Study AND d.StudyYear = StudyGroups.StudyYear AND d.RegionCode = StudyGroups.RegionCode AND d.MajorGroup = StudyGroups.MajorGroup
	
END

