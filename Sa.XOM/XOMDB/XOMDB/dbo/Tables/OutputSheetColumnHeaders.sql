﻿CREATE TABLE [dbo].[OutputSheetColumnHeaders] (
    [FSKey]                 INT            NOT NULL,
    [ColumnNo]              INT            NOT NULL,
    [RowNo]                 TINYINT        NOT NULL,
    [CellText]              NVARCHAR (255) NOT NULL,
    [CenterAcrossSelection] BIT            CONSTRAINT [DF_OutputSheetColumnHeaders_CenterAcrossSelection] DEFAULT ((0)) NOT NULL,
    [LeftBorderLineStyle]   INT            NOT NULL,
    [LeftBorderWeight]      INT            NOT NULL,
    [RightBorderLineStyle]  INT            NOT NULL,
    [RightBorderWeight]     INT            NOT NULL,
    [TopBorderLineStyle]    INT            NOT NULL,
    [TopBorderWeight]       INT            NOT NULL,
    [BottomBorderLineStyle] INT            NOT NULL,
    [BottomBorderWeight]    INT            NOT NULL,
    [tsModified]            SMALLDATETIME  DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OutputSheetColumnHeaders] PRIMARY KEY CLUSTERED ([FSKey] ASC, [ColumnNo] ASC, [RowNo] ASC)
);


GO

CREATE TRIGGER [dbo].[SetOutputSheetColumnHeadersModifiedAfterInsert]
   ON  [dbo].[OutputSheetColumnHeaders]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE OutputSheetColumnHeaders
	SET tsModified = getdate()
	FROM dbo.OutputSheetColumnHeaders OutputSheetColumnHeaders
	INNER JOIN inserted i ON i.FSKey = OutputSheetColumnHeaders.FSKey AND i.ColumnNo = OutputSheetColumnHeaders.ColumnNo AND i.RowNo = OutputSheetColumnHeaders.RowNo
	
END


GO

CREATE TRIGGER [dbo].[SetOutputSheetColumnHeadersModifiedAfterUpdate]
   ON  [dbo].[OutputSheetColumnHeaders]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT FSKey, ColumnNo, RowNo INTO #keychanged 
	FROM inserted 
	WHERE NOT EXISTS (SELECT * FROM deleted WHERE deleted.FSKey = inserted.FSKey AND deleted.ColumnNo = inserted.ColumnNo AND deleted.RowNo = inserted.RowNo)

	UPDATE OutputSheetColumnHeaders
	SET tsModified = getdate()
	FROM dbo.OutputSheetColumnHeaders OutputSheetColumnHeaders
	INNER JOIN deleted d ON d.FSKey = OutputSheetColumnHeaders.FSKey AND d.ColumnNo = OutputSheetColumnHeaders.ColumnNo AND d.RowNo = OutputSheetColumnHeaders.RowNo
	WHERE d.CellText <> OutputSheetColumnHeaders.CellText
	OR d.CenterAcrossSelection <> OutputSheetColumnHeaders.CenterAcrossSelection
	OR d.LeftBorderLineStyle <> OutputSheetColumnHeaders.LeftBorderLineStyle
	OR d.LeftBorderWeight <> OutputSheetColumnHeaders.LeftBorderWeight
	OR d.RightBorderLineStyle <> OutputSheetColumnHeaders.RightBorderLineStyle
	OR d.RightBorderWeight <> OutputSheetColumnHeaders.RightBorderWeight
	OR d.TopBorderLineStyle <> OutputSheetColumnHeaders.TopBorderLineStyle
	OR d.TopBorderWeight <> OutputSheetColumnHeaders.TopBorderWeight
	OR d.BottomBorderLineStyle <> OutputSheetColumnHeaders.BottomBorderLineStyle
	OR d.BottomBorderWeight <> OutputSheetColumnHeaders.BottomBorderWeight
	
	UPDATE OutputSheetColumnHeaders
	SET tsModified = getdate()
	FROM dbo.OutputSheetColumnHeaders OutputSheetColumnHeaders
	INNER JOIN #keychanged d ON d.FSKey = OutputSheetColumnHeaders.FSKey AND d.ColumnNo = OutputSheetColumnHeaders.ColumnNo AND d.RowNo = OutputSheetColumnHeaders.RowNo
	
	DROP TABLE #keychanged
END

