﻿CREATE TABLE [dbo].[OutputCurrencies] (
    [CurrencyCode] VARCHAR (4)   NOT NULL,
    [CurrencyDesc] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_OutputCurrencies] PRIMARY KEY CLUSTERED ([CurrencyCode] ASC)
);

