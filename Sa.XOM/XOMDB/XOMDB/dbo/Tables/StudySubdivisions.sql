﻿CREATE TABLE [dbo].[StudySubdivisions] (
    [Study]       VARCHAR (10) NOT NULL,
    [SubDivision] VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_StudySubdivisions] PRIMARY KEY CLUSTERED ([Study] ASC, [SubDivision] ASC)
);

