﻿CREATE TABLE [dbo].[StudySubdivisionCurrencies] (
    [Study]       VARCHAR (10) NOT NULL,
    [SubDivision] VARCHAR (10) NOT NULL,
    [Currency]    VARCHAR (4)  NOT NULL,
    CONSTRAINT [PK_StudySubdivisionCurrencies] PRIMARY KEY CLUSTERED ([Study] ASC, [SubDivision] ASC, [Currency] ASC),
    CONSTRAINT [FK_StudySubdivisionCurrencies_Subdivisions] FOREIGN KEY ([Study], [SubDivision]) REFERENCES [dbo].[StudySubdivisions] ([Study], [SubDivision])
);

