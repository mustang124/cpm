﻿CREATE TABLE [dbo].[ProcessPeerGroupSections] (
    [Study]       VARCHAR (10)  NOT NULL,
    [RegionCode]  VARCHAR (5)   NOT NULL,
    [MajorGroup]  VARCHAR (50)  NOT NULL,
    [ProcessID]   VARCHAR (8)   NOT NULL,
    [ProcessType] VARCHAR (10)  NOT NULL,
    [Class]       VARCHAR (50)  NULL,
    [SectionCode] VARCHAR (255) NOT NULL,
    [tsModified]  SMALLDATETIME DEFAULT (getdate()) NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_ProcessPeerGroupSections]
    ON [dbo].[ProcessPeerGroupSections]([Study] ASC, [RegionCode] ASC, [MajorGroup] ASC, [ProcessID] ASC, [ProcessType] ASC, [Class] ASC, [SectionCode] ASC);


GO
CREATE TRIGGER [dbo].[SetProcessPeerGroupSectionsModifiedAfterInsert]
   ON  [dbo].[ProcessPeerGroupSections]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessPeerGroupSections
	SET tsModified = getdate()
	FROM dbo.ProcessPeerGroupSections ProcessPeerGroupSections
	INNER JOIN inserted i ON i.Study = ProcessPeerGroupSections.Study 
	AND i.RegionCode = ProcessPeerGroupSections.RegionCode AND i.MajorGroup = ProcessPeerGroupSections.MajorGroup 
	AND i.ProcessID = ProcessPeerGroupSections.ProcessID AND i.ProcessType = ProcessPeerGroupSections.ProcessType
	AND ((i.Class IS NULL AND ProcessPeerGroupSections.Class IS NULL) OR (i.Class = ProcessPeerGroupSections.Class))
	AND i.SectionCode = ProcessPeerGroupSections.SectionCode
	
END



GO


CREATE TRIGGER [dbo].[SetProcessPeerGroupSectionsModifiedAfterUpdate]
   ON  [dbo].[ProcessPeerGroupSections]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT Study, RegionCode, MajorGroup, ProcessID, ProcessType, Class, SectionCode INTO #keychanged 
	FROM inserted 
	WHERE NOT EXISTS (
		SELECT * FROM deleted 
		WHERE deleted.Study = inserted.Study 
			AND deleted.RegionCode = inserted.RegionCode AND deleted.MajorGroup = inserted.MajorGroup 
			AND deleted.ProcessID = inserted.ProcessID AND deleted.ProcessType = inserted.ProcessType
			AND ((deleted.Class IS NULL AND inserted.Class IS NULL) OR (deleted.Class = inserted.Class))
			AND deleted.SectionCode = inserted.SectionCode
	)

	UPDATE ProcessPeerGroupSections
	SET tsModified = getdate()
	FROM dbo.ProcessPeerGroupSections ProcessPeerGroupSections
	INNER JOIN deleted d ON d.Study = ProcessPeerGroupSections.Study 
	AND d.RegionCode = ProcessPeerGroupSections.RegionCode AND d.MajorGroup = ProcessPeerGroupSections.MajorGroup 
	AND d.ProcessID = ProcessPeerGroupSections.ProcessID AND d.ProcessType = ProcessPeerGroupSections.ProcessType
	AND ((d.Class IS NULL AND ProcessPeerGroupSections.Class IS NULL) OR (d.Class = ProcessPeerGroupSections.Class))
	AND d.SectionCode = ProcessPeerGroupSections.SectionCode

	UPDATE ProcessPeerGroupSections
	SET tsModified = getdate()
	FROM dbo.ProcessPeerGroupSections ProcessPeerGroupSections
	INNER JOIN #keychanged d ON d.Study = ProcessPeerGroupSections.Study 
	AND d.RegionCode = ProcessPeerGroupSections.RegionCode AND d.MajorGroup = ProcessPeerGroupSections.MajorGroup 
	AND d.ProcessID = ProcessPeerGroupSections.ProcessID AND d.ProcessType = ProcessPeerGroupSections.ProcessType
	AND ((d.Class IS NULL AND ProcessPeerGroupSections.Class IS NULL) OR (d.Class = ProcessPeerGroupSections.Class))
	AND d.SectionCode = ProcessPeerGroupSections.SectionCode
		
	DROP TABLE #keychanged
END


