﻿CREATE TABLE [dbo].[UnitsOfMeasure] (
    [UOMCode]        VARCHAR (20)  NOT NULL,
    [UOMDescription] VARCHAR (100) NOT NULL,
    [IsMetric]       BIT           CONSTRAINT [DF_UnitsOfMeasure_IsMetric] DEFAULT ((0)) NOT NULL,
    [tsModified]     DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_UnitsOfMeasure] PRIMARY KEY CLUSTERED ([UOMCode] ASC)
);


GO

CREATE TRIGGER [dbo].[SetUnitsOfMeasureModifiedAfterInsert]
   ON  [dbo].[UnitsOfMeasure]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE UnitsOfMeasure
	SET tsModified = getdate()
	FROM dbo.UnitsOfMeasure UnitsOfMeasure
	INNER JOIN inserted i ON i.UOMCode = UnitsOfMeasure.UOMCode
	
END


GO

CREATE TRIGGER [dbo].[SetUnitsOfMeasureModifiedAfterUpdate]
   ON  [dbo].[UnitsOfMeasure]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (UOMCode) OR UPDATE (UOMDescription) OR UPDATE(isMetric)
		UPDATE UnitsOfMeasure
		SET tsModified =GETDATE()
		FROM dbo.UnitsOfMeasure UnitsOfMeasure 
		INNER JOIN deleted d ON d.UOMCode = UnitsOfMeasure.UOMCode
	
END

