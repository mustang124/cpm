﻿CREATE TABLE [dbo].[VariableLinkUSMetric] (
    [VariableGeneric] VARCHAR (255) NOT NULL,
    [VKey_USUnits]    INT           NOT NULL,
    [VariableUSUnits] VARCHAR (255) NOT NULL,
    [VKey_Metric]     INT           NOT NULL,
    [VariableMetric]  VARCHAR (255) NOT NULL,
    [tsModified]      DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VariableLinkUSMetric] PRIMARY KEY CLUSTERED ([VariableGeneric] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_VariableLinkUSMetric_VKeyUS]
    ON [dbo].[VariableLinkUSMetric]([VKey_USUnits] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_VariableLinkUSMetric_VKeyMetric]
    ON [dbo].[VariableLinkUSMetric]([VKey_Metric] ASC);


GO

CREATE TRIGGER [dbo].[SetVariableLinkUSMetricModifiedAfterInsert]
   ON  [dbo].[VariableLinkUSMetric]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE VariableLinkUSMetric
	SET tsModified = getdate()
	FROM dbo.VariableLinkUSMetric VariableLinkUSMetric
	INNER JOIN inserted i ON i.VariableGeneric = VariableLinkUSMetric.VariableGeneric
	
END


GO

CREATE TRIGGER [dbo].[SetVariableLinkUSMetricModifiedAfterUpdate]
   ON  [dbo].[VariableLinkUSMetric]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (VariableGeneric) OR UPDATE (VKey_USUnits) OR UPDATE(VariableUSUnits) OR UPDATE(VKey_Metric) OR UPDATE(VariableMetric)
		UPDATE VariableLinkUSMetric
		SET tsModified =GETDATE()
		FROM dbo.VariableLinkUSMetric VariableLinkUSMetric 
		INNER JOIN deleted d ON d.VariableGeneric = VariableLinkUSMetric.VariableGeneric
	
END

