﻿CREATE TABLE [dbo].[ProcessPeers] (
    [ProcessPeerKey] INT      IDENTITY (1, 1) NOT NULL,
    [FacilityPPGKey] INT      NOT NULL,
    [TargetPPGKey]   INT      NOT NULL,
    [IsRAMPeer]      BIT      CONSTRAINT [DF_ProcessPeers_IsRAMPeer] DEFAULT ((0)) NOT NULL,
    [tsModified]     DATETIME CONSTRAINT [DF_RamPeers_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RamPeers] PRIMARY KEY CLUSTERED ([ProcessPeerKey] ASC),
    CONSTRAINT [FK_ProcessPeersFacility_ProcessPeerGroupDef] FOREIGN KEY ([FacilityPPGKey]) REFERENCES [dbo].[ProcessPeerGroupDef] ([PPGKey]),
    CONSTRAINT [FK_ProcessPeersTarget_ProcessPeerGroupDef] FOREIGN KEY ([TargetPPGKey]) REFERENCES [dbo].[ProcessPeerGroupDef] ([PPGKey]),
    CONSTRAINT [IX_UniqueRamPeers] UNIQUE NONCLUSTERED ([FacilityPPGKey] ASC, [TargetPPGKey] ASC)
);


GO


CREATE TRIGGER [dbo].[SetProcessPeersModifiedAfterInsert]
   ON  [dbo].[ProcessPeers]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessPeers
	SET tsModified = getdate()
	FROM dbo.ProcessPeers ProcessPeers
	INNER JOIN inserted i ON i.ProcessPeerKey = ProcessPeers.ProcessPeerKey
	
END


GO


CREATE TRIGGER [dbo].[SetProcessPeersModifiedAfterUpdate]
   ON  [dbo].[ProcessPeers]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessPeers
	SET tsModified = getdate()
	FROM dbo.ProcessPeers ProcessPeers
	INNER JOIN deleted d ON d.ProcessPeerKey = ProcessPeers.ProcessPeerKey
	WHERE	d.FacilityPPGKey <> ProcessPeers.FacilityPPGKey OR d.TargetPPGKey <> ProcessPeers.TargetPPGKey 
		OR d.IsRAMPeer <> ProcessPeers.IsRAMPeer
	
END

