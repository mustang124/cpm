﻿CREATE TABLE [dbo].[PeerGroupHierarchyItems] (
    [PGHItemKey]   INT          IDENTITY (1, 1) NOT NULL,
    [PGHKey]       INT          NOT NULL,
    [PGHItemOrder] SMALLINT     NOT NULL,
    [RegionCode]   VARCHAR (5)  NOT NULL,
    [MajorGroup]   VARCHAR (50) NOT NULL,
    [SubGroup]     VARCHAR (50) NOT NULL,
    [tsModified]   DATETIME     DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_PeerGroupHierarchyItems] PRIMARY KEY CLUSTERED ([PGHItemKey] ASC),
    CONSTRAINT [FK_PeerGroupHierarchyItems_PeerGroupHierarchyList] FOREIGN KEY ([PGHKey]) REFERENCES [dbo].[PeerGroupHierarchyList] ([PGHKey])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PeerGroupHierarchyItems_KeyOrder]
    ON [dbo].[PeerGroupHierarchyItems]([PGHKey] ASC, [PGHItemOrder] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PeerGroupHierarchyItemKeyDef]
    ON [dbo].[PeerGroupHierarchyItems]([RegionCode] ASC, [MajorGroup] ASC, [SubGroup] ASC);


GO

CREATE TRIGGER [dbo].[SetPeerGroupHierarchyItemsModifiedAfterInsert]
   ON  [dbo].[PeerGroupHierarchyItems]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupHierarchyItems
	SET tsModified = getdate()
	FROM dbo.PeerGroupHierarchyItems PeerGroupHierarchyItems
	INNER JOIN inserted i ON i.PGHItemKey = PeerGroupHierarchyItems.PGHItemKey
	
END


GO

CREATE TRIGGER [dbo].[SetPeerGroupHierarchyItemsModifiedAfterUpdate]
   ON  [dbo].[PeerGroupHierarchyItems]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (PGHKey) OR UPDATE (PGHItemOrder)  OR UPDATE (RegionCode)  OR UPDATE (MajorGroup)  OR UPDATE (SubGroup) 
		UPDATE PeerGroupHierarchyItems
		SET tsModified =GETDATE()
		FROM dbo.PeerGroupHierarchyItems PeerGroupHierarchyItems 
		INNER JOIN deleted d ON d.PGHItemKey = PeerGroupHierarchyItems.PGHItemKey
	
END

