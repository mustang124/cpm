﻿CREATE TABLE [dbo].[LubePAProductSlateDetails] (
    [PGKey]                INT            NOT NULL,
    [ReportOrder]          INT            NOT NULL,
    [LWSCategory]          VARCHAR (4)    NOT NULL,
    [MaterialID]           VARCHAR (5)    NOT NULL,
    [MaterialName]         VARCHAR (60)   NOT NULL,
    [Bbl]                  INT            NULL,
    [PcntLWSBbl]           NUMERIC (6, 2) NULL,
    [PricePerBbl]          NUMERIC (6, 2) NULL,
    [RMC]                  NUMERIC (6, 2) NULL,
    [GrossMargin]          NUMERIC (6, 2) NULL,
    [NWE_150N_GrossMargin] NUMERIC (6, 2) NULL,
    [GrossMarginOver150N]  NUMERIC (6, 2) NULL,
    [ProdMixVariance]      NUMERIC (6, 2) NULL,
    [tsModified]           DATETIME       CONSTRAINT [DF_LubePAProductSlateDetails_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]           DATETIME       CONSTRAINT [DF_LubePAProductSlateDetails_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LubePAProductSlateDetails] PRIMARY KEY CLUSTERED ([PGKey] ASC, [LWSCategory] ASC, [MaterialID] ASC)
);

