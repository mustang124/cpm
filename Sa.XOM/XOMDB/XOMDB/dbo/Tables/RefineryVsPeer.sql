﻿CREATE TABLE [dbo].[RefineryVsPeer] (
    [RefineryPeerKey] INT         NOT NULL,
    [VKey]            INT         NOT NULL,
    [Currency]        VARCHAR (4) DEFAULT ('USD') NOT NULL,
    [numberValue]     REAL        NULL,
    [tsModified]      DATETIME    CONSTRAINT [DF_RefineryGaps_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]      DATETIME    CONSTRAINT [DF_RefineryGaps_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RefineryGaps] PRIMARY KEY CLUSTERED ([RefineryPeerKey] ASC, [VKey] ASC, [Currency] ASC),
    CONSTRAINT [FK_RefineryGapsPeerKey_RefineryPeerKey] FOREIGN KEY ([RefineryPeerKey]) REFERENCES [dbo].[RefineryPeers] ([RefineryPeerKey]),
    CONSTRAINT [FK_RefineryGapsVKey_VarDefVKey] FOREIGN KEY ([VKey]) REFERENCES [dbo].[VarDef] ([VKey])
);


GO



CREATE TRIGGER [dbo].[SetRefineryVsPeerModifiedAfterInsert]
   ON  [dbo].[RefineryVsPeer]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE RefineryVsPeer
	SET tsModified = getdate(), tsVerified = GETDATE()
	FROM dbo.RefineryVsPeer RefineryVsPeer
	INNER JOIN inserted i ON i.RefineryPeerKey = RefineryVsPeer.RefineryPeerKey
	
END



GO



CREATE TRIGGER [dbo].[SetRefineryVsPeerModifiedAfterUpdate]
   ON  [dbo].[RefineryVsPeer]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (RefineryPeerKey) OR UPDATE (VKey) OR UPDATE(Currency) OR UPDATE(numberValue)
		UPDATE RefineryVsPeer
		SET tsVerified = getdate()
		, tsModified = CASE 
			WHEN (d.numberValue IS NULL AND RefineryVsPeer.numberValue IS NOT NULL) OR (d.numberValue IS NOT NULL AND RefineryVsPeer.numberValue IS NULL) THEN GETDATE()
			WHEN (d.numberValue IS NOT NULL AND RefineryVsPeer.numberValue IS NOT NULL) AND ROUND(d.numberValue, v.Decimals+2) <> ROUND(RefineryVsPeer.numberValue, v.Decimals+2) THEN GETDATE()
			ELSE d.tsModified END
		FROM dbo.RefineryVsPeer RefineryVsPeer INNER JOIN dbo.VarDef v ON v.VKey = RefineryVsPeer.VKey
		INNER JOIN deleted d ON d.RefineryPeerKey = RefineryVsPeer.RefineryPeerKey AND d.VKey = RefineryVsPeer.VKey AND d.Currency = RefineryVsPeer.Currency
		CROSS APPLY (VALUES (CASE WHEN ROUND(d.numberValue, v.Decimals+2) <> ROUND(RefineryVsPeer.numberValue, v.Decimals+2) OR (d.numberValue IS NULL AND RefineryVsPeer.numberValue IS NOT NULL) OR (d.numberValue IS NOT NULL AND RefineryVsPeer.numberValue IS NULL) THEN 1 ELSE 0 END)) chk(Changed)
	
END


