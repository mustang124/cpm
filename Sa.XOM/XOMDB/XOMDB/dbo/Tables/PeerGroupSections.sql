﻿CREATE TABLE [dbo].[PeerGroupSections] (
    [Study]       VARCHAR (10)  NOT NULL,
    [RegionCode]  VARCHAR (5)   NOT NULL,
    [MajorGroup]  VARCHAR (50)  NOT NULL,
    [SectionCode] VARCHAR (255) NOT NULL,
    [tsModified]  SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PeerGroupSections] PRIMARY KEY CLUSTERED ([Study] ASC, [RegionCode] ASC, [MajorGroup] ASC, [SectionCode] ASC)
);


GO
CREATE TRIGGER [dbo].[SetPeerGroupSectionsModifiedAfterInsert]
   ON  [dbo].[PeerGroupSections]
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupSections
	SET tsModified = getdate()
	FROM dbo.PeerGroupSections PeerGroupSections
	INNER JOIN inserted i ON i.Study = PeerGroupSections.Study AND i.RegionCode = PeerGroupSections.RegionCode AND i.MajorGroup = PeerGroupSections.MajorGroup AND i.SectionCode = PeerGroupSections.SectionCode
	
END



GO


CREATE TRIGGER [dbo].[SetPeerGroupSectionsModifiedAfterUpdate]
   ON  [dbo].[PeerGroupSections]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT Study, RegionCode, MajorGroup, SectionCode INTO #keychanged 
	FROM inserted 
	WHERE NOT EXISTS (SELECT * FROM deleted WHERE deleted.Study = inserted.Study AND deleted.RegionCode = inserted.RegionCode AND deleted.MajorGroup = inserted.MajorGroup AND deleted.SectionCode = inserted.SectionCode)

	UPDATE PeerGroupSections
	SET tsModified = getdate()
	FROM dbo.PeerGroupSections PeerGroupSections
	INNER JOIN deleted d ON d.Study = PeerGroupSections.Study AND d.RegionCode = PeerGroupSections.RegionCode AND d.MajorGroup = PeerGroupSections.MajorGroup AND d.SectionCode = PeerGroupSections.SectionCode

	UPDATE PeerGroupSections
	SET tsModified = getdate()
	FROM dbo.PeerGroupSections PeerGroupSections
	INNER JOIN #keychanged d ON d.Study = PeerGroupSections.Study AND d.RegionCode = PeerGroupSections.RegionCode AND d.MajorGroup = PeerGroupSections.MajorGroup AND d.SectionCode = PeerGroupSections.SectionCode
	
	DROP TABLE #keychanged
END


