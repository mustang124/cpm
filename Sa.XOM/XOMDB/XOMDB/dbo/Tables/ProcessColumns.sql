﻿CREATE TABLE [dbo].[ProcessColumns] (
    [SheetName]   VARCHAR (25) NOT NULL,
    [SheetCol]    INT          NOT NULL,
    [StudyRefnum] VARCHAR (25) NOT NULL,
    [StudyUnitID] VARCHAR (25) NOT NULL
);

