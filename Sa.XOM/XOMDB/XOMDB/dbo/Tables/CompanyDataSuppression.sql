﻿CREATE TABLE [dbo].[CompanyDataSuppression] (
    [CompanyID]   VARCHAR (10)  NOT NULL,
    [SectionCode] VARCHAR (255) NULL,
    [VKey]        INT           NULL,
    [tsModified]  DATETIME      CONSTRAINT [DF_CompanyDataSuppression_tsModified] DEFAULT (getdate()) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CompanyDataSuppression]
    ON [dbo].[CompanyDataSuppression]([CompanyID] ASC);

