﻿CREATE TABLE [dbo].[StudyYearCurrencies] (
    [StudyYearID] INT         NOT NULL,
    [Currency]    VARCHAR (4) NOT NULL,
    CONSTRAINT [PK_StudyYearCurrencies] PRIMARY KEY CLUSTERED ([StudyYearID] ASC, [Currency] ASC),
    CONSTRAINT [FK_StudyYearCurrencies_StudyYears] FOREIGN KEY ([StudyYearID]) REFERENCES [dbo].[StudyYears] ([StudyYearID])
);

