﻿CREATE TABLE [dbo].[RefineryData] (
    [PGKey]       INT            NOT NULL,
    [VKey]        INT            NOT NULL,
    [Currency]    VARCHAR (4)    NOT NULL,
    [numberValue] FLOAT (53)     NULL,
    [textValue]   NVARCHAR (MAX) NULL,
    [dateValue]   SMALLDATETIME  NULL,
    [tsModified]  DATETIME       CONSTRAINT [DF_RefineryData_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]  DATETIME       CONSTRAINT [DF_RefineryData_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RefineryData] PRIMARY KEY CLUSTERED ([PGKey] ASC, [VKey] ASC, [Currency] ASC),
    CONSTRAINT [FK_RefineryData_PeerGroupDef] FOREIGN KEY ([PGKey]) REFERENCES [dbo].[PeerGroupDef] ([PGKey]),
    CONSTRAINT [FK_RefineryData_VarDef] FOREIGN KEY ([VKey]) REFERENCES [dbo].[VarDef] ([VKey])
);


GO

CREATE TRIGGER [dbo].[SetRefineryDataModifiedAfterUpdate]
   ON  [dbo].[RefineryData]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (PGKey) OR UPDATE (VKey) OR UPDATE(Currency) OR UPDATE(numberValue) OR UPDATE(textValue) OR UPDATE(dateValue)
		UPDATE RefineryData
		SET tsVerified = getdate()
		, tsModified = CASE 
			WHEN (d.numberValue IS NULL AND RefineryData.numberValue IS NOT NULL) OR (d.numberValue IS NOT NULL AND RefineryData.numberValue IS NULL) THEN GETDATE()
			WHEN (d.numberValue IS NOT NULL AND RefineryData.numberValue IS NOT NULL) AND ROUND(d.numberValue, v.Decimals+2) <> ROUND(RefineryData.numberValue, v.Decimals+2) THEN GETDATE()
			WHEN (d.textValue IS NULL AND RefineryData.textValue IS NOT NULL) OR (d.textValue IS NOT NULL AND RefineryData.textValue IS NULL) THEN GETDATE()
			WHEN (d.textValue IS NOT NULL AND RefineryData.textValue IS NOT NULL) AND d.textValue <> RefineryData.textValue THEN GETDATE()
			WHEN (d.dateValue IS NULL AND RefineryData.dateValue IS NOT NULL) OR (d.dateValue IS NOT NULL AND RefineryData.dateValue IS NULL) THEN GETDATE()
			WHEN (d.dateValue IS NOT NULL AND RefineryData.dateValue IS NOT NULL) AND d.dateValue <> RefineryData.dateValue THEN GETDATE()
			ELSE d.tsModified END
		FROM dbo.RefineryData RefineryData INNER JOIN dbo.VarDef v ON v.VKey = RefineryData.VKey
		INNER JOIN deleted d ON d.PGKey = RefineryData.PGKey AND d.VKey = RefineryData.VKey AND d.Currency = RefineryData.Currency

END

GO

CREATE TRIGGER dbo.SetRefineryDataModifiedAfterInsert
   ON  dbo.RefineryData
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE RefineryData
	SET tsModified = getdate(), tsVerified = getdate()
	FROM dbo.RefineryData RefineryData
	INNER JOIN inserted i ON i.PGKey = RefineryData.PGKey AND i.VKey = RefineryData.VKey AND i.Currency = RefineryData.Currency
	
END
