﻿CREATE TABLE [dbo].[CTMaterialReports] (
    [PGKey]                  INT             NOT NULL,
    [ReportType]             VARCHAR (15)    NOT NULL,
    [MaterialKey]            INT             NOT NULL,
    [MaterialCategory]       VARCHAR (5)     NOT NULL,
    [MaterialCode]           VARCHAR (5)     NOT NULL,
    [MaterialName]           NVARCHAR (60)   NOT NULL,
    [Barrels]                INT             NULL,
    [MetricTons]             INT             NULL,
    [PricePerBbl]            NUMERIC (10, 2) NULL,
    [PricePerMT]             NUMERIC (10, 2) NULL,
    [ValueMUSD]              NUMERIC (10, 1) NULL,
    [InSensibleHeat]         VARCHAR (1)     NULL,
    [IncludeVolumeInMargins] VARCHAR (1)     NULL,
    [CTFCategory]            VARCHAR (6)     NULL,
    [tsModified]             DATETIME        CONSTRAINT [DF_CTMaterialReports_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]             DATETIME        CONSTRAINT [DF_CTMaterialReports_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTMaterialReports] PRIMARY KEY CLUSTERED ([PGKey] ASC, [ReportType] ASC, [MaterialKey] ASC)
);

