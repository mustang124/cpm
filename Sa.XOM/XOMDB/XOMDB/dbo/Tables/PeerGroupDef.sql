﻿CREATE TABLE [dbo].[PeerGroupDef] (
    [PGKey]           INT            IDENTITY (1, 1) NOT NULL,
    [Study]           VARCHAR (10)   NOT NULL,
    [StudyYear]       SMALLINT       NOT NULL,
    [Methodology]     VARCHAR (10)   NOT NULL,
    [RegionCode]      VARCHAR (5)    NOT NULL,
    [MajorGroup]      VARCHAR (50)   NOT NULL,
    [SubGroup]        VARCHAR (50)   NOT NULL,
    [PricingScenario] VARCHAR (50)   NOT NULL,
    [LongText]        NVARCHAR (MAX) NOT NULL,
    [tsModified]      DATETIME       CONSTRAINT [DF_PeerGroupDef_tsModified] DEFAULT (getdate()) NOT NULL,
    [StudyRefnum]     VARCHAR (25)   NOT NULL,
    [SourceDatabase]  VARCHAR (25)   NOT NULL,
    [PublishEuro]     BIT            DEFAULT ((0)) NOT NULL,
    [PGType]          VARCHAR (10)   DEFAULT ('Undefined') NOT NULL,
    CONSTRAINT [PK_PeerGroupDef] PRIMARY KEY CLUSTERED ([PGKey] ASC),
    CONSTRAINT [FK_PeerGroupDef_Regions] FOREIGN KEY ([RegionCode]) REFERENCES [dbo].[Regions] ([RegionCode])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PeerGroupDef_Unique]
    ON [dbo].[PeerGroupDef]([Study] ASC, [StudyYear] ASC, [Methodology] ASC, [RegionCode] ASC, [MajorGroup] ASC, [SubGroup] ASC, [PricingScenario] ASC);


GO
CREATE TRIGGER [dbo].[SetPeerGroupDefModifiedAfterUpdate]
   ON  dbo.PeerGroupDef
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupDef
	SET tsModified = getdate()
	FROM dbo.PeerGroupDef PeerGroupDef
	INNER JOIN deleted d ON d.PGKey = PeerGroupDef.PGKey
	WHERE d.Study <> PeerGroupDef.Study
	OR d.StudyYear <> PeerGroupDef.StudyYear
	OR d.Methodology <> PeerGroupDef.Methodology
	OR d.RegionCode <> PeerGroupDef.RegionCode
	OR d.MajorGroup <> PeerGroupDef.MajorGroup
	OR d.SubGroup <> PeerGroupDef.Subgroup
	OR d.PricingScenario <> PeerGroupDef.PricingScenario
	OR d.LongText <> PeerGroupDef.LongText
	OR d.StudyRefnum <> PeerGroupDef.StudyRefnum
	
END

GO
CREATE TRIGGER dbo.SetPeerGroupDefModifiedAfterInsert
   ON  dbo.PeerGroupDef
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE PeerGroupDef
	SET tsModified = getdate()
	FROM dbo.PeerGroupDef PeerGroupDef
	INNER JOIN inserted i ON i.PGKey = PeerGroupDef.PGKey
	
END
