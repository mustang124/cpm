﻿CREATE TABLE [dbo].[CompanyPGKeys] (
    [CompanyKey] SMALLINT NOT NULL,
    [PGKey]      INT      NOT NULL,
    [tsAdded]    DATETIME CONSTRAINT [DF_CompanyPGKeys_tsAdded] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CompanyPGKeys] PRIMARY KEY CLUSTERED ([CompanyKey] ASC, [PGKey] ASC)
);

