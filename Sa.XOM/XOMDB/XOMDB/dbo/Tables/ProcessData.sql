﻿CREATE TABLE [dbo].[ProcessData] (
    [PPGKey]      INT            NOT NULL,
    [VKey]        INT            NOT NULL,
    [Currency]    VARCHAR (4)    NOT NULL,
    [numberValue] FLOAT (53)     NULL,
    [textValue]   NVARCHAR (MAX) NULL,
    [dateValue]   SMALLDATETIME  NULL,
    [tsModified]  DATETIME       CONSTRAINT [DF_ProcessData_tsModified] DEFAULT (getdate()) NOT NULL,
    [tsVerified]  DATETIME       CONSTRAINT [DF_ProcessData_tsVerified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ProcessData] PRIMARY KEY CLUSTERED ([PPGKey] ASC, [VKey] ASC, [Currency] ASC),
    CONSTRAINT [FK_ProcessData_ProcessPeerGroupDef] FOREIGN KEY ([PPGKey]) REFERENCES [dbo].[ProcessPeerGroupDef] ([PPGKey]),
    CONSTRAINT [FK_ProcessData_VarDef] FOREIGN KEY ([VKey]) REFERENCES [dbo].[VarDef] ([VKey])
);


GO

CREATE TRIGGER [dbo].[SetProcessDataModifiedAfterUpdate]
   ON  [dbo].[ProcessData]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	IF UPDATE (PPGKey) OR UPDATE (VKey) OR UPDATE(Currency) OR UPDATE(numberValue) OR UPDATE(textValue) OR UPDATE(dateValue)
		UPDATE ProcessData
		SET tsVerified = getdate()
		, tsModified = CASE 
			WHEN (d.numberValue IS NULL AND ProcessData.numberValue IS NOT NULL) OR (d.numberValue IS NOT NULL AND ProcessData.numberValue IS NULL) THEN GETDATE()
			WHEN (d.numberValue IS NOT NULL AND ProcessData.numberValue IS NOT NULL) AND ROUND(d.numberValue, v.Decimals+2) <> ROUND(ProcessData.numberValue, v.Decimals+2) THEN GETDATE()
			WHEN (d.textValue IS NULL AND ProcessData.textValue IS NOT NULL) OR (d.textValue IS NOT NULL AND ProcessData.textValue IS NULL) THEN GETDATE()
			WHEN (d.textValue IS NOT NULL AND ProcessData.textValue IS NOT NULL) AND d.textValue <> ProcessData.textValue THEN GETDATE()
			WHEN (d.dateValue IS NULL AND ProcessData.dateValue IS NOT NULL) OR (d.dateValue IS NOT NULL AND ProcessData.dateValue IS NULL) THEN GETDATE()
			WHEN (d.dateValue IS NOT NULL AND ProcessData.dateValue IS NOT NULL) AND d.dateValue <> ProcessData.dateValue THEN GETDATE()
			ELSE d.tsModified END
		FROM dbo.ProcessData ProcessData INNER JOIN dbo.VarDef v ON v.VKey = ProcessData.VKey
		INNER JOIN deleted d ON d.PPGKey = ProcessData.PPGKey AND d.VKey = ProcessData.VKey AND d.Currency = ProcessData.Currency
	
END

GO

CREATE TRIGGER dbo.SetProcessDataModifiedAfterInsert
   ON  dbo.ProcessData
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE ProcessData
	SET tsModified = getdate(), tsVerified = getdate()
	FROM dbo.ProcessData ProcessData
	INNER JOIN inserted i ON i.PPGKey = ProcessData.PPGKey AND i.VKey = ProcessData.VKey AND i.Currency = ProcessData.Currency
	
END
