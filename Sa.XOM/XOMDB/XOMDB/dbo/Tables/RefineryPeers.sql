﻿CREATE TABLE [dbo].[RefineryPeers] (
    [RefineryPeerKey] INT      IDENTITY (1, 1) NOT NULL,
    [FacilityPGKey]   INT      NOT NULL,
    [TargetPGKey]     INT      NOT NULL,
    [IsOpexPeer]      BIT      CONSTRAINT [DF_RefineryPeers_IsOpexPeer] DEFAULT ((0)) NOT NULL,
    [IsMarginPeer]    BIT      CONSTRAINT [DF_RefineryPeers_IsMarginPeer] DEFAULT ((0)) NOT NULL,
    [IsEIIPeer]       BIT      CONSTRAINT [DF_RefineryPeers_IsEIIPeer] DEFAULT ((0)) NOT NULL,
    [IsCEIPeer]       BIT      CONSTRAINT [DF_RefineryPeers_IsCEIPeer] DEFAULT ((0)) NOT NULL,
    [tsModified]      DATETIME CONSTRAINT [DF_RefineryPeers_tsModified] DEFAULT (getdate()) NOT NULL,
    [IsLubesPeer]     BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_RefineryPeers] PRIMARY KEY CLUSTERED ([RefineryPeerKey] ASC),
    CONSTRAINT [FK_RefineryPeersFacility_PeerGroupDef] FOREIGN KEY ([FacilityPGKey]) REFERENCES [dbo].[PeerGroupDef] ([PGKey]),
    CONSTRAINT [FK_RefineryPeersTarget_PeerGroupDef] FOREIGN KEY ([TargetPGKey]) REFERENCES [dbo].[PeerGroupDef] ([PGKey]),
    CONSTRAINT [IX_UniqueRefineryPeers] UNIQUE NONCLUSTERED ([FacilityPGKey] ASC, [TargetPGKey] ASC)
);


GO
CREATE TRIGGER [dbo].[SetRefineryPeersModifiedAfterUpdate]
   ON  [dbo].[RefineryPeers]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE RefineryPeers
	SET tsModified = getdate()
	FROM dbo.RefineryPeers RefineryPeers
	INNER JOIN deleted d ON d.RefineryPeerKey = RefineryPeers.RefineryPeerKey
	WHERE	d.FacilityPGKey <> RefineryPeers.FacilityPGKey OR d.TargetPGKey <> RefineryPeers.TargetPGKey 
		OR d.IsOpexPeer <> RefineryPeers.IsOpexPeer OR d.IsMarginPeer <> RefineryPeers.IsMarginPeer OR d.IsLubesPeer <> RefineryPeers.IsLubesPeer
		OR d.IsCEIPeer <> RefineryPeers.IsCEIPeer OR d.IsEIIPeer <> RefineryPeers.IsEIIPeer
	
END

GO
CREATE TRIGGER [dbo].[SetRefineryPeersModifiedAfterInsert]
   ON  dbo.RefineryPeers
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	UPDATE RefineryPeers
	SET tsModified = getdate()
	FROM dbo.RefineryPeers RefineryPeers
	INNER JOIN inserted i ON i.RefineryPeerKey = RefineryPeers.RefineryPeerKey
	
END
