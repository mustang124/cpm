﻿CREATE TABLE [dbo].[Companies] (
    [CompanyID]         VARCHAR (10)  NOT NULL,
    [DataPullStartDate] SMALLDATETIME NULL,
    [CompanyKey]        SMALLINT      IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED ([CompanyID] ASC)
);

