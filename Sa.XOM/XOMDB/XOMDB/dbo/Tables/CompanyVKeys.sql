﻿CREATE TABLE [dbo].[CompanyVKeys] (
    [CompanyKey]   SMALLINT NOT NULL,
    [VKey]         INT      NOT NULL,
    [tsAdded]      DATETIME CONSTRAINT [DF_CompanyVKeys_tsAdded] DEFAULT (getdate()) NOT NULL,
    [SuppressData] BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CompanyVKeys] PRIMARY KEY CLUSTERED ([CompanyKey] ASC, [VKey] ASC)
);

