﻿

CREATE VIEW [dbo].[OutputFileSheetCells]
AS
SELECT s.FSKey, f.FKey, f.Filename, s.SheetName, r.RowNo, c.ColumnNo, r.RowText, r.VKey, r.DBTableName, r.DBColumnName, r.DBFilter, r.SectionCode, c.PGKey, c.PPGKey, c.Refnum, c.UnitID, r.NumberFormat, s.SheetNo, f.Study, f.StudyYear, f.FileType, f.Currency, r.Hidden AS RowHidden
FROM dbo.OutputFiles f 
	INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
	INNER JOIN dbo.OutputSheetRows r ON r.FSKey = s.FSKey
	INNER JOIN dbo.OutputSheetColumns c ON c.FSKey = s.FSKey
	


