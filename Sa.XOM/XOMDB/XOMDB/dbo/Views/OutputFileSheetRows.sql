﻿

CREATE VIEW [dbo].[OutputFileSheetRows]
AS
SELECT s.FSKey, f.FKey, f.Filename, s.SheetName, r.RowNo, r.RowText, r.VKey, r.DBTableName, r.DBColumnName, r.DBFilter, r.SectionCode, s.SheetNo, f.Study, f.StudyYear, f.FileType, f.Currency, r.Hidden AS RowHidden, r.tsModified
FROM dbo.OutputFiles f 
	INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
	INNER JOIN dbo.OutputSheetRows r ON r.FSKey = s.FSKey
	


