﻿
CREATE VIEW [dbo].[CompanyStudyProcesses]  AS 
SELECT c.CompanyID, r.Study, r.StudyYear, u.ProcessID, tsAdded = CASE WHEN MIN(c.tsModified) > MIN(s.tsModified) THEN MIN(c.tsModified) ELSE MIN(s.tsModified) END
FROM PeerGroupDef r INNER JOIN ProcessPeerGroupDef u ON u.PGKey = r.PGKey
INNER JOIN CompanyRefnums c ON c.Refnum = r.StudyRefnum 
INNER JOIN CompanyStudies s ON s.CompanyID = c.CompanyID AND s.Study = r.Study AND s.StudyYear = r.StudyYear
GROUP BY c.CompanyID, r.Study, r.StudyYear, u.ProcessID
UNION /* For EXAMPLE */
SELECT DISTINCT s.CompanyID, s.Study, s.StudyYear, u.ProcessID, tsAdded = s.tsModified
FROM CompanyStudies s CROSS JOIN (VALUES ('CDU'),('VAC'),('COK')) u(ProcessID)
WHERE s.CompanyID = 'EXAMPLE' 




