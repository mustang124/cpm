﻿CREATE VIEW dbo.OutputFileSheets 
AS
SELECT s.FSKey, f.FKey, f.Filename, s.SheetName, s.SheetNo, f.Study, f.StudyYear, f.FileType, f.Currency
FROM dbo.OutputFiles f 
	INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
	
