﻿
CREATE PROCEDURE dbo.AddOutputSheetKey(@FKey int, @SheetNo int, @SheetName varchar(50))
AS
BEGIN
	SET NOCOUNT ON;

    IF NOT EXISTS (SELECT * FROM dbo.OutputSheets WHERE FKey = @FKey AND SheetNo = @SheetNo AND SheetName = @SheetName)
		INSERT dbo.OutputSheets(FKey, SheetNo, SheetName) VALUES (@FKey, @SheetNo, @SheetName)
	
	SET NOCOUNT OFF
	SELECT * FROM dbo.OutputSheets WHERE FKey = @FKey AND SheetNo = @SheetNo
END
