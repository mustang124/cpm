﻿CREATE PROC spDeletePeerGroup(@Study varchar(10), @StudyYear smallint, @RegionCode varchar(10), @MajorGroup varchar(50), @SubGroup varchar(50))
AS
DELETE FROM RefineryData WHERE PGKey IN (SELECT PGKey FROM PeerGroupDef WHERE Study = @Study AND MajorGroup = @MajorGroup AND SubGroup = @SubGroup AND StudyYear = @StudyYear AND RegionCode = @RegionCode)
DELETE FROM PeerGroupDef WHERE Study = @Study AND MajorGroup = @MajorGroup AND SubGroup = @SubGroup AND StudyYear = @StudyYear AND RegionCode = @RegionCode

