﻿
CREATE PROCEDURE dbo.GetOutputFileKey(@FileName varchar(255))
AS
BEGIN
	SET NOCOUNT ON;

    IF NOT EXISTS (SELECT * FROM dbo.OutputFiles WHERE FileName = @FileName)
		INSERT dbo.OutputFiles(FileName) VALUES (@FileName)
	
	SET NOCOUNT OFF
	SELECT * FROM dbo.OutputFiles WHERE FileName = @FileName
END
