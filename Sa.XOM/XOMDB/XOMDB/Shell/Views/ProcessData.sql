﻿


CREATE VIEW [Shell].[ProcessData]
AS
SELECT d.PPGKey, d.VKey, d.Currency, d.numberValue, d.textValue, d.dateValue, d.tsModified
FROM dbo.Companies c
CROSS APPLY dbo.GetProcessLevelData(c.CompanyID,c.DataPullStartDate) d
WHERE c.CompanyID = 'Shell'


