﻿



CREATE VIEW [EXAMPLE].[VarDef]
AS
SELECT DISTINCT vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode, vd.tsModified
FROM dbo.CompanyOutputFiles cof
INNER JOIN dbo.OutputFileSheetRows r ON r.FKey = cof.FKey
INNER JOIN dbo.VarDef vd ON vd.VKey = r.VKey
WHERE r.VKey IS NOT NULL AND r.RowHidden = 0
AND cof.CompanyID = 'EXAMPLE'
UNION
SELECT vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode, vd.tsModified
FROM dbo.VarDef vd WHERE VKey IN (SELECT DISTINCT VKey FROM dbo.RefineryData WHERE PGKey IN (SELECT PGKey FROM dbo.CompanyRefnums WHERE CompanyID = 'EXAMPLE'))




