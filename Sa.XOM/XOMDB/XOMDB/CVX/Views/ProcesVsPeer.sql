﻿



CREATE VIEW [CVX].[ProcesVsPeer]
AS
SELECT pp.ProcessPeerKey, pvp.VKey, pvp.Currency, pvp.numberValue, pvp.tsModified, pvp.tsVerified
FROM dbo.ProcessPeers pp INNER JOIN dbo.ProcessPeerGroupDef ppgd ON ppgd.PPGKey = pp.FacilityPPGKey
INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = ppgd.StudyRefnum
INNER JOIN dbo.Companies c ON c.CompanyID = cr.CompanyID
INNER JOIN dbo.ProcessVsPeer pvp ON pvp.ProcessPeerKey = pp.ProcessPeerKey
WHERE c.CompanyID = 'CVX'
AND (pp.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR ppgd.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR cr.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR pvp.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
)





