﻿
CREATE VIEW [CVX].[VarDef]
AS

SELECT DISTINCT vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode, vd.tsModified
FROM dbo.CompanyVarDef vd
WHERE vd.CompanyID = 'CVX' AND vd.tsModified > ISNULL((SELECT c.DataPullStartDate FROM dbo.Companies c WHERE CompanyID = 'CVX'), '1/1/2000')

/*
SELECT DISTINCT vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode, vd.tsModified
FROM CVX.OutputFiles cof
INNER JOIN dbo.OutputFileSheetRows r ON r.FKey = cof.FKey
INNER JOIN dbo.VarDef vd ON vd.VKey = r.VKey
INNER JOIN dbo.Companies c ON c.CompanyID = 'CVX'
WHERE r.VKey IS NOT NULL AND r.RowHidden = 0 AND (vd.tsModified > ISNULL(c.DataPullStartDate, '1/1/2000'))
UNION
SELECT vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode, vd.tsModified
FROM dbo.VarDef vd 
WHERE vd.tsModified > ISNULL((SELECT DataPullStartDate FROM dbo.Companies c WHERE c.CompanyID = 'CVX'), '1/1/2000')
AND EXISTS (SELECT 1 FROM dbo.RefineryData rd INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = rd.PGKey INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum WHERE cr.CompanyID = 'CVX' AND rd.VKey = vd.VKey)
UNION
SELECT vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode, vd.tsModified
FROM dbo.VarDef vd 
WHERE vd.tsModified > ISNULL((SELECT DataPullStartDate FROM dbo.Companies c WHERE c.CompanyID = 'CVX'), '1/1/2000')
AND EXISTS (SELECT 1 FROM dbo.ProcessData pd INNER JOIN dbo.ProcessPeerGroupDef ppgd ON ppgd.PPGKey = pd.PPGKey INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = ppgd.StudyRefnum WHERE cr.CompanyID = 'CVX' AND pd.VKey = vd.VKey)
UNION
SELECT vd.VKey, vd.Variable, vd.UOM, vd.LongText, vd.DataType, vd.Decimals, vd.IsMetric, vd.SectionCode, vd.tsModified
FROM dbo.VarDef vd 
WHERE vd.tsModified > ISNULL((SELECT DataPullStartDate FROM dbo.Companies c WHERE c.CompanyID = 'CVX'), '1/1/2000')
AND (EXISTS (SELECT 1 FROM dbo.RefineryVsPeer rvp WHERE rvp.VKey = vd.VKey) OR EXISTS (SELECT 1 FROM dbo.ProcessVsPeer rvp WHERE rvp.VKey = vd.VKey))
*/






