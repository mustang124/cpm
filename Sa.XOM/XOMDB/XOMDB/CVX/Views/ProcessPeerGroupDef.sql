﻿

CREATE VIEW [CVX].[ProcessPeerGroupDef]
AS
SELECT d.PPGKey, d.PGKey, d.ProcessID, d.ProcessType, d.LongText, d.Class, d.SubClass, d.PeerType, d.RamGroup, d.tsModified
FROM dbo.Companies c
CROSS APPLY dbo.GetProcessPeerGroupDefsNew(c.CompanyID,c.DataPullStartDate) d
WHERE c.CompanyID = 'CVX'





