﻿


CREATE VIEW [CVX].[RefineryPeers]
AS
SELECT rp.RefineryPeerKey, rp.FacilityPGKey, rp.TargetPGKey, rp.IsOpexPeer, rp.IsMarginPeer, rp.IsEIIPeer, rp.IsCEIPeer, rp.IsLubesPeer, rp.tsModified
FROM dbo.RefineryPeers rp INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = rp.FacilityPGKey
INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
INNER JOIN dbo.Companies c ON c.CompanyID = cr.CompanyID
WHERE c.CompanyID = 'CVX'
AND (rp.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR pgd.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR cr.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
)




