﻿CREATE TABLE [XOM].[RefineryDataForPA] (
    [PGKey]       INT            NOT NULL,
    [VKey]        INT            NOT NULL,
    [Currency]    VARCHAR (4)    NOT NULL,
    [numberValue] FLOAT (53)     NULL,
    [textValue]   NVARCHAR (MAX) NULL,
    [dateValue]   SMALLDATETIME  NULL,
    [tsModified]  DATETIME       NOT NULL
);

