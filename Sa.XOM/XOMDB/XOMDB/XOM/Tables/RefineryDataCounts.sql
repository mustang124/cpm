﻿CREATE TABLE [XOM].[RefineryDataCounts] (
    [PGKey]     INT NOT NULL,
    [DataCount] INT NULL,
    PRIMARY KEY CLUSTERED ([PGKey] ASC)
);

