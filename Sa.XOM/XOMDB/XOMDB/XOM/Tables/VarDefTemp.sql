﻿CREATE TABLE [XOM].[VarDefTemp] (
    [VKey]        INT            NOT NULL,
    [Variable]    VARCHAR (255)  NOT NULL,
    [UOM]         VARCHAR (50)   NULL,
    [LongText]    NVARCHAR (MAX) NOT NULL,
    [DataType]    VARCHAR (10)   NOT NULL,
    [Decimals]    TINYINT        NOT NULL,
    [IsMetric]    BIT            NOT NULL,
    [SectionCode] VARCHAR (255)  NULL,
    [tsModified]  DATETIME       NOT NULL
);

