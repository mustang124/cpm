﻿CREATE TABLE [XOM].[RefineryDataReview] (
    [PGKey]     INT      NOT NULL,
    [DataCount] INT      NULL,
    [MinDate]   DATETIME NULL,
    [MaxDate]   DATETIME NULL
);

