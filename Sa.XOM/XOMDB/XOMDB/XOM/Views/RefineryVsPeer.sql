﻿



CREATE VIEW [XOM].[RefineryVsPeer]
AS
SELECT rp.RefineryPeerKey, rvp.VKey, rvp.Currency, rvp.numberValue, rvp.tsModified, rvp.tsVerified
FROM dbo.RefineryPeers rp INNER JOIN dbo.PeerGroupDef pgd ON pgd.PGKey = rp.FacilityPGKey
INNER JOIN dbo.CompanyRefnums cr ON cr.Refnum = pgd.StudyRefnum
INNER JOIN dbo.Companies c ON c.CompanyID = cr.CompanyID
INNER JOIN dbo.RefineryVsPeer rvp ON rvp.RefineryPeerKey = rp.RefineryPeerKey
WHERE c.CompanyID = 'XOM'
AND (rp.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR pgd.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR cr.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR rvp.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
)



