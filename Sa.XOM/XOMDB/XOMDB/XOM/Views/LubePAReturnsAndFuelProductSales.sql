﻿




CREATE VIEW [XOM].[LubePAReturnsAndFuelProductSales]
AS
SELECT r.PGKey, r.MaterialKey, r.MaterialID, r.MaterialName, r.Bbl, r.PlantProdImpact, r.FinProdImpact, r.CashMarginBblImpact, r.CashMarginImpactMUSD, r.tsModified
FROM dbo.LubePAReturnsAndFuelProductSales r
WHERE r.PGKey IN (SELECT PGKey FROM dbo.GetPeerGroupDefsNew('XOM', NULL))
AND tsModified >= ISNULL((SELECT DataPullStartDate FROM dbo.Companies WHERE CompanyID = 'XOM'),'1/1/2000')

