﻿


CREATE VIEW [XOM].[OutputFileSheets]
AS
SELECT s.*
FROM dbo.Companies c INNER JOIN dbo.CompanyStudies cs ON cs.CompanyID = c.CompanyID
INNER JOIN dbo.OutputFiles f ON (f.Study = cs.Study) AND f.StudyYear = cs.StudyYear
INNER JOIN dbo.OutputSheets s ON s.FKey = f.FKey
WHERE c.CompanyID = 'XOM' 
AND (cs.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000')
	OR s.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000') 
	)



