﻿





CREATE VIEW [XOM].[OutputFiles]
AS
SELECT f.FKey, f.StudyYear, f.Study, f.FileType, f.Currency
FROM dbo.Companies c INNER JOIN dbo.CompanyStudies cs ON cs.CompanyID = c.CompanyID
INNER JOIN dbo.OutputFiles f ON (f.Study = cs.Study) AND f.StudyYear = cs.StudyYear
WHERE c.CompanyID = 'XOM' AND (f.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000') OR cs.tsModified > ISNULL(c.DataPullStartDate,'1/1/2000'))




