﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _1020_ProcessUnit : SqlDatabaseTestClass
    {

        public _1020_ProcessUnit()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }
        [TestMethod()]
        public void ProcessUnit_Capacity()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_CapacityData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_SuppTot()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_SuppTotData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_FeedPrep()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_FeedPrepData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_PyroGasHydroTreat()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_PyroGasHydroTreatData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_HydroPur()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_HydroPurData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_RedPropylene()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_RedPropyleneData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_RedEthylene()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_RedEthyleneData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_RedCrackedGasTrans()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_RedCrackedGasTransData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_BoilFiredSteam()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_BoilFiredSteamData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ProcessUnit_ElecGen()
        {
            SqlDatabaseTestActions testActions = this.ProcessUnit_ElecGenData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }











        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_Capacity_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1020_ProcessUnit));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_Capacity_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_Capacity_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_SuppTot_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_SuppTot_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_SuppTot_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_FeedPrep_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_FeedPrep_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_FeedPrep_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_PyroGasHydroTreat_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_PyroGasHydroTreat_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_PyroGasHydroTreat_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_HydroPur_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_HydroPur_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_HydroPur_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_RedPropylene_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_RedPropylene_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_RedPropylene_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_RedEthylene_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_RedEthylene_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_RedEthylene_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_RedCrackedGasTrans_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_RedCrackedGasTrans_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_RedCrackedGasTrans_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_BoilFiredSteam_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_BoilFiredSteam_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_BoilFiredSteam_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ProcessUnit_ElecGen_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ProcessUnit_ElecGen_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ProcessUnit_ElecGen_EmptySet;
            this.ProcessUnit_CapacityData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_SuppTotData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_FeedPrepData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_PyroGasHydroTreatData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_HydroPurData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_RedPropyleneData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_RedEthyleneData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_RedCrackedGasTransData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_BoilFiredSteamData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ProcessUnit_ElecGenData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            ProcessUnit_Capacity_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_Capacity_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_Capacity_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_SuppTot_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_SuppTot_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_SuppTot_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_FeedPrep_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_FeedPrep_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_FeedPrep_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_PyroGasHydroTreat_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_PyroGasHydroTreat_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_PyroGasHydroTreat_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_HydroPur_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_HydroPur_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_HydroPur_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_RedPropylene_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_RedPropylene_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_RedPropylene_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_RedEthylene_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_RedEthylene_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_RedEthylene_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_RedCrackedGasTrans_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_RedCrackedGasTrans_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_RedCrackedGasTrans_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_BoilFiredSteam_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_BoilFiredSteam_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_BoilFiredSteam_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ProcessUnit_ElecGen_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ProcessUnit_ElecGen_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ProcessUnit_ElecGen_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // ProcessUnit_Capacity_TestAction
            // 
            ProcessUnit_Capacity_TestAction.Conditions.Add(ProcessUnit_Capacity_NotEmpty);
            ProcessUnit_Capacity_TestAction.Conditions.Add(ProcessUnit_Capacity_EmptySet);
            resources.ApplyResources(ProcessUnit_Capacity_TestAction, "ProcessUnit_Capacity_TestAction");
            // 
            // ProcessUnit_Capacity_NotEmpty
            // 
            ProcessUnit_Capacity_NotEmpty.Enabled = true;
            ProcessUnit_Capacity_NotEmpty.Name = "ProcessUnit_Capacity_NotEmpty";
            ProcessUnit_Capacity_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_Capacity_EmptySet
            // 
            ProcessUnit_Capacity_EmptySet.Enabled = true;
            ProcessUnit_Capacity_EmptySet.Name = "ProcessUnit_Capacity_EmptySet";
            ProcessUnit_Capacity_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_SuppTot_TestAction
            // 
            ProcessUnit_SuppTot_TestAction.Conditions.Add(ProcessUnit_SuppTot_NotEmpty);
            ProcessUnit_SuppTot_TestAction.Conditions.Add(ProcessUnit_SuppTot_EmptySet);
            resources.ApplyResources(ProcessUnit_SuppTot_TestAction, "ProcessUnit_SuppTot_TestAction");
            // 
            // ProcessUnit_SuppTot_NotEmpty
            // 
            ProcessUnit_SuppTot_NotEmpty.Enabled = true;
            ProcessUnit_SuppTot_NotEmpty.Name = "ProcessUnit_SuppTot_NotEmpty";
            ProcessUnit_SuppTot_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_SuppTot_EmptySet
            // 
            ProcessUnit_SuppTot_EmptySet.Enabled = true;
            ProcessUnit_SuppTot_EmptySet.Name = "ProcessUnit_SuppTot_EmptySet";
            ProcessUnit_SuppTot_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_FeedPrep_TestAction
            // 
            ProcessUnit_FeedPrep_TestAction.Conditions.Add(ProcessUnit_FeedPrep_NotEmpty);
            ProcessUnit_FeedPrep_TestAction.Conditions.Add(ProcessUnit_FeedPrep_EmptySet);
            resources.ApplyResources(ProcessUnit_FeedPrep_TestAction, "ProcessUnit_FeedPrep_TestAction");
            // 
            // ProcessUnit_FeedPrep_NotEmpty
            // 
            ProcessUnit_FeedPrep_NotEmpty.Enabled = true;
            ProcessUnit_FeedPrep_NotEmpty.Name = "ProcessUnit_FeedPrep_NotEmpty";
            ProcessUnit_FeedPrep_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_FeedPrep_EmptySet
            // 
            ProcessUnit_FeedPrep_EmptySet.Enabled = true;
            ProcessUnit_FeedPrep_EmptySet.Name = "ProcessUnit_FeedPrep_EmptySet";
            ProcessUnit_FeedPrep_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_PyroGasHydroTreat_TestAction
            // 
            ProcessUnit_PyroGasHydroTreat_TestAction.Conditions.Add(ProcessUnit_PyroGasHydroTreat_NotEmpty);
            ProcessUnit_PyroGasHydroTreat_TestAction.Conditions.Add(ProcessUnit_PyroGasHydroTreat_EmptySet);
            resources.ApplyResources(ProcessUnit_PyroGasHydroTreat_TestAction, "ProcessUnit_PyroGasHydroTreat_TestAction");
            // 
            // ProcessUnit_PyroGasHydroTreat_NotEmpty
            // 
            ProcessUnit_PyroGasHydroTreat_NotEmpty.Enabled = true;
            ProcessUnit_PyroGasHydroTreat_NotEmpty.Name = "ProcessUnit_PyroGasHydroTreat_NotEmpty";
            ProcessUnit_PyroGasHydroTreat_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_PyroGasHydroTreat_EmptySet
            // 
            ProcessUnit_PyroGasHydroTreat_EmptySet.Enabled = true;
            ProcessUnit_PyroGasHydroTreat_EmptySet.Name = "ProcessUnit_PyroGasHydroTreat_EmptySet";
            ProcessUnit_PyroGasHydroTreat_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_HydroPur_TestAction
            // 
            ProcessUnit_HydroPur_TestAction.Conditions.Add(ProcessUnit_HydroPur_NotEmpty);
            ProcessUnit_HydroPur_TestAction.Conditions.Add(ProcessUnit_HydroPur_EmptySet);
            resources.ApplyResources(ProcessUnit_HydroPur_TestAction, "ProcessUnit_HydroPur_TestAction");
            // 
            // ProcessUnit_HydroPur_NotEmpty
            // 
            ProcessUnit_HydroPur_NotEmpty.Enabled = true;
            ProcessUnit_HydroPur_NotEmpty.Name = "ProcessUnit_HydroPur_NotEmpty";
            ProcessUnit_HydroPur_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_HydroPur_EmptySet
            // 
            ProcessUnit_HydroPur_EmptySet.Enabled = true;
            ProcessUnit_HydroPur_EmptySet.Name = "ProcessUnit_HydroPur_EmptySet";
            ProcessUnit_HydroPur_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_RedPropylene_TestAction
            // 
            ProcessUnit_RedPropylene_TestAction.Conditions.Add(ProcessUnit_RedPropylene_NotEmpty);
            ProcessUnit_RedPropylene_TestAction.Conditions.Add(ProcessUnit_RedPropylene_EmptySet);
            resources.ApplyResources(ProcessUnit_RedPropylene_TestAction, "ProcessUnit_RedPropylene_TestAction");
            // 
            // ProcessUnit_RedPropylene_NotEmpty
            // 
            ProcessUnit_RedPropylene_NotEmpty.Enabled = true;
            ProcessUnit_RedPropylene_NotEmpty.Name = "ProcessUnit_RedPropylene_NotEmpty";
            ProcessUnit_RedPropylene_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_RedPropylene_EmptySet
            // 
            ProcessUnit_RedPropylene_EmptySet.Enabled = true;
            ProcessUnit_RedPropylene_EmptySet.Name = "ProcessUnit_RedPropylene_EmptySet";
            ProcessUnit_RedPropylene_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_RedEthylene_TestAction
            // 
            ProcessUnit_RedEthylene_TestAction.Conditions.Add(ProcessUnit_RedEthylene_NotEmpty);
            ProcessUnit_RedEthylene_TestAction.Conditions.Add(ProcessUnit_RedEthylene_EmptySet);
            resources.ApplyResources(ProcessUnit_RedEthylene_TestAction, "ProcessUnit_RedEthylene_TestAction");
            // 
            // ProcessUnit_RedEthylene_NotEmpty
            // 
            ProcessUnit_RedEthylene_NotEmpty.Enabled = false;
            ProcessUnit_RedEthylene_NotEmpty.Name = "ProcessUnit_RedEthylene_NotEmpty";
            ProcessUnit_RedEthylene_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_RedEthylene_EmptySet
            // 
            ProcessUnit_RedEthylene_EmptySet.Enabled = false;
            ProcessUnit_RedEthylene_EmptySet.Name = "ProcessUnit_RedEthylene_EmptySet";
            ProcessUnit_RedEthylene_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_RedCrackedGasTrans_TestAction
            // 
            ProcessUnit_RedCrackedGasTrans_TestAction.Conditions.Add(ProcessUnit_RedCrackedGasTrans_NotEmpty);
            ProcessUnit_RedCrackedGasTrans_TestAction.Conditions.Add(ProcessUnit_RedCrackedGasTrans_EmptySet);
            resources.ApplyResources(ProcessUnit_RedCrackedGasTrans_TestAction, "ProcessUnit_RedCrackedGasTrans_TestAction");
            // 
            // ProcessUnit_RedCrackedGasTrans_NotEmpty
            // 
            ProcessUnit_RedCrackedGasTrans_NotEmpty.Enabled = true;
            ProcessUnit_RedCrackedGasTrans_NotEmpty.Name = "ProcessUnit_RedCrackedGasTrans_NotEmpty";
            ProcessUnit_RedCrackedGasTrans_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_RedCrackedGasTrans_EmptySet
            // 
            ProcessUnit_RedCrackedGasTrans_EmptySet.Enabled = true;
            ProcessUnit_RedCrackedGasTrans_EmptySet.Name = "ProcessUnit_RedCrackedGasTrans_EmptySet";
            ProcessUnit_RedCrackedGasTrans_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_BoilFiredSteam_TestAction
            // 
            ProcessUnit_BoilFiredSteam_TestAction.Conditions.Add(ProcessUnit_BoilFiredSteam_NotEmpty);
            ProcessUnit_BoilFiredSteam_TestAction.Conditions.Add(ProcessUnit_BoilFiredSteam_EmptySet);
            resources.ApplyResources(ProcessUnit_BoilFiredSteam_TestAction, "ProcessUnit_BoilFiredSteam_TestAction");
            // 
            // ProcessUnit_BoilFiredSteam_NotEmpty
            // 
            ProcessUnit_BoilFiredSteam_NotEmpty.Enabled = true;
            ProcessUnit_BoilFiredSteam_NotEmpty.Name = "ProcessUnit_BoilFiredSteam_NotEmpty";
            ProcessUnit_BoilFiredSteam_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_BoilFiredSteam_EmptySet
            // 
            ProcessUnit_BoilFiredSteam_EmptySet.Enabled = true;
            ProcessUnit_BoilFiredSteam_EmptySet.Name = "ProcessUnit_BoilFiredSteam_EmptySet";
            ProcessUnit_BoilFiredSteam_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_ElecGen_TestAction
            // 
            ProcessUnit_ElecGen_TestAction.Conditions.Add(ProcessUnit_ElecGen_NotEmpty);
            ProcessUnit_ElecGen_TestAction.Conditions.Add(ProcessUnit_ElecGen_EmptySet);
            resources.ApplyResources(ProcessUnit_ElecGen_TestAction, "ProcessUnit_ElecGen_TestAction");
            // 
            // ProcessUnit_ElecGen_NotEmpty
            // 
            ProcessUnit_ElecGen_NotEmpty.Enabled = true;
            ProcessUnit_ElecGen_NotEmpty.Name = "ProcessUnit_ElecGen_NotEmpty";
            ProcessUnit_ElecGen_NotEmpty.ResultSet = 1;
            // 
            // ProcessUnit_ElecGen_EmptySet
            // 
            ProcessUnit_ElecGen_EmptySet.Enabled = true;
            ProcessUnit_ElecGen_EmptySet.Name = "ProcessUnit_ElecGen_EmptySet";
            ProcessUnit_ElecGen_EmptySet.ResultSet = 2;
            // 
            // ProcessUnit_CapacityData
            // 
            this.ProcessUnit_CapacityData.PosttestAction = null;
            this.ProcessUnit_CapacityData.PretestAction = null;
            this.ProcessUnit_CapacityData.TestAction = ProcessUnit_Capacity_TestAction;
            // 
            // ProcessUnit_SuppTotData
            // 
            this.ProcessUnit_SuppTotData.PosttestAction = null;
            this.ProcessUnit_SuppTotData.PretestAction = null;
            this.ProcessUnit_SuppTotData.TestAction = ProcessUnit_SuppTot_TestAction;
            // 
            // ProcessUnit_FeedPrepData
            // 
            this.ProcessUnit_FeedPrepData.PosttestAction = null;
            this.ProcessUnit_FeedPrepData.PretestAction = null;
            this.ProcessUnit_FeedPrepData.TestAction = ProcessUnit_FeedPrep_TestAction;
            // 
            // ProcessUnit_PyroGasHydroTreatData
            // 
            this.ProcessUnit_PyroGasHydroTreatData.PosttestAction = null;
            this.ProcessUnit_PyroGasHydroTreatData.PretestAction = null;
            this.ProcessUnit_PyroGasHydroTreatData.TestAction = ProcessUnit_PyroGasHydroTreat_TestAction;
            // 
            // ProcessUnit_HydroPurData
            // 
            this.ProcessUnit_HydroPurData.PosttestAction = null;
            this.ProcessUnit_HydroPurData.PretestAction = null;
            this.ProcessUnit_HydroPurData.TestAction = ProcessUnit_HydroPur_TestAction;
            // 
            // ProcessUnit_RedPropyleneData
            // 
            this.ProcessUnit_RedPropyleneData.PosttestAction = null;
            this.ProcessUnit_RedPropyleneData.PretestAction = null;
            this.ProcessUnit_RedPropyleneData.TestAction = ProcessUnit_RedPropylene_TestAction;
            // 
            // ProcessUnit_RedEthyleneData
            // 
            this.ProcessUnit_RedEthyleneData.PosttestAction = null;
            this.ProcessUnit_RedEthyleneData.PretestAction = null;
            this.ProcessUnit_RedEthyleneData.TestAction = ProcessUnit_RedEthylene_TestAction;
            // 
            // ProcessUnit_RedCrackedGasTransData
            // 
            this.ProcessUnit_RedCrackedGasTransData.PosttestAction = null;
            this.ProcessUnit_RedCrackedGasTransData.PretestAction = null;
            this.ProcessUnit_RedCrackedGasTransData.TestAction = ProcessUnit_RedCrackedGasTrans_TestAction;
            // 
            // ProcessUnit_BoilFiredSteamData
            // 
            this.ProcessUnit_BoilFiredSteamData.PosttestAction = null;
            this.ProcessUnit_BoilFiredSteamData.PretestAction = null;
            this.ProcessUnit_BoilFiredSteamData.TestAction = ProcessUnit_BoilFiredSteam_TestAction;
            // 
            // ProcessUnit_ElecGenData
            // 
            this.ProcessUnit_ElecGenData.PosttestAction = null;
            this.ProcessUnit_ElecGenData.PretestAction = null;
            this.ProcessUnit_ElecGenData.TestAction = ProcessUnit_ElecGen_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions ProcessUnit_CapacityData;
        private SqlDatabaseTestActions ProcessUnit_SuppTotData;
        private SqlDatabaseTestActions ProcessUnit_FeedPrepData;
        private SqlDatabaseTestActions ProcessUnit_PyroGasHydroTreatData;
        private SqlDatabaseTestActions ProcessUnit_HydroPurData;
        private SqlDatabaseTestActions ProcessUnit_RedPropyleneData;
        private SqlDatabaseTestActions ProcessUnit_RedEthyleneData;
        private SqlDatabaseTestActions ProcessUnit_RedCrackedGasTransData;
        private SqlDatabaseTestActions ProcessUnit_BoilFiredSteamData;
        private SqlDatabaseTestActions ProcessUnit_ElecGenData;
    }
}
