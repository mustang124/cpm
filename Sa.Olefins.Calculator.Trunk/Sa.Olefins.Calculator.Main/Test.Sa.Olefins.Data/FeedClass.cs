﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Data
{
    [TestClass()]
    public class cFeedClass : SqlDatabaseTestClass
    {

        public cFeedClass()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void FeedClass()
        {
            SqlDatabaseTestActions testActions = this.FeedClassData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FeedClass_TestAction;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cFeedClass));
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FeedClass_EmptySet;
			this.FeedClassData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
			FeedClass_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
			FeedClass_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
			// 
			// FeedClass_TestAction
			// 
			FeedClass_TestAction.Conditions.Add(FeedClass_EmptySet);
			resources.ApplyResources(FeedClass_TestAction, "FeedClass_TestAction");
			// 
			// FeedClass_EmptySet
			// 
			FeedClass_EmptySet.Enabled = true;
			FeedClass_EmptySet.Name = "FeedClass_EmptySet";
			FeedClass_EmptySet.ResultSet = 1;
			// 
			// FeedClassData
			// 
			this.FeedClassData.PosttestAction = null;
			this.FeedClassData.PretestAction = null;
			this.FeedClassData.TestAction = FeedClass_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions FeedClassData;
    }
}
