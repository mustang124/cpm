﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Data
{
    [TestClass()]
    public class ante_Functions : SqlDatabaseTestClass
    {

        public ante_Functions()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Return_MethodologyId()
        {
            SqlDatabaseTestActions testActions = this.Return_MethodologyIdData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Return_MethodologyId_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ante_Functions));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition Return_MethodologyId_Scalar;
            this.Return_MethodologyIdData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Return_MethodologyId_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Return_MethodologyId_Scalar = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // Return_MethodologyIdData
            // 
            this.Return_MethodologyIdData.PosttestAction = null;
            this.Return_MethodologyIdData.PretestAction = null;
            this.Return_MethodologyIdData.TestAction = Return_MethodologyId_TestAction;
            // 
            // Return_MethodologyId_TestAction
            // 
            Return_MethodologyId_TestAction.Conditions.Add(Return_MethodologyId_Scalar);
            resources.ApplyResources(Return_MethodologyId_TestAction, "Return_MethodologyId_TestAction");
            // 
            // Return_MethodologyId_Scalar
            // 
            Return_MethodologyId_Scalar.ColumnNumber = 1;
            Return_MethodologyId_Scalar.Enabled = true;
            Return_MethodologyId_Scalar.ExpectedValue = "1";
            Return_MethodologyId_Scalar.Name = "Return_MethodologyId_Scalar";
            Return_MethodologyId_Scalar.NullExpected = false;
            Return_MethodologyId_Scalar.ResultSet = 1;
            Return_MethodologyId_Scalar.RowNumber = 1;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions Return_MethodologyIdData;
    }
}
