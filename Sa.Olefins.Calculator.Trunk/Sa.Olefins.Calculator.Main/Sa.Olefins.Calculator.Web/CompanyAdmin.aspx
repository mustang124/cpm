﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="CompanyAdmin.aspx.cs" Inherits="OlefinsCalculator.CompanyAdmin" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:UpdatePanel ID="upnlMain" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<div style="background-color: #E6EAED; height: 23px; width: 955px; padding-top: 5px; padding-left: 5px; margin-bottom: 10px;"><div style="width: 300px; float: left;">Company: <asp:DropDownList ID="ddlCompany" runat="server" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div><div style="float: right; padding-right: 5px;"><asp:LinkButton ID="lnkbtnAddCompany" runat="server" OnClick="lnkbtnAddCompany_Click" Text="Add Company" /> | <asp:LinkButton ID="lnkbtnAddPlant" runat="server" OnClick="lnkbtnAddPlant_Click" Text="Add Plant" /></div></div>
<div style="width: 960px; margin-top: 15px; margin-left: 10px; margin-right: 10px;">
<p />
<div style="width: 940px;">


<p></p>
<div id="divCoInfo" runat="server" visible="false" style="float: left;">
<div style="float: left;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 350px; color: #FFFFFF;">Company Information</div><div><img src="images/rightRound.png" /></div>
<div id="outerCC">
<div id="innterCC" style="border: 1px solid #0380cd; padding-left: 3px; width: 365px; height: 272px; overflow-y: auto;">
<table border="0" cellpadding="2" cellspacing="3" width="100%">
<tr><td>Company Name:</td><td><asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="rfvCompanyName" runat="server" ControlToValidate="txtCompanyName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgCoCoord"></asp:RequiredFieldValidator></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td colspan="1" style="font-weight: bold;">
Company Coordinator
</td><td><asp:DropDownList ID="ddlCompanyCoordinator" runat="server" OnSelectedIndexChanged="ddlCompanyCoordinator_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td>First Name:</td><td><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgCoCoord"></asp:RequiredFieldValidator></td></tr>
<tr><td>Last Name:</td><td><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgCoCoord"></asp:RequiredFieldValidator></td></tr>
<tr><td>Email:</td><td><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgCoCoord"></asp:RequiredFieldValidator></td></tr>
<tr><td>Password:</td><td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" style="width: 148px;"></asp:TextBox><asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" Enabled="false" ValidationGroup="vgCoCoord"></asp:RequiredFieldValidator></td></tr>
<tr><td>Re-Type Password:</td><td><asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" style="width: 148px;"></asp:TextBox></td></tr>
</table>
</div>
<asp:HiddenField ID="hidUserID" runat="server" />
<asp:HiddenField ID="hidCompanyID" runat="server" />
<asp:HiddenField ID="hidInsertUser" runat="server" Value="0" />
<div style="width: 362px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px;">
<asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" ValidationGroup="vgCoCoord" /><asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label><asp:CompareValidator ID="cvPasswords" runat="server" ControlToCompare="txtPassword2" ControlToValidate="txtPassword" EnableClientScript="true" ForeColor="Red" ErrorMessage="Passwords Must Match" ValidationGroup="vgCoCoord"></asp:CompareValidator>
</div>
</div>
</div>

<div id="divPlants" runat="server" visible="false" style="float: right; margin-bottom: 10px;">
<div style="float: left;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 530px; color: #FFFFFF;">Plants</div><div><img src="images/rightRound.png" /></div>
<div id="divOuterPlants">
<div id="divInnerPlants" style="border: 1px solid #0380cd; width: 545px; padding-left: 3px; height: 88px; overflow-y: auto;">
<asp:ListView ID="lvPlants" runat="server" GroupItemCount="4" OnItemDataBound="lvPlants_ItemDataBound">
   <LayoutTemplate>
      <table>
         <tr>
            <td>
               <table border="0" cellpadding="0" cellspacing="0">
                  <asp:PlaceHolder ID="groupPlaceHolder" runat="server"></asp:PlaceHolder>
               </table>
            </td>
         </tr>
      </table>
   </LayoutTemplate>
   <GroupTemplate>
      <tr>
         <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
      </tr>
   </GroupTemplate>
   <ItemTemplate>
      <td>
         <asp:CheckBox ID="chkPlant" runat="server" /> <asp:LinkButton ID="lnkbtnSelectPlant" runat="server" OnClick="lnkbtnSelectPlant_Click"></asp:LinkButton> <asp:Label ID="lblPlantName" runat="server" Visible="false"><%# Eval("PlantName") %></asp:Label><asp:HiddenField ID="hidPlantID" runat="server" />
      </td>
   </ItemTemplate>
</asp:ListView>
</div>
<div style="width: 542px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px;">
<asp:Button ID="btnDeletePlants" runat="server" OnClick="btnDeletePlants_Click" Text="Delete Selected" />
</div>
</div>
</div>

<div id="divCalcs" runat="server" visible="false" style="float: right;">
<div style="float: left;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 530px; color: #FFFFFF;">Calculations</div><div><img src="images/rightRound.png" /></div>
<div id="outerCalcs">
<div id="innerCalcs" style="border: 1px solid #0380cd; width: 545px; padding-left: 3px; height: 118px; overflow-y: auto;">
<table border="0" cellpadding="0" cellspacing="3" width="100%">
<tr><td>Plant:</td><td><asp:DropDownList ID="ddlPlant" runat="server" OnSelectedIndexChanged="ddlPlant_SelctedIndexChanged" AutoPostBack="true" ></asp:DropDownList> <asp:Label ID="lblRemainingCalcs" runat="server" Visible="false"></asp:Label></td></tr>
</table>
<asp:ListView ID="lvCalcs" runat="server" GroupItemCount="2" OnItemDataBound="lvCalcs_ItemDataBound">
<LayoutTemplate>
    <table>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0">
                    <asp:PlaceHolder ID="groupPlaceholder" runat="server"></asp:PlaceHolder>
                </table>
            </td>
        </tr>
    </table>
</LayoutTemplate>
<GroupTemplate>
    <tr>
        <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
    </tr>
</GroupTemplate>
<ItemTemplate>
    <td>
        <asp:CheckBox ID="chkCalc" runat="server" /> <asp:HyperLink ID="hlCalc" runat="server" Target="_blank"></asp:HyperLink><asp:HiddenField ID="hidCalcID" runat="server" />
    </td>
</ItemTemplate>
</asp:ListView>
</div>
<div style="width: 542px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px;">
<asp:Button ID="btnDeleteCalcs" runat="server" OnClick="btnDeleteCalcs_Click" Text="Delete Selected" />
</div>
</div>
</div>

<br clear="all" />
&nbsp;

</div>
</div>



<ajaxToolkit:ModalPopupExtender ID="mpePlant" runat="server" DropShadow="true" PopupControlID="pnlPlant" TargetControlID="btnHidPlant" Y="120" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
<asp:Button ID="btnHidPlant" runat="server" style="display: none;" />
<asp:Panel ID="pnlPlant" runat="server" style="border: 0px solid #000000; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px;">
<asp:UpdatePanel ID="upnlEditPlant" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<div style="margin-bottom: 25px; border-top: 1px solid #000000; border-left: 1px solid #000000; background-color: #0380cd; position: absolute; float: left; height: 23px; width: 336px; color: #FFFFFF; padding-left: 6px;">Plant Information</div>
<div style="float: right; height: 30px; position: absolute; left: 335px; top: -10px;"><img id="img1" runat="server" src="Images/close.png" alt="" onclick="$find('mpePlant').hide();" style="cursor: pointer;" /></div>
<div style="width: 330px; padding: 6px; background-color: #ffffff; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px; border: 1px solid #000000;">
<br clear="all" />
<div style="height: 30px; width: 100px; float: left;">Company:</div><div style="height: 30px;"><asp:DropDownList ID="ddlCompanyName" runat="server"></asp:DropDownList><asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="ddlCompanyName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgEditPlant"></asp:RequiredFieldValidator></div>
<div style="height: 30px; width: 100px; float: left;">Plant Name:</div><div style="height: 30px;"><asp:TextBox ID="txtPlantName" runat="server" /><asp:RequiredFieldValidator ID="rfvPlantName" runat="server" ControlToValidate="txtPlantName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgEditPlant"></asp:RequiredFieldValidator></div>
<asp:Button ID="btnEditPlantSubmit" runat="server" OnClick="btnEditPlantSubmit_Click" Text="Submit" ValidationGroup="vgEditPlant" /><asp:Label ID="lblPlantEditStatus" runat="server" Text="" Visible="false"></asp:Label>
<asp:HiddenField ID="hidPlantID" runat="server" />
<asp:HiddenField ID="hidInsertPlant" runat="server" Value="0" />
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>






<div ID="divPlantUsers" runat="server" visible="false" style="float: right; background-color: #FFFFFF; width: 960px;">
<div style="float: left; margin-left: 7px;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 925px; color: #FFFFFF;">Users&nbsp;<asp:Label ID="lblUsersPlant" runat="server" Text=""></asp:Label></div><div><img src="images/rightRound.png" /></div>
<div id="outerUsers">
<a name="Users"></a>
<div id="innerUsers" style="border: 1px solid #0380cd; width: 940px; padding-left: 3px; height: 200px; overflow-y: auto; margin-left: 7px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>


    <asp:Panel ID="pnlPlantUsers" runat="server" style="border: 0px solid #000000; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px;">
<asp:UpdatePanel ID="upnlPlantUsers" runat="server" UpdateMode="Conditional">
    <Triggers><asp:AsyncPostBackTrigger ControlID="btnUpdateUsers" EventName="Click" /></Triggers>
<ContentTemplate>


<asp:ListView ID="lvPlantUsers" runat="server" GroupItemCount="1" OnItemDataBound="lvPlantUsers_ItemDataBound">
<LayoutTemplate>
    <table>
        <tr>
            <td>
                <table border="0" cellpadding="2" cellspacing="0" style="width: 916px; border-bottom: 1px solid #000000;">
                    <tr><td style="background-color: #eaeaea; font-weight: bold; border-top: 1px solid #000000; border-left: 1px solid #000000;">Name</td><td style="background-color: #eaeaea; font-weight: bold; border-top: 1px solid #000000; border-left: 1px solid #000000;">Access</td><td style="background-color: #eaeaea; font-weight: bold;border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;">Active</td></tr>
                    <asp:PlaceHolder ID="groupPlaceholder" runat="server"></asp:PlaceHolder>
                </table>
            </td>
        </tr>
    </table>
</LayoutTemplate>
<GroupTemplate>
    <tr>
        <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
    </tr>
</GroupTemplate>
<ItemTemplate>
    <td style="border-top: 1px solid #000000; border-left: 1px solid #000000;">
        <asp:Label ID="lblName" runat="server"></asp:Label><asp:HiddenField ID="hidLoginID" runat="server" /><asp:HiddenField ID="hidPlantPermissionID" runat="server" />
    </td>
    <td style="border-top: 1px solid #000000; border-left: 1px solid #000000;">
        <asp:Label ID="lblAccess" runat="server"></asp:Label>
    </td>
    <td style="border-top: 1px solid #000000;  border-left: 1px solid #000000; border-right: 1px solid #000000;">
        <asp:CheckBox ID="chkStatus" runat="server" />
    </td>
</ItemTemplate>
</asp:ListView>


    </ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>

</td></tr>   
</table>
</div>
<div style="width: 937px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px; margin-left: 7px;">
<asp:Button ID="btnUpdateUsers" runat="server" OnClick="btnDeleteUsers_Click" Text="Update Users" /><asp:Label ID="lblUpdateUsersStatus" runat="server" Text="" Visible="false"></asp:Label>
</div>
</div>
</div>





</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
