﻿<%@ Page Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="OlefinsCalculator.ReportViewer" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        window.onload = function setstart() {
            var els = document.getElementsByTagName('div');
            var i = els.length;
            while (i--)
                if (els[i].id.indexOf('ReportDiv') > 0)
                    els[i].style.overflowY = 'hidden';
        }
        function setstart2() {
            var els = document.getElementsByTagName('div');
            var i = els.length;
            while (i--)
                if (els[i].id.indexOf('ReportDiv') > 0)
                    els[i].style.overflowY = 'hidden';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width: 100%; text-align: right;"><asp:Label runat="server" ID="lbltestlbl" text="Methodology: "></asp:Label><asp:DropDownList ID="ddlMethodology" runat="server" OnSelectedIndexChanged="ddlMethodology_SelectedIndexChanged" AutoPostBack="true" onchage="setstart2();"></asp:DropDownList> </div>
    
    <asp:UpdatePanel ID="upnlHeader" runat="server" UpdateMode="Conditional">
<Triggers><asp:AsyncPostBackTrigger ControlID="ddlMethodology" EventName="SelectedIndexChanged" /></Triggers>
<ContentTemplate>


    <div>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="110%" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="960px" style="overflow-y: hidden; overflow-x: hidden; width: 960px;" SizeToReportContent="False" AsyncRendering="True">
        <LocalReport ReportPath="Report.rdlc">            
        </LocalReport>
    </rsweb:ReportViewer>
    </div>


    </ContentTemplate>
</asp:UpdatePanel>


    <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>

</asp:Content>
