﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="PlantAdmin.aspx.cs" Inherits="OlefinsCalculator.PlantAdmin" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function matchPasswords() {
        if ($('#txtPassword').val() != '' && ($('#txtPassword2').val() != $('#txtPassword').val())) {
            $('#lblPasswordError').text('Passwords do not match.');
            return false;
        }
        $('#lblPasswordError').text('');
        return true;
    }
</script>
<style type="text/css">
.modalBackground
{
    background-color:Gray;
    filter:alpha(opacity=50);
    opacity:0.7;
}
.HiddenCol
{
    display: none;
}   
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="upnlMain" runat="server" UpdateMode="Conditional">
<Triggers><asp:AsyncPostBackTrigger ControlID="btnEditPlantSubmit" EventName="Click" /><asp:AsyncPostBackTrigger ControlID="btnEditUserSubmit" EventName="Click" /></Triggers>
<ContentTemplate>
    <div style="background-color: #E6EAED; height: 23px; width: 955px; padding-top: 5px; padding-left: 5px; margin-bottom: 10px;"><div style="width: 300px; float: left;"><asp:Label ID="lblCompanyName" runat="server" Text="Company Name" style="font-weight: bold;" /></div><div style="float: right; padding-right: 5px;"><asp:LinkButton ID="lnkbtnAddPlant" runat="server" OnClick="lnkbtnAddPlant_Click" Text="Add Plant" Visible="false" /><asp:LinkButton ID="lnkbtnAddUser" runat="server" OnClick="lnkbtnAddUser_Click" Text="Add User" /></div></div>

<div style="width: 960px; margin-top: 15px; margin-left: 10px; margin-right: 10px;">




<p />

<div style="width: 940px;">

<div style="float: left;">
<div style="float: left;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 410px; color: #FFFFFF;">Plants</div><div><img src="images/rightRound.png" /></div>
<div id="outerPlants">
<a name="Plants"></a>
<div id="innterPlants" style="border: 1px solid #0380cd; width: 425px; padding-left: 3px; height: 230px; overflow-y: auto;">
<asp:ListView ID="lvPlants" runat="server" GroupItemCount="2">
   <LayoutTemplate>
      <table>
         <tr>
            <td>
               <table border="0" cellpadding="0" cellspacing="0" style="width: 415px;">
                  <asp:PlaceHolder ID="groupPlaceHolder" runat="server"></asp:PlaceHolder>
               </table>
            </td>
         </tr>
      </table>
   </LayoutTemplate>
   <GroupTemplate>
      <tr>
         <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
      </tr>
   </GroupTemplate>
   <ItemTemplate>
      <td>
         <asp:LinkButton ID="lnkbtnSelectPlant" runat="server" OnCommand="lnkbtnSelectPlant_Click" CommandArgument='<%# Bind("PlantID") %>' Text='<%# Bind("PlantName") %>'></asp:LinkButton> <asp:Label ID="lblPlantName" runat="server" Visible="false"><%# Eval("PlantName") %></asp:Label><asp:HiddenField ID="lbldgvPlantsPlantID" runat="server" />
      </td>
   </ItemTemplate>
</asp:ListView>
</div>
<div style="width: 422px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px; display: none;">
<asp:Button ID="btnDeletePlants" runat="server" Text="Delete Selected" OnClick="btnDeletePlants_Click" />
</div>
</div>
</div>
<div style="float: right;">
<div style="float: left;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 410px; color: #FFFFFF;">Users</div><div><img src="images/rightRound.png" /></div>
<div id="outerUsers">
<a name="Users"></a>
<div id="innerUsers" style="border: 1px solid #0380cd; width: 425px; padding-left: 3px; height: 200px; overflow-y: auto;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>
<asp:GridView ID="dgvUsers" runat="server" AutoGenerateColumns="False" ShowHeader="False" BorderStyle="None">
<Columns>
 <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="CheckBox1" runat="server" />
        </ItemTemplate>
        <ItemStyle BorderStyle="None" />
   </asp:TemplateField>

    <asp:TemplateField>
        <ItemTemplate>
            <asp:LinkButton ID="lnkbtnSelectUser" runat="server" OnCommand="lnkbtnSelectUser_Click" CommandArgument='<%# Bind("UserID") %>'><asp:Label ID="lblLastName" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>, <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label></asp:LinkButton>
        </ItemTemplate>
        <ItemStyle BorderStyle="None" />
    </asp:TemplateField>
   
    <asp:TemplateField InsertVisible="False" ShowHeader="False" Visible="False">
        <ItemTemplate>
            <asp:Label ID="lbldgvUsersUserID" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField InsertVisible="False" ShowHeader="False" Visible="False">
        <ItemTemplate>
            <asp:Label ID="lbldgvUsersFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField InsertVisible="False" ShowHeader="False" Visible="False">
        <ItemTemplate>
            <asp:Label ID="lbldgvUsersLastName" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField InsertVisible="False" ShowHeader="False" Visible="False">
        <ItemTemplate>
            <asp:Label ID="lbldgvUsersUsername" runat="server" Text='<%# Bind("Username") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>
</td></tr>   
</table>
</div>
<div style="width: 422px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px;">
<asp:Button ID="btnDeleteUsers" runat="server" OnClick="btnDeleteUsers_Click" Text="Delete Selected" />
</div>
</div>
</div>

</div>


<br clear="all" />
<p>&nbsp;</p>
</div>


<ajaxToolkit:ModalPopupExtender ID="mpeUser" runat="server" DropShadow="true" PopupControlID="pnlUser" TargetControlID="btnHidUser" Y="120" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
<asp:Button ID="btnHidUser" runat="server" style="display: none;" />
<asp:Panel ID="pnlUser" runat="server" style="border: 0px solid #000000; -moz-border-radius: 20px 20px 0px 0px; -webkit-border-radius: 20px 20px 0px 0px; -khtml-border-radius: 20px 20px 0px 0px; border-radius: 20px 20px 0px 0px;">
<asp:UpdatePanel ID="upnlEditUser" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<div style="float: right; height: 30px; position: absolute; left: 435px; top: -10px;"><img id="imgCloseUser" runat="server" src="Images/close.png" alt="" onclick="$find('mpeUser').hide();" style="cursor: pointer;" /></div>
<div style="width: 430px; padding: 6px; background-color: #ffffff; border: 1px solid #000000;">
<div style="height: 30px; width: 130px; float: left;">First Name:</div><div style="height: 30px;"><asp:TextBox ID="txtFirstName" runat="server" /><asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgEditUser"></asp:RequiredFieldValidator></div>
<div style="height: 30px; width: 130px; float: left;">Last Name:</div><div style="height: 30px;"><asp:TextBox ID="txtLastName" runat="server" /><asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgEditUser"></asp:RequiredFieldValidator></div>
<div style="height: 30px; width: 130px; float: left;">Email Address:</div><div style="height: 30px;"><asp:TextBox ID="txtEmail" runat="server" /><asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgEditUser"></asp:RequiredFieldValidator></div>
<div style="height: 30px; width: 130px; float: left;">Password:</div><div style="height: 30px;"><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" style="width: 148px;" /><asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgEditUser" Enabled="false"></asp:RequiredFieldValidator></div>
<div style="height: 30px; width: 130px; float: left;">Re-type Password:</div><div style="height: 30px;"><asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" style="width: 148px;" /><asp:CompareValidator ID="cvPasswords" runat="server" ControlToCompare="txtPassword2" ControlToValidate="txtPassword" EnableClientScript="true" ForeColor="Red" ErrorMessage="Passwords Must Match" ValidationGroup="vgEditUser"></asp:CompareValidator></div>
<div><asp:Label ID="lblUserEditStatus" runat="server" Text="" Visible="false" style="font-size: 8pt;"></asp:Label></div>
<div style="float: left;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 410px;">Assign User To Plants</div><div><img src="images/rightRound.png" /></div>
<div id="Div3">
<div id="Div4" style="border: 1px solid #0380cd; width: 425px; padding-left: 3px; height: 100px; overflow-y: auto;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>
<asp:GridView ID="dgvEditUserPlants" runat="server" AutoGenerateColumns="False" ShowHeader="False" BorderStyle="None">
<Columns>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="chkActive" runat="server" Checked='<%#Eval("Active").ToString().Equals("1") %>' />
        </ItemTemplate>
        <ItemStyle BorderStyle="None" />
   </asp:TemplateField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:Label ID="lblPlantName" runat="server" Text='<%# Bind("PlantName") %>'></asp:Label>
        </ItemTemplate>
        <ItemStyle BorderStyle="None" />
    </asp:TemplateField>
    <asp:TemplateField InsertVisible="False" ShowHeader="False" Visible="False">
        <ItemTemplate>
            <asp:Label ID="lblPlantID" runat="server" Text='<%# Bind("PlantID") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>
</td></tr>   
</table>
</div>
<asp:Button ID="btnEditUserSubmit" runat="server" OnClick="btnEditUserSubmit_Click" Text="Submit" ValidationGroup="vgEditUser" /><asp:Label ID="lblPasswordError" runat="server" ForeColor="Red"></asp:Label>
<asp:HiddenField ID="hidUserID" runat="server" />
<asp:HiddenField ID="hidInsertUser" runat="server" Value="0" />
</div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>

<ajaxToolkit:ModalPopupExtender ID="mpePlant" runat="server" DropShadow="true" PopupControlID="pnlPlant" TargetControlID="btnHidPlant" Y="120" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
<asp:Button ID="btnHidPlant" runat="server" style="display: none;" />
<asp:Panel ID="pnlPlant" runat="server" style="border: 0px solid #000000; -moz-border-radius: 20px 20px 0px 0px; -webkit-border-radius: 20px 20px 0px 0px; -khtml-border-radius: 20px 20px 0px 0px; border-radius: 20px 20px 0px 0px;">
<asp:UpdatePanel ID="upnlEditPlant" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<div style="float: right; height: 30px; position: absolute; left: 435px; top: -10px;"><img id="imgClosePlant" runat="server" src="Images/close.png" alt="" onclick="$find('mpePlant').hide();" style="cursor: pointer;" /></div>
<div style="width: 430px; padding: 6px; background-color: #ffffff; border: 1px solid #000000;">
<div style="display: none;">
<div style="height: 30px; width: 100px; float: left;">Plant Name:</div><div style="height: 30px;"><asp:TextBox ID="txtPlantName" runat="server" /><asp:RequiredFieldValidator ID="rfvPlantName" runat="server" ControlToValidate="txtPlantName" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgEditPlant"></asp:RequiredFieldValidator></div>
</div>
<div style="float: left;"><img src="images/leftRound.png" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 410px;">Users Assigned To This Plant</div><div><img src="images/rightRound.png" /></div>
<div id="Div1">
<div id="Div2" style="border: 1px solid #0380cd; width: 425px; padding-left: 3px; height: 100px; overflow-y: auto;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>
<asp:GridView ID="dgvEditPlantUsers" runat="server" AutoGenerateColumns="False" ShowHeader="False" BorderStyle="None">
<Columns>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:CheckBox ID="CheckBox1" runat="server" />
        </ItemTemplate>
        <ItemStyle BorderStyle="None" />
   </asp:TemplateField>
    <asp:TemplateField>
        <ItemTemplate>
            <asp:Label ID="lblLastName" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>, <asp:Label ID="lblFirstName" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
        </ItemTemplate>
        <ItemStyle BorderStyle="None" />
    </asp:TemplateField>
   
   <asp:BoundField DataField="UserID" InsertVisible="False" ShowHeader="False" Visible="False" />
    <asp:TemplateField InsertVisible="False" ShowHeader="False" Visible="False">
        <ItemTemplate>
            <asp:Label ID="lblPlantPermissionID" runat="server" Text='<%# Bind("PlantPermissionID") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
</Columns>
</asp:GridView>
</td></tr>   
</table>
</div>
<div style="width: 422px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px;">
<asp:Button ID="btnRemoveUser" runat="server" Text="Remove Selected" OnClick="btnRemoveUser_Click" />
</div>
<div style="height: 30px; width: 190px; float: left; padding-top: 5px;">Assign User To This Plant:</div><div style="height: 30px; padding-top: 5px;"><asp:DropDownList ID="ddlCompanyUsers" runat="server"></asp:DropDownList></div>
<asp:Button ID="btnEditPlantSubmit" runat="server" OnClick="btnEditPlantSubmit_Click" Text="Submit" ValidationGroup="vgEditPlant" /><asp:Label ID="lblPlantEditStatus" runat="server" Text="" Visible="false"></asp:Label>
<asp:HiddenField ID="hidPlantID" runat="server" />
<asp:HiddenField ID="hidPlantName" runat="server" />
<asp:HiddenField ID="hidInsertPlant" runat="server" Value="0" />
</div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>

</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
