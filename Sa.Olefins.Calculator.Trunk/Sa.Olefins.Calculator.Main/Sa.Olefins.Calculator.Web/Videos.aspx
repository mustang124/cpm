﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="Videos.aspx.cs" Inherits="OlefinsCalculator.Videos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="./styles/flashplayer.css" rel="stylesheet" type="text/css" />
    <link href="./styles/page.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        div#wn {
            position: relative;
            width: 614px;
            height: 174px;
            overflow: hidden;
        }

        table.sample td {
            border-width: 0px;
            padding: 3px;
            border-style: inset;
            border-color: #ccc;
            padding-right: 15px;
            padding-left: 15px;
        }
        /*---------- bubble tooltip -----------*/
        a.tt {
            position: relative;
            z-index: 24;
            color: #3CA3FF;
            font-weight: bold;
            text-decoration: none;
        }

            a.tt span {
                display: none;
            }

            /*background:; ie hack, something must be changed in a for ie to execute it*/
            a.tt:hover {
                z-index: 25;
                color: #aaaaff;
                background:;
            }

                a.tt:hover span.tooltip {
                    display: block;
                    position: absolute;
                    top: 0px;
                    left: 0;
                    padding: 15px 0 0 0;
                    width: 200px;
                    color: #993300;
                    text-align: center;
                    filter: alpha(opacity:90);
                    KHTMLOpacity: 0.90;
                    MozOpacity: 0.90;
                    opacity: 0.90;
                }

                a.tt:hover span.top {
                    display: block;
                    padding: 30px 8px 0;
                    background: url(./images/bubble.gif) no-repeat top;
                }

                a.tt:hover span.middle { /* different middle bg for stretch */
                    display: block;
                    padding: 0 8px;
                    background: url(./images/bubble_filler.gif) repeat bottom;
                }

                a.tt:hover span.bottom {
                    display: block;
                    padding: 3px 8px 10px;
                    color: #548912;
                    background: url(./images/bubble.gif) no-repeat bottom;
                }
    </style>
    <link href="skin/blue.monday/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
    <link href="styles/Site.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="lib/jquery.min.js"></script>
    <script type="text/javascript" src="js/jplayer/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="js/add-on/jplayer.playlist.min.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        $(document).ready(function () {

            new jPlayerPlaylist({
                jPlayer: "#jquery_jplayer_1",
                cssSelectorAncestor: "#jp_container_1"
            }, [
            {
                title: "1 - Introduction",
                artist: "",
                m4v: "video/00_Intro_2015 Olefin DCS.mp4"
            },
            {
                title: "2 - Capacity and Facilities",
                artist: "",
                m4v: "video/02_Capacity_2015 Olefin DCS.mp4"
            },
            {
                title: "3 - Material Balance Feedstock and Product Quality",
                artist: "",
                m4v: "video/03_Material Balance_2015 Olefin DCS.mp4"
            },
            {
                title: "4 - Energy Consumption",
                artist: "",
                m4v: "video/04_Energy_2015 Olefin DCS.mp4"
            },
            {
                title: "5 - Maintenance and Reliability",
                artist: "",
                m4v: "video/05_Maintenance Reliability_2015 Olefin DCS.mp4"
            },
            {
                title: "6 - Personnel",
                artist: "",
                m4v: "video/06_Personnel_2015 Olefin DCS.mp4"
            },
            {
                title: "7 - Operating Expenses",
                artist: "",
                m4v: "video/07_OpEx_2015 Olefin DCS.mp4"
            },
            {
                title: "8 - General Plant Data",
                artist: "",
                m4v: "video/08_General Plant Data_2015 Olefin DCS.mp4"
            },
            {
                title: "9 - Olefin Study Input Module",
                artist: "",
                m4v: "video/09_Input_2015 Olefin DCS.mp4"
            },
            {
                title: "10 - Feedstock and Product Pricing",
                artist: "",
                m4v: "video/10_Prices_2015 Olefin DCS.mp4"
            },
            {
                title: "11 - Wrap-Up",
                artist: "",
                m4v: "video/11_Wrap Up_2015 Olefin DCS.mp4"
            }
            ], {
                swfPath: "js/jplayer",
                supplied: "webmv, ogv, m4v, mp4",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: true,
                size: {
                    width: "740px",
                    height: "560px",
                    cssClass: "jp-video-360p"
                }

            });


        });
        //]]>
    </script>

    <div style="margin-top: 0px; margin-left: auto; margin-right: auto; width: 960px;">
        <div id="header-fixed" style="background-color: #FFFFFF; width: 960px;">
            <div style="background-color: #E6EAED; height: 23px; width: 955px; padding-top: 5px; padding-left: 5px; margin-bottom: 10px;">
                <div style="width: 100px; float: left;">DCS Videos</div>
            </div>
            <div class="main">

                <h4>Instructions</h4>
                To view a video, click on the title and the video will automatically begin to play below. To expand the video to full screen, click the <img src="images/fullscreen.png" /> icon.
        <p></p>
                <div id="jp_container_1" class="jp-video jp-video-270p" role="application" aria-label="media player" style="width: 740px; margin-left: auto; margin-right: auto;">
                    <div class="jp-type-playlist">
                        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                        <div class="jp-gui">
                            <div class="jp-video-play">
                                <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
                            </div>
                            <div class="jp-interface">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar"></div>
                                    </div>
                                </div>
                                <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                <div class="jp-controls-holder">
                                    <div class="jp-controls">
                                        <button class="jp-previous" role="button" tabindex="0">previous</button>
                                        <button class="jp-play" role="button" tabindex="0">play</button>
                                        <button class="jp-next" role="button" tabindex="0">next</button>
                                        <button class="jp-stop" role="button" tabindex="0">stop</button>
                                    </div>
                                    <div class="jp-volume-controls">
                                        <button class="jp-mute" role="button" tabindex="0">mute</button>
                                        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                        <div class="jp-volume-bar">
                                            <div class="jp-volume-bar-value"></div>
                                        </div>
                                    </div>
                                    <div class="jp-toggles">
                                        <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                        <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                                        <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
                                    </div>
                                </div>
                                <div class="jp-details">
                                    <div class="jp-title" aria-label="title">&nbsp;</div>
                                </div>
                            </div>
                        </div>
                        <div class="jp-playlist">
                            <ul>
                                <!-- The method Playlist.displayPlaylist() uses this unordered list -->
                                <li>&nbsp;</li>
                            </ul>
                        </div>
                        <div class="jp-no-solution">
                            <span>Update Required</span>
                            To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
	
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>
