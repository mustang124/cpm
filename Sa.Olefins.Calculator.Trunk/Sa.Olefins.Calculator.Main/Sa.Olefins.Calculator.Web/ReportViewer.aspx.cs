﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace OlefinsCalculator
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        int CalcID;
        protected void Page_Load(object sender, EventArgs e)
        {
            

            ((System.Web.UI.HtmlControls.HtmlAnchor)this.Master.FindControl("alogin")).InnerText = "Logout";
            if (!Page.IsPostBack)
            {
                ReportViewer1.Reset();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                LocalReport localReport = ReportViewer1.LocalReport;
                if (Request.IsLocal)
                {
                    localReport.ReportPath = "Report.rdlc";
                    //localReport.ReportPath = "test.rdlc";
                }
                else
                {
                    localReport.ReportPath = "Report.rdlc";
                }

                if (Request.QueryString["enc"] != null)
                    CalcID = Convert.ToInt32(QueryStringModule.Decrypt(Request.QueryString["enc"].ToString()));

                if (Request.QueryString["CalcID"] != null)
                    CalcID = Convert.ToInt32(Request.QueryString["CalcID"].ToString());
                else
                    CalcID = 1;

                try
                {
                    SqlConnection conn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                    using (conn2)
                    {
                        SqlCommand command2 = new SqlCommand("[web].Get_Methodologies", conn2);
                        command2.CommandType = CommandType.StoredProcedure;
                        command2.Parameters.Add(new SqlParameter("@SubmissionId", CalcID));
                        SqlDataAdapter sda = new SqlDataAdapter(command2);
                        DataSet dsMethods = new DataSet();
                        sda.Fill(dsMethods);
                        ddlMethodology.DataSource = dsMethods;
                        ddlMethodology.DataTextField = "MethodologyName";
                        ddlMethodology.DataValueField = "MethodologyID";
                        ddlMethodology.DataBind();
                    }
                }
                catch (Exception ex)
                {
                }

                
                //Have to put in a "pause" because the website is faster than the database.
                //Without this the [web].Get_FactorsAndStandards returns nothing because the database hasn't finished the process yet.
                DateTime dtEnd = DateTime.Now.AddSeconds(5);
                do
                {
                    int x = 5;
                } while (DateTime.Now < dtEnd);

                RenderReport(CalcID);

                //Hide export to PDF option. Doing this because the table gets cut off and split into two tables because it's too wide.
                System.Reflection.FieldInfo info;
                foreach (RenderingExtension extension in localReport.ListRenderingExtensions())
                {
                    if (extension.Name == "PDF")
                    {
                        info = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                        info.SetValue(extension, false);
                    }
                }

            
            }
        
        
        }


        protected void ddlMethodology_SelectedIndexChanged(object sender, EventArgs e)
        {
             if (Request.QueryString["enc"] != null)
                    CalcID = Convert.ToInt32(QueryStringModule.Decrypt(Request.QueryString["enc"].ToString()));

                if (Request.QueryString["CalcID"] != null)
                    CalcID = Convert.ToInt32(Request.QueryString["CalcID"].ToString());
                else
                    CalcID = 1;
                //CalcID = 418;
                RenderReport(CalcID);
        }

        public void RenderReport(int CalcID)
        {
            //CalcID = 418;
            //LocalReport localReport = ReportViewer1.LocalReport;
            //ReportViewer1.Reset();
            //ReportViewer1.LocalReport.DataSources.Clear();

            this.ReportViewer1.Reset();
            this.ReportViewer1.LocalReport.DataSources.Clear();
            this.ReportViewer1.LocalReport.ReportPath = "Report.rdlc";
            
            DataSet dsCalcs = new DataSet();
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            SqlCommand command = new SqlCommand("[web].Get_FactorsAndStandards", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@MethodologyId", Convert.ToInt32(ddlMethodology.SelectedItem.Value)));
            command.Parameters.Add(new SqlParameter("@SubmissionId", CalcID));
            SqlDataAdapter sdaCalcs = new SqlDataAdapter(command);
            using (conn)
            {
                sdaCalcs.Fill(dsCalcs, "Calcs");
            }

            ReportDataSource dsCalcReport = new ReportDataSource();
            dsCalcReport.Name = "dsCalcs";
            dsCalcReport.Value = dsCalcs.Tables["Calcs"];
            //localReport.DataSources.Add(dsCalcReport);
            this.ReportViewer1.LocalReport.DataSources.Add(dsCalcReport);

            

            //localReport.Refresh();
            
        }

        private void GetSalesOrderData(int CalcID, ref DataSet dsSalesOrder)
        {
            string sqlSalesOrder = "SELECT * FROM Calcs WHERE ID = @CalcID";

            SqlConnection connection = new SqlConnection("Data Source=OPTIPLEXJ1B7F60\\SQLEXPRESS;Initial Catalog=OlefinCalculator;Integrated Security=True");

            SqlCommand command = new SqlCommand(sqlSalesOrder, connection);

            command.Parameters.Add(new SqlParameter("CalcID", CalcID));

            SqlDataAdapter salesOrderAdapter = new SqlDataAdapter(command);

            salesOrderAdapter.Fill(dsSalesOrder, "Calcs");
        }
    }
}