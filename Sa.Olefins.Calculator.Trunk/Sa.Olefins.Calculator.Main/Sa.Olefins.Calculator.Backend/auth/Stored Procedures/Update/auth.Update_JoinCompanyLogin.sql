﻿CREATE PROCEDURE [auth].[Update_JoinCompanyLogin]
(
	@JoinId			INT,

	@CompanyId		INT	= NULL,
	@LoginId		INT	= NULL,
	@Active			BIT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	UPDATE	[auth].[JoinCompanyLogin]
	SET		[CompanyId]	= COALESCE(@CompanyId,	[CompanyId]),
			[LoginId]	= COALESCE(@LoginId,	[LoginId]),
			[Active]	= COALESCE(@Active,		[Active])
	WHERE	[JoinId] = @JoinId
		AND(@CompanyId	IS NOT NULL
		OR	@LoginId	IS NOT NULL
		OR	@Active		IS NOT NULL);

	RETURN @JoinId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@JoinId:'			+ CONVERT(VARCHAR, @JoinId))
			+ COALESCE(', @CompanyId:'		+ CONVERT(VARCHAR, @CompanyId),	'')
			+ COALESCE(', @LoginId:'		+ CONVERT(VARCHAR, @LoginId),	'')
			+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO