﻿CREATE TABLE [auth].[JoinCompanyPlant]
(
	[JoinId]				INT					NOT	NULL	IDENTITY (1, 1),

	[CompanyId]				INT					NOT	NULL	CONSTRAINT [FK_JoinCompanyPlant_Companies]			REFERENCES [auth].[Companies] ([CompanyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[PlantId]				INT					NOT	NULL	CONSTRAINT [FK_JoinCompanyPlant_Plants]				REFERENCES [auth].[Plants] ([PlantId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_JoinCompanyPlant_Active]				DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_JoinCompanyPlant_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompanyPlant_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompanyPlant_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompanyPlant_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_JoinCompanyPlant]				PRIMARY KEY NONCLUSTERED([JoinId] ASC),
	CONSTRAINT [UK_JoinCompanyPlant_CompanyPlant]	UNIQUE CLUSTERED ([CompanyId] ASC, [PlantId] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinCompanyPlant_Active]
ON [auth].[JoinCompanyPlant]([CompanyId] ASC, [PlantId] ASC)
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_JoinCompanyPlant_u]
ON [auth].[JoinCompanyPlant]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinCompanyPlant]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinCompanyPlant].[JoinId]	= INSERTED.[JoinId];

END;
GO