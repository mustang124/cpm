﻿CREATE TABLE [auth].[Roles]
(
	[RoleId]				INT					NOT	NULL	IDENTITY (1, 1),

	[RoleTag]				VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Roles_RoleTag]			CHECK ([RoleTag]	<> ''),
															CONSTRAINT [UK_Roles_RoleTag]			UNIQUE NONCLUSTERED ([RoleTag]),
	[RoleName]				VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Roles_RoleName]			CHECK ([RoleName]	<> ''),
															CONSTRAINT [UK_Roles_RoleName]			UNIQUE NONCLUSTERED ([RoleName]),
	[RoleDetail]			VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Roles_RoleDetail]		CHECK ([RoleDetail]	<> ''),
															CONSTRAINT [UK_Roles_RoleDetail]		UNIQUE NONCLUSTERED ([RoleDetail]),
	[RoleLevel]				INT					NOT	NULL	CONSTRAINT [CR_Roles_RoleLevel]			CHECK ([RoleLevel]	>= 0),

	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_Roles_Active]			DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Roles_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Roles_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Roles_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Roles_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Roles]						PRIMARY KEY CLUSTERED ([RoleId] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Roles_Active]
ON [auth].[Roles]([RoleId] ASC)
INCLUDE ([RoleTag], [RoleName], [RoleDetail], [RoleLevel])
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_Roles_u]
ON [auth].[Roles]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[Roles]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[Roles].[RoleId]	= INSERTED.[RoleId];

END;
GO