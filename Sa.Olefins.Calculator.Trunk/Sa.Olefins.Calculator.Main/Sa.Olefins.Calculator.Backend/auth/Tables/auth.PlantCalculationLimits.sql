﻿CREATE TABLE [auth].[PlantCalculationLimits]
(
	[PlantId]				INT					NOT	NULL	CONSTRAINT [FK_PlantCalculationLimits_Plants]							REFERENCES [auth].[Plants] ([PlantId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[CalcsPerPeriod_Count]	INT					NOT	NULL	CONSTRAINT [DF_PlantCalculationLimits_CalcsPerPeriod_Count]				DEFAULT 4,
															CONSTRAINT [CR_PlantCalculationLimits_CalcsPerPeriod_Count_MinIncl_0]	CHECK([CalcsPerPeriod_Count] >= 0),
	[PeriodLen_Days]		INT					NOT	NULL	CONSTRAINT [DF_PlantCalculationLimits_PeriodLen_Days]					DEFAULT 90,
															CONSTRAINT [CR_PlantCalculationLimits_PeriodLen_Days_MinIncl_0]			CHECK([PeriodLen_Days] >= 0),
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_PlantCalculationLimits_Active]							DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_PlantCalculationLimits_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_PlantCalculationLimits_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_PlantCalculationLimits_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_PlantCalculationLimits_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_PlantCalculationLimits]		PRIMARY KEY CLUSTERED ([PlantId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_PlantCalculationLimits_Active]
ON [auth].[PlantCalculationLimits]([PlantId] ASC)
INCLUDE ([CalcsPerPeriod_Count], [PeriodLen_Days])
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_PlantCalculationLimits_u]
ON [auth].[PlantCalculationLimits]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[PlantCalculationLimits]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[PlantCalculationLimits].[PlantId]	= INSERTED.[PlantId];

END;
GO