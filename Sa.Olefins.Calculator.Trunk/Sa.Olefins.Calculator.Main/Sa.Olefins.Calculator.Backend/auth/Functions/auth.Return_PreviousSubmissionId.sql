﻿CREATE FUNCTION [auth].[Return_PreviousSubmissionId]
(
	@PlantId		INT,
	@PreviousRank	INT = 0
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	IF (@PreviousRank IS NULL) SET @PreviousRank = 0;

	SELECT TOP 1
		@Id = t.[SubmissionId]
	FROM (
		SELECT jps.[SubmissionId],
			ROW_NUMBER() OVER(ORDER BY jps.[SubmissionId] ASC) - 1 [PreviousRank] 
		FROM	[auth].[JoinPlantSubmission]	jps
		WHERE	jps.[Active]	= 1
			AND	jps.[PlantId]	= @PlantId
		) t
	WHERE	t.[PreviousRank] = @PreviousRank;

	RETURN @Id;

END;