﻿SELECT
	[c].[CompanyName],
	[l].[LoginTag],
	[a].*
FROM
	[auth].[Companies]			[c]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [jcl].[LoginId]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[LoginId]		= [l].[LoginId]
WHERE
		[c].[CompanyName] = 'Solomon'
	OR	[l].[LoginId] = 6;

UPDATE
	[auth].[LoginsAttributes]
SET
	[RoleId] = 4
WHERE
		[RoleId] = 2
	AND	[eMail] IN ('celia.he@solomononline.com', 'claire.cagnolatti@solomononline.com', 'jeanne.jeng@solomononline.com')

	SELECT * FROM auth.Roles