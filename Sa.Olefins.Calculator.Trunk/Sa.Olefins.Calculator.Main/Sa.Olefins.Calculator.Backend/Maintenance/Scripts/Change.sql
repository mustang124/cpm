﻿PRINT 'Change';

DECLARE @ChangeRoleId TABLE
(
	[Email]				VARCHAR(128)	NOT	NULL	CHECK ([Email]			<> ''),
	[NameFirst]			NVARCHAR(128)	NOT	NULL	CHECK ([NameFirst]		<> ''),
	[NameLast]			NVARCHAR(128)	NOT	NULL	CHECK ([NameLast]		<> ''),
	[CompanyTag]		VARCHAR(42)		NOT	NULL	CHECK ([CompanyTag]		<> ''),
	[CompanyName]		VARCHAR(84)		NOT	NULL	CHECK ([CompanyName]	<> ''),
	[CompanyDetail]		VARCHAR(256)	NOT	NULL	CHECK ([CompanyDetail]	<> ''),
	[PlantName]			VARCHAR(128)	NOT	NULL	CHECK ([PlantName]		<> ''),
	[Refnum]			VARCHAR(128)	NOT	NULL	CHECK ([Refnum]			<> ''),
	[RoleId]			INT				NOT	NULL,

	PRIMARY KEY CLUSTERED([Email] ASC, [PlantName] ASC)
);

INSERT INTO @ChangeRoleId([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId])
SELECT t.[Email], t.[NameFirst], t.[NameLast], t.[CompanyTag], t.[CompanyName], t.[CompanyDetail], t.[PlantName], t.[Refnum], t.[RoleId]
FROM(VALUES
('jelan.kuhn@basf.com', 'Jelan ', 'Kuhn', 'BASF', 'BASF', 'BASF', 'Ludwigshafen', '2015PCH003', 3),
('jelan.kuhn@basf.com', 'Jelan ', 'Kuhn', 'BASF', 'BASF', 'BASF', 'Antwerpen', '2015PCH115', 3),
('song.jinquan@cnoocshell.com', 'Song', 'Jinquan', 'CSPC', 'CSPC', 'CSPC', 'Huizhou', '2015PCH197', 3),
('teresa.l.gerber-1@dupont.com', 'Teresa', 'Gerber', 'DUPONT', 'DUPONT', 'DUPONT', 'Orange', '2015PCH092', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop', '2015PCH011', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop', '2015PCH011', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baton Rouge', '2015PCH012', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baton Rouge', '2015PCH012', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Ndg', '2015PCH013', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Ndg', '2015PCH013', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Fife', '2015PCH015', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Fife', '2015PCH015', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Beaumont', '2015PCH018', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Beaumont', '2015PCH018', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Sarnia', '2015PCH076', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Sarnia', '2015PCH076', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop-X', '2015PCH133', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop-X', '2015PCH133', 3),
('colby.f.jones@exxonmobil.com', 'Colby', 'Jones', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Singapore', '2015PCH159', 3),
('chelsey.m.bromley@exxonmobil.com', 'Chelsey', 'Bromley', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Singapore', '2015PCH159', 3),
('kasidida@scg.co.th', 'Kasidid', 'Asumpinpong', 'MOC', 'MOC', 'MOC', 'Map Tha Phut', '2015PCH228', 3),
('rajesh.rawat@ril.com', 'Rajesh', 'Rawat', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Hazira', '2015PCH122', 3),
('rajesh.rawat@ril.com', 'Rajesh', 'Rawat', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Nagothane', '2015PCH131', 3),
('rajesh.rawat@ril.com', 'Rajesh', 'Rawat', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Vadodara', '2015PCH167', 3),
('rajesh.rawat@ril.com', 'Rajesh', 'Rawat', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Gandhar', '2015PCH168', 3),
('lcasadop@repsol.com', 'Luis', 'Casado Padilla', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Sines', '2015PCH078', 3),
('lcasadop@repsol.com', 'Luis', 'Casado Padilla', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Tarragona', '2015PCH079', 3),
('michael.bacioi@bpge.de', 'Michael', 'Bacioi', 'RUHR OEL', 'RUHR OEL', 'RUHR OEL', 'Gelsenkirchen 3', '2015PCH095', 3),
('michael.bacioi@bpge.de', 'Michael', 'Bacioi', 'RUHR OEL', 'RUHR OEL', 'RUHR OEL', 'Gelsenkirchen 4', '2015PCH096', 3),
('mike.milanowski@us.sasol.com', 'Mike', 'Milanowski', 'SASOL NA', 'SASOL NA', 'SASOL NA', 'Westlake', '2015PCH106', 3),
('lukas.gasparovic@slovnaft.sk', 'Lukas', 'Gasparovic', 'SLOVNAFT', 'SLOVNAFT', 'SLOVNAFT', 'Bratislava', '2015PCH207', 3),
('takashi.yanagi@tonengeneral.co.jp', 'Takashi', 'Yanagi', 'TONEN', 'TONEN', 'TONEN', 'Tonen Kawasaki', '2015PCH105', 3)
) t([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId]);

UPDATE [a]
SET
	[RoleId]	= [x].[RoleId]
FROM
	@ChangeRoleId				[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
		AND	[a].[RoleId]		<> [x].[RoleId]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

--	Add New

DECLARE @ChangeCompanyName TABLE
(
	[FrCompanyName]		VARCHAR(42)		NOT	NULL	CHECK ([FrCompanyName]		<> ''),

	[ToCompanyName]		VARCHAR(42)		NOT	NULL	CHECK ([ToCompanyName]		<> ''),
													UNIQUE([ToCompanyName]		ASC),

	PRIMARY KEY CLUSTERED([FrCompanyName] ASC)
);

INSERT INTO @ChangeCompanyName([FrCompanyName], [ToCompanyName])
SELECT t.[FrCompanyName], t.[ToCompanyName]
FROM(VALUES
	('SAMSUNG TOTAL', 'HANWHA TOTAL'),
	('TVK', 'MOL PETROCHEMICALS')
	) t([FrCompanyName], [ToCompanyName]);

UPDATE [c]
SET
	[CompanyName]	= [x].[ToCompanyName]
FROM
	@ChangeCompanyName	[x]
INNER JOIN
	[auth].[Companies]	[c]
		ON	[c].[CompanyName]	= [x].[FrCompanyName];

INSERT INTO [dim].[Company_LookUp]([CompanyTag], [CompanyName], [CompanyDetail])
SELECT DISTINCT
	[x].[ToCompanyName],
	[x].[ToCompanyName],
	[x].[ToCompanyName]
FROM
	@ChangeCompanyName			[x]
LEFT OUTER JOIN
	[dim].[Company_LookUp]		[l]
		ON	[l].[CompanyName]	= [x].[ToCompanyName]
WHERE
	[l].[CompanyId]	IS NULL;

UPDATE	[c]
SET
	[CompanyRefId] = [l].[CompanyId]
FROM
	@ChangeCompanyName			[x]
INNER JOIN
	[dim].[Company_LookUp]		[l]
		ON	[l].[CompanyName]	= [x].[ToCompanyName]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[ToCompanyName]
WHERE
	[c].[CompanyRefId]	<> [l].[CompanyId];

INSERT INTO [auth].[Logins]([LoginTag])
SELECT DISTINCT
	[i].[Email]
FROM 
	@ChangeRoleId		[i]
LEFT OUTER JOIN
	[auth].[Logins]	[l]
		ON	[i].[Email]	= [l].[LoginTag]
WHERE
	[l].[LoginId] IS NULL;

INSERT INTO [auth].[LoginsAttributes]([LoginId], [NameLast], [NameFirst], [eMail], [RoleId])
SELECT DISTINCT
	[l].[LoginId],
	[i].[NameLast],
	[i].[NameFirst],
	[i].[Email],
	[i].[RoleId]
FROM
	[auth].[Logins]				[l]
INNER JOIN
	@ChangeRoleId					[i]
		ON	[i].[Email] = [l].[LoginTag]
LEFT OUTER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[LoginId]	= [l].[LoginId]
WHERE
	[a].[LoginId] IS NULL;

INSERT INTO [auth].[LoginsPassWords]([LoginId], [pSalt], [pWord])
SELECT 
	[l].[LoginId],
	0x2D228C933876329B03D86FF48431EF558C6F23B83BBA0037291D03E956037EC93835795D7F139E78E356E1F1210D52BA610B2054297B01C564C01F92CFD95EC67F6FD23D8CB645F3A0CCE866B0A336DD04379676EC371D37E76ED5A90529EDBA64E21326D5E73A9D338BE17DF6E87353B6378DA2C06B150D82A61B85C0BE34AC1D73C3B997651BED1A1745439CFAF1F9666D90105C09F31EF48CC3B7987A2973063DE784C8ED7D3723E761477498C09DAE7692A7EA72CF9E9E9E48527A4745267D72228C426789BC5E7DC124965EBD2308B2512AD1B9ECFEF9B69DDC9B94324A194834E1E8185363A9FF3D79503ECC1609922670E7D56D39F0770648C12F8BEF8CF1B0E10B7EEDFB5DAEE6820F8AC0D86A5828B794CE525517F992896A9A6D8958C6C70A834526D4FF25D101F28D4BB410D361667DE3BD761B30E1F570CD4CC8EA386401E409C24E3567A7A779B1376B72EBA15DD8B3CA88AE1DC6D4CFB6A3C2A912947EB336B380F2D2EA52D8804C06CA7BBB6E00DB1D4F1E589E443CC50BB5F1C7C9524C56D21A44AF1AA4E22E68B092ECC6039C2E2554E4918B0D366AD6B674CE4038291A727BD26FED2659C7844A19941CDF786CE953FCD96865AB7FA1D4433B52BB7695561E311A0C1D739D3039F04378B96B2972F675A7D73840A7198629D103CD26449BAAA9230AEDB043505D8CA365982F6548B85F1044B91C61C0FE,
	0xE4FF5A0165CDEFBC1961B796EEE0F33A2710AD184EA0A24C135C113EDA5B7995C902395460A33DB2EFEA1FBC7DE283B068B8E5FE013DEB9961921A05C6941F44
FROM
	[auth].[Logins]				[l]
LEFT OUTER JOIN
	[auth].[LoginsPassWords]	[p]
		ON	[p].[LoginId]	= [l].[LoginId]
WHERE [p].[LoginId] IS NULL;

INSERT INTO [auth].[JoinCompanyLogin]([CompanyId], [LoginId])
SELECT DISTINCT
	[c].[CompanyId],
	[l].[LoginId]
FROM
	@ChangeRoleId					[i]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [i].[CompanyName]
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginTag]		= [i].[Email]
LEFT OUTER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [l].[LoginId]
WHERE
	[jcl].[LoginId]	IS NULL;

INSERT INTO [auth].[JoinPlantLogin]([PlantId], [LoginId])
SELECT DISTINCT
	[p].[PlantId],
	[l].[LoginId]
FROM @ChangeRoleId				[i]
INNER JOIN
	[auth].[Plants]			[p]
		ON	[p].[PlantName]	= [i].[PlantName]
INNER JOIN
	[auth].[Logins]			[l]
		ON	[l].[LoginTag]	= [i].[Email]
LEFT OUTER JOIN
	[auth].[JoinPlantLogin]	[jpl]
		ON	[jpl].[PlantId]		= [p].[PlantId]
		AND	[jpl].[LoginId]		= [l].[LoginId]
WHERE
	[jpl].[LoginId]	IS NULL;