﻿CREATE TABLE [ante].[NicenessRanges]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_NicenessRanges_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_NicenessRanges_Stream_LookUp]				REFERENCES [dim].[Stream_LookUp] ([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[HistoricalMin]			FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessRanges_HistoricalMin_MinIncl_0.0]	CHECK([HistoricalMin]	>= 0.0),
	[HistoricalMax]			FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessRanges_HistoricalMax_MinIncl_0.0]	CHECK([HistoricalMax]	>= 0.0),
															CONSTRAINT [CV_NicenessRanges_Historical]					CHECK([HistoricalMin]	<= [HistoricalMax]),

	[YieldMin]				FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessRanges_YieldMin_MinIncl_0.0]			CHECK([YieldMin]		>= 0.0),
	[YieldMax]				FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessRanges_YieldMax_MinIncl_0.0]			CHECK([YieldMax]		>= 0.0),
															CONSTRAINT [CV_NicenessRanges_Yield]						CHECK([YieldMin]		<= [YieldMax]),
	
	[EnergyMin]				FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessRanges_EnergyMin_MinIncl_0.0]		CHECK([EnergyMin]		>= 0.0),
	[EnergyMax]				FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessRanges_EnergyMax_MinIncl_0.0]		CHECK([EnergyMax]		>= 0.0),
															CONSTRAINT [CV_NicenessRanges_Energy]						CHECK([EnergyMin]		<= [EnergyMax]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_NicenessRanges_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessRanges_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessRanges_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessRanges_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_NicenessRanges]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_NicenessRanges_u]
ON [ante].[NicenessRanges]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessRanges]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[NicenessRanges].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[NicenessRanges].[StreamId]		= INSERTED.[StreamId];

END;
GO