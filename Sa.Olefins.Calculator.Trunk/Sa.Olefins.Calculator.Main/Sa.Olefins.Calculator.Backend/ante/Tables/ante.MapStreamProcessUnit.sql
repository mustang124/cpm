﻿CREATE TABLE [ante].[MapStreamProcessUnit]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapStreamProcessUnit_Methodology]		REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_MapStreamProcessUnit_Stream_LookUp]		REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ProcessUnitId]			INT					NOT NULL	CONSTRAINT [FK_MapStreamProcessUnit_ProcessUnit]		REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapStreamProcessUnit_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamProcessUnit_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamProcessUnit_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamProcessUnit_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapStreamProcessUnit]	PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapStreamProcessUnit_u]
ON [ante].[MapStreamProcessUnit]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapStreamProcessUnit]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapStreamProcessUnit].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapStreamProcessUnit].[StreamId]		= INSERTED.[StreamId];

END;
GO