﻿CREATE TABLE [ante].[MapFactorHydroTreater]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapFactorHydroTreater_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[HydroTreaterTypeId]	INT					NOT	NULL	CONSTRAINT [FK_MapFactorHydroTreater_HydroTreater_LookUp]		REFERENCES [dim].[HydroTreaterType_LookUp]([HydroTreaterTypeId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_MapFactorHydroTreater_Factor_LookUp]				REFERENCES [dim].[Factor_LookUp] ([FactorId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapFactorHydroTreater_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorHydroTreater_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorHydroTreater_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorHydroTreater_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapFactorHydroTreater]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapFactorHydroTreater_u]
ON [ante].[MapFactorHydroTreater]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorHydroTreater].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorHydroTreater].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId];

END;
GO
