﻿CREATE TABLE [ante].[MapFactorFeedClass]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapFactorFeedClass_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FeedClassId]			INT					NOT	NULL	CONSTRAINT [FK_MapFactorFeedClass_FeedClass_LookUp]		REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_MapFactorFeedClass_Factor_LookUp]		REFERENCES [dim].[Factor_LookUp] ([FactorId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapFactorFeedClass_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorFeedClass_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorFeedClass_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorFeedClass_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapFactorFeedClass]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FeedClassId] ASC, [FactorId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapFactorFeedClass_u]
ON [ante].[MapFactorFeedClass]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorFeedClass]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorFeedClass].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorFeedClass].[FeedClassId]	= INSERTED.[FeedClassId]
		AND	[ante].[MapFactorFeedClass].[FactorId]		= INSERTED.[FactorId];

END;
GO
