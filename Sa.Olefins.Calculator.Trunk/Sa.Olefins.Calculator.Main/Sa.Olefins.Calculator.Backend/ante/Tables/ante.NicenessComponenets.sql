﻿CREATE TABLE [ante].[NicenessComponents]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_NicenessComponents_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_NicenessComponents_Component_LookUp]			REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[Coefficient]			FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessComponents_Coefficient_MinIncl_0.0]	CHECK([Coefficient]	>= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_NicenessComponents_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessComponents_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessComponents_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessComponents_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_NicenessComponents]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [ante].[t_NicenessComponents_u]
ON [ante].[NicenessComponents]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessComponents]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[NicenessComponents].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[NicenessComponents].[ComponentId]	= INSERTED.[ComponentId];

END;
GO