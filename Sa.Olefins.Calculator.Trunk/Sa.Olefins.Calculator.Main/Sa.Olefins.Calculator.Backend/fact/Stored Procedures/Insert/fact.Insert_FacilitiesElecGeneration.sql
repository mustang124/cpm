﻿CREATE PROCEDURE [fact].[Insert_FacilitiesElecGeneration]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[FacilitiesElecGeneration]([SubmissionId], [FacilityId], [Capacity_MW])
	SELECT
		e.[SubmissionId],
		e.[FacilityId],
		e.[Capacity_MW]
	FROM [stage].[FacilitiesElecGeneration]		e
	INNER JOIN [stage].[Facilities]				f
		ON	f.[SubmissionId]	= e.[SubmissionId]
		AND	f.[FacilityId]		= e.[FacilityId]
		AND	f.[Unit_Count]		> 0
	WHERE	e.[SubmissionId]	= @SubmissionId
		AND	e.[Capacity_MW]		> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;