﻿CREATE TABLE [fact].[Facilities]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Submissions]				REFERENCES [fact].[Submissions]([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Facility_LookUp]			REFERENCES [dim].[Facility_LookUp]([FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Unit_Count]			INT					NOT	NULL	CONSTRAINT [CR_Facility_Unit_Count_MinIncl_0]					CHECK([Unit_Count] >= 0),

	[_HasUnit_Bit]			AS CONVERT(BIT, CASE WHEN [Unit_Count] > 0 THEN 1 ELSE 0 END, 0)
							PERSISTED			NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Facility_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Facilities]					PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [fact].[t_Facility_u]
ON [fact].[Facilities]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Facilities]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[Facilities].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[fact].[Facilities].[FacilityId]		= INSERTED.[FacilityId];

END;
GO

