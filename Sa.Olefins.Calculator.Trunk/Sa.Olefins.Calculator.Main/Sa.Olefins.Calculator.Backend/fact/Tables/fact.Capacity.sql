﻿CREATE TABLE [fact].[Capacity]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_Capacity_Submissions]					REFERENCES [fact].[Submissions]([SubmissionId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_Capacity_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp]([StreamId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Capacity_kMT]			FLOAT					NULL	CONSTRAINT [CR_Capacity_Quantity_kMT_MinIncl_0.0]		CHECK([Capacity_kMT] >= 0.0),
	[StreamDay_MTSD]		FLOAT				NOT	NULL	CONSTRAINT [CR_Capacity_StreamDay_MTSD_MinIncl_0.0]		CHECK([StreamDay_MTSD] >= 0.0),
	[Record_MTSD]			FLOAT					NULL	CONSTRAINT [CR_Capacity_Record_MTd_MinIncl_0.0]			CHECK([Record_MTSD] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Capacity_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Capacity]					PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamId] ASC)
);
GO

CREATE TRIGGER [fact].[t_Capacity_u]
ON [fact].[Capacity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Capacity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[Capacity].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[fact].[Capacity].[StreamId]		= INSERTED.[StreamId];

END;
GO