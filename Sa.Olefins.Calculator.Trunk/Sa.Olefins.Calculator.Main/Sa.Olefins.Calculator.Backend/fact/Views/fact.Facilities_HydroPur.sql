﻿CREATE VIEW [fact].[Facilities_HydroPur]
WITH SCHEMABINDING
AS
SELECT
	f.[SubmissionId],
	b.[FacilityId],
	SUM(f.[Unit_Count])			[Unit_Count],
	COUNT_BIG(*)				[Items]
FROM [fact].[Facilities]			f
INNER JOIN [dim].[Facility_Bridge]	b
	ON	b.[DescendantId] = f.[FacilityId]
WHERE	b.[FacilityId] = 53
GROUP BY
	f.[SubmissionId],
	b.[FacilityId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_Facilities_HydroPur]
ON [fact].[Facilities_HydroPur]([SubmissionId] ASC, [FacilityId] ASC);
GO

CREATE INDEX [IX_Facilities_HydroPur]
ON [fact].[Facilities_HydroPur]([SubmissionId] ASC, [FacilityId] ASC)
INCLUDE([Unit_Count]);
GO