﻿PRINT 'INSERT INTO [dim].[Message_LookUp]([MessageTag], [MessageName], [MessageDetail])';

INSERT INTO [dim].[Message_LookUp]([MessageTag], [MessageName], [MessageDetail])
SELECT [MessageTag], [MessageName], [MessageDetail]
FROM (
	VALUES
		('SumbissionNameDuplicate', 'Duplicate submission name.', 'Duplicate submission name.'),
		('SumbissionDateOrder', 'Submission dates are out of order.', 'Submission dates are out of order.'),
		('SumbissionDuration', 'Data Duration is short.', 'The duration between the dates covered for the submission is short.'),

		('CapacityProduction', 'Production capacity is not entered.', 'Production capacity is not entered.'),
		('CapacityEthylene', 'Ethylene production capacity is not entered.', 'Ethylene production capacity is not entered.'),
		('CapacityPropylene', 'Propylene production capacity is not entered.', 'Propylene production capacity is not entered.'),
		('CapacityTotal', 'Total capacity is less than Ethylene capacity.', 'Total capacity is less than Ethylene capacity.'),

		('FacilityTrainCount', 'Train count is not equal to or greater than 1.', 'Train count is not equal to or greater than 1.'),

		('FacilityFractionatorAtt', 'Fractionator attributes not are entered, but the plant has a fractionator.', 'Fractionator attributes not are entered, but the plant has a fractionator.'),
		('FacilityFractionatorAttCount', 'The plant does not have a fractionator, but fractionator attributes are entered.', 'The plant does not have a fractionator, but fractionator attributes are entered'),
		('FacilityBoilHPAtt', 'High pressue boiler attributes not are entered, but the plant has a high pressue boiler.', 'High pressue boiler attributes not are entered, but the plant has a high pressue boiler.'),
		('FacilityBoilHPAttCount', 'The plant does not have a high pressue boiler, but high pressue boiler attributes are entered.', 'The plant does not have a high pressue boiler, but high pressue boiler attributes are entered'),
		('FacilityBoilLPAtt', 'Low pressue boiler attributes not are entered, but the plant has a low pressue boiler.', 'Low pressue boiler attributes not are entered, but the plant has a low pressue boiler.'),
		('FacilityBoilLPAttCount', 'The plant does not have a low pressue boiler, but low pressue boiler attributes are entered.', 'The plant does not have a low pressue boiler, but low pressue boiler attributes are entered'),
		('FacilityElecGenAtt', 'Electric power generator attributes not are entered, but the plant has a electric power generator.', 'Electric power generator attributes not are entered, but the plant has a electric power generator.'),
		('FacilityElecGenAttCount', 'The plant does not have a electric power generator, but electric power generator attributes are entered.', 'The plant does not have a electric power generator, but electric power generator attributes are entered'),
		('FacilityTowerPyroGasHTAtt', 'Pyrolysis gasoline hydrotreater attributes not are entered, but the plant has a pyrolysis gasoline hydrotreater.', 'Pyrolysis gasoline hydrotreater attributes not are entered, but the plant has a pyrolysis gasoline hydrotreater.'),
		('FacilityTowerPyroGasHTAttCount', 'The plant does not have a pyrolysis gasoline hydrotreater, but pyrolysis gasoline hydrotreater attributes are entered.', 'The plant does not have a pyrolysis gasoline hydrotreater, but pyrolysis gasoline hydrotreater are entered'),

		('StreamBalance', 'Streams do not mass balance.', 'Streams do not mass balance.'),
		('StreamQuantityComp', 'Stream does not have a quantity.', 'Stream does not have a quantity.'),
		('StreamComposition', 'Composition does not add to 100.0%.', 'Composition does not add to 100.0%.'),

		('StreamRecoveryPcnt', 'Stream does not have recovery percent between 0.0% and 100.0%.', 'Stream does not have recovery percent between 0.0% and 100.0%.'),
		('StreamRecoveryPcntQty', 'Stream does not have recovery quantity.', 'Stream does not have recovery quantity.')
	)t ([MessageTag], [MessageName], [MessageDetail]);

PRINT 'INSERT INTO [dim].[Message_Attributes]([MessageId], [SeverityId])';

INSERT INTO [dim].[Message_Attributes]([MessageId], [SeverityId])
SELECT [MessageId], [SeverityId]
FROM (
	VALUES
		([dim].[Return_MessageId]('SumbissionNameDuplicate'),			[dim].[Return_SeverityId]('Critical')),
		([dim].[Return_MessageId]('SumbissionDateOrder'),				[dim].[Return_SeverityId]('Critical')),
		([dim].[Return_MessageId]('SumbissionDuration'),				[dim].[Return_SeverityId]('Warning')),

		([dim].[Return_MessageId]('CapacityProduction'),				[dim].[Return_SeverityId]('Critical')),
		([dim].[Return_MessageId]('CapacityEthylene'),					[dim].[Return_SeverityId]('Warning')),
		([dim].[Return_MessageId]('CapacityPropylene'),					[dim].[Return_SeverityId]('Warning')),
		([dim].[Return_MessageId]('CapacityTotal'),						[dim].[Return_SeverityId]('Critical')),

		([dim].[Return_MessageId]('FacilityTrainCount'),				[dim].[Return_SeverityId]('Critical')),

		([dim].[Return_MessageId]('FacilityFractionatorAtt'),			[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityFractionatorAttCount'),		[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityBoilHPAtt'),					[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityBoilHPAttCount'),			[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityBoilLPAtt'),					[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityBoilLPAttCount'),			[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityElecGenAtt'),				[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityElecGenAttCount'),			[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityTowerPyroGasHTAtt'),			[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('FacilityTowerPyroGasHTAttCount'),	[dim].[Return_SeverityId]('Error')),

		([dim].[Return_MessageId]('StreamBalance'),						[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('StreamQuantityComp'),				[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('StreamComposition'),					[dim].[Return_SeverityId]('Error')),

		([dim].[Return_MessageId]('StreamRecoveryPcnt'),				[dim].[Return_SeverityId]('Error')),
		([dim].[Return_MessageId]('StreamRecoveryPcntQty'),				[dim].[Return_SeverityId]('Error'))
	) t([MessageId], [SeverityId]);

INSERT INTO [dim].[Message_Language]([LanguageId], [MessageId], [DisplayName], [DisplayDetail], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection])
SELECT [LanguageId], [MessageId], [DisplayName], [DisplayDetail], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection]
FROM (
	VALUES
		(1033, [dim].[Return_MessageId]('SumbissionNameDuplicate'),	NULL, NULL, 'Submission', 'Name', 'The submission name, (@(0)), has already been used.', 'Enter a unique submission name for this plant.'),
		(1033, [dim].[Return_MessageId]('SumbissionDateOrder'),		NULL, NULL, 'Submission', 'Dates', 'The start date, @(0), is after the end date, @(1).', 'Verify the start date is before the end date.'),
		(1033, [dim].[Return_MessageId]('SumbissionDuration'),		NULL, NULL, 'Submission', 'Dates', 'The duration between the start date, @(0), and the end date, @(1), is @(2) days.', 'Verify the @(0) day duration is correct.'),

		(1033, [dim].[Return_MessageId]('CapacityProduction'),		NULL, NULL, 'Capacity', 'Production', 'Production capacity has not been entered.', 'Enter production capacity for Ethylene and/or Propylene.'),
		(1033, [dim].[Return_MessageId]('CapacityEthylene'),		NULL, NULL, 'Capacity', 'Ethylene', ' Ethylene production capacity has not been entered.', 'Enter production capacity for Ethylene.'),
		(1033, [dim].[Return_MessageId]('CapacityPropylene'),		NULL, NULL, 'Capacity', 'Propylene', 'Propylene production capacity has not been entered.', 'Enter production capacity for Propylene.'),
		(1033, [dim].[Return_MessageId]('CapacityTotal'),			NULL, NULL, 'Capacity', 'Production', 'Total capacity, @(0), is less than Ethylene capacity, @(1).', 'Verify the total capacity is greater than or equal to @(0).'),

		(1033, [dim].[Return_MessageId]('FacilityTrainCount'),				NULL, NULL, 'Facility', 'TrainCount', 'The plant must have at least 1 train.', 'Enter at least 1 train.'),
		(1033, [dim].[Return_MessageId]('FacilityFractionatorAtt'),			NULL, NULL, 'Facility', 'Fractionator', 'The plant has @(0) fractionator(s), but no attributes.', 'Enter fractionator attributes or delete the fractionator.'),
		(1033, [dim].[Return_MessageId]('FacilityFractionatorAttCount'),	NULL, NULL, 'Facility', 'Fractionator', 'Fractionator attributes have been entered, but no fractionator has been entered.', 'Enter the number of fractionators or delete the fractionator attributes.'),
		(1033, [dim].[Return_MessageId]('FacilityBoilHPAtt'),				NULL, NULL, 'Facility', 'BoilHP', 'The plant has @(0) high pressure boiler(s), but no attributes.', 'Enter high pressure boiler attributes or delete the high pressure boiler.'),
		(1033, [dim].[Return_MessageId]('FacilityBoilHPAttCount'),			NULL, NULL, 'Facility', 'BoilHP', 'High pressure boiler attributes have been entered, but no high pressure boiler has been entered.', 'Enter the number of high pressure boilers or delete the high pressure boiler attributes.'),
		(1033, [dim].[Return_MessageId]('FacilityBoilLPAtt'),				NULL, NULL, 'Facility', 'BoilLP', 'The plant has @(0) low pressure boiler(s), but no attributes.', 'Enter low pressure boiler attributes or delete the low pressure boiler.'),
		(1033, [dim].[Return_MessageId]('FacilityBoilLPAttCount'),			NULL, NULL, 'Facility', 'BoilLP', 'Low pressure boiler attributes have been entered, but no low pressure boiler has been entered.', 'Enter the number of low pressure boilers or delete the low pressure boiler attributes.'),
		(1033, [dim].[Return_MessageId]('FacilityElecGenAtt'),				NULL, NULL, 'Facility', 'ElecGen', 'The plant has @(0) electric power generator(s), but no attributes.', 'Enter electric power generator attributes or delete the electric power generator.'),
		(1033, [dim].[Return_MessageId]('FacilityElecGenAttCount'),			NULL, NULL, 'Facility', 'ElecGen', 'Electric power generator attributes have been entered, but no electric power generator has been entered.', 'Enter the number of electric power generators or delete the electric power generator.'),
		(1033, [dim].[Return_MessageId]('FacilityTowerPyroGasHTAtt'),		NULL, NULL, 'Facility', 'TowerPyroGasHT', 'The plant has @(0) pyrolysis gasoline hydrotreater(s), but no attributes.', 'Enter pyrolysis gasoline hydrotreater attributes or delete the pyrolysis gasoline hydrotreater.'),
		(1033, [dim].[Return_MessageId]('FacilityTowerPyroGasHTAttCount'),	NULL, NULL, 'Facility', 'TowerPyroGasHT', 'Pyrolysis gasoline hydrotreater attributes have been entered, but no pyrolysis gasoline hydrotreater has been entered.', 'Enter the number of pyrolysis gasoline hydrotreaters or delete the pyrolysis gasoline hydrotreater.'),

		(1033, [dim].[Return_MessageId]('StreamBalance'),		NULL, NULL, 'Streams', 'Quantity', 'The fresh and supplemental feeds, @(0) kMT, does not balance with production, @(1) kMT.', 'Verify the quantity of feed + supplemental is the same as the production quantity.'),
		(1033, [dim].[Return_MessageId]('StreamQuantityComp'),	NULL, NULL, 'Streams', 'Composition', 'The stream @(0) has composition but no quantity.', 'Enter the composition for @(0) or delete the stream.'),
		(1033, [dim].[Return_MessageId]('StreamComposition'),	NULL, NULL, 'Streams', 'Composition', 'The composition for @(0) adds to @(1)%.', 'Verify the composition for @(0) adds to 100.0%.'),
		(1033, [dim].[Return_MessageId]('StreamRecoveryPcnt'),	NULL, NULL, 'Supplemental Feedstocks', 'Supplemental Recovery', '@(0) does not have an estimated recovery percent.', 'Enter an estimated recovery percent for @(0).'),
		(1033, [dim].[Return_MessageId]('StreamRecoveryPcntQty'),	NULL, NULL, 'Supplemental Feedstocks', 'Supplemental Recovery', '@(0) does not have a quanitty.', 'Enter an quantity for @(0).')
	) t ([LanguageId], [MessageId], [DisplayName], [DisplayDetail], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection]);
