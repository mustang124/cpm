﻿PRINT 'INSERT INTO [ante].[Factors]([MethodologyId], [StandardId], [FactorId], [Coefficient], [Exponent], [ValueMin], [ValueMax])';

INSERT INTO [ante].[Factors]([MethodologyId], [StandardId], [FactorId], [Coefficient], [Exponent], [ValueMin], [ValueMax])
SELECT t.[MethodologyId], t.[StandardId], t.[FactorId], t.[Coefficient], t.[Exponent], t.[ValueMin], t.[ValueMax]
FROM (VALUES
	--------------------------------------------------------------------------------------------------------------------

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('Ethane'),				305.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('LPG'),					315.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('Liquid'),				335.0, 1.0, NULL, NULL),

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('SuppEthane'),			100.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('SuppLPG'),				140.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('SuppLiquid'),			150.0, 1.0, NULL, NULL),

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('CompEthane'),			210.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('CompLPG'),				175.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('CompLiquid'),			185.0, 1.0, NULL, NULL),

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('TowerDeethanizer'),	1.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('TowerDepropanizer'),	1.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('TowerNAPS'),			0.9, 1.0, NULL, NULL),

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('TowerPyroGasB'),		2.5, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('TowerPyroGasDS'),		1.8, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('TowerPyroGasSS'),		1.5, 1.0, NULL, NULL),

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('HydroPurCryogenic'),	0.5, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('HydroPurMembrane'),	0.4, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('HydroPurPSA'),			0.4, 1.0, NULL, NULL),

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('BoilFiredSteam'),		43.0, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('ElecGen'),				0.15, 1.0, NULL, NULL),

	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('PropyleneAdjCG'),		0.4, 1.0, NULL, NULL),
	(@MethodologyId, dim.Return_StandardId('kEdc'), dim.Return_FactorId('PropyleneAdjRG'),		3.1, 1.0, NULL, NULL),

	--------------------------------------------------------------------------------------------------------------------

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('Ethane'),		158878, -0.9153, 1200, 3000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('LPG'),			16656, -0.5803, 1200, 4000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('Liquid'),			12584, -0.5434, 1200, 5000),

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('SuppEthane'),71495.1, -0.9153, 1200, 3000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('SuppLPG'),7495.2, -0.5803, 1200, 4000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('SuppLiquid'),5662.8, -0.5434, 1200, 5000),

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('CompEthane'),87382.9, -0.9153, 1200, 3000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('CompLPG'),9160.8, -0.5803, 1200, 4000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('CompLiquid'),6921.2, -0.5434, 1200, 4000),

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('TowerDeethanizer'),2.311, -0.19508, 3000, 40000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('TowerDepropanizer'),5.772195, -0.25898, 3000, 30000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('TowerNAPS'),200.6012, -0.66101, 2000, 45000),

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('TowerPyroGasB'),655.799, -0.638, 5000, 20000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('TowerPyroGasDS'),230.1835, -0.4794, 4000, 25000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('TowerPyroGasSS'),752.1633, -0.74774, 6000, 20000),

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('HydroPurCryogenic'),	3.515159, -0.20618, 8000, 122000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('HydroPurMembrane'),	63.09699, -0.56136, 4000, 52000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('HydroPurPSA'),		1.239644, -0.17267, 2000, 89000),

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('BoilFiredSteam'),	171.5, -0.136, 50, 2000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('ElecGen'),0.815, -0.0319, 5000, 100000),

	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('PropyleneAdjCG'),2.95393, -0.07113, 2000, 9000),
	(@MethodologyId, dim.Return_StandardId('PersMaint'), dim.Return_FactorId('PropyleneAdjRG'),49.76948, -0.3374, 2000, 18000),

	--------------------------------------------------------------------------------------------------------------------

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('Ethane'),211955, -0.9153, 1200, 3000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('LPG'),22221, -0.5803, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('Liquid'),16788, -0.5434, 1000, 4000),

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('SuppEthane'),105977.5, -0.9153, 1200, 3000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('SuppLPG'),11110.5, -0.5803, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('SuppLiquid'),8394, -0.5434, 1000, 4000),

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('CompEthane'),105977.5, -0.9153, 1200, 3000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('CompLPG'),11110.5, -0.5803, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('CompLiquid'),8394, -0.5434, 1000, 4000),

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('TowerDeethanizer'),4.091, -0.217, 3000, 40000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('TowerDepropanizer'),424.249, -0.652, 3000, 30000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('TowerNAPS'),215.854, -0.599, 2000, 45000),

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('TowerPyroGasB'),660, -0.638, 5000, 20000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('TowerPyroGasDS'),35, -0.3, 4000, 25000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('TowerPyroGasSS'),550, -0.6, 6000, 20000),

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('HydroPurCryogenic'),3.5, -0.2, 8000, 122000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('HydroPurMembrane'),0.921, -0.088, 4000, 52000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('HydroPurPSA'),7.046, -0.266, 2000, 89000),

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('BoilFiredSteam'),171.5, -0.136, 50, 2000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('ElecGen'),0.815, -0.0319, 5000, 100000),

	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('PropyleneAdjCG'),15.963, -0.202, 2000, 9000),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'), dim.Return_FactorId('PropyleneAdjRG'),8.446, -0.129, 2000, 18000),

	--------------------------------------------------------------------------------------------------------------------

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('Ethane'),44245, -0.7218, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('LPG'),12753, -0.5423, 1200, 4000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('Liquid'),5391, -0.4089, 1200, 4000),

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('SuppEthane'),19910.25, -0.7218, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('SuppLPG'),5738.85, -0.5423, 1200, 4000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('SuppLiquid'),2425.95, -0.4089, 1200, 4000),

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('CompEthane'),24334.75, -0.7218, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('CompLPG'),7014.15, -0.5423, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('CompLiquid'),2965.05, -0.4089, 1000, 4000),

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('TowerDeethanizer'),1.18, -0.14, 3000, 40000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('TowerDepropanizer'),7.141, -0.267, 3000, 30000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('TowerNAPS'),61.738, -0.553, 2000, 45000),

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('TowerPyroGasB'),77, -0.469, 5000, 20000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('TowerPyroGasDS'),33, -0.3, 4000, 25000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('TowerPyroGasSS'),71.824, -0.469, 6000, 20000),

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('HydroPurCryogenic'),1.623853, -0.245, 8000, 122000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('HydroPurMembrane'),0.2742966, -0.033, 4000, 52000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('HydroPurPSA'),3.708, -0.245, 2000, 89000),

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('BoilFiredSteam'),302, -0.15, 50, 2000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('ElecGen'),1.62, -0.0542, 5000, 100000),

	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('PropyleneAdjCG'),300, -0.617, 2000, 9000),
	(@MethodologyId, dim.Return_StandardId('Mes'), dim.Return_FactorId('PropyleneAdjRG'),25.264, -0.3121, 2000, 18000),

	--------------------------------------------------------------------------------------------------------------------

	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('Ethane'),24330, -0.6336, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('LPG'),11037, -0.5012, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('Liquid'),4856, -0.3799, 1000, 4000),

	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('SuppEthane'),11678.4, -0.6336, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('SuppLPG'),5297.76, -0.5012, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('SuppLiquid'),2330.88, -0.3799, 1000, 4000),

	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('CompEthane'),12651.6, -0.6336, 1000, 4000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('CompLPG'),5739.24, -0.5012, 1200, 4000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('CompLiquid'),2525.12, -0.3799, 1200, 4000),

	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('TowerDeethanizer'),1.792, -0.16, 3000, 40000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('TowerDepropanizer'),118.3, -0.493, 3000, 30000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('TowerNAPS'),90.247, -0.554, 2000, 45000),

	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('TowerPyroGasB'),2450, -0.73, 5000, 20000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('TowerPyroGasDS'),35, -0.3, 4000, 25000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('TowerPyroGasSS'),1760.14, -0.732, 6000, 20000),

	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('HydroPurCryogenic'),4.161, -0.215, 8000, 122000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('HydroPurMembrane'),1.075, -0.126, 4000, 52000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('HydroPurPSA'),6.145, -0.27, 2000, 89000),
			
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('BoilFiredSteam'),178, -0.0839, 50, 2000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('ElecGen'),1.74, -0.064, 5000, 100000),

	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('PropyleneAdjCG'),23.232, -0.307, 2000, 9000),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'), dim.Return_FactorId('PropyleneAdjRG'),16.22, -0.2466, 2000, 18000)

	--------------------------------------------------------------------------------------------------------------------
	)t([MethodologyId], [StandardId], [FactorId], [Coefficient], [Exponent], [ValueMin], [ValueMax]);