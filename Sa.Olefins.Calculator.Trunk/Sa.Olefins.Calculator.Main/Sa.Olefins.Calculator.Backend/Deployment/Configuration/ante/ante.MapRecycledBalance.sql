﻿PRINT 'INSERT INTO [ante].[MapRecycledBalance]([MethodologyId], [RecStreamId], [RecComponentId], [BalStreamId], [BalComponentId])';

INSERT INTO [ante].[MapRecycledBalance]([MethodologyId], [RecStreamId], [RecComponentId], [BalStreamId], [BalComponentId])
SELECT t.[MethodologyId], t.[RecStreamId], t.[RecComponentId], t.[BalStreamId], t.[BalComponentId]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('ConcButadiene'),	dim.Return_ComponentId('C4H6'),	NULL,	dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_StreamId('ConcBenzene'),	dim.Return_ComponentId('C6H6'),	NULL,	dim.Return_ComponentId('Other')),
	(@MethodologyId, dim.Return_StreamId('ConcEthylene'),	dim.Return_ComponentId('C2H4'),	NULL,	dim.Return_ComponentId('C2H6')),

	(@MethodologyId, dim.Return_StreamId('RogEthylene'),	dim.Return_ComponentId('C2H4'),	dim.Return_StreamId('RogEthane'),	dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_StreamId('RogPropylene'),	dim.Return_ComponentId('C3H6'),	dim.Return_StreamId('RogPropane'),	dim.Return_ComponentId('C3H8')),

	(@MethodologyId, dim.Return_StreamId('DilButadiene'),	dim.Return_ComponentId('C4H6'),	dim.Return_StreamId('DilButylene'),	dim.Return_ComponentId('C4H8')),
	(@MethodologyId, dim.Return_StreamId('DilBenzene'),		dim.Return_ComponentId('C6H6'),	dim.Return_StreamId('DilMoGas'),	dim.Return_ComponentId('Other')),
	(@MethodologyId, dim.Return_StreamId('DilEthylene'),	dim.Return_ComponentId('C2H4'),	dim.Return_StreamId('DilEthane'),	dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_StreamId('DilPropylene'),	dim.Return_ComponentId('C3H6'),	dim.Return_StreamId('DilPropane'),	dim.Return_ComponentId('C3H8'))
	)t([MethodologyId], [RecStreamId], [RecComponentId], [BalStreamId], [BalComponentId]);