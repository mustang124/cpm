﻿PRINT 'INSERT INTO [ante].[HvcProduction]([MethodologyId], [StreamId], [ComponentId], [LowerLimit_WtPcnt])';

INSERT INTO [ante].[HvcProduction]([MethodologyId], [StreamId], [ComponentId], [LowerLimit_WtPcnt])
SELECT t.[MethodologyId], t.[StreamId], t.[ComponentId], t.[LowerLimit_WtPcnt]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('EthylenePG'),		dim.Return_ComponentId('C2H4'),		 0.0),
	(@MethodologyId, dim.Return_StreamId('EthyleneCG'),		dim.Return_ComponentId('C2H4'),		 0.0),
	(@MethodologyId, dim.Return_StreamId('PropylenePG'),	dim.Return_ComponentId('C3H6'),		 0.0),
	(@MethodologyId, dim.Return_StreamId('PropyleneCG'),	dim.Return_ComponentId('C3H6'),		 0.0),
	(@MethodologyId, dim.Return_StreamId('PropyleneRG'),	dim.Return_ComponentId('C3H6'),		 0.0),
	(@MethodologyId, dim.Return_StreamId('Hydrogen'),		dim.Return_ComponentId('H2'),		 0.0),

	(@MethodologyId, dim.Return_StreamId('Acetylene'),		dim.Return_ComponentId('AcetMAPD'),	 0.0),	-- Use Default 100%, no 
	(@MethodologyId, dim.Return_StreamId('Benzene'),		dim.Return_ComponentId('C3H6'),		 0.0),	-- Use Default 100%, no 
	(@MethodologyId, dim.Return_StreamId('Butadiene'),		dim.Return_ComponentId('C3H6'),		 0.0),	-- Use Default 100%, no 

	(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('H2'),		85.0),
	(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('AcetMAPD'),	25.0),
	(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C2H4'),		25.0),
	(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C3H6'),		25.0),
	(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C4H6'),		32.0),
	(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C6H6'),		30.0)
	)t([MethodologyId], [StreamId], [ComponentId], [LowerLimit_WtPcnt]);