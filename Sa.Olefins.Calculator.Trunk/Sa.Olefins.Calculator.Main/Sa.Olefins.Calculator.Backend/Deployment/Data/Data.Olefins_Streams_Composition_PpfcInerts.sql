﻿PRINT 'INSERT INTO [stage].[StreamComposition]([SubmissionId], [StreamNumber], [ComponentId], [Component_WtPcnt]) PPFC Inerts';

INSERT INTO [stage].[StreamComposition]([SubmissionId], [StreamNumber], [ComponentId], [Component_WtPcnt])
SELECT
	[c].[SubmissionId],
	[c].[StreamNumber],
	164,
	ROUND(100.0 - SUM(Component_WtPcnt), @RoundWtPcnt)
FROM [stage].[StreamComposition] [c]
WHERE	[c].[StreamNumber]	= 5018
GROUP BY
	[c].[SubmissionId],
	[c].[StreamNumber]
HAVING	SUM(Component_WtPcnt) < 100.0