﻿/*
Run these only when tables are populated from workbook.
*/

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[StandardId])
	+ ', ' + CONVERT(VARCHAR, t.[ProcessUnitId])
	+ ', ' + CONVERT(VARCHAR, t.[StandardValue]) + '),'
FROM [xls].[Standards] t
WHERE	t.[StandardValue] <> 0.0;

--
SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[StreamId])
	+ ', ' + CONVERT(VARCHAR, t.[ComponentId])
	+ ', ' + CONVERT(VARCHAR, t.[StreamDay_MTSD]) 
	+ ', ' + CONVERT(VARCHAR, t.[Recovered_Ann_kMT])
	+ ', ' + CONVERT(VARCHAR, t.[Utilization_Pcnt]) + '),'
FROM [xls].[Capacity] t;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[FacilityId])
	+ ', ' + CONVERT(VARCHAR, t.[FeedStock_BSD]) + '),'
FROM [xls].[FacilitiesFractionator] t
WHERE	t.[FeedStock_BSD] <> 0.0;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[FacilityId])
	+ ', ' + CONVERT(VARCHAR, t.[Quantity_BSD]) + '),'
FROM [xls].[FacilitiesHydroTreater] t
WHERE	t.[Quantity_BSD] <> 0.0;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[FacilityId])
	+ ', ' + CONVERT(VARCHAR, t.[StreamNumber])
	+ ', ' + CONVERT(VARCHAR, t.[StreamId])
	+ ', ' + CONVERT(VARCHAR, t.[ComponentId])
	+ ', ' + CONVERT(VARCHAR, t.[Component_WtPcnt])
	+ ', ' + CONVERT(VARCHAR, t.[Component_kMT])
	+ ', ' + CONVERT(VARCHAR, t.[Utilization_Pcnt])
	+ ', ' + CONVERT(VARCHAR, t.[H2Unit_Count])
	+ ', ' + CONVERT(VARCHAR, t.[Duration_Days]) + '),'
FROM [xls].[HydrogenPurification] t
WHERE	t.[H2Unit_Count] <> 0;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[FacilityId])
	+ ', ' + CONVERT(VARCHAR, t.[Rate_kLbHr]) + '),'
FROM [xls].[FacilitiesBoilers] t
WHERE	t.[Rate_kLbHr] <> 0.0;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[FacilityId])
	+ ', ' + CONVERT(VARCHAR, t.[Capacity_MW]) + '),'
FROM [xls].[FacilitiesElecGeneration] t
WHERE	t.[Capacity_MW] <> 0.0;

--
SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, [dim].[Return_FeedClassId](t.[FeedClassId])) + '),'
FROM [xls].[FeedClass] t;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, [HvcProdDivisor_kMT]) + '),'
FROM [xls].[HvcProductionDivisor] t;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[StreamId])
	+ ', ' + CONVERT(VARCHAR, t.[ComponentId])
	+ ', ' + CONVERT(VARCHAR, t.[Duration_Days])
	+ ', ' + CONVERT(VARCHAR, t.[StreamDay_MTSD])
	+ ', ' + CONVERT(VARCHAR, t.[Component_kMT]) + '),'
FROM [xls].[Utilization] t;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[Energy_kBtu])
	+ ', ' + CONVERT(VARCHAR, t.[HvcYieldDivisor_kLbDay])
	+ ', ' + CONVERT(VARCHAR, t.[StandardEnergy_kBtuLbDay]) + '),'
FROM [xls].[StandardEnergy_PyrolysisBtu] t;

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, t.[ContainedProduct_Dur_kMT])
	+ ', ' + CONVERT(VARCHAR, t.[Recovered_Dur_kMT])
	+ ', ' + CONVERT(VARCHAR, t.[PyrolysisProduct_Dur_kMT])
	+ ', ' + CONVERT(VARCHAR, t.[PyrolysisProduct_Dur_klb])
	+ ', ' + CONVERT(VARCHAR, t.[HvcYieldDivisor_kLbDay]) + '),'
FROM [xls].[StandardEnergy_PyrolysisHvc] t;



SELECT t.* FROM [xls].[HvcProductionDivisor] t;