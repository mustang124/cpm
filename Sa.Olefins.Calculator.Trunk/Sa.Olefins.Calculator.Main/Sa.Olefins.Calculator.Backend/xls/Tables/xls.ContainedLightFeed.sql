﻿CREATE TABLE [xls].[ContainedLightFeed]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_ContainedLightFeed_Refnum]								CHECK([Refnum] <> ''),
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_ContainedLightFeed_Stream_LookUp]						REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_ContainedLightFeed_Component_LookUp]						REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [UK_ContainedLightFeed_ComponentId]							UNIQUE([Refnum], [ComponentId]),
	[ContainedFeed_Dur_kMT]	FLOAT				NOT	NULL	CONSTRAINT [CR_ContainedLightFeed_ContainedFeed_Dur_kMT_MinIncl_0.0]	CHECK([ContainedFeed_Dur_kMT] >= 0.0),
	[ContainedFeed_Ann_kMT]	FLOAT				NOT	NULL	CONSTRAINT [CR_ContainedLightFeed_ContainedFeed_Ann_kMT_MinIncl_0.0]	CHECK([ContainedFeed_Ann_kMT] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ContainedLightFeed]			PRIMARY KEY CLUSTERED ([Refnum] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [xls].[t_ContainedLightFeed_u]
ON [xls].[ContainedLightFeed]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[ContainedLightFeed]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[ContainedLightFeed].[Refnum]		= INSERTED.[Refnum]
		AND	[xls].[ContainedLightFeed].[StreamId]	= INSERTED.[StreamId];

END;
GO