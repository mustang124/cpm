﻿CREATE TABLE [xls].[Capacity]
(
	[Refnum]					VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_Capacity_Refnum]							CHECK([Refnum] <> ''),

	[StreamId]					INT					NOT	NULL	CONSTRAINT [FK_Capacity_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]				INT					NOT	NULL	CONSTRAINT [FK_Capacity_Component_LookUp]				REFERENCES [dim].[Component_LookUp] ([ComponentId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[StreamDay_MTSD]			FLOAT				NOT	NULL	CONSTRAINT [CR_Capacity_StreamDay_MTSD_MinIncl_0.0]		CHECK([StreamDay_MTSD]		>= 0.0),
	[Recovered_Ann_kMT]			FLOAT					NULL	CONSTRAINT [CR_Capacity_Recovered_Ann_kMT_MinIncl_0.0]	CHECK([Recovered_Ann_kMT]	>= 0.0),
	[Utilization_Pcnt]			FLOAT				NOT	NULL	CONSTRAINT [CR_Capacity_Utilization_Pcnt_MinIncl_0.0]	CHECK([Utilization_Pcnt]	>= 0.0),
																CONSTRAINT [CR_Capacity_Utilization_Pcnt_MaxIncl_100.0]	CHECK([Utilization_Pcnt]	<= 110.0),

	[_Supplemental_MTSD]		AS [Recovered_Ann_kMT] / 365.0 * 1000.0
								PERSISTED					,
	[_SupplementalInfer_kMT]	AS [Recovered_Ann_kMT]					/ [Utilization_Pcnt] * 100.0
								PERSISTED					,
	[_SupplementalInfer_MTSD]	AS [Recovered_Ann_kMT] / 365.0 * 1000.0 / [Utilization_Pcnt] * 100.0
								PERSISTED					,
	[_PlantCapacity_MTSD]		AS CONVERT(FLOAT, [StreamDay_MTSD] - COALESCE([Recovered_Ann_kMT], 0.0) / 365.0 * 1000.0 / [Utilization_Pcnt] * 100.0)
								PERSISTED			NOT	NULL,

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Capacity_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Capacity]						PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_Capacity_u]
ON [xls].[Capacity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[Capacity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[Capacity].[Refnum]		= INSERTED.[Refnum];

END;
GO