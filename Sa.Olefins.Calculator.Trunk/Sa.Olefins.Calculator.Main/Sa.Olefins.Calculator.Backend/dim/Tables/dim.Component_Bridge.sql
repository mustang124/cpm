﻿CREATE TABLE [dim].[Component_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Component_Bridge_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_Component_Bridge_ComponentId]			REFERENCES [dim].[Component_LookUp] ([ComponentId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Component_Bridge_Parent_Ancestor]		FOREIGN KEY ([MethodologyId], [ComponentId])
																													REFERENCES [dim].[Component_Parent] ([MethodologyId], [ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_Component_Bridge_DescendantID]			REFERENCES [dim].[Component_LookUp] ([ComponentId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Component_Bridge_Parent_Descendant]		FOREIGN KEY ([MethodologyId], [DescendantId])
																													REFERENCES [dim].[Component_Parent] ([MethodologyId], [ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_Component_Bridge_DescendantOperator]		DEFAULT ('+')
															CONSTRAINT [FK_Component_Bridge_DescendantOperator]		REFERENCES [dim].[Operator] ([Operator])									ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Component_Bridge_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_Bridge_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_Bridge_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_Bridge_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Component_Bridge]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Component_Bridge_u]
ON [dim].[Component_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Component_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Component_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Component_Bridge].[ComponentId]		= INSERTED.[ComponentId]
		AND	[dim].[Component_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
GO