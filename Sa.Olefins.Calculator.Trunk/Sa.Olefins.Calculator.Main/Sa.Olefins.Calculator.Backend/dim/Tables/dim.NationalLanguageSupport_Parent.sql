﻿CREATE TABLE [dim].[NationalLanguageSupport_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_NationalLanguageSupport_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])								ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Lcid]					SMALLINT			NOT	NULL	CONSTRAINT [FK_NationalLanguageSupport_Parent_LookUp_NLS]			REFERENCES [dim].[NationalLanguageSupport_LookUp] ([Lcid])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				SMALLINT			NOT	NULL	CONSTRAINT [FK_NationalLanguageSupport_Parent_LookUp_Parent]		REFERENCES [dim].[NationalLanguageSupport_LookUp] ([Lcid])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_NationalLanguageSupport_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																																REFERENCES [dim].[NationalLanguageSupport_Parent] ([MethodologyId], [Lcid])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Parent_Operator]				DEFAULT ('+')
															CONSTRAINT [FK_NationalLanguageSupport_Parent_Operator]				REFERENCES [dim].[Operator] ([Operator])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Parent_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_NationalLanguageSupport_Parent]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [Lcid] ASC)
);
GO

CREATE TRIGGER [dim].[t_NationalLanguageSupport_Parent_u]
ON [dim].[NationalLanguageSupport_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[NationalLanguageSupport_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[NationalLanguageSupport_Parent].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[NationalLanguageSupport_Parent].[Lcid]			= INSERTED.[Lcid];

END;
GO