﻿CREATE TABLE [dim].[FeedClass_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FeedClassId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_Parent_LookUp_FeedClass]		REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
		
	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_FeedClass_Parent_LookUp_Parent]			REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_FeedClass_Parent_Parent]					FOREIGN KEY ([MethodologyId], [ParentId])
																													REFERENCES [dim].[FeedClass_Parent] ([MethodologyId], [FeedClassId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_FeedClass_Parent_Operator]				DEFAULT ('+')
															CONSTRAINT [FK_FeedClass_Parent_Operator]				REFERENCES [dim].[Operator] ([Operator])								ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FeedClass_Parent_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_Parent_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_Parent_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_Parent_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FeedClass_Parent]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FeedClassId] ASC)
);
GO

CREATE TRIGGER [dim].[t_FeedClass_Parent_u]
ON [dim].[FeedClass_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedClass_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[FeedClass_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[FeedClass_Parent].[FeedClassId]		= INSERTED.[FeedClassId];

END;
GO