﻿CREATE TABLE [dim].[Stream_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Stream_Bridge_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_Stream_Bridge_StreamID]				REFERENCES [dim].[Stream_LookUp] ([StreamId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Stream_Bridge_Parent_Ancestor]		FOREIGN KEY ([MethodologyId], [StreamId])
																												REFERENCES [dim].[Stream_Parent] ([MethodologyId], [StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_Stream_Bridge_DescendantID]			REFERENCES [dim].[Stream_LookUp] ([StreamId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Stream_Bridge_Parent_Descendant]		FOREIGN KEY ([MethodologyId], [DescendantId])
																												REFERENCES [dim].[Stream_Parent] ([MethodologyId], [StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_Stream_Bridge_DescendantOperator]	DEFAULT ('+')
															CONSTRAINT [FK_Stream_Bridge_DescendantOperator]	REFERENCES [dim].[Operator] ([Operator])							ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Stream_Bridge_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_Bridge_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_Bridge_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_Bridge_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Stream_Bridge]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Stream_Bridge_u]
ON [dim].[Stream_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Stream_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Stream_Bridge].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Stream_Bridge].[StreamId]		= INSERTED.[StreamId]
		AND	[dim].[Stream_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
GO