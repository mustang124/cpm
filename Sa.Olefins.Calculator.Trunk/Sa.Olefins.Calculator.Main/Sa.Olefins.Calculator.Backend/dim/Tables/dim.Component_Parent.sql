﻿CREATE TABLE [dim].[Component_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Component_Parent_Methodology]		REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_Component_Parent_LookUp_Components]	REFERENCES [dim].[Component_LookUp] ([ComponentId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_Component_Parent_LookUp_Parent]		REFERENCES [dim].[Component_LookUp] ([ComponentId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Component_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																												REFERENCES [dim].[Component_Parent] ([MethodologyId], [ComponentId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_Component_Parent_Operator]			DEFAULT ('+')
															CONSTRAINT [FK_Component_Parent_Operator]			REFERENCES [dim].[Operator] ([Operator])								ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Component_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_Parent_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Component_Parent]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Component_Parent_u]
ON [dim].[Component_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Component_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Component_Parent].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Component_Parent].[ComponentId]		= INSERTED.[ComponentId];

END;
GO