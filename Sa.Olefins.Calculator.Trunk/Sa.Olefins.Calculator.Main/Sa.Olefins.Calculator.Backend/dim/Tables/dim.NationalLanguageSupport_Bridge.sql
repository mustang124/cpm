﻿CREATE TABLE [dim].[NationalLanguageSupport_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_NationalLanguageSupport_Bridge_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])								ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Lcid]					SMALLINT			NOT	NULL	CONSTRAINT [FK_NationalLanguageSupport_Bridge_NationalLanguageSupportId]	REFERENCES [dim].[NationalLanguageSupport_LookUp] ([Lcid])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_NationalLanguageSupport_Bridge_Parent_Ancestor]				FOREIGN KEY ([MethodologyId], [Lcid])
																																		REFERENCES [dim].[NationalLanguageSupport_Parent] ([MethodologyId], [Lcid])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			SMALLINT			NOT	NULL	CONSTRAINT [FK_NationalLanguageSupport_Bridge_DescendantID]					REFERENCES [dim].[NationalLanguageSupport_LookUp] ([Lcid])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_NationalLanguageSupport_Bridge_Parent_Descendant]			FOREIGN KEY ([MethodologyId], [DescendantId])
																																		REFERENCES [dim].[NationalLanguageSupport_Parent] ([MethodologyId], [Lcid])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Bridge_DescendantOperator]			DEFAULT ('~')
															CONSTRAINT [FK_NationalLanguageSupport_Bridge_DescendantOperator]			REFERENCES [dim].[Operator] ([Operator])										ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Bridge_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Bridge_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Bridge_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_Bridge_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_NationalLanguageSupport_Bridge]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [Lcid] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_NationalLanguageSupport_Bridge_u]
ON [dim].[NationalLanguageSupport_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[NationalLanguageSupport_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[NationalLanguageSupport_Bridge].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[NationalLanguageSupport_Bridge].[Lcid]			= INSERTED.[Lcid]
		AND	[dim].[NationalLanguageSupport_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
GO