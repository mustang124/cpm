﻿CREATE FUNCTION [dim].[Return_HydroTreaterTypeId]
(
	@HydroTreaterTypeTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[HydroTreaterTypeId]
	FROM [dim].[HydroTreaterType_LookUp]	l
	WHERE l.[HydroTreaterTypeTag] = @HydroTreaterTypeTag;

	RETURN @Id;

END;