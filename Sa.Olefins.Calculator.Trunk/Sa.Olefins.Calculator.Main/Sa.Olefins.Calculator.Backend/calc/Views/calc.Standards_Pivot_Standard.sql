﻿CREATE VIEW [calc].[Standards_Pivot_Standard]
WITH SCHEMABINDING
AS
SELECT
	p.[SubmissionId],
	p.[ProcessUnitId],
	p.[kEdc],
	p.[StdEnergy],
	p.[PersMaint],
	p.[PersNonMaint],
	p.[Pers],
	p.[Mes],
	p.[NonEnergy]
FROM (
	SELECT
		v.[SubmissionId],
		v.[ProcessUnitId],
		s.[StandardTag],
		v.[StandardValue]
	FROM	[calc].[Standards_Aggregate]		v WITH (NOEXPAND)
	INNER JOIN [dim].[Standard_LookUp]			s
		ON	s.[StandardId] = v.[StandardId]
	) u
	PIVOT (
		MAX(u.[StandardValue]) FOR u.[StandardTag] IN(
			[kEdc],
			[StdEnergy],
			[Pers],
				[PersMaint],
				[PersNonMaint],
			[Mes],
			[NonEnergy]
		)
	) p;