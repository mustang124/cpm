﻿CREATE VIEW [calc].[Standards_Aggregate]
WITH SCHEMABINDING
AS
SELECT
	c.[MethodologyId],
	c.[SubmissionId],
	s.[StandardId],
	p.[ProcessUnitId],
	SUM(c.[StandardValue])				[StandardValue],
	COUNT_BIG(*)						[Items]
FROM [calc].[Standards]					c
INNER JOIN [dim].[Standard_Bridge]		s
	ON	s.[MethodologyId]	= c.[MethodologyId]
	AND	s.[DescendantId]	= c.[StandardId]
INNER JOIN [dim].[ProcessUnit_Bridge]	p
	ON	p.[MethodologyId]	= c.[MethodologyId]
	AND	p.[DescendantId]	= c.[ProcessUnitId]
GROUP BY
	c.[MethodologyId],
	c.[SubmissionId],
	s.[StandardId],
	p.[ProcessUnitId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_Standards_Aggregate]
ON [calc].[Standards_Aggregate]([MethodologyId] DESC, [SubmissionId] ASC, [StandardId] ASC, [ProcessUnitId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_Standards_Aggregate]
ON [calc].[Standards_Aggregate]([MethodologyId] DESC, [SubmissionId] ASC, [StandardId] ASC, [ProcessUnitId] ASC)
INCLUDE([StandardValue]);
GO

CREATE NONCLUSTERED INDEX [IX_Standards_Aggregate_ProcessUnitId]
ON [calc].[Standards_Aggregate]([StandardId] ASC, [ProcessUnitId] ASC)
INCLUDE([MethodologyId], [SubmissionId], [StandardValue]);
GO