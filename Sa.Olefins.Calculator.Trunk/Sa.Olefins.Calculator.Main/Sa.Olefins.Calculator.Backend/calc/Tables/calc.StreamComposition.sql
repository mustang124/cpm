﻿CREATE TABLE [calc].[StreamComposition]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Submissions]						REFERENCES [fact].[Submissions] ([SubmissionId])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_StreamQuantity]
															FOREIGN KEY([SubmissionId], [StreamNumber])							REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Stream_LookUp]						REFERENCES [dim].[Stream_LookUp] ([StreamId])											ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_StreamComposition_StreamAttributes]
															FOREIGN KEY([SubmissionId], [StreamNumber], [StreamId])				REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber], [StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Component_LookUp]					REFERENCES [dim].[Component_LookUp] ([ComponentId])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[Component_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MinIncl_0.0]		CHECK([Component_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MaxIncl_100.0]	CHECK([Component_WtPcnt] <= 100.0),
	[Component_Dur_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamComposition_Component_Dur_kMT_MinIncl_0.0]		CHECK([Component_Dur_kMT] >= 0.0),
	[Component_Ann_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamComposition_Component_Ann_kMT_MinIncl_0.0]		CHECK([Component_Ann_kMT] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamComposition]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [StreamNumber] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [calc].[t_StreamComposition_u]
ON [calc].[StreamComposition]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StreamComposition]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StreamComposition].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StreamComposition].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[StreamComposition].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[calc].[StreamComposition].[ComponentId]	= INSERTED.[ComponentId];

END;
GO