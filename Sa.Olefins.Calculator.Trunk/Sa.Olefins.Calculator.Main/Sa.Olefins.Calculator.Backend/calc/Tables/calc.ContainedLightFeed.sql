﻿CREATE TABLE [calc].[ContainedLightFeed]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_ContainedLightFeed_Methodology]							REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_ContainedLightFeed_Submissions]							REFERENCES [fact].[Submissions] ([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_ContainedLightFeed_Stream_LookUp]						REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_ContainedLightFeed_Component_LookUp]						REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [UK_ContainedLightFeed_ComponentId]							UNIQUE([MethodologyId], [SubmissionId], [ComponentId]),
	[ContainedFeed_Dur_kMT]	FLOAT				NOT	NULL	CONSTRAINT [CR_ContainedLightFeed_ContainedFeed_Dur_kMT_MinIncl_0.0]	CHECK([ContainedFeed_Dur_kMT] >= 0.0),
	[ContainedFeed_Ann_kMT]	FLOAT				NOT	NULL	CONSTRAINT [CR_ContainedLightFeed_ContainedFeed_Ann_kMT_MinIncl_0.0]	CHECK([ContainedFeed_Ann_kMT] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContainedLightFeed_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ContainedLightFeed]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [StreamId] ASC)
);
GO

CREATE TRIGGER [calc].[t_ContainedLightFeed_u]
ON [calc].[ContainedLightFeed]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[ContainedLightFeed]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[ContainedLightFeed].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[calc].[ContainedLightFeed].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[calc].[ContainedLightFeed].[StreamId]			= INSERTED.[StreamId];

END;
GO