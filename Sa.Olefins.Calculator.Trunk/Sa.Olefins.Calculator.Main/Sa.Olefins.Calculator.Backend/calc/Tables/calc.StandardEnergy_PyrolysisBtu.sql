﻿CREATE TABLE [calc].[StandardEnergy_PyrolysisBtu]
(
	[MethodologyId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_PyrolysisBtu_Methodology]								REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_PyrolysisBtu_Submissions]								REFERENCES [fact].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Energy_kBtu]				FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_Energy_kBtu_MinIncl_0.0]					CHECK([Energy_kBtu] >= 0.0),
	[HvcYieldDivisor_kLbDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_HvcYieldDivisor_kLbDay_MinIncl_0.0]		CHECK([HvcYieldDivisor_kLbDay] >= 0.0),

	[StandardEnergy_kBtuLbDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_StandardEnergy_kBtuLbDay_MinIncl_0.0]	CHECK([StandardEnergy_kBtuLbDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModified]								DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedHost]							DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedUser]							DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedApp]							DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_PyrolysisBtu]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC)
);
GO

CREATE TRIGGER [calc].[t_StandardEnergy_PyrolysisBtu_u]
ON [calc].[StandardEnergy_PyrolysisBtu]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_PyrolysisBtu]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_PyrolysisBtu].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_PyrolysisBtu].[SubmissionId]		= INSERTED.[SubmissionId];

END;
GO