﻿CREATE TABLE [calc].[Utilization]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Utilization_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_Utilization_Submissions]						REFERENCES [fact].[Submissions] ([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_Utilization_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_Utilization_Component_LookUp]				REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Duration_Days]			FLOAT				NOT	NULL	CONSTRAINT [CR_Utilization_Duration_Days_MinIncl_0.0]		CHECK([Duration_Days] >= 0.0),
	[StreamDay_MTSD]		FLOAT				NOT	NULL	CONSTRAINT [CR_Utilization_StreamDay_MTSD_MinIncl_0.0]		CHECK([StreamDay_MTSD] >= 0.0),
	[Component_kMT]			FLOAT				NOT	NULL	CONSTRAINT [CR_Utilization_Component_kMT_MinIncl_0.0]		CHECK([Component_kMT] >= 0.0),

	[_Utilization_Pcnt]		AS CONVERT(FLOAT, [Component_kMT] / [StreamDay_MTSD] / [Duration_Days] * 100000.0)
							PERSISTED			NOT	NULL	CONSTRAINT [CR_Utilization_Utilization_Pcnt_MinIncl_0.0]	CHECK([_Utilization_Pcnt] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Utilization_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Utilization_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Utilization_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Utilization_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Utilization]					PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [ComponentId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Utilization]
ON [calc].[Utilization]([MethodologyId] DESC, [SubmissionId] ASC, [StreamId] ASC);
GO

CREATE TRIGGER [calc].[t_Utilization_u]
ON [calc].[Utilization]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[Utilization]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[Utilization].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[Utilization].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[calc].[Utilization].[ComponentId]		= INSERTED.[ComponentId];

END;
GO