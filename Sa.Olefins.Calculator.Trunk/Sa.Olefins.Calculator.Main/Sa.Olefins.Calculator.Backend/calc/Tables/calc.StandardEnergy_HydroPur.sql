﻿CREATE TABLE [calc].[StandardEnergy_HydroPur]
(
	[MethodologyId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_HydroPur_Methodology]							REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_HydroPur_Submissions]							REFERENCES [fact].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[H2Pur_kScfYear]			FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroPur_H2Pur_kScfYear_MinIncl_0.0]			CHECK([H2Pur_kScfYear] >= 0.0),
	[H2Pur_kScfDay]				FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroPur_H2Pur_kScfDay_MinIncl_0.0]			CHECK([H2Pur_kScfDay] >= 0.0),
	[StandardEnergy_MBtuDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroPur_StandardEnergy_MBtuDay_MinIncl_0.0]	CHECK([StandardEnergy_MBtuDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroPur_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroPur_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_HydroPur]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC)
);
GO

CREATE TRIGGER [calc].[t_StandardEnergy_HydroPur_u]
ON [calc].[StandardEnergy_HydroPur]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_HydroPur]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_HydroPur].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_HydroPur].[SubmissionId]		= INSERTED.[SubmissionId];

END;
GO