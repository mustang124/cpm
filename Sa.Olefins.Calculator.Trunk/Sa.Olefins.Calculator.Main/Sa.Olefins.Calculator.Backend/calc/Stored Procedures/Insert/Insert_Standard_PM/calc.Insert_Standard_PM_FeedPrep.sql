﻿CREATE PROCEDURE [calc].[Insert_Standard_PM_FeedPrep]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId_kEdc			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@StandardId_StdEnergy		INT	= dim.Return_StandardId('StdEnergy');

	DECLARE	@ProcessUnitId				INT	= dim.Return_ProcessUnitId('FeedPrep');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		map.MethodologyId,
		ff.SubmissionId,
		f.StandardId,
		@ProcessUnitId			[ProcessUnitId],
		f.Coefficient * ff.Quantity_kBSD * scu.WwCduScu *
		POWER(calc.MaxValue(f.ValueMin, calc.MinValue(f.ValueMax, ff._Quantity_BSD / CONVERT(FLOAT, ft.Train_Count))), f.Exponent)
		[StandardValue]
	FROM		fact.FacilitiesFractionator			ff
	INNER JOIN	fact.FacilitiesTrains				ft
		ON	ft.SubmissionId		= ff.SubmissionId
	INNER JOIN	ante.MapFactorFracType				map
		ON	map.StreamId		= ff.StreamId
	INNER JOIN	ante.Factors						f
		ON	f.MethodologyId		= map.MethodologyId
		AND	f.FactorId			= map.FactorId
		AND	f.StandardId		NOT IN (@StandardId_kEdc, @StandardId_StdEnergy)
	INNER JOIN ante.FactorsWwCduScu					scu
		ON	scu.MethodologyId	= map.MethodologyId
		AND	scu.StandardId		= f.StandardId
	WHERE	map.MethodologyId	= @MethodologyId
		AND	ff.SubmissionId		= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO