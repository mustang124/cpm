﻿CREATE PROCEDURE [calc].[Insert_HvcProductionDivisor]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY
	
	INSERT INTO [calc].[HvcProductionDivisor]([MethodologyId], [SubmissionId], [HvcProdDivisor_kMT])
	SELECT
		@MethodologyId,
		@SubmissionId		[SubmissionId],
		COALESCE(SUM(c.[Component_Dur_kMT]), 0.0) + COALESCE(SUM(q.[Quantity_kMT]), 0.0)	[HvcProdDivisor_kMT]
	FROM [ante].[HvcProduction]					h
	LEFT OUTER JOIN [calc].[StreamComposition]	c
		ON	c.[MethodologyId]		= h.[MethodologyId]
		AND	c.[SubmissionId]		= @SubmissionId
		AND	c.[StreamId]			= h.[StreamId]
		AND	c.[ComponentId]			= h.[ComponentId]
		AND	c.[Component_WtPcnt]	> h.[LowerLimit_WtPcnt]
	LEFT OUTER JOIN [fact].[StreamQuantity]		q
		ON	q.[SubmissionId]		= @SubmissionId
		AND	q.[StreamId]			= h.[StreamId]
		AND q.[StreamId]			IN (94, 95, 96)
	WHERE	h.[MethodologyId]		= @MethodologyId
		AND(c.[Component_Ann_kMT]	IS NOT NULL
		OR	q.[Quantity_kMT]		IS NOT NULL);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO