﻿CREATE TABLE [stage].[Errors_DataSet]
(
	[ErrorId]				INT					NOT NULL	IDENTITY (1, 1),

	[LanguageId]			SMALLINT			NOT	NULL	CONSTRAINT [DF_Errors_DataSet_LanguageId]			DEFAULT 1033
															CONSTRAINT [FK_Errors_DataSet_NLS_LookUp]			REFERENCES [dim].[NationalLanguageSupport_LookUp]([Lcid])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[MessageId]				INT					NOT NULL	CONSTRAINT [FK_Errors_DataSet_Message_LookUp]		REFERENCES [dim].[Message_LookUp]([MessageId])				ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SeverityId]			INT					NOT	NULL	CONSTRAINT [FK_Errors_DataSet_Severity_LookUp]		REFERENCES [dim].[Severity_LookUp]([SeverityId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[SubmissionId]			INT					NOT	NULL,

	[DisplayName]			NVARCHAR (256)			NULL	CONSTRAINT [CL_Errors_DataSet_MessageName]			CHECK ([DisplayName]		<> ''),
	[DisplayDetail]			NVARCHAR (256)			NULL	CONSTRAINT [CL_Errors_DataSet_MessageDetail]		CHECK ([DisplayDetail]		<> ''),
	[DisplaySection]		NVARCHAR (256)			NULL	CONSTRAINT [CL_Errors_DataSet_MessageSection]		CHECK ([DisplaySection]		<> ''),
	[DisplayLocation]		NVARCHAR (256)			NULL	CONSTRAINT [CL_Errors_DataSet_MessageLocation]		CHECK ([DisplayLocation]	<> ''),
	[DisplayError]			NVARCHAR (512)			NULL	CONSTRAINT [CL_Errors_DataSet_MessageError]			CHECK ([DisplayError]		<> ''),
	[DisplayCorrection]		NVARCHAR (512)			NULL	CONSTRAINT [CL_Errors_DataSet_MessageCorrection]	CHECK ([DisplayCorrection]	<> ''),

	[Notes]					NVARCHAR (MAX)			NULL	CONSTRAINT [CL_Errors_DataSet_Notes]				CHECK ([Notes]				<> ''),

	[tsInserted]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Errors_DataSet_tsInserted]			DEFAULT (sysdatetimeoffset()),
	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Errors_DataSet_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Errors_DataSet_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Errors_DataSet_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Errors_DataSet_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Errors_DataSet]				PRIMARY KEY CLUSTERED([ErrorId] DESC)
);
GO

CREATE TRIGGER [stage].[t_Errors_DataSet_u]
ON [stage].[Errors_DataSet]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[Errors_DataSet]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[Errors_DataSet].[ErrorId]	= INSERTED.[ErrorId];

END;
GO
