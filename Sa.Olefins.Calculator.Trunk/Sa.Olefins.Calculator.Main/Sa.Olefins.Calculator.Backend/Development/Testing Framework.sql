﻿DECLARE @StandardId		INT			= [dim].[Return_StandardId]('_BoilFiredSteam');
DECLARE @ProcessUnitId	INT			= [dim].[Return_ProcessUnitId]('_SuppTot');

DECLARE	@Round			TINYINT		= 5;
DECLARE	@ErrorRelative	FLOAT		= 0.005;
DECLARE	@ErrorAbsolute	FLOAT		= 0.005;

SELECT
	o.Refnum,
	n.Refnum,
	n.MethodologyId,
	n.SubmissionId,
	o.StreamId,
	n.StreamId,
	o.[TestValue],
	n.[TestValue],
	CASE WHEN o.[TestValue] <> 0.0 THEN ABS(o.[TestValue] - n.[TestValue]) / o.[TestValue] * 100.0 END		[ErrorRelative],
	o.[TestValue] - n.[TestValue]								[ErrorAbsolute]
FROM (
	SELECT
		s.[Refnum],
		s.[StreamId],
		s.[_Utilization_Pcnt]		[TestValue]
	FROM [xls].[Utilization]			s
			/* Olefin Capacity is less than Ethylene Capacity	*/
	WHERE	NOT((s.[Refnum]	= '07PCH141')
			OR	(s.[Refnum]	= '11PCH184')
			OR	(s.[Refnum]	= '11PCH23A')
			)
	) o
FULL OUTER JOIN (
	SELECT
		z.[SubmissionName]	[Refnum],
		s.[MethodologyId],
		s.[SubmissionId],
		s.[StreamId],
		s.[_Utilization_Pcnt]		[TestValue]
	FROM [calc].[Utilization]			s
	INNER JOIN [fact].[Submissions]		z
		ON	z.[SubmissionId] = s.[SubmissionId]
	WHERE	s.[StreamId] <> 90
		/* Olefin Capacity is less than Ethylene Capacity	*/
		AND NOT((z.[SubmissionName]	= '07PCH141')
			OR	(z.[SubmissionName]	= '11PCH184')
			OR	(z.[SubmissionName]	= '11PCH23A')
			)
	) n
	ON	n.[Refnum] = o.[Refnum]
	AND	n.[StreamId]	= o.[StreamId]
WHERE	[audit].[ValidateValue](ROUND(o.[TestValue], @Round), ROUND(n.[TestValue], @Round), @ErrorRelative, @ErrorAbsolute, 0) = 0;