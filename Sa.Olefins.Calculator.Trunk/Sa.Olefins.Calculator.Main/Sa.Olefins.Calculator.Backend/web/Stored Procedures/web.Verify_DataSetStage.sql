﻿CREATE PROCEDURE [web].[Verify_DataSetStage]
(
	@SubmissionId			INT,
	@LoginId				INT = NULL,
	@LanguageId				INT = 1033
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@Error_Count	INT;

	DECLARE	@LocaleId		SMALLINT = CONVERT(SMALLINT, @LanguageId);

	EXECUTE	@Error_Count = [stage].[Verify_DataSet] @SubmissionId, @LoginId, @LocaleId;

	RETURN	@Error_Count;

END;