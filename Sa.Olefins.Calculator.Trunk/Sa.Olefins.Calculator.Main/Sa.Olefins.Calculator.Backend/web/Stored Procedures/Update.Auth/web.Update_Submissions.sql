﻿CREATE PROCEDURE [web].[Update_Submissions]
(
	@SubmissionId	INT,
	@Active			BIT = 0
)
AS
BEGIN

	DECLARE @JoinId		INT;
	DECLARE @PlantID	INT;

	SELECT
		@JoinId		= jps.[JoinId],
		@PlantID	= jps.[PlantId]
	FROM [auth].[JoinPlantSubmission] jps WHERE jps.[SubmissionId] = @SubmissionId;

	EXECUTE @JoinId = [auth].[Update_JoinPlantSubmission] @JoinId, @PlantID, @SubmissionId, @Active;

	SELECT @JoinId;
	RETURN @JoinId;

END;