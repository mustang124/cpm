﻿CREATE PROCEDURE [web].[Update_PlantPermissions]
(
	@PlantPermissionId		INT	= NULL,
	@PlantId				INT	= NULL,
	@UserId					INT	= NULL,
	@Active					BIT	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @JoinId		INT = @PlantPermissionId;
	DECLARE @LoginId	INT = @UserId;

	IF EXISTS (SELECT TOP 1 1 FROM [auth].[JoinPlantLogin] jps WHERE jps.[JoinId] = @JoinId)
	BEGIN
		EXECUTE [auth].[Update_JoinPlantLogin] @JoinId, @PlantId, @LoginId, @Active;
	END
	ELSE
	BEGIN

		SELECT @JoinId = jps.[JoinId] FROM [auth].[JoinPlantLogin] jps WHERE jps.[PlantId] = @PlantId AND jps.[LoginId] = @LoginId;

		IF (@JoinId IS NOT NULL)
		BEGIN

			EXECUTE [auth].[Update_JoinPlantLogin] @JoinId, @PlantId, @LoginId, @Active;

		END
		ELSE
		BEGIN
			
			EXECUTE [auth].[Insert_JoinPlantLogin] @PlantId, @LoginId, @Active;

		END

	END;

	SELECT @JoinId;
	RETURN @JoinId;

END;