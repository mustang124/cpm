﻿CREATE PROCEDURE [web].[Process_DataSetStage]
(
	@MethodologyId		INT = NULL,
	@SubmissionId		INT,
	@LoginId			INT = NULL
)
AS
BEGIN

	EXECUTE [calc].[ProcessSubmission] @MethodologyId, @SubmissionId, @LoginId;

END;