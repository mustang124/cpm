﻿CREATE PROCEDURE [web].[Get_UsersByCompany_Active]
(
	@CompanyId		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		ubc.[JoinId],
		ubc.[CompanyId],
		ubc.[LoginId]		[UserId],
		ubc.[CompanyName],
		ubc.[LoginTag]		[UserName],
		ubc.[NameFirst]		[FirstName],
		ubc.[NameLast]		[LastName],
		ubc.[eMail],
		ubc.[_NameComma]	[LastFirst],
		ubc.[_NameFull]		[NameFull]
	FROM [auth].[Get_LoginsByCompany_Active](@CompanyId)	ubc
	ORDER BY
		ubc.[NameLast]	ASC,
		ubc.[NameFirst]	ASC,
		ubc.[eMail]		ASC;

END;