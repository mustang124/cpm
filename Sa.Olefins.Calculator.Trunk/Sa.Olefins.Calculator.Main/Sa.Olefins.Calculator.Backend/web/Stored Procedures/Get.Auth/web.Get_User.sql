﻿CREATE PROCEDURE [web].[Get_User]
(
	@Username	NVARCHAR(254)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LoginTag	NVARCHAR(254) = @Username;

	SELECT
		lbc.[JoinId],
		lbc.[CompanyId],
		lbc.[LoginId]		[UserId],
		lbc.[CompanyName],
		lbc.[LoginTag]		[Username],
		lbc.[NameLast]		[LastName],
		lbc.[NameFirst]		[FirstName],
		lbc.[_NameFull],
		lbc.[_NameComma],
		lbc.[eMail],
		lbc.[RoleId],
		lbc.[RoleLevel]		[SecurityLevelID],
		lbc.[pSalt]			[Salt],
		lbc.[pWord]			[Password]
	FROM [auth].[Get_LoginDetail_Active](@LoginTag)	lbc;

END;