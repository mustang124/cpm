﻿CREATE PROCEDURE [web].[Get_CompanyPlantsUser]
(
	@CompanyId		INT,
	@UserId			INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LoginId	INT = @UserId;

	SELECT
		cpu.[PlantId],
		cpu.[PlantName],
		[Active] = COALESCE(cpu.[Active], 0)
	FROM [auth].[Get_CompanyPlantsUser](@CompanyId, @LoginId) cpu;

END;