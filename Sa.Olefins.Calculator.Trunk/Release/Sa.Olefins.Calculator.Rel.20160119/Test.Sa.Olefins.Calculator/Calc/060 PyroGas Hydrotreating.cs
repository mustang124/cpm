﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _060_Hydrogen_Purification : SqlDatabaseTestClass
    {

        public _060_Hydrogen_Purification()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Hydrotreating()
        {
            SqlDatabaseTestActions testActions = this.HydrotreatingData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Hydrotreating_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_060_Hydrogen_Purification));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Hydrotreating_Results;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Hydrotreating_EmptySet;
            this.HydrotreatingData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Hydrotreating_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Hydrotreating_Results = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Hydrotreating_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // Hydrotreating_TestAction
            // 
            Hydrotreating_TestAction.Conditions.Add(Hydrotreating_Results);
            Hydrotreating_TestAction.Conditions.Add(Hydrotreating_EmptySet);
            resources.ApplyResources(Hydrotreating_TestAction, "Hydrotreating_TestAction");
            // 
            // Hydrotreating_Results
            // 
            Hydrotreating_Results.Enabled = true;
            Hydrotreating_Results.Name = "Hydrotreating_Results";
            Hydrotreating_Results.ResultSet = 1;
            // 
            // Hydrotreating_EmptySet
            // 
            Hydrotreating_EmptySet.Enabled = true;
            Hydrotreating_EmptySet.Name = "Hydrotreating_EmptySet";
            Hydrotreating_EmptySet.ResultSet = 2;
            // 
            // HydrotreatingData
            // 
            this.HydrotreatingData.PosttestAction = null;
            this.HydrotreatingData.PretestAction = null;
            this.HydrotreatingData.TestAction = Hydrotreating_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions HydrotreatingData;
    }
}
