﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.Eii_Support
{
    [TestClass()]
    public class EiiSupport : SqlDatabaseTestClass
    {

        public EiiSupport()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void ContainedProduct_Composite()
        {
            SqlDatabaseTestActions testActions = this.ContainedProduct_CompositeData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Supp()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_SuppData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void HvcProductionDivisor()
        {
            SqlDatabaseTestActions testActions = this.HvcProductionDivisorData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void SupplementalRecovered()
        {
            SqlDatabaseTestActions testActions = this.SupplementalRecoveredData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_PyrolysisHvc_kLb()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_PyrolysisHvc_kLbData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_PyrolysisHvc_klbDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_PyrolysisHvc_klbDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ContainedLightFeed()
        {
            SqlDatabaseTestActions testActions = this.ContainedLightFeedData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_PyrolysisBtu_kBtu()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_PyrolysisBtu_kBtuData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_PyrolysisBtu_kLbDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_PyrolysisBtu_kLbDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_PyrolysisBtu_kBtuLbDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_PyrolysisBtu_kBtuLbDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }










        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ContainedProduct_Composite_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EiiSupport));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ContainedProduct_Composite_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ContainedProduct_Composite_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Supp_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Supp_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Supp_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HvcProductionDivisor_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HvcProductionDivisor_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HvcProductionDivisor_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SupplementalRecovered_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition SupplementalRecovered_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition SupplementalRecovered_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_PyrolysisHvc_kLb_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_PyrolysisHvc_kMT_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_PyrolysisHvc_kMT_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_PyrolysisHvc_klbDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ContainedLightFeed_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ContainedLightFeed_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ContainedLightFeed_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_PyrolysisBtu_kBtu_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_PyrolysisBtu_kLbDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_PyrolysisBtu_kBtu_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_PyrolysisBtu_kBtu_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_PyrolysisBtu_kLbDay_EmptySet;
            this.ContainedProduct_CompositeData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_SuppData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.HvcProductionDivisorData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.SupplementalRecoveredData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_PyrolysisHvc_kLbData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_PyrolysisHvc_klbDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.ContainedLightFeedData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_PyrolysisBtu_kBtuData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_PyrolysisBtu_kLbDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_PyrolysisBtu_kBtuLbDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            ContainedProduct_Composite_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ContainedProduct_Composite_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ContainedProduct_Composite_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Supp_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Supp_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Supp_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            HvcProductionDivisor_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HvcProductionDivisor_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HvcProductionDivisor_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            SupplementalRecovered_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            SupplementalRecovered_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            SupplementalRecovered_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_PyrolysisHvc_kLb_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_PyrolysisHvc_kMT_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_PyrolysisHvc_kMT_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_PyrolysisHvc_klbDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            ContainedLightFeed_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ContainedLightFeed_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ContainedLightFeed_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_PyrolysisBtu_kBtu_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_PyrolysisBtu_kLbDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_PyrolysisBtu_kBtu_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_PyrolysisBtu_kBtu_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_PyrolysisBtu_kLbDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // ContainedProduct_Composite_TestAction
            // 
            ContainedProduct_Composite_TestAction.Conditions.Add(ContainedProduct_Composite_NotEmpty);
            ContainedProduct_Composite_TestAction.Conditions.Add(ContainedProduct_Composite_EmptySet);
            resources.ApplyResources(ContainedProduct_Composite_TestAction, "ContainedProduct_Composite_TestAction");
            // 
            // ContainedProduct_Composite_NotEmpty
            // 
            ContainedProduct_Composite_NotEmpty.Enabled = true;
            ContainedProduct_Composite_NotEmpty.Name = "ContainedProduct_Composite_NotEmpty";
            ContainedProduct_Composite_NotEmpty.ResultSet = 1;
            // 
            // ContainedProduct_Composite_EmptySet
            // 
            ContainedProduct_Composite_EmptySet.Enabled = true;
            ContainedProduct_Composite_EmptySet.Name = "ContainedProduct_Composite_EmptySet";
            ContainedProduct_Composite_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Supp_TestAction
            // 
            StandardEnergy_Supp_TestAction.Conditions.Add(StandardEnergy_Supp_NotEmpty);
            StandardEnergy_Supp_TestAction.Conditions.Add(StandardEnergy_Supp_EmptySet);
            resources.ApplyResources(StandardEnergy_Supp_TestAction, "StandardEnergy_Supp_TestAction");
            // 
            // StandardEnergy_Supp_NotEmpty
            // 
            StandardEnergy_Supp_NotEmpty.Enabled = true;
            StandardEnergy_Supp_NotEmpty.Name = "StandardEnergy_Supp_NotEmpty";
            StandardEnergy_Supp_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Supp_EmptySet
            // 
            StandardEnergy_Supp_EmptySet.Enabled = true;
            StandardEnergy_Supp_EmptySet.Name = "StandardEnergy_Supp_EmptySet";
            StandardEnergy_Supp_EmptySet.ResultSet = 2;
            // 
            // HvcProductionDivisor_TestAction
            // 
            HvcProductionDivisor_TestAction.Conditions.Add(HvcProductionDivisor_NotEmpty);
            HvcProductionDivisor_TestAction.Conditions.Add(HvcProductionDivisor_EmptySet);
            resources.ApplyResources(HvcProductionDivisor_TestAction, "HvcProductionDivisor_TestAction");
            // 
            // HvcProductionDivisor_NotEmpty
            // 
            HvcProductionDivisor_NotEmpty.Enabled = true;
            HvcProductionDivisor_NotEmpty.Name = "HvcProductionDivisor_NotEmpty";
            HvcProductionDivisor_NotEmpty.ResultSet = 1;
            // 
            // HvcProductionDivisor_EmptySet
            // 
            HvcProductionDivisor_EmptySet.Enabled = true;
            HvcProductionDivisor_EmptySet.Name = "HvcProductionDivisor_EmptySet";
            HvcProductionDivisor_EmptySet.ResultSet = 2;
            // 
            // SupplementalRecovered_TestAction
            // 
            SupplementalRecovered_TestAction.Conditions.Add(SupplementalRecovered_NotEmpty);
            SupplementalRecovered_TestAction.Conditions.Add(SupplementalRecovered_EmptySet);
            resources.ApplyResources(SupplementalRecovered_TestAction, "SupplementalRecovered_TestAction");
            // 
            // SupplementalRecovered_NotEmpty
            // 
            SupplementalRecovered_NotEmpty.Enabled = true;
            SupplementalRecovered_NotEmpty.Name = "SupplementalRecovered_NotEmpty";
            SupplementalRecovered_NotEmpty.ResultSet = 1;
            // 
            // SupplementalRecovered_EmptySet
            // 
            SupplementalRecovered_EmptySet.Enabled = true;
            SupplementalRecovered_EmptySet.Name = "SupplementalRecovered_EmptySet";
            SupplementalRecovered_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_PyrolysisHvc_kLb_TestAction
            // 
            StandardEnergy_PyrolysisHvc_kLb_TestAction.Conditions.Add(StandardEnergy_PyrolysisHvc_kMT_NotEmpty);
            StandardEnergy_PyrolysisHvc_kLb_TestAction.Conditions.Add(StandardEnergy_PyrolysisHvc_kMT_EmptySet);
            resources.ApplyResources(StandardEnergy_PyrolysisHvc_kLb_TestAction, "StandardEnergy_PyrolysisHvc_kLb_TestAction");
            // 
            // StandardEnergy_PyrolysisHvc_kMT_NotEmpty
            // 
            StandardEnergy_PyrolysisHvc_kMT_NotEmpty.Enabled = true;
            StandardEnergy_PyrolysisHvc_kMT_NotEmpty.Name = "StandardEnergy_PyrolysisHvc_kMT_NotEmpty";
            StandardEnergy_PyrolysisHvc_kMT_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_PyrolysisHvc_kMT_EmptySet
            // 
            StandardEnergy_PyrolysisHvc_kMT_EmptySet.Enabled = true;
            StandardEnergy_PyrolysisHvc_kMT_EmptySet.Name = "StandardEnergy_PyrolysisHvc_kMT_EmptySet";
            StandardEnergy_PyrolysisHvc_kMT_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_PyrolysisHvc_klbDay_TestAction
            // 
            StandardEnergy_PyrolysisHvc_klbDay_TestAction.Conditions.Add(StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty);
            StandardEnergy_PyrolysisHvc_klbDay_TestAction.Conditions.Add(StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet);
            resources.ApplyResources(StandardEnergy_PyrolysisHvc_klbDay_TestAction, "StandardEnergy_PyrolysisHvc_klbDay_TestAction");
            // 
            // StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty
            // 
            StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty.Enabled = true;
            StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty.Name = "StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty";
            StandardEnergy_PyrolysisHvc_MBtuDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet
            // 
            StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet.Enabled = true;
            StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet.Name = "StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet";
            StandardEnergy_PyrolysisHvc_MBtuDay_EmptySet.ResultSet = 2;
            // 
            // ContainedLightFeed_TestAction
            // 
            ContainedLightFeed_TestAction.Conditions.Add(ContainedLightFeed_NotEmpty);
            ContainedLightFeed_TestAction.Conditions.Add(ContainedLightFeed_EmptySet);
            resources.ApplyResources(ContainedLightFeed_TestAction, "ContainedLightFeed_TestAction");
            // 
            // ContainedLightFeed_NotEmpty
            // 
            ContainedLightFeed_NotEmpty.Enabled = true;
            ContainedLightFeed_NotEmpty.Name = "ContainedLightFeed_NotEmpty";
            ContainedLightFeed_NotEmpty.ResultSet = 1;
            // 
            // ContainedLightFeed_EmptySet
            // 
            ContainedLightFeed_EmptySet.Enabled = true;
            ContainedLightFeed_EmptySet.Name = "ContainedLightFeed_EmptySet";
            ContainedLightFeed_EmptySet.ResultSet = 2;
            // 
            // ContainedProduct_CompositeData
            // 
            this.ContainedProduct_CompositeData.PosttestAction = null;
            this.ContainedProduct_CompositeData.PretestAction = null;
            this.ContainedProduct_CompositeData.TestAction = ContainedProduct_Composite_TestAction;
            // 
            // StandardEnergy_SuppData
            // 
            this.StandardEnergy_SuppData.PosttestAction = null;
            this.StandardEnergy_SuppData.PretestAction = null;
            this.StandardEnergy_SuppData.TestAction = StandardEnergy_Supp_TestAction;
            // 
            // HvcProductionDivisorData
            // 
            this.HvcProductionDivisorData.PosttestAction = null;
            this.HvcProductionDivisorData.PretestAction = null;
            this.HvcProductionDivisorData.TestAction = HvcProductionDivisor_TestAction;
            // 
            // SupplementalRecoveredData
            // 
            this.SupplementalRecoveredData.PosttestAction = null;
            this.SupplementalRecoveredData.PretestAction = null;
            this.SupplementalRecoveredData.TestAction = SupplementalRecovered_TestAction;
            // 
            // StandardEnergy_PyrolysisHvc_kLbData
            // 
            this.StandardEnergy_PyrolysisHvc_kLbData.PosttestAction = null;
            this.StandardEnergy_PyrolysisHvc_kLbData.PretestAction = null;
            this.StandardEnergy_PyrolysisHvc_kLbData.TestAction = StandardEnergy_PyrolysisHvc_kLb_TestAction;
            // 
            // StandardEnergy_PyrolysisHvc_klbDayData
            // 
            this.StandardEnergy_PyrolysisHvc_klbDayData.PosttestAction = null;
            this.StandardEnergy_PyrolysisHvc_klbDayData.PretestAction = null;
            this.StandardEnergy_PyrolysisHvc_klbDayData.TestAction = StandardEnergy_PyrolysisHvc_klbDay_TestAction;
            // 
            // ContainedLightFeedData
            // 
            this.ContainedLightFeedData.PosttestAction = null;
            this.ContainedLightFeedData.PretestAction = null;
            this.ContainedLightFeedData.TestAction = ContainedLightFeed_TestAction;
            // 
            // StandardEnergy_PyrolysisBtu_kBtuData
            // 
            this.StandardEnergy_PyrolysisBtu_kBtuData.PosttestAction = null;
            this.StandardEnergy_PyrolysisBtu_kBtuData.PretestAction = null;
            this.StandardEnergy_PyrolysisBtu_kBtuData.TestAction = StandardEnergy_PyrolysisBtu_kBtu_TestAction;
            // 
            // StandardEnergy_PyrolysisBtu_kBtu_TestAction
            // 
            StandardEnergy_PyrolysisBtu_kBtu_TestAction.Conditions.Add(StandardEnergy_PyrolysisBtu_kBtu_NotEmpty);
            StandardEnergy_PyrolysisBtu_kBtu_TestAction.Conditions.Add(StandardEnergy_PyrolysisBtu_kBtu_EmptySet);
            resources.ApplyResources(StandardEnergy_PyrolysisBtu_kBtu_TestAction, "StandardEnergy_PyrolysisBtu_kBtu_TestAction");
            // 
            // StandardEnergy_PyrolysisBtu_kLbDayData
            // 
            this.StandardEnergy_PyrolysisBtu_kLbDayData.PosttestAction = null;
            this.StandardEnergy_PyrolysisBtu_kLbDayData.PretestAction = null;
            this.StandardEnergy_PyrolysisBtu_kLbDayData.TestAction = StandardEnergy_PyrolysisBtu_kLbDay_TestAction;
            // 
            // StandardEnergy_PyrolysisBtu_kLbDay_TestAction
            // 
            StandardEnergy_PyrolysisBtu_kLbDay_TestAction.Conditions.Add(StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty);
            StandardEnergy_PyrolysisBtu_kLbDay_TestAction.Conditions.Add(StandardEnergy_PyrolysisBtu_kLbDay_EmptySet);
            resources.ApplyResources(StandardEnergy_PyrolysisBtu_kLbDay_TestAction, "StandardEnergy_PyrolysisBtu_kLbDay_TestAction");
            // 
            // StandardEnergy_PyrolysisBtu_kBtuLbDayData
            // 
            this.StandardEnergy_PyrolysisBtu_kBtuLbDayData.PosttestAction = null;
            this.StandardEnergy_PyrolysisBtu_kBtuLbDayData.PretestAction = null;
            this.StandardEnergy_PyrolysisBtu_kBtuLbDayData.TestAction = StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction;
            // 
            // StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction
            // 
            StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction.Conditions.Add(StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty);
            StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction.Conditions.Add(StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet);
            resources.ApplyResources(StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction, "StandardEnergy_PyrolysisBtu_kBtuLbDay_TestAction");
            // 
            // StandardEnergy_PyrolysisBtu_kBtu_NotEmpty
            // 
            StandardEnergy_PyrolysisBtu_kBtu_NotEmpty.Enabled = true;
            StandardEnergy_PyrolysisBtu_kBtu_NotEmpty.Name = "StandardEnergy_PyrolysisBtu_kBtu_NotEmpty";
            StandardEnergy_PyrolysisBtu_kBtu_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_PyrolysisBtu_kBtu_EmptySet
            // 
            StandardEnergy_PyrolysisBtu_kBtu_EmptySet.Enabled = true;
            StandardEnergy_PyrolysisBtu_kBtu_EmptySet.Name = "StandardEnergy_PyrolysisBtu_kBtu_EmptySet";
            StandardEnergy_PyrolysisBtu_kBtu_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty
            // 
            StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty.Enabled = true;
            StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty.Name = "StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty";
            StandardEnergy_PyrolysisBtu_kBtuLbDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet
            // 
            StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet.Enabled = true;
            StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet.Name = "StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet";
            StandardEnergy_PyrolysisBtu_kBtuLbDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty
            // 
            StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty.Enabled = true;
            StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty.Name = "StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty";
            StandardEnergy_PyrolysisBtu_kLbDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_PyrolysisBtu_kLbDay_EmptySet
            // 
            StandardEnergy_PyrolysisBtu_kLbDay_EmptySet.Enabled = true;
            StandardEnergy_PyrolysisBtu_kLbDay_EmptySet.Name = "StandardEnergy_PyrolysisBtu_kLbDay_EmptySet";
            StandardEnergy_PyrolysisBtu_kLbDay_EmptySet.ResultSet = 2;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions ContainedProduct_CompositeData;
        private SqlDatabaseTestActions StandardEnergy_SuppData;
        private SqlDatabaseTestActions HvcProductionDivisorData;
        private SqlDatabaseTestActions SupplementalRecoveredData;
        private SqlDatabaseTestActions StandardEnergy_PyrolysisHvc_kLbData;
        private SqlDatabaseTestActions StandardEnergy_PyrolysisHvc_klbDayData;
        private SqlDatabaseTestActions ContainedLightFeedData;
        private SqlDatabaseTestActions StandardEnergy_PyrolysisBtu_kBtuData;
        private SqlDatabaseTestActions StandardEnergy_PyrolysisBtu_kLbDayData;
        private SqlDatabaseTestActions StandardEnergy_PyrolysisBtu_kBtuLbDayData;
    }
}
