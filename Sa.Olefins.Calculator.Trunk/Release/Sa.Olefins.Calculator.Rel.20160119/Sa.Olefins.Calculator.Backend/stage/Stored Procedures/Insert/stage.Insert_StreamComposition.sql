﻿CREATE PROCEDURE [stage].[Insert_StreamComposition]
(
	@SubmissionId			INT,
	@StreamNumber			INT,
	@ComponentId			INT,

	@Component_WtPcnt		FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[StreamComposition]([SubmissionId], [StreamNumber], [ComponentId], [Component_WtPcnt])
	SELECT
		@SubmissionId,
		@StreamNumber,
		@ComponentId,
		@Component_WtPcnt
	WHERE	@Component_WtPcnt >= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @StreamNumber:'		+ CONVERT(VARCHAR, @StreamNumber))
					+ (', @ComponentId:'		+ CONVERT(VARCHAR, @ComponentId))
			+ COALESCE(', @Component_WtPcnt:'	+ CONVERT(VARCHAR, @Component_WtPcnt),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;