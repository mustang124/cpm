﻿PRINT 'INSERT INTO [dim].[HydroTreaterType_LookUp]([HydroTreaterTypeTag], [HydroTreaterTypeName], [HydroTreaterTypeDetail])';

INSERT INTO [dim].[HydroTreaterType_LookUp]([HydroTreaterTypeTag], [HydroTreaterTypeName], [HydroTreaterTypeDetail])
SELECT t.[HydroTreaterTypeTag], t.[HydroTreaterTypeName], t.[HydroTreaterTypeDetail]
FROM (VALUES
	('All', 'All Hydrotreater Types', 'All Hydrotreater Types'),
	('B', 'Both', 'Both Selective Saturation and Desulfurization'),
	('DS', 'Desulfurization', 'Desulfurization'),
	('SS', 'Selective Saturation', 'Selective Saturation'),
	('None', 'No Hydrotreater Selected', 'No Hydrotreater Selected')
	)t([HydroTreaterTypeTag], [HydroTreaterTypeName], [HydroTreaterTypeDetail]);

PRINT 'INSERT INTO [dim].[HydroTreaterType_Parent]([MethodologyId], [HydroTreaterTypeId], [ParentId], [Operator], [SortKey], [Hierarchy])';

INSERT INTO [dim].[HydroTreaterType_Parent]([MethodologyId], [HydroTreaterTypeId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	m.MethodologyId,
	l.HydroTreaterTypeId,
	p.HydroTreaterTypeId,
	t.Operator,
	t.SortKey,
	'/'
FROM (VALUES
	('All', 'All', '+', 1),
	('B', 'All', '+', 2),
	('DS', 'All', '+', 3),
	('SS', 'All', '+', 4),
	('None', 'None', '+', 5)
	)	t(HydroTreaterTypeTag, ParentTag, Operator, SortKey)
INNER JOIN [dim].[HydroTreaterType_LookUp]	l
	ON	l.HydroTreaterTypeTag = t.HydroTreaterTypeTag
INNER JOIN [dim].[HydroTreaterType_LookUp]	p
	ON	p.HydroTreaterTypeTag = t.ParentTag
INNER JOIN [ante].[Methodology]				m
	ON	m.[MethodologyTag] = '2013';

EXECUTE dim.Update_Parent 'dim', 'HydroTreaterType_Parent', 'MethodologyId', 'HydroTreaterTypeId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE dim.Merge_Bridge 'dim', 'HydroTreaterType_Parent', 'dim', 'HydroTreaterType_Bridge', 'MethodologyId', 'HydroTreaterTypeId', 'SortKey', 'Hierarchy', 'Operator';