﻿PRINT 'INSERT INTO [ante].[MapFactorFracType]([MethodologyId], [StreamId], [FactorId])';

INSERT INTO [ante].[MapFactorFracType]([MethodologyId], [StreamId], [FactorId])
SELECT t.[MethodologyId], t.[StreamId], t.[FactorId]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('EPMix'),		dim.Return_FactorId('TowerDeethanizer')),
	(@MethodologyId, dim.Return_StreamId('LPG'),		dim.Return_FactorId('TowerDepropanizer')),
	(@MethodologyId, dim.Return_StreamId('Condensate'),	dim.Return_FactorId('TowerNAPS')),
	(@MethodologyId, dim.Return_StreamId('Naphtha'),	dim.Return_FactorId('TowerNAPS')),
	(@MethodologyId, dim.Return_StreamId('LiqHeavy'),	dim.Return_FactorId('TowerNAPS'))
	)t([MethodologyId], [StreamId], [FactorId]);