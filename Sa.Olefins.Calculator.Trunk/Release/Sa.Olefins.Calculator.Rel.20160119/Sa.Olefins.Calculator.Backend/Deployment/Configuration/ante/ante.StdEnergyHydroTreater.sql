﻿PRINT 'INSERT INTO [ante].[StdEnergyHydroTreater]([MethodologyId], [HydroTreaterTypeId], [Energy_kBtuBbl])';

INSERT INTO [ante].[StdEnergyHydroTreater]([MethodologyId], [HydroTreaterTypeId], [Energy_kBtuBbl])
SELECT t.[MethodologyId], t.[HydroTreaterTypeId], t.[Energy_kBtuBbl]
FROM (VALUES
	(@MethodologyId, dim.Return_HydroTreaterTypeId('B'),	100.0),
	(@MethodologyId, dim.Return_HydroTreaterTypeId('DS'),	 80.0),
	(@MethodologyId, dim.Return_HydroTreaterTypeId('SS'),	 55.0)
	)t([MethodologyId], [HydroTreaterTypeId], [Energy_kBtuBbl]);