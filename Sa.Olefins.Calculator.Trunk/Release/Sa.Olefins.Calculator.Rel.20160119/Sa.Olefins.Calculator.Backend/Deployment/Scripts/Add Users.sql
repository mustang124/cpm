﻿DECLARE @CalcsPerPeriod_Count INT = 11;

DECLARE @Companies TABLE
(
	[Email]				VARCHAR(128)	NOT	NULL	CHECK ([Email]			<> ''),
	[NameFirst]			NVARCHAR(128)	NOT	NULL	CHECK ([NameFirst]		<> ''),
	[NameLast]			NVARCHAR(128)	NOT	NULL	CHECK ([NameLast]		<> ''),
	[CompanyTag]		VARCHAR(42)		NOT	NULL	CHECK ([CompanyTag]		<> ''),
	[CompanyName]		VARCHAR(84)		NOT	NULL	CHECK ([CompanyName]	<> ''),
	[CompanyDetail]		VARCHAR(256)	NOT	NULL	CHECK ([CompanyDetail]	<> ''),
	[PlantName]			VARCHAR(128)	NOT	NULL	CHECK ([PlantName]		<> ''),
	[Refnum]			VARCHAR(128)	NOT	NULL	CHECK ([Refnum]			<> ''),
	[RoleId]			INT				NOT	NULL,

	PRIMARY KEY CLUSTERED([Email] ASC, [PlantName] ASC)
);

INSERT INTO @Companies([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId])
SELECT t.[Email], t.[NameFirst], t.[NameLast], t.[CompanyTag], t.[CompanyName], t.[CompanyDetail], t.[PlantName], t.[Refnum], t.[RoleId]
FROM(VALUES
	('aorejasn@repsol.com', N'Adriana', N'Orejas Nuñez', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Puertollano', '2013PCH041', 3),
	('aorejasn@repsol.com', N'Adriana', N'Orejas Nuñez', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Sines', '2013PCH078', 3),
	('aorejasn@repsol.com', N'Adriana', N'Orejas Nuñez', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Tarragona', '2013PCH079', 3),

	('vgutierrezp@repsol.com', N'Victor', N'Gutierrez Perez', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Puertollano', '2013PCH041', 3),
	('vgutierrezp@repsol.com', N'Victor', N'Gutierrez Perez', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Sines', '2013PCH078', 3),
	('vgutierrezp@repsol.com', N'Victor', N'Gutierrez Perez', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Tarragona', '2013PCH079', 3)

	) t([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId]);

DECLARE @MethodologyId	INT = IDENT_CURRENT('[ante].[Methodology]');

INSERT INTO [dim].[Company_LookUp]([CompanyTag], [CompanyName], [CompanyDetail])
SELECT DISTINCT
	[i].[CompanyTag],
	[i].[CompanyName],
	[i].[CompanyDetail]
FROM
	@Companies				[i]
LEFT OUTER JOIN
	[dim].[Company_LookUp]	[l]
		ON	[l].[CompanyTag]	= [i].[CompanyTag]
WHERE
	[l].[CompanyId]	IS NULL;

DECLARE @UpdateHierarchy TABLE
(
	[Item]	INT	NOT	NULL,
	PRIMARY KEY CLUSTERED([Item] ASC)
);

INSERT INTO [dim].[Company_Parent]([MethodologyId], [CompanyId], [ParentId], [Operator], [SortKey], [Hierarchy])
OUTPUT
	INSERTED.CompanyId
INTO @UpdateHierarchy
SELECT DISTINCT
	@MethodologyId,
	[dim].[Return_CompanyId]([i].[CompanyTag]),
	[dim].[Return_CompanyId]([i].[CompanyTag]),
	'~',
	ROW_NUMBER() OVER(ORDER BY [i].[CompanyTag]),
	'/'
FROM (
	SELECT DISTINCT
		[i].[CompanyTag]
	FROM @Companies	[i]
	) [i]
LEFT OUTER JOIN
	[dim].[Company_Parent]	[p]
		ON	[p].[CompanyId]		= [dim].[Return_CompanyId]([i].[CompanyTag])
		AND	[p].[MethodologyId]	= @MethodologyId
WHERE
	[p].[CompanyId] IS NULL;

IF EXISTS (SELECT TOP 1 1 FROM @UpdateHierarchy)
BEGIN
	EXECUTE [dim].[Update_Parent] 'dim', 'Company_Parent', 'MethodologyId', 'CompanyId', 'ParentId', 'SortKey', 'Hierarchy';
	EXECUTE [dim].[Merge_Bridge] 'dim', 'Company_Parent', 'dim', 'Company_Bridge', 'MethodologyId', 'CompanyId', 'SortKey', 'Hierarchy', 'Operator';
END;

INSERT INTO [auth].[Companies]([CompanyName], [CompanyRefId])
SELECT
	[l].[CompanyName],
	[l].[CompanyId]
FROM
	[dim].[Company_LookUp]	[l]
LEFT OUTER JOIN
	[auth].[Companies]		[a]
		ON	[a].[CompanyName]	= [l].[CompanyName]
WHERE
	[a].[CompanyId]	IS NULL;

INSERT INTO [auth].[Plants]([PlantName])
SELECT DISTINCT
	[i].[PlantName]
FROM
	@Companies			[i]
LEFT OUTER JOIN
	[auth].[Plants]		[a]
		ON	[a].[PlantName]	= [i].[PlantName]
WHERE
	[a].[PlantId]	IS NULL;

INSERT INTO [auth].[PlantCalculationLimits]([PlantId], [CalcsPerPeriod_Count])
SELECT
	[p].[PlantId], @CalcsPerPeriod_Count
FROM
	[auth].[Plants]					[p]
LEFT OUTER JOIN
	[auth].[PlantCalculationLimits]	[l]
		ON	[l].[PlantId]	= [p].[PlantId]
WHERE
	[l].[PlantId]	IS NULL;

INSERT INTO [auth].[Logins]([LoginTag])
SELECT DISTINCT
	[i].[Email]
FROM 
	@Companies		[i]
LEFT OUTER JOIN
	[auth].[Logins]	[l]
		ON	[i].[Email]	= [l].[LoginTag]
WHERE
	[l].[LoginId] IS NULL;

INSERT INTO [auth].[LoginsAttributes]([LoginId], [NameLast], [NameFirst], [eMail], [RoleId])
SELECT DISTINCT
	[l].[LoginId],
	[i].[NameLast],
	[i].[NameFirst],
	[i].[Email],
	[i].[RoleId]
FROM
	[auth].[Logins]				[l]
INNER JOIN
	@Companies					[i]
		ON	[i].[Email] = [l].[LoginTag]
LEFT OUTER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[LoginId]	= [l].[LoginId]
WHERE
	[a].[LoginId] IS NULL;

INSERT INTO [auth].[LoginsPassWords]([LoginId], [pSalt], [pWord])
SELECT 
	[l].[LoginId],
	0x2D228C933876329B03D86FF48431EF558C6F23B83BBA0037291D03E956037EC93835795D7F139E78E356E1F1210D52BA610B2054297B01C564C01F92CFD95EC67F6FD23D8CB645F3A0CCE866B0A336DD04379676EC371D37E76ED5A90529EDBA64E21326D5E73A9D338BE17DF6E87353B6378DA2C06B150D82A61B85C0BE34AC1D73C3B997651BED1A1745439CFAF1F9666D90105C09F31EF48CC3B7987A2973063DE784C8ED7D3723E761477498C09DAE7692A7EA72CF9E9E9E48527A4745267D72228C426789BC5E7DC124965EBD2308B2512AD1B9ECFEF9B69DDC9B94324A194834E1E8185363A9FF3D79503ECC1609922670E7D56D39F0770648C12F8BEF8CF1B0E10B7EEDFB5DAEE6820F8AC0D86A5828B794CE525517F992896A9A6D8958C6C70A834526D4FF25D101F28D4BB410D361667DE3BD761B30E1F570CD4CC8EA386401E409C24E3567A7A779B1376B72EBA15DD8B3CA88AE1DC6D4CFB6A3C2A912947EB336B380F2D2EA52D8804C06CA7BBB6E00DB1D4F1E589E443CC50BB5F1C7C9524C56D21A44AF1AA4E22E68B092ECC6039C2E2554E4918B0D366AD6B674CE4038291A727BD26FED2659C7844A19941CDF786CE953FCD96865AB7FA1D4433B52BB7695561E311A0C1D739D3039F04378B96B2972F675A7D73840A7198629D103CD26449BAAA9230AEDB043505D8CA365982F6548B85F1044B91C61C0FE,
	0xE4FF5A0165CDEFBC1961B796EEE0F33A2710AD184EA0A24C135C113EDA5B7995C902395460A33DB2EFEA1FBC7DE283B068B8E5FE013DEB9961921A05C6941F44
FROM
	[auth].[Logins]				[l]
LEFT OUTER JOIN
	[auth].[LoginsPassWords]	[p]
		ON	[p].[LoginId]	= [l].[LoginId]
WHERE [p].[LoginId] IS NULL;

INSERT INTO [auth].[JoinCompanyPlant]([CompanyId], [PlantId])
SELECT DISTINCT
	[c].[CompanyId],
	[p].[PlantId]
FROM 
	@Companies					[i]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [i].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [i].[PlantName]
LEFT OUTER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
WHERE
	[jcp].[JoinId]	IS NULL;
	
INSERT INTO [auth].[JoinCompanyLogin]([CompanyId], [LoginId])
SELECT DISTINCT
	[c].[CompanyId],
	[l].[LoginId]
FROM
	@Companies					[i]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [i].[CompanyName]
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginTag]		= [i].[Email]
LEFT OUTER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [l].[LoginId]
WHERE
	[jcl].[LoginId]	IS NULL;

INSERT INTO [auth].[JoinPlantLogin]([PlantId], [LoginId])
SELECT DISTINCT
	[p].[PlantId],
	[l].[LoginId]
FROM @Companies				[i]
INNER JOIN
	[auth].[Plants]			[p]
		ON	[p].[PlantName]	= [i].[PlantName]
INNER JOIN
	[auth].[Logins]			[l]
		ON	[l].[LoginTag]	= [i].[Email]
LEFT OUTER JOIN
	[auth].[JoinPlantLogin]	[jpl]
		ON	[jpl].[PlantId]		= [p].[PlantId]
		AND	[jpl].[LoginId]		= [l].[LoginId]
WHERE
	[jpl].[LoginId]	IS NULL;
