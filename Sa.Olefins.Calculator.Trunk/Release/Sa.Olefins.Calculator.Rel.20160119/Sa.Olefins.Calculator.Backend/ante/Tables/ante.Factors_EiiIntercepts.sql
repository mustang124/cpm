﻿CREATE TABLE [ante].[Factors_EiiIntercepts]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Factors_EiiIntercepts_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[HvcYield_Intercept]	FLOAT				NOT	NULL	CONSTRAINT [CR_Factors_EiiIntercepts_HvcYield_Intercept_MinExcl_0.0]	CHECK([HvcYield_Intercept]	> 0.0),
	[Energy_Intercept]		FLOAT				NOT	NULL	CONSTRAINT [CR_Factors_EiiIntercepts_Energy_Intercept_MinExcl_0.0]		CHECK([Energy_Intercept]	> 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Factors_EiiIntercepts_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_EiiIntercepts_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_EiiIntercepts_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_EiiIntercepts_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Factors_EiiIntercepts]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC)
);
GO

CREATE TRIGGER [ante].[t_Factors_EiiIntercepts_u]
ON [ante].[Factors_EiiIntercepts]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Factors_EiiIntercepts]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Factors_EiiIntercepts].[MethodologyId]	= INSERTED.[MethodologyId];

END;
GO