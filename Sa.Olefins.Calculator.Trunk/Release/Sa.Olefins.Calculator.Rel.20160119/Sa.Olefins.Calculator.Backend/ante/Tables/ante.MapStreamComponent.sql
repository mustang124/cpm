﻿CREATE TABLE [ante].[MapStreamComponent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapStreamComponent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_MapStreamComponent_Stream_LookUp]		REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_MapStreamComponent_Component_LookUp]		REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[Component_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_MapStreamComponent_Component_WtPcnt]		CHECK([Component_WtPcnt] >= 0.0 AND [Component_WtPcnt] <= 100.0),
	[Recovered_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_MapStreamComponent_Recovered_WtPcnt]		CHECK([Recovered_WtPcnt] >= 0.0 AND [Recovered_WtPcnt] <= 100.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapStreamComponent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamComponent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamComponent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamComponent_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapStreamComponent]	PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_MapStreamComponent_Production]
ON [ante].[MapStreamComponent]([MethodologyId] DESC, [StreamId] ASC, [ComponentId] ASC)
INCLUDE ([Component_WtPcnt])
WHERE [ComponentId] IN (5, 6, 7, 8, 15, 16, 17, 18, 19, 21, 24, 162, 163, 164, 165, 166)
GO

CREATE TRIGGER [ante].[t_MapStreamComponent_u]
ON [ante].[MapStreamComponent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapStreamComponent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapStreamComponent].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapStreamComponent].[StreamId]		= INSERTED.[StreamId];

END;
GO