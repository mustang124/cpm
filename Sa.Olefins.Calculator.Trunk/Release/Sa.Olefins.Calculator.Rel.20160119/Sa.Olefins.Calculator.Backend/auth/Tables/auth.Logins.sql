﻿CREATE TABLE [auth].[Logins]
(
	[LoginId]				INT					NOT	NULL	IDENTITY (1, 1),

	[LoginTag]				NVARCHAR(254)		NOT	NULL	CONSTRAINT [CL_Logins_UserTag]					CHECK([LoginTag] <> ''),
															CONSTRAINT [UK_Logins_UserTag]					UNIQUE NONCLUSTERED ([LoginTag]),
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_Logins_Active]					DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Logins_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Logins_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Logins_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Logins_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_LoginIds]					PRIMARY KEY CLUSTERED ([LoginId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Logins_Active]
ON [auth].[Logins]([LoginId] ASC)
INCLUDE ([LoginTag])
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_LoginIds_u]
ON [auth].[Logins]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[Logins]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[Logins].[LoginId]		= INSERTED.[LoginId];

END;
GO