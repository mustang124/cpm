﻿CREATE VIEW [auth].[LoginsByPlant_Active]
WITH SCHEMABINDING
AS
SELECT
	jpl.[JoinId],
	jpl.[PlantId],
	jpl.[LoginId],
	p.[PlantName],
	l.[LoginTag],
	a.[NameFirst],
	a.[NameLast],
	a.[eMail],
	a.[_NameComma],
	a.[_NameFull],
	a.[RoleId],
	r.[RoleLevel]
FROM [auth].[JoinPlantLogin]			jpl
INNER JOIN [auth].[Plants]				p
	ON	p.[PlantId]	= jpl.[PlantId]
INNER JOIN [auth].[Logins]				l
	ON	l.[LoginId]		= jpl.[LoginId]
INNER JOIN [auth].[LoginsAttributes]	a
	ON	a.[LoginId]		= jpl.[LoginId]
INNER JOIN [auth].[Roles]				r
	ON	r.[RoleId]		= a.[RoleId]
WHERE	jpl.[Active]	= 1
	AND	p.[Active]		= 1
	AND	l.[Active]		= 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UX_LoginsByPlant_Active]
ON [auth].[LoginsByPlant_Active]([PlantId] ASC, [LoginId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_LoginsByPlant_Active_Get]
ON [auth].[LoginsByPlant_Active]([PlantId] ASC, [LoginId] ASC)
INCLUDE ([PlantName], [LoginTag], [NameFirst], [NameLast], [eMail], [_NameComma], [_NameFull]);
GO
