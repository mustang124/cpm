﻿CREATE FUNCTION [auth].[Return_LoginId]
(
	@LoginTag	NVARCHAR(254)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT
		@Id = l.[LoginId]
	FROM [auth].[Logins]	l
	WHERE	l.[LoginTag] = @LoginTag;

	RETURN @Id;

END;
