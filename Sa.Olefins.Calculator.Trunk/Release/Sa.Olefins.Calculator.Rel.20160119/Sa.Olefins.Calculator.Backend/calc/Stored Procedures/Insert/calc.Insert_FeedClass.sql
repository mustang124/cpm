﻿CREATE PROCEDURE [calc].[Insert_FeedClass]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

IF(DB_NAME() = 'OlefinsOnline') OR (DB_NAME() = 'Sa.Olefins.Calculator.Backend')
BEGIN

	DECLARE @XACT_STATE		SMALLINT;
	DECLARE @Parameters		VARCHAR(4000);

	IF (1 = 1)	-- stage.FeedClass
	BEGIN TRY

		INSERT INTO [calc].[FeedClass]([MethodologyId], [SubmissionId], [FeedClassId], [IsCalculated])
		SELECT
			@MethodologyId,
			f.[SubmissionId],
			f.[FeedClassId],
			0
		FROM [stage].[FeedClass] f
		WHERE f.[SubmissionId] = @SubmissionId;

		RETURN 0;

	END TRY
	BEGIN CATCH

		SET @XACT_STATE			= XACT_STATE();
		SET @Parameters			=
			('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
		+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
		+ (' (Stage)');

		EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

		RETURN - ERROR_NUMBER();

	END CATCH;

	--IF (1 = 0)	-- calc.FeedClass
	--BEGIN TRY

	--	DECLARE @PlantId		INT = [auth].[Return_PlantIdFromSubmissionId](@SubmissionId);
	--	DECLARE @FeedClassId	INT = [dim].[Return_FeedClassId]([calc].[Return_FeedClassId_SubmissionId](@SubmissionId));

	--	IF (@PlantId IS NOT NULL AND @FeedClassId IS NOT NULL)
	--	INSERT INTO [calc].[FeedClass]([MethodologyId], [SubmissionId], [FeedClassId], [IsCalculated])
	--	VALUES (@MethodologyId, @SubmissionId, @FeedClassId, 0);

	--	RETURN 0;

	--END TRY
	--BEGIN CATCH

	--	SET @XACT_STATE			= XACT_STATE();
	--	SET @Parameters			=
	--		('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
	--	+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
	--	+ (' (Previous)');

	--	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	--	RETURN - ERROR_NUMBER();

	--END CATCH;

	--IF (1 = 0)	-- calculated
	--BEGIN TRY

	--	DECLARE	@FreshPyroFeed			INT		= [dim].[Return_StreamId]('FreshPyroFeed');
	--	DECLARE	@FreshPyroFeed_kMT		FLOAT;

	--	SELECT @FreshPyroFeed_kMT = SUM(q.[Quantity_kMT])
	--	FROM [fact].[StreamQuantity]		q
	--	INNER JOIN [dim].[Stream_Bridge]	b
	--		ON	b.[DescendantId]	= q.[StreamId]
	--	WHERE	b.[MethodologyId]	= @MethodologyId
	--		AND	q.[SubmissionId]	= @SubmissionId
	--		AND	b.[StreamId]		= @FreshPyroFeed;

	--	DECLARE	@C2H6					INT = [dim].[Return_ComponentId]('C2H6');
	--	DECLARE	@C2H6_kMT				FLOAT;

	--	SELECT @C2H6_kMT = SUM(q.[Component_Dur_kMT])
	--	FROM [calc].[StreamComposition]		q
	--	INNER JOIN [dim].[Stream_Bridge]	b
	--		ON	b.[DescendantId]	= q.[StreamId]
	--	WHERE	b.[MethodologyId]	= @MethodologyId
	--		AND	q.[SubmissionId]	= @SubmissionId
	--		AND	q.[ComponentId]		= @C2H6
	--		AND	b.[StreamId]		= @FreshPyroFeed;

	--	DECLARE	@C2H4					INT = [dim].[Return_ComponentId]('C2H4');
	--	DECLARE	@C3H8					INT = [dim].[Return_ComponentId]('C3H8');
	--	DECLARE	@C3H6					INT = [dim].[Return_ComponentId]('C3H6');
	--	DECLARE	@NBUTA					INT = [dim].[Return_ComponentId]('NBUTA');
	--	DECLARE	@IBUTA					INT = [dim].[Return_ComponentId]('IBUTA');
	--	DECLARE	@B2						INT = [dim].[Return_ComponentId]('B2');
	--	DECLARE	@B1						INT = [dim].[Return_ComponentId]('B1');
	--	DECLARE	@C4H6					INT = [dim].[Return_ComponentId]('C4H6');

	--	DECLARE	@LPG_kMT				FLOAT;

	--	SELECT @LPG_kMT = SUM(q.[Component_Dur_kMT])
	--	FROM [calc].[StreamComposition]		q
	--	INNER JOIN [dim].[Stream_Bridge]	b
	--		ON	b.[DescendantId]	= q.[StreamId]
	--	WHERE	b.[MethodologyId]	= @MethodologyId
	--		AND	q.[SubmissionId]	= @SubmissionId
	--		AND	b.[StreamId]		= @FreshPyroFeed
	--		AND	q.[ComponentId]		IN (@C2H4, @C3H8, @C3H6, @NBUTA, @IBUTA, @B2, @B1, @C4H6);

	--	DECLARE	@Liquid					INT		= [dim].[Return_StreamId]('Liquid');
	--	DECLARE	@Liquid_kMT				FLOAT;

	--	SELECT @Liquid_kMT = SUM(q.[Quantity_kMT])
	--	FROM [fact].[StreamQuantity]		q
	--	INNER JOIN [dim].[Stream_Bridge]	b
	--		ON	b.[DescendantId]	= q.[StreamId]
	--	WHERE	b.[MethodologyId]	= @MethodologyId
	--		AND	q.[SubmissionId]	= @SubmissionId
	--		AND	b.[StreamId]		= @Liquid;

	--	DECLARE @EpGT390				INT		= [dim].[Return_StreamId]('EpGT390');
	--	DECLARE @EpGT390_kMT			FLOAT;

	--	SELECT @EpGT390_kMT = SUM(q.[Quantity_kMT])
	--	FROM [fact].[StreamQuantity]		q
	--	INNER JOIN [dim].[Stream_Bridge]	b
	--		ON	b.[DescendantId]	= q.[StreamId]
	--	WHERE	b.[MethodologyId]	= @MethodologyId
	--		AND	q.[SubmissionId]	= @SubmissionId
	--		AND	b.[StreamId]		= @EpGT390;

	--	DECLARE	@Naphtha				INT		= [dim].[Return_StreamId]('Naphtha');
	--	DECLARE	@NaphthaDensity_SG		FLOAT;

	--	SELECT @NaphthaDensity_SG = SUM(q.[Quantity_kMT] * d.[Density_SG]) / SUM(q.[Quantity_kMT])
	--	FROM [fact].[StreamQuantity]		q
	--	INNER JOIN [fact].[StreamDensity]	d
	--		ON	d.[SubmissionId]	= q.[SubmissionId]
	--		AND	d.[StreamNumber]	= q.[StreamNumber]
	--	INNER JOIN [dim].[Stream_Bridge]	b
	--		ON	b.[DescendantId]	= q.[StreamId]
	--	WHERE	b.[MethodologyId]	= @MethodologyId
	--		AND	q.[SubmissionId]	= @SubmissionId
	--		AND	b.[StreamId]		= @Naphtha
	--	GROUP BY
	--		b.[MethodologyId],
	--		q.[SubmissionId];

	--	SET @NaphthaDensity_SG = COALESCE(@NaphthaDensity_SG, 0.0);

	--	DECLARE	@C2H6_Pcnt				FLOAT	= @C2H6_kMT		/ @FreshPyroFeed_kMT * 100.0;
	--	DECLARE	@LPG_Pcnt				FLOAT	= @LPG_kMT		/ @FreshPyroFeed_kMT * 100.0;
	--	DECLARE	@Liquid_Pcnt			FLOAT	= @Liquid_kMT	/ @FreshPyroFeed_kMT * 100.0;
	--	DECLARE	@EpGT390_Pcnt			FLOAT	= @EpGT390_kMT	/ @FreshPyroFeed_kMT * 100.0;

	--	DECLARE	@C2H6_Limit				FLOAT	= 85.0;
	--	DECLARE	@LPG_Limit				FLOAT	= 50.0;
	--	DECLARE	@Liquid_Limit			FLOAT	= 85.0;
	--	DECLARE	@EpGT390_Limit			FLOAT	= 25.0;
	--	DECLARE	@NaphthaDensity_Limit	FLOAT	= 0.7;

	--	DECLARE @FeedClassTag			VARCHAR(1)		= '1';

	--	IF	@C2H6_Pcnt > @C2H6_Limit SET @FeedClassTag = '1';
	--	IF	@LPG_Pcnt > @LPG_Limit AND @C2H6_Pcnt < @C2H6_Limit SET @FeedClassTag = '2';
	--	IF	(@Liquid_Pcnt > @Liquid_Limit AND @EpGT390_Pcnt < @EpGT390_Limit) OR @NaphthaDensity_SG < @NaphthaDensity_Limit SET @FeedClassTag = '4';
	--	IF	@FeedClassTag = '-' SET @FeedClassTag = '3'

	--	INSERT INTO [calc].[FeedClass]([MethodologyId], [SubmissionId], [FeedClassId], [IsCalculated])
	--	VALUES (@MethodologyId, @SubmissionId, [dim].[Return_FeedClassId](@FeedClassTag), 1);
	
	--	RETURN 0;

	--END TRY
	--BEGIN CATCH

	--	SET @XACT_STATE			= XACT_STATE();
	--	SET @Parameters			=
	--		('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
	--	+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
	--	+ (' (Calcualted)');

	--	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	--	RETURN - ERROR_NUMBER();

	--END CATCH;

END;

END;
GO