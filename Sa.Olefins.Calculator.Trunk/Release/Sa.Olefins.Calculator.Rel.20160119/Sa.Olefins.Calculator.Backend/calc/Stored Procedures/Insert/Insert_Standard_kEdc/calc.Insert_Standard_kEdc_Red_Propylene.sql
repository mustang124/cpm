﻿CREATE PROCEDURE [calc].[Insert_Standard_kEdc_Red_Propylene]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId_kEdc	INT	= dim.Return_StandardId('kEdc');

	DECLARE @sProdOlefins		INT = [dim].[Return_StreamId]('ProdOlefins');
	DECLARE @cProdOlefins		INT = [dim].[Return_ComponentId]('ProdOlefins');

	DECLARE @PropyleneCG		INT = [dim].[Return_StreamId]('PropyleneCG');
	DECLARE @PropyleneRG		INT = [dim].[Return_StreamId]('PropyleneRG');
	DECLARE	@C3H6				INT	= [dim].[Return_ComponentId]('C3H6');
	
	DECLARE	@FeedClass			INT = [dim].[Return_FeedClassId]('Ethane');
	DECLARE	@Threshold_Pcnt		FLOAT = 90.0;

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		adj.[MethodologyId],
		adj.[SubmissionId],
		f.[StandardId],
		mp.[ProcessUnitId],
		-1.0 * adj.[Adjusted_kMT] / u.[_Utilization_Pcnt] * 100.0 * 10.24 / z.[_Duration_Days] * f.[Coefficient]
	FROM (
		SELECT
			fc.[MethodologyId],
			fc.[SubmissionId],

			  CASE WHEN COALESCE(cg.[Component_WtPcnt], 0.0) <	@Threshold_Pcnt THEN COALESCE(cq.[Quantity_kMT], 0.0) ELSE 0.0 END
			+ CASE WHEN COALESCE(rg.[Component_WtPcnt], 0.0) <	@Threshold_Pcnt THEN COALESCE(rq.[Quantity_kMT], 0.0) ELSE 0.0 END	[PropyleneRG],

			( COALESCE(cq.[Quantity_kMT], 0.0) + COALESCE(rq.[Quantity_kMT], 0.0)
			- CASE WHEN cg.[Component_WtPcnt] <	@Threshold_Pcnt THEN cq.[Quantity_kMT] ELSE 0.0 END
			- CASE WHEN rg.[Component_WtPcnt] <	@Threshold_Pcnt THEN rq.[Quantity_kMT] ELSE 0.0 END) * 1.46							[PropyleneCG]

		FROM [calc].[FeedClass]							fc

		LEFT OUTER JOIN	[calc].[StreamComposition]		cg
			ON	cg.[MethodologyId]	= fc.[MethodologyId]
			AND	cg.[SubmissionId]	= fc.[SubmissionId]
			AND	cg.[StreamId]		= @PropyleneCG
			AND	cg.[ComponentId]	= @C3H6
		LEFT OUTER JOIN [fact].[StreamQuantity]			cq
			ON	cq.[SubmissionId]	= fc.[SubmissionId]
			AND	cq.[StreamId]		= cg.[StreamId]

		LEFT OUTER JOIN	[calc].[StreamComposition]		rg
			ON	rg.[MethodologyId]	= fc.[MethodologyId]
			AND	rg.[SubmissionId]	= fc.[SubmissionId]
			AND	rg.[StreamId]		= @PropyleneRG
			AND	rg.[ComponentId]	= @C3H6
		LEFT OUTER JOIN [fact].[StreamQuantity]			rq
			ON	rq.[SubmissionId]	= fc.[SubmissionId]
			AND	rq.[StreamId]		= rg.[StreamId]

		WHERE	fc.[MethodologyId]	= @MethodologyId
			AND	fc.[SubmissionId]	= @SubmissionId
			AND	fc.[FeedClassId]	<> @FeedClass
			AND (cg.[Component_Ann_kMT] <> 0.0 OR rg.[Component_Ann_kMT] <> 0.0)
		) t
		UNPIVOT (
			[Adjusted_kMT] FOR [StreamTag] IN ([PropyleneRG], [PropyleneCG])
		) adj
	INNER JOIN [fact].[Submissions]					z
		ON	z.[SubmissionId]	= adj.[SubmissionId]
	INNER JOIN [calc].[Utilization]					u
		ON	u.[MethodologyId]	= adj.[MethodologyId]
		AND	u.[SubmissionId]	= adj.[SubmissionId]
		AND	u.[StreamId]		= @sProdOlefins
		AND	u.[ComponentId]		= @cProdOlefins
	INNER JOIN [ante].[MapStreamProcessUnit]		mp
		ON	mp.[MethodologyId]	= adj.[MethodologyId]
		AND	mp.[StreamId]		= [dim].[Return_StreamId](adj.[StreamTag])
	INNER JOIN [ante].[MapFactorStream]				mf
		ON	mf.[MethodologyId]	= adj.[MethodologyId]
		AND	mf.[StreamId]		= [dim].[Return_StreamId](adj.[StreamTag])
	INNER JOIN [ante].[Factors]						f
		ON	f.[MethodologyId]	= adj.[MethodologyId]
		AND	f.[StandardId]		= @StandardId_kEdc
		AND	f.[FactorId]		= mf.[FactorId];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO