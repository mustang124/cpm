﻿CREATE TABLE [calc].[FacilitiesBoilers]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesBoilers_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesBoilers_Submissions]				REFERENCES [fact].[Submissions]([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesBoilers_Facility_LookUp]			REFERENCES [dim].[Facility_LookUp]([FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Rate_kLbHr]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesBoilers_Rate_kLbHr_MinIncl_0.0]	CHECK ([Rate_kLbHr] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesBoilers]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [calc].[t_FacilitiesBoilers_u]
ON [calc].[FacilitiesBoilers]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[FacilitiesBoilers]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[FacilitiesBoilers].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[FacilitiesBoilers].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[FacilitiesBoilers].[FacilityId]		= INSERTED.[FacilityId];

END;
GO