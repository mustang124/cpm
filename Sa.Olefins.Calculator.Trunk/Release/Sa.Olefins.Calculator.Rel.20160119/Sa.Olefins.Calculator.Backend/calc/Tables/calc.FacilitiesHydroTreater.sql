﻿CREATE TABLE [calc].[FacilitiesHydroTreater]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesHydroTreater_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesHydroTreater_Submissions]					REFERENCES [fact].[Submissions] ([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesHydroTreater_Facility_LookUp]				REFERENCES [dim].[Facility_LookUp]([FacilityId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Quantity_BSD]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesHydroTreater_Quantity_BSD_MinIncl_0.0]		CHECK([Quantity_BSD] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesHydroTreater]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC)
);
GO

CREATE TRIGGER [calc].[t_FacilitiesHydroTreater_u]
ON [calc].[FacilitiesHydroTreater]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[FacilitiesHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[FacilitiesHydroTreater].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[FacilitiesHydroTreater].[SubmissionId]	= INSERTED.[SubmissionId];

END;
GO