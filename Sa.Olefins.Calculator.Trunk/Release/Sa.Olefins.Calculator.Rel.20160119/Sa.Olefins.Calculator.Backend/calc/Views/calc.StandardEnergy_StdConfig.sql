﻿CREATE VIEW [calc].[StandardEnergy_StdConfig]
WITH SCHEMABINDING
AS
SELECT
	y.[MethodologyId],
	y.[SubmissionId],
	y.[HvcYieldDivisor_kLbDay],
	e.[StandardEnergy_kBtuLbDay],
	y.[HvcYieldDivisor_kLbDay] * e.[StandardEnergy_kBtuLbDay]	[StandardEnergy_MBtuDay]
FROM [calc].[StandardEnergy_PyrolysisHvc]		y
INNER JOIN [calc].[StandardEnergy_PyrolysisBtu]	e
	ON	e.[MethodologyId]	= y.[MethodologyId]
	AND	e.[SubmissionId]	= y.[SubmissionId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_StandardEnergy_StdConfig]
ON [calc].[StandardEnergy_StdConfig]([MethodologyId] DESC, [SubmissionId] ASC);
GO

CREATE INDEX [IX_StandardEnergy_StdConfig]
ON [calc].[StandardEnergy_StdConfig]([MethodologyId] DESC, [SubmissionId] ASC)
INCLUDE([StandardEnergy_MBtuDay])
GO