﻿CREATE VIEW [calc].[Standards_Pivot_ProcessUnit]
WITH SCHEMABINDING
AS
SELECT
	p.[SubmissionId],
	p.[StandardId],
	p.[Total],
	p.[ProcUnit],
	p.[Feed],
	p.[Fresh],
	p.[Supp],
	p.[Auxiliary],
	p.[FeedPrep],
	p.[PyroGasHydroTreat],
	p.[HydroPur],
	p.[Reduction],
	p.[RedPropylene],
	p.[RedPropyleneCG],
	p.[RedPropylenePG],
	p.[RedEthylene],
	p.[RedEthyleneCG],
	p.[RedCrackedGasTrans],
	p.[Utilities],
	p.[BoilFiredSteam],
	p.[ElecGen]
FROM (
	SELECT
		v.[SubmissionId],
		v.[StandardId],
		p.[ProcessUnitTag],
		v.[StandardValue]
	FROM	[calc].[Standards_Aggregate]			v WITH (NOEXPAND)
	INNER JOIN [dim].[ProcessUnit_LookUp]			p
		ON	p.[ProcessUnitId] = v.[ProcessUnitId]
	) u
	PIVOT (
		MAX(u.[StandardValue]) FOR u.[ProcessUnitTag] IN(
			[Total],
				[ProcUnit],
					[Feed],
						[Fresh],
						[Supp],
					[Auxiliary],
						[FeedPrep],
						[PyroGasHydroTreat],
						[HydroPur],
					[Reduction],
						[RedPropylene],
							[RedPropyleneCG],
							[RedPropylenePG],
						[RedEthylene],
							[RedEthyleneCG],
						[RedCrackedGasTrans],
				[Utilities],
					[BoilFiredSteam],
					[ElecGen]
		)
	) p;