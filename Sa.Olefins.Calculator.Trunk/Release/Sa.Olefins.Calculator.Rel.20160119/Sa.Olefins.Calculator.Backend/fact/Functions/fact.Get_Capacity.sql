﻿CREATE FUNCTION [fact].[Get_Capacity]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		c.[SubmissionId],
		c.[StreamId],
		c.[Capacity_kMT],
		c.[StreamDay_MTSD],
		c.[Record_MTSD]
	FROM [fact].[Capacity]				c
	WHERE	c.[SubmissionId] = @SubmissionId
);