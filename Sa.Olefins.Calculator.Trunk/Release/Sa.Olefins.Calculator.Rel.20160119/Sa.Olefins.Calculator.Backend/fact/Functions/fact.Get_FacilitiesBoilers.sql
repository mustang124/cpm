﻿CREATE FUNCTION [fact].[Get_FacilitiesBoilers]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		b.[SubmissionId],
		b.[FacilityId],
		b.[Rate_kLbHr],
		b.[Pressure_PSIg]
	FROM [fact].[FacilitiesBoilers]		b
	WHERE	b.[SubmissionId]	= @SubmissionId
);