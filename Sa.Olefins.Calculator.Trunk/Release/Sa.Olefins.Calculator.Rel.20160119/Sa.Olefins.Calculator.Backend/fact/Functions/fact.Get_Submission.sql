﻿CREATE FUNCTION [fact].[Get_Submission]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		z.[SubmissionId],
		z.[SubmissionName],
		z.[DateBeg],
		z.[DateEnd]
	FROM [fact].[Submissions]		z
	WHERE	z.[SubmissionId] = @SubmissionId
);