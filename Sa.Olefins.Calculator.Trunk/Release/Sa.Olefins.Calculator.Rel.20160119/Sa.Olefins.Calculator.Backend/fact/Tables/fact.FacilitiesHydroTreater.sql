﻿CREATE TABLE [fact].[FacilitiesHydroTreater]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesHydroTreater_Submissions]					REFERENCES [fact].[Submissions]([SubmissionId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesHydroTreater_Facility_LookUp]				REFERENCES [dim].[Facility_LookUp]([FacilityId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_FacilitiesHydroTreater_Facilities]
															FOREIGN KEY ([SubmissionId], [FacilityId])							REFERENCES [fact].[Facilities]([SubmissionId], [FacilityId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Quantity_kBSD]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesHydroTreater_Quantity_kBSD_MinIncl_0.0]	CHECK([Quantity_kBSD] >= 0.0),
	[_Quantity_BSD]			AS CONVERT(FLOAT, [Quantity_kBSD] * 1000.0)
							PERSISTED			NOT	NULL,
	[HydroTreaterTypeId]	INT					NOT	NULL	CONSTRAINT [FK_FacilitiesHydroTreater_HydroTreater_LookUp]			REFERENCES [dim].[HydroTreaterType_LookUp]([HydroTreaterTypeId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Pressure_PSIg]			FLOAT					NULL	CONSTRAINT [CR_FacilitiesHydroTreater_Pressure_PSIg_MinIncl_0.0]	CHECK ([Pressure_PSIg] >= 0.0),
	[Processed_kMT]			FLOAT					NULL	CONSTRAINT [CR_FacilitiesHydroTreater_Processed_kMT_MinIncl_0.0]	CHECK([Processed_kMT] >= 0.0),
	[Processed_Pcnt]		FLOAT					NULL	CONSTRAINT [CR_FacilitiesHydroTreater_Processed_Pcnt_MinIncl_0.0]	CHECK([Processed_Pcnt] >= 0.0),
															CONSTRAINT [CR_FacilitiesHydroTreater_Processed_Pcnt_MaxIncl_100.0]	CHECK([Processed_Pcnt] <= 100.0),
	[Density_SG]			FLOAT					NULL	CONSTRAINT [CR_FacilitiesHydroTreater_Density_SG_MinIncl_0.0]		CHECK([Density_SG] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesHydroTreater]		PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [fact].[t_FacilitiesHydroTreater_u]
ON [fact].[FacilitiesHydroTreater]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[FacilitiesHydroTreater].[SubmissionId]		= INSERTED.[SubmissionId];

END;
GO