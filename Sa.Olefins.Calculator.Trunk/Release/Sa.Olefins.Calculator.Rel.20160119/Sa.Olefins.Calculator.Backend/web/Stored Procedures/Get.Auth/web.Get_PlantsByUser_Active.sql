﻿CREATE PROCEDURE [web].[Get_PlantsByUser_Active]
(
	@UserId		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		pbl.[JoinId],
		pbl.[PlantId],
		pbl.[LoginId],
		pbl.[PlantName],
		pbl.[LoginTag]
	FROM [auth].[Get_PlantsByLogin_Active](@UserId)	pbl
	ORDER BY
		pbl.[PlantName]	ASC;

END;