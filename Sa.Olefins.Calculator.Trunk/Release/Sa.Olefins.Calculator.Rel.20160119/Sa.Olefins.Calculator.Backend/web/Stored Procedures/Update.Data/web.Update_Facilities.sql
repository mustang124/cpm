﻿CREATE PROCEDURE [web].[Update_Facilities]
(
	@SubmissionId					INT				= NULL,

	--	Submissions
	@SubmissionName					NVARCHAR(42)	= NULL,
	@DateBeg						DATE			= NULL,
	@DateEnd						DATE			= NULL,
	@SubmissionComment				NVARCHAR(MAX)	= NULL,

	@FeedClass						VARCHAR(42)		= NULL,

	--	Capacity
	@Ethylene_StreamDay_MTSD		FLOAT			= NULL,
	@Propylene_StreamDay_MTSD		FLOAT			= NULL,
	@Olefins_StreamDay_MTSD			FLOAT			= NULL,

	--	Facility Count
	@FracFeed_UnitCount				INT				= NULL,
	@BoilHP_UnitCount				INT				= NULL,
	@BoilLP_UnitCount				INT				= NULL,
	@ElecGen_UnitCount				INT				= NULL,
	@HydroPurCryogenic_UnitCount	INT				= NULL,
	@HydroPurPSA_UnitCount			INT				= NULL,
	@HydroPurMembrane_UnitCount		INT				= NULL,
	@TowerPyroGasHT_UnitCount		INT				= NULL,
	@Trains_UnitCount				INT				= NULL,
	
	--	Facility - Boilers
	@BoilHP_Rate_kLbHr				FLOAT			= NULL,
	@BoilLP_Rate_kLbHr				FLOAT			= NULL,
	
	--	Facility - Electricity Generation
	@Capacity_MW					FLOAT			= NULL,

	--	Facility - FracFeed
	@FracFeed_Quantity_kBSD			FLOAT			= NULL,
	@FracFeed_StreamId				INT				= NULL,
	@FracFeed_Throughput_kMT		FLOAT			= NULL,
	@FracFeed_Density_SG			FLOAT			= NULL,

	--	Facility - HydroTreater
	@HT_Quantity_kBSD				FLOAT			= NULL,
	@HT_HydroTreaterTypeId			INT				= NULL,
	@HT_Pressure_PSIg				FLOAT			= NULL,
	@HT_Processed_kMT				FLOAT			= NULL,
	@HT_Processed_Pcnt				FLOAT			= NULL,
	@HT_Density_SG					FLOAT			= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@ErrorCount		INT;

	--	Submissions
	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Submissions] t WHERE t.[SubmissionId] = @SubmissionId)
	BEGIN
		EXECUTE	@SubmissionId	= [stage].[Update_Submission] @SubmissionId, @SubmissionName, @DateBeg, @DateEnd;
	END
	ELSE
	BEGIN
		EXECUTE	@SubmissionId	= [stage].[Insert_Submission] @SubmissionName, @DateBeg, @DateEnd;
	END;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[SubmissionComments] t WHERE t.[SubmissionId] = @SubmissionId) 
	BEGIN
		EXECUTE	@ErrorCount		= [stage].[Update_SubmissionComments] @SubmissionId, @SubmissionComment;
	END
	ELSE
	BEGIN
		IF(@SubmissionComment <> '')
		EXECUTE	@ErrorCount		= [stage].[Insert_SubmissionComments] @SubmissionId, @SubmissionComment;
	END;

	--	Capacity
	DECLARE	@EthyleneId		INT = [dim].[Return_StreamId]('Ethylene');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Capacity] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamId] = @EthyleneId)
	BEGIN
		SET @Ethylene_StreamDay_MTSD = COALESCE(@Ethylene_StreamDay_MTSD, 0.0)
		EXECUTE	@ErrorCount	 = [stage].[Update_Capacity] @SubmissionId, @EthyleneId, NULL, @Ethylene_StreamDay_MTSD, NULL;
	END
	ELSE
	BEGIN
		IF (@Ethylene_StreamDay_MTSD >= 0.0)
		EXECUTE @ErrorCount	 = [stage].[Insert_Capacity] @SubmissionId, @EthyleneId, NULL, @Ethylene_StreamDay_MTSD, NULL;
	END

	IF	(COALESCE(@Olefins_StreamDay_MTSD, 0.0) > @Ethylene_StreamDay_MTSD)
	AND (COALESCE(@Propylene_StreamDay_MTSD, 0.0) <> COALESCE(@Olefins_StreamDay_MTSD, 0.0) - @Ethylene_StreamDay_MTSD)
	BEGIN
		SET @Propylene_StreamDay_MTSD = @Olefins_StreamDay_MTSD - @Ethylene_StreamDay_MTSD;
	END;
	
	DECLARE	@PropyleneId	INT = [dim].[Return_StreamId]('Propylene');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Capacity] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamId] = @PropyleneId)
	BEGIN
		SET @Propylene_StreamDay_MTSD = COALESCE(@Propylene_StreamDay_MTSD, 0.0);
		EXECUTE	@ErrorCount		= [stage].[Update_Capacity] @SubmissionId, @PropyleneId, NULL, @Propylene_StreamDay_MTSD, NULL;
	END
	ELSE
	BEGIN
		IF (@Propylene_StreamDay_MTSD >= 0.0)
		EXECUTE	@ErrorCount		= [stage].[Insert_Capacity] @SubmissionId, @PropyleneId, NULL, @Propylene_StreamDay_MTSD, NULL;
	END;

	--	Facility
	DECLARE @FacilityId		INT;

	SET @FacilityId = [dim].[Return_FacilityId]('FracFeed');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @FracFeed_UnitCount = COALESCE(@FracFeed_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @FracFeed_UnitCount;
	END
	ELSE
	BEGIN
		IF(@FracFeed_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @FracFeed_UnitCount;
	END;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[FacilitiesFractionator] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @FracFeed_Quantity_kBSD		=  COALESCE(@FracFeed_Quantity_kBSD, 0.0);
		SET @FracFeed_StreamId			=  COALESCE(@FracFeed_StreamId, [dim].[Return_StreamId]('NoStream'));
		SET @FracFeed_Throughput_kMT	=  COALESCE(@FracFeed_Throughput_kMT, 0.0);
		SET @FracFeed_Density_SG		=  COALESCE(@FracFeed_Density_SG, 0.0);
		EXECUTE	@ErrorCount		= [stage].[Update_FacilitiesFractionator] @SubmissionId, @FacilityId, @FracFeed_Quantity_kBSD, @FracFeed_StreamId, @FracFeed_Throughput_kMT, @FracFeed_Density_SG;
	END
	ELSE
	BEGIN
		IF(@FracFeed_Quantity_kBSD >= 0.0 AND @FracFeed_StreamId >= 1 AND @FracFeed_Throughput_kMT >= 0.0 AND @FracFeed_Density_SG >= 0.0)
		EXECUTE	@ErrorCount	= [stage].[Insert_FacilitiesFractionator] @SubmissionId, @FacilityId, @FracFeed_Quantity_kBSD, @FracFeed_StreamId, @FracFeed_Throughput_kMT, @FracFeed_Density_SG;
	END;
	
	SET @FacilityId = [dim].[Return_FacilityId]('BoilHP');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @BoilHP_UnitCount = COALESCE(@BoilHP_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @BoilHP_UnitCount;
	END
	ELSE
	BEGIN
		IF(@BoilHP_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @BoilHP_UnitCount;
	END;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[FacilitiesBoilers] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @BoilHP_Rate_kLbHr	=  COALESCE(@BoilHP_Rate_kLbHr, 0.0);
		EXECUTE	@ErrorCount		= [stage].[Update_FacilitiesBoilers] @SubmissionId, @FacilityId, @BoilHP_Rate_kLbHr, NULL;
	END
	ELSE
	BEGIN
		IF(@BoilHP_Rate_kLbHr >= 0.0)
		EXECUTE	@ErrorCount	= [stage].[Insert_FacilitiesBoilers] @SubmissionId, @FacilityId, @BoilHP_Rate_kLbHr, NULL;
	END;

	SET @FacilityId = [dim].[Return_FacilityId]('BoilLP');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @BoilLP_UnitCount = COALESCE(@BoilLP_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @BoilLP_UnitCount;
	END
	ELSE
	BEGIN
		IF(@BoilLP_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @BoilLP_UnitCount;
	END;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[FacilitiesBoilers] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @BoilLP_Rate_kLbHr	=  COALESCE(@BoilLP_Rate_kLbHr, 0.0);
		EXECUTE	@ErrorCount		= [stage].[Update_FacilitiesBoilers] @SubmissionId, @FacilityId, @BoilLP_Rate_kLbHr, NULL;
	END
	ELSE
	BEGIN
		IF(@BoilLP_Rate_kLbHr >= 0.0)
		EXECUTE	@ErrorCount	= [stage].[Insert_FacilitiesBoilers] @SubmissionId, @FacilityId, @BoilLP_Rate_kLbHr, NULL;
	END;

	SET @FacilityId = [dim].[Return_FacilityId]('ElecGen');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @ElecGen_UnitCount = COALESCE(@ElecGen_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @ElecGen_UnitCount;
	END
	ELSE
	BEGIN
		IF(@ElecGen_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @ElecGen_UnitCount;
	END;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[FacilitiesElecGeneration] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @Capacity_MW =  COALESCE(@Capacity_MW, 0.0);
		EXECUTE	@ErrorCount = [stage].[Update_FacilitiesElecGeneration] @SubmissionId, @FacilityId, @Capacity_MW;
	END
	ELSE
	BEGIN
		IF(@Capacity_MW >= 0.0)
		EXECUTE	@ErrorCount	= [stage].[Insert_FacilitiesElecGeneration] @SubmissionId, @FacilityId, @Capacity_MW;
	END;

	SET @FacilityId = [dim].[Return_FacilityId]('HydroPurCryogenic');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @HydroPurCryogenic_UnitCount = COALESCE(@HydroPurCryogenic_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @HydroPurCryogenic_UnitCount;
	END
	ELSE
	BEGIN
		IF(@HydroPurCryogenic_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @HydroPurCryogenic_UnitCount;
	END;

	SET @FacilityId = [dim].[Return_FacilityId]('HydroPurPSA');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @HydroPurPSA_UnitCount = COALESCE(@HydroPurPSA_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @HydroPurPSA_UnitCount;
	END
	ELSE
	BEGIN
		IF(@HydroPurPSA_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @HydroPurPSA_UnitCount;
	END;

	SET @FacilityId = [dim].[Return_FacilityId]('HydroPurMembrane');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @HydroPurMembrane_UnitCount = COALESCE(@HydroPurMembrane_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @HydroPurMembrane_UnitCount;
	END
	ELSE
	BEGIN
		IF(@HydroPurMembrane_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @HydroPurMembrane_UnitCount;
	END;

	SET @FacilityId = [dim].[Return_FacilityId]('TowerPyroGasHT');

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[Facilities] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @TowerPyroGasHT_UnitCount = COALESCE(@TowerPyroGasHT_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_Facilities] @SubmissionId, @FacilityId, @TowerPyroGasHT_UnitCount;
	END
	ELSE
	BEGIN
		IF(@TowerPyroGasHT_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_Facilities] @SubmissionId, @FacilityId, @TowerPyroGasHT_UnitCount;
	END;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[FacilitiesHydroTreater] t WHERE t.[SubmissionId] = @SubmissionId AND t.[FacilityId] = @FacilityId)
	BEGIN
		SET @HT_Quantity_kBSD		= COALESCE(@HT_Quantity_kBSD, 0.0);
		SET @HT_HydroTreaterTypeId	= COALESCE(@HT_HydroTreaterTypeId, [dim].[Return_StreamId]('NoStream'));
		SET @HT_Processed_Pcnt		= COALESCE(@HT_Processed_Pcnt, 0.0);
		SET @HT_Density_SG			= COALESCE(@HT_Density_SG, 0.0);
		EXECUTE	@ErrorCount			= [stage].[Update_FacilitiesHydroTreater] @SubmissionId, @FacilityId, @HT_Quantity_kBSD, @HT_HydroTreaterTypeId, @HT_Pressure_PSIg, @HT_Processed_kMT, @HT_Processed_Pcnt, @HT_Density_SG;
	END
	ELSE
	BEGIN
		IF(@HT_Quantity_kBSD >= 0.0 AND @HT_HydroTreaterTypeId >= 1 AND @HT_Processed_Pcnt >= 0.0 AND @HT_Density_SG >= 0.0)
		EXECUTE	@ErrorCount	= [stage].[Insert_FacilitiesHydroTreater] @SubmissionId, @FacilityId, @HT_Quantity_kBSD, @HT_HydroTreaterTypeId, @HT_Pressure_PSIg, @HT_Processed_kMT, @HT_Processed_Pcnt, @HT_Density_SG;
	END;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[FacilitiesTrains] t WHERE t.[SubmissionId] = @SubmissionId)
	BEGIN
		SET @Trains_UnitCount = COALESCE(@Trains_UnitCount, 0.0)
		EXECUTE	@ErrorCount		= [stage].[Update_FacilitiesTrains] @SubmissionId, @Trains_UnitCount;
	END
	ELSE
	BEGIN
		IF(@Trains_UnitCount >= 1)
		EXECUTE	@ErrorCount		= [stage].[Insert_FacilitiesTrains] @SubmissionId, @Trains_UnitCount;
	END;

	--	Feed Class
	IF(@FeedClass <> '')
	BEGIN

		DECLARE @FeedClassId	INT = CONVERT(INT, @FeedClass);

		IF EXISTS (SELECT TOP 1 1 FROM [stage].[FeedClass] t WHERE t.[SubmissionId] = @SubmissionId)
		BEGIN
			EXECUTE	@ErrorCount		= [stage].[Update_FeedClass] @SubmissionId, @FeedClassId;
		END
		ELSE
		BEGIN
			EXECUTE	@ErrorCount		= [stage].[Insert_FeedClass] @SubmissionId, @FeedClassId;
		END;

	END;

	SELECT @SubmissionId;
	RETURN @SubmissionId;

END;