﻿CREATE TABLE [dim].[Facility_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Bridge_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Bridge_FacilityId]			REFERENCES [dim].[Facility_LookUp] ([FacilityId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Facility_Bridge_Parent_Ancestor]		FOREIGN KEY ([MethodologyId], [FacilityId])
																												REFERENCES [dim].[Facility_Parent] ([MethodologyId], [FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Bridge_DescendantID]		REFERENCES [dim].[Facility_LookUp] ([FacilityId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Facility_Bridge_Parent_Descendant]	FOREIGN KEY ([MethodologyId], [DescendantId])
																												REFERENCES [dim].[Facility_Parent] ([MethodologyId], [FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_Facility_Bridge_DescendantOperator]	DEFAULT ('+')
															CONSTRAINT [FK_Facility_Bridge_DescendantOperator]	REFERENCES [dim].[Operator] ([Operator])							ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Facility_Bridge_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_Bridge_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_Bridge_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_Bridge_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Facility_Bridge]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FacilityId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Facility_Bridge_u]
ON [dim].[Facility_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Facility_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Facility_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Facility_Bridge].[FacilityId]		= INSERTED.[FacilityId]
		AND	[dim].[Facility_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;
GO