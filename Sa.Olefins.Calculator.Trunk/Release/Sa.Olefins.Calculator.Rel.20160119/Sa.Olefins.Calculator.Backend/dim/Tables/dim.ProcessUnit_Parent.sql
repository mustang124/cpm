﻿CREATE TABLE [dim].[ProcessUnit_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_ProcessUnit_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])								ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ProcessUnitId]			INT					NOT	NULL	CONSTRAINT [FK_ProcessUnit_Parent_LookUp_ProcessUnit]	REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])							ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_ProcessUnit_Parent_LookUp_Parent]		REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_ProcessUnit_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																													REFERENCES [dim].[ProcessUnit_Parent] ([MethodologyId], [ProcessUnitId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_ProcessUnit_Parent_Operator]				DEFAULT ('+')
															CONSTRAINT [FK_ProcessUnit_Parent_Operator]				REFERENCES [dim].[Operator] ([Operator])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ProcessUnit_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_Parent_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ProcessUnit_Parent]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [dim].[t_ProcessUnit_Parent_u]
ON [dim].[ProcessUnit_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnit_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[ProcessUnit_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[ProcessUnit_Parent].[ProcessUnitId]		= INSERTED.[ProcessUnitId];

END;
GO