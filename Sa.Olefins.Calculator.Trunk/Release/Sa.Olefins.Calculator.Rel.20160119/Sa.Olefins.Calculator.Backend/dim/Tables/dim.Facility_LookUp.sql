﻿CREATE TABLE [dim].[Facility_LookUp]
(
	[FacilityId]			INT					NOT NULL	IDENTITY (1, 1),

	[FacilityTag]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Facility_LookUp_FacilitiesTag]			CHECK ([FacilityTag]	<> ''),
															CONSTRAINT [UK_Facility_LookUp_FacilitiesTag]			UNIQUE NONCLUSTERED ([FacilityTag]),
	[FacilityName]			VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Facility_LookUp_FacilitiesName]			CHECK ([FacilityName]	<> ''),
															CONSTRAINT [UK_Facility_LookUp_FacilitiesName]			UNIQUE NONCLUSTERED ([FacilityName]),
	[FacilityDetail]		VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Facility_LookUp_FacilitiesDetail]		CHECK ([FacilityDetail]	<> ''),
															CONSTRAINT [UK_Facility_LookUp_FacilitiesDetail]		UNIQUE NONCLUSTERED ([FacilityDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Facility_LookUp_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_LookUp_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_LookUp_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_LookUp_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Facility_LookUp]				PRIMARY KEY CLUSTERED ([FacilityId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Facility_LookUp_u]
ON [dim].[Facility_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Facility_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Facility_LookUp].[FacilityId]		= INSERTED.[FacilityId];

END;
GO