﻿CREATE TABLE [dim].[Facility_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Parent_LookUp_Facilities]	REFERENCES [dim].[Facility_LookUp] ([FacilityId])					ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_Facility_Parent_LookUp_Parent]		REFERENCES [dim].[Facility_LookUp] ([FacilityId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Facility_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																												REFERENCES [dim].[Facility_Parent] ([MethodologyId], [FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_Facility_Parent_Operator]			DEFAULT ('+')
															CONSTRAINT [FK_Facility_Parent_Operator]			REFERENCES [dim].[Operator] ([Operator])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Facility_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_Parent_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Facility_Parent]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Facility_Parent_u]
ON [dim].[Facility_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Facility_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Facility_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Facility_Parent].[FacilityId]		= INSERTED.[FacilityId];

END;
GO