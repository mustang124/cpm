﻿CREATE TABLE [dim].[Standard_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Standard_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StandardId]			INT					NOT	NULL	CONSTRAINT [FK_Standard_Parent_LookUp_Standard]		REFERENCES [dim].[Standard_LookUp] ([StandardId])					ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_Standard_Parent_LookUp_Parent]		REFERENCES [dim].[Standard_LookUp] ([StandardId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Standard_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																												REFERENCES [dim].[Standard_Parent] ([MethodologyId], [StandardId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_Standard_Parent_Operator]			DEFAULT ('+')
															CONSTRAINT [FK_Standard_Parent_Operator]			REFERENCES [dim].[Operator] ([Operator])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Standard_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_Parent_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Standard_Parent]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Standard_Parent_u]
ON [dim].[Standard_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Standard_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Standard_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Standard_Parent].[StandardId]		= INSERTED.[StandardId];

END;
GO