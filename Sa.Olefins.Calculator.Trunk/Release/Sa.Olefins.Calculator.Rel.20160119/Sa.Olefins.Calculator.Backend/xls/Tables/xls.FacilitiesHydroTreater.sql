﻿CREATE TABLE [xls].[FacilitiesHydroTreater]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_FacilitiesHydroTreater_Refnum]						CHECK([Refnum] <> ''),

	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesHydroTreater_Facility_LookUp]				REFERENCES [dim].[Facility_LookUp]([FacilityId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Quantity_BSD]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesHydroTreater_Quantity_BSD_MinIncl_0.0]		CHECK([Quantity_BSD] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesHydroTreater]		PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_FacilitiesHydroTreater_u]
ON [xls].[FacilitiesHydroTreater]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesHydroTreater].[Refnum]	= INSERTED.[Refnum];

END;
GO