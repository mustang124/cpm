﻿CREATE TABLE [xls].[StreamRecovered]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_StreamRecovered_Refnum]							CHECK([Refnum] <> ''),
	[StreamNumber]			INT					NOT	NULL,

	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_StreamRecovered_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])										ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Quantity_Dur_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecovered_Quantity_Dur_kMT_MinIncl_0.0]	CHECK([Quantity_Dur_kMT] >= 0.0),
	[Quantity_Ann_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecovered_Quantity_Ann_kMT_MinIncl_0.0]	CHECK([Quantity_Ann_kMT] >= 0.0),

	[Recovered_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MinIncl_0.0]	CHECK([Recovered_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MaxIncl_100.0]	CHECK([Recovered_WtPcnt] <= 100.0),
	
	[_Recovered_Dur_kMT]	AS CONVERT(FLOAT, [Quantity_Dur_kMT] * [Recovered_WtPcnt] / 100.0)
							PERSISTED			NOT	NULL,
	[_Recovered_Ann_kMT]	AS CONVERT(FLOAT, [Quantity_Ann_kMT] * [Recovered_WtPcnt] / 100.0)
							PERSISTED			NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamRecovered]				PRIMARY KEY CLUSTERED ([Refnum] DESC, [StreamNumber] ASC)
);
GO

CREATE TRIGGER [xls].[t_StreamRecovered_u]
ON [xls].[StreamRecovered]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StreamRecovered]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StreamRecovered].[Refnum]		= INSERTED.[Refnum]
		AND	[xls].[StreamRecovered].[StreamNumber]	= INSERTED.[StreamNumber];

END;
GO