﻿CREATE TABLE [xls].[FeedClass]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_FeedClass_Refnum]			CHECK([Refnum] <> ''),

	[FeedClassId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_FeedClassId]		REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FeedClass_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FeedClass]					PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_FeedClass_u]
ON [xls].[FeedClass]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FeedClass]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FeedClass].[Refnum]		= INSERTED.[Refnum];

END;
GO