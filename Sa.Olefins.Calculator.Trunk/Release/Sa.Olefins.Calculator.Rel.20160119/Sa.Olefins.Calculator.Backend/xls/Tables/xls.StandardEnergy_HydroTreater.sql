﻿CREATE TABLE [xls].[StandardEnergy_HydroTreater]
(
	[Refnum]					VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_StandardEnergy_HydroTreater_Refnum]								CHECK([Refnum] <> ''),

	[Quantity_kMT]				FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_Quantity_kMT_MinIncl_0.0]			CHECK([Quantity_kMT] >= 0.0),
	[Quantity_Barrels]			FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_Quantity_Barrels_MinIncl_0.0]		CHECK([Quantity_Barrels] >= 0.0),
	[Quantity_BarrelsDay]		FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_Quantity_BarrelsDay_MinIncl_0.0]		CHECK([Quantity_BarrelsDay] >= 0.0),
	[StandardEnergy_MBtuDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_StandardEnergy_MBtuDay_MinIncl_0.0]	CHECK([StandardEnergy_MBtuDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_HydroTreater]		PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_StandardEnergy_HydroTreater_u]
ON [xls].[StandardEnergy_HydroTreater]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StandardEnergy_HydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StandardEnergy_HydroTreater].[Refnum]	= INSERTED.[Refnum];

END;
GO