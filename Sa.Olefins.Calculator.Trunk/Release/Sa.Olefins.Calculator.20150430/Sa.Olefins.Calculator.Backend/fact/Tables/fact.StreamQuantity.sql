﻿CREATE TABLE [fact].[StreamQuantity]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamQuantity_Submissions]					REFERENCES [fact].[Submissions]([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [CR_StreamQuantity_StreamNumber]					CHECK([StreamNumber] > 0),

	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_StreamQuantity_Stream_LookUp]				REFERENCES [dim].[Stream_LookUp]([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Quantity_kMT]			FLOAT				NOT	NULL	CONSTRAINT [CR_StreamQuantity_Quantity_kMT_MinIncl_0.0]		CHECK([Quantity_kMT] >= 0.0),

															CONSTRAINT [UK_StreamQuantity_Attributes]					UNIQUE([SubmissionId] ASC, [StreamNumber] ASC, [StreamId] ASC),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamQuantity_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamQuantity_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamQuantity_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamQuantity_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamQuantity]				PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamNumber] ASC)
);
GO

CREATE INDEX [IX_StreamQuantity_StreamId]
ON [fact].[StreamQuantity]([SubmissionId] ASC, [StreamId] ASC)
INCLUDE([Quantity_kMT])
GO

CREATE INDEX [IX_StreamQuantity_ReferencedFields]
ON [fact].[StreamQuantity]([SubmissionId] ASC, [StreamNumber] ASC, [StreamId] ASC)
INCLUDE([Quantity_kMT])
GO

CREATE TRIGGER [fact].[t_StreamQuantity_u]
ON [fact].[StreamQuantity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamQuantity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamQuantity].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[fact].[StreamQuantity].[StreamNumber]		= INSERTED.[StreamNumber];

END;
GO

EXEC sp_addextendedproperty @name = N'MS_Description',
	@value = N'1000 Light; 2000 Liquid; 3000 Recycle; 4000 Supplemental; 5000 Product',
	@level0type = N'SCHEMA',
	@level0name = N'fact',
	@level1type = N'TABLE',
	@level1name = N'StreamQuantity',
	@level2type = N'COLUMN',
	@level2name = N'StreamNumber'