﻿CREATE PROCEDURE [fact].[Insert_StreamCompositionMol]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[StreamCompositionMol]([SubmissionId], [StreamNumber], [ComponentId], [Component_MolPcnt])
	SELECT
		c.[SubmissionId],
		c.[StreamNumber],
		c.[ComponentId],
		c.[Component_MolPcnt]
	FROM [stage].[StreamCompositionMol]		c
	INNER JOIN [stage].[StreamQuantity]		q
		ON	q.[SubmissionId]		= c.[SubmissionId]
		AND	q.[StreamNumber]		= c.[StreamNumber]
		AND	q.[Quantity_kMT]		> 0
	WHERE	c.[SubmissionId]		= @SubmissionId
		AND	c.[Component_MolPcnt]	> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;