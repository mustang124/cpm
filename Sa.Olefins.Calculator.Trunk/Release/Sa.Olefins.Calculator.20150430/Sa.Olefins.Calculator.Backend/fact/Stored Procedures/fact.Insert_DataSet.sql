﻿CREATE PROCEDURE [fact].[Insert_DataSet]
(
	@SubmissionId			INT
)
AS
BEGIN

	DECLARE @TimeBeg		DATETIMEOFFSET = SYSDATETIMEOFFSET();
	DECLARE @Error			INT = 0;
	DECLARE @Error_Count	INT = 0;

	EXECUTE @Error = [fact].[Insert_Audit]						@SubmissionId, @TimeBeg;
																				IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_Submission]					@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_SubmissionComments]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_Capacity]					@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_Facilities]					@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesBoilers]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesElecGeneration]	@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesFractionator]		@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesHydroTreater]		@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesTrains]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_StreamQuantity]				@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamDescription]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamComposition]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamCompositionMol]		@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamDensity]				@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamRecovered]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamRecycled]				@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	
	DECLARE @TimeEnd		DATETIMEOFFSET = SYSDATETIMEOFFSET();

	EXECUTE @Error = [fact].[Update_Audit]						@SubmissionId, @TimeEnd, @Error_Count;

END;