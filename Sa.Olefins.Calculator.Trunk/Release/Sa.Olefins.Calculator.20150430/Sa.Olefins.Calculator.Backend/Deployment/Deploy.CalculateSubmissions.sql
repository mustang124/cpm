﻿PRINT 'Initializing Calculations';

SET NOCOUNT ON;

DECLARE @curError_Count	INT;
DECLARE @curMethodologyId	INT;
DECLARE @curSubmissionId	INT;

DECLARE curTemp CURSOR LOCAL FAST_FORWARD FOR
SELECT s.[SubmissionId]
FROM [stage].[Submissions] s;

IF (@curMethodologyId IS NULL)
SET @curMethodologyId = IDENT_CURRENT('[ante].[Methodology]');
	
OPEN curTemp;
FETCH NEXT FROM curTemp INTO @curSubmissionId;
WHILE @@FETCH_STATUS = 0
BEGIN

	PRINT 'Calculating: ' + CONVERT(VARCHAR, @curSubmissionId);
	EXECUTE [calc].[ProcessSubmission] @curMethodologyId, @curSubmissionId;

FETCH NEXT FROM curTemp INTO @curSubmissionId;
END;

CLOSE curTemp;
DEALLOCATE curTemp;