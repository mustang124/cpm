﻿PRINT 'INSERT INTO [dim].[Operator]([Operator])';

INSERT INTO [dim].[Operator]([Operator])
SELECT t.[Operator]
FROM (
	VALUES
		('*'),
		('+'),
		('-'),
		('/'),
		('~')
	) t([Operator]);