﻿PRINT 'INSERT INTO [ante].[NicenessRanges]([MethodologyId], [StreamId], [HistoricalMin], [HistoricalMax], [YieldMin], [YieldMax], [EnergyMin], [EnergyMax])';

INSERT INTO [ante].[NicenessRanges]([MethodologyId], [StreamId], [HistoricalMin], [HistoricalMax], [YieldMin], [YieldMax], [EnergyMin], [EnergyMax])
SELECT t.[MethodologyId], t.[StreamId], t.[HistoricalMin], t.[HistoricalMax], t.[YieldMin], t.[YieldMax], t.[EnergyMin], t.[EnergyMax]
FROM (VALUES
	(@MethodologyId, [dim].[Return_StreamId]('Condensate'), 0.924886048301329, 1.02939776622654, 0.95, 1.05, 0.99, 1.01),
	(@MethodologyId, [dim].[Return_StreamId]('HeavyNGL'), 0.924886048301329, 1.02939776622654, 0.95, 1.05, 0.99, 1.01),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaLt'), 0.945620391640057, 1.0424774710542, 0.95, 1.05, 0.99, 1.01),
	(@MethodologyId, [dim].[Return_StreamId]('Raffinate'), 0.945620391640057, 1.0424774710542, 0.95, 1.05, 0.99, 1.01),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaFr'), 0.957707923676346, 1.02884158170874, 0.95, 1.05, 0.99, 1.01),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaHv'), 0.857970999181025, 1.06728778453785, 0.95, 1.05, 0.99, 1.01),
	(@MethodologyId, [dim].[Return_StreamId]('Diesel'), 0.971597340962019, 1.06875708046953, 0.99, 1.01, 1, 1),
	(@MethodologyId, [dim].[Return_StreamId]('GasOilHv'), 0.964610330890017, 1.05379260871715, 0.99, 1.01, 1, 1),
	(@MethodologyId, [dim].[Return_StreamId]('GasOilHt'), 0.952412345400265, 1.0359841042563, 0.99, 1.01, 1, 1)
	)t([MethodologyId], [StreamId], [HistoricalMin], [HistoricalMax], [YieldMin], [YieldMax], [EnergyMin], [EnergyMax]);