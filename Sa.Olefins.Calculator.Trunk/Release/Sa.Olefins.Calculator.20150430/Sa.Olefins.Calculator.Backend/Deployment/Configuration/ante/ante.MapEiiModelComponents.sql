﻿PRINT 'INSERT INTO [ante].[MapEiiModelComponents]([MethodologyId], [FreshComponentId], [ModelComponentId])';

INSERT INTO [ante].[MapEiiModelComponents]([MethodologyId], [FreshComponentId], [ModelComponentId])
SELECT t.[MethodologyId], t.[FreshComponentId], t.[ModelComponentId]
FROM (VALUES
	(@MethodologyId, dim.Return_ComponentId('CH4'),		dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_ComponentId('C2H6'),	dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_ComponentId('C2H4'),	dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_ComponentId('CO_CO2'),	dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_ComponentId('H2'),		dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_ComponentId('S'),		dim.Return_ComponentId('C2H6')),

	(@MethodologyId, dim.Return_ComponentId('C3H8'),	dim.Return_ComponentId('C3H8')),
	(@MethodologyId, dim.Return_ComponentId('C3H6'),	dim.Return_ComponentId('C3H8')),

	(@MethodologyId, dim.Return_ComponentId('NBUTA'),	dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('IBUTA'),	dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('B2'),		dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('B1'),		dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('C4H6'),	dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('NC5'),		dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('IC5'),		dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('NC6'),		dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('C6ISO'),	dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('C7H16'),	dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_ComponentId('C8H18'),	dim.Return_ComponentId('C4H10'))
	)t([MethodologyId], [FreshComponentId], [ModelComponentId]);