﻿PRINT 'INSERT INTO [ante].[UtilizationFilter]([MethodologyId], [StreamId], [ComponentId])';

INSERT INTO [ante].[UtilizationFilter]([MethodologyId], [StreamId], [ComponentId])
SELECT t.[MethodologyId], t.[StreamId], t.[ComponentId]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('EthyleneCG'), dim.Return_ComponentId('C2H4')),
	(@MethodologyId, dim.Return_StreamId('EthylenePG'), dim.Return_ComponentId('C2H4')),
	(@MethodologyId, dim.Return_StreamId('PropylenePG'), dim.Return_ComponentId('C3H6')),
	(@MethodologyId, dim.Return_StreamId('PropyleneCG'), dim.Return_ComponentId('C3H6')),
	(@MethodologyId, dim.Return_StreamId('PropyleneRG'), dim.Return_ComponentId('C3H6')),
	(@MethodologyId, dim.Return_StreamId('ProdOther'), dim.Return_ComponentId('C2H4')),
	(@MethodologyId, dim.Return_StreamId('ProdOther'), dim.Return_ComponentId('C3H6'))
	)t([MethodologyId], [StreamId], [ComponentId]);