﻿PRINT 'INSERT INTO [ante].[MapFactorHydroTreater]([MethodologyId], [HydroTreaterTypeId], [FactorId])';

INSERT INTO [ante].[MapFactorHydroTreater]([MethodologyId], [HydroTreaterTypeId], [FactorId])
SELECT t.[MethodologyId], t.[HydroTreaterTypeId], t.[FactorId]
FROM (VALUES
	(@MethodologyId, dim.Return_HydroTreaterTypeId('B'),  dim.Return_FactorId('TowerPyroGasB')),
	(@MethodologyId, dim.Return_HydroTreaterTypeId('DS'), dim.Return_FactorId('TowerPyroGasDS')),
	(@MethodologyId, dim.Return_HydroTreaterTypeId('SS'), dim.Return_FactorId('TowerPyroGasSS'))
	)t([MethodologyId], [HydroTreaterTypeId], [FactorId]);