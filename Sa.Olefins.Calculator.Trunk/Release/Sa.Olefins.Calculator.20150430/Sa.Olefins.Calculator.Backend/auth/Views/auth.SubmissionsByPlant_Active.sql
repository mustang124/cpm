﻿CREATE VIEW [auth].[SubmissionsByPlant_Active]
WITH SCHEMABINDING
AS
SELECT
	jps.[JoinId],
	jps.[PlantId],
	jps.[SubmissionId],
	p.[PlantName],
	z.[SubmissionName],
	z.[DateBeg],
	z.[DateEnd],
	a.[TimeBeg],
	a.[TimeEnd]
FROM [auth].[JoinPlantSubmission]	jps
INNER JOIN [auth].[Plants]			p
	ON	p.[PlantId]			= jps.[PlantId]
INNER JOIN [fact].[Submissions]		z
	ON	z.[SubmissionId]	= jps.[SubmissionId]
INNER JOIN [calc].[Audit]			a
	ON	a.[SubmissionId]	= jps.[SubmissionId]
WHERE	jps.[Active]		= 1
	AND	p.[Active]			= 1
	AND	z.[Active]			= 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UX_SubmissionsByPlant_Active]
ON [auth].[SubmissionsByPlant_Active]([PlantId] ASC, [SubmissionId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_SubmissionsByPlant_Active_Get]
ON [auth].[SubmissionsByPlant_Active]([PlantId] ASC)
INCLUDE([PlantName], [SubmissionId], [SubmissionName], [TimeBeg], [TimeEnd]);
GO