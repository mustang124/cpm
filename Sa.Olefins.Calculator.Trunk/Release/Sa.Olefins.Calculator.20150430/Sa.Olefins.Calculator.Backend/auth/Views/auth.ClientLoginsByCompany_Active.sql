﻿CREATE VIEW [auth].[ClientLoginsByCompany_Active]
WITH SCHEMABINDING
AS
SELECT
	jcl.[JoinId],
	jcl.[CompanyId],
	jcl.[LoginId],
	c.[CompanyName],
	l.[LoginTag],
	a.[NameLast],
	a.[NameFirst],
	a.[_NameFull],
	a.[_NameComma],
	a.[eMail],
	a.[RoleId],
	r.[RoleLevel],
	p.[pSalt],
	p.[pWord]
FROM [auth].[JoinCompanyLogin]			jcl
INNER JOIN [auth].[Companies]			c
	ON	c.[CompanyId]	= jcl.[CompanyId]
INNER JOIN [auth].[Logins]				l
	ON	l.[LoginId]		= jcl.[LoginId]
INNER JOIN [auth].[LoginsAttributes]	a
	ON	a.[LoginId]		= jcl.[LoginId]
INNER JOIN [auth].[LoginsPassWords]		p
	ON	p.[LoginId]		= jcl.[LoginId]
INNER JOIN [auth].[Roles]				r
	ON	r.[RoleId]		=  a.[RoleId]
	AND	r.[RoleLevel]	>= 5
WHERE	jcl.[Active]	=  1
	AND	c.[Active]		=  1
	AND	l.[Active]		=  1;
GO

CREATE UNIQUE CLUSTERED INDEX [UX_ClientLoginsByCompany_Active]
ON [auth].[ClientLoginsByCompany_Active]([CompanyId] ASC, [LoginId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_ClientLoginsByCompany_Active_Get]
ON [auth].[ClientLoginsByCompany_Active]([CompanyId] ASC, [LoginId] ASC)
INCLUDE ([CompanyName], [LoginTag], [NameLast], [NameFirst], [_NameFull], [_NameComma], [eMail], [RoleId], [pSalt], [pWord]);
GO
