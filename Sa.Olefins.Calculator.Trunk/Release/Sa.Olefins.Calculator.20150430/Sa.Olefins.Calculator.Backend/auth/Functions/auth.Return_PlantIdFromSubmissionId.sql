﻿CREATE FUNCTION [auth].[Return_PlantIdFromSubmissionId]
(
	@SubmissionId			INT
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT TOP 1
		@Id = jps.[JoinId]
	FROM [auth].[JoinPlantSubmission]	jps
	WHERE	jps.[Active] = 1
		AND	jps.[SubmissionId] = @SubmissionId;

	RETURN @Id;

END
