﻿CREATE FUNCTION [auth].[Return_SubmissionId]
(
	@PlantName			NVARCHAR(128),
	@SubmissionName		NVARCHAR(128)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT TOP 1
		@Id = p.[SubmissionId]
	FROM [auth].[SubmissionsByPlant_Active]	p WITH(NOEXPAND)
	WHERE	p.[PlantName]		= @PlantName
		AND	p.[SubmissionName]	= @SubmissionName
	ORDER BY
		p.[SubmissionId] ASC;

	RETURN @Id;

END;