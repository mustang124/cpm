﻿CREATE TABLE [auth].[JoinLoginSubmission]
(
	[JoinId]				INT					NOT	NULL	IDENTITY (1, 1),

	[LoginId]				INT					NOT	NULL	CONSTRAINT [FK_JoinLoginSubmission_Logins]			REFERENCES [auth].[Logins] ([LoginId])				ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_JoinLoginSubmission_Submissions]		REFERENCES [stage].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_JoinLoginSubmission_Active]			DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_JoinLoginSubmission_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinLoginSubmission_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinLoginSubmission_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinLoginSubmission_tsModifiedApp]	DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_JoinLoginSubmission]		PRIMARY KEY NONCLUSTERED([JoinId] ASC),
	CONSTRAINT [UK_JoinLoginSubmission]		UNIQUE CLUSTERED ([LoginId] ASC, [SubmissionId] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinLoginSubmission_Active]
ON [auth].[JoinLoginSubmission]([LoginId] ASC, [SubmissionId] ASC)
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_JoinLoginSubmission_u]
ON [auth].[JoinLoginSubmission]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinLoginSubmission]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinLoginSubmission].[JoinId]		= INSERTED.[JoinId];

END;
GO