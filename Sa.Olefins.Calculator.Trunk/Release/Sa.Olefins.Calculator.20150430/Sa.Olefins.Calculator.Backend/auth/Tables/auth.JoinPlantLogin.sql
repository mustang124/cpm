﻿CREATE TABLE [auth].[JoinPlantLogin]
(
	[JoinId]				INT					NOT	NULL	IDENTITY (1, 1),

	[PlantId]				INT					NOT	NULL	CONSTRAINT [FK_JoinPlantLogin_Plants]				REFERENCES [auth].[Plants] ([PlantId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[LoginId]				INT					NOT	NULL	CONSTRAINT [FK_JoinPlantLogin_Logins]				REFERENCES [auth].[Logins] ([LoginId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_JoinPlantLogin_Active]				DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_JoinPlantLogin_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinPlantLogin_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinPlantLogin_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinPlantLogin_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_JoinPlantLogin]				PRIMARY KEY NONCLUSTERED([JoinId] ASC),
	CONSTRAINT [UK_JoinPlantLogin_PlantLogin]	UNIQUE CLUSTERED ([PlantId] ASC, [LoginId] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinPlantLogin_Active]
ON [auth].[JoinPlantLogin]([PlantId] ASC, [LoginId] ASC)
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_JoinPlantLogin_u]
ON [auth].[JoinPlantLogin]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinPlantLogin]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinPlantLogin].[JoinId]		= INSERTED.[JoinId];

END;
GO