﻿CREATE TABLE [auth].[LoginsPassWords]
(
	[LoginId]				INT						NOT	NULL	CONSTRAINT [FK_LoginsPassWords_LoginAttributes]		REFERENCES [auth].[Logins] ([LoginId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[pSalt]					BINARY(512)				NOT	NULL,
	[pWord]					VARBINARY(512)			NOT	NULL	CONSTRAINT [CL_LoginsPassWords_pWord]				CHECK(datalength([pWord]) >= 64),

	[ValidFrom]				DATETIMEOFFSET(7)		NOT	NULL	CONSTRAINT [DF_LoginsPassWords_ValidFrom]			DEFAULT (sysdatetimeoffset()),
	[ValidTo]				DATETIMEOFFSET(7)			NULL,

	[tsModified]			DATETIMEOFFSET(7)		NOT	NULL	CONSTRAINT [DF_LoginsPassWords_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_LoginsPassWords_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_LoginsPassWords_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_LoginsPassWords_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION				NOT	NULL,

	CONSTRAINT [CV_UsersPassWords_ValidDates]		CHECK([ValidTo] >= [ValidFrom]),

	CONSTRAINT [PK_UsersPasswords]					PRIMARY KEY CLUSTERED ([LoginId] ASC, [ValidFrom] DESC)
);
GO

CREATE NONCLUSTERED INDEX [IX_LoginsPassWords_Current]
ON [auth].[LoginsPassWords](LoginId ASC, pWord ASC)
WHERE [ValidTo] IS NULL;
GO

CREATE TRIGGER [auth].[t_LoginsPassWords_u]
ON [auth].[LoginsPassWords]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[LoginsPassWords]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[LoginsPassWords].[LoginId]		= INSERTED.[LoginId]
		AND	[auth].[LoginsPassWords].[ValidFrom]	= INSERTED.[ValidFrom];

END;
GO