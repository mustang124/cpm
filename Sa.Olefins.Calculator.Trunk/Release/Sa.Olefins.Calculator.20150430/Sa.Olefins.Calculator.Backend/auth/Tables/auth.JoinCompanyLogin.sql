﻿CREATE TABLE [auth].[JoinCompanyLogin]
(
	[JoinId]				INT					NOT	NULL	IDENTITY (1, 1),

	[CompanyId]				INT					NOT	NULL	CONSTRAINT [FK_JoinCompanyLogin_Companies]			REFERENCES [auth].[Companies] ([CompanyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[LoginId]				INT					NOT	NULL	CONSTRAINT [FK_JoinCompanyLogin_Logins]				REFERENCES [auth].[Logins] ([LoginId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_JoinCompanyLogin_Active]				DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_JoinCompanyLogin_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompanyLogin_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompanyLogin_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompanyLogin_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_JoinCompanyLogin]				PRIMARY KEY NONCLUSTERED([JoinId] ASC),
	CONSTRAINT [UK_JoinCompanyLogin_CompanyLogin]	UNIQUE CLUSTERED ([CompanyId] ASC, [LoginId] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinCompanyLogin_Active]
ON [auth].[JoinCompanyLogin]([CompanyId] ASC, [LoginId] ASC)
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_JoinCompanyLogin_u]
ON [auth].[JoinCompanyLogin]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinCompanyLogin]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinCompanyLogin].[JoinId]	= INSERTED.[JoinId];

END;
GO