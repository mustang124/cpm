﻿CREATE PROCEDURE [auth].[Insert_Company]
(
	@CompanyName		NVARCHAR(128),
	@CompanyTag			VARCHAR(42)		= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [auth].[Companies]([CompanyName], [CompanyRefId])
	OUTPUT [INSERTED].[CompanyId] INTO @Id([Id])
	VALUES(
		@CompanyName,
		[dim].[Return_CompanyId](@CompanyTag)
		);

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@CompanyName:'	+ CONVERT(VARCHAR, @CompanyName))
					+ (', @CompanyTag:'		+ CONVERT(VARCHAR, @CompanyTag));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO