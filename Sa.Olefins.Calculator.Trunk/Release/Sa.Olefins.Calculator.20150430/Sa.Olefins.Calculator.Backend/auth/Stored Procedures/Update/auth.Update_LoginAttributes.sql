﻿CREATE PROCEDURE [auth].[Update_LoginAttributes]
(
	@LoginId		INT,

	@NameLast		NVARCHAR(128)	= NULL,
	@NameFirst		NVARCHAR(128)	= NULL,
	@eMail			NVARCHAR(254)	= NULL,
	@RoleId			INT				= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	UPDATE [auth].[LoginsAttributes]
	SET	[NameLast]	= COALESCE(@NameLast,	[NameLast]),
		[NameFirst]	= COALESCE(@NameFirst,	[NameFirst]),
		[eMail]		= COALESCE(@eMail,		[eMail]),
		[RoleId]	= COALESCE(@RoleId,		[RoleId])
	WHERE	[LoginId] = @LoginId
		AND(@NameLast	IS NOT NULL
		OR	@NameFirst	IS NOT NULL
		OR	@eMail		IS NOT NULL
		OR	@RoleId		IS NOT NULL);

	RETURN @LoginId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@LoginId:'		+ CONVERT(VARCHAR, @LoginId))
			+ COALESCE(', @NameLast:'		+ CONVERT(VARCHAR, @NameLast),	'')
			+ COALESCE(', @NameFirst:'		+ CONVERT(VARCHAR, @NameFirst),	'')
			+ COALESCE(', @eMail:'			+ CONVERT(VARCHAR, @eMail),		'')
			+ COALESCE(', @RoleId:'			+ CONVERT(VARCHAR, @RoleId),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO