﻿CREATE PROCEDURE [auth].[Update_Plant]
(
	@PlantId		INT,

	@PlantName		NVARCHAR(128)	= NULL,
	@Active			BIT				= NULL
)
AS
BEGIN
	
SET NOCOUNT ON;

BEGIN TRY

	UPDATE [auth].[Plants]
	SET	[PlantName]		= COALESCE(@PlantName, [PlantName]),
		[Active]		= COALESCE(@Active, [Active])
	WHERE	[PlantId] = @PlantId
		AND(@Active		IS NOT NULL
		OR	@PlantName	IS NOT NULL);

	RETURN @PlantId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@PlantId:'		+ CONVERT(VARCHAR, @PlantId))
			+ COALESCE(', @PlantName:'		+ CONVERT(VARCHAR, @PlantName),	'')
			+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO