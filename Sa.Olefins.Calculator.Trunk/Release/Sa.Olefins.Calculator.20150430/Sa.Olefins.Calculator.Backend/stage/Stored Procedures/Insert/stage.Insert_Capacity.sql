﻿CREATE PROCEDURE [stage].[Insert_Capacity]
(
	@SubmissionId			INT,
	@StreamId				INT,

	@Capacity_kMT			FLOAT	= NULL,
	@StreamDay_MTSD			FLOAT,
	@Record_MTSD			FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[Capacity]([SubmissionId], [StreamId], [Capacity_kMT], [StreamDay_MTSD], [Record_MTSD])
	SELECT
		@SubmissionId,
		@StreamId,
		@Capacity_kMT,
		@StreamDay_MTSD,
		@Record_MTSD
	WHERE	@StreamDay_MTSD	>= 0.0
		OR	@Capacity_kMT	>= 0.0
		OR	@Record_MTSD	>= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @StreamId:'		+ CONVERT(VARCHAR, @StreamId))
			+ COALESCE(', @Capacity_kMT:'	+ CONVERT(VARCHAR, @Capacity_kMT),		'')
			+ COALESCE(', @StreamDay_MTSD:'	+ CONVERT(VARCHAR, @StreamDay_MTSD),	'')
			+ COALESCE(', @Record_MTSD:'	+ CONVERT(VARCHAR, @Record_MTSD),		'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;