﻿CREATE PROCEDURE [stage].[Update_StreamQuantity]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamTypeId			INT		= NULL,
	@Quantity_kMT			FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	UPDATE [stage].[StreamQuantity]
	SET	[StreamId]			= COALESCE(@StreamTypeId,	[StreamId]),
		[Quantity_kMT]		= COALESCE(@Quantity_kMT,	[Quantity_kMT])
	WHERE	[SubmissionId]	= @SubmissionId
		AND	[StreamNumber]	= @StreamNumber
		AND(@StreamTypeId	>= 1
		OR	@Quantity_kMT	>= 0.0)

		AND([StreamId]		<> COALESCE(@StreamTypeId,	[StreamId])
		OR	[Quantity_kMT]	<> COALESCE(@Quantity_kMT,	[Quantity_kMT]));

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @StreamNumber:'	+ CONVERT(VARCHAR, @StreamNumber))
			+ COALESCE(', @StreamTypeId:'	+ CONVERT(VARCHAR, @StreamTypeId),	'')
			+ COALESCE(', @Quantity_kMT:'	+ CONVERT(VARCHAR, @Quantity_kMT),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;