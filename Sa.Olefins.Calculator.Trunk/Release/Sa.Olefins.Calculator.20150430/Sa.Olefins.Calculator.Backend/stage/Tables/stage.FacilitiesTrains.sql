﻿CREATE TABLE [stage].[FacilitiesTrains]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_Trains_Submissions]				REFERENCES [stage].[Submissions]([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Train_Count]			INT					NOT	NULL	CONSTRAINT [CR_Trains_Train_Count_MinIncl_0]	CHECK([Train_Count] >= 0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Trains_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Trains_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Trains_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Trains_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Trains]						PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [stage].[t_Trains_u]
ON [stage].[FacilitiesTrains]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[FacilitiesTrains]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[FacilitiesTrains].[SubmissionId]	= INSERTED.[SubmissionId];

END;
GO