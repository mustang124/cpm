﻿CREATE TABLE [stage].[FacilitiesFractionator]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesFractionator_Submissions]					REFERENCES [stage].[Submissions]([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesFractionator_Facility_LookUp]				REFERENCES [dim].[Facility_LookUp]([FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Quantity_kBSD]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesFractionator_Quantity_kBSD_MinIncl_0.0]	CHECK([Quantity_kBSD] >= 0.0),
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_FacilitiesFractionator_Stream_LookUp]				REFERENCES [dim].[Stream_LookUp]([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Throughput_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesFractionator_Throughput_kMT_MinIncl_0.0]	CHECK([Throughput_kMT] >= 0.0),
	[Density_SG]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesFractionator_Density_SG_MinIncl_0.0]		CHECK([Density_SG] >= 0.0),
	
	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesFractionator]		PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [stage].[t_FacilitiesFractionator_u]
ON [stage].[FacilitiesFractionator]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[FacilitiesFractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[FacilitiesFractionator].[SubmissionId]		= INSERTED.[SubmissionId];

END;
GO