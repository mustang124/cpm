﻿CREATE FUNCTION [dim].[Return_NlsLcid]
(
	@CultureName		VARCHAR(12)
)
RETURNS SMALLINT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@Id	SMALLINT;

	SELECT @Id = [l].[Lcid]
	FROM [dim].[NationalLanguageSupport_LookUp]	[l]
	WHERE [l].[CultureName] = @CultureName;

	RETURN	@Id;

END;