﻿CREATE TABLE [dim].[HydroTreaterType_LookUp]
(
	[HydroTreaterTypeId]		INT					NOT NULL	IDENTITY (1, 1),

	[HydroTreaterTypeTag]		VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_HydroTreaterType_LookUp_HydroTreaterTypeTag]			CHECK ([HydroTreaterTypeTag]	<> ''),
																CONSTRAINT [UK_HydroTreaterType_LookUp_HydroTreaterTypeTag]			UNIQUE NONCLUSTERED ([HydroTreaterTypeTag]),
	[HydroTreaterTypeName]		VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_HydroTreaterType_LookUp_HydroTreaterTypeName]		CHECK ([HydroTreaterTypeName]	<> ''),
																CONSTRAINT [UK_HydroTreaterType_LookUp_HydroTreaterTypeName]		UNIQUE NONCLUSTERED ([HydroTreaterTypeName]),
	[HydroTreaterTypeDetail]	VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_HydroTreaterType_LookUp_HydroTreaterTypeDetail]		CHECK ([HydroTreaterTypeDetail]	<> ''),
																CONSTRAINT [UK_HydroTreaterType_LookUp_HydroTreaterTypeDetail]		UNIQUE NONCLUSTERED ([HydroTreaterTypeDetail]),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_HydroTreaterType_LookUp_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_LookUp_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_LookUp_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_LookUp_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_HydroTreaterType_LookUp]			PRIMARY KEY CLUSTERED ([HydroTreaterTypeId] ASC)
);
GO

CREATE TRIGGER [dim].[t_HydroTreaterType_LookUp_u]
ON [dim].[HydroTreaterType_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[HydroTreaterType_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[HydroTreaterType_LookUp].[HydroTreaterTypeId]		= INSERTED.[HydroTreaterTypeId];

END;
GO