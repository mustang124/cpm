﻿CREATE TABLE [dim].[Factor_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Factor_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_Factor_Parent_LookUp_Facilities]		REFERENCES [dim].[Factor_LookUp] ([FactorId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_Factor_Parent_LookUp_Parent]			REFERENCES [dim].[Factor_LookUp] ([FactorId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Factor_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																												REFERENCES [dim].[Factor_Parent] ([MethodologyId], [FactorId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_Factor_Parent_Operator]				DEFAULT ('+')
															CONSTRAINT [FK_Factor_Parent_Operator]				REFERENCES [dim].[Operator] ([Operator])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Factor_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_Parent_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Factor_Parent]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FactorId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Factor_Parent_u]
ON [dim].[Factor_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Factor_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Factor_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Factor_Parent].[FactorId]			= INSERTED.[FactorId];

END;
GO