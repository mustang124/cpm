﻿CREATE TABLE [dim].[Company_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Company_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[CompanyId]				INT					NOT	NULL	CONSTRAINT [FK_Company_Parent_LookUp_Companys]		REFERENCES [dim].[Company_LookUp] ([CompanyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_Company_Parent_LookUp_Parent]		REFERENCES [dim].[Company_LookUp] ([CompanyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Company_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																												REFERENCES [dim].[Company_Parent] ([MethodologyId], [CompanyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_Company_Parent_Operator]				DEFAULT ('+')
															CONSTRAINT [FK_Company_Parent_Operator]				REFERENCES [dim].[Operator] ([Operator])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Company_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_Parent_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Company_Parent]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [CompanyId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Company_Parent_u]
ON [dim].[Company_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Company_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Company_Parent].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Company_Parent].[CompanyId]		= INSERTED.[CompanyId];

END;
GO