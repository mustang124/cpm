﻿CREATE TABLE [dim].[NationalLanguageSupport_LookUp]
(
	[Lcid]					SMALLINT			NOT	NULL,
	
	[CultureName]			VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_NationalLanguageSupport_LookUp_CultureName]		CHECK([CultureName] <> ''),
	
	[Locale]				NVARCHAR(64)		NOT	NULL	CONSTRAINT [CL_NationalLanguageSupport_LookUp_Locale]			CHECK([Locale] <> ''),
	[LocaleLanguage]		NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL_NationalLanguageSupport_LookUp_LocaleLanguage]	CHECK([LocaleLanguage] <> ''),
	[LocaleLocal]			NVARCHAR(64)		NOT	NULL,
	
	[CodePageANSI]			INT					NOT	NULL,
	[CodePageOEM]			INT					NOT	NULL,
	[CountryIso]			CHAR(3)				NOT	NULL	CONSTRAINT [CL_NationalLanguageSupport_LookUp_CountryIso]		CHECK([CountryIso] <> ''),
	[LanguageIso]			CHAR(3)				NOT	NULL	CONSTRAINT [CL_NationalLanguageSupport_LookUp_LanguageIso]		CHECK([LanguageIso] <> ''),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_LookUp_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_LookUp_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_LookUp_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NationalLanguageSupport_LookUp_tsModifiedApp]	DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_NationalLanguageSupport_LookUp]	PRIMARY KEY CLUSTERED([Lcid] ASC),
	CONSTRAINT [UK_NationalLanguageSupport_LookUp]	UNIQUE NONCLUSTERED([CultureName] ASC)
);
GO

CREATE TRIGGER [dim].[t_NationalLanguageSupport_LookUp_u]
ON [dim].[NationalLanguageSupport_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[NationalLanguageSupport_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[NationalLanguageSupport_LookUp].[Lcid]	= INSERTED.[Lcid];

END;
GO