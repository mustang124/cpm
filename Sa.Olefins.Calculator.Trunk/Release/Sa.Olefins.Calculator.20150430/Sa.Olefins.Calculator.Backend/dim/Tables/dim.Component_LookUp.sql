﻿CREATE TABLE [dim].[Component_LookUp]
(
	[ComponentId]			INT					NOT NULL	IDENTITY (1, 1),

	[ComponentTag]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Component_LookUp_ComponentsTag]			CHECK ([ComponentTag]		<> ''),
															CONSTRAINT [UK_Component_LookUp_ComponentsTag]			UNIQUE NONCLUSTERED ([ComponentTag]),
	[ComponentName]			VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Component_LookUp_ComponentsName]			CHECK ([ComponentName]		<> ''),
															CONSTRAINT [UK_Component_LookUp_ComponentsName]			UNIQUE NONCLUSTERED ([ComponentName]),
	[ComponentDetail]		VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Component_LookUp_ComponentsDetail]		CHECK ([ComponentDetail]	<> ''),
															CONSTRAINT [UK_Component_LookUp_ComponentsDetail]		UNIQUE NONCLUSTERED ([ComponentDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Component_LookUp_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Component_LookUp]			PRIMARY KEY CLUSTERED ([ComponentId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Component_LookUp_u]
ON [dim].[Component_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Component_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Component_LookUp].[ComponentId]		= INSERTED.[ComponentId];

END;
GO