﻿CREATE PROCEDURE [web].[Get_FeedClassForPlant]
(
	@PlantId			INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FeedClassTag	INT = [calc].[Return_FeedClassId_PlantId](@PlantId);

	SELECT @FeedClassTag	[FeedClassId];

END;