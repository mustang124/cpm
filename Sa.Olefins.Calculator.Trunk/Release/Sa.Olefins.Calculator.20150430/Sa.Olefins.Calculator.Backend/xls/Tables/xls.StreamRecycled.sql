﻿CREATE TABLE [xls].[StreamRecycled]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_StreamRecycled_Refnum]							CHECK([Refnum] <> ''),
	[StreamNumber]			INT					NOT	NULL,

	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Quantity_Dur_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecycled_Quantity_Dur_kMT_MinIncl_0.0]		CHECK([Quantity_Dur_kMT] >= 0.0),
	[Quantity_Ann_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecycled_Quantity_Ann_kMT_MinIncl_0.0]		CHECK([Quantity_Ann_kMT] >= 0.0),
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Component_LookUp]					REFERENCES [dim].[Component_LookUp] ([ComponentId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Recycled_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MinIncl_0.0]		CHECK([Recycled_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MaxIncl_100.0]	CHECK([Recycled_WtPcnt] <= 100.0),

	[_Recycled_Dur_kMT]		AS CONVERT(FLOAT, [Quantity_Dur_kMT] * [Recycled_WtPcnt] / 100.0)
							PERSISTED			NOT	NULL,
	[_Recycled_Ann_kMT]		AS CONVERT(FLOAT, [Quantity_Ann_kMT] * [Recycled_WtPcnt] / 100.0)
							PERSISTED			NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamRecycled]				PRIMARY KEY CLUSTERED ([Refnum] DESC, [StreamNumber] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [xls].[t_StreamRecycled_u]
ON [xls].[StreamRecycled]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StreamRecycled]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StreamRecycled].[Refnum]			= INSERTED.[Refnum]
		AND	[xls].[StreamRecycled].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[xls].[StreamRecycled].[ComponentId]	= INSERTED.[ComponentId];

END;
GO