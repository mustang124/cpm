﻿CREATE TABLE [xls].[StreamComposition]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_StreamComposition_Refnum]							CHECK([Refnum] <> ''),
	[StreamNumber]			INT					NOT	NULL,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Stream_LookUp]						REFERENCES [dim].[Stream_LookUp] ([StreamId])											ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Component_LookUp]					REFERENCES [dim].[Component_LookUp] ([ComponentId])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[Component_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MinIncl_0.0]		CHECK([Component_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MaxIncl_100.0]	CHECK([Component_WtPcnt] <= 100.0),
	[Component_Dur_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamComposition_Component_Dur_kMT_MinIncl_0.0]		CHECK([Component_Dur_kMT] >= 0.0),
	[Component_Ann_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamComposition_Component_Ann_kMT_MinIncl_0.0]		CHECK([Component_Ann_kMT] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamComposition]			PRIMARY KEY CLUSTERED ([Refnum] DESC, [StreamNumber] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [xls].[t_StreamComposition_u]
ON [xls].[StreamComposition]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StreamComposition]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StreamComposition].[Refnum]			= INSERTED.[Refnum]
		AND	[xls].[StreamComposition].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[xls].[StreamComposition].[ComponentId]		= INSERTED.[ComponentId];

END;
GO