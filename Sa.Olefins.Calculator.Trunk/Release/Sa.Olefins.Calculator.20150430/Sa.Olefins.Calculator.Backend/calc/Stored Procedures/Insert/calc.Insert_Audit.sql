﻿CREATE PROCEDURE [calc].[Insert_Audit]
(
	@MethodologyId		INT,
	@SubmissionId		INT,
	@TimeBeg			DATETIMEOFFSET
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [calc].[Audit]([MethodologyId], [SubmissionId], [TimeBeg])
	SELECT
		@MethodologyId,
		@SubmissionId,
		@TimeBeg;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @TimeBeg:'		+ CONVERT(VARCHAR, @TimeBeg));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO