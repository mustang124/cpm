﻿CREATE PROCEDURE [calc].[Insert_Standard_kEdc_Red_CrackedGas]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@ProcessUnitId		INT	= dim.Return_ProcessUnitId('RedCrackedGasTrans');
	DECLARE @FactorId_Feed		INT	= dim.Return_FactorId('FreshPyroFeed');
	DECLARE @FactorId_CompGas	INT	= dim.Return_FactorId('CompGas');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		cap.[MethodologyId],
		cap.[SubmissionId],
		@StandardId,
		@ProcessUnitId,
		cap.[_CapacityInfer_Ann_kMT] *
		SUM(CASE b.[FactorId]
			WHEN @FactorId_Feed		THEN - f.[Coefficient]
			WHEN @FactorId_CompGas	THEN + f.[Coefficient]
			END) / 1000.0
	FROM [calc].[Capacity_CrackedGasTransfers]			cap
	INNER JOIN	[calc].[FeedClass]						fdc
		ON	fdc.[MethodologyId]	= cap.[MethodologyId]
		AND	fdc.[SubmissionId]	= cap.[SubmissionId]
	INNER JOIN	[ante].[MapFactorFeedClass]				map
		ON	map.[MethodologyId]	= fdc.[MethodologyId]
		AND	map.[FeedClassId]	= fdc.[FeedClassId]
	INNER JOIN	[ante].[Factors]						f
		ON	f.[MethodologyId]	= map.[MethodologyId]
		AND	f.[FactorId]		= map.[FactorId]
		AND	f.[StandardId]		= @StandardId
	INNER JOIN	[dim].[Factor_Bridge]					b
		ON	b.[MethodologyId]	= cap.[MethodologyId]
		AND	b.[DescendantId]	= f.[FactorId]
		AND	b.[FactorId]		IN (@FactorId_Feed, @FactorId_CompGas)
	WHERE	cap.[MethodologyId]	= @MethodologyId
		AND	cap.[SubmissionId]	= @SubmissionId
	GROUP BY
		cap.[MethodologyId],
		cap.[SubmissionId],
		cap.[_CapacityInfer_Ann_kMT];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO