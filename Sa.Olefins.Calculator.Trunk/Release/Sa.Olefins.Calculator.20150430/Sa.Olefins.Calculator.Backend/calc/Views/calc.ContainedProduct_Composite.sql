﻿CREATE VIEW [calc].[ContainedProduct_Composite]
WITH SCHEMABINDING
AS
SELECT
	c.[MethodologyId],
	c.[SubmissionId],
	c.[ComponentId],
	SUM(c.[Component_Dur_kMT])		[ContainedProduct_Dur_kMT],
	SUM(c.[Component_Ann_kMT])		[ContainedProduct_Ann_kMT],
	COUNT_BIG(*)					[Items]
FROM [calc].[StreamComposition]			c
WHERE c.[StreamNumber] >= 5000 AND c.[StreamNumber] <= 5999
GROUP BY
	c.[MethodologyId],
	c.[SubmissionId],
	c.[ComponentId]
GO

CREATE UNIQUE CLUSTERED INDEX [UX_ContainedProduct_Composite]
ON [calc].[ContainedProduct_Composite]([MethodologyId] DESC, [SubmissionId] ASC, [ComponentId] ASC);
GO

CREATE INDEX [IX_ContainedProduct_Composite]
ON [calc].[ContainedProduct_Composite]([MethodologyId] DESC, [SubmissionId] ASC, [ComponentId] ASC)
INCLUDE([ContainedProduct_Dur_kMT], [ContainedProduct_Ann_kMT])
GO