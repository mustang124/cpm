﻿CREATE TABLE [calc].[StreamRecycled]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Submissions]						REFERENCES [fact].[Submissions] ([SubmissionId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_StreamQuantity]
															FOREIGN KEY([SubmissionId], [StreamNumber])						REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Quantity_Dur_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecycled_Quantity_Dur_kMT_MinIncl_0.0]		CHECK([Quantity_Dur_kMT] >= 0.0),
	[Quantity_Ann_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecycled_Quantity_Ann_kMT_MinIncl_0.0]		CHECK([Quantity_Ann_kMT] >= 0.0),
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Component_LookUp]					REFERENCES [dim].[Component_LookUp] ([ComponentId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Recycled_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MinIncl_0.0]		CHECK([Recycled_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MaxIncl_100.0]	CHECK([Recycled_WtPcnt] <= 100.0),

	[_Recycled_Dur_kMT]		AS CONVERT(FLOAT, [Quantity_Dur_kMT] * [Recycled_WtPcnt] / 100.0)
							PERSISTED			NOT	NULL,
	[_Recycled_Ann_kMT]		AS CONVERT(FLOAT, [Quantity_Ann_kMT] * [Recycled_WtPcnt] / 100.0)
							PERSISTED			NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamRecycled]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [StreamNumber] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [calc].[t_StreamRecycled_u]
ON [calc].[StreamRecycled]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StreamRecycled]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StreamRecycled].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[calc].[StreamRecycled].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[calc].[StreamRecycled].[StreamNumber]		= INSERTED.[StreamNumber]
		AND	[calc].[StreamRecycled].[ComponentId]		= INSERTED.[ComponentId];

END;
GO