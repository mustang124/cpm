﻿CREATE TABLE [calc].[HvcProductionDivisor]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_HvcProductionDivisor_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_HvcProductionDivisor_Submissions]					REFERENCES [fact].[Submissions] ([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[HvcProdDivisor_kMT]	FLOAT				NOT	NULL	CONSTRAINT [CR_HvcProductionDivisor_HvcProdDivisor_kMT_MinIncl_0.0]	CHECK([HvcProdDivisor_kMT] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_HvcProductionDivisor_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HvcProductionDivisor_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HvcProductionDivisor_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HvcProductionDivisor_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_HvcProductionDivisor]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC)
);
GO

CREATE TRIGGER [calc].[t_HvcProductionDivisor_u]
ON [calc].[HvcProductionDivisor]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[HvcProductionDivisor]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[HvcProductionDivisor].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[HvcProductionDivisor].[SubmissionId]	= INSERTED.[SubmissionId];

END;
GO