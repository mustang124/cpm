﻿CREATE TABLE [calc].[FacilitiesElecGeneration]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesElecGeneration_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesElecGeneration_Submissions]				REFERENCES [fact].[Submissions]([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesElecGeneration_Facility_LookUp]			REFERENCES [dim].[Facility_LookUp]([FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Capacity_MW]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesElecGeneration_Capacity_MW_MinIncl_0.0]	CHECK([Capacity_MW] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesElecGeneration]	PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [calc].[t_FacilitiesElecGeneration_u]
ON [calc].[FacilitiesElecGeneration]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[FacilitiesElecGeneration]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[FacilitiesElecGeneration].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[FacilitiesElecGeneration].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[FacilitiesElecGeneration].[FacilityId]		= INSERTED.[FacilityId];

END;
GO