﻿CREATE FUNCTION [calc].[Return_HydroPurFacilityId]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @FacilityId		INT;

	IF(@FacilityId IS NULL)
	SELECT
		@FacilityId = f.FacilityId
	FROM fact.Facilities			f
	WHERE	f.FacilityId = [dim].[Return_FacilityId]('HydroPurCryogenic')
		AND	f.Unit_Count >= 1
		AND	f.SubmissionId = @SubmissionId;

	IF(@FacilityId IS NULL)
	SELECT
		@FacilityId = f.FacilityId
	FROM fact.Facilities			f
	WHERE	f.FacilityId = [dim].[Return_FacilityId]('HydroPurMembrane')
		AND	f.Unit_Count >= 1
		AND	f.SubmissionId = @SubmissionId;

	IF(@FacilityId IS NULL)
	SELECT
		@FacilityId = f.FacilityId
	FROM fact.Facilities			f
	WHERE	f.FacilityId = [dim].[Return_FacilityId]('HydroPurPSA')
		AND	f.Unit_Count >= 1
		AND	f.SubmissionId = @SubmissionId;

	RETURN @FacilityId;

END