﻿CREATE TABLE [ante].[MapFactorHydroPur]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapFactorHydroPur_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_MapFactorHydroPur_Facility_LookUp]		REFERENCES [dim].[Facility_LookUp] ([FacilityId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_MapFactorHydroPur_Factor_LookUp]			REFERENCES [dim].[Factor_LookUp] ([FactorId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapFactorHydroPur_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorHydroPur_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorHydroPur_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorHydroPur_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapFactorHydroPur]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapFactorHydroPur_u]
ON [ante].[MapFactorHydroPur]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorHydroPur]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorHydroPur].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorHydroPur].[FacilityId]		= INSERTED.[FacilityId];

END;
GO
