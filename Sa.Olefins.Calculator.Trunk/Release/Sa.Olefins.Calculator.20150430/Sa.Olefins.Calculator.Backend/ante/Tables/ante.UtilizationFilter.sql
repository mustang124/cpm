﻿CREATE TABLE [ante].[UtilizationFilter]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_UtilizationFilter_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_UtilizationFilter_Stream_LookUp]				REFERENCES [dim].[Stream_LookUp] ([StreamId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_UtilizationFilter_Component_LookUp]			REFERENCES [dim].[Component_LookUp] ([ComponentId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_UtilizationFilter_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UtilizationFilter_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UtilizationFilter_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UtilizationFilter_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_UtilizationFilter]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [ante].[t_UtilizationFilter_u]
ON [ante].[UtilizationFilter]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[UtilizationFilter]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[UtilizationFilter].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[UtilizationFilter].[StreamId]		= INSERTED.[StreamId];

END;
GO
