﻿CREATE TABLE [ante].[StdEnergyFractionator]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_StdEnergyFractionator_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_StdEnergyFractionator_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp]([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Energy_kBtuBbl]		FLOAT				NOT	NULL	CONSTRAINT [CR_StdEnergyFractionator_Energy_kBtuBbl_MinIncl_0.0]	CHECK([Energy_kBtuBbl] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StdEnergyFractionator_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StdEnergyFractionator_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StdEnergyFractionator_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StdEnergyFractionator_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StdEnergyFractionator]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_StdEnergyFractionator_u]
ON [ante].[StdEnergyFractionator]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[StdEnergyFractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[StdEnergyFractionator].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[StdEnergyFractionator].[StreamId]		= INSERTED.[StreamId];

END;
GO