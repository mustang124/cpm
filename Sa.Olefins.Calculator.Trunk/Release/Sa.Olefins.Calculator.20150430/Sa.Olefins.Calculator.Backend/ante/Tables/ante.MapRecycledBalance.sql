﻿CREATE TABLE [ante].[MapRecycledBalance]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapRecycledBalance_Methodology]		REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[RecStreamId]			INT					NOT	NULL	CONSTRAINT [FK_MapRecycledBalance_RecStream_LookUp]		REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[RecComponentId]		INT					NOT	NULL	CONSTRAINT [FK_MapRecycledBalance_RecComponent_LookUp]	REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[BalStreamId]			INT						NULL	CONSTRAINT [FK_MapRecycledBalance_BalStream_LookUp]		REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[BalComponentId]		INT					NOT	NULL	CONSTRAINT [FK_MapRecycledBalance_BalComponent_LookUp]	REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
															
															CONSTRAINT [CV_MapRecycledBalance_Mapping]				CHECK(NOT([RecStreamId] = [BalStreamId] AND [RecComponentId] = [BalComponentId])),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapRecycledBalance_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapRecycledBalance_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapRecycledBalance_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapRecycledBalance_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapRecycledBalance]	PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [RecStreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapRecycledBalance_u]
ON [ante].[MapRecycledBalance]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapRecycledBalance]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapRecycledBalance].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapRecycledBalance].[RecStreamId]	= INSERTED.[RecStreamId];

END;
GO