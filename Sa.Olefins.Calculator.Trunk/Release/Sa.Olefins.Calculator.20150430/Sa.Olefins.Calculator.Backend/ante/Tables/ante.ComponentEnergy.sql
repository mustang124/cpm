﻿CREATE TABLE [ante].[ComponentEnergy]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_ComponentEnergy_Methodology]							REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_ComponentEnergy_Component_LookUp]					REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[StandardEnergy_BtuLb]	FLOAT				NOT	NULL	CONSTRAINT [CR_ComponentEnergy_StandardEnergy_BtuLb_MinIncl_0.0]	CHECK([StandardEnergy_BtuLb] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ComponentEnergy_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ComponentEnergy_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ComponentEnergy_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ComponentEnergy_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ComponentEnergy]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [ante].[t_ComponentEnergy_u]
ON [ante].[ComponentEnergy]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ComponentEnergy]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[ComponentEnergy].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[ComponentEnergy].[ComponentId]			= INSERTED.[ComponentId];

END;
GO