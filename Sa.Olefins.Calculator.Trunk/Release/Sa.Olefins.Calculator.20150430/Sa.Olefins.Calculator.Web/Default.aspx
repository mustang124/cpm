﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OlefinsCalculator.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
        function clink() {
            if (document.getElementById('alogin').innerHTML == "Login") {
                document.getElementById('alogin').innerHTML = 'Logout';
            }
            else {
                document.getElementById('alogin').innerHTML = 'Login';
                $('#landingCopy').hide();
                $('#calcs').hide();
                $('#plantHeader').hide();
                $('#divPlantInfo').hide();
            }
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div style="background: url('Images/Background-rht.png') right no-repeat;">
<div id="landingCopy" style="margin: 10px;">
<h3>Welcome to the Olefin Study Factors and Standards Online Calculator</h3>
<p></p><b>Purpose</b>
<br />This site allows participants in the Solomon Worldwide Olefins Plant Performance Analysis to calculate EDC, EII Standard Energy, and other Efficiency Standards (Personnel, Maintenance, and Non-Energy Operating Expense) for each plant in their company as a result of changes in plant feedstock, supplemental feeds and production.  The calculated standards can be used to calculate plant Efficiency Indices (as a % of Standard) and to track performance trends.
<p></p><b>Rules for Use</b>
<br />Companies are limited to a set number of calculations per plant per quarter.
<br />The tool is not to be used to reverse engineer or back-calculate the factors used derive EDC, EII, and the Efficiency Standards.
<p></p><b>Limitations</b>
<br />The data entered by the client in this tool is not rigorously validated by Solomon Associates as in the Worldwide Olefin Plant Performance Analysis.  The client should make sure the data are accurate and representative of the plant’s performance before using the results.
<p></p><b>Confidentiality</b>
<br />The information on this website is confidential and intended for the sole use of the client to whom it is addressed. The information and methodologies outlined herein are proprietary and their expression in this document is copyrighted, with all rights reserved to HSB Solomon Associates LLC (Solomon). Copying or distributing this material without permission is strictly prohibited.
<p />M³ – Measure. Manage. Maximize.®, Comparative Performance Analysis™, CPA™, NCM³®, Q1 Day 1™, EII®, CEI™, CWB™, and Solomon Profile® II are registered and proprietary trademarks of Solomon. The absence of any indication as such does not constitute a waiver of any and all intellectual property rights that Solomon has established.
<p><br /></p>
</div>
</div>

</asp:Content>
