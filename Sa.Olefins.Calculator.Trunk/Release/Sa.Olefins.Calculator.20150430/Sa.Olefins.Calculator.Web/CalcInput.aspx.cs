﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace OlefinsCalculator
{
    public partial class CalcInput : System.Web.UI.Page
    {
        int CompanyID;
        OlefinsCalculator.User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            ((System.Web.UI.HtmlControls.HtmlAnchor)this.Master.FindControl("alogin")).InnerText = "Logout";
            user = (OlefinsCalculator.User)Session["User"];
            CompanyID = user._companyID;

            if (!Page.IsPostBack)
            {
                btnRunCalc.Enabled = false;
                if (user._securityLevelID < 5)
                    //bindPlantsByCompany(CompanyID);
                    bindPlantsBySolomonUser(user._userID);
                else
                    bindPlantsByUser(user._userID);
                bindFeedClass();
                bindFeedstock();
                bindPGhydrotreaterType();
                if (Request.QueryString["CalcID"] != null)
                {
                    bindPlantsByCompany(Convert.ToInt32(Request.QueryString["CompanyID"].ToString()));
                    ddlPlant.SelectedIndex = -1;
                    foreach (ListItem li in ddlPlant.Items)
                        if (li.Value == Request.QueryString["PlantID"].ToString())
                            li.Selected = true;
                    ddlPlant_SelectedIndexChanged(new object(), new EventArgs());
                    ddlCalcs.SelectedIndex = -1;
                    foreach (ListItem li in ddlCalcs.Items)
                        if (li.Value == Request.QueryString["CalcID"].ToString())
                            li.Selected = true;
                    divddlCalc.Visible = true;

                    ddlCalcs_SelectedIndexChanged(new object(), new EventArgs());
                    calcs.Style.Clear();
                    calcs.Style.Add("display", "block");
                }
            }
        }

        #region Bind Plants By Company
        public void bindPlantsBySolomonUser(int UserID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_PlantsBySolomonUser_Active", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@UserID", UserID));
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsPlants = new DataSet();
                    sda.Fill(dsPlants);
                    ddlPlant.DataSource = dsPlants;
                    ddlPlant.DataTextField = "PlantName";
                    ddlPlant.DataValueField = "PlantID";
                    ddlPlant.DataBind();
                    ddlPlant.Items.Insert(0, (new ListItem("-- Select --", "")));
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindPlantsByCompany", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Bind Plants By Company
        //DEPRECATED
        public void bindPlantsByCompany(int CompanyID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_PlantsByCompany_Active", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsPlants = new DataSet();
                    sda.Fill(dsPlants);
                    ddlPlant.DataSource = dsPlants;
                    ddlPlant.DataTextField = "PlantName";
                    ddlPlant.DataValueField = "PlantID";
                    ddlPlant.DataBind();
                    ddlPlant.Items.Insert(0, (new ListItem("-- Select --", "")));
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindPlantsByCompany", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Bind Feed Class
        public void bindFeedClass()
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].get_FeedClass", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsFeedClass = new DataSet();
                    sda.Fill(dsFeedClass);
                    ddlFeedClass.DataSource = dsFeedClass;
                    ddlFeedClass.DataTextField = "FeedClassDesc";
                    ddlFeedClass.DataValueField = "FeedClassID";
                    ddlFeedClass.DataBind();
                    ddlFeedClass.Items.Insert(0, (new ListItem("--", "")));
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindFeedClass", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Bind Feedstock
        public void bindFeedstock()
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].get_FractionatorFeedstock", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsFeedstock = new DataSet();
                    sda.Fill(dsFeedstock);
                    ddlFFractionatorFeedstock.DataSource = dsFeedstock;
                    ddlFFractionatorFeedstock.DataTextField = "FeedstockDesc";
                    ddlFFractionatorFeedstock.DataValueField = "FeedstockID";
                    ddlFFractionatorFeedstock.DataBind();
                    ddlFFractionatorFeedstock.Items.Insert(0, (new ListItem("-- Select --", "132")));
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindFeedstock", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Bind Pyrolysis Gasoline Hydrotreater Type
        public void bindPGhydrotreaterType()
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].get_PGasHydrotreaterType", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsPGhydrotreaterType = new DataSet();
                    sda.Fill(dsPGhydrotreaterType);
                    ddlPGhydrotreaterType.DataSource = dsPGhydrotreaterType;
                    ddlPGhydrotreaterType.DataTextField = "PGasHydrotreaterTypeDesc";
                    ddlPGhydrotreaterType.DataValueField = "PGasHydrotreaterTypeID";
                    ddlPGhydrotreaterType.DataBind();
                    ddlPGhydrotreaterType.Items.Insert(0, (new ListItem("-- Select --", "5")));
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindPGhydrotreaterType", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Bind Plants By User
        public void bindPlantsByUser(int UserID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_PlantsByUser_Active", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@UserID", UserID));
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsPlants = new DataSet();
                    sda.Fill(dsPlants);
                    ddlPlant.DataSource = dsPlants;
                    ddlPlant.DataTextField = "PlantName";
                    ddlPlant.DataValueField = "PlantID";
                    ddlPlant.DataBind();
                    ddlPlant.Items.Insert(0, (new ListItem("-- Select --", "")));
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindPlantsByUser", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Bind Calcs
        public void bindCalcs(int PlantID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_SubmissionsByPlant_Active", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@PlantID", PlantID));
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsCalcs = new DataSet();
                    sda.Fill(dsCalcs);
                    ddlCalcs.DataSource = dsCalcs;
                    ddlCalcs.DataTextField = "CalcDescription";
                    ddlCalcs.DataValueField = "ID";
                    ddlCalcs.DataBind();
                    ddlCalcs.Items.Insert(0, (new ListItem("-- Select --", "")));
                    ddlCalcs.Items.Insert(1, (new ListItem("- New Calculation -", "0")));
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindCalcs", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region ddlCalcs_SelectedIndexChanged Event
        protected void ddlCalcs_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hidSubID.Value = "";
                if (ddlCalcs.SelectedIndex > 0)
                {
                    btnRunCalc.Enabled = true;
                    calcs.Visible = true;
                    if (user._securityLevelID > 1 && Convert.ToInt32(((Label)this.Master.FindControl("lblCalcsRemaining")).Text) < 1)
                        btnRunCalc.Enabled = false;
                    if (ddlCalcs.SelectedItem.Value != "0")
                    {
                        string queryString = QueryStringModule.Encrypt("CalcID=" + ddlCalcs.SelectedItem.Value);
                        lnkbtnViewResults.Attributes.Add("onclick", "javascript:window.open('ReportViewer.aspx" + queryString + "'); return false;");
                        lnkbtnViewResults.Visible = true;
                        ClearFromValues(calcs);
                        getSubmission(Convert.ToInt32(ddlCalcs.SelectedItem.Value));
                    }
                    else
                    {
                        lnkbtnViewResults.Visible = false;
                        ClearFromValues(calcs);
                    }
                }
                else
                {
                    btnRunCalc.Enabled = false;
                    calcs.Visible = false;
                    lnkbtnViewResults.Visible = false;
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("ddlCalcs_SelectedIndexChanged", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Clear Form Values
        public void ClearFromValues(Control parent)
        {
            try
            {
                foreach (Control c in parent.Controls)
                {
                    if (c is TextBox)
                        ((TextBox)(c)).Text = string.Empty;
                    else if (c is DropDownList && c.ClientID != ddlFeedClass.ClientID)
                        ((DropDownList)c).ClearSelection();
                }
                trProduct1.Style.Add("display", "none");
                txtProduct1Desc.Text = "";
                lblProduct1Name.Text = "";
                ((System.Web.UI.HtmlControls.HtmlInputButton)btnAddProduct).Disabled = false;
                trProduct2.Style.Add("display", "none");
                txtProduct2Desc.Text = "";
                lblProduct2Name.Text = "";
                txtProductionOther1.Text = "";
                txtProductionOther2.Text = "";
            }
            catch (Exception ex)
            {
                SendErrorEmail("ClearFormValues", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Get Submission
        public void getSubmission(int SubID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_Fact", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@SubmissionId", SubID));
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsFact = new DataSet();
                    sda.Fill(dsFact);

                    if (dsFact.Tables[0].Rows[0]["FeedClass"].ToString() != "")
                    {
                        ddlFeedClass.SelectedIndex = -1;
                        foreach (ListItem li in ddlFeedClass.Items)
                        {
                            if (li.Value == dsFact.Tables[0].Rows[0]["FeedClass"].ToString())
                            {
                                li.Selected = true;
                                break;
                            }
                        }
                    }

                    txtNameOfRun.Text = dsFact.Tables[0].Rows[0]["SubmissionName"].ToString();
                    txtStartDate.Text = Convert.ToDateTime(dsFact.Tables[0].Rows[0]["DateBeg"].ToString()).ToShortDateString();
                    txtEndDate.Text = Convert.ToDateTime(dsFact.Tables[0].Rows[0]["DateEnd"].ToString()).ToShortDateString();
                    DateTime dtStart = DateTime.Parse(txtStartDate.Text.ToString());
                    DateTime dtEnd = DateTime.Parse(txtEndDate.Text.ToString());
                    txtNumOfDays.Text = dtEnd.Subtract(dtStart.AddDays(-1)).Days.ToString();
                    txtComments.Text = dsFact.Tables[0].Rows[0]["SubmissionComment"].ToString();
                    //Capacity
                    double d;
                    txtCapacityEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["Ethylene_StreamDay_MTSD"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtCapacityEthylenePropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["Olefins_StreamDay_MTSD"].ToString(), out d)) ? d.ToString("F2") : "";
                    //Facility Count
                    txtNOUFFrationators.Text = dsFact.Tables[0].Rows[0]["FracFeed_UnitCount"].ToString();
                    txtNOUHighPBoilers.Text = dsFact.Tables[0].Rows[0]["BoilHP_UnitCount"].ToString();
                    txtNOULowPBoilers.Text = dsFact.Tables[0].Rows[0]["BoilLP_UnitCount"].ToString();
                    txtNOUEPGenerators.Text = dsFact.Tables[0].Rows[0]["ElecGen_UnitCount"].ToString();
                    txtNOUCHPurification.Text = dsFact.Tables[0].Rows[0]["HydroPurCryogenic_UnitCount"].ToString();
                    txtNOUPSAPurification.Text = dsFact.Tables[0].Rows[0]["HydroPurPSA_UnitCount"].ToString();
                    txtNOUMembranePurification.Text = dsFact.Tables[0].Rows[0]["HydroPurMembrane_UnitCount"].ToString();
                    txtNOUPGHydrotreater.Text = dsFact.Tables[0].Rows[0]["TowerPyroGasHT_UnitCount"].ToString();
                    txtTrains.Text = dsFact.Tables[0].Rows[0]["Trains_UnitCount"].ToString();
                    //Facility - Boilers
                    txtAddDataHighPBoiler.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["BoilHP_Rate_kLbHr"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtAddDataLowPBoiler.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["BoilLP_Rate_kLbHr"].ToString(), out d)) ? d.ToString("F2") : "";
                    //Facility - Electricity Generation
                    txtAddDataEPGenerator.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["Capacity_MW"].ToString(), out d)) ? d.ToString("F2") : "";
                    //Facility - FracFeed
                    txtAddDataFFractionator.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["FracFeed_Quantity_kBSD"].ToString(), out d)) ? d.ToString("F2") : "";
                    ddlFFractionatorFeedstock.SelectedIndex = -1;
                    foreach (ListItem li in ddlFFractionatorFeedstock.Items)
                        if (dsFact.Tables[0].Rows[0]["FracFeed_StreamId"].ToString() == li.Value)
                            li.Selected = true;
                    txtFFTonsKMT.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["FracFeed_Throughput_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFFTonsSG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["FracFeed_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";
                    //Facility - HydroTreater
                    txtAddDataPGHydrotreater.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["HT_Quantity_kBSD"].ToString(), out d)) ? d.ToString("F2") : "";
                    ddlPGhydrotreaterType.SelectedIndex = -1;
                    foreach (ListItem li in ddlPGhydrotreaterType.Items)
                        if (dsFact.Tables[0].Rows[0]["HT_HydroTreaterTypeId"].ToString() == li.Value)
                            li.Selected = true;
                    txtPGhydrotreaterPygas.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["HT_Processed_Pcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPGHydrotreaterSG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["HT_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtEthaneTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLNTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2001_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFRNTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2002_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2003_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtRAETotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2004_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNGLTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2005_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFCTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2006_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtDGOTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2007_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHATotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2008_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtSGOTotal.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2009_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF1Total.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2010_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF2Total.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2011_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF3Total.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2012_kMT"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtCCCPSQuantityEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4001_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtCCCPSQuantityPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4002_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtCCCPSQuantityButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4003_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtCCCPSQuantityBenzene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4004_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4005_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4006_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4007_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4008_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4009_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4010_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityC4.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4011_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGQuantityCOCO2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4012_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4013_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4014_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4015_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4016_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4017_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4018_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4019_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityButylenes.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4020_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4021_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityBenzene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4022_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityOGC.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4023_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityCWO.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4024_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityOGO.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4025_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSQuantityInertN2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4026_kMT"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtProductionHydrogenRG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5001_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionFuelGas.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5002_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionAcetylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5003_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionPGEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5004_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionCGEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5005_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionPGPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5006_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionCGPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5007_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionRGPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5008_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionRGPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5009_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5010_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionIsobutylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5011_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionOtherC4.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5012_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionBenzene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5013_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionOtherPGasoline.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5014_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionPGasOil.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5015_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionPFuelOil.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5016_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionAcidGas.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5017_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionOther1.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionOther2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionFuelConsumedInPlant.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5018_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionLossesFlareAndVent.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5022_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionLossesOther.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5023_kMT"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProductionLossesMeasurement.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5024_kMT"].ToString(), out d)) ? d.ToString("F2") : "";

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLFETotals", "calcFeedstocks('.ethane','#txtEthaneTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLFEPTotals", "calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLFPTotals", "calcFeedstocks('.propane','#txtPropaneTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLFLPGTotals", "calcFeedstocks('.LPG','#txtLPGTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLFBTotals", "calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLFOTotals", "calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doSupTotals", "calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doProdTotals", "calcProduction('.production','#txtProductionSubtotal');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doProdLosses", "calcProductionLosses('.productionlosses','#txtProductionLossesSubtotal');", true);

                    txtPriceQualityHydrogenRG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5001_H2_MolPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityFuelGas.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5002_CH4_MolPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtDGOSG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2007_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHFSG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2008_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtSHGOSG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2009_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF1SG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2010_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF2SG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2011_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF3SG.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2012_Density_SG"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtEthaneMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneNButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_NBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneIButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_IBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneIsobutylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_IBB2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthane1Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_B1_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneNPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_NC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneIPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_IC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneNHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_NC6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneIHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C6ISO_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneC7.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C7H16_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneC8.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_C8H18_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneCOCO2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_CO_CO2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthaneHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanSulfur.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1001_S_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtEthanePropaneMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropanePropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropanePropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneNButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_NBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneIButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_IBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneIsobutylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_IBB2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropane1Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_B1_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneNPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_NC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneIPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_IC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneNHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_NC6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneIHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C6ISO_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneC7.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C7H16_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneC8.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_C8H18_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneCOCO2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_CO_CO2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtEthanePropaneSulfur.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1002_S_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtPropaneMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropanePropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropanePropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneNButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_NBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneIButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_IBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneIsobutylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_IBB2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropane1Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_B1_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneNPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_NC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneIPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_IC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneNHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_NC6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneIHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C6ISO_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneC7.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C7H16_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneC8.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_C8H18_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneCOCO2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_CO_CO2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPropaneSulfur.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1003_S_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtLPGMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGNButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_NBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGIButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_IBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGIsobutylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_IBB2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPG1Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_B1_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGNPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_NC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGIPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_IC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGNHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_NC6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGIHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C6ISO_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGC7.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C7H16_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGC8.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_C8H18_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGCOCO2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_CO_CO2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLPGSulfur.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1004_S_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtButaneC4sMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sNButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_NBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sIButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_IBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sIsobutylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_IBB2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4s1Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_B1_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sNPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_NC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sIPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_IC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sNHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_NC6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sIHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C6ISO_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sC7.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C7H16_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sC8.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_C8H18_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sCOCO2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_CO_CO2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtButaneC4sSulfur.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1005_S_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtOtherMethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherNButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_NBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherIButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_IBUTA_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherIsobutylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_IBB2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOther1Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_B1_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherNPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_NC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherIPentane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_IC5_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherNHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_NC6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherIHexane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C6ISO_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txOtherC7.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C7H16_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherC8.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_C8H18_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherCOCO2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_CO_CO2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherHydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOtherSulfur.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_1006_S_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtLNNP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2001_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLNIP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2001_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLNNA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2001_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLNOL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2001_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtLNTA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2001_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtFRNNP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2002_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFRNIP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2002_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFRNNA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2002_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFRNOL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2002_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFRNTA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2002_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtHNNP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2003_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNIP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2003_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNNA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2003_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNOL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2003_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNTA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2003_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtRAENP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2004_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtRAEIP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2004_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtRAENA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2004_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtRAEOL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2004_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtRAWTA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2004_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtHNGLNP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2005_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNGLIP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2005_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNGLNA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2005_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNGLOL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2005_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtHNGLTA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2005_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtFCNP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2006_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFCIP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2006_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFCNA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2006_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFCOL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2006_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtFCTA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2006_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtOLF1NP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2010_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF1IP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2010_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF1NA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2010_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF1OL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2010_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF1TA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2010_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtOLF2NP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2011_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF2IP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2011_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF2NA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2011_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF2OL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2011_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF2TA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2011_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtOLF3NP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2012_P_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF3IP.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2012_I_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF3NA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2012_N_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF3OL.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2012_O_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtOLF3TA.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_2012_A_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqLNTotals", "calcFeedstocks('.ln','#txtLNTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqFRNTotals", "calcFeedstocks('.frn','#txtFRNTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqHNTotals", "calcFeedstocks('.hn','#txtHNTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqRAETotals", "calcFeedstocks('.rae','#txtRAETotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqHNGLTotals", "calcFeedstocks('.hngl','#txtHNGLTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqFCTotals", "calcFeedstocks('.fc','#txtFCTotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqOLF1Totals", "calcFeedstocks('.olf1','#txtOLF1TotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqOLF2Totals", "calcFeedstocks('.olf2','#txtOLF2TotalPercent');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doLiqOLF3Totals", "calcFeedstocks('.olf2','#txtOLF3TotalPercent');", true);

                    txtPriceQualityPGEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5004_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityCGEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5005_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityPGPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5006_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityCGPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5007_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityRGPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5008_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityRGPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5009_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityPPFCH2.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5018_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityPPFCCH4.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5018_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityPPFCEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5018_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    //txtPriceQualityPPFCEOtherFuel.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5018_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityPPFCEOtherFuel.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5018_Other_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtPriceQualityPPFCEInerts.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5018_Inerts_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtProduct1Hydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Methane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Acetylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C2H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Ethylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Ethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Propylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Propane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Butadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Butylenes.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C4H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C4H10_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Benzene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_C6H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1OtherPyrogasoline.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_OtherPyroGasoline_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1PyroFuelOil.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_PyroFuelOil_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct1Inerts.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5019_Inerts_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtProduct2Hydrogen.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Methane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_CH4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Acetylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C2H2_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Ethylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C2H4_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Ethane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Propylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C3H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Propane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Butadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C4H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Butylenes.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C4H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Butane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C4H10_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Benzene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_C6H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    //txtProduct2OtherPyrogasoline.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_PyroGasOil_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2OtherPyrogasoline.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_OtherPyroGasoline_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2PyroFuelOil.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_PyroFuelOil_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtProduct2Inerts.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_5020_Inerts_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doOP1Totals", "calcFeedstocks('.product1','#txtProduct1Total');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "doOP2Totals", "calcFeedstocks('.product2','#txtProduct2Total');", true);


                    txtLightFeedOther.Text = dsFact.Tables[0].Rows[0]["St_1006_Desc"].ToString();
                    txtOLF1.Text = dsFact.Tables[0].Rows[0]["St_2010_Desc"].ToString();
                    txtOLF2.Text = dsFact.Tables[0].Rows[0]["St_2011_Desc"].ToString();
                    txtOLF3.Text = dsFact.Tables[0].Rows[0]["St_2012_Desc"].ToString();
                    if (dsFact.Tables[0].Rows[0]["St_5019_Desc"].ToString() != "")
                        txtProduct1Desc.Text = dsFact.Tables[0].Rows[0]["St_5019_Desc"].ToString();
                    if (dsFact.Tables[0].Rows[0]["St_5020_Desc"].ToString() != "")
                        txtProduct2Desc.Text = dsFact.Tables[0].Rows[0]["St_5020_Desc"].ToString();
                    if (txtProduct1Desc.Text != "")
                    {
                        trProduct1.Style.Add("display", "block");
                        lblProduct1Desc.Style.Add("display", "block");
                        lblProduct1Desc.Text = txtProduct1Desc.Text;
                        lblProduct1Name.Text = txtProduct1Desc.Text;
                        //lblProduct1Name.Attributes.Add("onclick", "$('#divProduct1').css('display', 'block');$('#divProduct1Desc').css('display', 'none');$('#tblProduct1').css('display', 'block');$('#divProduct1').css('position', 'absolute');$('#divProduct1').css('top', Math.max(0, (($(window).height() - $('#divProduct1').outerHeight()) / 2) + $(window).scrollTop()) + 'px');$('#divProduct1').css('left', Math.max(0, (($(window).width() - $('#divProduct1').outerWidth()) / 2) + $(window).scrollLeft()) + 'px');");
                        ((System.Web.UI.HtmlControls.HtmlInputButton)btnAddProduct).Disabled = true;
                    }
                    else
                    {
                        trProduct1.Style.Add("display", "none");
                        txtProduct1Desc.Text = "";
                        lblProduct1Name.Text = "";
                        ((System.Web.UI.HtmlControls.HtmlInputButton)btnAddProduct).Disabled = false;
                    }

                    if (txtProduct2Desc.Text != "")
                    {
                        trProduct2.Style.Add("display", "block");
                        lblProduct2Desc.Style.Add("display", "block");
                        lblProduct2Desc.Text = txtProduct2Desc.Text;
                        lblProduct2Name.Text = txtProduct2Desc.Text;
                        //lblProduct2Name.Attributes.Add("onclick", "$('#divProduct2').css('display', 'block');$('#divProduct2Desc').css('display', 'none');$('#tblProduct2').css('display', 'block');$('#divProduct2').css('position', 'absolute');$('#divProduct2').css('top', Math.max(0, (($(window).height() - $('#divProduct2').outerHeight()) / 2) + $(window).scrollTop()) + 'px');$('#divProduct2').css('left', Math.max(0, (($(window).width() - $('#divProduct2').outerWidth()) / 2) + $(window).scrollLeft()) + 'px');");
                        //lblProduct2Name.Attributes.Add("onclick", "product2window();");
                    }
                    else
                    {
                        //lblProduct2Desc.Style.Add("display", "none");
                        trProduct2.Style.Add("display", "none");
                        txtProduct2Desc.Text = "";
                        lblProduct2Name.Text = "";
                    }

                    txtCCCPSRecoveryEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4001_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtCCCPSRecoveryPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4002_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtCCCPSRecoveryButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4003_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtCCCPSRecoveryBenzene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4004_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGRecoveryEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4008_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtROGRecoveryPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4010_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSRecoveryEthylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4016_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSRecoveryPropylene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4018_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSRecoveryButadiene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4021_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtODSRecoveryBenzene.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["St_4022_Recovered_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";

                    txtRecycleFeedEthane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["Recycled_C2H6_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtRecycleFeedPropane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["Recycled_C3H8_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                    txtRecycleFeedButane.Text = (Double.TryParse(dsFact.Tables[0].Rows[0]["Recycled_C4H10_WtPcnt"].ToString(), out d)) ? d.ToString("F2") : "";
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("getSubmission", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region ddlPlant_SelectedIndexChanged Event
        protected void ddlPlant_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlPlant.SelectedIndex > 0)
                {
                    bindCalcs(Convert.ToInt32(ddlPlant.SelectedItem.Value));
                    divddlCalc.Visible = true;
                    bindCalcsRemaining(Convert.ToInt32(ddlPlant.SelectedItem.Value));

                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                    using (conn)
                    {
                        SqlCommand command = new SqlCommand("[web].[Get_FeedClassForPlant]", conn);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@PlantID", ddlPlant.SelectedItem.Value));
                        conn.Open();

                        SqlDataReader dr = command.ExecuteReader();
                        dr.Read();
                        if (dr.HasRows)
                        {
                            if (dr["FeedClassId"] != null)
                            {
                                ddlFeedClass.SelectedIndex = -1;
                                foreach (ListItem li in ddlFeedClass.Items)
                                {
                                    if (li.Value == dr["FeedClassId"].ToString())
                                    {
                                        li.Selected = true;
                                        break;
                                    }
                                }
                            }
                        }
                        conn.Close();
                    }

                }
                else
                {
                    btnRunCalc.Enabled = false;
                    divddlCalc.Visible = false;
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("ddlPlant_SelectedIndexChanged", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Bind Calcs Remaining
        public void bindCalcsRemaining(int plantID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].get_PlantCalculationLimits", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@PlantID", plantID));
                    //SqlDataAdapter sda = new SqlDataAdapter(command);
                    conn.Open();

                    SqlDataReader dr = command.ExecuteReader();
                    dr.Read();
                    if (dr.HasRows)
                    {
                        ((Label)this.Master.FindControl("lblCalcsRemaining")).Text = dr["SubmissionsRemaining_Count"].ToString();
                        ((Label)this.Master.FindControl("lblCalcsResetDate")).Text = DateTime.Parse(dr["SubmissionRoll_Date"].ToString()).ToShortDateString();
                        ((UpdatePanel)this.Master.FindControl("upnlPlantInfo")).Update();
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("bindCalcsRemaining", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Execute Statement
        public void execStatement(SqlConnection conn, string storedProcedure, List<SqlParameter> parms, object val1 = null, object val2 = null, object val3 = null, object val4 = null, object val5 = null)
        {
            try
            {
                SqlCommand command = new SqlCommand(storedProcedure, conn);
                command.CommandType = CommandType.StoredProcedure;
                if (val1 != null) parms[0].Value = val1;
                if (val2 != null) parms[1].Value = val2;
                if (val3 != null) parms[2].Value = val3;
                if (val4 != null) parms[3].Value = val4;
                if (val5 != null) parms[4].Value = val5;
                foreach (SqlParameter parm in parms)
                    command.Parameters.Add(parm);
                command.ExecuteNonQuery();
                command.Parameters.Clear();
            }
            catch (Exception ex)
            {
                SendErrorEmail("execStatement", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region btnRunCalc_Click Event
        protected void btnRunCalc_Click(object sender, EventArgs e)
        {
            try
            {
                int subID = 0;
                if (hidSubID.Value != "")
                    subID = Convert.ToInt32(hidSubID.Value);

                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    conn.Open();

                    List<SqlParameter> parms = new List<SqlParameter>();

                    #region INSERT FACILITY
                    parms.Add(new SqlParameter("@FeedClass", ddlFeedClass.SelectedItem.Value));

                    parms.Add(new SqlParameter("@SubmissionName", txtNameOfRun.Text));
                    parms.Add(new SqlParameter("@DateBeg", txtStartDate.Text));
                    parms.Add(new SqlParameter("@DateEnd", txtEndDate.Text));
                    parms.Add(new SqlParameter("@SubmissionComment", txtComments.Text));
                    //Capacity
                    parms.Add(new SqlParameter("@Ethylene_StreamDay_MTSD", txtCapacityEthylene.Text));
                    parms.Add(new SqlParameter("@Olefins_StreamDay_MTSD", txtCapacityEthylenePropylene.Text));
                    //Facility Count
                    parms.Add(new SqlParameter("@FracFeed_UnitCount", txtNOUFFrationators.Text));
                    parms.Add(new SqlParameter("@BoilHP_UnitCount", txtNOUHighPBoilers.Text));
                    parms.Add(new SqlParameter("@BoilLP_UnitCount", txtNOULowPBoilers.Text));
                    parms.Add(new SqlParameter("@ElecGen_UnitCount", txtNOUEPGenerators.Text));
                    parms.Add(new SqlParameter("@HydroPurCryogenic_UnitCount", txtNOUCHPurification.Text));
                    parms.Add(new SqlParameter("@HydroPurPSA_UnitCount", txtNOUPSAPurification.Text));
                    parms.Add(new SqlParameter("@HydroPurMembrane_UnitCount", txtNOUMembranePurification.Text));
                    parms.Add(new SqlParameter("@TowerPyroGasHT_UnitCount", txtNOUPGHydrotreater.Text));
                    parms.Add(new SqlParameter("@Trains_UnitCount", txtTrains.Text));
                    //Facility - Boilers
                    parms.Add(new SqlParameter("@BoilHP_Rate_kLbHr", txtAddDataHighPBoiler.Text.Replace("k lb/hr", "")));
                    parms.Add(new SqlParameter("@BoilLP_Rate_kLbHr", txtAddDataLowPBoiler.Text.Replace("k lb/hr", "")));
                    //Facility - Electricity Generation
                    parms.Add(new SqlParameter("@Capacity_MW", txtAddDataEPGenerator.Text.Replace("Generation Capacity, MW", "")));
                    //Facility - FracFeed
                    parms.Add(new SqlParameter("@FracFeed_Quantity_kBSD", txtAddDataFFractionator.Text.Replace("k b/sd Feed", "")));
                    parms.Add(new SqlParameter("@FracFeed_StreamId", ddlFFractionatorFeedstock.SelectedItem.Value));
                    parms.Add(new SqlParameter("@FracFeed_Throughput_kMT", txtFFTonsKMT.Text.Replace("k MT", "")));
                    parms.Add(new SqlParameter("@FracFeed_Density_SG", txtFFTonsSG.Text.Replace("SG", "")));
                    //Facility - HydroTreater
                    parms.Add(new SqlParameter("@HT_Quantity_kBSD", txtAddDataPGHydrotreater.Text.Replace("k b/sd Feed", "")));
                    parms.Add(new SqlParameter("@HT_HydroTreaterTypeId", ddlPGhydrotreaterType.SelectedItem.Value));
                    parms.Add(new SqlParameter("@HT_Processed_Pcnt", txtPGhydrotreaterPygas.Text));
                    parms.Add(new SqlParameter("@HT_Density_SG", txtPGHydrotreaterSG.Text.Replace("SG", "")));

                    if (subID > 0)
                        parms.Add(new SqlParameter("@SubmissionId", subID));

                    SqlParameter parmOut = new SqlParameter();
                    parmOut.Direction = ParameterDirection.ReturnValue;
                    parmOut.ParameterName = "@SubId";
                    parms.Add(parmOut);

                    SqlCommand command = new SqlCommand("[web].Update_Facilities", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter parm in parms)
                        command.Parameters.Add(parm);
                    command.ExecuteNonQuery();

                    subID = Convert.ToInt32(command.Parameters["@SubId"].Value);

                    hidSubID.Value = subID.ToString();
                    #endregion


                    SqlCommand command2 = new SqlCommand("[web].Update_JoinPlantSubmission", conn);
                    command2.CommandType = CommandType.StoredProcedure;
                    command2.Parameters.Add(new SqlParameter("@PlantID", ddlPlant.SelectedItem.Value));
                    command2.Parameters.Add(new SqlParameter("@SubmissionId", subID));
                    command2.Parameters.Add(new SqlParameter("@Active", true));
                    command2.ExecuteNonQuery();

                    parms.Clear();

                    #region INSERT STREAM QUANTITY
                    parms.Add(new SqlParameter("@SubmissionId", null));
                    parms.Add(new SqlParameter("@StreamNumber", null));
                    parms.Add(new SqlParameter("@StreamTypeId", null));
                    parms.Add(new SqlParameter("@Quantity_kMT", null));

                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 1001, 9, txtEthaneTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 1002, 8, txtEthanePropaneTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 1003, 10, txtPropaneTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 1004, 11, txtLPGTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 1005, 12, txtButaneC4sTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 1006, 14, txtOtherTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2001, 20, txtLNTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2002, 21, txtFRNTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2003, 22, txtHNTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2004, 23, txtRAETotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2005, 18, txtHNGLTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2006, 24, txtFCTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2007, 28, txtDGOTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2008, 30, txtHATotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2009, 31, txtSGOTotal.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2010, 33, txtOLF1Total.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2011, 33, txtOLF2Total.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 2012, 33, txtOLF3Total.Text);

                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4001, 43, txtCCCPSQuantityEthylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4002, 44, txtCCCPSQuantityPropylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4003, 42, txtCCCPSQuantityButadiene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4004, 41, txtCCCPSQuantityBenzene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4005, 51, txtROGQuantityHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4006, 53, txtROGQuantityMethane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4007, 47, txtROGQuantityEthane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4008, 50, txtROGQuantityEthylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4009, 54, txtROGQuantityPropane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4010, 57, txtROGQuantityPropylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4011, 46, txtROGQuantityC4.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4012, 52, txtROGQuantityCOCO2.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4013, 70, txtODSQuantityHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4014, 71, txtODSQuantityMethane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4015, 66, txtODSQuantityEthane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4016, 69, txtODSQuantityEthylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4017, 72, txtODSQuantityPropane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4018, 60, txtODSQuantityPropylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4019, 62, txtODSQuantityButane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4020, 65, txtODSQuantityButylenes.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4021, 61, txtODSQuantityButadiene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4022, 59, txtODSQuantityBenzene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4023, 75, txtODSQuantityOGC.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4024, 78, txtODSQuantityCWO.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4025, 77, txtODSQuantityOGO.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 4026, 80, txtODSQuantityInertN2.Text);

                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5001, 97, txtProductionHydrogenRG.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5002, 102, txtProductionFuelGas.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5003, 94, txtProductionAcetylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5004, 88, txtProductionPGEthylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5005, 89, txtProductionCGEthylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5006, 91, txtProductionPGPropylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5007, 92, txtProductionCGPropylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5008, 93, txtProductionRGPropylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5009, 106, txtProductionRGPropane.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5010, 96, txtProductionButadiene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5011, 103, txtProductionIsobutylene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5012, 104, txtProductionOtherC4.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5013, 95, txtProductionBenzene.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5014, 105, txtProductionOtherPGasoline.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5015, 108, txtProductionPGasOil.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5016, 107, txtProductionPFuelOil.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5017, 101, txtProductionAcidGas.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5019, 98, txtProductionOther1.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5020, 98, txtProductionOther2.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5018, 113, txtProductionFuelConsumedInPlant.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5022, 124, txtProductionLossesFlareAndVent.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5023, 126, txtProductionLossesOther.Text);
                    execStatement(conn, "[web].Update_StreamQuantity", parms, subID, 5024, 125, txtProductionLossesMeasurement.Text);

                    parms.Clear();
                    #endregion

                    #region INSERT STREAM RECOVERED
                    parms.Add(new SqlParameter("@SubmissionId", null));
                    parms.Add(new SqlParameter("@StreamNumber", null));
                    parms.Add(new SqlParameter("@Recovered_WtPcnt", null));

                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4001, txtCCCPSRecoveryEthylene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4002, txtCCCPSRecoveryPropylene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4003, txtCCCPSRecoveryButadiene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4004, txtCCCPSRecoveryBenzene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4008, txtROGRecoveryEthylene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4010, txtROGRecoveryPropylene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4016, txtODSRecoveryEthylene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4018, txtODSRecoveryPropylene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4021, txtODSRecoveryButadiene.Text);
                    execStatement(conn, "[web].Update_StreamRecovered", parms, subID, 4022, txtODSRecoveryBenzene.Text);

                    parms.Clear();
                    #endregion

                    #region INSERT STREAM RECYCLED
                    parms.Add(new SqlParameter("@SubmissionId", null));
                    parms.Add(new SqlParameter("@ComponentId", null));
                    parms.Add(new SqlParameter("@Recycled_WtPcnt", null));

                    execStatement(conn, "[web].Update_StreamRecycled", parms, subID, 18, (txtRecycleFeedEthane.Text != "" && txtRecycleFeedEthane.Text != "wt %") ? txtRecycleFeedEthane.Text : "");
                    execStatement(conn, "[web].Update_StreamRecycled", parms, subID, 19, (txtRecycleFeedPropane.Text != "" && txtRecycleFeedPropane.Text != "wt %") ? txtRecycleFeedPropane.Text : "");
                    execStatement(conn, "[web].Update_StreamRecycled", parms, subID, 21, (txtRecycleFeedButane.Text != "" && txtRecycleFeedButane.Text != "wt %") ? txtRecycleFeedButane.Text : "");

                    parms.Clear();
                    #endregion

                    #region INSERT STREAM DESCRIPTION
                    parms.Add(new SqlParameter("@SubmissionId", null));
                    parms.Add(new SqlParameter("@StreamNumber", null));
                    parms.Add(new SqlParameter("@StreamDescription", null));

                    execStatement(conn, "[web].Update_StreamDescription", parms, subID, 1006, (txtLightFeedOther.Text != "" && txtLightFeedOther.Text != "DESCRIBE") ? txtLightFeedOther.Text : "");
                    execStatement(conn, "[web].Update_StreamDescription", parms, subID, 2010, (txtOLF1.Text != "" && txtOLF1.Text != "DESCRIBE") ? txtOLF1.Text : "");
                    execStatement(conn, "[web].Update_StreamDescription", parms, subID, 2011, (txtOLF2.Text != "" && txtOLF2.Text != "DESCRIBE") ? txtOLF2.Text : "");
                    execStatement(conn, "[web].Update_StreamDescription", parms, subID, 2012, (txtOLF3.Text != "" && txtOLF3.Text != "DESCRIBE") ? txtOLF3.Text : "");
                    execStatement(conn, "[web].Update_StreamDescription", parms, subID, 5019, txtProduct1Desc.Text);
                    execStatement(conn, "[web].Update_StreamDescription", parms, subID, 5020, txtProduct2Desc.Text);

                    parms.Clear();
                    #endregion

                    #region INSERT STREAM COMPOSITION
                    parms.Add(new SqlParameter("@SubmissionId", null));
                    parms.Add(new SqlParameter("@StreamNumber", null));
                    parms.Add(new SqlParameter("@ComponentId", null));
                    parms.Add(new SqlParameter("@Component_WtPcnt", null));

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 17, txtEthaneMethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 18, txtEthaneEthane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 15, txtEthaneEthylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 19, txtEthanePropane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 16, txtEthanePropylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 22, txtEthaneNButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 23, txtEthaneIButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 182, txtEthaneIsobutylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 26, txtEthane1Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 5, txtEthaneButadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 98, txtEthaneNPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 99, txtEthaneIPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 114, txtEthaneNHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 115, txtEthaneIHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 116, txtEthaneC7.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 119, txtEthaneC8.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 29, txtEthaneCOCO2.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 7, txtEthaneHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1001, 32, txtEthanSulfur.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 17, txtEthanePropaneMethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 18, txtEthanePropaneEthane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 15, txtEthanePropaneEthylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 19, txtEthanePropanePropane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 16, txtEthanePropanePropylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 22, txtEthanePropaneNButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 23, txtEthanePropaneIButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 182, txtEthanePropaneIsobutylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 26, txtEthanePropane1Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 5, txtEthanePropaneButadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 98, txtEthanePropaneNPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 99, txtEthanePropaneIPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 114, txtEthanePropaneNHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 115, txtEthanePropaneIHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 116, txtEthanePropaneC7.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 119, txtEthanePropaneC8.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 29, txtEthanePropaneCOCO2.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 7, txtEthanePropaneHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1002, 32, txtEthanePropaneSulfur.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 17, txtPropaneMethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 18, txtPropaneEthane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 15, txtPropaneEthylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 19, txtPropanePropane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 16, txtPropanePropylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 22, txtPropaneNButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 23, txtPropaneIButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 182, txtPropaneIsobutylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 26, txtPropane1Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 5, txtPropaneButadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 98, txtPropaneNPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 99, txtPropaneIPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 114, txtPropaneNHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 115, txtPropaneIHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 116, txtPropaneC7.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 119, txtPropaneC8.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 29, txtPropaneCOCO2.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 7, txtPropaneHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1003, 32, txtPropaneSulfur.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 17, txtLPGMethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 18, txtLPGEthane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 15, txtLPGEthylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 19, txtLPGPropane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 16, txtLPGPropylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 22, txtLPGNButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 23, txtLPGIButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 182, txtLPGIsobutylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 26, txtLPG1Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 5, txtLPGButadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 98, txtLPGNPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 99, txtLPGIPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 114, txtLPGNHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 115, txtLPGIHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 116, txtLPGC7.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 119, txtLPGC8.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 29, txtLPGCOCO2.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 7, txtLPGHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1004, 32, txtLPGSulfur.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 17, txtButaneC4sMethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 18, txtButaneC4sEthane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 15, txtButaneC4sEthylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 19, txtButaneC4sPropane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 16, txtButaneC4sPropylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 22, txtButaneC4sNButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 23, txtButaneC4sIButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 182, txtButaneC4sIsobutylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 26, txtButaneC4s1Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 5, txtButaneC4sButadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 98, txtButaneC4sNPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 99, txtButaneC4sIPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 114, txtButaneC4sNHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 115, txtButaneC4sIHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 116, txtButaneC4sC7.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 119, txtButaneC4sC8.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 29, txtButaneC4sCOCO2.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 7, txtButaneC4sHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1005, 32, txtButaneC4sSulfur.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 17, txtOtherMethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 18, txtOtherEthane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 15, txtOtherEthylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 19, txtOtherPropane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 16, txtOtherPropylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 22, txtOtherNButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 23, txtOtherIButane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 182, txtOtherIsobutylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 26, txtOther1Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 5, txtOtherButadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 98, txtOtherNPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 99, txtOtherIPentane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 114, txtOtherNHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 115, txtOtherIHexane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 116, txOtherC7.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 119, txtOtherC8.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 29, txtOtherCOCO2.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 7, txtOtherHydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 1006, 32, txtOtherSulfur.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2001, 169, txtLNNP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2001, 170, txtLNIP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2001, 171, txtLNNA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2001, 172, txtLNOL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2001, 173, txtLNTA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2002, 169, txtFRNNP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2002, 170, txtFRNIP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2002, 171, txtFRNNA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2002, 172, txtFRNOL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2002, 173, txtFRNTA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2003, 169, txtHNNP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2003, 170, txtHNIP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2003, 171, txtHNNA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2003, 172, txtHNOL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2003, 173, txtHNTA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2004, 169, txtRAENP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2004, 170, txtRAEIP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2004, 171, txtRAENA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2004, 172, txtRAEOL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2004, 173, txtRAWTA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2005, 169, txtHNGLNP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2005, 170, txtHNGLIP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2005, 171, txtHNGLNA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2005, 172, txtHNGLOL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2005, 173, txtHNGLTA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2006, 169, txtFCNP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2006, 170, txtFCIP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2006, 171, txtFCNA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2006, 172, txtFCOL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2006, 173, txtFCTA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2010, 169, txtOLF1NP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2010, 170, txtOLF1IP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2010, 171, txtOLF1NA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2010, 172, txtOLF1OL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2010, 173, txtOLF1TA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2011, 169, txtOLF2NP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2011, 170, txtOLF2IP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2011, 171, txtOLF2NA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2011, 172, txtOLF2OL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2011, 173, txtOLF2TA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2012, 169, txtOLF3NP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2012, 170, txtOLF3IP.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2012, 171, txtOLF3NA.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2012, 172, txtOLF3OL.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 2012, 173, txtOLF3TA.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5004, 15, (txtPriceQualityPGEthylene.Text != "" && txtPriceQualityPGEthylene.Text != "wt % Ethylene") ? txtPriceQualityPGEthylene.Text : "");
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5005, 15, (txtPriceQualityCGEthylene.Text != "" && txtPriceQualityCGEthylene.Text != "wt % Ethylene") ? txtPriceQualityCGEthylene.Text : "");
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5006, 16, (txtPriceQualityPGPropylene.Text != "" && txtPriceQualityPGPropylene.Text != "wt % Propylene") ? txtPriceQualityPGPropylene.Text : "");
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5007, 16, (txtPriceQualityCGPropylene.Text != "" && txtPriceQualityCGPropylene.Text != "wt % Propylene") ? txtPriceQualityCGPropylene.Text : "");
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5008, 16, (txtPriceQualityRGPropylene.Text != "" && txtPriceQualityRGPropylene.Text != "wt % Propylene") ? txtPriceQualityRGPropylene.Text : "");
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5009, 19, (txtPriceQualityRGPropane.Text != "" && txtPriceQualityRGPropane.Text != "wt % Propane") ? txtPriceQualityRGPropane.Text : "");
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5018, 7, txtPriceQualityPPFCH2.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5018, 17, txtPriceQualityPPFCCH4.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5018, 15, txtPriceQualityPPFCEthylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5018, 165, txtPriceQualityPPFCEOtherFuel.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5018, 164, txtPriceQualityPPFCEInerts.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 7, txtProduct1Hydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 17, txtProduct1Methane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 9, txtProduct1Acetylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 15, txtProduct1Ethylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 18, txtProduct1Ethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 16, txtProduct1Propylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 19, txtProduct1Propane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 5, txtProduct1Butadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 24, txtProduct1Butylenes.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 21, txtProduct1Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 6, txtProduct1Benzene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 33, txtProduct1OtherPyrogasoline.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 163, txtProduct1PyroFuelOil.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5019, 164, txtProduct1Inerts.Text);

                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 7, txtProduct2Hydrogen.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 17, txtProduct2Methane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 9, txtProduct2Acetylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 15, txtProduct2Ethylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 18, txtProduct2Ethane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 16, txtProduct2Propylene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 19, txtProduct2Propane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 5, txtProduct2Butadiene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 24, txtProduct2Butylenes.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 21, txtProduct2Butane.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 6, txtProduct2Benzene.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 33, txtProduct2OtherPyrogasoline.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 163, txtProduct2PyroFuelOil.Text);
                    execStatement(conn, "[web].Update_StreamComposition", parms, subID, 5020, 164, txtProduct2Inerts.Text);

                    parms.Clear();
                    #endregion

                    #region INSERT STREAM COMPOSITION MOL
                    parms.Add(new SqlParameter("@SubmissionId", null));
                    parms.Add(new SqlParameter("@StreamNumber", null));
                    parms.Add(new SqlParameter("@ComponentId", null));
                    parms.Add(new SqlParameter("@Component_MolPcnt", null));

                    execStatement(conn, "[web].Update_StreamCompositionMol", parms, subID, 5001, 7, (txtPriceQualityHydrogenRG.Text != "" && txtPriceQualityHydrogenRG.Text != "mol % Hydrogen") ? txtPriceQualityHydrogenRG.Text : "");
                    execStatement(conn, "[web].Update_StreamCompositionMol", parms, subID, 5002, 17, (txtPriceQualityFuelGas.Text != "" && txtPriceQualityFuelGas.Text != "mol % Methane") ? txtPriceQualityFuelGas.Text : "");

                    parms.Clear();
                    #endregion

                    #region INSERT STREAM DENSITY
                    parms.Add(new SqlParameter("@SubmissionId", null));
                    parms.Add(new SqlParameter("@StreamNumber", null));
                    parms.Add(new SqlParameter("@Density_SG", null));

                    execStatement(conn, "[web].Update_StreamDensity", parms, subID, 2007, txtDGOSG.Text);
                    execStatement(conn, "[web].Update_StreamDensity", parms, subID, 2008, txtHFSG.Text);
                    execStatement(conn, "[web].Update_StreamDensity", parms, subID, 2009, txtSHGOSG.Text);
                    execStatement(conn, "[web].Update_StreamDensity", parms, subID, 2010, txtOLF1SG.Text);
                    execStatement(conn, "[web].Update_StreamDensity", parms, subID, 2011, txtOLF2SG.Text);
                    execStatement(conn, "[web].Update_StreamDensity", parms, subID, 2012, txtOLF3SG.Text);

                    parms.Clear();
                    #endregion

                    #region VERIFY DATASET
                    VerifyDatasetStage(subID);
                    #endregion

                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("btnRunCalc_Click", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Verify Dataset Stage
        public void VerifyDatasetStage(int subID)
        {
            try
            {
                //subID = 62;
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    user = (OlefinsCalculator.User)Session["User"];
                    conn.Open();
                    SqlCommand command = new SqlCommand("[web].verify_DatasetStage", conn);
                    //SqlCommand command = new SqlCommand("[stage].verify_Dataset", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@SubmissionId", subID));
                    System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Request.UserLanguages[0]);
                    command.Parameters.Add(new SqlParameter("@LanguageId", System.Threading.Thread.CurrentThread.CurrentCulture.LCID));
                    command.Parameters.Add(new SqlParameter("@LoginId", user._userID));

                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsErrors = new DataSet();
                    sda.Fill(dsErrors);
                    if (dsErrors.Tables[0].Rows.Count > 0)
                    {
                        DataTable dtHeaders = dsErrors.Tables[0].DefaultView.ToTable(true, "DisplaySection");
                        foreach (DataRow dr in dtHeaders.Rows)
                        {
                            DataTable dtSections = new DataTable();
                            dtSections = dsErrors.Tables[0].DefaultView.ToTable(true, "DisplaySection", "DisplayCorrection");
                            GridView dgvError1 = new GridView();
                            dgvError1.AutoGenerateColumns = true;
                            dgvError1.ShowHeader = true;
                            dgvError1.BorderStyle = BorderStyle.None;
                            DataTable dtSection1 = new DataTable();
                            dtSection1.Columns.Add();
                            dtSection1.Columns.Add();
                            //DataTable dtBound = dtSections.Select("DisplaySection = '" + dtSections.Rows[0][0].ToString() + "'").CopyToDataTable();
                            DataTable dtBound = dtSections.Select("DisplaySection = '" + dr[0].ToString() + "'").CopyToDataTable();
                            dtBound.Columns.RemoveAt(0);
                            dgvError1.DataSource = dtBound;
                            dgvError1.DataBind();
                            dgvError1.HeaderRow.Cells[0].Text = dr[0].ToString();
                            dgvError1.HeaderRow.Cells[0].Style.Add("background-color", "#0380cd");
                            dgvError1.HeaderRow.Cells[0].Style.Add("color", "#ffffff");
                            dgvError1.HeaderRow.Cells[0].Style.Add("font-weight", "normal");
                            dgvError1.Style.Add("width", "100%");
                            dgvError1.Style.Add("margin-bottom", "15px");
                            dgvError1.RowStyle.BorderStyle = BorderStyle.Solid;
                            dgvError1.RowStyle.BorderWidth = 1;
                            dgvError1.HeaderStyle.BorderStyle = BorderStyle.Solid;
                            dgvError1.HeaderStyle.BorderWidth = 1;
                            phDataChecks.Controls.Add(dgvError1);
                        }
                        mpeDataChecks.Show();
                    }
                    else
                    {
                        mpeUseCalc.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("verifyDatasetStage", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Process Dataset Stage
        public int ProcessDatasetStage(int subID)
        {
            try
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    user = (OlefinsCalculator.User)Session["User"];
                    conn.Open();
                    var statusID = new object();
                    SqlCommand command = new SqlCommand("[web].process_DatasetStage", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@SubmissionId", subID));
                    command.Parameters.Add(new SqlParameter("@MethodologyId", DBNull.Value));
                    command.Parameters.Add(new SqlParameter("@LoginId", user._userID));

                    SqlParameter parmResult = new SqlParameter();
                    parmResult.Direction = ParameterDirection.ReturnValue;
                    parmResult.ParameterName = "ResultId";
                    command.Parameters.Add(parmResult);

                    command.ExecuteNonQuery();

                    return Convert.ToInt32(command.Parameters["ResultId"].Value);
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("ProcessDatasetStage", ex.Message, ex.StackTrace);
                return -1;
            }
        }
        #endregion

        #region btnUseCalc_Click Event
        protected void btnUseCalc_Click(object sender, EventArgs e)
        {
            try
            {
                string subID = hidSubID.Value;
                if (ProcessDatasetStage(Convert.ToInt32(subID)) < 0)
                {
                    throw new Exception("An Error Occured Processing DataSet Stage.");
                }
                else
                {
                    hidSubID.Value = "";
                    Response.Redirect("ReportViewer.aspx" + QueryStringModule.Encrypt("CalcID=" + subID), false);
                }
            }
            catch (Exception ex)
            {
                SendErrorEmail("btnUseCalc_Click", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region btnCancelCalc_Click Event
        protected void btnCancelCalc_Click(object sender, EventArgs e)
        {
            try
            {
                mpeUseCalc.Hide();
            }
            catch (Exception ex)
            {
                SendErrorEmail("btnCancelCalc_Click", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Send Error Email
        private void SendErrorEmail(string Method, string Error, string StackTrace)
        {
            try
            {
                string body = string.Empty;
                body = "<html>Olefins Online Calculator has reported an error. The details of the exception are below:<br /><br /><table border=\"0\" cellspacing=\"2\" cellpadding=\"2\"><tr><td width=\"100\"><b>Time:</b></td><td>" + DateTime.Now;
                body += "</td></tr><tr><td><b>User:</b></td><td>" + HttpContext.Current.User.Identity.Name;
                body += "</td></tr><tr><td><b>Method:</b></td><td>" + Method;
                body += "</td></tr><tr><td valign=\"top\"><b>Exception Message:</b></td><td valign=\"top\">" + Error;
                body += "</td></tr><tr><td valign=\"top\"><b>Stack Trace:</b></td><td valign=\"top\">" + StackTrace;
                body += "</td></tr></table></html>";

                SendMail sm = new SendMail();
                sm.SendEMailMessage("Error Handler", "no-reply@solomononline.com", "jbf@solomononline.com;rrh@solomononline.com", null, null, "Olefins Online Calculator Error Report", body);
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}