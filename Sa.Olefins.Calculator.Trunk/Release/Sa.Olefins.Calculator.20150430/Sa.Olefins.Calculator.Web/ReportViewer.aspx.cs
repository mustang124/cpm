﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace OlefinsCalculator
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        int CalcID;
        protected void Page_Load(object sender, EventArgs e)
        {
            

            ((System.Web.UI.HtmlControls.HtmlAnchor)this.Master.FindControl("alogin")).InnerText = "Logout";
            if (!Page.IsPostBack)
            {
                ReportViewer1.Reset();
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                LocalReport localReport = ReportViewer1.LocalReport;
                if (Request.IsLocal)
                {
                    localReport.ReportPath = "Report.rdlc";
                    //localReport.ReportPath = "test.rdlc";
                }
                else
                {
                    localReport.ReportPath = "Report.rdlc";
                }

                if (Request.QueryString["enc"] != null)
                    CalcID = Convert.ToInt32(QueryStringModule.Decrypt(Request.QueryString["enc"].ToString()));

                if (Request.QueryString["CalcID"] != null)
                    CalcID = Convert.ToInt32(Request.QueryString["CalcID"].ToString());
                else
                    CalcID = 1;

                DataSet dsCalcs = new DataSet();
                //SqlConnection conn = new SqlConnection("Data Source=OPTIPLEXJ1B7F60\\SQLEXPRESS;Initial Catalog=OlefinCalculator;Integrated Security=True");
                SqlConnection conn = new SqlConnection("data source=10.10.41.7;initial catalog=OlefinsOnline;persist security info=True;user id=jbf;password=jbf@1760;MultipleActiveResultSets=True");
                //SqlCommand command = new SqlCommand("getcalc", conn);
                SqlCommand command = new SqlCommand("[web].Get_FactorsAndStandards", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@MethodologyId", DBNull.Value));
                command.Parameters.Add(new SqlParameter("@SubmissionId", CalcID));
                SqlDataAdapter sdaCalcs = new SqlDataAdapter(command);
                using (conn)
                {
                    sdaCalcs.Fill(dsCalcs, "Calcs");
                }

                ReportDataSource dsCalcReport = new ReportDataSource();
                dsCalcReport.Name = "dsCalcs";
                dsCalcReport.Value = dsCalcs.Tables["Calcs"];
                localReport.DataSources.Add(dsCalcReport);

                //Hide export to PDF option. Doing this because the table gets cut off and split into two tables because it's too wide.
                System.Reflection.FieldInfo info;
                foreach (RenderingExtension extension in localReport.ListRenderingExtensions())
                {
                    if (extension.Name == "PDF")
                    {
                        info = extension.GetType().GetField("m_isVisible", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                        info.SetValue(extension, false);
                    }
                }

            

                //ReportParameter rpCalcID = new ReportParameter();
                //rpCalcID.Name = "CalcID";
                //rpCalcID.Values.Add("2");
                //localReport.SetParameters(new ReportParameter[] { rpCalcID });

                //ReportViewer1.Reset();
                //ReportViewer1.ProcessingMode = ProcessingMode.Local;
                //LocalReport localReport = ReportViewer1.LocalReport;
                //localReport.ReportPath = "Report.rdlc";
                //DataSet dataset = new DataSet("Sales Order Detail");
                //int CalcID = 1;
                //GetSalesOrderData(CalcID, ref dataset);
                //ReportDataSource DataSet1 = new ReportDataSource();
                //DataSet1.Name = "DataSet1";
                //DataSet1.Value = dataset.Tables["Calcs"];
                //localReport.DataSources.Add(DataSet1);
                //// Create the sales order number report parameter
                //ReportParameter rpCalcID = new ReportParameter();
                //rpCalcID.Name = "CalcID";
                //rpCalcID.Values.Add("2");
                //// Set the report parameters for the report
                //localReport.SetParameters(new ReportParameter[] { rpCalcID });

                //localReport.Refresh();
                //System.Data.DataSet ds = GetDataSet();
                ////reportViewer1.LocalReport.ReportPath = "Report1.rdlc";
                //ReportDataSource rds = new ReportDataSource("ProductsDataSet", ds.Tables[0]);
                //this.reportViewer1.LocalReport.DataSources.Clear();
                //this.reportViewer1.LocalReport.DataSources.Add(rds);
                //this.bindingSource1.DataSource = rds;
                //this.reportViewer1.RefreshReport();





                //ReportViewer1.Reset();


                //ReportViewer1.LocalReport rep = new Microsoft.Reporting.WebForms.LocalReport();

                //Dim rep As LocalReport = ReportViewer1.LocalReport
                //rep.ReportPath = "Report1.rdlc"

                //Dim printRep As New MyClass
                //Dim dsRep As New ReportDataSource
                //dsRep.Name = "DataSet1_DataSet1"
                //dsRep.Value = printRep.GetDataTable("Select * from Whatever", "Whatever")        ' This function simply returns a DataTable
                //rep.DataSources.Clear()
                //rep.DataSources.Add(dsInvoice)
            }
        }

        private void GetSalesOrderData(int CalcID, ref DataSet dsSalesOrder)
        {
            string sqlSalesOrder = "SELECT * FROM Calcs WHERE ID = @CalcID";

            SqlConnection connection = new SqlConnection("Data Source=OPTIPLEXJ1B7F60\\SQLEXPRESS;Initial Catalog=OlefinCalculator;Integrated Security=True");

            SqlCommand command = new SqlCommand(sqlSalesOrder, connection);

            command.Parameters.Add(new SqlParameter("CalcID", CalcID));

            SqlDataAdapter salesOrderAdapter = new SqlDataAdapter(command);

            salesOrderAdapter.Fill(dsSalesOrder, "Calcs");
        }
    }
}