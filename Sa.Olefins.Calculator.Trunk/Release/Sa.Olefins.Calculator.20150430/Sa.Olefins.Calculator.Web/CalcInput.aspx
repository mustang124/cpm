﻿<%@ Page Title="Olefin Study Factors and Standards Online Calculator" Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="CalcInput.aspx.cs" Inherits="OlefinsCalculator.CalcInput" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<title>Olefin Study Factors and Standards Online Calculator</title>
    <style type="text/css">
        div#header-fixed
        {
            position: fixed;
            top: 0px;
            margin: auto;
            z-index: 100000;
            width: 100%;
        }
        .tableHeader
        {
            background-color: #E6EAED;
            border-left: 1px solid #000000;
            vertical-align: top;
            font-weight: bold;
            text-align: center;
        }
        .tableHeader2ndRow
        {
            border-left: 1px solid #000000;
            background-color: #E6EAED;
            border-bottom: 0px solid #000000;
        }
        .tableHeaderSingleRow
        {
            border-left: 1px solid #000000;
            background-color: #E6EAED;
            border-bottom: 0px solid #000000;
            vertical-align: top;
            font-weight: bold;
            text-align: center;
        }
        .sectionHeader
        {
            color: #FFFFFF;
        }
        .ethane
        {
            text-align: right;
        }
        .ethanepropane
        {
            text-align: right;
        }
        .propane
        {
            text-align: right;
        }
        .LPG
        {
            text-align: right;
        }
        .ButaneC4s
        {
            text-align: right;
        }
        .OtherLightFeed
        {
            text-align: right;
        }
        .dgo, .hf, .shgo, .olf1, .olf2, .olf3, .ln, .frn, .hn, .rae, .hngl, .fc, .supQuantity, .production, .productionlosses
        {
            text-align: right;
        }
        .modalBackground
        {
            background-color:Gray;
            filter:alpha(opacity=50);
            opacity:0.7;
        }
        .modalBackgroundFixed
        {
            background-color:Gray;
            filter:alpha(opacity=50);
            opacity:0.7;
            position: fixed;
            top: 0px;
            margin: auto;
            z-index: 100000;
            width: 100%;
        }
    </style>
    <script type="text/javascript" src="Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui.js"></script>
    <script type="text/javascript">
        function clink() {
            if (document.getElementById('alogin').innerHTML == "Login") {
                document.getElementById('alogin').innerHTML = 'Logout';
            }
            else {
                document.getElementById('alogin').innerHTML = 'Login';
                $('#landingCopy').hide();
                $('#calcs').hide();
                $('#plantHeader').hide();
                $('#divPlantInfo').hide();
            }
        }

        function login() {
            $('#outer1').slideToggle().find('.inner div').fadeToggle();
            $('#landingCopy').hide();
            //$('#calcs').show();
            $('#plantHeader').show();
            //$('#divPlantInfo').show();
            $('#runCalc').hide();
        }

        function selectPlant() {
            //$('#divddlCalc').show();
            if ($('#ddlPlant').val() != "") {
                $('#divPlantInfo').show();
                if ($('#menu1').is(":visible")) {
                    $('#menu1').hide();
                }
            }
            else {
                $('#divPlantInfo').hide();
            }
            $('#calcs').hide();
        }

        function selectCalc() {
            $('#divPlantInfo').show();
            $('#calcs').show();
        }

        //        function calcLightNaphtha() {
        //            var totalPoints = 0;
        //            var totalNulls = 0;
        //            $('.ln').each(function () {
        //                //if (isNaN(parseInt($(this).val(), 10)) || isNaN(parseInt($(this).val().substring($(this).val().length - 1, $(this).val().length)), 10)) {
        //                if (isNaN(parseFloat($(this).val())) || isNaN(parseFloat($(this).val().substring($(this).val().length - 1, $(this).val().length)))) {
        //                    var pattern = /[a-zA-Z\'\"<>;`~@#%&\-_,*+?^=!:${}()|\[\]\/\\]+/g;
        //                    $(this).val($(this).val().replace(pattern, ''));
        //                }
        //                if ($(this).val() != '') {
        //                    //totalPoints += parseInt($(this).val(), 10);
        //                    totalPoints += parseFloat($(this).val());
        //                }
        //                if ($(this).val() == '') {
        //                    totalNulls++;
        //                }
        //            });
        //            if (totalPoints > 0) {
        //                $('#txtLNTotalPercent').val(parseFloat(totalPoints).toFixed(2));
        //                if (totalNulls == 0 && parseFloat(totalPoints).toFixed(2) != 100.00) {
        //                    alert('Total must equal 100%.');
        //                }
        //            }
        //            else {
        //                $('#txtLNTotalPercent').val(0);
        //            }
        //        }

        function calcFeedstocks(section, totalField) {
            var totalPoints = 0;
            var totalNulls = 0;
            $(section).each(function () {
                //$(this).css('text-align', 'right');
                if (isNaN(parseFloat($(this).val())) || isNaN(parseFloat($(this).val().substring($(this).val().length - 1, $(this).val().length)))) {
                    //var pattern = /[a-zA-Z\'\"<>;`~@#%&\-_,*+?^=!:${}()|\[\]\/\\]+/g;
                    var pattern = /([^0-9\.])/g;
                    $(this).val($(this).val().replace(pattern, ''));
                }
                if ($(this).val() != '') {
                    totalPoints += parseFloat($(this).val());
                }
                if ($(this).val() == '') {
                    totalNulls++;
                }
            });
            if (totalPoints > 0) {
                $(totalField).val(parseFloat(totalPoints).toFixed(2));
                //if (totalNulls == 0 && parseFloat(totalPoints).toFixed(2) != 100.00) {
                //    alert('Total must equal 100%.');
                //}
            }
            else {
                $(totalField).val(0);
            }
        }

        function calcProduction(section, totalField) {
            var totalPoints = 0;
            var totalNulls = 0;
            $(section).each(function () {
                if (isNaN(parseFloat($(this).val())) || isNaN(parseFloat($(this).val().substring($(this).val().length - 1, $(this).val().length)))) {
                    //var pattern = /[a-zA-Z\'\"<>;`~@#%&\-_,*+?^=!:${}()|\[\]\/\\]+/g;
                    var pattern = /([^0-9\.])/g;
                    $(this).val($(this).val().replace(pattern, ''));
                }
                if ($(this).val() != '') {
                    totalPoints += parseFloat($(this).val());
                }
                if ($(this).val() == '') {
                    totalNulls++;
                }
            });
            if (totalPoints > 0) {
                $(totalField).val(parseFloat(totalPoints).toFixed(2));
                $('#txtProductionTotal').val(parseFloat(totalPoints).toFixed(2));
                var sub = parseFloat($('#txtProductionSubtotal').val()).toFixed(2);
                var lsub = parseFloat($('#txtProductionLossesSubtotal').val()).toFixed(2);
                if (lsub > 0) {
                    $('#txtProductionTotal').val(parseFloat(sub) + parseFloat(lsub));
                }
            }
            else {
                $(totalField).val(0);
                $('#txtProductionTotal').val(0);
            }
        }

        function calcProductionLosses(section, totalField) {
            var totalPoints = 0;
            var totalNulls = 0;
            $(section).each(function () {
                if (isNaN(parseFloat($(this).val())) || isNaN(parseFloat($(this).val().substring($(this).val().length - 1, $(this).val().length)))) {
                    //var pattern = /[a-zA-Z\'\"<>;`~@#%&\-_,*+?^=!:${}()|\[\]\/\\]+/g;
                    var pattern = /([^0-9\.])/g;
                    $(this).val($(this).val().replace(pattern, ''));
                }
                if ($(this).val() != '') {
                    totalPoints += parseFloat($(this).val());
                }
                if ($(this).val() == '') {
                    totalNulls++;
                }
            });
            if (totalPoints > 0) {
                $(totalField).val(parseFloat(totalPoints).toFixed(2));
                //if (!isNaN(parseFloat($('#txtProductionSubtotal').val()).toFixed(2) + parseFloat($(totalField).val()).toFixed(2))) {
                if (!isNaN(parseFloat($('#txtProductionSubtotal').val()) + parseFloat($(totalField).val()))) {
                    $('#txtProductionTotal').val(parseFloat($('#txtProductionSubtotal').val()) + parseFloat($(totalField).val()));
                    $('#txtProductionTotal').val(parseFloat($('#txtProductionTotal').val()).toFixed(2));
                    //$('#txtProductionTotal').val(parseFloat($('#txtProductionSubtotal').val()).toFixed(2) + parseFloat($(totalField).val()).toFixed(2));
                }
            }
            else {
                $(totalField).val(0);
            }
        }

        function watermark(control, text) {
            if ($(control).val().length > 0) {
                if ($(control).val() == text)
                    $(control).val('');
            }
            else
                $(control).val(text);
        }

        function showToolTip(e) {
            //$('.divToolTip').css('top', (e.pageY) + 'px');
            //$('.divToolTip').css('left', (e.pageX) + 'px');
            $('.divToolTip').css('visibility', 'visible');
            $('.divToolTip').css("position", "absolute");
            $('.divToolTip').css("top", Math.max(0, (($(window).height() - $('.divToolTip').outerHeight()) / 2) + $(window).scrollTop()) + "px");
            $('.divToolTip').css("left", Math.max(0, (($(window).width() - $('.divToolTip').outerWidth()) / 2) + $(window).scrollLeft()) + "px");
            //$('.divToolTip').center();
        }

        function hideToolTip(e) {
            $('.divToolTip').css('visibility', 'hidden');
        }

        function addProduct(e) {
            //if ($('#divProduct1').css('display') == 'none') {
            //    $('#divProduct1').css('display', 'block');
            //}
            //else {
            //    $('#divProduct2').css('display', 'block');
            //}
            if ($('#trProduct1').css('display') == 'none') {
                $('#divProduct1').css('display', 'block');
            }
            else {
                $('#divProduct2').css('display', 'block');
                $('#btnAddProduct').prop('disabled', true);
            }
            $('#divProduct1').css("position", "absolute");
            $('#divProduct1').css("top", Math.max(0, (($(window).height() - $('#divProduct1').outerHeight()) / 2) + $(window).scrollTop()) + "px");
            $('#divProduct1').css("left", Math.max(0, (($(window).width() - $('#divProduct1').outerWidth()) / 2) + $(window).scrollLeft()) + "px");
            $('#divProduct2').css("position", "absolute");
            $('#divProduct2').css("top", Math.max(0, (($(window).height() - $('#divProduct2').outerHeight()) / 2) + $(window).scrollTop()) + "px");
            $('#divProduct2').css("left", Math.max(0, (($(window).width() - $('#divProduct2').outerWidth()) / 2) + $(window).scrollLeft()) + "px");
            //$('#divProduct1').center();
            //$('#divProduct2').center();
        }

        function addProductDesc(product) {
            if (product == 'product1') {
                $('#divProduct1Desc').css('display', 'none');
                $('#tblProduct1').css('display', 'block');
                $('#lblProduct1Desc').text($('#txtProduct1Desc').val());
                //$('#lblProduct1Desc').val(($('#txtProduct1Desc').val()));

            }
            else {
                $('#divProduct2Desc').css('display', 'none');
                $('#tblProduct2').css('display', 'block');
                $('#lblProduct2Desc').text($('#txtProduct2Desc').val());
                //$('#lblProduct2Desc').val(($('#txtProduct2Desc').val()));
            }

        }

        function addProductSave(product) {
            if (product == 'product1') {
                $('#divProduct1').css('display', 'none');
                $('#lblProduct1Name').text($('#txtProduct1Desc').val());
                //$('#lblProduct1Name').val(($('#txtProduct1Desc').val()));
                $('#lblProduct1Name').attr('onclick', '$(\'#divProduct1\').css(\'display\', \'block\');');
                $('#trProduct1').css('display', 'block');
            }
            else {
                $('#divProduct2').css('display', 'none');
                $('#lblProduct2Name').text($('#txtProduct2Desc').val());
                //$('#lblProduct2Name').val(($('#txtProduct2Desc').val()));
                $('#lblProduct2Name').attr('onclick', '$(\'#divProduct2\').css(\'display\', \'block\');');
                $('#trProduct2').css('display', 'block');
            }
        }

        function cancelAddProduct(product) {
            if (product == 'product1') {
                $('#divProduct1').css('display', 'none');
            }
            else {
                $('#divProduct2').css('display', 'none');
                $('#btnAddProduct').prop('disabled', false);
            }
        }

        function expandCollapseSections(section, innerSection, link) {
            //$('#outerPlantFacilities').slideToggle().find('.innerPlantFacilities div').fadeToggle(); aPlantFacilities.innerHTML = 'moo';
            $(section).slideToggle().find(innerSection).fadeToggle();
            if ($(link).text() == '[+]') {
                $(link).text('[-]');
            }
            else if ($(link).text() == '[-]') {
                $(link).text('[+]');
            }
            if (innerSection != '.innerGeneralInformation div' && $(link).text() == '[-]') {
                $('#innerGeneralInformation').css('width', '933px');
            }
            if (innerSection != '.innerGeneralInformation div' && $(link).text() == '[+]') {
                $('#innerGeneralInformation').css('width', '934px');
            }
        }

        function calcDates() {
            //            var date = new date($('#txtStartDate').val());
            //            var today = new date($('#txtEndDate').val());
            //            var diff = Math.floor((today - date) / (1000 * 60 * 60 * 24));
            //            alert(diff);
            var diff = Math.floor((Date.parse($('#txtEndDate').val()) - Date.parse($('#txtStartDate').val())) / 86400000);
            if (!isNaN(diff)) {
                $('#txtNumOfDays').val(diff + 1);
            }
        }

        function restrictNumeric(e) {
            var pattern = /([^0-9\.])/g;
            $(e).val($(e).val().replace(pattern, ''));
            $(e).css('text-align', 'right');
        }

        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
            return this;
        }

        window.onload = function () {
            //$('#calcs').hide(); //$('#divddlCalc').hide(); //$('#outerPlantFacilities').slideToggle(); $('#outerLightFeedstocks').slideToggle(); $('#outerLiquidFeedstocks').slideToggle(); $('#outerSupplemental').slideToggle(); $('#outerProducts').slideToggle(); $('#outerOtherProducts').slideToggle();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () { toggleSections(); });
        }

        function toggleSections() {
            $('#outerPlantFacilities').slideToggle().hide(); $('#outerLightFeedstocks').slideToggle().hide(); $('#outerLiquidFeedstocks').slideToggle().hide(); $('#outerSupplemental').slideToggle().hide(); $('#outerProducts').slideToggle().hide(); $('#outerOtherProducts').slideToggle().hide();
        }

        function toggleSectionsSubmit() {
            $('#outerGeneralInformation').show(); $('#outerPlantFacilities').show(); $('#outerLightFeedstocks').show(); $('#outerLiquidFeedstocks').show(); $('#outerSupplemental').show(); $('#outerProducts').show(); $('#outerOtherProducts').show();
        }

        function highlightAddData(e) {
            if (e == 'txtNOUFFrationators') {
                if ($('#'+e).val() != '0' && $('#'+e).val() != '') {
                    $('#txtAddDataFFractionator').css('background-color', '#F3F781');
                    $('#ddlFFractionatorFeedstock').css('background-color', '#F3F781');
                    $('#txtFFTonsKMT').css('background-color', '#F3F781');
                    $('#txtFFTonsSG').css('background-color', '#F3F781');
                }
                else {
                    $('#txtAddDataFFractionator').css('background-color', '');
                    $('#ddlFFractionatorFeedstock').css('background-color', '');
                    $('#txtFFTonsKMT').css('background-color', '');
                    $('#txtFFTonsSG').css('background-color', '');
                }
            }

            
            if (e == 'txtNOUHighPBoilers') {
                if ($('#'+e).val() != '0' && $('#'+e).val() != '') {
                    $('#txtAddDataHighPBoiler').css('background-color', '#F3F781');
                }
                else {
                    $('#txtAddDataHighPBoiler').css('background-color', '');
                }
            }

            if (e == 'txtNOULowPBoilers') {
                if ($('#'+e).val() != '0' && $('#'+e).val() != '') {
                    $('#txtAddDataLowPBoiler').css('background-color', '#F3F781');
                }
                else {
                    $('#txtAddDataLowPBoiler').css('background-color', '');
                }
            }

            if (e == 'txtNOUEPGenerators') {
                if ($('#'+e).val() != '0' && $('#'+e).val() != '') {
                    $('#txtAddDataEPGenerator').css('background-color', '#F3F781');
                }
                else {
                    $('#txtAddDataEPGenerator').css('background-color', '');
                }
            }

            if (e == 'txtNOUPGHydrotreater') {
                if ($('#'+e).val() != '0' && $('#'+e).val() != '') {
                    $('#txtAddDataPGHydrotreater').css('background-color', '#F3F781');
                    $('#ddlPGhydrotreaterType').css('background-color', '#F3F781');
                    $('#txtPGhydrotreaterPygas').css('background-color', '#F3F781');
                    $('#txtPGHydrotreaterSG').css('background-color', '#F3F781');
                }
                else {
                    $('#txtAddDataPGHydrotreater').css('background-color', '');
                    $('#ddlPGhydrotreaterType').css('background-color', '');
                    $('#txtPGhydrotreaterPygas').css('background-color', '');
                    $('#txtPGHydrotreaterSG').css('background-color', '');
                }
            }
            
        }

        function disableButton(e) {
            $(e).prop("value", "Please wait...");
            setTimeout(function () { $(e).prop("disabled", true); }, 1000);
        }

       

    </script>
    
<script type="text/javascript">
    $(document).on("click", "#lblProduct2Name", function (e) {
        var xOffset = e.pageX;
        var yOffset = e.pageY;
        //alert("xOffset is " + xOffset + "px and " + "yOffset is " + yOffset + "px");
        $('#divProduct2').css('display', 'block');
        $('#divProduct2Desc').css('display', 'none');
        $('#tblProduct2').css('display', 'block');
        $('#divProduct2').css('position', 'absolute');
        $('#divProduct2').css('top', (yOffset - $('#divProduct2').outerHeight())+ 'px');
        $('#divProduct2').css('left', Math.max(0, (($(window).width() - $('#divProduct2').outerWidth()) / 2) + $(window).scrollLeft()) + 'px');
    });
</script>
<script type="text/javascript">
    $(document).on("click", "#lblProduct1Name", function (e) {
        var xOffset = e.pageX;
        var yOffset = e.pageY;
        //alert("xOffset is " + xOffset + "px and " + "yOffset is " + yOffset + "px");
        $('#divProduct1').css('display', 'block');
        $('#divProduct1Desc').css('display', 'none');
        $('#tblProduct1').css('display', 'block');
        $('#divProduct1').css('position', 'absolute');
        $('#divProduct1').css('top', (yOffset - $('#divProduct1').outerHeight()) + 'px');
        $('#divProduct1').css('left', Math.max(0, (($(window).width() - $('#divProduct1').outerWidth()) / 2) + $(window).scrollLeft()) + 'px');
    });
</script>
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:UpdatePanel ID="upnlHeader" runat="server" UpdateMode="Conditional">
<Triggers><asp:AsyncPostBackTrigger ControlID="ddlCalcs" EventName="SelectedIndexChanged" /><asp:AsyncPostBackTrigger ControlID="ddlPlant" EventName="SelectedIndexChanged" /></Triggers>
<ContentTemplate>
<div id="plantHeader" style="background-color: #E6EAED; height: 28px; width: 950px; padding-left: 5px; padding-right: 5px; margin-bottom: 10px; margin-top: 7px;"><div style="margin-top: 4px; float: left;">Plant: <asp:DropDownList ID="ddlPlant" runat="server" OnSelectedIndexChanged="ddlPlant_SelectedIndexChanged" AutoPostBack="true" onchange="selectPlant();"></asp:DropDownList></div><div id="divddlCalc" runat="server" visible="false" style="margin-top: 4px; float: left; margin-left:6px;">Calculation: <asp:DropDownList ID="ddlCalcs" runat="server" OnSelectedIndexChanged="ddlCalcs_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div><div style="float: right; padding-top: 2px;"><asp:Button ID="btnRunCalc" runat="server" OnClick="btnRunCalc_Click" OnClientClick="toggleSectionsSubmit();" Text="Run Calculation" ValidationGroup="vgCalc" /><asp:HiddenField ID="hidSubID" runat="server" /></div><div id="divProgress" runat="server" style="display: none;"><img src="Images/update-progress.gif" alt="" /></div></div>
</ContentTemplate>
</asp:UpdatePanel>
<div style="width: 100%; text-align: center;"><asp:UpdateProgress ID="upLoadCalc" runat="server" AssociatedUpdatePanelID="upnlHeader" DisplayAfter="0"><ProgressTemplate><img src="Images/update-progress.gif" alt="" /></ProgressTemplate></asp:UpdateProgress></div>
</div>

<div style="background-color: #ffffff;">

<asp:UpdatePanel ID="upnlMainContent" runat="server" UpdateMode="Conditional">
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="ddlCalcs" EventName="SelectedIndexChanged" />
</Triggers>
<ContentTemplate>

<div id="calcs" runat="server" visible="false">

<p></p><br />&nbsp;<br /><br /><br /><br /><br /><br />

<%--<ajaxToolkit:ToolkitScriptManager ID="mgr" runat="server"></ajaxToolkit:ToolkitScriptManager>--%>

<div style="width: 960px; text-align: center;"><asp:LinkButton ID="lnkbtnViewResults" runat="server" Text="View Previously Calculated Results" Visible="false"></asp:LinkButton></div>
    
<div style="width: 960px; margin-top: 15px; margin-left: 10px;">
<span style="color: Red; font-weight: bold;">*</span> = Required<br />
<div style="float: left;"><img src="images/leftRound.png" /></div><div class="sectionHeader" style="background: #0380cd; float: left; height: 23px; width: 903px;">General Information</div><div style="float: left; height: 23px; background: #0380cd;"><a id="aGeneralInformation" href="#GeneralInformation" onclick="expandCollapseSections('#outerGeneralInformation','.innerGeneralInformation div', this);" style="color: #FFFFFF; text-decoration: none;">[-]</a></div><div><img src="images/rightRound.png" /></div>
<div id="outerGeneralInformation">
<a name="GeneralInformation"></a>
<div id="innerGeneralInformation" style="border: 1px solid #0380cd; width: 934px; padding-left: 3px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>
<div style="float: left; width: 200px; height: 25px;">Name of Run:</div><div style=" height: 25px;"><asp:TextBox ID="txtNameOfRun" runat="server" /><asp:RequiredFieldValidator ID="rfvNameOfRun" runat="server" ControlToValidate="txtNameOfRun" EnableClientScript="true" ErrorMessage="Name of Run is required" Display="None" ForeColor="Red" ValidationGroup="vgCalc"></asp:RequiredFieldValidator><span style="color: Red; font-weight: bold;">*</span></div>
<div style="float: left; width: 200px; height: 25px;">Comments:</div><div><asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="5" Columns="40"></asp:TextBox></div>
<div style="float: left; width: 200px; height: 25px; margin-top: 3px;">Start Date of Data Entered:</div><div style="height: 25px; float: left; margin-top: 3px;"><asp:TextBox ID="txtStartDate" runat="server" Enabled="false" /></div><div style="margin-top: 4px; height: 25px;"><img id="imgCalendarStartDate" src="images/calendar.png" /><ajaxToolkit:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtStartDate" PopupButtonID="imgCalendarStartDate"></ajaxToolkit:CalendarExtender><asp:RequiredFieldValidator ID="rvfStartDate" runat="server" ControlToValidate="txtStartDate" EnableClientScript="true" ErrorMessage="Start Date is required" Display="None" ForeColor="Red" ValidationGroup="vgCalc"></asp:RequiredFieldValidator><span style="color: Red; font-weight: bold;">*</span></div>
<div style="float: left; width: 200px; height: 25px; margin-top: 3px;">End Date of Data Entered:</div><div style="height: 25px; float: left; margin-top: 3px;"><asp:TextBox ID="txtEndDate" runat="server" Enabled="false" /></div><div style="margin-top: 4px; height: 25px;"><img id="imgCalendarEndDate" src="images/calendar.png" /><ajaxToolkit:CalendarExtender ID="calEndDate" runat="server" TargetControlID="txtEndDate" PopupButtonID="imgCalendarEndDate" OnClientDateSelectionChanged="calcDates"></ajaxToolkit:CalendarExtender><asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtEndDate" EnableClientScript="true" ErrorMessage="Start Date is required" Display="None" ForeColor="Red" ValidationGroup="vgCalc"></asp:RequiredFieldValidator><span style="color: Red; font-weight: bold;">*</span></div>
<div style="float: left; width: 200px; height: 25px;">Number of Days:</div><div style=" height: 25px;"><asp:TextBox ID="txtNumOfDays" runat="server" Enabled="false" /></div>
</td></tr>
</table>
<asp:ValidationSummary ID="vsum" runat="server" DisplayMode="BulletList" EnableClientScript="true" ShowSummary="false" ShowMessageBox="true" ValidationGroup="vgCalc" />
</div>
</div>
</div>

<div style="width: 960px; margin-top: 15px; margin-left: 10px;">
<div style="float: left;"><img src="images/leftRound.png" /></div><div class="sectionHeader" style="background-color: #0380cd; float: left; height: 23px; width: 903px;">Olefin Plant Facilities (OSIM Tables 1-1 and 1-3)</div><div style="float: left; height: 23px; background-color: #0380cd;"><a id="aPlantFacilities" href="#PlantFacilities" onclick="expandCollapseSections('#outerPlantFacilities','.innerPlantFacilities div', this);" style="color: #FFFFFF; text-decoration: none;">[+]</a></div><div><img src="images/rightRound.png" /></div>
<div id="outerPlantFacilities">
<a name="PlantFacilities"></a>
<div id="innerPlantFacilities" style="border: 1px solid #0380cd; width: 933px; padding-left: 3px;">
<table border="0" cellpadding="0" cellspacing="0" width="99%">
<tr><td>
<div style="float:left; width: 48%;">
<div style="float: left; height: 25px; width: 160px;">Number of Trains</div>
<div style="height: 25px;"><asp:TextBox ID="txtTrains" runat="server" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox><asp:RequiredFieldValidator ID="rfvTrains" runat="server" ControlToValidate="txtTrains" EnableClientScript="true" ErrorMessage="Number of Trains is required" Display="None" ForeColor="Red" SetFocusOnError="true" ValidationGroup="vgCalc"></asp:RequiredFieldValidator><asp:RangeValidator ID="rvTrains" runat="server" ControlToValidate="txtTrains" EnableClientScript="true" ErrorMessage="Number of Trains must be > 0 & < 5" Display="None" ForeColor="Red" MinimumValue="1" MaximumValue="5" SetFocusOnError="true" ValidationGroup="vgCalc"></asp:RangeValidator><span style="color: Red; font-weight: bold;">*</span></div>
<div style="float: left; height: 25px; width: 160px;">Feed Class&nbsp;<img src="images/question4.png" onmousedown="showToolTip(this);" alt="Help" /></div><div class="divToolTip" style="padding: 3px; visibility: hidden; position: absolute;"><div style="border-top: 1px solid #000000; border-left: 1px solid #000000; background-color: #0380cd; position: absolute; float: left; height: 23px; width: 536px; color: #FFFFFF; padding-left: 6px;">Feed Class</div><div style="float: right; height: 30px; position: absolute; left: 535px; top: -7px;"><img id="img1" runat="server" src="Images/close.png" alt="" onclick="hideToolTip(this);" style="cursor: pointer;" /></div><div style="width: 530px; padding: 6px; background-color: #ffffff; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px; border: 1px solid #000000; box-shadow: 0 0 30px 5px #000000;"><br clear="all" /><table border="0" cellpadding="2" cellspacing="2"><tr><td>1 (Ethane)</td><td> (>85%)</td></tr><tr><td>2 (LPG Feeds)</td><td> (C2+C3+C4 > 50%; C2<85%)</td></tr><tr><td>3 (Mixed Feeds)</td><td> (>5% gas oil, >10% ethane, propane and naphtha flexibility)</td></tr><tr><td>4 (Naphtha)</td><td> (LIQ. FEEDS > 85%; G.O. < 25% & S.G. OF NAPH < 0.70)</td></tr></table></div></div>
<div style="height: 25px;"><asp:DropDownList ID="ddlFeedClass" runat="server" Enabled="true"></asp:DropDownList></div>
</div>
<div style="float: left; width: 48%;">
<div style="height: 25px; width: 210px;"><b>Stream-Day Capacity MT/d</b></div>
<div style="float:left; height: 25px; width: 170px;">Ethylene</div>
<div style="height: 25px;"><asp:TextBox ID="txtCapacityEthylene" runat="server" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox><asp:RequiredFieldValidator ID="rfvCapacityEthylene" runat="server" ControlToValidate="txtCapacityEthylene" EnableClientScript="true" ErrorMessage="Stream-Day Capacity Ethylene is required" Display="None" ForeColor="Red" SetFocusOnError="true" ValidationGroup="vgCalc"></asp:RequiredFieldValidator><span style="color: Red; font-weight: bold;">*</span></div>
<div style="float: left; height: 25px; width: 170px;">Ethylene plus Propylene</div>
<div style="height: 25px;"><asp:TextBox ID="txtCapacityEthylenePropylene" runat="server" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox><asp:RequiredFieldValidator ID="rfvCapacityEthylenePropylene" runat="server" ControlToValidate="txtCapacityEthylenePropylene" EnableClientScript="true" ErrorMessage="Stream-Day Capacity Ethylene plus Propylene is required" Display="None" ForeColor="Red" SetFocusOnError="true" ValidationGroup="vgCalc"></asp:RequiredFieldValidator><span style="color: Red; font-weight: bold;">*</span></div>
</div>
</td></tr>
</table>
<br />
<b>Utilities and OSBL Facilities Operated and Maintained by Olefin Plant</b>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr><td>&nbsp;</td><td>Number Of Units</td><td>Additional Data</td><td></td></tr>
<tr><td>Feedstock Fractionator</td><td><asp:TextBox ID="txtNOUFFrationators" runat="server" onkeyup="restrictNumeric(this);highlightAddData('txtNOUFFrationators');" style="text-align: right;"></asp:TextBox></td><td><asp:TextBox ID="txtAddDataFFractionator" runat="server" onblur="watermark(this,'k b/sd Feed');" onfocus="watermark(this,'k b/sd Feed');" onkeyup="restrictNumeric(this);" style="text-align: right;">k b/sd Feed</asp:TextBox></td><td>Capacity, k b/sd Feed</td></tr>
<tr><td>Feed Type</td><td></td><td><asp:DropDownList ID="ddlFFractionatorFeedstock" runat="server"></asp:DropDownList></td><td>Feedstock</td></tr>
<tr><td>Tons Processed<td></td></td><td><asp:TextBox ID="txtFFTonsKMT" runat="server" onblur="watermark(this,'k MT');" onfocus="watermark(this,'k MT');" onkeyup="restrictNumeric(this);" style="text-align: right;">k MT</asp:TextBox></td><td>k MT</td></tr>
<tr><td>Feed Specific Gravity<td></td></td><td><asp:TextBox ID="txtFFTonsSG" runat="server" onblur="watermark(this,'SG');" onfocus="watermark(this,'SG');" onkeyup="restrictNumeric(this);" style="text-align: right;">SG</asp:TextBox></td><td>S.G.</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>High-Pressure Fired Steam Boilers</td><td><asp:TextBox ID="txtNOUHighPBoilers" runat="server" onkeyup="restrictNumeric(this);highlightAddData('txtNOUHighPBoilers');" style="text-align: right;"></asp:TextBox></td><td><asp:TextBox ID="txtAddDataHighPBoiler" runat="server" onblur="watermark(this,'k lb/hr');" onfocus="watermark(this,'k lb/hr');" onkeyup="restrictNumeric(this);" style="text-align: right;">k lb/hr</asp:TextBox></td><td>Capacity, k lb/hr</td></tr>
<tr><td>Low-Pressure Fired Steam Boilers</td><td><asp:TextBox ID="txtNOULowPBoilers" runat="server" onkeyup="restrictNumeric(this);highlightAddData('txtNOULowPBoilers');" style="text-align: right;"></asp:TextBox></td><td><asp:TextBox ID="txtAddDataLowPBoiler" runat="server" onblur="watermark(this,'k lb/hr');" onfocus="watermark(this,'k lb/hr');" onkeyup="restrictNumeric(this);" style="text-align: right;">k lb/hr</asp:TextBox></td><td>Capacity, k lb/hr</td></tr>
<tr><td>Electrical Power Generation</td><td><asp:TextBox ID="txtNOUEPGenerators" runat="server" onkeyup="restrictNumeric(this);highlightAddData('txtNOUEPGenerators');" style="text-align: right;"></asp:TextBox></td><td><asp:TextBox ID="txtAddDataEPGenerator" runat="server" onblur="watermark(this,'Generation Capacity, MW');" onfocus="watermark(this,'Generation Capacity, MW');" onkeyup="restrictNumeric(this);" style="text-align: right;">Generation Capacity, MW</asp:TextBox></td><td>Generation Capacity, MW</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>Cryogenic Hydrogen Purification</td><td><asp:TextBox ID="txtNOUCHPurification" runat="server" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox></td></tr>
<tr><td>Pressure Swing Adsorption (PSA) Hydrogen Purification</td><td><asp:TextBox ID="txtNOUPSAPurification" runat="server" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox></td></tr>
<tr><td>Membrane Hydrogen Purification</td><td><asp:TextBox ID="txtNOUMembranePurification" runat="server" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>Pyrolysis Gasoline Hydrotreater</td><td><asp:TextBox ID="txtNOUPGHydrotreater" runat="server" onkeyup="restrictNumeric(this);highlightAddData('txtNOUPGHydrotreater');" style="text-align: right;"></asp:TextBox></td><td><asp:TextBox ID="txtAddDataPGHydrotreater" runat="server" onblur="watermark(this,'k b/sd Feed');" onfocus="watermark(this,'k b/sd Feed');" onkeyup="restrictNumeric(this);" style="text-align: right;">k b/sd Feed</asp:TextBox></td><td>Capacity, k b/sd Feed</td></tr>
<tr><td>Hydrotreater Type</td><td></td><td><asp:DropDownList ID="ddlPGhydrotreaterType" runat="server"></asp:DropDownList></td><td>Type</td></tr>
<tr><td>% of Pygas Processed through Hydrotreater</td><td></td><td><asp:TextBox ID="txtPGhydrotreaterPygas" runat="server" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox><asp:RangeValidator ID="rvPGhydrotreaterPygas" runat="server" ControlToValidate="txtPGhydrotreaterPygas" EnableClientScript="true" ErrorMessage="% of Pygas Processed must be between 0 and 100" Display="None" ForeColor="Red" MinimumValue="0" MaximumValue="100" Enabled="true" Type="Double" SetFocusOnError="true" ValidationGroup="vgCalc"></asp:RangeValidator><span style="color: Red; font-weight: bold; display: none;">*</span></td><td>%</td></tr>
<tr><td>Specific Gravity of Non-Benzene Portion of Hydrotreater Feed</td><td></td><td><asp:TextBox ID="txtPGHydrotreaterSG" runat="server" Text="SG" onblur="watermark(this,'SG');" onfocus="watermark(this,'SG');" onkeyup="restrictNumeric(this);" style="text-align: right;"></asp:TextBox></td><td>S.G.</td></tr>
</table>
<br />
<!--
<b>Additional Information Regarding Feedstock Fractionators and Hydrotreaters</b>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>Actual Tons Processed through Feedstock Fractionator</td></tr>
<tr><td>Pyrolysis Gasoline Hydrotreater Type</td></tr>
<tr><td>% of Pygas Processed through Pyrolysis Gasoline Hydrotreater</td></tr>
<tr><td>Specific Gravity of Non-Benzene Portion of Pyrolysis Gasoline Hydrotreater Feed</td></tr>
</table>
-->
</div>
</div>
</div>

<div style="width: 960px; margin-top: 15px; margin-left: 10px;">
<div style="float: left;"><img src="images/leftRound.png" /></div><div class="sectionHeader" style="background-color: #0380cd; float: left; height: 23px; width: 903px;">Light Feedstocks (OSIM Table 2A-1)</div><div style="float: left; height: 23px; background-color: #0380cd;"><a id="alightfeedstocks" href="#lightfeedstocks" onclick="expandCollapseSections('#outerLightFeedstocks','.innerLightFeedstocks div', this);" style="color: #FFFFFF; text-decoration: none;">[+]</a></div><div><img src="images/rightRound.png" /></div>
<div id="outerLightFeedstocks">
<a name="lightfeedstocks"></a>
<div id="innerLightFeedstocks" style="border: 1px solid #0380cd; width: 933px; padding-left: 3px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td style="vertical-align: bottom;">Composition, wt %</td><td class="tableHeader">Ethane</td><td class="tableHeader">Ethane/Propane Mix</td><td class="tableHeader">Propane</td><td class="tableHeader">LPG</td><td class="tableHeader">Butane and Other C<sub>4</sub>s</td><td class="tableHeader" style="border-right: 0px solid #000000;">Other Light Feedstock<br /><asp:TextBox ID="txtLightFeedOther" runat="server" style="width: 120px;" onblur="watermark(this,'DESCRIBE');" onfocus="watermark(this,'DESCRIBE');">DESCRIBE</asp:TextBox></td></tr>
<tr><td style="background-color: #d9edf9;">Methane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneMethane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneMethane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneMethane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGMethane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sMethane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherMethane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">Ethane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneEthane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneEthane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneEthane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGEthane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sEthane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherEthane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">Ethylene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneEthylene" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneEthylene" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneEthylene" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGEthylene" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sEthylene" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherEthylene" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">Propane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropanePropane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropanePropane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGPropane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sPropane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherPropane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">Propylene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropylene" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropanePropylene" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropanePropylene" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGPropylene" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sPropylene" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherPropylene" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">n-Butane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneNButane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneNButane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneNButane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGNButane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sNButane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherNButane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">i-Butane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneIButane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneIButane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneIButane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGIButane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sIButane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherIButane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">Isobutylene & 2-Butane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneIsobutylene" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneIsobutylene" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneIsobutylene" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGIsobutylene" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sIsobutylene" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherIsobutylene" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">1-Butane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthane1Butane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropane1Butane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropane1Butane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPG1Butane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4s1Butane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOther1Butane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">Butadiene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneButadiene" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneButadiene" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneButadiene" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGButadiene" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sButadiene" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherButadiene" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">n-Pentane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneNPentane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneNPentane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneNPentane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGNPentane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sNPentane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherNPentane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">i-Pentane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneIPentane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneIPentane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneIPentane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGIPentane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sIPentane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherIPentane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">n-Hexane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneNHexane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneNHexane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneNHexane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGNHexane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sNHexane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherNHexane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">i-Hexane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneIHexane" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneIHexane" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneIHexane" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGIHexane" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sIHexane" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherIHexane" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">C<sub>7</sub>s</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneC7" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneC7" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneC7" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGC7" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sC7" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txOtherC7" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">C<sub>8</sub>s</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneC8" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneC8" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneC8" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGC8" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sC8" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherC8" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">CO & CO<sub>2</sub></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneCOCO2" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneCOCO2" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneCOCO2" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGCOCO2" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sCOCO2" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherCOCO2" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd;">Hydrogen</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneHydrogen" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneHydrogen" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneHydrogen" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGHydrogen" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sHydrogen" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherHydrogen" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9;">Sulfur</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanSulfur" runat="server" style="margin: 1px; width: 121px;" class="ethane" onkeyup="calcFeedstocks('.ethane','#txtEthaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneSulfur" runat="server" style="margin: 1px; width: 121px;" class="ethanepropane" onkeyup="calcFeedstocks('.ethanepropane','#txtEthanePropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneSulfur" runat="server" style="margin: 1px; width: 121px;" class="propane" onkeyup="calcFeedstocks('.propane','#txtPropaneTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGSulfur" runat="server" style="margin: 1px; width: 121px;" class="LPG" onkeyup="calcFeedstocks('.LPG','#txtLPGTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sSulfur" runat="server" style="margin: 1px; width: 121px;" class="ButaneC4s" onkeyup="calcFeedstocks('.ButaneC4s','#txtButaneC4sTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherSulfur" runat="server" style="margin: 1px; width: 121px;" class="OtherLightFeed" onkeyup="calcFeedstocks('.OtherLightFeed','#txtOtherTotalPercent');" /></td></tr>
<tr><td style="white-space: nowrap; background-color: #f6fafd; font-weight: bold;">Total (must equal 100%)</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthaneTotalPercent" runat="server" Enabled="false" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtEthanePropaneTotalPercent" runat="server" Enabled="false" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPropaneTotalPercent" runat="server" Enabled="false" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLPGTotalPercent" runat="server" Enabled="false" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtButaneC4sTotalPercent" runat="server" Enabled="false" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtOtherTotalPercent" runat="server" Enabled="false" style="margin: 1px; width: 121px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9; font-weight: bold;">Total Quantity (in k MT)</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthaneTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtEthanePropaneTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPropaneTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLPGTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtButaneC4sTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; width: 121px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOtherTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; width: 121px; text-align: right;" /></td></tr>
</table>
</div>
</div>

<br />

<div style="float: left;"><img src="images/leftRound.png" /></div><div class="sectionHeader" style="background-color: #0380cd; float: left; height: 23px; width: 903px;">Liquid Feedstocks (OSIM Table 2A-2)</div><div style="float: left; height: 23px; background-color: #0380cd;"><a id="aliquidfeedstocks" href="#liquidfeedstocks" onclick="expandCollapseSections('#outerLiquidFeedstocks','.innerLiquidFeedstocks div', this);" style="color: #FFFFFF; text-decoration: none;">[+]</a></div><div><img src="images/rightRound.png" /></div>
<div id="outerLiquidFeedstocks">
<a name="liquidfeedstocks"></a>
<div id="innterLiquidFeedstocks" style="border: 1px solid #0380cd; width: 933px; overflow-x: scroll; padding-left: 3px;">
<table border="0" cellpadding="0" cellspacing="0" width="200%">
<tr><td style="white-space: nowrap; vertical-align: bottom;">Composition, wt %</td><td class="tableHeaderSingleRow">Light Naphtha (mostly C<sub>5</sub> & C<sub>6</sub>)</td><td class="tableHeaderSingleRow">Full-Range Naphtha</td><td class="tableHeaderSingleRow">Heavy Naphtha (mostly C<sub>6</sub>+)</td><td class="tableHeaderSingleRow">Raffinates from Aromatics Extraction</td><td class="tableHeaderSingleRow">Heavy Natural Gas Liquids</td><td class="tableHeaderSingleRow">Field Condensate</td><td class="tableHeaderSingleRow">Diesel & Gas Oil with EP <390°C</td><td class="tableHeaderSingleRow">Heavy Feeds, Vacuum Gas Oil with EP >390°C</td><td class="tableHeaderSingleRow">Severely Hydrotreated Gas Oil</td><td class="tableHeaderSingleRow">Other Liquid Feedstock<br /><asp:TextBox ID="txtOLF1" runat="server" onblur="watermark(this,'DESCRIBE');" onfocus="watermark(this,'DESCRIBE');">DESCRIBE</asp:TextBox></td><td class="tableHeaderSingleRow">Other Liquid Feedstock<br /><asp:TextBox ID="txtOLF2" runat="server" onblur="watermark(this,'DESCRIBE');" onfocus="watermark(this,'DESCRIBE');">DESCRIBE</asp:TextBox></td><td class="tableHeaderSingleRow">Other Liquid Feedstock<br /><asp:TextBox ID="txtOLF3" runat="server" onblur="watermark(this,'DESCRIBE');" onfocus="watermark(this,'DESCRIBE');">DESCRIBE</asp:TextBox></td></tr>
<tr><td style="background-color: #d9edf9; white-space: nowrap;">Specific Gravity at 15 &deg;C</td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtDGOSG" runat="server" style="margin: 1px; text-align: right;" class="dgo" onkeyup="calcFeedstocks('.dgo','#txtDGOTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHFSG" runat="server" style="margin: 1px; text-align: right;" class="hf" onkeyup="calcFeedstocks('.hf','#txtHFTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtSHGOSG" runat="server" style="margin: 1px; text-align: right;" class="shgo" onkeyup="calcFeedstocks('.shgo','#txtSHGOTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1SG" runat="server" style="margin: 1px; text-align: right;" onkeyup="restrictNumeric(this);" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2SG" runat="server" style="margin: 1px; text-align: right;" onkeyup="restrictNumeric(this);" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3SG" runat="server" style="margin: 1px; text-align: right;" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #f6fafd; white-space: nowrap;">n-Paraffins</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLNNP" runat="server" style="margin: 1px; text-align: right;" class="ln" onkeyup="calcFeedstocks('.ln','#txtLNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtFRNNP" runat="server" style="margin: 1px; text-align: right;" class="frn" onkeyup="calcFeedstocks('.frn','#txtFRNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNNP" runat="server" style="margin: 1px; text-align: right;" class="hn" onkeyup="calcFeedstocks('.hn','#txtHNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtRAENP" runat="server" style="margin: 1px; text-align: right;" class="rae" onkeyup="calcFeedstocks('.rae','#txtRAETotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNGLNP" runat="server" style="margin: 1px; text-align: right;" class="hngl" onkeyup="calcFeedstocks('.hngl','#txtHNGLTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFCNP" runat="server" style="margin: 1px; text-align: right;" class="fc" onkeyup="calcFeedstocks('.fc','#txtFCTotalPercent');" /></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1NP" runat="server" style="margin: 1px; text-align: right;" class="olf1" onkeyup="calcFeedstocks('.olf1','#txtOLF1TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2NP" runat="server" style="margin: 1px; text-align: right;" class="olf2" onkeyup="calcFeedstocks('.olf2','#txtOLF2TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3NP" runat="server" style="margin: 1px; text-align: right;" class="olf3" onkeyup="calcFeedstocks('.olf3','#txtOLF3TotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9; white-space: nowrap;">Iso-Paraffins</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLNIP" runat="server" style="margin: 1px; text-align: right;" class="ln" onkeyup="calcFeedstocks('.ln','#txtLNTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFRNIP" runat="server" style="margin: 1px; text-align: right;" class="frn" onkeyup="calcFeedstocks('.frn','#txtFRNTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHNIP" runat="server" style="margin: 1px; text-align: right;" class="hn" onkeyup="calcFeedstocks('.hn','#txtHNTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtRAEIP" runat="server" style="margin: 1px; text-align: right;" class="rae" onkeyup="calcFeedstocks('.rae','#txtRAETotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHNGLIP" runat="server" style="margin: 1px; text-align: right;" class="hngl" onkeyup="calcFeedstocks('.hngl','#txtHNGLTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFCIP" runat="server" style="margin: 1px; text-align: right;" class="fc" onkeyup="calcFeedstocks('.fc','#txtFCTotalPercent');" /></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1IP" runat="server" style="margin: 1px; text-align: right;" class="olf1" onkeyup="calcFeedstocks('.olf1','#txtOLF1TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2IP" runat="server" style="margin: 1px; text-align: right;" class="olf2" onkeyup="calcFeedstocks('.olf2','#txtOLF2TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3IP" runat="server" style="margin: 1px; text-align: right;" class="olf3" onkeyup="calcFeedstocks('.olf3','#txtOLF3TotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd; white-space: nowrap;">Naphthenes</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLNNA" runat="server" style="margin: 1px; text-align: right;" class="ln" onkeyup="calcFeedstocks('.ln','#txtLNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtFRNNA" runat="server" style="margin: 1px; text-align: right;" class="frn" onkeyup="calcFeedstocks('.frn','#txtFRNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNNA" runat="server" style="margin: 1px; text-align: right;" class="hn" onkeyup="calcFeedstocks('.hn','#txtHNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtRAENA" runat="server" style="margin: 1px; text-align: right;" class="rae" onkeyup="calcFeedstocks('.rae','#txtRAETotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNGLNA" runat="server" style="margin: 1px; text-align: right;" class="hngl" onkeyup="calcFeedstocks('.hngl','#txtHNGLTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFCNA" runat="server" style="margin: 1px; text-align: right;" class="fc" onkeyup="calcFeedstocks('.fc','#txtFCTotalPercent');" /></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1NA" runat="server" style="margin: 1px; text-align: right;" class="olf1" onkeyup="calcFeedstocks('.olf1','#txtOLF1TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2NA" runat="server" style="margin: 1px; text-align: right;" class="olf2" onkeyup="calcFeedstocks('.olf2','#txtOLF2TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3NA" runat="server" style="margin: 1px; text-align: right;" class="olf3" onkeyup="calcFeedstocks('.olf3','#txtOLF3TotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9; white-space: nowrap;">Olefins</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLNOL" runat="server" style="margin: 1px; text-align: right;" class="ln" onkeyup="calcFeedstocks('.ln','#txtLNTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFRNOL" runat="server" style="margin: 1px; text-align: right;" class="frn" onkeyup="calcFeedstocks('.frn','#txtFRNTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHNOL" runat="server" style="margin: 1px; text-align: right;" class="hn" onkeyup="calcFeedstocks('.hn','#txtHNTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtRAEOL" runat="server" style="margin: 1px; text-align: right;" class="rae" onkeyup="calcFeedstocks('.rae','#txtRAETotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHNGLOL" runat="server" style="margin: 1px; text-align: right;" class="hngl" onkeyup="calcFeedstocks('.hngl','#txtHNGLTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFCOL" runat="server" style="margin: 1px; text-align: right;" class="fc" onkeyup="calcFeedstocks('.fc','#txtFCTotalPercent');" /></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1OL" runat="server" style="margin: 1px; text-align: right;" class="olf1" onkeyup="calcFeedstocks('.olf1','#txtOLF1TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2OL" runat="server" style="margin: 1px; text-align: right;" class="olf2" onkeyup="calcFeedstocks('.olf2','#txtOLF2TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3OL" runat="server" style="margin: 1px; text-align: right;" class="olf3" onkeyup="calcFeedstocks('.olf3','#txtOLF3TotalPercent');" /></td></tr>
<tr><td style="background-color: #f6fafd; white-space: nowrap;">Total Aromatics</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLNTA" runat="server" style="margin: 1px; text-align: right;" class="ln" onkeyup="calcFeedstocks('.ln','#txtLNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtFRNTA" runat="server" style="margin: 1px; text-align: right;" class="frn" onkeyup="calcFeedstocks('.frn','#txtFRNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNTA" runat="server" style="margin: 1px; text-align: right;" class="hn" onkeyup="calcFeedstocks('.hn','#txtHNTotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtRAWTA" runat="server" style="margin: 1px; text-align: right;" class="rae" onkeyup="calcFeedstocks('.rae','#txtRAETotalPercent');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNGLTA" runat="server" style="margin: 1px; text-align: right;" class="hngl" onkeyup="calcFeedstocks('.hngl','#txtHNGLTotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFCTA" runat="server" style="margin: 1px; text-align: right;" class="fc" onkeyup="calcFeedstocks('.fc','#txtFCTotalPercent');" /></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1TA" runat="server" style="margin: 1px; text-align: right;" class="olf1" onkeyup="calcFeedstocks('.olf1','#txtOLF1TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2TA" runat="server" style="margin: 1px; text-align: right;" class="olf2" onkeyup="calcFeedstocks('.olf2','#txtOLF2TotalPercent');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3TA" runat="server" style="margin: 1px; text-align: right;" class="olf3" onkeyup="calcFeedstocks('.olf3','#txtOLF3TotalPercent');" /></td></tr>
<tr><td style="background-color: #d9edf9; white-space: nowrap; font-weight: bold;">Total (must equal 100%)</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtLNTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFRNTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHNTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtRAETotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHNGLTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFCTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtDGOTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHFTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtSHGOTotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1TotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2TotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3TotalPercent" runat="server" Enabled="false" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd; white-space: nowrap; font-weight: bold;">Total Quantity (in k MT)</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtLNTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtFRNTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtRAETotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtHNGLTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtFCTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtDGOTotal" onkeyup="restrictNumeric(this);" runat="server" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtHATotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtSGOTotal" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF1Total" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF2Total" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtOLF3Total" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>   
</table>
</div>
</div>

<br />

<div style="float: left;"><img src="images/leftRound.png" /></div><div class="sectionHeader" style="background-color: #0380cd; float: left; height: 23px; width: 903px;">Supplemental Feeds (OSIM Table 2C)</div><div style="float: left; height: 23px; background-color: #0380cd;"><a id="asupplementalfeeds" href="#supplementalfeeds" onclick="expandCollapseSections('#outerSupplemental','.innerSupplemental div', this);" style="color: #FFFFFF; text-decoration: none;">[+]</a></div><div><img src="images/rightRound.png" /></div>
<div id="outerSupplemental">
<a name="supplementalfeeds"></a>
<div id="innerSupplemental" style="border: 1px solid #0380cd; width: 933px; padding-left: 3px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td></td><td class="tableHeader">Quantity of Feedstock Input, k MT</td><td class="tableHeader">Estimated Percent Recovery</td></tr>
<tr><td style="background-color: #E6EAED; font-weight: bold; white-space: nowrap;">Chemicals Content of Concentrated Petrochemicals Streams</td><td class="tableHeader2ndRow"></td><td class="tableHeader2ndRow"></td></tr>
<tr><td style="background-color: #d9edf9;">Ethylene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtCCCPSQuantityEthylene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtCCCPSRecoveryEthylene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Propylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtCCCPSQuantityPropylene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtCCCPSRecoveryPropylene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Butadiene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtCCCPSQuantityButadiene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtCCCPSRecoveryButadiene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Benzene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtCCCPSQuantityBenzene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtCCCPSRecoveryBenzene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td colspan="3" style="background-color: #E6EAED; font-weight: bold;">Refinery Off-Gas</td></tr>
<tr><td style="background-color: #d9edf9;">Hydrogen</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtROGQuantityHydrogen" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Methane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtROGQuantityMethane" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Ethane*</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtROGQuantityEthane" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Ethylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtROGQuantityEthylene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtROGRecoveryEthylene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Propane*</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtROGQuantityPropane" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Propylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtROGQuantityPropylene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtROGRecoveryPropylene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">C<sub>4</sub> & Heavier</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtROGQuantityC4" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">CO, CO<sub>2</sub>, & Inerts</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtROGQuantityCOCO2" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td colspan="3" style="background-color: #E6EAED; font-weight: bold;">Other Dilute Streams & Other Components in Concentrated Streams</td></tr>
<tr><td style="background-color: #d9edf9;">Hydrogen</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSQuantityHydrogen" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Methane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSQuantityMethane" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Ethane*</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSQuantityEthane" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Ethylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSQuantityEthylene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSRecoveryEthylene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Propane*</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSQuantityPropane" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Propylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSQuantityPropylene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSRecoveryPropylene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Butane*</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSQuantityButane" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Butylenes</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSQuantityButylenes" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Butadiene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSQuantityButadiene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSRecoveryButadiene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Benzene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSQuantityBenzene" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSRecoveryBenzene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Other Gasoline Components</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSQuantityOGC" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Compressor Wash Oil</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSQuantityCWO" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Other Gas Oil or Fuel Oil</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtODSQuantityOGO" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Other</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtODSQuantityInertN2" runat="server" style="margin: 1px; text-align: right;" class="supQuantity" onkeyup="calcFeedstocks('.supQuantity','#txtSupplementalFeedsTotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9; font-weight: bold;">Total</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtSupplementalFeedsTotal" runat="server" Enabled="false" style="text-align: right;"></asp:TextBox></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td colspan="3" style="background-color: #E6EAED; font-weight: bold;">*Estimate the weight percent of supplemental ethane, propane, and butane that are separated for recycle feed to pyrolysis furnaces.</td></tr>
<tr><td style="background-color: #d9edf9;">Ethane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtRecycleFeedEthane" runat="server" Text="wt %" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt %');" onfocus="watermark(this,'wt %');" onkeyup="restrictNumeric(this);" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Propane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtRecycleFeedPropane" runat="server" Text="wt %" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt %');" onfocus="watermark(this,'wt %');" onkeyup="restrictNumeric(this);" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Butane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtRecycleFeedButane" runat="server" Text="wt %" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt %');" onfocus="watermark(this,'wt %');" onkeyup="restrictNumeric(this);" /></td><td style="background-color: #d9edf9;"></td></tr>
</table>
</div>
</div>

<br />

<div style="float: left;"><img src="images/leftRound.png" /></div><div class="sectionHeader" style="background-color: #0380cd; float: left; height: 23px; width: 903px;">Olefin Plant Production (OSIM Table 3)</div><div style="float: left; height: 23px; background-color: #0380cd;"><a id="aproducts" href="#products" onclick="expandCollapseSections('#outerProducts','.innerProducts div', this);" style="color: #FFFFFF; text-decoration: none;">[+]</a></div><div><img src="images/rightRound.png" /></div>
<div id="outerProducts">
<a name="products"></a>
<div id="innerProducts" style="border: 1px solid #0380cd; width: 933px; padding-left: 3px;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td><b>Products</b></td><td class="tableHeader">Production, k MT</td><td class="tableHeader">Typical Quality Data</td></tr>
<tr><td style="background-color: #d9edf9;">Hydrogen-Rich Gas</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionHydrogenRG" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPriceQualityHydrogenRG" runat="server" Text="mol % Hydrogen" style="margin: 1px; text-align: right;" onblur="watermark(this,'mol % Hydrogen');" onfocus="watermark(this,'mol % Hydrogen');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #f6fafd;">Fuel Gas Sales or Transfers</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionFuelGas" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPriceQualityFuelGas" runat="server" Text="mol % Methane" style="margin: 1px; text-align: right;" onblur="watermark(this,'mol % Methane');" onfocus="watermark(this,'mol % Methane');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #d9edf9;">Acetylene Product</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionAcetylene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Polymer-Grade Ethylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionPGEthylene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPriceQualityPGEthylene" runat="server" Text="wt % Ethylene" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt % Ethylene');" onfocus="watermark(this,'wt % Ethylene');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #d9edf9;">Chemical-Grade Ethylene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionCGEthylene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPriceQualityCGEthylene" runat="server" Text="wt % Ethylene" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt % Ethylene');" onfocus="watermark(this,'wt % Ethylene');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #f6fafd;">Polymer-Grade Propylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionPGPropylene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPriceQualityPGPropylene" runat="server" Text="wt % Propylene" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt % Propylene');" onfocus="watermark(this,'wt % Propylene');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #d9edf9;">Chemical-Grade Propylene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionCGPropylene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPriceQualityCGPropylene" runat="server" Text="wt % Propylene" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt % Propylene');" onfocus="watermark(this,'wt % Propylene');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #f6fafd;">Refinery-Grade Propylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionRGPropylene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPriceQualityRGPropylene" runat="server" Text="wt % Propylene" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt % Propylene');" onfocus="watermark(this,'wt % Propylene');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td style="background-color: #d9edf9;">Propane (Residue C<sub>3</sub>s)</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionRGPropane" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPriceQualityRGPropane" runat="server" Text="wt % Propane" style="margin: 1px; text-align: right;" onblur="watermark(this,'wt % Propane');" onfocus="watermark(this,'wt % Propane');" onkeyup="restrictNumeric(this);" /></td></tr>
<tr><td colspan="3" style="background-color: #E6EAED;">Mixed C<sub>4</sub> Product</td></tr>
<tr><td style="background-color: #d9edf9;">&nbsp;&nbsp;&nbsp;Butadiene Quantity</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionButadiene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">&nbsp;&nbsp;&nbsp;Isobutylene Quantity</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionIsobutylene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">&nbsp;&nbsp;&nbsp;Other</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionOtherC4" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td colspan="3" style="background-color: #E6EAED;">Pyrolysis Gasoline</td></tr>
<tr><td style="background-color: #d9edf9;">&nbsp;&nbsp;&nbsp;Benzene Quantity </td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionBenzene" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">&nbsp;&nbsp;&nbsp;Other Pyrolysis Gasoline Components</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionOtherPGasoline" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Pyrolysis Gas Oil</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionPGasOil" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Pyrolysis Fuel Oil</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionPFuelOil" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Acid Gas</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionAcidGas" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #E6EAED;">Olefins Plant-Produced Fuel Consumed in Plant</td><td style="background-color: #E6EAED;"><asp:TextBox ID="txtProductionFuelConsumedInPlant" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #E6EAED;"></td></tr>
<tr><td style="background-color: #d9edf9;">&nbsp;&nbsp;&nbsp;PPFC H<sub>2</sub> Content, wt %</td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPriceQualityPPFCH2" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">&nbsp;&nbsp;&nbsp;PPFC CH<sub>4</sub> Content, wt %</td><td style="background-color: #f6fafd;"></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPriceQualityPPFCCH4" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">&nbsp;&nbsp;&nbsp;PPFC Ethylene Content, wt %</td><td style="background-color: #d9edf9;"></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtPriceQualityPPFCEthylene" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">&nbsp;&nbsp;&nbsp;PPFC Other Fuel (Non-Fuel-Gas) Content, wt %</td><td style="background-color: #f6fafd;"></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPriceQualityPPFCEOtherFuel" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">&nbsp;&nbsp;&nbsp;PPFC Inert Content, wt %</td><td style="background-color: #f6fafd;"></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtPriceQualityPPFCEInerts" runat="server" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td></tr>
<tr><td style="background-color: #E6EAED;">Other Products</td><td style="background-color: #E6EAED;"></td><td style="background-color: #E6EAED;"></td></tr>
<tr><td colspan="3" style="background-color: #E6EAED;"><input id="btnAddProduct" runat="server" type="button" onclick="addProduct(this);" value="Add Product" /></td></tr>
<tr id="trProduct1" runat="server" style="display: none;"><td style="background-color: #d9edf9;"><asp:Label ID="lblProduct1Name" runat="server" style="text-decoration: underline; cursor: pointer;" /></td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionOther1" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr id="trProduct2" runat="server" style="display: none;"><td style="background-color: #f6fafd;"><asp:Label ID="lblProduct2Name" runat="server" style="text-decoration: underline; cursor: pointer;" /></td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionOther2" runat="server" style="margin: 1px; text-align: right;" class="production" onkeyup="calcProduction('.production','#txtProductionSubtotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9; font-weight: bold;">Subtotal Products</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionSubtotal" runat="server" Enabled="false" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Flare & Vent Losses</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionLossesFlareAndVent" runat="server" style="margin: 1px; text-align: right;" class="productionlosses" onkeyup="calcProductionLosses('.productionlosses','#txtProductionLossesSubtotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9;">Other Losses</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionLossesOther" runat="server" style="margin: 1px; text-align: right;" class="productionlosses" onkeyup="calcProductionLosses('.productionlosses','#txtProductionLossesSubtotal');" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd;">Measurement Losses</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionLossesMeasurement" runat="server" style="margin: 1px; text-align: right;" class="productionlosses" onkeyup="calcProductionLosses('.productionlosses','#txtProductionLossesSubtotal');" /></td><td style="background-color: #f6fafd;"></td></tr>
<tr><td style="background-color: #d9edf9; font-weight: bold;">Subtotal Losses</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProductionLossesSubtotal" runat="server" Enabled="false" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #d9edf9;"></td></tr>
<tr><td style="background-color: #f6fafd; font-weight: bold;">Total</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProductionTotal" runat="server" Enabled="false" onkeyup="restrictNumeric(this);" style="margin: 1px; text-align: right;" /></td><td style="background-color: #f6fafd;"></td></tr>
</table>
</div>
</div>

<br />

<div id="divProduct1" style="width: 600px; padding: 6px; display: none; background-color: #ffffff; border: 1px solid #000000; -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px; box-shadow: 0 0 30px 5px #000000; border-radius: 20px;">
<div id="divProduct1Desc">Product Description: <asp:TextBox ID="txtProduct1Desc" runat="server"></asp:TextBox> <input type="button" onclick="addProductDesc('product1');" value="Add" /><input type="button" onclick="cancelAddProduct('product1');" value="Cancel" /></div>
<table id="tblProduct1" border="0" cellpadding="0" cellspacing="0" width="98%" style="display: none;">
<tr><td>Composition, wt %</td><td class="tableHeader"><asp:Label ID="lblProduct1Desc" runat="server" /></td></tr>
<tr><td style="background-color: #d9edf9;">Hydrogen</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1Hydrogen" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Methane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct1Methane" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Acetylene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1Acetylene" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Ethylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct1Ethylene" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Ethane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1Ethane" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Propylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct1Propylene" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Propane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1Propane" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Butadiene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct1Butadiene" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Butylenes</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1Butylenes" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Butane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct1Butane" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Benzene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1Benzene" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Other Pyrogasoline</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct1OtherPyrogasoline" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Pyro Fuel Oil</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1PyroFuelOil" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Inerts</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct1Inerts" runat="server" class="product1" onkeyup="calcFeedstocks('.product1','#txtProduct1Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Total</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct1Total" runat="server" Enabled="false" style="text-align: right;" /></td></tr>
<tr><td></td><td style="text-align: right;"><input type="button" value="Save/Close" onclick="addProductSave('product1');" /></td></tr>
</table>
</div>
<div id="divProduct2" style="width: 600px; padding: 6px; display: none; background-color: #ffffff; border: 1px solid #000000; -moz-border-radius: 20px; -webkit-border-radius: 20px; -khtml-border-radius: 20px; box-shadow: 0 0 30px 5px #000000; border-radius: 20px;">
<div id="divProduct2Desc">Product Description: <asp:TextBox ID="txtProduct2Desc" runat="server"></asp:TextBox> <input type="button" onclick="addProductDesc('product2');" value="Add" /><input type="button" onclick="cancelAddProduct('product2');" value="Cancel" /></div>
<table id="tblProduct2" border="0" cellpadding="0" cellspacing="0" width="98%" style="display: none;">
<tr><td>Composition, wt %</td><td class="tableHeader"><asp:Label ID="lblProduct2Desc" runat="server" /></td></tr>
<tr><td style="background-color: #d9edf9;">Hydrogen</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2Hydrogen" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Methane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct2Methane" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Acetylene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2Acetylene" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Ethylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct2Ethylene" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Ethane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2Ethane" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Propylene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct2Propylene" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Propane</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2Propane" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Butadiene</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct2Butadiene" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Butylenes</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2Butylenes" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Butane</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct2Butane" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Benzene</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2Benzene" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Other Pyrogasoline</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct2OtherPyrogasoline" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Pyro Fuel Oil</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2PyroFuelOil" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #f6fafd;">Inerts</td><td style="background-color: #f6fafd;"><asp:TextBox ID="txtProduct2Inerts" runat="server" class="product2" onkeyup="calcFeedstocks('.product2','#txtProduct2Total');" style="text-align: right;" /></td></tr>
<tr><td style="background-color: #d9edf9;">Total</td><td style="background-color: #d9edf9;"><asp:TextBox ID="txtProduct2Total" runat="server" Enabled="false" style="text-align: right;" /></td></tr>
<tr><td></td><td style="text-align: right;"><input type="button" value="Save/Close" onclick="addProductSave('product2');" /></td></tr>
</table>
</div>

<p><br /></p>




</div>


<!-- content! -->
<p><br /></p>


</div>








<ajaxToolkit:ModalPopupExtender ID="mpeDataChecks" runat="server" DropShadow="true" PopupControlID="pnlDataChecks" TargetControlID="btnHidDataChecks" Y="180" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
<asp:Button ID="btnHidDataChecks" runat="server" style="display: none;" />
<asp:Panel ID="pnlDataChecks" runat="server" style="border: 0px solid #000000; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px;">
<asp:UpdatePanel ID="upnlDataChecks" runat="server" UpdateMode="Conditional">
<Triggers><asp:AsyncPostBackTrigger ControlID="btnRunCalc" EventName="Click" /></Triggers>
<ContentTemplate>
<div style="border-top: 1px solid #000000; border-left: 1px solid #000000; background-color: #0380cd; float: left; height: 23px; width: 536px; color: #FFFFFF; padding-left: 6px;">Data Checks</div>
<div style="float: right; height: 30px; position: absolute; left: 535px; top: -10px;"><img id="imgClosePlant" runat="server" src="Images/close.png" alt="" onclick="$find('mpeDataChecks').hide();" style="cursor: pointer;" /></div>
<div style="width: 530px; padding: 6px; background-color: #ffffff; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px; border: 1px solid #000000;">
<div style="height: 30px; margin-top: 7px; float: left;"><img src="Images/StatusImage.png" alt="" /></div><div style="height: 50px;">The following errors were detected with your submission. Please make the appropriate corrections and try again.</div>
<p></p>
<asp:PlaceHolder ID="phDataChecks" runat="server"></asp:PlaceHolder>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>


<ajaxToolkit:ModalPopupExtender ID="mpeUseCalc" runat="server" DropShadow="true" PopupControlID="pnlUseCalc" TargetControlID="btnHidUseCalc" Y="180" BackgroundCssClass="modalBackground"></ajaxToolkit:ModalPopupExtender>
<asp:Button ID="btnHidUseCalc" runat="server" style="display: none;" />
<asp:Panel ID="pnlUseCalc" runat="server" style="border: 0px solid #000000; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px;">
<asp:UpdatePanel ID="upnlUseCalc" runat="server" UpdateMode="Conditional">
<Triggers><asp:AsyncPostBackTrigger ControlID="btnUseCalc" EventName="Click" /></Triggers>
<ContentTemplate>
<div style="border-top: 1px solid #000000; border-left: 1px solid #000000; background-color: #0380cd; float: left; height: 23px; width: 536px; color: #FFFFFF; padding-left: 6px;">Run Calculation?</div>
<div style="float: right; height: 30px; position: absolute; left: 535px; top: -10px;"><img id="img2" runat="server" src="Images/close.png" alt="" onclick="$find('mpeUseCalc').hide();" style="cursor: pointer;" /></div>
<div style="width: 530px; padding: 6px; background-color: #ffffff; -moz-border-radius: 0px 0px 0px 0px; -webkit-border-radius: 0px 0px 0px 0px; -khtml-border-radius: 0px 0px 0px 0px; border-radius: 0px 0px 0px 0px; border: 1px solid #000000;">
<div>Continuing will use one of your available calculations. Are you sure you want to continue?</div>
<asp:Button ID="btnUseCalc" runat="server" Text="Continue" OnClick="btnUseCalc_Click" OnClientClick="disableButton(this);" style="margin-right: 15px;" /><asp:Button ID="btnCancelCalc" runat="server" Text="Cancel" OnClick="btnCancelCalc_Click" />
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Panel>


</ContentTemplate></asp:UpdatePanel>


</div>


</asp:Content>
