﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.StandardEnergy
{
    [TestClass()]
    public class _1030_HydroPur : SqlDatabaseTestClass
    {

        public _1030_HydroPur()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void StandardEnergy_HydroPur_MBtuDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_HydroPur_MBtuDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_HydroPur_kScfDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_HydroPur_kScfDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_HydroPur_kScfDur()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_HydroPur_kScfDurData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }



        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_HydroPur_MBtuDay_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1030_HydroPur));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_HydroPur_kScfDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_HydroPur_kScfDur_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_HydroPur_kScfDur_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_HydroPur_kScfDur_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_HydroPur_kScfDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_HydroPur_kScfDay_EmtpySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_HydroPur_MBtuDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_HydroPur_MBtuDay_EmptySet;
            this.StandardEnergy_HydroPur_MBtuDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_HydroPur_kScfDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_HydroPur_kScfDurData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            StandardEnergy_HydroPur_MBtuDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_HydroPur_kScfDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_HydroPur_kScfDur_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_HydroPur_kScfDur_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_HydroPur_kScfDur_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_HydroPur_kScfDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_HydroPur_kScfDay_EmtpySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_HydroPur_MBtuDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_HydroPur_MBtuDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // StandardEnergy_HydroPur_MBtuDayData
            // 
            this.StandardEnergy_HydroPur_MBtuDayData.PosttestAction = null;
            this.StandardEnergy_HydroPur_MBtuDayData.PretestAction = null;
            this.StandardEnergy_HydroPur_MBtuDayData.TestAction = StandardEnergy_HydroPur_MBtuDay_TestAction;
            // 
            // StandardEnergy_HydroPur_MBtuDay_TestAction
            // 
            StandardEnergy_HydroPur_MBtuDay_TestAction.Conditions.Add(StandardEnergy_HydroPur_MBtuDay_NotEmpty);
            StandardEnergy_HydroPur_MBtuDay_TestAction.Conditions.Add(StandardEnergy_HydroPur_MBtuDay_EmptySet);
            resources.ApplyResources(StandardEnergy_HydroPur_MBtuDay_TestAction, "StandardEnergy_HydroPur_MBtuDay_TestAction");
            // 
            // StandardEnergy_HydroPur_kScfDayData
            // 
            this.StandardEnergy_HydroPur_kScfDayData.PosttestAction = null;
            this.StandardEnergy_HydroPur_kScfDayData.PretestAction = null;
            this.StandardEnergy_HydroPur_kScfDayData.TestAction = StandardEnergy_HydroPur_kScfDay_TestAction;
            // 
            // StandardEnergy_HydroPur_kScfDay_TestAction
            // 
            StandardEnergy_HydroPur_kScfDay_TestAction.Conditions.Add(StandardEnergy_HydroPur_kScfDay_NotEmpty);
            StandardEnergy_HydroPur_kScfDay_TestAction.Conditions.Add(StandardEnergy_HydroPur_kScfDay_EmtpySet);
            resources.ApplyResources(StandardEnergy_HydroPur_kScfDay_TestAction, "StandardEnergy_HydroPur_kScfDay_TestAction");
            // 
            // StandardEnergy_HydroPur_kScfDurData
            // 
            this.StandardEnergy_HydroPur_kScfDurData.PosttestAction = null;
            this.StandardEnergy_HydroPur_kScfDurData.PretestAction = null;
            this.StandardEnergy_HydroPur_kScfDurData.TestAction = StandardEnergy_HydroPur_kScfDur_TestAction;
            // 
            // StandardEnergy_HydroPur_kScfDur_TestAction
            // 
            StandardEnergy_HydroPur_kScfDur_TestAction.Conditions.Add(StandardEnergy_HydroPur_kScfDur_NotEmpty);
            StandardEnergy_HydroPur_kScfDur_TestAction.Conditions.Add(StandardEnergy_HydroPur_kScfDur_EmptySet);
            resources.ApplyResources(StandardEnergy_HydroPur_kScfDur_TestAction, "StandardEnergy_HydroPur_kScfDur_TestAction");
            // 
            // StandardEnergy_HydroPur_kScfDur_NotEmpty
            // 
            StandardEnergy_HydroPur_kScfDur_NotEmpty.Enabled = true;
            StandardEnergy_HydroPur_kScfDur_NotEmpty.Name = "StandardEnergy_HydroPur_kScfDur_NotEmpty";
            StandardEnergy_HydroPur_kScfDur_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_HydroPur_kScfDur_EmptySet
            // 
            StandardEnergy_HydroPur_kScfDur_EmptySet.Enabled = true;
            StandardEnergy_HydroPur_kScfDur_EmptySet.Name = "StandardEnergy_HydroPur_kScfDur_EmptySet";
            StandardEnergy_HydroPur_kScfDur_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_HydroPur_kScfDay_NotEmpty
            // 
            StandardEnergy_HydroPur_kScfDay_NotEmpty.Enabled = true;
            StandardEnergy_HydroPur_kScfDay_NotEmpty.Name = "StandardEnergy_HydroPur_kScfDay_NotEmpty";
            StandardEnergy_HydroPur_kScfDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_HydroPur_kScfDay_EmtpySet
            // 
            StandardEnergy_HydroPur_kScfDay_EmtpySet.Enabled = true;
            StandardEnergy_HydroPur_kScfDay_EmtpySet.Name = "StandardEnergy_HydroPur_kScfDay_EmtpySet";
            StandardEnergy_HydroPur_kScfDay_EmtpySet.ResultSet = 2;
            // 
            // StandardEnergy_HydroPur_MBtuDay_NotEmpty
            // 
            StandardEnergy_HydroPur_MBtuDay_NotEmpty.Enabled = true;
            StandardEnergy_HydroPur_MBtuDay_NotEmpty.Name = "StandardEnergy_HydroPur_MBtuDay_NotEmpty";
            StandardEnergy_HydroPur_MBtuDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_HydroPur_MBtuDay_EmptySet
            // 
            StandardEnergy_HydroPur_MBtuDay_EmptySet.Enabled = true;
            StandardEnergy_HydroPur_MBtuDay_EmptySet.Name = "StandardEnergy_HydroPur_MBtuDay_EmptySet";
            StandardEnergy_HydroPur_MBtuDay_EmptySet.ResultSet = 2;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardEnergy_HydroPur_MBtuDayData;
        private SqlDatabaseTestActions StandardEnergy_HydroPur_kScfDayData;
        private SqlDatabaseTestActions StandardEnergy_HydroPur_kScfDurData;
    }
}
