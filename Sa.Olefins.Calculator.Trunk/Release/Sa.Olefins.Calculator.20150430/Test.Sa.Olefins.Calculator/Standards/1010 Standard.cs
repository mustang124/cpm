﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _1010_Standards : SqlDatabaseTestClass
    {

        public _1010_Standards()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Standards_kEdc()
        {
            SqlDatabaseTestActions testActions = this.Standards_kEdcData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_PersMaint()
        {
            SqlDatabaseTestActions testActions = this.Standards_PersMaintData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_PersNonMaint()
        {
            SqlDatabaseTestActions testActions = this.Standards_PersNonMaintData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_Mes()
        {
            SqlDatabaseTestActions testActions = this.Standards_MesData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_NonEnergy()
        {
            SqlDatabaseTestActions testActions = this.Standards_NonEnergyData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_StdEnergy()
        {
            SqlDatabaseTestActions testActions = this.Standards_StdEnergyData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_Cap()
        {
            SqlDatabaseTestActions testActions = this.Standards_CapData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }










        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_kEdc_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1010_Standards));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_kEdc_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_kEdc_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_PersMaint_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_PersMaint_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_PersMaint_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_PersNonMaint_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_PersNonMaint_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_PersNonMaint_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_Mes_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_Mes_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_Mes_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_NonEnergy_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_NonEnergy_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_NonEnergy_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_StdEnergy_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_StdEnergy_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_StdEnergy_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_Cap_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_Cap_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_Cap_EmptySet;
            this.Standards_kEdcData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Standards_PersMaintData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Standards_PersNonMaintData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Standards_MesData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Standards_NonEnergyData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Standards_StdEnergyData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Standards_CapData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Standards_kEdc_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Standards_kEdc_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Standards_kEdc_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Standards_PersMaint_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Standards_PersMaint_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Standards_PersMaint_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Standards_PersNonMaint_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Standards_PersNonMaint_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Standards_PersNonMaint_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Standards_Mes_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Standards_Mes_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Standards_Mes_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Standards_NonEnergy_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Standards_NonEnergy_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Standards_NonEnergy_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Standards_StdEnergy_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Standards_StdEnergy_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Standards_StdEnergy_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Standards_Cap_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Standards_Cap_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Standards_Cap_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // Standards_kEdc_TestAction
            // 
            Standards_kEdc_TestAction.Conditions.Add(Standards_kEdc_NotEmpty);
            Standards_kEdc_TestAction.Conditions.Add(Standards_kEdc_EmptySet);
            resources.ApplyResources(Standards_kEdc_TestAction, "Standards_kEdc_TestAction");
            // 
            // Standards_kEdc_NotEmpty
            // 
            Standards_kEdc_NotEmpty.Enabled = true;
            Standards_kEdc_NotEmpty.Name = "Standards_kEdc_NotEmpty";
            Standards_kEdc_NotEmpty.ResultSet = 1;
            // 
            // Standards_kEdc_EmptySet
            // 
            Standards_kEdc_EmptySet.Enabled = true;
            Standards_kEdc_EmptySet.Name = "Standards_kEdc_EmptySet";
            Standards_kEdc_EmptySet.ResultSet = 2;
            // 
            // Standards_PersMaint_TestAction
            // 
            Standards_PersMaint_TestAction.Conditions.Add(Standards_PersMaint_NotEmpty);
            Standards_PersMaint_TestAction.Conditions.Add(Standards_PersMaint_EmptySet);
            resources.ApplyResources(Standards_PersMaint_TestAction, "Standards_PersMaint_TestAction");
            // 
            // Standards_PersMaint_NotEmpty
            // 
            Standards_PersMaint_NotEmpty.Enabled = true;
            Standards_PersMaint_NotEmpty.Name = "Standards_PersMaint_NotEmpty";
            Standards_PersMaint_NotEmpty.ResultSet = 1;
            // 
            // Standards_PersMaint_EmptySet
            // 
            Standards_PersMaint_EmptySet.Enabled = true;
            Standards_PersMaint_EmptySet.Name = "Standards_PersMaint_EmptySet";
            Standards_PersMaint_EmptySet.ResultSet = 2;
            // 
            // Standards_PersNonMaint_TestAction
            // 
            Standards_PersNonMaint_TestAction.Conditions.Add(Standards_PersNonMaint_NotEmpty);
            Standards_PersNonMaint_TestAction.Conditions.Add(Standards_PersNonMaint_EmptySet);
            resources.ApplyResources(Standards_PersNonMaint_TestAction, "Standards_PersNonMaint_TestAction");
            // 
            // Standards_PersNonMaint_NotEmpty
            // 
            Standards_PersNonMaint_NotEmpty.Enabled = true;
            Standards_PersNonMaint_NotEmpty.Name = "Standards_PersNonMaint_NotEmpty";
            Standards_PersNonMaint_NotEmpty.ResultSet = 1;
            // 
            // Standards_PersNonMaint_EmptySet
            // 
            Standards_PersNonMaint_EmptySet.Enabled = true;
            Standards_PersNonMaint_EmptySet.Name = "Standards_PersNonMaint_EmptySet";
            Standards_PersNonMaint_EmptySet.ResultSet = 2;
            // 
            // Standards_Mes_TestAction
            // 
            Standards_Mes_TestAction.Conditions.Add(Standards_Mes_NotEmpty);
            Standards_Mes_TestAction.Conditions.Add(Standards_Mes_EmptySet);
            resources.ApplyResources(Standards_Mes_TestAction, "Standards_Mes_TestAction");
            // 
            // Standards_Mes_NotEmpty
            // 
            Standards_Mes_NotEmpty.Enabled = true;
            Standards_Mes_NotEmpty.Name = "Standards_Mes_NotEmpty";
            Standards_Mes_NotEmpty.ResultSet = 1;
            // 
            // Standards_Mes_EmptySet
            // 
            Standards_Mes_EmptySet.Enabled = true;
            Standards_Mes_EmptySet.Name = "Standards_Mes_EmptySet";
            Standards_Mes_EmptySet.ResultSet = 2;
            // 
            // Standards_NonEnergy_TestAction
            // 
            Standards_NonEnergy_TestAction.Conditions.Add(Standards_NonEnergy_NotEmpty);
            Standards_NonEnergy_TestAction.Conditions.Add(Standards_NonEnergy_EmptySet);
            resources.ApplyResources(Standards_NonEnergy_TestAction, "Standards_NonEnergy_TestAction");
            // 
            // Standards_NonEnergy_NotEmpty
            // 
            Standards_NonEnergy_NotEmpty.Enabled = true;
            Standards_NonEnergy_NotEmpty.Name = "Standards_NonEnergy_NotEmpty";
            Standards_NonEnergy_NotEmpty.ResultSet = 1;
            // 
            // Standards_NonEnergy_EmptySet
            // 
            Standards_NonEnergy_EmptySet.Enabled = true;
            Standards_NonEnergy_EmptySet.Name = "Standards_NonEnergy_EmptySet";
            Standards_NonEnergy_EmptySet.ResultSet = 2;
            // 
            // Standards_StdEnergy_TestAction
            // 
            Standards_StdEnergy_TestAction.Conditions.Add(Standards_StdEnergy_NotEmpty);
            Standards_StdEnergy_TestAction.Conditions.Add(Standards_StdEnergy_EmptySet);
            resources.ApplyResources(Standards_StdEnergy_TestAction, "Standards_StdEnergy_TestAction");
            // 
            // Standards_StdEnergy_NotEmpty
            // 
            Standards_StdEnergy_NotEmpty.Enabled = true;
            Standards_StdEnergy_NotEmpty.Name = "Standards_StdEnergy_NotEmpty";
            Standards_StdEnergy_NotEmpty.ResultSet = 1;
            // 
            // Standards_StdEnergy_EmptySet
            // 
            Standards_StdEnergy_EmptySet.Enabled = true;
            Standards_StdEnergy_EmptySet.Name = "Standards_StdEnergy_EmptySet";
            Standards_StdEnergy_EmptySet.ResultSet = 2;
            // 
            // Standards_Cap_TestAction
            // 
            Standards_Cap_TestAction.Conditions.Add(Standards_Cap_NotEmpty);
            Standards_Cap_TestAction.Conditions.Add(Standards_Cap_EmptySet);
            resources.ApplyResources(Standards_Cap_TestAction, "Standards_Cap_TestAction");
            // 
            // Standards_Cap_NotEmpty
            // 
            Standards_Cap_NotEmpty.Enabled = true;
            Standards_Cap_NotEmpty.Name = "Standards_Cap_NotEmpty";
            Standards_Cap_NotEmpty.ResultSet = 1;
            // 
            // Standards_Cap_EmptySet
            // 
            Standards_Cap_EmptySet.Enabled = true;
            Standards_Cap_EmptySet.Name = "Standards_Cap_EmptySet";
            Standards_Cap_EmptySet.ResultSet = 2;
            // 
            // Standards_kEdcData
            // 
            this.Standards_kEdcData.PosttestAction = null;
            this.Standards_kEdcData.PretestAction = null;
            this.Standards_kEdcData.TestAction = Standards_kEdc_TestAction;
            // 
            // Standards_PersMaintData
            // 
            this.Standards_PersMaintData.PosttestAction = null;
            this.Standards_PersMaintData.PretestAction = null;
            this.Standards_PersMaintData.TestAction = Standards_PersMaint_TestAction;
            // 
            // Standards_PersNonMaintData
            // 
            this.Standards_PersNonMaintData.PosttestAction = null;
            this.Standards_PersNonMaintData.PretestAction = null;
            this.Standards_PersNonMaintData.TestAction = Standards_PersNonMaint_TestAction;
            // 
            // Standards_MesData
            // 
            this.Standards_MesData.PosttestAction = null;
            this.Standards_MesData.PretestAction = null;
            this.Standards_MesData.TestAction = Standards_Mes_TestAction;
            // 
            // Standards_NonEnergyData
            // 
            this.Standards_NonEnergyData.PosttestAction = null;
            this.Standards_NonEnergyData.PretestAction = null;
            this.Standards_NonEnergyData.TestAction = Standards_NonEnergy_TestAction;
            // 
            // Standards_StdEnergyData
            // 
            this.Standards_StdEnergyData.PosttestAction = null;
            this.Standards_StdEnergyData.PretestAction = null;
            this.Standards_StdEnergyData.TestAction = Standards_StdEnergy_TestAction;
            // 
            // Standards_CapData
            // 
            this.Standards_CapData.PosttestAction = null;
            this.Standards_CapData.PretestAction = null;
            this.Standards_CapData.TestAction = Standards_Cap_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions Standards_kEdcData;
        private SqlDatabaseTestActions Standards_PersMaintData;
        private SqlDatabaseTestActions Standards_PersNonMaintData;
        private SqlDatabaseTestActions Standards_MesData;
        private SqlDatabaseTestActions Standards_NonEnergyData;
        private SqlDatabaseTestActions Standards_StdEnergyData;
        private SqlDatabaseTestActions Standards_CapData;
    }
}
