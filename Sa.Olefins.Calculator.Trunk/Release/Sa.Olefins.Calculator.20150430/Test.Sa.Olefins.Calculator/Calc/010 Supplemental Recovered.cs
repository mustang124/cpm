﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _010_Supplemental_Recovered : SqlDatabaseTestClass
    {

        public _010_Supplemental_Recovered()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Supplemental_Recovered_Dur()
        {
            SqlDatabaseTestActions testActions = this.Supplemental_Recovered_DurData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Supplemental_Recovered_Ann()
        {
            SqlDatabaseTestActions testActions = this.Supplemental_Recovered_AnnData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }


        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Supplemental_Recovered_Dur_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_010_Supplemental_Recovered));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Supplemental_Recovered_Dur_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Supplemental_Recovered_Dur_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Supplemental_Recovered_Ann_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Supplemental_Recovered_Ann_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Supplemental_Recovered_Ann_EmptySet;
            this.Supplemental_Recovered_DurData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Supplemental_Recovered_AnnData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Supplemental_Recovered_Dur_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Supplemental_Recovered_Dur_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Supplemental_Recovered_Dur_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Supplemental_Recovered_Ann_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Supplemental_Recovered_Ann_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Supplemental_Recovered_Ann_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // Supplemental_Recovered_Dur_TestAction
            // 
            Supplemental_Recovered_Dur_TestAction.Conditions.Add(Supplemental_Recovered_Dur_NotEmpty);
            Supplemental_Recovered_Dur_TestAction.Conditions.Add(Supplemental_Recovered_Dur_EmptySet);
            resources.ApplyResources(Supplemental_Recovered_Dur_TestAction, "Supplemental_Recovered_Dur_TestAction");
            // 
            // Supplemental_Recovered_Dur_NotEmpty
            // 
            Supplemental_Recovered_Dur_NotEmpty.Enabled = true;
            Supplemental_Recovered_Dur_NotEmpty.Name = "Supplemental_Recovered_Dur_NotEmpty";
            Supplemental_Recovered_Dur_NotEmpty.ResultSet = 1;
            // 
            // Supplemental_Recovered_Dur_EmptySet
            // 
            Supplemental_Recovered_Dur_EmptySet.Enabled = true;
            Supplemental_Recovered_Dur_EmptySet.Name = "Supplemental_Recovered_Dur_EmptySet";
            Supplemental_Recovered_Dur_EmptySet.ResultSet = 2;
            // 
            // Supplemental_Recovered_Ann_TestAction
            // 
            Supplemental_Recovered_Ann_TestAction.Conditions.Add(Supplemental_Recovered_Ann_NotEmpty);
            Supplemental_Recovered_Ann_TestAction.Conditions.Add(Supplemental_Recovered_Ann_EmptySet);
            resources.ApplyResources(Supplemental_Recovered_Ann_TestAction, "Supplemental_Recovered_Ann_TestAction");
            // 
            // Supplemental_Recovered_Ann_NotEmpty
            // 
            Supplemental_Recovered_Ann_NotEmpty.Enabled = true;
            Supplemental_Recovered_Ann_NotEmpty.Name = "Supplemental_Recovered_Ann_NotEmpty";
            Supplemental_Recovered_Ann_NotEmpty.ResultSet = 1;
            // 
            // Supplemental_Recovered_Ann_EmptySet
            // 
            Supplemental_Recovered_Ann_EmptySet.Enabled = true;
            Supplemental_Recovered_Ann_EmptySet.Name = "Supplemental_Recovered_Ann_EmptySet";
            Supplemental_Recovered_Ann_EmptySet.ResultSet = 2;
            // 
            // Supplemental_Recovered_DurData
            // 
            this.Supplemental_Recovered_DurData.PosttestAction = null;
            this.Supplemental_Recovered_DurData.PretestAction = null;
            this.Supplemental_Recovered_DurData.TestAction = Supplemental_Recovered_Dur_TestAction;
            // 
            // Supplemental_Recovered_AnnData
            // 
            this.Supplemental_Recovered_AnnData.PosttestAction = null;
            this.Supplemental_Recovered_AnnData.PretestAction = null;
            this.Supplemental_Recovered_AnnData.TestAction = Supplemental_Recovered_Ann_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions Supplemental_Recovered_DurData;
        private SqlDatabaseTestActions Supplemental_Recovered_AnnData;
    }
}
