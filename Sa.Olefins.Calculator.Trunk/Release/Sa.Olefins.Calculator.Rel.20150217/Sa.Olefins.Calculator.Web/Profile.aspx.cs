﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace OlefinsCalculator
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((System.Web.UI.HtmlControls.HtmlAnchor)this.Master.FindControl("alogin")).InnerText = "Logout";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.Length < 8)
            {
                lblStatus.Text = "Password must be at least 8 characters.";
                lblStatus.Visible = true;
            }
            else
            {
                lblStatus.Visible = false;

                OlefinsCalculator.User user = (OlefinsCalculator.User)Session["User"];

                byte[] dbPassword;
                string hashedPassword = string.Empty;
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_User", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Username", user._username));
                    conn.Open();

                    SqlDataReader dr = command.ExecuteReader();
                    dr.Read();
                    if (!dr.HasRows)
                    {
                        conn.Close();
                        lblStatus.Text = "Incorrect Password.";
                        lblStatus.Visible = true;
                    }
                    else
                    {
                        dbPassword = ((byte[])dr["Password"]);
                        byte[] hashedBytes;

                        using (SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider())
                        {
                            byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(Convert.ToBase64String(((byte[])dr["Salt"])) + txtCurrentPassword.Text + txtCurrentPassword.Text));
                            hashedPassword = Convert.ToBase64String(bb);
                            hashedBytes = bb;
                        }

                        if (Convert.ToBase64String(dbPassword) == Convert.ToBase64String(hashedBytes))
                            UpdateUser(user._userID);
                        else
                        {
                            lblStatus.Text = "Incorrect Password.";
                            lblStatus.Visible = true;
                        }
                        
                    }
                }

            }
        }

        public void UpdateUser(int userID)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                conn.Open();
                SqlCommand command = new SqlCommand("[web].Update_User", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@UserID", userID));

                string hashedPassword = string.Empty;
                byte[] salt = generateSalt();
                string saltString = Convert.ToBase64String(salt);

                using (SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider())
                {
                    byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(Convert.ToBase64String(salt) + txtPassword.Text + txtPassword.Text));
                    hashedPassword = Convert.ToBase64String(bb);
                    command.Parameters.Add(new SqlParameter("@Password", bb));
                }
                command.Parameters.Add(new SqlParameter("@Salt", salt));
                command.ExecuteNonQuery();
                conn.Close();
                lblStatus.Text = "Password Changed.";
                lblStatus.Visible = true;
            }
        }
        
        #region Generate Salt
        private byte[] generateSalt()
        {
            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                byte[] data = new byte[512];
                rngCsp.GetBytes(data);
                return data;
            }
        }
        #endregion

            
        
    }
}