﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace OlefinsCalculator
{
    public partial class PlantAdmin : System.Web.UI.Page
    {
        int CompanyID;

        protected void Page_Load(object sender, EventArgs e)
        {
            ((System.Web.UI.HtmlControls.HtmlAnchor)this.Master.FindControl("alogin")).InnerText = "Logout";

            OlefinsCalculator.User user = (OlefinsCalculator.User)Session["User"];
            CompanyID = user._companyID;
            lblCompanyName.Text = user._companyName;

            if (!Page.IsPostBack)
            {
                bindPlants(CompanyID);
                bindUsers(CompanyID);
            }
        }

        public void bindPlants(int CompanyID)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("[web].Get_PlantsByCompany_Active", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsPlants = new DataSet();
                sda.Fill(dsPlants);
                //dgvPlants.DataSource = dsPlants;
                //dgvPlants.DataBind();
                lvPlants.DataSource = dsPlants;
                lvPlants.DataBind();
            }
        }

        public void bindUsers(int CompanyID)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                //SqlCommand command = new SqlCommand("[web].Get_UsersByCompany_Active", conn);
                SqlCommand command = new SqlCommand("[web].Get_ClientUsersByCompany_Active", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUsers = new DataSet();
                sda.Fill(dsUsers);
                dgvUsers.DataSource = dsUsers;
                dgvUsers.DataBind();

                //ddlCompanyUsers.AppendDataBoundItems = true;
                ddlCompanyUsers.DataSource = dsUsers;
                ddlCompanyUsers.DataTextField = "LastFirst";
                ddlCompanyUsers.DataValueField = "UserID";
                ddlCompanyUsers.DataBind();
                ddlCompanyUsers.Items.Insert(0, (new ListItem("-- Select --", "")));
            }
        }

        public void bindUsersByPlant(int PlantID)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("[web].Get_UsersByPlant_Active", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@PlantID", PlantID));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUsers = new DataSet();
                sda.Fill(dsUsers);
                dgvEditPlantUsers.DataSource = dsUsers;
                dgvEditPlantUsers.DataBind();

                hidPlantID.Value = PlantID.ToString();
            }
        }

        public void bindPlantsAndPermissions(int CompanyID, int UserID)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("[web].Get_CompanyPlantsUser", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                command.Parameters.Add(new SqlParameter("@UserID", UserID));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsUsers = new DataSet();
                sda.Fill(dsUsers);
                dgvEditUserPlants.DataSource = dsUsers;
                dgvEditUserPlants.DataBind();
            }
        }

        public void lnkbtnSelectPlant_Click(object sender, CommandEventArgs c)
        {
            hidInsertPlant.Value = "0";
            bindUsersByPlant(Convert.ToInt32(c.CommandArgument));
            if (dgvEditPlantUsers.Rows.Count < 1)
                btnRemoveUser.Enabled = false;
            else
                btnRemoveUser.Enabled = true;
            txtPlantName.Text = ((LinkButton)sender).Text;
            hidPlantName.Value = txtPlantName.Text;
            lblPlantEditStatus.Visible = false;
            mpePlant.Show();
        }

        public void lnkbtnSelectUser_Click(object sender, CommandEventArgs c)
        {
            rfvPassword.Enabled = false;
            hidInsertUser.Value = "0";
            lblPasswordError.Text = "";
            bindPlantsAndPermissions(CompanyID, Convert.ToInt32(c.CommandArgument));
            hidUserID.Value = c.CommandArgument.ToString();
            foreach (GridViewRow gvr in dgvUsers.Rows)
            {
                if (((Label)gvr.Cells[2].FindControl("lbldgvUsersUserID")).Text == c.CommandArgument.ToString())
                {
                    txtFirstName.Text = ((Label)gvr.Cells[3].FindControl("lbldgvUsersFirstName")).Text;
                    txtLastName.Text = ((Label)gvr.Cells[4].FindControl("lbldgvUsersLAstName")).Text;
                    txtEmail.Text = ((Label)gvr.Cells[5].FindControl("lbldgvUsersUsername")).Text;
                    break;
                }
            }
            mpeUser.Show();
        }

        protected void btnEditUserSubmit_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                //SqlCommand command = new SqlCommand("UpdateUser", conn);
                SqlCommand command = new SqlCommand("[web].Update_User", conn);
                
                command.CommandType = CommandType.StoredProcedure;
                if (hidInsertUser.Value == "0")
                    command.Parameters.Add(new SqlParameter("@UserID", hidUserID.Value));
                else
                    command.Parameters.Add(new SqlParameter("@Active", 1));
                command.Parameters.Add(new SqlParameter("@FirstName", txtFirstName.Text));
                command.Parameters.Add(new SqlParameter("@LastName", txtLastName.Text));
                command.Parameters.Add(new SqlParameter("@Username", txtEmail.Text));
                command.Parameters.Add(new SqlParameter("@SecurityLevelID", 10));
                if (txtPassword.Text != "")
                {
                    string hashedPassword = string.Empty;
                    byte[] salt = generateSalt();
                    using (SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider())
                    {
                        //byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(salt + txtPassword.Text + txtPassword.Text));
                        byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(Convert.ToBase64String(salt) + txtPassword.Text + txtPassword.Text));
                        hashedPassword = Convert.ToBase64String(bb);
                        command.Parameters.Add(new SqlParameter("@Password", bb));
                    }
                    //command.Parameters.Add(new SqlParameter("@Password", hashedPassword));
                    command.Parameters.Add(new SqlParameter("@Salt", salt));
                }
                conn.Open();
                var UID = command.ExecuteScalar();//.ExecuteNonQuery();
                if (UID != null)
                {
                    hidUserID.Value = UID.ToString();
                    SqlCommand command3 = new SqlCommand("[web].Update_CompanyPermissions", conn);
                    command3.CommandType = CommandType.StoredProcedure;
                    command3.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                    command3.Parameters.Add(new SqlParameter("@UserID", hidUserID.Value));
                    command3.Parameters.Add(new SqlParameter("@Active", 1));
                    command3.ExecuteNonQuery();

                }
                conn.Close();

                conn.Open();
                foreach (GridViewRow gvr in dgvEditUserPlants.Rows)
                {
                    SqlCommand command2 = new SqlCommand("[web].Update_PlantPermissions", conn);
                    command2.CommandType = CommandType.StoredProcedure;
                    command2.Parameters.Add(new SqlParameter("@PlantID", Convert.ToInt32(((Label)gvr.Cells[2].FindControl("lblPlantID")).Text)));
                    command2.Parameters.Add(new SqlParameter("@UserID", hidUserID.Value));
                    command2.Parameters.Add(new SqlParameter("@Active", ((CheckBox)gvr.Cells[0].FindControl("chkActive")).Checked));
                    command2.ExecuteNonQuery();
                }
                conn.Close();
            }
            lblPasswordError.Text = "User Updated.";
            bindUsers(CompanyID);
            mpeUser.Show();
        }

        #region Generate Salt
        private byte[] generateSalt()
        {
            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                byte[] data = new byte[512];
                rngCsp.GetBytes(data);
                return data;
            }
        }
        #endregion

        protected void btnRemoveUser_Click(object sender, EventArgs e)
        {
            lblPlantEditStatus.Visible = false;
            
            foreach (GridViewRow gvr in dgvEditPlantUsers.Rows)
            {
                if (((CheckBox)gvr.Cells[0].FindControl("CheckBox1")).Checked == true)
                {
                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                    using (conn)
                    {
                        SqlCommand command = new SqlCommand("[web].Update_PlantPermissions", conn);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@PlantPermissionID", Convert.ToInt32(((Label)gvr.Cells[3].FindControl("lblPlantPermissionID")).Text)));
                        command.Parameters.Add(new SqlParameter("@Active", false));
                        conn.Open();
                        command.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }

            bindUsersByPlant(Convert.ToInt32(hidPlantID.Value));

            lblPlantEditStatus.ForeColor = System.Drawing.Color.Red;
            lblPlantEditStatus.Visible = true;
            lblPlantEditStatus.Text = "Plant Updated.";

            if (dgvEditPlantUsers.Rows.Count < 1)
                btnRemoveUser.Enabled = false;
            else
                btnRemoveUser.Enabled = true;
        }

        protected void btnEditPlantSubmit_Click(object sender, EventArgs e)
        {
            lblPlantEditStatus.Visible = false;

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());

            //int CompanyID = 1;
            //SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                //SqlCommand command = new SqlCommand("UpdatePlant", conn);
                SqlCommand command = new SqlCommand("[web].Update_Plant", conn);
                command.CommandType = CommandType.StoredProcedure;
                if (hidInsertPlant.Value == "0")
                    command.Parameters.Add(new SqlParameter("@PlantID", hidPlantID.Value));
                command.Parameters.Add(new SqlParameter("@PlantName", txtPlantName.Text));
                command.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                conn.Open();
                //command.ExecuteNonQuery();
                var pID = command.ExecuteScalar();
                if (Convert.ToInt32(pID) > 0)
                {
                    hidPlantID.Value = pID.ToString();
                    hidInsertPlant.Value = "0";
                }
                conn.Close();
            }

            bindPlants(CompanyID);

            lblPlantEditStatus.ForeColor = System.Drawing.Color.Red;
            lblPlantEditStatus.Visible = true;
            lblPlantEditStatus.Text = "Plant Updated.";

            hidPlantName.Value = txtPlantName.Text;

            if (ddlCompanyUsers.SelectedIndex > 0)
            {
                SqlConnection conn2 = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn2)
                {
                    SqlCommand command = new SqlCommand("[web].Update_PlantPermissions", conn2);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@PlantID", hidPlantID.Value));
                    command.Parameters.Add(new SqlParameter("@UserID", ddlCompanyUsers.SelectedItem.Value));
                    command.Parameters.Add(new SqlParameter("@Active", 1));
                    conn2.Open();
                    command.ExecuteNonQuery();
                    conn2.Close();
                }

                bindUsersByPlant(Convert.ToInt32(hidPlantID.Value));

                if (dgvEditPlantUsers.Rows.Count < 1)
                    btnRemoveUser.Enabled = false;
                else
                    btnRemoveUser.Enabled = true;

                ddlCompanyUsers.SelectedIndex = 0;

                mpePlant.Show();
            }

            
        }

        protected void lnkbtnAddUser_Click(object sender, EventArgs e)
        {
            lblPasswordError.Text = "";
            txtEmail.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtPassword.Text = "";
            txtPassword2.Text = "";
            rfvPassword.Enabled = true;
            hidInsertUser.Value = "1";
            bindPlantsAndPermissions(CompanyID, 0);
            mpeUser.Show();
        }

        protected void lnkbtnAddPlant_Click(object sender, EventArgs e)
        {
            txtPlantName.Text = "";
            bindUsersByPlant(0);
            hidInsertPlant.Value = "1";
            mpePlant.Show();
        }

        protected void btnDeleteUsers_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in dgvUsers.Rows)
            {
                if (((CheckBox)gvr.Cells[0].FindControl("CheckBox1")).Checked == true)
                {
                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                    using (conn)
                    {
                        SqlCommand command = new SqlCommand("[web].Update_User", conn);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@UserID", Convert.ToInt32(((Label)gvr.Cells[2].FindControl("lbldgvUsersUserID")).Text)));
                        command.Parameters.Add(new SqlParameter("@Active", false));
                        conn.Open();
                        command.ExecuteNonQuery();
                        conn.Close();
                    }

                }
            }

            bindUsers(CompanyID);
        }

        protected void btnDeletePlants_Click(object sender, EventArgs e)
        {
        //    foreach (GridViewRow gvr in dgvPlants.Rows)
        //    {
        //        if (((CheckBox)gvr.Cells[0].FindControl("CheckBox1")).Checked == true)
        //        {
        //            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
        //            using (conn)
        //            {
        //                SqlCommand command = new SqlCommand("UpdatePlant", conn);
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.Add(new SqlParameter("@PlantID", Convert.ToInt32(((Label)gvr.Cells[2].FindControl("lbldgvPlantsPlantID")).Text)));
        //                command.Parameters.Add(new SqlParameter("@Active", false));
        //                conn.Open();
        //                command.ExecuteNonQuery();
        //                conn.Close();
        //            }

        //        }
        //    }

        //    bindPlants(CompanyID);
        }


    }
}
