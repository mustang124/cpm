﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OlefinsCalculator
{
    public partial class Calculator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.UI.HtmlControls.HtmlGenericControl body = (System.Web.UI.HtmlControls.HtmlGenericControl)Page.Master.FindControl("MasterBody");
            body.Attributes.Add("OnLoad", "$('#divddlCalc').hide();$('#calcs').hide();$('#plantHeader').hide();$('#outerPlantFacilities').slideToggle();$('#outerLightFeedstocks').slideToggle();$('#outerLiquidFeedstocks').slideToggle();$('#outerSupplemental').slideToggle();$('#outerProducts').slideToggle();$('#outerOtherProducts').slideToggle();");
        }
    }
}