﻿PRINT 'INSERT INTO [ante].[NicenessComponents]([MethodologyId], [ComponentId], [Coefficient])';

INSERT INTO [ante].[NicenessComponents]([MethodologyId], [ComponentId], [Coefficient])
SELECT t.[MethodologyId], t.[ComponentId], t.[Coefficient]
FROM (VALUES
	(@MethodologyId, dim.Return_ComponentId('P'), 1.05),
	(@MethodologyId, dim.Return_ComponentId('I'), 1.00),
	(@MethodologyId, dim.Return_ComponentId('A'), 0.67),
	(@MethodologyId, dim.Return_ComponentId('N'), 0.95),
	(@MethodologyId, dim.Return_ComponentId('O'), 0.95)
	)t([MethodologyId], [ComponentId], [Coefficient]);