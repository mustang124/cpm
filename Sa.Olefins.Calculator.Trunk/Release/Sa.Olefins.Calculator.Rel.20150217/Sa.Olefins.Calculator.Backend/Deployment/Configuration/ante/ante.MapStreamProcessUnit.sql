﻿PRINT 'INSERT INTO [ante].[MapStreamProcessUnit]([MethodologyId], [StreamId], [ProcessUnitId])';

INSERT INTO [ante].[MapStreamProcessUnit]([MethodologyId], [StreamId], [ProcessUnitId])
SELECT t.[MethodologyId], t.[StreamId], t.[ProcessUnitId]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('Ethylene'),		dim.Return_ProcessUnitId('RedEthylene')),
	(@MethodologyId, dim.Return_StreamId('EthyleneCG'),		dim.Return_ProcessUnitId('RedEthyleneCG')),
	(@MethodologyId, dim.Return_StreamId('Propylene'),		dim.Return_ProcessUnitId('RedPropylene')),
	(@MethodologyId, dim.Return_StreamId('PropyleneCG'),	dim.Return_ProcessUnitId('RedPropyleneCG')),
	(@MethodologyId, dim.Return_StreamId('PropyleneRG'),	dim.Return_ProcessUnitId('RedPropyleneRG')),
	(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ProcessUnitId('RedCrackedGasTrans'))
	)t([MethodologyId], [StreamId], [ProcessUnitId]);