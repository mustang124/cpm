﻿PRINT 'INSERT INTO [ante].[MapFactorHydroPur]([MethodologyId], [FacilityId], [FactorId])';

INSERT INTO [ante].[MapFactorHydroPur]([MethodologyId], [FacilityId], [FactorId])
SELECT t.[MethodologyId], t.[FacilityId], t.[FactorId]
FROM (VALUES
	(@MethodologyId, dim.Return_FacilityId('HydroPurCryogenic'),	dim.Return_FactorId('HydroPurCryogenic')),
	(@MethodologyId, dim.Return_FacilityId('HydroPurPSA'),			dim.Return_FactorId('HydroPurPSA')),
	(@MethodologyId, dim.Return_FacilityId('HydroPurMembrane'),		dim.Return_FactorId('HydroPurMembrane'))
	)t([MethodologyId], [FacilityId], [FactorId]);