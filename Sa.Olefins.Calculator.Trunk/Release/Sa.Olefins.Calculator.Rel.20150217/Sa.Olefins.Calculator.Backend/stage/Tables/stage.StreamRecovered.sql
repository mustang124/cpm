﻿CREATE TABLE [stage].[StreamRecovered]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecovered_Submissions]							REFERENCES [stage].[Submissions]([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [CR_StreamRecovered_StreamNumber]						CHECK([StreamNumber] > 0),

	[Recovered_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MinIncl_0.0]		CHECK([Recovered_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MaxIncl_100.0]		CHECK([Recovered_WtPcnt] <= 100.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecovered_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamRecovered]				PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamNumber] ASC)
);
GO

CREATE TRIGGER [stage].[t_StreamRecovered_u]
ON [stage].[StreamRecovered]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamRecovered]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamRecovered].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[stage].[StreamRecovered].[StreamNumber]		= INSERTED.[StreamNumber];

END;
GO