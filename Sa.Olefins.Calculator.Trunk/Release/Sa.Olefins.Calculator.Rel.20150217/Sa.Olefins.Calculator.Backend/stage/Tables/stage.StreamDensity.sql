﻿CREATE TABLE [stage].[StreamDensity]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamDensity_Submissions]				REFERENCES [stage].[Submissions]([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [CR_StreamDensity_StreamNumber]				CHECK([StreamNumber] > 0),

	[Density_SG]			FLOAT				NOT	NULL	CONSTRAINT [CR_StreamDensity_Density_SG_MinIncl_0.0]	CHECK([Density_SG] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamDensity_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDensity_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDensity_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDensity_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamDensity]				PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamNumber] ASC)
);
GO

CREATE TRIGGER [stage].[t_StreamDensity_u]
ON [stage].[StreamDensity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamDensity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamDensity].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[StreamDensity].[StreamNumber]	= INSERTED.[StreamNumber];

END;
GO