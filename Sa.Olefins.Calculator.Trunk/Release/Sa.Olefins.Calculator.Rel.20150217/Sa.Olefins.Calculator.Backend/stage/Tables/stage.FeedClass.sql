﻿CREATE TABLE [stage].[FeedClass]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_Submissions]		REFERENCES [stage].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FeedClassId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_FeedClassId]		REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[IsCalculated]			BIT					NOT NULL	CONSTRAINT [DF_FeedClass_Calcualted]		DEFAULT (0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FeedClass_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FeedClass]					PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [stage].[t_FeedClass_u]
ON [stage].[FeedClass]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[FeedClass]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[FeedClass].[SubmissionId]		= INSERTED.[SubmissionId];

END;
GO