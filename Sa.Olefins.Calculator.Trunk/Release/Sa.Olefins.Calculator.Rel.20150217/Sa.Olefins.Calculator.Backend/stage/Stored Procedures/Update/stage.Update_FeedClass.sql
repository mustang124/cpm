﻿CREATE PROCEDURE [stage].[Update_FeedClass]
(
	@SubmissionId			INT,

	@FeedClassId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY
	
	UPDATE [stage].[FeedClass]
	SET	[FeedClassId] = @FeedClassId
	WHERE	[SubmissionId]		= @SubmissionId
		AND	@FeedClassId > 0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
			+ COALESCE(', @FeedClassId:'	+ CONVERT(VARCHAR, @FeedClassId),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;