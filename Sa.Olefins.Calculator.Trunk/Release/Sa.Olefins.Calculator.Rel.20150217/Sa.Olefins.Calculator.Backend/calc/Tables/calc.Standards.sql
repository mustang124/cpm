﻿CREATE TABLE [calc].[Standards]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Standards_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_Standards_Submissions]			REFERENCES [fact].[Submissions] ([SubmissionId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StandardId]			INT					NOT NULL	CONSTRAINT [FK_Standards_Standard_LookUp]		REFERENCES [dim].[Standard_LookUp] ([StandardId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ProcessUnitId]			INT					NOT NULL	CONSTRAINT [FK_Standards_ProcessUnit]			REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[StandardValue]			FLOAT				NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Standards_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standards_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standards_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standards_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Standards]					PRIMARY KEY CLUSTERED([MethodologyId] DESC, [SubmissionId] ASC, [StandardId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE NONCLUSTERED INDEX [IX_Standards_Aggregate]
ON [calc].[Standards] ([MethodologyId],[StandardId],[ProcessUnitId])
INCLUDE ([SubmissionId],[StandardValue])
GO

CREATE TRIGGER [calc].[t_Standards_u]
ON [calc].[Standards]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[Standards]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[Standards].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[Standards].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[Standards].[StandardId]		= INSERTED.[StandardId]
		AND	[calc].[Standards].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;
GO