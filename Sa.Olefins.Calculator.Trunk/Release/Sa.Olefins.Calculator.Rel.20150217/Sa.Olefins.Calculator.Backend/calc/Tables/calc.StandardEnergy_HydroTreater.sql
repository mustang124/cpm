﻿CREATE TABLE [calc].[StandardEnergy_HydroTreater]
(
	[MethodologyId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_HydroTreater_Methodology]							REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_HydroTreater_Submissions]							REFERENCES [fact].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Quantity_kMT]				FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_Quantity_kMT_MinIncl_0.0]			CHECK([Quantity_kMT] >= 0.0),
	[Quantity_Barrels]			FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_Quantity_Barrels_MinIncl_0.0]		CHECK([Quantity_Barrels] >= 0.0),
	[Quantity_BarrelsDay]		FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_Quantity_BarrelsDay_MinIncl_0.0]		CHECK([Quantity_BarrelsDay] >= 0.0),
	[StandardEnergy_MBtuDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_HydroTreater_StandardEnergy_MBtuDay_MinIncl_0.0]	CHECK([StandardEnergy_MBtuDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_HydroTreater_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_HydroTreater]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC)
);
GO

CREATE TRIGGER [calc].[t_StandardEnergy_HydroTreater_u]
ON [calc].[StandardEnergy_HydroTreater]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_HydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_HydroTreater].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_HydroTreater].[SubmissionId]		= INSERTED.[SubmissionId];

END;
GO