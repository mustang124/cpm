﻿CREATE FUNCTION [calc].[Return_FeedClassId_PlantId]
(
	@PlantId		INT
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT TOP 1
		@Id = l.[FeedClassId]
	FROM [calc].[FeedClass]				fc
	INNER JOIN [dim].[FeedClass_LookUp]			l
		ON	l.[FeedClassId]		= fc.[FeedClassId]
	WHERE	fc.[SubmissionId]	= [auth].[Return_PreviousSubmissionId](@PlantId, 0)
		AND	fc.[MethodologyId]	= (SELECT MAX(MethodologyId) FROM [ante].[Methodology] m);

	RETURN @Id;

END;