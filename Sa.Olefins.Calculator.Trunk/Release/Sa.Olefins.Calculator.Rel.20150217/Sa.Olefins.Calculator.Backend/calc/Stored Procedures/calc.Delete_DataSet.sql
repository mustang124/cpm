﻿CREATE PROCEDURE [calc].[Delete_DataSet]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

	EXECUTE [calc].[Delete_Standard]							@MethodologyId, @SubmissionId;

	EXECUTE	[calc].[Delete_StandardEnergy_PyrolysisBtu]			@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StandardEnergy_PyrolysisHvc]			@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StandardEnergy_Reduction]			@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StandardEnergy_HydroPur]				@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StandardEnergy_HydroTreater]			@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StandardEnergy_Fractionator]			@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StandardEnergy_Supplemental]			@MethodologyId, @SubmissionId;

	EXECUTE [calc].[Delete_ContainedLightFeed]					@MethodologyId, @SubmissionId;

	EXECUTE	[calc].[Delete_FeedClass]							@MethodologyId, @SubmissionId;

	EXECUTE	[calc].[Delete_Capacity_CrackedGasTransfers]		@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_SupplementalRecovered]				@MethodologyId, @SubmissionId;

	EXECUTE	[calc].[Delete_FacilitiesElecGeneration]			@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_FacilitiesBoilers]					@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_FacilitiesHydroTreater]				@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_FacilitiesFractionator]				@MethodologyId, @SubmissionId;

	EXECUTE	[calc].[Delete_HvcProductionDivisor]				@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_HydrogenPurification]				@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_Capacity]							@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_Utilization]							@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StreamRecycled]						@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StreamRecovered]						@MethodologyId, @SubmissionId;
	EXECUTE	[calc].[Delete_StreamComposition]					@MethodologyId, @SubmissionId;

	EXECUTE	[calc].[Delete_Audit]								@MethodologyId, @SubmissionId;

END;
GO