﻿CREATE PROCEDURE [calc].[Insert_SupplementalRecovered_SuppWashOil_PyroFuelOil]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@SuppWashOil	INT	= [dim].[Return_StreamId]('SuppWashOil');
	DECLARE	@PyroFuelOil	INT	= [dim].[Return_ComponentId]('PyroFuelOil');

	INSERT INTO [calc].[SupplementalRecovered]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Recovered_Dur_kMT], [Recovered_Ann_kMT])
	SELECT
		x.[MethodologyId],
		x.[SubmissionId],
		@SuppWashOil,
		@PyroFuelOil,
		COALESCE(q.[Quantity_kMT], 0.0) - COALESCE(r.[Recovered_Dur_kMT], 0.0),
		(COALESCE(q.[Quantity_kMT], 0.0) - COALESCE(r.[Recovered_Dur_kMT], 0.0)) * s.[_Duration_Multiplier]
	FROM (VALUES
		(@MethodologyId, @SubmissionId)
		)												x ([MethodologyId], [SubmissionId])
	INNER JOIN [fact].[Submissions]						s
		ON	s.[SubmissionId]	= x.[SubmissionId]
	LEFT OUTER JOIN [calc].[SupplementalRecovered]		r
		ON	r.[MethodologyId]	= x.[MethodologyId]
		AND	r.[SubmissionId]	= x.[SubmissionId]
		AND	r.[StreamId]		= @SuppWashOil
	LEFT OUTER JOIN [fact].[StreamQuantity]				q
		ON	q.[SubmissionId]	= x.[SubmissionId]
		AND	q.[StreamId]		= r.[StreamId]
	WHERE COALESCE(q.[Quantity_kMT], 0.0) - COALESCE(r.[Recovered_Dur_kMT], 0.0) > 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO