﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_PyrolysisHvc]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [calc].[StandardEnergy_PyrolysisHvc]([MethodologyId], [SubmissionId], [ContainedProduct_Dur_kMT], [Recovered_Dur_kMT], [PyrolysisProduct_Dur_kMT], [PyrolysisProduct_Dur_klb], [HvcYieldDivisor_kLbDay])
	SELECT
		b.[MethodologyId],
		s.[SubmissionId],

		SUM(p.[ContainedProduct_Dur_kMT])												[ContainedProduct_Dur_kMT],
		SUM(r.[Recovered_Dur_kMT])														[Recovered_Dur_kMT],
		(SUM(p.[ContainedProduct_Dur_kMT]) - COALESCE(SUM(r.[Recovered_Dur_kMT]), 0.0))	[PyrolysisProduct_Dur_kMT],
		(SUM(p.[ContainedProduct_Dur_kMT]) - COALESCE(SUM(r.[Recovered_Dur_kMT]), 0.0)) * 2204.6
																						[PyrolysisProduct_Dur_klb],
		(SUM(p.[ContainedProduct_Dur_kMT]) - COALESCE(SUM(r.[Recovered_Dur_kMT]), 0.0)) * 2204.6 / s.[_Duration_Days] 
																						[PyrolysisProduct_klbDay]
	FROM [dim].[Component_Bridge]								b
	CROSS JOIN [fact].[Submissions]								s
	LEFT OUTER JOIN	[calc].[ContainedProduct_CompositeHVC]		p WITH (NOEXPAND)
		ON	p.[MethodologyId]	= b.[MethodologyId]
		AND	p.[ComponentId]		= b.[DescendantId]
		AND	p.[SubmissionId]	= s.[SubmissionId]
	LEFT OUTER JOIN	[calc].[SupplementalRecovered_Composite]	r WITH (NOEXPAND)
		ON	r.[MethodologyId]	= b.[MethodologyId]
		AND	r.[ComponentId]		= b.[DescendantId]
		AND	r.[SubmissionId]	= s.[SubmissionId]
	WHERE	b.[ComponentId]		= [dim].[Return_ComponentId]('ProdHVC')
		AND	b.[MethodologyId]	= @MethodologyId
		AND	s.[SubmissionId]	= @SubmissionId
	GROUP BY
		b.[MethodologyId],
		s.[SubmissionId],
		s.[_Duration_Days]
	HAVING
		(SUM(p.[ContainedProduct_Dur_kMT]) - COALESCE(SUM(r.[Recovered_Dur_kMT]), 0.0)) <> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO