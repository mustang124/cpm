﻿CREATE PROCEDURE [fact].[Insert_FacilitiesTrains]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[FacilitiesTrains]([SubmissionId], [Train_Count])
	SELECT
		t.[SubmissionId],
		t.[Train_Count]
	FROM [stage].[FacilitiesTrains]			t
	INNER JOIN [stage].[Submissions]		z
		ON	z.[SubmissionId] = t.[SubmissionId]
	WHERE	t.[SubmissionId] = @SubmissionId
		AND	t.Train_Count	>= 1;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;