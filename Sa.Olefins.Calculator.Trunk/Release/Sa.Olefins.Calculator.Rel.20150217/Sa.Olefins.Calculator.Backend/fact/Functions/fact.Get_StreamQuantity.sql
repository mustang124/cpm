﻿CREATE FUNCTION [fact].[Get_StreamQuantity]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT]
	FROM [fact].[StreamQuantity]		q
	WHERE	q.[SubmissionId] = @SubmissionId
);