﻿CREATE FUNCTION [auth].[Return_CompanyId]
(
	@CompanyName	NVARCHAR(128)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT
		@Id = c.[CompanyId]
	FROM [auth].[Companies]	c
	WHERE	c.[CompanyName] = @CompanyName;

	RETURN @Id;

END;