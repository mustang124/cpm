﻿CREATE FUNCTION [auth].[Get_LoginsByCompany_Active]
(
	@CompanyId		INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		lbc.[JoinId],
		lbc.[CompanyId],
		lbc.[LoginId],
		lbc.[CompanyName],
		lbc.[LoginTag],
		lbc.[NameFirst],
		lbc.[NameLast],
		lbc.[eMail],
		lbc.[_NameComma],
		lbc.[_NameFull],
		lbc.[RoleId],
		lbc.[RoleLevel]
	FROM [auth].[LoginsByCompany_Active]	lbc WITH (NOEXPAND)
	WHERE	lbc.[CompanyId]	= @CompanyId
);