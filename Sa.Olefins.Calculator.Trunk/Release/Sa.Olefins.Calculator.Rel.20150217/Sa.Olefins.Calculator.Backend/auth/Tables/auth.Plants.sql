﻿CREATE TABLE [auth].[Plants]
(
	[PlantId]				INT					NOT	NULL	IDENTITY (1, 1),
	[CompanyId]				INT						NULL	CONSTRAINT [FK_Plants_Companies]			REFERENCES [auth].[Companies] ([CompanyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[PlantName]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [CL_Plants_PlantName]			CHECK(LEN([PlantName]) > 0),
															CONSTRAINT [UK_Plants_PlantName]			UNIQUE NONCLUSTERED ([PlantName]),
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_Plants_Active]				DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Plants_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Plants_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Plants_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Plants_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Plants]						PRIMARY KEY CLUSTERED ([PlantId] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Plants_Active]
ON [auth].[Plants]([PlantId] ASC)
INCLUDE ([PlantName])
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_Plants_u]
ON [auth].[Plants]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[Plants]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[Plants].[PlantId]	= INSERTED.[PlantId];

END;
GO