﻿CREATE PROCEDURE [auth].[Insert_PlantCalculationLimits]
(
	@PlantID				INT,

	@CalcsPerPeriod_Count	INT	= NULL,
	@PeriodLen_Days			INT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	IF (@CalcsPerPeriod_Count IS NULL) SET @CalcsPerPeriod_Count = 4;
	--SELECT
	--	@CalcsPerPeriod_Count = CONVERT(INT, REPLACE(REPLACE(c.COLUMN_DEFAULT, '(', ''), ')', ''))
	--FROM INFORMATION_SCHEMA.COLUMNS c
	--WHERE c.[TABLE_SCHEMA] = 'auth'
	--AND	c.[TABLE_NAME] = 'PlantCalculationLimits'
	--AND	c.[COLUMN_NAME] = 'CalcsPerPeriod';

	IF (@PeriodLen_Days IS NULL) SET @PeriodLen_Days = 90;
	--SELECT
	--	@PeriodLen_Days = CONVERT(INT, REPLACE(REPLACE(c.COLUMN_DEFAULT, '(', ''), ')', ''))
	--FROM INFORMATION_SCHEMA.COLUMNS c
	--WHERE c.[TABLE_SCHEMA] = 'auth'
	--AND	c.[TABLE_NAME] = 'PlantCalculationLimits'
	--AND	c.[COLUMN_NAME] = 'PeriodLen_Days';

	INSERT INTO [auth].[PlantCalculationLimits]([PlantId], [CalcsPerPeriod_Count], [PeriodLen_Days])
	VALUES (@PlantID, @CalcsPerPeriod_Count, @PeriodLen_Days);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@PlantID:'		+ CONVERT(VARCHAR, @PlantID))
			+ (', @CalcsPerPeriod_Count:'	+ CONVERT(VARCHAR, @CalcsPerPeriod_Count))
			+ (', @PeriodLen_Days:'			+ CONVERT(VARCHAR, @PeriodLen_Days));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO