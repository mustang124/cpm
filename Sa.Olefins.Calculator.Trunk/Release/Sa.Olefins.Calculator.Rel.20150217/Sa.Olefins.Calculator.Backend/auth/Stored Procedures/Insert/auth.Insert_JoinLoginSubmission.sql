﻿CREATE PROCEDURE [auth].[Insert_JoinLoginSubmission]
(
	@LoginId			INT,
	@SubmissionId		INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO @Id([Id])
	SELECT [jls].[JoinId]
	FROM [auth].[JoinLoginSubmission]	[jls]
	WHERE	[jls].[LoginId]			= @LoginId
		AND	[jls].[SubmissionId]	= @SubmissionId;

	IF (NOT EXISTS(SELECT [Id] FROM @Id))
	BEGIN 

		INSERT INTO [auth].[JoinLoginSubmission]([LoginId], [SubmissionId])
		OUTPUT [INSERTED].[JoinId] INTO @Id([Id])
		SELECT
			[l].[LoginId],
			[z].[SubmissionId]
		FROM [auth].[Logins]							[l]
		CROSS APPLY [stage].[Submissions]				[z]
		LEFT OUTER JOIN [auth].[JoinLoginSubmission]	[jls]
			ON	[jls].[LoginId]			= [l].[LoginId]
			AND	[jls].[SubmissionId]	= [z].[SubmissionId]
		WHERE	[jls].[JoinId]		IS NULL
			AND	[l].[LoginId]		= @LoginId
			AND	[z].[SubmissionId]	= @SubmissionId;

	END;

	RETURN COALESCE((SELECT [Id] FROM @Id), 0);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@LoginId:'		+ CONVERT(VARCHAR, @LoginId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO