﻿CREATE PROCEDURE [auth].[Update_LoginPassWord]
(
	@LoginId		INT,

	@pSalt			BINARY(512),
	@pWord			VARBINARY(512)
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	UPDATE [auth].[LoginsPassWords]
	SET	[pSalt]	= @pSalt,
		[pWord]	= @pWord
	WHERE	[LoginId] = @LoginId
		AND	@pSalt <> [pSalt]
		AND @pWord <> [pWord];

	RETURN @LoginId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@LoginId:'		+ CONVERT(VARCHAR, @LoginId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO