﻿CREATE TABLE [xls].[StandardEnergy_PyrolysisHvc]
(
	[Refnum]					VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_StandardEnergy_PyrolysisHvc_Refnum]									CHECK([Refnum] <> ''),

	[ContainedProduct_Dur_kMT]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_ContainedProduct_Dur_kMT_MinIncl_0.0]	CHECK([ContainedProduct_Dur_kMT] >= 0.0),
	[Recovered_Dur_kMT]			FLOAT					NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_Recovered_Dur_kMT_MinIncl_0.0]			CHECK([Recovered_Dur_kMT] >= 0.0),
	[PyrolysisProduct_Dur_kMT]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_PyrolysisProduct_Dur_kMT_MinIncl_0.0]	CHECK([PyrolysisProduct_Dur_kMT] >= 0.0),
	[PyrolysisProduct_Dur_klb]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_PyrolysisProduct_Dur_klb_MinIncl_0.0]	CHECK([PyrolysisProduct_Dur_klb] >= 0.0),

	[HvcYieldDivisor_kLbDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_PyrolysisHvc_HvcYieldDivisor_kLbDay_MinIncl_0.0]		CHECK([HvcYieldDivisor_kLbDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModified]								DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModifiedHost]							DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModifiedUser]							DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_PyrolysisHvc_tsModifiedApp]							DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_PyrolysisHvc]		PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_StandardEnergy_PyrolysisHvc_u]
ON [xls].[StandardEnergy_PyrolysisHvc]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StandardEnergy_PyrolysisHvc]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StandardEnergy_PyrolysisHvc].[Refnum]	= INSERTED.[Refnum];

END;
GO