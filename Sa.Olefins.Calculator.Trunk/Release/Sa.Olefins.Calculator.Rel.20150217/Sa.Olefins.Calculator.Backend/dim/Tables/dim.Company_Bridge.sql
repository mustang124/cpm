﻿CREATE TABLE [dim].[Company_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Company_Bridge_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[CompanyId]				INT					NOT	NULL	CONSTRAINT [FK_Company_Bridge_CompanyId]			REFERENCES [dim].[Company_LookUp] ([CompanyId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Company_Bridge_Parent_Ancestor]		FOREIGN KEY ([MethodologyId], [CompanyId])
																												REFERENCES [dim].[Company_Parent] ([MethodologyId], [CompanyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_Company_Bridge_DescendantID]			REFERENCES [dim].[Company_LookUp] ([CompanyId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Company_Bridge_Parent_Descendant]	FOREIGN KEY ([MethodologyId], [DescendantId])
																												REFERENCES [dim].[Company_Parent] ([MethodologyId], [CompanyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_Company_Bridge_DescendantOperator]	DEFAULT '~'
															CONSTRAINT [FK_Company_Bridge_DescendantOperator]	REFERENCES [dim].[Operator] ([Operator])								ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Company_Bridge_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_Bridge_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_Bridge_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_Bridge_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Company_Bridge]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [CompanyId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Company_Bridge_u]
ON [dim].[Company_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Company_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Company_Bridge].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Company_Bridge].[CompanyId]		= INSERTED.[CompanyId]
		AND	[dim].[Company_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
GO