﻿CREATE TABLE [dim].[HydroTreaterType_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_HydroTreaterType_Parent_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	[HydroTreaterTypeId]	INT					NOT	NULL	CONSTRAINT [FK_HydroTreaterType_Parent_LookUp_HydroTreaterType]		REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_HydroTreaterType_Parent_LookUp_Parent]				REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_HydroTreaterType_Parent_Parent]						FOREIGN KEY ([MethodologyId], [ParentId])
																																REFERENCES [dim].[HydroTreaterType_Parent] ([MethodologyId], [HydroTreaterTypeId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Parent_Operator]					DEFAULT ('+')
															CONSTRAINT [FK_HydroTreaterType_Parent_Operator]					REFERENCES [dim].[Operator] ([Operator])												ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Parent_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Parent_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Parent_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Parent_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_HydroTreaterType_Parent]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC)
);
GO

CREATE TRIGGER [dim].[t_HydroTreaterType_Parent_u]
ON [dim].[HydroTreaterType_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[HydroTreaterType_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[HydroTreaterType_Parent].[MethodologyId]			= INSERTED.[MethodologyId]
		AND	[dim].[HydroTreaterType_Parent].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId];

END;
GO