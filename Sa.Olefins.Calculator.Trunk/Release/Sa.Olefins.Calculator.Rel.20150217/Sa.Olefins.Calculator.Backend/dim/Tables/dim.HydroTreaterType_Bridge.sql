﻿CREATE TABLE [dim].[HydroTreaterType_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_HydroTreaterType_Bridge_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	[HydroTreaterTypeId]	INT					NOT	NULL	CONSTRAINT [FK_HydroTreaterType_Bridge_HydroTreaterTypeId]		REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_HydroTreaterType_Bridge_Parent_Ancestor]			FOREIGN KEY ([MethodologyId], [HydroTreaterTypeId])
																															REFERENCES [dim].[HydroTreaterType_Parent] ([MethodologyId], [HydroTreaterTypeId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_HydroTreaterType_Bridge_DescendantID]			REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_HydroTreaterType_Bridge_Parent_Descendant]		FOREIGN KEY ([MethodologyId], [DescendantId])
																															REFERENCES [dim].[HydroTreaterType_Parent] ([MethodologyId], [HydroTreaterTypeId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Bridge_DescendantOperator]		DEFAULT ('+')
															CONSTRAINT [FK_HydroTreaterType_Bridge_DescendantOperator]		REFERENCES [dim].[Operator] ([Operator])												ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Bridge_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Bridge_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Bridge_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydroTreaterType_Bridge_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_HydroTreaterType_Bridge]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_HydroTreaterType_Bridge_u]
ON [dim].[HydroTreaterType_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[HydroTreaterType_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[HydroTreaterType_Bridge].[MethodologyId]			= INSERTED.[MethodologyId]
		AND	[dim].[HydroTreaterType_Bridge].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId]
		AND	[dim].[HydroTreaterType_Bridge].[DescendantId]			= INSERTED.[DescendantId];

END;
GO