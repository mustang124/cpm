﻿CREATE TABLE [dim].[Company_LookUp]
(
	[CompanyId]				INT					NOT NULL	IDENTITY (1, 1),

	[CompanyTag]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Company_LookUp_CompanyTag]			CHECK ([CompanyTag]		<> ''),
															CONSTRAINT [UK_Company_LookUp_CompanyTag]			UNIQUE NONCLUSTERED ([CompanyTag]),
	[CompanyName]			VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Company_LookUp_CompanyName]			CHECK ([CompanyName]	<> ''),
															CONSTRAINT [UK_Company_LookUp_CompanyName]			UNIQUE NONCLUSTERED ([CompanyName]),
	[CompanyDetail]			VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Company_LookUp_CompanyDetail]		CHECK ([CompanyDetail]	<> ''),
															CONSTRAINT [UK_Company_LookUp_CompanyDetail]		UNIQUE NONCLUSTERED ([CompanyDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Company_LookUp_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_LookUp_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_LookUp_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Company_LookUp_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Company_LookUp]				PRIMARY KEY CLUSTERED ([CompanyId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Company_LookUp_u]
ON [dim].[Company_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Company_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Company_LookUp].[CompanyId]		= INSERTED.[CompanyId];

END;
GO