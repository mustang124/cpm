﻿CREATE PROCEDURE [web].[Get_PlantsByCompany_Active]
(
	@CompanyId		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		pbc.[JoinId],
		pbc.[CompanyId],
		pbc.[PlantId],
		pbc.[CompanyName],
		pbc.[PlantName]
	FROM [auth].[Get_PlantsByCompany_Active](@CompanyId)	pbc
	ORDER BY
		pbc.[PlantName]	ASC;

END;