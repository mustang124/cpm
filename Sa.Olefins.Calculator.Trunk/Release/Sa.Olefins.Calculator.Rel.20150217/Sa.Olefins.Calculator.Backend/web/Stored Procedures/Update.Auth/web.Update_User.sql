﻿CREATE PROCEDURE [web].[Update_User]
(
	@UserID				INT				= NULL,

	@Username			NVARCHAR(254)	= NULL,

	@FirstName			NVARCHAR(128)	= NULL,
	@LastName			NVARCHAR(128)	= NULL,
	@Email				NVARCHAR(254)	= NULL,

	@Password			VARBINARY(512)	= NULL,
	@Salt				BINARY(512)		= NULL,

	@SecurityLevelID	INT				= NULL,
	@Active				BIT				= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LoginTag	NVARCHAR(254)	= @Username;
	DECLARE @LoginId	INT				= @UserID;
	DECLARE @RoleId		INT				= [auth].[Return_RoleId](@SecurityLevelID, NULL);
	
	IF (@LoginId IS NULL)
	SET @LoginId = [auth].[Return_LoginId](@LoginTag)

	IF (@Email IS NULL)
	SET @Email = @Username;

	IF (@LoginId IS NOT NULL)
	BEGIN

		EXECUTE @LoginId = [auth].[Update_LoginId] @LoginId, @Active;
		EXECUTE [auth].[Update_LoginAttributes]	@LoginId, @LastName, @FirstName, @Email, @RoleId;
		EXECUTE [auth].[Update_LoginPassWord] @LoginId, @Salt, @Password;

	END
	ELSE
	BEGIN

		EXECUTE @LoginId = [auth].[Insert_LoginId] @LoginTag;
		EXECUTE [auth].[Insert_LoginAttributes] @LoginId, @LastName, @FirstName, @Email, @RoleId;
		EXECUTE [auth].[Insert_LoginPassWord] @LoginId, @Salt, @Password;
	
	END;

	SELECT @LoginId;
	RETURN @LoginId;

END;