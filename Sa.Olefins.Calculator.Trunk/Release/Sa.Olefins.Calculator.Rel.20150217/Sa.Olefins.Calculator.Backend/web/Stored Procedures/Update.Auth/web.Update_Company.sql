﻿CREATE PROCEDURE [web].[Update_Company]
(
	@CompanyId		INT				= NULL,

	@CompanyName	VARCHAR(128)	= NULL,
	@Active			BIT				= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (@CompanyId IS NULL)
	SET @CompanyId = [auth].[Return_CompanyId](@CompanyName);

	IF (@CompanyId IS NOT NULL)
	BEGIN
		EXECUTE @CompanyId = [auth].[Update_Company] @CompanyId, @CompanyName, NULL, @Active;
	END
	ELSE
	BEGIN
		EXECUTE @CompanyId = [auth].[Insert_Company] @CompanyName, NULL;
	END;

	SELECT @CompanyId;
	RETURN @CompanyId;

END;