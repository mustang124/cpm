﻿CREATE TABLE [ante].[ReportMap_Standard]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_ReportMap_Standard_Methodology]		REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StandardId]			INT					NOT NULL	CONSTRAINT [FK_ReportMap_Standard_Standard]			REFERENCES [dim].[Standard_LookUp] ([StandardId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Report_Prefix]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_ReportMap_Standard_Report_Prefix]	CHECK([Report_Prefix] <> ''),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ReportMap_Standard_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportMap_Standard_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportMap_Standard_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportMap_Standard_tsModifiedApp]	DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ReportMap_Standard]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC)
);
GO

CREATE TRIGGER [ante].[t_ReportMap_Standard_u]
ON [ante].[ReportMap_Standard]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ReportMap_Standard]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[ReportMap_Standard].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[ReportMap_Standard].[StandardId]	= INSERTED.[StandardId];

END;
GO