﻿CREATE TABLE [ante].[NicenessStreams]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_NicenessStreams_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_NicenessStreams_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[EmptyComposition]		FLOAT					NULL	CONSTRAINT [CR_NicenessStreams_EmptyComposition_MinIncl_0.0]	CHECK([EmptyComposition] >= 0.0),
	[Divisor]				FLOAT				NOT	NULL	CONSTRAINT [CR_NicenessStreams_Divisor_MinIncl_0.0]				CHECK([Divisor]	>= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_NicenessStreams_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessStreams_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessStreams_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_NicenessStreams_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_NicenessStreams]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_NicenessStreams_u]
ON [ante].[NicenessStreams]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessStreams]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[NicenessStreams].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[NicenessStreams].[StreamId]			= INSERTED.[StreamId];

END;
GO