﻿CREATE TABLE [ante].[StdEnergyHydroTreater]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_StdEnergyHydroTreater_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[HydroTreaterTypeId]	INT					NOT	NULL	CONSTRAINT [FK_StdEnergyHydroTreater_HydroTreater_LookUp]			REFERENCES [dim].[HydroTreaterType_LookUp]([HydroTreaterTypeId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Energy_kBtuBbl]		FLOAT				NOT	NULL	CONSTRAINT [CR_StdEnergyHydroTreater_Energy_kBtuBbl_MinIncl_0.0]	CHECK([Energy_kBtuBbl] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StdEnergyHydroTreater_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StdEnergyHydroTreater_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StdEnergyHydroTreater_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StdEnergyHydroTreater_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StdEnergyHydroTreater]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC)
);
GO

CREATE TRIGGER [ante].[t_StdEnergyHydroTreater_u]
ON [ante].[StdEnergyHydroTreater]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[StdEnergyHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[StdEnergyHydroTreater].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[StdEnergyHydroTreater].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId];

END;
GO