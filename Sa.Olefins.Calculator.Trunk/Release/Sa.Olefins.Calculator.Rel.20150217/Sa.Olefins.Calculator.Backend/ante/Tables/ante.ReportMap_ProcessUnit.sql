﻿CREATE TABLE [ante].[ReportMap_ProcessUnit]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_ReportMap_ProcessUnit_Methodology]		REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ProcessUnitId]			INT					NOT NULL	CONSTRAINT [FK_ReportMap_ProcessUnit_ProcessUnit]		REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Report_Suffix]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_ReportMap_ProcessUnit_Report_Suffix]		CHECK([Report_Suffix] <> ''),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ReportMap_ProcessUnit_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportMap_ProcessUnit_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportMap_ProcessUnit_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportMap_ProcessUnit_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ReportMap_ProcessUnit]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [ante].[t_ReportMap_ProcessUnit_u]
ON [ante].[ReportMap_ProcessUnit]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ReportMap_ProcessUnit]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[ReportMap_ProcessUnit].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[ReportMap_ProcessUnit].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;
GO