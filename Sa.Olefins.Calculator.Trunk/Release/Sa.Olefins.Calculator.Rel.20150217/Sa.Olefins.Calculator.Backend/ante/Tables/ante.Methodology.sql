﻿CREATE TABLE [ante].[Methodology]
(
	[MethodologyId]			INT					NOT NULL	IDENTITY (1, 1),

	[MethodologyTag]		VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Methodology_LookUp_MethodologyTag]		CHECK ([MethodologyTag] <> ''),
															CONSTRAINT [UK_Methodology_LookUp_MethodologyTag]		UNIQUE NONCLUSTERED ([MethodologyTag]),
	[MethodologyName]		VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Methodology_LookUp_MethodologyName]		CHECK ([MethodologyName] <> ''),
															CONSTRAINT [UK_Methodology_LookUp_MethodologyName]		UNIQUE NONCLUSTERED ([MethodologyName]),
	[MethodologyDetail]		VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Methodology_LookUp_MethodologyDetail]	CHECK ([MethodologyDetail] <> ''),
															CONSTRAINT [UK_Methodology_LookUp_MethodologyDetail]	UNIQUE NONCLUSTERED ([MethodologyDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Methodology_LookUp_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Methodology_LookUp_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Methodology_LookUp_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Methodology_LookUp_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Methodology_LookUp]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC)
);
GO

CREATE TRIGGER [ante].[t_Methodology_u]
ON [ante].[Methodology]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Methodology]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Methodology].[MethodologyId]	= INSERTED.[MethodologyId];

END;
GO