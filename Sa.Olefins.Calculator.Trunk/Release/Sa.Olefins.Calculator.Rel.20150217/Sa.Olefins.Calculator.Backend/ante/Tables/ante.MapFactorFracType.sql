﻿CREATE TABLE [ante].[MapFactorFracType]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapFactorFracType_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_MapFactorFracType_Stream_LookUp]				REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_MapFactorFracType_Factor_LookUp]				REFERENCES [dim].[Factor_LookUp] ([FactorId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapFactorFracType_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorFracType_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorFracType_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorFracType_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapFactorFracType]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapFactorFracType_u]
ON [ante].[MapFactorFracType]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorFracType]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorFracType].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorFracType].[StreamId]		= INSERTED.[StreamId];

END;
GO
