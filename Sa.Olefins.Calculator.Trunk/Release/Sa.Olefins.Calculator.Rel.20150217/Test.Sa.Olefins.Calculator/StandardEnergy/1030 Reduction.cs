﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.StandardEnergy
{
    [TestClass()]
    public class _1030_Reduction : SqlDatabaseTestClass
    {

        public _1030_Reduction()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void StandardEnergy_Reduction_Quantity_kMT()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Reduction_Quantity_kMTData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Reduction_Quantity_bbl()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Reduction_Quantity_bblData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Reduction_MBtu()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Reduction_MBtuData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Reduction_MBtuDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Reduction_MBtuDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }




        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Reduction_Quantity_kMT_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1030_Reduction));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Reduction_Quantity_kMT_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Reduction_Quantity_kMT_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Reduction_Quantity_bbl_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Reduction_Quantity_bbl_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Reduction_Quantity_bbl_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Reduction_MBtu_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Reduction_MBtu_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Reduction_MBtu_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Reduction_MBtuDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Reduction_MBtuDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Reduction_MBtuDay_EmptySet;
            this.StandardEnergy_Reduction_Quantity_kMTData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Reduction_Quantity_bblData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Reduction_MBtuData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Reduction_MBtuDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            StandardEnergy_Reduction_Quantity_kMT_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Reduction_Quantity_kMT_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Reduction_Quantity_kMT_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Reduction_Quantity_bbl_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Reduction_Quantity_bbl_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Reduction_Quantity_bbl_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Reduction_MBtu_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Reduction_MBtu_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Reduction_MBtu_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Reduction_MBtuDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Reduction_MBtuDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Reduction_MBtuDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // StandardEnergy_Reduction_Quantity_kMT_TestAction
            // 
            StandardEnergy_Reduction_Quantity_kMT_TestAction.Conditions.Add(StandardEnergy_Reduction_Quantity_kMT_NotEmpty);
            StandardEnergy_Reduction_Quantity_kMT_TestAction.Conditions.Add(StandardEnergy_Reduction_Quantity_kMT_EmptySet);
            resources.ApplyResources(StandardEnergy_Reduction_Quantity_kMT_TestAction, "StandardEnergy_Reduction_Quantity_kMT_TestAction");
            // 
            // StandardEnergy_Reduction_Quantity_kMT_NotEmpty
            // 
            StandardEnergy_Reduction_Quantity_kMT_NotEmpty.Enabled = true;
            StandardEnergy_Reduction_Quantity_kMT_NotEmpty.Name = "StandardEnergy_Reduction_Quantity_kMT_NotEmpty";
            StandardEnergy_Reduction_Quantity_kMT_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Reduction_Quantity_kMT_EmptySet
            // 
            StandardEnergy_Reduction_Quantity_kMT_EmptySet.Enabled = true;
            StandardEnergy_Reduction_Quantity_kMT_EmptySet.Name = "StandardEnergy_Reduction_Quantity_kMT_EmptySet";
            StandardEnergy_Reduction_Quantity_kMT_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Reduction_Quantity_bbl_TestAction
            // 
            StandardEnergy_Reduction_Quantity_bbl_TestAction.Conditions.Add(StandardEnergy_Reduction_Quantity_bbl_NotEmpty);
            StandardEnergy_Reduction_Quantity_bbl_TestAction.Conditions.Add(StandardEnergy_Reduction_Quantity_bbl_EmptySet);
            resources.ApplyResources(StandardEnergy_Reduction_Quantity_bbl_TestAction, "StandardEnergy_Reduction_Quantity_bbl_TestAction");
            // 
            // StandardEnergy_Reduction_Quantity_bbl_NotEmpty
            // 
            StandardEnergy_Reduction_Quantity_bbl_NotEmpty.Enabled = true;
            StandardEnergy_Reduction_Quantity_bbl_NotEmpty.Name = "StandardEnergy_Reduction_Quantity_bbl_NotEmpty";
            StandardEnergy_Reduction_Quantity_bbl_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Reduction_Quantity_bbl_EmptySet
            // 
            StandardEnergy_Reduction_Quantity_bbl_EmptySet.Enabled = true;
            StandardEnergy_Reduction_Quantity_bbl_EmptySet.Name = "StandardEnergy_Reduction_Quantity_bbl_EmptySet";
            StandardEnergy_Reduction_Quantity_bbl_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Reduction_MBtu_TestAction
            // 
            StandardEnergy_Reduction_MBtu_TestAction.Conditions.Add(StandardEnergy_Reduction_MBtu_NotEmpty);
            StandardEnergy_Reduction_MBtu_TestAction.Conditions.Add(StandardEnergy_Reduction_MBtu_EmptySet);
            resources.ApplyResources(StandardEnergy_Reduction_MBtu_TestAction, "StandardEnergy_Reduction_MBtu_TestAction");
            // 
            // StandardEnergy_Reduction_MBtu_NotEmpty
            // 
            StandardEnergy_Reduction_MBtu_NotEmpty.Enabled = true;
            StandardEnergy_Reduction_MBtu_NotEmpty.Name = "StandardEnergy_Reduction_MBtu_NotEmpty";
            StandardEnergy_Reduction_MBtu_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Reduction_MBtu_EmptySet
            // 
            StandardEnergy_Reduction_MBtu_EmptySet.Enabled = true;
            StandardEnergy_Reduction_MBtu_EmptySet.Name = "StandardEnergy_Reduction_MBtu_EmptySet";
            StandardEnergy_Reduction_MBtu_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Reduction_MBtuDay_TestAction
            // 
            StandardEnergy_Reduction_MBtuDay_TestAction.Conditions.Add(StandardEnergy_Reduction_MBtuDay_NotEmpty);
            StandardEnergy_Reduction_MBtuDay_TestAction.Conditions.Add(StandardEnergy_Reduction_MBtuDay_EmptySet);
            resources.ApplyResources(StandardEnergy_Reduction_MBtuDay_TestAction, "StandardEnergy_Reduction_MBtuDay_TestAction");
            // 
            // StandardEnergy_Reduction_MBtuDay_NotEmpty
            // 
            StandardEnergy_Reduction_MBtuDay_NotEmpty.Enabled = true;
            StandardEnergy_Reduction_MBtuDay_NotEmpty.Name = "StandardEnergy_Reduction_MBtuDay_NotEmpty";
            StandardEnergy_Reduction_MBtuDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Reduction_MBtuDay_EmptySet
            // 
            StandardEnergy_Reduction_MBtuDay_EmptySet.Enabled = true;
            StandardEnergy_Reduction_MBtuDay_EmptySet.Name = "StandardEnergy_Reduction_MBtuDay_EmptySet";
            StandardEnergy_Reduction_MBtuDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Reduction_Quantity_kMTData
            // 
            this.StandardEnergy_Reduction_Quantity_kMTData.PosttestAction = null;
            this.StandardEnergy_Reduction_Quantity_kMTData.PretestAction = null;
            this.StandardEnergy_Reduction_Quantity_kMTData.TestAction = StandardEnergy_Reduction_Quantity_kMT_TestAction;
            // 
            // StandardEnergy_Reduction_Quantity_bblData
            // 
            this.StandardEnergy_Reduction_Quantity_bblData.PosttestAction = null;
            this.StandardEnergy_Reduction_Quantity_bblData.PretestAction = null;
            this.StandardEnergy_Reduction_Quantity_bblData.TestAction = StandardEnergy_Reduction_Quantity_bbl_TestAction;
            // 
            // StandardEnergy_Reduction_MBtuData
            // 
            this.StandardEnergy_Reduction_MBtuData.PosttestAction = null;
            this.StandardEnergy_Reduction_MBtuData.PretestAction = null;
            this.StandardEnergy_Reduction_MBtuData.TestAction = StandardEnergy_Reduction_MBtu_TestAction;
            // 
            // StandardEnergy_Reduction_MBtuDayData
            // 
            this.StandardEnergy_Reduction_MBtuDayData.PosttestAction = null;
            this.StandardEnergy_Reduction_MBtuDayData.PretestAction = null;
            this.StandardEnergy_Reduction_MBtuDayData.TestAction = StandardEnergy_Reduction_MBtuDay_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardEnergy_Reduction_Quantity_kMTData;
        private SqlDatabaseTestActions StandardEnergy_Reduction_Quantity_bblData;
        private SqlDatabaseTestActions StandardEnergy_Reduction_MBtuData;
        private SqlDatabaseTestActions StandardEnergy_Reduction_MBtuDayData;
    }
}
