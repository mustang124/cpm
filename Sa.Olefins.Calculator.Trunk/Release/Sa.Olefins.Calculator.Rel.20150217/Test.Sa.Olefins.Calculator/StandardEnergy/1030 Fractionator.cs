﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.StandardEnergy
{
    [TestClass()]
    public class _1030_Fractionator : SqlDatabaseTestClass
    {

        public _1030_Fractionator()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void StandardEnergy_Fractionator_MBtuDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Fractionator_MBtuDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Fractionator_bblDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Fractionator_bblDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Fractionator_bbl()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Fractionator_bblData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Fractionator_Convert_bblTon()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Fractionator_Convert_bblTonData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Fractionator_Throughput_kMT()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Fractionator_Throughput_kMTData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }





        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Fractionator_MBtuDay_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1030_Fractionator));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Fractionator_bblDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Fractionator_bbl_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Fractionator_Convert_bblTon_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Fractionator_Throughput_kMT_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Fractionator_bbl_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Fractionator_bbl_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Fractionator_bblDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Fractionator_bblDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Fractionator_Convert_bblTon_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Fractionator_Convert_bblTon_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Fractionator_MBtuDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Fractionator_MBtuDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Fractionator_Throughput_kMT_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Fractionator_Throughput_kMT_EmptySet;
            this.StandardEnergy_Fractionator_MBtuDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Fractionator_bblDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Fractionator_bblData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Fractionator_Convert_bblTonData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Fractionator_Throughput_kMTData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            StandardEnergy_Fractionator_MBtuDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Fractionator_bblDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Fractionator_bbl_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Fractionator_Convert_bblTon_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Fractionator_Throughput_kMT_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Fractionator_bbl_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Fractionator_bbl_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Fractionator_bblDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Fractionator_bblDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Fractionator_Convert_bblTon_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Fractionator_Convert_bblTon_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Fractionator_MBtuDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Fractionator_MBtuDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Fractionator_Throughput_kMT_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Fractionator_Throughput_kMT_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // StandardEnergy_Fractionator_MBtuDayData
            // 
            this.StandardEnergy_Fractionator_MBtuDayData.PosttestAction = null;
            this.StandardEnergy_Fractionator_MBtuDayData.PretestAction = null;
            this.StandardEnergy_Fractionator_MBtuDayData.TestAction = StandardEnergy_Fractionator_MBtuDay_TestAction;
            // 
            // StandardEnergy_Fractionator_MBtuDay_TestAction
            // 
            StandardEnergy_Fractionator_MBtuDay_TestAction.Conditions.Add(StandardEnergy_Fractionator_MBtuDay_NotEmpty);
            StandardEnergy_Fractionator_MBtuDay_TestAction.Conditions.Add(StandardEnergy_Fractionator_MBtuDay_EmptySet);
            resources.ApplyResources(StandardEnergy_Fractionator_MBtuDay_TestAction, "StandardEnergy_Fractionator_MBtuDay_TestAction");
            // 
            // StandardEnergy_Fractionator_bblDayData
            // 
            this.StandardEnergy_Fractionator_bblDayData.PosttestAction = null;
            this.StandardEnergy_Fractionator_bblDayData.PretestAction = null;
            this.StandardEnergy_Fractionator_bblDayData.TestAction = StandardEnergy_Fractionator_bblDay_TestAction;
            // 
            // StandardEnergy_Fractionator_bblDay_TestAction
            // 
            StandardEnergy_Fractionator_bblDay_TestAction.Conditions.Add(StandardEnergy_Fractionator_bblDay_NotEmpty);
            StandardEnergy_Fractionator_bblDay_TestAction.Conditions.Add(StandardEnergy_Fractionator_bblDay_EmptySet);
            resources.ApplyResources(StandardEnergy_Fractionator_bblDay_TestAction, "StandardEnergy_Fractionator_bblDay_TestAction");
            // 
            // StandardEnergy_Fractionator_bblData
            // 
            this.StandardEnergy_Fractionator_bblData.PosttestAction = null;
            this.StandardEnergy_Fractionator_bblData.PretestAction = null;
            this.StandardEnergy_Fractionator_bblData.TestAction = StandardEnergy_Fractionator_bbl_TestAction;
            // 
            // StandardEnergy_Fractionator_bbl_TestAction
            // 
            StandardEnergy_Fractionator_bbl_TestAction.Conditions.Add(StandardEnergy_Fractionator_bbl_NotEmpty);
            StandardEnergy_Fractionator_bbl_TestAction.Conditions.Add(StandardEnergy_Fractionator_bbl_EmptySet);
            resources.ApplyResources(StandardEnergy_Fractionator_bbl_TestAction, "StandardEnergy_Fractionator_bbl_TestAction");
            // 
            // StandardEnergy_Fractionator_Convert_bblTonData
            // 
            this.StandardEnergy_Fractionator_Convert_bblTonData.PosttestAction = null;
            this.StandardEnergy_Fractionator_Convert_bblTonData.PretestAction = null;
            this.StandardEnergy_Fractionator_Convert_bblTonData.TestAction = StandardEnergy_Fractionator_Convert_bblTon_TestAction;
            // 
            // StandardEnergy_Fractionator_Convert_bblTon_TestAction
            // 
            StandardEnergy_Fractionator_Convert_bblTon_TestAction.Conditions.Add(StandardEnergy_Fractionator_Convert_bblTon_NotEmpty);
            StandardEnergy_Fractionator_Convert_bblTon_TestAction.Conditions.Add(StandardEnergy_Fractionator_Convert_bblTon_EmptySet);
            resources.ApplyResources(StandardEnergy_Fractionator_Convert_bblTon_TestAction, "StandardEnergy_Fractionator_Convert_bblTon_TestAction");
            // 
            // StandardEnergy_Fractionator_Throughput_kMTData
            // 
            this.StandardEnergy_Fractionator_Throughput_kMTData.PosttestAction = null;
            this.StandardEnergy_Fractionator_Throughput_kMTData.PretestAction = null;
            this.StandardEnergy_Fractionator_Throughput_kMTData.TestAction = StandardEnergy_Fractionator_Throughput_kMT_TestAction;
            // 
            // StandardEnergy_Fractionator_Throughput_kMT_TestAction
            // 
            StandardEnergy_Fractionator_Throughput_kMT_TestAction.Conditions.Add(StandardEnergy_Fractionator_Throughput_kMT_NotEmpty);
            StandardEnergy_Fractionator_Throughput_kMT_TestAction.Conditions.Add(StandardEnergy_Fractionator_Throughput_kMT_EmptySet);
            resources.ApplyResources(StandardEnergy_Fractionator_Throughput_kMT_TestAction, "StandardEnergy_Fractionator_Throughput_kMT_TestAction");
            // 
            // StandardEnergy_Fractionator_bbl_NotEmpty
            // 
            StandardEnergy_Fractionator_bbl_NotEmpty.Enabled = true;
            StandardEnergy_Fractionator_bbl_NotEmpty.Name = "StandardEnergy_Fractionator_bbl_NotEmpty";
            StandardEnergy_Fractionator_bbl_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Fractionator_bbl_EmptySet
            // 
            StandardEnergy_Fractionator_bbl_EmptySet.Enabled = true;
            StandardEnergy_Fractionator_bbl_EmptySet.Name = "StandardEnergy_Fractionator_bbl_EmptySet";
            StandardEnergy_Fractionator_bbl_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Fractionator_bblDay_NotEmpty
            // 
            StandardEnergy_Fractionator_bblDay_NotEmpty.Enabled = true;
            StandardEnergy_Fractionator_bblDay_NotEmpty.Name = "StandardEnergy_Fractionator_bblDay_NotEmpty";
            StandardEnergy_Fractionator_bblDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Fractionator_bblDay_EmptySet
            // 
            StandardEnergy_Fractionator_bblDay_EmptySet.Enabled = true;
            StandardEnergy_Fractionator_bblDay_EmptySet.Name = "StandardEnergy_Fractionator_bblDay_EmptySet";
            StandardEnergy_Fractionator_bblDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Fractionator_Convert_bblTon_NotEmpty
            // 
            StandardEnergy_Fractionator_Convert_bblTon_NotEmpty.Enabled = true;
            StandardEnergy_Fractionator_Convert_bblTon_NotEmpty.Name = "StandardEnergy_Fractionator_Convert_bblTon_NotEmpty";
            StandardEnergy_Fractionator_Convert_bblTon_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Fractionator_Convert_bblTon_EmptySet
            // 
            StandardEnergy_Fractionator_Convert_bblTon_EmptySet.Enabled = true;
            StandardEnergy_Fractionator_Convert_bblTon_EmptySet.Name = "StandardEnergy_Fractionator_Convert_bblTon_EmptySet";
            StandardEnergy_Fractionator_Convert_bblTon_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Fractionator_MBtuDay_NotEmpty
            // 
            StandardEnergy_Fractionator_MBtuDay_NotEmpty.Enabled = true;
            StandardEnergy_Fractionator_MBtuDay_NotEmpty.Name = "StandardEnergy_Fractionator_MBtuDay_NotEmpty";
            StandardEnergy_Fractionator_MBtuDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Fractionator_MBtuDay_EmptySet
            // 
            StandardEnergy_Fractionator_MBtuDay_EmptySet.Enabled = true;
            StandardEnergy_Fractionator_MBtuDay_EmptySet.Name = "StandardEnergy_Fractionator_MBtuDay_EmptySet";
            StandardEnergy_Fractionator_MBtuDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Fractionator_Throughput_kMT_NotEmpty
            // 
            StandardEnergy_Fractionator_Throughput_kMT_NotEmpty.Enabled = true;
            StandardEnergy_Fractionator_Throughput_kMT_NotEmpty.Name = "StandardEnergy_Fractionator_Throughput_kMT_NotEmpty";
            StandardEnergy_Fractionator_Throughput_kMT_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Fractionator_Throughput_kMT_EmptySet
            // 
            StandardEnergy_Fractionator_Throughput_kMT_EmptySet.Enabled = true;
            StandardEnergy_Fractionator_Throughput_kMT_EmptySet.Name = "StandardEnergy_Fractionator_Throughput_kMT_EmptySet";
            StandardEnergy_Fractionator_Throughput_kMT_EmptySet.ResultSet = 2;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardEnergy_Fractionator_MBtuDayData;
        private SqlDatabaseTestActions StandardEnergy_Fractionator_bblDayData;
        private SqlDatabaseTestActions StandardEnergy_Fractionator_bblData;
        private SqlDatabaseTestActions StandardEnergy_Fractionator_Convert_bblTonData;
        private SqlDatabaseTestActions StandardEnergy_Fractionator_Throughput_kMTData;
    }
}
