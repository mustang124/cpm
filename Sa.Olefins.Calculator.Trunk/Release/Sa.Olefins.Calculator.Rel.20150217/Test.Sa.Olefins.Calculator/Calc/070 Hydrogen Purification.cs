﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _070_Hydrogen_Purification : SqlDatabaseTestClass
    {

        public _070_Hydrogen_Purification()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void HydrogenPurification_Component_WtPcnt()
        {
            SqlDatabaseTestActions testActions = this.HydrogenPurification_Component_WtPcntData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void HydrogenPurification_Component_kMT()
        {
            SqlDatabaseTestActions testActions = this.HydrogenPurification_Component_kMTData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void HydrogenPurification_Utilization_Pcnt()
        {
            SqlDatabaseTestActions testActions = this.HydrogenPurification_Utilization_PcntData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void HydrogenPurification_Unit_Count()
        {
            SqlDatabaseTestActions testActions = this.HydrogenPurification_Unit_CountData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void HydrogenPurification_Purification_MSCF()
        {
            SqlDatabaseTestActions testActions = this.HydrogenPurification_Purification_MSCFData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void HydrogenPurification_Purification_kSCF()
        {
            SqlDatabaseTestActions testActions = this.HydrogenPurification_Purification_kSCFData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void HydrogenPurification_Purification_Sales()
        {
            SqlDatabaseTestActions testActions = this.HydrogenPurification_Purification_SalesData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }







        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HydrogenPurification_Component_WtPcnt_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_070_Hydrogen_Purification));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HydrogenPurification_Component_WtPcnt_Results;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HydrogenPurification_Component_WtPcnt_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HydrogenPurification_Component_kMT_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HydrogenPurification_Component_kMT_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HydrogenPurification_Component_kMT_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HydrogenPurification_Utilization_Pcnt_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HydrogenPurification_Utilization_Pcnt_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HydrogenPurification_Utilization_Pcnt_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HydrogenPurification_Unit_Count_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HydrogenPurification_Unit_Count_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HydrogenPurification_Unit_Count_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HydrogenPurification_Purification_MSCF_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HydrogenPurification_Purification_MSCF_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HydrogenPurification_Purification_MSCF_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HydrogenPurification_Purification_kSCF_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HydrogenPurification_Purification_kSCF_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HydrogenPurification_Purification_kSCF_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction HydrogenPurification_Purification_Sales_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition HydrogenPurification_Purification_Sales_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition HydrogenPurification_Purification_Sales_EmptySet;
            this.HydrogenPurification_Component_WtPcntData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.HydrogenPurification_Component_kMTData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.HydrogenPurification_Utilization_PcntData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.HydrogenPurification_Unit_CountData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.HydrogenPurification_Purification_MSCFData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.HydrogenPurification_Purification_kSCFData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.HydrogenPurification_Purification_SalesData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            HydrogenPurification_Component_WtPcnt_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HydrogenPurification_Component_WtPcnt_Results = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HydrogenPurification_Component_WtPcnt_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            HydrogenPurification_Component_kMT_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HydrogenPurification_Component_kMT_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HydrogenPurification_Component_kMT_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            HydrogenPurification_Utilization_Pcnt_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HydrogenPurification_Utilization_Pcnt_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HydrogenPurification_Utilization_Pcnt_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            HydrogenPurification_Unit_Count_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HydrogenPurification_Unit_Count_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HydrogenPurification_Unit_Count_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            HydrogenPurification_Purification_MSCF_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HydrogenPurification_Purification_MSCF_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HydrogenPurification_Purification_MSCF_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            HydrogenPurification_Purification_kSCF_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HydrogenPurification_Purification_kSCF_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HydrogenPurification_Purification_kSCF_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            HydrogenPurification_Purification_Sales_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            HydrogenPurification_Purification_Sales_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            HydrogenPurification_Purification_Sales_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // HydrogenPurification_Component_WtPcnt_TestAction
            // 
            HydrogenPurification_Component_WtPcnt_TestAction.Conditions.Add(HydrogenPurification_Component_WtPcnt_Results);
            HydrogenPurification_Component_WtPcnt_TestAction.Conditions.Add(HydrogenPurification_Component_WtPcnt_EmptySet);
            resources.ApplyResources(HydrogenPurification_Component_WtPcnt_TestAction, "HydrogenPurification_Component_WtPcnt_TestAction");
            // 
            // HydrogenPurification_Component_WtPcnt_Results
            // 
            HydrogenPurification_Component_WtPcnt_Results.Enabled = true;
            HydrogenPurification_Component_WtPcnt_Results.Name = "HydrogenPurification_Component_WtPcnt_Results";
            HydrogenPurification_Component_WtPcnt_Results.ResultSet = 1;
            // 
            // HydrogenPurification_Component_WtPcnt_EmptySet
            // 
            HydrogenPurification_Component_WtPcnt_EmptySet.Enabled = true;
            HydrogenPurification_Component_WtPcnt_EmptySet.Name = "HydrogenPurification_Component_WtPcnt_EmptySet";
            HydrogenPurification_Component_WtPcnt_EmptySet.ResultSet = 2;
            // 
            // HydrogenPurification_Component_kMT_TestAction
            // 
            HydrogenPurification_Component_kMT_TestAction.Conditions.Add(HydrogenPurification_Component_kMT_NotEmpty);
            HydrogenPurification_Component_kMT_TestAction.Conditions.Add(HydrogenPurification_Component_kMT_EmptySet);
            resources.ApplyResources(HydrogenPurification_Component_kMT_TestAction, "HydrogenPurification_Component_kMT_TestAction");
            // 
            // HydrogenPurification_Component_kMT_NotEmpty
            // 
            HydrogenPurification_Component_kMT_NotEmpty.Enabled = true;
            HydrogenPurification_Component_kMT_NotEmpty.Name = "HydrogenPurification_Component_kMT_NotEmpty";
            HydrogenPurification_Component_kMT_NotEmpty.ResultSet = 1;
            // 
            // HydrogenPurification_Component_kMT_EmptySet
            // 
            HydrogenPurification_Component_kMT_EmptySet.Enabled = true;
            HydrogenPurification_Component_kMT_EmptySet.Name = "HydrogenPurification_Component_kMT_EmptySet";
            HydrogenPurification_Component_kMT_EmptySet.ResultSet = 2;
            // 
            // HydrogenPurification_Utilization_Pcnt_TestAction
            // 
            HydrogenPurification_Utilization_Pcnt_TestAction.Conditions.Add(HydrogenPurification_Utilization_Pcnt_NotEmpty);
            HydrogenPurification_Utilization_Pcnt_TestAction.Conditions.Add(HydrogenPurification_Utilization_Pcnt_EmptySet);
            resources.ApplyResources(HydrogenPurification_Utilization_Pcnt_TestAction, "HydrogenPurification_Utilization_Pcnt_TestAction");
            // 
            // HydrogenPurification_Utilization_Pcnt_NotEmpty
            // 
            HydrogenPurification_Utilization_Pcnt_NotEmpty.Enabled = true;
            HydrogenPurification_Utilization_Pcnt_NotEmpty.Name = "HydrogenPurification_Utilization_Pcnt_NotEmpty";
            HydrogenPurification_Utilization_Pcnt_NotEmpty.ResultSet = 1;
            // 
            // HydrogenPurification_Utilization_Pcnt_EmptySet
            // 
            HydrogenPurification_Utilization_Pcnt_EmptySet.Enabled = true;
            HydrogenPurification_Utilization_Pcnt_EmptySet.Name = "HydrogenPurification_Utilization_Pcnt_EmptySet";
            HydrogenPurification_Utilization_Pcnt_EmptySet.ResultSet = 2;
            // 
            // HydrogenPurification_Unit_Count_TestAction
            // 
            HydrogenPurification_Unit_Count_TestAction.Conditions.Add(HydrogenPurification_Unit_Count_NotEmpty);
            HydrogenPurification_Unit_Count_TestAction.Conditions.Add(HydrogenPurification_Unit_Count_EmptySet);
            resources.ApplyResources(HydrogenPurification_Unit_Count_TestAction, "HydrogenPurification_Unit_Count_TestAction");
            // 
            // HydrogenPurification_Unit_Count_NotEmpty
            // 
            HydrogenPurification_Unit_Count_NotEmpty.Enabled = true;
            HydrogenPurification_Unit_Count_NotEmpty.Name = "HydrogenPurification_Unit_Count_NotEmpty";
            HydrogenPurification_Unit_Count_NotEmpty.ResultSet = 1;
            // 
            // HydrogenPurification_Unit_Count_EmptySet
            // 
            HydrogenPurification_Unit_Count_EmptySet.Enabled = true;
            HydrogenPurification_Unit_Count_EmptySet.Name = "HydrogenPurification_Unit_Count_EmptySet";
            HydrogenPurification_Unit_Count_EmptySet.ResultSet = 2;
            // 
            // HydrogenPurification_Purification_MSCF_TestAction
            // 
            HydrogenPurification_Purification_MSCF_TestAction.Conditions.Add(HydrogenPurification_Purification_MSCF_NotEmpty);
            HydrogenPurification_Purification_MSCF_TestAction.Conditions.Add(HydrogenPurification_Purification_MSCF_EmptySet);
            resources.ApplyResources(HydrogenPurification_Purification_MSCF_TestAction, "HydrogenPurification_Purification_MSCF_TestAction");
            // 
            // HydrogenPurification_Purification_MSCF_NotEmpty
            // 
            HydrogenPurification_Purification_MSCF_NotEmpty.Enabled = true;
            HydrogenPurification_Purification_MSCF_NotEmpty.Name = "HydrogenPurification_Purification_MSCF_NotEmpty";
            HydrogenPurification_Purification_MSCF_NotEmpty.ResultSet = 1;
            // 
            // HydrogenPurification_Purification_MSCF_EmptySet
            // 
            HydrogenPurification_Purification_MSCF_EmptySet.Enabled = true;
            HydrogenPurification_Purification_MSCF_EmptySet.Name = "HydrogenPurification_Purification_MSCF_EmptySet";
            HydrogenPurification_Purification_MSCF_EmptySet.ResultSet = 2;
            // 
            // HydrogenPurification_Purification_kSCF_TestAction
            // 
            HydrogenPurification_Purification_kSCF_TestAction.Conditions.Add(HydrogenPurification_Purification_kSCF_NotEmpty);
            HydrogenPurification_Purification_kSCF_TestAction.Conditions.Add(HydrogenPurification_Purification_kSCF_EmptySet);
            resources.ApplyResources(HydrogenPurification_Purification_kSCF_TestAction, "HydrogenPurification_Purification_kSCF_TestAction");
            // 
            // HydrogenPurification_Purification_kSCF_NotEmpty
            // 
            HydrogenPurification_Purification_kSCF_NotEmpty.Enabled = true;
            HydrogenPurification_Purification_kSCF_NotEmpty.Name = "HydrogenPurification_Purification_kSCF_NotEmpty";
            HydrogenPurification_Purification_kSCF_NotEmpty.ResultSet = 1;
            // 
            // HydrogenPurification_Purification_kSCF_EmptySet
            // 
            HydrogenPurification_Purification_kSCF_EmptySet.Enabled = true;
            HydrogenPurification_Purification_kSCF_EmptySet.Name = "HydrogenPurification_Purification_kSCF_EmptySet";
            HydrogenPurification_Purification_kSCF_EmptySet.ResultSet = 2;
            // 
            // HydrogenPurification_Purification_Sales_TestAction
            // 
            HydrogenPurification_Purification_Sales_TestAction.Conditions.Add(HydrogenPurification_Purification_Sales_NotEmpty);
            HydrogenPurification_Purification_Sales_TestAction.Conditions.Add(HydrogenPurification_Purification_Sales_EmptySet);
            resources.ApplyResources(HydrogenPurification_Purification_Sales_TestAction, "HydrogenPurification_Purification_Sales_TestAction");
            // 
            // HydrogenPurification_Purification_Sales_NotEmpty
            // 
            HydrogenPurification_Purification_Sales_NotEmpty.Enabled = true;
            HydrogenPurification_Purification_Sales_NotEmpty.Name = "HydrogenPurification_Purification_Sales_NotEmpty";
            HydrogenPurification_Purification_Sales_NotEmpty.ResultSet = 1;
            // 
            // HydrogenPurification_Purification_Sales_EmptySet
            // 
            HydrogenPurification_Purification_Sales_EmptySet.Enabled = true;
            HydrogenPurification_Purification_Sales_EmptySet.Name = "HydrogenPurification_Purification_Sales_EmptySet";
            HydrogenPurification_Purification_Sales_EmptySet.ResultSet = 2;
            // 
            // HydrogenPurification_Component_WtPcntData
            // 
            this.HydrogenPurification_Component_WtPcntData.PosttestAction = null;
            this.HydrogenPurification_Component_WtPcntData.PretestAction = null;
            this.HydrogenPurification_Component_WtPcntData.TestAction = HydrogenPurification_Component_WtPcnt_TestAction;
            // 
            // HydrogenPurification_Component_kMTData
            // 
            this.HydrogenPurification_Component_kMTData.PosttestAction = null;
            this.HydrogenPurification_Component_kMTData.PretestAction = null;
            this.HydrogenPurification_Component_kMTData.TestAction = HydrogenPurification_Component_kMT_TestAction;
            // 
            // HydrogenPurification_Utilization_PcntData
            // 
            this.HydrogenPurification_Utilization_PcntData.PosttestAction = null;
            this.HydrogenPurification_Utilization_PcntData.PretestAction = null;
            this.HydrogenPurification_Utilization_PcntData.TestAction = HydrogenPurification_Utilization_Pcnt_TestAction;
            // 
            // HydrogenPurification_Unit_CountData
            // 
            this.HydrogenPurification_Unit_CountData.PosttestAction = null;
            this.HydrogenPurification_Unit_CountData.PretestAction = null;
            this.HydrogenPurification_Unit_CountData.TestAction = HydrogenPurification_Unit_Count_TestAction;
            // 
            // HydrogenPurification_Purification_MSCFData
            // 
            this.HydrogenPurification_Purification_MSCFData.PosttestAction = null;
            this.HydrogenPurification_Purification_MSCFData.PretestAction = null;
            this.HydrogenPurification_Purification_MSCFData.TestAction = HydrogenPurification_Purification_MSCF_TestAction;
            // 
            // HydrogenPurification_Purification_kSCFData
            // 
            this.HydrogenPurification_Purification_kSCFData.PosttestAction = null;
            this.HydrogenPurification_Purification_kSCFData.PretestAction = null;
            this.HydrogenPurification_Purification_kSCFData.TestAction = HydrogenPurification_Purification_kSCF_TestAction;
            // 
            // HydrogenPurification_Purification_SalesData
            // 
            this.HydrogenPurification_Purification_SalesData.PosttestAction = null;
            this.HydrogenPurification_Purification_SalesData.PretestAction = null;
            this.HydrogenPurification_Purification_SalesData.TestAction = HydrogenPurification_Purification_Sales_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions HydrogenPurification_Component_WtPcntData;
        private SqlDatabaseTestActions HydrogenPurification_Component_kMTData;
        private SqlDatabaseTestActions HydrogenPurification_Utilization_PcntData;
        private SqlDatabaseTestActions HydrogenPurification_Unit_CountData;
        private SqlDatabaseTestActions HydrogenPurification_Purification_MSCFData;
        private SqlDatabaseTestActions HydrogenPurification_Purification_kSCFData;
        private SqlDatabaseTestActions HydrogenPurification_Purification_SalesData;
    }
}
