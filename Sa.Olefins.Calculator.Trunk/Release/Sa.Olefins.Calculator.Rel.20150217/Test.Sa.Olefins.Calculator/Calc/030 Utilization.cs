﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _030_Utilization : SqlDatabaseTestClass
    {

        public _030_Utilization()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Utilization()
        {
            SqlDatabaseTestActions testActions = this.UtilizationData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Utilization_Duration()
        {
            SqlDatabaseTestActions testActions = this.Utilization_DurationData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Utilization_Capacity()
        {
            SqlDatabaseTestActions testActions = this.Utilization_CapacityData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Utilization_Production()
        {
            SqlDatabaseTestActions testActions = this.Utilization_ProductionData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }




        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Utilization_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_030_Utilization));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Utilization_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Utilization_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Utilization_Duration_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Utilization_Duration_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Utilization_Duration_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Utilization_Capacity_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Utilization_Capacity_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Utilization_Capacity_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Utilization_Production_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Utilization_Production_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Utilization_Production_EmptySet;
            this.UtilizationData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Utilization_DurationData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Utilization_CapacityData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Utilization_ProductionData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Utilization_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Utilization_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Utilization_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Utilization_Duration_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Utilization_Duration_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Utilization_Duration_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Utilization_Capacity_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Utilization_Capacity_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Utilization_Capacity_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Utilization_Production_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Utilization_Production_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Utilization_Production_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // Utilization_TestAction
            // 
            Utilization_TestAction.Conditions.Add(Utilization_NotEmpty);
            Utilization_TestAction.Conditions.Add(Utilization_EmptySet);
            resources.ApplyResources(Utilization_TestAction, "Utilization_TestAction");
            // 
            // Utilization_NotEmpty
            // 
            Utilization_NotEmpty.Enabled = true;
            Utilization_NotEmpty.Name = "Utilization_NotEmpty";
            Utilization_NotEmpty.ResultSet = 1;
            // 
            // Utilization_EmptySet
            // 
            Utilization_EmptySet.Enabled = true;
            Utilization_EmptySet.Name = "Utilization_EmptySet";
            Utilization_EmptySet.ResultSet = 2;
            // 
            // Utilization_Duration_TestAction
            // 
            Utilization_Duration_TestAction.Conditions.Add(Utilization_Duration_NotEmpty);
            Utilization_Duration_TestAction.Conditions.Add(Utilization_Duration_EmptySet);
            resources.ApplyResources(Utilization_Duration_TestAction, "Utilization_Duration_TestAction");
            // 
            // Utilization_Duration_NotEmpty
            // 
            Utilization_Duration_NotEmpty.Enabled = true;
            Utilization_Duration_NotEmpty.Name = "Utilization_Duration_NotEmpty";
            Utilization_Duration_NotEmpty.ResultSet = 1;
            // 
            // Utilization_Duration_EmptySet
            // 
            Utilization_Duration_EmptySet.Enabled = true;
            Utilization_Duration_EmptySet.Name = "Utilization_Duration_EmptySet";
            Utilization_Duration_EmptySet.ResultSet = 2;
            // 
            // Utilization_Capacity_TestAction
            // 
            Utilization_Capacity_TestAction.Conditions.Add(Utilization_Capacity_NotEmpty);
            Utilization_Capacity_TestAction.Conditions.Add(Utilization_Capacity_EmptySet);
            resources.ApplyResources(Utilization_Capacity_TestAction, "Utilization_Capacity_TestAction");
            // 
            // Utilization_Capacity_NotEmpty
            // 
            Utilization_Capacity_NotEmpty.Enabled = true;
            Utilization_Capacity_NotEmpty.Name = "Utilization_Capacity_NotEmpty";
            Utilization_Capacity_NotEmpty.ResultSet = 1;
            // 
            // Utilization_Capacity_EmptySet
            // 
            Utilization_Capacity_EmptySet.Enabled = true;
            Utilization_Capacity_EmptySet.Name = "Utilization_Capacity_EmptySet";
            Utilization_Capacity_EmptySet.ResultSet = 2;
            // 
            // Utilization_Production_TestAction
            // 
            Utilization_Production_TestAction.Conditions.Add(Utilization_Production_NotEmpty);
            Utilization_Production_TestAction.Conditions.Add(Utilization_Production_EmptySet);
            resources.ApplyResources(Utilization_Production_TestAction, "Utilization_Production_TestAction");
            // 
            // Utilization_Production_NotEmpty
            // 
            Utilization_Production_NotEmpty.Enabled = true;
            Utilization_Production_NotEmpty.Name = "Utilization_Production_NotEmpty";
            Utilization_Production_NotEmpty.ResultSet = 1;
            // 
            // Utilization_Production_EmptySet
            // 
            Utilization_Production_EmptySet.Enabled = true;
            Utilization_Production_EmptySet.Name = "Utilization_Production_EmptySet";
            Utilization_Production_EmptySet.ResultSet = 2;
            // 
            // UtilizationData
            // 
            this.UtilizationData.PosttestAction = null;
            this.UtilizationData.PretestAction = null;
            this.UtilizationData.TestAction = Utilization_TestAction;
            // 
            // Utilization_DurationData
            // 
            this.Utilization_DurationData.PosttestAction = null;
            this.Utilization_DurationData.PretestAction = null;
            this.Utilization_DurationData.TestAction = Utilization_Duration_TestAction;
            // 
            // Utilization_CapacityData
            // 
            this.Utilization_CapacityData.PosttestAction = null;
            this.Utilization_CapacityData.PretestAction = null;
            this.Utilization_CapacityData.TestAction = Utilization_Capacity_TestAction;
            // 
            // Utilization_ProductionData
            // 
            this.Utilization_ProductionData.PosttestAction = null;
            this.Utilization_ProductionData.PretestAction = null;
            this.Utilization_ProductionData.TestAction = Utilization_Production_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions UtilizationData;
        private SqlDatabaseTestActions Utilization_DurationData;
        private SqlDatabaseTestActions Utilization_CapacityData;
        private SqlDatabaseTestActions Utilization_ProductionData;
    }
}
