﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Data
{
    [TestClass()]
    public class StandardEnergy_Pyrolysis : SqlDatabaseTestClass
    {

        public StandardEnergy_Pyrolysis()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void StandardEnergy_PyrolysisHvc()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_PyrolysisHvcData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_PyrolysisBtu()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_PyrolysisBtuData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }


        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_PyrolysisHvc_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StandardEnergy_Pyrolysis));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_PyrolysisHvc_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_PyrolysisBtu_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_PyrolysisBtu_EmptySet;
            this.StandardEnergy_PyrolysisHvcData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_PyrolysisBtuData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            StandardEnergy_PyrolysisHvc_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_PyrolysisHvc_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_PyrolysisBtu_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_PyrolysisBtu_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // StandardEnergy_PyrolysisHvc_TestAction
            // 
            StandardEnergy_PyrolysisHvc_TestAction.Conditions.Add(StandardEnergy_PyrolysisHvc_EmptySet);
            resources.ApplyResources(StandardEnergy_PyrolysisHvc_TestAction, "StandardEnergy_PyrolysisHvc_TestAction");
            // 
            // StandardEnergy_PyrolysisHvc_EmptySet
            // 
            StandardEnergy_PyrolysisHvc_EmptySet.Enabled = true;
            StandardEnergy_PyrolysisHvc_EmptySet.Name = "StandardEnergy_PyrolysisHvc_EmptySet";
            StandardEnergy_PyrolysisHvc_EmptySet.ResultSet = 1;
            // 
            // StandardEnergy_PyrolysisBtu_TestAction
            // 
            StandardEnergy_PyrolysisBtu_TestAction.Conditions.Add(StandardEnergy_PyrolysisBtu_EmptySet);
            resources.ApplyResources(StandardEnergy_PyrolysisBtu_TestAction, "StandardEnergy_PyrolysisBtu_TestAction");
            // 
            // StandardEnergy_PyrolysisBtu_EmptySet
            // 
            StandardEnergy_PyrolysisBtu_EmptySet.Enabled = true;
            StandardEnergy_PyrolysisBtu_EmptySet.Name = "StandardEnergy_PyrolysisBtu_EmptySet";
            StandardEnergy_PyrolysisBtu_EmptySet.ResultSet = 1;
            // 
            // StandardEnergy_PyrolysisHvcData
            // 
            this.StandardEnergy_PyrolysisHvcData.PosttestAction = null;
            this.StandardEnergy_PyrolysisHvcData.PretestAction = null;
            this.StandardEnergy_PyrolysisHvcData.TestAction = StandardEnergy_PyrolysisHvc_TestAction;
            // 
            // StandardEnergy_PyrolysisBtuData
            // 
            this.StandardEnergy_PyrolysisBtuData.PosttestAction = null;
            this.StandardEnergy_PyrolysisBtuData.PretestAction = null;
            this.StandardEnergy_PyrolysisBtuData.TestAction = StandardEnergy_PyrolysisBtu_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardEnergy_PyrolysisHvcData;
        private SqlDatabaseTestActions StandardEnergy_PyrolysisBtuData;
    }
}
