﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OlefinVideos.Startup))]
namespace OlefinVideos
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
