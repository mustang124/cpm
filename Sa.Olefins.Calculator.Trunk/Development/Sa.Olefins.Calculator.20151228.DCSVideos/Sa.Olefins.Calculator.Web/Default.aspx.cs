﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OlefinsCalculator
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity(string.Empty), null);

            //Login lg = ((Login)this.Master.FindControl("LoginUser"));
            //((Button)lg.FindControl("LoginButton")).Enabled = true;
            
            //This works most of the time
            //if (Request.IsAuthenticated)
            //    Response.Redirect("~/CalcInput.aspx");

            if (Request.IsAuthenticated)
            {
                OlefinsCalculator.User user = (OlefinsCalculator.User)Session["User"];
                if (user != null)
                {
                    Response.Redirect("~/CalcInput.aspx");
                }
            }

            //OlefinsCalculator.User user = (OlefinsCalculator.User)Session["User"];
            //if (user != null)
            //{
            //    Response.Redirect("~/CalcInput.aspx");
            //}
        }
    }
}