﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="OlefinsCalculator.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:UpdatePanel ID="upnlMain" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<div style="background-color: #E6EAED; height: 23px; width: 955px; padding-top: 5px; padding-left: 5px; margin-bottom: 10px;"><div style="width: 300px; float: left;">Change Password</div></div>
<div style="width: 960px; margin-top: 15px; margin-left: 10px; margin-right: 10px; background-color: #FFFFFF;">

<p></p>
<div id="divCoInfo" runat="server" style="float: left;">
<div style="float: left;"><img src="images/leftRound.png" alt="" /></div><div style="background-color: #0380cd; float: left; height: 23px; width: 350px; color: #FFFFFF;">Password Information</div><div><img src="images/rightRound.png" alt="" /></div>
<div id="outerCC">
<div id="innterCC" style="border: 1px solid #0380cd; padding-left: 3px; width: 365px;">
<table border="0" cellpadding="2" cellspacing="3" width="100%">
<tr><td>Current Password:</td><td><asp:TextBox ID="txtCurrentPassword" runat="server" TextMode="Password" style="width: 148px;"></asp:TextBox><asp:RequiredFieldValidator ID="rfvCurrentPassword" runat="server" ControlToValidate="txtCurrentPassword" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" ValidationGroup="vgProfile"></asp:RequiredFieldValidator></td></tr>
<tr><td>New Password:</td><td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" style="width: 148px;"></asp:TextBox><asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" Enabled="true" ValidationGroup="vgProfile"></asp:RequiredFieldValidator></td></tr>
<tr><td>Re-Type Password:</td><td><asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" style="width: 148px;"></asp:TextBox><asp:RequiredFieldValidator ID="rfvRetypePassword" runat="server" ControlToValidate="txtPassword2" EnableClientScript="true" ErrorMessage="*" ForeColor="Red" Enabled="true" ValidationGroup="vgProfile"></asp:RequiredFieldValidator></td></tr>
</table>
</div>
<div style="width: 362px; border-bottom: 1px solid #0380cd; border-right: 1px solid #0380cd; border-left: 1px solid #0380cd; padding: 3px;">
<asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" ValidationGroup="vgProfile" /><asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label><asp:CompareValidator ID="cvPasswords" runat="server" ControlToCompare="txtPassword2" ControlToValidate="txtPassword" EnableClientScript="true" ForeColor="Red" ErrorMessage="Passwords Must Match" ValidationGroup="vgProfile"></asp:CompareValidator>
</div>
</div>
</div>
</div>

<br clear="all" />
&nbsp;

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>
