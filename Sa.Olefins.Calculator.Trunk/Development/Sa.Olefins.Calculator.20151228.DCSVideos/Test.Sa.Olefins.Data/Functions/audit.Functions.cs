﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Data
{
    [TestClass()]
    public class audit_Functions : SqlDatabaseTestClass
    {

        public audit_Functions()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void ParseString()
        {
            SqlDatabaseTestActions testActions = this.ParseStringData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void ReplaceParameters()
        {
            SqlDatabaseTestActions testActions = this.ReplaceParametersData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }


        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ParseString_TestAction;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(audit_Functions));
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ParseString_EmptySet;
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ReplaceParameters_TestAction;
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition ReplaceParameters;
			this.ParseStringData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
			this.ReplaceParametersData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
			ParseString_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
			ParseString_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
			ReplaceParameters_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
			ReplaceParameters = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
			// 
			// ParseString_TestAction
			// 
			ParseString_TestAction.Conditions.Add(ParseString_EmptySet);
			resources.ApplyResources(ParseString_TestAction, "ParseString_TestAction");
			// 
			// ParseString_EmptySet
			// 
			ParseString_EmptySet.Enabled = true;
			ParseString_EmptySet.Name = "ParseString_EmptySet";
			ParseString_EmptySet.ResultSet = 1;
			// 
			// ReplaceParameters_TestAction
			// 
			ReplaceParameters_TestAction.Conditions.Add(ReplaceParameters);
			resources.ApplyResources(ReplaceParameters_TestAction, "ReplaceParameters_TestAction");
			// 
			// ReplaceParameters
			// 
			ReplaceParameters.ColumnNumber = 1;
			ReplaceParameters.Enabled = true;
			ReplaceParameters.ExpectedValue = "1";
			ReplaceParameters.Name = "ReplaceParameters";
			ReplaceParameters.NullExpected = false;
			ReplaceParameters.ResultSet = 1;
			ReplaceParameters.RowNumber = 1;
			// 
			// ParseStringData
			// 
			this.ParseStringData.PosttestAction = null;
			this.ParseStringData.PretestAction = null;
			this.ParseStringData.TestAction = ParseString_TestAction;
			// 
			// ReplaceParametersData
			// 
			this.ReplaceParametersData.PosttestAction = null;
			this.ReplaceParametersData.PretestAction = null;
			this.ReplaceParametersData.TestAction = ReplaceParameters_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions ParseStringData;
        private SqlDatabaseTestActions ReplaceParametersData;
    }
}
