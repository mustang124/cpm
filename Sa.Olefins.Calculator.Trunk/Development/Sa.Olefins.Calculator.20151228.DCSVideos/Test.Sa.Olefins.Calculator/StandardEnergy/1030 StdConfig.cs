﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.StandardEnergy
{
    [TestClass()]
    public class _1030_StdConfig : SqlDatabaseTestClass
    {

        public _1030_StdConfig()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void StandardEnergy_StdConfig_MBtuDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_StdConfig_MBtuDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_StdConfig_kBtuLbDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_StdConfig_kBtuLbDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_StdConfig_kLbDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_StdConfig_kLbDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }



        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_StdConfig_MBtuDay_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1030_StdConfig));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_StdConfig_kBtuLbDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_StdConfig_kLbDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_StdConfig_kBtuLbDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_StdConfig_kBtuLbDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_StdConfig_kLbDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_StdConfig_kLbDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_StdConfig_MBtuDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_StdConfig_MBtuDay_EmptySet;
            this.StandardEnergy_StdConfig_MBtuDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_StdConfig_kBtuLbDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_StdConfig_kLbDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            StandardEnergy_StdConfig_MBtuDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_StdConfig_kBtuLbDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_StdConfig_kLbDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_StdConfig_kBtuLbDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_StdConfig_kBtuLbDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_StdConfig_kLbDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_StdConfig_kLbDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_StdConfig_MBtuDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_StdConfig_MBtuDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // StandardEnergy_StdConfig_MBtuDayData
            // 
            this.StandardEnergy_StdConfig_MBtuDayData.PosttestAction = null;
            this.StandardEnergy_StdConfig_MBtuDayData.PretestAction = null;
            this.StandardEnergy_StdConfig_MBtuDayData.TestAction = StandardEnergy_StdConfig_MBtuDay_TestAction;
            // 
            // StandardEnergy_StdConfig_MBtuDay_TestAction
            // 
            StandardEnergy_StdConfig_MBtuDay_TestAction.Conditions.Add(StandardEnergy_StdConfig_MBtuDay_NotEmpty);
            StandardEnergy_StdConfig_MBtuDay_TestAction.Conditions.Add(StandardEnergy_StdConfig_MBtuDay_EmptySet);
            resources.ApplyResources(StandardEnergy_StdConfig_MBtuDay_TestAction, "StandardEnergy_StdConfig_MBtuDay_TestAction");
            // 
            // StandardEnergy_StdConfig_kBtuLbDayData
            // 
            this.StandardEnergy_StdConfig_kBtuLbDayData.PosttestAction = null;
            this.StandardEnergy_StdConfig_kBtuLbDayData.PretestAction = null;
            this.StandardEnergy_StdConfig_kBtuLbDayData.TestAction = StandardEnergy_StdConfig_kBtuLbDay_TestAction;
            // 
            // StandardEnergy_StdConfig_kBtuLbDay_TestAction
            // 
            StandardEnergy_StdConfig_kBtuLbDay_TestAction.Conditions.Add(StandardEnergy_StdConfig_kBtuLbDay_NotEmpty);
            StandardEnergy_StdConfig_kBtuLbDay_TestAction.Conditions.Add(StandardEnergy_StdConfig_kBtuLbDay_EmptySet);
            resources.ApplyResources(StandardEnergy_StdConfig_kBtuLbDay_TestAction, "StandardEnergy_StdConfig_kBtuLbDay_TestAction");
            // 
            // StandardEnergy_StdConfig_kLbDayData
            // 
            this.StandardEnergy_StdConfig_kLbDayData.PosttestAction = null;
            this.StandardEnergy_StdConfig_kLbDayData.PretestAction = null;
            this.StandardEnergy_StdConfig_kLbDayData.TestAction = StandardEnergy_StdConfig_kLbDay_TestAction;
            // 
            // StandardEnergy_StdConfig_kLbDay_TestAction
            // 
            StandardEnergy_StdConfig_kLbDay_TestAction.Conditions.Add(StandardEnergy_StdConfig_kLbDay_NotEmpty);
            StandardEnergy_StdConfig_kLbDay_TestAction.Conditions.Add(StandardEnergy_StdConfig_kLbDay_EmptySet);
            resources.ApplyResources(StandardEnergy_StdConfig_kLbDay_TestAction, "StandardEnergy_StdConfig_kLbDay_TestAction");
            // 
            // StandardEnergy_StdConfig_kBtuLbDay_NotEmpty
            // 
            StandardEnergy_StdConfig_kBtuLbDay_NotEmpty.Enabled = true;
            StandardEnergy_StdConfig_kBtuLbDay_NotEmpty.Name = "StandardEnergy_StdConfig_kBtuLbDay_NotEmpty";
            StandardEnergy_StdConfig_kBtuLbDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_StdConfig_kBtuLbDay_EmptySet
            // 
            StandardEnergy_StdConfig_kBtuLbDay_EmptySet.Enabled = true;
            StandardEnergy_StdConfig_kBtuLbDay_EmptySet.Name = "StandardEnergy_StdConfig_kBtuLbDay_EmptySet";
            StandardEnergy_StdConfig_kBtuLbDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_StdConfig_kLbDay_NotEmpty
            // 
            StandardEnergy_StdConfig_kLbDay_NotEmpty.Enabled = true;
            StandardEnergy_StdConfig_kLbDay_NotEmpty.Name = "StandardEnergy_StdConfig_kLbDay_NotEmpty";
            StandardEnergy_StdConfig_kLbDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_StdConfig_kLbDay_EmptySet
            // 
            StandardEnergy_StdConfig_kLbDay_EmptySet.Enabled = true;
            StandardEnergy_StdConfig_kLbDay_EmptySet.Name = "StandardEnergy_StdConfig_kLbDay_EmptySet";
            StandardEnergy_StdConfig_kLbDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_StdConfig_MBtuDay_NotEmpty
            // 
            StandardEnergy_StdConfig_MBtuDay_NotEmpty.Enabled = true;
            StandardEnergy_StdConfig_MBtuDay_NotEmpty.Name = "StandardEnergy_StdConfig_MBtuDay_NotEmpty";
            StandardEnergy_StdConfig_MBtuDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_StdConfig_MBtuDay_EmptySet
            // 
            StandardEnergy_StdConfig_MBtuDay_EmptySet.Enabled = true;
            StandardEnergy_StdConfig_MBtuDay_EmptySet.Name = "StandardEnergy_StdConfig_MBtuDay_EmptySet";
            StandardEnergy_StdConfig_MBtuDay_EmptySet.ResultSet = 2;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardEnergy_StdConfig_MBtuDayData;
        private SqlDatabaseTestActions StandardEnergy_StdConfig_kBtuLbDayData;
        private SqlDatabaseTestActions StandardEnergy_StdConfig_kLbDayData;
    }
}
