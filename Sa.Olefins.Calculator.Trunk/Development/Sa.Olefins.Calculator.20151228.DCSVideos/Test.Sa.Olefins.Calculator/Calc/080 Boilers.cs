﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _080_Boilers : SqlDatabaseTestClass
    {

        public _080_Boilers()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void FiredSteamBoilers()
        {
            SqlDatabaseTestActions testActions = this.FiredSteamBoilersData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FiredSteamBoilers_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_080_Boilers));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.InconclusiveCondition FiredSteamBoilers_Inconclusive;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition FiredSteamBoilers_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FiredSteamBoilers_EmptySet;
            this.FiredSteamBoilersData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            FiredSteamBoilers_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FiredSteamBoilers_Inconclusive = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.InconclusiveCondition();
            FiredSteamBoilers_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            FiredSteamBoilers_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // FiredSteamBoilers_TestAction
            // 
            FiredSteamBoilers_TestAction.Conditions.Add(FiredSteamBoilers_Inconclusive);
            FiredSteamBoilers_TestAction.Conditions.Add(FiredSteamBoilers_NotEmpty);
            FiredSteamBoilers_TestAction.Conditions.Add(FiredSteamBoilers_EmptySet);
            resources.ApplyResources(FiredSteamBoilers_TestAction, "FiredSteamBoilers_TestAction");
            // 
            // FiredSteamBoilers_Inconclusive
            // 
            FiredSteamBoilers_Inconclusive.Enabled = false;
            FiredSteamBoilers_Inconclusive.Name = "FiredSteamBoilers_Inconclusive";
            // 
            // FiredSteamBoilers_NotEmpty
            // 
            FiredSteamBoilers_NotEmpty.Enabled = true;
            FiredSteamBoilers_NotEmpty.Name = "FiredSteamBoilers_NotEmpty";
            FiredSteamBoilers_NotEmpty.ResultSet = 1;
            // 
            // FiredSteamBoilers_EmptySet
            // 
            FiredSteamBoilers_EmptySet.Enabled = true;
            FiredSteamBoilers_EmptySet.Name = "FiredSteamBoilers_EmptySet";
            FiredSteamBoilers_EmptySet.ResultSet = 2;
            // 
            // FiredSteamBoilersData
            // 
            this.FiredSteamBoilersData.PosttestAction = null;
            this.FiredSteamBoilersData.PretestAction = null;
            this.FiredSteamBoilersData.TestAction = FiredSteamBoilers_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions FiredSteamBoilersData;
    }
}
