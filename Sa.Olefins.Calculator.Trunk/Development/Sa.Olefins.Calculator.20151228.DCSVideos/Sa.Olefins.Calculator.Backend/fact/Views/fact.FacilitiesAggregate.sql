﻿CREATE VIEW [fact].[FacilitiesAggregate]
WITH SCHEMABINDING
AS
SELECT
	f.[SubmissionId],
	b.[FacilityId],
	SUM(f.[Unit_Count])			[Unit_Count],
	COUNT_BIG(*)				[Items]
FROM [fact].[Facilities]			f
INNER JOIN [dim].[Facility_Bridge]	b
	ON	b.[DescendantId] = f.[FacilityId]
GROUP BY
	f.[SubmissionId],
	b.[FacilityId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_FacilitiesAggregate]
ON [fact].[FacilitiesAggregate]([SubmissionId] ASC, [FacilityId] ASC);
GO

CREATE INDEX [IX_FacilitiesAggregate]
ON [fact].[FacilitiesAggregate]([SubmissionId] ASC, [FacilityId] ASC)
INCLUDE([Unit_Count]);
GO