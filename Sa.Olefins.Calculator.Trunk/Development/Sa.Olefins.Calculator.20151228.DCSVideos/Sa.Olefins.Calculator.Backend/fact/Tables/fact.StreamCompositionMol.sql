﻿CREATE TABLE [fact].[StreamCompositionMol]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamCompositionMol_Submissions]						REFERENCES [fact].[Submissions]([SubmissionId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [FK_StreamCompositionMol_StreamQuantity]
															FOREIGN KEY([SubmissionId], [StreamNumber])								REFERENCES [fact].[StreamQuantity]([SubmissionId], [StreamNumber])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_StreamCompositionMol_Component_LookUp]					REFERENCES [dim].[Component_LookUp]([ComponentId])					ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Component_MolPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamCompositionMol_Component_MolPcnt_MinIncl_0.0]		CHECK([Component_MolPcnt] >= 0.0),
															CONSTRAINT [CR_StreamCompositionMol_Component_MolPcnt_MaxIncl_100.0]	CHECK([Component_MolPcnt] <= 100.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamCompositionMol_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamCompositionMol_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamCompositionMol_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamCompositionMol_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamCompositionMol]		PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamNumber] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [fact].[t_StreamCompositionMol_u]
ON [fact].[StreamCompositionMol]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamCompositionMol]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamCompositionMol].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[fact].[StreamCompositionMol].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[fact].[StreamCompositionMol].[ComponentId]		= INSERTED.[ComponentId];

END;
GO