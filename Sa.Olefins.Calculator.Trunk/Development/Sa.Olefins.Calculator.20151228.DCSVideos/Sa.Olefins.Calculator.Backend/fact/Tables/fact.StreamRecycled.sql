﻿CREATE TABLE [fact].[StreamRecycled]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Submissions]						REFERENCES [fact].[Submissions]([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_StreamRecycled_Component_LookUp]					REFERENCES [dim].[Component_LookUp]([ComponentId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Recycled_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MinIncl_0.0]		CHECK([Recycled_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MaxIncl_100.0]	CHECK([Recycled_WtPcnt] <= 100.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamRecycled_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamRecycled]				PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [fact].[t_StreamRecycled_u]
ON [fact].[StreamRecycled]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamRecycled]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamRecycled].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[fact].[StreamRecycled].[ComponentId]		= INSERTED.[ComponentId];

END;
GO