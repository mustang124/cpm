﻿CREATE TABLE [fact].[Submissions]
(
	[SubmissionId]			INT					NOT	NULL,

	[SubmissionName]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [CL_Sumbissions_SumbissionName]		CHECK([SubmissionName] <> ''),

	[DateBeg]				DATE				NOT	NULL,
	[DateEnd]				DATE				NOT	NULL,

	[_Duration_Days]		AS CONVERT(FLOAT,		  (DATEDIFF(DAY, [DateBeg], [DateEnd]) + 1.0))
							PERSISTED			NOT	NULL,
	[_Duration_Multiplier]	AS CONVERT(FLOAT, 365.0 / (DATEDIFF(DAY, [DateBeg], [DateEnd]) + 1.0))
							PERSISTED			NOT	NULL,

	[_SubmissionNameFull]	AS [SubmissionName] + N' (' + CONVERT(NVARCHAR, [DateBeg], 106) + N' - ' + CONVERT(NVARCHAR, [DateEnd], 106) + ')',

	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_Submissions_Active]				DEFAULT 1,

	CONSTRAINT [CV_Submissions_Dates]	CHECK([DateEnd] >= [DateBeg]),
	
	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Submissions_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Submissions]					PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [fact].[t_Submissions_u]
ON [fact].[Submissions]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Submissions]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[Submissions].[SubmissionId]		= INSERTED.[SubmissionId]

END;
GO

