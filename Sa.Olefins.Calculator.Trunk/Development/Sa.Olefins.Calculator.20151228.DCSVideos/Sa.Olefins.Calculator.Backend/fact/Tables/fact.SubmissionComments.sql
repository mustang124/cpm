﻿CREATE TABLE [fact].[SubmissionComments]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_SubmissionComments_Submissions]				REFERENCES [fact].[Submissions]([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[SubmissionComment]		NVARCHAR(MAX)		NOT	NULL	CONSTRAINT [CL_SubmissionComments_SumbissionComment]		CHECK([SubmissionComment] <> ''),
	
	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_SubmissionComments]			PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [fact].[t_SubmissionComments_u]
ON [fact].[SubmissionComments]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[SubmissionComments]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[SubmissionComments].[SubmissionId]		= INSERTED.[SubmissionId]

END;
GO

