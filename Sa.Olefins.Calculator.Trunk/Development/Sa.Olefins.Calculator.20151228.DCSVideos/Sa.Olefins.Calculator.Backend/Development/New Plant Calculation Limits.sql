﻿
SELECT * FROM [audit].[LogError];

SELECT * FROM [auth].[Companies]				[c]		WHERE [c].[CompanyName]	= 'SHOWA DENKO';

SELECT * FROM [auth].[JoinCompanyPlant]			[jcp]	WHERE [jcp].[CompanyId]	= 61;

SELECT * FROM [auth].[Plants]					[p]		WHERE [p].[PlantId]		= 138;

SELECT * FROM [auth].[PlantCalculationLimits]	[pcl]	WHERE [pcl].[PlantId]	= 138;

INSERT INTO [auth].[PlantCalculationLimits]
(
	[PlantId],
	[CalcsPerPeriod_Count],
	[PeriodLen_Days],
	[Active]
)
VALUES
(
	138,
	11,
	90,
	1
);

EXECUTE [web].[Get_PlantCalculationLimits] 138;

SELECT * FROM [stage].[Submissions] ORDER BY tsModified DESC
