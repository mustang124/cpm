﻿CREATE FUNCTION [auth].[Get_LoginsByPlant_Active]
(
	@PlantId		INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		pbl.[JoinId],
		pbl.[PlantId],
		pbl.[LoginId],
		pbl.[PlantName],
		pbl.[LoginTag],
		pbl.[NameFirst],
		pbl.[NameLast],
		pbl.[eMail],
		pbl.[_NameComma],
		pbl.[_NameFull],
		pbl.[RoleId],
		pbl.[RoleLevel]
	FROM [auth].[LoginsByPlant_Active]	pbl WITH (NOEXPAND)
	WHERE	pbl.[PlantId]	= @PlantId
);