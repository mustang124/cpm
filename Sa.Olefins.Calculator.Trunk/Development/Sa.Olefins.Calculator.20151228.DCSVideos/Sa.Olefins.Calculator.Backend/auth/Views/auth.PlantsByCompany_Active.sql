﻿CREATE VIEW [auth].[PlantsByCompany_Active]
WITH SCHEMABINDING
AS
SELECT
	jcp.[JoinId],
	jcp.[CompanyId],
	jcp.[PlantId],
	c.[CompanyName],
	p.[PlantName]
FROM [auth].[JoinCompanyPlant]	jcp
INNER JOIN [auth].[Companies]	c
	ON	c.[CompanyId]	= jcp.[CompanyId]
INNER JOIN [auth].[Plants]		p
	ON	p.[PlantId]		= jcp.[PlantId]
WHERE	jcp.[Active]	= 1
	AND	c.[Active]		= 1
	AND	p.[Active]		= 1;
GO

CREATE UNIQUE CLUSTERED INDEX [UX_PlantsByCompany_Active]
ON [auth].[PlantsByCompany_Active]([CompanyId] ASC, [PlantId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_PlantsByCompany_Active_Get]
ON [auth].[PlantsByCompany_Active]([CompanyId] ASC)
INCLUDE ([CompanyName], [PlantId], [PlantName]);
GO
