﻿CREATE TABLE [auth].[LoginsAttributes]
(
	[LoginId]				INT					NOT	NULL	CONSTRAINT [FK_LoginsAttributes_Logins]					REFERENCES [auth].[Logins] ([LoginId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[NameLast]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [CL_LoginsAttributes_NameLast]				CHECK([NameLast]	<> ''),
	[NameFirst]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [CL_LoginsAttributes_NameFirst]				CHECK([NameFirst]	<> ''),
	[_NameFull]				AS [NameFirst] + ' ' + [NameLast]
							PERSISTED			NOT	NULL,
	[_NameComma]			AS [NameLast] + ', ' + [NameFirst]
							PERSISTED			NOT	NULL,

	[eMail]					NVARCHAR(254)			NULL	CONSTRAINT [CL_LoginsAttributes_UserEMail]				CHECK ([eMail]	<> ''),

	[RoleId]				INT					NOT	NULL	CONSTRAINT [FK_LoginsAttributes_Roles]					REFERENCES [auth].[Roles] ([RoleId])	ON DELETE NO ACTION ON UPDATE NO ACTION DEFAULT 0,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_LoginsAttributes_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LoginsAttributes_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LoginsAttributes_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LoginsAttributes_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_LoginsAttributes]			PRIMARY KEY CLUSTERED([LoginId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_LoginsAttributes_EMail]
ON [auth].[LoginsAttributes]([eMail] ASC)
WHERE [eMail] IS NOT NULL;
GO

CREATE TRIGGER [auth].[t_LoginsAttributes_u]
ON [auth].[LoginsAttributes]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[LoginsAttributes]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[LoginsAttributes].[LoginId]		= INSERTED.[LoginId];

END;
GO;