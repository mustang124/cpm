﻿CREATE PROCEDURE [auth].[Insert_JoinPlantSubmission]
(
	@PlantId			INT,
	@SubmissionId		INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [auth].[JoinPlantSubmission]([PlantId], [SubmissionId])
	OUTPUT [INSERTED].[JoinId] INTO @Id([Id])
	VALUES(
		@PlantId,
		@SubmissionId
		);

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@PlantId:'		+ CONVERT(VARCHAR, @PlantId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO