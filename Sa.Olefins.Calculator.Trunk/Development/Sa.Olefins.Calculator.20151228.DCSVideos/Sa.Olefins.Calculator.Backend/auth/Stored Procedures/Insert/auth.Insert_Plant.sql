﻿CREATE PROCEDURE [auth].[Insert_Plant]
(
	@PlantName			NVARCHAR(128)
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [auth].[Plants]([PlantName])
	OUTPUT [INSERTED].[PlantId] INTO @Id([Id])
	VALUES(
		@PlantName
		);

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@PlantName:'		+ CONVERT(VARCHAR, @PlantName));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO