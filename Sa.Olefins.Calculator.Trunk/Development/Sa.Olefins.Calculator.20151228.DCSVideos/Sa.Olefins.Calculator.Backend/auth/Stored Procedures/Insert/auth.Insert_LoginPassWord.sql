﻿CREATE PROCEDURE [auth].[Insert_LoginPassWord]
(
	@LoginId		INT,

	@pSalt			BINARY(512),
	@pWord			VARBINARY(512)
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [auth].[LoginsPassWords]([LoginId], [pSalt], [pWord])
	VALUES(
		@LoginId,
		@pSalt,
		@pWord
		);

	RETURN @LoginId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@LoginId:'		+ CAST(@LoginId AS VARCHAR));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO