﻿PRINT 'INSERT INTO [ante].[ComponentEnergy]([MethodologyId], [ComponentId], [StandardEnergy_BtuLb])';

INSERT INTO [ante].[ComponentEnergy]([MethodologyId], [ComponentId], [StandardEnergy_BtuLb])
SELECT t.[MethodologyId], t.[ComponentId], t.[StandardEnergy_BtuLb]
FROM (VALUES
	(@MethodologyId, dim.Return_ComponentId('H2'),			1951.0),
	(@MethodologyId, dim.Return_ComponentId('CH4'),			1951.0),
	(@MethodologyId, dim.Return_ComponentId('C2H4'),		1882.0),
	(@MethodologyId, dim.Return_ComponentId('C3H6'),		1467.0),
	(@MethodologyId, dim.Return_ComponentId('C4H6'),		 947.0),
	(@MethodologyId, dim.Return_ComponentId('C4H8'),		 947.0),
	(@MethodologyId, dim.Return_ComponentId('C4H10'),		 947.0),
	(@MethodologyId, dim.Return_ComponentId('C6H6'),		 150.0),
	(@MethodologyId, dim.Return_ComponentId('Other'),		 150.0),
	(@MethodologyId, dim.Return_ComponentId('PyroGasOil'),	 150.0),
	(@MethodologyId, dim.Return_ComponentId('PyroFuelOil'),  150.0),
	(@MethodologyId, dim.Return_ComponentId('Inerts'),		 947.0),
	(@MethodologyId, dim.Return_ComponentId('C2H6'),		1882.0),
	(@MethodologyId, dim.Return_ComponentId('C3H8'),		1467.0)
	)t([MethodologyId], [ComponentId], [StandardEnergy_BtuLb]);