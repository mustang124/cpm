﻿PRINT 'INSERT INTO [ante].[ReportMap_Standard]([MethodologyId], [StandardId], [Report_Prefix])';

INSERT INTO [ante].[ReportMap_Standard]([MethodologyId], [StandardId], [Report_Prefix])
SELECT t.[MethodologyId], t.[StandardId], t.[Report_Prefix]
FROM (VALUES
	(@MethodologyId, dim.Return_StandardId('kEdc'),			'TotalkEDC'),
	(@MethodologyId, dim.Return_StandardId('StdEnergy'),	'EIIStdEnergy'),
	(@MethodologyId, dim.Return_StandardId('PersMaint'),	'PESMaintenance'),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'),	'PESNonMaintenance'),
	(@MethodologyId, dim.Return_StandardId('Pers'),			'PESTotal'),
	(@MethodologyId, dim.Return_StandardId('Mes'),			'MES'),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'),	'NonEnergyCES')
	)t([MethodologyId], [StandardId], [Report_Prefix]);