﻿PRINT 'INSERT INTO [ante].[NicenessStreams]([MethodologyId], [StreamId], [EmptyComposition], [Divisor])';

INSERT INTO [ante].[NicenessStreams]([MethodologyId], [StreamId], [EmptyComposition], [Divisor])
SELECT t.[MethodologyId], t.[StreamId], t.[EmptyComposition], t.[Divisor]
FROM (VALUES
	(@MethodologyId, [dim].[Return_StreamId]('Condensate'),	0.95, 0.983),
	(@MethodologyId, [dim].[Return_StreamId]('HeavyNGL'),	0.95, 0.983),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaLt'),	0.95, 1.001),
	(@MethodologyId, [dim].[Return_StreamId]('Raffinate'),	0.95, 1.001),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaFr'),	0.95, 0.986),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaHv'),	0.95, 0.966),

	(@MethodologyId, [dim].[Return_StreamId]('Diesel'),		NULL, 0.8443),
	(@MethodologyId, [dim].[Return_StreamId]('GasOilHv'),	NULL, 0.8742),
	(@MethodologyId, [dim].[Return_StreamId]('GasOilHt'),	NULL, 0.8371)
	)t([MethodologyId], [StreamId], [EmptyComposition], [Divisor]);