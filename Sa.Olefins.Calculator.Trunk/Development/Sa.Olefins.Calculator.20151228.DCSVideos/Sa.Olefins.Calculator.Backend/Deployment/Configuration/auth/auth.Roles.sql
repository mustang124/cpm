﻿PRINT 'INSERT INTO [auth].[Roles]([RoleTag], [RoleName], [RoleDetail], [RoleLevel])';

INSERT INTO [auth].[Roles]([RoleTag], [RoleName], [RoleDetail], [RoleLevel])
SELECT t.[RoleTag], t.[RoleName], t.[RoleDetail], t.[RoleLevel]
FROM (VALUES
	('GreenLantern', 'Green Lantern', 'Green Lantern', 0),
	('Super', 'Super Admin', 'Super Administrator', 1),
	('Company', 'Company Coordinator', 'Company Coordinator', 5),
	('Site', 'Site Coordinator', 'Site Coordinator', 10)
	)t([RoleTag], [RoleName], [RoleDetail], [RoleLevel]);