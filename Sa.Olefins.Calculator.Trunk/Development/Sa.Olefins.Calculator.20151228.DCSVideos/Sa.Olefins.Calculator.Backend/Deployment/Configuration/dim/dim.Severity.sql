﻿PRINT 'INSERT INTO [dim].[Severity_LookUp]([SeverityTag], [SeverityName], [SeverityDetail])';

INSERT INTO [dim].[Severity_LookUp]([SeverityTag], [SeverityName], [SeverityDetail])
SELECT t.[SeverityTag], t.[SeverityName], t.[SeverityDetail]
FROM (
	VALUES
		('Information', 'Informational Message', 'Informational Message'),
		('Warning', 'Warning Message', 'Warning Message'),
		('Error', 'Error Message', 'Error Message; Calculations will fail'),
		('Critical', 'Critical Error Message', 'Critical Error Message; Processing will fail')
	) t ([SeverityTag], [SeverityName], [SeverityDetail]);