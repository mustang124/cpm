﻿PRINT 'INSERT INTO [dim].[Standard_LookUp]([StandardTag], [StandardName], [StandardDetail])';

INSERT INTO [dim].[Standard_LookUp]([StandardTag], [StandardName], [StandardDetail])
SELECT t.[StandardTag], t.[StandardName], t.[StandardDetail]
FROM (VALUES
	('kEdc', 'Total EDC', 'Total EDC'),
	('StdEnergy', 'EII Standard Energy (MBtu/d)', 'Energy Intensity Index Standard Energy (MBtu/d)'),
	('Pers', 'Personnel Efficiency (K Hours)', 'Personnel Efficiency Standard (K Hours)'),
	('PersMaint', 'Maintenance (K Hours)', 'Personnel Efficiency Maintenance Standard (K Hours)'),
	('PersNonMaint', 'Non-Maintenance (K Hours)', 'Personnel Efficiency Non-Maintenance Standard (K Hours)'),
	('Mes', 'Maintenance', 'Maintenance Efficiency Standard (K USD)'),
	('NonEnergy', 'Non-Energy', 'Non-Energy Efficiency Standard (K USD)')
	)t([StandardTag], [StandardName], [StandardDetail]);

PRINT 'INSERT INTO [dim].[Standard_Parent]([MethodologyId], [StandardId], [ParentId], [Operator], [SortKey], [Hierarchy])';

INSERT INTO [dim].[Standard_Parent]([MethodologyId], [StandardId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	m.MethodologyId,
	l.StandardId,
	p.StandardId,
	t.Operator,
	t.SortKey,
	'/'
FROM (VALUES
	('kEdc', 'kEdc',			'+', 1),
	('StdEnergy', 'StdEnergy',	'+', 2),
	('Pers', 'Pers',			'+', 6),
	('PersMaint', 'Pers',		'+', 4),
	('PersNonMaint', 'Pers',	'+', 5),
	('Mes', 'Mes',				'+', 7),
	('NonEnergy', 'NonEnergy',	'+', 8)
	)	t(StandardTag, ParentTag, Operator, SortKey)
INNER JOIN [dim].[Standard_LookUp]			l
	ON	l.StandardTag = t.StandardTag
INNER JOIN [dim].[Standard_LookUp]			p
	ON	p.StandardTag = t.ParentTag
INNER JOIN [ante].[Methodology]				m
	ON	m.[MethodologyTag] = '2013';

EXECUTE dim.Update_Parent 'dim', 'Standard_Parent', 'MethodologyId', 'StandardId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE dim.Merge_Bridge 'dim', 'Standard_Parent', 'dim', 'Standard_Bridge', 'MethodologyId', 'StandardId', 'SortKey', 'Hierarchy', 'Operator';