﻿PRINT 'INSERT INTO [dim].[Factor_LookUp]([FactorTag], [FactorName], [FactorDetail])';

INSERT INTO [dim].[Factor_LookUp]([FactorTag], [FactorName], [FactorDetail])
SELECT t.[FactorTag], t.[FactorName], t.[FactorDetail]
FROM (VALUES
	('FreshPyroFeed', 'Fresh Pyrolysis Feeds', 'Fresh Pyrolysis Feeds'),
		('Ethane', 'Ethane', 'Ethane'),
		('LPG', 'Liquid Petroleum Gas', 'Liquid Petroleum Gas'),
		('Liquid', 'Liquid Feedstock', 'Liquid Feedstock'),

	('Supp', 'Suplemental Feeds', 'Suplemental Feeds Total'),
		('SuppEthane', 'Supplemental Ethane', 'Supplemental Ethane'),
		('SuppLPG', 'Supplemental LPG', 'Supplemental LPG'),
		('SuppLiquid', 'Supplemental Liquid', 'Supplemental Liquid'),
			
	('CompGas', 'Process / Cracked / Charge Gas Compressors', 'Process / Cracked / Charge Gas Compressors'),
		('CompEthane', 'CGC Ethane', 'CGC Ethane'),
		('CompLPG', 'CGC Liquid Petroleum Gas', 'CGC  Liquid Petroleum Gas'),
		('CompLiquid', 'CGC Liquid Feedstock', 'CGC Liquid Feedstock'),

	('FreshPyroFeedPur', 'Fresh Pyrolysis Feed Purification', 'Fresh Pyrolysis Feed Purification'),
		('TowerDeethanizer', 'Deethanizer Tower', 'Deethanizer Tower'),
		('TowerDepropanizer', 'Depropanizer Tower', 'Depropanizer Tower'),
		('TowerNAPS', 'Tower NAPS', 'Tower NAPS'),

	('TowerPyroGasHT', 'Pyrolysis Gasoline Hydrotreater', 'Pyrolysis Gasoline Hydrotreater'),
		('TowerPyroGasB', 'Both Selective Saturation and Desulfurization', 'Both Selective Saturation and Desulfurization'),
		('TowerPyroGasDS', 'Desulfurization', 'Desulfurization'),
		('TowerPyroGasSS', 'Selective Saturation', 'Selective Saturation'),

	('HydroPur', 'Hydrogen Purification', 'Hydrogen Purification'),
		('HydroPurCryogenic', 'Hydrogen Purification Cryogenic', 'Hydrogen Purification Cryogenic'),
		('HydroPurPSA', 'Hydrogen Purification Pressure Swing Adsorption', 'Hydrogen Purification Pressure Swing Adsorption'),
		('HydroPurMembrane', 'Hydrogen Purification Membrane', 'Hydrogen Purification Membrane'),

	('BoilFiredSteam', 'Fired Steam Boilers', 'Fired Steam Boilers'),
	('ElecGen', 'Electric Power Generation', 'Electric Power Generation'),
	('PropyleneAdj', 'Propylene CG and RG Adjustment', 'Propylene CG and RG Adjustment'),
		('PropyleneAdjCG', 'Chemical-Grade Propylene Adjustment', 'Chemical-Grade Propylene Adjustment'),
		('PropyleneAdjRG', 'Refinery-Grade Propylene Adjustment', 'Refinery-Grade Propylene Adjustment')
	)t([FactorTag], [FactorName], [FactorDetail]);

PRINT 'INSERT INTO [dim].[Factor_Parent]([MethodologyId], [FactorId], [ParentId], [Operator], [SortKey], [Hierarchy])';

INSERT INTO [dim].[Factor_Parent]([MethodologyId], [FactorId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	m.MethodologyId,
	l.FactorId,
	p.FactorId,
	t.Operator,
	t.SortKey,
	'/'
FROM (VALUES
	('FreshPyroFeed', 'FreshPyroFeed',				'+', 102000),
		('Ethane', 'FreshPyroFeed',						'+', 102020),
		('LPG', 'FreshPyroFeed',						'+', 102050),
		('Liquid', 'FreshPyroFeed',						'+', 102080),

	('Supp', 'Supp',								'+', 104000),
		('SuppEthane', 'Supp',							'+', 104020),
		('SuppLPG', 'Supp',								'+', 104050),
		('SuppLiquid', 'Supp',							'+', 104080),

	('CompGas', 'CompGas',							'+', 106000),
		('CompEthane', 'CompGas',						'+', 106020),
		('CompLPG', 'CompGas',							'+', 106050),
		('CompLiquid', 'CompGas',						'+', 106080),

	('FreshPyroFeedPur', 'FreshPyroFeedPur',		'+', 108000),
		('TowerDeethanizer', 'FreshPyroFeedPur',		'+', 108020),
		('TowerDepropanizer', 'FreshPyroFeedPur',		'+', 108050),
		('TowerNAPS', 'FreshPyroFeedPur',				'+', 108080),

	('TowerPyroGasHT', 'TowerPyroGasHT',			'+', 110000),
		('TowerPyroGasB', 'TowerPyroGasHT',				'+', 110020),
		('TowerPyroGasDS', 'TowerPyroGasHT',			'+', 110050),
		('TowerPyroGasSS', 'TowerPyroGasHT',			'+', 110080),

	('HydroPur', 'HydroPur',						'+', 112000),
		('HydroPurCryogenic', 'HydroPur',				'+', 112020),
		('HydroPurPSA', 'HydroPur',						'+', 112050),
		('HydroPurMembrane', 'HydroPur',				'+', 112080),

	('BoilFiredSteam', 'BoilFiredSteam',			'+', 114000),
	('ElecGen', 'ElecGen',							'+', 116000),
	('PropyleneAdj', 'PropyleneAdj',				'+', 118000),
		('PropyleneAdjCG', 'PropyleneAdj',				'+', 118020),
		('PropyleneAdjRG', 'PropyleneAdj',				'+', 118050)
	)	t(FactorTag, ParentTag, Operator, SortKey)
INNER JOIN [dim].[Factor_LookUp]			l
	ON	l.FactorTag = t.FactorTag
INNER JOIN [dim].[Factor_LookUp]			p
	ON	p.FactorTag = t.ParentTag
INNER JOIN [ante].[Methodology]				m
	ON	m.[MethodologyTag] = '2013';

EXECUTE dim.Update_Parent 'dim', 'Factor_Parent', 'MethodologyId', 'FactorId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE dim.Merge_Bridge 'dim', 'Factor_Parent', 'dim', 'Factor_Bridge', 'MethodologyId', 'FactorId', 'SortKey', 'Hierarchy', 'Operator';