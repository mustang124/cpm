﻿SET NOCOUNT ON;

:r .\Configuration\ante\ante.Methodology.sql

:r .\Configuration\dim\dim.Operator.sql
:r .\Configuration\dim\dim.NationalLanguageSupport.sql
:r .\Configuration\dim\dim.Severity.sql
:r .\Configuration\dim\dim.Message.sql

:r .\Configuration\dim\dim.Stream.sql
:r .\Configuration\dim\dim.Component.sql
:r .\Configuration\dim\dim.Facility.sql
:r .\Configuration\dim\dim.FeedClass.sql
:r .\Configuration\dim\dim.HydroTreaterType.sql
:r .\Configuration\dim\dim.ProcessUnit.sql
:r .\Configuration\dim\dim.Standard.sql
:r .\Configuration\dim\dim.Factor.sql

:r .\Configuration\auth\auth.Roles.sql

DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

:r .\Configuration\ante\ante.UtilizationFilter.sql
:r .\Configuration\ante\ante.Factors.sql
:r .\Configuration\ante\ante.Factors_EiiCoefficients.sql
:r .\Configuration\ante\ante.Factors_EiiIntercepts.sql
:r .\Configuration\ante\ante.Factors_WwCduScu.sql
:r .\Configuration\ante\ante.NicenessRanges.sql
:r .\Configuration\ante\ante.NicenessComponents.sql
:r .\Configuration\ante\ante.NicenessStreams.sql
:r .\Configuration\ante\ante.ComponentEnergy.sql
:r .\Configuration\ante\ante.HvcProduction.sql
:r .\Configuration\ante\ante.StdEnergyFractionator.sql
:r .\Configuration\ante\ante.StdEnergyHydroTreater.sql
:r .\Configuration\ante\ante.MapStreamProcessUnit.sql
:r .\Configuration\ante\ante.MapStreamNumberId.sql
:r .\Configuration\ante\ante.MapStreamComponent.sql

:r .\Configuration\ante\ante.MapComponentRecycle.sql
:r .\Configuration\ante\ante.MapFactorFeedClass.sql
:r .\Configuration\ante\ante.MapFactorFracType.sql
:r .\Configuration\ante\ante.MapFactorHydroTreater.sql
:r .\Configuration\ante\ante.MapFactorHydroPur.sql
:r .\Configuration\ante\ante.MapFactorStreams.sql
:r .\Configuration\ante\ante.MapRecycledBalance.sql
:r .\Configuration\ante\ante.MapEiiModelComponents.sql
:r .\Configuration\ante\ante.ReportMap_ProcessUnit.sql
:r .\Configuration\ante\ante.ReportMap_Standard.sql

:r .\Authorization\Seed.Solomon.sql
:r .\Authorization\Seed.Companies.sql

DECLARE @RoundDensity		INT = 2;
DECLARE @RoundPressure		INT = 2;
DECLARE @RoundQuantity		INT = 2;
DECLARE @RoundRate			INT = 2;
DECLARE @RoundWtPcnt		INT = 2;

:r .\Data\Data.Olefins_Submissions.sql
:r .\Data\Data.Olefins_Facilities.sql
:r .\Data\Data.Olefins_Streams.sql
:r .\Data\Data.Olefins_Streams_Attributes.sql
:r .\Data\Data.Olefins_Streams_Composition.sql
:r .\Data\Data.Olefins_Streams_Composition_PpfcInerts.sql

:r .\Authorization\Post.Solomon.sql
:r .\Authorization\Post.Companies.sql

:r .\Deploy.CalculateSubmissions.sql

--:r .\Data.Olefins\Deploy.Results.Olefins.Xls.sql
--:r .\Data.Olefins\Deploy.Results.Olefins.Xls.Standards.sql