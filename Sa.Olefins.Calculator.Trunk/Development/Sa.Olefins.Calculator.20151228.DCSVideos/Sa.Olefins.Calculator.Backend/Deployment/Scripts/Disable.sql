﻿PRINT 'Disable';

DECLARE @DisableCompanies TABLE
(
	[Email]				VARCHAR(128)	NOT	NULL	CHECK ([Email]			<> ''),
	[NameFirst]			NVARCHAR(128)	NOT	NULL	CHECK ([NameFirst]		<> ''),
	[NameLast]			NVARCHAR(128)	NOT	NULL	CHECK ([NameLast]		<> ''),
	[CompanyTag]		VARCHAR(42)		NOT	NULL	CHECK ([CompanyTag]		<> ''),
	[CompanyName]		VARCHAR(84)		NOT	NULL	CHECK ([CompanyName]	<> ''),
	[CompanyDetail]		VARCHAR(256)	NOT	NULL	CHECK ([CompanyDetail]	<> ''),
	[PlantName]			VARCHAR(128)	NOT	NULL	CHECK ([PlantName]		<> ''),
	[Refnum]			VARCHAR(128)	NOT	NULL	CHECK ([Refnum]			<> ''),
	[RoleId]			INT				NOT	NULL,

	PRIMARY KEY CLUSTERED([Email] ASC, [PlantName] ASC)
);

INSERT INTO @DisableCompanies([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId])
SELECT t.[Email], t.[NameFirst], t.[NameLast], t.[CompanyTag], t.[CompanyName], t.[CompanyDetail], t.[PlantName], t.[Refnum], t.[RoleId]
FROM(VALUES
('santosh.kolipaka@borouge.com', 'Santosh Kumar', 'Kolipaka', 'BOROUGE', 'BOROUGE', 'BOROUGE', 'Ruwais #1', '2015PCH163', 3),
('santosh.kolipaka@borouge.com', 'Santosh Kumar', 'Kolipaka', 'BOROUGE', 'BOROUGE', 'BOROUGE', 'Ruwais #2', '2015PCH233', 3),
('helmi@capcx.com', 'Helmilus', 'Moesa', 'CHANDRA ASRI', 'CHANDRA ASRI', 'CHANDRA ASRI', 'Cilegon', '2015PCH234', 3),
('sharmarishi@indianoil.in', 'Rishi', 'Sharma', 'IOCL', 'IOCL', 'IOCL', 'Panipat', '2015PCH223', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC SADAF', 'SABIC SADAF', 'SABIC SADAF', 'Sadaf', '2015PCH141', 3),
('harbiam3@sadaf.sabic.com', 'Ahmed Mesfer', 'Al-Harbi', 'SABIC SADAF', 'SABIC SADAF', 'SABIC SADAF', 'Sadaf', '2015PCH141', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk1', '2015PCH169', 3),
('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk1', '2015PCH169', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk2', '2015PCH170', 3),
('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk2', '2015PCH170', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk3', '2015PCH171', 3),
('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk3', '2015PCH171', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC KEMYA', 'SABIC KEMYA', 'SABIC KEMYA', 'Kemya', '2015PCH172', 3),
('asmarima@kemya.sabic.com', 'Mohammed', 'Al-Asmari', 'SABIC KEMYA', 'SABIC KEMYA', 'SABIC KEMYA', 'Kemya', '2015PCH172', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 1', '2015PCH173', 3),
('badrosom@yanpet.sabic.com', 'Omar', 'Badros', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 1', '2015PCH173', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 2', '2015PCH174', 3),
('badrosom@yanpet.sabic.com', 'Omar', 'Badros', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 2', '2015PCH174', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC UNITED', 'SABIC UNITED', 'SABIC UNITED', 'United', '2015PCH191', 3),
('mutairiyb@united.sabic.com', 'Yasser Battal', 'Al-Mutairi', 'SABIC UNITED', 'SABIC UNITED', 'SABIC UNITED', 'United', '2015PCH191', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC SHARQ', 'SABIC SHARQ', 'SABIC SHARQ', 'Sharq', '2015PCH230', 3),
('khalileh@sharq.sabic.com', 'Ezzat', 'Khalil', 'SABIC SHARQ', 'SABIC SHARQ', 'SABIC SHARQ', 'Sharq', '2015PCH230', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'Saudi Kayan', '2015PCH231', 3),
('thotakurapr@saudikayan.sabic.com', 'Prasada', 'Thotakura', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'Saudi Kayan', '2015PCH231', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC YANSAB', 'SABIC YANSAB', 'SABIC YANSAB', 'Yansab', '2015PCH232', 3),
('qarnimali@yansab.sabic.com', 'Manea', 'Al-Qarni', 'SABIC YANSAB', 'SABIC YANSAB', 'SABIC YANSAB', 'Yansab', '2015PCH232', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Wilton 6', '2015PCH068', 3),
('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Wilton 6', '2015PCH068', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 3', '2015PCH084', 3),
('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 3', '2015PCH084', 3),
('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 4', '2015PCH085', 3),
('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 4', '2015PCH085', 3),
('ah.alzahrani@tasnee.com', 'Ahmad', 'Al Zahrani', 'TASNEE', 'TASNEE', 'TASNEE', 'Al Jubail', '2015PCH212', 3),
('r.annapureddy@tasnee.com', 'Ramireddy', 'Annapureddy', 'TASNEE', 'TASNEE', 'TASNEE', 'Al Jubail', '2015PCH212', 3),
('takushi.nagashima@noe.jx-group.co.jp', 'Takushi', 'Nagashima', 'JX NIPPON', 'JX NIPPON', 'JX NIPPON', 'Jx Nippon Kawasaki', '2015PCH157', 3),
('michela.frisieri@versalis.eni.com', 'Michela', 'Frisieri', 'VERSALIS', 'VERSALIS', 'VERSALIS', 'Dunkerque', '2015PCH070', 3),
('michela.frisieri@versalis.eni.com', 'Michela', 'Frisieri', 'VERSALIS', 'VERSALIS', 'VERSALIS', 'Brindisi', '2015PCH111', 3)
	) t([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId]);

DECLARE @Active BIT = 0;

/*
	Need edits when enabling the logins
	AND	[w].[ValidTo]		IS NULL
*/

UPDATE [jcp]
SET
	[Active]	= @Active
FROM
	@DisableCompanies					[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [jcl]
SET
	[Active]	= @Active
FROM
	@DisableCompanies					[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [jpl]
SET
	[Active]	= @Active
FROM
	@DisableCompanies					[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [c]
SET
	[Active]	= @Active
FROM
	@DisableCompanies					[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [p]
SET
	[Active]	= @Active
FROM
	@DisableCompanies					[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [l]
SET
	[Active]	= @Active
FROM
	@DisableCompanies					[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [w]
SET
	[ValidTo]	= CASE WHEN (@Active = 0) THEN SYSDATETIMEOFFSET() ELSE NULL END
FROM
	@DisableCompanies					[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

DECLARE @DisableUsers TABLE
(
	[Email]				VARCHAR(128)	NOT	NULL	CHECK ([Email]			<> ''),
	[NameFirst]			NVARCHAR(128)	NOT	NULL	CHECK ([NameFirst]		<> ''),
	[NameLast]			NVARCHAR(128)	NOT	NULL	CHECK ([NameLast]		<> ''),
	[CompanyTag]		VARCHAR(42)		NOT	NULL	CHECK ([CompanyTag]		<> ''),
	[CompanyName]		VARCHAR(84)		NOT	NULL	CHECK ([CompanyName]	<> ''),
	[CompanyDetail]		VARCHAR(256)	NOT	NULL	CHECK ([CompanyDetail]	<> ''),
	[PlantName]			VARCHAR(128)	NOT	NULL	CHECK ([PlantName]		<> ''),
	[Refnum]			VARCHAR(128)	NOT	NULL	CHECK ([Refnum]			<> ''),
	[RoleId]			INT				NOT	NULL,

	PRIMARY KEY CLUSTERED([Email] ASC, [PlantName] ASC)
);

INSERT INTO @DisableUsers([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId])
SELECT t.[Email], t.[NameFirst], t.[NameLast], t.[CompanyTag], t.[CompanyName], t.[CompanyDetail], t.[PlantName], t.[Refnum], t.[RoleId]
FROM(VALUES
('ffernandezl@repsol.com', 'Felipe', 'Fernandez Lores', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Puertollano', '2015PCH041', 3)
	) t([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId]);

UPDATE [jcl]
SET
	[Active]	= @Active
FROM
	@DisableUsers						[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [jpl]
SET
	[Active]	= @Active
FROM
	@DisableUsers						[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [l]
SET
	[Active]	= @Active
FROM
	@DisableUsers						[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];

UPDATE [w]
SET
	[ValidTo]	= CASE WHEN (@Active = 0) THEN SYSDATETIMEOFFSET() ELSE NULL END
FROM
	@DisableUsers						[x]
INNER JOIN
	[auth].[LoginsAttributes]	[a]
		ON	[a].[NameFirst]		= [x].[NameFirst]
		AND	[a].[NameLast]		= [x].[NameLast]
		AND	[a].[eMail]			= [x].[Email]
INNER JOIN
	[auth].[LoginsPassWords]	[w]
		ON	[w].[LoginId]		= [a].[LoginId]
		AND	[w].[ValidTo]		IS NULL
INNER JOIN
	[auth].[Logins]				[l]
		ON	[l].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[Companies]			[c]
		ON	[c].[CompanyName]	= [x].[CompanyName]
INNER JOIN
	[auth].[Plants]				[p]
		ON	[p].[PlantName]		= [x].[PlantName]
INNER JOIN
	[auth].[JoinCompanyLogin]	[jcl]
		ON	[jcl].[CompanyId]	= [c].[CompanyId]
		AND	[jcl].[LoginId]		= [a].[LoginId]
INNER JOIN
	[auth].[JoinCompanyPlant]	[jcp]
		ON	[jcp].[CompanyId]	= [c].[CompanyId]
		AND	[jcp].[PlantId]		= [p].[PlantId]
INNER JOIN
	[auth].[JoinPlantLogin]		[jpl]
		ON	[jpl].[LoginId]		= [a].[LoginId]
		AND	[jpl].[PlantId]		= [p].[PlantId];