﻿PRINT 'Post.Companies.sql';

INSERT INTO [auth].[JoinPlantSubmission]([PlantId], [SubmissionId])
SELECT DISTINCT
	p.[PlantId],
	s.[SubmissionId]
FROM @Companies						i
INNER JOIN	[auth].[Plants]			p
	ON	p.[PlantName]		= i.[PlantName]
INNER JOIN	[stage].[Submissions]	s
	ON	s.[SubmissionName]	= i.[Refnum];