﻿--	Seed Data

SELECT
	'(''' + RTRIM(LTRIM(t.[Email])) + ''''
	+ ', ''' + RTRIM(LTRIM(t.[NameFirst])) + ''''
	+ ', ''' + RTRIM(LTRIM(t.[NameLast])) + ''''
	+ ', ''' + RTRIM(LTRIM(t.[CompanyID])) + ''''
	+ ', ''' + RTRIM(LTRIM(t.[Co])) + ''''
	+ ', ''' + RTRIM(LTRIM(t.[CoName])) + ''''
	+ ', ''' + RTRIM(LTRIM(t.[PlantName])) + ''''
	+ ', ''' + RTRIM(LTRIM(t.[Refnum])) + '''),'
FROM (
	SELECT DISTINCT
		t.[Email],
		t.[NameFirst],
		t.[NameLast],
		CASE t.CompanyID
			WHEN 'CHEVRON PHILLIPS'				THEN 'CP'
			WHEN 'TEST'							THEN 'SOLOMON'
			ELSE t.CompanyID
			END		[CompanyID],
		CASE t.Co
			WHEN 'CP'							THEN 'CHEVRON PHILLIPS'
			WHEN 'TEST'							THEN 'SOLOMON'
			WHEN 'KARPATNAFTOCHIM'				THEN 'LUKOIL'
			WHEN 'PETRONAS (ETHYLENE MALAYSIA)'	THEN 'PETRONAS'
			WHEN 'PETRONAS (OPTIMAL OLEFINS)'	THEN 'PETRONAS'
			WHEN 'UNIPETROL'					THEN 'PKN'
			ELSE t.Co
			END		[Co],
		CASE t.CoName
			WHEN 'CP'							THEN 'CHEVRON PHILLIPS'
			WHEN 'KARPATNAFTOCHIM'				THEN 'LUKOIL'
			WHEN 'PEMEX PETROQUIMICA'			THEN 'PEMEX'
			WHEN 'PETRONAS (ETHYLENE MALAYSIA)'	THEN 'PETRONAS'
			WHEN 'PETRONAS (OPTIMAL OLEFINS)'	THEN 'PETRONAS'
			WHEN 'REPSOL PETROLEO'				THEN 'REPSOL YPF'
			WHEN 'TOTAL PETROCHEMICALS France'	THEN 'TOTAL PETROCHEMICALS'
			WHEN 'UNIPETROL'					THEN 'PKN'
			WHEN 'EXXONMOBIL'					THEN CASE WHEN t.CompanyID = 'TONEN' THEN 'TONEN GENERAL' ELSE t.CompanyID END
			ELSE t.CoName
			END		[CoName],
		CASE t.PlantName
			WHEN 'CHIBA'		THEN t.PlantName + ' (' + t.[Refnum] + ')'
			WHEN 'Dallas'		THEN 'Solomon Plant'
			WHEN 'GANDHAR,Dahej Manufacturing division' THEN 'GANDHAR'
			WHEN 'PORT ARTHUR'	THEN t.PlantName + ' (' + t.[Refnum] + ')'
			WHEN 'RAYONG'		THEN t.PlantName + ' (' + t.[Refnum] + ')'
			WHEN 'TARRAGONA'	THEN t.PlantName + ' (' + t.[Refnum] + ')'
			WHEN 'KERTEH'		THEN t.PlantName + ' (' + t.[Refnum] + ')'
			ELSE t.PlantName
			END		[PlantName],
		t.[Refnum]
		FROM (
			SELECT 
				COALESCE(RTRIM(LTRIM(c.[Email])), 'jon.bowen@solomononline.com')	[Email],
				COALESCE(RTRIM(LTRIM(c.[FirstName])), 'Jon')						[NameFirst],
				COALESCE(RTRIM(LTRIM(c.[LastName])), 'Bowen')						[NameLast],
				RTRIM(LTRIM(t.[Refnum]))	[Refnum],
				RTRIM(LTRIM(t.[CompanyID]))	[CompanyID],
				RTRIM(LTRIM(t.[Co]))		[Co],
				RTRIM(LTRIM(t.[CoName]))	[CoName],
				RTRIM(LTRIM(t.[PlantName]))	[PlantName]
			FROM [Olefins].[dbo].[TSort]					t
			LEFT OUTER JOIN [Olefins].[dbo].[CoContactInfo]	c
				ON	c.[ContactCode] = t.[ContactCode]
				AND	c.[StudyYear]	= t.[StudyYear]
			WHERE	t.[PlantName] IS NOT NULL
				AND	t.[StudyYear] = 2011
		) t
	) t;

--	[stage].[Submissions]

SELECT
	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ''01/01/' + CONVERT(CHAR(4), t.[StudyYear]) + ''''
	+ ', ''12/31/' + CONVERT(CHAR(4), t.[StudyYear]) + '''),'	[Values]
FROM [Olefins].[dbo].[TSort]	t
WHERE	t.[Refnum] LIKE '2013PCH%'
ORDER BY t.[Refnum];

--	[stage].[Capacity]

--SELECT
--	'(''' +  RTRIM(LTRIM(u.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_StreamId](u.[StreamTag]))
--	+ ', ' + CONVERT(VARCHAR, u.[StreamDay_MTSD])
--	+ '),'
--FROM (
--	SELECT
--		c.[Refnum],
--		c.[EthylCapMTD]							[Ethylene],
--		c.[OlefinsCapMTD] - c.[EthylCapMTD]		[Propylene]
--	FROM [Olefins].[dbo].[Capacity]	c
--	INNER JOIN [Olefins].[dbo].[_RL]	l
--		ON	l.[Refnum] = c.[Refnum]
--		AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--	) p
--	UNPIVOT (
--		[StreamDay_MTSD] FOR [StreamTag] IN (
--			p.[Ethylene],
--			p.[Propylene]
--		)
--	) u
--WHERE u.[StreamDay_MTSD] > 0.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CASE t.[StreamId]
				WHEN 'Ethylene'		THEN '87' 
				WHEN 'Propylene'	THEN '90'
			END
	+ ', ' + CONVERT(VARCHAR, t.[Stream_MTd])
	+ '),',
	t.[Refnum],
	CASE t.[StreamId]
		WHEN 'Ethylene'		THEN '87' 
		WHEN 'Propylene'	THEN '90'
	END,
	t.[Stream_MTd]
FROM [Olefins].[fact].[Capacity]		t
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[StreamId]	IN ('Ethylene', 'Propylene')
	AND	t.[Stream_MTd]	>= 0.0;

--	[stage].[FacilitiesTrains]

--SELECT
--	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, COUNT(t.[PlanInterval])) + '),'
--FROM [Olefins].[dbo].[TADT]	t
--INNER JOIN [Olefins].[dbo].[_RL]	l
--	ON	l.[Refnum] = t.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--WHERE t.[PlanInterval] > 0.0
--GROUP BY
--	t.[Refnum];

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, COUNT(DISTINCT t.[TrainId])) + '),'
FROM [Olefins].[fact].[ReliabilityTA]	t
WHERE	t.[Refnum] LIKE '2013PCH%'	
GROUP BY
	t.[Refnum];
	
--	[stage].[Facilities]

--SELECT
--	'(''' +  RTRIM(LTRIM(u.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_FacilityId](u.[FacilityTag]))
--	+ ', ' + CONVERT(VARCHAR, u.[Unit_Count]) + '),'
--FROM (
--	SELECT
--		f.[Refnum],
--		f.[FracFeedCnt]		[FracFeed],
--		f.[HPBoilCnt]		[BoilHP],
--		f.[LPBoilCnt]		[BoilLP],
--		f.[ElecGenCnt]		[ElecGen],
--		f.[HydroCryoCnt]	[HydroPurCryogenic],
--		f.[HydroPSACnt]		[HydroPurPSA],
--		f.[HydroMembCnt]	[HydroPurMembrane],
--		f.[PGasHydroCnt]	[TowerPyroGasHT]
--	FROM [Olefins].[dbo].[Facilities]	f
--	INNER JOIN [Olefins].[dbo].[_RL]	l
--		ON	l.[Refnum] = f.[Refnum]
--		AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--	) p
--	UNPIVOT (
--		[Unit_Count] FOR [FacilityTag] IN (
--			p.[FracFeed],
--			p.[BoilHP],
--			p.[BoilLP],
--			p.[ElecGen],
--			p.[HydroPurCryogenic],
--			p.[HydroPurPSA],
--			p.[HydroPurMembrane],
--			p.[TowerPyroGasHT]
--		)
--	) u
--WHERE u.[Unit_Count] > 0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CASE t.[FacilityId]
				WHEN 'FracFeed'				THEN '28' 
				WHEN 'BoilHP'				THEN '66'
				WHEN 'BoilLP'				THEN '67'
				WHEN 'ElecGenDist'			THEN '70'
				WHEN 'HydroPurCryogenic'	THEN '54'
				WHEN 'HydroPurMembrane'		THEN '56'
				WHEN 'HydroPurPSA'			THEN '55'
				WHEN 'TowerPyroGasHT'		THEN '41'
			END
	+ ', ' + CONVERT(VARCHAR, t.[Unit_Count]) + '),',
	t.[Refnum],
	t.[FacilityId],
	t.[Unit_Count]
FROM [Olefins].[fact].[Facilities]	t
WHERE t.[Refnum] LIKE '2013PCH%'
	AND	t.[Unit_Count] > 0
	AND	t.[FacilityID] IN ('FracFeed', 'BoilHP', 'BoilLP', 'ElecGenDist', 'HydroPurCryogenic', 'HydroPurMembrane', 'HydroPurPSA', 'TowerPyroGasHT')
ORDER BY t.[Refnum] ASC, t.[FacilityId] ASC;

--	[stage].[FacilitiesBoilers]

--SELECT
--	'(''' +  RTRIM(LTRIM(f.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_FacilityId]('BoilHP'))
--	+ ', ' + CONVERT(VARCHAR, f.[HPBoilPSIG])
--	+ ', ' + CONVERT(VARCHAR, f.[HPBoilRate])
--	+ '),'
--FROM [Olefins].[dbo].[Facilities]	f
--INNER JOIN [Olefins].[dbo].[_RL]	l
--	ON	l.[Refnum] = f.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--WHERE f.[HPBoilRate]	> 0.0;

--SELECT
--	'(''' +  RTRIM(LTRIM(f.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_FacilityId]('BoilLP'))
--	+ ', ' + CONVERT(VARCHAR, f.[LPBoilPSIG])
--	+ ', ' + CONVERT(VARCHAR, f.[LPBoilRate])
--	+ '),'
--FROM [Olefins].[dbo].[Facilities]	f
--INNER JOIN [Olefins].[dbo].[_RL]	l
--	ON	l.[Refnum] = f.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--WHERE f.[LPBoilRate]	> 0.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CASE t.[FacilityID]
				WHEN 'BoilHP'	THEN '66'
				WHEN 'BoilLP'	THEN '67'
			END
	+ ', ' + CONVERT(VARCHAR, t.[Pressure_PSIg])
	+ ', ' + CONVERT(VARCHAR, t.[Rate_kLbHr])
	+ '),',
	t.*
FROM [Olefins].[fact].[FacilitiesPressure]	t
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[Pressure_PSIg]	>= 0.0
	AND	t.[Rate_kLbHr]		>= 0.0
	AND	t.[FacilityID] IN ('BoilHP', 'BoilLP')

--	[stage].[FacilitiesElecGeneration]

--SELECT
--	'(''' +  RTRIM(LTRIM(f.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_FacilityId]('ElecGen'))
--	+ ', ' + CONVERT(VARCHAR, f.[ElecGenMW])
--	+ '),'
--FROM [Olefins].[dbo].[Facilities]	f
--INNER JOIN [Olefins].[dbo].[_RL]	l
--	ON	l.[Refnum] = f.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--WHERE f.[ElecGenMW]	> 0.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', 70'
	+ ', ' + CONVERT(VARCHAR, t.[ElecGen_MW])
	+ '),'
FROM [Olefins].[fact].[FacilitiesMisc]	t
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[ElecGen_MW] >= 0.0;

--	[stage].[FacilitiesFractionator]

--SELECT
--	'(''' +  RTRIM(LTRIM(f.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_FacilityId]('FracFeed'))
--	+ ', ' + CONVERT(VARCHAR, f.[FracFeedKBSD])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_StreamId]
--		(CASE f.[FracFeed]
--			WHEN 'E/P'				THEN 'EPMix'
--			WHEN 'EP'				THEN 'EPMix'
--			WHEN 'ETHANE/PROPANE'	THEN 'EPMix'
--			WHEN 'E/P MIX'			THEN 'EPMix'
--			WHEN 'C2/C3'			THEN 'EPMix'
--			WHEN 'LPG'				THEN 'LPG'
--			WHEN 'Condensate'		THEN 'Condensate'
--			WHEN 'NAPHTHA'			THEN 'Naphtha'
--			WHEN 'Y-GRADE'			THEN 'LiqHeavy'
--		END))
--	+ ', ' + CONVERT(VARCHAR, f.[FracFeedProcMT])
--	+ ', ' + CONVERT(VARCHAR, f.[SG_FracFeedProc])
--	+ '),'
--FROM [Olefins].[dbo].[Facilities]	f
--INNER JOIN [Olefins].[dbo].[_RL]	l
--	ON	l.[Refnum] = f.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--WHERE	f.[FracFeedCnt] >= 1
--	AND	f.[FracFeed] IN ('E/P', 'EP', 'ETHANE/PROPANE', 'E/P MIX', 'C2/C3', 'LPG', 'Condensate', 'NAPHTHA', 'Y-GRADE');

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', 28'
	+ ', ' + CONVERT(VARCHAR, t.[FeedRate_kBsd])
	+ ', ' + CASE t.[StreamId]
				WHEN 'Naphtha'		THEN '19' 
				WHEN 'EPMix'		THEN '8'
				WHEN 'LPG'			THEN '11'
				WHEN 'Condensate'	THEN '24'
				WHEN 'LiqHeavy'		THEN '26'
			END
	+ ', ' + CONVERT(VARCHAR, t.[FeedProcessed_kMT])
	+ ', ' + CONVERT(VARCHAR, t.[FeedDensity_SG])
	+ '),',
	t.*
FROM [Olefins].[fact].[FacilitiesFeedFrac]	t
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[FeedRate_kBsd]		> 0.0
	AND	t.[FeedProcessed_kMT]	> 0.0
	AND	t.[FeedDensity_SG]		> 0.0;

--	[stage].[FacilitiesHydroTreater]

--SELECT
--	'(''' +  RTRIM(LTRIM(f.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_FacilityId]('TowerPyroGasHT'))
--	+ ', ' + CONVERT(VARCHAR, f.[Quantity_kBSD])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_HydroTreaterTypeId](f.[HydroTreaterTypeTag]))
--	+ ', ' + COALESCE(CONVERT(VARCHAR,
--			CASE WHEN f.[Processed_kMT] > f.[Product_kMT]
--			THEN 1.0
--			ELSE f.[Processed_kMT] / f.[Product_kMT]
--			END * 100.0), '0.0')
--	+ ', ' + COALESCE(CONVERT(VARCHAR, f.[Density_SG]), '0.0')
--	+ '),'
--FROM (
--	SELECT
--		f.[Refnum],
--		f.[PGasHydroKBSD]		[Quantity_kBSD],
--		CASE f.[PyrGasHydroType]
--			WHEN 'Selective Saturation'	THEN 'SS'
--			WHEN 'Both'					THEN 'B'
--			ELSE 'None'
--			END		[HydroTreaterTypeTag],
--		f.[SG_NonBnzPGH]		[Density_SG],
--		f.[PyrGasHydroMT]		[Processed_kMT],
--		SUM(COALESCE(q.[Q1Feed], 0.0) +
--			COALESCE(q.[Q2Feed], 0.0) +
--			COALESCE(q.[Q3Feed], 0.0) +
--			COALESCE(q.[Q4Feed], 0.0)) [Product_kMT]
--	FROM [Olefins].[dbo].[Facilities]			f
--	LEFT OUTER JOIN [Olefins].[dbo].[Quantity]	q
--		ON	q.[Refnum] = f.[Refnum]
--		AND	q.[FeedProdID] IN ('Benzene', 'OthPyGas')
--	INNER JOIN [Olefins].[dbo].[_RL]	l
--		ON	l.[Refnum] = f.[Refnum]
--		AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--	WHERE	f.[PGasHydroCnt] >= 1
--	GROUP BY
--		f.[Refnum],
--		f.[PGasHydroKBSD],
--		f.[PyrGasHydroType],
--		f.[SG_NonBnzPGH],
--		f.[PyrGasHydroMT]
--	) f;
	
SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', 41'
	+ ', ' + CONVERT(VARCHAR, t.[FeedRate_kBsd])
	+ ', ' + CASE t.[HTTypeID]
				WHEN 'B'		THEN '2' 
				WHEN 'SS'		THEN '4'
				ELSE '5'
			END
	+ ', ' + COALESCE(CONVERT(VARCHAR,
		CASE WHEN t.[FeedProcessed_kMT] > SUM(q.[Quantity_kMT])
		THEN 1.0
		ELSE t.[FeedProcessed_kMT] / SUM(q.[Quantity_kMT])
		END * 100.0), '0.0')
	+ ', ' + CONVERT(VARCHAR, t.[FeedDensity_SG])
	+ '),',
	t.[Refnum],
	t.[FeedRate_kBsd],
	t.[HTTypeID],
	t.[FeedProcessed_kMT],
	t.[FeedDensity_SG],
	SUM(q.[Quantity_kMT])
FROM [Olefins].[fact].[FacilitiesHydroTreat]	t
INNER JOIN [Olefins].[fact].[Quantity]			q
	ON	q.[Refnum]		= t.[Refnum]
	AND	q.[StreamID]	IN ('Benzene', 'PyroGasoline')
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[FeedRate_kBsd]		> 0.0
	AND	t.[FeedProcessed_kMT]	> 0.0
	AND	t.[FeedDensity_SG]		> 0.0
GROUP BY
	t.[Refnum],
	t.[FeedRate_kBsd],
	t.[HTTypeID],
	t.[FeedProcessed_kMT],
	t.[FeedDensity_SG];

--	[stage].[FeedClass]

--SELECT
--	'(''' + RTRIM(LTRIM(t.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, t.[FeedClass])
--	+ '),'
--	--RTRIM(LTRIM(t.[Refnum])),
--	--t.[FeedClass]
--FROM [Olefins].[dbo].[TSort]	t
--INNER JOIN [Olefins].[dbo].[_RL]	l
--	ON	l.[Refnum] = t.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE');

SELECT DISTINCT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, COALESCE(s.[PeerGroup], t.[PeerGroup]))
	+ '),'
FROM [Olefins].[calc].[PeerGroupFeedClass]				t
LEFT OUTER JOIN [Olefins].[super].[PeerGroupFeedClass]	s
	ON	s.[Refnum]	= t.[Refnum]
WHERE t.[Refnum] LIKE '2013PCH%'
	AND	t.[FactorSetId] = '2013';

------------------------------------------------------------------------------

DECLARE @MapStreams	TABLE
(
	[FeedProdId]		VARCHAR(25),
	[StreamTag]			VARCHAR(25),
	[StreamNumber]		INT,
	PRIMARY KEY CLUSTERED([FeedProdId] ASC)
)

INSERT INTO @MapStreams([FeedProdId], [StreamTag], [StreamNumber])
SELECT ms.[FeedProdId], ms.[StreamTag], ms.[StreamNumber]
FROM (VALUES
	('Ethane',			'Ethane',			1001),
	('EPMix',			'EPMix',			1002),
	('Propane',			'Propane',			1003),
	('LPG',				'LPG',				1004),
	('Butane',			'Butane',			1005),
	('OthLtFeed',		'FeedLtOther',		1006),

	('LtNaphtha',		'NaphthaLt',		2001),
	('FRNaphtha',		'NaphthaFr',		2002),
	('HeavyNaphtha',	'NaphthaHv',		2003),
	('Raffinate',		'Raffinate',		2004),
	('HeavyNGL',		'HeavyNGL',			2005),
	('Condensate',		'Condensate',		2006),
	('Diesel',			'Diesel',			2007),
	('HeavyGasoil',		'GasOilHv',			2008),
	('HTGasoil',		'GasOilHt',			2009),
	('OthLiqFeed1',		'FeedLiqOther',		2010),
	('OthLiqFeed2',		'FeedLiqOther',		2011),
	('OthLiqFeed3',		'FeedLiqOther',		2012),

	('EthRec',			'EthRec',			3001),
	('ProRec',			'ProRec',			3002),
	('ButRec',			'ButRec',			3003),

	('ConcEthylene',	'ConcEthylene',		4001),
	('ConcPropylene',	'ConcPropylene',	4002),
	('ConcButadiene',	'ConcButadiene',	4003),
	('ConcBenzene',		'ConcBenzene',		4004),

	('RefHydrogen',		'RogHydrogen',		4005),
	('RefMethane',		'RogMethane',		4006),
	('RefEthane',		'RogEthane',		4007),
	('RefEthylene',		'RogEthylene',		4008),
	('RefPropane',		'RogPropane',		4009),
	('RefPropylene',	'RogPropylene',		4010),
	('RefC4Plus',		'RogC4Plus',		4011),
	('RefInerts',		'RogInerts',		4012),

	('DiHydrogen',		'DilHydrogen',		4013),
	('DiMethane',		'DilMethane',		4014),
	('DiEthane',		'DilEthane',		4015),
	('DiEthylene',		'DilEthylene',		4016),
	('DiPropane',		'DilPropane',		4017),
	('DiPropylene',		'DilPropylene',		4018),
	('DiButane',		'DilButane',		4019),
	('DiButylenes',		'DilButylene',		4020),
	('DiButadiene',		'DilButadiene',		4021),
	('DiBenzene',		'DilBenzene',		4022),
	('DiMogas',			'DilMogas',			4023),

	('WashOil',			'SuppWashOil',		4024),
	('GasOil',			'SuppGasOil',		4025),
	('OthSpl',			'SuppOther',		4026),

	('Hydrogen',		'Hydrogen',			5001),
	('FuelGasSales',	'Methane',			5002),
	('Acetylene',		'Acetylene',		5003),
	('Ethylene',		'EthylenePG',		5004),
	('EthyleneCG',		'EthyleneCG',		5005),
	('Propylene',		'PropylenePG',		5006),
	('PropyleneCG',		'PropyleneCG',		5007),
	('PropyleneRG',		'PropyleneRG',		5008),
	('PropaneC3',		'PropaneC3Resid',	5009),
	('Butadiene',		'Butadiene',		5010),
	('Isobutylene',		'Isobutylene',		5011),
	('OthC4',			'C4Oth',			5012),
	('Benzene',			'Benzene',			5013),
	('OthPyGas',		'PyroGasoline',		5014),
	('PyGasoil',		'PyroGasOil',		5015),
	('PyFuelOil',		'PyroFuelOil',		5016),
	('AcidGas',			'AcidGas',			5017),

	('PPCFuel',			'PPFC',				5018),
	('PPCFuel_CH4',		'PPFC',				5018),
	('PPCFuel_ETH',		'PPFC',				5018),
	('PPCFuel_H2',		'PPFC',				5018),
	('PPCFuel_Other',	'PPFC',				5018),

	('OthProd1',		'ProdOther',		5019),
	('OthProd2',		'ProdOther',		5020),
	('FlareLoss',		'LossFlareVent',	5022),
	('OthLoss',			'LossOther',		5023),
	('MeasureLoss',		'LossMeasure',		5024)
	) [ms]([FeedProdId], [StreamTag], [StreamNumber]);

DECLARE @MapComponent	TABLE
(
	[FeedProdId]		VARCHAR(25),
	[ComponentTag]		VARCHAR(25),
	PRIMARY KEY CLUSTERED([FeedProdId] ASC)
);

INSERT INTO @MapComponent([FeedProdId], [ComponentTag])
VALUES
	('Hydrogen',		'H2'),
	('FuelGasSales',	'CH4'),
	('Ethylene',		'C2H4'),
	('EthyleneCG',		'C2H4'),
	('Propylene',		'C3H6'),
	('PropyleneCG',		'C3H6'),
	('PropyleneRG',		'C3H6'),
	('PropaneC3',		'C3H8'),
	('PPCFuel_CH4',		'CH4'),
	('PPCFuel_ETH',		'C2H4'),
	('PPCFuel_H2',		'H2'),
	('PPCFuel_Other',	'C2H6');

--	[stage].[StreamComposition]

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
	+ ', ''' + CASE t.[ComponentId]
				WHEN 'IB' THEN 'IBB2'
				ELSE t.[ComponentId]
				END
				 + ''''
	+ ', ' + CONVERT(VARCHAR, t.[Component_WtPcnt])
	+ '),'
FROM [Olefins].[fact].[CompositionQuantity]		t
INNER JOIN @MapStreams							s
	ON	s.[StreamTag]	= t.[StreamId]
	AND	s.[FeedProdId] NOT IN ('PPCFuel_CH4', 'PPCFuel_ETH', 'PPCFuel_H2', 'PPCFuel_Other')
	AND	s.[StreamNumber] = CASE
			WHEN CHARINDEX(' (Other Feed 1)', t.[StreamDescription]) >= 1 THEN 2010
			WHEN CHARINDEX(' (Other Feed 2)', t.[StreamDescription]) >= 1 THEN 2011
			WHEN CHARINDEX(' (Other Feed 3)', t.[StreamDescription]) >= 1 THEN 2012
			WHEN CHARINDEX(' (Other Prod 1)', t.[StreamDescription]) >= 1 THEN 5019
			WHEN CHARINDEX(' (Other Prod 2)', t.[StreamDescription]) >= 1 THEN 5020
			ELSE s.[StreamNumber]
		END
WHERE t.[Refnum] LIKE '2013PCH%'
ORDER BY	t.[Refnum]	ASC,
	s.[StreamNumber]	ASC,
	t.[ComponentId]		ASC;

--	[stage].[StreamComposition]	(Products)

--SELECT
--	'(''' +  RTRIM(LTRIM(q.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_ComponentId](c.[ComponentTag]))
--	+ ', ' + CONVERT(VARCHAR, p.[WtPcnt] * 100.0)
--	+ '),'
--FROM [Olefins].[dbo].[Quantity]		q
--INNER JOIN [Olefins].[dbo].[_RL]			l
--	ON	l.[Refnum] = q.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--INNER JOIN [Olefins].[dbo].[ProdQuality]	p
--	ON	p.[Refnum] = q.[Refnum]
--	AND	p.[FeedProdId] = q.[FeedProdId]
--INNER JOIN @MapStreams						s
--	ON	s.[FeedProdId]	= q.[FeedProdId]
--INNER JOIN @MapComponent					c
--	ON	c.[FeedProdId]	= q.[FeedProdId]
--WHERE  (COALESCE(q.[Q1Feed], 0.0) +
--		COALESCE(q.[Q2Feed], 0.0) +
--		COALESCE(q.[Q3Feed], 0.0) +
--		COALESCE(q.[Q4Feed], 0.0)	> 0.0)
--	AND	p.[WtPcnt]		>  0.0
--	AND p.[WtPcnt]		<= 1.0
--	AND	q.[FeedProdID]	<> 'PPCFuel';

--	[stage].[StreamComposition]	(PPFC)

--SELECT
--	'(''' +  RTRIM(LTRIM(p.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_ComponentId](c.[ComponentTag]))
--	+ ', ' + CONVERT(VARCHAR, p.[WtPcnt] * 100.0)
--	+ '),'
--FROM [Olefins].[dbo].[ProdQuality]		p
--INNER JOIN [Olefins].[dbo].[_RL]			l
--	ON	l.[Refnum] = p.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--INNER JOIN @MapStreams						s
--	ON	s.[FeedProdId]	= p.[FeedProdId]
--INNER JOIN @MapComponent					c
--	ON	c.[FeedProdId]	= p.[FeedProdId]
--WHERE	p.[WtPcnt]		>  0.0
--	AND p.[WtPcnt]		<= 1.0
--	AND	s.StreamNumber	= 5018;

--	[stage].[StreamComposition]	(Other Products)

--SELECT
--	'(''' +  RTRIM(LTRIM(u.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_ComponentId](u.[ComponentTag]))
--	+ ', ' + CONVERT(VARCHAR, u.[Component_WtPcnt] * 100.0)
--	+ '),'
--FROM (
--	SELECT
--		q.[Refnum],
--		q.[FeedProdId],
--		q.[H2],
--		q.[CH4],
--		q.[C2H2],
--		q.[C2H6],
--		q.[C2H4],
--		q.[C3H6],
--		q.[C3H8],
--		q.[BUTAD]	[C4H6],
--		q.[C4S]		[C4H8],
--		q.[C4H10],
--		q.[BZ]		[C6H6],
--		q.[PYGAS]	[PyroGasOil],
--		q.[PYOIL]	[PyroFuelOil],
--		q.[INERT]	[Inerts]
--	FROM  [Olefins].[dbo].[Composition]	q
--	INNER JOIN [Olefins].[dbo].[_RL]		l
--		ON	l.[Refnum] = q.[Refnum]
--		AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--	WHERE q.[FeedProdId] IN ('OthProd1', 'OthProd2')
--	) p
--	UNPIVOT (
--		[Component_WtPcnt] FOR [ComponentTag] IN (
--			p.[H2],
--			p.[CH4],
--			p.[C2H2],
--			p.[C2H6],
--			p.[C2H4],
--			p.[C3H6],
--			p.[C3H8],
--			p.[C4H6],
--			p.[C4H8],
--			p.[C4H10],
--			p.[C6H6],
--			p.[PyroGasOil],
--			p.[PyroFuelOil],
--			p.[Inerts]
--		)
--	) u
--INNER JOIN @MapStreams		s
--	ON	s.[FeedProdId] = u.[FeedProdID]
--WHERE	u.[Component_WtPcnt] >	0.0
--	AND	u.[Component_WtPcnt] <= 1.0;

--	[stage].[StreamComposition]	(Feed)

--SELECT
--	'(''' +  RTRIM(LTRIM(u.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_ComponentId](u.[ComponentTag]))
--	+ ', ' + CONVERT(VARCHAR, u.[Component_WtPcnt] * 100.0)
--	+ '),'
--FROM (
--	SELECT
--		q.[Refnum],
--		q.[FeedProdId],
--		p.[Methane]			[CH4],
--		p.[Ethane]			[C2H6],
--		p.[Ethylene]		[C2H4],
--		p.[Propane]			[C3H8],
--		p.[Propylene]		[C3H6],
--		p.[nButane]			[NBUTA],
--		p.[iButane]			[IBUTA],
--		p.[Isobutylene]		[B2],
--		p.[Butene1]			[B1],
--		p.[Butadiene]		[C4H6],
--		p.[nPentane]		[NC5],
--		p.[iPentane]		[IC5],
--		p.[nHexane]			[NC6],
--		p.[iHexane]			[C6ISO],
--		p.[Septane]			[C7H16],
--		p.[Octane]			[C8H18],
--		p.[CO2]				[CO_CO2],
--		p.[Hydrogen]		[H2],
--		p.[Sulfur]			[S],
--		p.[NParaffins]		[P],
--		p.[IsoParaffins]	[I],
--		p.[Naphthenes]		[N],
--		p.[Olefins]			[O],
--		p.[Aromatics]		[A]
--	FROM [Olefins].[dbo].[Quantity]			q
--	INNER JOIN [Olefins].[dbo].[_RL]			l
--		ON	l.[Refnum] = q.[Refnum]
--		AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--	INNER JOIN  [Olefins].[dbo].[FeedQuality]	p
--		ON	p.[Refnum] = q.[Refnum]
--		AND	p.[FeedProdId] = q.[FeedProdId]
--	--WHERE  (COALESCE(q.[Q1Feed], 0.0) +
--	--		COALESCE(q.[Q2Feed], 0.0) +
--	--		COALESCE(q.[Q3Feed], 0.0) +
--	--		COALESCE(q.[Q4Feed], 0.0)	> 0.0)
--	) p
--	UNPIVOT (
--		[Component_WtPcnt] FOR [ComponentTag] IN (
--			p.[CH4],
--			p.[C2H6],
--			p.[C2H4],
--			p.[C3H8],
--			p.[C3H6],
--			p.[NBUTA],
--			p.[IBUTA],
--			p.[B2],
--			p.[B1],
--			p.[C4H6],
--			p.[NC5],
--			p.[IC5],
--			p.[NC6],
--			p.[C6ISO],
--			p.[C7H16],
--			p.[C8H18],
--			p.[CO_CO2],
--			p.[H2],
--			p.[S],
--			p.[P],
--			p.[I],
--			p.[N],
--			p.[O],
--			p.[A]
--		)
--	) u
--INNER JOIN @MapStreams							s
--	ON	s.[FeedProdId] = u.[FeedProdID]
--WHERE	u.[Component_WtPcnt] >	0.0
--	AND	u.[Component_WtPcnt] <= 1.0;

--	[stage].[StreamRecycled]

--SELECT
--	'(''' +  RTRIM(LTRIM(u.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_ComponentId](u.[ComponentTag]))
--	+ ', ' + CONVERT(VARCHAR, u.[Recycled_WtPcnt] * 100.0)
--	+ '),'
--FROM (
--	SELECT
--		m.[Refnum],
--		m.[EthPcntTot]	[C2H6],
--		m.[ProPcntTot]	[C3H8],
--		m.[ButPcntTot]	[C4H10]
--	FROM [Olefins].[dbo].[Misc]			m
--	INNER JOIN [Olefins].[dbo].[_RL]			l
--		ON	l.[Refnum] = m.[Refnum]
--		AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--	) p
--	UNPIVOT (
--		[Recycled_WtPcnt] FOR [ComponentTag] IN (
--			p.[C2H6],
--			p.[C3H8],
--			p.[C4H10]
--		)
--	) u
--WHERE	u.[Recycled_WtPcnt] >  0.0
--	AND	u.[Recycled_WtPcnt] <= 1.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, CASE t.[ComponentId]
				WHEN 'C2H6'		THEN 18
				WHEN 'C3H8'		THEN 19
				WHEN 'C4H10'	THEN 21
				END)
	+ ', ' + CONVERT(VARCHAR, t.[Recycled_WtPcnt])
	+ '),'
FROM [Olefins].[fact].[QuantitySuppRecycled]	t
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[Recycled_WtPcnt] <> 0.0
ORDER BY	t.[Refnum]	ASC,
	CASE t.[ComponentId]
				WHEN 'C2H6'		THEN 18
				WHEN 'C3H8'		THEN 19
				WHEN 'C4H10'	THEN 21
				END;

--	[stage].[StreamCompositionMol]

--SELECT
--	'(''' +  RTRIM(LTRIM(q.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_ComponentId](c.[ComponentTag]))
--	+ ', ' + CONVERT(VARCHAR, p.[MolePcnt] * 100.0)
--	+ '),'
--FROM [Olefins].[dbo].[Quantity]			q
--INNER JOIN [Olefins].[dbo].[_RL]			l
--	ON	l.[Refnum] = q.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--INNER JOIN [Olefins].[dbo].[ProdQuality]	p
--	ON	p.[Refnum] = q.[Refnum]
--	AND	p.[FeedProdId] = q.[FeedProdId]
--INNER JOIN @MapStreams							s
--	ON	s.[FeedProdId]	= q.[FeedProdID]
--INNER JOIN @MapComponent						c
--	ON	c.[FeedProdId]	= q.[FeedProdID]
--WHERE  (COALESCE(q.[Q1Feed], 0.0) +
--		COALESCE(q.[Q2Feed], 0.0) +
--		COALESCE(q.[Q3Feed], 0.0) +
--		COALESCE(q.[Q4Feed], 0.0)	> 0.0)
--	AND	p.[MolePcnt]	>  0.0
--	AND p.[MolePcnt]	<= 1.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
	+ ', ' + CONVERT(VARCHAR, CASE s.[StreamNumber] WHEN 5001 THEN 7 WHEN 5002 THEN 17 END)
	+ ', ' + CONVERT(VARCHAR, [Olefins].[calc].[ConvMoleWeight](t.[ComponentID], t.[Component_WtPcnt], 'M'))
	+ '),'
FROM [Olefins].[fact].[CompositionQuantity]	t
INNER JOIN @MapStreams							s
	ON	s.[StreamTag]	= t.[StreamId]
	AND	s.[FeedProdId] NOT IN ('PPCFuel_CH4', 'PPCFuel_ETH', 'PPCFuel_H2', 'PPCFuel_Other')
	AND	s.[StreamNumber] = CASE
			WHEN CHARINDEX(' (Other Feed 1)', t.[StreamDescription]) >= 1 THEN 2010
			WHEN CHARINDEX(' (Other Feed 2)', t.[StreamDescription]) >= 1 THEN 2011
			WHEN CHARINDEX(' (Other Feed 3)', t.[StreamDescription]) >= 1 THEN 2012
			WHEN CHARINDEX(' (Other Prod 1)', t.[StreamDescription]) >= 1 THEN 5019
			WHEN CHARINDEX(' (Other Prod 2)', t.[StreamDescription]) >= 1 THEN 5020
			ELSE s.[StreamNumber]
		END
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[StreamId]	IN ('Hydrogen', 'Methane')
	AND	t.[ComponentID]	IN ('H2', 'CH4')
ORDER BY	t.[Refnum]	ASC,
	s.[StreamNumber]	ASC;

--	[stage].[StreamRecovered]

--SELECT
--	'(''' +  RTRIM(LTRIM(q.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, CASE WHEN q.[RecPcnt] = 100.0 THEN 1.0 ELSE q.[RecPcnt] END * 100.0)
--	+ '),'
--FROM [Olefins].[dbo].[Quantity]		q
--INNER JOIN [Olefins].[dbo].[_RL]		l
--	ON	l.[Refnum] = q.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--INNER JOIN @MapStreams						s
--	ON	s.[FeedProdId]	= q.[FeedProdID]
--WHERE  (COALESCE(q.[Q1Feed], 0.0) +
--		COALESCE(q.[Q2Feed], 0.0) +
--		COALESCE(q.[Q3Feed], 0.0) +
--		COALESCE(q.[Q4Feed], 0.0)	> 0.0)
--	AND	q.[RecPcnt]		>  0.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
	+ ', ' + CONVERT(VARCHAR, ABS(t.[Recovered_WtPcnt]))
	+ '),'
FROM [Olefins].[fact].[QuantitySuppRecovery]	t
INNER JOIN @MapStreams							s
	ON	s.[StreamTag]	= t.[StreamId]
	AND	s.[FeedProdId] NOT IN ('PPCFuel_CH4', 'PPCFuel_ETH', 'PPCFuel_H2', 'PPCFuel_Other')
	AND	s.[StreamNumber] = CASE
			WHEN CHARINDEX(' (Other Feed 1)', t.[StreamDescription]) >= 1 THEN 2010
			WHEN CHARINDEX(' (Other Feed 2)', t.[StreamDescription]) >= 1 THEN 2011
			WHEN CHARINDEX(' (Other Feed 3)', t.[StreamDescription]) >= 1 THEN 2012
			WHEN CHARINDEX(' (Other Prod 1)', t.[StreamDescription]) >= 1 THEN 5019
			WHEN CHARINDEX(' (Other Prod 2)', t.[StreamDescription]) >= 1 THEN 5020
			ELSE s.[StreamNumber]
		END
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[Recovered_WtPcnt]	IS NOT NULL;

--	[stage].[StreamDensity]

--SELECT
--	'(''' +  RTRIM(LTRIM(q.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, d.[SG])
--	+ '),'
--FROM [Olefins].[dbo].[Quantity]				q
--INNER JOIN [Olefins].[dbo].[_RL]				l
--	ON	l.[Refnum] = q.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--LEFT OUTER JOIN [Olefins].[dbo].[FeedQuality]	d
--	ON	d.[Refnum] = q.[Refnum]
--	AND	d.[FeedProdID] = q.[FeedProdID]
--INNER JOIN @MapStreams	s
--	ON	s.[FeedProdId] = q.[FeedProdID]
--WHERE  (COALESCE(q.[Q1Feed], 0.0) +
--		COALESCE(q.[Q2Feed], 0.0) +
--		COALESCE(q.[Q3Feed], 0.0) +
--		COALESCE(q.[Q4Feed], 0.0)	> 0.0)
--	AND	d.[SG] <> 0.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
	+ ', ' + CONVERT(VARCHAR, ABS(t.[Density_SG]))
	+ '),',
	 t.*
FROM [Olefins].[fact].[FeedStockCrackingParameters]	t
INNER JOIN @MapStreams									s
	ON	s.[StreamTag]	= t.[StreamId]
	AND	s.[FeedProdId] NOT IN ('PPCFuel_CH4', 'PPCFuel_ETH', 'PPCFuel_H2', 'PPCFuel_Other')
	AND	s.[StreamNumber] = CASE
			WHEN CHARINDEX(' (Other Feed 1)', t.[StreamDescription]) >= 1 THEN 2010
			WHEN CHARINDEX(' (Other Feed 2)', t.[StreamDescription]) >= 1 THEN 2011
			WHEN CHARINDEX(' (Other Feed 3)', t.[StreamDescription]) >= 1 THEN 2012
			WHEN CHARINDEX(' (Other Prod 1)', t.[StreamDescription]) >= 1 THEN 5019
			WHEN CHARINDEX(' (Other Prod 2)', t.[StreamDescription]) >= 1 THEN 5020
			ELSE s.[StreamNumber]
		END
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[Density_SG]		IS NOT NULL;

--	[stage].[StreamDescription]

--SELECT
--	'(''' +  RTRIM(LTRIM(q.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + '''' + REPLACE(COALESCE(q.[MiscFeed], q.[OthProdDesc], q.[MiscProd1], q.[MiscProd2], d.[OthLiqFeedDESC]), '''', '''''') + ''''
--	+ '),'
--FROM [Olefins].[dbo].[Quantity]				q
--INNER JOIN [Olefins].[dbo].[_RL]			l
--	ON	l.[Refnum] = q.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--LEFT OUTER JOIN [Olefins].[dbo].[FeedQuality]	d
--	ON	d.[Refnum] = q.[Refnum]
--	AND	d.[FeedProdID] = q.[FeedProdID]
--INNER JOIN @MapStreams								s
--	ON	s.[FeedProdId] = q.[FeedProdID]
--WHERE  (COALESCE(q.[Q1Feed], 0.0) +
--		COALESCE(q.[Q2Feed], 0.0) +
--		COALESCE(q.[Q3Feed], 0.0) +
--		COALESCE(q.[Q4Feed], 0.0)	> 0.0)
--	AND	COALESCE(q.[MiscFeed], q.[OthProdDesc], q.[MiscProd1], q.[MiscProd2], d.[OthLiqFeedDESC]) IS NOT NULL
--	AND COALESCE(q.[MiscFeed], q.[OthProdDesc], q.[MiscProd1], q.[MiscProd2], d.[OthLiqFeedDESC]) <> '0';

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
	+ ', ''' + REPLACE(t.[StreamDescription], '''', '''''') + ''''
	+ '),'
FROM [Olefins].[fact].[StreamQuantityAggregate]	t WITH (NOEXPAND)
INNER JOIN @MapStreams								s
	ON	s.[StreamTag]	= t.[StreamId]
	AND	s.[FeedProdId] NOT IN ('PPCFuel_CH4', 'PPCFuel_ETH', 'PPCFuel_H2', 'PPCFuel_Other')
	AND	s.[StreamNumber] = CASE
			WHEN CHARINDEX(' (Other Feed 1)', t.[StreamDescription]) >= 1 THEN 2010
			WHEN CHARINDEX(' (Other Feed 2)', t.[StreamDescription]) >= 1 THEN 2011
			WHEN CHARINDEX(' (Other Feed 3)', t.[StreamDescription]) >= 1 THEN 2012
			WHEN CHARINDEX(' (Other Prod 1)', t.[StreamDescription]) >= 1 THEN 5019
			WHEN CHARINDEX(' (Other Prod 2)', t.[StreamDescription]) >= 1 THEN 5020
			ELSE s.[StreamNumber]
		END
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[FactorSetId] = LEFT(t.[Refnum], 4)
	AND	t.[StreamDescription]	IS NOT NULL
ORDER BY
	t.[Refnum]			ASC,
	s.[StreamNumber]	ASC;

--	[stage].[StreamQuantity]

--SELECT
--	'(''' +  RTRIM(LTRIM(q.[Refnum])) + ''''
--	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
--	+ ', ' + CONVERT(VARCHAR, [dim].[Return_StreamId](s.[StreamTag]))
--	+ ', ' + CONVERT(VARCHAR,
--				COALESCE(q.[Q1Feed], 0.0) +
--				COALESCE(q.[Q2Feed], 0.0) +
--				COALESCE(q.[Q3Feed], 0.0) +
--				COALESCE(q.[Q4Feed], 0.0))
--	+ '),'
--FROM [Olefins].[dbo].[Quantity]	q
--INNER JOIN [Olefins].[dbo].[_RL]	l
--	ON	l.[Refnum] = q.[Refnum]
--	AND	l.ListName IN ('_07PCH', '_09PCH', '_11PCH', '13PCH+LATE')
--INNER JOIN @MapStreams					s
--	ON	s.[FeedProdId] = q.[FeedProdID]
--WHERE	COALESCE(q.[Q1Feed], 0.0) +
--		COALESCE(q.[Q2Feed], 0.0) +
--		COALESCE(q.[Q3Feed], 0.0) +
--		COALESCE(q.[Q4Feed], 0.0)	> 0.0;

SELECT
	'(''' +  RTRIM(LTRIM(t.[Refnum])) + ''''
	+ ', ' + CONVERT(VARCHAR, s.[StreamNumber])
	+ ', ''' + CONVERT(VARCHAR, t.[StreamId]) + ''''
	+ ', ' + CONVERT(VARCHAR, ABS(t.[Quantity_kMT]))
	+ '),'
FROM [Olefins].[fact].[StreamQuantityAggregate]	t WITH (NOEXPAND)
INNER JOIN @MapStreams								s
	ON	s.[StreamTag]	= t.[StreamId]
	AND	s.[FeedProdId] NOT IN ('PPCFuel_CH4', 'PPCFuel_ETH', 'PPCFuel_H2', 'PPCFuel_Other')
	AND	s.[StreamNumber] = CASE
			WHEN CHARINDEX(' (Other Feed 1)', t.[StreamDescription]) >= 1 THEN 2010
			WHEN CHARINDEX(' (Other Feed 2)', t.[StreamDescription]) >= 1 THEN 2011
			WHEN CHARINDEX(' (Other Feed 3)', t.[StreamDescription]) >= 1 THEN 2012
			WHEN CHARINDEX(' (Other Prod 1)', t.[StreamDescription]) >= 1 THEN 5019
			WHEN CHARINDEX(' (Other Prod 2)', t.[StreamDescription]) >= 1 THEN 5020
			ELSE s.[StreamNumber]
		END
WHERE	t.[Refnum] LIKE '2013PCH%'
	AND	t.[FactorSetId] = LEFT(t.[Refnum], 4)
ORDER BY
	RTRIM(LTRIM(t.[Refnum]))	ASC,
	s.[StreamNumber]			ASC,
	t.[StreamId]				ASC;






------------------------------------------------------------------------------
------------------------------------------------------------------------------

SELECT
'(' + CONVERT(VARCHAR, s.[MethodologyId])
+ ', ' + CONVERT(VARCHAR, s.[SubmissionId])
+ ', ' + CONVERT(VARCHAR, s.[StandardId])
+ ', ' + CONVERT(VARCHAR, s.[ProcessUnitId])
+ ', ' + CONVERT(VARCHAR, s.[StandardValue], 2)
+ '),'
FROM [calc].[Standards] s
WHERE	s.[StandardId] = dim.Return_StandardId('PersNonMaint')

SELECT
'(''' +  RTRIM(LTRIM(s.[Refnum])) + ''''
+ ', ' + CONVERT(VARCHAR, s.[StandardId])
+ ', ' + CONVERT(VARCHAR, s.[ProcessUnitId])
+ ', ' + CONVERT(VARCHAR, s.[StandardValue], 2)
+ '),'
FROM [xls].[Standards] s
WHERE	s.ProcessUnitId = dim.Return_ProcessUnitId('Fresh');
