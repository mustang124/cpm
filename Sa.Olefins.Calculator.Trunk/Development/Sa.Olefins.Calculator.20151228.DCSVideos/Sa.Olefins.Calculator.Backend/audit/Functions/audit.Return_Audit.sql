﻿CREATE FUNCTION [audit].[Return_Audit]
(
	@RunInThePastDays			INT  = 10
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
SELECT
	[c].[CompanyId],
	[c].[CompanyName],
	[p].[PlantId],
	[p].[PlantName],
	[l].[LoginId],
	[l].[LoginTag],
	[z].[SubmissionId],
	[z].[SubmissionName],
	[SubDate]			= CONVERT(DATE, [z].[tsModified]),
	[FactBeg]			= CONVERT(TIME(2), [f].[TimeBeg]),
	[CalcEnd]			= CONVERT(TIME(2), [k].[TimeEnd]),
	[DataSetErrors]		= [e].[Items],
	[StandardsCount]	= [s].[Items],
	'-'[-],
	[t].[TransactionID],
	[TimeBeg]			= CONVERT(TIME(2), [t].[TimeBeg]),
	[TimeEnd]			= CONVERT(TIME(2), [t].[TimeEnd]),
	[Objects (8272)]	= [t].[Items],
	[Procedure] = '[' + [d].[ProcedureSchema] + '].[' + [d].[ProcedureName] + ']',
	[d].[ErrorMessage]
FROM	[stage].[Submissions]						[z]
LEFT OUTER JOIN [auth].[JoinPlantSubmission]		[jps]
	ON	[jps].[SubmissionId]	= [z].[SubmissionId]
LEFT OUTER JOIN [auth].[Plants]						[p]
	ON	[p].[PlantId]			= [jps].[PlantId]
LEFT OUTER JOIN [auth].[JoinCompanyPlant]			[jcp]
	ON	[jcp].[PlantId]			= [jps].[PlantId]
LEFT OUTER JOIN [auth].[Companies]					[c]
	ON	[c].CompanyId			= [jcp].[CompanyId]
LEFT OUTER JOIN	[auth].[JoinLoginSubmission]		[jls]
	ON	[jls].[SubmissionId]	= [z].[SubmissionId]
LEFT OUTER JOIN [auth].[Logins]						[l]
	ON	[l].[LoginId]			= [jls].[LoginId]

LEFT OUTER JOIN [fact].[Audit]						[f]
	ON	[f].[SubmissionId]		= [z].[SubmissionId]
LEFT OUTER JOIN [calc].[Audit]						[k]
	ON	[k].[SubmissionId]		= [z].[SubmissionId]
LEFT OUTER JOIN [audit].[LogError]					[d]
	ON	[d].[ProcedureParam]	LIKE '@SubmissionId:' + CONVERT(VARCHAR, [z].[SubmissionId]) + '%'

LEFT OUTER JOIN (
	SELECT
		[s].[SubmissionId], 
		[Items] = COUNT_BIG(*)
	FROM [stage].[Errors_DataSet] [s]
	GROUP BY
		[s].[SubmissionId]
	)												[e]
	ON	[e].[SubmissionId]		= [z].[SubmissionId]

LEFT OUTER JOIN (
	SELECT
		[s].[SubmissionId], 
		[Items] = COUNT_BIG(*)
	FROM [calc].[Standards] [s]
	GROUP BY
		[s].[SubmissionId]
	)												[s]
	ON	[s].[SubmissionId]		= [z].[SubmissionId]

LEFT OUTER JOIN	(
	SELECT
		[TransactionID]		= [t].[TransactionID],
		[TimeBeg]			= TODATETIMEOFFSET(MIN([t].[StartTime]), -360),
		[TimeEnd]			= TODATETIMEOFFSET(MAX([t].[EndTime]), -360),
		[Duration (mcs)]	= SUM([t].[Duration]),
		[Items]				= COUNT_BIG(*)
	FROM [audit].[Trace]	[t]
	WHERE	[t].[TransactionID]	IS NOT NULL
		AND	[t].[EndTime]		IS NOT NULL
		AND	[t].[NTUserName]	IS NULL
		AND	[t].[DatabaseName]	= 'OlefinsOnline'
		AND	[t].[HostName]		= 'DBS8'
		AND	[t].[ObjectType]	= 8272
	GROUP BY
		[t].[TransactionID]
	HAVING
		COUNT_BIG(*)	>= 0
	)												[t]
	ON	CONVERT(DATETIME2(2), [t].[TimeBeg])	<  CONVERT(DATETIME2(2), [f].[TimeBeg])
	AND	CONVERT(DATETIME2(2), [t].[TimeEnd])	>= CONVERT(DATETIME2(2), [k].[TimeEnd])
WHERE	[z].[SubmissionId]	>  120
	AND	DATEDIFF(d, [z].[tsModified], SYSDATETIMEOFFSET())	<= @RunInThePastDays
);