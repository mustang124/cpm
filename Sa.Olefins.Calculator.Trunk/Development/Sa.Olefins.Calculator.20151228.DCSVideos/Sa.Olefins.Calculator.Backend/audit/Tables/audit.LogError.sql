﻿CREATE TABLE [audit].[LogError]
(
	[ErrorNumber]			INT					NOT	NULL,
	[XActState]				SMALLINT			NOT	NULL,
	[ProcedureSchema]		SYSNAME				NOT	NULL	CONSTRAINT [CL_LogError_ProcedureSchema]	CHECK([ProcedureSchema]		<> ''),
	[ProcedureName]			VARCHAR(128)		NOT	NULL	CONSTRAINT [CL_LogError_ProcedureName]		CHECK([ProcedureName]		<> ''),
	[ProcedureLine]			INT					NOT	NULL	CONSTRAINT [CR_LogError_ProcedureLine]		CHECK([ProcedureLine]		> 0),
	[ProcedureParam]		VARCHAR(4000)			NULL	CONSTRAINT [CL_LogError_ProcedureParam]		CHECK([ProcedureParam]		<> ''),
	[ErrorMessage]			VARCHAR(MAX)		NOT	NULL	CONSTRAINT [CL_LogError_ErrorMessage]		CHECK([ErrorMessage]		<> ''),
	[ErrorSeverity]			INT					NOT	NULL,
	[ErrorState]			INT					NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_LogError_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogError_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogError_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogError_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_LogError]					PRIMARY KEY CLUSTERED([tsModified] DESC, [tsModifiedRV] DESC)
);
GO

CREATE TRIGGER [audit].[t_LogError_u]
ON [audit].[LogError]
AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [audit].[LogError]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[audit].[LogError].[tsModifiedRV]		= INSERTED.[tsModifiedRV];

END;