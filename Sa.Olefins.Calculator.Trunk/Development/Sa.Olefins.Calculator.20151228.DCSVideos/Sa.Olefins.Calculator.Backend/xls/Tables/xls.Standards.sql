﻿CREATE TABLE [xls].[Standards]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_Standards_Refnum]				CHECK([Refnum] <> ''),
	[StandardId]			INT					NOT NULL	CONSTRAINT [FK_Standards_Standard_LookUp]		REFERENCES [dim].[Standard_LookUp] ([StandardId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ProcessUnitId]			INT					NOT NULL	CONSTRAINT [FK_Standards_ProcessUnit]			REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[StandardValue]			FLOAT				NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Standards_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standards_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standards_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standards_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Standards]					PRIMARY KEY CLUSTERED([Refnum] DESC, [StandardId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [xls].[t_Standards_u]
ON [xls].[Standards]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[Standards]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[Standards].[Refnum]			= INSERTED.[Refnum]
		AND	[xls].[Standards].[StandardId]		= INSERTED.[StandardId]
		AND	[xls].[Standards].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;
GO