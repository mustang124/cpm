﻿CREATE TABLE [xls].[HydrogenPurification]
(
	[Refnum]					VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_HydrogenPurification_Refnum]							CHECK([Refnum] <> ''),
	
	[FacilityId]				INT					NOT	NULL	CONSTRAINT [FK_HydrogenPurification_Facility_LookUp]				REFERENCES [dim].[Facility_LookUp] ([FacilityId])									ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]				INT					NOT	NULL,
	[StreamId]					INT					NOT	NULL	CONSTRAINT [FK_HydrogenPurification_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])										ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]				INT					NOT	NULL	CONSTRAINT [FK_HydrogenPurification_Component_LookUp]				REFERENCES [dim].[Component_LookUp] ([ComponentId])									ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[Component_WtPcnt]			FLOAT				NOT	NULL	CONSTRAINT [CR_HydrogenPurification_Component_WtPcnt_MinIncl_0.0]	CHECK([Component_WtPcnt] >= 0.0),
																CONSTRAINT [CR_HydrogenPurification_Component_WtPcnt_MaxIncl_100.0]	CHECK([Component_WtPcnt] <= 100.0),
	[Component_kMT]				FLOAT				NOT	NULL	CONSTRAINT [CR_HydrogenPurification_Component_kMT_MinIncl_0.0]		CHECK([Component_kMT] >= 0.0),
	[Utilization_Pcnt]			FLOAT				NOT	NULL	CONSTRAINT [CR_HydrogenPurification_Utilization_Pcnt_MinIncl_0.0]	CHECK([Utilization_Pcnt] >= 0.0),
																CONSTRAINT [CR_HydrogenPurification_Utilization_Pcnt_MaxIncl_100.0]	CHECK([Utilization_Pcnt] <= 110.0),
	[H2Unit_Count]				INT					NOT	NULL	CONSTRAINT [CR_HydrogenPurification_H2Unit_Count_MinIncl_0]			CHECK([H2Unit_Count] >= 0),
	[Duration_Days]				FLOAT				NOT	NULL	CONSTRAINT [CR_HydrogenPurification_Duration_Days_MinIncl_0.0]		CHECK([Duration_Days] >= 0.0),

	[_H2Pur_MScfDur]			AS		 CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT])
								PERSISTED			NOT	NULL,
	[_H2Pur_MScfDay]			AS CASE WHEN ([Duration_Days] <> 0.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days]
									END
								PERSISTED					,
	[_H2Pur_MScfYear]			AS CASE WHEN ([Duration_Days] <> 0.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days] * 365.0
									END
								PERSISTED					,

	[_H2Pur_kScfDur]			AS		 CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT])							* 1000.0
								PERSISTED			NOT	NULL,
	[_H2Pur_kScfDay]			AS CASE WHEN ([Duration_Days] <> 0.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days]			* 1000.0
									END
								PERSISTED					,
	[_H2Pur_kScfYear]			AS CASE WHEN ([Duration_Days] <> 0.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days] * 365.0	* 1000.0
									END
								PERSISTED					,

	[_H2PurUtil_kScfDay]		AS CASE WHEN ([Duration_Days] <> 0.0 AND [Utilization_Pcnt] <> 0.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days]			* 1000.0 / [Utilization_Pcnt] * 100.0
									END
								PERSISTED					,
	[_H2PurUtil_kScfYear]		AS CASE WHEN ([Duration_Days] <> 0.0 AND [Utilization_Pcnt] <> 0.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days] * 365.0	* 1000.0 / [Utilization_Pcnt] * 100.0
									END
								PERSISTED					,

	[_H2PurUtilSales_kScfDay]	AS CASE WHEN ([Duration_Days] <> 0.0 AND [Utilization_Pcnt] <> 0.0 AND [H2Unit_Count] > 0 AND [Component_WtPcnt] > 40.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days]			* 1000.0 / [Utilization_Pcnt] * 100.0
									ELSE 0.0
									END
								PERSISTED			NOT	NULL,
	[_H2PurUtilSales_kScfYear]	AS CASE WHEN ([Duration_Days] <> 0.0 AND [Utilization_Pcnt] <> 0.0 AND [H2Unit_Count] > 0 AND [Component_WtPcnt] > 40.0)
									THEN CONVERT(FLOAT, (423.3 * 1.1 + 49.33 * 3.0 * 1.1) * [Component_kMT]) / [Duration_Days] * 365.0	* 1000.0 / [Utilization_Pcnt] * 100.0
									ELSE 0.0
									END
								PERSISTED			NOT	NULL,

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_HydrogenPurification_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydrogenPurification_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydrogenPurification_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HydrogenPurification_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_HydrogenPurification]			PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_HydrogenPurification_u]
ON [xls].[HydrogenPurification]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[HydrogenPurification]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[HydrogenPurification].[Refnum]	= INSERTED.[Refnum];

END;
GO