﻿CREATE TABLE [xls].[FacilitiesFractionator]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_FacilitiesFractionator_Refnum]						CHECK([Refnum] <> ''),

	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesFractionator_Facility_LookUp]				REFERENCES [dim].[Facility_LookUp]([FacilityId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FeedStock_BSD]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesFractionator_FeedStock_BSD_MinIncl_0.0]	CHECK([FeedStock_BSD] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesFractionator_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesFractionator]		PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_FacilitiesFractionator_u]
ON [xls].[FacilitiesFractionator]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesFractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesFractionator].[Refnum]	= INSERTED.[Refnum];

END;
GO