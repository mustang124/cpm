﻿CREATE TABLE [ante].[HvcProduction]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_HvcProduction_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_HvcProduction_Stream_LookUp]						REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_HvcProduction_Component_LookUp]					REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[LowerLimit_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_HvcProduction_LowerLimit_WtPcnt_MinIncl_0.0]		CHECK([LowerLimit_WtPcnt] >= 0.0),
															CONSTRAINT [CR_HvcProduction_LowerLimit_WtPcnt_MaxIncl_100.0]	CHECK([LowerLimit_WtPcnt] <= 100.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_HvcProduction_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HvcProduction_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HvcProduction_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_HvcProduction_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_HvcProduction]	PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [ante].[t_HvcProduction_u]
ON [ante].[HvcProduction]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[HvcProduction]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[HvcProduction].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[HvcProduction].[StreamId]		= INSERTED.[StreamId]
		AND	[ante].[HvcProduction].[ComponentId]		= INSERTED.[ComponentId];

END;
GO