﻿CREATE TABLE [ante].[Factors]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Factors_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StandardId]			INT					NOT	NULL	CONSTRAINT [FK_Factors_Standards_LookUp]				REFERENCES [dim].[Standard_LookUp] ([StandardId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_Factors_Factor_LookUp]					REFERENCES [dim].[Factor_LookUp] ([FactorId])							ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Coefficient]			FLOAT				NOT	NULL,
	[Exponent]				FLOAT				NOT	NULL,
	[ValueMin]				FLOAT					NULL,
	[ValueMax]				FLOAT					NULL,

	CONSTRAINT [CV_Factors_MinMax]							CHECK([ValueMin] <= [ValueMax]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Factors_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Factors]						PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_kEdc]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 1;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_PersMaint]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 4;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_PersNonMaint]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 5;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_Mes]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 6;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_NonEnergy]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 7;
GO

CREATE TRIGGER [ante].[t_Factors_u]
ON [ante].[Factors]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Factors]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Factors].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[Factors].[StandardId]			= INSERTED.[StandardId]
		AND	[ante].[Factors].[FactorId]				= INSERTED.[FactorId];

END;
GO