﻿CREATE VIEW [calc].[SupplementalRecovered_Composite]
WITH SCHEMABINDING
AS
SELECT
	c.[MethodologyId],
	c.[SubmissionId],
	c.[ComponentId],
	SUM(c.[Recovered_Dur_kMT])		[Recovered_Dur_kMT],
	SUM(c.[Recovered_Ann_kMT])		[Recovered_Ann_kMT],
	COUNT_BIG(*)					[Items]
FROM [calc].[SupplementalRecovered]		c
GROUP BY
	c.[MethodologyId],
	c.[SubmissionId],
	c.[ComponentId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_SupplementalRecovered_Composite]
ON [calc].[SupplementalRecovered_Composite]([MethodologyId] DESC, [SubmissionId] ASC, [ComponentId] ASC);
GO

CREATE INDEX [IX_SupplementalRecovered_Composite]
ON [calc].[SupplementalRecovered_Composite]([MethodologyId] DESC, [SubmissionId] ASC, [ComponentId] ASC)
INCLUDE([Recovered_Dur_kMT])
GO