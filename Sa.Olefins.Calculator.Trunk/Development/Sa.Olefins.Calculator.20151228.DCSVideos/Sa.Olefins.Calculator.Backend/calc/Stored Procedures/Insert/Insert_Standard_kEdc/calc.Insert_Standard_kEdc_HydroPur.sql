﻿CREATE PROCEDURE [calc].[Insert_Standard_kEdc_HydroPur]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId			INT	= dim.Return_StandardId('kEdc');
	DECLARE @ProcessUnitId		INT	= dim.Return_ProcessUnitId('HydroPur');
	DECLARE @FacilityId			INT	= dim.Return_FacilityId('HydroPur');

	SELECT
		@FacilityId = MIN(f.[FacilityId])
	FROM fact.Facilities				f
	INNER JOIN [dim].[Facility_Bridge]	b
		ON	b.[DescendantId] = f.[FacilityId]
		AND	b.[FacilityId] = @FacilityId
	WHERE	b.[MethodologyId] = @MethodologyId
		AND	f.[SubmissionId] = @SubmissionId

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		pur.[MethodologyId],
		pur.[SubmissionId],
		f.[StandardId],
		@ProcessUnitId,
		f.[Coefficient] * pur.[_H2PurUtilSales_kScfDay] / 1000.0
	FROM	[calc].[HydrogenPurification]				pur
	INNER JOIN [fact].[Facilities]						fac
		ON	fac.[SubmissionId]	= pur.[SubmissionId]
	INNER JOIN	[ante].[MapFactorHydroPur]				map
		ON	map.[MethodologyId]	= pur.[MethodologyId]
		AND	map.[FacilityId]	= fac.[FacilityId]
	INNER JOIN	[ante].[Factors]						f
		ON	f.[MethodologyId]	= pur.[MethodologyId]
		AND	f.[FactorId]		= map.[FactorId]
		AND	f.[StandardId]		= @StandardId
	WHERE	pur.[MethodologyId]	= @MethodologyId
		AND	pur.[SubmissionId]	= @SubmissionId
		AND	fac.[FacilityId]	= @FacilityId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO