﻿CREATE PROCEDURE [calc].[Insert_Utilization]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @cProdOlefins	INT = dim.Return_ComponentId('ProdOlefins');
	DECLARE @cC2H4			INT = dim.Return_ComponentId('C2H4');
	DECLARE @cC3H6			INT = dim.Return_ComponentId('C3H6');

	DECLARE @sProdHVC		INT = dim.Return_StreamId('ProdHVC');
	DECLARE @sProdOlefins	INT = dim.Return_StreamId('ProdOlefins');
	DECLARE @sEthylene		INT = dim.Return_StreamId('Ethylene');
	DECLARE @sPropylene		INT = dim.Return_StreamId('Propylene');

	INSERT INTO [calc].[Utilization]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Duration_Days], [StreamDay_MTSD], [Component_kMT])
	SELECT
		cmp.[MethodologyId],
		sub.[SubmissionId],
		sMap.[StreamId],
		cBridge.[ComponentId],
		sub.[_Duration_Days],
		SUM(DISTINCT cap.[StreamDay_MTSD]) 	[StreamDay_MTSD],
		SUM(cmp.[Component_Dur_kMT])		[Component_kMT]
	FROM [fact].[Submissions]				sub
	INNER JOIN [dim].[Component_Bridge]		cBridge
		ON	cBridge.[ComponentId] IN (@cProdOlefins, @cC2H4, @cC3H6)
	INNER JOIN [ante].[MapStreamComponent]	cMap
		ON	cMap.[ComponentId] = cBridge.[DescendantId]
		AND	cMap.[StreamId] IN (@sProdOlefins, @sEthylene, @sPropylene)
	INNER JOIN [ante].[MapStreamComponent]	sMap
		ON	sMap.[MethodologyId] = cMap.[MethodologyId]
		AND	sMap.[ComponentId] = cBridge.[ComponentId]
		AND	sMap.[StreamId]  IN (@sProdOlefins, @sEthylene, @sPropylene)
	LEFT OUTER JOIN [fact].[Capacity]				cap
		ON	cap.[SubmissionId] = sub.[SubmissionId]
		AND	cap.[StreamId] = cMap.[StreamId]
	INNER JOIN [calc].[StreamComposition]		cmp
		ON	cmp.[SubmissionId] = sub.[SubmissionId]
		AND	cmp.[ComponentId] = cMap.[ComponentId]
	INNER JOIN [ante].[UtilizationFilter]		uFilter
		ON	uFilter.[MethodologyId] = cmp.[MethodologyId]
		AND	uFilter.[StreamId] = cmp.[StreamId]
		AND	uFilter.[ComponentId] = cmp.[ComponentId]
	WHERE	cmp.[MethodologyId] = @MethodologyId
		AND	sub.[SubmissionId] = @SubmissionId
	GROUP BY
		cmp.[MethodologyId],
		sub.[SubmissionId],
		sub.[_Duration_Days],
		sMap.[StreamId],
		cBridge.[ComponentId]
	HAVING
		SUM(DISTINCT cap.[StreamDay_MTSD])IS NOT NULL;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO