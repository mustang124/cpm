﻿CREATE PROCEDURE [calc].[Insert_SupplementalRecovered_SuppWashOil_PyroGasOil]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@SuppWashOil	INT	= [dim].[Return_StreamId]('SuppWashOil');
	DECLARE	@PyroGasOil		INT	= [dim].[Return_ComponentId]('PyroGasOil');

	INSERT INTO [calc].[SupplementalRecovered]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Recovered_Dur_kMT], [Recovered_Ann_kMT])
	SELECT
		x.[MethodologyId],
		x.[SubmissionId],
		@SuppWashOil,
		@PyroGasOil,
		calc.MinValue(p.[ContainedProduct_Dur_kMT], q.[Quantity_kMT]),
		calc.MinValue(p.[ContainedProduct_Dur_kMT], q.[Quantity_kMT]) * s.[_Duration_Multiplier]
	FROM (VALUES
		(@MethodologyId, @SubmissionId)
		)												x ([MethodologyId], [SubmissionId])
	INNER JOIN [fact].[Submissions]						s
		ON	s.[SubmissionId]	= x.[SubmissionId]
	LEFT OUTER JOIN	[calc].[ContainedProduct_Composite]	p WITH (NOEXPAND)
		ON	p.[MethodologyId]	= x.[MethodologyId]
		AND	p.[SubmissionId]	= x.[SubmissionId]
		AND	p.[ComponentId]		= @PyroGasOil
	LEFT OUTER JOIN [fact].[StreamQuantity]				q
		ON	q.[SubmissionId]	= x.[SubmissionId]
		AND	q.[StreamId]		= @SuppWashOil
	WHERE	calc.MinValue(p.[ContainedProduct_Dur_kMT], q.[Quantity_kMT]) > 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO