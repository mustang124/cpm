﻿CREATE FUNCTION [calc].[Get_Utilization]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[Ethylene],
		c.[ProdOlefins]
	FROM (
		SELECT
			u.[MethodologyId],
			u.[SubmissionId],
			l.[StreamTag],
			u.[_Utilization_Pcnt]
		FROM [calc].[Utilization]				u
		INNER JOIN [dim].[Stream_LookUp]		l
			ON	l.[StreamId] = u.[StreamId]
		WHERE	l.[StreamTag] IN ('Ethylene', 'ProdOlefins')
			AND	u.[MethodologyId]	= @MethodologyId
			AND	u.[SubmissionId]	= @SubmissionId
		) u
		PIVOT(MAX(u.[_Utilization_Pcnt])
			FOR u.[StreamTag] IN (
				[Ethylene],
				[ProdOlefins]
				)
			) c
);