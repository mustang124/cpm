﻿CREATE TABLE [calc].[StandardEnergy_Reduction]
(
	[MethodologyId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_Reduction_Methodology]						REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_Reduction_Submissions]						REFERENCES [fact].[Submissions] ([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ProcessUnitId]				INT					NOT NULL	CONSTRAINT [FK_StandardEnergy_Reduction_ProcessUnit]						REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[StreamId]					INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_Reduction_Stream_LookUp]						REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Quantity_kMT]				FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Reduction_Quantity_kMT_MinIncl_0.0]			CHECK([Quantity_kMT] >= 0.0),
	[Quantity_bbl]				FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Reduction_Quantity_bbl_MinIncl_0.0]			CHECK([Quantity_bbl] >= 0.0),
	[StandardEnergy_MBtu]		FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Reduction_StandardEnergy_MBtu_MinIncl_0.0]	CHECK([StandardEnergy_MBtu] >= 0.0),
	[StandardEnergy_MBtuDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Reduction_StandardEnergy_MBtuDay_MinIncl_0.0]	CHECK([StandardEnergy_MBtuDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_Reduction_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Reduction_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Reduction_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Reduction_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_Reduction]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [calc].[t_StandardEnergy_Reduction_u]
ON [calc].[StandardEnergy_Reduction]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_Reduction]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_Reduction].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_Reduction].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[StandardEnergy_Reduction].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;
GO