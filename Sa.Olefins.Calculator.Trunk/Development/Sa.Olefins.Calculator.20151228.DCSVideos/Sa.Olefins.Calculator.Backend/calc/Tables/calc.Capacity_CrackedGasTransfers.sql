﻿CREATE TABLE [calc].[Capacity_CrackedGasTransfers]
(
	[MethodologyId]				INT					NOT	NULL	CONSTRAINT [FK_Capacity_CrackedGasTransfers_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]				INT					NOT	NULL	CONSTRAINT [FK_Capacity_CrackedGasTransfers_Submissions]					REFERENCES [fact].[Submissions] ([SubmissionId])					ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Capacity_Ann_kMT]			FLOAT				NOT	NULL	CONSTRAINT [CR_Capacity_CrackedGasTransfers_Capacity_Ann_kMT_MinIncl_0.0]	CHECK([Capacity_Ann_kMT] >= 0.0),
	[Utilization_Pcnt]			FLOAT				NOT	NULL	CONSTRAINT [CR_Capacity_CrackedGasTransfers_Utilization_Pcnt_MinIncl_0.0]	CHECK([Utilization_Pcnt] >= 0.0),
																--CONSTRAINT [CR_Capacity_CrackedGasTransfers_Utilization_Pcnt_MaxIncl_100.0]	CHECK([Utilization_Pcnt] <= 100.0),

	[_CapacityInfer_Ann_kMT]	AS CONVERT(FLOAT, [Capacity_Ann_kMT] / [Utilization_Pcnt] * 100.0)
								PERSISTED			NOT	NULL,	

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Capacity_CrackedGasTransfers]	PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC)
);
GO

CREATE TRIGGER [calc].[t_Capacity_CrackedGasTransfers_u]
ON [calc].[Capacity_CrackedGasTransfers]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[Capacity_CrackedGasTransfers]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[Capacity_CrackedGasTransfers].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[Capacity_CrackedGasTransfers].[SubmissionId]	= INSERTED.[SubmissionId];

END;
GO