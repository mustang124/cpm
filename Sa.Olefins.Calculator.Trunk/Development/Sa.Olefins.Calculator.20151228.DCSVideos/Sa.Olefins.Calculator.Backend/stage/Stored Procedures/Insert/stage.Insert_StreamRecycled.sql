﻿CREATE PROCEDURE [stage].[Insert_StreamRecycled]
(
	@SubmissionId			INT,
	@ComponentId			INT,

	@Recycled_WtPcnt		FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[StreamRecycled]([SubmissionId], [ComponentId], [Recycled_WtPcnt])
	SELECT
		@SubmissionId,
		@ComponentId,
		@Recycled_WtPcnt
	WHERE	@Recycled_WtPcnt >= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @ComponentId:'		+ CONVERT(VARCHAR, @ComponentId))
			+ COALESCE(', @Recycled_WtPcnt:'	+ CONVERT(VARCHAR, @Recycled_WtPcnt),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;