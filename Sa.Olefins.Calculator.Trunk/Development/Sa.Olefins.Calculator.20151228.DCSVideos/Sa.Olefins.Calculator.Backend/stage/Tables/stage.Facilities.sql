﻿CREATE TABLE [stage].[Facilities]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Submissions]				REFERENCES [stage].[Submissions]([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_Facility_Facility_LookUp]			REFERENCES [dim].[Facility_LookUp]([FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Unit_Count]			INT					NOT	NULL	CONSTRAINT [CR_Facility_Unit_Count_MinIncl_0]		CHECK([Unit_Count] >= 0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Facility_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facility_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Facilities]					PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [stage].[t_Facility_u]
ON [stage].[Facilities]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[Facilities]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[Facilities].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[stage].[Facilities].[FacilityId]		= INSERTED.[FacilityId];

END;
GO

