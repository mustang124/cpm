﻿CREATE TABLE [stage].[Submissions]
(
	[SubmissionId]			INT					NOT	NULL	IDENTITY (1, 1),

	[SubmissionName]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [CL_Sumbissions_SumbissionName]		CHECK([SubmissionName] <> ''),

	[DateBeg]				DATE				NOT	NULL,
	[DateEnd]				DATE				NOT	NULL,

	[_Duration_Days]		AS CONVERT(FLOAT, (DATEDIFF(DAY, [DateBeg], [DateEnd]) + 1.0))
							PERSISTED			NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Submissions_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Submissions]					PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [stage].[t_Submissions_u]
ON [stage].[Submissions]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[Submissions]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[Submissions].[SubmissionId]		= INSERTED.[SubmissionId]

END;
GO

