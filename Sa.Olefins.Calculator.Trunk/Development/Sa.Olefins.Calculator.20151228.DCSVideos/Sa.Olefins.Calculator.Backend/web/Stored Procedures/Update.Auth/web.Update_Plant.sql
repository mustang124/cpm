﻿CREATE PROCEDURE [web].[Update_Plant]
(
	@PlantId		INT				= NULL,

	@CompanyId		INT				= NULL,
	@PlantName		VARCHAR(128)	= NULL,
	@Active			BIT				= NULL,

	@NewCompanyId	INT				= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (@PlantId IS NULL)
	SET @PlantId = [auth].[Return_PlantId](@PlantName);

	IF (@PlantId IS NOT NULL)
	BEGIN
		EXECUTE @PlantId = [auth].[Update_Plant] @PlantId, @PlantName, @Active;

		DECLARE @JoinId		INT = NULL;
		SELECT	@JoinId		= jcp.[JoinId] FROM [auth].[JoinCompanyPlant] jcp WHERE jcp.[PlantId] = @PlantId; -- AND jcp.CompanyId = @CompanyId;

		IF (@JoinId IS NOT NULL)
		BEGIN
			SET @NewCompanyId = @CompanyId;
			EXECUTE [auth].[Update_JoinCompanyPlant] @JoinId, @NewCompanyId, @PlantId, @Active;
		END
		
	END
	ELSE
	BEGIN
		EXECUTE @PlantId = [auth].[Insert_Plant] @PlantName;
		EXECUTE [auth].[Insert_JoinCompanyPlant] @CompanyId, @PlantId;
		EXECUTE [auth].[Insert_PlantCalculationLimits] @PlantId, DEFAULT, DEFAULT; 
	END;

	SELECT @PlantId;
	RETURN @PlantId;

END;
GO