﻿CREATE TABLE [dim].[Stream_LookUp]
(
	[StreamId]				INT					NOT NULL	IDENTITY (1, 1),

	[StreamTag]				VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Stream_LookUp_StreamTag]			CHECK ([StreamTag]		<> ''),
															CONSTRAINT [UK_Stream_LookUp_StreamTag]			UNIQUE NONCLUSTERED ([StreamTag]),
	[StreamName]			VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Stream_LookUp_StreamName]		CHECK ([StreamName]		<> ''),
															CONSTRAINT [UK_Stream_LookUp_StreamName]		UNIQUE NONCLUSTERED ([StreamName]),
	[StreamDetail]			VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Stream_LookUp_StreamDetail]		CHECK ([StreamDetail]	<> ''),
															CONSTRAINT [UK_Stream_LookUp_StreamDetail]		UNIQUE NONCLUSTERED ([StreamDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Stream_LookUp_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_LookUp_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_LookUp_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_LookUp_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Stream_LookUp]				PRIMARY KEY CLUSTERED ([StreamId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Stream_LookUp_u]
ON [dim].[Stream_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Stream_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Stream_LookUp].[StreamId]	= INSERTED.[StreamId];

END;
GO