﻿CREATE TABLE [dim].[ProcessUnit_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_ProcessUnit_Bridge_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])								ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ProcessUnitId]			INT					NOT	NULL	CONSTRAINT [FK_ProcessUnit_Bridge_StandardId]			REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_ProcessUnit_Bridge_Parent_Ancestor]		FOREIGN KEY ([MethodologyId], [ProcessUnitId])
																													REFERENCES [dim].[ProcessUnit_Parent] ([MethodologyId], [ProcessUnitId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_ProcessUnit_Bridge_DescendantID]			REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId])							ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_ProcessUnit_Bridge_Parent_Descendant]	FOREIGN KEY ([MethodologyId], [DescendantId])
																													REFERENCES [dim].[ProcessUnit_Parent] ([MethodologyId], [ProcessUnitId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_ProcessUnit_Bridge_DescendantOperator]	DEFAULT ('+')
															CONSTRAINT [FK_ProcessUnit_Bridge_DescendantOperator]	REFERENCES [dim].[Operator] ([Operator])										ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ProcessUnit_Bridge_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_Bridge_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_Bridge_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_Bridge_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ProcessUnit_Bridge]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ProcessUnitId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_ProcessUnit_Bridge_u]
ON [dim].[ProcessUnit_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnit_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[ProcessUnit_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[ProcessUnit_Bridge].[ProcessUnitId]		= INSERTED.[ProcessUnitId]
		AND	[dim].[ProcessUnit_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;
GO