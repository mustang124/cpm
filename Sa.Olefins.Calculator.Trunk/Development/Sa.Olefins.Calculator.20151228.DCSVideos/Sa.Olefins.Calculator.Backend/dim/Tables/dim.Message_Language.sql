﻿CREATE TABLE [dim].[Message_Language]
(
	[LanguageId]			SMALLINT			NOT	NULL	CONSTRAINT [DF_Message_Language_LanguageId]			DEFAULT (1033)
															CONSTRAINT [FK_Message_Language_NLS_LookUp]			REFERENCES [dim].[NationalLanguageSupport_LookUp]([Lcid])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[MessageId]				INT					NOT NULL	CONSTRAINT [FK_Message_Language_Message_LookUp]		REFERENCES [dim].[Message_LookUp]([MessageId])				ON DELETE NO ACTION ON UPDATE NO ACTION,

	[DisplayName]			NVARCHAR(256)			NULL	CONSTRAINT [CL_Message_Language_MessageName]		CHECK ([DisplayName]		<> ''),
	[DisplayDetail]			NVARCHAR(256)			NULL	CONSTRAINT [CL_Message_Language_MessageDetail]		CHECK ([DisplayDetail]		<> ''),
	[DisplaySection]		NVARCHAR(256)			NULL	CONSTRAINT [CL_Message_Language_MessageSection]		CHECK ([DisplaySection]		<> ''),
	[DisplayLocation]		NVARCHAR(256)			NULL	CONSTRAINT [CL_Message_Language_MessageLocation]	CHECK ([DisplayLocation]	<> ''),
	[DisplayError]			NVARCHAR(256)			NULL	CONSTRAINT [CL_Message_Language_MessageError]		CHECK ([DisplayError]		<> ''),
	[DisplayCorrection]		NVARCHAR(256)			NULL	CONSTRAINT [CL_Message_Language_MessageCorrection]	CHECK ([DisplayCorrection]	<> ''),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Message_Language_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_Language_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_Language_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_Language_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [CV_Message]	CHECK(
								[DisplayName]		IS NOT NULL
							OR	[DisplayDetail]		IS NOT NULL
							OR	[DisplaySection]	IS NOT NULL
							OR	[DisplayLocation]	IS NOT NULL
							OR	[DisplayError]		IS NOT NULL
							OR	[DisplayCorrection]	IS NOT NULL),

	CONSTRAINT [PK_Message_Language]			PRIMARY KEY CLUSTERED ([LanguageId] ASC, [MessageId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Message_Language_LangId=1033]
ON [dim].[Message_Language]([LanguageId] ASC, [MessageId] ASC)
INCLUDE ([DisplayName], [DisplayDetail], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection])
WHERE [LanguageId] = 1033;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayName]
ON [dim].[Message_Language]([DisplayName] ASC)
WHERE [DisplayName] IS NOT NULL;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayDetail]
ON [dim].[Message_Language]([DisplayDetail] ASC)
WHERE [DisplayDetail] IS NOT NULL;
GO

CREATE NONCLUSTERED INDEX [UX_Message_Language_DisplaySection]
ON [dim].[Message_Language]([DisplaySection] ASC)
WHERE [DisplaySection] IS NOT NULL;
GO

CREATE NONCLUSTERED INDEX [UX_Message_Language_DisplayLocation]
ON [dim].[Message_Language]([DisplayLocation] ASC)
WHERE [DisplayLocation] IS NOT NULL;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayError]
ON [dim].[Message_Language]([DisplayError] ASC)
WHERE [DisplayError] IS NOT NULL;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayCorrection]
ON [dim].[Message_Language]([DisplayCorrection] ASC)
WHERE [DisplayCorrection] IS NOT NULL;
GO

CREATE TRIGGER [dim].[t_Message_Language_u]
ON [dim].[Message_Language]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Message_Language]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Message_Language].[MessageId]		= INSERTED.[MessageId]
		AND	[dim].[Message_Language].[LanguageId]		= INSERTED.[LanguageId];

END;
GO