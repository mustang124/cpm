﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Data
{
    [TestClass()]
    public class clStandards : SqlDatabaseTestClass
    {

        public clStandards()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Standards()
        {
            SqlDatabaseTestActions testActions = this.StandardsData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_Aggregate()
        {
            SqlDatabaseTestActions testActions = this.Standards_AggregateData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Standards_DataSet()
        {
            SqlDatabaseTestActions testActions = this.Standards_DataSetData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }



        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_TestAction;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(clStandards));
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_EmptySet;
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_Aggregate_TestAction;
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Standards_Aggregate_EmptySet;
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Standards_DataSet_TestAction;
			Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Standards_DataSet_NotEmpty;
			this.StandardsData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
			this.Standards_AggregateData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
			this.Standards_DataSetData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
			Standards_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
			Standards_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
			Standards_Aggregate_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
			Standards_Aggregate_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
			Standards_DataSet_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
			Standards_DataSet_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
			// 
			// Standards_TestAction
			// 
			Standards_TestAction.Conditions.Add(Standards_EmptySet);
			resources.ApplyResources(Standards_TestAction, "Standards_TestAction");
			// 
			// Standards_EmptySet
			// 
			Standards_EmptySet.Enabled = true;
			Standards_EmptySet.Name = "Standards_EmptySet";
			Standards_EmptySet.ResultSet = 1;
			// 
			// Standards_Aggregate_TestAction
			// 
			Standards_Aggregate_TestAction.Conditions.Add(Standards_Aggregate_EmptySet);
			resources.ApplyResources(Standards_Aggregate_TestAction, "Standards_Aggregate_TestAction");
			// 
			// Standards_Aggregate_EmptySet
			// 
			Standards_Aggregate_EmptySet.Enabled = true;
			Standards_Aggregate_EmptySet.Name = "Standards_Aggregate_EmptySet";
			Standards_Aggregate_EmptySet.ResultSet = 1;
			// 
			// Standards_DataSet_TestAction
			// 
			Standards_DataSet_TestAction.Conditions.Add(Standards_DataSet_NotEmpty);
			resources.ApplyResources(Standards_DataSet_TestAction, "Standards_DataSet_TestAction");
			// 
			// Standards_DataSet_NotEmpty
			// 
			Standards_DataSet_NotEmpty.Enabled = true;
			Standards_DataSet_NotEmpty.Name = "Standards_DataSet_NotEmpty";
			Standards_DataSet_NotEmpty.ResultSet = 1;
			// 
			// StandardsData
			// 
			this.StandardsData.PosttestAction = null;
			this.StandardsData.PretestAction = null;
			this.StandardsData.TestAction = Standards_TestAction;
			// 
			// Standards_AggregateData
			// 
			this.Standards_AggregateData.PosttestAction = null;
			this.Standards_AggregateData.PretestAction = null;
			this.Standards_AggregateData.TestAction = Standards_Aggregate_TestAction;
			// 
			// Standards_DataSetData
			// 
			this.Standards_DataSetData.PosttestAction = null;
			this.Standards_DataSetData.PretestAction = null;
			this.Standards_DataSetData.TestAction = Standards_DataSet_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardsData;
        private SqlDatabaseTestActions Standards_AggregateData;
        private SqlDatabaseTestActions Standards_DataSetData;
    }
}
