﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Data
{
    [TestClass()]
    public class cCapacity : SqlDatabaseTestClass
    {

        public cCapacity()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Capacity_FacilitiesFractionator()
        {
            SqlDatabaseTestActions testActions = this.Capacity_FacilitiesFractionatorData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_FacilitiesHydroTreater()
        {
            SqlDatabaseTestActions testActions = this.Capacity_FacilitiesHydroTreaterData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_HydrogenPurification()
        {
            SqlDatabaseTestActions testActions = this.Capacity_HydrogenPurificationData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_FacilitiesBoilers()
        {
            SqlDatabaseTestActions testActions = this.Capacity_FacilitiesBoilersData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_FacilitiesElecGeneration()
        {
            SqlDatabaseTestActions testActions = this.Capacity_FacilitiesElecGenerationData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }





        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_FacilitiesHydroTreater_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cCapacity));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_FacilitiesHydroTreater_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_HydrogenPurification_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_HydrogenPurification_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_FacilitiesBoilers_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_FacilitiesBoilers_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_FacilitiesElecGeneration_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_FacilitiesElecGeneration_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_FacilitiesFractionator_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_FacilitiesFractionator_EmptySet;
            this.Capacity_FacilitiesFractionatorData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_FacilitiesHydroTreaterData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_HydrogenPurificationData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_FacilitiesBoilersData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_FacilitiesElecGenerationData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Capacity_FacilitiesHydroTreater_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_FacilitiesHydroTreater_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_HydrogenPurification_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_HydrogenPurification_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_FacilitiesBoilers_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_FacilitiesBoilers_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_FacilitiesElecGeneration_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_FacilitiesElecGeneration_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_FacilitiesFractionator_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_FacilitiesFractionator_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // Capacity_FacilitiesHydroTreater_TestAction
            // 
            Capacity_FacilitiesHydroTreater_TestAction.Conditions.Add(Capacity_FacilitiesHydroTreater_EmptySet);
            resources.ApplyResources(Capacity_FacilitiesHydroTreater_TestAction, "Capacity_FacilitiesHydroTreater_TestAction");
            // 
            // Capacity_FacilitiesHydroTreater_EmptySet
            // 
            Capacity_FacilitiesHydroTreater_EmptySet.Enabled = true;
            Capacity_FacilitiesHydroTreater_EmptySet.Name = "Capacity_FacilitiesHydroTreater_EmptySet";
            Capacity_FacilitiesHydroTreater_EmptySet.ResultSet = 1;
            // 
            // Capacity_HydrogenPurification_TestAction
            // 
            Capacity_HydrogenPurification_TestAction.Conditions.Add(Capacity_HydrogenPurification_EmptySet);
            resources.ApplyResources(Capacity_HydrogenPurification_TestAction, "Capacity_HydrogenPurification_TestAction");
            // 
            // Capacity_HydrogenPurification_EmptySet
            // 
            Capacity_HydrogenPurification_EmptySet.Enabled = true;
            Capacity_HydrogenPurification_EmptySet.Name = "Capacity_HydrogenPurification_EmptySet";
            Capacity_HydrogenPurification_EmptySet.ResultSet = 1;
            // 
            // Capacity_FacilitiesBoilers_TestAction
            // 
            Capacity_FacilitiesBoilers_TestAction.Conditions.Add(Capacity_FacilitiesBoilers_EmptySet);
            resources.ApplyResources(Capacity_FacilitiesBoilers_TestAction, "Capacity_FacilitiesBoilers_TestAction");
            // 
            // Capacity_FacilitiesBoilers_EmptySet
            // 
            Capacity_FacilitiesBoilers_EmptySet.Enabled = true;
            Capacity_FacilitiesBoilers_EmptySet.Name = "Capacity_FacilitiesBoilers_EmptySet";
            Capacity_FacilitiesBoilers_EmptySet.ResultSet = 1;
            // 
            // Capacity_FacilitiesElecGeneration_TestAction
            // 
            Capacity_FacilitiesElecGeneration_TestAction.Conditions.Add(Capacity_FacilitiesElecGeneration_EmptySet);
            resources.ApplyResources(Capacity_FacilitiesElecGeneration_TestAction, "Capacity_FacilitiesElecGeneration_TestAction");
            // 
            // Capacity_FacilitiesElecGeneration_EmptySet
            // 
            Capacity_FacilitiesElecGeneration_EmptySet.Enabled = true;
            Capacity_FacilitiesElecGeneration_EmptySet.Name = "Capacity_FacilitiesElecGeneration_EmptySet";
            Capacity_FacilitiesElecGeneration_EmptySet.ResultSet = 1;
            // 
            // Capacity_FacilitiesFractionator_TestAction
            // 
            Capacity_FacilitiesFractionator_TestAction.Conditions.Add(Capacity_FacilitiesFractionator_EmptySet);
            resources.ApplyResources(Capacity_FacilitiesFractionator_TestAction, "Capacity_FacilitiesFractionator_TestAction");
            // 
            // Capacity_FacilitiesFractionator_EmptySet
            // 
            Capacity_FacilitiesFractionator_EmptySet.Enabled = true;
            Capacity_FacilitiesFractionator_EmptySet.Name = "Capacity_FacilitiesFractionator_EmptySet";
            Capacity_FacilitiesFractionator_EmptySet.ResultSet = 1;
            // 
            // Capacity_FacilitiesFractionatorData
            // 
            this.Capacity_FacilitiesFractionatorData.PosttestAction = null;
            this.Capacity_FacilitiesFractionatorData.PretestAction = null;
            this.Capacity_FacilitiesFractionatorData.TestAction = Capacity_FacilitiesFractionator_TestAction;
            // 
            // Capacity_FacilitiesHydroTreaterData
            // 
            this.Capacity_FacilitiesHydroTreaterData.PosttestAction = null;
            this.Capacity_FacilitiesHydroTreaterData.PretestAction = null;
            this.Capacity_FacilitiesHydroTreaterData.TestAction = Capacity_FacilitiesHydroTreater_TestAction;
            // 
            // Capacity_HydrogenPurificationData
            // 
            this.Capacity_HydrogenPurificationData.PosttestAction = null;
            this.Capacity_HydrogenPurificationData.PretestAction = null;
            this.Capacity_HydrogenPurificationData.TestAction = Capacity_HydrogenPurification_TestAction;
            // 
            // Capacity_FacilitiesBoilersData
            // 
            this.Capacity_FacilitiesBoilersData.PosttestAction = null;
            this.Capacity_FacilitiesBoilersData.PretestAction = null;
            this.Capacity_FacilitiesBoilersData.TestAction = Capacity_FacilitiesBoilers_TestAction;
            // 
            // Capacity_FacilitiesElecGenerationData
            // 
            this.Capacity_FacilitiesElecGenerationData.PosttestAction = null;
            this.Capacity_FacilitiesElecGenerationData.PretestAction = null;
            this.Capacity_FacilitiesElecGenerationData.TestAction = Capacity_FacilitiesElecGeneration_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions Capacity_FacilitiesFractionatorData;
        private SqlDatabaseTestActions Capacity_FacilitiesHydroTreaterData;
        private SqlDatabaseTestActions Capacity_HydrogenPurificationData;
        private SqlDatabaseTestActions Capacity_FacilitiesBoilersData;
        private SqlDatabaseTestActions Capacity_FacilitiesElecGenerationData;
    }
}
