﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.Functions
{
    [TestClass()]
    public class calc_Functions : SqlDatabaseTestClass
    {

        public calc_Functions()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void MaxValue()
        {
            SqlDatabaseTestActions testActions = this.MaxValueData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void MinValue()
        {
            SqlDatabaseTestActions testActions = this.MinValueData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void GemoetricContains()
        {
            SqlDatabaseTestActions testActions = this.GemoetricContainsData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }



        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction MaxValue_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(calc_Functions));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition MaxValue_Scalar1;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition MaxValue_Scalar;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction MinValue_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition MinValue_Scalar1;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition MinValue_Scalar0;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction GemoetricContains_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition GemoetricContains_Scalar;
            this.MaxValueData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.MinValueData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.GemoetricContainsData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            MaxValue_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            MaxValue_Scalar1 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            MaxValue_Scalar = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            MinValue_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            MinValue_Scalar1 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            MinValue_Scalar0 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            GemoetricContains_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            GemoetricContains_Scalar = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // MaxValue_TestAction
            // 
            MaxValue_TestAction.Conditions.Add(MaxValue_Scalar1);
            MaxValue_TestAction.Conditions.Add(MaxValue_Scalar);
            resources.ApplyResources(MaxValue_TestAction, "MaxValue_TestAction");
            // 
            // MaxValue_Scalar1
            // 
            MaxValue_Scalar1.ColumnNumber = 1;
            MaxValue_Scalar1.Enabled = true;
            MaxValue_Scalar1.ExpectedValue = "1";
            MaxValue_Scalar1.Name = "MaxValue_Scalar1";
            MaxValue_Scalar1.NullExpected = false;
            MaxValue_Scalar1.ResultSet = 1;
            MaxValue_Scalar1.RowNumber = 1;
            // 
            // MaxValue_Scalar
            // 
            MaxValue_Scalar.ColumnNumber = 1;
            MaxValue_Scalar.Enabled = true;
            MaxValue_Scalar.ExpectedValue = "0";
            MaxValue_Scalar.Name = "MaxValue_Scalar";
            MaxValue_Scalar.NullExpected = false;
            MaxValue_Scalar.ResultSet = 2;
            MaxValue_Scalar.RowNumber = 1;
            // 
            // MinValue_TestAction
            // 
            MinValue_TestAction.Conditions.Add(MinValue_Scalar1);
            MinValue_TestAction.Conditions.Add(MinValue_Scalar0);
            resources.ApplyResources(MinValue_TestAction, "MinValue_TestAction");
            // 
            // MinValue_Scalar1
            // 
            MinValue_Scalar1.ColumnNumber = 1;
            MinValue_Scalar1.Enabled = true;
            MinValue_Scalar1.ExpectedValue = "1";
            MinValue_Scalar1.Name = "MinValue_Scalar1";
            MinValue_Scalar1.NullExpected = false;
            MinValue_Scalar1.ResultSet = 1;
            MinValue_Scalar1.RowNumber = 1;
            // 
            // MinValue_Scalar0
            // 
            MinValue_Scalar0.ColumnNumber = 1;
            MinValue_Scalar0.Enabled = true;
            MinValue_Scalar0.ExpectedValue = "0";
            MinValue_Scalar0.Name = "MinValue_Scalar0";
            MinValue_Scalar0.NullExpected = false;
            MinValue_Scalar0.ResultSet = 2;
            MinValue_Scalar0.RowNumber = 1;
            // 
            // GemoetricContains_TestAction
            // 
            GemoetricContains_TestAction.Conditions.Add(GemoetricContains_Scalar);
            resources.ApplyResources(GemoetricContains_TestAction, "GemoetricContains_TestAction");
            // 
            // GemoetricContains_Scalar
            // 
            GemoetricContains_Scalar.ColumnNumber = 1;
            GemoetricContains_Scalar.Enabled = true;
            GemoetricContains_Scalar.ExpectedValue = "True";
            GemoetricContains_Scalar.Name = "GemoetricContains_Scalar";
            GemoetricContains_Scalar.NullExpected = false;
            GemoetricContains_Scalar.ResultSet = 1;
            GemoetricContains_Scalar.RowNumber = 1;
            // 
            // MaxValueData
            // 
            this.MaxValueData.PosttestAction = null;
            this.MaxValueData.PretestAction = null;
            this.MaxValueData.TestAction = MaxValue_TestAction;
            // 
            // MinValueData
            // 
            this.MinValueData.PosttestAction = null;
            this.MinValueData.PretestAction = null;
            this.MinValueData.TestAction = MinValue_TestAction;
            // 
            // GemoetricContainsData
            // 
            this.GemoetricContainsData.PosttestAction = null;
            this.GemoetricContainsData.PretestAction = null;
            this.GemoetricContainsData.TestAction = GemoetricContains_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions MaxValueData;
        private SqlDatabaseTestActions MinValueData;
        private SqlDatabaseTestActions GemoetricContainsData;
    }
}
