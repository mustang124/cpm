﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _050_FeedStock_Preperation : SqlDatabaseTestClass
    {

        public _050_FeedStock_Preperation()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void FeedStockPrep()
        {
            SqlDatabaseTestActions testActions = this.FeedStockPrepData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FeedStockPrep_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_050_FeedStock_Preperation));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition FeedStockPrep_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FeedStockPrep_EmptySet;
            this.FeedStockPrepData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            FeedStockPrep_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FeedStockPrep_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            FeedStockPrep_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // FeedStockPrep_TestAction
            // 
            FeedStockPrep_TestAction.Conditions.Add(FeedStockPrep_NotEmpty);
            FeedStockPrep_TestAction.Conditions.Add(FeedStockPrep_EmptySet);
            resources.ApplyResources(FeedStockPrep_TestAction, "FeedStockPrep_TestAction");
            // 
            // FeedStockPrep_NotEmpty
            // 
            FeedStockPrep_NotEmpty.Enabled = true;
            FeedStockPrep_NotEmpty.Name = "FeedStockPrep_NotEmpty";
            FeedStockPrep_NotEmpty.ResultSet = 1;
            // 
            // FeedStockPrep_EmptySet
            // 
            FeedStockPrep_EmptySet.Enabled = true;
            FeedStockPrep_EmptySet.Name = "FeedStockPrep_EmptySet";
            FeedStockPrep_EmptySet.ResultSet = 2;
            // 
            // FeedStockPrepData
            // 
            this.FeedStockPrepData.PosttestAction = null;
            this.FeedStockPrepData.PretestAction = null;
            this.FeedStockPrepData.TestAction = FeedStockPrep_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions FeedStockPrepData;
    }
}
