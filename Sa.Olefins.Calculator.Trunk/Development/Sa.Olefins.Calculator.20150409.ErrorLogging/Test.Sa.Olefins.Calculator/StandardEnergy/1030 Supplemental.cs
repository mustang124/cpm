﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.Calc
{
    [TestClass()]
    public class _1030_Supplemental : SqlDatabaseTestClass
    {

        public _1030_Supplemental()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void StandardEnergy_Supplemental_MBtuDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Supplemental_MBtuDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Supplemental_MBtu()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Supplemental_MBtuData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_Supplemental_Quantity()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_Supplemental_QuantityData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }




        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Supplemental_MBtuDay_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1030_Supplemental));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Supplemental_MBtu_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_Supplemental_Quantity_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Supplemental_Quantity_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Supplemental_Quantity_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Supplemental_MBtuDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Supplemental_MBtuDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_Supplemental_MBtu_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_Supplemental_MBtu_EmptySet;
            this.StandardEnergy_Supplemental_MBtuDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Supplemental_MBtuData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_Supplemental_QuantityData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            StandardEnergy_Supplemental_MBtuDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Supplemental_MBtu_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Supplemental_Quantity_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_Supplemental_Quantity_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Supplemental_Quantity_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Supplemental_MBtuDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Supplemental_MBtuDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_Supplemental_MBtu_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_Supplemental_MBtu_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // StandardEnergy_Supplemental_MBtuDay_TestAction
            // 
            StandardEnergy_Supplemental_MBtuDay_TestAction.Conditions.Add(StandardEnergy_Supplemental_MBtuDay_NotEmpty);
            StandardEnergy_Supplemental_MBtuDay_TestAction.Conditions.Add(StandardEnergy_Supplemental_MBtuDay_EmptySet);
            resources.ApplyResources(StandardEnergy_Supplemental_MBtuDay_TestAction, "StandardEnergy_Supplemental_MBtuDay_TestAction");
            // 
            // StandardEnergy_Supplemental_MBtuDayData
            // 
            this.StandardEnergy_Supplemental_MBtuDayData.PosttestAction = null;
            this.StandardEnergy_Supplemental_MBtuDayData.PretestAction = null;
            this.StandardEnergy_Supplemental_MBtuDayData.TestAction = StandardEnergy_Supplemental_MBtuDay_TestAction;
            // 
            // StandardEnergy_Supplemental_MBtuData
            // 
            this.StandardEnergy_Supplemental_MBtuData.PosttestAction = null;
            this.StandardEnergy_Supplemental_MBtuData.PretestAction = null;
            this.StandardEnergy_Supplemental_MBtuData.TestAction = StandardEnergy_Supplemental_MBtu_TestAction;
            // 
            // StandardEnergy_Supplemental_MBtu_TestAction
            // 
            StandardEnergy_Supplemental_MBtu_TestAction.Conditions.Add(StandardEnergy_Supplemental_MBtu_NotEmpty);
            StandardEnergy_Supplemental_MBtu_TestAction.Conditions.Add(StandardEnergy_Supplemental_MBtu_EmptySet);
            resources.ApplyResources(StandardEnergy_Supplemental_MBtu_TestAction, "StandardEnergy_Supplemental_MBtu_TestAction");
            // 
            // StandardEnergy_Supplemental_QuantityData
            // 
            this.StandardEnergy_Supplemental_QuantityData.PosttestAction = null;
            this.StandardEnergy_Supplemental_QuantityData.PretestAction = null;
            this.StandardEnergy_Supplemental_QuantityData.TestAction = StandardEnergy_Supplemental_Quantity_TestAction;
            // 
            // StandardEnergy_Supplemental_Quantity_TestAction
            // 
            StandardEnergy_Supplemental_Quantity_TestAction.Conditions.Add(StandardEnergy_Supplemental_Quantity_NotEmpty);
            StandardEnergy_Supplemental_Quantity_TestAction.Conditions.Add(StandardEnergy_Supplemental_Quantity_EmptySet);
            resources.ApplyResources(StandardEnergy_Supplemental_Quantity_TestAction, "StandardEnergy_Supplemental_Quantity_TestAction");
            // 
            // StandardEnergy_Supplemental_Quantity_NotEmpty
            // 
            StandardEnergy_Supplemental_Quantity_NotEmpty.Enabled = true;
            StandardEnergy_Supplemental_Quantity_NotEmpty.Name = "StandardEnergy_Supplemental_Quantity_NotEmpty";
            StandardEnergy_Supplemental_Quantity_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Supplemental_Quantity_EmptySet
            // 
            StandardEnergy_Supplemental_Quantity_EmptySet.Enabled = true;
            StandardEnergy_Supplemental_Quantity_EmptySet.Name = "StandardEnergy_Supplemental_Quantity_EmptySet";
            StandardEnergy_Supplemental_Quantity_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Supplemental_MBtuDay_NotEmpty
            // 
            StandardEnergy_Supplemental_MBtuDay_NotEmpty.Enabled = true;
            StandardEnergy_Supplemental_MBtuDay_NotEmpty.Name = "StandardEnergy_Supplemental_MBtuDay_NotEmpty";
            StandardEnergy_Supplemental_MBtuDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Supplemental_MBtuDay_EmptySet
            // 
            StandardEnergy_Supplemental_MBtuDay_EmptySet.Enabled = true;
            StandardEnergy_Supplemental_MBtuDay_EmptySet.Name = "StandardEnergy_Supplemental_MBtuDay_EmptySet";
            StandardEnergy_Supplemental_MBtuDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_Supplemental_MBtu_NotEmpty
            // 
            StandardEnergy_Supplemental_MBtu_NotEmpty.Enabled = true;
            StandardEnergy_Supplemental_MBtu_NotEmpty.Name = "StandardEnergy_Supplemental_MBtu_NotEmpty";
            StandardEnergy_Supplemental_MBtu_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_Supplemental_MBtu_EmptySet
            // 
            StandardEnergy_Supplemental_MBtu_EmptySet.Enabled = true;
            StandardEnergy_Supplemental_MBtu_EmptySet.Name = "StandardEnergy_Supplemental_MBtu_EmptySet";
            StandardEnergy_Supplemental_MBtu_EmptySet.ResultSet = 2;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardEnergy_Supplemental_MBtuDayData;
        private SqlDatabaseTestActions StandardEnergy_Supplemental_MBtuData;
        private SqlDatabaseTestActions StandardEnergy_Supplemental_QuantityData;
    }
}
