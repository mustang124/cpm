﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend.StandardEnergy
{
    [TestClass()]
    public class _1030_HydroTreating : SqlDatabaseTestClass
    {

        public _1030_HydroTreating()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void StandardEnergy_HydroTreater_MBtuDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_HydroTreater_MBtuDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_HydroTreater_BarrelsDay()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_HydroTreater_BarrelsDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_HydroTreater_Barrels()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_HydroTreater_BarrelsData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StandardEnergy_HydroTreater_Quantity_kMT()
        {
            SqlDatabaseTestActions testActions = this.StandardEnergy_HydroTreater_Quantity_kMTData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }




        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_HydroTreater_MBtuDay_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_1030_HydroTreating));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_HydroTreater_BarrelsDay_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_HydroTreater_Barrels_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StandardEnergy_HydroTreater_Quantity_kMT_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_HydroTreater_Barrels_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_HydroTreater_Barrels_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_HydroTreater_BarrelsDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_HydroTreater_BarrelsDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_HydroTreater_MBtuDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_HydroTreater_MBtuDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StandardEnergy_HydroTreater_Quantity_kMT_EmptySet;
            this.StandardEnergy_HydroTreater_MBtuDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_HydroTreater_BarrelsDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_HydroTreater_BarrelsData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StandardEnergy_HydroTreater_Quantity_kMTData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            StandardEnergy_HydroTreater_MBtuDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_HydroTreater_BarrelsDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_HydroTreater_Barrels_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_HydroTreater_Quantity_kMT_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StandardEnergy_HydroTreater_Barrels_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_HydroTreater_Barrels_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_HydroTreater_BarrelsDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_HydroTreater_BarrelsDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_HydroTreater_MBtuDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_HydroTreater_MBtuDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            StandardEnergy_HydroTreater_Quantity_kMT_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // StandardEnergy_HydroTreater_MBtuDay_TestAction
            // 
            StandardEnergy_HydroTreater_MBtuDay_TestAction.Conditions.Add(StandardEnergy_HydroTreater_MBtuDay_NotEmpty);
            StandardEnergy_HydroTreater_MBtuDay_TestAction.Conditions.Add(StandardEnergy_HydroTreater_MBtuDay_EmptySet);
            resources.ApplyResources(StandardEnergy_HydroTreater_MBtuDay_TestAction, "StandardEnergy_HydroTreater_MBtuDay_TestAction");
            // 
            // StandardEnergy_HydroTreater_MBtuDayData
            // 
            this.StandardEnergy_HydroTreater_MBtuDayData.PosttestAction = null;
            this.StandardEnergy_HydroTreater_MBtuDayData.PretestAction = null;
            this.StandardEnergy_HydroTreater_MBtuDayData.TestAction = StandardEnergy_HydroTreater_MBtuDay_TestAction;
            // 
            // StandardEnergy_HydroTreater_BarrelsDayData
            // 
            this.StandardEnergy_HydroTreater_BarrelsDayData.PosttestAction = null;
            this.StandardEnergy_HydroTreater_BarrelsDayData.PretestAction = null;
            this.StandardEnergy_HydroTreater_BarrelsDayData.TestAction = StandardEnergy_HydroTreater_BarrelsDay_TestAction;
            // 
            // StandardEnergy_HydroTreater_BarrelsDay_TestAction
            // 
            StandardEnergy_HydroTreater_BarrelsDay_TestAction.Conditions.Add(StandardEnergy_HydroTreater_BarrelsDay_NotEmpty);
            StandardEnergy_HydroTreater_BarrelsDay_TestAction.Conditions.Add(StandardEnergy_HydroTreater_BarrelsDay_EmptySet);
            resources.ApplyResources(StandardEnergy_HydroTreater_BarrelsDay_TestAction, "StandardEnergy_HydroTreater_BarrelsDay_TestAction");
            // 
            // StandardEnergy_HydroTreater_BarrelsData
            // 
            this.StandardEnergy_HydroTreater_BarrelsData.PosttestAction = null;
            this.StandardEnergy_HydroTreater_BarrelsData.PretestAction = null;
            this.StandardEnergy_HydroTreater_BarrelsData.TestAction = StandardEnergy_HydroTreater_Barrels_TestAction;
            // 
            // StandardEnergy_HydroTreater_Barrels_TestAction
            // 
            StandardEnergy_HydroTreater_Barrels_TestAction.Conditions.Add(StandardEnergy_HydroTreater_Barrels_NotEmpty);
            StandardEnergy_HydroTreater_Barrels_TestAction.Conditions.Add(StandardEnergy_HydroTreater_Barrels_EmptySet);
            resources.ApplyResources(StandardEnergy_HydroTreater_Barrels_TestAction, "StandardEnergy_HydroTreater_Barrels_TestAction");
            // 
            // StandardEnergy_HydroTreater_Quantity_kMTData
            // 
            this.StandardEnergy_HydroTreater_Quantity_kMTData.PosttestAction = null;
            this.StandardEnergy_HydroTreater_Quantity_kMTData.PretestAction = null;
            this.StandardEnergy_HydroTreater_Quantity_kMTData.TestAction = StandardEnergy_HydroTreater_Quantity_kMT_TestAction;
            // 
            // StandardEnergy_HydroTreater_Quantity_kMT_TestAction
            // 
            StandardEnergy_HydroTreater_Quantity_kMT_TestAction.Conditions.Add(StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty);
            StandardEnergy_HydroTreater_Quantity_kMT_TestAction.Conditions.Add(StandardEnergy_HydroTreater_Quantity_kMT_EmptySet);
            resources.ApplyResources(StandardEnergy_HydroTreater_Quantity_kMT_TestAction, "StandardEnergy_HydroTreater_Quantity_kMT_TestAction");
            // 
            // StandardEnergy_HydroTreater_Barrels_NotEmpty
            // 
            StandardEnergy_HydroTreater_Barrels_NotEmpty.Enabled = true;
            StandardEnergy_HydroTreater_Barrels_NotEmpty.Name = "StandardEnergy_HydroTreater_Barrels_NotEmpty";
            StandardEnergy_HydroTreater_Barrels_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_HydroTreater_Barrels_EmptySet
            // 
            StandardEnergy_HydroTreater_Barrels_EmptySet.Enabled = true;
            StandardEnergy_HydroTreater_Barrels_EmptySet.Name = "StandardEnergy_HydroTreater_Barrels_EmptySet";
            StandardEnergy_HydroTreater_Barrels_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_HydroTreater_BarrelsDay_NotEmpty
            // 
            StandardEnergy_HydroTreater_BarrelsDay_NotEmpty.Enabled = true;
            StandardEnergy_HydroTreater_BarrelsDay_NotEmpty.Name = "StandardEnergy_HydroTreater_BarrelsDay_NotEmpty";
            StandardEnergy_HydroTreater_BarrelsDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_HydroTreater_BarrelsDay_EmptySet
            // 
            StandardEnergy_HydroTreater_BarrelsDay_EmptySet.Enabled = true;
            StandardEnergy_HydroTreater_BarrelsDay_EmptySet.Name = "StandardEnergy_HydroTreater_BarrelsDay_EmptySet";
            StandardEnergy_HydroTreater_BarrelsDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_HydroTreater_MBtuDay_NotEmpty
            // 
            StandardEnergy_HydroTreater_MBtuDay_NotEmpty.Enabled = true;
            StandardEnergy_HydroTreater_MBtuDay_NotEmpty.Name = "StandardEnergy_HydroTreater_MBtuDay_NotEmpty";
            StandardEnergy_HydroTreater_MBtuDay_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_HydroTreater_MBtuDay_EmptySet
            // 
            StandardEnergy_HydroTreater_MBtuDay_EmptySet.Enabled = true;
            StandardEnergy_HydroTreater_MBtuDay_EmptySet.Name = "StandardEnergy_HydroTreater_MBtuDay_EmptySet";
            StandardEnergy_HydroTreater_MBtuDay_EmptySet.ResultSet = 2;
            // 
            // StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty
            // 
            StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty.Enabled = true;
            StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty.Name = "StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty";
            StandardEnergy_HydroTreater_Quantity_kMT_NotEmpty.ResultSet = 1;
            // 
            // StandardEnergy_HydroTreater_Quantity_kMT_EmptySet
            // 
            StandardEnergy_HydroTreater_Quantity_kMT_EmptySet.Enabled = true;
            StandardEnergy_HydroTreater_Quantity_kMT_EmptySet.Name = "StandardEnergy_HydroTreater_Quantity_kMT_EmptySet";
            StandardEnergy_HydroTreater_Quantity_kMT_EmptySet.ResultSet = 2;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions StandardEnergy_HydroTreater_MBtuDayData;
        private SqlDatabaseTestActions StandardEnergy_HydroTreater_BarrelsDayData;
        private SqlDatabaseTestActions StandardEnergy_HydroTreater_BarrelsData;
        private SqlDatabaseTestActions StandardEnergy_HydroTreater_Quantity_kMTData;
    }
}
