﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.ETL
{
    [TestClass()]
    public class Stage : SqlDatabaseTestClass
    {

        public Stage()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void Capacity()
        {
            SqlDatabaseTestActions testActions = this.CapacityData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Facilities()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesBoilers()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesBoilersData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesElecGeneration()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesElecGenerationData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesFractionator_Quantity_kBSD()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesFractionator_Quantity_kBSDData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesHydroTreater_Quantity_kBSD()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesHydroTreater_Quantity_kBSDData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesHydroTreater_Processed_Pcnt()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesHydroTreater_Processed_PcntData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesHydroTreater_TypeId()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesHydroTreater_TypeIdData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesHydroTreater_Density_SG()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesHydroTreater_Density_SGData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesTrains()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesTrainsData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamQuantity_StreamId()
        {
            SqlDatabaseTestActions testActions = this.StreamQuantity_StreamIdData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamDescription()
        {
            SqlDatabaseTestActions testActions = this.StreamDescriptionData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamComposition()
        {
            SqlDatabaseTestActions testActions = this.StreamCompositionData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamCompositionMol()
        {
            SqlDatabaseTestActions testActions = this.StreamCompositionMolData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamDensity()
        {
            SqlDatabaseTestActions testActions = this.StreamDensityData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamRecovered()
        {
            SqlDatabaseTestActions testActions = this.StreamRecoveredData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamQuantity()
        {
            SqlDatabaseTestActions testActions = this.StreamQuantityData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesFractionator_StreamId()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesFractionator_StreamIdData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesFractionator_Throughput_kMT()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesFractionator_Throughput_kMTData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void FacilitiesFractionator_Density_SG()
        {
            SqlDatabaseTestActions testActions = this.FacilitiesFractionator_Density_SGData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void StreamRecycled()
        {
            SqlDatabaseTestActions testActions = this.StreamRecycledData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }





















        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stage));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Facilities_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Facilities_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesBoilers_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesBoilers_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesElecGeneration_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesElecGeneration_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesFractionator_Quantity_kBSD_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesFractionator_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesHydroTreater_Quantity_kBSD_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesHydroTreater_Quantity_kBSD_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesHydroTreater_Processed_Pcnt_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesHydroTreater_Processed_Pcnt_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesHydroTreater_TypeId_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesHydroTreater_TypeId_EmtpySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesHydroTreater_Density_SG_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesHydroTreater_Density_SG_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesTrains_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesTrains_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamQuantity_StreamId_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamQuantity_StreamId_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamDescription_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamDescription_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamComposition_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamComposition_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamCompositionMol_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamCompositionMol_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamDensity_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamDensity_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamRecovered_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamRecovered_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamQuantity_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamQuantity_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesFractionator_StreamId_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesFractionator_StreamId_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesFractionator_Throughput_kMT_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesFractionator_Throughput_kMT_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction FacilitiesFractionator_Density_SG_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition FacilitiesFractionator_Density_SG_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction StreamRecycled_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition StreamRecycled_EmtpySet;
            this.CapacityData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesBoilersData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesElecGenerationData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesFractionator_Quantity_kBSDData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesHydroTreater_Quantity_kBSDData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesHydroTreater_Processed_PcntData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesHydroTreater_TypeIdData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesHydroTreater_Density_SGData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesTrainsData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamQuantity_StreamIdData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamDescriptionData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamCompositionData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamCompositionMolData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamDensityData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamRecoveredData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamQuantityData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesFractionator_StreamIdData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesFractionator_Throughput_kMTData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.FacilitiesFractionator_Density_SGData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.StreamRecycledData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Capacity_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Facilities_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Facilities_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesBoilers_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesBoilers_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesElecGeneration_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesElecGeneration_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesFractionator_Quantity_kBSD_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesFractionator_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesHydroTreater_Quantity_kBSD_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesHydroTreater_Quantity_kBSD_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesHydroTreater_Processed_Pcnt_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesHydroTreater_Processed_Pcnt_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesHydroTreater_TypeId_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesHydroTreater_TypeId_EmtpySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesHydroTreater_Density_SG_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesHydroTreater_Density_SG_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesTrains_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesTrains_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamQuantity_StreamId_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamQuantity_StreamId_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamDescription_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamDescription_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamComposition_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamComposition_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamCompositionMol_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamCompositionMol_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamDensity_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamDensity_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamRecovered_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamRecovered_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamQuantity_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamQuantity_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesFractionator_StreamId_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesFractionator_StreamId_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesFractionator_Throughput_kMT_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesFractionator_Throughput_kMT_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            FacilitiesFractionator_Density_SG_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            FacilitiesFractionator_Density_SG_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            StreamRecycled_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            StreamRecycled_EmtpySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // Capacity_TestAction
            // 
            Capacity_TestAction.Conditions.Add(Capacity_EmptySet);
            resources.ApplyResources(Capacity_TestAction, "Capacity_TestAction");
            // 
            // Capacity_EmptySet
            // 
            Capacity_EmptySet.Enabled = true;
            Capacity_EmptySet.Name = "Capacity_EmptySet";
            Capacity_EmptySet.ResultSet = 1;
            // 
            // Facilities_TestAction
            // 
            Facilities_TestAction.Conditions.Add(Facilities_EmptySet);
            resources.ApplyResources(Facilities_TestAction, "Facilities_TestAction");
            // 
            // Facilities_EmptySet
            // 
            Facilities_EmptySet.Enabled = true;
            Facilities_EmptySet.Name = "Facilities_EmptySet";
            Facilities_EmptySet.ResultSet = 1;
            // 
            // FacilitiesBoilers_TestAction
            // 
            FacilitiesBoilers_TestAction.Conditions.Add(FacilitiesBoilers_EmptySet);
            resources.ApplyResources(FacilitiesBoilers_TestAction, "FacilitiesBoilers_TestAction");
            // 
            // FacilitiesBoilers_EmptySet
            // 
            FacilitiesBoilers_EmptySet.Enabled = true;
            FacilitiesBoilers_EmptySet.Name = "FacilitiesBoilers_EmptySet";
            FacilitiesBoilers_EmptySet.ResultSet = 1;
            // 
            // FacilitiesElecGeneration_TestAction
            // 
            FacilitiesElecGeneration_TestAction.Conditions.Add(FacilitiesElecGeneration_EmptySet);
            resources.ApplyResources(FacilitiesElecGeneration_TestAction, "FacilitiesElecGeneration_TestAction");
            // 
            // FacilitiesElecGeneration_EmptySet
            // 
            FacilitiesElecGeneration_EmptySet.Enabled = true;
            FacilitiesElecGeneration_EmptySet.Name = "FacilitiesElecGeneration_EmptySet";
            FacilitiesElecGeneration_EmptySet.ResultSet = 1;
            // 
            // FacilitiesFractionator_Quantity_kBSD_TestAction
            // 
            FacilitiesFractionator_Quantity_kBSD_TestAction.Conditions.Add(FacilitiesFractionator_EmptySet);
            resources.ApplyResources(FacilitiesFractionator_Quantity_kBSD_TestAction, "FacilitiesFractionator_Quantity_kBSD_TestAction");
            // 
            // FacilitiesFractionator_EmptySet
            // 
            FacilitiesFractionator_EmptySet.Enabled = true;
            FacilitiesFractionator_EmptySet.Name = "FacilitiesFractionator_EmptySet";
            FacilitiesFractionator_EmptySet.ResultSet = 1;
            // 
            // FacilitiesHydroTreater_Quantity_kBSD_TestAction
            // 
            FacilitiesHydroTreater_Quantity_kBSD_TestAction.Conditions.Add(FacilitiesHydroTreater_Quantity_kBSD_EmptySet);
            resources.ApplyResources(FacilitiesHydroTreater_Quantity_kBSD_TestAction, "FacilitiesHydroTreater_Quantity_kBSD_TestAction");
            // 
            // FacilitiesHydroTreater_Quantity_kBSD_EmptySet
            // 
            FacilitiesHydroTreater_Quantity_kBSD_EmptySet.Enabled = true;
            FacilitiesHydroTreater_Quantity_kBSD_EmptySet.Name = "FacilitiesHydroTreater_Quantity_kBSD_EmptySet";
            FacilitiesHydroTreater_Quantity_kBSD_EmptySet.ResultSet = 1;
            // 
            // FacilitiesHydroTreater_Processed_Pcnt_TestAction
            // 
            FacilitiesHydroTreater_Processed_Pcnt_TestAction.Conditions.Add(FacilitiesHydroTreater_Processed_Pcnt_EmptySet);
            resources.ApplyResources(FacilitiesHydroTreater_Processed_Pcnt_TestAction, "FacilitiesHydroTreater_Processed_Pcnt_TestAction");
            // 
            // FacilitiesHydroTreater_Processed_Pcnt_EmptySet
            // 
            FacilitiesHydroTreater_Processed_Pcnt_EmptySet.Enabled = true;
            FacilitiesHydroTreater_Processed_Pcnt_EmptySet.Name = "FacilitiesHydroTreater_Processed_Pcnt_EmptySet";
            FacilitiesHydroTreater_Processed_Pcnt_EmptySet.ResultSet = 1;
            // 
            // FacilitiesHydroTreater_TypeId_TestAction
            // 
            FacilitiesHydroTreater_TypeId_TestAction.Conditions.Add(FacilitiesHydroTreater_TypeId_EmtpySet);
            resources.ApplyResources(FacilitiesHydroTreater_TypeId_TestAction, "FacilitiesHydroTreater_TypeId_TestAction");
            // 
            // FacilitiesHydroTreater_TypeId_EmtpySet
            // 
            FacilitiesHydroTreater_TypeId_EmtpySet.Enabled = true;
            FacilitiesHydroTreater_TypeId_EmtpySet.Name = "FacilitiesHydroTreater_TypeId_EmtpySet";
            FacilitiesHydroTreater_TypeId_EmtpySet.ResultSet = 1;
            // 
            // FacilitiesHydroTreater_Density_SG_TestAction
            // 
            FacilitiesHydroTreater_Density_SG_TestAction.Conditions.Add(FacilitiesHydroTreater_Density_SG_EmptySet);
            resources.ApplyResources(FacilitiesHydroTreater_Density_SG_TestAction, "FacilitiesHydroTreater_Density_SG_TestAction");
            // 
            // FacilitiesHydroTreater_Density_SG_EmptySet
            // 
            FacilitiesHydroTreater_Density_SG_EmptySet.Enabled = true;
            FacilitiesHydroTreater_Density_SG_EmptySet.Name = "FacilitiesHydroTreater_Density_SG_EmptySet";
            FacilitiesHydroTreater_Density_SG_EmptySet.ResultSet = 1;
            // 
            // FacilitiesTrains_TestAction
            // 
            FacilitiesTrains_TestAction.Conditions.Add(FacilitiesTrains_EmptySet);
            resources.ApplyResources(FacilitiesTrains_TestAction, "FacilitiesTrains_TestAction");
            // 
            // FacilitiesTrains_EmptySet
            // 
            FacilitiesTrains_EmptySet.Enabled = true;
            FacilitiesTrains_EmptySet.Name = "FacilitiesTrains_EmptySet";
            FacilitiesTrains_EmptySet.ResultSet = 1;
            // 
            // StreamQuantity_StreamId_TestAction
            // 
            StreamQuantity_StreamId_TestAction.Conditions.Add(StreamQuantity_StreamId_EmptySet);
            resources.ApplyResources(StreamQuantity_StreamId_TestAction, "StreamQuantity_StreamId_TestAction");
            // 
            // StreamQuantity_StreamId_EmptySet
            // 
            StreamQuantity_StreamId_EmptySet.Enabled = true;
            StreamQuantity_StreamId_EmptySet.Name = "StreamQuantity_StreamId_EmptySet";
            StreamQuantity_StreamId_EmptySet.ResultSet = 1;
            // 
            // StreamDescription_TestAction
            // 
            StreamDescription_TestAction.Conditions.Add(StreamDescription_EmptySet);
            resources.ApplyResources(StreamDescription_TestAction, "StreamDescription_TestAction");
            // 
            // StreamDescription_EmptySet
            // 
            StreamDescription_EmptySet.Enabled = true;
            StreamDescription_EmptySet.Name = "StreamDescription_EmptySet";
            StreamDescription_EmptySet.ResultSet = 1;
            // 
            // StreamComposition_TestAction
            // 
            StreamComposition_TestAction.Conditions.Add(StreamComposition_EmptySet);
            resources.ApplyResources(StreamComposition_TestAction, "StreamComposition_TestAction");
            // 
            // StreamComposition_EmptySet
            // 
            StreamComposition_EmptySet.Enabled = true;
            StreamComposition_EmptySet.Name = "StreamComposition_EmptySet";
            StreamComposition_EmptySet.ResultSet = 1;
            // 
            // StreamCompositionMol_TestAction
            // 
            StreamCompositionMol_TestAction.Conditions.Add(StreamCompositionMol_EmptySet);
            resources.ApplyResources(StreamCompositionMol_TestAction, "StreamCompositionMol_TestAction");
            // 
            // StreamCompositionMol_EmptySet
            // 
            StreamCompositionMol_EmptySet.Enabled = true;
            StreamCompositionMol_EmptySet.Name = "StreamCompositionMol_EmptySet";
            StreamCompositionMol_EmptySet.ResultSet = 1;
            // 
            // StreamDensity_TestAction
            // 
            StreamDensity_TestAction.Conditions.Add(StreamDensity_EmptySet);
            resources.ApplyResources(StreamDensity_TestAction, "StreamDensity_TestAction");
            // 
            // StreamDensity_EmptySet
            // 
            StreamDensity_EmptySet.Enabled = true;
            StreamDensity_EmptySet.Name = "StreamDensity_EmptySet";
            StreamDensity_EmptySet.ResultSet = 1;
            // 
            // StreamRecovered_TestAction
            // 
            StreamRecovered_TestAction.Conditions.Add(StreamRecovered_EmptySet);
            resources.ApplyResources(StreamRecovered_TestAction, "StreamRecovered_TestAction");
            // 
            // StreamRecovered_EmptySet
            // 
            StreamRecovered_EmptySet.Enabled = true;
            StreamRecovered_EmptySet.Name = "StreamRecovered_EmptySet";
            StreamRecovered_EmptySet.ResultSet = 1;
            // 
            // StreamQuantity_TestAction
            // 
            StreamQuantity_TestAction.Conditions.Add(StreamQuantity_EmptySet);
            resources.ApplyResources(StreamQuantity_TestAction, "StreamQuantity_TestAction");
            // 
            // StreamQuantity_EmptySet
            // 
            StreamQuantity_EmptySet.Enabled = true;
            StreamQuantity_EmptySet.Name = "StreamQuantity_EmptySet";
            StreamQuantity_EmptySet.ResultSet = 1;
            // 
            // FacilitiesFractionator_StreamId_TestAction
            // 
            FacilitiesFractionator_StreamId_TestAction.Conditions.Add(FacilitiesFractionator_StreamId_EmptySet);
            resources.ApplyResources(FacilitiesFractionator_StreamId_TestAction, "FacilitiesFractionator_StreamId_TestAction");
            // 
            // FacilitiesFractionator_StreamId_EmptySet
            // 
            FacilitiesFractionator_StreamId_EmptySet.Enabled = true;
            FacilitiesFractionator_StreamId_EmptySet.Name = "FacilitiesFractionator_StreamId_EmptySet";
            FacilitiesFractionator_StreamId_EmptySet.ResultSet = 1;
            // 
            // FacilitiesFractionator_Throughput_kMT_TestAction
            // 
            FacilitiesFractionator_Throughput_kMT_TestAction.Conditions.Add(FacilitiesFractionator_Throughput_kMT_EmptySet);
            resources.ApplyResources(FacilitiesFractionator_Throughput_kMT_TestAction, "FacilitiesFractionator_Throughput_kMT_TestAction");
            // 
            // FacilitiesFractionator_Throughput_kMT_EmptySet
            // 
            FacilitiesFractionator_Throughput_kMT_EmptySet.Enabled = true;
            FacilitiesFractionator_Throughput_kMT_EmptySet.Name = "FacilitiesFractionator_Throughput_kMT_EmptySet";
            FacilitiesFractionator_Throughput_kMT_EmptySet.ResultSet = 1;
            // 
            // FacilitiesFractionator_Density_SG_TestAction
            // 
            FacilitiesFractionator_Density_SG_TestAction.Conditions.Add(FacilitiesFractionator_Density_SG_EmptySet);
            resources.ApplyResources(FacilitiesFractionator_Density_SG_TestAction, "FacilitiesFractionator_Density_SG_TestAction");
            // 
            // FacilitiesFractionator_Density_SG_EmptySet
            // 
            FacilitiesFractionator_Density_SG_EmptySet.Enabled = true;
            FacilitiesFractionator_Density_SG_EmptySet.Name = "FacilitiesFractionator_Density_SG_EmptySet";
            FacilitiesFractionator_Density_SG_EmptySet.ResultSet = 1;
            // 
            // StreamRecycled_TestAction
            // 
            StreamRecycled_TestAction.Conditions.Add(StreamRecycled_EmtpySet);
            resources.ApplyResources(StreamRecycled_TestAction, "StreamRecycled_TestAction");
            // 
            // StreamRecycled_EmtpySet
            // 
            StreamRecycled_EmtpySet.Enabled = true;
            StreamRecycled_EmtpySet.Name = "StreamRecycled_EmtpySet";
            StreamRecycled_EmtpySet.ResultSet = 1;
            // 
            // CapacityData
            // 
            this.CapacityData.PosttestAction = null;
            this.CapacityData.PretestAction = null;
            this.CapacityData.TestAction = Capacity_TestAction;
            // 
            // FacilitiesData
            // 
            this.FacilitiesData.PosttestAction = null;
            this.FacilitiesData.PretestAction = null;
            this.FacilitiesData.TestAction = Facilities_TestAction;
            // 
            // FacilitiesBoilersData
            // 
            this.FacilitiesBoilersData.PosttestAction = null;
            this.FacilitiesBoilersData.PretestAction = null;
            this.FacilitiesBoilersData.TestAction = FacilitiesBoilers_TestAction;
            // 
            // FacilitiesElecGenerationData
            // 
            this.FacilitiesElecGenerationData.PosttestAction = null;
            this.FacilitiesElecGenerationData.PretestAction = null;
            this.FacilitiesElecGenerationData.TestAction = FacilitiesElecGeneration_TestAction;
            // 
            // FacilitiesFractionator_Quantity_kBSDData
            // 
            this.FacilitiesFractionator_Quantity_kBSDData.PosttestAction = null;
            this.FacilitiesFractionator_Quantity_kBSDData.PretestAction = null;
            this.FacilitiesFractionator_Quantity_kBSDData.TestAction = FacilitiesFractionator_Quantity_kBSD_TestAction;
            // 
            // FacilitiesHydroTreater_Quantity_kBSDData
            // 
            this.FacilitiesHydroTreater_Quantity_kBSDData.PosttestAction = null;
            this.FacilitiesHydroTreater_Quantity_kBSDData.PretestAction = null;
            this.FacilitiesHydroTreater_Quantity_kBSDData.TestAction = FacilitiesHydroTreater_Quantity_kBSD_TestAction;
            // 
            // FacilitiesHydroTreater_Processed_PcntData
            // 
            this.FacilitiesHydroTreater_Processed_PcntData.PosttestAction = null;
            this.FacilitiesHydroTreater_Processed_PcntData.PretestAction = null;
            this.FacilitiesHydroTreater_Processed_PcntData.TestAction = FacilitiesHydroTreater_Processed_Pcnt_TestAction;
            // 
            // FacilitiesHydroTreater_TypeIdData
            // 
            this.FacilitiesHydroTreater_TypeIdData.PosttestAction = null;
            this.FacilitiesHydroTreater_TypeIdData.PretestAction = null;
            this.FacilitiesHydroTreater_TypeIdData.TestAction = FacilitiesHydroTreater_TypeId_TestAction;
            // 
            // FacilitiesHydroTreater_Density_SGData
            // 
            this.FacilitiesHydroTreater_Density_SGData.PosttestAction = null;
            this.FacilitiesHydroTreater_Density_SGData.PretestAction = null;
            this.FacilitiesHydroTreater_Density_SGData.TestAction = FacilitiesHydroTreater_Density_SG_TestAction;
            // 
            // FacilitiesTrainsData
            // 
            this.FacilitiesTrainsData.PosttestAction = null;
            this.FacilitiesTrainsData.PretestAction = null;
            this.FacilitiesTrainsData.TestAction = FacilitiesTrains_TestAction;
            // 
            // StreamQuantity_StreamIdData
            // 
            this.StreamQuantity_StreamIdData.PosttestAction = null;
            this.StreamQuantity_StreamIdData.PretestAction = null;
            this.StreamQuantity_StreamIdData.TestAction = StreamQuantity_StreamId_TestAction;
            // 
            // StreamDescriptionData
            // 
            this.StreamDescriptionData.PosttestAction = null;
            this.StreamDescriptionData.PretestAction = null;
            this.StreamDescriptionData.TestAction = StreamDescription_TestAction;
            // 
            // StreamCompositionData
            // 
            this.StreamCompositionData.PosttestAction = null;
            this.StreamCompositionData.PretestAction = null;
            this.StreamCompositionData.TestAction = StreamComposition_TestAction;
            // 
            // StreamCompositionMolData
            // 
            this.StreamCompositionMolData.PosttestAction = null;
            this.StreamCompositionMolData.PretestAction = null;
            this.StreamCompositionMolData.TestAction = StreamCompositionMol_TestAction;
            // 
            // StreamDensityData
            // 
            this.StreamDensityData.PosttestAction = null;
            this.StreamDensityData.PretestAction = null;
            this.StreamDensityData.TestAction = StreamDensity_TestAction;
            // 
            // StreamRecoveredData
            // 
            this.StreamRecoveredData.PosttestAction = null;
            this.StreamRecoveredData.PretestAction = null;
            this.StreamRecoveredData.TestAction = StreamRecovered_TestAction;
            // 
            // StreamQuantityData
            // 
            this.StreamQuantityData.PosttestAction = null;
            this.StreamQuantityData.PretestAction = null;
            this.StreamQuantityData.TestAction = StreamQuantity_TestAction;
            // 
            // FacilitiesFractionator_StreamIdData
            // 
            this.FacilitiesFractionator_StreamIdData.PosttestAction = null;
            this.FacilitiesFractionator_StreamIdData.PretestAction = null;
            this.FacilitiesFractionator_StreamIdData.TestAction = FacilitiesFractionator_StreamId_TestAction;
            // 
            // FacilitiesFractionator_Throughput_kMTData
            // 
            this.FacilitiesFractionator_Throughput_kMTData.PosttestAction = null;
            this.FacilitiesFractionator_Throughput_kMTData.PretestAction = null;
            this.FacilitiesFractionator_Throughput_kMTData.TestAction = FacilitiesFractionator_Throughput_kMT_TestAction;
            // 
            // FacilitiesFractionator_Density_SGData
            // 
            this.FacilitiesFractionator_Density_SGData.PosttestAction = null;
            this.FacilitiesFractionator_Density_SGData.PretestAction = null;
            this.FacilitiesFractionator_Density_SGData.TestAction = FacilitiesFractionator_Density_SG_TestAction;
            // 
            // StreamRecycledData
            // 
            this.StreamRecycledData.PosttestAction = null;
            this.StreamRecycledData.PretestAction = null;
            this.StreamRecycledData.TestAction = StreamRecycled_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions CapacityData;
        private SqlDatabaseTestActions FacilitiesData;
        private SqlDatabaseTestActions FacilitiesBoilersData;
        private SqlDatabaseTestActions FacilitiesElecGenerationData;
        private SqlDatabaseTestActions FacilitiesFractionator_Quantity_kBSDData;
        private SqlDatabaseTestActions FacilitiesHydroTreater_Quantity_kBSDData;
        private SqlDatabaseTestActions FacilitiesHydroTreater_Processed_PcntData;
        private SqlDatabaseTestActions FacilitiesHydroTreater_TypeIdData;
        private SqlDatabaseTestActions FacilitiesHydroTreater_Density_SGData;
        private SqlDatabaseTestActions FacilitiesTrainsData;
        private SqlDatabaseTestActions StreamQuantity_StreamIdData;
        private SqlDatabaseTestActions StreamDescriptionData;
        private SqlDatabaseTestActions StreamCompositionData;
        private SqlDatabaseTestActions StreamCompositionMolData;
        private SqlDatabaseTestActions StreamDensityData;
        private SqlDatabaseTestActions StreamRecoveredData;
        private SqlDatabaseTestActions StreamQuantityData;
        private SqlDatabaseTestActions FacilitiesFractionator_StreamIdData;
        private SqlDatabaseTestActions FacilitiesFractionator_Throughput_kMTData;
        private SqlDatabaseTestActions FacilitiesFractionator_Density_SGData;
        private SqlDatabaseTestActions StreamRecycledData;
    }
}
