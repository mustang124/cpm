﻿CREATE TABLE [ante].[MapStreamNumberId]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapStreamNumberId_Methodology]		REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [CR_MapStreamNumberId_StreamNumber]		CHECK([StreamNumber] > 0),

	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_MapStreamNumberId_Stream_LookUp]		REFERENCES [dim].[Stream_LookUp] ([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapStreamNumberId_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamNumberId_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamNumberId_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapStreamNumberId_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapStreamNumberId]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamNumber] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_MapStreamNumberId]
ON [ante].[MapStreamNumberId]([MethodologyId] DESC, [StreamNumber] ASC, [StreamId] ASC);
GO

CREATE TRIGGER [ante].[t_MapStreamNumberId_u]
ON [ante].[MapStreamNumberId]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapStreamNumberId]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapStreamNumberId].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapStreamNumberId].[StreamNumber]	= INSERTED.[StreamNumber];

END;
GO
