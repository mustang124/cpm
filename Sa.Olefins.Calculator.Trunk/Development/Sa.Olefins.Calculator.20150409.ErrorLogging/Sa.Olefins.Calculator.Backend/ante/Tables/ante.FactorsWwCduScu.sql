﻿CREATE TABLE [ante].[FactorsWwCduScu]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_FactorsWwCduScu_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StandardId]			INT					NOT	NULL	CONSTRAINT [FK_FactorsWwCduScu_Standards_LookUp]		REFERENCES [dim].[Standard_LookUp] ([StandardId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[WwCduScu]				FLOAT				NOT	NULL	CONSTRAINT [CR_FactorsWwCduScu_WwCduScu_MinIncl_0.0]	CHECK([WwCduScu] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FactorsWwCduScu_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FactorsWwCduScu_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FactorsWwCduScu_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FactorsWwCduScu_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FactorsWwCduScu]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC)
);
GO

CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_PersMaint]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 4;
GO

CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_PersNonMaint]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 5;
GO

CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_Mes]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 6;
GO

CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_NonEnergy]
ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
INCLUDE ([Coefficient], [Exponent], [ValueMin], [ValueMax])
WHERE [StandardId] = 7;
GO

CREATE TRIGGER [ante].[t_FactorsWwCduScu_u]
ON [ante].[FactorsWwCduScu]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[FactorsWwCduScu]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[FactorsWwCduScu].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[FactorsWwCduScu].[StandardId]			= INSERTED.[StandardId];

END;
GO