﻿CREATE TABLE [ante].[MapComponentRecycle]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapComponentRecycle_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_MapComponentRecycle_Stream_LookUp]		REFERENCES [dim].[Stream_LookUp] ([StreamId])				ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_MapComponentRecycle_Component_LookUp]	REFERENCES [dim].[Component_LookUp] ([ComponentId])			ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapComponentRecycle_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapComponentRecycle_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapComponentRecycle_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapComponentRecycle_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapComponentRecycle]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapComponentRecycle_u]
ON [ante].[MapComponentRecycle]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapComponentRecycle]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapComponentRecycle].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapComponentRecycle].[StreamId]		= INSERTED.[StreamId];

END;
GO
