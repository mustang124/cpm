﻿CREATE TABLE [ante].[MapFactorStream]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapFactorStream_Methodology]		REFERENCES [ante].[Methodology]([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_MapFactorStream_Stream_LookUp]	REFERENCES [dim].[Stream_LookUp]([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_MapFactorStream_Factor_LookUp]	REFERENCES [dim].[Factor_LookUp]([FactorId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapFactorStream_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorStream_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorStream_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapFactorStream_tsModifiedApp]	DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapFactorStream]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapFactorStream_u]
ON [ante].[MapFactorStream]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorStream]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorStream].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorStream].[StreamId]			= INSERTED.[StreamId];

END;
GO
