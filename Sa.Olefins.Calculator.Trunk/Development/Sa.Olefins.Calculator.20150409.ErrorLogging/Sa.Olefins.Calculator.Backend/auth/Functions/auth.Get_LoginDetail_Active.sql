﻿CREATE FUNCTION [auth].[Get_LoginDetail_Active]
(
	@LoginTag		VARCHAR(254)
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		lbc.[JoinId],
		lbc.[CompanyId],
		lbc.[LoginId],
		lbc.[CompanyName],
		lbc.[LoginTag],
		lbc.[NameLast],
		lbc.[NameFirst],
		lbc.[_NameFull],
		lbc.[_NameComma],
		lbc.[eMail],
		lbc.[RoleId],
		lbc.[RoleLevel],
		lbc.[pSalt],
		lbc.[pWord]
	FROM [auth].[LoginsByCompany_Active] lbc WITH (NOEXPAND)
	WHERE lbc.LoginTag = @LoginTag
);