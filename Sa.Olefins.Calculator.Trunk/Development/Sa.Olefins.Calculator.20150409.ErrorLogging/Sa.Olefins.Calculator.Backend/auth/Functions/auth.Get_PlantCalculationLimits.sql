﻿CREATE FUNCTION [auth].[Get_PlantCalculationLimits]
(
	@PlantId		INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		pcl.[PlantId],
		pcl.[CalcsPerPeriod_Count],
		pcl.[PeriodLen_Days],

		CONVERT(DATE, MAX(cLast.[TimeEnd]))											[LastSubmission_Date],

		CONVERT(DATE, COALESCE(
			DATEADD(DAY, pcl.[PeriodLen_Days], MIN(cAudit.[TimeEnd])),
			DATEADD(DAY, pcl.[PeriodLen_Days], SYSDATETIMEOFFSET())
			))																		[SubmissionRoll_Date],

		COALESCE(COUNT(cAudit.[SubmissionId]), 0)									[Submissions_Count],

		CASE WHEN pcl.[CalcsPerPeriod_Count] < COALESCE(COUNT(cAudit.[SubmissionId]), 0)
		THEN 0
		ELSE pcl.[CalcsPerPeriod_Count] - COALESCE(COUNT(cAudit.[SubmissionId]), 0)
		END																			[SubmissionsRemaining_Count],

		pcl.[CalcsPerPeriod_Count] - COALESCE(COUNT(cAudit.[SubmissionId]), 0)		[SubmissionsBalance_Count]

	FROM [auth].[PlantCalculationLimits]			pcl
	LEFT OUTER JOIN [auth].[JoinPlantSubmission]	jps
		ON	jps.[PlantId]			= pcl.[PlantId]
		AND	jps.[Active]			= 1
	LEFT OUTER JOIN [calc].[Audit]					cLast
		ON	cLast.[SubmissionId]	= jps.[SubmissionId]
		AND	cLast.[Error_Count]		= 0
		AND	cLast.[TimeEnd]			IS NOT NULL
		AND	cLast.[Active]			= 1
	LEFT OUTER JOIN [calc].[Audit]					cAudit
		ON	cAudit.[SubmissionId]	= jps.[SubmissionId]
		AND	cAudit.[Error_Count]	= 0
		AND	cAudit.[TimeEnd]		IS NOT NULL
		AND	cAudit.[Active]			= 1
		AND (DATEDIFF(DAY, cAudit.[TimeBeg], SYSDATETIMEOFFSET()) < pcl.[PeriodLen_Days])
	WHERE	pcl.[PlantId]	= @PlantId
		AND	pcl.[Active]	= 1
	GROUP BY
		pcl.[PlantId],
		pcl.[CalcsPerPeriod_Count],
		pcl.[PeriodLen_Days]
);