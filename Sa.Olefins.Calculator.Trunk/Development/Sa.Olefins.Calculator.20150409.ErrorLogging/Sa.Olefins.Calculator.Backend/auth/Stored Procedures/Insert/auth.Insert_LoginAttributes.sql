﻿CREATE PROCEDURE [auth].[Insert_LoginAttributes]
(
	@LoginId		INT,

	@NameLast		NVARCHAR(128),
	@NameFirst		NVARCHAR(128),
	@eMail			NVARCHAR(254)	= NULL,
	@RoleId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [auth].[LoginsAttributes]([LoginId], [NameLast], [NameFirst], [eMail], [RoleId])
	VALUES(
		@LoginId,
		@NameLast,
		@NameFirst,
		@eMail,
		@RoleId
		);

	RETURN @LoginId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@LoginId:'		+ CAST(@LoginId AS VARCHAR))
					+ (', @NameLast:'		+ CONVERT(VARCHAR, @NameLast))
					+ (', @NameFirst:'		+ CONVERT(VARCHAR, @NameFirst))
			+ COALESCE(', @eMail:'			+ CONVERT(VARCHAR, @eMail),		'')
					+ (', @RoleId:'			+ CONVERT(VARCHAR, @RoleId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO