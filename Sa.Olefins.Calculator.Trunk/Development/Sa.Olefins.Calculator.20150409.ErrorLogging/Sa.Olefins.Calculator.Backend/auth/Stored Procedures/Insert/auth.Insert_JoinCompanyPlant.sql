﻿CREATE PROCEDURE [auth].[Insert_JoinCompanyPlant]
(
	@CompanyId			INT,
	@PlantId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [auth].[JoinCompanyPlant]([CompanyId], [PlantId])
	OUTPUT [INSERTED].[JoinId] INTO @Id([Id])
	VALUES(
		@CompanyId,
		@PlantId
		);

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@CompanyId:'		+ CONVERT(VARCHAR, @CompanyId))
					+ (', @PlantId:'		+ CONVERT(VARCHAR, @PlantId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO