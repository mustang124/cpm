﻿CREATE PROCEDURE [auth].[Insert_JoinCompanyLogin]
(
	@CompanyId			INT,
	@LoginId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [auth].[JoinCompanyLogin]([CompanyId], [LoginId])
	OUTPUT [INSERTED].[JoinId] INTO @Id([Id])
	VALUES(
		@CompanyId,
		@LoginId
		);

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@CompanyId:'		+ CONVERT(VARCHAR, @CompanyId))
					+ (', @LoginId:'		+ CONVERT(VARCHAR, @LoginId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO