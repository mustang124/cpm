﻿DELETE FROM audit.LogError;
GO

ALTER PROCEDURE [audit].[Insert_LogError]
(
	@ProcId			INT,
	@Parameters		VARCHAR (4000) = NULL,
	@XActState		SMALLINT
)
AS
BEGIN

	SET NOCOUNT ON;

IF(@@TRANCOUNT > 0) ROLLBACK TRANSACTION;

--DECLARE @XACT_POINT	CHAR(36)	= CONVERT(CHAR(36), NEWID());
--BEGIN TRANSACTION	@XACT_POINT;
	
	IF (RTRIM(LTRIM(@Parameters)) = '') BEGIN SET @Parameters = NULL; END;

	INSERT INTO [audit].[LogError](
		[ErrorNumber],
		[XActState],
		[ProcedureSchema],	[ProcedureName],		[ProcedureLine],		[ProcedureParam],
		[ErrorMessage],		[ErrorSeverity],		[ErrorState],
		[tsModified],		[tsModifiedHost],		[tsModifiedUser],		[tsModifiedApp])
	SELECT
		ERROR_NUMBER(),
		@XActState,
		OBJECT_SCHEMA_NAME(@ProcId),		OBJECT_NAME(@ProcId),	ERROR_LINE(),		@Parameters,
		ERROR_MESSAGE(),	ERROR_SEVERITY(),	ERROR_STATE(),
		SYSDATETIMEOFFSET(),HOST_NAME(),		SUSER_SNAME(),		APP_NAME();

	DECLARE @ERROR_MESSAGE		VARCHAR (MAX)  = ERROR_MESSAGE();
	DECLARE @ERROR_SEVERITY		VARCHAR (4000) = ERROR_SEVERITY();
	DECLARE @ERROR_STATE		VARCHAR (4000) = ERROR_STATE();

	DECLARE @Index	INT = CHARINDEX(CHAR(10), @ERROR_MESSAGE);
	DECLARE @Len	INT = LEN(@ERROR_MESSAGE)

	SET @ERROR_MESSAGE = CHAR(13) + CHAR(10) +
		'Msg ' + CONVERT(VARCHAR(20), ERROR_NUMBER()) +
		', Level ' + CONVERT(VARCHAR(20), ERROR_SEVERITY()) +
		', State ' + CONVERT(VARCHAR(20), ERROR_STATE()) +
		', Procedure ' + SCHEMA_NAME() + '.' + ERROR_PROCEDURE() +
		', Line ' + CONVERT(VARCHAR(20), ERROR_LINE()) +
		CHAR(13) + CHAR(10) +
		RIGHT(@ERROR_MESSAGE, @Len - @Index)+
		CHAR(13) + CHAR(10) +
		@Parameters;

--COMMIT TRANSACTION @XACT_POINT;

	RAISERROR(@ERROR_MESSAGE, @ERROR_SEVERITY, @ERROR_STATE) WITH NOWAIT, SETERROR;

END;
GO

ALTER PROCEDURE [calc].[ProcessSubmission]
(
	@MethodologyId		INT = NULL,
	@SubmissionId		INT
)
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @Point_Delete	CHAR(36)	= CONVERT(CHAR(36), NEWID());
	DECLARE @Point_Insert	CHAR(36)	= CONVERT(CHAR(36), NEWID());

	IF (@MethodologyId IS NULL)
	SET @MethodologyId = IDENT_CURRENT('[ante].[Methodology]');
	
	BEGIN TRANSACTION	@Point_Delete;
	
	EXECUTE [calc].[Delete_DataSet]	@MethodologyId,	@SubmissionId;
	EXECUTE [fact].[Delete_DataSet]					@SubmissionId;

	COMMIT TRANSACTION	@Point_Delete;
	BEGIN TRANSACTION	@Point_Insert;

	EXECUTE [fact].[Insert_DataSet]					@SubmissionId;
	EXECUTE [calc].[Insert_DataSet]	@MethodologyId,	@SubmissionId;

	COMMIT TRANSACTION	@Point_Insert;

END;
GO

ALTER PROCEDURE [fact].[Insert_Submission]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	SELECT 1.0 / 0.0;

	INSERT INTO [fact].[Submissions]([SubmissionId], [SubmissionName], [DateBeg], [DateEnd])
	SELECT
		z.[SubmissionId],
		z.[SubmissionName],
		z.[DateBeg],
		z.[DateEnd]
	FROM [stage].[Submissions]		z
	WHERE	z.[SubmissionId] = @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO

EXECUTE [calc].[ProcessSubmission] 1, 1;
GO

SELECT l.* FROM audit.LogError l;

SELECT * FROM calc.Standards s WHERE s.SubmissionId = 1;

--DECLARE	@LoginId	INT;
--DECLARE	@LoginTag	NVARCHAR(254)	= CONVERT(CHAR(36), NEWID());
--DECLARE	@pSalt		VARBINARY(512)	= CRYPT_GEN_RANDOM(512);
--DECLARE	@pWord		VARBINARY(512)	= CRYPT_GEN_RANDOM(512);

--DECLARE	@NameLast	NVARCHAR(254)	= CONVERT(CHAR(36), NEWID());
--DECLARE	@NameFirst	NVARCHAR(254)	= CONVERT(CHAR(36), NEWID());
--DECLARE @RoleId		INT				= [auth].[Return_RoleId](5, NULL);

--SET IDENTITY_INSERT [auth].[Logins] ON;

--EXECUTE	@LoginId = [auth].[Insert_LoginId] @LoginTag;

--SET IDENTITY_INSERT [auth].[Logins] OFF;

--GO



--
--SELECT IDENT_CURRENT('[auth].[Logins]');
--SELECT * FROM [auth].[Logins] ORDER BY LoginId  DESC;

