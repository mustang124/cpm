﻿CREATE PROCEDURE [stage].[Update_StreamDescription]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamDescription		NVARCHAR(256)	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	UPDATE [stage].[StreamDescription]
	SET	[StreamDescription]		= @StreamDescription
	WHERE	[SubmissionId]		= @SubmissionId
		AND	[StreamNumber]		= @StreamNumber
		AND	@StreamDescription	<> '';
		--AND	[StreamDescription]	<> COALESCE(@StreamDescription,	[StreamDescription]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @StreamNumber:'		+ CONVERT(VARCHAR, @StreamNumber))
		+ COALESCE(', @StreamDescription:'	+ CONVERT(VARCHAR, @StreamDescription),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;