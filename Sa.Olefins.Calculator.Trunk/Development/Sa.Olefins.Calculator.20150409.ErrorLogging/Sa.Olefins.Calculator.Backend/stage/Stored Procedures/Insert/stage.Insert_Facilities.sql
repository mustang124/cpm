﻿CREATE PROCEDURE [stage].[Insert_Facilities]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@UnitCount				INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[Facilities]([SubmissionId], [FacilityId], [Unit_Count])
	SELECT
		@SubmissionId,
		@FacilityId,
		@UnitCount
	WHERE	@UnitCount >= 0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @FacilityId:'		+ CONVERT(VARCHAR, @FacilityId))
			+ COALESCE(', @UnitCount:'		+ CONVERT(VARCHAR, @UnitCount),	'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;