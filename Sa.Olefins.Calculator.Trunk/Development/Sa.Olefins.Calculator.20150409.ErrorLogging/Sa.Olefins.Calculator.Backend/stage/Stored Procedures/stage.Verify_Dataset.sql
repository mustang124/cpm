﻿CREATE PROCEDURE [stage].[Verify_DataSet]
(
	@SubmissionId			INT,
	@LoginId				INT			= NULL,
	@LocaleId				SMALLINT	= 1033
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @MethodologyId	INT = IDENT_CURRENT('[ante].[Methodology]');
	DECLARE @tsInserted		DATETIMEOFFSET = SYSDATETIMEOFFSET();

	DECLARE @Lcid			SMALLINT;

	SELECT @Lcid = [l].[Lcid]
	FROM [dim].[NationalLanguageSupport_LookUp] [l]
	WHERE [l].[Lcid] = @LocaleId;

	IF(@Lcid IS NULL)		SET @Lcid = 1033;

	DECLARE	@FeedProdLoss	INT = [dim].[Return_StreamId]('FeedProdLoss');
	DECLARE	@Ethylene		INT = [dim].[Return_StreamId]('Ethylene');;
	DECLARE	@Propylene		INT = [dim].[Return_StreamId]('Propylene');
	DECLARE	@Light			INT = [dim].[Return_StreamId]('Light');
	DECLARE	@LiqLight		INT = [dim].[Return_StreamId]('LiqLight');
	DECLARE	@FeedLiqOther	INT = [dim].[Return_StreamId]('FeedLiqOther');
	DECLARE	@PPFC			INT = [dim].[Return_StreamId]('PPFC');
	DECLARE	@ProdOther		INT = [dim].[Return_StreamId]('ProdOther');

	DECLARE @ConcBenzene	INT = [dim].[Return_StreamId]('ConcBenzene');
	DECLARE @ConcButadiene	INT = [dim].[Return_StreamId]('ConcButadiene');
	DECLARE @ConcEthylene	INT = [dim].[Return_StreamId]('ConcEthylene');
	DECLARE @ConcPropylene	INT = [dim].[Return_StreamId]('ConcPropylene');
	DECLARE @RogEthylene	INT = [dim].[Return_StreamId]('RogEthylene');
	DECLARE @RogPropylene	INT = [dim].[Return_StreamId]('RogPropylene');
	DECLARE @DilEthylene	INT = [dim].[Return_StreamId]('DilEthylene');
	DECLARE @DilPropylene	INT = [dim].[Return_StreamId]('DilPropylene');
	DECLARE @DilButadiene	INT = [dim].[Return_StreamId]('DilButadiene');
	DECLARE @DilBenzene		INT = [dim].[Return_StreamId]('DilBenzene');

	DECLARE @Round			INT	= 2;

	DECLARE	@MessageId		INT;
	DECLARE	@FacilityId		INT;

	EXECUTE [auth].[Insert_JoinLoginSubmission] @LoginId, @SubmissionId;

	BEGIN	--	Submissions

	SET @MessageId	= [dim].[Return_MessageId]('SumbissionDateOrder')

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, s.[DateBeg], 106), '<DATE ERROR>') + '|' + COALESCE(CONVERT(VARCHAR, s.[DateEnd], 106), '<DATE ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Submissions]								s
		ON	s.[SubmissionId] = @SubmissionId
		AND	s.[DateEnd] < s.[DateBeg];

	SET @MessageId	= [dim].[Return_MessageId]('SumbissionNameDuplicate')

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(s.[SubmissionName], '<NAME ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	CROSS JOIN [auth].[JoinPlantSubmission]					jps
	INNER JOIN [stage].[Submissions]						s
		ON	s.[SubmissionId] = jps.[SubmissionId]
	WHERE
		s.[SubmissionId] = @SubmissionId
	GROUP BY 
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		jps.[PlantId],
		s.SubmissionName
	HAVING COUNT(1) > 1;

	SET @MessageId	= [dim].[Return_MessageId]('SumbissionDuration')

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, s.[DateBeg], 106), '<DATE ERROR>') + '|' + COALESCE(CONVERT(VARCHAR, s.[DateEnd], 106), '<DATE ERROR>') + '|' + COALESCE(CONVERT(VARCHAR, s.[_Duration_Days]), '<DURATION ERROR>'), '|')),
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayCorrection], COALESCE(CONVERT(VARCHAR, s.[_Duration_Days]), '<DURATION ERROR>'), '|')),
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Submissions]		s
		ON	s.[SubmissionId] = @SubmissionId
		AND	DATEDIFF(DAY, s.[DateBeg], s.[DateEnd]) < 25;

	END;

	BEGIN	--	Capacity

	SET @MessageId	= [dim].[Return_MessageId]('CapacityProduction');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	LEFT OUTER JOIN [stage].[Capacity]		c
		ON	c.[SubmissionId] = @SubmissionId
	GROUP BY
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection]
	HAVING	SUM(c.StreamDay_MTSD) = 0
		OR 	SUM(c.StreamDay_MTSD) IS NULL;

	SET @MessageId	= [dim].[Return_MessageId]('CapacityEthylene');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Capacity]		c
		ON	c.[SubmissionId] = @SubmissionId
		AND	c.[StreamId] = @Ethylene
		AND(c.[StreamDay_MTSD] = 0
		OR	c.[StreamDay_MTSD] IS NULL);

	SET @MessageId	= [dim].[Return_MessageId]('CapacityPropylene');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Capacity]		c
		ON	c.[SubmissionId] = @SubmissionId
		AND	c.[StreamId] = @Propylene
		AND(c.[StreamDay_MTSD] = 0
		OR	c.[StreamDay_MTSD] IS NULL);

	SET @MessageId	= [dim].[Return_MessageId]('CapacityTotal');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], CONVERT(VARCHAR, COALESCE(e.[StreamDay_MTSD], 0.0) + COALESCE(p.[StreamDay_MTSD], 0.0)) + '|' + CONVERT(VARCHAR, COALESCE(e.[StreamDay_MTSD], 0.0)), '|')),
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayCorrection], CONVERT(VARCHAR, COALESCE(e.[StreamDay_MTSD], 0.0)), '|')),
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	LEFT OUTER JOIN [stage].[Capacity]		e
		ON	e.[SubmissionId] = @SubmissionId
		AND	e.[StreamId] = @Ethylene
	LEFT OUTER JOIN [stage].[Capacity]		p
		ON	e.[SubmissionId] = @SubmissionId
		AND	e.[StreamId] = @Propylene
	WHERE
		COALESCE(p.[StreamDay_MTSD], 0.0) > 0.0;

	END;

	BEGIN	--	Facilities

	SET @MessageId	= [dim].[Return_MessageId]('FacilityTrainCount');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	LEFT OUTER JOIN [stage].[FacilitiesTrains]		f
		ON	f.[SubmissionId] = @SubmissionId
		AND	f.[Train_Count] >= 1
	WHERE	f.[SubmissionId] IS NULL;

	SET @FacilityId	= [dim].[Return_FacilityId]('FracFeed');

	SET @MessageId	= [dim].[Return_MessageId]('FacilityFractionatorAtt');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, f.[Unit_Count]), '0 <COUNT ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Facilities]						f
		ON	f.[SubmissionId]	=  @SubmissionId
		AND	f.[FacilityId]		=  @FacilityId
		AND	f.[Unit_Count]		>  0
	LEFT OUTER JOIN [stage].[FacilitiesFractionator]	ff
		ON	ff.[SubmissionId]	=  f.[SubmissionId]
		AND	ff.[StreamId]		<> 132
	WHERE	ff.[SubmissionId] IS NULL;

	SET @MessageId	= [dim].[Return_MessageId]('FacilityFractionatorAttCount');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[FacilitiesFractionator]		ff
		ON	ff.[SubmissionId]	=  @SubmissionId
		AND	ff.[FacilityId]		=  @FacilityId
		AND	ff.[StreamId]		<> 132
	LEFT OUTER JOIN [stage].[Facilities]			f
		ON	f.[SubmissionId]	=  ff.[SubmissionId]
		AND	f.[Unit_Count]		>  0
	WHERE	f.[SubmissionId]	IS NULL;

	SET @FacilityId	= [dim].[Return_FacilityId]('BoilHP');

	SET @MessageId	= [dim].[Return_MessageId]('FacilityBoilHPAtt');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, f.[Unit_Count]), '0 <COUNT ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Facilities]					f
		ON	f.[SubmissionId]	= @SubmissionId
		AND	f.[FacilityId]		= @FacilityId
		AND	f.[Unit_Count]		> 0
	LEFT OUTER JOIN [stage].[FacilitiesBoilers]		fb
		ON	fb.[SubmissionId]	= f.[SubmissionId]
		AND	fb.[FacilityId]		= f.[FacilityId]
	WHERE	fb.[SubmissionId]	IS NULL;

	SET @MessageId	= [dim].[Return_MessageId]('FacilityBoilHPAttCount');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[FacilitiesFractionator]		fb
		ON	fb.[SubmissionId]	= @SubmissionId
		AND	fb.[FacilityId]		= @FacilityId
	LEFT OUTER JOIN [stage].[Facilities]			f
		ON	f.[SubmissionId]	= fb.[SubmissionId]
		AND	f.[FacilityId]		= fb.[FacilityId]
		AND	f.[Unit_Count]		> 0
	WHERE	f.[SubmissionId] IS NULL;

	SET @FacilityId	= [dim].[Return_FacilityId]('BoilLP');

	SET @MessageId	= [dim].[Return_MessageId]('FacilityBoilLPAtt');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, f.[Unit_Count]), '0 <COUNT ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Facilities]					f
		ON	f.[SubmissionId]	= @SubmissionId
		AND	f.[FacilityId]		= @FacilityId
		AND	f.[Unit_Count]		> 0
	LEFT OUTER JOIN [stage].[FacilitiesBoilers]		fb
		ON	fb.[SubmissionId]	= f.[SubmissionId]
		AND	fb.[FacilityId]		= f.[FacilityId]
	WHERE	fb.[SubmissionId]	IS NULL;

	SET @MessageId	= [dim].[Return_MessageId]('FacilityBoilLPAttCount');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[FacilitiesFractionator]		fb
		ON	fb.[SubmissionId]	= @SubmissionId
		AND	fb.[FacilityId]		= @FacilityId
	LEFT OUTER JOIN [stage].[Facilities]			f
		ON	f.[SubmissionId]	= fb.[SubmissionId]
		AND	f.[FacilityId]		= fb.[FacilityId]
		AND	f.[Unit_Count]		> 0
	WHERE	f.[SubmissionId] IS NULL;

	SET @FacilityId	= [dim].[Return_FacilityId]('ElecGen');

	SET @MessageId	= [dim].[Return_MessageId]('FacilityElecGenAtt');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, f.[Unit_Count]), '0 <COUNT ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Facilities]					f
		ON	f.[SubmissionId]	= @SubmissionId
		AND	f.[FacilityId]		= @FacilityId
		AND	f.[Unit_Count]		> 0
	LEFT OUTER JOIN [stage].[FacilitiesElecGeneration]		fb
		ON	fb.[SubmissionId]	= f.[SubmissionId]
		AND	fb.[FacilityId]		= f.[FacilityId]
	WHERE	fb.[SubmissionId]	IS NULL;

	SET @MessageId	= [dim].[Return_MessageId]('FacilityElecGenAttCount');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[FacilitiesElecGeneration]	fb
		ON	fb.[SubmissionId]	= @SubmissionId
		AND	fb.[FacilityId]		= @FacilityId
		AND	fb.[Capacity_MW]	> 0.0
	LEFT OUTER JOIN [stage].[Facilities]			f
		ON	f.[SubmissionId]	= fb.[SubmissionId]
		AND	f.[FacilityId]		= fb.[FacilityId]
		AND	f.[Unit_Count]		> 0
	WHERE	f.[SubmissionId] IS NULL;

	SET @FacilityId	= [dim].[Return_FacilityId]('TowerPyroGasHT');

	SET @MessageId	= [dim].[Return_MessageId]('FacilityTowerPyroGasHTAtt');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, f.[Unit_Count]), '0 <COUNT ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[Facilities]						f
		ON	f.[SubmissionId]	= @SubmissionId
		AND	f.[FacilityId]		= @FacilityId
		AND	f.[Unit_Count]		> 0
	LEFT OUTER JOIN [stage].[FacilitiesHydroTreater]	fb
		ON	fb.[SubmissionId]		=	f.[SubmissionId]
		AND	fb.[FacilityId]			=	f.[FacilityId]
		AND	fb.HydroTreaterTypeId	<> 5
	WHERE	fb.[SubmissionId]	IS NULL;

	SET @MessageId	= [dim].[Return_MessageId]('FacilityTowerPyroGasHTAttCount');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[FacilitiesHydroTreater]		fb
		ON	fb.[SubmissionId]		=  @SubmissionId
		AND	fb.[FacilityId]			=  @FacilityId
		AND(fb.[Density_SG]			>  0.0
		OR	fb.[HydroTreaterTypeId]	<> 5
		OR	fb.[Pressure_PSIg]		>  0.0
		OR	fb.[Processed_kMT]		>  0.0
		OR	fb.[Processed_Pcnt]		>  0.0
		OR	fb.[Quantity_kBSD]		>  0.0)
	LEFT OUTER JOIN [stage].[Facilities]			f
		ON	f.[SubmissionId]	= fb.[SubmissionId]
		AND	f.[FacilityId]		= fb.[FacilityId]
		AND	f.[Unit_Count]		> 0
	WHERE	f.[SubmissionId] IS NULL;

	END;

	BEGIN	--	Streams

	SET @MessageId	= [dim].[Return_MessageId]('StreamBalance');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], COALESCE(CONVERT(VARCHAR, ROUND(SUM(CASE WHEN b.DescendantOperator = '-' THEN q.[Quantity_kMT] END), @Round)), '0.0 <QUANTITY ERROR>') + '|' + COALESCE(CONVERT(VARCHAR, ROUND(SUM(CASE WHEN b.DescendantOperator = '+' THEN q.[Quantity_kMT] END), @Round)), '0.0 <QUANTITY ERROR>'), '|')),
		msg.[DisplayCorrection],
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[StreamQuantity]			q
		ON	q.[SubmissionId]		= @SubmissionId
		AND	q.[Quantity_kMT]		> 0.0
	INNER JOIN [dim].[Stream_Bridge]			b
		ON	b.[MethodologyId]		= @MethodologyId
		AND	b.[DescendantId]		= q.[StreamId]
		AND	b.[StreamId]			= @FeedProdLoss
	GROUP BY
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection]
	HAVING	ABS((SUM(CASE WHEN b.DescendantOperator = '-' THEN q.[Quantity_kMT] END) - SUM(CASE WHEN b.DescendantOperator = '+' THEN q.[Quantity_kMT] END)))
			/ SUM(CASE WHEN b.DescendantOperator = '-' THEN q.[Quantity_kMT] END)	> 0.1;

	SET @MessageId	= [dim].[Return_MessageId]('StreamComposition');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], l.[StreamName] + COALESCE(' (' + d.[StreamDescription] + ') ', '') + '|' + COALESCE(CONVERT(VARCHAR, ROUND(SUM(c.[Component_WtPcnt]), @Round)), '0.0 <COMPOSITION ERROR>'), '|')),
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayCorrection], l.[StreamName] + COALESCE(' (' + d.[StreamDescription] + ') ', ''), '|')),
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[StreamQuantity]			q
		ON	q.[SubmissionId]		= @SubmissionId
		AND	q.[Quantity_kMT]		> 0.0
	INNER JOIN [dim].[Stream_Bridge]			b
		ON	b.[MethodologyId]		= @MethodologyId
		AND	b.[DescendantId]		= q.[StreamId]
		AND	b.[StreamId]			IN(@Light, @LiqLight, @ProdOther, @PPFC)
	INNER JOIN [dim].[Stream_LookUp]			l
		ON	l.[StreamId]			= q.[StreamId]
	LEFT OUTER JOIN [stage].[StreamDescription]	d
		ON	d.[SubmissionId]		= q.SubmissionId
		AND	d.[StreamNumber]		= q.StreamNumber
	LEFT OUTER JOIN [stage].[StreamComposition]	c
		ON	c.[SubmissionId]		= q.[SubmissionId]
		AND	c.[StreamNumber]		= q.[StreamNumber]
		AND	c.[Component_WtPcnt]	> 0.0
	GROUP BY
		q.[StreamNumber],
		l.[StreamName],
		d.[StreamDescription],
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection]
	HAVING	ABS(100.0 - SUM(c.[Component_WtPcnt])) > 0.05
		OR	COUNT(c.[Component_WtPcnt]) = 0
		OR	COUNT(c.[Component_WtPcnt]) IS NULL;

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], l.[StreamName] + COALESCE(' (' + d.[StreamDescription] + ') ', '') + '|' + COALESCE(CONVERT(VARCHAR, ROUND(SUM(c.[Component_WtPcnt]), @Round)), '0.0 <COMPOSITION ERROR>'), '|')),
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayCorrection], l.[StreamName] + COALESCE(' (' + d.[StreamDescription] + ') ', ''), '|')),
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[StreamQuantity]			q
		ON	q.[SubmissionId]		= @SubmissionId
		AND	q.[Quantity_kMT]		> 0.0
	INNER JOIN [dim].[Stream_Bridge]			b
		ON	b.[MethodologyId]		= @MethodologyId
		AND	b.[DescendantId]		= q.[StreamId]
		AND	b.[StreamId]			= @FeedLiqOther
	INNER JOIN [dim].[Stream_LookUp]			l
		ON	l.[StreamId]			= q.[StreamId]
	LEFT OUTER JOIN [stage].[StreamDescription]	d
		ON	d.[SubmissionId]		= q.SubmissionId
		AND	d.[StreamNumber]		= q.StreamNumber
	LEFT OUTER JOIN [stage].[StreamComposition]	c
		ON	c.[SubmissionId]		= q.[SubmissionId]
		AND	c.[StreamNumber]		= q.[StreamNumber]
		AND	c.[Component_WtPcnt]	> 0.0
	GROUP BY
		q.[StreamNumber],
		l.[StreamName],
		d.[StreamDescription],
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection]
	HAVING	NOT (ABS(100.0 - SUM(c.[Component_WtPcnt])) / 100.0 < 0.05
		OR	SUM(c.[Component_WtPcnt]) = 0.0);

	SET @MessageId	= [dim].[Return_MessageId]('StreamQuantityComp');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], l.[StreamName], '|')),
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayCorrection], l.[StreamName], '|')),
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[StreamComposition]			c
		ON	c.[SubmissionId]		= @SubmissionId
		AND	c.[Component_WtPcnt]	> 0.0
	INNER JOIN [ante].[MapStreamNumberId]			m
		ON	m.[MethodologyId]		= @MethodologyId
		AND	m.[StreamNumber]		= c.[StreamNumber]
		AND	m.[StreamId]			IN(@Light, @LiqLight, @PPFC, @ProdOther)
	INNER JOIN [dim].[Stream_LookUp]				l
		ON	l.[StreamId]			= m.[StreamId]
	LEFT OUTER  JOIN [stage].[StreamQuantity]		q
		ON	q.[SubmissionId]		= c.[SubmissionId]
		AND	q.[StreamNumber]		= c.[StreamNumber]
		AND q.[Quantity_kMT]		> 0.0
	WHERE	q.[Quantity_kMT]		IS NULL
	GROUP BY
		l.[StreamName],
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		msg.[DisplayError],
		msg.[DisplayCorrection]
	HAVING	SUM(c.[Component_WtPcnt]) > 0.05;

	SET	@MessageId	= [dim].[Return_MessageId]('StreamRecoveryPcnt');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], l.[StreamName], '|')),
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayCorrection], l.[StreamName], '|')),
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[StreamQuantity]				q
		ON	q.[SubmissionId]		= @SubmissionId
		AND	q.[StreamId]			IN (@ConcBenzene, @ConcButadiene, @ConcEthylene, @ConcPropylene, @RogEthylene, @RogPropylene, @DilEthylene, @DilPropylene, @DilButadiene, @DilBenzene)
		AND q.[Quantity_kMT]		> 0.0
	INNER JOIN [dim].[Stream_LookUp]				l
		ON	l.[StreamId] = q.[StreamId]
	LEFT OUTER JOIN [stage].[StreamRecovered]		r
		ON	r.[SubmissionId]		= q.[SubmissionId]
		AND	r.[StreamNumber]		= q.[StreamNumber]
		AND	r.[Recovered_WtPcnt]	>= 0.0
		AND	r.[Recovered_WtPcnt]	<= 100.0
	WHERE	r.[Recovered_WtPcnt] IS NULL;

	SET	@MessageId	= [dim].[Return_MessageId]('StreamRecoveryPcntQty');

	INSERT INTO [stage].[Errors_DataSet]([LanguageId], [MessageId], [SubmissionId], [SeverityId], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection], [tsInserted])
	SELECT
		@Lcid,
		@MessageId,
		@SubmissionId,
		msg.[SeverityId],
		msg.[DisplaySection],
		msg.[DisplayLocation],
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayError], l.[StreamName], '|')),
		CONVERT(VARCHAR(256), [audit].[ReplaceParameters](msg.[DisplayCorrection], l.[StreamName], '|')),
		@tsInserted
	FROM [dim].[Return_MessageInLanguage](@Lcid, @MessageId)	msg
	INNER JOIN [stage].[StreamRecovered]			r
		ON	r.[SubmissionId]	= @SubmissionId
		AND	r.Recovered_WtPcnt	> 0.0
	INNER JOIN [ante].[MapStreamNumberId]			m
		ON	m.[MethodologyId]	= @MethodologyId
		AND	m.[StreamNumber]	= r.[StreamNumber]
	INNER JOIN [dim].[Stream_LookUp]				l
		ON	l.[StreamId]		= m.[StreamId]
	LEFT OUTER  JOIN [stage].[StreamQuantity]		q
		ON	q.[SubmissionId]	= r.[SubmissionId]
		AND	q.[StreamNumber]	= r.[StreamNumber]
		AND q.[Quantity_kMT]	>= 0.0
	WHERE	q.[Quantity_kMT]	IS NULL;

	END

	DECLARE	@Error_Count	INT = 0;

	SELECT @Error_Count = COUNT(1)
	FROM [stage].[Errors_DataSet]	e
	WHERE	e.[tsInserted]		= @tsInserted
		AND	e.[SubmissionId]	= @SubmissionId;

	SELECT
		[ErrorId],
		[LanguageId],
		[MessageId],
		[SubmissionId],
		[DisplaySection],
		[DisplayLocation],
		[DisplayError],
		[DisplayCorrection],
		[tsInserted]
	FROM [stage].[Errors_DataSet]	e
	WHERE	e.[tsInserted]		= @tsInserted
		AND	e.[SubmissionId]	= @SubmissionId;

	RETURN @Error_Count;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @LoginId:'		+ CONVERT(VARCHAR, @LoginId))
					+ (', @LocaleId:'		+ CONVERT(VARCHAR, @LocaleId))
					+ (', @Lcid:'			+ CONVERT(VARCHAR, @Lcid))
					+ (', @MessageId:'		+ CONVERT(VARCHAR, @MessageId))
					+ (', @tsInserted:'		+ CONVERT(VARCHAR, @tsInserted));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;