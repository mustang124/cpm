﻿CREATE TABLE [stage].[StreamComposition]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Submissions]						REFERENCES [stage].[Submissions]([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [CR_StreamComposition_StreamNumber]						CHECK([StreamNumber] > 0),
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_StreamComposition_Component_LookUp]					REFERENCES [dim].[Component_LookUp]([ComponentId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Component_WtPcnt]		FLOAT				NOT	NULL	CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MinIncl_0.0]		CHECK([Component_WtPcnt] >= 0.0),
															CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MaxIncl_100.0]	CHECK([Component_WtPcnt] <= 100.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamComposition_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamComposition]			PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamNumber] ASC, [ComponentId] ASC)
);
GO

CREATE TRIGGER [stage].[t_StreamComposition_u]
ON [stage].[StreamComposition]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamComposition]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamComposition].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[StreamComposition].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[stage].[StreamComposition].[ComponentId]	= INSERTED.[ComponentId];

END;
GO