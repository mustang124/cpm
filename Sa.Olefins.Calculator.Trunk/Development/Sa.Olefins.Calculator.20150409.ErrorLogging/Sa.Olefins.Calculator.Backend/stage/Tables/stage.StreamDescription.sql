﻿CREATE TABLE [stage].[StreamDescription]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamDescription_Submissions]		REFERENCES [stage].[Submissions]([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [CR_StreamDescription_StreamNumber]		CHECK([StreamNumber] > 0),

	[StreamDescription]		NVARCHAR(256)			NULL	CONSTRAINT [CL_StreamDescription_StreamDescription]	CHECK([StreamDescription] <> ''),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamDescription]			PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamNumber] ASC)
);
GO;


CREATE TRIGGER [stage].[t_StreamDescription_u]
ON [stage].[StreamDescription]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[StreamDescription]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[StreamDescription].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[stage].[StreamDescription].[StreamNumber]	= INSERTED.[StreamNumber];

END;
GO