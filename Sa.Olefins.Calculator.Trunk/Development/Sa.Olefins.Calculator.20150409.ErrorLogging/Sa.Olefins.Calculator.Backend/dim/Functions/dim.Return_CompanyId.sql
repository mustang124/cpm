﻿CREATE FUNCTION [dim].[Return_CompanyId]
(
	@CompanyTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[CompanyId]
	FROM [dim].[Company_LookUp]	l
	WHERE l.[CompanyTag] = @CompanyTag;

	RETURN @Id;

END;