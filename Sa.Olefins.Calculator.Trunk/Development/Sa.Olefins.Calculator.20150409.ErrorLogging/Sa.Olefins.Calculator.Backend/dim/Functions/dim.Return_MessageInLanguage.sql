﻿CREATE FUNCTION [dim].[Return_MessageInLanguage]
(
	@Lcid			SMALLINT	= 1033,
	@MessageId		INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		mg.[MessageId],
		ma.[SeverityId],
		COALESCE(fl.[DisplayName],			mg.[DisplayName])		[DisplayName],
		COALESCE(fl.[DisplayDetail],		mg.[DisplayDetail])		[DisplayDetail],
		COALESCE(fl.[DisplaySection],		mg.[DisplaySection])	[DisplaySection],
		COALESCE(fl.[DisplayLocation],		mg.[DisplayLocation])	[DisplayLocation],
		COALESCE(fl.[DisplayError],			mg.[DisplayError])		[DisplayError],
		COALESCE(fl.[DisplayCorrection],	mg.[DisplayCorrection])	[DisplayCorrection]
	FROM [dim].[Message_Language]				mg
	INNER JOIN [dim].[Message_Attributes]		ma
		ON	ma.[MessageId]	= mg.[MessageId]
	LEFT OUTER JOIN [dim].[Message_Language]	fl
		ON	fl.[MessageId]	= mg.[MessageId]
		AND	fl.[LanguageId]	= @Lcid
	WHERE	mg.[LanguageId]	= 1033
		AND	mg.[MessageId]	= @MessageId
);