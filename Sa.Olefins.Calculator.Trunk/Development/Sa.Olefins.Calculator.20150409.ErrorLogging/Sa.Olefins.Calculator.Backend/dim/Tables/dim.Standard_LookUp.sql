﻿CREATE TABLE [dim].[Standard_LookUp]
(
	[StandardId]			INT					NOT NULL	IDENTITY (1, 1),

	[StandardTag]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Standard_LookUp_StandardTag]			CHECK ([StandardTag]	<> ''),
															CONSTRAINT [UK_Standard_LookUp_StandardTag]			UNIQUE NONCLUSTERED ([StandardTag]),
	[StandardName]			VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Standard_LookUp_StandardName]		CHECK ([StandardName]	<> ''),
															CONSTRAINT [UK_Standard_LookUp_StandardName]		UNIQUE NONCLUSTERED ([StandardName]),
	[StandardDetail]		VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Standard_LookUp_StandardDetail]		CHECK ([StandardDetail]	<> ''),
															CONSTRAINT [UK_Standard_LookUp_StandardDetail]		UNIQUE NONCLUSTERED ([StandardDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Standard_LookUp_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_LookUp_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_LookUp_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_LookUp_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardTypes_LookUp]		PRIMARY KEY CLUSTERED ([StandardId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Standard_LookUp_u]
ON [dim].[Standard_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Standard_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Standard_LookUp].[StandardId]	= INSERTED.[StandardId];

END;
GO