﻿CREATE TABLE [dim].[ProcessUnit_LookUp]
(
	[ProcessUnitId]			INT					NOT NULL	IDENTITY (1, 1),
	
	[ProcessUnitTag]		VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_ProcessUnit_LookUp_ProcessUnitTag]			CHECK ([ProcessUnitTag]		<> ''),
															CONSTRAINT [UK_ProcessUnit_LookUp_ProcessUnitTag]			UNIQUE NONCLUSTERED ([ProcessUnitTag]),
	[ProcessUnitName]		VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_ProcessUnit_LookUp_ProcessUnitName]			CHECK ([ProcessUnitName]	<> ''),
															CONSTRAINT [UK_ProcessUnit_LookUp_ProcessUnitName]			UNIQUE NONCLUSTERED ([ProcessUnitName]),
	[ProcessUnitDetail]		VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_ProcessUnit_LookUp_ProcessUnitDetail]		CHECK ([ProcessUnitDetail]	<> ''),
															CONSTRAINT [UK_ProcessUnit_LookUp_ProcessUnitDetail]		UNIQUE NONCLUSTERED ([ProcessUnitDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_ProcessUnit_LookUp]			PRIMARY KEY CLUSTERED ([ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [dim].[t_ProcessUnit_LookUp_u]
ON [dim].[ProcessUnit_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnit_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[ProcessUnit_LookUp].[ProcessUnitId]		= INSERTED.[ProcessUnitId];

END;
GO