﻿CREATE TABLE [dim].[Severity_LookUp]
(
	[SeverityId]			INT					NOT NULL	IDENTITY (1, 1),

	[SeverityTag]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Severity_LookUp_SeverityTag]			CHECK ([SeverityTag]	<> ''),
															CONSTRAINT [UK_Severity_LookUp_LanguageTag]			UNIQUE NONCLUSTERED ([SeverityTag]),
	[SeverityName]			NVARCHAR(84)		NOT	NULL	CONSTRAINT [CL_Severity_LookUp_SeverityName]		CHECK ([SeverityName]	<> ''),
															CONSTRAINT [UK_Severity_LookUp_SeverityName]		UNIQUE NONCLUSTERED ([SeverityName]),
	[SeverityDetail]		NVARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Severity_LookUp_SeverityDetail]		CHECK ([SeverityDetail]	<> ''),
															CONSTRAINT [UK_Severity_LookUp_SeverityDetail]		UNIQUE NONCLUSTERED ([SeverityDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Severity_LookUp_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Severity_LookUp_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Severity_LookUp_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Severity_LookUp_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Severity_LookUp]				PRIMARY KEY CLUSTERED ([SeverityId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Severity_LookUp_u]
ON [dim].[Severity_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Severity_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Severity_LookUp].[SeverityId]	= INSERTED.[SeverityId];

END;
GO