﻿CREATE TABLE [dim].[Standard_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Standard_Bridge_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StandardId]			INT					NOT	NULL	CONSTRAINT [FK_Standard_Bridge_StandardID]			REFERENCES [dim].[Standard_LookUp] ([StandardId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Standard_Bridge_Parent_Ancestor]		FOREIGN KEY ([MethodologyId], [StandardId])
																												REFERENCES [dim].[Standard_Parent] ([MethodologyId], [StandardId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_Standard_Bridge_DescendantID]		REFERENCES [dim].[Standard_LookUp] ([StandardId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Standard_Bridge_Parent_Descendant]	FOREIGN KEY ([MethodologyId], [DescendantId])
																												REFERENCES [dim].[Standard_Parent] ([MethodologyId], [StandardId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_Standard_Bridge_DescendantOperator]	DEFAULT ('+')
															CONSTRAINT [FK_Standard_Bridge_DescendantOperator]	REFERENCES [dim].[Operator] ([Operator])								ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Standard_Bridge_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_Bridge_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_Bridge_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Standard_Bridge_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Standard_Bridge]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Standard_Bridge_u]
ON [dim].[Standard_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Standard_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Standard_Bridge].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Standard_Bridge].[StandardId]	= INSERTED.[StandardId]
		AND	[dim].[Standard_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;
GO