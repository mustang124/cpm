﻿CREATE TABLE [dim].[Message_Attributes]
(
	[MessageId]				INT					NOT NULL	CONSTRAINT [FK_Message_Attributes_Message_LookUp]		REFERENCES [dim].[Message_LookUp]([MessageId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[SeverityId]			INT					NOT	NULL	CONSTRAINT [FK_Message_Attributes_Severity_LookUp]		REFERENCES [dim].[Severity_LookUp]([SeverityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Message_Attributes_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_Attributes_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_Attributes_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_Attributes_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Message_Attributes]			PRIMARY KEY CLUSTERED ([MessageId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Message_Attributes_u]
ON [dim].[Message_Attributes]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Message_Attributes]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Message_Attributes].[MessageId]	= INSERTED.[MessageId];

END;
GO