﻿CREATE TABLE [dim].[Factor_LookUp]
(
	[FactorId]				INT					NOT NULL	IDENTITY (1, 1),

	[FactorTag]				VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Factor_LookUp_FactorTag]				CHECK ([FactorTag]		<> ''),
															CONSTRAINT [UK_Factor_LookUp_FactorTag]				UNIQUE NONCLUSTERED ([FactorTag]),
	[FactorName]			VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_Factor_LookUp_FactorName]			CHECK ([FactorName]		<> ''),
															CONSTRAINT [UK_Factor_LookUp_FactorName]			UNIQUE NONCLUSTERED ([FactorName]),
	[FactorDetail]			VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Factor_LookUp_FactorDetail]			CHECK ([FactorDetail]	<> ''),
															CONSTRAINT [UK_Factor_LookUp_FactorDetail]			UNIQUE NONCLUSTERED ([FactorDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Factor_LookUp_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_LookUp_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_LookUp_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_LookUp_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FactorTypes_LookUp]			PRIMARY KEY CLUSTERED ([FactorId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Factor_LookUp_u]
ON [dim].[Factor_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Factor_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Factor_LookUp].[FactorId]	= INSERTED.[FactorId];

END;
GO