﻿CREATE VIEW [calc].[StreamRecovered_EP_Aggregate]
WITH SCHEMABINDING
AS
SELECT
	c.[MethodologyId],
	c.[SubmissionId],
	SUM(c.[_Recovered_Dur_kMT])		[Recovered_Dur_kMT],
	SUM(c.[_Recovered_Ann_kMT])		[Recovered_Ann_kMT],
	COUNT_BIG(*)					[Items]
FROM [calc].[StreamRecovered]		c
WHERE c.[StreamId] IN (43, 44, 50, 57, 60, 69)
GROUP BY
	c.[MethodologyId],
	c.[SubmissionId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_StreamRecovered_EP_Aggregate]
ON [calc].[StreamRecovered_EP_Aggregate]([MethodologyId] DESC, [SubmissionId] ASC);
GO

CREATE INDEX [IX_StreamRecovered_EP_Aggregate]
ON [calc].[StreamRecovered_EP_Aggregate]([MethodologyId] DESC, [SubmissionId] ASC)
INCLUDE([Recovered_Dur_kMT], [Recovered_Ann_kMT])
GO