﻿CREATE VIEW [calc].[StreamComposition_Aggregate]
WITH SCHEMABINDING
AS
SELECT
	c.[MethodologyId],
	c.[SubmissionId],
	c.[StreamNumber],
	c.[StreamId],
	b.[ComponentId],
	SUM(c.[Component_WtPcnt])	[Component_WtPcnt],
	SUM(c.[Component_Dur_kMT])	[Component_Dur_kMT],
	SUM(c.[Component_Ann_kMT])	[Component_Ann_kMT],
	COUNT_BIG(*)				[Items]
FROM [calc].[StreamComposition]			c
INNER JOIN [dim].[Component_Bridge]	b
	ON	b.[DescendantId] = c.[ComponentId]
GROUP BY
	c.[MethodologyId],
	c.[SubmissionId],
	c.[StreamNumber],
	c.[StreamId],
	b.[ComponentId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_StreamComposition_Aggregate]
ON [calc].[StreamComposition_Aggregate]([MethodologyId] DESC, [SubmissionId] ASC, [StreamNumber] ASC, [StreamId] ASC, [ComponentId] ASC);
GO

CREATE INDEX [IX_StreamComposition_Aggregate]
ON [calc].[StreamComposition_Aggregate]([MethodologyId] DESC, [SubmissionId] ASC, [StreamNumber] ASC, [StreamId] ASC, [ComponentId] ASC)
INCLUDE([Component_WtPcnt], [Component_Dur_kMT], [Component_Ann_kMT])
GO