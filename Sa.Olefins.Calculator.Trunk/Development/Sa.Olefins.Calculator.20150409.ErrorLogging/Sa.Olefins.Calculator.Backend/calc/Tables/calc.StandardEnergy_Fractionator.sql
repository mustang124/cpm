﻿CREATE TABLE [calc].[StandardEnergy_Fractionator]
(
	[MethodologyId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_Fractionator_Methodology]							REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]				INT					NOT	NULL	CONSTRAINT [FK_StandardEnergy_Fractionator_Submissions]							REFERENCES [fact].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Throughput_kMT]			FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Fractionator_Throughput_kMT_MinIncl_0.0]			CHECK([Throughput_kMT] >= 0.0),
	[Convert_bblTon]			FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Fractionator_Convert_bblTon_MinIncl_0.0]			CHECK([Convert_bblTon] >= 0.0),
	[Throughput_bbl]			FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Fractionator_Throughput_bbl_MinIncl_0.0]			CHECK([Throughput_bbl] >= 0.0),
	[Throughput_bblDay]			FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Fractionator_Throughput_bblDay_MinIncl_0.0]		CHECK([Throughput_bblDay] >= 0.0),
	[StandardEnergy_MBtuDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Fractionator_StandardEnergy_MBtuDay_MinIncl_0.0]	CHECK([StandardEnergy_MBtuDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_Fractionator_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Fractionator_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Fractionator_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Fractionator_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_Fractionator]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC)
);
GO

CREATE TRIGGER [calc].[t_StandardEnergy_Fractionator_u]
ON [calc].[StandardEnergy_Fractionator]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_Fractionator]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_Fractionator].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_Fractionator].[SubmissionId]	= INSERTED.[SubmissionId];

END;
GO