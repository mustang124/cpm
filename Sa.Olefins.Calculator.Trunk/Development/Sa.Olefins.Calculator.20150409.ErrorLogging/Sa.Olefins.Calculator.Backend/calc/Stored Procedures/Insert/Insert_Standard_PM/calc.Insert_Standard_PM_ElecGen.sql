﻿CREATE PROCEDURE [calc].[Insert_Standard_PM_ElecGen]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId_kEdc			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@StandardId_StdEnergy		INT	= dim.Return_StandardId('StdEnergy');

	DECLARE	@ProcessUnitId				INT	= dim.Return_ProcessUnitId('ElecGen');
	DECLARE	@FactorId					INT	= dim.Return_FactorId('ElecGen');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		f.MethodologyId,
		feg.SubmissionId,
		f.StandardId,
		@ProcessUnitId			[ProcessUnitId],
		f.Coefficient * feg.Capacity_MW * scu.WwCduScu *
		POWER(calc.MaxValue(f.ValueMin, calc.MinValue(f.ValueMax, feg.Capacity_MW * 1000.0 / CONVERT(FLOAT, ft.Unit_Count))), f.Exponent)
		[StandardValue]
	FROM		fact.FacilitiesElecGeneration		feg
	INNER JOIN	fact.Facilities						ft
		ON	ft.SubmissionId		= feg.SubmissionId
		AND	ft.FacilityId		= feg.FacilityId
	INNER JOIN	ante.Factors						f
		ON	f.StandardId		NOT IN (@StandardId_kEdc, @StandardId_StdEnergy)
	INNER JOIN ante.FactorsWwCduScu					scu
		ON	scu.MethodologyId	= f.MethodologyId
		AND	scu.StandardId		= f.StandardId
	WHERE	f.MethodologyId		= @MethodologyId
		AND	f.FactorId			= @FactorId
		AND	feg.SubmissionId	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO