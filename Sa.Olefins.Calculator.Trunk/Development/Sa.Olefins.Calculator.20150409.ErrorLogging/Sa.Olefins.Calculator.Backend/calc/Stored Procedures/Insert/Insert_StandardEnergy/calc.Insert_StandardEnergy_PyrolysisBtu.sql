﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_PyrolysisBtu]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @LiqLight		INT	= [dim].[Return_StreamId]('LiqLight');
	DECLARE @LiqHeavy		INT	= [dim].[Return_StreamId]('LiqHeavy');
	DECLARE @FeedLiqOther	INT	= [dim].[Return_StreamId]('FeedLiqOther');

	DECLARE	@Condensate		INT	= [dim].[Return_StreamId]('Condensate');
	DECLARE	@HeavyNGL		INT	= [dim].[Return_StreamId]('HeavyNGL');
	DECLARE	@NaphthaLt		INT	= [dim].[Return_StreamId]('NaphthaLt');
	DECLARE	@Raffinate		INT	= [dim].[Return_StreamId]('Raffinate');
	DECLARE	@NaphthaFr		INT	= [dim].[Return_StreamId]('NaphthaFr');
	DECLARE	@NaphthaHv		INT	= [dim].[Return_StreamId]('NaphthaHv');
	DECLARE	@Diesel			INT	= [dim].[Return_StreamId]('Diesel');
	DECLARE	@GasOilHv		INT	= [dim].[Return_StreamId]('GasOilHv');
	DECLARE	@GasOilHt		INT	= [dim].[Return_StreamId]('GasOilHt');

	DECLARE	@SgNapthaLt		FLOAT	= 0.71;
	DECLARE	@SgNapthaFr		FLOAT	= 0.75;
	DECLARE	@SgNapthaHv		FLOAT	= 0.83;
	DECLARE	@SgDiesel		FLOAT	= 0.87;

	DECLARE @NicenessQuantity	TABLE
	(
		MethodologyId		INT,
		SubmissionId		INT,
		StreamNumber		INT,
		StreamId			INT,
		Quantity_kMT		FLOAT,
		Niceness_Idx		FLOAT,
		Niceness_kMT		FLOAT,
		PRIMARY KEY CLUSTERED(MethodologyId DESC, SubmissionId DESC, StreamNumber ASC)
	);

	/*	Light	*/
	INSERT INTO @NicenessQuantity(MethodologyId, SubmissionId, StreamNumber, StreamId, Quantity_kMT, Niceness_Idx, Niceness_kMT)
	SELECT
		b.[MethodologyId],
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT],
		COALESCE(SUM(k.[Component_WtPcnt] * nc.[Coefficient]) / 100.0, ns.[EmptyComposition]) / ns.[Divisor],
		COALESCE(SUM(k.[Component_WtPcnt] * nc.[Coefficient]) / 100.0, ns.[EmptyComposition]) / ns.[Divisor] * q.[Quantity_kMT]
	FROM [fact].[StreamQuantity]					q
	INNER JOIN [dim].[Stream_Bridge]				b
		ON	b.[DescendantId]	= q.[StreamId]
		AND	b.[StreamId]		= @LiqLight
	INNER JOIN [ante].[NicenessStreams]				ns
		ON	ns.[StreamId]		= q.[StreamId]
	LEFT OUTER JOIN [calc].[StreamComposition]		k
		ON	k.[SubmissionId]	= q.[SubmissionId]
		AND	k.[StreamNumber]	= q.[StreamNumber]
	LEFT OUTER JOIN [ante].[NicenessComponents]	nc
		ON	nc.[MethodologyId]	= @MethodologyId
		AND	nc.[ComponentId]	= k.[ComponentId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND q.[SubmissionId]	= @SubmissionId
	GROUP BY
		b.[MethodologyId],
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT],
		ns.[EmptyComposition],
		ns.[Divisor];

	/*	Heavy	*/
	INSERT INTO @NicenessQuantity(MethodologyId, SubmissionId, StreamNumber, StreamId, Quantity_kMT, Niceness_Idx, Niceness_kMT)
	SELECT
		b.[MethodologyId],
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT],
		d.[Density_SG] / ns.[Divisor],
		d.[Density_SG] / ns.[Divisor] * q.[Quantity_kMT]
	FROM [fact].[StreamQuantity]					q
	INNER JOIN [dim].[Stream_Bridge]				b
		ON	b.[DescendantId]	= q.[StreamId]
		AND	b.[StreamId]		= @LiqHeavy
	INNER JOIN [fact].[StreamDensity]				d
		ON	d.[SubmissionId]	= q.[SubmissionId]
		AND	d.[StreamNumber]	= q.[StreamNumber]
	INNER JOIN [ante].[NicenessStreams]				ns
		ON	ns.[StreamId]		= q.[StreamId]
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND q.[SubmissionId]	= @SubmissionId;

	/*	Other	*/
	INSERT INTO @NicenessQuantity(MethodologyId, SubmissionId, StreamNumber, StreamId, Quantity_kMT, Niceness_Idx, Niceness_kMT)
	SELECT
		b.[MethodologyId],
		q.[SubmissionId],
		q.[StreamNumber],
		[dim].[Return_StreamId](
			CASE
				WHEN									d.[Density_SG] < @SgNapthaLt	THEN 'NaphthaLt'
				WHEN d.[Density_SG] >= @SgNapthaLt AND	d.[Density_SG] < @SgNapthaFr	THEN 'NaphthaFr'
				WHEN d.[Density_SG] >= @SgNapthaFr AND	d.[Density_SG] < @SgNapthaHv	THEN 'NaphthaHv'
				WHEN d.[Density_SG] >= @SgNapthaHv AND	d.[Density_SG] < @SgDiesel		THEN 'Diesel'
				ELSE																		 'GasOilHv'
			END)		[StreamId],
		q.[Quantity_kMT],
		COALESCE(SUM(k.[Component_WtPcnt] * nc.[Coefficient] / 100.0), ns.[EmptyComposition]) / ns.[Divisor],
		COALESCE(SUM(k.[Component_WtPcnt] * nc.[Coefficient] / 100.0), ns.[EmptyComposition]) / ns.[Divisor] * q.[Quantity_kMT]
	FROM [fact].[StreamQuantity]					q
	INNER JOIN [dim].[Stream_Bridge]				b
		ON	b.[DescendantId]	= q.[StreamId]
		AND	b.[StreamId]		= @FeedLiqOther
	LEFT OUTER JOIN [calc].[StreamComposition]		k
		ON	k.[MethodologyId]	= b.[MethodologyId]
		AND	k.[SubmissionId]	= q.[SubmissionId]
		AND	k.[StreamNumber]	= q.[StreamNumber]
		AND	k.[StreamId]		= q.[StreamId]
	LEFT OUTER JOIN [ante].[NicenessComponents]		nc
		ON	nc.[MethodologyId]	= k.[MethodologyId]
		AND	nc.[ComponentId]	= k.[ComponentId]

	INNER JOIN [fact].[StreamDensity]				d
		ON	d.[SubmissionId]	= q.[SubmissionId]
		AND	d.[StreamNumber]	= q.[StreamNumber]
	INNER JOIN [ante].[NicenessStreams]				ns
		ON	ns.[MethodologyId]	= b.[MethodologyId]
		AND	ns.[StreamId]		= 
			[dim].[Return_StreamId](
				CASE
					WHEN									d.[Density_SG] < @SgNapthaLt	THEN 'NaphthaLt'
					WHEN d.[Density_SG] >= @SgNapthaLt AND	d.[Density_SG] < @SgNapthaFr	THEN 'NaphthaFr'
					WHEN d.[Density_SG] >= @SgNapthaFr AND	d.[Density_SG] < @SgNapthaHv	THEN 'NaphthaHv'
					WHEN d.[Density_SG] >= @SgNapthaHv AND	d.[Density_SG] < @SgDiesel		THEN 'NaphthaHv'
					ELSE																		 'NaphthaHv'
				END)
	WHERE	b.[MethodologyId]	= @MethodologyId
		AND q.[SubmissionId]	= @SubmissionId
	GROUP BY
		b.[MethodologyId],
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT],
		d.[Density_SG],
		ns.[EmptyComposition],
		ns.[Divisor];

	DECLARE @MapNicenessStreams	TABLE
	(
		MethodologyId	INT,
		AggStreamId		INT,
		MapStreamId		INT,
		PRIMARY KEY CLUSTERED (MethodologyId DESC, AggStreamId ASC, MapStreamId ASC)
	);

	INSERT INTO @MapNicenessStreams(MethodologyId, AggStreamId, MapStreamId)
	VALUES
		(@MethodologyId, @Condensate,	@Condensate),
		(@MethodologyId, @Condensate,	@HeavyNGL),
		(@MethodologyId, @NaphthaLt,	@NaphthaLt),
		(@MethodologyId, @NaphthaLt,	@Raffinate),
		(@MethodologyId, @NaphthaFr,	@NaphthaFr),
		(@MethodologyId, @NaphthaHv,	@NaphthaHv),
		(@MethodologyId, @Diesel,		@Diesel),
		(@MethodologyId, @GasOilHv,		@GasOilHv),
		(@MethodologyId, @GasOilHt,		@GasOilHt);

	DECLARE @Niceness TABLE
	(
		MethodologyId		INT,
		SubmissionId		INT,
		StreamId			INT,
		Niceness_Idx		FLOAT,
		Quantity_kMT		FLOAT,
		PRIMARY KEY CLUSTERED(MethodologyId DESC, SubmissionId DESC, StreamId ASC)
	);

	INSERT INTO @Niceness(MethodologyId, SubmissionId, StreamId, Niceness_Idx, Quantity_kMT)
	SELECT
		q.MethodologyId,
		q.SubmissionId,
		m.AggStreamId,
		SUM(q.Niceness_kMT) / SUM(q.Quantity_kMT),
		SUM(q.Quantity_kMT)
	FROM @NicenessQuantity				q
	INNER JOIN @MapNicenessStreams		m
		ON	m.MethodologyId = q.MethodologyId
		AND	m.MapStreamId	= q.StreamId
	GROUP BY
		q.MethodologyId,
		q.SubmissionId,
		m.AggStreamId;

	DECLARE @HistMin	FLOAT = 0.8;
	DECLARE @HistMax	FLOAT = 1.2;

	DECLARE @Quantity TABLE
	(
		MethodologyId		INT			NOT	NULL,
		SubmissionId		INT			NOT	NULL,
		StreamId			INT			NOT	NULL,
		Quantity_kMT		FLOAT			NULL,
		Niceness_AdjkMT		FLOAT		NOT	NULL,
		Niceness_ScaledkMT	FLOAT			NULL,
		PRIMARY KEY CLUSTERED(MethodologyId DESC, SubmissionId DESC, StreamId ASC)
	);

	INSERT INTO @Quantity(MethodologyId, SubmissionId, StreamId, Quantity_kMT, Niceness_AdjkMT, Niceness_ScaledkMT)
	SELECT
		n.[MethodologyId],
		n.[SubmissionId],
		n.[StreamId],
		n.[Quantity_kMT],
		n.[Quantity_kMT] / CASE
			WHEN n.[Niceness_Idx] < @HistMin * r.[HistoricalMin] THEN 1.0
			WHEN n.[Niceness_Idx] > @HistMax * r.[HistoricalMax] THEN 1.0
			ELSE (r.[YieldMax] - r.[YieldMin]) / (r.[HistoricalMax] - r.[HistoricalMin]) * (n.[Niceness_Idx] - r.[HistoricalMin]) + r.[YieldMin]
		END [Niceness_AdjkMT],
		n.[Quantity_kMT] / CASE
			WHEN n.[Niceness_Idx] < @HistMin * r.[HistoricalMin] THEN 1.0
			WHEN n.[Niceness_Idx] > @HistMax * r.[HistoricalMax] THEN 1.0
			ELSE (r.[EnergyMax] - r.[EnergyMin]) / (r.[HistoricalMax] - r.[HistoricalMin]) * (n.[Niceness_Idx] - r.[HistoricalMin]) + r.[EnergyMin]
		END [Niceness_ScaledkMT]
	FROM @Niceness						n
	INNER JOIN [ante].[NicenessRanges]	r
		ON	r.[MethodologyId] = n.[MethodologyId]
		AND	r.[StreamId] = n.[StreamId];

	INSERT INTO @Quantity(MethodologyId, SubmissionId, StreamId, Quantity_kMT, Niceness_AdjkMT, Niceness_ScaledkMT)
	SELECT
		l.[MethodologyId],
		l.[SubmissionId],
		l.[StreamId],
		l.[ContainedFeed_Dur_kMT],
		l.[ContainedFeed_Dur_kMT],
		l.[ContainedFeed_Dur_kMT]
	FROM [calc].[ContainedLightFeed]	l
	WHERE	l.[MethodologyId]	= @MethodologyId
		AND	l.[SubmissionId]	= @SubmissionId;

	DECLARE	@CH4		INT = [dim].[Return_ComponentId]('CH4');
	DECLARE @Light		INT	= [dim].[Return_StreamId]('Light');
	DECLARE @Methane	INT	= [dim].[Return_StreamId]('Methane');

	INSERT INTO @Quantity(MethodologyId, SubmissionId, StreamId, Quantity_kMT, Niceness_AdjkMT, Niceness_ScaledkMT)
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		@Methane,
		NULL,
		SUM(c.[Component_Dur_kMT]),
		NULL
	FROM [calc].[StreamComposition]		c
	INNER JOIN [dim].[Stream_Bridge]	b
		ON	b.[MethodologyId]	= c.[MethodologyId]
		AND	b.[DescendantId]	= c.[StreamId]
		AND	b.[StreamId]		= @Light
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
		AND	c.[ComponentId]		= @CH4
	GROUP BY
		c.[MethodologyId],
		c.[SubmissionId];

	INSERT INTO [calc].[StandardEnergy_PyrolysisBtu]([MethodologyId], [SubmissionId], [Energy_kBtu], [HvcYieldDivisor_kLbDay], [StandardEnergy_kBtuLbDay])
	SELECT
		t.[MethodologyId],
		t.[SubmissionId],
		SUM(t.EnergyNumerator)	+ i.Energy_Intercept,
		SUM(t.Divisor)			+ i.HvcYield_Intercept,
		(SUM(t.EnergyNumerator)	+ i.Energy_Intercept) / (SUM(t.Divisor)	+ i.HvcYield_Intercept)
	FROM (
		SELECT
			q.[MethodologyId],
			q.[SubmissionId],
			q.[StreamId],

			q.[Niceness_ScaledkMT] / SUM(q.[Niceness_ScaledkMT]) OVER() * m.[Energy_Coefficient]		[EnergyNumerator],

			CASE WHEN  q.[StreamId] <> @Methane
				THEN q.[Niceness_AdjkMT] / SUM(CASE WHEN q.[StreamId] <> @Methane THEN q.[Niceness_AdjkMT] END ) OVER()
				ELSE q.[Niceness_AdjkMT] / SUM(q.[Quantity_kMT]) OVER()
				END * m.[HvcYield_Coefficient]															[Divisor]

		FROM @Quantity	q
		INNER JOIN [ante].[Factors_EiiCoefficients]	m
			ON	m.[MethodologyId]	= q.[MethodologyId]
			AND	m.[StreamId]		= q.[StreamId]
		) t
	INNER JOIN [ante].[Factors_EiiIntercepts] i
		ON	i.[MethodologyId] = t.MethodologyId
	GROUP BY
		t.[MethodologyId],
		t.[SubmissionId],
		i.Energy_Intercept, 
		i.HvcYield_Intercept
	HAVING	 SUM(t.EnergyNumerator) IS NOT NULL
		 AND SUM(t.Divisor)			IS NOT NULL;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO