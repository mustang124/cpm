﻿CREATE PROCEDURE [calc].[Insert_StreamRecovered]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @SuppTot			INT = [dim].[Return_StreamId]('SuppTot')

	INSERT INTO [calc].[StreamRecovered]([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [Quantity_Dur_kMT], [Quantity_Ann_kMT], [Recovered_WtPcnt])
	SELECT
		@MethodologyId,
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT],
		q.[Quantity_kMT] * s.[_Duration_Multiplier],
		COALESCE(r.[Recovered_WtPcnt], 0.0)
	FROM [fact].[StreamQuantity]					q
	INNER JOIN [dim].[Stream_Bridge]				b
		ON	b.[MethodologyId]	= @MethodologyId
		AND	b.[DescendantId]	= q.[StreamId]
		AND	b.[StreamId]		= @SuppTot
	LEFT OUTER JOIN [fact].[StreamRecovered]		r
		ON	q.[SubmissionId] = r.[SubmissionId]
		AND	q.[StreamNumber] = r.[StreamNumber]
	INNER JOIN [fact].[Submissions]					s
		ON	s.[SubmissionId] = q.[SubmissionId]
	WHERE	q.[SubmissionId] = @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO