﻿CREATE PROCEDURE [calc].[Insert_Standard_StdEnergy]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @StandardId		INT = [dim].[Return_StandardId]('StdEnergy');
	DECLARE @ProcessUnitId	INT;

	SET @ProcessUnitId = [dim].[Return_ProcessUnitId]('Fresh');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		e.[MethodologyId],
		e.[SubmissionId],
		@StandardId,
		@ProcessUnitId,
		e.[StandardEnergy_MBtuDay]
	FROM [calc].[StandardEnergy_StdConfig]			e WITH (NOEXPAND)
	WHERE	e.[MethodologyId]	= @MethodologyId
		AND	e.[SubmissionId]	= @SubmissionId;

	SET @ProcessUnitId = [dim].[Return_ProcessUnitId]('Supp');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		e.[MethodologyId],
		e.[SubmissionId],
		@StandardId,
		@ProcessUnitId,
		e.[StandardEnergy_MBtuDay]
	FROM [calc].[StandardEnergy_Supplemental]		e
	WHERE	e.[MethodologyId]	= @MethodologyId
		AND	e.[SubmissionId]	= @SubmissionId;

	SET @ProcessUnitId = [dim].[Return_ProcessUnitId]('FeedPrep');	-- Fractionator

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		e.[MethodologyId],
		e.[SubmissionId],
		@StandardId,
		@ProcessUnitId,
		e.[StandardEnergy_MBtuDay]
	FROM [calc].[StandardEnergy_Fractionator] e
	WHERE	e.[MethodologyId]	= @MethodologyId
		AND	e.[SubmissionId]	= @SubmissionId;

	SET @ProcessUnitId = [dim].[Return_ProcessUnitId]('PyroGasHydroTreat');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		e.[MethodologyId],
		e.[SubmissionId],
		@StandardId,
		@ProcessUnitId,
		e.[StandardEnergy_MBtuDay]
	FROM [calc].[StandardEnergy_HydroTreater]		e
	WHERE	e.[MethodologyId]	= @MethodologyId
		AND	e.[SubmissionId]	= @SubmissionId;

	SET @ProcessUnitId = [dim].[Return_ProcessUnitId]('HydroPur');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		e.[MethodologyId],
		e.[SubmissionId],
		@StandardId,
		@ProcessUnitId,
		e.[StandardEnergy_MBtuDay]
	FROM [calc].[StandardEnergy_HydroPur]			e
	WHERE	e.[MethodologyId]	= @MethodologyId
		AND	e.[SubmissionId]	= @SubmissionId;

	SET @ProcessUnitId = NULL;

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		e.[MethodologyId],
		e.[SubmissionId],
		@StandardId,
		e.[ProcessUnitId],
		- e.[StandardEnergy_MBtuDay]
	FROM [calc].[StandardEnergy_Reduction]			e
	WHERE	e.[MethodologyId]	= @MethodologyId
		AND	e.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO
