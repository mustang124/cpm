﻿CREATE PROCEDURE [calc].[Insert_FacilitiesFractionator]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [calc].[FacilitiesFractionator]([MethodologyId], [SubmissionId], [FacilityId], [FeedStock_BSD])
	SELECT
		@MethodologyId,
		f.[SubmissionId],
		f.[FacilityId],
		f.[_Quantity_BSD]
	FROM [fact].[FacilitiesFractionator]	f
	WHERE	f.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO