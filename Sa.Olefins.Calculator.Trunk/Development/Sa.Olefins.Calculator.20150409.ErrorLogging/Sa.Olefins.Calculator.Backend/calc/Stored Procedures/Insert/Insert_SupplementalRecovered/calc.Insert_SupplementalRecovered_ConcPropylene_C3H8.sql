﻿CREATE PROCEDURE [calc].[Insert_SupplementalRecovered_ConcPropylene_C3H8]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@ConcPropylene		INT	= [dim].[Return_StreamId]('ConcPropylene');
	DECLARE	@C3H8				INT	= [dim].[Return_ComponentId]('C3H8');

	INSERT INTO [calc].[SupplementalRecovered]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Recovered_Dur_kMT], [Recovered_Ann_kMT])
	SELECT
		x.[MethodologyId],
		x.[SubmissionId],
		@ConcPropylene,
		@C3H8,
		--calc.MinValue(p.[ContainedProduct_Dur_kMT], r.[Quantity_Dur_kMT] * (100.0 - r.[Recovered_WtPcnt]) / 100.0),
		--calc.MinValue(p.[ContainedProduct_Ann_kMT], r.[Quantity_Ann_kMT] * (100.0 - r.[Recovered_WtPcnt]) / 100.0)
		r.[Quantity_Dur_kMT] * (100.0 - r.[Recovered_WtPcnt]) / 100.0,
		r.[Quantity_Ann_kMT] * (100.0 - r.[Recovered_WtPcnt]) / 100.0
	FROM (VALUES
		(@MethodologyId, @SubmissionId)
		)												x ([MethodologyId], [SubmissionId])
	LEFT OUTER JOIN	[calc].[ContainedProduct_Composite]	p WITH (NOEXPAND)
		ON	p.[MethodologyId]	= x.[MethodologyId]
		AND	p.[SubmissionId]	= x.[SubmissionId]
		AND	p.[ComponentId]		= @C3H8
	LEFT OUTER JOIN	[calc].[StreamRecovered]			r
		ON	r.[MethodologyId]	= x.[MethodologyId]
		AND	r.[SubmissionId]	= x.[SubmissionId]
		AND	r.[StreamId]		= @ConcPropylene
	WHERE	calc.MinValue(p.[ContainedProduct_Dur_kMT], r.[Quantity_Dur_kMT] * (100.0 - r.[Recovered_WtPcnt]) / 100.0) > 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO