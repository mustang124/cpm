﻿CREATE PROCEDURE [calc].[Insert_SupplementalRecovered_ConcPropylene_C3H6]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@ConcPropylene		INT	= [dim].[Return_StreamId]('ConcPropylene');
	DECLARE	@C3H6				INT	= [dim].[Return_ComponentId]('C3H6');

	INSERT INTO [calc].[SupplementalRecovered]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Recovered_Dur_kMT], [Recovered_Ann_kMT])
	SELECT
		x.[MethodologyId],
		x.[SubmissionId],
		@ConcPropylene,
		@C3H6,
		COALESCE(c.[Quantity_Dur_kMT], 0.0) - COALESCE(r.[Recovered_Dur_kMT], 0.0),
		COALESCE(c.[Quantity_Ann_kMT], 0.0) - COALESCE(r.[Recovered_Ann_kMT], 0.0)
	FROM (VALUES
		(@MethodologyId, @SubmissionId)
		)												x ([MethodologyId], [SubmissionId])
	LEFT OUTER JOIN [calc].[SupplementalRecovered]		r
		ON	r.[MethodologyId]	= x.[MethodologyId]
		AND	r.[SubmissionId]	= x.[SubmissionId]
		AND	r.[StreamId]		= @ConcPropylene
	LEFT OUTER JOIN	[calc].[StreamRecovered]			c
		ON	c.[MethodologyId]	= x.[MethodologyId]
		AND	c.[SubmissionId]	= x.[SubmissionId]
		AND	c.[StreamId]		= @ConcPropylene
	WHERE COALESCE(c.[Quantity_Dur_kMT], 0.0) - COALESCE(r.[Recovered_Dur_kMT], 0.0) > 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO