﻿CREATE PROCEDURE [calc].[Insert_SupplementalRecovered_ClientSpec_Balance]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [calc].[SupplementalRecovered]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Recovered_Dur_kMT], [Recovered_Ann_kMT])
	SELECT
		x.[MethodologyId],
		x.[SubmissionId],
		m.[RecStreamId],
		m.[BalComponentId],
		COALESCE(q.[Quantity_kMT], 0.0) + COALESCE(r.[Quantity_Dur_kMT], 0.0) - COALESCE(r.[_Recovered_Dur_kMT], 0.0),
		(COALESCE(q.[Quantity_kMT], 0.0) + COALESCE(r.[Quantity_Dur_kMT], 0.0) - COALESCE(r.[_Recovered_Dur_kMT], 0.0)) * s.[_Duration_Multiplier]
	FROM (VALUES
		(@MethodologyId, @SubmissionId)
		)												x ([MethodologyId], [SubmissionId])
	INNER JOIN [fact].[Submissions]						s
		ON	s.[SubmissionId]	= x.[SubmissionId]
	INNER JOIN [ante].[MapRecycledBalance]				m
		ON	m.[MethodologyId]	= x.[MethodologyId]
	LEFT OUTER JOIN [fact].[StreamQuantity]				q
		ON	q.[SubmissionId]	= x.[SubmissionId]
		AND	q.[StreamId]		= m.[BalStreamId]
	LEFT OUTER JOIN [calc].[StreamRecovered]			r
		ON	r.[MethodologyId]	= x.[MethodologyId]
		AND	r.[SubmissionId]	= x.[SubmissionId]
		AND	r.[StreamId]		= m.[RecStreamId]
	WHERE COALESCE(q.[Quantity_kMT], 0.0) + COALESCE(r.[Quantity_Dur_kMT], 0.0) - COALESCE(r.[_Recovered_Dur_kMT], 0.0) > 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO