﻿PRINT 'INSERT INTO [ante].[MapFactorStream]([MethodologyId], [StreamId], [FactorId])';

INSERT INTO [ante].[MapFactorStream]([MethodologyId], [StreamId], [FactorId])
SELECT t.[MethodologyId], t.[StreamId], t.[FactorId]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('PropyleneCG'), dim.Return_FactorId('PropyleneAdjCG')),
	(@MethodologyId, dim.Return_StreamId('PropyleneRG'), dim.Return_FactorId('PropyleneAdjRG'))
	)t([MethodologyId], [StreamId], [FactorId]);