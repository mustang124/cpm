﻿PRINT 'INSERT INTO [ante].[MapFactorFeedClass]([MethodologyId], [FeedClassId], [FactorId])';

INSERT INTO [ante].[MapFactorFeedClass]([MethodologyId], [FeedClassId], [FactorId])
SELECT t.[MethodologyId], t.[FeedClassId], t.[FactorId]
FROM (VALUES
	(@MethodologyId, dim.Return_FeedClassId('1'), dim.Return_FactorId('Ethane')),
	(@MethodologyId, dim.Return_FeedClassId('2'), dim.Return_FactorId('LPG')),
	(@MethodologyId, dim.Return_FeedClassId('3'), dim.Return_FactorId('Liquid')),
	(@MethodologyId, dim.Return_FeedClassId('4'), dim.Return_FactorId('Liquid')),

	(@MethodologyId, dim.Return_FeedClassId('1'), dim.Return_FactorId('SuppEthane')),
	(@MethodologyId, dim.Return_FeedClassId('2'), dim.Return_FactorId('SuppLPG')),
	(@MethodologyId, dim.Return_FeedClassId('3'), dim.Return_FactorId('SuppLiquid')),
	(@MethodologyId, dim.Return_FeedClassId('4'), dim.Return_FactorId('SuppLiquid')),

	(@MethodologyId, dim.Return_FeedClassId('1'), dim.Return_FactorId('CompEthane')),
	(@MethodologyId, dim.Return_FeedClassId('2'), dim.Return_FactorId('CompLPG')),
	(@MethodologyId, dim.Return_FeedClassId('3'), dim.Return_FactorId('CompLiquid')),
	(@MethodologyId, dim.Return_FeedClassId('4'), dim.Return_FactorId('CompLiquid'))
	)t([MethodologyId], [FeedClassId], [FactorId]);