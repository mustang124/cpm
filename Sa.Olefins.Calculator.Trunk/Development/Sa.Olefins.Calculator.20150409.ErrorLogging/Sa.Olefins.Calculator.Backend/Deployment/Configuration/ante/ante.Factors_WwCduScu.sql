﻿PRINT 'INSERT INTO [ante].[FactorsWwCduScu]([MethodologyId], [StandardId], [WwCduScu])';

INSERT INTO [ante].[FactorsWwCduScu]([MethodologyId], [StandardId], [WwCduScu])
SELECT t.[MethodologyId], t.[StandardId], t.[WwCduScu]
FROM (VALUES
	(@MethodologyId, dim.Return_StandardId('PersMaint'),	 0.597),
	(@MethodologyId, dim.Return_StandardId('PersNonMaint'),	 0.5932),
	(@MethodologyId, dim.Return_StandardId('Mes'),			28.17),
	(@MethodologyId, dim.Return_StandardId('NonEnergy'),	64.25)
	)t([MethodologyId], [StandardId], [WwCduScu]);