﻿PRINT 'INSERT INTO [ante].[MapComponentRecycle]([MethodologyId], [StreamId], [ComponentId])';

INSERT INTO [ante].[MapComponentRecycle]([MethodologyId], [StreamId], [ComponentId])
SELECT t.[MethodologyId], t.[StreamId], t.[ComponentId]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('RogEthane'), dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_StreamId('DilEthane'), dim.Return_ComponentId('C2H6')),
	(@MethodologyId, dim.Return_StreamId('RogPropane'), dim.Return_ComponentId('C3H8')),
	(@MethodologyId, dim.Return_StreamId('DilPropane'), dim.Return_ComponentId('C3H8')),
	(@MethodologyId, dim.Return_StreamId('DilButane'), dim.Return_ComponentId('C4H10')),
	(@MethodologyId, dim.Return_StreamId('RogC4Plus'), dim.Return_ComponentId('C4H10'))
	)t([MethodologyId], [StreamId], [ComponentId]);