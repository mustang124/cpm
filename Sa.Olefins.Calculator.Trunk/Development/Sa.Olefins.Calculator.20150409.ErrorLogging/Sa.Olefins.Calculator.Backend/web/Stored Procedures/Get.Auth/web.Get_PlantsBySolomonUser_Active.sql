﻿CREATE PROCEDURE [web].[Get_PlantsBySolomonUser_Active]
(
	@UserId		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		pbl.[JoinId],
		pbl.[PlantId],
		pbl.[LoginId],
		[PlantName] = pbl.[CompanyName] + ' - ' + pbl.[PlantName],
		pbl.[LoginTag]
	FROM [auth].[Get_PlantsByLogin_Active](@UserId)	pbl
	ORDER BY
		pbl.[CompanyName]	ASC,
		pbl.[PlantName]		ASC;

END;