﻿CREATE PROCEDURE [web].[Get_FractionatorFeedStock]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		l.[StreamId]	[FeedStockId],
		l.[StreamName]	[FeedStockDesc]
	FROM [dim].[Stream_LookUp]	l
	WHERE
		l.[StreamId] IN (132, 8, 11, 19, 24, 26)
	ORDER BY l.[StreamName]	 ASC;

END;