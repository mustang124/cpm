﻿CREATE TABLE [xls].[FacilitiesBoilers]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_FacilitiesBoilers_Refnum]					CHECK([Refnum] <> ''),
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesBoilers_Facility_LookUp]			REFERENCES [dim].[Facility_LookUp]([FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Rate_kLbHr]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesBoilers_Rate_kLbHr_MinIncl_0.0]	CHECK ([Rate_kLbHr] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesBoilers_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesBoilers]			PRIMARY KEY CLUSTERED ([Refnum] DESC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [xls].[t_FacilitiesBoilers_u]
ON [xls].[FacilitiesBoilers]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesBoilers]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesBoilers].[Refnum]		= INSERTED.[Refnum]
		AND	[xls].[FacilitiesBoilers].[FacilityId]	= INSERTED.[FacilityId];

END;
GO