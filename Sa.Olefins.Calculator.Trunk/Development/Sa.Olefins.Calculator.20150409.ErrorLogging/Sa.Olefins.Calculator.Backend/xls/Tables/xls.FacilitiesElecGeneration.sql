﻿CREATE TABLE [xls].[FacilitiesElecGeneration]
(
	[Refnum]				VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_FacilitiesElecGeneration_Refnum]						CHECK([Refnum] <> ''),
	[FacilityId]			INT					NOT	NULL	CONSTRAINT [FK_FacilitiesElecGeneration_Facility_LookUp]			REFERENCES [dim].[Facility_LookUp]([FacilityId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Capacity_MW]			FLOAT				NOT	NULL	CONSTRAINT [CR_FacilitiesElecGeneration_Capacity_MW_MinIncl_0.0]	CHECK([Capacity_MW] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FacilitiesElecGeneration_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FacilitiesElecGeneration]	PRIMARY KEY CLUSTERED ([Refnum] DESC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [xls].[t_FacilitiesElecGeneration_u]
ON [xls].[FacilitiesElecGeneration]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesElecGeneration]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesElecGeneration].[Refnum]		= INSERTED.[Refnum]
		AND	[xls].[FacilitiesElecGeneration].[FacilityId]	= INSERTED.[FacilityId];

END;
GO