﻿CREATE TABLE [fact].[StreamDescription]
(
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_StreamDescription_Submissions]		REFERENCES [fact].[Submissions]([SubmissionId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamNumber]			INT					NOT	NULL	CONSTRAINT [FK_StreamDescription_StreamQuantity]
															FOREIGN KEY([SubmissionId], [StreamNumber])			REFERENCES [fact].[StreamQuantity]([SubmissionId], [StreamNumber])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[StreamDescription]		NVARCHAR(256)		NOT	NULL	CONSTRAINT [CL_StreamDescription_StreamDescription]	CHECK([StreamDescription] <> ''),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StreamDescription_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StreamDescription]			PRIMARY KEY CLUSTERED ([SubmissionId] ASC, [StreamNumber] ASC)
);
GO;

CREATE TRIGGER [fact].[t_StreamDescription_u]
ON [fact].[StreamDescription]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamDescription]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamDescription].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[fact].[StreamDescription].[StreamNumber]	= INSERTED.[StreamNumber];

END;
GO