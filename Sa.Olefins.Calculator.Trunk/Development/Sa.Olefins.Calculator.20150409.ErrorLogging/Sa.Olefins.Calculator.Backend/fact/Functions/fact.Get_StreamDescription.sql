﻿CREATE FUNCTION [fact].[Get_StreamDescription]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		a.[SubmissionId],
		a.[StreamNumber],
		a.[StreamDescription]
	FROM [fact].[StreamDescription]		a
	WHERE	a.[SubmissionId] = @SubmissionId
);