﻿CREATE FUNCTION [fact].[Get_FacilitiesTrains]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		t.[SubmissionId],
		t.[Train_Count]
	FROM [fact].[FacilitiesTrains]			t
	WHERE	t.[SubmissionId] = @SubmissionId
);