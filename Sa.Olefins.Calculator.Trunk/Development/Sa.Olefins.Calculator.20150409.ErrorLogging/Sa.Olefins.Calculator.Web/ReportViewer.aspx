﻿<%@ Page Language="C#" MasterPageFile="~/OCalc.Master" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="OlefinsCalculator.ReportViewer" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="100%" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" style="overflow-y: hidden;" ZoomMode="PageWidth" SizeToReportContent="True">
        <LocalReport ReportPath="Report.rdlc">            
        </LocalReport>
    </rsweb:ReportViewer>
    </div>

    <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>

</asp:Content>
