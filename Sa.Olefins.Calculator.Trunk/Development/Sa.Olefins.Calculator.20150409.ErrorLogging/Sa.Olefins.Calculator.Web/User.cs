﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OlefinsCalculator
{
    public class User
    {
        public int _userID { get; set; }
        public int _securityLevelID { get; set; }
        public int _companyID { get; set; }
        public string _companyName { get; set; }
        public string _firstName { get; set; }
        public string _lastName { get; set; }
        public string _username { get; set; }
    }
}