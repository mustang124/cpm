﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace OlefinsCalculator
{

    public partial class Video : System.Web.UI.MasterPage
    {
        //User user = new User();
        //public IFormsAuthenticationService FormsService { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            //OlefinsCalculator.User user = (OlefinsCalculator.User)Session["User"];
            //if (user != null)
            //{
            //    lblMenuHeader.Visible = true;

            //    if (user._securityLevelID <= 5)
            //        //lnkbtnAdministration.Visible = true;
            //        divadmin.Visible = true;
            //    if (user._securityLevelID <= 1)
            //        lnkbtnAdministration.PostBackUrl = "~/CompanyAdmin.aspx";
            //}
            ////this.form1.DefaultButton = LoginUser.FindControl("LoginButton").UniqueID;
            //pnlLogin.DefaultButton = LoginUser.FindControl("LoginButton").UniqueID;
        }
        public interface IFormsAuthenticationService
	    {
	        void SignIn(string userName, bool createPersistentCookie);

            void SignOut();
	    }

        public class FormsAuthenticationService : IFormsAuthenticationService
        {
            public void SignIn(string userName, bool createPersistentCookie)
            {
                //Login user = new LoginHelper().GetUser(userName);
                //System.Web.HttpContext.Current.Session["UserInfo"] = user;
                //System.Web.HttpContext.Current.Session["IsAdmin"] = (user.SecurityLevel == 2 || user.SecurityLevel >= 32000);

                //using (var db = new RAMEntities())
                //{
                //    System.Web.HttpContext.Current.Session["CompanySID"] = db.CurrentStudyForUsers.FirstOrDefault(u => u.UserID.Trim() == user.UserID.Trim()).CompanySID.Trim();
                //    System.Web.HttpContext.Current.Session["IsSiteAdmin"] = db.SitePermissions.Any(p => user.UserID.Trim() == p.UserID.Trim() && p.SecurityLevel == 1);
                //}

                //if (user.SecurityLevel < 32000)
                //    SessionChecker.UpdateInsertSessionDictionary(user.LoginID);

                //if (user.CompanyID.Contains("00SAI") && user.SecurityLevel >= 32000)
                //    System.Web.HttpContext.Current.Session["IsSuperAdmin"] = true;

                var authTicket = new FormsAuthenticationTicket(userName, true, 60); 
                var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

                FormsAuthentication.SetAuthCookie(userName, false);

                System.Web.HttpContext.Current.Response.Cookies.Add(authCookie);
            }

            public void SignOut()
            {
                var request = System.Web.HttpContext.Current.Request;
                var response = System.Web.HttpContext.Current.Response;

                foreach (var cookie in request.Cookies.AllKeys)
                {
                    request.Cookies.Remove(cookie);
                }
                foreach (var cookie in response.Cookies.AllKeys)
                {
                    response.Cookies.Remove(cookie);
                }

                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity(string.Empty), null);
                System.Web.HttpContext.Current.Session.Abandon();
                FormsAuthentication.SignOut();
            }

        }

        public void Authenticate(object sender, AuthenticateEventArgs e)
        {

            //FormsAuthenticationService fas = new FormsAuthenticationService();
            
            //e.Authenticated = validateUser(LoginUser.UserName, LoginUser.Password);
            //if (e.Authenticated)
            //{
            //    fas.SignIn(LoginUser.UserName, false);
            //    alogin.InnerText = "Logout";
            //    if (Request.QueryString["ReturnUrl"] != null)
            //        Response.Redirect(Request.QueryString["ReturnUrl"]);
            //    else
            //        Response.Redirect("CalcInput.aspx");
            //}
            //else
            //{
            //    divInvalidPW.Visible = true;
            //    outer1.Style.Add("display", "block");
            //}
        }

        #region Validate User
        private bool validateUser(string username, string password)
        {
            //string dbPassword = string.Empty;
            //byte[] dbPassword;
            //string hashedPassword = string.Empty;
            //SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            //using (conn)
            //{
            //    SqlCommand command = new SqlCommand("[web].Get_User", conn);
            //    command.CommandType = CommandType.StoredProcedure;
            //    command.Parameters.Add(new SqlParameter("@Username", username));
            //    conn.Open();

            //    SqlDataReader dr = command.ExecuteReader();
            //    dr.Read();
            //    if (!dr.HasRows)
            //    {
            //        conn.Close();
            //        return false;
            //    }
            //    else
            //    {
            //        //dbPassword = dr["Password"].ToString();
            //        dbPassword = ((byte[])dr["Password"]);
            //        byte[] hashedBytes;

            //        using (SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider())
            //        {
            //            //byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(dr["Salt"].toString() + password + password));
            //            byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(Convert.ToBase64String(((byte[])dr["Salt"])) + password + password));
            //            hashedPassword = Convert.ToBase64String(bb);
            //            hashedBytes = bb;
            //        }

            //        //conn.Close();

            //        //if (dbPassword == hashedPassword)
            //        if (Convert.ToBase64String(dbPassword) == Convert.ToBase64String(hashedBytes))
            //        {
            //            User user = new User();
            //            user._userID = Convert.ToInt32(dr["UserID"].ToString());
            //            user._securityLevelID = Convert.ToInt32(dr["SecurityLevelID"].ToString());
            //            user._firstName = dr["FirstName"].ToString();
            //            user._lastName = dr["LastName"].ToString();
            //            user._username = dr["Username"].ToString();
            //            user._companyID = Convert.ToInt32(dr["CompanyID"].ToString());
            //            user._companyName = dr["CompanyName"].ToString();
            //            Session["User"] = user;
            //            conn.Close();
            //            return true;
            //        }
            //        else
            //        {
            //            conn.Close();
            //            return false;
            //        }
            //    }
            //}
            return true;
        }
        #endregion

        #region Generate Salt
        private byte[] generateSalt()
        {
            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                byte[] data = new byte[512];
                rngCsp.GetBytes(data);
                return data;                
            }
        }
        #endregion

        #region btnLogout Click Event
        public void btnLogout_Click(object sender, EventArgs e)
        {
            FormsAuthenticationService fas = new FormsAuthenticationService();
            fas.SignOut();
            Response.Redirect("Default.aspx",true);
        }
        #endregion

        protected void lnkbtnForgotPassword_Click(object sender, EventArgs e)
        {
            //outer1.Visible = true;
            //lblResetPasswordStatus.Visible = false;
            //txtEmailFP.Text = "";
            //mpePassword.Show();
        }

        protected void btnResetPassword_Click(object sender, EventArgs e)
        {
            //lblResetPasswordStatus.Visible = false;
            //SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            //using (conn)
            //{
            //    SqlCommand command = new SqlCommand("[web].Get_User", conn);
            //    command.CommandType = CommandType.StoredProcedure;
            //    command.Parameters.Add(new SqlParameter("@Username", txtEmailFP.Text));
            //    conn.Open();

            //    SqlDataReader dr = command.ExecuteReader();
            //    dr.Read();
            //    if (!dr.HasRows)
            //    {
            //        lblResetPasswordStatus.Visible = true;
            //        lblResetPasswordStatus.Text = "Account not found.";
            //    }
            //    else
            //    {
            //        string password = Membership.GeneratePassword(8, 1);
            //        byte[] hashedBytes;
            //        byte[] Salt = generateSalt();

            //        using (SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider())
            //        {
            //            byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(Convert.ToBase64String(Salt) + password + password));
            //            hashedBytes = bb;
            //        }

            //        SqlCommand command2 = new SqlCommand("[web].Update_User", conn);
            //        command2.CommandType = CommandType.StoredProcedure;
            //        command2.Parameters.Add(new SqlParameter("@UserID", dr["UserID"].ToString()));
            //        command2.Parameters.Add(new SqlParameter("@Password", hashedBytes));
            //        command2.Parameters.Add(new SqlParameter("@Salt", Salt));
            //        command2.ExecuteNonQuery();

            //        string body = "<html><head><title></title></head><body><img src=\"https://webservices.solomononline.com/Sandbox/Images/Logo1.png\"><br />This email is to confirm that your password for the Solomon Associates Olefin Study Factors and Standards Online Calculator has been reset. Please use the password below to login.";
            //        body += "<p>Password: " + password + "</p>";
            //        if (Convert.ToInt32(dr["SecurityLevelID"]) >= 10)
            //        {
            //            body += "<p>If you would like to change your password, please contact your company coordinator.</p>";
            //        }
            //        if (Convert.ToInt32(dr["SecurityLevelID"]) <= 5)
            //        {
            //            body += "<p>If you would like to change your password, login to the Olefin Study Factors and Standards Online Calculator and click on Administration from the menu. On the Administration page, you can change your password by clicking on your name and then entering a new password.</p>";
            //        }
            //        body += "<p>If you did not request your password be reset, please contact Solomon Associates immediately.</p><p>Thank you.</p></body></html>";

            //        //SendMail sm = new SendMail();
            //        sm.SendEMailMessage("Solomon Associates", "no-reply@solomononline.com", txtEmailFP.Text, null, null, "Password Reset", body);

            //        lblResetPasswordStatus.Visible = true;
            //        lblResetPasswordStatus.Text = "Password has been reset. Please check your email.";
            //    }
            //    conn.Close();
            //}


        }

    }
}