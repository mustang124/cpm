﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OlefinsVideos.Startup))]
namespace OlefinsVideos
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
