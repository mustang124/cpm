﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using OlefinsVideos.Models;
using System.Data.SqlClient;
using System.Data;

namespace OlefinsVideos
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                //RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                //var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);
                bool result = false;
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_VideoUser", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Username", Email.Text));
                    command.Parameters.Add(new SqlParameter("@Password", Password.Text));
                    conn.Open();

                    SqlDataReader dr = command.ExecuteReader();
                    dr.Read();
                    if (!dr.HasRows)
                    {
                        conn.Close();
                        result = false;
                    }
                    else
                    {
                        result = true;
                        conn.Close();
                        
                    }
                }


                if (result)
                {
                    Session["Valid"] = true;
                    //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                    Response.Redirect("video/Videos");
                }
                 else
                {
                    Session["Valid"] = false;
                    FailureText.Text = "Invalid login attempt";
                    ErrorMessage.Visible = true;
                }      
                    //case SignInStatus.LockedOut:
                    //    Response.Redirect("/Account/Lockout");
                    //    break;
                    //case SignInStatus.RequiresVerification:
                    //    Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                    //                                    Request.QueryString["ReturnUrl"],
                    //                                    RememberMe.Checked),
                    //                      true);
                    //    break;
                    //case SignInStatus.Failure:
                    //default:
                    //    FailureText.Text = "Invalid login attempt";
                    //    ErrorMessage.Visible = true;
                    //    break;
                
            }
        }
    }
}