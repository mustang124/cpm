﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace OlefinsCalculator
{
    public partial class CompanyAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((System.Web.UI.HtmlControls.HtmlAnchor)this.Master.FindControl("alogin")).InnerText = "Logout";

            OlefinsCalculator.User user = (OlefinsCalculator.User)Session["User"];
            if (user._securityLevelID > 1)
                Response.Redirect("https://www.google.com/search?q=stop+it&tbm=isch&tbo=u&source=univ&sa=X");

            if (!Page.IsPostBack)
            {
                bindCompanies();
            }
        }

        protected void lnkbtnAddCompany_Click(object sender, EventArgs e)
        {
            txtCompanyName.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtPassword.Text = "";
            lblStatus.Text = "";
            hidInsertUser.Value = "1";
            divCalcs.Visible = false;
            divCoInfo.Visible = true;
            rfvPassword.Enabled = true;
            ddlCompany.SelectedIndex = -1;
            divPlants.Visible = false;
        }

        public void bindCompanies()
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("[web].Get_Companies_Active", conn);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsCompanies = new DataSet();
                sda.Fill(dsCompanies);
                ddlCompany.DataSource = dsCompanies;
                ddlCompany.DataValueField = "CompanyID";
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataBind();
                ddlCompany.Items.Insert(0, (new ListItem("-- Select --", "")));

                ddlCompanyName.DataSource = dsCompanies;
                ddlCompanyName.DataValueField = "CompanyID";
                ddlCompanyName.DataTextField = "CompanyName";
                ddlCompanyName.DataBind();
                ddlCompanyName.Items.Insert(0, (new ListItem("-- Select --", "")));
            }
        }

        public void bindPlants(int CompanyID)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("[web].Get_PlantsByCompany_Active", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                SqlDataAdapter sda = new SqlDataAdapter(command);
                DataSet dsPlants = new DataSet();
                sda.Fill(dsPlants);
                ddlPlant.DataSource = dsPlants;
                ddlPlant.DataValueField = "PlantID";
                ddlPlant.DataTextField = "PlantName";
                ddlPlant.DataBind();
                ddlPlant.Items.Insert(0, (new ListItem("-- Select --", "")));

                lvPlants.DataSource = dsPlants;
                lvPlants.DataBind();
            }
        }

        public void bindCalcsRemaining(int plantID)
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("[web].get_PlantCalculationLimits", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@PlantID", plantID));
                conn.Open();

                SqlDataReader dr = command.ExecuteReader();
                dr.Read();
                if (dr.HasRows)
                    lblRemainingCalcs.Text = "(" + dr["SubmissionsRemaining_Count"].ToString() + " calcs remaining)";
                else
                    lblRemainingCalcs.Text = "";
                conn.Close();
            }
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblRemainingCalcs.Text = "";

            if (ddlCompany.SelectedIndex > 0)
            {
                bindPlants(Convert.ToInt32(ddlCompany.SelectedItem.Value));
                divPlants.Visible = true;
                divCalcs.Visible = true;
                divCoInfo.Visible = true;
                hidCompanyID.Value = ddlCompany.SelectedItem.Value;
                txtPassword.Text = "";
                txtPassword2.Text = "";
                lblStatus.Text = "";
                rfvPassword.Enabled = false;
                hidInsertUser.Value = "0";
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtEmail.Text = "";
                hidUserID.Value = "";

                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_LoginsByCompanyRole_Active", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@CompanyID", ddlCompany.SelectedItem.Value));
                    command.Parameters.Add(new SqlParameter("@SecurityLevelID", 5));
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsCoCoordinator = new DataSet();
                    sda.Fill(dsCoCoordinator);
                    ddlCompanyCoordinator.DataSource = dsCoCoordinator;
                    ddlCompanyCoordinator.DataTextField = "LastFirst";
                    ddlCompanyCoordinator.DataValueField = "UserName";
                    ddlCompanyCoordinator.DataBind();
                    ddlCompanyCoordinator.Items.Insert(0, (new ListItem("-- Select --", "")));
                    if (dsCoCoordinator.Tables[0].Rows.Count == 1)
                    {
                        ddlCompanyCoordinator.SelectedIndex = 1;
                        ddlCompanyCoordinator_SelectedIndexChanged(new object(), new EventArgs());
                    }
                    //conn.Open();
                    //SqlDataReader dr = command.ExecuteReader();
                    //dr.Read();
                    //if (dr.HasRows)
                    //{
                        txtCompanyName.Text = ddlCompany.SelectedItem.Text;
                    //    txtFirstName.Text = dr["FirstName"].ToString();
                    //    txtLastName.Text = dr["LastName"].ToString();
                    //    txtEmail.Text = dr["Username"].ToString();
                    //    hidUserID.Value = dr["UserID"].ToString();
                    //}
                    //conn.Close();
                }
            }
            else
            {
                divCalcs.Visible = false;
                divCoInfo.Visible = false;
                divPlants.Visible = false;
            }

            lvCalcs.Visible = false;
        }

        protected void ddlCompanyCoordinator_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCompanyCoordinator.SelectedIndex > 0)
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_User", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Username", ddlCompanyCoordinator.SelectedItem.Value));
                    conn.Open();

                    SqlDataReader dr = command.ExecuteReader();
                    dr.Read();
                    if (dr.HasRows)
                    {
                        txtFirstName.Text = dr["FirstName"].ToString();
                        txtLastName.Text = dr["LastName"].ToString();
                        txtEmail.Text = dr["Username"].ToString();
                        hidUserID.Value = dr["UserID"].ToString();
                    }
                    conn.Close();
                }
            }
        }

        protected void ddlPlant_SelctedIndexChanged(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            hidInsertUser.Value = "0";
            lblRemainingCalcs.Text = "";

            if (ddlPlant.SelectedIndex > 0)
            {
                lblRemainingCalcs.Visible = true;
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                using (conn)
                {
                    SqlCommand command = new SqlCommand("[web].Get_SubmissionsByPlant_Active", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@PlantID", ddlPlant.SelectedItem.Value));
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    DataSet dsCalcs = new DataSet();
                    sda.Fill(dsCalcs);
                    lvCalcs.DataSource = dsCalcs;
                    lvCalcs.DataBind();
                }

                if (lvCalcs.Items.Count > 0)
                    lvCalcs.Visible = true;

                bindCalcsRemaining(Convert.ToInt32(ddlPlant.SelectedItem.Value));
            }
            else
                lblRemainingCalcs.Visible = false;
        }

        protected void btnDeletePlants_Click(object sender, EventArgs e)
        {
            foreach (ListViewDataItem lvdi in lvPlants.Items)
            {
                if (((CheckBox)lvdi.FindControl("chkPlant")).Checked == true)
                {
                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                    using (conn)
                    {
                        conn.Open();
                        SqlCommand command = new SqlCommand("[web].Update_Plant", conn);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@Active", false));
                        command.Parameters.Add(new SqlParameter("@PlantID", ((HiddenField)lvdi.FindControl("hidPlantID")).Value));
                        command.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        }

        protected void lvPlants_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            LinkButton linkbutton = (LinkButton)e.Item.FindControl("lnkbtnSelectPlant");

            linkbutton.Text = drv.DataView[e.Item.DataItemIndex]["PlantName"].ToString();

            HiddenField hidPlantID = (HiddenField)e.Item.FindControl("hidPlantID");
            hidPlantID.Value = drv.DataView[e.Item.DataItemIndex]["PlantID"].ToString();
        }

        protected void btnDeleteCalcs_Click(object sender, EventArgs e)
        {
            //foreach (RepeaterItem ri in lvCalcs.Items)
            foreach (ListViewDataItem lvdi in lvCalcs.Items)
            {
                if (((CheckBox)lvdi.FindControl("chkCalc")).Checked == true)
                {
                    string abc = ((HiddenField)lvdi.FindControl("hidCalcID")).Value;
                    SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
                    using (conn)
                    {
                        conn.Open();
                        SqlCommand command = new SqlCommand("[web].Update_Submissions", conn);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@SubmissionID", ((HiddenField)lvdi.FindControl("hidCalcID")).Value));
                        command.Parameters.Add(new SqlParameter("@Active", 0));
                        command.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }

            ddlPlant_SelctedIndexChanged(new object(), new EventArgs());
        }

        protected void lvCalcs_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            HyperLink hyperlink = (HyperLink)e.Item.FindControl("hlCalc");

            string queryString = QueryStringModule.Encrypt("CalcID=" + drv.DataView[e.Item.DataItemIndex]["ID"] + "&CompanyID=" + ddlCompany.SelectedItem.Value + "&PlantID=" + ddlPlant.SelectedItem.Value);
            hyperlink.NavigateUrl = "~/CalcInput.aspx" + queryString;
            hyperlink.Text = drv.DataView[e.Item.DataItemIndex]["CalcDescription"].ToString();
            //hyperlink.Attributes.Add("onclick", "javascript:window.open('CalcInput.aspx" + queryString + "'); return false;");
            //hyperlink.NavigateUrl = "~/CalcInput.aspx?CalcID=" + drv.DataView[e.Item.ItemIndex]["ID"];

            HiddenField hidCalcID = (HiddenField)e.Item.FindControl("hidCalcID");
            hidCalcID.Value = drv.DataView[e.Item.DataItemIndex]["ID"].ToString();
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            var CID = new object();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                conn.Open();
                //SqlCommand command = new SqlCommand("UpdateUser", conn);
                SqlCommand command = new SqlCommand("[web].Update_User", conn);
                command.CommandType = CommandType.StoredProcedure;
                
                if (hidInsertUser.Value == "0")
                    command.Parameters.Add(new SqlParameter("@UserID", hidUserID.Value));
                else
                    command.Parameters.Add(new SqlParameter("@Active", 1));
                
                command.Parameters.Add(new SqlParameter("@FirstName", txtFirstName.Text));
                command.Parameters.Add(new SqlParameter("@LastName", txtLastName.Text));
                command.Parameters.Add(new SqlParameter("@Username", txtEmail.Text));
                command.Parameters.Add(new SqlParameter("@SecurityLevelID", 5));
                if (txtPassword.Text != "")
                {
                    string hashedPassword = string.Empty;
                    byte[] salt = generateSalt();
                    string saltString = Convert.ToBase64String(salt);
                    using (SHA512CryptoServiceProvider sha512 = new SHA512CryptoServiceProvider())
                    {
                        //byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(salt + txtPassword.Text + txtPassword.Text));
                        byte[] bb = sha512.ComputeHash(Encoding.UTF8.GetBytes(Convert.ToBase64String(salt) + txtPassword.Text + txtPassword.Text));
                        hashedPassword = Convert.ToBase64String(bb);
                        command.Parameters.Add(new SqlParameter("@Password", bb));
                    }
                    //command.Parameters.Add(new SqlParameter("@Password", hashedPassword));
                    command.Parameters.Add(new SqlParameter("@Salt", salt));
                }
                //command.ExecuteNonQuery();
                var UID = command.ExecuteScalar();//.ExecuteNonQuery();

                if (hidInsertUser.Value == "0")
                {
                    command.CommandText = "[web].Update_Company";
                    command.Parameters.Clear();
                    command.Parameters.Add(new SqlParameter("@CompanyID", hidCompanyID.Value));
                    command.Parameters.Add(new SqlParameter("@CompanyName", txtCompanyName.Text));
                    command.ExecuteNonQuery();
                }
                else
                {
                    SqlCommand command2 = new SqlCommand("[web].Update_Company", conn);
                    command2.CommandType = CommandType.StoredProcedure;
                    command2.Parameters.Add(new SqlParameter("@CompanyName", txtCompanyName.Text));
                    CID = command2.ExecuteScalar();
                }

                if (UID != null)
                {
                    hidUserID.Value = UID.ToString();
                    SqlCommand command3 = new SqlCommand("[web].Update_CompanyPermissions", conn);
                    command3.CommandType = CommandType.StoredProcedure;
                    command3.Parameters.Add(new SqlParameter("@CompanyID", CID));
                    command3.Parameters.Add(new SqlParameter("@UserID", UID));
                    command3.Parameters.Add(new SqlParameter("@Active", 1));
                    command3.ExecuteNonQuery();
                }

                if (hidInsertUser.Value != "0")
                    bindCompanies();
                
                lblStatus.Text = "Record Updated";
                conn.Close();
                hidInsertUser.Value = "0";
            }
        }

        protected void btnEditPlantSubmit_Click(object sender, EventArgs e)
        {
            lblPlantEditStatus.Visible = false;

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["OlefinCalculatorConnectionString"].ToString());
            using (conn)
            {
                SqlCommand command = new SqlCommand("[web].Update_Plant", conn);
                command.CommandType = CommandType.StoredProcedure;

                if (hidInsertPlant.Value == "0")
                    command.Parameters.Add(new SqlParameter("@PlantID", hidPlantID.Value));

                command.Parameters.Add(new SqlParameter("@PlantName", txtPlantName.Text));
                command.Parameters.Add(new SqlParameter("@CompanyID", ddlCompanyName.SelectedItem.Value));
                conn.Open();
             
                var pID = command.ExecuteScalar();
                if (Convert.ToInt32(pID) > 0)
                {
                    hidPlantID.Value = pID.ToString();
                    hidInsertPlant.Value = "0";
                }
                conn.Close();
            }

            if (ddlCompany.SelectedItem.Value == ddlCompanyName.SelectedItem.Value)
                bindPlants(Convert.ToInt32(ddlCompanyName.SelectedItem.Value));

            lblPlantEditStatus.ForeColor = System.Drawing.Color.Red;
            lblPlantEditStatus.Visible = true;
            lblPlantEditStatus.Text = "Plant Updated.";
            upnlMain.Update();
            mpePlant.Show();
        }

        protected void lnkbtnAddPlant_Click(object sender, EventArgs e)
        {
            lblPlantEditStatus.Visible = false;
            ddlCompanyName.SelectedIndex = -1;
            txtPlantName.Text = "";
            hidInsertPlant.Value = "1";
            mpePlant.Show();
        }

        protected void lnkbtnSelectPlant_Click(object sender, EventArgs e)
        {
            LinkButton linkbutton = (LinkButton)sender;
            hidPlantID.Value = ((HiddenField)linkbutton.FindControl("hidPlantID")).Value;
            txtPlantName.Text = linkbutton.Text;
            lblPlantEditStatus.Visible = false;
            ddlCompanyName.SelectedIndex = ddlCompany.SelectedIndex;
            hidInsertPlant.Value = "0";
            mpePlant.Show();
        }

        #region Generate Salt
        private byte[] generateSalt()
        {
            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                byte[] data = new byte[512];
                rngCsp.GetBytes(data);
                return data;
            }
        }
        #endregion


    }
}