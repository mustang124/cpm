﻿CREATE FUNCTION [dim].[Return_FeedClassId]
(
	@FeedClassName	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	IF(ISNUMERIC(@FeedClassName) = 1)
	BEGIN
		SELECT @Id = l.[FeedClassId]
		FROM [dim].[FeedClass_LookUp]	l
		WHERE l.[FeedClassTag] = @FeedClassName;
	END
	ELSE
	BEGIN
		SELECT @Id = l.[FeedClassId]
		FROM [dim].[FeedClass_LookUp]	l
		WHERE l.[FeedClassName] = @FeedClassName;
	END

	RETURN @Id;

END;