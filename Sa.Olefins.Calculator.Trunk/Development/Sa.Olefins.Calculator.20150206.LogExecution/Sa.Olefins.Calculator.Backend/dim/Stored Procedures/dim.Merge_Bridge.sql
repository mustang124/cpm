﻿CREATE PROCEDURE [dim].[Merge_Bridge]
(
	@SourceSchemaName		VARCHAR(48)	= 'dim',
	@SourceTableName		VARCHAR(48),

	@TargetSchemaName		VARCHAR(48)	= 'dim',
	@TargetTableName		VARCHAR(48),

	@MethodologyIdName		VARCHAR(48)	= 'MethodologyId',
	@PrimeIdName			VARCHAR(48),
	@SortKeyName			VARCHAR(48)	= 'SortKey',
	@HierarchyName			VARCHAR(48)	= 'Hierarchy',
	@OperatorName			VARCHAR(48)	= 'Operator'
)
AS
BEGIN

SET NOCOUNT ON;

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	= '[' + @SourceSchemaName + '].[' + @SourceTableName + '] -> [' + @TargetSchemaName + '].[' + @TargetTableName + ']'

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @SQL	VARCHAR(MAX) = 
	'MERGE INTO [' + @TargetSchemaName + '].[' + @TargetTableName + '] AS Target
	USING
	(
		SELECT
			a.[' + @MethodologyIdName + '],
			a.[' + @PrimeIdName + '],
			a.[' + @SortKeyName + '],
			a.[' + @HierarchyName + '],
			d.[' + @PrimeIdName + ']		[DescendantId],
			d.[' + @OperatorName + ']		[DescendantOperator]
		FROM [' + @SourceSchemaName + '].[' + @SourceTableName + ']			a
		INNER JOIN [' + @SourceSchemaName + '].[' + @SourceTableName + ']	d
			ON	d.[' + @MethodologyIdName + '] = a.[' + @MethodologyIdName + ']
			AND	d.[' + @HierarchyName + '].IsDescendantOf(a.[' + @HierarchyName + ']) = 1
	)
	AS Source([' + @MethodologyIdName + '], [' + @PrimeIdName + '], [' + @SortKeyName + '], [' + @HierarchyName + '], [DescendantId], [DescendantOperator])
	ON	Target.[' + @MethodologyIdName + '] = Source.[' + @MethodologyIdName + ']
	AND	Target.[' + @PrimeIdName + '] = Source.[' + @PrimeIdName + ']
	AND	Target.[DescendantId] = Source.[DescendantId]
	WHEN MATCHED THEN UPDATE SET
		Target.[' + @SortKeyName + ']			= Source.[' + @SortKeyName + '],
		Target.[' + @HierarchyName + ']			= Source.[' + @HierarchyName + '],
		Target.[DescendantOperator]	= Source.[DescendantOperator]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([' + @MethodologyIdName + '], [' + @PrimeIdName + '], [' + @SortKeyName + '], [' + @HierarchyName + '], [DescendantId], [DescendantOperator])
		VALUES([' + @MethodologyIdName + '], [' + @PrimeIdName + '], [' + @SortKeyName + '], [' + @HierarchyName + '], [DescendantId], [DescendantOperator])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;'

	EXECUTE(@SQL);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();

	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;

/*
EXECUTE dim.Merge_Bridge 'dim', 'Stream_Parent', 'dim', 'Stream_Bridge', 'MethodologyId', 'StreamId', 'SortKey', 'Hierarchy', 'Operator';
*/