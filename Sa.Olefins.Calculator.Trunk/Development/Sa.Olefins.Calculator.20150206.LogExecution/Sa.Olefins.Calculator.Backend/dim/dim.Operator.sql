﻿CREATE TABLE [dim].[Operator]
(
	[Operator]	CHAR(1)	NOT	NULL	CONSTRAINT [DF_Operator]	DEFAULT '+',
									CONSTRAINT [CR_Operator]	CHECK ([Operator] = '*' OR [Operator] = '/' OR [Operator] = '+' OR [Operator] = '-' OR [Operator] = '~'),
	CONSTRAINT [PK_Operator]		PRIMARY KEY CLUSTERED ([Operator] ASC)
);
GO