﻿CREATE TABLE [dim].[Message_LookUp]
(
	[MessageId]				INT					NOT NULL	IDENTITY (1, 1),

	[MessageTag]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_Message_LookUp_MessageTag]			CHECK ([MessageTag]		<> ''),
															CONSTRAINT [UK_Message_LookUp_LanguageTag]			UNIQUE NONCLUSTERED ([MessageTag]),
	[MessageName]			NVARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Message_LookUp_MessageName]			CHECK ([MessageName]	<> ''),
															CONSTRAINT [UK_Message_LookUp_MessageName]			UNIQUE NONCLUSTERED ([MessageName]),
	[MessageDetail]			NVARCHAR(256)		NOT	NULL	CONSTRAINT [CL_Message_LookUp_MessageDetail]		CHECK ([MessageDetail]	<> ''),
															CONSTRAINT [UK_Message_LookUp_MessageDetail]		UNIQUE NONCLUSTERED ([MessageDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Message_LookUp_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_LookUp_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_LookUp_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Message_LookUp_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Message_LookUp]				PRIMARY KEY CLUSTERED ([MessageId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Message_LookUp_u]
ON [dim].[Message_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Message_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Message_LookUp].[MessageId]	= INSERTED.[MessageId];

END;
GO