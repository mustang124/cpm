﻿CREATE TABLE [dim].[Stream_Parent]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Stream_Parent_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_Stream_Parent_LookUp_Stream]			REFERENCES [dim].[Stream_LookUp] ([StreamId])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[ParentId]				INT					NOT	NULL	CONSTRAINT [FK_Stream_Parent_LookUp_Parent]			REFERENCES [dim].[Stream_LookUp] ([StreamId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Stream_Parent_Parent]				FOREIGN KEY ([MethodologyId], [ParentId])
																												REFERENCES [dim].[Stream_Parent] ([MethodologyId], [StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Operator]				CHAR(1)				NOT	NULL	CONSTRAINT [DF_Stream_Parent_Operator]				DEFAULT ('+')
															CONSTRAINT [FK_Stream_Parent_Operator]				REFERENCES [dim].[Operator] ([Operator])							ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Stream_Parent_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_Parent_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_Parent_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_Parent_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Stream_Parent]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Stream_Parent_u]
ON [dim].[Stream_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Stream_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Stream_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Stream_Parent].[StreamId]			= INSERTED.[StreamId];

END;
GO