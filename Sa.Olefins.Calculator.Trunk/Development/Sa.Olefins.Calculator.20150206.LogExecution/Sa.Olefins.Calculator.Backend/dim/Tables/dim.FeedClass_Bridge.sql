﻿CREATE TABLE [dim].[FeedClass_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_Bridge_Methodology]			REFERENCES [ante].[Methodology] ([MethodologyId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FeedClassId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_Bridge_FeedClassId]			REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_FeedClass_Bridge_Parent_Ancestor]		FOREIGN KEY ([MethodologyId], [FeedClassId])
																													REFERENCES [dim].[FeedClass_Parent] ([MethodologyId], [FeedClassId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_FeedClass_Bridge_DescendantID]			REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId])						ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_FeedClass_Bridge_Parent_Descendant]		FOREIGN KEY ([MethodologyId], [DescendantId])
																													REFERENCES [dim].[FeedClass_Parent] ([MethodologyId], [FeedClassId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_FeedClass_Bridge_DescendantOperator]		DEFAULT ('+')
															CONSTRAINT [FK_FeedClass_Bridge_DescendantOperator]		REFERENCES [dim].[Operator] ([Operator])								ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FeedClass_Bridge_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_Bridge_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_Bridge_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_Bridge_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FeedClass_Bridge]			PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FeedClassId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_FeedClass_Bridge_u]
ON [dim].[FeedClass_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedClass_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[FeedClass_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[FeedClass_Bridge].[FeedClassId]		= INSERTED.[FeedClassId]
		AND	[dim].[FeedClass_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;
GO