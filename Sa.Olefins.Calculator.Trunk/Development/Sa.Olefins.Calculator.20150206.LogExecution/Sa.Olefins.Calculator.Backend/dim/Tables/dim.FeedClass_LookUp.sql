﻿CREATE TABLE [dim].[FeedClass_LookUp]
(
	[FeedClassId]			INT					NOT NULL	IDENTITY (1, 1),

	[FeedClassTag]			VARCHAR(42)			NOT	NULL	CONSTRAINT [CL_FeedClass_LookUp_FeedClassTag]			CHECK ([FeedClassTag]		<> ''),
															CONSTRAINT [UK_FeedClass_LookUp_FeedClassTag]			UNIQUE NONCLUSTERED ([FeedClassTag]),
	[FeedClassName]			VARCHAR(84)			NOT	NULL	CONSTRAINT [CL_FeedClass_LookUp_FeedClassName]			CHECK ([FeedClassName]		<> ''),
															CONSTRAINT [UK_FeedClass_LookUp_FeedClassName]			UNIQUE NONCLUSTERED ([FeedClassName]),
	[FeedClassDetail]		VARCHAR(256)		NOT	NULL	CONSTRAINT [CL_FeedClass_LookUp_FeedClassDetail]		CHECK ([FeedClassDetail]	<> ''),
															CONSTRAINT [UK_FeedClass_LookUp_FeedClassDetail]		UNIQUE NONCLUSTERED ([FeedClassDetail]),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FeedClass_LookUp_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_LookUp_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_LookUp_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FeedClass_LookUp_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_FeedClass_LookUp]			PRIMARY KEY CLUSTERED ([FeedClassId] ASC)
);
GO

CREATE TRIGGER [dim].[t_FeedClass_LookUp_u]
ON [dim].[FeedClass_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedClass_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[FeedClass_LookUp].[FeedClassId]		= INSERTED.[FeedClassId];

END;
GO