﻿CREATE TABLE [dim].[Factor_Bridge]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Factor_Bridge_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])				ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FactorId]				INT					NOT	NULL	CONSTRAINT [FK_Factor_Bridge_FactorId]					REFERENCES [dim].[Factor_LookUp] ([FactorId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Factor_Bridge_Parent_Ancestor]			FOREIGN KEY ([MethodologyId], [FactorId])
																													REFERENCES [dim].[Factor_Parent] ([MethodologyId], [FactorId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SortKey]				INT					NOT	NULL,
	[Hierarchy]				SYS.HIERARCHYID		NOT	NULL,

	[DescendantId]			INT					NOT	NULL	CONSTRAINT [FK_Factor_Bridge_DescendantID]				REFERENCES [dim].[Factor_LookUp] ([FactorId])					ON DELETE NO ACTION ON UPDATE NO ACTION,
															CONSTRAINT [FK_Factor_Bridge_Parent_Descendant]			FOREIGN KEY ([MethodologyId], [DescendantId])
																													REFERENCES [dim].[Factor_Parent] ([MethodologyId], [FactorId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[DescendantOperator]	CHAR(1)				NOT	NULL	CONSTRAINT [DF_Factor_Bridge_DescendantOperator]		DEFAULT ('+')
															CONSTRAINT [FK_Factor_Bridge_DescendantOperator]		REFERENCES [dim].[Operator] ([Operator])						ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Factor_Bridge_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_Bridge_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_Bridge_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factor_Bridge_tsModifiedApp]				DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Factor_Bridge]				PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FactorId] ASC, [DescendantId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Factor_Bridge_u]
ON [dim].[Factor_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Factor_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Factor_Bridge].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Factor_Bridge].[FactorId]			= INSERTED.[FactorId]
		AND	[dim].[Factor_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;
GO