﻿using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with the SQLCLR assembly.
[assembly: AssemblyTitle("Sa.Olefins.Calculator.Backend")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("HSB Solomon Associates LLC")]
[assembly: AssemblyProduct("Sa.Olefins.Calculator.Backend")]
[assembly: AssemblyCopyright("© 2013 HSB Solomon Associates LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
