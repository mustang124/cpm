﻿DECLARE @tsInserted			DATETIMEOFFSET = SYSDATETIMEOFFSET();
DECLARE @LanguageId			INT = 1033;
DECLARE @MethodologyId		INT = IDENT_CURRENT('[ante].[Methodology]');

DECLARE	@SubmissionId		INT	= NULL; --322;
DECLARE @MessageId			INT = [dim].[Return_MessageId]('StreamBalance')

DECLARE @Refnum				VARCHAR (12) = '11PCH003'; --091
IF (@SubmissionId IS NULL)	SELECT @SubmissionId = s.[SubmissionId] FROM [fact].[Submissions] s WHERE s.[SubmissionName] = @Refnum ORDER BY s.[SubmissionId] ASC;

---------------------------------------------------------------------------------------------------

SELECT
	v.SubmissionId,
	v.StandardId,
	v.ProcessUnitId,
	s.StandardTag,
	p.ProcessUnitTag,
	v.StandardValue
FROM calc.Standards_Aggregate		v
INNER JOIN dim.Standard_LookUp		s
	ON	s.[StandardId]		= v.[StandardId]
INNER JOIN dim.ProcessUnit_LookUp	p
	ON	p.[ProcessUnitId]	= v.[ProcessUnitId]
WHERE	v.SubmissionId		= @SubmissionId
	AND	v.StandardId		= 1
ORDER BY
	v.StandardId	ASC,
	v.ProcessUnitId	ASC;

