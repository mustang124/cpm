﻿DECLARE @SubmissionId	INT = 141;
DECLARE @MethodologyId	INT = [ante].[Return_MethodologyId]('2013');
DECLARE @LanguageId		INT = [dim].[Return_NlsLcid]('en-US');

SELECT [le].* FROM [audit].[LogError] [le] WHERE [le].[ProcedureParam] LIKE '@SubmissionId:' + CONVERT(VARCHAR, @SubmissionId) + '%';


SELECT * FROM [stage].[Errors_DataSet] [e] WHERE [e].[SubmissionId] = @SubmissionId;


EXECUTE [web].[Verify_DataSetStage] @SubmissionId, @LanguageId;


--EXECUTE [web].[Get_FactorsAndStandards] @MethodologyId, @SubmissionId;
--EXECUTE [web].[Process_DataSetStage] @MethodologyId, @SubmissionId;
