﻿SELECT
	l.[FacilityId],
	l.[FacilityTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[FacilityTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[FacilityName],
	l.[FacilityDetail]
FROM [dim].[Facility_LookUp]		l
INNER JOIN [dim].[Facility_Parent]	p
	ON	p.[FacilityId] = l.[FacilityId]
ORDER BY p.[Hierarchy];

SELECT
	l.[StreamId],
	l.[StreamTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[StreamTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[StreamName],
	l.[StreamDetail]
FROM [dim].[Stream_LookUp]			l
INNER JOIN [dim].[Stream_Parent]	p
	ON	p.[StreamId] = l.[StreamId]
ORDER BY p.[Hierarchy];

SELECT
	l.[ComponentId],
	l.[ComponentTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[ComponentTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[ComponentName],
	l.[ComponentDetail]
FROM [dim].[Component_LookUp]			l
INNER JOIN [dim].[Component_Parent]	p
	ON	p.[ComponentId] = l.[ComponentId]
ORDER BY p.[Hierarchy];

SELECT
	l.[ProcessUnitId],
	l.[ProcessUnitTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() , 2)) + l.[ProcessUnitTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() , 2)) + l.[ProcessUnitName],
	l.[ProcessUnitDetail]
FROM [dim].[ProcessUnit_LookUp]			l
INNER JOIN [dim].[ProcessUnit_Parent]	p
	ON	p.[ProcessUnitId] = l.[ProcessUnitId]
ORDER BY p.[Hierarchy];

SELECT
	l.[FactorId],
	l.[FactorTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 0, 2)) + l.[FactorTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 0, 2)) + l.[FactorName],
	l.[FactorDetail]
FROM [dim].[Factor_LookUp]			l
INNER JOIN [dim].[Factor_Parent]	p
	ON	p.[FactorId] = l.[FactorId]
ORDER BY p.[Hierarchy];

SELECT
	l.[StandardId],
	l.[StandardTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[StandardTag],
	REPLICATE(' ', POWER(p.[Hierarchy].GetLevel() - 1, 2)) + l.[StandardName],
	l.[StandardDetail]
FROM [dim].[Standard_LookUp]		l
INNER JOIN [dim].[Standard_Parent]	p
	ON	p.[StandardId] = l.[StandardId]
ORDER BY p.[Hierarchy];