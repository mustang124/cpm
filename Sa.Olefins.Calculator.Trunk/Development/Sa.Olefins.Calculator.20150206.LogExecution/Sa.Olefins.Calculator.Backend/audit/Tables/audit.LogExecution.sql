﻿CREATE TABLE [audit].[LogExecution]
(
	[ProcedureSchema]		SYSNAME				NOT	NULL	CONSTRAINT [CL_LogExecution_ProcedureSchema]	CHECK([ProcedureSchema]		<> ''),
	[ProcedureName]			VARCHAR(128)		NOT	NULL	CONSTRAINT [CL_LogExecution_ProcedureName]		CHECK([ProcedureName]		<> ''),
	[ProcedureParam]		VARCHAR(4000)			NULL	CONSTRAINT [CL_LogExecution_ProcedureParam]		CHECK([ProcedureParam]		<> ''),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_LogExecution_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogExecution_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogExecution_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogExecution_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL
);
GO

CREATE TRIGGER [audit].[t_LogExecution_u]
ON [audit].[LogExecution]
AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [audit].[LogExecution]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[audit].[LogExecution].[tsModifiedRV]		= INSERTED.[tsModifiedRV];

END;