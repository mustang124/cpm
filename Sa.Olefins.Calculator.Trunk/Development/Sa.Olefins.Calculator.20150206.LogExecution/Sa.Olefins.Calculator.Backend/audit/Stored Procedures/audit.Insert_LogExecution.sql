﻿CREATE PROCEDURE [audit].[Insert_LogExecution]
(
	@ProcId			INT,
	@tsExecuted		DATETIMEOFFSET(7),
	@Parameters		VARCHAR (4000)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @PROCEDURE_SCHEMA	VARCHAR(128)	= OBJECT_SCHEMA_NAME(@ProcId);
	DECLARE @PROCEDURE_NAME		VARCHAR(128)	= OBJECT_NAME(@ProcId);

	IF (RTRIM(LTRIM(@Parameters)) = '') SET @Parameters = NULL;

	INSERT INTO [audit].[LogExecution]
	(
		[ProcedureSchema],
		[ProcedureName],
		[ProcedureParam],
		[tsModified]
	)
	SELECT
		@PROCEDURE_SCHEMA,
		@PROCEDURE_NAME,
		@Parameters,
		@tsExecuted;

END;