﻿CREATE TABLE [xls].[Capacity_CrackedGasTransfers]
(
	[Refnum]					VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_Capacity_CrackedGasTransfers_Refnum]							CHECK([Refnum] <> ''),

	[Capacity_Ann_kMT]			FLOAT				NOT	NULL	CONSTRAINT [CR_Capacity_CrackedGasTransfers_Capacity_Ann_kMT_MinIncl_0.0]	CHECK([Capacity_Ann_kMT] >= 0.0),
	[Utilization_Pcnt]			FLOAT				NOT	NULL	CONSTRAINT [CR_Capacity_CrackedGasTransfers_Utilization_Pcnt_MinIncl_0.0]	CHECK([Utilization_Pcnt] >= 0.0),
																--CONSTRAINT [CR_Capacity_CrackedGasTransfers_Utilization_Pcnt_MaxIncl_100.0]	CHECK([Utilization_Pcnt] <= 100.0),

	[_CapacityInfer_Ann_kMT]	AS CONVERT(FLOAT, [Capacity_Ann_kMT] / [Utilization_Pcnt] * 100.0)
								PERSISTED			NOT	NULL,	

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModified]						DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedHost]					DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedUser]					DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Capacity_CrackedGasTransfers_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Capacity_CrackedGasTransfers]	PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_Capacity_CrackedGasTransfers_u]
ON [xls].[Capacity_CrackedGasTransfers]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[Capacity_CrackedGasTransfers]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[Capacity_CrackedGasTransfers].[Refnum]	= INSERTED.[Refnum];

END;
GO