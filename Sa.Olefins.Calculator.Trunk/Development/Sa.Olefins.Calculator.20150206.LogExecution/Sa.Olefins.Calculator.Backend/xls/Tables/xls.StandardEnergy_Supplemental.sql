﻿CREATE TABLE [xls].[StandardEnergy_Supplemental]
(
	[Refnum]					VARCHAR(12)			NOT	NULL	CONSTRAINT [CL_StandardEnergy_Supplemental_Refnum]								CHECK([Refnum] <> ''),

	[Quantity_kMT]				FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Supplemental_Quantity_kMT_MinIncl_0.0]			CHECK([Quantity_kMT] >= 0.0),
	[StandardEnergy_MBtu]		FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Supplemental_StandardEnergy_MBtu_MinIncl_0.0]		CHECK([StandardEnergy_MBtu] >= 0.0),
	[StandardEnergy_MBtuDay]	FLOAT				NOT	NULL	CONSTRAINT [CR_StandardEnergy_Supplemental_StandardEnergy_MBtuDay_MinIncl_0.0]	CHECK([StandardEnergy_MBtuDay] >= 0.0),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_StandardEnergy_Supplemental_tsModified]							DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Supplemental_tsModifiedHost]						DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Supplemental_tsModifiedUser]						DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_StandardEnergy_Supplemental_tsModifiedApp]						DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_StandardEnergy_Supplemental]		PRIMARY KEY CLUSTERED ([Refnum] DESC)
);
GO

CREATE TRIGGER [xls].[t_StandardEnergy_Supplemental_u]
ON [xls].[StandardEnergy_Supplemental]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StandardEnergy_Supplemental]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StandardEnergy_Supplemental].[Refnum]	= INSERTED.[Refnum];

END;
GO