﻿CREATE PROCEDURE [web].[Get_FeedClass]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		l.[FeedClassId]			[FeedClassID],
		l.[FeedClassName]		[FeedClassDesc]
	FROM [dim].[FeedClass_LookUp]	l
	WHERE l.[FeedClassId] >= 2
	ORDER BY
		l.[FeedClassTag] ASC;

END;