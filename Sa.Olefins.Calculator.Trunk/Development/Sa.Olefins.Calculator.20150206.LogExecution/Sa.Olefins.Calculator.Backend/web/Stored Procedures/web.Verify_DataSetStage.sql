﻿CREATE PROCEDURE [web].[Verify_DataSetStage]
(
	@SubmissionId			INT,
	@LanguageId				INT = 1033
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@Error_Count	INT;

	DECLARE	@LocaleId		SMALLINT = CONVERT(SMALLINT, @LanguageId);

	EXECUTE	@Error_Count = [stage].[Verify_DataSet] @SubmissionId, @LocaleId;

	RETURN	@Error_Count;

END;