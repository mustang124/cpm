﻿CREATE PROCEDURE [web].[Get_SubmissionsByPlant_Active]
(
	@PlantId		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		sbp.[JoinId],
		sbp.[PlantId],
		sbp.[SubmissionId]			[Id],
		sbp.[PlantName],
		sbp.[SubmissionName],
		sbp.[SubmissionNameFull],
		sbp.[TimeEnd],
		sbp.[SubmissionName] + ' (' + CONVERT(VARCHAR, sbp.[TimeEnd], 106) + ')' AS [CalcDescription],
		sbp.[SubmissionName] + ' (' + CONVERT(VARCHAR, CONVERT(DATETIME2(0), SYSDATETIMEOFFSET()), 113) + ')' AS [SubmissionName_DateTime]
	FROM [auth].[Get_SubmissionsByPlant_Active](@PlantId)	sbp
	ORDER BY
		sbp.[TimeEnd]				DESC,
		sbp.[SubmissionName]		ASC,
		sbp.[SubmissionNameFull]	ASC;

END;
