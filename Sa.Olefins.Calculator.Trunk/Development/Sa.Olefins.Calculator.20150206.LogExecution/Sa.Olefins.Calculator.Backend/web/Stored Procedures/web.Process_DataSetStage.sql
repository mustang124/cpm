﻿CREATE PROCEDURE [web].[Process_DataSetStage]
(
	@MethodologyId			INT = NULL,
	@SubmissionId			INT
)
AS
BEGIN

	EXECUTE [calc].[ProcessSubmission] @MethodologyId, @SubmissionId;

END;