﻿CREATE TABLE [ante].[MapEiiModelComponents]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_MapEiiModelComponents_Methodology]		REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[FreshComponentId]		INT					NOT	NULL	CONSTRAINT [FK_MapEiiModelComponents_Fresh]				REFERENCES [dim].[Stream_LookUp] ([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[ModelComponentId]		INT					NOT	NULL	CONSTRAINT [FK_MapEiiModelComponents_Model]				REFERENCES [dim].[Stream_LookUp] ([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_MapEiiModelComponents_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapEiiModelComponents_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapEiiModelComponents_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_MapEiiModelComponents_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_MapEiiModelComponents]	PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FreshComponentId] ASC)
);
GO

CREATE TRIGGER [ante].[t_MapEiiModelComponents_u]
ON [ante].[MapEiiModelComponents]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapEiiModelComponents]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapEiiModelComponents].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[MapEiiModelComponents].[ModelComponentId]	= INSERTED.[ModelComponentId];

END;
GO