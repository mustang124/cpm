﻿CREATE TABLE [ante].[Factors_EiiCoefficients]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_Factors_EiiCoefficients_Methodology]				REFERENCES [ante].[Methodology] ([MethodologyId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_Factors_EiiCoefficients_Stream_LookUp]			REFERENCES [dim].[Stream_LookUp] ([StreamId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	
	[HvcYield_Coefficient]	FLOAT					NULL	CONSTRAINT [CR_Factors_EiiCoefficients_HvcYield_Coefficient_MaxIncl_0.0]	CHECK([HvcYield_Coefficient]	<= 0.0),
	[Energy_Coefficient]	FLOAT					NULL	CONSTRAINT [CR_Factors_EiiCoefficients_Energy_Coefficient_MaxIncl_0.0]		CHECK([Energy_Coefficient]		<= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Factors_EiiCoefficients_tsModified]				DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_EiiCoefficients_tsModifiedHost]			DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_EiiCoefficients_tsModifiedUser]			DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Factors_EiiCoefficients_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Factors_EiiCoefficients]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC)
);
GO

CREATE TRIGGER [ante].[t_Factors_EiiCoefficients_u]
ON [ante].[Factors_EiiCoefficients]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Factors_EiiCoefficients]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Factors_EiiCoefficients].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[Factors_EiiCoefficients].[StreamId]			= INSERTED.[StreamId];

END;
GO