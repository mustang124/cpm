﻿CREATE FUNCTION [calc].[Return_FeedClassId_SubmissionId]
(
	@SubmissionId			INT
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT TOP 1
		@Id = l.[FeedClassId]
	FROM [calc].[FeedClass]					fc
	INNER JOIN [dim].[FeedClass_LookUp]		l
		ON	l.[FeedClassId]		= fc.[FeedClassId]
	WHERE	fc.[SubmissionId]	= @SubmissionId;

	RETURN @Id;

END;