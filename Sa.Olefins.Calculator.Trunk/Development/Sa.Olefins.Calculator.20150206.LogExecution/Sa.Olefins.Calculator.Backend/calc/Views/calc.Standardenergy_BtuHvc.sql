﻿CREATE VIEW [calc].[StandardEnergy_BtuHvc]
WITH SCHEMABINDING
AS
SELECT
	a.[MethodologyId],
	a.[SubmissionId],
	SUM(a.[StandardValue] * ISNULL(z.[_Duration_Days], 0.0) / d.[HvcProdDivisor_kMT] / 2.2046) [EIIStdEnergy_BtuHvc],
	COUNT_BIG(*)						[Items]
FROM [fact].[Submissions]					z
INNER JOIN [calc].[Standards]				a
	ON	a.[SubmissionId]		= z.[SubmissionId]
INNER JOIN [calc].[HvcProductionDivisor]	d
	ON	d.[MethodologyId]		= a.[MethodologyId]
	AND	d.[SubmissionId]		= a.[SubmissionId]
WHERE	a.[StandardId]			= 2
GROUP BY
	a.[MethodologyId],
	a.[SubmissionId]
GO

CREATE UNIQUE CLUSTERED INDEX [UX_StandardEnergy_BtuHvc]
ON [calc].[StandardEnergy_BtuHvc]([MethodologyId] DESC, [SubmissionId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_StandardEnergy_BtuHvc]
ON [calc].[StandardEnergy_BtuHvc]([MethodologyId] DESC, [SubmissionId] ASC)
INCLUDE([EIIStdEnergy_BtuHvc]);
GO