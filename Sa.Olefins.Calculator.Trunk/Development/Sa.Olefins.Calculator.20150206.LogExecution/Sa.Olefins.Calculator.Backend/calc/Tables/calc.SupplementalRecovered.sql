﻿CREATE TABLE [calc].[SupplementalRecovered]
(
	[MethodologyId]			INT					NOT	NULL	CONSTRAINT [FK_SupplementalRecovered_Methodology]					REFERENCES [ante].[Methodology] ([MethodologyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_SupplementalRecovered_Submissions]					REFERENCES [fact].[Submissions] ([SubmissionId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
	[StreamId]				INT					NOT	NULL	CONSTRAINT [FK_SupplementalRecovered_Stream_LookUp]					REFERENCES [dim].[Stream_LookUp] ([StreamId])			ON DELETE NO ACTION ON UPDATE NO ACTION,
	[ComponentId]			INT					NOT	NULL	CONSTRAINT [FK_SupplementalRecovered_Component_LookUp]				REFERENCES [dim].[Component_LookUp] ([ComponentId])		ON DELETE NO ACTION ON UPDATE NO ACTION,

	[Recovered_Dur_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_SupplementalRecovered_Recovered_Dur_kMT_MinIncl_0.0]	CHECK([Recovered_Dur_kMT] >= 0.0),
	[Recovered_Ann_kMT]		FLOAT				NOT	NULL	CONSTRAINT [CR_SupplementalRecovered_Recovered_Ann_kMT_MinIncl_0.0]	CHECK([Recovered_Ann_kMT] >= 0.0),

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_SupplementalRecovered_tsModified]					DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SupplementalRecovered_tsModifiedHost]				DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SupplementalRecovered_tsModifiedUser]				DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SupplementalRecovered_tsModifiedApp]					DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_SupplementalRecovered]		PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] ASC, [ComponentId] ASC, [StreamId] ASC)
);
GO

CREATE TRIGGER [calc].[t_SupplementalRecovered_u]
ON [calc].[SupplementalRecovered]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[SupplementalRecovered]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[SupplementalRecovered].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[SupplementalRecovered].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[SupplementalRecovered].[ComponentId]	= INSERTED.[ComponentId];

END;
GO