﻿CREATE PROCEDURE [calc].[Insert_DataSet]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

	DECLARE @Error			INT = 0;
	DECLARE @Error_Count	INT = 0;

	EXECUTE @Error = [calc].[Insert_Audit]								@MethodologyId, @SubmissionId;

	EXECUTE	@Error = [calc].[Insert_StreamComposition_Expansion]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StreamComposition_Fact]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StreamComposition_Default]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE	@Error = [calc].[Insert_StreamRecovered]					@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StreamRecycled]						@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_Utilization]						@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_Capacity]							@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_HydrogenPurification]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_HvcProductionDivisor]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE	@Error = [calc].[Insert_FacilitiesFractionator]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_FacilitiesHydroTreater]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_FacilitiesBoilers]					@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_FacilitiesElecGeneration]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [calc].[Insert_SupplementalRecovered_ConcPropylene_C3H8]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_SupplementalRecovered_ConcPropylene_C3H6]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_SupplementalRecovered_SuppWashOil_PyroGasOil]	@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_SupplementalRecovered_SuppWashOil_PyroFuelOil]	@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_SupplementalRecovered_Default]					@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_SupplementalRecovered_ClientSpec]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_SupplementalRecovered_ClientSpec_Balance]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE	@Error = [calc].[Insert_Capacity_CrackedGasTransfers]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE	@Error = [calc].[Insert_FeedClass]							@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [calc].[Insert_ContainedLightFeed]					@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE	@Error = [calc].[Insert_StandardEnergy_Supplemental]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_Fractionator]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_HydroTreater]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_HydroPur]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_Red_Ethylene]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_Red_Propylene]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_Red_CrackedGas]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_PyrolysisHvc]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE	@Error = [calc].[Insert_StandardEnergy_PyrolysisBtu]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [calc].[Insert_Standard_PM_Capacity]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_Supplemental]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_FeedPrep]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_HydroTreater]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_HydroPur]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_Red_Propylene]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_Red_CrackedGas]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_Boilers]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_PM_ElecGen]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [calc].[Insert_Standard_kEdc_Capacity]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_Supplemental]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_FeedPrep]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_HydroTreater]			@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_HydroPur]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_Red_Propylene]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_Red_CrackedGas]		@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_Boilers]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [calc].[Insert_Standard_kEdc_ElecGen]				@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [calc].[Insert_Standard_StdEnergy]					@MethodologyId, @SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	DECLARE @TimeEnd DATETIMEOFFSET = SYSDATETIMEOFFSET();

	EXECUTE [calc].[Update_Audit]										@MethodologyId, @SubmissionId, @TimeEnd, @Error_Count;

END;
GO