﻿CREATE PROCEDURE [calc].[Insert_Capacity]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @ProdOlefins		INT = [dim].[Return_StreamId]('ProdOlefins');
	DECLARE @ComponentId		INT = [dim].[Return_ComponentId]('ProdOlefins');

	INSERT INTO [calc].[Capacity]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [StreamDay_MTSD], [Recovered_Ann_kMT], [Utilization_Pcnt])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamId],
		c.[ComponentId],
		c.[StreamDay_MTSD],
		r.[Recovered_Ann_kMT],
		c.[_Utilization_Pcnt]
	FROM [calc].[Utilization]							c
	LEFT OUTER JOIN [calc].[StreamRecovered_EP_Aggregate]	r WITH (NOEXPAND)
		ON	r.[MethodologyId]	= c.[MethodologyId]
		AND	r.[SubmissionId]	= c.[SubmissionId]
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
		AND	c.[StreamId]		= @ProdOlefins
		AND	c.[ComponentId]		= @ComponentId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO