﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Red_Ethylene]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @Ethylene		INT = [dim].[Return_StreamId]('Ethylene');
	DECLARE @EthyleneCG		INT = [dim].[Return_StreamId]('EthyleneCG');

	INSERT INTO [calc].[StandardEnergy_Reduction]([MethodologyId], [SubmissionId], [ProcessUnitId], [StreamId], [Quantity_kMT], [Quantity_bbl], [StandardEnergy_MBtu], [StandardEnergy_MBtuDay])
	SELECT
		m.[MethodologyId],
		q.[SubmissionId],
		m.[ProcessUnitId],
		@Ethylene,
		q.[Quantity_kMT],
		q.[Quantity_kMT] * 11.0,
		q.[Quantity_kMT] * 11.0 * 30.0,
		q.[Quantity_kMT] * 11.0 * 30.0 / s.[_Duration_Days]
	FROM [fact].[StreamQuantity]			q
	INNER JOIN [fact].[Submissions]			s
		ON	s.[SubmissionId]	= q.[SubmissionId]
		AND s.[_Duration_Days]	<> 0.0
	INNER JOIN [ante].[MapStreamProcessUnit]		m
		ON	m.[MethodologyId]	= @MethodologyId
		AND	m.[StreamId]		= q.[StreamId]
	WHERE	q.[StreamId]		= @EthyleneCG
		AND	q.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO