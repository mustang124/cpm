﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Fractionator]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [calc].[StandardEnergy_Fractionator]([MethodologyId], [SubmissionId], [Throughput_kMT], [Convert_bblTon], [Throughput_bbl], [Throughput_bblDay], [StandardEnergy_MBtuDay])
	SELECT
		@MethodologyId,
		f.[SubmissionId],
		f.[Throughput_kMT],
							 [calc].[MinValue](11.0, [calc].[MaxValue](6.0, 0.159 / f.[Density_SG]))											[Convert_bblTon],
		f.[Throughput_kMT] * [calc].[MinValue](11.0, [calc].[MaxValue](6.0, 0.159 / f.[Density_SG]))											[Throughput_bbl],
		f.[Throughput_kMT] * [calc].[MinValue](11.0, [calc].[MaxValue](6.0, 0.159 / f.[Density_SG])) / s.[_Duration_Days]						[Throughput_bblDay],
		f.[Throughput_kMT] * [calc].[MinValue](11.0, [calc].[MaxValue](6.0, 0.159 / f.[Density_SG])) / s.[_Duration_Days] * c.[Energy_kBtuBbl]	[StandardEnergy_MBtuDay]
	FROM [fact].[FacilitiesFractionator]		f
	INNER JOIN [fact].[Submissions]				s
		ON	s.[SubmissionId]	= f.[SubmissionId]
	INNER JOIN [ante].[StdEnergyFractionator]	c
		ON	c.[MethodologyId]	= @MethodologyId
		AND	c.[StreamId]		= f.[StreamId]
	WHERE 	s.[SubmissionId]	= @SubmissionId
		AND	f.[Density_SG]		<> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO