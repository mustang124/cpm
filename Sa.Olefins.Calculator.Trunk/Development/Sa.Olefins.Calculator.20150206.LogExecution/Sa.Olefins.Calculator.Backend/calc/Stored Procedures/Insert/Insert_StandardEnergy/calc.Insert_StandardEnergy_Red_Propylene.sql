﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Red_Propylene]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @Propylene		INT = [dim].[Return_StreamId]('Propylene');
	DECLARE @PropyleneCG	INT = [dim].[Return_StreamId]('PropyleneCG');
	DECLARE @PropyleneRG	INT = [dim].[Return_StreamId]('PropyleneRG');
	
	INSERT INTO [calc].[StandardEnergy_Reduction]([MethodologyId], [SubmissionId], [ProcessUnitId], [StreamId], [Quantity_kMT], [Quantity_bbl], [StandardEnergy_MBtu], [StandardEnergy_MBtuDay])
	SELECT
		@MethodologyId,
		q.[SubmissionId],
		m.[ProcessUnitId],
		b.[StreamId],
		SUM(q.[Quantity_kMT]),
		SUM(q.[Quantity_kMT] *
			CASE q.[StreamId]
				WHEN @PropyleneCG THEN 1.46			/* (bbl/mt) * (bbl feed/bbl product)				*/
				WHEN @PropyleneRG THEN 1.0
			END) * 10.0,
		SUM(q.[Quantity_kMT] *
			CASE q.[StreamId]
				WHEN @PropyleneCG THEN 1.46 *  30.0	/* (bbl/mt) * (bbl feed/bbl product) * (kBtu/bbl)	*/
				WHEN @PropyleneRG THEN 1.00 * 180.0
			END) * 10.0,
		SUM(q.[Quantity_kMT] *
			CASE q.[StreamId]
				WHEN @PropyleneCG THEN 1.46 *  30.0	/* (bbl/mt) * (bbl feed/bbl product) * (kBtu/bbl)	*/
				WHEN @PropyleneRG THEN 1.00 * 180.0
			END) * 10.0 / s.[_Duration_Days]
	FROM [fact].[StreamQuantity]					q
	INNER JOIN [fact].[Submissions]					s
		ON	s.[SubmissionId]	= q.[SubmissionId]
	INNER JOIN [dim].[Stream_Bridge]				b
		ON	b.[DescendantId]	= q.[StreamId]
		AND	b.[StreamId]		= @Propylene
		AND	b.[DescendantId]	IN (@PropyleneCG, @PropyleneRG)
	INNER JOIN [calc].[FeedClass]					f
		ON	f.[MethodologyId]	= @MethodologyId
		AND	f.[SubmissionId]	= s.[SubmissionId]
		AND	f.[FeedClassId]		<> dim.Return_FeedClassId('Ethane')
	INNER JOIN [ante].[MapStreamProcessUnit]		m
		ON	m.[MethodologyId]	= f.[MethodologyId]
		AND	m.[StreamId]		= q.[StreamId]
	WHERE	q.[SubmissionId]	= @SubmissionId
		AND	s.[_Duration_Days]	<> 0.0
	GROUP BY
		q.[SubmissionId],
		m.[ProcessUnitId],
		b.[StreamId],
		s.[_Duration_Days];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO