﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_HydroTreater]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @Benzene		INT = [dim].[Return_StreamId]('Benzene');
	DECLARE @PyroGasoline	INT = [dim].[Return_StreamId]('PyroGasoline');

	INSERT INTO [calc].[StandardEnergy_HydroTreater]([MethodologyId], [SubmissionId], [Quantity_kMT], [Quantity_Barrels], [Quantity_BarrelsDay], [StandardEnergy_MBtuDay])
	SELECT
		e.[MethodologyId],
		q.[SubmissionId],
		SUM(q.[Quantity_kMT] * h.[Processed_Pcnt]) / 100.0,
		SUM(q.[Quantity_kMT] * h.[Processed_Pcnt])			* 7.16	* 10.0,
		SUM(q.[Quantity_kMT] * h.[Processed_Pcnt])			* 7.16	* 10.0	/ s.[_Duration_Days],
		SUM(q.[Quantity_kMT] * h.[Processed_Pcnt]) / 100.0	* 7.16			/ s.[_Duration_Days] * e.[Energy_kBtuBbl]
	FROM [fact].[StreamQuantity]				q
	INNER JOIN [fact].[FacilitiesHydroTreater]	h
		ON	h.[SubmissionId] = q.[SubmissionId]
	INNER JOIN [fact].[Submissions]				s
		ON	s.[SubmissionId] = q.[SubmissionId]
	INNER JOIN [ante].[StdEnergyHydroTreater]	e
		ON	e.[MethodologyId] = @MethodologyId
		AND	e.[HydroTreaterTypeId] = h.[HydroTreaterTypeId]
	WHERE	q.[StreamId] IN (@Benzene, @PyroGasoline)
		AND	s.[SubmissionId] = @SubmissionId
	GROUP BY
		e.[MethodologyId],
		q.[SubmissionId],
		s.[_Duration_Days],
		e.[Energy_kBtuBbl];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO