﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Supplemental]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [calc].[StandardEnergy_Supplemental]([MethodologyId], [SubmissionId], [Quantity_kMT], [StandardEnergy_MBtu], [StandardEnergy_MBtuDay])
	SELECT
		m.[MethodologyId],
		q.[SubmissionId],
		SUM(q.Quantity_kMT),
		SUM(q.Quantity_kMT * e.[StandardEnergy_BtuLb] * 2.2046),
		SUM(q.Quantity_kMT * e.[StandardEnergy_BtuLb] * 2.2046)	/ s.[_Duration_Days]
	FROM [fact].[StreamQuantity]					q
	INNER JOIN	[ante].[MapStreamComponent]			m
		ON	m.[StreamId]		= q.[StreamId]
	INNER JOIN [ante].[ComponentEnergy]				e
		ON	e.[MethodologyId]	= m.[MethodologyId]
		AND	e.[ComponentId]		= m.[ComponentId]
	INNER JOIN [fact].Submissions					s
		ON	s.[SubmissionId]	= q.[SubmissionId]
	WHERE	q.[StreamNumber]	>= 4000 AND q.[StreamNumber] <= 4999
		AND	m.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId
		AND	s.[_Duration_Days]	<> 0.0
	GROUP BY
		m.[MethodologyId],
		q.[SubmissionId],
		s.[_Duration_Days];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO