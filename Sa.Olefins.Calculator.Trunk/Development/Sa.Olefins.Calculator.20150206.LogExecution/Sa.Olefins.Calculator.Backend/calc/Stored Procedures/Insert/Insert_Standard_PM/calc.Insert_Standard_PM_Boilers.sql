﻿CREATE PROCEDURE [calc].[Insert_Standard_PM_Boilers]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE	@StandardId_kEdc			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@StandardId_StdEnergy		INT	= dim.Return_StandardId('StdEnergy');

	DECLARE	@ProcessUnitId				INT	= dim.Return_ProcessUnitId('BoilFiredSteam');
	DECLARE	@FactorId					INT	= dim.Return_FactorId('BoilFiredSteam');
	DECLARE	@FacilityId					INT	= dim.Return_FacilityId('BoilFiredSteam');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		f.[MethodologyId],
		fba.[SubmissionId],
		f.[StandardId],
		@ProcessUnitId			[ProcessUnitId],
		f.[Coefficient] * fba.[Rate_kLbHr] * scu.[WwCduScu] / 1000.0 *
		POWER([calc].[MaxValue](f.[ValueMin], [calc].[MinValue](f.[ValueMax], fba.[Rate_kLbHr] / CONVERT(FLOAT, fba.[Boiler_Count]))), f.[Exponent])
		[StandardValue]
	FROM		[fact].[FacilitiesBoilers_FiredSteam]	fba WITH (NOEXPAND)
	INNER JOIN	[ante].[Factors]						f
		ON	f.[StandardId]		NOT IN (@StandardId_kEdc, @StandardId_StdEnergy)
	INNER JOIN [ante].[FactorsWwCduScu]					scu
		ON	scu.[MethodologyId]	= f.[MethodologyId]
		AND	scu.[StandardId]	= f.[StandardId]
	WHERE	f.[MethodologyId]		= @MethodologyId
		AND	f.[FactorId]			= @FactorId
		AND	fba.[FacilityId]		= @FacilityId
		AND	fba.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO