﻿CREATE PROCEDURE [calc].[Insert_Standard_PM_Red_CrackedGas]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE	@StandardId_kEdc			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@StandardId_StdEnergy		INT	= dim.Return_StandardId('StdEnergy');

	DECLARE	@ProcessUnitId				INT	= dim.Return_ProcessUnitId('RedCrackedGasTrans');
	DECLARE @FactorId_Feed				INT	= dim.Return_FactorId('FreshPyroFeed');
	DECLARE @FactorId_CompGas			INT	= dim.Return_FactorId('CompGas');
	
	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		cap.[MethodologyId],
		cap.[SubmissionId],
		fStd.[StandardId],
		@ProcessUnitId,
		(fCgt.[Coefficient] - fStd.[Coefficient]) * POWER(cap.StreamDay_MTSD, fCgt.Exponent) * cgt._CapacityInfer_Ann_kMT * scu.WwCduScu / 1000.0
	FROM [calc].[Capacity]								cap
	INNER JOIN	[calc].[Capacity_CrackedGasTransfers]	cgt
		ON	cgt.[MethodologyId]		= cap.[MethodologyId]
		AND	cgt.[SubmissionId]		= cap.[SubmissionId]
	INNER JOIN	[calc].[FeedClass]						fdc
		ON	fdc.[MethodologyId]		= cap.[MethodologyId]
		AND	fdc.[SubmissionId]		= cap.[SubmissionId]

	INNER JOIN	[ante].[MapFactorFeedClass]				mStd
		ON	mStd.[MethodologyId]	= fdc.[MethodologyId]
		AND	mStd.[FeedClassId]		= fdc.[FeedClassId]
	INNER JOIN	[ante].[Factors]						fStd
		ON	fStd.[MethodologyId]	= mStd.[MethodologyId]
		AND	fStd.[FactorId]			= mStd.[FactorId]
		AND	fStd.[StandardId]		NOT IN (@StandardId_kEdc, @StandardId_StdEnergy)
	INNER JOIN	[dim].[Factor_Bridge]					bStd
		ON	bStd.[MethodologyId]	= cap.[MethodologyId]
		AND	bStd.[DescendantId]		= fStd.[FactorId]
		AND	bStd.[FactorId]			= @FactorId_Feed

	INNER JOIN	[ante].[MapFactorFeedClass]				mCgt
		ON	mCgt.[MethodologyId]	= fdc.[MethodologyId]
		AND	mCgt.[FeedClassId]		= fdc.[FeedClassId]
	INNER JOIN	[ante].[Factors]						fCgt
		ON	fCgt.[MethodologyId]	= mCgt.[MethodologyId]
		AND	fCgt.[FactorId]			= mCgt.[FactorId]
		AND	fCgt.[StandardId]		NOT IN (@StandardId_kEdc, @StandardId_StdEnergy)
	INNER JOIN	[dim].[Factor_Bridge]					bCgt
		ON	bCgt.[MethodologyId]	= cap.[MethodologyId]
		AND	bCgt.[DescendantId]		= fCgt.[FactorId]
		AND	bCgt.[FactorId]			= @FactorId_CompGas

	INNER JOIN ante.FactorsWwCduScu						scu
		ON	scu.MethodologyId	= mCgt.MethodologyId
		AND	scu.StandardId		= fCgt.StandardId

	WHERE	fStd.[StandardId]	= fCgt.[StandardId]
		AND	cap.[MethodologyId]	= @MethodologyId
		AND	cap.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO