﻿CREATE PROCEDURE [calc].[Insert_Standard_PM_HydroPur]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE	@StandardId_kEdc			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@StandardId_StdEnergy		INT	= dim.Return_StandardId('StdEnergy');

	DECLARE	@ProcessUnitId				INT	= dim.Return_ProcessUnitId('HydroPur');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		hp.MethodologyId,
		hp.SubmissionId,
		f.StandardId,
		@ProcessUnitId			[ProcessUnitId],
		f.Coefficient * hp._H2PurUtilSales_kScfDay * scu.WwCduScu / 1000.0 *
		POWER(calc.MaxValue(f.ValueMin, calc.MinValue(f.ValueMax, hp._H2PurUtilSales_kScfDay / CONVERT(FLOAT, hp.H2Unit_Count))), f.Exponent)
		[StandardValue]
	FROM		calc.HydrogenPurification			hp
	INNER JOIN	ante.MapFactorHydroPur				map
		ON	map.MethodologyId	= hp.MethodologyId
		AND	map.FacilityId		= hp.FacilityId
	INNER JOIN	ante.Factors						f
		ON	f.MethodologyId		= hp.MethodologyId
		AND	f.FactorId			= map.FactorId
		AND	f.StandardId		NOT IN (@StandardId_kEdc, @StandardId_StdEnergy)
	INNER JOIN ante.FactorsWwCduScu					scu
		ON	scu.MethodologyId	= hp.MethodologyId
		AND	scu.StandardId		= f.StandardId
	WHERE	hp._H2PurUtilSales_kScfDay > 0.0
		AND	hp.MethodologyId	= @MethodologyId
		AND	hp.SubmissionId		= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO