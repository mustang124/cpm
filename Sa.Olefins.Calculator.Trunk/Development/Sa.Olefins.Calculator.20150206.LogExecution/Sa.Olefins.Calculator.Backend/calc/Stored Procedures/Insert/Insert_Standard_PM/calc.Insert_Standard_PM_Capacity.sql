﻿CREATE PROCEDURE [calc].[Insert_Standard_PM_Capacity]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE	@StandardId_kEdc			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@StandardId_StdEnergy		INT	= dim.Return_StandardId('StdEnergy');

	DECLARE	@ProcessUnitId				INT	= dim.Return_ProcessUnitId('Fresh');
	DECLARE	@FactorId					INT	= dim.Return_FactorId('FreshPyroFeed');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		cap.MethodologyId,
		cap.SubmissionId,
		f.StandardId,
		@ProcessUnitId			[ProcessUnitId],
		f.Coefficient * cap._PlantCapacity_MTSD * scu.WwCduScu / 1000.0 *
		POWER(calc.MaxValue(f.ValueMin, calc.MinValue(f.ValueMax, cap._PlantCapacity_MTSD / CONVERT(FLOAT, ft.Train_Count))), f.Exponent)
		[StandardValue]
	FROM		[calc].[Capacity]					cap
	INNER JOIN	[fact].[FacilitiesTrains]			ft
		ON	ft.[SubmissionId]	= cap.[SubmissionId]
	INNER JOIN	[calc].[FeedClass]					fdc
		ON	fdc.[MethodologyId]	= cap.[MethodologyId]
		AND	fdc.[SubmissionId]	= cap.[SubmissionId]
	INNER JOIN	[ante].[MapFactorFeedClass]			map
		ON	map.[MethodologyId]	= fdc.[MethodologyId]
		AND	map.[FeedClassId]	= fdc.[FeedClassId]
	INNER JOIN	[ante].[Factors]					f
		ON	f.[MethodologyId]	= map.[MethodologyId]
		AND	f.[FactorId]		= map.[FactorId]
		AND	f.[StandardId]		NOT IN (@StandardId_kEdc, @StandardId_StdEnergy)
	INNER JOIN	[dim].[Factor_Bridge]				b
		ON	b.[MethodologyId]	= cap.[MethodologyId]
		AND	b.[DescendantId]	= f.[FactorId]
		AND	b.[FactorId]		= @FactorId
	INNER JOIN [ante].[FactorsWwCduScu]				scu
		ON	scu.[MethodologyId]	= cap.[MethodologyId]
		AND	scu.[StandardId]	= f.[StandardId]
	WHERE	cap.[MethodologyId]	= @MethodologyId
		AND	cap.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO