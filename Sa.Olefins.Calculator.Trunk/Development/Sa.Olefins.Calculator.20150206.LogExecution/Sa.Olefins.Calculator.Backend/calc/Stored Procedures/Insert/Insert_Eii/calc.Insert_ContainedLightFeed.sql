﻿CREATE PROCEDURE [calc].[Insert_ContainedLightFeed]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @FreshPyroFeed	INT	= [dim].[Return_StreamId]('Light');

	DECLARE @Contained	TABLE
	(
		[MethodologyId]			INT					NOT	NULL,
		[SubmissionId]			INT					NOT	NULL,
		[StreamId]				INT					NOT	NULL,
		[ComponentId]			INT					NOT	NULL,
		[ContainedFeed_Dur_kMT]	FLOAT				NOT	NULL	CHECK([ContainedFeed_Dur_kMT] >= 0.0),
		[ContainedFeed_Ann_kMT]	FLOAT				NOT	NULL	CHECK([ContainedFeed_Ann_kMT] >= 0.0)
	);

	INSERT INTO @Contained([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [ContainedFeed_Dur_kMT], [ContainedFeed_Ann_kMT])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		s.[StreamId],
		m.[ModelComponentId],
		SUM(c.[Component_Dur_kMT])		[Contained_Dur_kMT],
		SUM(c.[Component_Ann_kMT])		[Contained_Ann_kMT]
	FROM [calc].[StreamComposition]				c
	INNER JOIN [ante].[MapEiiModelComponents]	m
		ON	m.[MethodologyId]		= c.[MethodologyId]
		AND	m.[FreshComponentId]	= c.[ComponentId]
	INNER JOIN [dim].[Stream_Bridge]			b
		ON	b.[MethodologyId]		= c.[MethodologyId]
		AND	b.[DescendantId]		= c.[StreamId]
		AND	b.[StreamId]			= @FreshPyroFeed
	INNER JOIN [ante].[MapStreamComponent]		s
		ON	s.[MethodologyId]		= m.[MethodologyId]
		AND	s.[ComponentId]			= m.[ModelComponentId]
		AND	s.[StreamId]			IN (9, 10, 12)
	WHERE c.SubmissionId = @SubmissionId
	GROUP BY
		c.[MethodologyId],
		c.[SubmissionId],
		s.[StreamId],
		m.[ModelComponentId];

	INSERT INTO @Contained([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [ContainedFeed_Dur_kMT], [ContainedFeed_Ann_kMT])
	SELECT
		r.[MethodologyId],
		r.[SubmissionId],
		m.[StreamId],
		m.[ComponentId],
		r.[_Recycled_Dur_kMT],
		r.[_Recycled_Ann_kMT]
	FROM [calc].[StreamRecycled]	r
	INNER JOIN [ante].[MapStreamComponent]	m
		ON	m.[MethodologyId]	= r.[MethodologyId]
		AND	m.[ComponentId]		= r.[ComponentId]
		AND	m.[StreamId]		IN (9, 10, 12)
	WHERE	r.[MethodologyId]	= @MethodologyId
		AND	r.[SubmissionId]	= @SubmissionId
	ORDER BY
		r.[MethodologyId],
		r.[ComponentId];

	INSERT INTO [calc].[ContainedLightFeed]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [ContainedFeed_Dur_kMT], [ContainedFeed_Ann_kMT])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamId],
		c.[ComponentId],
		SUM(c.[ContainedFeed_Dur_kMT]),
		SUM(c.[ContainedFeed_Ann_kMT])
	FROM @Contained c
	GROUP BY
		c.[MethodologyId],
		c.[SubmissionId],
		c.[StreamId],
		c.[ComponentId];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO