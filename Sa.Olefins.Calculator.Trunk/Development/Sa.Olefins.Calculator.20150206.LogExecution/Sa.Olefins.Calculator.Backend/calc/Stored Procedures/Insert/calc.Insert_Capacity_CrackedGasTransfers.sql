﻿CREATE PROCEDURE [calc].[Insert_Capacity_CrackedGasTransfers]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @StreamId_ProdOther			INT = dim.Return_StreamId('ProdOther');
	DECLARE @StreamId_ProdOlefins		INT = dim.Return_StreamId('ProdOlefins');

	DECLARE @ComponentId_ProdOlefins	INT = dim.Return_ComponentId('ProdOlefins');
	DECLARE @ComponentId_C2H4			INT = dim.Return_ComponentId('C2H4');
	DECLARE @ComponentId_C3H6			INT = dim.Return_ComponentId('C3H6');

	INSERT INTO [calc].[Capacity_CrackedGasTransfers]([MethodologyId], [SubmissionId], [Capacity_Ann_kMT], [Utilization_Pcnt])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		SUM(c.[Component_Ann_kMT]),
		u.[_Utilization_Pcnt]
	FROM [calc].[StreamComposition]		c
	INNER JOIN [calc].[Utilization]		u
		ON	u.[MethodologyId]	= c.[MethodologyId]
		AND	u.[SubmissionId]	= c.[SubmissionId]
		AND	u.[StreamId]		= @StreamId_ProdOlefins
		AND	u.[ComponentId]		= @ComponentId_ProdOlefins
	WHERE	c.[StreamId]		= @StreamId_ProdOther
		AND	c.[ComponentId] IN (@ComponentId_C2H4, @ComponentId_C3H6)
		AND	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
	GROUP BY
		c.[MethodologyId],
		c.[SubmissionId],
		u.[_Utilization_Pcnt]

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO