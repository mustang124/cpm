﻿CREATE PROCEDURE [calc].[Insert_Standard_kEdc_Boilers]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE	@StandardId			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@ProcessUnitId		INT	= dim.Return_ProcessUnitId('BoilFiredSteam');
	DECLARE	@FacilityId			INT	= dim.Return_FacilityId('BoilFiredSteam');
	DECLARE	@FactorId			INT	= dim.Return_FactorId('BoilFiredSteam');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		f.[MethodologyId],
		boil.[SubmissionId],
		f.[StandardId],
		@ProcessUnitId,
		f.[Coefficient] * boil.[Rate_kLbHr] / 1000.0
	FROM	[fact].[FacilitiesBoilers_FiredSteam]		boil WITH (NOEXPAND)
	CROSS JOIN	[ante].[Factors]					f
	WHERE	boil.[FacilityId]	= @FacilityId
		AND	boil.[SubmissionId]	= @SubmissionId
		AND	f.[MethodologyId]	= @MethodologyId
		AND	f.[StandardId]		= @StandardId
		AND	f.[FactorId]		= @FactorId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO