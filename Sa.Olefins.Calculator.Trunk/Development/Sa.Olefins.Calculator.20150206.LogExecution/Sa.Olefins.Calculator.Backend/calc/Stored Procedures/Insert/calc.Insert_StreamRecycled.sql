﻿CREATE PROCEDURE [calc].[Insert_StreamRecycled]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [calc].[StreamRecycled]([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [Quantity_Dur_kMT], [Quantity_Ann_kMT], [ComponentId], [Recycled_WtPcnt])
	SELECT
		@MethodologyId,
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT],
		q.[Quantity_kMT] * s.[_Duration_Multiplier],
		m.[ComponentId],
		r.[Recycled_WtPcnt]
	FROM [fact].[StreamQuantity]					q
	INNER JOIN [ante].[MapComponentRecycle]			m
		ON	m.[StreamId] = q.[StreamId]
	INNER JOIN [fact].[StreamRecycled]				r
		ON	r.[SubmissionId]	= q.[SubmissionId]
		AND	r.[ComponentId]		= m.[ComponentId]
		AND	r.[Recycled_WtPcnt]	> 0.0
	INNER JOIN [fact].[Submissions]					s
		ON	s.[SubmissionId]	= q.[SubmissionId]
	WHERE	m.[MethodologyId]	= @MethodologyId
		AND	q.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO