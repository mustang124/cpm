﻿CREATE PROCEDURE [calc].[Insert_SupplementalRecovered_Default]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [calc].[SupplementalRecovered]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Recovered_Dur_kMT], [Recovered_Ann_kMT])
	SELECT
		x.[MethodologyId],
		x.[SubmissionId],
		q.[StreamId],
		m.[ComponentId],
		q.[Quantity_kMT] * m.[Recovered_WtPcnt] / 100.0,
		q.[Quantity_kMT] * m.[Recovered_WtPcnt] / 100.0 * s.[_Duration_Multiplier]
	FROM (VALUES
		(@MethodologyId, @SubmissionId)
		)												x ([MethodologyId], [SubmissionId])
	INNER JOIN [fact].[Submissions]						s
		ON	s.[SubmissionId]	= x.[SubmissionId]
	INNER JOIN [fact].[StreamQuantity]					q
		ON	q.[SubmissionId]	= x.[SubmissionId]
	INNER JOIN [ante].[MapStreamComponent]				m
		ON	m.[MethodologyId]	= x.[MethodologyId]
		AND	m.[StreamId]		= q.[StreamId]
	WHERE m.[Recovered_WtPcnt] > 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO