﻿CREATE PROCEDURE [calc].[Insert_SupplementalRecovered_ClientSpec]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE	@ConcPropylene		INT	= [dim].[Return_StreamId]('ConcPropylene');

	INSERT INTO [calc].[SupplementalRecovered]([MethodologyId], [SubmissionId], [StreamId], [ComponentId], [Recovered_Dur_kMT], [Recovered_Ann_kMT])
	SELECT
		x.[MethodologyId],
		x.[SubmissionId],
		r.[StreamId],
		m.[ComponentId],
		r.[_Recovered_Dur_kMT],
		r.[_Recovered_Ann_kMT]
	FROM (VALUES
		(@MethodologyId, @SubmissionId)
		)												x ([MethodologyId], [SubmissionId])
	INNER JOIN [calc].[StreamRecovered]					r
		ON	r.[MethodologyId]	= x.[MethodologyId]
		AND	r.[SubmissionId]	= x.[SubmissionId]
		AND	r.[StreamId]		<>	@ConcPropylene
	INNER JOIN [ante].[MapStreamComponent]				m
		ON	m.[MethodologyId]	= x.[MethodologyId]
		AND	m.[StreamId]		= r.[StreamId]
	WHERE r.[_Recovered_Dur_kMT] > 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO