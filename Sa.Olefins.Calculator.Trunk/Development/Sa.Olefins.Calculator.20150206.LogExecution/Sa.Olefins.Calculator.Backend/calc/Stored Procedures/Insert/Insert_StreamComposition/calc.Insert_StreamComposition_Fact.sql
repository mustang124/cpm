﻿CREATE PROCEDURE [calc].[Insert_StreamComposition_Fact]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [calc].[StreamComposition]([MethodologyId], [SubmissionId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt], [Component_Dur_kMT], [Component_Ann_kMT])
	SELECT
		@MethodologyId,
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		c.[ComponentId],
		c.[Component_WtPcnt],
		c.[Component_WtPcnt] * q.[Quantity_kMT] / 100.0,
		c.[Component_WtPcnt] * q.[Quantity_kMT] / 100.0 * z.[_Duration_Multiplier]
	FROM [fact].[StreamQuantity]					q
	INNER JOIN [fact].[StreamComposition]			c
		ON	c.[SubmissionId]		= q.[SubmissionId]
		AND	c.[StreamNumber]		= q.[StreamNumber]
	LEFT OUTER JOIN [calc].[StreamComposition]		s
		ON	s.[MethodologyId]		= @MethodologyId
		AND	s.[SubmissionId]		= @SubmissionId
		AND	s.[StreamNumber]		= q.[StreamNumber]
		AND	s.[StreamId]			= q.[StreamId]
		AND	s.[ComponentId]			= c.[ComponentId]
	INNER JOIN [fact].[Submissions]					z
		ON	z.[SubmissionId]		= q.[SubmissionId]
	WHERE	q.[SubmissionId]		= @SubmissionId
		AND	q.[Quantity_kMT]		> 0.0
		AND	c.[Component_WtPcnt]	> 0.0
		AND	s.[MethodologyId]		IS NULL;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO