﻿CREATE PROCEDURE [calc].[ProcessSubmission]
(
	@MethodologyId		INT = NULL,
	@SubmissionId		INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @XACT_POINT	CHAR(36) = CONVERT(CHAR(36), NEWID());

	BEGIN TRANSACTION	@XACT_POINT;

	IF (@MethodologyId IS NULL)
	SET @MethodologyId = IDENT_CURRENT('[ante].[Methodology]');

	EXECUTE [calc].[Delete_DataSet]	@MethodologyId,	@SubmissionId;
	EXECUTE [fact].[Delete_DataSet]					@SubmissionId;
	EXECUTE [fact].[Insert_DataSet]					@SubmissionId;
	EXECUTE [calc].[Insert_DataSet]	@MethodologyId,	@SubmissionId;

	COMMIT TRANSACTION	@XACT_POINT;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE	SMALLINT = XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;