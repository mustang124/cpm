﻿CREATE PROCEDURE [stage].[Update_FacilitiesFractionator]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@Quantity_kBSD			FLOAT	= NULL,
	@StreamId				INT		= NULL,
	@Throughput_kMT			FLOAT	= NULL,
	@Density_SG				FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @FacilityId:'		+ CONVERT(VARCHAR, @FacilityId))
		+ COALESCE(', @Quantity_kBSD:'	+ CONVERT(VARCHAR, @Quantity_kBSD),		'')
		+ COALESCE(', @StreamId:'		+ CONVERT(VARCHAR, @StreamId),			'')
		+ COALESCE(', @Throughput_kMT:'	+ CONVERT(VARCHAR, @Throughput_kMT),	'')
		+ COALESCE(', @Density_SG:'		+ CONVERT(VARCHAR, @Density_SG),		'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[FacilitiesFractionator]
	SET	[Quantity_kBSD]		= COALESCE(@Quantity_kBSD,	[Quantity_kBSD]),
		[StreamId]			= COALESCE(@StreamId,		[StreamId]),
		[Throughput_kMT]	= COALESCE(@Throughput_kMT,	[Throughput_kMT]),
		[Density_SG]		= COALESCE(@Density_SG,		[Density_SG])
	WHERE	[SubmissionId]	= @SubmissionId
		AND	[FacilityId]	= @FacilityId

		AND(@Quantity_kBSD	>= 0.0
		OR	@StreamId		>= 1
		OR	@Throughput_kMT	>= 0.0
		OR	@Density_SG		>= 0.0)
		
		AND([Quantity_kBSD]		<> COALESCE(@Quantity_kBSD,		[Quantity_kBSD])
		OR	[StreamId]			<> COALESCE(@StreamId,			[StreamId])
		OR	[Throughput_kMT]	<> COALESCE(@Throughput_kMT,	[Throughput_kMT])
		OR	[Density_SG]		<> COALESCE(@Density_SG,		[Density_SG]));

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;