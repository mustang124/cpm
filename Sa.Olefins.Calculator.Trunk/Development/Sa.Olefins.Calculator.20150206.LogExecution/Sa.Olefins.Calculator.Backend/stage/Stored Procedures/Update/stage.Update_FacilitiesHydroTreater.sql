﻿CREATE PROCEDURE [stage].[Update_FacilitiesHydroTreater]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@Quantity_kBSD			FLOAT	= NULL,
	@HydroTreaterTypeId		INT		= NULL,
	@Pressure_PSIg			FLOAT	= NULL,
	@Processed_kMT			FLOAT	= NULL,
	@Processed_Pcnt			FLOAT	= NULL,
	@Density_SG				FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
				('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
			+ (', @FacilityId:'			+ CONVERT(VARCHAR, @FacilityId))
	+ COALESCE(', @Quantity_kBSD:'		+ CONVERT(VARCHAR, @Quantity_kBSD),			'')
	+ COALESCE(', @HydroTreaterTypeId:'	+ CONVERT(VARCHAR, @HydroTreaterTypeId),	'')
	+ COALESCE(', @Pressure_PSIg:'		+ CONVERT(VARCHAR, @Pressure_PSIg),			'')
	+ COALESCE(', @Processed_kMT:'		+ CONVERT(VARCHAR, @Processed_kMT),			'')
	+ COALESCE(', @Processed_Pcnt:'		+ CONVERT(VARCHAR, @Processed_Pcnt),		'')
	+ COALESCE(', @Density_SG:'			+ CONVERT(VARCHAR, @Density_SG),			'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[FacilitiesHydroTreater]
	SET	[Quantity_kBSD]			= COALESCE(@Quantity_kBSD,		[Quantity_kBSD]),
		[HydroTreaterTypeId]	= COALESCE(@HydroTreaterTypeId,	[HydroTreaterTypeId]),
		[Pressure_PSIg]			= COALESCE(@Pressure_PSIg,		[Pressure_PSIg]),
		[Processed_kMT]			= COALESCE(@Processed_kMT,		[Processed_kMT]),
		[Processed_Pcnt]		= COALESCE(@Processed_Pcnt,		[Processed_Pcnt]),
		[Density_SG]			= COALESCE(@Density_SG,			[Density_SG])
	WHERE	[SubmissionId]		= @SubmissionId
		AND	[FacilityId]		= @FacilityId

		AND(@Quantity_kBSD		>= 0.0
		OR	@HydroTreaterTypeId	>= 1
		OR	@Pressure_PSIg		>= 0.0
		OR	@Processed_kMT		>= 0.0
		OR	@Processed_Pcnt		>= 0.0
		OR	@Density_SG			>= 0.0);
		
		--AND([Quantity_kBSD]			<> COALESCE(@Quantity_kBSD,			[Quantity_kBSD])
		--OR	[HydroTreaterTypeId]	<> COALESCE(@HydroTreaterTypeId,	[HydroTreaterTypeId])
		--OR	[Pressure_PSIg]			<> COALESCE(@Pressure_PSIg,			[Pressure_PSIg])
		--OR	[Processed_kMT]			<> COALESCE(@Processed_kMT,			[Processed_kMT])
		--OR	[Processed_Pcnt]		<> COALESCE(@Processed_Pcnt,		[Processed_Pcnt])
		--OR	[Density_SG]			<> COALESCE(@Density_SG,			[Density_SG]));

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;