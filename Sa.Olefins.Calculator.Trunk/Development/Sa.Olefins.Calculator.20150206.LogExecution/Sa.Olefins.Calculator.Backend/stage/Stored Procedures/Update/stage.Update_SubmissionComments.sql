﻿CREATE PROCEDURE [stage].[Update_SubmissionComments]
(
	@SubmissionId			INT,

	@SubmissionComment		NVARCHAR(MAX)	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
				('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
	+ COALESCE(', @SubmissionComment:'	+ CONVERT(VARCHAR, @SubmissionComment),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[SubmissionComments]
	SET [SubmissionComment] = @SubmissionComment
	WHERE	[SubmissionId]	= @SubmissionId
		AND	@SubmissionComment <> ''
		AND	[SubmissionComment] <> COALESCE(@SubmissionComment, [SubmissionComment]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;