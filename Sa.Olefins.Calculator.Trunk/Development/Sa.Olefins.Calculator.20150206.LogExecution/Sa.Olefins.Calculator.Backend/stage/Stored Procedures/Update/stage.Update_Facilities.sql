﻿CREATE PROCEDURE [stage].[Update_Facilities]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@UnitCount				INT		= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @FacilityId:'		+ CONVERT(VARCHAR, @FacilityId))
		+ COALESCE(', @UnitCount:'		+ CONVERT(VARCHAR, @UnitCount),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[Facilities]
	SET	[Unit_Count]		= @UnitCount
	WHERE	[SubmissionId]	= @SubmissionId
		AND	[FacilityId]	= @FacilityId

		AND @UnitCount		>= 0

		AND	[Unit_Count]	<> COALESCE(@UnitCount, [Unit_Count]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;