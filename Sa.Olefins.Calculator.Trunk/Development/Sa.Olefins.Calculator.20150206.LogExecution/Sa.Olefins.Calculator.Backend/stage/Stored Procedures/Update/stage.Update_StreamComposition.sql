﻿CREATE PROCEDURE [stage].[Update_StreamComposition]
(
	@SubmissionId			INT,
	@StreamNumber			INT,
	@ComponentId			INT,

	@Component_WtPcnt		FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
				('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
			+ (', @StreamNumber:'		+ CONVERT(VARCHAR, @StreamNumber))
			+ (', @ComponentId:'		+ CONVERT(VARCHAR, @ComponentId))
	+ COALESCE(', @Component_WtPcnt:'	+ CONVERT(VARCHAR, @Component_WtPcnt),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters

BEGIN TRY

	UPDATE [stage].[StreamComposition]
	SET	[Component_WtPcnt]		= COALESCE(@Component_WtPcnt,	[Component_WtPcnt])
	WHERE	[SubmissionId]		= @SubmissionId
		AND	[StreamNumber]		= @StreamNumber
		AND	[ComponentId]		= @ComponentId
		AND	@Component_WtPcnt	>= 0.0
		AND	[Component_WtPcnt]	<> COALESCE(@Component_WtPcnt,	[Component_WtPcnt]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;