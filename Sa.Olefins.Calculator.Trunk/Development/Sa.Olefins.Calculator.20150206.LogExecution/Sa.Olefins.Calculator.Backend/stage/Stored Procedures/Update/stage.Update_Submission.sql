﻿CREATE PROCEDURE [stage].[Update_Submission]
(
	@SubmissionId			INT,

	@SubmissionName			NVARCHAR(42)	= NULL,
	@DateBeg				DATE			= NULL,
	@DateEnd				DATE			= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
			+ COALESCE(', @SubmissionName:'	+ CONVERT(VARCHAR, @SubmissionName),	'')
			+ COALESCE(', @DateBeg:'		+ CONVERT(VARCHAR, @DateBeg),			'')
			+ COALESCE(', @DateEnd:'		+ CONVERT(VARCHAR, @DateEnd),			'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[Submissions]
	SET [SubmissionName]	= COALESCE(@SubmissionName,	[SubmissionName]),
		[DateBeg]			= COALESCE(@DateBeg,		[DateBeg]),
		[DateEnd]			= COALESCE(@DateEnd,		[DateEnd])
	WHERE	[SubmissionId]	= @SubmissionId
		AND(@SubmissionName	<> ''
		OR	@DateBeg		IS NOT NULL
		OR	@DateEnd		IS NOT NULL)
		
		AND([SubmissionName]	<> COALESCE(@SubmissionName,	[SubmissionName])
		OR	[DateBeg]			<> COALESCE(@DateBeg,			[DateBeg])
		OR	[DateEnd]			<> COALESCE(@DateEnd,			[DateEnd]));

	RETURN @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;