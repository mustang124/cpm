﻿CREATE PROCEDURE [stage].[Update_FacilitiesTrains]
(
	@SubmissionId			INT,

	@Train_Count			INT	 = NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
		+ COALESCE(', @Train_Count:'	+ CONVERT(VARCHAR, @Train_Count),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[FacilitiesTrains]
	SET	[Train_Count]		= COALESCE(@Train_Count,	[Train_Count])
	WHERE	[SubmissionId]	= @SubmissionId
		AND	@Train_Count	>= 0
		AND	[Train_Count]	<> COALESCE(@Train_Count,	[Train_Count]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;