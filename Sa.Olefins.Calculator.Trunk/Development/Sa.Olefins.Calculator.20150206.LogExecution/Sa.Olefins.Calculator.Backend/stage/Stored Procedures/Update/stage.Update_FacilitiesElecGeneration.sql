﻿CREATE PROCEDURE [stage].[Update_FacilitiesElecGeneration]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@Capacity_MW			FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @FacilityId:'		+ CONVERT(VARCHAR, @FacilityId))
		+ COALESCE(', @Capacity_MW:'	+ CONVERT(VARCHAR, @Capacity_MW),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[FacilitiesElecGeneration]
	SET	[Capacity_MW]		= COALESCE(@Capacity_MW, [Capacity_MW])
	WHERE	[SubmissionId]	= @SubmissionId
		AND	[FacilityId]	= @FacilityId

		AND @Capacity_MW	>= 0.0
		
		AND	[Capacity_MW]	<> COALESCE(@Capacity_MW, [Capacity_MW]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;