﻿CREATE PROCEDURE [stage].[Update_Capacity]
(
	@SubmissionId		INT,
	@StreamId			INT,

	@Capacity_kMT		FLOAT	= NULL,
	@StreamDay_MTSD		FLOAT	= NULL,
	@Record_MTSD		FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ ('@StreamId:'		+ CONVERT(VARCHAR, @StreamId))
		+ COALESCE(', @Capacity_kMT:'	+ CONVERT(VARCHAR, @Capacity_kMT),		'')
		+ COALESCE(', @StreamDay_MTSD:'	+ CONVERT(VARCHAR, @StreamDay_MTSD),	'')
		+ COALESCE(', @Record_MTSD:'	+ CONVERT(VARCHAR, @Record_MTSD),		'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY
	
	UPDATE [stage].[Capacity]
	SET [Capacity_kMT]			= @Capacity_kMT,
		[StreamDay_MTSD]		= @StreamDay_MTSD,
		[Record_MTSD]			= @Record_MTSD

	WHERE	[SubmissionId]		= @SubmissionId
		AND	[StreamId]			= @StreamId

		AND(@Capacity_kMT		>= 0.0
		OR	@StreamDay_MTSD		>= 0.0
		OR	@Record_MTSD		>= 0.0);

		--AND([Capacity_kMT]		<> COALESCE(@Capacity_kMT,		[Capacity_kMT])
		--OR	[StreamDay_MTSD]	<> COALESCE(@StreamDay_MTSD,	[StreamDay_MTSD])
		--OR	[Record_MTSD]		<> COALESCE(@Record_MTSD,		[Record_MTSD]));

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;