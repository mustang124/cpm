﻿CREATE PROCEDURE [stage].[Update_StreamRecovered]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@Recovery_WtPcnt		FLOAT		= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
	DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @StreamNumber:'		+ CONVERT(VARCHAR, @StreamNumber))
		+ COALESCE(', @Recovery_WtPcnt:'	+ CONVERT(VARCHAR, @Recovery_WtPcnt),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[StreamRecovered]
	SET	[Recovered_WtPcnt]		= COALESCE(@Recovery_WtPcnt,	[Recovered_WtPcnt])
	WHERE	[SubmissionId]		= @SubmissionId
		AND	[StreamNumber]		= @StreamNumber
		AND	@Recovery_WtPcnt	>= 0.0
		AND	[Recovered_WtPcnt]	<> COALESCE(@Recovery_WtPcnt,	[Recovered_WtPcnt]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;