﻿CREATE PROCEDURE [stage].[Update_StreamRecycled]
(
	@SubmissionId			INT,
	@ComponentId			INT,

	@Recycled_WtPcnt		FLOAT		= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
				('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
			+ (', @ComponentId:'		+ CONVERT(VARCHAR, @ComponentId))
	+ COALESCE(', @Recycled_WtPcnt:'	+ CONVERT(VARCHAR, @Recycled_WtPcnt),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;


BEGIN TRY

	UPDATE [stage].[StreamRecycled]
	SET	[Recycled_WtPcnt]		= COALESCE(@Recycled_WtPcnt,	[Recycled_WtPcnt])
	WHERE	[SubmissionId]		= @SubmissionId
		AND	[ComponentId]		= @ComponentId
		AND	@Recycled_WtPcnt	>= 0.0
		AND	[Recycled_WtPcnt]	<> COALESCE(@Recycled_WtPcnt,	[Recycled_WtPcnt]);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;