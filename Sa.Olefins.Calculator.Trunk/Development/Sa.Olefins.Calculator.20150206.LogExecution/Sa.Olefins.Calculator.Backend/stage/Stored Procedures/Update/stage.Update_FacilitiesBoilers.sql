﻿CREATE PROCEDURE [stage].[Update_FacilitiesBoilers]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@Rate_kLbHr				FLOAT	= NULL,
	@Pressure_PSIg			FLOAT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @FacilityId:'		+ CONVERT(VARCHAR, @FacilityId))
		+ COALESCE(', @Rate_kLbHr:'		+ CONVERT(VARCHAR, @Rate_kLbHr),	'')
		+ COALESCE(', @Pressure_PSIg:'	+ CONVERT(VARCHAR, @Pressure_PSIg),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [stage].[FacilitiesBoilers]
	SET	[Rate_kLbHr]		= COALESCE(@Rate_kLbHr,		[Rate_kLbHr]),
		[Pressure_PSIg]		= COALESCE(@Pressure_PSIg,	[Pressure_PSIg])
	WHERE	[SubmissionId]	= @SubmissionId
		AND	[FacilityId]	= @FacilityId

		AND(@Rate_kLbHr		>= 0.0
		OR	@Pressure_PSIg	>= 0.0);
		
		--AND([Rate_kLbHr]	<> COALESCE(@Rate_kLbHr,	[Rate_kLbHr])
		--OR	[Pressure_PSIg]	<> COALESCE(@Pressure_PSIg,	[Pressure_PSIg]));

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;