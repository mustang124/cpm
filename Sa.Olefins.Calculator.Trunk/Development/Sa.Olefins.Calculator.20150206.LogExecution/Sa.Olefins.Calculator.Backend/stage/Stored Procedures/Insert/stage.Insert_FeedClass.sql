﻿CREATE PROCEDURE [stage].[Insert_FeedClass]
(
	@SubmissionId			INT,

	@FeedClassId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
		+ COALESCE(', @FeedClassId:'	+ CONVERT(VARCHAR, @FeedClassId),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [stage].[FeedClass]([SubmissionId], [FeedClassId])
	SELECT
		@SubmissionId,
		@FeedClassId
	WHERE	@FeedClassId > 0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;