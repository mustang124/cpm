﻿CREATE PROCEDURE [stage].[Insert_StreamCompositionMol]
(
	@SubmissionId			INT,
	@StreamNumber			INT,
	@ComponentId			INT,

	@Component_MolPcnt		FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @StreamNumber:'		+ CONVERT(VARCHAR, @StreamNumber))
				+ (', @ComponentId:'		+ CONVERT(VARCHAR, @ComponentId))
		+ COALESCE(', @Component_MolPcnt:'	+ CONVERT(VARCHAR, @Component_MolPcnt),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [stage].[StreamCompositionMol]([SubmissionId], [StreamNumber], [ComponentId], [Component_MolPcnt])
	SELECT
		@SubmissionId,
		@StreamNumber,
		@ComponentId,
		@Component_MolPcnt
	WHERE	@Component_MolPcnt >= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;