﻿CREATE PROCEDURE [stage].[Insert_StreamDensity]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@Density_SG				FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @StreamNumber:'	+ CONVERT(VARCHAR, @StreamNumber))
		+ COALESCE(', @Density_SG:'		+ CONVERT(VARCHAR, @Density_SG),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [stage].[StreamDensity]([SubmissionId], [StreamNumber], [Density_SG])
	SELECT
		@SubmissionId,
		@StreamNumber,
		@Density_SG
	WHERE	@Density_SG >= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;