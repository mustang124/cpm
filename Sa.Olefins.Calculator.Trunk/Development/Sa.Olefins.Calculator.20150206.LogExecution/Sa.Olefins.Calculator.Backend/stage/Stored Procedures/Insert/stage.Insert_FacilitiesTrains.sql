﻿CREATE PROCEDURE [stage].[Insert_FacilitiesTrains]
(
	@SubmissionId			INT,

	@Train_Count			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
		+ COALESCE(', @Train_Count:'	+ CONVERT(VARCHAR, @Train_Count),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [stage].[FacilitiesTrains]([SubmissionId], [Train_Count])
	SELECT
		@SubmissionId,
		@Train_Count
	WHERE	@Train_Count >= 0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;