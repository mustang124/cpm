﻿CREATE PROCEDURE [stage].[Insert_FacilitiesElecGeneration]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@Capacity_MW			FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @FacilityId:'		+ CONVERT(VARCHAR, @FacilityId))
		+ COALESCE(', @Capacity_MW:'	+ CONVERT(VARCHAR, @Capacity_MW),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [stage].[FacilitiesElecGeneration]([SubmissionId], [FacilityId], [Capacity_MW])
	SELECT
		@SubmissionId,
		@FacilityId,
		@Capacity_MW
	WHERE	@Capacity_MW >= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;