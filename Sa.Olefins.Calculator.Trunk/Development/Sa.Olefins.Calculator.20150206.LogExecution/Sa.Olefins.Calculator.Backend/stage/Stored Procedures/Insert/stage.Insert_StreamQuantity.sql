﻿CREATE PROCEDURE [stage].[Insert_StreamQuantity]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamTypeId			INT,
	@Quantity_kMT			FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @StreamNumber:'	+ CONVERT(VARCHAR, @StreamNumber))
		+ COALESCE(', @StreamTypeId:'	+ CONVERT(VARCHAR, @StreamTypeId),	'')
		+ COALESCE(', @Quantity_kMT:'	+ CONVERT(VARCHAR, @Quantity_kMT),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [stage].[StreamQuantity]([SubmissionId], [StreamNumber], [StreamId], [Quantity_kMT])
	SELECT
		@SubmissionId,
		@StreamNumber,
		@StreamTypeId,
		@Quantity_kMT
	WHERE	@Quantity_kMT >= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;