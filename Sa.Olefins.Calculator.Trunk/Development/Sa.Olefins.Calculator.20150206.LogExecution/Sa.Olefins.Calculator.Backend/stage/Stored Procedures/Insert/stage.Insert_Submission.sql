﻿CREATE PROCEDURE [stage].[Insert_Submission]
(
	@SubmissionName			NVARCHAR(42),
	@DateBeg				DATE,
	@DateEnd				DATE
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionName:'	+ CONVERT(VARCHAR, @SubmissionName))
		+ COALESCE(', @DateBeg:'		+ CONVERT(VARCHAR, @DateBeg),	'')
		+ COALESCE(', @DateEnd:'		+ CONVERT(VARCHAR, @DateEnd),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [stage].[Submissions]([SubmissionName], [DateBeg], [DateEnd])
	OUTPUT [INSERTED].[SubmissionId] INTO @Id([Id])
	SELECT
		@SubmissionName,
		@DateBeg,
		@DateEnd
	WHERE	@SubmissionName <> ''
		AND	@DateBeg IS NOT NULL
		AND	@DateEnd IS NOT NULL;

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;