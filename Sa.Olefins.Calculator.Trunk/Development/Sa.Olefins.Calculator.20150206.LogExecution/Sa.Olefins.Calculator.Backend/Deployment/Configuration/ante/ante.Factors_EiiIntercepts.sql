﻿PRINT 'INSERT INTO [ante].[Factors_EiiIntercepts]([MethodologyId], [HvcYield_Intercept], [Energy_Intercept])';

INSERT INTO [ante].[Factors_EiiIntercepts]([MethodologyId], [HvcYield_Intercept], [Energy_Intercept])
SELECT t.[MethodologyId], t.[HvcYield_Intercept], t.[Energy_Intercept]
FROM (VALUES
	(@MethodologyId, 0.8631, 6.4880)
	)t([MethodologyId], [HvcYield_Intercept], [Energy_Intercept]);