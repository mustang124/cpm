﻿PRINT 'INSERT INTO [ante].[Factors_EiiCoefficients]([MethodologyId], [StreamId], [HvcYield_Coefficient], [Energy_Coefficient])';

INSERT INTO [ante].[Factors_EiiCoefficients]([MethodologyId], [StreamId], [HvcYield_Coefficient], [Energy_Coefficient])
SELECT t.[MethodologyId], t.[StreamId], t.[HvcYield_Coefficient], t.[Energy_Coefficient]
FROM (VALUES
	(@MethodologyId, [dim].[Return_StreamId]('Ethane'),		NULL,		NULL),
	(@MethodologyId, [dim].[Return_StreamId]('Propane'),	-0.1937,	-1.743),
	(@MethodologyId, [dim].[Return_StreamId]('Butane'),		-0.2886,	-2.312),
	(@MethodologyId, [dim].[Return_StreamId]('Condensate'),	-0.3252,	-2.489),
	(@MethodologyId, [dim].[Return_StreamId]('HeavyNGL'),	-0.3252,	-2.489),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaLt'),	-0.2586,	-2.296),
	(@MethodologyId, [dim].[Return_StreamId]('Raffinate'),	-0.2586,	-2.296),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaFr'),	-0.2888,	-2.435),
	(@MethodologyId, [dim].[Return_StreamId]('NaphthaHv'),	-0.3252,	-2.53),
	(@MethodologyId, [dim].[Return_StreamId]('Diesel'),		-0.3401,	-2.696),
	(@MethodologyId, [dim].[Return_StreamId]('GasOilHv'),	-0.4199,	-2.914),
	(@MethodologyId, [dim].[Return_StreamId]('GasOilHt'),	-0.3191,	-2.518),
	(@MethodologyId, [dim].[Return_StreamId]('Methane'),	-1.1,		NULL)
	)t([MethodologyId], [StreamId], [HvcYield_Coefficient], [Energy_Coefficient]);