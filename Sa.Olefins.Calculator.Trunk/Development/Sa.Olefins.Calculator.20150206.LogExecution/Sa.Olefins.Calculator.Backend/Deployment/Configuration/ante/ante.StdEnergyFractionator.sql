﻿PRINT 'INSERT INTO [ante].[StdEnergyFractionator]([MethodologyId], [StreamId], [Energy_kBtuBbl])';

INSERT INTO [ante].[StdEnergyFractionator]([MethodologyId], [StreamId], [Energy_kBtuBbl])
SELECT t.[MethodologyId], t.[StreamId], t.[Energy_kBtuBbl]
FROM (VALUES
	(@MethodologyId, dim.Return_StreamId('EPMix'),		60.0),
	(@MethodologyId, dim.Return_StreamId('LPG'),		50.0),
	(@MethodologyId, dim.Return_StreamId('Naphtha'),	55.0),
	(@MethodologyId, dim.Return_StreamId('Condensate'), 55.0),
	(@MethodologyId, dim.Return_StreamId('LiqHeavy'),	55.0)
	)t([MethodologyId], [StreamId], [Energy_kBtuBbl]);