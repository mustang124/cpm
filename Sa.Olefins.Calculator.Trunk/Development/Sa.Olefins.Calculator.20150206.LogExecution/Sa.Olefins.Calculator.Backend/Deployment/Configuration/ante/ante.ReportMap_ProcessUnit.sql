﻿PRINT 'INSERT INTO [ante].[ReportMap_ProcessUnit]([MethodologyId], [ProcessUnitId], [Report_Suffix])';

INSERT INTO [ante].[ReportMap_ProcessUnit]([MethodologyId], [ProcessUnitId], [Report_Suffix])
SELECT t.[MethodologyId], t.[ProcessUnitId], t.[Report_Suffix]
FROM (VALUES
	(@MethodologyId, dim.Return_ProcessUnitId('Fresh'),					'StdConfigEandP'),
	(@MethodologyId, dim.Return_ProcessUnitId('Supp'),					'SuppFeedsEandP'),
	(@MethodologyId, dim.Return_ProcessUnitId('Auxiliary'),				'Auxiliary'),
	(@MethodologyId, dim.Return_ProcessUnitId('Reduction'),				'Reduction'),
	(@MethodologyId, dim.Return_ProcessUnitId('Utilities'),				'Utilities'),
	(@MethodologyId, dim.Return_ProcessUnitId('Total'),					'Total')
	)t([MethodologyId], [ProcessUnitId], [Report_Suffix]);