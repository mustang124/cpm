﻿PRINT 'INSERT INTO [dim].[FeedClass_LookUp]([FeedClassTag], [FeedClassName], [FeedClassDetail])';

INSERT INTO [dim].[FeedClass_LookUp]([FeedClassTag], [FeedClassName], [FeedClassDetail])
SELECT t.[FeedClassTag], t.[FeedClassName], t.[FeedClassDetail]
FROM (VALUES
	('0', 'All', 'All Feed Classes'),
	('1', 'Ethane', 'Ethane (>85%)'),
	('2', 'LPG Feeds', 'LPG Feeds (C2+C3+C4 > 50%; C2<85%)'),
	('3', 'Mixed Feeds', 'Mixed Feeds (>5% gas oil, >10% ethane, propane and naphtha flexibility)'),
	('4', 'Naphtha', 'Naphtha (LIQ. FEEDS > 85%; G.O. < 25% & S.G. OF NAPH < 0.70)')
	)t([FeedClassTag], [FeedClassName], [FeedClassDetail]);

PRINT 'INSERT INTO [dim].[FeedClass_Parent]([MethodologyId], [FeedClassId], [ParentId], [Operator], [SortKey], [Hierarchy])';

INSERT INTO [dim].[FeedClass_Parent]([MethodologyId], [FeedClassId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT
	m.MethodologyId,
	l.FeedClassId,
	p.FeedClassId,
	t.Operator,
	t.SortKey,
	'/'
FROM (VALUES
	('0', '0', '+', 0),
	('1', '0', '+', 1),
	('2', '0', '+', 2),
	('3', '0', '+', 3),
	('4', '0', '+', 4)
	)	t(FeedClassTag, ParentTag, Operator, SortKey)
INNER JOIN [dim].[FeedClass_LookUp]			l
	ON	l.FeedClassTag = t.FeedClassTag
INNER JOIN [dim].[FeedClass_LookUp]			p
	ON	p.FeedClassTag = t.ParentTag
INNER JOIN [ante].[Methodology]				m
	ON	m.[MethodologyTag] = '2013';

EXECUTE dim.Update_Parent 'dim', 'FeedClass_Parent', 'MethodologyId', 'FeedClassId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE dim.Merge_Bridge 'dim', 'FeedClass_Parent', 'dim', 'FeedClass_Bridge', 'MethodologyId', 'FeedClassId', 'SortKey', 'Hierarchy', 'Operator';