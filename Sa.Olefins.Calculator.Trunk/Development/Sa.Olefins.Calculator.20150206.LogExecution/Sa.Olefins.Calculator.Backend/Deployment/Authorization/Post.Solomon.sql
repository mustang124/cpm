﻿PRINT 'Post.Solomon.sql';

INSERT INTO [auth].[JoinCompanyLogin]([CompanyId], [LoginId])
SELECT DISTINCT
	c.[CompanyId],
	l.[LoginId]
FROM @Solomon					i
INNER JOIN [auth].[Logins]		l
	ON	l.[LoginTag] = i.[Email]
CROSS JOIN [auth].[Companies]	c;

INSERT INTO [auth].[JoinPlantLogin]([PlantId], [LoginId])
SELECT DISTINCT
	p.[PlantId],
	l.[LoginId]
FROM @Solomon					i
INNER JOIN [auth].[Logins]		l
	ON	l.[LoginTag] = i.[Email]
CROSS JOIN [auth].[Plants]		p;

INSERT INTO [auth].[JoinLoginSubmission]([LoginId], [SubmissionId])
SELECT DISTINCT
	l.[LoginId],
	s.[SubmissionId]
FROM @Solomon						i
INNER JOIN	[auth].[Logins]			l
	ON	l.[LoginTag]		= i.[Email]
CROSS JOIN	[stage].[Submissions]	s;