﻿PRINT 'Seed.Companies.sql';

DECLARE @Companies TABLE
(
	[Email]				VARCHAR(128)	NOT	NULL	CHECK ([Email]			<> ''),
	[NameFirst]			VARCHAR(128)	NOT	NULL	CHECK ([NameFirst]		<> ''),
	[NameLast]			VARCHAR(128)	NOT	NULL	CHECK ([NameLast]		<> ''),
	[CompanyTag]		VARCHAR(42)		NOT	NULL	CHECK ([CompanyTag]		<> ''),
	[CompanyName]		VARCHAR(84)		NOT	NULL	CHECK ([CompanyName]	<> ''),
	[CompanyDetail]		VARCHAR(256)	NOT	NULL	CHECK ([CompanyDetail]	<> ''),
	[PlantName]			VARCHAR(128)	NOT	NULL	CHECK ([PlantName]		<> ''),
	[Refnum]			VARCHAR(128)	NOT	NULL	CHECK ([Refnum]			<> ''),
	[RoleId]			INT				NOT	NULL,

	PRIMARY KEY CLUSTERED([Email] ASC, [PlantName] ASC)
);

INSERT INTO @Companies([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId])
SELECT t.[Email], t.[NameFirst], t.[NameLast], t.[CompanyTag], t.[CompanyName], t.[CompanyDetail], t.[PlantName], t.[Refnum], t.[RoleId]
FROM(VALUES
	--('sonja.giesa@basf.com', 'Sonja', 'Giesa', 'BASF', 'BASF', 'BASF', 'Ludwigshafen', '2013PCH003', 3),
	--('sonja.giesa@basf.com', 'Sonja', 'Giesa', 'BASF', 'BASF', 'BASF', 'Antwerpen', '2013PCH115', 3),
	--('george.franklin@basf.com', 'George', 'Franklin', 'BASF TOTAL', 'BASF TOTAL', 'BASF TOTAL', 'BASF Port Arthur', '2013PCH162', 3),
	--('zhaojq@basf-ypc.com.cn', 'Jianqing', 'Zhao', 'BASF YPC', 'BASF YPC', 'BASF YPC', 'Nanjing', '2013PCH196', 3),
	--('elisabeth.ryding@borealisgroup.com', 'Elisabeth', 'Ryding', 'BOREALIS', 'BOREALIS', 'BOREALIS', 'Stenungsund', '2013PCH028', 3),
	--('elisabeth.ryding@borealisgroup.com', 'Elisabeth', 'Ryding', 'BOREALIS', 'BOREALIS', 'BOREALIS', 'Porvoo', '2013PCH077', 3),
	--('santosh.kolipaka@borouge.com', 'Santosh Kumar', 'Kolipaka', 'BOROUGE', 'BOROUGE', 'BOROUGE', 'Ruwais #1', '2013PCH163', 3),
	--('santosh.kolipaka@borouge.com', 'Santosh Kumar', 'Kolipaka', 'BOROUGE', 'BOROUGE', 'BOROUGE', 'Ruwais #2', '2013PCH233', 3),
	--('helmi@capcx.com', 'Helmilus', 'Moesa', 'CHANDRA ASRI', 'CHANDRA ASRI', 'CHANDRA ASRI', 'Cilegon', '2013PCH234', 3),
	--('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'CP Cedar Bayou', '2013PCH004', 3),
	--('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'CP Port Arthur', '2013PCH005', 3),
	--('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'CP Qchem Mesaieed', '2013PCH184', 3),
	--('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'CP Ras Laffan', '2013PCH222', 3),
	--('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'CP Sweeny 22', '2013PCH23A', 3),
	--('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'CP Sweeny 24', '2013PCH23B', 3),
	--('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'CP Sweeny 33', '2013PCH23C', 3),
	--('deridder.ronny@cnoocshell.com', 'Ronny', 'De Ridder', 'CSPC', 'CSPC', 'CSPC', 'Huizhou', '2013PCH197', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Tarragona', '2013PCH045', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Terneuzen 1', '2013PCH046', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Terneuzen 2', '2013PCH047', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Freeport 7', '2013PCH057', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Plaquemine 2', '2013PCH058', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Plaquemine 3', '2013PCH059', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW St Charles', '2013PCH082', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Bahia Blanca 1', '2013PCH097', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Freeport 8', '2013PCH099', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Ft Saskatchewan', '2013PCH100', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Boehlen', '2013PCH112', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Terneuzen 3', '2013PCH164', 3),
	--('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'DOW Bahia Blanca 2', '2013PCH165', 3),
	--('jonathan.n.pickering@dupont.com', 'Jonathan', 'Pickering', 'DUPONT', 'DUPONT', 'DUPONT', 'Orange', '2013PCH092', 3),
	--('vangalss@equate.com', 'Sarma', 'Vangala', 'EQUATE', 'EQUATE', 'EQUATE', 'Kuwait Olefins 1', '2013PCH148', 3),
	--('vangalss@equate.com', 'Sarma', 'Vangala', 'EQUATE', 'EQUATE', 'EQUATE', 'Kuwait Olefins 2', '2013PCH215', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop', '2013PCH011', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baton Rouge', '2013PCH012', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Ndg', '2013PCH013', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Fife', '2013PCH015', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Beaumont', '2013PCH018', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Sarnia', '2013PCH076', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop-X', '2013PCH133', 3),
	--('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Singapore', '2013PCH159', 3),
	--('troyh@ftpc.fpcusa.com', 'Troy', 'Hardegree', 'FORMOSA', 'FORMOSA', 'FORMOSA', 'Point Comfort', '2013PCH119', 3),
	--('liheping@fjrep.com', 'He-Ping', 'Li', 'FREP', 'FREP', 'FREP', 'Fujian', '2013PCH209', 3),
	--('sharmarishi@indianoil.in', 'Rishi', 'Sharma', 'IOCL', 'IOCL', 'IOCL', 'Panipat', '2013PCH223', 3),
	--('wiwat.n@irpc.co.th', 'Wiwat', 'Narach', 'IRPC', 'IRPC', 'IRPC', 'IRPC Rayong', '2013PCH198', 3),
	--('takushi.nagashima@noe.jx-group.co.jp', 'Takushi', 'Nagashima', 'JX NIPPON', 'JX NIPPON', 'JX NIPPON', 'JXN Kawasaki', '2013PCH157', 3),
	--('alexander.kapustin@honeywell.com', 'Alexander', 'Kapustin', 'LUKOIL', 'LUKOIL', 'LUKOIL', 'Stavrolen', '2013PCH217', 3),
	--('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Berre', '2013PCH025', 3),
	--('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Corpus Christi', '2013PCH036', 3),
	--('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Wesseling Om4', '2013PCH055', 3),
	--('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Wesseling Om6', '2013PCH056', 3),
	--('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Morris', '2013PCH087', 3),
	--('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Clinton', '2013PCH088', 3),
	--('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Munchsmunster', '2013PCH094', 3),
	--('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Channelview Op-1', '2013PCH16A', 3),
	--('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Channelview Op-2', '2013PCH16B', 3),
	--('thanitw@scg.co.th', 'Thanit', 'Wangruangsatit', 'MOC', 'MOC', 'MOC', 'Map Tha Phut', '2013PCH228', 3),
	--('thomas.svolba@omv.com', 'Thomas', 'Svolba', 'OMV', 'OMV', 'OMV', 'Schwechat', '2013PCH020', 3),
	--('thomas.svolba@omv.com', 'Thomas', 'Svolba', 'OMV', 'OMV', 'OMV', 'Burghausen', '2013PCH033', 3),
	--('lee.wai.chyn@pcs-chem.com.sg', 'Lee', 'Wai Chyn', 'PCS', 'PCS', 'PCS', 'Singapore 1', '2013PCH053', 3),
	--('lee.wai.chyn@pcs-chem.com.sg', 'Lee', 'Wai Chyn', 'PCS', 'PCS', 'PCS', 'Singapore 2', '2013PCH144', 3),
	--('leszek.slawinski@orlen.pl', 'Leszek', 'Slawinski', 'PKN', 'PKN', 'PKN', 'Plock', '2013PCH199', 3),
	--('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #1', '2013PCH083', 3),
	--('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #2-1', '2013PCH113', 3),
	--('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #2-2', '2013PCH190', 3),
	--('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #3', '2013PCH221', 3),
	--('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Hazira', '2013PCH122', 3),
	--('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Nagothane', '2013PCH131', 3),
	--('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Vadodara', '2013PCH167', 3),
	--('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Gandhar', '2013PCH168', 3),
	--('ffernandezl@repsol.com', 'Felipe', 'Fernandez Lores', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Puertollano', '2013PCH041', 3),
	--('ffernandezl@repsol.com', 'Felipe', 'Fernandez Lores', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Sines', '2013PCH078', 3),
	--('ffernandezl@repsol.com', 'Felipe', 'Fernandez Lores', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Tarragona', '2013PCH079', 3),
	--('kasidida@scg.co.th', 'Kasidid', 'Asumpinpong', 'ROC', 'ROC', 'ROC', 'ROC Rayong', '2013PCH145', 3),
	--('rainer.auberger@bpge.de', 'Rainer', 'Auberger', 'RUHR OEL', 'RUHR OEL', 'RUHR OEL', 'Gelsenkirchen 3', '2013PCH095', 3),
	--('rainer.auberger@bpge.de', 'Rainer', 'Auberger', 'RUHR OEL', 'RUHR OEL', 'RUHR OEL', 'Gelsenkirchen 4', '2013PCH096', 3),
	--('harbiam3@sadaf.sabic.com', 'Ahmed Mesfer', 'Al-Harbi', 'SABIC', 'SABIC', 'SABIC', 'Sadaf', '2013PCH141', 3),
	--('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC', 'SABIC', 'SABIC', 'Pk1', '2013PCH169', 3),
	--('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC', 'SABIC', 'SABIC', 'Pk2', '2013PCH170', 3),
	--('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC', 'SABIC', 'SABIC', 'Pk3', '2013PCH171', 3),
	--('asmarima@kemya.sabic.com', 'Mohammed', 'Al-Asmari', 'SABIC', 'SABIC', 'SABIC', 'Kemya', '2013PCH172', 3),
	--('badrosom@yanpet.sabic.com', 'Omar', 'Badros', 'SABIC', 'SABIC', 'SABIC', 'Yanpet 1', '2013PCH173', 3),
	--('badrosom@yanpet.sabic.com', 'Omar', 'Badros', 'SABIC', 'SABIC', 'SABIC', 'Yanpet 2', '2013PCH174', 3),
	--('mutairiyb@united.sabic.com', 'Yasser Battal', 'Al-Mutairi', 'SABIC', 'SABIC', 'SABIC', 'United', '2013PCH191', 3),
	--('khalileh@sharq.sabic.com', 'Ezzat', 'Khalil', 'SABIC', 'SABIC', 'SABIC', 'Sharq', '2013PCH230', 3),
	--('thotakurapr@saudikayan.sabic.com', 'Prasada', 'Thotakura', 'SABIC', 'SABIC', 'SABIC', 'Saudi Kayan', '2013PCH231', 3),
	--('qarnimali@yansab.sabic.com', 'Manea', 'Al-Qarni', 'SABIC', 'SABIC', 'SABIC', 'Yansab', '2013PCH232', 3),
	--('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Wilton 6', '2013PCH068', 3),
	--('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 3', '2013PCH084', 3),
	--('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 4', '2013PCH085', 3),
	--('laurentius.na@samsung.com', 'Laurentiius', 'Na', 'SAMSUNG TOTAL', 'SAMSUNG TOTAL', 'SAMSUNG TOTAL', 'Daesan', '2013PCH175', 3),
	--('trobie.thompson@us.sasol.com', 'Trobie', 'Thompson', 'SASOL NA', 'SASOL NA', 'SASOL NA', 'Westlake', '2013PCH106', 3),
	--('huang.xuefeng@secco.com.cn', 'Huang', 'Xuefeng', 'SECCO', 'SECCO', 'SECCO', 'Shanghai', '2013PCH195', 3),
	--('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Moerdijk', '2013PCH026', 3),
	--('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Rheinland', '2013PCH043', 3),
	--('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Norco', '2013PCH089', 3),
	--('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Deer Park', '2013PCH091', 3),
	--('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Pulau Bukom', '2013PCH235', 3),
	--('alexander.kapustin@honeywell.com', 'Alexander', 'Kapustin', 'SIBUR', 'SIBUR', 'SIBUR', 'Nizhni Novgorod', '2013PCH213', 3),
	--('alexander.kapustin@honeywell.com', 'Alexander', 'Kapustin', 'SIBUR', 'SIBUR', 'SIBUR', 'Tomsk', '2013PCH214', 3),
	--('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Shanghai2#', '2013PCH128', 3),
	--('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Yangzi', '2013PCH149', 3),
	--('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Maoming', '2013PCH152', 3),
	--('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Qilu', '2013PCH153', 3),
	--('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Yanshan', '2013PCH154', 3),
	--('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Zhongsha', '2013PCH224', 3),
	--('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Zhenhai', '2013PCH225', 3),
	--('jozef.miklos@slovnaft.sk', 'Jozef', 'Miklos', 'SLOVNAFT', 'SLOVNAFT', 'SLOVNAFT', 'Bratislava', '2013PCH207', 3),
	--('r.annapureddy@tasnee.com', 'Ramireddy', 'Annapureddy', 'TASNEE', 'TASNEE', 'TASNEE', 'Al Jubail', '2013PCH212', 3),
	--('hirokazu.tsukamoto@tonengeneral.co.jp', 'Hirokazu', 'Tsukamoto', 'TONEN', 'TONEN', 'TONEN', 'Tonen Kawasaki', '2013PCH105', 3),
	--('laurent.bourgeois@total.com', 'Laurent', 'Bourgeois', 'TOTAL', 'TOTAL', 'TOTAL', 'Gonfreville', '2013PCH042', 3),
	--('laurent.bourgeois@total.com', 'Laurent', 'Bourgeois', 'TOTAL', 'TOTAL', 'TOTAL', 'Antwerp Nc3', '2013PCH049', 3),
	--('laurent.bourgeois@total.com', 'Laurent', 'Bourgeois', 'TOTAL', 'TOTAL', 'TOTAL', 'Antwerp Nc2', '2013PCH50B', 3),
	--('koncsik@tvk.hu', 'Joszef', 'Koncsik', 'TVK', 'TVK', 'TVK', 'Tvk 1', '2013PCH205', 3),
	--('koncsik@tvk.hu', 'Joszef', 'Koncsik', 'TVK', 'TVK', 'TVK', 'Tvk 2', '2013PCH206', 3),
	--('leszek.slawinski@orlen.pl', 'Leszek', 'Slawinski', 'UNIPETROL', 'UNIPETROL', 'UNIPETROL', 'Litvinov', '2013PCH208', 3),
	--('michela.frisieri@versalis.eni.com', 'Michela', 'Frisieri', 'VERSALIS', 'VERSALIS', 'VERSALIS', 'Dunkerque', '2013PCH070', 3),
	--('michela.frisieri@versalis.eni.com', 'Michela', 'Frisieri', 'VERSALIS', 'VERSALIS', 'VERSALIS', 'Brindisi', '2013PCH111', 3)

	('sonja.giesa@basf.com', 'Sonja', 'Giesa', 'BASF', 'BASF', 'BASF', 'Ludwigshafen', '2013PCH003', 3),
	('sonja.giesa@basf.com', 'Sonja', 'Giesa', 'BASF', 'BASF', 'BASF', 'Antwerpen', '2013PCH115', 3),
	('george.franklin@basf.com', 'George', 'Franklin', 'BASF TOTAL', 'BASF TOTAL', 'BASF TOTAL', 'Basf Total Port Arthur', '2013PCH162', 3),
	('zhaojq@basf-ypc.com.cn', 'Jianqing', 'Zhao', 'BASF YPC', 'BASF YPC', 'BASF YPC', 'Nanjing', '2013PCH196', 3),
	('elisabeth.ryding@borealisgroup.com', 'Elisabeth', 'Ryding', 'BOREALIS', 'BOREALIS', 'BOREALIS', 'Stenungsund', '2013PCH028', 3),
	('elisabeth.ryding@borealisgroup.com', 'Elisabeth', 'Ryding', 'BOREALIS', 'BOREALIS', 'BOREALIS', 'Porvoo', '2013PCH077', 3),
	('santosh.kolipaka@borouge.com', 'Santosh Kumar', 'Kolipaka', 'BOROUGE', 'BOROUGE', 'BOROUGE', 'Ruwais #1', '2013PCH163', 3),
	('santosh.kolipaka@borouge.com', 'Santosh Kumar', 'Kolipaka', 'BOROUGE', 'BOROUGE', 'BOROUGE', 'Ruwais #2', '2013PCH233', 3),
	('helmi@capcx.com', 'Helmilus', 'Moesa', 'CHANDRA ASRI', 'CHANDRA ASRI', 'CHANDRA ASRI', 'Cilegon', '2013PCH234', 3),
	('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'Cp Cedar Bayou', '2013PCH004', 3),
	('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'Cp Port Arthur', '2013PCH005', 3),
	('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'Cp Qchem Mesaieed', '2013PCH184', 3),
	('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'Cp Ras Laffan', '2013PCH222', 3),
	('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'Cp Sweeny 22', '2013PCH23A', 3),
	('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'Cp Sweeny 24', '2013PCH23B', 3),
	('svatekm@cpchem.com', 'Kristen', 'Brown', 'CP', 'CP', 'CP', 'Cp Sweeny 33', '2013PCH23C', 3),
	('deridder.ronny@cnoocshell.com', 'Ronny', 'De Ridder', 'CSPC', 'CSPC', 'CSPC', 'Huizhou', '2013PCH197', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Tarragona', '2013PCH045', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Terneuzen 1', '2013PCH046', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Terneuzen 2', '2013PCH047', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Freeport 7', '2013PCH057', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Plaquemine 2', '2013PCH058', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Plaquemine 3', '2013PCH059', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow St Charles', '2013PCH082', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Bahia Blanca 1', '2013PCH097', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Freeport 8', '2013PCH099', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Ft Saskatchewan', '2013PCH100', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Boehlen', '2013PCH112', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Terneuzen 3', '2013PCH164', 3),
	('bentonjh@dow.com', 'Jim', 'Benton', 'DOW', 'DOW', 'DOW', 'Dow Bahia Blanca 2', '2013PCH165', 3),
	('jonathan.n.pickering@dupont.com', 'Jonathan', 'Pickering', 'DUPONT', 'DUPONT', 'DUPONT', 'Orange', '2013PCH092', 3),
	('vangalss@equate.com', 'Sarma', 'Vangala', 'EQUATE', 'EQUATE', 'EQUATE', 'Kuwait Olefins 1', '2013PCH148', 3),
	('vangalss@equate.com', 'Sarma', 'Vangala', 'EQUATE', 'EQUATE', 'EQUATE', 'Kuwait Olefins 2', '2013PCH215', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop', '2013PCH011', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop', '2013PCH011', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baton Rouge', '2013PCH012', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baton Rouge', '2013PCH012', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Ndg', '2013PCH013', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Ndg', '2013PCH013', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Fife', '2013PCH015', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Fife', '2013PCH015', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Beaumont', '2013PCH018', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Beaumont', '2013PCH018', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Sarnia', '2013PCH076', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Sarnia', '2013PCH076', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop-X', '2013PCH133', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Baytown Bop-X', '2013PCH133', 3),
	('karthik.senthilnathan@esso.com', 'Karthik', 'Senthilnathan', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Singapore', '2013PCH159', 3),
	('james.d.davison@exxonmobil.com', 'James', 'Davison', 'EXXONMOBIL', 'EXXONMOBIL', 'EXXONMOBIL', 'Singapore', '2013PCH159', 3),
	('troyh@ftpc.fpcusa.com', 'Troy', 'Hardegree', 'FORMOSA', 'FORMOSA', 'FORMOSA', 'Point Comfort', '2013PCH119', 3),
	('liheping@fjrep.com', 'He-Ping', 'Li', 'FREP', 'FREP', 'FREP', 'Fujian', '2013PCH209', 3),
	('sharmarishi@indianoil.in', 'Rishi', 'Sharma', 'IOCL', 'IOCL', 'IOCL', 'Panipat', '2013PCH223', 3),
	('wiwat.n@irpc.co.th', 'Wiwat', 'Narach', 'IRPC', 'IRPC', 'IRPC', 'Irpc Rayong', '2013PCH198', 3),
	('alexander.kapustin@honeywell.com', 'Alexander', 'Kapustin', 'LUKOIL', 'LUKOIL', 'LUKOIL', 'Stavrolen', '2013PCH217', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Berre', '2013PCH025', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Berre', '2013PCH025', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Corpus Christi', '2013PCH036', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Corpus Christi', '2013PCH036', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Wesseling Om4', '2013PCH055', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Wesseling Om4', '2013PCH055', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Wesseling Om6', '2013PCH056', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Wesseling Om6', '2013PCH056', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Morris', '2013PCH087', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Morris', '2013PCH087', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Clinton', '2013PCH088', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Clinton', '2013PCH088', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Munchsmunster', '2013PCH094', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Munchsmunster', '2013PCH094', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Channelview Op-1', '2013PCH16A', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Channelview Op-1', '2013PCH16A', 3),
	('leland.johnson@lyondellbasell.com', 'Leland', 'Johnson', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Channelview Op-2', '2013PCH16B', 3),
	('oliver.franzheim@lyondellbasell.com', 'Oliver', 'Franzheim', 'LYONDELLBASELL', 'LYONDELLBASELL', 'LYONDELLBASELL', 'Channelview Op-2', '2013PCH16B', 3),
	('thanitw@scg.co.th', 'Thanit', 'Wangruangsatit', 'MOC', 'MOC', 'MOC', 'Map Tha Phut', '2013PCH228', 3),
	('thomas.svolba@omv.com', 'Thomas', 'Svolba', 'OMV', 'OMV', 'OMV', 'Schwechat', '2013PCH020', 3),
	('thomas.svolba@omv.com', 'Thomas', 'Svolba', 'OMV', 'OMV', 'OMV', 'Burghausen', '2013PCH033', 3),
	('lee.wai.chyn@pcs-chem.com.sg', 'Lee', 'Wai Chyn', 'PCS', 'PCS', 'PCS', 'Singapore 1', '2013PCH053', 3),
	('lee.wai.chyn@pcs-chem.com.sg', 'Lee', 'Wai Chyn', 'PCS', 'PCS', 'PCS', 'Singapore 2', '2013PCH144', 3),
	('leszek.slawinski@orlen.pl', 'Leszek', 'Slawinski', 'PKN', 'PKN', 'PKN', 'Plock', '2013PCH199', 3),
	('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #1', '2013PCH083', 3),
	('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #2-1', '2013PCH113', 3),
	('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #2-2', '2013PCH190', 3),
	('pornchai.s@pttgcgroup.com', 'Pornchai', 'Sae-Chung', 'PTT', 'PTT', 'PTT', 'Map Ta Phut #3', '2013PCH221', 3),
	('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Hazira', '2013PCH122', 3),
	('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Nagothane', '2013PCH131', 3),
	('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Vadodara', '2013PCH167', 3),
	('amit.chaturvedi@ril.com', 'Amit', 'Chaturvedi', 'RELIANCE', 'RELIANCE', 'RELIANCE', 'Gandhar', '2013PCH168', 3),
	('ffernandezl@repsol.com', 'Felipe', 'Fernandez Lores', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Puertollano', '2013PCH041', 3),
	('ffernandezl@repsol.com', 'Felipe', 'Fernandez Lores', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Sines', '2013PCH078', 3),
	('ffernandezl@repsol.com', 'Felipe', 'Fernandez Lores', 'REPSOL', 'REPSOL', 'REPSOL', 'Repsol Tarragona', '2013PCH079', 3),
	('kasidida@scg.co.th', 'Kasidid', 'Asumpinpong', 'ROC', 'ROC', 'ROC', 'Roc Rayong', '2013PCH145', 3),
	('rainer.auberger@bpge.de', 'Rainer', 'Auberger', 'RUHR OEL', 'RUHR OEL', 'RUHR OEL', 'Gelsenkirchen 3', '2013PCH095', 3),
	('rainer.auberger@bpge.de', 'Rainer', 'Auberger', 'RUHR OEL', 'RUHR OEL', 'RUHR OEL', 'Gelsenkirchen 4', '2013PCH096', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC SADAF', 'SABIC SADAF', 'SABIC SADAF', 'Sadaf', '2013PCH141', 3),
	('harbiam3@sadaf.sabic.com', 'Ahmed Mesfer', 'Al-Harbi', 'SABIC SADAF', 'SABIC SADAF', 'SABIC SADAF', 'Sadaf', '2013PCH141', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk1', '2013PCH169', 3),
	('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk1', '2013PCH169', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk2', '2013PCH170', 3),
	('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk2', '2013PCH170', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk3', '2013PCH171', 3),
	('bugshanos@petrokemya.sabic.com', 'Omar', 'Bugshan', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'SABIC PETROKEMYA', 'Pk3', '2013PCH171', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC KEMYA', 'SABIC KEMYA', 'SABIC KEMYA', 'Kemya', '2013PCH172', 3),
	('asmarima@kemya.sabic.com', 'Mohammed', 'Al-Asmari', 'SABIC KEMYA', 'SABIC KEMYA', 'SABIC KEMYA', 'Kemya', '2013PCH172', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 1', '2013PCH173', 3),
	('badrosom@yanpet.sabic.com', 'Omar', 'Badros', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 1', '2013PCH173', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 2', '2013PCH174', 3),
	('badrosom@yanpet.sabic.com', 'Omar', 'Badros', 'SABIC YANPET', 'SABIC YANPET', 'SABIC YANPET', 'Yanpet 2', '2013PCH174', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC UNITED', 'SABIC UNITED', 'SABIC UNITED', 'United', '2013PCH191', 3),
	('mutairiyb@united.sabic.com', 'Yasser Battal', 'Al-Mutairi', 'SABIC UNITED', 'SABIC UNITED', 'SABIC UNITED', 'United', '2013PCH191', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC SHARQ', 'SABIC SHARQ', 'SABIC SHARQ', 'Sharq', '2013PCH230', 3),
	('khalileh@sharq.sabic.com', 'Ezzat', 'Khalil', 'SABIC SHARQ', 'SABIC SHARQ', 'SABIC SHARQ', 'Sharq', '2013PCH230', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'Saudi Kayan', '2013PCH231', 3),
	('thotakurapr@saudikayan.sabic.com', 'Prasada', 'Thotakura', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'SABIC SAUDI KAYAN', 'Saudi Kayan', '2013PCH231', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC YANSAB', 'SABIC YANSAB', 'SABIC YANSAB', 'Yansab', '2013PCH232', 3),
	('qarnimali@yansab.sabic.com', 'Manea', 'Al-Qarni', 'SABIC YANSAB', 'SABIC YANSAB', 'SABIC YANSAB', 'Yansab', '2013PCH232', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Wilton 6', '2013PCH068', 3),
	('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Wilton 6', '2013PCH068', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 3', '2013PCH084', 3),
	('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 3', '2013PCH084', 3),
	('ol1aam1@sabic.com', 'Abdulrazzaq', 'Al-Mulla', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 4', '2013PCH085', 3),
	('iain.macgregor@sabic-europe.com', 'Iain', 'Macgregor', 'SABIC EUR', 'SABIC EUR', 'SABIC EUR', 'Geleen Olefins 4', '2013PCH085', 3),
	('laurentius.na@samsung.com', 'Laurentiius', 'Na', 'SAMSUNG TOTAL', 'SAMSUNG TOTAL', 'SAMSUNG TOTAL', 'Daesan', '2013PCH175', 3),
	('trobie.thompson@us.sasol.com', 'Trobie', 'Thompson', 'SASOL NA', 'SASOL NA', 'SASOL NA', 'Westlake', '2013PCH106', 3),
	('huang.xuefeng@secco.com.cn', 'Huang', 'Xuefeng', 'SECCO', 'SECCO', 'SECCO', 'Shanghai', '2013PCH195', 3),
	('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Moerdijk', '2013PCH026', 3),
	('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Rheinland', '2013PCH043', 3),
	('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Norco', '2013PCH089', 3),
	('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Deer Park', '2013PCH091', 3),
	('rob.loonen@shell.com', 'Rob', 'Loonen', 'SHELL', 'SHELL', 'SHELL', 'Pulau Bukom', '2013PCH235', 3),
	('alexander.kapustin@honeywell.com', 'Alexander', 'Kapustin', 'SIBUR', 'SIBUR', 'SIBUR', 'Nizhni Novgorod', '2013PCH213', 3),
	('alexander.kapustin@honeywell.com', 'Alexander', 'Kapustin', 'SIBUR', 'SIBUR', 'SIBUR', 'Tomsk', '2013PCH214', 3),
	('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Shanghai2#', '2013PCH128', 3),
	('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Yangzi', '2013PCH149', 3),
	('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Maoming', '2013PCH152', 3),
	('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Qilu', '2013PCH153', 3),
	('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Yanshan', '2013PCH154', 3),
	('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Zhongsha', '2013PCH224', 3),
	('liurb.edri@sinopec.com', 'Liu', 'Ranbing', 'SINOPEC', 'SINOPEC', 'SINOPEC', 'Zhenhai', '2013PCH225', 3),
	('jozef.miklos@slovnaft.sk', 'Jozef', 'Miklos', 'SLOVNAFT', 'SLOVNAFT', 'SLOVNAFT', 'Bratislava', '2013PCH207', 3),
	('ahalzahrani@tasnee.com', 'Ahmad', 'Al Zahrani', 'TASNEE', 'TASNEE', 'TASNEE', 'Al Jubail', '2013PCH212', 3),
	('r.annapureddy@tasnee.com', 'Ramireddy', 'Annapureddy', 'TASNEE', 'TASNEE', 'TASNEE', 'Al Jubail', '2013PCH212', 3),
	('hirokazu.tsukamoto@tonengeneral.co.jp', 'Hirokazu', 'Tsukamoto', 'TONEN', 'TONEN', 'TONEN', 'Tonen Kawasaki', '2013PCH105', 3),
	('takushi.nagashima@noe.jx-group.co.jp', 'Takushi', 'Nagashima', 'JX NIPPON', 'JX NIPPON', 'JX NIPPON', 'Jx Nippon Kawasaki', '2013PCH157', 3),
	('laurent.bourgeois@total.com', 'Laurent', 'Bourgeois', 'TOTAL', 'TOTAL', 'TOTAL', 'Gonfreville', '2013PCH042', 3),
	('laurent.bourgeois@total.com', 'Laurent', 'Bourgeois', 'TOTAL', 'TOTAL', 'TOTAL', 'Antwerp Nc3', '2013PCH049', 3),
	('laurent.bourgeois@total.com', 'Laurent', 'Bourgeois', 'TOTAL', 'TOTAL', 'TOTAL', 'Antwerp Nc2', '2013PCH50B', 3),
	('koncsik@tvk.hu', 'Joszef', 'Koncsik', 'TVK', 'TVK', 'TVK', 'Tvk 1', '2013PCH205', 3),
	('koncsik@tvk.hu', 'Joszef', 'Koncsik', 'TVK', 'TVK', 'TVK', 'Tvk 2', '2013PCH206', 3),
	('leszek.slawinski@orlen.pl', 'Leszek', 'Slawinski', 'UNIPETROL', 'UNIPETROL', 'UNIPETROL', 'Litvinov', '2013PCH208', 3),
	('michela.frisieri@versalis.eni.com', 'Michela', 'Frisieri', 'VERSALIS', 'VERSALIS', 'VERSALIS', 'Dunkerque', '2013PCH070', 3),
	('michela.frisieri@versalis.eni.com', 'Michela', 'Frisieri', 'VERSALIS', 'VERSALIS', 'VERSALIS', 'Brindisi', '2013PCH111', 3)
	) t([Email], [NameFirst], [NameLast], [CompanyTag], [CompanyName], [CompanyDetail], [PlantName], [Refnum], [RoleId]);

INSERT INTO [dim].[Company_LookUp]([CompanyTag], [CompanyName], [CompanyDetail])
SELECT DISTINCT
	i.[CompanyTag],
	i.[CompanyName],
	i.[CompanyDetail]
FROM @Companies	i;

INSERT INTO [dim].[Company_Parent]([MethodologyId], [CompanyId], [ParentId], [Operator], [SortKey], [Hierarchy])
SELECT DISTINCT
	@MethodologyId,
	dim.Return_CompanyId(i.[CompanyTag]),
	dim.Return_CompanyId(i.[CompanyTag]),
	'~',
	ROW_NUMBER() OVER(ORDER BY i.[CompanyTag]),
	'/'
FROM (
	SELECT DISTINCT
		i.[CompanyTag]
	FROM @Companies	i
	) i;

EXECUTE dim.Update_Parent 'dim', 'Company_Parent', 'MethodologyId', 'CompanyId', 'ParentId', 'SortKey', 'Hierarchy';
EXECUTE dim.Merge_Bridge 'dim', 'Company_Parent', 'dim', 'Company_Bridge', 'MethodologyId', 'CompanyId', 'SortKey', 'Hierarchy', 'Operator';

INSERT INTO [auth].[Companies]([CompanyName], [CompanyRefId])
SELECT
	l.[CompanyName],
	l.[CompanyId]
FROM [dim].[Company_LookUp]	l;

INSERT INTO [auth].[Plants]([PlantName])
SELECT DISTINCT
	i.[PlantName]
FROM @Companies	i;

INSERT INTO [auth].[PlantCalculationLimits]([PlantId], [CalcsPerPeriod_Count])
SELECT
	p.[PlantId], 100
FROM [auth].[Plants] p;

INSERT INTO [auth].[Logins]([LoginTag])
SELECT DISTINCT
	i.[Email]
FROM @Companies	i;

INSERT INTO [auth].[LoginsAttributes]([LoginId], [NameLast], [NameFirst], [eMail], [RoleId])
SELECT DISTINCT
	l.[LoginId],
	i.[NameLast],
	i.[NameFirst],
	i.[Email],
	i.[RoleId]
FROM [auth].[Logins]	l
INNER JOIN @Companies		i
	ON	i.[Email] = l.[LoginTag];

INSERT INTO [auth].[LoginsPassWords]([LoginId], [pSalt], [pWord])
SELECT 
	l.[LoginId],
	0x2D228C933876329B03D86FF48431EF558C6F23B83BBA0037291D03E956037EC93835795D7F139E78E356E1F1210D52BA610B2054297B01C564C01F92CFD95EC67F6FD23D8CB645F3A0CCE866B0A336DD04379676EC371D37E76ED5A90529EDBA64E21326D5E73A9D338BE17DF6E87353B6378DA2C06B150D82A61B85C0BE34AC1D73C3B997651BED1A1745439CFAF1F9666D90105C09F31EF48CC3B7987A2973063DE784C8ED7D3723E761477498C09DAE7692A7EA72CF9E9E9E48527A4745267D72228C426789BC5E7DC124965EBD2308B2512AD1B9ECFEF9B69DDC9B94324A194834E1E8185363A9FF3D79503ECC1609922670E7D56D39F0770648C12F8BEF8CF1B0E10B7EEDFB5DAEE6820F8AC0D86A5828B794CE525517F992896A9A6D8958C6C70A834526D4FF25D101F28D4BB410D361667DE3BD761B30E1F570CD4CC8EA386401E409C24E3567A7A779B1376B72EBA15DD8B3CA88AE1DC6D4CFB6A3C2A912947EB336B380F2D2EA52D8804C06CA7BBB6E00DB1D4F1E589E443CC50BB5F1C7C9524C56D21A44AF1AA4E22E68B092ECC6039C2E2554E4918B0D366AD6B674CE4038291A727BD26FED2659C7844A19941CDF786CE953FCD96865AB7FA1D4433B52BB7695561E311A0C1D739D3039F04378B96B2972F675A7D73840A7198629D103CD26449BAAA9230AEDB043505D8CA365982F6548B85F1044B91C61C0FE,
	0xE4FF5A0165CDEFBC1961B796EEE0F33A2710AD184EA0A24C135C113EDA5B7995C902395460A33DB2EFEA1FBC7DE283B068B8E5FE013DEB9961921A05C6941F44
FROM [auth].[Logins]	l
LEFT OUTER JOIN [auth].[LoginsPassWords] p
	ON	p.[LoginId]	= l.[LoginId]
WHERE p.[LoginId] IS NULL;

INSERT INTO [auth].[JoinCompanyPlant]([CompanyId], [PlantId])
SELECT DISTINCT
	c.[CompanyId],
	p.[PlantId]
FROM @Companies					i
INNER JOIN [auth].[Companies]	c
	ON	c.[CompanyName]	= i.[CompanyName]
INNER JOIN [auth].[Plants]		p
	ON	p.[PlantName]	= i.[PlantName];

INSERT INTO [auth].[JoinCompanyLogin]([CompanyId], [LoginId])
SELECT DISTINCT
	c.[CompanyId],
	l.[LoginId]
FROM @Companies					i
INNER JOIN [auth].[Companies]	c
	ON	c.[CompanyName]	= i.[CompanyName]
INNER JOIN [auth].[Logins]		l
	ON	l.[LoginTag]	= i.[Email];

INSERT INTO [auth].[JoinPlantLogin]([PlantId], [LoginId])
SELECT DISTINCT
	p.[PlantId],
	l.[LoginId]
FROM @Companies					i
INNER JOIN [auth].[Plants]		p
	ON	p.[PlantName]	= i.[PlantName]
INNER JOIN [auth].[Logins]		l
	ON	l.[LoginTag]	= i.[Email];
