﻿CREATE PROCEDURE [auth].[Update_PlantCalculationLimits]
(
	@PlantId				INT,

	@CalcsPerPeriod_Count	INT	= NULL,
	@PeriodLen_Days			INT	= NULL
)
AS
BEGIN
	
SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
						('@PlantId:'	+ CONVERT(VARCHAR, @PlantId))
+ COALESCE(', @CalcsPerPeriod_Count:'	+ CONVERT(VARCHAR, @CalcsPerPeriod_Count),	'')
+ COALESCE(', @PeriodLen_Days:'			+ CONVERT(VARCHAR, @PeriodLen_Days),		'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [auth].[PlantCalculationLimits]
	SET [CalcsPerPeriod_Count]		= @CalcsPerPeriod_Count,
		[PeriodLen_Days]			= @PeriodLen_Days

	WHERE	[PlantId]				= @PlantId

		AND(@CalcsPerPeriod_Count	>= 0.0
		OR	@PeriodLen_Days			>= 0.0)

		AND([CalcsPerPeriod_Count]	<> COALESCE(@CalcsPerPeriod_Count,	[CalcsPerPeriod_Count])
		OR	[PeriodLen_Days]		<> COALESCE(@PeriodLen_Days,		[PeriodLen_Days]));

	RETURN @PlantId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO