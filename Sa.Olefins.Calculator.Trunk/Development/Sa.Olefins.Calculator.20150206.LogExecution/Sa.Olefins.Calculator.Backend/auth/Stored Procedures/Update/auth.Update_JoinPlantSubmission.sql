﻿CREATE PROCEDURE [auth].[Update_JoinPlantSubmission]
(
	@JoinId			INT	= NULL,
	@PlantId		INT	= NULL,
	@SubmissionId	INT	= NULL,

	@Active			BIT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
		  COALESCE(', @JoinId:'			+ CONVERT(VARCHAR, @JoinId),		'')
		+ COALESCE(', @PlantId:'		+ CONVERT(VARCHAR, @PlantId),		'')
		+ COALESCE(', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId),	'')
		+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),		'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE	[auth].[JoinPlantSubmission]
	SET		[Active]	= @Active
	WHERE	[JoinId]	= @JoinId
		AND @Active		IS NOT NULL;

	RETURN @JoinId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO