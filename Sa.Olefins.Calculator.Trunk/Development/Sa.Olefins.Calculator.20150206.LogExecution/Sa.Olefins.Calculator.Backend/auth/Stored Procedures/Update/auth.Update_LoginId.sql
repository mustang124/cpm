﻿CREATE PROCEDURE [auth].[Update_LoginId]
(
	@LoginId		INT,

	@Active			BIT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@LoginId:'		+ CONVERT(VARCHAR, @LoginId))
		+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [auth].[Logins]
	SET		[Active] = @Active 
	WHERE	[LoginId] = @LoginId
		AND	@Active IS NOT NULL;

	RETURN @LoginId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO