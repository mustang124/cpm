﻿CREATE PROCEDURE [auth].[Update_JoinCompanyPlant]
(
	@JoinId			INT,

	@CompanyId		INT	= NULL,
	@PlantId		INT	= NULL,
	@Active			BIT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@JoinId:'			+ CONVERT(VARCHAR, @JoinId))
		+ COALESCE(', @CompanyId:'		+ CONVERT(VARCHAR, @CompanyId),	'')
		+ COALESCE(', @PlantId:'		+ CONVERT(VARCHAR, @PlantId),	'')
		+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE	[auth].[JoinCompanyPlant]
	SET		[CompanyId]	= COALESCE(@CompanyId,	[CompanyId]),
			[PlantId]	= COALESCE(@PlantId,	[PlantId]),
			[Active]	= COALESCE(@Active,		[Active])
	WHERE	[JoinId] = @JoinId
		AND(@CompanyId	IS NOT NULL
		OR	@PlantId	IS NOT NULL
		OR	@Active		IS NOT NULL);

	RETURN @JoinId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO