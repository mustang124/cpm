﻿CREATE PROCEDURE [auth].[Update_Company]
(
	@CompanyId		INT,

	@CompanyName	NVARCHAR(128)	= NULL,
	@CompanyRefId	INT				= NULL,
	@Active			BIT				= NULL
)
AS
BEGIN
	
SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@CompanyId:'		+ CONVERT(VARCHAR, @CompanyId))
		+ COALESCE(', @CompanyRefId:'	+ CONVERT(VARCHAR, @CompanyRefId),	'')
		+ COALESCE(', @CompanyName:'	+ CONVERT(VARCHAR, @CompanyName),	'')
		+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),		'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE	[auth].[Companies]
	SET	[CompanyName]	= COALESCE(@CompanyName,	[CompanyName]),
		[CompanyRefId]	= COALESCE(@CompanyRefId,	[CompanyRefId]),
		[Active]		= COALESCE(@Active,			[Active])
	WHERE	[CompanyId] = @CompanyId
		AND(@Active			IS NOT NULL
		OR	@CompanyName	IS NOT NULL
		OR	@CompanyRefId	IS NOT NULL);

	RETURN @CompanyId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO