﻿CREATE PROCEDURE [auth].[Update_JoinPlantLogin]
(
	@JoinId			INT,

	@PlantId		INT	= NULL,
	@LoginId		INT	= NULL,
	@Active			BIT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@JoinId:'			+ CONVERT(VARCHAR, @JoinId))
		+ COALESCE(', @PlantId:'		+ CONVERT(VARCHAR, @PlantId),	'')
		+ COALESCE(', @LoginId:'		+ CONVERT(VARCHAR, @LoginId),	'')
		+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active),	'');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE	[auth].[JoinPlantLogin]
	SET		[PlantId]	= COALESCE(@PlantId, [PlantId]),
			[LoginId]	= COALESCE(@LoginId, [LoginId]),
			[Active]	= COALESCE(@Active,	[Active])
	WHERE	[JoinId] = @JoinId
		AND(@PlantId	IS NOT NULL
		OR	@LoginId	IS NOT NULL
		OR	@Active		IS NOT NULL);

	RETURN @JoinId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO