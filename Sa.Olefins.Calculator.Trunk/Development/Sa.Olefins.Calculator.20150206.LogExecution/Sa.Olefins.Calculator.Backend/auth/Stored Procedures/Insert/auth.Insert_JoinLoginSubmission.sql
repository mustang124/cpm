﻿CREATE PROCEDURE [auth].[Insert_JoinLoginSubmission]
(
	@LoginId			INT,
	@SubmissionId		INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@LoginId:'		+ CONVERT(VARCHAR, @LoginId))
				+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [auth].[JoinLoginSubmission]([LoginId], [SubmissionId])
	OUTPUT [INSERTED].[JoinId] INTO @Id([Id])
	VALUES(
		@LoginId,
		@SubmissionId
		);

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO