﻿CREATE PROCEDURE [auth].[Insert_JoinPlantLogin]
(
	@PlantId			INT,
	@LoginId			INT,
	@Active				BIT	= NULL
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@PlantId:'		+ CONVERT(VARCHAR, @PlantId))
				+ (', @LoginId:'		+ CONVERT(VARCHAR, @LoginId))
		+ COALESCE(', @Active:'			+ CONVERT(VARCHAR, @Active), '');

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	DECLARE @Id TABLE([Id] INT PRIMARY KEY CLUSTERED);

	INSERT INTO [auth].[JoinPlantLogin]([PlantId], [LoginId], [Active])
	OUTPUT [INSERTED].[JoinId] INTO @Id([Id])
	VALUES(
		@PlantId,
		@LoginId,
		@Active
		);

	RETURN (SELECT [Id] FROM @Id);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO