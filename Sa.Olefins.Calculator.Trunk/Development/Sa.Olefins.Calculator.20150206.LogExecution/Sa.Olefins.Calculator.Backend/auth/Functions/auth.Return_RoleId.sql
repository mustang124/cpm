﻿CREATE FUNCTION [auth].[Return_RoleId]
(
	@RoleLevel	INT				= NULL,
	@RoleTag	NVARCHAR(42)	= NULL
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT
		@Id = r.[RoleId]
	FROM [auth].[Roles]	r
	WHERE  (r.[RoleLevel]	= @RoleLevel
		OR	r.[RoleTag]		= @RoleTag);

	RETURN @Id;

END;