﻿CREATE FUNCTION [auth].[Get_PlantsByLogin_Active]
(
	@LoginId		INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		pbl.[JoinId],
		pbl.[PlantId],
		pbl.[LoginId],
		pbl.[CompanyName],
		pbl.[PlantName],
		pbl.[LoginTag]
	FROM [auth].[PlantsByLogin_Active]	pbl --WITH (NOEXPAND)
	WHERE	pbl.[LoginId]	= @LoginId
);