﻿CREATE TABLE [auth].[JoinPlantSubmission]
(
	[JoinId]				INT					NOT	NULL	IDENTITY (1, 1),

	[PlantId]				INT					NOT	NULL	CONSTRAINT [FK_JoinPlantSubmission_Plants]			REFERENCES [auth].[Plants] ([PlantId])				ON DELETE NO ACTION ON UPDATE NO ACTION,
	[SubmissionId]			INT					NOT	NULL	CONSTRAINT [FK_JoinPlantSubmission_Submissions]		REFERENCES [stage].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_JoinPlantSubmission_Active]			DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_JoinPlantSubmission_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinPlantSubmission_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinPlantSubmission_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinPlantSubmission_tsModifiedApp]	DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_JoinPlantSubmission]				PRIMARY KEY NONCLUSTERED([JoinId] ASC),
	CONSTRAINT [UK_JoinPlantSubmission_Submissions]	UNIQUE CLUSTERED ([PlantId] ASC, [SubmissionId] ASC),
	CONSTRAINT [UX_JoinPlantSubmission_Submissions]	UNIQUE NONCLUSTERED ([SubmissionId] ASC),
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinPlantSubmission_Active]
ON [auth].[JoinPlantSubmission]([PlantId] ASC, [SubmissionId] ASC)
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_JoinPlantSubmission_u]
ON [auth].[JoinPlantSubmission]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinPlantSubmission]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinPlantSubmission].[JoinId]		= INSERTED.[JoinId];

END;
GO