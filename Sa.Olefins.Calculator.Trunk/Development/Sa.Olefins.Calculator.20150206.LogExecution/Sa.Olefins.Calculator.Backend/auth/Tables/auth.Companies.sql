﻿CREATE TABLE [auth].[Companies]
(
	[CompanyId]				INT					NOT	NULL	IDENTITY (1, 1),

	[CompanyName]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [CL_Companies_CompanyName]			CHECK([CompanyName]	<> ''),
															CONSTRAINT [UK_Companies_CompanyName]			UNIQUE NONCLUSTERED ([CompanyName]),
	[CompanyRefId]			INT						NULL	CONSTRAINT [FK_Companies_Company_LookUp]		REFERENCES [dim].[Company_LookUp] ([CompanyId])		ON DELETE NO ACTION ON UPDATE NO ACTION,
												
	[Active]				BIT					NOT	NULL	CONSTRAINT [DF_Companies_Active]				DEFAULT 1,

	[tsModified]			DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Companies_tsModified]			DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_tsModifiedHost]		DEFAULT (host_name()),
	[tsModifiedUser]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_tsModifiedUser]		DEFAULT (suser_sname()),
	[tsModifiedApp]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_tsModifiedApp]			DEFAULT (app_name()),
	[tsModifiedRV]			ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Companies]					PRIMARY KEY CLUSTERED ([CompanyId] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Companies_CompanyRefId]
ON [auth].[Companies]([CompanyRefId] ASC)
WHERE [CompanyRefId] IS NOT NULL;
GO

CREATE UNIQUE NONCLUSTERED INDEX [UX_Companies_Active]
ON [auth].[Companies]([CompanyId] ASC)
INCLUDE ([CompanyName], [CompanyRefId], [Active])
WHERE [Active] = 1;
GO

CREATE TRIGGER [auth].[t_Companies_u]
ON [auth].[Companies]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[Companies]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[Companies].[CompanyId]	= INSERTED.[CompanyId];

END;
GO