﻿CREATE FUNCTION [fact].[Get_SubmissionComments]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		c.[SubmissionId],
		c.[SubmissionComment]
	FROM [fact].[SubmissionComments]		c
	WHERE	c.[SubmissionId] = @SubmissionId
);