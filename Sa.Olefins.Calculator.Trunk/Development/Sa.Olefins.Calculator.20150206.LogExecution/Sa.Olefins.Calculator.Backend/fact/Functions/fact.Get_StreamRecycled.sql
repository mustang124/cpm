﻿CREATE FUNCTION [fact].[Get_StreamRecycled]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		r.[SubmissionId],
		r.[ComponentId],
		r.[Recycled_WtPcnt]
	FROM [fact].[StreamRecycled]		r
	WHERE	r.[SubmissionId] = @SubmissionId
);