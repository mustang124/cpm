﻿CREATE FUNCTION [fact].[Get_FacilitiesElecGeneration]
(
	@SubmissionId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		e.[SubmissionId],
		e.[FacilityId],
		e.[Capacity_MW]
	FROM [fact].[FacilitiesElecGeneration]		e
	WHERE	e.[SubmissionId]	= @SubmissionId
);