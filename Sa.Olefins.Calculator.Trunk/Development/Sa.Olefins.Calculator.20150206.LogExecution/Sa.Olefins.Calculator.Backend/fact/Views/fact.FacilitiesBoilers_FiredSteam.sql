﻿CREATE VIEW [fact].[FacilitiesBoilers_FiredSteam]
WITH SCHEMABINDING
AS
SELECT
	f.[SubmissionId],
	b.[FacilityId],
	SUM(f.[Unit_Count])		[Boiler_Count],
	SUM(fb.[Rate_kLbHr])	[Rate_kLbHr],
	COUNT_BIG(*)			[Items]
FROM [fact].[Facilities]				f
INNER JOIN [fact].[FacilitiesBoilers]	fb
	ON	fb.[SubmissionId]	= f.[SubmissionId]
	AND	fb.[FacilityId]		= f.[FacilityId]
INNER JOIN [dim].[Facility_Bridge]		b
	ON	b.[DescendantId] = f.[FacilityId]
WHERE b.[FacilityId] IN (65, 66, 67)
GROUP BY
	f.[SubmissionId],
	b.[FacilityId];
GO

CREATE UNIQUE CLUSTERED INDEX [UX_FacilitiesBoilers_FiredSteam]
ON [fact].[FacilitiesBoilers_FiredSteam]([SubmissionId] ASC, [FacilityId] ASC);
GO

CREATE INDEX [IX_FacilitiesBoilers_FiredSteam]
ON [fact].[FacilitiesBoilers_FiredSteam]([SubmissionId] ASC, [FacilityId] ASC)
INCLUDE([Rate_kLbHr], [Boiler_Count]);
GO