﻿CREATE TABLE [fact].[Audit]
(
	[SubmissionId]				INT					NOT	NULL	CONSTRAINT [FK_Audit_Submissions]		REFERENCES [fact].[Submissions] ([SubmissionId])	ON DELETE NO ACTION ON UPDATE NO ACTION,

	[TimeBeg]					DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Audit_TimeBeg]			DEFAULT (sysdatetimeoffset()),
	[TimeEnd]					DATETIMEOFFSET(7)		NULL,
	[_Duration_mcs]				AS DATEDIFF(MICROSECOND, [TimeBeg], [TimeEnd]),
	[Error_Count]				INT						NULL,
	[Active]					BIT					NOT	NULL	CONSTRAINT [DF_Audit_Active]			DEFAULT 1,

	CONSTRAINT [CV_Audit_Times]						CHECK([TimeBeg] <= [TimeEnd]),

	[tsModified]				DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Audit_tsModified]		DEFAULT (sysdatetimeoffset()),
	[tsModifiedHost]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Audit_tsModifiedHost]	DEFAULT (host_name()),
	[tsModifiedUser]			NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Audit_tsModifiedUser]	DEFAULT (suser_sname()),
	[tsModifiedApp]				NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Audit_tsModifiedApp]		DEFAULT (app_name()),
	[tsModifiedRV]				ROWVERSION			NOT	NULL,

	CONSTRAINT [PK_Audit]		PRIMARY KEY CLUSTERED ([SubmissionId] ASC)
);
GO

CREATE TRIGGER [fact].[Audit_LogChanges]
ON [fact].[Audit]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Audit]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM inserted
	WHERE [fact].[Audit].[SubmissionId]	= INSERTED.[SubmissionId];

END;
GO