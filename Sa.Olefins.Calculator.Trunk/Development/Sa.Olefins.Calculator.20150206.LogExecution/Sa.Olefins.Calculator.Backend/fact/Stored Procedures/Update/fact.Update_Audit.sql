﻿CREATE PROCEDURE [fact].[Update_Audit]
(
	@SubmissionId		INT,

	@TimeEnd			DATETIMEOFFSET,
	@Error_Count		INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
				+ (', @TimeEnd:'		+ CONVERT(VARCHAR, @TimeEnd))
				+ (', @Error_Count:'	+ CONVERT(VARCHAR, @Error_Count));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	UPDATE [fact].[Audit]
	SET	[TimeEnd]			= @TimeEnd,
		[Error_Count]		= @Error_Count
	WHERE	[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
GO