﻿CREATE PROCEDURE [fact].[Delete_DataSet]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

	EXECUTE [fact].[Delete_Audit]						@SubmissionId;

	EXECUTE [fact].[Delete_Capacity]					@SubmissionId;

	EXECUTE [fact].[Delete_FacilitiesBoilers]			@SubmissionId;
	EXECUTE [fact].[Delete_FacilitiesElecGeneration]	@SubmissionId;
	EXECUTE [fact].[Delete_FacilitiesFractionator]		@SubmissionId;
	EXECUTE [fact].[Delete_FacilitiesHydroTreater]		@SubmissionId;
	EXECUTE [fact].[Delete_FacilitiesTrains]			@SubmissionId;
	EXECUTE [fact].[Delete_Facilities]					@SubmissionId;

	EXECUTE [fact].[Delete_StreamDescription]			@SubmissionId;
	EXECUTE [fact].[Delete_StreamComposition]			@SubmissionId;
	EXECUTE [fact].[Delete_StreamCompositionMol]		@SubmissionId;
	EXECUTE [fact].[Delete_StreamDensity]				@SubmissionId;
	EXECUTE [fact].[Delete_StreamRecovered]				@SubmissionId;
	EXECUTE [fact].[Delete_StreamRecycled]				@SubmissionId;
	EXECUTE [fact].[Delete_StreamQuantity]				@SubmissionId;

	EXECUTE [fact].[Delete_SubmissionComments]			@SubmissionId;
	EXECUTE [fact].[Delete_Submission]					@SubmissionId;

END;