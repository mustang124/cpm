﻿CREATE PROCEDURE [fact].[Insert_DataSet]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

	DECLARE @Error			INT = 0;
	DECLARE @Error_Count	INT = 0;

	ALTER TABLE [fact].[Audit]	NOCHECK CONSTRAINT [FK_Audit_Submissions];

	EXECUTE @Error = [fact].[Insert_Audit]						@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_Submission]					@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_SubmissionComments]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_Capacity]					@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_Facilities]					@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesBoilers]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesElecGeneration]	@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesFractionator]		@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesHydroTreater]		@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_FacilitiesTrains]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;

	EXECUTE @Error = [fact].[Insert_StreamQuantity]				@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamDescription]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamComposition]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamCompositionMol]		@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamDensity]				@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamRecovered]			@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	EXECUTE @Error = [fact].[Insert_StreamRecycled]				@SubmissionId;	IF(@Error <> 0) SET @Error_Count = @Error_Count + 1;
	
	DECLARE @TimeEnd DATETIMEOFFSET = SYSDATETIMEOFFSET();

	EXECUTE @Error = [fact].[Update_Audit]						@SubmissionId, @TimeEnd, @Error_Count;

END;