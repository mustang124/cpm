﻿CREATE PROCEDURE [fact].[Insert_Capacity]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[Capacity]([SubmissionId], [StreamId], [Capacity_kMT], [StreamDay_MTSD], [Record_MTSD])
	SELECT
		c.[SubmissionId],
		c.[StreamId],
		c.[Capacity_kMT],
		c.[StreamDay_MTSD],
		c.[Record_MTSD]
	FROM [stage].[Capacity]				c
	INNER JOIN [stage].[Submissions]	z
		ON	z.[SubmissionId]	= c.[SubmissionId]
	WHERE	c.[SubmissionId]	= @SubmissionId
		AND	c.[StreamDay_MTSD]	> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;
