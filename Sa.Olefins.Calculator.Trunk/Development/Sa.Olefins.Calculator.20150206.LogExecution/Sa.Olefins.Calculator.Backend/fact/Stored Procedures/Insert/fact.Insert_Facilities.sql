﻿CREATE PROCEDURE [fact].[Insert_Facilities]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[Facilities]([SubmissionId], [FacilityId], [Unit_Count])
	SELECT
		f.[SubmissionId],
		f.[FacilityId],
		f.[Unit_Count]
	FROM [stage].[Facilities]			f
	INNER JOIN [stage].[Submissions]	z
		ON	z.[SubmissionId] = f.[SubmissionId]
	WHERE	f.[SubmissionId] = @SubmissionId
		AND	f.[Unit_Count] >= 1;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;