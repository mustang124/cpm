﻿CREATE PROCEDURE [fact].[Insert_StreamComposition]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[StreamComposition]([SubmissionId], [StreamNumber], [ComponentId], [Component_WtPcnt])
	SELECT
		c.[SubmissionId],
		c.[StreamNumber],
		c.[ComponentId],
		c.[Component_WtPcnt]
	FROM [stage].[StreamComposition]		c
	INNER JOIN [stage].[StreamQuantity]		q
		ON	q.[SubmissionId]		= c.[SubmissionId]
		AND	q.[StreamNumber]		= c.[StreamNumber]
		AND	q.[Quantity_kMT]		> 0.0
	WHERE	c.[SubmissionId]		= @SubmissionId
		AND	c.[Component_WtPcnt]	> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;