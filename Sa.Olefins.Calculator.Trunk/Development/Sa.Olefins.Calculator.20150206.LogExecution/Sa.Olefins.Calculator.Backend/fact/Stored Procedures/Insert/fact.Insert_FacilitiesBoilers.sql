﻿CREATE PROCEDURE [fact].[Insert_FacilitiesBoilers]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[FacilitiesBoilers]([SubmissionId], [FacilityId], [Rate_kLbHr], [Pressure_PSIg])
	SELECT
		b.[SubmissionId],
		b.[FacilityId],
		b.[Rate_kLbHr],
		b.[Pressure_PSIg]
	FROM [stage].[FacilitiesBoilers]		b
	INNER JOIN [stage].[Facilities]			f
		ON	f.[SubmissionId]	= b.[SubmissionId]
		AND	f.[FacilityId]		= b.[FacilityId]
		AND	f.[Unit_Count]		> 0
	WHERE	b.[SubmissionId]	= @SubmissionId
		AND	b.[Rate_kLbHr]		> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;