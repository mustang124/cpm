﻿CREATE PROCEDURE [fact].[Insert_StreamQuantity]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[StreamQuantity]([SubmissionId], [StreamNumber], [StreamId], [Quantity_kMT])
	SELECT
		q.[SubmissionId],
		q.[StreamNumber],
		q.[StreamId],
		q.[Quantity_kMT]
	FROM [stage].[StreamQuantity]		q
	INNER JOIN [stage].[Submissions]	z
		ON	z.[SubmissionId]	= q.[SubmissionId]
	WHERE	q.[SubmissionId]	= @SubmissionId
		AND	q.[Quantity_kMT]	> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;