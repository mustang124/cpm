﻿CREATE PROCEDURE [fact].[Insert_Submission]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[Submissions]([SubmissionId], [SubmissionName], [DateBeg], [DateEnd])
	SELECT
		z.[SubmissionId],
		z.[SubmissionName],
		z.[DateBeg],
		z.[DateEnd]
	FROM [stage].[Submissions]		z
	WHERE	z.[SubmissionId] = @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;