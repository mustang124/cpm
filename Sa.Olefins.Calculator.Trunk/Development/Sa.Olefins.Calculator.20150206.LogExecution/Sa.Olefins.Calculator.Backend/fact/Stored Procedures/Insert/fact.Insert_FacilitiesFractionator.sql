﻿CREATE PROCEDURE [fact].[Insert_FacilitiesFractionator]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[FacilitiesFractionator]([SubmissionId], [FacilityId], [Quantity_kBSD], [StreamId], [Throughput_kMT], [Density_SG])
	SELECT
		s.[SubmissionId],
		s.[FacilityId],
		s.[Quantity_kBSD],
		s.[StreamId],
		s.[Throughput_kMT],
		s.[Density_SG]
	FROM [stage].[FacilitiesFractionator]		s
	INNER JOIN [stage].[Facilities]				f
		ON	f.[SubmissionId] = s.[SubmissionId]
		AND	f.[FacilityId] = s.[FacilityId]
		AND	f.[Unit_Count] > 0
	WHERE	s.[SubmissionId] = @SubmissionId
		AND(s.[Quantity_kBSD]	> 0.0
		OR	s.[Throughput_kMT]	> 0.0
		OR	s.[Density_SG]		> 0.0);

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;