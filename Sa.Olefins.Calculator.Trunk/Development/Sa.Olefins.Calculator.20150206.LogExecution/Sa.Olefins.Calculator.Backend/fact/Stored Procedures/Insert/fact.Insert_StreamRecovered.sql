﻿CREATE PROCEDURE [fact].[Insert_StreamRecovered]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

DECLARE	@tsExecuted		DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();
DECLARE @Parameters		VARCHAR(4000)	=
					('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

EXECUTE	[audit].[Insert_LogExecution] @@PROCID, @tsExecuted, @Parameters;

BEGIN TRY

	INSERT INTO [fact].[StreamRecovered]([SubmissionId], [StreamNumber], [Recovered_WtPcnt])
	SELECT
		r.[SubmissionId],
		r.[StreamNumber],
		r.[Recovered_WtPcnt]
	FROM [stage].[StreamRecovered]		r
	INNER JOIN [stage].[StreamQuantity]	q
		ON	q.[SubmissionId]		= r.[SubmissionId]
		AND	q.[StreamNumber]		= r.[StreamNumber]
		AND	q.[Quantity_kMT]		> 0
	WHERE	r.[SubmissionId]		= @SubmissionId
		AND	r.[Recovered_WtPcnt]	> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	EXECUTE [audit].[Insert_LogError] @@PROCID, @tsExecuted, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;