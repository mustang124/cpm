﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _090_ElectricityGeneration : SqlDatabaseTestClass
    {

        public _090_ElectricityGeneration()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void ElecticityGeneration()
        {
            SqlDatabaseTestActions testActions = this.ElecticityGenerationData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction ElecticityGeneration_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_090_ElectricityGeneration));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition ElecticityGeneration_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition ElecticityGeneration_EmptySet;
            this.ElecticityGenerationData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            ElecticityGeneration_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ElecticityGeneration_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            ElecticityGeneration_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // ElecticityGeneration_TestAction
            // 
            ElecticityGeneration_TestAction.Conditions.Add(ElecticityGeneration_NotEmpty);
            ElecticityGeneration_TestAction.Conditions.Add(ElecticityGeneration_EmptySet);
            resources.ApplyResources(ElecticityGeneration_TestAction, "ElecticityGeneration_TestAction");
            // 
            // ElecticityGeneration_NotEmpty
            // 
            ElecticityGeneration_NotEmpty.Enabled = true;
            ElecticityGeneration_NotEmpty.Name = "ElecticityGeneration_NotEmpty";
            ElecticityGeneration_NotEmpty.ResultSet = 1;
            // 
            // ElecticityGeneration_EmptySet
            // 
            ElecticityGeneration_EmptySet.Enabled = true;
            ElecticityGeneration_EmptySet.Name = "ElecticityGeneration_EmptySet";
            ElecticityGeneration_EmptySet.ResultSet = 2;
            // 
            // ElecticityGenerationData
            // 
            this.ElecticityGenerationData.PosttestAction = null;
            this.ElecticityGenerationData.PretestAction = null;
            this.ElecticityGenerationData.TestAction = ElecticityGeneration_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions ElecticityGenerationData;
    }
}
