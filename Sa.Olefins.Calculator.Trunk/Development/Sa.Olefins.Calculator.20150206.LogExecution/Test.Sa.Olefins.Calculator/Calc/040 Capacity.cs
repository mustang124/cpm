﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test.Sa.Olefins.Calculator.Backend
{
    [TestClass()]
    public class _040_Capacity : SqlDatabaseTestClass
    {

        public _040_Capacity()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }

        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_StreamDay_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(_040_Capacity));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Capacity_StreamDay_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_StreamDay_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_Recovered_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Capacity_Recovered_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_Recovered_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_Utilization_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Capacity_Utilization_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_Utilization_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_Supplemental_MTSD_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Capacity_Supplemental_MTSD_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_Supplemental_MTSD_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_SupplementalInfer_kMT_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Capacity_SupplementalInfer_kMT_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_SupplementalInfer_kMT_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_SupplementalInfer_MTSD_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Capacity_SupplementalInfer_MTSD_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_SupplementalInfer_MTSD_EmptySet;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction Capacity_Plant_MTSD_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition Capacity_Plant_MTSD_NotEmpty;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition Capacity_Plant_MTSD_EmtpySet;
            this.Capacity_StreamDayData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_RecoveredData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_UtilizationData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_Supplemental_MTSDData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_SupplementalInfer_kMTData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_SupplementalInfer_MTSDData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.Capacity_Plant_MTSDData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            Capacity_StreamDay_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_StreamDay_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Capacity_StreamDay_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_Recovered_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_Recovered_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Capacity_Recovered_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_Utilization_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_Utilization_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Capacity_Utilization_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_Supplemental_MTSD_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_Supplemental_MTSD_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Capacity_Supplemental_MTSD_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_SupplementalInfer_kMT_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_SupplementalInfer_kMT_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Capacity_SupplementalInfer_kMT_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_SupplementalInfer_MTSD_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_SupplementalInfer_MTSD_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Capacity_SupplementalInfer_MTSD_EmptySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            Capacity_Plant_MTSD_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            Capacity_Plant_MTSD_NotEmpty = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.NotEmptyResultSetCondition();
            Capacity_Plant_MTSD_EmtpySet = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.EmptyResultSetCondition();
            // 
            // Capacity_StreamDay_TestAction
            // 
            Capacity_StreamDay_TestAction.Conditions.Add(Capacity_StreamDay_NotEmpty);
            Capacity_StreamDay_TestAction.Conditions.Add(Capacity_StreamDay_EmptySet);
            resources.ApplyResources(Capacity_StreamDay_TestAction, "Capacity_StreamDay_TestAction");
            // 
            // Capacity_StreamDay_NotEmpty
            // 
            Capacity_StreamDay_NotEmpty.Enabled = true;
            Capacity_StreamDay_NotEmpty.Name = "Capacity_StreamDay_NotEmpty";
            Capacity_StreamDay_NotEmpty.ResultSet = 1;
            // 
            // Capacity_StreamDay_EmptySet
            // 
            Capacity_StreamDay_EmptySet.Enabled = true;
            Capacity_StreamDay_EmptySet.Name = "Capacity_StreamDay_EmptySet";
            Capacity_StreamDay_EmptySet.ResultSet = 2;
            // 
            // Capacity_Recovered_TestAction
            // 
            Capacity_Recovered_TestAction.Conditions.Add(Capacity_Recovered_NotEmpty);
            Capacity_Recovered_TestAction.Conditions.Add(Capacity_Recovered_EmptySet);
            resources.ApplyResources(Capacity_Recovered_TestAction, "Capacity_Recovered_TestAction");
            // 
            // Capacity_Recovered_NotEmpty
            // 
            Capacity_Recovered_NotEmpty.Enabled = true;
            Capacity_Recovered_NotEmpty.Name = "Capacity_Recovered_NotEmpty";
            Capacity_Recovered_NotEmpty.ResultSet = 1;
            // 
            // Capacity_Recovered_EmptySet
            // 
            Capacity_Recovered_EmptySet.Enabled = true;
            Capacity_Recovered_EmptySet.Name = "Capacity_Recovered_EmptySet";
            Capacity_Recovered_EmptySet.ResultSet = 2;
            // 
            // Capacity_Utilization_TestAction
            // 
            Capacity_Utilization_TestAction.Conditions.Add(Capacity_Utilization_NotEmpty);
            Capacity_Utilization_TestAction.Conditions.Add(Capacity_Utilization_EmptySet);
            resources.ApplyResources(Capacity_Utilization_TestAction, "Capacity_Utilization_TestAction");
            // 
            // Capacity_Utilization_NotEmpty
            // 
            Capacity_Utilization_NotEmpty.Enabled = true;
            Capacity_Utilization_NotEmpty.Name = "Capacity_Utilization_NotEmpty";
            Capacity_Utilization_NotEmpty.ResultSet = 1;
            // 
            // Capacity_Utilization_EmptySet
            // 
            Capacity_Utilization_EmptySet.Enabled = true;
            Capacity_Utilization_EmptySet.Name = "Capacity_Utilization_EmptySet";
            Capacity_Utilization_EmptySet.ResultSet = 2;
            // 
            // Capacity_Supplemental_MTSD_TestAction
            // 
            Capacity_Supplemental_MTSD_TestAction.Conditions.Add(Capacity_Supplemental_MTSD_NotEmpty);
            Capacity_Supplemental_MTSD_TestAction.Conditions.Add(Capacity_Supplemental_MTSD_EmptySet);
            resources.ApplyResources(Capacity_Supplemental_MTSD_TestAction, "Capacity_Supplemental_MTSD_TestAction");
            // 
            // Capacity_Supplemental_MTSD_NotEmpty
            // 
            Capacity_Supplemental_MTSD_NotEmpty.Enabled = true;
            Capacity_Supplemental_MTSD_NotEmpty.Name = "Capacity_Supplemental_MTSD_NotEmpty";
            Capacity_Supplemental_MTSD_NotEmpty.ResultSet = 1;
            // 
            // Capacity_Supplemental_MTSD_EmptySet
            // 
            Capacity_Supplemental_MTSD_EmptySet.Enabled = true;
            Capacity_Supplemental_MTSD_EmptySet.Name = "Capacity_Supplemental_MTSD_EmptySet";
            Capacity_Supplemental_MTSD_EmptySet.ResultSet = 2;
            // 
            // Capacity_SupplementalInfer_kMT_TestAction
            // 
            Capacity_SupplementalInfer_kMT_TestAction.Conditions.Add(Capacity_SupplementalInfer_kMT_NotEmpty);
            Capacity_SupplementalInfer_kMT_TestAction.Conditions.Add(Capacity_SupplementalInfer_kMT_EmptySet);
            resources.ApplyResources(Capacity_SupplementalInfer_kMT_TestAction, "Capacity_SupplementalInfer_kMT_TestAction");
            // 
            // Capacity_SupplementalInfer_kMT_NotEmpty
            // 
            Capacity_SupplementalInfer_kMT_NotEmpty.Enabled = true;
            Capacity_SupplementalInfer_kMT_NotEmpty.Name = "Capacity_SupplementalInfer_kMT_NotEmpty";
            Capacity_SupplementalInfer_kMT_NotEmpty.ResultSet = 1;
            // 
            // Capacity_SupplementalInfer_kMT_EmptySet
            // 
            Capacity_SupplementalInfer_kMT_EmptySet.Enabled = true;
            Capacity_SupplementalInfer_kMT_EmptySet.Name = "Capacity_SupplementalInfer_kMT_EmptySet";
            Capacity_SupplementalInfer_kMT_EmptySet.ResultSet = 2;
            // 
            // Capacity_SupplementalInfer_MTSD_TestAction
            // 
            Capacity_SupplementalInfer_MTSD_TestAction.Conditions.Add(Capacity_SupplementalInfer_MTSD_NotEmpty);
            Capacity_SupplementalInfer_MTSD_TestAction.Conditions.Add(Capacity_SupplementalInfer_MTSD_EmptySet);
            resources.ApplyResources(Capacity_SupplementalInfer_MTSD_TestAction, "Capacity_SupplementalInfer_MTSD_TestAction");
            // 
            // Capacity_SupplementalInfer_MTSD_NotEmpty
            // 
            Capacity_SupplementalInfer_MTSD_NotEmpty.Enabled = true;
            Capacity_SupplementalInfer_MTSD_NotEmpty.Name = "Capacity_SupplementalInfer_MTSD_NotEmpty";
            Capacity_SupplementalInfer_MTSD_NotEmpty.ResultSet = 1;
            // 
            // Capacity_SupplementalInfer_MTSD_EmptySet
            // 
            Capacity_SupplementalInfer_MTSD_EmptySet.Enabled = true;
            Capacity_SupplementalInfer_MTSD_EmptySet.Name = "Capacity_SupplementalInfer_MTSD_EmptySet";
            Capacity_SupplementalInfer_MTSD_EmptySet.ResultSet = 2;
            // 
            // Capacity_Plant_MTSD_TestAction
            // 
            Capacity_Plant_MTSD_TestAction.Conditions.Add(Capacity_Plant_MTSD_NotEmpty);
            Capacity_Plant_MTSD_TestAction.Conditions.Add(Capacity_Plant_MTSD_EmtpySet);
            resources.ApplyResources(Capacity_Plant_MTSD_TestAction, "Capacity_Plant_MTSD_TestAction");
            // 
            // Capacity_Plant_MTSD_NotEmpty
            // 
            Capacity_Plant_MTSD_NotEmpty.Enabled = true;
            Capacity_Plant_MTSD_NotEmpty.Name = "Capacity_Plant_MTSD_NotEmpty";
            Capacity_Plant_MTSD_NotEmpty.ResultSet = 1;
            // 
            // Capacity_Plant_MTSD_EmtpySet
            // 
            Capacity_Plant_MTSD_EmtpySet.Enabled = true;
            Capacity_Plant_MTSD_EmtpySet.Name = "Capacity_Plant_MTSD_EmtpySet";
            Capacity_Plant_MTSD_EmtpySet.ResultSet = 2;
            // 
            // Capacity_StreamDayData
            // 
            this.Capacity_StreamDayData.PosttestAction = null;
            this.Capacity_StreamDayData.PretestAction = null;
            this.Capacity_StreamDayData.TestAction = Capacity_StreamDay_TestAction;
            // 
            // Capacity_RecoveredData
            // 
            this.Capacity_RecoveredData.PosttestAction = null;
            this.Capacity_RecoveredData.PretestAction = null;
            this.Capacity_RecoveredData.TestAction = Capacity_Recovered_TestAction;
            // 
            // Capacity_UtilizationData
            // 
            this.Capacity_UtilizationData.PosttestAction = null;
            this.Capacity_UtilizationData.PretestAction = null;
            this.Capacity_UtilizationData.TestAction = Capacity_Utilization_TestAction;
            // 
            // Capacity_Supplemental_MTSDData
            // 
            this.Capacity_Supplemental_MTSDData.PosttestAction = null;
            this.Capacity_Supplemental_MTSDData.PretestAction = null;
            this.Capacity_Supplemental_MTSDData.TestAction = Capacity_Supplemental_MTSD_TestAction;
            // 
            // Capacity_SupplementalInfer_kMTData
            // 
            this.Capacity_SupplementalInfer_kMTData.PosttestAction = null;
            this.Capacity_SupplementalInfer_kMTData.PretestAction = null;
            this.Capacity_SupplementalInfer_kMTData.TestAction = Capacity_SupplementalInfer_kMT_TestAction;
            // 
            // Capacity_SupplementalInfer_MTSDData
            // 
            this.Capacity_SupplementalInfer_MTSDData.PosttestAction = null;
            this.Capacity_SupplementalInfer_MTSDData.PretestAction = null;
            this.Capacity_SupplementalInfer_MTSDData.TestAction = Capacity_SupplementalInfer_MTSD_TestAction;
            // 
            // Capacity_Plant_MTSDData
            // 
            this.Capacity_Plant_MTSDData.PosttestAction = null;
            this.Capacity_Plant_MTSDData.PretestAction = null;
            this.Capacity_Plant_MTSDData.TestAction = Capacity_Plant_MTSD_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void Capacity_StreamDay()
        {
            SqlDatabaseTestActions testActions = this.Capacity_StreamDayData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_Recovered()
        {
            SqlDatabaseTestActions testActions = this.Capacity_RecoveredData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_Utilization()
        {
            SqlDatabaseTestActions testActions = this.Capacity_UtilizationData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_Supplemental_MTSD()
        {
            SqlDatabaseTestActions testActions = this.Capacity_Supplemental_MTSDData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_SupplementalInfer_kMT()
        {
            SqlDatabaseTestActions testActions = this.Capacity_SupplementalInfer_kMTData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_SupplementalInfer_MTSD()
        {
            SqlDatabaseTestActions testActions = this.Capacity_SupplementalInfer_MTSDData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void Capacity_Plant_MTSD()
        {
            SqlDatabaseTestActions testActions = this.Capacity_Plant_MTSDData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }






        private SqlDatabaseTestActions Capacity_StreamDayData;
        private SqlDatabaseTestActions Capacity_RecoveredData;
        private SqlDatabaseTestActions Capacity_UtilizationData;
        private SqlDatabaseTestActions Capacity_Supplemental_MTSDData;
        private SqlDatabaseTestActions Capacity_SupplementalInfer_kMTData;
        private SqlDatabaseTestActions Capacity_SupplementalInfer_MTSDData;
        private SqlDatabaseTestActions Capacity_Plant_MTSDData;
    }
}
