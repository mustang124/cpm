﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Solomon.LanguageTranslation.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <p class="text-success">
        <asp:Literal runat="server" ID="SuccessMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Register A New User</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="col-md-6">
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Email">Email</asp:Label>
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email" />
                <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="Email" CssClass="text-danger" ErrorMessage="The email field is required." />
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="UserName">UserName</asp:Label>
                <asp:TextBox runat="server" ID="UserName" CssClass="form-control" TextMode="SingleLine" />
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <asp:Label runat="server" AssociatedControlID="FirstName">First Name</asp:Label>
                        <asp:TextBox runat="server" ID="FirstName" CssClass="form-control" TextMode="SingleLine" />
                    </div>
                    <div class="col-md-6">
                        <asp:Label runat="server" AssociatedControlID="LastName">Last Name</asp:Label>
                        <asp:TextBox runat="server" ID="LastName" CssClass="form-control" TextMode="SingleLine" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="UserRole">User Role</asp:Label>
                <asp:DropDownList ID="UserRole" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <asp:Label runat="server" AssociatedControlID="Password">Password</asp:Label>
                        <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="Password"
                            CssClass="text-danger" ErrorMessage="The password field is required." />
                    </div>
                    <div class="col-md-6">
                        <asp:Label runat="server" AssociatedControlID="ConfirmPassword">Confirm password</asp:Label>
                        <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                            CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                        <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                            CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
                    </div>
                </div>
            </div>
            <asp:Button runat="server" OnClick="CreateUser_Click" Text="Register" CssClass="btn btn-primary" />
        </div>
    </div>
</asp:Content>
