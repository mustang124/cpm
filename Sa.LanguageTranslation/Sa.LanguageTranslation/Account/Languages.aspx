﻿<%@ Page Title="Languages" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Languages.aspx.cs" Inherits="Solomon.LanguageTranslation.Account.Languages" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <p class="text-success">
        <asp:Literal runat="server" ID="SuccessMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Configure System Languages</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <div class="form-row">
                <div class="col-md-4">
                    <asp:Label runat="server" AssociatedControlID="Ddlanguage">Language: </asp:Label>
                    <asp:DropDownList ID="Ddlanguage" runat="server" CssClass="form-control">
                        <asp:ListItem Enabled="true" Text="- Select Language -" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col-md-4">
                    <asp:Button runat="server" ID="AddLanguage" OnClick="AddLanguage_Click" Text="Add Language" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>

        <h4 class="padding-top-30">Added Languages</h4>
        <hr />
        <div class="form-group">
            <div class="form-row">
                <div class="col-md-12">
                    <asp:GridView ID="GridView1" CssClass="table table-striped table-bordered table-condensed" runat="server">
                        <%--PageSize="10" AllowPaging="true"--%>
                        <Columns>
                            <asp:BoundField DataField="LanguageName" HeaderText="Language Name" />
                            <asp:BoundField DataField="LanguageAbbr" HeaderText="Language Abbreviation" />
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <span id="nl" runat="server" enabled="false"><i class="fa fa-fw fa-trash" aria-hidden="true"></i></span>
                                    <%--<asp:LinkButton runat="server" CommandName="DeleteLanugage" ID="DeleteLanguage" CssClass="btn btn-default" ToolTip="Delete">
                                                    <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                                            </asp:LinkButton>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
