﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Solomon.LanguageTranslation.Account
{
    public partial class Documents : System.Web.UI.Page
    {
        #region private variables
        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        private static string _currentRecord;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }

            var SqlCon1 = new SqlConnection(_connectionString);
            DataSet ds = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter("Select * from dbo.Document", SqlCon1);
            ad.Fill(ds);
            GridView1.AutoGenerateColumns = false;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count != 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.FileUpload1.HasFile)
                {
                    var sb = new StringBuilder();
                    XmlDocument xmlDoc = new XmlDocument();
                    //Cleanup Xml file remove comments etc
                    StreamReader reader = new StreamReader(FileUpload1.FileContent, Encoding.UTF8);
                    string inputLine = "";
                    string str = "";
                    while ((inputLine = reader.ReadLine()) != null)
                    {
                        if (inputLine.Trim().StartsWith("<"))
                        {
                            str += inputLine + "\n";
                        }
                    }
                    //Load xml content
                    xmlDoc.LoadXml(str);

                    //save in document table 
                    string query = string.Empty;
                    var translatorId = GetUserTranslatorId(User.Identity.GetUserId());
                    var documentId = string.Empty;
                    query += "INSERT INTO Document (DocumentName, IsActive, CreatedBy) ";
                    query += "VALUES (@1, @2, @3); SELECT SCOPE_IDENTITY() ";
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand comm = new SqlCommand())
                        {
                            {
                                var withBlock = comm;
                                withBlock.Connection = conn;
                                withBlock.CommandType = CommandType.Text;
                                withBlock.CommandText = query;
                                withBlock.Parameters.AddWithValue("@1", FileUpload1.FileName);
                                withBlock.Parameters.AddWithValue("@2", 1);
                                withBlock.Parameters.AddWithValue("@3", translatorId);
                            }
                            try
                            {
                                conn.Open();
                                var rows = comm.ExecuteScalar();

                                if(rows != null)
                                {
                                    documentId = rows.ToString();
                                }
                                if (conn.State == ConnectionState.Open)
                                    conn.Close();
                            }
                            catch (SqlException ex)
                            {
                                ErrorMessage.Text = ex.Message.ToString();
                            }
                        }
                    }

                    var engLangeId = GetDefaultLanguageId();

                    //store values in a model
                    XmlNodeList parentNode = xmlDoc.GetElementsByTagName("text");

                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Insert into Translation (DocumentID, LanguageID, TagName, TagText, Retranslate, Active, IsAttribute, CreatedBy) values (@DocumentID, @LanguageID, @TagName, @TagText, @Retranslate, @Active, @IsAttribute, @CreatedBy)", conn))
                        {
                            conn.Open();
                            for (int i = 0; i < parentNode.Count; i++)
                            {
                                if (parentNode[i].Attributes != null && parentNode[i].Attributes["name"] != null)
                                {
                                    var tagName = parentNode[i].Attributes["name"].Value;
                                    var tagText = (parentNode[i].Attributes["value"] != null) ? parentNode[i].Attributes["value"].Value : parentNode[i].InnerText;
                                    int isAttribute = (parentNode[i].Attributes["value"] != null) ? 1 : 0;
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.AddWithValue("@DocumentID", documentId);
                                    cmd.Parameters.AddWithValue("@LanguageID", engLangeId);
                                    cmd.Parameters.AddWithValue("@TagName", tagName);
                                    cmd.Parameters.AddWithValue("@TagText", tagText);
                                    cmd.Parameters.AddWithValue("@Retranslate", 0);
                                    cmd.Parameters.AddWithValue("@Active", 1);
                                    cmd.Parameters.AddWithValue("@IsAttribute", isAttribute);
                                    cmd.Parameters.AddWithValue("@CreatedBy", translatorId);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            conn.Close();
                        }
                    }
                    SuccessMessage.Text = "Document Added Successfully!";
                    Response.Redirect(Request.RawUrl);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = ex.ToString();
            }
        }


        public int GetUserTranslatorId(string userId)
        {
            try
            {
                int translatrId = 0;
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT TranslatorID FROM Translator WHERE UserID = @UserId", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("UserId", SqlDbType.VarChar).Value = userId;
                        translatrId = (Int32)cmd.ExecuteScalar();
                        conn.Close();
                    }
                }
                return translatrId;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int GetDefaultLanguageId()
        {
            try
            {
                int languageId = 0;
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT LanguageID FROM Languages WHERE LanguageAbbr = @abbr", conn))
                    {
                        conn.Open();
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("abbr", SqlDbType.VarChar).Value = "en";
                        languageId = (Int32)cmd.ExecuteScalar();
                        conn.Close();
                    }
                }
                return languageId;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName != "DeleteDocument") return;
            //int id = Convert.ToInt32(e.CommandArgument);
            //_currentRecord = id.ToString();
            //lblModalTitle.Text = "Delete User";
            //lblModalBody.Text = "Are you sure you want to Delete this document. You will loose all the translations associated with this document!";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            //upModal.Update();
        }

        protected void delete_ServerClick(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("Delete FROM Translation WHERE DocumentID = @DocumentID; DELETE FROM Document WHERE DocumentID = @DocumentID2;", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("DocumentID", SqlDbType.Int).Value = Convert.ToInt32(_currentRecord);
                    cmd.Parameters.Add("DocumentID2", SqlDbType.Int).Value = Convert.ToInt32(_currentRecord);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            _currentRecord = "";
            Response.Redirect(Request.RawUrl);
        }
    }
}