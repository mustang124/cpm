﻿<%@ Page Title="Documents" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="Solomon.LanguageTranslation.Account.Documents" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">

    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>
    <p class="text-success">
        <asp:Literal runat="server" ID="SuccessMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Upload A New Document</h4>
        <hr />
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <div class="form-row">
                <div class="col-md-4">
                    <asp:Label runat="server" AssociatedControlID="FileUpload1">Upload</asp:Label>
                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control" accept=".txt" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                        ErrorMessage="You can olny upload *.txt type files!" Display="Dynamic" ControlToValidate="FileUpload1"
                        ValidationExpression="(.+\.([t][x][t]))"> </asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col-md-4">
                    <asp:Button runat="server" ID="UploadButton" OnClick="UploadButton_Click" Text="Upload" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>

        <div class="padding-top-30">
            <h4>Added Documents</h4>
            <hr />
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-12">
                        <asp:GridView ID="GridView1" DataKeyNames="DocumentID" OnRowCommand="GridView1_RowCommand" CssClass="table table-striped table-bordered table-condensed" runat="server">
                            <%--PageSize="10" AllowPaging="true"--%>
                            <Columns>
                                <asp:BoundField DataField="DocumentName" HeaderText="Document Name" />
                                <asp:TemplateField HeaderText="Active" SortExpression="Active">
                                    <ItemTemplate><%# (Boolean.Parse(Eval("IsActive").ToString())) ? "Yes" : "No" %></ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <span id="nl" runat="server" enabled="false"><i class="fa fa-fw fa-trash" aria-hidden="true"></i></span>
                                        <%--<asp:LinkButton runat="server" CommandName="DeleteDocument" ID="DeleteDocument" CssClass="btn btn-default" ToolTip="Delete" CommandArgument='<%# Eval("DocumentID") %>'>
                                                    <i class="fa fa-fw fa-trash" aria-hidden="true"></i>
                                        </asp:LinkButton>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <!-- Bootstrap Modal Dialog -->
                        <%--<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title">
                                                        <asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" runat="server" onserverclick="delete_ServerClick" data-dismiss="modal" class="btn btn-primary" id="delete">Yes</button>
                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
