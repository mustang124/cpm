﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace Solomon.LanguageTranslation.Account
{
    public partial class Languages : System.Web.UI.Page
    {
        #region private variables
        string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString.ToString();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }

            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);

            foreach (CultureInfo culture in cultures.OrderBy(i => i.DisplayName))
            {
                if (!string.IsNullOrEmpty(culture.Name))
                {
                    Ddlanguage.Items.Add(new ListItem(culture.DisplayName, culture.Name));
                }
            }

            var SqlCon1 = new SqlConnection(_connectionString);
            DataSet ds = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter("Select * from dbo.Languages", SqlCon1);
            ad.Fill(ds);
            GridView1.AutoGenerateColumns = false;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count != 0)
                {
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
        }

        protected void AddLanguage_Click(object sender, EventArgs e)
        {
            if (Ddlanguage.SelectedItem.Value != "-1")
            {
                if (!CheckIfLanguageExists(Ddlanguage.SelectedItem.Value))
                {
                    string query = string.Empty;
                    query += "INSERT INTO Languages (LanguageName, LanguageAbbr)  ";
                    query += "VALUES (@1, @2)";

                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        using (SqlCommand comm = new SqlCommand())
                        {
                            {
                                var withBlock = comm;
                                withBlock.Connection = conn;
                                withBlock.CommandType = CommandType.Text;
                                withBlock.CommandText = query;
                                withBlock.Parameters.AddWithValue("@1", Ddlanguage.SelectedItem.Text);
                                withBlock.Parameters.AddWithValue("@2", Ddlanguage.SelectedItem.Value);
                            }
                            try
                            {
                                conn.Open();
                                comm.ExecuteNonQuery();
                                SuccessMessage.Text = "Language added to the system successfully";
                                Response.Redirect(Request.RawUrl);
                            }
                            catch (SqlException ex)
                            {
                                ErrorMessage.Text = ex.Message.ToString();
                            }
                        }
                    }
                }else
                {
                    ErrorMessage.Text = "Selected Language have been already added!";
                }
            }
        }

        public bool CheckIfLanguageExists(string labbr)
        {
            bool exists = false;
            var SqlConnection2 = new SqlConnection();
            SqlConnection2.ConnectionString = _connectionString;
            SqlCommand cmd1 = new SqlCommand("Select * from Languages where LanguageAbbr = @LID", SqlConnection2);
            cmd1.Parameters.AddWithValue("@LID", labbr);
            cmd1.Connection.Open();
            SqlDataReader reader1 = cmd1.ExecuteReader(CommandBehavior.CloseConnection);
            exists = (reader1.HasRows);
            cmd1.Connection.Close();
            return exists;
        }

    }
}