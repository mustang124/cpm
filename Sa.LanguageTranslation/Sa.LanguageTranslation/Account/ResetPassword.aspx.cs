﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Solomon.LanguageTranslation.Models;

namespace Solomon.LanguageTranslation.Account
{
    public partial class ResetPassword : Page
    {
        protected string StatusMessage
        {
            get;
            private set;
        }

        protected void Reset_Click(object sender, EventArgs e)
        {
            //string code = IdentityHelper.GetCodeFromRequest(Request);
            //if (code != null)
            //{
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                //var user = manager.FindByName(Email.Text);
                //if (user == null)
                //{
                //    ErrorMessage.Text = "No user found";
                //    return;
                //}
                string resetToken = manager.GeneratePasswordResetToken("89393032-9c6c-4b81-ba6e-861f1509883d");
                var result = manager.ResetPassword("89393032-9c6c-4b81-ba6e-861f1509883d", resetToken, Password.Text);
                if (result.Succeeded)
                {
                    Response.Redirect("~/Account/ResetPasswordConfirmation");
                    return;
                }
                ErrorMessage.Text = result.Errors.FirstOrDefault();
                return;
            //}

            //ErrorMessage.Text = "An error has occurred";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }
        }
    }
}