﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using Solomon.LanguageTranslation.Models;

namespace Solomon.LanguageTranslation.Account
{
    public partial class ResetPasswordConfirmation : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!((HttpContext.Current.User != null) && HttpContext.Current.User.Identity.IsAuthenticated))
            {
                Response.Redirect("/Account/Login");
            }
        }
    }
}