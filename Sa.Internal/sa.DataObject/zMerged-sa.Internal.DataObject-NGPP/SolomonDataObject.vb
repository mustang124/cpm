﻿Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Collections
Imports System.Configuration

Public Class DataObject
    Enum StudyTypes
        REFINING
        OLEFINS
        OLEFINSLEGACY
        POWER
        RAM
        PIPELINES
        REFINING_GLOBAL
        REFINING_DEV
        REFININGDEV
        NGPP
    End Enum
    Private _conn As SqlConnection
    Private _connectionString As String
    'Private study As String
    Private _studyType As StudyTypes

    Private _error As String
    Public Property DBError() As String
        Get
            Return _error
        End Get
        Set(ByVal value As String)
            _error = value
        End Set
    End Property

    Public Property StudyType() As StudyTypes
        Get
            Return _studyType
        End Get
        Set(ByVal value As StudyTypes)
            _studyType = value
        End Set
    End Property

    Public Property ConnectionString() As String
        Get
            Return _connectionString
        End Get
        Set(ByVal value As String)
            _connectionString = value
        End Set
    End Property

    Private mStudyYear As String
    Public Property StudyYear() As String
        Get
            Return mStudyYear
        End Get
        Set(ByVal value As String)
            mStudyYear = value
        End Set
    End Property
    Private _DBCommand As String
    Public Property DBCommand() As String
        Get
            Return _DBCommand
        End Get
        Set(ByVal value As String)
            _DBCommand = value
        End Set
    End Property

    Private _SQL As String
    Public Property SQL() As String
        Get
            Return _SQL
        End Get
        Set(ByVal value As String)
            _SQL = value
        End Set
    End Property


    Public Sub New(studyType As StudyTypes, UserName As String, Password As String)
        _conn = New SqlConnection(ConfigurationManager.ConnectionStrings(studyType.ToString()).ConnectionString)
        _connectionString = _conn.ConnectionString
        Try
            _conn.Open()
        Catch ex As Exception
            If ex.Message.Contains("provider: Named Pipes Provider, error: 40 - Could not open a connection to SQL Server") Then
                For triesCount As Integer = 1 To 5
                    Dim msg As String = TryAgain(_conn)
                    If msg.Length < 1 Or Not msg.Contains("provider: Named Pipes Provider, error: 40 - Could not open a connection to SQL Server") Then Exit For
                    Debug.WriteLine("######## Sql Conx Failures: " + triesCount.ToString())
                Next
            Else
                DBError = ex.Message
            End If
        End Try
        CloseConx()
    End Sub

    Private Function TryAgain(ByRef sqlconn As SqlConnection) As String
        Try
            CloseConx()
            sqlconn.Open()
            Return String.Empty
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function IsValid(DataConnection As String) As Boolean

        Dim Valid As Boolean = False

        Try
            If Not OpenConx() Then Return False
            Valid = True

        Catch ex As System.Exception
            Valid = False
        End Try
        CloseConx()
        Return Valid

    End Function
    Public Function GetCompanyPassword(params As List(Of String)) As DataSet
        Dim ds As DataSet

        ds = ExecuteStoredProc("CONSOLE.[GetCompanyPassword]", params)

        Return ds
    End Function
    Public Function GetCompanyContacts(params As List(Of String)) As DataSet
        Dim ds As DataSet

        ds = ExecuteStoredProc("CONSOLE.[GetCompanyContactInfo]", params)

        Return ds

    End Function
    Public Function SaveCompanyContact(params As List(Of String)) As Integer
        Dim ret As Integer = 0
        For Each item As String In params
            If item.Contains("StudyYear") Then
                '//"StudyYear/" + "20" + cboStudy.Text.Substring(3, 2))
                Dim paramSplit As String() = item.Split("/")
                If paramSplit(1).Length < 1 Then
                    Throw New Exception("Invalid call to CONSOLE.[UpdateCompanyContact]: Study year '" + paramSplit(1) + "' is invalid")
                End If
            End If
        Next

        ret = ExecuteNonQuery("CONSOLE.[UpdateCompanyContact]", params)

        Return ret
    End Function
    Public Function GetContacts(params As List(Of String)) As DataSet
        Dim ds As DataSet

        ds = ExecuteStoredProc("CONSOLE.[GetContactInfo]", params)

        Return ds

    End Function


    Public Function GetParameters(ByRef cmd As SqlCommand, params As List(Of String)) As SqlCommand
        If Not params Is Nothing Then

            Dim pm() As String
            Dim p As String

            For Each p In params
                pm = p.Split("/")
                cmd.Parameters.AddWithValue(pm(0), pm(1))
            Next

            Return cmd
        Else
            Return cmd
        End If

    End Function
    Public Function ExecuteStoredProc(spName As String, Optional params As List(Of String) = Nothing) As DataSet

        Dim ds As New DataSet("Studies")
        Dim da As New SqlDataAdapter()
        Try
            Dim cmd As New SqlCommand(SQL)
            cmd.Connection = _conn
            cmd.CommandText = spName
            cmd = GetParameters(cmd, params)

            cmd.CommandType = CommandType.StoredProcedure
            If Not OpenConx() Then Return Nothing
            cmd.Connection = _conn
            da.SelectCommand = cmd
            da.Fill(ds)
        Catch Ex As System.Exception
            Dim exmsg As String = Ex.Message
        End Try
        CloseConx()
        Return ds

    End Function
    Public Function ExecuteReader(spName As String, Optional params As List(Of String) = Nothing) As SqlDataReader
        Dim rdr As SqlDataReader = Nothing
        Dim cmd As New SqlCommand()
        Try

            cmd.CommandText = spName
            cmd = GetParameters(cmd, params)
            cmd.CommandType = CommandType.StoredProcedure
            If Not OpenConx() Then Return Nothing
            cmd.Connection = _conn
            rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Catch Ex As System.Exception
            Dim err As String = Ex.Message
        End Try
        CloseConx()
        Return rdr
    End Function
    Public Function ExecuteReader() As SqlDataReader

        Dim rdr As SqlDataReader = Nothing
        Dim cmd As New SqlCommand(SQL)
        If Not OpenConx() Then Return Nothing
        cmd.Connection = _conn
        Try
            rdr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Catch
            CloseConx()
        End Try
        Return rdr

    End Function

    Public Function Execute() As DataSet

        Dim ds As New DataSet("Studies")
        Dim da As New SqlDataAdapter()
        Dim cmd As New SqlCommand(SQL)
        da.SelectCommand = cmd
        cmd.CommandType = CommandType.Text

        If Not OpenConx() Then Return Nothing
        cmd.Connection = _conn
        Try
            da.Fill(ds)
        Catch ex As Exception

        End Try
        CloseConx()
        Return ds

    End Function
    Public Function ExecuteNonQuery(spName As String, Optional params As List(Of String) = Nothing) As Integer

        Dim c As Integer = 0
        Dim cmd As New SqlCommand()
        cmd.Connection = _conn
        cmd.CommandText = spName
        cmd = GetParameters(cmd, params)

        cmd.CommandType = CommandType.StoredProcedure
        If Not OpenConx() Then Return Nothing
        cmd.Connection = _conn
        Try
            c = cmd.ExecuteNonQuery()

            CloseConx()
            Return c
        Catch Ex As System.Exception
            If Ex.Message.ToUpper.IndexOf("VIOLATION") >= 0 Then c = 1
            CloseConx()
            Return c
        End Try

    End Function
    Public Function ExecuteNonQuery() As Integer

        Dim count As Integer = 0
        Dim cmd As New SqlCommand(SQL)
        cmd.Connection = _conn
        If Not OpenConx() Then Return Nothing
        Try
            count = cmd.ExecuteNonQuery
        Catch
        End Try
        CloseConx()
        Return count

    End Function

    Public Function ExecuteScalar(spName As String, Optional params As List(Of String) = Nothing) As Integer

        Dim c As Integer = 0
        Dim cmd As New SqlCommand()
        cmd.Connection = _conn
        cmd.CommandText = spName
        cmd = GetParameters(cmd, params)

        cmd.CommandType = CommandType.StoredProcedure
        If Not OpenConx() Then Return Nothing
        cmd.Connection = _conn
        Try
            c = cmd.ExecuteScalar()
            CloseConx()
            Return c
        Catch Ex As System.Exception
            CloseConx()
            Return c
        End Try

    End Function

    Private Function OpenConx() As Boolean
        Try
            _conn.Open()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub CloseConx()
        Try
            _conn.Close()
        Catch
        End Try
    End Sub


    Protected Overrides Sub Finalize()
        CloseConx()
        MyBase.Finalize()
    End Sub
End Class
