﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using sa.Internal.Console.DataObject;
namespace QA
{
    public partial class frmAdmin : Form
    {
       
        
        sa.Internal.Console.DataObject.DataObject db;
        string Study;
        string StudyYear;
        int QuestionID=0;
        int iAnswerID = 0;
        string txtAnswer;
        string txtQuestion;
        
        

        public frmAdmin()
        {
            SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");
            InitializeComponent();
            GetConsultants();

            GetQuestions("ALL","ALL","ALL","ALL","");
            cboConsultant.SelectedIndex = 0;
            cboStudy.SelectedIndex = 0;
            cboCategory.SelectedIndex = 0;
            
        }

        private void GetConsultants()
        {
            DataSet ds;

            db = new sa.Internal.Console.DataObject.DataObject(sa.Internal.Console.DataObject.DataObject.StudyTypes.REFINING, "mgv", "solomon2012");

            ds = db.ExecuteStoredProc("Console.GetAllConsultants");
            cboConsultant.Items.Add("ALL");
            if (ds.Tables.Count > 0)
                foreach (DataRow Row in ds.Tables[0].Rows)
                    cboConsultant.Items.Add(Row["Consultant"].ToString().ToUpper().Trim() + " - " + Row["ConsultantName"].ToString().ToUpper().Trim());
        }

        private void UpdateParameters(string refnum, string study, string category, string consultant, string keywords)
        {
            cboCategory.SelectedIndexChanged -= cboCategory_SelectedIndexChanged;
            cboStudy.SelectedIndexChanged -= cboStudy_SelectedIndexChanged;
            cboRefNum.SelectedIndexChanged -= cboRefNum_SelectedIndexChanged;
            cboConsultant.SelectedIndexChanged -= cboConsultant_SelectedIndexChanged;
            txtKeywords.TextChanged -= txtKeywords_TextChanged;
            cboCategory.SelectedIndex = cboCategory.FindString(category);
            cboStudy.SelectedIndex = cboStudy.FindString(study);
            GetRefNums();
            cboRefNum.SelectedIndex = cboRefNum.FindString(refnum);
            cboConsultant.SelectedIndex = cboConsultant.FindString(consultant);
            txtKeywords.Text = keywords;
            cboCategory.SelectedIndexChanged += cboCategory_SelectedIndexChanged;
            cboStudy.SelectedIndexChanged += cboStudy_SelectedIndexChanged;
            cboRefNum.SelectedIndexChanged += cboRefNum_SelectedIndexChanged;
            cboConsultant.SelectedIndexChanged += cboConsultant_SelectedIndexChanged;
            txtKeywords.TextChanged += txtKeywords_TextChanged;
        }


        public void GetQuestions(string refnum, string study, string category, string consultant, string keywords)
        {
            UpdateParameters(refnum,study,category,consultant,keywords);
            SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");
            ClearGrids("Q");
            List<string> qs = new List<string>();
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetQuestions";
            cmd.Connection = cn;
            if (cn.State==ConnectionState.Closed) cn.Open();

            cmd.Parameters.AddWithValue("@RefNum", refnum);
            cmd.Parameters.AddWithValue("@Study", study);
            cmd.Parameters.AddWithValue("@Category", category);
            cmd.Parameters.AddWithValue("@Keywords",keywords);
            if (cboConsultant.Text.Length > 3)
                cmd.Parameters.AddWithValue("@Consultant", consultant.Substring(0, 3));
            else
                cmd.Parameters.AddWithValue("@Consultant", consultant);

            int id = 0;
            string Item = null;
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                id = dr.GetInt32(0);
                Item = dr.GetString(1);
                if (id>0)
                    dgQ.Rows.Add(id,Item);
            }

            dr.Close();
                 
            cn.Close();
            if (dgQ.Rows.Count > 0)
            GetAnswers(Convert.ToInt32(dgQ.Rows[0].Cells[0].Value));
           

        }

        public void GetQuestion(int id)
        {
            SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");
            List<string> qs = new List<string>();
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetQuestion";
            cmd.Connection = cn;
            if (cn.State == ConnectionState.Closed) cn.Open();

            cmd.Parameters.AddWithValue("@QuestionID", id);
            
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                cboCategory.SelectedIndexChanged -= cboCategory_SelectedIndexChanged;
                cboStudy.SelectedIndexChanged -= cboStudy_SelectedIndexChanged;
                cboRefNum.SelectedIndexChanged -= cboRefNum_SelectedIndexChanged;
                cboConsultant.SelectedIndexChanged -= cboConsultant_SelectedIndexChanged;
                txtKeywords.TextChanged -= txtKeywords_TextChanged;

                cboCategory.SelectedIndex = cboCategory.FindString(dr.GetString(0));
                cboStudy.SelectedIndex = cboStudy.FindString(dr.GetString(1));
                GetRefNums();
                cboRefNum.SelectedIndex = cboRefNum.FindString(dr.GetString(2));
                cboConsultant.SelectedIndex = cboConsultant.FindString(dr.GetString(3));
                txtKeywords.Text = dr.GetString(4);

                cboCategory.SelectedIndexChanged += cboCategory_SelectedIndexChanged;
                cboStudy.SelectedIndexChanged += cboStudy_SelectedIndexChanged;
                cboRefNum.SelectedIndexChanged += cboRefNum_SelectedIndexChanged;
                cboConsultant.SelectedIndexChanged += cboConsultant_SelectedIndexChanged;
                txtKeywords.TextChanged += txtKeywords_TextChanged;
            }

            dr.Close();
            cn.Close();



        }

        public void GetAnswers(int id)
        {
            SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");
            ClearGrids("A");
            List<string> qs = new List<string>();
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetAnswers";
            cmd.Connection = cn;
            if (cn.State == ConnectionState.Closed) cn.Open();

            cmd.Parameters.AddWithValue("@QuestionID", id);
            string Item = null;
            int nid;
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                nid = dr.GetInt32(0);
                Item = dr.GetString(1);
                if (Item.Length > 0)
                    dgA.Rows.Add(nid, Item);
            }

            dr.Close();
            cn.Close();

            

        }

        private void GetRefNums()
        {
            cboCompany.SelectedIndexChanged -= cboCompany_SelectedIndexChanged;
            
            cboRefNum.Items.Clear();
            if (cboStudy.Text == "ALL")
            {
                cboRefNum.Items.Add("ALL");
                cboCompany.Items.Add("ALL");
                cboRefNum.SelectedIndex = 0;
                cboCompany.SelectedIndex = 0;
                return;
            }
            cboRefNum.Items.Add("ALL");
            cboCompany.Items.Add("ALL");
            DataSet ds;
            List<string> p = new List<string>();
            StudyYear = "20" + cboStudy.Text.Substring(3, 2);
            Study = cboStudy.Text.Substring(0, 3);
            p.Add("StudyYear/" + StudyYear);
            p.Add("Study/" + Study);

           ds = db.ExecuteStoredProc("Console.GetRefNums", p);
           
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                cboRefNum.Items.Add(row["RefNum"].ToString().Trim());
                cboCompany.Items.Add(row["CoLoc"].ToString().Trim());
               
            }

            cboRefNum.SelectedIndex = 0;
            cboCompany.SelectedIndex = 0;
            cboCompany.SelectedIndexChanged += cboCompany_SelectedIndexChanged;
            
        }

        private void cboStudy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboStudy.SelectedIndex != -1)
            {
                GetRefNums();
                RefreshGrid();
            }
        }


        private void GetRefNumByCompany()
        {

            DataSet ds;
            List<string> p = new List<string>();

            p.Add("Company/" + cboCompany.Text);
            p.Add("StudyYear/" + StudyYear);
            p.Add("Study/" + Study);

            ds = db.ExecuteStoredProc("Console.GetRefNumByCompany", p);
            if (ds.Tables.Count > 0)
            {
            if (ds.Tables[0].Rows.Count > 0)
            cboRefNum.SelectedIndex = cboRefNum.FindString(ds.Tables[0].Rows[0]["RefNum"].ToString().Trim());
            }
           

        }


        private void GetCompanyByRefNum()
        {
            DataSet ds;
            List<string> p = new List<string>();

            p.Add("RefNum/" + cboRefNum.Text);
          
            ds = db.ExecuteStoredProc("Console.GetCompanyName", p);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                    cboCompany.SelectedIndex = cboCompany.FindString(ds.Tables[0].Rows[0]["Coloc"].ToString().Trim());
            }
                  
                   
        }


        private void btnSaveQuestion_Click(object sender, EventArgs e)
        {
            SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SaveQuestion";
            cmd.Connection = cn;
            if (cn.State == ConnectionState.Closed) cn.Open();

            
            cmd.Parameters.AddWithValue("@RefNum", (cboRefNum.Text=="ALL")?"":cboRefNum.Text);
            cmd.Parameters.AddWithValue("@Study", (cboStudy.Text=="ALL") ? "" : cboStudy.Text);
            cmd.Parameters.AddWithValue("@Category",(cboCategory.Text=="ALL") ? "" : cboCategory.Text);
            cmd.Parameters.AddWithValue("@Keywords",txtKeywords.Text);
            if (cboConsultant.Text.Length > 3)
                cmd.Parameters.AddWithValue("@Consultant", cboConsultant.Text.Substring(0, 3));
            else
                cmd.Parameters.AddWithValue("@Consultant", cboConsultant.Text);

                QuestionID = Convert.ToInt32(cmd.ExecuteScalar());
                if (QuestionID > 0) btnAddAnswer.Enabled = true;

            
            cn.Close();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Are you sure?", "Clearing ALL Data", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
            
            cboStudy.SelectedIndex = -1;
            cboRefNum.SelectedIndex = -1;
            cboCompany.SelectedIndex = -1;
            cboConsultant.SelectedIndex = -1;
            
            txtKeywords.Text = "";
            cboCategory.SelectedIndex = -1;
            btnAddAnswer.Enabled = false;
            
            
            }
        }

        private void cboRefNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCompanyByRefNum();
            RefreshGrid();
            
        }

        private void cboCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetRefNumByCompany();
            RefreshGrid();
            
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            cboCategory.SelectedIndex = 0;
            cboStudy.SelectedIndex = 0;
            cboRefNum.SelectedIndex = 0;
            cboCompany.SelectedIndex = 0;
            cboConsultant.SelectedIndex = 0;
            txtKeywords.Text = "";
        }

        private void ClearGrids(string qa)
        {
            if (qa == "A")
            {
                dgA.Rows.Clear();
                dgA.Refresh();
            }

            else if (qa == "Q")
            {
                dgQ.Rows.Clear();
                dgQ.Refresh();
            }
            else
            {
                dgA.Rows.Clear();
                dgA.Refresh();
                dgQ.Rows.Clear();
                dgQ.Refresh();
            }



           
        }


        public void RefreshGrid()
        {
            ClearGrids("");
            GetQuestions(cboRefNum.Text, cboStudy.Text, cboCategory.Text, cboConsultant.Text, txtKeywords.Text);
        }

        private void dgQ_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ClearGrids("A");
            QuestionID = Convert.ToInt32(dgQ.SelectedCells[0].Value);
            txtQuestion = dgQ.SelectedCells[1].Value.ToString();
            GetQuestion(QuestionID);
            GetAnswers(QuestionID);
            
        }

        private void dgQ_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            Form frm = new Edit("Edit", "Question", QuestionID, 0, dgQ.SelectedCells[1].Value.ToString(),"", cboRefNum.Text, cboStudy.Text, cboCategory.Text, txtKeywords.Text,cboConsultant.Text);
            
            frm.Show();
            
        }

        private void dgA_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            iAnswerID = Convert.ToInt32(dgA.SelectedCells[0].Value);
            txtAnswer = dgA.SelectedCells[1].Value.ToString();
            QuestionID = Convert.ToInt32(dgQ.SelectedCells[0].Value);
            txtQuestion = dgQ.SelectedCells[1].Value.ToString();
            
            Form frm = new Edit("Edit", "Answer", QuestionID, iAnswerID,txtQuestion, dgA.SelectedCells[1].Value.ToString(), cboRefNum.Text, cboStudy.Text, cboCategory.Text, txtKeywords.Text, cboConsultant.Text);
            frm.Show();
        }


        private void btnAddAnswer_Click(object sender, EventArgs e)
        {
            QuestionID = Convert.ToInt32(dgQ.SelectedCells[0].Value);
            txtQuestion = dgQ.SelectedCells[1].Value.ToString();
            Form frm = new Edit("Insert", "Answer", QuestionID, 0, txtQuestion,txtAnswer,cboRefNum.Text, cboStudy.Text, cboCategory.Text, txtKeywords.Text, cboConsultant.Text);
            frm.Show();
        }

        private void btnAddQ_Click(object sender, EventArgs e)
        {
            Form frm = new Edit("Insert", "Question", 0, 0, null,null, cboRefNum.Text, cboStudy.Text, cboCategory.Text, txtKeywords.Text, cboConsultant.Text);
            frm.Show();
        }

        private void cboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void cboConsultant_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void txtKeywords_TextChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }



        private void dgQ_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteQuestion();
            }

        }

        private void DeleteQuestion()
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete?", "Wait", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                QuestionID = Convert.ToInt32(dgQ.SelectedCells[0].Value);

                SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");
                
                List<string> qs = new List<string>();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "DeleteQuestion";
                cmd.Connection = cn;
                if (cn.State == ConnectionState.Closed) cn.Open();

                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);

                cmd.ExecuteNonQuery();
                RefreshGrid();
            }
        }

        private void DeleteAnswer()
        {
            DialogResult dr = MessageBox.Show("Are you sure you want to delete?", "Wait", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                iAnswerID = Convert.ToInt32(dgA.SelectedCells[0].Value);

                SqlConnection cn = new SqlConnection("Data Source=10.10.27.45;Initial Catalog=DevelopQA;User Id=MGV;Password=solomon2012;");

                List<string> qs = new List<string>();

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "DeleteAnswer";
                cmd.Connection = cn;
                if (cn.State == ConnectionState.Closed) cn.Open();

                cmd.Parameters.AddWithValue("@AnswerID", iAnswerID);

                cmd.ExecuteNonQuery();
                RefreshGrid();
            }
        }

        private void dgA_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DeleteAnswer();
            }
        }


   


       



        

        

                    
        }
  
        
}
