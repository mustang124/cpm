﻿namespace QA
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cboCompany = new System.Windows.Forms.ComboBox();
            this.lblCompany = new System.Windows.Forms.Label();
            this.Label60 = new System.Windows.Forms.Label();
            this.cboRefNum = new System.Windows.Forms.ComboBox();
            this.Label59 = new System.Windows.Forms.Label();
            this.cboStudy = new System.Windows.Forms.ComboBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.cboConsultant = new System.Windows.Forms.ComboBox();
            this.dgQ = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddAnswer = new System.Windows.Forms.Button();
            this.dgA = new System.Windows.Forms.DataGridView();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboCategory = new System.Windows.Forms.ComboBox();
            this.btnAddQ = new System.Windows.Forms.Button();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Question = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStudy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cRefNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cConsultant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cKeywords = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AnswerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Answer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgA)).BeginInit();
            this.SuspendLayout();
            // 
            // cboCompany
            // 
            this.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCompany.FormattingEnabled = true;
            this.cboCompany.Location = new System.Drawing.Point(420, 43);
            this.cboCompany.Name = "cboCompany";
            this.cboCompany.Size = new System.Drawing.Size(276, 24);
            this.cboCompany.Sorted = true;
            this.cboCompany.TabIndex = 101;
            this.cboCompany.SelectedIndexChanged += new System.EventHandler(this.cboCompany_SelectedIndexChanged);
            // 
            // lblCompany
            // 
            this.lblCompany.AutoSize = true;
            this.lblCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.ForeColor = System.Drawing.Color.Maroon;
            this.lblCompany.Location = new System.Drawing.Point(330, 43);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(65, 17);
            this.lblCompany.TabIndex = 100;
            this.lblCompany.Text = "Refinery:";
            // 
            // Label60
            // 
            this.Label60.AutoSize = true;
            this.Label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label60.ForeColor = System.Drawing.Color.Maroon;
            this.Label60.Location = new System.Drawing.Point(24, 78);
            this.Label60.Name = "Label60";
            this.Label60.Size = new System.Drawing.Size(63, 17);
            this.Label60.TabIndex = 99;
            this.Label60.Text = "RefNum:";
            // 
            // cboRefNum
            // 
            this.cboRefNum.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboRefNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRefNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboRefNum.FormattingEnabled = true;
            this.cboRefNum.Location = new System.Drawing.Point(109, 76);
            this.cboRefNum.Name = "cboRefNum";
            this.cboRefNum.Size = new System.Drawing.Size(216, 24);
            this.cboRefNum.TabIndex = 98;
            this.cboRefNum.SelectedIndexChanged += new System.EventHandler(this.cboRefNum_SelectedIndexChanged);
            // 
            // Label59
            // 
            this.Label59.AutoSize = true;
            this.Label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label59.ForeColor = System.Drawing.Color.Maroon;
            this.Label59.Location = new System.Drawing.Point(24, 43);
            this.Label59.Name = "Label59";
            this.Label59.Size = new System.Drawing.Size(48, 17);
            this.Label59.TabIndex = 97;
            this.Label59.Text = "Study:";
            // 
            // cboStudy
            // 
            this.cboStudy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStudy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboStudy.FormattingEnabled = true;
            this.cboStudy.Items.AddRange(new object[] {
            "ALL",
            "NSA14",
            "EUR14",
            "PAC14",
            "LUB14",
            "NSA12",
            "EUR12",
            "PAC12",
            "LUB12"});
            this.cboStudy.Location = new System.Drawing.Point(109, 43);
            this.cboStudy.Name = "cboStudy";
            this.cboStudy.Size = new System.Drawing.Size(216, 24);
            this.cboStudy.TabIndex = 96;
            this.cboStudy.SelectedIndexChanged += new System.EventHandler(this.cboStudy_SelectedIndexChanged);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.ForeColor = System.Drawing.Color.Maroon;
            this.Label8.Location = new System.Drawing.Point(330, 12);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(79, 17);
            this.Label8.TabIndex = 111;
            this.Label8.Text = "Consultant:";
            // 
            // cboConsultant
            // 
            this.cboConsultant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboConsultant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboConsultant.FormattingEnabled = true;
            this.cboConsultant.Location = new System.Drawing.Point(420, 11);
            this.cboConsultant.MaxLength = 3;
            this.cboConsultant.Name = "cboConsultant";
            this.cboConsultant.Size = new System.Drawing.Size(276, 24);
            this.cboConsultant.TabIndex = 110;
            this.cboConsultant.SelectedIndexChanged += new System.EventHandler(this.cboConsultant_SelectedIndexChanged);
            // 
            // dgQ
            // 
            this.dgQ.AllowUserToAddRows = false;
            this.dgQ.AllowUserToDeleteRows = false;
            this.dgQ.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dgQ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgQ.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Question,
            this.cCategory,
            this.cStudy,
            this.cRefNum,
            this.cConsultant,
            this.cKeywords});
            this.dgQ.Location = new System.Drawing.Point(19, 135);
            this.dgQ.Name = "dgQ";
            this.dgQ.ReadOnly = true;
            this.dgQ.RowHeadersVisible = false;
            this.dgQ.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgQ.Size = new System.Drawing.Size(790, 238);
            this.dgQ.TabIndex = 114;
            this.dgQ.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgQ_CellClick);
            this.dgQ.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgQ_CellDoubleClick);
            this.dgQ.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgQ_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(417, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 121;
            this.label3.Text = "(Separated by Commas)";
            // 
            // txtKeywords
            // 
            this.txtKeywords.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKeywords.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtKeywords.Location = new System.Drawing.Point(420, 71);
            this.txtKeywords.Multiline = true;
            this.txtKeywords.Name = "txtKeywords";
            this.txtKeywords.Size = new System.Drawing.Size(389, 33);
            this.txtKeywords.TabIndex = 120;
            this.txtKeywords.TextChanged += new System.EventHandler(this.txtKeywords_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(330, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 119;
            this.label2.Text = "Keywords:";
            // 
            // btnAddAnswer
            // 
            this.btnAddAnswer.Location = new System.Drawing.Point(724, 657);
            this.btnAddAnswer.Name = "btnAddAnswer";
            this.btnAddAnswer.Size = new System.Drawing.Size(85, 23);
            this.btnAddAnswer.TabIndex = 118;
            this.btnAddAnswer.Text = "Add Answer";
            this.btnAddAnswer.UseVisualStyleBackColor = true;
            this.btnAddAnswer.Click += new System.EventHandler(this.btnAddAnswer_Click);
            // 
            // dgA
            // 
            this.dgA.AllowUserToAddRows = false;
            this.dgA.AllowUserToDeleteRows = false;
            this.dgA.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dgA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AnswerID,
            this.Answer});
            this.dgA.Location = new System.Drawing.Point(19, 416);
            this.dgA.Name = "dgA";
            this.dgA.ReadOnly = true;
            this.dgA.RowHeadersVisible = false;
            this.dgA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgA.Size = new System.Drawing.Size(790, 225);
            this.dgA.TabIndex = 124;
            this.dgA.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgA_CellDoubleClick);
            this.dgA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgA_KeyDown);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(734, 11);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 125;
            this.btnRefresh.Text = "Reset";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 127;
            this.label1.Text = "Category:";
            // 
            // cboCategory
            // 
            this.cboCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.Items.AddRange(new object[] {
            "ALL",
            "Refinery History",
            "Study Boundary (Table 1)",
            "Material Balance",
            "Energy",
            "Personnel",
            "Maintenance",
            "Operating Expenses",
            "Process Data (Table 2)",
            "Miscellaneous",
            "Pricing",
            "Results Presentation"});
            this.cboCategory.Location = new System.Drawing.Point(109, 10);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(216, 24);
            this.cboCategory.TabIndex = 126;
            this.cboCategory.SelectedIndexChanged += new System.EventHandler(this.cboCategory_SelectedIndexChanged);
            // 
            // btnAddQ
            // 
            this.btnAddQ.Location = new System.Drawing.Point(724, 379);
            this.btnAddQ.Name = "btnAddQ";
            this.btnAddQ.Size = new System.Drawing.Size(85, 23);
            this.btnAddQ.TabIndex = 128;
            this.btnAddQ.Text = "Add Question";
            this.btnAddQ.UseVisualStyleBackColor = true;
            this.btnAddQ.Click += new System.EventHandler(this.btnAddQ_Click);
            // 
            // ID
            // 
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ID.DefaultCellStyle = dataGridViewCellStyle1;
            this.ID.HeaderText = "Question ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            // 
            // Question
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Question.DefaultCellStyle = dataGridViewCellStyle2;
            this.Question.HeaderText = "Question";
            this.Question.Name = "Question";
            this.Question.ReadOnly = true;
            this.Question.Width = 785;
            // 
            // cCategory
            // 
            this.cCategory.HeaderText = "Column1";
            this.cCategory.Name = "cCategory";
            this.cCategory.ReadOnly = true;
            this.cCategory.Visible = false;
            // 
            // cStudy
            // 
            this.cStudy.HeaderText = "Column1";
            this.cStudy.Name = "cStudy";
            this.cStudy.ReadOnly = true;
            this.cStudy.Visible = false;
            // 
            // cRefNum
            // 
            this.cRefNum.HeaderText = "Column1";
            this.cRefNum.Name = "cRefNum";
            this.cRefNum.ReadOnly = true;
            this.cRefNum.Visible = false;
            // 
            // cConsultant
            // 
            this.cConsultant.HeaderText = "Column1";
            this.cConsultant.Name = "cConsultant";
            this.cConsultant.ReadOnly = true;
            this.cConsultant.Visible = false;
            // 
            // cKeywords
            // 
            this.cKeywords.HeaderText = "Column1";
            this.cKeywords.Name = "cKeywords";
            this.cKeywords.ReadOnly = true;
            this.cKeywords.Visible = false;
            // 
            // AnswerID
            // 
            this.AnswerID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AnswerID.DefaultCellStyle = dataGridViewCellStyle3;
            this.AnswerID.HeaderText = "AnswerID";
            this.AnswerID.Name = "AnswerID";
            this.AnswerID.ReadOnly = true;
            this.AnswerID.Visible = false;
            this.AnswerID.Width = 59;
            // 
            // Answer
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Answer.DefaultCellStyle = dataGridViewCellStyle4;
            this.Answer.HeaderText = "Answer";
            this.Answer.Name = "Answer";
            this.Answer.ReadOnly = true;
            this.Answer.Width = 785;
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(841, 692);
            this.Controls.Add(this.btnAddQ);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboCategory);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgA);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtKeywords);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAddAnswer);
            this.Controls.Add(this.dgQ);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.cboConsultant);
            this.Controls.Add(this.cboCompany);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.Label60);
            this.Controls.Add(this.cboRefNum);
            this.Controls.Add(this.Label59);
            this.Controls.Add(this.cboStudy);
            this.Name = "frmAdmin";
            this.Text = "Q & A Administration";
            ((System.ComponentModel.ISupportInitialize)(this.dgQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ComboBox cboCompany;
        internal System.Windows.Forms.Label lblCompany;
        internal System.Windows.Forms.Label Label60;
        internal System.Windows.Forms.ComboBox cboRefNum;
        internal System.Windows.Forms.Label Label59;
        internal System.Windows.Forms.ComboBox cboStudy;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.ComboBox cboConsultant;
        private System.Windows.Forms.DataGridView dgQ;
      
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.TextBox txtKeywords;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddAnswer;
        private System.Windows.Forms.DataGridView dgA;
        private System.Windows.Forms.Button btnRefresh;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox cboCategory;
        private System.Windows.Forms.Button btnAddQ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Question;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStudy;
        private System.Windows.Forms.DataGridViewTextBoxColumn cRefNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn cConsultant;
        private System.Windows.Forms.DataGridViewTextBoxColumn cKeywords;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnswerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Answer;
    }
}

