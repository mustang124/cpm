﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Management;
using System.Threading;
using System.Net;

namespace sa.SystemInformation
{
    public class NetworkManager
    {
        NetworkInfo m_Informations;
        public NetworkInfo Informations
        {
            get { return m_Informations; }
        }

        private string ParseProperty(object data)
        {
            if (data != null)
                return data.ToString();
            return "";
        }

        private void SetIP(NetworkInfo info)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            foreach (ManagementObject mo in objMC.GetInstances())
            {
                try
                {
                    if (!(bool)mo["ipEnabled"])
                        continue;
                    if (mo["MACAddress"].ToString().Equals(info.MacAddress))
                    {
                        string[] ip = (string[])mo["IPAddress"];
                        info.IP = ip[0];
                        string[] mask = (string[])mo["IPSubnet"];
                        info.Mask = mask[0];
                        string[] gateway = (string[])mo["DefaultIPGateway"];
                        info.DefaultGateway = gateway[0];
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("[SetIP]:" + ex.Message);
                }
            }
        }

        public NetworkInfo NetworkStatus()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionID IS NOT NULL");
            NetworkInfo info = new NetworkInfo();
            foreach (ManagementObject mo in searcher.Get())
            {
                
                info.DeviceName = ParseProperty(mo["Description"]);
                info.AdapterType = ParseProperty(mo["AdapterType"]);
                info.MacAddress = ParseProperty(mo["MACAddress"]);
                info.ConnectionID = ParseProperty(mo["NetConnectionID"]);
                info.Status = (NetConnectionStatus)Convert.ToInt32(mo["NetConnectionStatus"]);
                SetIP(info);
                
            }
            return info;
        }
    }
    public enum NetConnectionStatus
    {
        Disconnected = 0,
        Connecting = 1,
        Connected = 2,
        Disconnecting = 3,
        HardwareNotPresent = 4,
        HardwareDisabled = 5,
        HardwareMalfunction = 6,
        MediaDisconnected = 7,
        Authenticating = 8,
        AuthenticationSucceeded = 9,
        AuthenticationFailed = 10,
        InvalidAddress = 11,
        CredentialsRequired = 12
    }

    public class NetworkInfo
    {
        string m_DeviceName;
        public string DeviceName
        {
            get { return m_DeviceName; }
            set { m_DeviceName = value; }
        }
        string m_MacAddress;
        public string MacAddress
        {
            get { return m_MacAddress; }
            set { m_MacAddress = value; }
        }
        string m_AdapterType;
        public string AdapterType
        {
            get { return m_AdapterType; }
            set { m_AdapterType = value; }
        }
        string m_IP;
        public string IP
        {
            get { return m_IP; }
            set { m_IP = value; }
        }
        string m_Mask;
        public string Mask
        {
            get { return m_Mask; }
            set { m_Mask = value; }
        }
        string m_DefaultGateway;
        public string DefaultGateway
        {
            get { return m_DefaultGateway; }
            set { m_DefaultGateway = value; }
        }
        string m_ConnectionID;
        public string ConnectionID
        {
            get { return m_ConnectionID; }
            set { m_ConnectionID = value; }
        }
        NetConnectionStatus m_status;
        public NetConnectionStatus Status
        {
            get { return m_status; }
            set { m_status = value; }
        }

        public string GetHelp()
        {
            string t_msg = "Normal Connection.";
            if (m_status == NetConnectionStatus.Connected)
            {
                t_msg = "Connect succeed.";
            }
            else if (m_status == NetConnectionStatus.Disconnected)
            {
                t_msg = "Your connection was disable, please check Network Setting in Console.";
            }
            else if (m_status == NetConnectionStatus.MediaDisconnected)
            {
                t_msg = "Cable had bad contact with Network Card! Please check it.";
            }
            else if (m_status == NetConnectionStatus.InvalidAddress)
            {
                t_msg = "IP address is Invalid, please check DHCP/Router or IP setting.";
            }
            else
            {
                t_msg = string.Format("NetConnectionStatus is {0}", m_status.ToString());
            }

            return t_msg;
        }
    }
}
