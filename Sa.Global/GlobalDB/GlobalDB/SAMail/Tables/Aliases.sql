﻿CREATE TABLE [SAMail].[Aliases] (
    [CompanyID] VARCHAR (10) NOT NULL,
    [Alias]     VARCHAR (50) NOT NULL,
    [AliasType] INT          NULL
);

