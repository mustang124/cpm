﻿CREATE ROLE [Admin Support]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [Admin Support] ADD MEMBER [DC1\APCStudy_Admins];


GO
ALTER ROLE [Admin Support] ADD MEMBER [DC1\SupportDatabases_Admins];


GO
ALTER ROLE [Admin Support] ADD MEMBER [DC1\NGPP_Admins];


GO
ALTER ROLE [Admin Support] ADD MEMBER [DC1\Refining_Admins];


GO
ALTER ROLE [Admin Support] ADD MEMBER [DC1\Pipeline_Admins];


GO
ALTER ROLE [Admin Support] ADD MEMBER [DC1\Power_Admins];

