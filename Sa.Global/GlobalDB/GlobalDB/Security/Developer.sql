﻿CREATE ROLE [Developer]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [Developer] ADD MEMBER [DC1\SupportDatabases_Developers];


GO
ALTER ROLE [Developer] ADD MEMBER [CalcEngine];


GO
ALTER ROLE [Developer] ADD MEMBER [sbard];

