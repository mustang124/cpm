﻿CREATE ROLE [Consultants]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\SupportDatabases_Consultants];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\APCStudy_Consultants];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\Chemicals_Admins];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\Chemicals_Consultants];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\NGPP_Consultants];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\Refining_Consultants];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\Pipeline_Consultants];


GO
ALTER ROLE [Consultants] ADD MEMBER [DC1\Power_Consultants];

