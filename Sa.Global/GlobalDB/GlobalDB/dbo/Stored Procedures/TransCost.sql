﻿CREATE PROCEDURE TransCost (@StudyYear StudyYear, 
	@Period Period = 'TY',
	@CrudeOrigin PricingLocation, 
	@CrudeDest PricingLocation, 
	@BblAdder real OUTPUT,
	@MTAdder real OUTPUT,
	@PcntAdj real OUTPUT, 
	@Details bit = 0)
AS
DECLARE @tblTrans table (
	AdjType char(2) NOT NULL,
	BblAdder real NULL,
	MTAdder real NULL,
	PcntAdj real NULL
	)
INSERT INTO @tblTrans
SELECT T.AdjType, 
BblAdder = CASE WHEN TL.AdjMethod='+' THEN T.AdjValue END, 
MTAdder = CASE WHEN TL.AdjMethod='+' THEN T.AdjValueMT END, 
PcntAdj = CASE WHEN TL.AdjMethod='%' THEN T.AdjValue END
FROM Transportation T INNER JOIN TransAdjType_LU TL ON T.AdjType = TL.AdjType
WHERE T.StudyYear = @StudyYear AND T.Period = @Period
AND T.CrudeOrigin = @CrudeOrigin AND T.CrudeDest = @CrudeDest 
SELECT 	@BblAdder = SUM(BblAdder),
	@MTAdder = SUM(MTAdder),
	@PcntAdj = SUM(PcntAdj)
FROM @tblTrans
IF @Details = 1
BEGIN 
	DECLARE @strPrint varchar(255)
	SELECT @strPrint = 'For the period ' + @Period
		+ ' of ' + CONVERT(char(4), @StudyYear)
		+ ' from Origin ' + CONVERT(char(4), @CrudeOrigin)
		+ ' to Destination ' + CONVERT(char(4), @CrudeDest)
	PRINT @strPrint
	PRINT 'BblAdder  MTAdder   PcntAdj   '
	PRINT CONVERT(char(10), @BblAdder) + CONVERT(char(10), @MTAdder) + CONVERT(char(10), @PcntAdj)
	PRINT ''
	SELECT * FROM @tblTrans
END
