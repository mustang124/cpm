﻿
/****** Object:  Stored Procedure dbo.CheckCrudePrice    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE  PROCEDURE CheckCrudePrice;1 (
	@StudyYear smallint, @Period varchar(2), @CNum varchar(5), 
	@BasePrice real, @APIScale smallint, @PostedAPI real, @CrudeOrigin smallint,
	@PostedSulfur real = NULL, @SulfurAdjSlope real = NULL)
AS
DECLARE @NewPrice tinyint
EXEC CheckCrudePrice;2 @StudyYear, @Period, @CNum, @BasePrice OUTPUT,
	@APIScale OUTPUT, @PostedAPI OUTPUT, @CrudeOrigin OUTPUT, @NewPrice OUTPUT
	, @PostedSulfur OUTPUT, @SulfurAdjSlope OUTPUT
EXEC CheckCrudePrice;3 @BasePrice, @APIScale, @PostedAPI, @CrudeOrigin, @NewPrice, @PostedSulfur, @SulfurAdjSlope

