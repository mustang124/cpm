﻿
/****** Object:  Stored Procedure dbo.UpdateProductAdj    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE  PROCEDURE UpdateProductAdj;1 (
	@StudyYear smallint, @Period char(2) = 'TY', @PricingBasis char(4) = 'BASE',
	@PriceLoc smallint, @ProductID char(5), @Property char(5), 
	@MinValue1 real = NULL,
	@MaxValue1 real = NULL, @Adj1 real = NULL, @AdjMT1 real = NULL,
	@MaxValue2 real = NULL, @Adj2 real = NULL, @AdjMT2 real = NULL,
	@MaxValue3 real = NULL, @Adj3 real = NULL, @AdjMT3 real = NULL,
	@MaxValue4 real = NULL, @Adj4 real = NULL, @AdjMT4 real = NULL,
	@MaxValue5 real = NULL, @Adj5 real = NULL, @AdjMT5 real = NULL,
	@MaxValue6 real = NULL, @Adj6 real = NULL, @AdjMT6 real = NULL,
	@MaxValue7 real = NULL, @Adj7 real = NULL, @AdjMT7 real = NULL,
	@MaxValue8 real = NULL, @Adj8 real = NULL, @AdjMT8 real = NULL)
AS
DECLARE	@CurrMinValue1 real, 
	@CurrMaxValue1 real,  @CurrAdj1 real, @CurrAdjMT1 real,
	@CurrMaxValue2 real,  @CurrAdj2 real, @CurrAdjMT2 real,
	@CurrMaxValue3 real,  @CurrAdj3 real, @CurrAdjMT3 real,
	@CurrMaxValue4 real,  @CurrAdj4 real, @CurrAdjMT4 real,
	@CurrMaxValue5 real,  @CurrAdj5 real, @CurrAdjMT5 real,
	@CurrMaxValue6 real,  @CurrAdj6 real, @CurrAdjMT6 real,
	@CurrMaxValue7 real,  @CurrAdj7 real, @CurrAdjMT7 real,
	@CurrMaxValue8 real,  @CurrAdj8 real, @CurrAdjMT8 real
DECLARE @s1 char(1), @s2 char(1), @s3 char(1), @s4 char(1), 
	@s5 char(1), @s6 char(1), @s7 char(1), @s8 char(1)
IF @MinValue1 IS NULL AND @MaxValue1 Is NOT NULL
	SELECT @MinValue1 = -1000
SELECT	@CurrMinValue1 = @MinValue1, 
	@CurrMaxValue1 = @MaxValue1,  @CurrAdj1 = @Adj1, @CurrAdjMT1 = @AdjMT1,
	@CurrMaxValue2 = @MaxValue2,  @CurrAdj2 = @Adj2, @CurrAdjMT2 = @AdjMT2,
	@CurrMaxValue3 = @MaxValue3,  @CurrAdj3 = @Adj3, @CurrAdjMT3 = @AdjMT3,
	@CurrMaxValue4 = @MaxValue4,  @CurrAdj4 = @Adj4, @CurrAdjMT4 = @AdjMT4,
	@CurrMaxValue5 = @MaxValue5,  @CurrAdj5 = @Adj5, @CurrAdjMT5 = @AdjMT5,
	@CurrMaxValue6 = @MaxValue6,  @CurrAdj6 = @Adj6, @CurrAdjMT6 = @AdjMT6,
	@CurrMaxValue7 = @MaxValue7,  @CurrAdj7 = @Adj7, @CurrAdjMT7 = @AdjMT7,
	@CurrMaxValue8 = @MaxValue8,  @CurrAdj8 = @Adj8, @CurrAdjMT8 = @AdjMT8
EXEC CheckProductAdj;2 	@StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, @CurrMinValue1 OUTPUT,
	@CurrMaxValue1 OUTPUT, @CurrAdj1 OUTPUT, @CurrAdjMT1 OUTPUT,
	@CurrMaxValue2 OUTPUT, @CurrAdj2 OUTPUT, @CurrAdjMT2 OUTPUT,
	@CurrMaxValue3 OUTPUT, @CurrAdj3 OUTPUT, @CurrAdjMT3 OUTPUT,
	@CurrMaxValue4 OUTPUT, @CurrAdj4 OUTPUT, @CurrAdjMT4 OUTPUT,
	@CurrMaxValue5 OUTPUT, @CurrAdj5 OUTPUT, @CurrAdjMT5 OUTPUT,
	@CurrMaxValue6 OUTPUT, @CurrAdj6 OUTPUT, @CurrAdjMT6 OUTPUT,
	@CurrMaxValue7 OUTPUT, @CurrAdj7 OUTPUT, @CurrAdjMT7 OUTPUT,
	@CurrMaxValue8 OUTPUT, @CurrAdj8 OUTPUT, @CurrAdjMT8 OUTPUT,
	@S1 OUTPUT, @S2 OUTPUT, @S3 OUTPUT, @S4 OUTPUT,
	@S5 OUTPUT, @S6 OUTPUT, @S7 OUTPUT, @S8 OUTPUT
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 1, @s1, @MinValue1, @MaxValue1, @Adj1, @AdjMT1
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 2, @s2, @MaxValue1, @MaxValue2, @Adj2, @AdjMT2
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 3, @s3, @MaxValue2, @MaxValue3, @Adj3, @AdjMT3
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 4, @s4, @MaxValue3, @MaxValue4, @Adj4, @AdjMT4
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 5, @s5, @MaxValue4, @MaxValue5, @Adj5, @AdjMT5
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 6, @s6, @MaxValue5, @MaxValue6, @Adj6, @AdjMT6
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 7, @s7, @MaxValue6, @MaxValue7, @Adj7, @AdjMT7
EXEC UpdateProductAdj;2 @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Property, 8, @s8, @MaxValue7, @MaxValue8, @Adj8, @AdjMT8
EXEC CheckProductAdj;3 	@CurrMinValue1, 
	@CurrMaxValue1, @CurrAdj1, @CurrAdjMT1, 
	@CurrMaxValue2, @CurrAdj2, @CurrAdjMT2,
	@CurrMaxValue3, @CurrAdj3, @CurrAdjMT3, 
	@CurrMaxValue4, @CurrAdj4, @CurrAdjMT4,
	@CurrMaxValue5, @CurrAdj5, @CurrAdjMT5, 
	@CurrMaxValue6, @CurrAdj6, @CurrAdjMT6,
	@CurrMaxValue7, @CurrAdj7, @CurrAdjMT7, 
	@CurrMaxValue8, @CurrAdj8, @CurrAdjMT8,
	@s1, @s2, @s3, @s4, @s5, @s6, @s7, @s8
