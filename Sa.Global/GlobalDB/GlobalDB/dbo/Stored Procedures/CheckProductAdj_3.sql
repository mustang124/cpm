﻿
CREATE PROCEDURE [dbo].[CheckProductAdj];3 (@MinValue1 real = NULL,
	@MaxValue1 real = NULL OUTPUT, @Adj1 real = NULL OUTPUT, @AdjMT1 real = NULL OUTPUT,
	@MaxValue2 real = NULL OUTPUT, @Adj2 real = NULL OUTPUT, @AdjMT2 real = NULL OUTPUT,
	@MaxValue3 real = NULL OUTPUT, @Adj3 real = NULL OUTPUT, @AdjMT3 real = NULL OUTPUT,
	@MaxValue4 real = NULL OUTPUT, @Adj4 real = NULL OUTPUT, @AdjMT4 real = NULL OUTPUT,
	@MaxValue5 real = NULL OUTPUT, @Adj5 real = NULL OUTPUT, @AdjMT5 real = NULL OUTPUT,
	@MaxValue6 real = NULL OUTPUT, @Adj6 real = NULL OUTPUT, @AdjMT6 real = NULL OUTPUT,
	@MaxValue7 real = NULL OUTPUT, @Adj7 real = NULL OUTPUT, @AdjMT7 real = NULL OUTPUT,
	@MaxValue8 real = NULL OUTPUT, @Adj8 real = NULL OUTPUT, @AdjMT8 real = NULL OUTPUT,
	@s1 char(1) OUTPUT, @s2 char(1) OUTPUT, @s3 char(1) OUTPUT, @s4 char(1) OUTPUT, 
	@s5 char(1) OUTPUT, @s6 char(1) OUTPUT, @s7 char(1) OUTPUT, @s8 char(1) OUTPUT)
AS
IF @MaxValue1 IS NOT NULL AND @MinValue1 IS NULL 
	SELECT @MinValue1 = -1000
SELECT Action = @s1, PLevel = 1, MinValue = @MinValue1, MaxValue = @MaxValue1, Adj = @Adj1, AdjMT = @AdjMT1
WHERE @s1 <> 'N'
UNION
SELECT Action = @s2, PLevel = 2, MinValue = @MaxValue1, MaxValue = @MaxValue2, Adj = @Adj2, AdjMT = @AdjMT2
WHERE @s2 <> 'N'
UNION
SELECT Action = @s3, PLevel = 3, MinValue = @MaxValue2, MaxValue = @MaxValue3, Adj = @Adj3, AdjMT = @AdjMT3
WHERE @s3 <> 'N'
UNION
SELECT Action = @s4, PLevel = 4, MinValue = @MaxValue3, MaxValue = @MaxValue4, Adj = @Adj4, AdjMT = @AdjMT4
WHERE @s4 <> 'N'
UNION
SELECT Action = @s5, PLevel = 5, MinValue = @MaxValue4, MaxValue = @MaxValue5, Adj = @Adj5, AdjMT = @AdjMT5
WHERE @s5 <> 'N'
UNION
SELECT Action = @s6, PLevel = 6, MinValue = @MaxValue5, MaxValue = @MaxValue6, Adj = @Adj6, AdjMT = @AdjMT6
WHERE @s6 <> 'N'
UNION
SELECT Action = @s7, PLevel = 7, MinValue = @MaxValue6, MaxValue = @MaxValue7, Adj = @Adj7, AdjMT = @AdjMT7
WHERE @s7 <> 'N'
UNION
SELECT Action = @s8, PLevel = 8, MinValue = @MaxValue7, MaxValue = @MaxValue8, Adj = @Adj8, AdjMT = @AdjMT8
WHERE @s8 <> 'N'

