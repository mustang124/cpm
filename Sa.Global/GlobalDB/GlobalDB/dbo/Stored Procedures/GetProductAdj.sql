﻿CREATE PROCEDURE [dbo].[GetProductAdj](@StudyYear smallint, @Period char(2), 
	@PricingBasis char(4), @PriceLoc smallint, @ProductID char(5), 
	@Property char(5), @PropertyValue real, @Density real = NULL,
	@Adj real OUTPUT, @AdjMT real OUTPUT)
AS
SELECT @Adj = NULL, @AdjMT = NULL
SELECT @Adj = Adj, @AdjMT = AdjMT
FROM ProductAdj 
WHERE StudyYear = @StudyYear AND Period = @Period 
AND PricingBasis = @PricingBasis AND PriceLoc = @PriceLoc 
AND ProductID = @ProductID AND Property = @Property AND MinValue < @PropertyValue AND MaxValue >= @PropertyValue

IF @Density IS NOT NULL AND @AdjMT IS NOT NULL
	SELECT @Adj = @AdjMT*@Density/6289.7

