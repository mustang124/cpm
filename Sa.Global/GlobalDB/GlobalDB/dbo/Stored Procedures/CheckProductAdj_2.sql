﻿
CREATE PROCEDURE [dbo].[CheckProductAdj];2 (
	@StudyYear smallint, @Period char(2) = 'TY', @PricingBasis char(4) = 'BASE',
	@PriceLoc smallint, @ProductID char(5), @Property char(5),
	@MinValue1 real = -1000 OUTPUT,
	@MaxValue1 real = NULL OUTPUT, @Adj1 real = NULL OUTPUT, @AdjMT1 real = NULL OUTPUT,
	@MaxValue2 real = NULL OUTPUT, @Adj2 real = NULL OUTPUT, @AdjMT2 real = NULL OUTPUT,
	@MaxValue3 real = NULL OUTPUT, @Adj3 real = NULL OUTPUT, @AdjMT3 real = NULL OUTPUT,
	@MaxValue4 real = NULL OUTPUT, @Adj4 real = NULL OUTPUT, @AdjMT4 real = NULL OUTPUT,
	@MaxValue5 real = NULL OUTPUT, @Adj5 real = NULL OUTPUT, @AdjMT5 real = NULL OUTPUT,
	@MaxValue6 real = NULL OUTPUT, @Adj6 real = NULL OUTPUT, @AdjMT6 real = NULL OUTPUT,
	@MaxValue7 real = NULL OUTPUT, @Adj7 real = NULL OUTPUT, @AdjMT7 real = NULL OUTPUT,
	@MaxValue8 real = NULL OUTPUT, @Adj8 real = NULL OUTPUT, @AdjMT8 real = NULL OUTPUT,
	@s1 char(1) OUTPUT, @s2 char(1) OUTPUT, @s3 char(1) OUTPUT, @s4 char(1) OUTPUT, 
	@s5 char(1) OUTPUT, @s6 char(1) OUTPUT, @s7 char(1) OUTPUT, @s8 char(1) OUTPUT)
AS
DECLARE @CurrMinValue1 real,  @CurrMinValue real
DECLARE	@CurrMaxValue1 real,  @CurrAdj1 real, @CurrAdjMT1 real,
	@CurrMaxValue2 real,  @CurrAdj2 real, @CurrAdjMT2 real,
	@CurrMaxValue3 real,  @CurrAdj3 real, @CurrAdjMT3 real,
	@CurrMaxValue4 real,  @CurrAdj4 real, @CurrAdjMT4 real,
	@CurrMaxValue5 real,  @CurrAdj5 real, @CurrAdjMT5 real,
	@CurrMaxValue6 real,  @CurrAdj6 real, @CurrAdjMT6 real,
	@CurrMaxValue7 real,  @CurrAdj7 real, @CurrAdjMT7 real,
	@CurrMaxValue8 real,  @CurrAdj8 real, @CurrAdjMT8 real
SET NOCOUNT ON
IF @Adj1 IS NULL AND @AdjMT1 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT1, @Adj1 OUTPUT
IF @Adj2 IS NULL AND @AdjMT2 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT2, @Adj2 OUTPUT
IF @Adj3 IS NULL AND @AdjMT3 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT3, @Adj3 OUTPUT
IF @Adj4 IS NULL AND @AdjMT4 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT4, @Adj4 OUTPUT
IF @Adj5 IS NULL AND @AdjMT5 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT5, @Adj5 OUTPUT
IF @Adj6 IS NULL AND @AdjMT6 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT6, @Adj6 OUTPUT
IF @Adj7 IS NULL AND @AdjMT7 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT7, @Adj7 OUTPUT
IF @Adj8 IS NULL AND @AdjMT8 IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT8, @Adj8 OUTPUT
IF @MaxValue1 IS NOT NULL AND @MinValue1 IS NULL 
	SELECT @MinValue1 = -1000
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 1, 
	@MinValue1, @MaxValue1, @Adj1, @AdjMT1,
	@CurrMinValue1 OUTPUT, @CurrMaxValue1 OUTPUT, @CurrAdj1 OUTPUT, 
	@CurrAdjMT1 OUTPUT, @S1 OUTPUT
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 2, 
	@MaxValue1, @MaxValue2, @Adj2, @AdjMT2,
	@CurrMinValue OUTPUT, @CurrMaxValue2 OUTPUT, @CurrAdj2 OUTPUT, 
	@CurrAdjMT2 OUTPUT, @S2 OUTPUT
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 3, 
	@MaxValue2, @MaxValue3, @Adj3, @AdjMT3,
	@CurrMinValue OUTPUT, @CurrMaxValue3 OUTPUT, @CurrAdj3 OUTPUT, 
	@CurrAdjMT3 OUTPUT, @S3 OUTPUT
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 4, 
	@MaxValue3, @MaxValue4, @Adj4, @AdjMT4,
	@CurrMinValue OUTPUT, @CurrMaxValue4 OUTPUT, @CurrAdj4 OUTPUT, 
	@CurrAdjMT4 OUTPUT, @S4 OUTPUT
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 5, 
	@MaxValue4, @MaxValue5, @Adj5, @AdjMT5,
	@CurrMinValue OUTPUT, @CurrMaxValue5 OUTPUT, @CurrAdj5 OUTPUT, 
	@CurrAdjMT5 OUTPUT, @S5 OUTPUT
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 6, 
	@MaxValue5, @MaxValue6, @Adj6, @AdjMT6,
	@CurrMinValue OUTPUT, @CurrMaxValue6 OUTPUT, @CurrAdj6 OUTPUT, 
	@CurrAdjMT6 OUTPUT, @S6 OUTPUT
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 7, 
	@MaxValue6, @MaxValue7, @Adj7, @AdjMT7,
	@CurrMinValue OUTPUT, @CurrMaxValue7 OUTPUT, @CurrAdj7 OUTPUT, 
	@CurrAdjMT7 OUTPUT, @S7 OUTPUT
EXEC CheckProductAdj;4 @StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, 8, 
	@MaxValue7, @MaxValue8, @Adj8, @AdjMT8,
	@CurrMinValue OUTPUT, @CurrMaxValue8 OUTPUT, @CurrAdj8 OUTPUT, 
	@CurrAdjMT8 OUTPUT, @S8 OUTPUT
SELECT  @MinValue1 = @CurrMinValue1,
	@MaxValue1 = @CurrMaxValue1, @Adj1 = @CurrAdj1, @AdjMT1 = @CurrAdjMT1,
	@MaxValue2 = @CurrMaxValue2, @Adj2 = @CurrAdj2, @AdjMT2 = @CurrAdjMT2,
	@MaxValue3 = @CurrMaxValue3, @Adj3 = @CurrAdj3, @AdjMT3 = @CurrAdjMT3,
	@MaxValue4 = @CurrMaxValue4, @Adj4 = @CurrAdj4, @AdjMT4 = @CurrAdjMT4, 
	@MaxValue5 = @CurrMaxValue5, @Adj5 = @CurrAdj5, @AdjMT5 = @CurrAdjMT5, 
	@MaxValue6 = @CurrMaxValue6, @Adj6 = @CurrAdj6, @AdjMT6 = @CurrAdjMT6, 
	@MaxValue7 = @CurrMaxValue7, @Adj7 = @CurrAdj7, @AdjMT7 = @CurrAdjMT7,
	@MaxValue8 = @CurrMaxValue8, @Adj8 = @CurrAdj8, @AdjMT8 = @CurrAdjMT8

