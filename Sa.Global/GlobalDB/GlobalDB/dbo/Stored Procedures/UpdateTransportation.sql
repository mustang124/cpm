﻿/****** Object:  Stored Procedure dbo.UpdateTransportation    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE PROCEDURE UpdateTransportation;1 (
	@StudyYear smallint, @Period char(2), 
	@CrudeOrigin smallint, @CrudeDest smallint, 
	@Duty real = NULL, @Facilities real = NULL, @Gathering real = NULL,
	@Insurance real = NULL, @Losses real = NULL, @Lightering real = NULL, 
	@MiscPcnt real = NULL, @MiscAdd real = NULL, @MarketAdj real = NULL, 
	@OceanFreight real = NULL, @OceanFreightMT real = NULL, 
	@OPAInsurance real = NULL, @Port real = NULL, 
	@Pipeline real = NULL, @Superfund real = NULL)
AS
DECLARE	@CurrDuty real,  @CurrFacilities real, @CurrGathering real,
	@CurrInsurance real, @CurrLosses real, @CurrLightering real, 
	@CurrMiscPcnt real, @CurrMiscAdd real, @CurrMarketAdj real, 
	@CurrOceanFreight real, @CurrOceanFreightMT real, @CurrOPAInsurance real,
	@CurrPort real, @CurrPipeline real, @CurrSuperfund real
DECLARE @sDuty char(1), @sFacilities char(1), @sGathering char(1),
	@sInsurance char(1), @sLosses char(1), @sLightering char(1), 
	@sMiscPcnt char(1), @sMiscAdd char(1), @sMarketAdj char(1), 
	@sOceanFreight char(1), @sOPAInsurance char(1),
	@sPort char(1), @sPipeline char(1), @sSuperfund char(1)
SELECT	@CurrDuty = @Duty,  @CurrFacilities = @Facilities, 
	@CurrGathering = @Gathering, @CurrInsurance = @Insurance, 
	@CurrLosses = @Losses, @CurrLightering = @Lightering, 
	@CurrMiscPcnt = @MiscPcnt, @CurrMiscAdd = @MiscAdd, 
	@CurrMarketAdj = @MarketAdj, @CurrOceanFreight = @OceanFreight, 
	@CurrOceanFreightMT = @OceanFreightMT, 
	@CurrOPAInsurance = @OPAInsurance, @CurrPort = @Port, 
	@CurrPipeline = @Pipeline, @CurrSuperfund = @Superfund
EXEC CheckTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, 
	@CurrDuty OUTPUT, @CurrFacilities OUTPUT, @CurrGathering OUTPUT,
	@CurrInsurance OUTPUT, @CurrLosses OUTPUT, @CurrLightering OUTPUT, 
	@CurrMiscPcnt OUTPUT, @CurrMiscAdd OUTPUT, @CurrMarketAdj OUTPUT, 
	@CurrOceanFreight OUTPUT, @CurrOceanFreightMT OUTPUT, 
	@CurrOPAInsurance OUTPUT,
	@CurrPort OUTPUT, @CurrPipeline OUTPUT, @CurrSuperfund OUTPUT,
	@sDuty OUTPUT, @sFacilities OUTPUT, @sGathering OUTPUT,
	@sInsurance OUTPUT, @sLosses OUTPUT, @sLightering OUTPUT, 
	@sMiscPcnt OUTPUT, @sMiscAdd OUTPUT, @sMarketAdj OUTPUT, 
	@sOceanFreight OUTPUT, @sOPAInsurance OUTPUT,
	@sPort OUTPUT, @sPipeline OUTPUT, @sSuperfund OUTPUT
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sDuty, 'D', @Duty
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sFacilities, 'FF', @Facilities
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sGathering, 'GA', @Gathering
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sInsurance, 'IN', @Insurance
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sLosses, 'L', @Losses
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sLightering, 'LI', @Lightering
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sMiscPcnt, 'M%', @MiscPcnt
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sMiscAdd, 'M+', @MiscAdd
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sMarketAdj, 'MA', @MarketAdj
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sOceanFreight, 'OF', @OceanFreight, @OceanFreightMT
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sOPAInsurance, 'OI', @OPAInsurance
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sPort, 'PF', @Port
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sPipeline, 'PT', @Pipeline
EXEC UpdateTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, @sSuperfund, 'SF', @Superfund
EXEC CheckTransportation;3 @CurrDuty, @CurrFacilities, @CurrGathering,
	@CurrInsurance, @CurrLosses, @CurrLightering, @CurrMiscPcnt, 
	@CurrMiscAdd, @CurrMarketAdj, @CurrOceanFreight, @CurrOceanFreightMT, 
	@CurrOPAInsurance, @CurrPort, @CurrPipeline, @CurrSuperfund,
	@sDuty, @sFacilities, @sGathering, @sInsurance, @sLosses, @sLightering, 
	@sMiscPcnt, @sMiscAdd, @sMarketAdj, @sOceanFreight, 
	@sOPAInsurance, @sPort, @sPipeline, @sSuperfund
