﻿CREATE PROCEDURE GetGasolinePrice(@Study char(3), @StudyYear smallint, @Period char(2), 
	@PricingBasis char(4), @PriceLoc smallint, 
	@Grade char(5), @Leadgpl real, @Type char(5), @RVP real, @Sulfur real, @Density real = NULL,
	@BasePrice real OUTPUT, @BasePriceMT real OUTPUT,
	@LeadAdj real OUTPUT, @LeadAdjMT real OUTPUT,
	@TypeAdj real OUTPUT, @TypeAdjMT real OUTPUT,
	@RVPAdj real OUTPUT, @RVPAdjMT real OUTPUT,
	@SulfurAdj real OUTPUT, @SulfurAdjMT real OUTPUT,
	@PricePerBbl real OUTPUT, @PricePerMT real OUTPUT)
AS
DECLARE @ProductID char(5)
SELECT @ProductID = PricingID FROM Material_LU WHERE MaterialID = @Grade

IF @Study IN ('EUR', 'PAC', 'FL')
BEGIN
        IF @StudyYear < 2004
	BEGIN
		-- Apply the price for low lead gasolines
		IF @Leadgpl <= 0.15 AND @Grade IN ('LR','LP')
			SELECT @ProductID = CASE @Grade WHEN 'LR' THEN 'P204' ELSE 'P205' END
	END
	IF @Leadgpl IS NOT NULL
		EXEC dbo.GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'LEAD', @Leadgpl, @Density, @LeadAdj OUTPUT, @LeadAdjMT OUTPUT
END
ELSE
	SELECT @LeadAdj = NULL, @LeadAdjMT = NULL

EXEC dbo.GetBaseProductPrice @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Density, @BasePrice OUTPUT, @BasePriceMT OUTPUT
EXEC dbo.GetProductTypeAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Type, @Density, @TypeAdj OUTPUT, @TypeAdjMT OUTPUT
IF @RVP IS NOT NULL AND @Grade <> 'AVGAS'
BEGIN
	IF (@StudyYear < 2002 AND @Type = 'CON') 
	OR (@StudyYear = 2002 AND @Type IN ('CON', 'CONBB', 'OTHER', 'OXY', 'RBB', 'RFG', 'RFO'))
	OR (@StudyYear > 2002 AND @Type IN ('CON', 'CONBB'))
		EXEC dbo.GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'RVP', @RVP, @Density, @RVPAdj OUTPUT, @RVPAdjMT OUTPUT
END
EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'SULF', @Sulfur, @Density, @SulfurAdj OUTPUT, @SulfurAdjMT OUTPUT

SELECT @PricePerBbl = @BasePrice + ISNULL(@LeadAdj, 0) + ISNULL(@TypeAdj, 0) + ISNULL(@RVPAdj, 0) + ISNULL(@SulfurAdj, 0),
	@PricePerMT = @BasePriceMT + ISNULL(@LeadAdjMT, 0) + ISNULL(@TypeAdjMT, 0) + ISNULL(@RVPAdjMT, 0) + ISNULL(@SulfurAdjMT, 0)



