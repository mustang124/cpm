﻿/****** Object:  Stored Procedure dbo.ConvProductMTPrice    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE PROCEDURE [dbo].[ConvProductMTPrice] (
		@ProductID char(5), @PriceMT real, @PriceBbl real OUTPUT)
AS
DECLARE @TypicalDensity real
SELECT @TypicalDensity = TypicalDensity
FROM Material_LU WHERE MaterialID = @ProductID
IF @TypicalDensity IS NULL
	SELECT @TypicalDensity = TypicalDensity
	FROM Product_LU WHERE ProductID = @ProductID	
SELECT @PriceBbl = @PriceMT * @TypicalDensity/6289.7