﻿CREATE PROCEDURE [dbo].[CheckProductPrice];2 (
	@StudyYear smallint, @Period varchar(2), 
	@PricingBasis char(4) = 'BASE', @PriceLoc smallint, @ProductID char(5), 
	@BasePrice real = NULL OUTPUT, @BasePriceMT real = NULL OUTPUT, @NewPrice tinyint OUTPUT)
AS
DECLARE @CurrBasePrice real, @CurrBasePriceMT real, @TypicalDensity real
SET NOCOUNT ON
IF @BasePrice IS NULL AND @BasePriceMT IS NOT NULL
BEGIN
	IF @BasePriceMT = 0
		SET @BasePrice = 0
	ELSE
		EXEC ConvProductMTPrice @ProductID, @BasePriceMT, @BasePrice OUTPUT
END
SELECT @CurrBasePrice = BasePrice, @CurrBasePriceMT = BasePriceMT
FROM ProductPrice
WHERE StudyYear = @StudyYear
AND PricingBasis = @PricingBasis
AND PriceLoc = @PriceLoc
AND Period = @Period
AND ProductID = @ProductID
IF @@ROWCOUNT = 0 AND @BasePrice IS NOT NULL
BEGIN
	SELECT @NewPrice = 1, @BasePrice = NULL, @BasePriceMT = NULL
END
ELSE
BEGIN
	IF ABS(@BasePrice - @CurrBasePrice) < 0.01
		SELECT @CurrBasePrice = NULL
	IF ABS(@BasePriceMT - @CurrBasePriceMT) < 0.01
		SELECT @CurrBasePriceMT = NULL
	SELECT @NewPrice = 0, @BasePrice = @CurrBasePrice, 
		@BasePriceMT = @CurrBasePriceMT
END
