﻿

/****** Object:  Stored Procedure dbo.CheckProductAdj    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE   PROCEDURE [dbo].[CheckProductAdj] (
	@StudyYear smallint, @Period char(2) = 'TY', @PricingBasis char(4) = 'BASE',
	@PriceLoc smallint, @ProductID char(5), @Property char(5), 
	@MinValue1 real = NULL,
	@MaxValue1 real = NULL, @Adj1 real = NULL, @AdjMT1 real = NULL,
	@MaxValue2 real = NULL, @Adj2 real = NULL, @AdjMT2 real = NULL,
	@MaxValue3 real = NULL, @Adj3 real = NULL, @AdjMT3 real = NULL,
	@MaxValue4 real = NULL, @Adj4 real = NULL, @AdjMT4 real = NULL,
	@MaxValue5 real = NULL, @Adj5 real = NULL, @AdjMT5 real = NULL,
	@MaxValue6 real = NULL, @Adj6 real = NULL, @AdjMT6 real = NULL,
	@MaxValue7 real = NULL, @Adj7 real = NULL, @AdjMT7 real = NULL,
	@MaxValue8 real = NULL, @Adj8 real = NULL, @AdjMT8 real = NULL)
AS
DECLARE @s1 char(1), @s2 char(1), @s3 char(1), @s4 char(1), 
	@s5 char(1), @s6 char(1), @s7 char(1), @s8 char(1)
IF @MaxValue1 IS NOT NULL AND @MinValue1 IS NULL 
	SELECT @MinValue1 = -1000
EXEC CheckProductAdj;2 	@StudyYear, @Period, @PricingBasis,
	@PriceLoc, @ProductID, @Property, @MinValue1 OUTPUT,
	@MaxValue1 OUTPUT, @Adj1 OUTPUT, @AdjMT1 OUTPUT,
	@MaxValue2 OUTPUT, @Adj2 OUTPUT, @AdjMT2 OUTPUT,
	@MaxValue3 OUTPUT, @Adj3 OUTPUT, @AdjMT3 OUTPUT,
	@MaxValue4 OUTPUT, @Adj4 OUTPUT, @AdjMT4 OUTPUT,
	@MaxValue5 OUTPUT, @Adj5 OUTPUT, @AdjMT5 OUTPUT,
	@MaxValue6 OUTPUT, @Adj6 OUTPUT, @AdjMT6 OUTPUT,
	@MaxValue7 OUTPUT, @Adj7 OUTPUT, @AdjMT7 OUTPUT,
	@MaxValue8 OUTPUT, @Adj8 OUTPUT, @AdjMT8 OUTPUT,
	@s1 OUTPUT, @s2 OUTPUT, @s3 OUTPUT, @s4 OUTPUT, 
	@s5 OUTPUT, @s6 OUTPUT, @s7 OUTPUT, @s8 OUTPUT
EXEC CheckProductAdj;3 	@MinValue1, 
	@MaxValue1, @Adj1, @AdjMT1, @MaxValue2, @Adj2, @AdjMT2,
	@MaxValue3, @Adj3, @AdjMT3, @MaxValue4, @Adj4, @AdjMT4,
	@MaxValue5, @Adj5, @AdjMT5, @MaxValue6, @Adj6, @AdjMT6,
	@MaxValue7, @Adj7, @AdjMT7, @MaxValue8, @Adj8, @AdjMT8,
	@s1, @s2, @s3, @s4, @s5, @s6, @s7, @s8


