﻿
CREATE PROCEDURE UpdateProductAdj;2 (@StudyYear smallint, @Period char(2) = 'TY', 
	@PricingBasis char(4), @PriceLoc smallint, @ProductID char(5), 
	@Property char(5), @PLevel tinyint,
	@Action char(1), @MinValue real, @MaxValue real, @Adj real, @AdjMT real = NULL)
AS
IF @Action <> 'D' AND @Adj IS NULL AND @AdjMT IS NOT NULL
	EXEC ConvProductMTPrice @ProductID, @AdjMT, @Adj OUTPUT
IF @Action = 'I'
	INSERT INTO ProductAdj (StudyYear, Period, PricingBasis, PriceLoc, 
	ProductID, Property, PLevel, MinValue, MaxValue, Adj, AdjMT, SaveDate)
	SELECT @StudyYear, @Period, @PricingBasis, @PriceLoc,
	@ProductID, @Property, @PLevel, @MinValue, @MaxValue, @Adj, @AdjMT, GetDate()
ELSE IF @Action = 'U'
	UPDATE ProductAdj 
	SET MinValue = @MinValue, MaxValue = @MaxValue,
	Adj = @Adj, AdjMT = @AdjMT
	WHERE StudyYear = @StudyYear AND Period = @Period 
	AND PricingBasis = @PricingBasis AND PriceLoc = @PriceLoc
	AND ProductId = @ProductID AND Property = @Property AND PLevel = @PLevel
ELSE IF @Action = 'D'
	DELETE FROM ProductAdj 
	WHERE StudyYear = @StudyYear AND Period = @Period 
	AND PricingBasis = @PricingBasis AND PriceLoc = @PriceLoc
	AND ProductId = @ProductID AND Property = @Property AND PLevel = @PLevel
