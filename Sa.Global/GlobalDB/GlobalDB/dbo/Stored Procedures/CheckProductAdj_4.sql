﻿
CREATE PROCEDURE [dbo].[CheckProductAdj];4 (
	@StudyYear smallint, @Period char(2) = 'TY', @PricingBasis char(4) = 'BASE',
	@PriceLoc smallint, @ProductID char(5), @Property char(5), @PLevel tinyint,
	@NewMinValue real = NULL, @NewMaxValue real = NULL, 
	@NewAdj real = NULL, @NewAdjMT real = NULL,
	@MinValue real = NULL OUTPUT, @MaxValue real = NULL OUTPUT, 
	@Adj real = NULL OUTPUT, @AdjMT real = NULL OUTPUT, @Status char(1) OUTPUT)
AS
IF @NewMaxValue IS NULL AND @NewMinValue IS NOT NULL
	SELECT @NewMaxValue = 1000000000000
SELECT @MinValue = MinValue, @MaxValue = MaxValue, @Adj = Adj, @AdjMT = AdjMT
FROM ProductAdj
WHERE StudyYear = @StudyYear AND Period = @Period AND PricingBasis = @PricingBasis
AND PriceLoc = @PriceLoc AND ProductID = @ProductID 
AND Property = @Property AND PLevel = @PLevel
IF @Adj IS NULL AND @NewAdj IS NULL AND @AdjMT IS NULL AND @NewAdjMT IS NULL
	SELECT @Status = 'N'
ELSE IF @Adj IS NULL AND @NewADJ IS NOT NULL
	SELECT @Status = 'I'
ELSE IF @Adj IS NOT NULL AND @NewAdj IS NULL
	SELECT @Status = 'D'
ELSE IF (@AdjMT IS NULL AND @NewAdjMT IS NOT NULL) OR (@AdjMT IS NOT NULL AND @NewAdjMT IS NULL)
	SELECT @Status = 'U'
ELSE IF ABS(@Adj - @NewAdj) > 0.01
	SELECT @Status = 'U'
ELSE IF ABS(@AdjMT - @NewAdjMT) > 0.01
	SELECT @Status = 'U'
ELSE IF ABS(@MaxValue - @NewMaxValue) > 0.01
	SELECT @Status = 'U'
ELSE IF ABS(@MinValue - @NewMinValue) > 0.01
	SELECT @Status = 'U'
ELSE
	SELECT @Status = 'N'

