﻿
CREATE PROCEDURE CheckProductAdjLubes;3 (@BaseProductID char(5) = NULL,
	@AnilineSlope real = NULL OUTPUT, @AnilineSpec real = NULL OUTPUT,  @AnilineDefault real = NULL OUTPUT, 
	@N2PPMSlope real = NULL OUTPUT, @N2PPMSpec real = NULL OUTPUT, @N2PPMDefault real = NULL OUTPUT, 
	@VABPSlope real = NULL OUTPUT, @VABPSpec real = NULL OUTPUT, @VABPDefault real = NULL OUTPUT, 
	@SulfurSlope real = NULL OUTPUT, @SulfurSpec real = NULL OUTPUT, @SulfurDefault real = NULL OUTPUT,
	@FuelOilPcnt real = NULL, @FuelOilID char(5) = NULL,
	@sAnilineSlope char(1) OUTPUT, @sAnilineSpec char(1) OUTPUT, @sAnilineDefault char(1) OUTPUT,
	@sN2PPMSlope char(1) OUTPUT, @sN2PPMSpec char(1) OUTPUT, @sN2PPMDefault char(1) OUTPUT,
	@sVABPSlope char(1) OUTPUT, @sVABPSpec char(1) OUTPUT, @sVABPDefault char(1) OUTPUT,
	@sSulfurSlope char(1) OUTPUT, @sSulfurSpec char(1) OUTPUT, @sSulfurDefault char(1) OUTPUT,
	@sFuelOilPcnt char(1), @sFuelOilID char(1), @sBaseProductID char(1), @NewAdj tinyint)
AS
	SELECT 	BaseProductID = CASE WHEN @sBaseProductID = 'N' THEN NULL ELSE ISNULL(@BaseProductID, '') END
		, AnilineSlope = CASE WHEN @sAnilineSlope = 'N' THEN NULL ELSE ISNULL(@AnilineSlope, 0) END
		, AnilineSpec = CASE WHEN @sAnilineSpec = 'N' THEN NULL ELSE ISNULL(@AnilineSpec, 0) END
		, AnilineDefault = CASE WHEN @sAnilineDefault = 'N' THEN NULL ELSE ISNULL(@AnilineDefault, 0) END
		, N2PPMSlope = CASE WHEN @sN2PPMSlope = 'N' THEN NULL ELSE ISNULL(@N2PPMSlope, 0) END
		, N2PPMSpec = CASE WHEN @sN2PPMSpec = 'N' THEN NULL ELSE ISNULL(@N2PPMSpec, 0) END
		, N2PPMDefault = CASE WHEN @sN2PPMDefault = 'N' THEN NULL ELSE ISNULL(@N2PPMDefault, 0) END
		, VABPSlope = CASE WHEN @sVABPSlope = 'N' THEN NULL ELSE ISNULL(@VABPSlope, 0) END
		, VABPSpec = CASE WHEN @sVABPSpec = 'N' THEN NULL ELSE ISNULL(@VABPSpec, 0) END
		, VABPDefault = CASE WHEN @sVABPDefault = 'N' THEN NULL ELSE ISNULL(@VABPDefault, 0) END
		, SulfurSlope = CASE WHEN @sSulfurSlope = 'N' THEN NULL ELSE ISNULL(@SulfurSlope, 0) END
		, SulfurSpec = CASE WHEN @sSulfurSpec = 'N' THEN NULL ELSE ISNULL(@SulfurSpec, 0) END
		, SulfurDefault = CASE WHEN @sSulfurDefault = 'N' THEN NULL ELSE ISNULL(@SulfurDefault, 0) END
		, FuelOilPcnt = CASE WHEN @sFuelOilPcnt = 'N' THEN NULL ELSE ISNULL(@FuelOilPcnt, 0) END
		, FuelOilID = CASE WHEN @sFuelOilID = 'N' THEN NULL ELSE ISNULL(@FuelOilID, '') END
		, NewAdj = @NewAdj




