﻿
CREATE PROCEDURE CheckCrudePrice;3 (
	@BasePrice real, @APIScale smallint, @PostedAPI real, 
	@CrudeOrigin smallint, @NewPrice tinyint, @PostedSulfur real, @SulfurAdjSlope real)
AS 
SELECT NewPrice = @NewPrice, BasePrice = @BasePrice, 
	APIScale = @APIScale, PostedAPI = @PostedAPI, CrudeOrigin = @CrudeOrigin
	, PostedSulfur = @PostedSulfur, SulfurAdjSlope = @SulfurAdjSlope

