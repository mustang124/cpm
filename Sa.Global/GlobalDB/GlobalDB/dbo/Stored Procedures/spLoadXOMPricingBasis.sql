﻿CREATE PROC spLoadXOMPricingBasis(@StudyYear smallint, @BaseStudy char(3))
AS

DECLARE @BasisMap TABLE (SourceBasis varchar(4) NOT NULL, NewBasis varchar(4) NOT NULL)
INSERT @BasisMap VALUES ('BASE','XOMB'),('SPOT','XOMS')

DELETE FROM ProductPrice 
WHERE PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)
AND StudyYear = @StudyYear AND Period = 'TY' AND PricingBasis IN (SELECT NewBasis FROM @BasisMap)

DELETE FROM ProductAdj
WHERE PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)
AND StudyYear = @StudyYear AND Period = 'TY' AND PricingBasis IN (SELECT NewBasis FROM @BasisMap)

INSERT ProductPrice (StudyYear, Period, PricingBasis, PriceLoc, ProductID, BasePrice, BasePriceMT, SaveDate)
SELECT pp.StudyYear, pp.Period, m.NewBasis, pp.PriceLoc, pp.ProductID, pp.BasePrice, pp.BasePriceMT, pp.SaveDate
FROM ProductPrice pp INNER JOIN @BasisMap m ON m.SourceBasis = pp.PricingBasis
WHERE PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)
AND StudyYear = @StudyYear AND Period = 'TY'

INSERT ProductAdj (StudyYear, Period, PricingBasis, PriceLoc, ProductID, Property, PLevel, MinValue, MaxValue, Adj, AdjMT, SaveDate)
SELECT pa.StudyYear, pa.Period, m.NewBasis, pa.PriceLoc, pa.ProductID, pa.Property, pa.PLevel, pa.MinValue, pa.MaxValue, pa.Adj, pa.AdjMT, pa.SaveDate
FROM ProductAdj pa INNER JOIN @BasisMap m ON m.SourceBasis = pa.PricingBasis
WHERE pa.PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)
AND StudyYear = @StudyYear AND Period = 'TY'

/* Set solvent prices to octane values */
UPDATE ProductPrice	
SET 	BasePrice = p2.BasePrice, BasePriceMT = p2.BasePriceMT, SaveDate = GETDATE()
FROM 	ProductPrice INNER JOIN ProductPrice p2	ON ProductPrice.StudyYear = p2.StudyYear AND ProductPrice.Period = p2.Period AND ProductPrice.PriceLoc = p2.PriceLoc
INNER JOIN @BasisMap m ON m.NewBasis = ProductPrice.PricingBasis AND m.SourceBasis = p2.PricingBasis
INNER JOIN (VALUES ('M141','M33'), ('M142','M28'), ('M143','M33'), ('M146','M28'), ('M140','M62'), ('M231','M104')
			) MA(ProductID1, ProductID2) ON MA.ProductID1 = ProductPrice.ProductID AND MA.ProductID2 = p2.ProductID
WHERE ProductPrice.PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)

/* Set aromatics prices to gasoline blending values */
DECLARE @GasoMNum varchar(4)
IF @BaseStudy = 'NSA'
	SET @GasoMNum = 'M86'
ELSE
	SET @GasoMNum = 'M88'
	
UPDATE ProductPrice		
SET 	BasePrice = p2.BasePrice*b.MultBBL, BasePriceMT = p2.BasePriceMT*b.MultMT, SaveDate = GETDATE()
FROM 	ProductPrice 
INNER JOIN ProductPrice p2 ON ProductPrice.StudyYear = p2.StudyYear AND ProductPrice.Period = p2.Period AND ProductPrice.PriceLoc = p2.Priceloc AND p2.ProductID = @GasoMNum
INNER JOIN @BasisMap m ON m.NewBasis = ProductPrice.PricingBasis AND m.SourceBasis = p2.PricingBasis
INNER JOIN (VALUES ('M6','BNZ'), ('M124','XYL'), ('M139','XYL'), ('M64','XYL'), ('M125','XYL'), ('M22','C9+'), ('M2','C9+')) MB(ProductID, BlendProd) ON MB.ProductID = ProductPrice.ProductID
INNER JOIN BlendingValues b ON b.PriceLoc=p2.PriceLoc AND b.StudyYear=p2.StudyYear AND b.Product = MB.BlendProd
WHERE ProductPrice.PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)
AND ((b.MultBBL IS NOT NULL AND p2.BasePrice IS NOT NULL) OR (b.MultMT IS NOT NULL AND p2.BasePriceMT IS NOT NULL))
AND ProductPrice.StudyYear = @StudyYear

DECLARE @PricingFormulas TABLE (updProductID varchar(4) NOT NULL, ComponentID varchar(4) NOT NULL, Multiplier real NOT NULL)
INSERT @PricingFormulas VALUES ('M147', 'M46', 0.865), ('M147', 'M6', 0.646), ('M147', 'M45', -0.26), ('M147', 'M22', -0.025)
INSERT @PricingFormulas VALUES ('M148', 'M68', 0.272), ('M148', 'M6', 0.729)
INSERT @PricingFormulas VALUES ('M149', 'M26', 0.168), ('M149', 'M6', 0.822)

UPDATE 	ProductPrice
SET BasePrice = (SELECT SUM(p2.BasePrice*f.Multiplier)
	FROM ProductPrice p2 INNER JOIN @PricingFormulas f ON f.ComponentID = p2.ProductID
	WHERE p2.StudyYear = ProductPrice.StudyYear AND p2.Period = ProductPrice.Period AND p2.PriceLoc = ProductPrice.PriceLoc AND p2.PricingBasis = ProductPrice.PricingBasis
	AND f.updProductID = ProductPrice.ProductID) , SaveDate = GETDATE()
WHERE ProductPrice.StudyYear = @StudyYear AND ProductPrice.PricingBasis IN (SELECT NewBasis FROM @BasisMap)
AND	ProductPrice.ProductID IN (SELECT updProductID FROM @PricingFormulas)
AND ProductPrice.PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)

/* Calculate MT price for aromatics calculated above */
UPDATE 	ProductPrice
SET BasePriceMT = BasePrice / m.TypicalDensity * 1000 / 0.159, SaveDate = GETDATE()
FROM ProductPrice INNER JOIN Material_LU m ON m.MaterialID = ProductPrice.ProductID
WHERE ProductPrice.StudyYear = @StudyYear AND ProductPrice.PricingBasis IN (SELECT NewBasis FROM @BasisMap)
AND ProductPrice.ProductID in ('M6','M124','M139','M64','M125','M22','M147','M148','M149') AND BasePriceMT IS NOT NULL
AND ProductPrice.PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)

/* 
	Set prices for M243, M144, and M2 to previously calculated prices.
	Set Mixed Aromatics (M2) to the C9+ (M22) price (less than B, T or X as it takes OPEX to separate) 
*/
UPDATE ProductPrice	
SET 	BasePrice = p2.BasePrice, BasePriceMT = p2.BasePriceMT, SaveDate = GETDATE()
FROM 	ProductPrice 
INNER JOIN ProductPrice p2	ON ProductPrice.StudyYear = p2.StudyYear AND ProductPrice.Period = p2.Period AND ProductPrice.PricingBasis = p2.PricingBasis AND ProductPrice.PriceLoc = p2.PriceLoc
INNER JOIN (VALUES ('M243','M124'), ('M144','M64'), ('M2','M22')
			) MA(ProductID1, ProductID2) ON MA.ProductID1 = ProductPrice.ProductID AND MA.ProductID2 = p2.ProductID
WHERE ProductPrice.PriceLoc IN (SELECT PriceLoc FROM PriceLoc_LU WHERE Study = @BaseStudy)
AND ProductPrice.PricingBasis IN (SELECT NewBasis FROM @BasisMap)

