﻿/****** Object:  Stored Procedure dbo.CheckTransportation    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE PROCEDURE CheckTransportation;1 (
	@StudyYear smallint, @Period char(2), 
	@CrudeOrigin smallint, @CrudeDest smallint, 
	@Duty real = NULL, @Facilities real = NULL, @Gathering real = NULL,
	@Insurance real = NULL, @Losses real = NULL, @Lightering real = NULL, 
	@MiscPcnt real = NULL, @MiscAdd real = NULL, @MarketAdj real = NULL, 
	@OceanFreight real = NULL, @OceanFreightMT real = NULL, 
	@OPAInsurance real = NULL, @Port real = NULL, 
	@Pipeline real = NULL, @Superfund real = NULL)
AS
DECLARE @sDuty char(1), @sFacilities char(1), @sGathering char(1),
	@sInsurance char(1), @sLosses char(1), @sLightering char(1), 
	@sMiscPcnt char(1), @sMiscAdd char(1), @sMarketAdj char(1), 
	@sOceanFreight char(1), @sOPAInsurance char(1),
	@sPort char(1), @sPipeline char(1), @sSuperfund char(1)
EXEC CheckTransportation;2 @StudyYear, @Period, @CrudeOrigin, @CrudeDest, 
	@Duty OUTPUT, @Facilities OUTPUT, @Gathering OUTPUT,
	@Insurance OUTPUT, @Losses OUTPUT, @Lightering OUTPUT, 
	@MiscPcnt OUTPUT, @MiscAdd OUTPUT, @MarketAdj OUTPUT, 
	@OceanFreight OUTPUT, @OceanFreightMT OUTPUT, @OPAInsurance OUTPUT,
	@Port OUTPUT, @Pipeline OUTPUT, @Superfund OUTPUT,
	@sDuty OUTPUT, @sFacilities OUTPUT, @sGathering OUTPUT,
	@sInsurance OUTPUT, @sLosses OUTPUT, @sLightering OUTPUT, 
	@sMiscPcnt OUTPUT, @sMiscAdd OUTPUT, @sMarketAdj OUTPUT, 
	@sOceanFreight OUTPUT, @sOPAInsurance OUTPUT,
	@sPort OUTPUT, @sPipeline OUTPUT, @sSuperfund OUTPUT
EXEC CheckTransportation;3 @Duty, @Facilities, @Gathering,
	@Insurance, @Losses, @Lightering, @MiscPcnt, @MiscAdd, @MarketAdj, 
	@OceanFreight, @OceanFreightMT, @OPAInsurance, @Port, @Pipeline, @Superfund,
	@sDuty, @sFacilities, @sGathering, @sInsurance, @sLosses, @sLightering, 
	@sMiscPcnt, @sMiscAdd, @sMarketAdj, @sOceanFreight,
	@sOPAInsurance, @sPort, @sPipeline, @sSuperfund