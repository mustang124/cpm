﻿
CREATE PROCEDURE TemperatureConv (@TempF real OUTPUT, @TempC real OUTPUT) 
AS
IF @TempC IS NULL
	SELECT @TempC = (@TempF-32)/1.8
ELSE
	SELECT @TempF = 1.8*@TempC + 32

