﻿/****** Object:  Stored Procedure dbo.CheckProductPrice    Script Date: 11/09/2001 2:28:50 PM ******/
CREATE PROCEDURE [dbo].[CheckProductPrice] (
	@StudyYear smallint, @Period varchar(2) = 'TY', 
	@PricingBasis char(4) = 'BASE', @PriceLoc smallint,
	@ProductID char(5), @BasePrice real = NULL, @BasePriceMT real = NULL)
AS
DECLARE @NewPrice tinyint
EXEC CheckProductPrice;2 @StudyYear, @Period, @PricingBasis, 
	@PriceLoc, @ProductID, @BasePrice OUTPUT, @BasePriceMT OUTPUT, @NewPrice OUTPUT
EXEC CheckProductPrice;3 @BasePrice, @BasePriceMT, @NewPrice
