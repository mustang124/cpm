﻿CREATE PROCEDURE [dbo].[GetProductTypeAdj](@StudyYear smallint, @Period char(2), 
	@PricingBasis char(4), @PriceLoc smallint, @ProductID char(5), 
	@ProductType char(5), @Density real = NULL,
	@Adj real OUTPUT, @AdjMT real OUTPUT)
AS
SELECT @Adj = NULL, @AdjMT = NULL
SELECT @Adj = Adj, @AdjMT = AdjMT
FROM ProductAdj 
WHERE StudyYear = @StudyYear AND Period = @Period 
AND PricingBasis = @PricingBasis AND PriceLoc = @PriceLoc 
AND ProductID = @ProductID AND Property = @ProductType

IF @Density IS NOT NULL AND @AdjMT IS NOT NULL
	SELECT @Adj = @AdjMT*@Density/6289.7

