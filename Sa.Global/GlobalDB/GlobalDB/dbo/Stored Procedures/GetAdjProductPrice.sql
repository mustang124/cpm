﻿






CREATE        PROCEDURE GetAdjProductPrice(@Study char(3), @StudyYear smallint, @Period char(2), 
	@PricingBasis char(4), @PriceLoc smallint, 
	@Grade char(5), @Type char(5) = NULL, 
	@Leadgpl real = NULL, @RVP real = NULL, @Sulfur real = NULL, @Viscosity real = NULL, 
	@FreezePt real = NULL, @Cetane real = NULL, @CloudPt real = NULL, @ViscIndex real = NULL, @Density real = NULL,
	@Aniline real = NULL, @N2PPM real = NULL, @VABP real = NULL,
	@BasePrice real = NULL OUTPUT, @BasePriceMT real = NULL OUTPUT,
	@LeadAdj real = NULL OUTPUT, @LeadAdjMT real = NULL OUTPUT,
	@TypeAdj real = NULL OUTPUT, @TypeAdjMT real =NULL OUTPUT,
	@RVPAdj real = NULL OUTPUT, @RVPAdjMT real =NULL OUTPUT,
	@SulfurAdj real = NULL OUTPUT, @SulfurAdjMT real =NULL OUTPUT,
	@ViscosityAdj real = NULL OUTPUT, @ViscosityAdjMT real =NULL OUTPUT,
	@FreezePtAdj real = NULL OUTPUT, @FreezePtAdjMT real =NULL OUTPUT, 
	@CetaneAdj real = NULL OUTPUT, @CetaneAdjMT real =NULL OUTPUT, 
	@CloudPtAdj real = NULL OUTPUT, @CloudPtAdjMT real =NULL OUTPUT, 
	@ViscIndexAdj real = NULL OUTPUT, @ViscIndexAdjMT real =NULL OUTPUT,
	@AnilineAdj real = NULL OUTPUT, @AnilineAdjMT real =NULL OUTPUT,
	@N2Adj real = NULL OUTPUT, @N2AdjMT real =NULL OUTPUT,
	@VABPAdj real = NULL OUTPUT, @VABPAdjMT real =NULL OUTPUT,
	@PricePerBbl real = NULL OUTPUT, @PricePerMT real =NULL OUTPUT)
AS
DECLARE @ProductID char(5), @IsScanDiesel bit, @IsCARBDiesel bit, @TypeAdjInclSulfur bit
DECLARE @AnilineSpec real, @N2PPMSpec real, @VABPSpec real, @SulfurSpec real
SELECT	@BasePrice = NULL, @BasePriceMT = NULL,
	@LeadAdj = NULL, @LeadAdjMT = NULL,
	@TypeAdj = NULL, @TypeAdjMT = NULL,
	@RVPAdj = NULL, @RVPAdjMT = NULL,
	@SulfurAdj = NULL, @SulfurAdjMT = NULL,
	@ViscosityAdj = NULL, @ViscosityAdjMT = NULL,
	@FreezePtAdj = NULL, @FreezePtAdjMT = NULL, 
	@CetaneAdj = NULL, @CetaneAdjMT = NULL, 
	@CloudPtAdj = NULL, @CloudPtAdjMT = NULL, 
	@ViscIndexAdj = NULL, @ViscIndexAdjMT = NULL,
	@AnilineAdj = NULL, @AnilineAdjMT = NULL,
	@N2Adj = NULL, @N2AdjMT = NULL,
	@VABPAdj = NULL, @VABPAdjMT = NULL,
	@PricePerBbl = NULL, @PricePerMT = NULL
IF @Grade LIKE 'I[1-9]%'
BEGIN
	SELECT @Grade = STUFF(@Grade, 1, 1, 'L')
	IF NOT EXISTS (SELECT * FROM Material_LU LU WHERE MaterialID = @Grade)
	BEGIN
		SELECT @Grade = STUFF(@Grade, 1, 1, 'M')
		IF NOT EXISTS (SELECT * FROM Material_LU LU WHERE MaterialID = @Grade)
			SELECT @Grade = STUFF(@Grade, 1, 1, 'I')
	END
END
IF EXISTS (SELECT * FROM ProductAdjLubes WHERE StudyYear = @StudyYear AND PriceLoc = @PriceLoc AND ProductID = @Grade)
BEGIN
	DECLARE @FuelOilID varchar(5), @FuelOilPcnt real, @FuelOilPrice real, @FuelOilPriceMT real
	DECLARE @AnilineDefault real, @N2PPMDefault real, @VABPDefault real, @SulfurDefault real
	SELECT 	@ProductID = ISNULL(BaseProductID, ProductID)
		, @AnilineAdj = AnilineSlope, @AnilineSpec = AnilineSpec, @AnilineDefault = AnilineDefault
		, @N2Adj = N2ppmSlope, @N2PPMSpec = N2PPMSpec, @N2PPMDefault = N2PPMDefault
		, @VABPAdj = VABPSlope, @VABPSpec = VABPSpec, @VABPDefault = VABPDefault
		, @SulfurAdj = SulfurSlope, @SulfurSpec = SulfurSpec, @SulfurDefault = SulfurDefault
		, @FuelOilID = FuelOilID, @FuelOilPcnt = FuelOilPcnt
	FROM ProductAdjLubes 
	WHERE StudyYear = @StudyYear AND PriceLoc = @PriceLoc AND ProductID = @Grade

	EXEC dbo.GetBaseProductPrice @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Density, @BasePrice OUTPUT, @BasePriceMT OUTPUT
	IF @FuelOilID IS NOT NULL
		EXEC dbo.GetBaseProductPrice @StudyYear, @Period, @PricingBasis, @PriceLoc, @FuelOilID, @Density, @FuelOilPrice OUTPUT, @FuelOilPriceMT OUTPUT
--
	IF @Sulfur IS NULL AND @SulfurDefault IS NOT NULL SELECT @Sulfur = @SulfurDefault
	IF @Sulfur IS NOT NULL AND ISNULL(@SulfurAdj, 0) <> 0
		SELECT @SulfurAdj = @SulfurAdj*(@Sulfur-@SulfurSpec)
	ELSE	SELECT @SulfurAdj = NULL
--
	IF @Aniline IS NULL AND @AnilineDefault IS NOT NULL SELECT @Aniline = @AnilineDefault
	IF @Aniline IS NOT NULL AND ISNULL(@AnilineAdj, 0) <> 0
		SELECT @AnilineAdj = @AnilineAdj*(@Aniline-@AnilineSpec)
	ELSE	SELECT @AnilineAdj = NULL
--
	IF @N2PPM IS NULL AND @N2PPMDefault IS NOT NULL SELECT @N2PPM = @N2PPMDefault
	IF @N2PPM IS NOT NULL AND ISNULL(@N2Adj, 0) <> 0
		SELECT @N2Adj = @N2Adj*(@N2PPM-@N2PPMSpec)
	ELSE	SELECT @N2Adj = NULL
--
	IF @VABP IS NULL AND @VABPDefault IS NOT NULL SELECT @VABP = @VABPDefault
	IF @VABP IS NOT NULL AND ISNULL(@VABPAdj, 0) <> 0
		SELECT @VABPAdj = @VABPAdj*(@VABP-@VABPSpec)
	ELSE	SELECT @VABPAdj = NULL
--
	SELECT 	@PricePerBbl = (@BasePrice + ISNULL(@SulfurAdj, 0) + ISNULL(@AnilineAdj, 0) + ISNULL(@N2Adj, 0) + ISNULL(@VABPAdj, 0)) * (1-ISNULL(@FuelOilPcnt/100,0))
				+ ISNULL(@FuelOilPrice, 0)*ISNULL(@FuelOilPcnt/100,0)

	IF @BasePriceMT > 0
		SELECT @PricePerMT = @PricePerBbl * (@BasePriceMT/@BasePrice),
			@SulfurAdjMT = @SulfurAdj * (@BasePriceMT/@BasePrice),
			@AnilineAdjMT = @AnilineAdj * (@BasePriceMT/@BasePrice),
			@N2AdjMT = @N2Adj * (@BasePriceMT/@BasePrice),
			@VABPAdjMT = @VABPAdj * (@BasePriceMT/@BasePrice)
END
ELSE BEGIN
	SELECT @ProductID = PricingID FROM Material_LU WHERE MaterialID = @Grade
	SELECT @IsScanDiesel = 0, @IsCARBDiesel = 0, @TypeAdjInclSulfur = 0
	IF @Grade = 'DTF'
	BEGIN
		IF @Type IS NULL
			SELECT @IsScanDiesel = 1, @IsCarbDiesel = 1
		ELSE IF @Type IN ('SCAN','SW1','SW2')
			SELECT @IsScanDiesel = 1
		ELSE IF @Type = 'CARB'
			SELECT @IsCARBDiesel = 1
		IF @StudyYear >= 2006 AND @Type IN ('CARB','EPA','ULSD','UULSD','SW1','SW2','SCAN','ULSOR')
			SELECT @TypeAdjInclSulfur = 1
	END
	IF @Study IN ('EUR', 'PAC', 'FL') AND @Grade IN ('LR','LP','LLR','LLP','LOL')
	BEGIN
	        IF @StudyYear < 2004
		BEGIN
			-- Apply the price for low lead gasolines
			IF @Leadgpl <= 0.15 AND @Grade IN ('LR','LP')
				SELECT @ProductID = CASE @Grade WHEN 'LR' THEN 'P204' ELSE 'P205' END
		END
		IF @Leadgpl IS NOT NULL
			EXEC dbo.GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'LEAD', @Leadgpl, @Density, @LeadAdj OUTPUT, @LeadAdjMT OUTPUT
	END
	ELSE
		SELECT @LeadAdj = NULL, @LeadAdjMT = NULL
	
	EXEC dbo.GetBaseProductPrice @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Density, @BasePrice OUTPUT, @BasePriceMT OUTPUT
	EXEC dbo.GetProductTypeAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, @Type, @Density, @TypeAdj OUTPUT, @TypeAdjMT OUTPUT
	
	IF @RVP IS NOT NULL AND @Grade <> 'AVGAS'
	BEGIN
		IF (@StudyYear < 2002 AND @Type = 'CON') 
		OR (@StudyYear = 2002 AND @Type IN ('CON', 'CONBB', 'OTHER', 'OXY', 'RBB', 'RFG', 'RFO'))
		OR (@StudyYear > 2002 AND @Type IN ('CON', 'CONBB', 'NACON'))
			EXEC dbo.GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'RVP', @RVP, @Density, @RVPAdj OUTPUT, @RVPAdjMT OUTPUT
	END
	IF @Sulfur IS NOT NULL AND @IsScanDiesel = 0 AND @IsCARBDiesel = 0 AND NOT (@TypeAdjInclSulfur = 1 AND (@TypeAdj IS NOT NULL OR @TypeAdjMT IS NOT NULL))
		EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'SULF', @Sulfur, @Density, @SulfurAdj OUTPUT, @SulfurAdjMT OUTPUT
	IF @Viscosity IS NOT NULL
		EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'VISC', @Viscosity, @Density, @ViscosityAdj OUTPUT, @ViscosityAdjMT OUTPUT
	IF @FreezePt IS NOT NULL AND @Grade = 'KJET'
		EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'FP', @FreezePt, @Density, @FreezePtAdj OUTPUT, @FreezePtAdjMT OUTPUT
	IF @Cetane IS NOT NULL AND ((@StudyYear < 2006 AND @IsScanDiesel = 0 AND @IsCARBDiesel = 0) OR (@StudyYear >= 2006 AND @Type IN ('OFF','ULSOR')))
		EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'CET', @Cetane, @Density, @CetaneAdj OUTPUT, @CetaneAdjMT OUTPUT
	IF @CloudPt IS NOT NULL AND @IsScanDiesel = 0 AND (@IsCarbDiesel = 0 OR @StudyYear < 2004)
		EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'CP', @CloudPt, @Density, @CloudPtAdj OUTPUT, @CloudPtAdjMT OUTPUT
	IF @ViscIndex IS NOT NULL
		EXEC GetProductAdj @StudyYear, @Period, @PricingBasis, @PriceLoc, @ProductID, 'VI', @ViscIndex, @Density, @ViscIndexAdj OUTPUT, @ViscIndexAdjMT OUTPUT
	
	SELECT 	@PricePerBbl = @BasePrice + ISNULL(@LeadAdj, 0) + ISNULL(@TypeAdj, 0) + ISNULL(@RVPAdj, 0) + ISNULL(@SulfurAdj, 0) + ISNULL(@ViscosityAdj, 0) 
				+ ISNULL(@FreezePtAdj, 0) + ISNULL(@CetaneAdj, 0) + ISNULL(@CloudPtAdj, 0) + ISNULL(@ViscIndexAdj, 0),
		@PricePerMT = @BasePriceMT + ISNULL(@LeadAdjMT, 0) + ISNULL(@TypeAdjMT, 0) + ISNULL(@RVPAdjMT, 0) + ISNULL(@SulfurAdjMT, 0) + ISNULL(@ViscosityAdjMT, 0)
				+ ISNULL(@FreezePtAdjMT, 0) + ISNULL(@CetaneAdjMT, 0) + ISNULL(@CloudPtAdjMT, 0) + ISNULL(@ViscIndexAdjMT, 0)
END





