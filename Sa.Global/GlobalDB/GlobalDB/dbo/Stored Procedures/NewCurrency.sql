﻿CREATE PROCEDURE dbo.NewCurrency(@OldCurrCode char(4), @NewCurrCode char(4) OUTPUT, @SwitchYear smallint OUTPUT)
AS
	SELECT @NewCurrCode = NewCurrency, @SwitchYear = LastYear FROM Currency_LU 
	WHERE CurrencyCode = @OldCurrCode
