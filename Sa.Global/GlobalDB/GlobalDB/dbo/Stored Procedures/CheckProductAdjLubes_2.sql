﻿
CREATE PROCEDURE CheckProductAdjLubes;2 (
	@StudyYear smallint, @PriceLoc smallint, @ProductID char(5), @BaseProductID char(5) = NULL OUTPUT,
	@AnilineSlope real = NULL OUTPUT, @AnilineSpec real = NULL OUTPUT,  @AnilineDefault real = NULL OUTPUT, 
	@N2PPMSlope real = NULL OUTPUT, @N2PPMSpec real = NULL OUTPUT, @N2PPMDefault real = NULL OUTPUT, 
	@VABPSlope real = NULL OUTPUT, @VABPSpec real = NULL OUTPUT, @VABPDefault real = NULL OUTPUT, 
	@SulfurSlope real = NULL OUTPUT, @SulfurSpec real = NULL OUTPUT, @SulfurDefault real = NULL OUTPUT, 
	@FuelOilPcnt real = NULL OUTPUT, @FuelOilID char(5) = NULL OUTPUT,
	@sAnilineSlope char(1) OUTPUT, @sAnilineSpec char(1) OUTPUT, @sAnilineDefault char(1) OUTPUT,
	@sN2PPMSlope char(1) OUTPUT, @sN2PPMSpec char(1) OUTPUT, @sN2PPMDefault char(1) OUTPUT,
	@sVABPSlope char(1) OUTPUT, @sVABPSpec char(1) OUTPUT, @sVABPDefault char(1) OUTPUT,
	@sSulfurSlope char(1) OUTPUT, @sSulfurSpec char(1) OUTPUT, @sSulfurDefault char(1) OUTPUT,
	@sFuelOilPcnt char(1) OUTPUT, @sFuelOilID char(1) OUTPUT, @sBaseProductID char(1) OUTPUT,
	@NewAdj tinyint OUTPUT)
AS
DECLARE	@CurrAnilineSlope real, @CurrAnilineSpec real, @CurrAnilineDefault real, 
	@CurrN2PPMSlope real, @CurrN2PPMSpec real,@CurrN2PPMDefault real, 
	@CurrVABPSlope real, @CurrVABPSpec real, @CurrVABPDefault real, 
	@CurrSulfurSlope real, @CurrSulfurSpec real, @CurrSulfurDefault real, 
	@CurrFuelOilPcnt real, @CurrFuelOilID char(5), @CurrBaseProductID char(5)
SELECT 	@CurrBaseProductID = BaseProductID,
	@CurrAnilineSlope = AnilineSlope, @CurrAnilineSpec = AnilineSpec, @CurrAnilineDefault = AnilineDefault,
	@CurrN2PPMSlope = N2PPMSlope, @CurrN2PPMSpec = N2PPMSpec, @CurrN2PPMDefault = N2PPMDefault,
	@CurrVABPSlope = VABPSlope, @CurrVABPSpec = VABPSpec, @CurrVABPDefault = VABPDefault,
	@CurrSulfurSlope = SulfurSlope, @CurrSulfurSpec = SulfurSpec, @CurrSulfurDefault = SulfurDefault,
	@CurrFuelOilPcnt = FuelOilPcnt, @CurrFuelOilID = FuelOilID
FROM ProductAdjLubes
WHERE StudyYear = @StudyYear AND PriceLoc = @PriceLoc AND ProductID = @ProductID
IF @@ROWCOUNT = 0
	SELECT  @NewAdj = 1,
		@AnilineSlope = NULL, @AnilineSpec = NULL, @AnilineDefault = NULL,
		@N2PPMSlope = NULL, @N2PPMSpec = NULL,  @N2PPMDefault = NULL,
		@VABPSlope = NULL, @VABPSpec = NULL,  @VABPDefault = NULL,
		@SulfurSlope = NULL, @SulfurSpec = NULL, @SulfurDefault = NULL,
		@FuelOilPcnt = NULL, @FuelOilID = NULL, @BaseProductID = NULL
ELSE BEGIN
	SET @NewAdj = 0

	IF @CurrBaseProductID IS NULL AND @BaseProductID IS NOT NULL
		SELECT @sBaseProductID = 'U'
	ELSE IF @CurrBaseProductID IS NOT NULL AND @BaseProductID IS NULL
		SELECT @sBaseProductID = 'U'
	ELSE IF @CurrBaseProductID <> @BaseProductID
		SELECT @sBaseProductID = 'U'
	ELSE
		SELECT @sBaseProductID = 'N'
	
	IF @CurrAnilineSlope IS NULL AND @AnilineSlope IS NOT NULL
		SELECT @sAnilineSlope = 'U'
	ELSE IF @CurrAnilineSlope IS NOT NULL AND @AnilineSlope IS NULL
		SELECT @sAnilineSlope = 'U'
	ELSE IF ABS(@CurrAnilineSlope - @AnilineSlope) > 0.0001
		SELECT @sAnilineSlope = 'U'
	ELSE
		SELECT @sAnilineSlope = 'N'
	
	IF @CurrAnilineSpec IS NULL AND @AnilineSpec IS NOT NULL
		SELECT @sAnilineSpec = 'U'
	ELSE IF @CurrAnilineSpec IS NOT NULL AND @AnilineSpec IS NULL
		SELECT @sAnilineSpec = 'U'
	ELSE IF ABS(@CurrAnilineSpec - @AnilineSpec) > 0.001
		SELECT @sAnilineSpec = 'U'
	ELSE
		SELECT @sAnilineSpec = 'N'

	IF @CurrAnilineDefault IS NULL AND @AnilineDefault IS NOT NULL
		SELECT @sAnilineDefault = 'U'
	ELSE IF @CurrAnilineDefault IS NOT NULL AND @AnilineDefault IS NULL
		SELECT @sAnilineDefault = 'U'
	ELSE IF ABS(@CurrAnilineDefault - @AnilineDefault) > 0.001
		SELECT @sAnilineDefault = 'U'
	ELSE
		SELECT @sAnilineDefault = 'N'
	
	IF @CurrN2PPMSlope IS NULL AND @N2PPMSlope IS NOT NULL
		SELECT @sN2PPMSlope = 'U'
	ELSE IF @CurrN2PPMSlope IS NOT NULL AND @N2PPMSlope IS NULL
		SELECT @sN2PPMSlope = 'U'
	ELSE IF ABS(@CurrN2PPMSlope - @N2PPMSlope) > 0.0001
		SELECT @sN2PPMSlope = 'U'
	ELSE
		SELECT @sN2PPMSlope = 'N'
	
	IF @CurrN2PPMSpec IS NULL AND @N2PPMSpec IS NOT NULL
		SELECT @sN2PPMSpec = 'U'
	ELSE IF @CurrN2PPMSpec IS NOT NULL AND @N2PPMSpec IS NULL
		SELECT @sN2PPMSpec = 'U'
	ELSE IF ABS(@CurrN2PPMSpec - @N2PPMSpec) > 0.001
		SELECT @sN2PPMSpec = 'U'
	ELSE
		SELECT @sN2PPMSpec = 'N'
	
	IF @CurrN2PPMDefault IS NULL AND @N2PPMDefault IS NOT NULL
		SELECT @sN2PPMDefault = 'U'
	ELSE IF @CurrN2PPMDefault IS NOT NULL AND @N2PPMDefault IS NULL
		SELECT @sN2PPMDefault = 'U'
	ELSE IF ABS(@CurrN2PPMDefault - @N2PPMDefault) > 0.001
		SELECT @sN2PPMDefault = 'U'
	ELSE
		SELECT @sN2PPMDefault = 'N'
	
	IF @CurrVABPSlope IS NULL AND @VABPSlope IS NOT NULL
		SELECT @sVABPSlope = 'U'
	ELSE IF @CurrVABPSlope IS NOT NULL AND @VABPSlope IS NULL
		SELECT @sVABPSlope = 'U'
	ELSE IF ABS(@CurrVABPSlope - @VABPSlope) > 0.0001
		SELECT @sVABPSlope = 'U'
	ELSE
		SELECT @sVABPSlope = 'N'
	
	IF @CurrVABPSpec IS NULL AND @VABPSpec IS NOT NULL
		SELECT @sVABPSpec = 'U'
	ELSE IF @CurrVABPSpec IS NOT NULL AND @VABPSpec IS NULL
		SELECT @sVABPSpec = 'U'
	ELSE IF ABS(@CurrVABPSpec - @VABPSpec) > 0.001
		SELECT @sVABPSpec = 'U'
	ELSE
		SELECT @sVABPSpec = 'N'
	
	IF @CurrVABPDefault IS NULL AND @VABPDefault IS NOT NULL
		SELECT @sVABPDefault = 'U'
	ELSE IF @CurrVABPDefault IS NOT NULL AND @VABPDefault IS NULL
		SELECT @sVABPDefault = 'U'
	ELSE IF ABS(@CurrVABPDefault - @VABPDefault) > 0.001
		SELECT @sVABPDefault = 'U'
	ELSE
		SELECT @sVABPDefault = 'N'
	
	IF @CurrSulfurSlope IS NULL AND @SulfurSlope IS NOT NULL
		SELECT @sSulfurSlope = 'U'
	ELSE IF @CurrSulfurSlope IS NOT NULL AND @SulfurSlope IS NULL
		SELECT @sSulfurSlope = 'U'
	ELSE IF ABS(@CurrSulfurSlope - @SulfurSlope) > 0.001
		SELECT @sSulfurSlope = 'U'
	ELSE
		SELECT @sSulfurSlope = 'N'
	
	IF @CurrSulfurSpec IS NULL AND @SulfurSpec IS NOT NULL
		SELECT @sSulfurSpec = 'U'
	ELSE IF @CurrSulfurSpec IS NOT NULL AND @SulfurSpec IS NULL
		SELECT @sSulfurSpec = 'U'
	ELSE IF ABS(@CurrSulfurSpec - @SulfurSpec) > 0.001
		SELECT @sSulfurSpec = 'U'
	ELSE
		SELECT @sSulfurSpec = 'N'
	
	IF @CurrSulfurDefault IS NULL AND @SulfurDefault IS NOT NULL
		SELECT @sSulfurDefault = 'U'
	ELSE IF @CurrSulfurDefault IS NOT NULL AND @SulfurDefault IS NULL
		SELECT @sSulfurDefault = 'U'
	ELSE IF ABS(@CurrSulfurDefault - @SulfurDefault) > 0.001
		SELECT @sSulfurDefault = 'U'
	ELSE
		SELECT @sSulfurDefault = 'N'
	
	SELECT @sFuelOilPcnt = 'N'
	IF (@CurrFuelOilPcnt IS NULL) AND (@FuelOilPcnt IS NOT NULL)
		SELECT @sFuelOilPcnt = 'U'
	ELSE IF @CurrFuelOilPcnt IS NOT NULL AND @FuelOilPcnt IS NULL
		SELECT @sFuelOilPcnt = 'U'
	ELSE IF ABS(ISNULL(@CurrFuelOilPcnt,0) - ISNULL(@FuelOilPcnt,0)) > 0.001
		SELECT @sFuelOilPcnt = 'U'
	
	IF @CurrFuelOilID IS NULL AND @FuelOilID IS NOT NULL
		SELECT @sFuelOilID = 'U'
	ELSE IF @CurrFuelOilID IS NOT NULL AND @FuelOilID IS NULL
		SELECT @sFuelOilID = 'U'
	ELSE IF @CurrFuelOilID <> @FuelOilID
		SELECT @sFuelOilID = 'U'
	ELSE
		SELECT @sFuelOilID = 'N'

	SELECT  @AnilineSlope = @CurrAnilineSlope, @AnilineSpec = @CurrAnilineSpec, @AnilineDefault = @CurrAnilineDefault, 
		@N2PPMSlope = @CurrN2PPMSlope, @N2PPMSpec = @CurrN2PPMSpec, @N2PPMDefault = @CurrN2PPMDefault, 
		@VABPSlope = @CurrVABPSlope, @VABPSpec = @CurrVABPSpec, @VABPDefault = @CurrVABPDefault, 
		@SulfurSlope = @CurrSulfurSlope, @SulfurSpec = @CurrSulfurSpec,@SulfurDefault = @CurrSulfurDefault, 
		@FuelOilPcnt = @CurrFuelOilPcnt, @FuelOilID = @CurrFuelOilID, @BaseProductID = @CurrBaseProductID
END
	


