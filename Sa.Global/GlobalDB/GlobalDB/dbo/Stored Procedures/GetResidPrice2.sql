﻿
CREATE PROCEDURE GetResidPrice2(@StudyYear smallint, @Period char(2), 
	@PricingBasis char(4), @PriceLoc smallint, @ProductID char(5), 
	@Viscosity real, @Sulfur real, @Density real = NULL,
	@BasePrice real OUTPUT, @BasePriceMT real OUTPUT,
	@ViscosityAdj real OUTPUT, @ViscosityAdjMT real OUTPUT,
	@SulfurAdj real OUTPUT, @SulfurAdjMT real OUTPUT,
	@PricePerBbl real OUTPUT, @PricePerMT real OUTPUT)
AS

EXEC GetAdjProductPrice @Study = NULL, @StudyYear = @StudyYear, @Period = @Period, @PricingBasis = @PricingBasis, 
	@PriceLoc = @PriceLoc, @Grade = @ProductID, @Viscosity = @Viscosity, @Sulfur = @Sulfur, @Density = @Density, 
	@BasePrice = @BasePrice OUTPUT, @BasePriceMT = @BasePriceMT OUTPUT, 
	@ViscosityAdj = @ViscosityAdj OUTPUT, @ViscosityAdjMT = @ViscosityAdjMT OUTPUT,
	@SulfurAdj = @SulfurAdj OUTPUT, @SulfurAdjMT = @SulfurAdjMT OUTPUT,
	@PricePerbbl = @PricePerbbl OUTPUT, @PricePerMT = @PricePerMT OUTPUT


