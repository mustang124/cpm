﻿CREATE TABLE [dbo].[CrudeOrigin_LU] (
    [CrudeOrigin] [dbo].[PricingLocation] NOT NULL,
    [Name]        VARCHAR (50)            NULL,
    CONSTRAINT [PK_CrudeOrigin_LU_1__13] PRIMARY KEY CLUSTERED ([CrudeOrigin] ASC) WITH (FILLFACTOR = 90)
);

