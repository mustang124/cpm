﻿CREATE TABLE [dbo].[PriceLoc_LU] (
    [PriceLoc]       SMALLINT  NOT NULL,
    [Region]         CHAR (30) NOT NULL,
    [Study]          CHAR (3)  NOT NULL,
    [SortKey]        INT       NULL,
    [EnergyPriceLoc] INT       NULL,
    CONSTRAINT [PK_PriceLoc_LU_1__13] PRIMARY KEY CLUSTERED ([PriceLoc] ASC) WITH (FILLFACTOR = 90)
);

