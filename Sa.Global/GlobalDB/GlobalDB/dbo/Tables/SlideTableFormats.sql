﻿CREATE TABLE [dbo].[SlideTableFormats] (
    [SlideTableFormatID] INT  NOT NULL,
    [TableX]             INT  NULL,
    [TableY]             INT  NULL,
    [FontHeight]         REAL NULL,
    [RowHeight]          INT  NULL,
    [CellFill]           BIT  NULL,
    [CellColorGroup]     INT  NULL,
    [BorderFill]         BIT  NULL,
    [BorderColorGroup]   INT  NULL,
    [ColWidth]           REAL NULL
);

