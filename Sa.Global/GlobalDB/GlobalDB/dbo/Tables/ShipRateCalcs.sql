﻿CREATE TABLE [dbo].[ShipRateCalcs] (
    [CalcRouteNo]   SMALLINT NOT NULL,
    [PostedRouteNo] SMALLINT NOT NULL,
    [Weighting]     REAL     NOT NULL
);

