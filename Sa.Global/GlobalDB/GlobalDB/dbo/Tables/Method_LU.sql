﻿CREATE TABLE [dbo].[Method_LU] (
    [MethodNo] TINYINT   NOT NULL,
    [Name]     CHAR (11) NULL,
    CONSTRAINT [PK___11__13] PRIMARY KEY CLUSTERED ([MethodNo] ASC) WITH (FILLFACTOR = 90)
);

