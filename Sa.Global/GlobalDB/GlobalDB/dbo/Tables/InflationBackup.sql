﻿CREATE TABLE [dbo].[InflationBackup] (
    [CurrencyCode] CHAR (4) NOT NULL,
    [Year]         SMALLINT NOT NULL,
    [InflIndex]    REAL     NOT NULL
);

