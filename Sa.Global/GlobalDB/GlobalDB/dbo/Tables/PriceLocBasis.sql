﻿CREATE TABLE [dbo].[PriceLocBasis] (
    [PriceLoc]     SMALLINT      NOT NULL,
    [StudyYear]    SMALLINT      NOT NULL,
    [PricingBasis] CHAR (8)      CONSTRAINT [DF_PriceLocBa_Scenario_2__13] DEFAULT ('BASE') NOT NULL,
    [Basis]        VARCHAR (30)  NOT NULL,
    [Differential] REAL          CONSTRAINT [DF_PriceLocBa_Differentia1__13] DEFAULT (0) NOT NULL,
    [Notes]        VARCHAR (100) NULL,
    CONSTRAINT [PK_PriceLocBasis_3__13] PRIMARY KEY CLUSTERED ([PriceLoc] ASC, [StudyYear] ASC, [PricingBasis] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_PriceLocBasis_PriceLoc_LU] FOREIGN KEY ([PriceLoc]) REFERENCES [dbo].[PriceLoc_LU] ([PriceLoc])
);

