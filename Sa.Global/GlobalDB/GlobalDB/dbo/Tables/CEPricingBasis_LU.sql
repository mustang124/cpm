﻿CREATE TABLE [dbo].[CEPricingBasis_LU] (
    [EPricingBasis] CHAR (5)          NOT NULL,
    [StudyYear]     [dbo].[StudyYear] NULL,
    [Description]   VARCHAR (50)      NULL,
    CONSTRAINT [PK_CEPricingBasis_LU] PRIMARY KEY CLUSTERED ([EPricingBasis] ASC) WITH (FILLFACTOR = 90)
);

