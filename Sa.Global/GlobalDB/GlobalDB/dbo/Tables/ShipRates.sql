﻿CREATE TABLE [dbo].[ShipRates] (
    [RouteNo]        SMALLINT      NOT NULL,
    [PostingDate]    SMALLDATETIME NOT NULL,
    [WorldscaleRate] SMALLINT      NOT NULL,
    CONSTRAINT [PK_ShipRates] PRIMARY KEY CLUSTERED ([RouteNo] ASC, [PostingDate] ASC)
);

