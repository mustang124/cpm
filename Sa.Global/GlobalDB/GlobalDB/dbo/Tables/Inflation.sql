﻿CREATE TABLE [dbo].[Inflation] (
    [CurrencyCode] CHAR (4) NOT NULL,
    [Year]         SMALLINT NOT NULL,
    [InflIndex]    REAL     NOT NULL,
    CONSTRAINT [PK_Inflation] PRIMARY KEY CLUSTERED ([CurrencyCode] ASC, [Year] ASC) WITH (FILLFACTOR = 90)
);

