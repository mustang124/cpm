﻿CREATE TABLE [dbo].[CrudeDest_LU] (
    [CrudeDest]  [dbo].[PricingLocation] NOT NULL,
    [Name]       VARCHAR (50)            NULL,
    [RegionCode] CHAR (1)                NULL,
    CONSTRAINT [PK_Destination_LU_1__13] PRIMARY KEY CLUSTERED ([CrudeDest] ASC) WITH (FILLFACTOR = 90)
);

