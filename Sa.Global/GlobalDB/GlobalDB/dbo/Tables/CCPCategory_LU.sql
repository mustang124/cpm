﻿CREATE TABLE [dbo].[CCPCategory_LU] (
    [CCPCat]        CHAR (4)     NOT NULL,
    [CategoryName]  VARCHAR (50) NOT NULL,
    [BasePricingID] CHAR (5)     NULL,
    CONSTRAINT [PK_CCPCategory_LU] PRIMARY KEY CLUSTERED ([CCPCat] ASC)
);

