﻿CREATE TABLE [dbo].[ShipStudyRoutes] (
    [Study]        CHAR (3) NOT NULL,
    [CargoSizeDWT] SMALLINT NOT NULL,
    [RouteNo]      SMALLINT NOT NULL,
    [Multiplier]   REAL     CONSTRAINT [DF_ShipRatesStudy_Multiplier] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ShipRatesStudy] PRIMARY KEY CLUSTERED ([Study] ASC, [CargoSizeDWT] ASC)
);

