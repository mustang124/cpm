﻿CREATE TABLE [dbo].[nParaffinBoilPt] (
    [CarbonNo]    INT  NOT NULL,
    [BoilingPt_F] REAL NOT NULL,
    CONSTRAINT [PK_nParaffinBoilPt] PRIMARY KEY CLUSTERED ([CarbonNo] ASC)
);

