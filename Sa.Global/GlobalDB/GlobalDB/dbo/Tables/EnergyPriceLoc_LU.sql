﻿CREATE TABLE [dbo].[EnergyPriceLoc_LU] (
    [EnergyPriceLoc]     INT       NOT NULL,
    [EnergyPriceLocDesc] CHAR (50) NOT NULL,
    CONSTRAINT [PK_EnergyPriceLoc_LU] PRIMARY KEY CLUSTERED ([EnergyPriceLoc] ASC)
);

