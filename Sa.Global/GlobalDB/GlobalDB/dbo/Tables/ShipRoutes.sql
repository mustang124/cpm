﻿CREATE TABLE [dbo].[ShipRoutes] (
    [RouteNo]      SMALLINT     IDENTITY (1, 1) NOT NULL,
    [PortCodeFrom] VARCHAR (5)  NOT NULL,
    [PortCodeTo]   VARCHAR (5)  NOT NULL,
    [CargoType]    VARCHAR (5)  NOT NULL,
    [CargoSizeMT]  INT          NOT NULL,
    [PlattsCode]   VARCHAR (7)  NULL,
    [Calculated]   [dbo].[YorN] CONSTRAINT [DF_ShipRoutes_Calculated] DEFAULT ('N') NOT NULL,
    CONSTRAINT [PK_ShipRoutes] PRIMARY KEY CLUSTERED ([RouteNo] ASC)
);

