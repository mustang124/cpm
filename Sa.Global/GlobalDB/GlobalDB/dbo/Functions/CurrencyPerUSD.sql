﻿CREATE FUNCTION dbo.CurrencyPerUSD(@CurrCode char(4), @Year smallint)
RETURNS real
BEGIN
	DECLARE @Value real
	SELECT @Value = ConvRate FROM CurrencyConv WHERE CurrencyCode = @CurrCode AND Year = @Year
	RETURN @Value
END

