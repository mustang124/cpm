﻿CREATE Function ConvertKGM3toAPI(@Density real)
RETURNS real
BEGIN
	DECLARE @API real
	IF @Density=0
		SELECT @API = NULL
	ELSE
		SELECT @API = (1000*141.5/@Density)-131.5
	RETURN @API
END

