﻿
CREATE Function GetPricePerBbl(@StudyYear smallint, @Period char(2), @PricingBasis char(4), @PriceLoc smallint, @ProductID char(5))
RETURNS real
AS
BEGIN
	DECLARE @BasePrice real
	SELECT @BasePrice = BasePrice FROM ProductPrice
	WHERE StudyYear = @StudyYear AND Period = @Period 
	AND PricingBasis = @PricingBasis AND PriceLoc = @PriceLoc
	AND ProductID = @ProductID

	RETURN @BasePrice

END

