﻿CREATE  FUNCTION dbo.ClosestCurrencyPerUSD(@CurrCode char(4), @Year smallint)
RETURNS real
BEGIN
	DECLARE @FirstYear smallint, @LastYear smallint
	DECLARE @NewCurrCode char(4)
	DECLARE @ConvRate real
	DECLARE @Value real
	SELECT @Value = dbo.CurrencyPerUSD(@CurrCode, @Year)
	IF @Value IS NULL
	BEGIN
		SELECT @FirstYear = MIN(Year), @LastYear = MAX(Year)
		FROM CurrencyConv WHERE CurrencyCode = @CurrCode

		IF @Year < @FirstYear
			SELECT @Value =  dbo.CurrencyPerUSD(@CurrCode, (SELECT MIN(Year) FROM CurrencyConv WHERE CurrencyCode = @CurrCode))
		ELSE BEGIN
			SELECT @NewCurrCode = NewCurrency, @FirstYear = LastYear FROM Currency_LU 
			WHERE CurrencyCode = @CurrCode

			IF ISNULL(@NewCurrCode, '') = ''
           			SELECT @Value = dbo.CurrencyPerUSD(@CurrCode, @LastYear)
			ELSE
				SELECT @Value = dbo.CurrencyPerUSD(@CurrCode, @FirstYear) * (dbo.ClosestCurrencyPerUSD(@NewCurrCode, @Year) / dbo.CurrencyPerUSD(@NewCurrCode, @FirstYear))
		END
	END
	RETURN @Value
END



