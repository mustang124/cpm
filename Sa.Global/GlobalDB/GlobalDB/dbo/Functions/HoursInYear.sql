﻿
create function [dbo].[HoursInYear](@Yr smallint)
RETURNS smallint
BEGIN
	DECLARE @hrs smallint
	SELECT @hrs = dbo.DaysInYear(@Yr)*24
	RETURN @hrs
END

