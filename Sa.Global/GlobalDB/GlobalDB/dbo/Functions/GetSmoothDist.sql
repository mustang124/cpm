﻿CREATE FUNCTION [dbo].[GetSmoothDist](@sourceData dbo.DistSourceTable READONLY)
RETURNS @tblDist TABLE(pct tinyint NOT NULL, value REAL NULL)
BEGIN

	DECLARE @FirstValue REAL, @FirstWeight REAL, @LastValue REAL, @LastWeight REAL
	DECLARE @recCount INT, @SortedASC BIT

	SELECT @recCount = COUNT(*) FROM @sourceData
	SELECT TOP 1 @FirstValue = Value, @FirstWeight = WeightFactor FROM @sourceData ORDER BY RowNumber ASC
	SELECT TOP 1 @LastValue = Value, @LastWeight = WeightFactor FROM @sourceData ORDER BY RowNumber DESC
	--determine the sort order
	IF @FirstValue <= @LastValue
		SET @SortedASC = 1
	ELSE
		SET @SortedASC = 0

	IF @recCount < 10
	BEGIN
		RETURN
	END
	IF @FirstValue < @LastValue
	BEGIN
		IF EXISTS (SELECT * FROM @sourceData a, @sourceData b WHERE a.RowNumber < b.RowNumber AND a.Value > b.Value)
			RETURN
	END
	ELSE IF @FirstValue > @LastValue
	BEGIN
		IF EXISTS (SELECT * FROM @sourceData a, @sourceData b WHERE a.RowNumber < b.RowNumber AND a.Value < b.Value)
			RETURN
	END
	ELSE IF @FirstValue = @LastValue
	BEGIN
		IF EXISTS (SELECT * FROM @sourceData WHERE Value <> @FirstValue)
			RETURN
	END

	--calculate the total weight factor
	DECLARE @TotalWeight REAL
	SELECT @TotalWeight = SUM(weightfactor) FROM @sourceData
	--adjust TotalWeight by removing the end two points
	SET @TotalWeight = @TotalWeight - @FirstWeight - @LastWeight

	--create a distribution table with pct set from 0 to 100
	INSERT @tblDist(pct) SELECT Num - 1 FROM PowerGlobal.dbo.Numbers WHERE Num <= 101

	UPDATE @tblDist SET value = (SELECT AVG(value) FROM (SELECT TOP 2 value FROM @sourceData ORDER BY RowNumber ASC) b) WHERE pct = 0
	UPDATE @tblDist SET value = (SELECT AVG(value) FROM (SELECT TOP 2 value FROM @sourceData ORDER BY RowNumber DESC) b) WHERE pct = 100

	--set up some variables for later use
	DECLARE @CummPcnt REAL = 0
	DECLARE @r REAL = 1
	DECLARE @c INT = 2

	DECLARE @value real, @weightfactor REAL
	DECLARE @CummMP REAL
	DECLARE @arDatacminus1 REAL
	DECLARE @arDist100 REAL
	DECLARE @arDist0 REAL
	DECLARE @arDatac REAL
	DECLARE @LastMP REAL
	SET @LastMP = 0

	SELECT @arDist0 = value FROM @tblDist WHERE pct = 0
	SELECT @arDist100 = value FROM @tblDist WHERE pct = 100


	--now loop through each record in the source data, calculating as we go
	DECLARE cSource CURSOR FAST_FORWARD LOCAL
	FOR SELECT value, weightfactor FROM @sourceData ORDER BY RowNumber

	OPEN cSource
	FETCH NEXT FROM cSource INTO @value, @weightfactor
	SELECT @arDatacminus1 = @arDatac --this is the previous value in the source
	SELECT @arDatac = @value --this is the current value in the source

	FETCH NEXT FROM cSource INTO @value, @weightfactor --we want to throw out the first row (already calced it), so we do this twice
	SELECT @arDatacminus1 = @arDatac
	SELECT @arDatac = @value
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--all this math comes from the spreadsheet
		SET @CummPcnt = @CummPcnt + ((@weightfactor/@TotalWeight) * 100)
		SET @CummMP = @CummPcnt - ((0.5 * (@weightfactor/@TotalWeight)) * 100)

		WHILE @CummMP > @r AND @r < 100
			BEGIN
				IF @c = @recCount
					UPDATE @tblDist SET value = @arDatacminus1 - (@arDatacminus1 - @arDist100) * (@LastMP - @r) / (@LastMP - @CummMP) WHERE pct = @r
				ELSE
					IF @c = 2
						UPDATE @tblDist SET value = @arDist0 - (@arDist0 - @arDatac) * (0 - @r)/(0 - @CummMP) WHERE pct = @r
					ELSE
						UPDATE @tblDist SET value = @arDatacminus1 - (@arDatacminus1 - @arDatac) * (@LastMP - @r)/(@LastMP - @CummMP) WHERE pct = @r

				SET @r = @r + 1
			END

		SET @LastMP = @CummMP
		SET @c = @c + 1

		FETCH NEXT FROM cSource INTO @value, @weightfactor
		
		SELECT @arDatacminus1 = @arDatac
		SELECT @arDatac = @value

	END

	CLOSE cSource
	DEALLOCATE cSource

	RETURN

END

