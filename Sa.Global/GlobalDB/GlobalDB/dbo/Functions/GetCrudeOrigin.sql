﻿
CREATE Function [dbo].[GetCrudeOrigin](@StudyYear StudyYear, @CNum varchar(5), @Period Period = 'TY')
RETURNS PricingLocation
AS
BEGIN
	DECLARE @Origin PricingLocation
	SELECT 	@Origin = P.CrudeOrigin
	FROM CrudePrice P
	WHERE P.StudyYear = @StudyYear AND P.Period = @Period AND P.CNum = @CNum
	
	RETURN @Origin
END

