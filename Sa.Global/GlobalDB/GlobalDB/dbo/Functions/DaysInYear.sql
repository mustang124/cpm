﻿
create function [dbo].[DaysInYear](@Yr smallint)
RETURNS smallint
BEGIN
	DECLARE @days smallint
	IF dbo.IsLeapYear(@Yr) = 1
		SELECT @days = 366
	ELSE
		SELECT @days = 365
	RETURN @days
END

