﻿CREATE Function dbo.GetCrudeGravityAdj(@StudyYear StudyYear, @CNum varchar(5), @ActualAPI real, @Period Period = 'TY')
RETURNS real
AS
BEGIN
	DECLARE @GravityAdj real, @PostedAPI real, @APIScale tinyint

	SELECT 	@PostedAPI = P.PostedAPI, @APIScale = P.APIScale
	FROM CrudePrice P
	WHERE P.StudyYear = @StudyYear AND P.Period = @Period AND P.CNum = @CNum

	DECLARE @tblAPI table(APIScale tinyint, APILevel tinyint, MinAPI real, MaxAPI real, APIAdj real NULL)
	INSERT INTO @tblAPI (APIScale, APILevel, MinAPI, MaxAPI, APIAdj)
	SELECT APIScale, APILevel, MinAPI, MaxAPI, 
		APIAdj = CASE 
			/* Gravity<MinAPI */
			WHEN (@ActualAPI<S.MinAPI) THEN
				/* AND PostedAPI>=MinAPI, PostedAPI<=MaxAPI */
				CASE WHEN (@PostedAPI>=S.MinAPI AND @PostedAPI<=S.MaxAPI) THEN (S.MinAPI-@PostedAPI)*S.Adj
				/* PostedAPI>MaxAPI */
				WHEN (@PostedAPI>S.MaxAPI) THEN (S.MinAPI-S.MaxAPI)*S.Adj
				END
			/*Gravity>=MinAPI, Gravity<=MaxAPI */
			WHEN (@ActualAPI>=S.MinAPI And @ActualAPI<=S.MaxAPI) THEN
				/* AND PostedAPI>MaxAPI*/
				CASE WHEN (@PostedAPI>S.MaxAPI) THEN (@ActualAPI-S.MaxAPI)*S.Adj
	    		/* AND PostedAPI<=MinAPI */
				WHEN (@PostedAPI<=S.MinAPI) THEN (@ActualAPI-S.MinAPI)*S.Adj
				/* AND PostedAPI>MinAPI, PostedAPI<=MaxAPI */
				WHEN (@PostedAPI>S.MinAPI AND @PostedAPI<=S.MaxAPI) THEN (@ActualAPI-@PostedAPI)*S.Adj
				END
			/* Gravity>MaxAPI */
			WHEN (@ActualAPI>S.MaxAPI) THEN
				/* AND PostedAPI>MinAPI, PostedAPI<=MaxAPI */
				CASE WHEN (@PostedAPI>S.MinAPI AND @PostedAPI<=MaxAPI) THEN (S.MaxAPI-@PostedAPI)*S.Adj
				/* AND PostedAPI<=MinAPI */
				WHEN (@PostedAPI<=S.MinAPI) THEN (S.MaxAPI-S.MinAPI)*S.Adj
				END
			END
	FROM APIScale S
	WHERE S.StudyYear = @StudyYear AND S.APIScale = @APIScale 
	SELECT @GravityAdj = SUM(APIAdj) FROM @tblAPI

	RETURN @GravityAdj
END
