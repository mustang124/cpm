﻿
CREATE VIEW [dbo].[ShipStudyRatesPivot]
AS
SELECT PostingYear, Study, CargoSizeDWT, Jan = [01], Feb = [02], Mar = [03], Apr = [04], May = [05], Jun = [06], Jul = [07], Aug = [08], Sep = [09], Oct = [10], Nov = [11], [Dec] = [12],[Q1],[Q2],[Q3],[Q4],[YR]
FROM (
	SELECT Study, CargoSizeDWT, PostingYear, PostingPeriod, WorldScaleMult
	FROM dbo.ShipStudyRates) src
PIVOT (MIN(WorldScaleMult) FOR PostingPeriod IN ([01],[02],[03],[04],[05],[06],[07],[08],[09],[10],[11],[12],[Q1],[Q2],[Q3],[Q4],[YR])) pvt


