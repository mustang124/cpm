﻿

CREATE VIEW [dbo].[ShipRateAvg]
AS
WITH PostedRates (RouteNo, PostingYear, PostingPeriod, WorldscaleRate)
AS (
	SELECT RouteNo, PostingYear = DATEPART(yy, PostingDate), PostingPeriod = RIGHT('0'+CAST(DATEPART(mm, PostingDate) as varchar),2), CAST(AVG(WorldscaleRate*1.0) AS numeric(6,1)) AS WorldscaleRate
	FROM dbo.ShipRates
	GROUP BY RouteNo, DATEPART(yy, PostingDate), DATEPART(mm, PostingDate)
	UNION
	SELECT RouteNo, PostingYear = DATEPART(yy, PostingDate), PostingPeriod = 'Q'+CAST(DATEPART(QUARTER, PostingDate) as varchar), CAST(AVG(WorldscaleRate*1.0) AS numeric(6,1)) AS WorldscaleRate
	FROM dbo.ShipRates
	GROUP BY RouteNo, DATEPART(yy, PostingDate), DATEPART(QUARTER, PostingDate)
	UNION
	SELECT RouteNo, PostingYear = DATEPART(yy, PostingDate), PostingPeriod = 'YR', CAST(AVG(WorldscaleRate*1.0) AS numeric(6,1)) AS WorldscaleRate
	FROM dbo.ShipRates
	GROUP BY RouteNo, DATEPART(yy, PostingDate)
)
SELECT RouteNo, PostingYear, PostingPeriod, WorldscaleRate
FROM PostedRates
UNION
SELECT rc.CalcRouteNo, pr.PostingYear, pr.PostingPeriod, WorldscaleRate = CAST(dbo.WtAvg(pr.WorldscaleRate, rc.Weighting) as numeric(6,1))
FROM PostedRates pr INNER JOIN dbo.ShipRateCalcs rc ON rc.PostedRouteNo = pr.RouteNo
WHERE NOT EXISTS (SELECT * FROM PostedRates x WHERE x.RouteNo = rc.CalcRouteNo AND x.PostingYear = pr.PostingYear)
GROUP BY rc.CalcRouteNo, pr.PostingYear, pr.PostingPeriod



