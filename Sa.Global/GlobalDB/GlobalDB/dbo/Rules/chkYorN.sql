﻿CREATE RULE [dbo].[chkYorN]
    AS @Value IN ('Y', 'N') OR @Value IS NULL;


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Material_LU].[Display]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ShipRoutes].[Calculated]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Material_LU].[IncludeForLossCalc]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Material_LU].[IncludeForMarginCalc]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Gravity]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[SpecGravity]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[UOPK]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Sulfur]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[MerSulfur]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[TotalN]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[BasicN]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Naphthenes]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Aromatics]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[RON]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Aniline]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Cetane]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[SmokePt]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[FreezePt]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[CloudPt]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[PourPt]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[TAN]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Visc104F]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Visc122F]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Visc212F]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[ConCarbon]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Nickel]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[Vanadium]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CrudeAssayCutProperties].[VABP]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[YorN]';

