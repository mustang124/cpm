﻿CREATE TYPE [dbo].[DistSourceTable] AS TABLE (
    [Value]        REAL NOT NULL,
    [WeightFactor] REAL NOT NULL,
    [RowNumber]    INT  IDENTITY (1, 1) NOT NULL);

