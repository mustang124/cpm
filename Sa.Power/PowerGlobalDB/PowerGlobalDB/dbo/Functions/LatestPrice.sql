﻿

CREATE  FUNCTION dbo.LatestPrice (@PricingHub varchar(15))  
RETURNS smalldatetime AS  
BEGIN 
DECLARE @date smalldatetime
SELECT @date = MAX(EndTime) FROM Prices WHERE PricingHub = @PricingHub
RETURN @date
END



