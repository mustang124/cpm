﻿CREATE TABLE [dbo].[CRVFactors] (
    [StudyYear]        SMALLINT NOT NULL,
    [CRVGroup]         CHAR (5) NOT NULL,
    [AvgFuelCostMBTU]  REAL     NOT NULL,
    [AvgUUWorth]       REAL     NOT NULL,
    [ConstantFactor]   REAL     NULL,
    [NOFFactor]        REAL     NULL,
    [NMCFactor]        REAL     NULL,
    [PSIGFactor]       REAL     NULL,
    [ServiceHrsFactor] REAL     NULL,
    [StartsFactor]     REAL     NULL,
    CONSTRAINT [PK_CRVFactors] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [CRVGroup] ASC) WITH (FILLFACTOR = 90)
);

