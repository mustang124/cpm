﻿CREATE TABLE [dbo].[SectionQuestion] (
    [SectionID]        CHAR (3) NOT NULL,
    [QuestionID]       SMALLINT NOT NULL,
    [QuestionLvl]      CHAR (1) CONSTRAINT [DF_SectionQue_QuestionLvl2__17] DEFAULT ('X') NOT NULL,
    [DetailedCategory] CHAR (3) CONSTRAINT [DF_SectionQue_DetailedCat2__17] DEFAULT ('N') NOT NULL,
    [Question]         TEXT     NULL,
    [GroupCode]        CHAR (4) NULL,
    [QuestionCode]     CHAR (4) NULL,
    CONSTRAINT [PK_SectionQuestion_1__17] PRIMARY KEY CLUSTERED ([SectionID] ASC, [QuestionID] ASC, [QuestionLvl] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK___1__17] FOREIGN KEY ([SectionID]) REFERENCES [dbo].[Sections] ([SectionID])
);

