﻿CREATE TABLE [dbo].[Country_LU] (
    [id]          INT       IDENTITY (1, 1) NOT NULL,
    [Country]     CHAR (50) NOT NULL,
    [Abbrev2ch]   CHAR (2)  NULL,
    [Abbrev3ch]   CHAR (3)  NULL,
    [NumericCode] INT       NULL,
    [FrequencyHz] INT       NULL,
    CONSTRAINT [PK_Country_LU] PRIMARY KEY CLUSTERED ([Country] ASC)
);

