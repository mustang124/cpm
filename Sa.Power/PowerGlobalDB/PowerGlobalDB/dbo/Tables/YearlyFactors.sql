﻿CREATE TABLE [dbo].[YearlyFactors] (
    [Year]               INT  NOT NULL,
    [InflationFactor]    REAL NOT NULL,
    [ContractorRate]     REAL NULL,
    [CTGContractorRate]  REAL NULL,
    [CCNonOHLumpSumRate] REAL NULL,
    [SOXValue]           REAL NULL,
    [NOXValue]           REAL NULL,
    [CO2Value]           REAL NULL,
    CONSTRAINT [PK_Inflation_1__10] PRIMARY KEY CLUSTERED ([Year] ASC) WITH (FILLFACTOR = 90)
);

