﻿CREATE TABLE [dbo].[Technology_LU] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [Technology]  VARCHAR (15)  NOT NULL,
    [Description] VARCHAR (100) NULL,
    [UserID]      VARCHAR (20)  NULL,
    [TimeStamp]   DATETIME      NULL
);

