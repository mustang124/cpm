﻿CREATE TABLE [dbo].[SelectionLU] (
    [ListCode]   CHAR (4)      NOT NULL,
    [ListChoice] VARCHAR (255) NULL,
    CONSTRAINT [PK___6__13] PRIMARY KEY CLUSTERED ([ListCode] ASC) WITH (FILLFACTOR = 90)
);

