﻿CREATE TABLE [dbo].[Unit_LU] (
    [UnitID]         CHAR (8)     NOT NULL,
    [SiteNo]         INT          NULL,
    [UnitName]       VARCHAR (50) NULL,
    [NERCUtility]    INT          NULL,
    [NERCUnit]       INT          NULL,
    [NERCUnitAbbr]   VARCHAR (25) NULL,
    [UnitLabel]      CHAR (5)     NULL,
    [NERCTimeOffset] SMALLINT     NULL,
    [NERCDSTAdj]     TINYINT      NULL,
    CONSTRAINT [PK_UnitLU] PRIMARY KEY CLUSTERED ([UnitID] ASC) WITH (FILLFACTOR = 90)
);

