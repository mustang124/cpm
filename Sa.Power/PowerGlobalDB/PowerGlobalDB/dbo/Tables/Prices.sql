﻿CREATE TABLE [dbo].[Prices] (
    [PricingHub] CHAR (15)     NOT NULL,
    [StartTime]  DATETIME2 (7) NOT NULL,
    [EndTime]    DATETIME2 (7) NOT NULL,
    [Price]      SMALLMONEY    NULL,
    CONSTRAINT [PK_Prices_1__14] PRIMARY KEY CLUSTERED ([PricingHub] ASC, [StartTime] ASC) WITH (FILLFACTOR = 90)
);

