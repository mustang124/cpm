﻿CREATE TABLE [dbo].[Continent_LU] (
    [id]        INT       IDENTITY (1, 1) NOT NULL,
    [Continent] CHAR (20) NOT NULL,
    CONSTRAINT [PK_Continent_LU] PRIMARY KEY CLUSTERED ([Continent] ASC)
);

