﻿CREATE TABLE [dbo].[EGCRegions] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [RegionName]  VARCHAR (20)  NOT NULL,
    [Description] VARCHAR (500) NOT NULL
);

