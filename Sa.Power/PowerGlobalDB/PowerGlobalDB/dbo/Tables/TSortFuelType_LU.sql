﻿CREATE TABLE [dbo].[TSortFuelType_LU] (
    [id]   INT          IDENTITY (1, 1) NOT NULL,
    [Type] CHAR (10)    NOT NULL,
    [Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_TSortFuelType_LU] PRIMARY KEY CLUSTERED ([Type] ASC)
);

