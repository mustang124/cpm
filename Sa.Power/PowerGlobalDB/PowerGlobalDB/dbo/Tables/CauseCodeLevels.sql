﻿CREATE TABLE [dbo].[CauseCodeLevels] (
    [CauseCode] INT         NOT NULL,
    [Level1]    NCHAR (100) NULL,
    [Level2]    NCHAR (100) NULL,
    [Level3]    NCHAR (100) NULL
);

