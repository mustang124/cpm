﻿CREATE TABLE [dbo].[SteamTable] (
    [Press]        INT  NOT NULL,
    [Temp]         INT  NOT NULL,
    [Enthalpy]     REAL NULL,
    [Entropy]      REAL NULL,
    [BaseEnthalpy] REAL NULL,
    CONSTRAINT [PK_SteamTable] PRIMARY KEY CLUSTERED ([Press] ASC, [Temp] ASC)
);

