﻿CREATE TABLE [dbo].[Sections] (
    [Section]   CHAR (30) NOT NULL,
    [SectionID] CHAR (3)  NOT NULL,
    [SortKey]   TINYINT   NOT NULL,
    CONSTRAINT [PK_Sections_1__18] PRIMARY KEY CLUSTERED ([SectionID] ASC) WITH (FILLFACTOR = 90)
);

