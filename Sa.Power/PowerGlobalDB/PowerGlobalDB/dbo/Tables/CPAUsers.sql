﻿CREATE TABLE [dbo].[CPAUsers] (
    [UserID]    VARCHAR (50) NOT NULL,
    [FirstName] VARCHAR (50) NOT NULL,
    [LastName]  VARCHAR (50) NOT NULL,
    [Role]      CHAR (10)    NULL,
    CONSTRAINT [PK_CPAUsers] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

