﻿CREATE TABLE [dbo].[ComponentTargets] (
    [StudyYear]       SMALLINT  NOT NULL,
    [TargetGroup]     CHAR (12) NOT NULL,
    [EquipGroup]      CHAR (8)  NOT NULL,
    [AnnMaintCostMWH] REAL      NOT NULL,
    CONSTRAINT [PK_ComponentTargets_1__15] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [TargetGroup] ASC, [EquipGroup] ASC) WITH (FILLFACTOR = 90)
);

