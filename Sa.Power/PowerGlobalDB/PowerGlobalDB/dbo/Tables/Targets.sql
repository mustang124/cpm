﻿CREATE TABLE [dbo].[Targets] (
    [StudyYear]               SMALLINT  NOT NULL,
    [TargetGroup]             CHAR (12) NOT NULL,
    [SiteWagesBen]            REAL      NOT NULL,
    [CentralWagesBen]         REAL      NOT NULL,
    [Contract]                REAL      NOT NULL,
    [OverhaulAdj]             REAL      NOT NULL,
    [Materials]               REAL      NOT NULL,
    [TaxInsurEnvir]           REAL      NOT NULL,
    [AGPers]                  REAL      NOT NULL,
    [OthFixed]                REAL      NOT NULL,
    [OtherVar]                REAL      NOT NULL,
    [OCCCompensation]         REAL      NOT NULL,
    [MPSCompensation]         REAL      NOT NULL,
    [OCCOVTPcnt]              REAL      NOT NULL,
    [MPSOVTPcnt]              REAL      NOT NULL,
    [InternalEnergyUsage]     REAL      NOT NULL,
    [HeatRateVariance]        REAL      NULL,
    [InternalEnergyLessScrub] REAL      NULL,
    CONSTRAINT [PK_Targets_1__15] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [TargetGroup] ASC) WITH (FILLFACTOR = 90)
);

