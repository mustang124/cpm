﻿CREATE TABLE [dbo].[RankSpecIDs] (
    [RankSpecsID]    [dbo].[DD_ID]     IDENTITY (1, 1) NOT NULL,
    [RefListNo]      [dbo].[RefListNo] NOT NULL,
    [BreakID]        [dbo].[DD_ID]     NOT NULL,
    [BreakValue]     CHAR (12)         NULL,
    [RankVariableID] [dbo].[DD_ID]     NOT NULL,
    CONSTRAINT [PK_RankSpecsLU_1] PRIMARY KEY CLUSTERED ([RankSpecsID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqRankSpec_1] UNIQUE NONCLUSTERED ([RefListNo] ASC, [BreakID] ASC, [BreakValue] ASC, [RankVariableID] ASC) WITH (FILLFACTOR = 90)
);

