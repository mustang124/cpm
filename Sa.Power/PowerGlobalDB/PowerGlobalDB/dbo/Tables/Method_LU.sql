﻿CREATE TABLE [dbo].[Method_LU] (
    [MethodNo] TINYINT      NOT NULL,
    [Name]     VARCHAR (11) NULL,
    CONSTRAINT [PK_Method_LU_1__21] PRIMARY KEY CLUSTERED ([MethodNo] ASC) WITH (FILLFACTOR = 90)
);

