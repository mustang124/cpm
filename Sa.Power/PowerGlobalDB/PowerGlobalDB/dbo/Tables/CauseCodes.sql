﻿CREATE TABLE [dbo].[CauseCodes] (
    [Cause_Code]      SMALLINT   NOT NULL,
    [Description]     CHAR (100) NULL,
    [SAIMajorEquip]   CHAR (40)  NULL,
    [SAIMinorEquip]   CHAR (40)  NULL,
    [PresDescription] CHAR (30)  NULL,
    [Preventable]     CHAR (1)   NULL,
    CONSTRAINT [PK_CauseCodes] PRIMARY KEY CLUSTERED ([Cause_Code] ASC)
);

