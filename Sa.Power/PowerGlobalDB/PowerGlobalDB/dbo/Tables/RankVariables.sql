﻿CREATE TABLE [dbo].[RankVariables] (
    [RankVariableID]  [dbo].[DD_ID]    IDENTITY (1, 1) NOT NULL,
    [Description]     VARCHAR (40)     NULL,
    [VariableID]      [dbo].[DD_ID]    NOT NULL,
    [Scenario]        [dbo].[Scenario] CONSTRAINT [DF__RankVaria__Scena__297722B6] DEFAULT ('BASE') NOT NULL,
    [SortOrder]       CHAR (4)         CONSTRAINT [DF__RankVaria__SortO__2A6B46EF] DEFAULT ('ASC') NULL,
    [Active]          [dbo].[YorN]     CONSTRAINT [DF__RankVaria__Activ__2B5F6B28] DEFAULT ('Y') NOT NULL,
    [Criteria]        VARCHAR (50)     NULL,
    [FullDescription] VARCHAR (100)    NULL,
    CONSTRAINT [PK__RankVariables] PRIMARY KEY CLUSTERED ([RankVariableID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [RankVariables_Desc]
    ON [dbo].[RankVariables]([Description] ASC) WITH (FILLFACTOR = 90);

