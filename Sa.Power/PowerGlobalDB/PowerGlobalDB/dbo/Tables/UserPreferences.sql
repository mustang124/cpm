﻿CREATE TABLE [dbo].[UserPreferences] (
    [PreferenceID] CHAR (5)     NOT NULL,
    [Description]  VARCHAR (50) NULL,
    [CurrencyID]   INT          NULL,
    [LiqVolume]    CHAR (4)     CONSTRAINT [DF__UserPrefe__LiqVo__2D27B809] DEFAULT ('BG') NOT NULL,
    [GasVolume]    CHAR (4)     CONSTRAINT [DF__UserPrefe__GasVo__2E1BDC42] DEFAULT ('SCF') NOT NULL,
    [Weight]       CHAR (4)     CONSTRAINT [DF__UserPrefe__Weigh__2F10007B] DEFAULT ('LB') NOT NULL,
    [ThermEnergy]  CHAR (4)     CONSTRAINT [DF__UserPrefe__Therm__300424B4] DEFAULT ('MBTU') NOT NULL,
    [Temperature]  CHAR (4)     CONSTRAINT [DF__UserPrefe__Tempe__30F848ED] DEFAULT ('TF') NOT NULL,
    [Pressure]     CHAR (4)     CONSTRAINT [DF__UserPrefe__Press__31EC6D26] DEFAULT ('PSI') NOT NULL,
    [Gravity]      CHAR (4)     CONSTRAINT [DF__UserPrefe__Densi__32E0915F] DEFAULT ('API') NOT NULL,
    [Viscosity]    CHAR (4)     CONSTRAINT [DF__UserPrefe__Visco__33D4B598] DEFAULT ('CS') NOT NULL,
    CONSTRAINT [PK_UserPreferences_1__12] PRIMARY KEY CLUSTERED ([PreferenceID] ASC) WITH (FILLFACTOR = 90)
);

