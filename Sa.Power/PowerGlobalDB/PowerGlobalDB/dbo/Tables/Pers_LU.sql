﻿CREATE TABLE [dbo].[Pers_LU] (
    [PersCat]          CHAR (15)    NOT NULL,
    [SortKey]          INT          NOT NULL,
    [Description]      VARCHAR (65) NOT NULL,
    [CalcCat]          CHAR (4)     NULL,
    [IncludeInAbsence] CHAR (1)     NULL,
    [SectionID]        CHAR (4)     NULL,
    CONSTRAINT [PK__PersLU_1__16] PRIMARY KEY CLUSTERED ([PersCat] ASC) WITH (FILLFACTOR = 90)
);

