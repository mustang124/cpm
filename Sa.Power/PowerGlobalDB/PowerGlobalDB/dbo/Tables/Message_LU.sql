﻿CREATE TABLE [dbo].[Message_LU] (
    [MessageID]   INT           IDENTITY (1, 1) NOT NULL,
    [Severity]    VARCHAR (1)   NULL,
    [MessageText] VARCHAR (255) NULL,
    [SysAdmin]    BIT           NOT NULL,
    [Consultant]  BIT           NOT NULL,
    [Pricing]     BIT           NOT NULL,
    [Audience4]   BIT           NOT NULL,
    [Audience5]   BIT           NOT NULL,
    [Audience6]   BIT           NOT NULL,
    [Audience7]   BIT           NOT NULL,
    CONSTRAINT [PK_Message_LU_1__21] PRIMARY KEY CLUSTERED ([MessageID] ASC) WITH (FILLFACTOR = 90)
);

