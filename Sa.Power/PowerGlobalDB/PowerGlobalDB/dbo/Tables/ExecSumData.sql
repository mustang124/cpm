﻿CREATE TABLE [dbo].[ExecSumData] (
    [ListName]    CHAR (8)   NOT NULL,
    [TopLabel]    CHAR (20)  NOT NULL,
    [BottomLabel] CHAR (20)  NOT NULL,
    [Variable]    CHAR (20)  NOT NULL,
    [TopP3]       FLOAT (53) NULL,
    [TopP2]       FLOAT (53) NULL,
    [TopP1]       FLOAT (53) NULL,
    [Top]         FLOAT (53) NOT NULL,
    [BtmP3]       FLOAT (53) NULL,
    [BtmP2]       FLOAT (53) NULL,
    [BtmP1]       FLOAT (53) NULL,
    [Btm]         FLOAT (53) NOT NULL,
    CONSTRAINT [PK_ExecSumData_1__10] PRIMARY KEY CLUSTERED ([ListName] ASC, [Variable] ASC) WITH (FILLFACTOR = 90)
);

