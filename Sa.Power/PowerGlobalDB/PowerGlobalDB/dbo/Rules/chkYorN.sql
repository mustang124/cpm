﻿CREATE RULE [dbo].[chkYorN]
    AS @Value IN ('Y', 'N');


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[RankBreaks].[Active]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[RankVariables].[Active]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[YorN]';

