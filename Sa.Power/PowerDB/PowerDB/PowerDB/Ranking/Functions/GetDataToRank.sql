﻿
CREATE FUNCTION [Ranking].[GetDataToRank](@Refnums [dbo].[RefnumList] READONLY, @RankVariable sysname)
RETURNS TABLE
AS
 RETURN (
		SELECT rd.Refnum, RankValue = rd.Value, rd.PcntFactor 
		FROM Ranking.UnitValues rd INNER JOIN @Refnums r ON r.Refnum = rd.Refnum
		WHERE rd.RankVariable = @RankVariable AND rd.Value IS NOT NULL AND rd.PcntFactor IS NOT NULL
		)
