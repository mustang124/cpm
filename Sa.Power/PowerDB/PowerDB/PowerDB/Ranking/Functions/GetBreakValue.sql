﻿
CREATE FUNCTION [Ranking].[GetBreakValue](@Refnum varchar(18), @RankBreak sysname, @RefListName varchar(30))
RETURNS varchar(12)
AS
BEGIN
	DECLARE @BreakValue varchar(12)
	IF @RankBreak = 'TotalList'
		SET @BreakValue = 'ALL'
	ELSE IF @RankBreak = 'UserGroup'
		SELECT @BreakValue = UserGroup FROM dbo._RL WHERE ListName = @RefListName AND Refnum = @Refnum
	ELSE
		SELECT @BreakValue = BreakValue
		FROM Ranking.UnitBreakValues
		WHERE Refnum = @Refnum AND RankBreak = @RankBreak
		
	RETURN @BreakValue
END

