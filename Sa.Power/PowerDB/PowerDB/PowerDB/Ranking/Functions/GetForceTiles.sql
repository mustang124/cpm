﻿CREATE FUNCTION [Ranking].[GetForceTiles](@RefListName varchar(30), @RankBreak sysname, @BreakValue varchar(12))
RETURNS tinyint
AS
BEGIN
	DECLARE @NumTiles tinyint
	
	SELECT @NumTiles = NumTiles FROM Ranking.RankForce
	WHERE ListName = @RefListName AND RankBreak = @RankBreak AND BreakValue = @BreakValue
	
	RETURN @NumTiles
END

