﻿
CREATE FUNCTION [Ranking].[GetUnitRankingVsRankedData](@Refnum varchar(18), @RefListName varchar(30), @RankBreak varchar(128), @BreakValue varchar(12), @RankVariable varchar(128))
RETURNS @results TABLE (Value real NULL, Percentile real NULL, PercentRanking tinyint NULL, Rank smallint NULL)
AS
BEGIN
	DECLARE @Value real
	SELECT @Value = Value FROM Ranking.UnitValues
	WHERE Refnum = @Refnum AND RankVariable = @RankVariable
	
	INSERT @results (Value, Percentile, PercentRanking, Rank)
	SELECT @Value, Percentile, PercentRanking, Rank
	FROM Ranking.GetValueRankingVsRankedData(@Value, @RefListName, @RankBreak, @BreakValue, @RankVariable)
	
	RETURN
END


