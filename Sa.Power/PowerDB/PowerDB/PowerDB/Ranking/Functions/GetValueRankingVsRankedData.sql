﻿CREATE FUNCTION [Ranking].[GetValueRankingVsRankedData](@Value float, @RefListName varchar(30), @RankBreak varchar(128), @BreakValue varchar(12), @RankVariable varchar(128))
RETURNS @results TABLE (Percentile real NULL, PercentRanking tinyint NULL, Rank smallint NULL)
AS BEGIN
	DECLARE @Value1Before real, @Rank1Before smallint, @Percentile1Before real, @Value1After real, @Percentile1After real
	DECLARE @HigherIsBetter bit
	SELECT @HigherIsBetter = CASE WHEN SortOrder = 'ASC' THEN 0 ELSE 1 END FROM Ranking.RankVariables WHERE RankVariable = @RankVariable

	SELECT TOP 1 @Value1Before = Value, @Percentile1Before = Percentile, @Rank1Before = [Rank] FROM Ranking.RankView
	WHERE ListName = @RefListName AND RankBreak = @RankBreak AND BreakValue = @BreakValue AND RankVariable = @RankVariable
	AND ((@HigherIsBetter = 1 AND Value >= @Value) OR (@HigherIsBetter = 0 AND Value <= @Value))
	ORDER BY [Rank] DESC

	SELECT TOP 1 @Value1After = Value, @Percentile1After = Percentile FROM Ranking.RankView
	WHERE ListName = @RefListName AND RankBreak = @RankBreak AND BreakValue = @BreakValue AND RankVariable = @RankVariable
	AND ((@HigherIsBetter = 1 AND Value <= @Value) OR (@HigherIsBetter = 0 AND Value >= @Value))
	ORDER BY [Rank] ASC

	IF @Percentile1Before IS NULL AND @Percentile1After IS NULL
		INSERT @results VALUES (NULL, NULL, NULL)
	ELSE IF @Percentile1Before IS NULL
		INSERT @results VALUES (100, 0, 1)
	ELSE IF @Percentile1After IS NULL
		INSERT @results VALUES (0, 100, @Rank1Before + 1)
	ELSE IF @Value1After = @Value1Before
		INSERT @results SELECT @Percentile1Before, 100-@Percentile1Before+0.5, @Rank1Before
	ELSE BEGIN
		INSERT @results (Percentile)
		SELECT ((@Percentile1After - @Percentile1Before)/(@Value1After - @Value1Before))*(@Value - @Value1Before) + @Percentile1Before
		UPDATE @results SET PercentRanking = 100-Percentile+0.5, Rank = @Rank1Before + 1
	END
	
	RETURN
END

