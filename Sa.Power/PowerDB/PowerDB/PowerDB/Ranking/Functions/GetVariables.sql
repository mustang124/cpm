﻿CREATE FUNCTION [Ranking].[GetVariables](@RefListName varchar(30) = NULL, @OnlyActive bit = 1)
RETURNS @RankVariables TABLE (RankVariable varchar(128) NOT NULL, SortOrder varchar(4) NOT NULL, RangeMinValue real NULL, RangeMaxValue real NULL)
AS
BEGIN
	IF @RefListName IS NULL OR NOT EXISTS (SELECT * FROM Ranking.RankSpecs_LU WHERE ListName = @RefListName)
		INSERT @RankVariables (RankVariable, SortOrder, RangeMinValue, RangeMaxValue)
		SELECT RankVariable, SortOrder, RangeMinValue, RangeMaxValue
		FROM Ranking.RankVariables WHERE Active = 'Y' OR @OnlyActive = 1
	ELSE
		INSERT @RankVariables (RankVariable, SortOrder, RangeMinValue, RangeMaxValue)
		SELECT DISTINCT rv.RankVariable, rv.SortOrder, rv.RangeMinValue, rv.RangeMaxValue
		FROM Ranking.RankSpecs_LU rs INNER JOIN Ranking.RankVariables rv ON rv.RankVariable = rs.RankVariable
		WHERE ListName = @RefListName
		
	RETURN
END

