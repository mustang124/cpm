﻿
CREATE VIEW [Ranking].[UnitBreakValues]
AS
SELECT t.Refnum, breaks.RankBreak, breaks.BreakValue 
FROM dbo.TSort t LEFT JOIN dbo.Breaks b ON b.Refnum = t.Refnum
CROSS APPLY (VALUES 
	('BaseCoal', CAST(b.BaseCoal as varchar(12))),
	('CCNMCGroup', CAST(b.CCNMCGroup as varchar(12))),
	('CoalHVGroup', CAST(b.CoalHVGroup as varchar(12))),
	('CoalNMCGroup', CAST(b.CoalNMCGroup as varchar(12))),
	('CoalNMCGroup2', CAST(b.CoalNMCGroup2 as varchar(12))),
	('CoalNMCOverlap', CAST(b.CoalNMCOverlap as varchar(12))),
	('CoalNMCOverlap2', CAST(b.CoalNMCOverlap2 as varchar(12))),
	('CogenElec', CAST(b.CogenElec as varchar(12))),
	('CRVGroup', CAST(b.CRVGroup as varchar(12))),
	('CRVSizeGroup', CAST(b.CRVSizeGroup as varchar(12))),
	('CRVSizeGroup2', CAST(b.CRVSizeGroup2 as varchar(12))),
	('FuelGroup', CAST(b.FuelGroup as varchar(12))),
	('GasOilNMCGroup', CAST(b.GasOilNMCGroup as varchar(12))),
	('LoadType', CAST(b.LoadType as varchar(12))),
	('Scrubbers', CAST(b.Scrubbers as varchar(12))),
	('ScrubberSize', CAST(b.ScrubberSize as varchar(12))),
	('SteamGasOil', CAST(b.SteamGasOil as varchar(12))),
	('Region', CAST(t.Region as varchar(12))),
	('Regulated', CAST(t.Regulated as varchar(12))),
	('StudyYear', CAST(t.StudyYear as varchar(12)))
) breaks(RankBreak, BreakValue)
WHERE breaks.BreakValue IS NOT NULL

