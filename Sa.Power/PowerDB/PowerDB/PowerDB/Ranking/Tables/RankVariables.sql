﻿CREATE TABLE [Ranking].[RankVariables] (
    [RankVariable]  VARCHAR (128) NOT NULL,
    [Description]   VARCHAR (100) NOT NULL,
    [SortOrder]     CHAR (4)      CONSTRAINT [DF__RankVariables__SortOrder] DEFAULT ('ASC') NOT NULL,
    [Active]        CHAR (1)      CONSTRAINT [DF__RankVariables__Active] DEFAULT ('Y') NOT NULL,
    [RangeMinValue] REAL          NULL,
    [RangeMaxValue] REAL          NULL,
    CONSTRAINT [PK__RankVariables] PRIMARY KEY CLUSTERED ([RankVariable] ASC) WITH (FILLFACTOR = 70)
);

