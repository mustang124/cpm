﻿CREATE PROC [Ranking].[RankRefList](@RefListName varchar(30), @RankBreak varchar(128) = NULL, @RankVariable varchar(128) = NULL)
AS
SET NOCOUNT ON

DECLARE @SnapshotTime smalldatetime
DECLARE @ListName nvarchar(84)
IF @RefListName IS NULL
BEGIN
	RAISERROR ('You must supply a valid ListName.', 16, 1)
	RETURN 1
END
ELSE BEGIN
	SELECT @ListName = ListName FROM dbo.RefList_LU WHERE ListName = @RefListName
	IF @ListName IS NULL
	BEGIN
		RAISERROR ('You must supply a valid ListName.', 16, 1)
		RETURN 1
	END
END

UPDATE Ranking.RankSpecs_LU
SET ListName = @ListName
WHERE ListName = @RefListName AND (ListName <> @ListName OR ListName IS NULL)

DECLARE @BreaksAndVars TABLE (
	RankBreak varchar(128) NOT NULL,
	RankVariable varchar(128) NOT NULL)
IF @RankBreak IS NULL AND @RankVariable IS NULL
	INSERT @BreaksAndVars (RankBreak, RankVariable)
	SELECT b.RankBreak, v.RankVariable
	FROM Ranking.GetBreaks(@RefListName, 1) b, Ranking.GetVariables(@RefListName, 1) v
ELSE IF @RankBreak IS NOT NULL AND @RankVariable IS NOT NULL
	INSERT @BreaksAndVars (RankBreak, RankVariable)
	SELECT b.RankBreak, v.RankVariable
	FROM Ranking.GetBreaks(0, 0) b, Ranking.GetVariables(0, 0) v
	WHERE b.RankBreak = @RankBreak AND v.RankVariable = @RankVariable
ELSE IF @RankBreak IS NOT NULL 
	INSERT @BreaksAndVars (RankBreak, RankVariable)
	SELECT b.RankBreak, v.RankVariable
	FROM Ranking.GetBreaks(0, 0) b, Ranking.GetVariables(0, 1) v
	WHERE b.RankBreak = @RankBreak 
ELSE IF @RankVariable IS NOT NULL
	INSERT @BreaksAndVars (RankBreak, RankVariable)
	SELECT b.RankBreak, v.RankVariable
	FROM Ranking.GetBreaks(0, 1) b, Ranking.GetVariables(0, 0) v
	WHERE v.RankVariable = @RankVariable

DECLARE @WhatToRank TABLE (
	RankBreak varchar(128) NOT NULL,
	BreakValue varchar(12) NOT NULL,
	RankVariable varchar(128) NOT NULL,
	ForceTiles tinyint NULL,
	PRIMARY KEY (RankBreak, BreakValue, RankVariable))

INSERT @WhatToRank (RankBreak, BreakValue, RankVariable)
SELECT DISTINCT w.RankBreak, v.BreakValue, w.RankVariable
FROM @BreaksAndVars w CROSS APPLY Ranking.GetBreakValues(@RefListName, w.RankBreak) v

UPDATE @WhatToRank SET ForceTiles = Ranking.GetForceTiles(@RefListName, RankBreak, BreakValue)

DECLARE @Break varchar(128), @Variable varchar(128), @BreakValue varchar(12), @ForceTiles tinyint
DECLARE @NumTiles tinyint, @DataCount int, @Top2 real, @Tile1Break real, @Tile2Break real, @Tile3Break real, @Btm2 real

DECLARE @RankedData Ranking.RankedData, @RankSpecsID int

WHILE EXISTS (SELECT * FROM @WhatToRank w)
BEGIN
	SELECT TOP 1 @Break = RankBreak, @Variable = RankVariable, @BreakValue = BreakValue, @ForceTiles = ForceTiles
	FROM @WhatToRank

	EXEC Ranking.GetRankSpecsID @RefListName, @Break, @BreakValue, @Variable, @RankSpecsID OUTPUT
	
	DELETE FROM @RankedData
	INSERT @RankedData(Refnum, Rank, Percentile, Tile, Value)	
	EXEC Ranking.spRankOne @RefListName, @Break, @BreakValue, @Variable, @ForceTiles, @NumTiles OUTPUT, @DataCount OUTPUT, @Top2 OUTPUT, @Tile1Break OUTPUT, @Tile2Break OUTPUT, @Tile3Break OUTPUT, @Btm2 OUTPUT

	DELETE FROM Ranking.Rank WHERE RankSpecsID = @RankSpecsID
	INSERT Ranking.Rank (RankSpecsID, Refnum, Rank, Percentile, Tile, Value)
	SELECT @RankSpecsID, Refnum, Rank, Percentile, Tile, Value FROM @RankedData

	IF EXISTS (SELECT * FROM Ranking.RankSpecsStats WHERE RankSpecsID = @RankSpecsID)
		UPDATE RankSpecsStats
		SET 	LastTime = GETDATE(), NumTiles = @NumTiles, DataCount = @DataCount, 
				Top2 = @Top2, Tile1Break = @Tile1Break, Tile2Break = @Tile2Break, Tile3Break = @Tile3Break, Btm2 = @Btm2
		WHERE RankSpecsID = @RankSpecsID
	ELSE
		INSERT INTO Ranking.RankSpecsStats (RankSpecsID, LastTime, NumTiles, DataCount, Top2, Tile1Break, Tile2Break, Tile3Break, Btm2)
		SELECT @RankSpecsID, GETDATE(), @NumTiles, @DataCount, @Top2, @Tile1Break, @Tile2Break, @Tile3Break, @Btm2

	DELETE FROM @WhatToRank WHERE RankBreak = @Break AND BreakValue = @BreakValue AND RankVariable = @Variable
END

