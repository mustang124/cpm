﻿CREATE ROLE [Developer]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [Developer] ADD MEMBER [DC1\Power_Developers];


GO
ALTER ROLE [Developer] ADD MEMBER [PWRAPPS];

