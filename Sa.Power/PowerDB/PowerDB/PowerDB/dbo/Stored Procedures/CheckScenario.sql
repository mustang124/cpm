﻿CREATE PROCEDURE [dbo].[CheckScenario] @TableName char (30), @RecCount tinyint OUTPUT
AS
SELECT @RecCount = (SELECT count(*) FROM sys.columns
	WHERE Name = 'Scenario'
	AND object_id = (SELECT object_id FROM sys.objects WHERE Name = @TableName AND schema_id = 1))
