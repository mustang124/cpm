﻿CREATE PROCEDURE [dbo].[CalcCost](@SiteID SiteID)
AS
DECLARE @StudyYear StudyYear; SELECT @StudyYear = StudyYear FROM StudySites WHERE SiteID = @SiteID

--     EmissionsAdjustment
DECLARE @SOxValue real, @NOxValue real, @CO2Value real
SELECT @SOxValue = ISNULL(SOXValue,0), @NOxValue = ISNULL(NOXValue,0), @CO2Value = ISNULL(CO2Value,0)
FROM YearlyFactors WHERE Year = @StudyYear

UPDATE Misc SET SO2Allow = SO2Emitted WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND ISNULL(SO2Allow,0) = 0
UPDATE Misc SET NOxAllow = NOxEmitted WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND ISNULL(NOxAllow,0) = 0
UPDATE Misc SET CO2Allow = CO2Emitted WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND ISNULL(CO2Allow,0) = 0
UPDATE Misc SET 
	SO2EmissionAdj = CASE WHEN ISNULL(SO2Allow,0) = 0 THEN 0 ELSE (ISNULL(SO2Emitted,0)-SO2Allow)*@SOxValue/1000 END,
    NOxEmissionAdj = CASE WHEN ISNULL(NOxAllow,0) = 0 THEN 0 ELSE (ISNULL(NOxEmitted,0)-NOxAllow)*@NOxValue/1000 END, 
    CO2EmissionAdj = CASE WHEN ISNULL(CO2Allow,0) = 0 THEN 0 ELSE (ISNULL(CO2Emitted,0)-CO2Allow)*@CO2Value/1000 END
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

--     MiscCalcs;
/* RevAsh is T7 Ash Revenue. RevOth is T7 "Other Chemical & Byproduct sales"
   OthRevenue is T8 "Other Revenue" which, by instructions, includes above T7 fields.
   OthNonT7Revenue is the T8 input(OthRev) minus the T7 input (RevAsh and RevOth)
   TotRevenueRevised is Sum of all of this (without counting anything twice) plus Ancillary Revenue
*/
/*
	RevAsh, RevOth, OthRevenue, AshHandle: Single;
	OthNonT7Revenue, TotRevenueRevised, TotAncillaryRevenue: Single;
	ScrubNonMaint, ScrubMaint: single;
	SOxTons, NOxTons, CO2Tons, SOxValue, NOxValue, CO2Value: single;
	SO2Emitted, SO2Allow: single;
	NOxEmitted, NOxAllow: single;
	CO2Emitted, CO2Allow: single;
	CurrConv: single;
	CurrencyID: integer;
	i: integer;
*/
DELETE FROM MiscCalc WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

DECLARE @MiscCalc TABLE (
	Refnum varchar(12) NOT NULL,
	RevAsh real NULL,
	RevOth real NULL,
	OthRevenue real NULL,
	OthNonT7Revenue real NULL,
	TotAncillaryRevenue real NULL,
	TotRevenueRevised real NULL,
	TotRevenue real NULL,
	TotAshHandling real NULL,
	AESOxTons real NULL, 
	AENOxTons real NULL,
	AESOxRevenue real NULL,
	AENOxRevenue real NULL,
	SO2Emitted real NULL,
	NOxEmitted real NULL,
	CO2Emitted real NULL,
	SO2EmissionAdj real NULL,
	NOxEmissionAdj real NULL,
	CO2EmissionAdj real NULL,
	SO2RemovalCost real NULL,
	TotScrubberNonMaint real NULL,
	ScrubberAnnMaintCost real NULL,
	ScrubberPowerCost real NULL,
	TotScrubber real NULL,
	ScrubConsMWH real NULL)	
INSERT INTO @MiscCalc (Refnum, RevAsh, RevOth, TotAshHandling, 
	TotScrubberNonMaint,
	AESOxTons, AENOxTons, AESOxRevenue, AENOxRevenue,
	SO2Emitted, NOxEmitted, CO2Emitted,
	SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj,
	OthRevenue, ScrubberAnnMaintCost, TotAncillaryRevenue, ScrubConsMWH)
SELECT Refnum, RevAsh = SUM(ISNULL(RevAsh,0)), RevOth = SUM(ISNULL(RevOth,0)), AshHandle = SUM(ISNULL(AshCostOp,0) + ISNULL(AshCostDisp,0)),
	ScrubNonMaint = SUM(ISNULL(ScrCostLime,0) + ISNULL(ScrCostChem,0) + ISNULL(ScrCostNMaint,0)),
	AESOxTons = CASE WHEN @StudyYear <= 1998 THEN SUM(ISNULL(AESOxTons,0)) END, 
	AENOxTons = CASE WHEN @StudyYear <= 1998 THEN SUM(ISNULL(AENOxTons,0)) END,
	AESOxRevenue = CASE WHEN @StudyYear <= 1998 THEN SUM(ISNULL(AESOxTons,0))*@SOxValue END, 
	AENOxRevenue = CASE WHEN @StudyYear <= 1998 THEN SUM(ISNULL(AENOxTons,0))*@NOxValue END,
	SO2Emitted = CASE WHEN @StudyYear > 1998 THEN SUM(ISNULL(SO2Emitted,0)) END, 
	NOxEmitted = CASE WHEN @StudyYear > 1998 THEN SUM(ISNULL(NOxEmitted,0)) END, 
	CO2Emitted = CASE WHEN @StudyYear > 1998 THEN SUM(ISNULL(CO2Emitted,0)) END,
	SO2EmissionAdj = CASE WHEN @StudyYear > 1998 THEN SUM(ISNULL(SO2EmissionAdj,0)) END, 
	NOxEmissionAdj = CASE WHEN @StudyYear > 1998 THEN SUM(ISNULL(NOxEmissionAdj,0)) END, 
	CO2EmissionAdj = CASE WHEN @StudyYear > 1998 THEN SUM(ISNULL(CO2EmissionAdj,0)) END,
	OthRevenue = ISNULL((SELECT OthRevenue FROM Opex WHERE Opex.Refnum = Misc.Refnum),0),
	ScrubMaint = ISNULL((SELECT SUM(AnnMaintCostKUS) FROM MaintEquipCalc WHERE MaintEquipCalc.Refnum = Misc.Refnum AND EquipGroup IN ('WGS', 'DGS')),0),
	TotAncillaryRevenue = ISNULL((SELECT SUM(Revenue) FROM Ancillary WHERE Ancillary.Refnum = Misc.Refnum),0),
	ScrubConsMWH = SUM(ISNULL(ScrConsMWH,0))
FROM Misc WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
GROUP BY Refnum

-- Opex Calculations
DELETE FROM OpexCalc WHERE Refnum In (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

DECLARE @Opex TABLE (
	Refnum varchar(12) NOT NULL,
	OCCWages real NULL,
	MPSWages real NULL,
	STWages real NULL,
	OCCBenefits real NULL,
	MPSBenefits real NULL,
	STBenefits real NULL,
	CentralOCCWagesBen real NULL,
	CentralMPSWagesBen real NULL,
	MaintMatl real NULL,
	LTSAAdj real NULL,
	ContMaintLabor real NULL,
	ContMaintMatl real NULL,
	ContMaintLumpSum real NULL,
	OthContSvc real NULL,
	OverhaulAdj real NULL,
	Envir real NULL,
	PropTax real NULL,
	OthTax real NULL,
	Insurance real NULL,
	AGPers real NULL,
	Supply real NULL,
	OthFixed real NULL,
	STFixed real NULL,
	Chemicals real NULL,
	Water real NULL,
	PurGas real NULL,
	PurLiquid real NULL,
	PurSolid real NULL,
	FuelInven real NULL,
	OthVar real NULL,
	STVar real NULL,
	TotCash real NULL,
	AGNonPers real NULL,
	Depreciation real NULL,
	Interest real NULL,
	STNonCash real NULL,
	GrandTot real NULL,
	OCCCompensation real NULL,
	MPSCompensation real NULL,
	TotCompensation real NULL,
	STFuelCost real NULL,
	TotCashLessFuel real NULL,
	ActCashLessFuel real NULL,
	TotCashLessFuelEm real NULL,
	SO2EmissionAdj real NULL,
	NOxEmissionAdj real NULL,
	CO2EmissionAdj real NULL,
	STFuelCostPlusSO2 real NULL,
	AllocSparesInven real NULL,
	DemandCapacity real NULL)
INSERT @Opex (Refnum, OCCWages, MPSWages, OCCBenefits, MPSBenefits, CentralOCCWagesBen, CentralMPSWagesBen, 
	MaintMatl, ContMaintLabor, ContMaintMatl, ContMaintLumpSum, OthContSvc, Envir, PropTax, OthTax, Insurance, AGPers, Supply, OthFixed,
	Chemicals, Water, PurGas, PurLiquid, PurSolid, FuelInven, OthVar, AGNonPers, Depreciation, Interest, AllocSparesInven, DemandCapacity,
	OverhaulAdj, LTSAAdj, SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj)
SELECT Refnum, ISNULL(OCCWages,0), ISNULL(MPSWages,0), ISNULL(OCCBenefits,0), ISNULL(MPSBenefits,0), ISNULL(CentralOCCWagesBen,0), ISNULL(CentralMPSWagesBen,0), 
	ISNULL(MaintMatl,0), ISNULL(ContMaintLabor,0), ISNULL(ContMaintMatl,0), ISNULL(ContMaintLumpSum,0), ISNULL(OthContSvc,0), 
	ISNULL(Envir,0), ISNULL(PropTax,0), ISNULL(OthTax,0), ISNULL(Insurance,0), ISNULL(AGPers,0), ISNULL(Supply,0), ISNULL(OthFixed,0),
	ISNULL(Chemicals,0), ISNULL(Water,0), ISNULL(PurGas,0), ISNULL(PurLiquid,0), ISNULL(PurSolid,0), ISNULL(FuelInven,0), ISNULL(OthVar,0), 
	ISNULL(AGNonPers,0), ISNULL(Depreciation,0), ISNULL(Interest,0), ISNULL(AllocSparesInven,0), ISNULL(DemandCapacity,0),
	OverhaulAdj = ISNULL((SELECT SUM(TotAnnOHCost) FROM OHMaint WHERE OHMaint.Refnum = Opex.Refnum),0),
	LTSAAdj = ISNULL((SELECT AnnLTSACost FROM MaintTotCalc WHERE MaintTotCalc.Refnum = Opex.Refnum),0),
	SO2EmissionAdj = ISNULL((SELECT SO2EmissionAdj FROM @MiscCalc m WHERE m.Refnum = Opex.Refnum),0),
	NOxEmissionAdj = ISNULL((SELECT NOxEmissionAdj FROM @MiscCalc m WHERE m.Refnum = Opex.Refnum),0),
	CO2EmissionAdj = ISNULL((SELECT CO2EmissionAdj FROM @MiscCalc m WHERE m.Refnum = Opex.Refnum),0)
FROM OpEx WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

DECLARE @OpexDivisors TABLE (Refnum varchar(12) NOT NULL, DataType char(5) NOT NULL, DivFactor real NULL)

/* Removed Supply Carrying cost on 8/3/01 per Bob Ruhlman */
/* FRS Moved AGNonPers from STNonCash to STFixed 6/20/06 per John Carney */
/* Moved FuelInven to NonCash 8/3/01 per Bob Ruhlman */
/* Added Supply Carrying Cost on 8/23/02 - should have been on 8/3/01 */
/* AGNonPers added to TotCash for 2005 */
UPDATE @Opex SET
	STWages = OCCWages + MPSWages, STBenefits = OCCBenefits + MPSBenefits,
	OCCCompensation = OCCWages + OCCBenefits, MPSCompensation = MPSWages + MPSBenefits, TotCompensation = OCCWages + OCCBenefits + MPSWages + MPSBenefits,
	STFixed = OCCWages + MPSWages + OCCBenefits + MPSBenefits + MaintMatl + ContMaintLabor + ContMaintMatl + ContMaintLumpSum +
            OthContSvc + Envir + PropTax + Insurance + AGPers + AGNonPers + OthFixed + OverhaulAdj + LTSAAdj +
            CentralOCCWagesBen + CentralMPSWagesBen + OthTax,
	STFuelCost = PurGas + PurLiquid + PurSolid,
	STVar = PurGas + PurLiquid + PurSolid + OthVar + SO2EmissionAdj + NOxEmissionAdj + CO2EmissionAdj + Chemicals + Water,
	STNonCash = Depreciation + Interest + FuelInven + Supply
UPDATE @Opex SET
	TotCash = STFixed + STVar, 
	TotCashLessFuel = STFixed + STVar - STFuelCost,
	TotCashLessFuelEm = STFixed + STVar - STFuelCost - (SO2EmissionAdj + NOxEmissionAdj + CO2EmissionAdj)
UPDATE @Opex SET
	GrandTot = TotCash + STNonCash,
	ActCashLessFuel = TotCashLessFuelEm - PropTax - OthTax - Insurance - AGPers - AGNonPers,
    STFuelCostPlusSO2 = STFuelCost + SO2EmissionAdj

INSERT @OpexDivisors (Refnum, DataType, DivFactor)
SELECT Refnum, 'ADJ', 1 FROM @Opex 
INSERT @OpexDivisors (Refnum, DataType, DivFactor)
SELECT Refnum, 'MWH', AdjNetMWH/1000 FROM GenerationTotCalc WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
INSERT @OpexDivisors (Refnum, DataType, DivFactor)
SELECT Refnum, 'KW', NMC FROM NERCFactors WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
INSERT @OpexDivisors (Refnum, DataType, DivFactor)
SELECT Refnum, 'PCNT', TotCash/100 FROM @Opex 

INSERT INTO dbo.OpExCalc (Refnum, DataType, OCCWages, MPSWages, STWages, OCCBenefits, MPSBenefits, STBenefits, 
	MaintMatl, LTSAAdj, ContMaintLabor, ContMaintMatl, ContMaintLumpSum, OthContSvc, OverhaulAdj, Envir, 
	PropTax, Insurance, AGPers, Supply, OthFixed, STFixed, PurGas, PurLiquid, PurSolid, FuelInven, OthVar, 
	STVar, TotCash, AGNonPers, Depreciation, Interest, STNonCash, GrandTot, OCCCompensation, MPSCompensation, 
	TotCompensation, STFuelCost, CentralOCCWagesBen, CentralMPSWagesBen, OthTax, TotCashLessFuel, ActCashLessFuel, 
	SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj, Chemicals, Water, STFuelCostPlusSO2, AllocSparesInven, 
	DivFactor, TotCashLessFuelEm)
SELECT o.Refnum, d.DataType, OCCWages/d.DivFactor, MPSWages/d.DivFactor, STWages/d.DivFactor, OCCBenefits/d.DivFactor, 
	MPSBenefits/d.DivFactor, STBenefits/d.DivFactor, MaintMatl/d.DivFactor, LTSAAdj/d.DivFactor, 
	ContMaintLabor/d.DivFactor, ContMaintMatl/d.DivFactor, ContMaintLumpSum/d.DivFactor, OthContSvc/d.DivFactor, 
	OverhaulAdj/d.DivFactor, Envir/d.DivFactor, PropTax/d.DivFactor, Insurance/d.DivFactor, AGPers/d.DivFactor, 
	Supply/d.DivFactor, OthFixed/d.DivFactor, STFixed/d.DivFactor, PurGas/d.DivFactor, PurLiquid/d.DivFactor, 
	PurSolid/d.DivFactor, FuelInven/d.DivFactor, OthVar/d.DivFactor, STVar/d.DivFactor, TotCash/d.DivFactor, 
	AGNonPers/d.DivFactor, Depreciation/d.DivFactor, Interest/d.DivFactor, STNonCash/d.DivFactor, GrandTot/d.DivFactor, 
	OCCCompensation/d.DivFactor, MPSCompensation/d.DivFactor, TotCompensation/d.DivFactor, STFuelCost/d.DivFactor, 
	CentralOCCWagesBen/d.DivFactor, CentralMPSWagesBen/d.DivFactor, OthTax/d.DivFactor, TotCashLessFuel/d.DivFactor, 
	ActCashLessFuel/d.DivFactor, SO2EmissionAdj/d.DivFactor, NOxEmissionAdj/d.DivFactor, CO2EmissionAdj/d.DivFactor, 
	Chemicals/d.DivFactor, Water/d.DivFactor, STFuelCostPlusSO2/d.DivFactor, AllocSparesInven/d.DivFactor, 
	DivFactor = d.DivFactor, TotCashLessFuelEm/d.DivFactor
FROM @Opex o INNER JOIN @OpexDivisors d ON d.Refnum = o.Refnum
WHERE d.DivFactor >0

-- Do scrubber power cost calculations and other total calculations for MiscCalc
UPDATE mc SET 
	ScrubberPowerCost = ISNULL((SELECT o.STVar*mc.ScrubConsMWH/1000 FROM OpexCalc o WHERE o.Refnum = mc.Refnum AND o.DataType = 'MWH'), 0)
FROM @MiscCalc mc
UPDATE @MiscCalc SET 
	TotScrubber = ISNULL(ScrubberAnnMaintCost,0) + ISNULL(TotScrubberNonMaint,0),
	OthNonT7Revenue = ISNULL(OthRevenue,0) - ISNULL(RevAsh,0) - ISNULL(RevOth,0),
	TotRevenueRevised = ISNULL(OthRevenue,0) + ISNULL(TotAncillaryRevenue,0),
	TotRevenue = ISNULL(RevAsh,0) + ISNULL(RevOth,0) + ISNULL(OthRevenue,0)
UPDATE @MiscCalc SET TotScrubberNonMaint = TotScrubberNonMaint + ISNULL(ScrubberPowerCost,0)
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
UPDATE @MiscCalc SET TotScrubber = ISNULL(ScrubberAnnMaintCost,0) + ISNULL(TotScrubberNonMaint,0)
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

INSERT INTO MiscCalc (Refnum, 
	RevAsh, RevOth, OthRevenue, OthNonT7Revenue, TotAncillaryRevenue, TotRevenueRevised ,TotRevenue, 
	TotAshHandling, TotScrubberNonMaint,
	AESOxTons, AENOxTons, AESOxRevenue, AENOxRevenue,
	SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj,
	ScrubberAnnMaintCost, ScrubberPowerCost, TotScrubber,
	SO2Emitted, NOxEmitted, CO2Emitted)
SELECT Refnum, 
	RevAsh, RevOth, OthRevenue, OthNonT7Revenue, TotAncillaryRevenue, TotRevenueRevised, TotRevenue,
	TotAshHandling, TotScrubberNonMaint,
	AESOxTons, AENOxTons, AESOxRevenue, AENOxRevenue,
	SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj,
	ScrubberAnnMaintCost, ScrubberPowerCost, TotScrubber,
	SO2Emitted, NOxEmitted, CO2Emitted
FROM @MiscCalc
