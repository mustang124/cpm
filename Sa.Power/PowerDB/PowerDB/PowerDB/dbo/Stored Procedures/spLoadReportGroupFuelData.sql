﻿



CREATE PROC [dbo].[spLoadReportGroupFuelData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

	--remove the existing record in the table
	DELETE FROM ReportGroupFuelData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


			INSERT ReportGroupFuelData (RefNum, 
				ReportTitle, 
				ListName, 
				TotalFuelCost,
				HHVGasCost,
				HHVLiquidCost,
				HHVCoalCost,
				HHVAvgCost,
				LHVGasCost,
				LHVLiquidCost,
				LHVCoalCost,
				LHVAvgCost,
				CoalMineCost,
				CoalTransport,
				CoalOnSitePrep,
				CoalTotalCost,
				CoalTranportPerMile,
				HHVTotalGasConsumed,
				HHVNatGasConsumed,
				HHVOffGasConsumed,
				HHVHydroGasConsumed,
				HHVJetFuelConsumed,
				HHVDieselConsumed,
				HHVFuelOilConsumed,
				HHVCoalConsumed,
				LHVTotalGasConsumed,
				LHVNatGasConsumed,
				LHVOffGasConsumed,
				LHVHydroGasConsumed,
				LHVJetFuelConsumed,
				LHVDieselConsumed,
				LHVFuelOilConsumed,
				LHVCoalConsumed,
				CoalHHV,
				CoalLHV,
				CoalAsh,
				CoalSulfur,
				CoalMoisture,
				FuelOilHHV,
				FuelOilLHV,
				FuelOilSulfur,
				FuelOilViscosity,
				FuelOilGravity,
				FuelOilPourPoint,
				FuelOilVanadium,
				FuelOilAsphaltenes)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				TotalFuelCost = GlobalDB.dbo.WtAvg(omwh.STFuelCost, gtc.AdjNetMWH),
				HHVGasCost = CASE WHEN SUM(ftc.TotGasMBTU) <> 0 THEN SUM(ISNULL(o.PurGas,0))/SUM(ftc.TotGasMBTU) * 1000 END,
				HHVLiquidCost = CASE WHEN SUM(ftc.LiquidsMBTU) <> 0 THEN SUM(ISNULL(o.PurLiquid,0))/SUM(ftc.LiquidsMBTU) * 1000 END,
				HHVCoalCost = CASE WHEN SUM(ftc.CoalMBTU) <> 0 THEN SUM(ISNULL(o.PurSolid,0))/SUM(ftc.CoalMBTU) * 1000 END,
				HHVAvgCost = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(oadj.STFuelCost,0))/SUM(ftc.TotMBTU) * 1000 END,
				LHVGasCost = CASE WHEN SUM(ftc.TotGasMBTULHV) <> 0 THEN SUM(ISNULL(o.PurGas,0))/SUM(ftc.TotGasMBTULHV) * 1000 END,
				LHVLiquidCost = CASE WHEN SUM(ftc.LiquidsMBTULHV) <> 0 THEN SUM(ISNULL(o.PurLiquid,0))/SUM(ftc.LiquidsMBTULHV) * 1000 END,
				LHVCoalCost = CASE WHEN SUM(ftc.CoalMBTULHV) <> 0 THEN SUM(ISNULL(o.PurSolid,0))/SUM(ftc.CoalMBTULHV) * 1000 END,
				LHVAvgCost = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(oadj.STFuelCost,0))/SUM(ftc.TotMBTULHV) * 1000 END,
				CoalMineCost = GlobalDB.dbo.WtAvg(ctc.MineCostTon, ctc.TotTons),
				CoalTransport = GlobalDB.dbo.WtAvg(ctc.TransCostTon, ctc.TotTons),
				CoalOnSitePrep = GlobalDB.dbo.WtAvg(ctc.OnsitePrepCostTon, ctc.TotTons),
				CoalTotalCost = GlobalDB.dbo.WtAvg(ctc.TotCostTon, ctc.TotTons),
				CoalTranportPerMile = GlobalDB.dbo.WtAvg(ctc.TransCostTM, ctc.TotTonMiles),
				HHVTotalGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.TotGasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				HHVNatGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.NatGasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				HHVOffGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.OffGasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				HHVHydroGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.H2GasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				HHVJetFuelConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.JetTurbMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				HHVDieselConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.DieselMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				HHVFuelOilConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.FuelOilMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				HHVCoalConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.CoalMBTU,0))/SUM(ftc.TotMBTU) END * 100,
				LHVTotalGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.TotGasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				LHVNatGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.NatGasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				LHVOffGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.OffGasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				LHVHydroGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.H2GasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				LHVJetFuelConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.JetTurbMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				LHVDieselConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.DieselMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				LHVFuelOilConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.FuelOilMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				LHVCoalConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.CoalMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
				CoalHHV = GlobalDB.dbo.WtAvgNZ(ctc.HeatValue, ctc.TotTons),
				CoalLHV = GlobalDB.dbo.WtAvgNZ(ctc.LHV, ctc.TotTons),
				CoalAsh = GlobalDB.dbo.WtAvgNZ(ctc.AshPcnt, ctc.TotTons),
				CoalSulfur = GlobalDB.dbo.WtAvgNZ(ctc.SulfurPcnt, ctc.TotTons),
				CoalMoisture = GlobalDB.dbo.WtAvgNZ(ctc.MoistPcnt, ctc.TotTons),
				FuelOilHHV = GlobalDB.dbo.WtAvgNZ(ftc.FuelOilHeatValue, ftc.FuelOilKGal),
				FuelOilLHV = GlobalDB.dbo.WtAvgNZ(ftc.FuelOilLHV, ftc.FuelOilKGal),
				FuelOilSulfur = GlobalDB.dbo.WtAvgNZ(fp.Sulfur, ftc.FuelOilKGal),
				FuelOilViscosity = GlobalDB.dbo.WtAvgNN(fp.Viscosity, ftc.FuelOilKGal),
				FuelOilGravity = GlobalDB.dbo.WtAvgNN(fp.Gravity, ftc.FuelOilKGal),
				FuelOilPourPoint = GlobalDB.dbo.WtAvgNN(fp.PourPt, ftc.FuelOilKGal),
				FuelOilVanadium = GlobalDB.dbo.WtAvgNN(fp.Vanadium, ftc.FuelOilKGal),
				FuelOilAsphaltenes = GlobalDB.dbo.WtAvgNN(fp.Asphaltenes, ftc.FuelOilKGal)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN OpExCalc omwh ON omwh.Refnum = t.Refnum and omwh.DataType = 'MWH'
				INNER JOIN OpEx o ON o.refnum = t.Refnum
				INNER JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
				INNER JOIN FuelTotCalc ftc on ftc.Refnum = t.Refnum
				INNER JOIN OpExCalc oadj ON oadj.Refnum = t.Refnum and oadj.DataType = 'ADJ'
				INNER JOIN CoalTotCalc ctc ON ctc.Refnum = t.Refnum
				LEFT JOIN FuelProperties fp ON fp.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''

			
			--and this is the actual query being run
			EXEC ('INSERT ReportGroupFuelData (RefNum, 
					ReportTitle, 
					ListName,
					TotalFuelCost,
					HHVGasCost,
					HHVLiquidCost,
					HHVCoalCost,
					HHVAvgCost,
					LHVGasCost,
					LHVLiquidCost,
					LHVCoalCost,
					LHVAvgCost,
					CoalMineCost,
					CoalTransport,
					CoalOnSitePrep,
					CoalTotalCost,
					CoalTranportPerMile,
					HHVTotalGasConsumed,
					HHVNatGasConsumed,
					HHVOffGasConsumed,
					HHVHydroGasConsumed,
					HHVJetFuelConsumed,
					HHVDieselConsumed,
					HHVFuelOilConsumed,
					HHVCoalConsumed,
					LHVTotalGasConsumed,
					LHVNatGasConsumed,
					LHVOffGasConsumed,
					LHVHydroGasConsumed,
					LHVJetFuelConsumed,
					LHVDieselConsumed,
					LHVFuelOilConsumed,
					LHVCoalConsumed,
					CoalHHV,
					CoalLHV,
					CoalAsh,
					CoalSulfur,
					CoalMoisture,
					FuelOilHHV,
					FuelOilLHV,
					FuelOilSulfur,
					FuelOilViscosity,
					FuelOilGravity,
					FuelOilPourPoint,
					FuelOilVanadium,
					FuelOilAsphaltenes)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					TotalFuelCost = GlobalDB.dbo.WtAvg(omwh.STFuelCost, gtc.AdjNetMWH),
					HHVGasCost = CASE WHEN SUM(ftc.TotGasMBTU) <> 0 THEN SUM(ISNULL(o.PurGas,0))/SUM(ftc.TotGasMBTU) * 1000 END,
					HHVLiquidCost = CASE WHEN SUM(ftc.LiquidsMBTU) <> 0 THEN SUM(ISNULL(o.PurLiquid,0))/SUM(ftc.LiquidsMBTU) * 1000 END,
					HHVCoalCost = CASE WHEN SUM(ftc.CoalMBTU) <> 0 THEN SUM(ISNULL(o.PurSolid,0))/SUM(ftc.CoalMBTU) * 1000 END,
					HHVAvgCost = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(oadj.STFuelCost,0))/SUM(ftc.TotMBTU) * 1000 END,
					LHVGasCost = CASE WHEN SUM(ftc.TotGasMBTULHV) <> 0 THEN SUM(ISNULL(o.PurGas,0))/SUM(ftc.TotGasMBTULHV) * 1000 END,
					LHVLiquidCost = CASE WHEN SUM(ftc.LiquidsMBTULHV) <> 0 THEN SUM(ISNULL(o.PurLiquid,0))/SUM(ftc.LiquidsMBTULHV) * 1000 END,
					LHVCoalCost = CASE WHEN SUM(ftc.CoalMBTULHV) <> 0 THEN SUM(ISNULL(o.PurSolid,0))/SUM(ftc.CoalMBTULHV) * 1000 END,
					LHVAvgCost = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(oadj.STFuelCost,0))/SUM(ftc.TotMBTULHV) * 1000 END,
					CoalMineCost = GlobalDB.dbo.WtAvg(ctc.MineCostTon, ctc.TotTons),
					CoalTransport = GlobalDB.dbo.WtAvg(ctc.TransCostTon, ctc.TotTons),
					CoalOnSitePrep = GlobalDB.dbo.WtAvg(ctc.OnsitePrepCostTon, ctc.TotTons),
					CoalTotalCost = GlobalDB.dbo.WtAvg(ctc.TotCostTon, ctc.TotTons),
					CoalTranportPerMile = GlobalDB.dbo.WtAvg(ctc.TransCostTM, ctc.TotTonMiles),
					HHVTotalGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.TotGasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					HHVNatGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.NatGasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					HHVOffGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.OffGasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					HHVHydroGasConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.H2GasMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					HHVJetFuelConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.JetTurbMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					HHVDieselConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.DieselMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					HHVFuelOilConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.FuelOilMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					HHVCoalConsumed = CASE WHEN SUM(ftc.TotMBTU) <> 0 THEN SUM(ISNULL(ftc.CoalMBTU,0))/SUM(ftc.TotMBTU) END * 100,
					LHVTotalGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.TotGasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					LHVNatGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.NatGasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					LHVOffGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.OffGasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					LHVHydroGasConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.H2GasMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					LHVJetFuelConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.JetTurbMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					LHVDieselConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.DieselMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					LHVFuelOilConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.FuelOilMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					LHVCoalConsumed = CASE WHEN SUM(ftc.TotMBTULHV) <> 0 THEN SUM(ISNULL(ftc.CoalMBTULHV,0))/SUM(ftc.TotMBTULHV) END * 100,
					CoalHHV = GlobalDB.dbo.WtAvgNZ(ctc.HeatValue, ctc.TotTons),
					CoalLHV = GlobalDB.dbo.WtAvgNZ(ctc.LHV, ctc.TotTons),
					CoalAsh = GlobalDB.dbo.WtAvgNZ(ctc.AshPcnt, ctc.TotTons),
					CoalSulfur = GlobalDB.dbo.WtAvgNZ(ctc.SulfurPcnt, ctc.TotTons),
					CoalMoisture = GlobalDB.dbo.WtAvgNZ(ctc.MoistPcnt, ctc.TotTons),
					FuelOilHHV = GlobalDB.dbo.WtAvgNZ(ftc.FuelOilHeatValue, ftc.FuelOilKGal),
					FuelOilLHV = GlobalDB.dbo.WtAvgNZ(ftc.FuelOilLHV, ftc.FuelOilKGal),
					FuelOilSulfur = GlobalDB.dbo.WtAvgNZ(fp.Sulfur, ftc.FuelOilKGal),
					FuelOilViscosity = GlobalDB.dbo.WtAvgNN(fp.Viscosity, ftc.FuelOilKGal),
					FuelOilGravity = GlobalDB.dbo.WtAvgNN(fp.Gravity, ftc.FuelOilKGal),
					FuelOilPourPoint = GlobalDB.dbo.WtAvgNN(fp.PourPt, ftc.FuelOilKGal),
					FuelOilVanadium = GlobalDB.dbo.WtAvgNN(fp.Vanadium, ftc.FuelOilKGal),
					FuelOilAsphaltenes = GlobalDB.dbo.WtAvgNN(fp.Asphaltenes, ftc.FuelOilKGal)
				FROM TSort t 
					INNER JOIN OpExCalc omwh ON omwh.Refnum = t.Refnum and omwh.DataType = ''MWH''
					INNER JOIN OpEx o ON o.refnum = t.Refnum
					INNER JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
					INNER JOIN FuelTotCalc ftc on ftc.Refnum = t.Refnum
					INNER JOIN OpExCalc oadj ON oadj.Refnum = t.Refnum and oadj.DataType = ''ADJ''
					INNER JOIN CoalTotCalc ctc ON ctc.Refnum = t.Refnum
					LEFT JOIN FuelProperties fp ON fp.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END





