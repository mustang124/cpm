﻿CREATE PROC [dbo].[LoadUnitsToCopy](@ListName RefListName, @ForYear smallint)
AS
DELETE FROM UnitsToCopy

INSERT UnitsToCopy (OldRefnum, OldSiteID, NewRefnum, StudyYear)
SELECT t.Refnum, t.SiteID, dbo.RefnumBase(t.Refnum) + RIGHT(@ForYear, 2) + 'P', @ForYear
FROM _RL INNER JOIN TSort t  ON t.Refnum = _RL.Refnum
WHERE _RL.ListName = @ListName

UPDATE UnitsToCopy
SET NewSiteID = STUFF(OldSiteID, CHARINDEX('PN',OldSiteID,  1) + 2, 3, RIGHT(RTRIM(NewRefnum), 3))
WHERE OldSiteID LIKE '%PN%'

UPDATE UnitsToCopy
SET NewSiteID = STUFF(OldSiteID, CHARINDEX('CC',OldSiteID,  1) + 2, 3, RIGHT(RTRIM(NewRefnum), 3))
WHERE OldSiteID LIKE '%CC%'

SELECT * FROM UnitsToCopy WHERE OldRefnum IS NULL
