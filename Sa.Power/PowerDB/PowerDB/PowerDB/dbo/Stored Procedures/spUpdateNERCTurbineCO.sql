﻿







CREATE  PROC spUpdateNERCTurbineCO (@Refnum varchar(10))
AS
Declare @NMC real, @NDC real
Declare @EFOR real, @EFORWtFactor real
Declare @EPOF real
Declare @EUOF real
Declare @EPOR real, @EPORWtFactor real
Declare @EUOR real, @EUORWtFactor real
Declare @EUF  real
Declare @EAF  real
Declare @POF  real
Declare @EOR  real, @EORWtFactor real
Declare @FOF  real
Declare @MOF  real 
Declare	@NUMFO real 	-- F5 #16
Declare	@NUMMO real 	-- F5 #17
Declare	@NUMPO real 	-- F4 #14
Declare	@NUMUO real  	-- F4 #15
Declare @PH    real 	-- F2 #11
Declare @SH    real     -- F1 #1
Declare @UOF   real	-- F7 #2
Declare @PH_Unweighted real
Declare @SH_Unweighted real
Declare @AGE	real
Declare	@ACT_Starts real 
Declare	@ATM_Starts real 
	
Declare @UtilityUnitCode char(6)	
Declare @EventYear int
Declare @EventYearMinusOne as int

--If CalcCommUnavail is set to N in TSort
--Then DON'T run this procedure.
IF EXISTS (SELECT * FROM TSort WHERE Refnum = @Refnum AND CalcCommUnavail = 'N')
	RETURN

Declare cTurbines cursor
For 	Select UtilityUnitCode 
	FROM NERCTurbine
	WHERE Refnum = @Refnum

-- Get the Event Year from TSort
SELECT @EventYear = t.EvntYear, @EventYearMinusOne = t.EvntYear - 1
FROM TSort t
WHERE Refnum = @Refnum

OPEN cTurbines
FETCH NEXT FROM cTurbines INTO @UtilityUnitCode
WHILE (@@FETCH_STATUS <> -1)
BEGIN
	-- Load up all of the 1-year fields
	EXEC spGadsFactors @UtilityUnitCode ,@EventYear , @EventYear,
	@NMC = @NMC output, @NDC = @NDC output,
	@EFOR = @EFOR output, @EFORWtFactor = @EFORWtFactor output,
	@EPOF = @EPOF output, 
	@EUOF = @EUOF output, 
	@EPOR  = @EPOR output, @EPORWtFactor = @EPORWtFactor output,
	@EUOR = @EUOR output, @EUORWtFactor = @EUORWtFactor output,
	@EUF = @EUF output, 
	@EAF = @EAF output,
	@POF = @POF output, 
	@EOR = @EOR output, @EORWtFactor = @EORWtFactor output,
	@FOF = @FOF output,
	@MOF = @MOF  output,
	@NUMFO = @NUMFO output,	-- F5 #16
	@NUMMO = @NUMMO output,	-- F5 #17
	@NUMPO = @NUMPO output,	-- F4 #14
	@NUMUO = @NUMUO output,	-- F4 #15
	@PH = @PH	output,
	@SH = @SH	output,
	@UOF = @UOF     output, -- F7 #2
	@PH_Unweighted = @PH_Unweighted output,
	@SH_Unweighted = @SH_Unweighted output,
	@AGE = @AGE output,
	@ACT_Starts = @ACT_Starts output,
	@ATM_Starts = @ATM_Starts output
	
	-- Update the record with the 1-year fields
	UPDATE NERCTurbine SET 
--	NMC = @NMC, NDC = @NDC,
--	EFOR = @EFOR, EFORWtFactor = @EFORWtFactor,  
	EPOF = @EPOF, 
	EUOF = @EUOF, 
	EPOR = @EPOR, EPORWtFactor = @EPORWtFactor,
	EUOR = @EUOR, EUORWtFactor = @EUORWtFactor,
--	EUF = @EUF, 
--	EAF = @EAF,
--	POF = @POF, 
--	EOR = @EOR, EORWtFactor = @EORWtFactor,
	FOF = @FOF,
	MOF = @MOF
--	NUMFO = @NUMFO,
--	NUMMO = @NUMMO,
--	NUMPO = @NUMPO,
--	NUMUO = @NUMUO,
--	PeriodHrs = @PH_Unweighted,
--	ServiceHrs = @SH_Unweighted,
--	UOF = @UOF,
--	Age = @AGE,
--	ACTStarts = @ACT_Starts,
--	ATMStarts = @ATM_Starts
	WHERE Refnum = @Refnum AND UtilityUnitCode = @UtilityUnitCode

	-- Load up all of the 2-year fields
	EXEC spGadsFactors @UtilityUnitCode ,@EventYear , @EventYearMinusOne,
	@NMC = @NMC output, @NDC = @NDC output,
	@EFOR = @EFOR output, @EFORWtFactor = @EFORWtFactor output,
	@EPOF = @EPOF output, 
	@EUOF = @EUOF output, 
	@EPOR  = @EPOR output, @EPORWtFactor = @EPORWtFactor output,
	@EUOR = @EUOR output, @EUORWtFactor = @EUORWtFactor output,
	@EUF = @EUF output, 
	@EAF = @EAF output,
	@POF = @POF output, 
	@EOR = @EOR output, @EORWtFactor = @EORWtFactor output,
	@FOF = @FOF output,
	@MOF = @MOF  output,
	@NUMFO = @NUMFO output,	-- F5 #16
	@NUMMO = @NUMMO output,	-- F5 #17
	@NUMPO = @NUMPO output,	-- F4 #14
	@NUMUO = @NUMUO output,	-- F4 #15
	@PH = @PH	output,
	@SH = @SH	output,
	@UOF = @UOF     output, -- F7 #2
	@PH_Unweighted = @PH_Unweighted output,
	@SH_Unweighted = @SH_Unweighted output,
	@AGE = @AGE output,
	@ACT_Starts = @ACT_Starts output,
	@ATM_Starts = @ATM_Starts output

	-- Update the record with the 2-year fields
	UPDATE NERCTurbine SET 
--	NMC2Yr = @NMC, NDC2Yr = @NDC,
--	EFOR2Yr = @EFOR, EFOR2YrWtFactor = @EFORWtFactor,  
	EPOF2Yr = @EPOF, 
	EUOF2Yr = @EUOF, 
	EPOR2Yr = @EPOR, EPOR2YrWtFactor = @EPORWtFactor,
	EUOR2Yr = @EUOR, EUOR2YrWtFactor = @EUORWtFactor,
--	EUF2Yr = @EUF, 
--	EAF2Yr = @EAF,
--	POF2Yr = @POF, 
	EOR2Yr = @EOR, EOR2YrWtFactor = @EORWtFactor
--	FOF2Yr = @FOF,
--	MOF2Yr = @MOF,
--	PeriodHrs2Yr = @PH_Unweighted,
--	ServiceHrs2Yr = @SH_Unweighted,
--	ServiceHrs2YrAvg = @SH_Unweighted / 2,
--	UOF2Yr = @UOF
	WHERE Refnum = @Refnum AND UtilityUnitCode = @UtilityUnitCode

	FETCH NEXT FROM cTurbines INTO @UtilityUnitCode
END
CLOSE cTurbines
PRINT 'Dealloc cTurbines'
DEALLOCATE cTurbines








