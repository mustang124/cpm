﻿
CREATE PROC [dbo].[WaterfallDataPopulate]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10)) --should be MWH or MW (maybe EGC someday?)
AS

	DECLARE @IsPS varchar(3)
	set @IsPS = case when @ParentList like '%Pacesetter%' then ' PS' else '' end



	DECLARE refs CURSOR FOR SELECT Refnum, UnitName from TSort where Refnum in (select Refnum from _RL where ListName = @ListName) and StudyYear = @StudyYear
	DECLARE @Refnum VARCHAR(12)
	DECLARE @UnitName VARCHAR(50)

	OPEN refs
	FETCH NEXT FROM refs INTO @Refnum, @UnitName
		WHILE @@FETCH_STATUS = 0
	BEGIN

		create table #tmp (Title varchar(30), Title2 varchar(30), BeginCell real, Gap1 real, Label real, Down real, Up real, EndCell real, sort int)

		insert #tmp select * from WaterfallTop (@ListName, @StudyYear, @ParentList, @DataType) where Title = @UnitName

		insert #tmp (Title, Title2, sort, Label, Down, up) select unitname, title, Rank + 1, ABS(value), case when Value <0 then ABS(value) end, case when Value >0 then Value end from WaterfallEventSummary (@ListName, @StudyYear, @ParentList, @DataType) where unitname = @UnitName

		update #tmp set Gap1 = (select BeginCell from #tmp where sort = 1) - label - ISNULL(case when down is not null then down end,0) where sort = 2
		update #tmp set Gap1 = (select Gap1 + label from #tmp where sort = 2) - Label - ISNULL(case when Down is not null then Down end,0) where sort = 3
		update #tmp set Gap1 = Gap1 + (select isnull(Up,0) from #tmp where sort = 2) where sort = 3
		update #tmp set Gap1 = (select Gap1 + label from #tmp where sort = 3) - Label - ISNULL(case when Down is not null then Down end,0) where sort = 4
		update #tmp set Gap1 = Gap1 + (select isnull(Up,0) from #tmp where sort = 3) where sort = 4
		update #tmp set Gap1 = (select Gap1 + label from #tmp where sort = 4) - Label - ISNULL(case when Down is not null then Down end,0) where sort = 5
		update #tmp set Gap1 = Gap1 + (select isnull(Up,0) from #tmp where sort = 4) where sort = 5
		update #tmp set Gap1 = (select Gap1 + label from #tmp where sort = 5) - Label - ISNULL(case when Down is not null then Down end,0) where sort = 6
		update #tmp set Gap1 = Gap1 + (select isnull(Up,0) from #tmp where sort = 5) where sort = 6
		update #tmp set Gap1 = (select Gap1 + label from #tmp where sort = 6) - Label - ISNULL(case when Down is not null then Down end,0) where sort = 7
		update #tmp set Gap1 = Gap1 + (select isnull(Up,0) from #tmp where sort = 6) where sort = 7



		insert #tmp (Title, Title2, EndCell, sort) select Title, Title2 = CASE WHEN @IsPS = ' PS' THEN 'Pacesetter' ELSE 'Peer Group' END, EndCell, sort from WaterfallBot (@ListName, @StudyYear, @ParentList, @DataType) where Title = @UnitName
		--select * from #tmp order by sort

		insert waterfallData (Refnum, Title, Title2, BeginCell, Gap1, Label, Down, Up, EndCell, sort, DataType) 
			select @Refnum, rtrim(Title) + @IsPS, Title2, BeginCell, Gap1, Label, Down, Up, EndCell, sort, @DataType from #tmp order by sort


		drop table #tmp

			FETCH NEXT FROM refs INTO @Refnum, @UnitName
	END
	CLOSE refs
	DEALLOCATE refs



