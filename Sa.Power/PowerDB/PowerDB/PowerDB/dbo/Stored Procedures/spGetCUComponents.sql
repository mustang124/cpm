﻿CREATE  PROCEDURE spGetCUComponents (@Refnum Refnum, 
	@PricingHub char(15),
	@StartPeriod smalldatetime, 
	@EndPeriod smalldatetime,
	@TotPeakMWH real OUTPUT, 
	@TotMRO real OUTPUT,
	@PeakMWHLost_P real OUTPUT, 
	@PeakMWHLost_M real OUTPUT, 
	@PeakMWHLost_F real OUTPUT, 
	@PeakMWHLost_Tot real OUTPUT,
	@LRO_P real OUTPUT, 
	@LRO_M real OUTPUT, 
	@LRO_F real OUTPUT, 
	@LRO_Tot real OUTPUT
) AS
SELECT @TotMRO = SUM(MRO), @TotPeakMWH = SUM(PotentialNetMWH)
FROM RevenuePeaks
WHERE Refnum = @Refnum AND PricingHub = @PricingHub
AND StartTime >= @StartPeriod AND EndTime <= @EndPeriod
SELECT @PeakMWHLost_P = SUM(CASE WHEN EVNT_Category = 'P' THEN LostMW*PeakHrs ELSE 0 END), 
@LRO_P = SUM(CASE WHEN EVNT_Category = 'P' THEN LRO ELSE 0 END),
@PeakMWHLost_M = SUM(CASE WHEN EVNT_Category = 'M' THEN LostMW*PeakHrs ELSE 0 END), 
@LRO_M = SUM(CASE WHEN EVNT_Category = 'M' THEN LRO ELSE 0 END),
@PeakMWHLost_F = SUM(CASE WHEN EVNT_Category = 'F' THEN LostMW*PeakHrs ELSE 0 END), 
@LRO_F = SUM(CASE WHEN EVNT_Category = 'F' THEN LRO ELSE 0 END)
FROM Events e INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_no = e.Evnt_no AND lro.Phase = e.Phase
WHERE e.Refnum = @Refnum AND lro.PricingHub = @PricingHub
AND Start_Time >= @StartPeriod AND End_Time <= @EndPeriod
IF @PeakMWHLost_P IS NULL SELECT @PeakMWHLost_P = 0
IF @LRO_P IS NULL SELECT @LRO_P = 0
IF @PeakMWHLost_M IS NULL SELECT @PeakMWHLost_M = 0
IF @LRO_M IS NULL SELECT @LRO_M = 0
IF @PeakMWHLost_F IS NULL SELECT @PeakMWHLost_F = 0
IF @LRO_F IS NULL SELECT @LRO_F = 0
SELECT @PeakMWHLost_Tot = @PeakMWHLost_P + @PeakMWHLost_M + @PeakMWHLost_F, 
@LRO_Tot = @LRO_P + @LRO_M + @LRO_F
