﻿CREATE PROC [dbo].[spAddOpexCalc] (@Refnum Refnum, @DataType char(5), @DivFactor real)
AS
DELETE FROM dbo.OpexCalc WHERE Refnum = @Refnum AND DataType = @DataType
IF ISNULL(@DivFactor, 0) <> 0 
	INSERT INTO dbo.OpExCalc (Refnum, DataType, OCCWages, MPSWages, STWages, OCCBenefits, MPSBenefits, STBenefits, 
		MaintMatl, LTSAAdj, ContMaintLabor, ContMaintMatl, ContMaintLumpSum, OthContSvc, OverhaulAdj, Envir, 
		PropTax, Insurance, AGPers, Supply, OthFixed, STFixed, PurGas, PurLiquid, PurSolid, FuelInven, OthVar, 
		STVar, TotCash, AGNonPers, Depreciation, Interest, STNonCash, GrandTot, OCCCompensation, MPSCompensation, 
		TotCompensation, STFuelCost, CentralOCCWagesBen, CentralMPSWagesBen, OthTax, TotCashLessFuel, ActCashLessFuel, 
		SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj, Chemicals, Water, STFuelCostPlusSO2, AllocSparesInven, 
		DivFactor, TotCashLessFuelEm)
	SELECT Refnum, @DataType, OCCWages/@DivFactor, MPSWages/@DivFactor, STWages/@DivFactor, OCCBenefits/@DivFactor, 
		MPSBenefits/@DivFactor, STBenefits/@DivFactor, MaintMatl/@DivFactor, LTSAAdj/@DivFactor, 
		ContMaintLabor/@DivFactor, ContMaintMatl/@DivFactor, ContMaintLumpSum/@DivFactor, OthContSvc/@DivFactor, 
		OverhaulAdj/@DivFactor, Envir/@DivFactor, PropTax/@DivFactor, Insurance/@DivFactor, AGPers/@DivFactor, 
		Supply/@DivFactor, OthFixed/@DivFactor, STFixed/@DivFactor, PurGas/@DivFactor, PurLiquid/@DivFactor, 
		PurSolid/@DivFactor, FuelInven/@DivFactor, OthVar/@DivFactor, STVar/@DivFactor, TotCash/@DivFactor, 
		AGNonPers/@DivFactor, Depreciation/@DivFactor, Interest/@DivFactor, STNonCash/@DivFactor, GrandTot/@DivFactor, 
		OCCCompensation/@DivFactor, MPSCompensation/@DivFactor, TotCompensation/@DivFactor, STFuelCost/@DivFactor, 
		CentralOCCWagesBen/@DivFactor, CentralMPSWagesBen/@DivFactor, OthTax/@DivFactor, TotCashLessFuel/@DivFactor, 
		ActCashLessFuel/@DivFactor, SO2EmissionAdj/@DivFactor, NOxEmissionAdj/@DivFactor, CO2EmissionAdj/@DivFactor, 
		Chemicals/@DivFactor, Water/@DivFactor, STFuelCostPlusSO2/@DivFactor, AllocSparesInven/@DivFactor, 
		DivFactor = @DivFactor, TotCashLessFuelEm/@DivFactor
	FROM dbo.OpexCalc WHERE Refnum = @Refnum AND DataType = 'ADJ'
