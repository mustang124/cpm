﻿CREATE PROCEDURE spGetColumnID
	@ObjectName sysname = NULL, 
	@ColumnName sysname,
	@ObjectID DD_ID = NULL,
	@ColumnID DD_ID OUTPUT
AS
IF @ObjectName IS NULL AND @ObjectID IS NULL 
	RETURN -101
ELSE BEGIN
	IF @ObjectName IS NOT NULL
		SELECT @ColumnID = ColumnID
		FROM DD_Objects o INNER JOIN DD_Columns c
		ON o.ObjectID = c.ObjectID
		WHERE o.ObjectName = @ObjectName AND c.ColumnName = @ColumnName
	ELSE
		SELECT @ColumnID = ColumnID
		FROM DD_Columns c
		WHERE c.ObjectID = @ObjectID AND c.ColumnName = @ColumnName
END
