﻿CREATE     PROCEDURE [dbo].[CalcPers] (@SiteID SiteID)
AS
SET NOCOUNT ON
DECLARE @OCCAbsPcnt real, @MPSAbsPcnt real
DECLARE @SiteOCCSTH real, @SiteMPSSTH real, @StudyYear smallint

SELECT @StudyYear = StudyYear FROM StudySites WHERE SiteID = @SiteID

-- Get unit refnums for this site and divisors used in the personnel calculations
DECLARE @Units TABLE (
	Refnum varchar(12) NOT NULL,
	MWH100K real NULL,
	MW100 real NULL,
	KSH real NULL)
INSERT @Units (Refnum, MWH100K, MW100, KSH)
SELECT t.Refnum, gtc.AdjNetMWH/100000, nf.NMC/100, nf.ServiceHrs/1000
FROM TSort t LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
WHERE t.SiteID = @SiteID

-- Calculate absence percentages for the site
DECLARE @OHOCCSTH real, @OHMPSSTH real
SELECT 	@OHOCCSTH = SUM(o.SiteOCCSTH), 
	@OHMPSSTH = SUM(o.SiteMPSSTH)
FROM OHMaint o
WHERE DATEPART(YY, o.OHDate) = @StudyYear
AND Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
SELECT 	@SiteOCCSTH = SUM(SiteSTH*CASE WHEN IncludeInAbsence = 'O' THEN 1 ELSE 0 END), 
@SiteMPSSTH = SUM(SiteSTH*CASE WHEN IncludeInAbsence = 'M' THEN 1 ELSE 0 END)
FROM PersSiteCalc p INNER JOIN Pers_LU l ON p.PersCat = l.PersCat
WHERE p.SiteID = @SiteID
SELECT 	@SiteOCCSTH = ISNULL(@SiteOCCSTH, 0) + ISNULL(@OHOCCSTH, 0),
	@SiteMPSSTH = ISNULL(@SiteMPSSTH, 0) + ISNULL(@OHMPSSTH, 0)
UPDATE SiteAbsences
SET OCCAbsPcnt = CASE WHEN @SiteOCCSTH > 0 THEN ISNULL(OCCAbs, 0)*100/@SiteOCCSTH ELSE NULL END,
	MPSAbsPcnt = CASE WHEN @SiteMPSSTH > 0 THEN ISNULL(MPSAbs, 0)*100/@SiteMPSSTH ELSE NULL END,
	TotAbsPcnt = CASE WHEN (@SiteOCCSTH + @SiteMPSSTH)> 0 THEN (ISNULL(OCCAbs, 0)+ISNULL(MPSAbs, 0))*100/(@SiteOCCSTH + @SiteMPSSTH) ELSE NULL END,
	TotAbs = ISNULL(OCCAbs, 0) + ISNULL(MPSAbs, 0)
WHERE SiteID = @SiteID
SELECT @OCCAbsPcnt = SUM(OCCAbsPcnt), @MPSAbsPcnt = SUM(MPSAbsPcnt)
FROM SiteAbsences WHERE SiteID = @SiteID
SELECT @OCCAbsPcnt = ISNULL(@OCCAbsPcnt, 0), @MPSAbsPcnt = ISNULL(@MPSAbsPcnt, 0)

/* Effective Personnel Calcs */
UPDATE Pers
SET SiteSTEffPers = ISNULL(SiteSTH, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
SiteOVTEffPers = ISNULL(SiteOVTHrs, 0)/1800,
CentralSTEffPers = ISNULL(CentralSTH, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
CentralOVTEffPers = ISNULL(CentralOVTHrs, 0)/1800,
AGOnSiteEffPers = ISNULL(AGOnSite, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
AGOthLocEffPers = ISNULL(AGOthLoc, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
ContractEffPers = ISNULL(Contract, 0)/1800,
SiteOVTPcnt = CASE WHEN SiteSTH > 0 THEN 100*SiteOVTHrs/SiteSTH ELSE 0 END,
SectionID = l.SectionID
FROM Pers INNER JOIN Pers_LU l ON l.PersCat = Pers.PersCat
WHERE Refnum IN (SELECT Refnum FROM @Units) AND Pers.PersCat NOT IN ('OCC-Maint%', 'MPS-Maint%')

UPDATE Pers
SET	SiteEffPers = SiteSTEffPers + SiteOVTEffPers, 
	CentralEffPers = CentralSTEffPers + CentralOVTEffPers, 
	AGEffPers = AGOnSiteEffPers + AGOthLocEffPers,
	TotEffPers = SiteSTEffPers + SiteOVTEffPers 
			+ CentralSTEffPers + CentralOVTEffPers 
			+ AGOnSiteEffPers + AGOthLocEffPers + ContractEffPers
WHERE Refnum IN (SELECT Refnum FROM @Units)

UPDATE Pers
SET	TotEffPersMWH = CASE WHEN u.MWH100K > 0 THEN TotEffPers/u.MWH100K ELSE NULL END,
	TotEffPersMW = CASE WHEN u.MW100 > 0 THEN TotEffPers/u.MW100 ELSE NULL END
FROM Pers INNER JOIN @Units u ON u.Refnum = Pers.Refnum

/* Sub-totals and Totals */
	/* Link SectionIDs to ST section IDs */
	DECLARE @tblSTSections TABLE (
		STSection char(4) NOT NULL,
		SectionID char(4) NOT NULL)
	-- Get all sections to roll up
	INSERT @tblSTSections (STSection, SectionID)
	SELECT DISTINCT SectionID, SectionID FROM Pers WHERE Refnum IN (SELECT Refnum FROM @Units) AND SectionID IS NOT NULL
	-- Load OCC and MPS ST sections
	INSERT @tblSTSections (STSection, SectionID)
	SELECT 'TOCC', SectionID FROM @tblSTSections WHERE SectionID IN ('OCCO', 'OCCM', 'OCCT', 'OCCA')
	INSERT @tblSTSections (STSection, SectionID)
	SELECT 'TMPS', SectionID FROM @tblSTSections WHERE SectionID IN ('MPSO', 'MPSM', 'MPST', 'MPSA')
	-- Load sections for total personnel
	INSERT @tblSTSections (STSection, SectionID)
	SELECT 'TP', SectionID FROM @tblSTSections WHERE STSection IN ('TOCC', 'TMPS')
	-- Calculate all sub-totals
DELETE FROM PersSTCalc WHERE Refnum IN (SELECT Refnum FROM @Units)
INSERT INTO PersSTCalc (Refnum, SectionID, SiteNumPers, SiteSTH, SiteOVTHrs,
	CentralNumPers, CentralSTH, CentralOVTHrs, 
	AGOnSite, AGOthLoc, Contract, 
	SiteSTEffPers, SiteOVTEffPers, SiteEffPers, 
	CentralSTEffPers, CentralOVTEffPers, CentralEffPers, 
	ContractEffPers, AGOnSiteEffPers, AGOthLocEffPers, AGEffPers, 
	TotEffPers, SiteOVTPcnt, TotEffPersMWH, TotEffPersMW)
SELECT p.Refnum, s.STSection, SUM(ISNULL(SiteNumPers,0)), SUM(ISNULL(SiteSTH,0)), SUM(ISNULL(SiteOVTHrs,0)),
	SUM(ISNULL(CentralNumPers,0)), SUM(ISNULL(CentralSTH,0)), SUM(ISNULL(CentralOVTHrs,0)),
	SUM(ISNULL(AGOnSite,0)), SUM(ISNULL(AGOthLoc,0)), SUM(ISNULL(Contract,0)),
	SUM(ISNULL(SiteSTEffPers,0)), SUM(ISNULL(SiteOVTEffPers,0)), SUM(ISNULL(SiteEffPers,0)),
	SUM(ISNULL(CentralSTEffPers,0)), SUM(ISNULL(CentralOVTEffPers,0)), SUM(ISNULL(CentralEffPers,0)),
	SUM(ISNULL(ContractEffPers,0)), SUM(ISNULL(AGOnSiteEffPers,0)), SUM(ISNULL(AGOthLocEffPers,0)), SUM(ISNULL(AGEffPers,0)),
	SUM(ISNULL(TotEffPers,0)), CASE WHEN SUM(SiteSTH) > 0 THEN 100*SUM(SiteOVTHrs)/SUM(SiteSTH) ELSE NULL END, 
	SUM(TotEffPersMWH), SUM(TotEffPersMW)
FROM Pers p INNER JOIN @tblSTSections s ON s.SectionID = p.SectionID
WHERE p.Refnum In (SELECT Refnum FROM @Units) AND p.PersCat NOT IN ('OCC-Maint%', 'MPS-Maint%')
GROUP BY p.Refnum, s.STSection

UPDATE PersSTCalc
SET CompNumPers = ISNULL(SiteNumPers, 0) + ISNULL(CentralNumPers, 0), 
	CompSTH = ISNULL(SiteSTH, 0) + ISNULL(CentralSTH, 0), 
	CompSTEffPers = ISNULL(SiteSTEffPers, 0) + ISNULL(CentralSTEffPers, 0), 
	CompOVTEffPers = ISNULL(SiteOVTEffPers, 0) + ISNULL(CentralOVTEffPers, 0), 
	CompEffPers = ISNULL(SiteEffPers, 0) + ISNULL(CentralEffPers, 0),
	CompOVTPcnt = CASE WHEN (ISNULL(SiteSTH, 0) + ISNULL(CentralSTH, 0)) > 0 THEN 100*(ISNULL(SiteOVTHrs, 0) + ISNULL(CentralOVTHrs, 0))/(ISNULL(SiteSTH, 0) + ISNULL(CentralSTH, 0)) END
WHERE Refnum IN (SELECT Refnum FROM @Units)
