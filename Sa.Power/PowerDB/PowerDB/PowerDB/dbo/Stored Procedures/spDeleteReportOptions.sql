﻿CREATE PROCEDURE spDeleteReportOptions 
	@ReportSetID DD_ID = NULL, @ReportSetName varchar(30) = NULL
AS
DECLARE @Check integer, @CurrUser varchar(30), @ErrMsg varchar(255)
SELECT @CurrUser = USER_NAME()
IF @ReportSetID IS NOT NULL 
BEGIN
	EXEC @Check = spCheckReportSetOwner @ReportSetID, @ReportSetName, @CurrUser
	IF @Check >= 0
		DELETE FROM ReportOptions WHERE ReportSetID = @ReportSetID
	ELSE BEGIN
		SELECT @ErrMsg = 'Insufficient rights to delete ReportOptions for ReportSet ' +
				CONVERT(varchar(20), @ReportSetID)
		RAISERROR(@ErrMsg, 11, -1)
	END
END
ELSE
BEGIN
	IF @ReportSetName IS NOT NULL 
	BEGIN
		EXEC @Check = spCheckReportSetOwner @ReportSetID, @ReportSetName, @CurrUser
		IF @Check >= 0
			DELETE FROM ReportOptions 
			WHERE ReportSetID = 
				(SELECT ReportSetID FROM ReportSets
				WHERE ReportSetName = @ReportSetName)
		ELSE BEGIN
			SELECT @ErrMsg = 'Insufficient rights to delete ReportOptions for ' +
				 @ReportSetName
			RAISERROR(@ErrMsg, 11, -1)
		END
	END
END
