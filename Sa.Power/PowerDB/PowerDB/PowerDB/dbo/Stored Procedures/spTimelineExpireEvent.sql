﻿CREATE PROCEDURE [dbo].[spTimelineExpireEvent]

AS
BEGIN

--	DECLARE @LogIDNum INT
	DECLARE @CalcIDNum INT
	--print 'select log'
	--SELECT @LogIDNum = (MAX(IDNum) + 1) FROM TimelineLog 

	SELECT @CalcIDNum = MAX(IDNum) FROM TimelineCalc 
	--print 'if'
	IF @CalcIDNum > 0
	BEGIN
		INSERT INTO TimelineLog
			(--[IDNum]
			[UtilityUnitCode]
			,[Evnt_Year]
			,[Evnt_No]
			,[Evnt_Type]
			,[Evnt_Group]
			,[Phase]
			,[StartTime]
			,[EndTime]
			,[AvailCap]
			,[LostMW]
			,[Duration])
		           
		SELECT --@LogIDNum
			[UtilityUnitCode]
			,[Evnt_Year]
			,[Evnt_No]
			,[Evnt_Type]
			,[Evnt_Group]
			,[Phase]
			,[StartTime]
			,[EndTime]
			,[AvailCap]
			,[LostMW]
			,Duration = CAST(DATEDIFF(MINUTE, StartTime, EndTime) AS FLOAT)/60
		FROM TimelineCalc
			WHERE IDNum = @CalcIDNum

		DELETE FROM TimelineCalc
			WHERE IDNum = @CalcIDNum
	END

END
