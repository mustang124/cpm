﻿--EXEC SPEGCCALCS '0AF100P06'

CREATE        PROC [dbo].[spEGCCalcsPRD] (@Refnum Refnum)
AS
-- Updated 6/2006 by FRS to new formulas.
-- Builds records for 2004 method and Default method and Carryover method
DECLARE @EGC_2004 real, @EGC_2005 real, @EGC real
DECLARE @Boilers tinyint, @STGs tinyint, @CTGs tinyint, @Scrubbers tinyint, @Prec tinyint, @Bag tinyint
-- xxxYN vars will be 0 if count is 0, 1 if count is > 0.
DECLARE @ScrubbersYN tinyint, @PrecYN tinyint, @PrecBagYN tinyint
DECLARE @CoalNDC real, @OilNDC real, @GasNDC real, 
	@STG_NDC real, @CTG_NDC real, @NDC real, @NOF real, @ServiceHrs real, @BlrPSIG real, @Age real,
	@AdjNetMWH real, @Sulfur real, @AshPcnt real, @FuelType varchar(8), @LoadType varchar(4)
DECLARE @OilMBTU real, @GasMBTU real, @NumUnitsAtSite real, @ElecGenStarts real
DECLARE @CoalMBTU real, @CoalPcnt real
DECLARE @CoalSulfur real, @CoalAsh real, @OilSulfur real, @OilAsh real
DECLARE @CombinedCycle char, @FuelGroup char(9), @SteamGasOil char
DECLARE @Technology char(10)

SELECT @CombinedCycle = CombinedCycle, 
	@FuelGroup = FuelGroup, 
	@SteamGasOil = SteamGasOil
	FROM Breaks WHERE Refnum = @Refnum

SELECT @Boilers = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'BOIL'
SELECT @Scrubbers = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType IN ('DGS', 'WGS')
IF @Scrubbers > 0 
	SELECT @ScrubbersYN = 1
ELSE 
	SELECT @ScrubbersYN = 0
SELECT @Prec = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'PREC'
IF @Prec > 0 
	SELECT @PrecYN = 1
ELSE 
	SELECT @PrecYN = 0
SELECT @Bag = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'BAG'
IF @Prec > 0 OR @Bag > 0
	SELECT @PrecBagYN = 1
ELSE 
	SELECT @PrecBagYN = 0
SELECT @STGs = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'STG'
--SELECT @CTGs = COUNT(*) FROM CTGData WHERE Refnum = @Refnum
SELECT @CTGs = COUNT(*), @CTG_NDC = SUM(NDC) FROM NERCTurbine WHERE Refnum = @Refnum AND TurbineType = 'CTG'
SELECT @STGs = COUNT(*), @STG_NDC = SUM(NDC) FROM NERCTurbine WHERE Refnum = @Refnum AND TurbineType IN ('STG', 'CST')
SELECT @BlrPSIG = AVG(BlrPSIG) 
	FROM DesignData d 
	INNER JOIN NERCTurbine NT ON nt.UtilityUnitCode = d.UtilityUnitCode and nt.refnum =@Refnum
SELECT @NDC = NDC, @NOF = NOF, @ServiceHrs = ServiceHrs, @Age = Age 
FROM NERCFactors WHERE Refnum = @Refnum
IF @Scrubbers > @Boilers
	SELECT @Scrubbers = @Boilers
IF @Prec > @Boilers 
	SELECT @Prec = @Boilers
IF @Bag > @Boilers 
	SELECT @Bag = @Boilers

-- From 8/22/06 to 11/7/06, had 'STG' included in the "IN" clause at the end 
-- of this Q. Was trying to fix problem with Sask, the problem turned
-- out to be something else.
-- FRS changed on 10/19/06 to use GadsSetup instead of DesignData
SELECT @CoalNDC = SUM(CASE WHEN s.PriFuelCode IN ('CC','LI','PC') THEN NDC ELSE 0 END) ,
@OilNDC = SUM(CASE WHEN s.PriFuelCode IN ('OO','DI') THEN NDC ELSE 0 END),
@GasNDC = SUM(CASE WHEN s.PriFuelCode IN ('GG','OG') THEN NDC ELSE 0 END)
	FROM NERCTurbine n 
	LEFT JOIN GadsSetup s ON s.UtilityUnitCode = n.UtilityUnitCode
	WHERE s.PriFuelCode Is NOT NULL AND n.TurbineType IN ('BLR') AND n.Refnum = @Refnum

-- FRS 5/21/2007 Temporary to get SSE Peterhead 2 to calc as Multi.
IF @Refnum = '565CC05' 
    BEGIN
	Select @OilNDC = 214
	Select @GasNDC = 428
    END

SELECT @AdjNetMWH = AdjNetMWH FROM Gensum WHERE Refnum = @Refnum
SELECT @FuelType = FuelType FROM FuelTotCalc WHERE Refnum = @Refnum
SELECT @LoadType = LoadType FROM Breaks WHERE Refnum = @Refnum
SELECT @OilMBTU = SUM(MBTU) FROM Fuel 
WHERE Refnum = @Refnum AND FuelType IN ('Jet', 'FOil', 'Diesel') AND TurbineID IN ('STG','Site')
IF @OilMBTU IS NULL 
	SELECT @OilMBTU = 0
SELECT @GasMBTU = SUM(MBTU) FROM Fuel 
WHERE Refnum = @Refnum AND FuelType IN ('NatGas', 'OffGas', 'H2Gas') AND TurbineID IN ('STG','Site')
IF @GasMBTU IS NULL 
	SELECT @GasMBTU = 0

-- CoalMBTU and CoalPcnt added for 2005
SELECT @CoalMBTU = SUM(MBTU) FROM Coal
WHERE Refnum = @Refnum

SELECT @CTG_NDC = 0 WHERE @CTG_NDC IS NULL
SELECT @STG_NDC = 0 WHERE @STG_NDC IS NULL

IF @CoalNDC IS NULL AND @OilNDC IS NULL AND @GasNDC IS NULL AND @CTGs = 0
BEGIN
	SELECT @CoalNDC = @NDC-@CTG_NDC WHERE @FuelType = 'Coal'
	SELECT @OilNDC = @NDC-@CTG_NDC WHERE @FuelType = 'Oil'
	SELECT @GasNDC = @NDC-@CTG_NDC WHERE @FuelType = 'Gas'
END
SELECT @CoalNDC = 0 WHERE @CoalNDC IS NULL
SELECT @OilNDC = 0 WHERE @OilNDC IS NULL
SELECT @GasNDC = 0 WHERE @GasNDC IS NULL

SELECT @NumUnitsAtSite = TotExclSCNum from UnitGenData WHERE Refnum = @Refnum
SELECT @ElecGenStarts = ISNULL(SUM(TotalOpps),0)
FROM StartsAnalysis sa INNER JOIN NERCTurbine nt ON nt.Refnum = sa.Refnum AND nt.TurbineID = sa.TurbineID
WHERE nt.TurbineType IN ('CTG', 'STG', 'CST') AND sa.Refnum = @Refnum

SELECT @CoalSulfur = ISNULL(SulfurPcnt,0), @CoalAsh = ISNULL(AshPcnt,0) FROM CoalTotCalc
WHERE Refnum = @Refnum
SELECT @OilSulfur = Sulfur, @OilAsh = 0.1 FROM FuelProperties WHERE Refnum = @Refnum
IF @OilSulfur IS NULL
	SELECT @OilSulfur = 2.3, @OilAsh = 0.1 
SELECT @Sulfur = (ISNULL(@CoalSulfur,0)*@CoalNDC + ISNULL(@OilSulfur,0)*@OilNDC)/@NDC
SELECT @AshPcnt = (ISNULL(@CoalAsh,0)*@CoalNDC + ISNULL(@OilAsh,0)*@OilNDC)/@NDC

-- GasPcnt added for 2005
DECLARE @OilPcnt real, @GasPcnt real
IF (@OilMBTU + @GasMBTU) > 0
	SELECT @OilPcnt = @OilMBTU/(@OilMBTU + @GasMBTU)
ELSE
	SELECT @OilPcnt = 0
IF @OilPcnt < 0.1
	SELECT @OilPcnt = 0
IF @OilPcnt > 0.9
	SELECT @OilPcnt = 1

IF (@OilMBTU + @GasMBTU) > 0
	SELECT @GasPcnt = @GasMBTU/(@OilMBTU + @GasMBTU)
ELSE
	SELECT @GasPcnt = 0
IF @GasPcnt < 0.1
	SELECT @GasPcnt = 0
IF @GasPcnt > 0.9
	SELECT @GasPcnt = 1

IF @CoalMBTU > 0
	SELECT @CoalPcnt = @CoalMBTU/(@CoalMBTU + @OilMBTU + @GasMBTU)
ELSE
	SELECT @CoalPcnt = 0
IF @CoalPcnt < 0.1
	SELECT @CoalPcnt = 0
IF @CoalPcnt > 0.9
	SELECT @CoalPcnt = 1

-- Calculation of all of the items needed should now be done.
-- Load up the EGC variables.
---This is for 2004

SELECT @EGC_2004 = 1488*POWER(@Scrubbers*(@AdjNetMWH/1000000), 0.24) + 604*(@Prec+@Bag) 
		+ 1123*POWER(@AdjNetMWH/1000000, 0.6)
		- 75*ISNULL(@NumUnitsAtSite, 0)
		+ 1*ISNULL(@ElecGenStarts, 0)
		+ 571*POWER(@AdjNetMWH/1000000*@Sulfur, 0.96)
		+ 0.00142*POWER(@AdjNetMWH/1000000*ISNULL(@AshPcnt, 0), 3.52)
		+ 208.7*POWER(@CoalNDC, 0.68)
		+ 10.7*POWER(@OilPcnt*(@OilNDC+@GasNDC), 1.11)
		+ 69.9*POWER((1-@OilPcnt)*(@OilNDC+@GasNDC), 0.68)
IF @CTGs > 0
	SELECT @EGC_2004 = @EGC_2004 + 48.5*POWER(@CTGs, 3.12)
		+ 215.7*POWER(@CTG_NDC, 0.71)
		
-- Delete all of the EGCCalc records that now exist for this refnum
DELETE FROM EGCCalc WHERE Refnum = @Refnum
-- Build the 2004 
INSERT INTO EGCCalc (Refnum, Method, EGC, STGs, STG_NDC, CTGs, CTG_NDC, NDC, Scrubbers, PrecBag, AdjNetMWH, Sulfur, AshPcnt, FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt, GasPcnt, CoalPcnt, Age, NumUnitsAtSite, ScrubbersYN, PrecYN, PrecBagYN)
VALUES (@Refnum, '2004', @EGC_2004, @STGs, @STG_NDC, @CTGs, @CTG_NDC, @NDC, @Scrubbers, @Prec+@Bag, @AdjNetMWH, @Sulfur, @AshPcnt, @FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt, @GasPcnt, @CoalPcnt, @Age, @NumUnitsAtSite, @ScrubbersYN, @PrecYN, @PrecBagYN)
---- 2005 EGC
-- Default 'Carryover' Calc
If RIGHT(@Refnum,1) = 'P'
	SELECT @EGC_2005 = 
		0 * Power(@CTGs,3.0) -- Placeholder, always zero for this formula
		+ 603 * Power(@ScrubbersYN,0.9999)
		+ 5250 * Power(@PrecBagYN,1.0)
		+ 29.2 * Power(@CoalNDC,0.994)
		+ 4.34 * Power(@OilPcnt * (@GasNDC + @OilNDC),1.3)
		+ 13.2 * @GasPcnt * (@GasNDC + @OilNDC)
		+ 335.8 * Power(@CTG_NDC,0.69)
		+ 677.4 * Power(@AdjNetMWH/1000000,.9999)
		- 651.67 * @NumUnitsAtSite
		+ 1.22 * @ElecGenStarts
		+ 1.73 * Power((@AdjNetMWH/1000000) * @CoalSulfur,1.984)
		+ .00046 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.6)
		+ 87.6 * @Age,
		@Technology = 'Carryover'

-- FRS 10/19/06 Added gas-ctg and coal-ctg and oil-ctg
ELSE IF ((@CoalNDC > 0 and @OilNDC > 0) 
	OR (@CoalNDC > 0 and @GasNDC > 0) 
	OR (@CoalNDC > 0 and @CTG_NDC > 0)
	OR (@OilNDC > 0 and @CTG_NDC > 0)
	OR (@GasNDC > 0 and @OilNDC > 0) 
	OR (@GasNDC > 0 and @CTG_NDC >0))
	-- Multi Fuel Site
	SELECT @EGC_2005 = 
		29.3 * Power(@CTGs,3.0)
		+ 804 * (@ScrubbersYN * (@AdjNetMWH/1000000) * @CoalPcnt)
		+ 841 * @PrecBagYN 
		+ 81.8 * Power(@CoalNDC,0.86)
		+ 79.98 * Power(@OilPcnt * (@GasNDC + @OilNDC),0.84)
		+ 15.8 * @GasPcnt * (@GasNDC + @OilNDC)
		+ 542.1 * Power(@CTG_NDC,0.57)
		+ 321 * (@AdjNetMWH/1000000)
		- 75 * @NumUnitsAtSite
		+ 1 * @ElecGenStarts
		+ 34 * Power((@AdjNetMWH/1000000) * @CoalSulfur,1.51)
		+ .00097 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.6)
		+ (0* 270 * @Age),
		@Technology = 'Multi'
ELSE IF @CombinedCycle = 'Y' 
	BEGIN
	IF @STGs > 0 
		-- Combined Cycle 
		SELECT @EGC_2005 = 
			72.11 * Power(@CTGs,2.4)
			+ 123.9 * Power(@CTG_NDC,0.8)
			+ 2248 * Power((@AdjNetMWH/1000000),0.59)
			- (75 * @NumUnitsAtSite)
			+ (1 * @ElecGenStarts),
			@Technology = 'CC'
	ELSE
		-- CoGen with no steam turbine
		SELECT @EGC_2005 = 
			--0 * Power(@CTGs,3.0)
			--+ 0 * (@ScrubbersYN * (@AdjNetMWH/1000000) * @CoalPcnt)
			--+ 800 * @PrecBagYN 
			--+ 0 * Power(@CoalNDC,0.78)
			--+ 0 * Power(@OilPcnt * (@GasNDC + @OilNDC),1.3)
			--+ 0 * @GasPcnt * (@GasNDC + @OilNDC)
			 166.8 * Power(@CTG_NDC,0.53)
			+ 2716 * Power((@AdjNetMWH/1000000),.88)
			--- 0 * @NumUnitsAtSite
			+ 5.5 * @ElecGenStarts
			--+ 0 * Power((@AdjNetMWH/1000000) * @CoalSulfur,1.15)
			--+ 0 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.71)
			--+ 0 * Power(@Age,.50),
			,
			@Technology = 'CGNT'
	END	

ELSE IF @SteamGasOil = 'Y' 
	SELECT @EGC_2005 = 
		800 * (@PrecYN) 
		+ 1.294 * Power(@OilPcnt * @NDC, 1.3)
		+ Power(@GasPcnt * @NDC, 0.68)
		+ 8624 * Power(@AdjNetMWH/1000000, 0.44)
		- 208 * @NumUnitsAtSite
		+ 1 * @ElecGenStarts,
		@Technology = 'SGO'

ELSE IF @FuelGroup = 'Coal' 
	SELECT @EGC_2005 = 
		1750 * Power(@ScrubbersYN * (@AdjNetMWH/1000000) * @CoalPcnt,0.44)
		+ 2412 * @PrecBagYN
		+ 145 * Power(@NDC,0.78)
		- 726 * @NumUnitsAtSite
		+ 1 * @ElecGenStarts
		+ 41 * Power((@AdjNetMWH/1000000) * @CoalSulfur,1.46)
		+ .001 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.71)
		+ 20.3 * Power(@Age,1.0),
		@Technology = 'Coal'

ELSE BEGIN	
	-- If we got here, we could not determine what type of unit it is.
	DECLARE @msg1 varchar(255)
	DECLARE @msg2 varchar(255)
	SELECT @msg1 = ' Coal NDC = ' 
		+ CAST(@CoalNDC AS VARCHAR(5)) 
		+ ' Oil NDC = ' 
		+ CAST(@OilNDC AS VARCHAR(5)) 
		+ ' Gas NDC = ' 
		+ CAST(@GasNDC AS VARCHAR(5))
	SELECT	@msg2 =	
		'CC = ' + ISNULL(@CombinedCycle,' ') 
		+ ' SGO = ' + ISNULL(@SteamGasOil,' ') 
		+ ' FuelGroup = ' + ISNULL(@FuelGroup,' ')
	EXEC sp_InsertMessage @Refnum, 112, 'EGCCalcs', @msg1, @msg2
END

-- Build the "2005" EGCCalc record.
INSERT INTO EGCCalc (Refnum, Method, Technology, EGC, STGs, STG_NDC, CTGs, CTG_NDC, NDC, Scrubbers, PrecBag, AdjNetMWH, Sulfur, AshPcnt, FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt, GasPcnt, CoalPcnt, Age, NumUnitsAtSite, ScrubbersYN, PrecYN, PrecBagYN)
VALUES (@Refnum, '2005', @Technology, @EGC_2005, @STGs, @STG_NDC, @CTGs, @CTG_NDC, @NDC, @Scrubbers, @Prec+@Bag, @AdjNetMWH, @Sulfur, @AshPcnt, @FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt, @GasPcnt, @CoalPcnt, @Age, @NumUnitsAtSite, @ScrubbersYN, @PrecYN, @PrecBagYN)
-------------******************************start 2006 here
--This is the one that will be used.  "Default"
---starting with 2006, using certain fields from TSORT that is design related and once defined, should not change
SELECT @CTGs = CTGs from TSort WHERE Refnum = @Refnum
SELECT @PrecBagYN = PrecBagYN from TSort WHERE Refnum = @Refnum
SELECT @ScrubbersYN = ScrubbersYN from TSort WHERE Refnum = @Refnum
SELECT @CoalNDC = Coal_NDC from TSort WHERE Refnum = @Refnum
SELECT @GasNDC = Gas_NDC from TSort WHERE Refnum = @Refnum
SELECT @CTG_NDC = CTG_NDC from TSort WHERE Refnum = @Refnum
SELECT @OilNDC = Oil_NDC from TSort WHERE Refnum = @Refnum
SELECT @NDC = NDC from TSort WHERE Refnum = @Refnum
SELECT @NumUnitsAtSite = NumUnitsAtSite from TSort WHERE Refnum = @Refnum




-- Default 'Carryover' Calc
If RIGHT(@Refnum,1) = 'P'
	SELECT @EGC = 
		0 * Power(@CTGs,1.99) -- Placeholder, always zero for this formula
		+ 0 * Power(@ScrubbersYN,1.0)
		+ 2885 * Power(@PrecBagYN,1.0)
		+ 192.379 * Power(@CoalNDC,0.76)
		+ 2041.94 * Power(@OilPcnt * (@GasNDC + @OilNDC),0.29)
		+ 53.57 * Power((@GasPcnt * (@GasNDC + @OilNDC)),0.84)
		+ 1263.70 * Power(@CTG_NDC,0.41)
		+ 0 * Power(@AdjNetMWH/1000000,0.42)
		- 75.0 * @NumUnitsAtSite
		+ 1.0 * @ElecGenStarts
		+ 0 * Power((@AdjNetMWH/1000000) * @CoalSulfur,0.9)
		+ .00010 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.6)
		+ 0 * @Age,
		@Technology = 'Carryover'

-- FRS 10/19/06 Added gas-ctg and coal-ctg and oil-ctg
ELSE IF ((@CoalNDC > 0 and @OilNDC > 0) 
	OR (@CoalNDC > 0 and @GasNDC > 0) 
	OR (@CoalNDC > 0 and @CTG_NDC > 0)
	OR (@OilNDC > 0 and @CTG_NDC > 0)
	OR (@GasNDC > 0 and @OilNDC > 0) 
	OR (@GasNDC > 0 and @CTG_NDC >0))
	-- Multi Fuel Site
	SELECT @EGC = 
		6 * Power(@CTGs,3.15)
		+ 535 * Power((@ScrubbersYN * (@AdjNetMWH/1000000) * @CoalPcnt),0.98)
		+ 800 * @PrecBagYN 
		+ 14.046 * Power(@CoalNDC,1.0)
		+ 2.30 * Power(@OilPcnt * (@GasNDC + @OilNDC),1.30)
		+ 7.11 * @GasPcnt * (@GasNDC + @OilNDC)
		+ 34.29 * Power(@CTG_NDC,0.94)
		+ 6640.40 * Power((@AdjNetMWH/1000000),0.47)
		- 155.30 * @NumUnitsAtSite
		+ 4.91 * @ElecGenStarts
		+ 382.978 * Power((@AdjNetMWH/1000000) * @CoalSulfur,0.95)
		+ .00100 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.68)
		+ 0 * @Age,
		@Technology = 'Multi'
ELSE IF @CombinedCycle = 'Y' 
	BEGIN
	IF @STGs > 0 
		-- Combined Cycle 
		SELECT @EGC = 
			157 * Power(@CTGs,1.71)
			+ 7792 * @PrecBagYN 
			+ 88.896 * Power(@CoalNDC,.48)
			+ 150.68 * Power(@CTG_NDC,0.78)
			+ 1867.14 * Power((@AdjNetMWH/1000000),0.57)
			- (75 * @NumUnitsAtSite)
			+ (1 * @ElecGenStarts)
			+ 15551.753 * Power((@AdjNetMWH/1000000) * @CoalSulfur,1.72),
			@Technology = 'CC'
	ELSE
		-- CoGen with no steam turbine
		SELECT @EGC = 
			--0 * Power(@CTGs,3.0)
			--+ 0 * (@ScrubbersYN * (@AdjNetMWH/1000000) * @CoalPcnt)
			--+ 800 * @PrecBagYN 
			--+ 0 * Power(@CoalNDC,0.78)
			--+ 0 * Power(@OilPcnt * (@GasNDC + @OilNDC),1.3)
			--+ 0 * @GasPcnt * (@GasNDC + @OilNDC)
			 166.8 * Power(@CTG_NDC,0.53)
			+ 2716 * Power((@AdjNetMWH/1000000),.88)
			--- 0 * @NumUnitsAtSite
			+ 5.5 * @ElecGenStarts
			--+ 0 * Power((@AdjNetMWH/1000000) * @CoalSulfur,1.15)
			--+ 0 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.71)
			--+ 0 * Power(@Age,.50),
			,
			@Technology = 'CGNT'
	END	

ELSE IF @SteamGasOil = 'Y' 
	SELECT @EGC = 
		800 * (@PrecYN) 
		+ 1.67 * Power(@OilPcnt * @NDC, 1.3)
		+ 2.19 * Power(@GasPcnt * @NDC, 0.76)
		+ 9357.28 * Power(@AdjNetMWH/1000000, 0.48)
		- 351.32 * @NumUnitsAtSite
		+ 12.13 * @ElecGenStarts,
		@Technology = 'SGO'

ELSE IF @FuelGroup = 'Coal' 
	SELECT @EGC = 
		627 * Power(@ScrubbersYN * (@AdjNetMWH/1000000) * @CoalPcnt,1.0)
		+ 910 * @PrecBagYN
		+ 238.9 * Power(@NDC,0.69)
		+ 1889.12 * Power((@AdjNetMWH/1000000),0.52)
		- 572.57 * @NumUnitsAtSite
		+ 2.55 * @ElecGenStarts
		+ 10.216 * Power((@AdjNetMWH/1000000) * @CoalSulfur,1.89)
		+ .001 * Power((@AdjNetMWH/1000000) * @AshPcnt,3.69)
		+ 2.67 * Power(@Age,1.0),
		@Technology = 'Coal'

ELSE BEGIN	
	-- If we got here, we could not determine what type of unit it is.
	SELECT @msg1 = ' Coal NDC = ' 
		+ CAST(@CoalNDC AS VARCHAR(5)) 
		+ ' Oil NDC = ' 
		+ CAST(@OilNDC AS VARCHAR(5)) 
		+ ' Gas NDC = ' 
		+ CAST(@GasNDC AS VARCHAR(5))
	SELECT	@msg2 =	
		'CC = ' + ISNULL(@CombinedCycle,' ') 
		+ ' SGO = ' + ISNULL(@SteamGasOil,' ') 
		+ ' FuelGroup = ' + ISNULL(@FuelGroup,' ')
	EXEC sp_InsertMessage @Refnum, 112, 'EGCCalcs', @msg1, @msg2
END

-- Build the "REAL" EGCCalc record. The one that will be used.
INSERT INTO EGCCalc (Refnum, Method, Technology, EGC, STGs, STG_NDC, CTGs, CTG_NDC, NDC, Scrubbers, PrecBag, AdjNetMWH, Sulfur, AshPcnt, FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt, GasPcnt, CoalPcnt, Age, NumUnitsAtSite, ScrubbersYN, PrecYN, PrecBagYN)
VALUES (@Refnum, 'Default', @Technology, @EGC, @STGs, @STG_NDC, @CTGs, @CTG_NDC, @NDC, @Scrubbers, @Prec+@Bag, @AdjNetMWH, @Sulfur, @AshPcnt, @FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt, @GasPcnt, @CoalPcnt, @Age, @NumUnitsAtSite, @ScrubbersYN, @PrecYN, @PrecBagYN)

-------------******************************end 2006 here
-- Go update all of the EGC fields spread thoughout the database.
UPDATE GenerationTotCalc SET EGC = @EGC WHERE Refnum = @Refnum
UPDATE Gensum SET EGC = @EGC WHERE Refnum = @Refnum

UPDATE NonOHMaint SET AnnNonOHCostEGC = CASE WHEN @EGC > 0 THEN AnnNonOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE OHEquipCalc SET TotAnnOHCostEGC = CASE WHEN @EGC > 0 THEN TotAnnOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE MaintEquipCalc SET AnnOHCostEGC = CASE WHEN @EGC > 0 THEN AnnOHCostKUS/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintEquipCalc SET AnnNonOHCostEGC = CASE WHEN @EGC > 0 THEN AnnNonOHCostKUS/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintEquipCalc SET AnnMaintCostEGC = CASE WHEN @EGC > 0 THEN AnnMaintCostKUS/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE MaintTotCalc SET AnnNonOHCostEGC = CASE WHEN @EGC > 0 THEN AnnNonOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnOHCostEGC = CASE WHEN @EGC > 0 THEN AnnOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnMaintCostEGC = CASE WHEN @EGC > 0 THEN AnnMaintCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnOHProjCostEGC = CASE WHEN @EGC > 0 THEN AnnOHProjCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnOHExclProjEGC = CASE WHEN @EGC > 0 THEN AnnOHExclProj/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnMaintExclProjEGC = CASE WHEN @EGC > 0 THEN AnnMaintExclProj/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnLTSACostEGC = CASE WHEN @EGC > 0 THEN AnnLTSACost/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE Pers SET TotEffPersEGC = CASE WHEN @EGC > 0 THEN TotEffPers*1000/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE PersSTCalc SET TotEffPersEGC = CASE WHEN @EGC > 0 THEN TotEffPers*1000/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE GenSum SET TotEffPersEGC = (SELECT TotEffPersEGC FROM PersSTCalc WHERE PersSTCalc.Refnum = Gensum.Refnum AND PersSTCalc.SectionID = 'TP') WHERE Refnum = @Refnum

EXEC spAddOpexCalc @Refnum = @Refnum, @DataType = 'EGC', @DivFactor = @EGC
