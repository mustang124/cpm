﻿CREATE PROCEDURE spDDLookups;3
	@ObjName sysname ,
	@ColName sysname ,
	@DescField sysname ,
	@Desc varchar(100) = NULL ,
	@LookupID DD_ID OUTPUT
AS
IF NOT EXISTS (SELECT * FROM DD_Lookups 
	WHERE ObjectName = @ObjName)
	INSERT INTO DD_Lookups (ObjectName, ColumnName, Description, DescField)
	VALUES (@ObjName, @ColName, @Desc, @DescField)
EXEC spDDLookups;1 @ObjName, @LookupID OUTPUT
