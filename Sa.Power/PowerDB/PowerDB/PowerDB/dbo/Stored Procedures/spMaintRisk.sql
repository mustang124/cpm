﻿CREATE PROCEDURE [dbo].[spMaintRisk] @CompanyID char(12), @StartYear int = 0, @EndYear int = 0
AS
BEGIN

--@CompanyID passed to procedure should be the name of the company matching what is in Tsort

--user can pass 0 as start and end to get all years, or specific start and end dates
IF @StartYear = 0 
	SET @StartYear = 1900

IF @EndYear = 0 
	SET @EndYear = 2500

IF @EndYear < @StartYear 
	SET @EndYear = @StartYear 


--temporary output table
CREATE TABLE #tmp (Type varchar(4), Site varchar(75), Unit varchar(50), Year int, Component varchar(75), 
	Name varchar(75), EUFPct float(10), EUOFPct float(10), MaintIndex float(10), EPH float(10))

--start by gathering all the individual component rows for each unit
INSERT #tmp
SELECT * FROM (
SELECT Type = 'Unit', 
	Site = slu.SiteName, 
	Unit = t.unitname, 
	Year = t.StudyYear, 
	Component = cplu.Component,
	Name = t.UnitName + ' ' + CAST(t.StudyYear AS VARCHAR) + ' ' + cplu.Component,
	EUFPct = SUM(cep.EUF),
	EUOFPct = SUM(cep.EUOF),
	MaintIndex = SUM(mec.AnnMaintCostMWH),
	EPH = MAX(cep.E_PH)
 FROM TSort t
	LEFT JOIN PowerWork.dbo.ComponentEUFPlus cep ON cep.refnum = t.Refnum AND cep.EUF > 0
	INNER JOIN Site_LU slu ON slu.CompanyID = @CompanyID AND CAST(slu.SiteNo AS VARCHAR) = CAST(LEFT(t.SiteID, 3) AS VARCHAR)
	INNER JOIN PowerGlobal.dbo.ComponentEUFPlus_LU cplu ON cplu.equipgroup = cep.EquipGroup 
	LEFT JOIN MaintEquipCalc mec ON mec.Refnum = t.Refnum AND mec.EquipGroup = cplu.equipgroup 
WHERE t.Refnum IN (SELECT Refnum FROM TSort WHERE CompanyID = @CompanyID AND StudyYear >= @StartYear AND StudyYear <= @EndYear AND Refnum NOT LIKE '%P')
GROUP BY slu.SiteName, t.UnitName, t.StudyYear, cplu.component) b
WHERE MaintIndex IS NOT NULL --for components we drop anyone that doesn't have EUF or MaintIndex 



--next we move onto site component totals
INSERT #tmp
SELECT Type = 'Site',
	Site,
	Unit = 'TOTAL',
	Year,
	Component,
	Name = Site + ' ' + CAST(Year AS VARCHAR) + ' ' + Component,
	EUFPct = GlobalDB.dbo.WtAvgNZ(EUFPct, EPH),
	EUOFPct = ISNULL(GlobalDB.dbo.WtAvgNZ(EUOFPct, EPH),0),
	MaintIndex = GlobalDB.dbo.WtAvgNZ(MaintIndex, EPH),
	0
FROM #tmp 
GROUP BY Site, Year, Component 

--then company component totals
INSERT #tmp
SELECT Type = 'Site',
	Site = 'COMPANY',
	Unit = 'TOTAL',
	Year,
	Component,
	Name = 'COMPANY ' + CAST(Year AS VARCHAR) + ' ' + Component,
	EUFPct = GlobalDB.dbo.WtAvgNZ(EUFPct, EPH),
	EUOFPct = ISNULL(GlobalDB.dbo.WtAvgNZ(EUOFPct, EPH),0),
	MaintIndex = GlobalDB.dbo.WtAvgNZ(MaintIndex, EPH),
	0
FROM #tmp 
WHERE Unit <> 'TOTAL'
GROUP BY Year, Component 

--next is the totals for the individual units
INSERT #tmp 
SELECT Type = 'Unit', 
	Site = slu.SiteName, 
	Unit = t.unitname, 
	Year = t.StudyYear, 
	Component = 'TOTAL',
	Name = t.UnitName + ' ' + cast(t.StudyYear as varchar),
	EUFPct = nf.euf2yr,
	EUOFPct = nf.EUOF ,
	MaintIndex = mtc.AnnMaintCostMWH,
	0
FROM NERCFactors nf
	INNER JOIN TSort t on t.Refnum = nf.Refnum 
	INNER JOIN Site_LU slu ON slu.CompanyID = @CompanyID AND CAST(slu.SiteNo AS VARCHAR) = CAST(LEFT(t.SiteID, 3) AS VARCHAR)
	INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum 
WHERE nf.Refnum IN (SELECT Refnum FROM TSort WHERE CompanyID = @CompanyID AND StudyYear >= @StartYear AND StudyYear <= @EndYear AND Refnum NOT LIKE '%P')

--and site overall totals
INSERT #tmp 
SELECT Type = 'Site', 
	Site = slu.SiteName, 
	Unit = 'TOTAL', 
	Year = t.StudyYear, 
	Component = 'TOTAL',
	Name = slu.SiteName + ' ' + cast(t.StudyYear as varchar),
	EUFPct = GlobalDB.dbo.WtAvg(nf.euf2yr, nf.WPH),
	EUOFPct = GlobalDB.dbo.WtAvg(nf.EUOF, nf.WPH),
	MaintIndex = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostMWH, gtc.AdjNetMWH2Yr),
	0
FROM NERCFactors nf
	INNER JOIN TSort t on t.Refnum = nf.Refnum 
	INNER JOIN Site_LU slu ON slu.CompanyID = @CompanyID AND CAST(slu.SiteNo AS VARCHAR) = CAST(LEFT(t.SiteID, 3) AS VARCHAR)
	INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum 
	INNER JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
WHERE nf.Refnum IN (SELECT Refnum FROM TSort WHERE CompanyID = @CompanyID AND StudyYear >= @StartYear AND StudyYear <= @EndYear AND Refnum NOT LIKE '%P')
GROUP BY slu.SiteName, t.StudyYear


--and company overall totals
INSERT #tmp 
SELECT Type = 'Site', 
	Site = 'COMPANY', 
	Unit = 'TOTAL', 
	Year = t.StudyYear, 
	Component = 'TOTAL',
	Name = 'COMPANY ' + cast(t.StudyYear as varchar),
	EUFPct = GlobalDB.dbo.WtAvg(nf.euf2yr, nf.WPH),
	EUOFPct = GlobalDB.dbo.WtAvg(nf.EUOF, nf.WPH),
	MaintIndex = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostMWH, gtc.AdjNetMWH2Yr),
	0
FROM NERCFactors nf
	INNER JOIN TSort t on t.Refnum = nf.Refnum 
	INNER JOIN Site_LU slu ON slu.CompanyID = @CompanyID AND CAST(slu.SiteNo AS VARCHAR) = CAST(LEFT(t.SiteID, 3) AS VARCHAR)
	INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum 
	INNER JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
WHERE nf.Refnum IN (SELECT Refnum FROM TSort WHERE CompanyID = @CompanyID AND StudyYear >= @StartYear AND StudyYear <= @EndYear AND Refnum NOT LIKE '%P')
GROUP BY t.StudyYear



SELECT Type, Site, Unit, Year, RTRIM(Component) AS Component, RTRIM(Name) AS Name, EUFPct, EUOFPct, MaintIndex FROM #tmp 
where EUFPct is not null and EUOFPct is not null and MaintIndex is not null 


DROP TABLE #tmp


END
