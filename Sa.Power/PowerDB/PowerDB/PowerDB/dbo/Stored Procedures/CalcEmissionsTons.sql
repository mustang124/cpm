﻿CREATE PROCEDURE [dbo].[CalcEmissionsTons](@SiteID SiteID)
AS

DECLARE @StudyYear smallint; SELECT @StudyYear = StudyYear FROM StudySites WHERE SiteID = @SiteID
IF @StudyYear > 2001 AND NOT EXISTS (SELECT * FROM TSort WHERE SiteID = @SiteID AND Refnum LIKE '%P')
BEGIN
	DELETE FROM EmissionsTons WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
	INSERT INTO EmissionsTons (Refnum, SO2Tons, NOxTons, CO2Tons, TotEmissionsTons)
	SELECT Refnum, SUM(SO2Emitted), SUM(NOxEmitted), SUM(CO2Emitted), SUM(ISNULL(SO2Emitted, 0) + ISNULL(NOxEmitted, 0) + ISNULL(CO2Emitted, 0))
	FROM Misc WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
	GROUP BY Refnum
	HAVING SUM(ISNULL(SO2Emitted, 0) + ISNULL(NOxEmitted, 0) + ISNULL(CO2Emitted, 0))>0
END
