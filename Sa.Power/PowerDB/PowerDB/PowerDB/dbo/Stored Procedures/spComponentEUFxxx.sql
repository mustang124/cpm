﻿CREATE PROC [dbo].[spComponentEUFxxx] (@Refnum varchar(10))
AS

	-- Delete any records in ComponentEUF for this Refnum
	Delete from ComponentEUF Where Refnum = @Refnum

	-- Create the new records in ComponentEUF for this Refnum
	Insert into ComponentEUF
		Select Refnum, EquipGroup, Sum(EUF), Avg(E_PH)
			From ComponentEUFCollector euf
			Inner Join ComponentEUF_LU lu on euf.UnitType = lu.UnitType 
			and euf.SAIMajorEquip = lu.SAIMajorEquip
			and euf.SAIMinorEquip = lu.SAIMinorEquip
			Where euf.Refnum = @Refnum
			Group By Refnum, EquipGroup
