﻿-- =============================================
-- Author: Rodrick Blanton
-- Create date: 27 March 2017
-- Description:	Retrieves top level slide data for selection
-- =============================================
CREATE PROCEDURE [dbo].[presBuilder_getSlideInfo] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		slds.SlideID,
		slds.Name,
		slch.SlideChartID [ChartID],
		slch.ChartType,
		sldt.SlideDataID,
		sldt.DataSource,
		sldt.DataType [DataSourceType],
		so.type_desc [DataSourceTypeDescription],
		parm.name [ParameterName], 
		typ.name [ParameterType],
		parm.max_length [MaxLength],
		parm.precision [Precision],
		parm.scale [Scale],
		(
			Select ISNULL(sdv.Value,'')
			From SlideDataValues sdv
			Where 
					(
						ISNULL(sdv.ParameterName,'') = ISNULL(parm.name,'') 
						AND ISNULL(sdv.SlideDataID,0) = 0
						AND ISNULL(sdv.SlideChartID,0) = 0
						AND ISNULL(sdv.ObjectID,0) = 0
					)
					--OR
					--(
					--	ISNULL(sdv.ObjectID,0) = ISNULL(parm.object_id,0)
					--	AND ISNULL(sdv.ParameterName,'') = ISNULL(parm.name,'')
					--)
					OR
					(
						ISNULL(sdv.SlideDataID,0) = ISNULL(sldt.SlideDataID,0) 
						AND ISNULL(sdv.SlideChartID,0) = ISNULL(slch.SlideChartID,0)
						AND ISNULL(sdv.ObjectID,0) = ISNULL(parm.object_id,0)
						AND ISNULL(sdv.ParameterName,'') = ISNULL(parm.name,'')
					)
		) AS [Value],
		(
			Select 1 
			FROM SlideDataValues sdv
			Where ISNULL(sdv.ParameterName,'') = ISNULL(parm.name,'') 
			AND ISNULL(sdv.SlideDataID,0) = 0 
			AND ISNULL(sdv.SlideChartID,0) = 0 
			AND ISNULL(sdv.ObjectID,0) = 0
		) AS [IsGlobal]
		--CASE  -- The mapping is not direct so parm1 != Filter1 can happen.  So, removing this for now.
		--	when parm.parameter_id = 1 THEN sldt.Filter1
		--	when parm.parameter_id = 2 THEN sldt.Filter2
		--	when parm.parameter_id = 3 THEN sldt.Filter3
		--	when parm.parameter_id = 4 THEN sldt.Filter4
		--	when parm.parameter_id = 5 THEN sldt.Filter5
		--	when parm.parameter_id = 6 THEN sldt.Filter6
		--	when parm.parameter_id = 7 THEN sldt.Filter7
		--END AS [Map]
	FROM Slides slds
		LEFT JOIN SlideCharts slch ON slds.SlideID = slch.SlideID
		LEFT JOIN SlideData sldt ON slch.SlideChartID = sldt.SlideChartID
		LEFT JOIN sys.objects so ON sldt.DataSource = so.name AND so.[type] in ('IF','P','TF')  --Select DISTINCT([type]), type_desc from sys.objects
		LEFT JOIN sys.parameters parm ON so.object_id = parm.object_id
		LEFT JOIN sys.types typ ON typ.user_type_id = parm.user_type_id
	WHERE
		ISNULL(slds.Active,0) = 1
	ORDER BY 
		slds.SlideID,
		slch.SlideChartID,
		sldt.SlideDataID,
		parm.parameter_id

END
