﻿CREATE PROCEDURE spListToCalcQueue 
	@ListName RefListName,
	@Calculate bit = 1, @Compare bit = 1, @Copy bit = 1,
	@ClientTables bit = 1, @CalcSummary bit = 1,
	@MessageLog bit = 1, @EmailNotify bit = 1,
	@EMailMessage varchar(255) = NULL
AS
IF EXISTS (SELECT * FROM CalcQueue WHERE ListName = @ListName)
	UPDATE CalcQueue
	SET Calculate = @Calculate, Compare = @Compare, Copy = @Copy,
	ClientTables = @ClientTables, CalcSummary = @CalcSummary,
	MessageLog = @MessageLog, EMailNotify = @EmailNotify,
	EMailMessage = @EMailMessage 
	WHERE ListName = @ListName
ELSE
	INSERT INTO CalcQueue (ListName, Calculate, Compare, Copy,
	ClientTables, CalcSummary, MessageLog, EMailNotify, EMailMessage)
	VALUES (@ListName, @Calculate, @Compare, @Copy, @ClientTables, 
	@CalcSummary, @MessageLog, @EMailNotify, @EMailMessage)
