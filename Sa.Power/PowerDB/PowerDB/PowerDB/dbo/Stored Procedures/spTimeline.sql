﻿CREATE PROCEDURE [dbo].[spTimeline] (@SiteID CHAR(10))

AS
BEGIN

set nocount on 


	DECLARE @UUC CHAR(6)
	DECLARE @AC DECIMAL(18,2)
	DECLARE @LastEvent DATETIME	
	DECLARE @TimeLineStart DATETIME
	SET @TimeLineStart = '1/1/2010'	--
	DECLARE @Time2 DATETIME
	DECLARE @StartTime DATETIME	--
	DECLARE @EndTime DATETIME	--
	DECLARE @ActiveEventStart DATETIME
	DECLARE @ActiveEventEnd DATETIME
	DECLARE @NDC DECIMAL(7,2)
--	DECLARE @CalcIndex INTEGER	--
--	SET @CalcIndex = 1
--	DECLARE @LogIndex INTEGER	--
--	SET @LogIndex = 1
	DECLARE @Counter INTEGER
	DECLARE @MaxID INTEGER
	
	DECLARE @EventNumber SMALLINT
	DECLARE @EventType CHAR(2)
	DECLARE @EventGroup CHAR(2)
	DECLARE @StartDateTime DATETIME
	DECLARE @EndDateTime DATETIME
	DECLARE @GrossAvailCapacity DECIMAL(7,2)
	DECLARE @NetAvailCapacity DECIMAL(7,2)
	DECLARE @cEvnt INTEGER
	DECLARE @i INTEGER
	
--	--clean out the working tables used to load the data
	TRUNCATE TABLE PowerWork.dbo.TimelineCalc
	TRUNCATE TABLE PowerWork.dbo.TimelineLog

	--make a cursor of the UtilityUnitCodes for this SiteID, they'll be looped for processing
	DECLARE rsNERCTurbine CURSOR FOR 
		SELECT nt.UtilityUnitCode FROM NERCTurbine nt 
			INNER JOIN TSort t ON t.Refnum = nt.Refnum 
		WHERE t.SiteID = @SiteID

	OPEN rsNERCTurbine
	FETCH NEXT FROM rsNERCTURBINE INTO @UUC

	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SELECT @AC = ISNULL(NetMaxCapacity, ISNULL(GrossMaxCapacity, 0)) FROM GADSOS.GADSNG.Setup WHERE UtilityUnitCode = @UUC
		SELECT @LastEvent = MAX(EndDateTime) FROM GADSOS.GADSNG.EventData01 WHERE UtilityUnitCode = @UUC

		IF @LastEvent > @TimeLineStart 
			BEGIN
				SELECT @Time2 = DATEADD(day, 1, @LastEvent)
    --print CAST(@UUC AS VARCHAR) + ' 0 ' + CAST(@Time2 AS VARCHAR)
				
				EXEC spTimelineAddEvent 0, '', '', @TimeLineStart, @Time2, @AC, 1, @UUC
				SELECT @MaxID = MAX(IDNum) FROM TimelineCalc 
				SELECT @ActiveEventStart = StartTime FROM TimelineCalc WHERE IDNum = @MaxID
				SELECT @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
				--SELECT @ActiveEventStart = StartTime, @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
			END

		DECLARE rs CURSOR FOR 
			SELECT EventNumber, e.EventType, l.EVNT_GROUP, StartDateTime, EndDateTime, GrossAvailCapacity , NetAvailCapacity 
			FROM Event01 e 
				INNER JOIN NERC.DBO.EVNT_LU l ON e.EventType = l.EVNT_TYPE 
			WHERE StartDateTime >= @TimeLineStart And UtilityUnitCode = @UUC
				AND e.RevisionCard01 <> 'X' AND l.EVNT_GROUP IS NOT NULL 
			ORDER BY StartDateTime, NetAvailCapacity DESC, GrossAvailCapacity DESC, EndDateTime, EventNumber

		OPEN rs

		--do something to it
		FETCH NEXT FROM rs INTO @EventNumber, @EventType, @EventGroup, @StartDateTime, @EndDateTime, @GrossAvailCapacity, @NetAvailCapacity
		
		WHILE @@FETCH_STATUS = 0 
		BEGIN

			SET @StartTime = @StartDateTime
			SET @EndTime = ISNULL(@EndDateTime, '1/1/' + CAST(YEAR(@StartDateTime) AS VARCHAR))

			--SELECT @Counter = COUNT(*) FROM Timelinecalc WHERE @StartTime >= @EndTime
		--		PRINT CAST(@StartTime AS VARCHAR) + ', ' + CAST(@ActiveEventEnd AS VARCHAR)
			
			--WHILE @Counter > 0 
			WHILE @StartTime >= @ActiveEventEnd 
			BEGIN
		--		PRINT CAST(@StartTime AS VARCHAR) + ', ' + CAST(@ActiveEventEnd AS VARCHAR)
				EXEC spTimelineExpireEvent
				SELECT @MaxID = MAX(IDNum) FROM TimelineCalc 
				SELECT @ActiveEventStart = StartTime FROM TimelineCalc WHERE IDNum = @MaxID
				SELECT @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
				--SELECT @ActiveEventStart = StartTime, @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
				--SET @Counter = @Counter - 1
			END

			SELECT @NDC = CASE WHEN ISNULL(NetDepCap,0) > 0 THEN NetDepCap ELSE 
						CASE WHEN ISNULL(GrossDepCap,0) > 0 THEN GrossDepCap ELSE
						CASE WHEN ISNULL(NetMaxCap,0) > 0 THEN NetMaxCap ELSE
						CASE WHEN ISNULL(GrossMaxCap,0) > 0 THEN GrossMaxCap ELSE 0 END END END END
			FROM GADSOS.GADSNG.PerformanceRecords pr 
				INNER JOIN GADSOS.GADSNG.Setup s on s.UnitShortName = pr.UnitShortName 
			WHERE s.UtilityUnitCode = @UUC
				AND Year = YEAR(@StartTime)
				AND Period <> 'YR' 
				AND Period NOT LIKE 'Q%' 
				AND Period = CASE WHEN MONTH(@StartTime) < 10 THEN '0' + CAST(MONTH(@StartTime) AS VARCHAR) ELSE CAST (MONTH(@StartTime) AS VARCHAR) END
			
			IF ISNULL(@NDC,0) > 0
			BEGIN
				SELECT @MaxID = MAX(IDNum) FROM TimelineCalc
				SELECT @cEvnt = Evnt_No FROM TimelineCalc WHERE IDNum = @MaxID 
				IF @cEvnt = 0
					UPDATE TimelineCalc SET AvailCap = @NDC WHERE IDNum = 2-- @MaxID
			END


			IF @EventGroup = 'O'
				SET @AC = 0
			ELSE
				IF @NetAvailCapacity > 0
					SET @AC = @NetAvailCapacity 
				ELSE
					SET @AC = ISNULL(@GrossAvailCapacity, 0)
    --print CAST(@UUC AS VARCHAR) + ' ' + CAST(@EventNumber AS VARCHAR) + ' ' + CAST(@EndTime AS VARCHAR)

			EXEC spTimelineAddEvent @EventNumber, @EventType, @EventGroup, @StartTime, @EndTime, @AC, 1, @UUC
			SELECT @MaxID = MAX(IDNum) FROM TimelineCalc 
			SELECT @ActiveEventStart = StartTime FROM TimelineCalc WHERE IDNum = @MaxID
			SELECT @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
				--SELECT @ActiveEventStart = StartTime, @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
				
			FETCH NEXT FROM rs INTO @EventNumber, @EventType, @EventGroup, @StartDateTime, @EndDateTime, @GrossAvailCapacity, @NetAvailCapacity
		END
		CLOSE rs
		DEALLOCATE rs
		
		SELECT @i = MAX(IDNum) FROM TimelineCalc 
		WHILE @i > 2
		BEGIN
			EXEC spTimelineExpireEvent
			SELECT @MaxID = MAX(IDNum) FROM TimelineCalc 
			SELECT @ActiveEventStart = StartTime FROM TimelineCalc WHERE IDNum = @MaxID
			SELECT @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
				--SELECT @ActiveEventStart = StartTime, @ActiveEventEnd = EndTime FROM TimelineCalc WHERE IDNum = @MaxID
			SET @i = @i - 1
		END

	TRUNCATE TABLE TimelineCalc
	
		FETCH NEXT FROM rsNERCTURBINE INTO @UUC
	END
	CLOSE rsNERCTurbine
	DEALLOCATE rsNERCTurbine




END
