﻿CREATE            PROC [dbo].[spGadsFactors] (
 	@UtilityUnitCode varchar(6), 
	@Year1 int, 
	@Year2 int,
	@NMC real output,
	@NDC real output,
	@EFOR real output,	-- F11 #24
	@EFORWtFactor real output,
	@EPOF real output,	-- F9 #18
	@EUOF real output,	-- F10 #21
	@EPOR real output,	-- F11 #26
	@EPORWtFactor real output,
	@EUOR real output,	-- F11 #28
	@EUORWtFactor real output,
	@EUF  real output,	-- F8 #11
	@EAF  real output,	-- F9 #12
	@POF  real output,	-- F7 #1
	@EOR  real output,	-- Solomon Item
	@EORWtFactor real output,
	@FOF  real output,	-- F7 #3
	@MOF  real output,	-- F7 #4
	@NUMFO real output,	-- F5 #16
	@NUMMO real output,	-- F5 #17
	@NUMPO real output,	-- F4 #14
	@NUMUO real output, 	-- F4 #15
	@PH    real output,	-- F2 #11
	@SH    real output, 	-- F1 #1
	@UOF   real output,	-- F7 #2
	@PH_Unweighted real output,
	@SH_Unweighted real output,
	@AGE 	real output,
	@ACT_Starts real output,
	@ATM_Starts real output
)
AS

--                      Gads appendix F Page 1 and 2 definition number:
--DECLARE		@SH real   	--  1
DECLARE		@AH  real	--  4
DECLARE		@POH real   	--  5
DECLARE		@UOH real   	--  6
DECLARE		@FOH real   	--  7
DECLARE		@MOH real 	--  8
--DECLARE		@PH real   	--  11
DECLARE		@EFDH real   	--  13A
DECLARE		@EPDH real   	--  13B
DECLARE		@EUDH real   	--  13C
DECLARE		@EFDHRS real   	--  13D
DECLARE		@EPDHRS real   	--  13E
DECLARE		@EUDHRS real  	--  ??
--DECLARE		@NUMFO real 	-- F5 #16
--DECLARE		@NUMMO real 	-- F5 #17
--DECLARE		@NUMPO real 	-- F4 #14
--DECLARE		@NUMUO real  	-- F4 #15
DECLARE 	@EAFx real -- unweigthed interim var
			-- to avoid having to get NMC.

-- Load up the items
SELECT 
@SH = SUM(eh.E_SH),
@SH_Unweighted = SUM(eh.SH),
@POH = SUM(eh.E_PO + eh.E_PO_SE),
@UOH = SUM(eh.E_U1 + eh.E_U2 + eh.E_U3 + eh.E_SF + eh.E_MO + eh.E_MO_SE),
@FOH = SUM(eh.E_U1 + eh.E_U2 + eh.E_U3 + eh.E_SF),
@MOH = SUM(eh.E_MO + eh.E_MO_SE),
@PH = SUM(eh.E_PH),         
@PH_Unweighted = SUM(eh.PH),                      
@EFDH = SUM(eh.E_D1 + eh.E_D2 + eh.E_D3),
@EPDH = SUM(eh.E_PD + eh.E_PD_DE),
@EUDH = SUM(eh.E_D1 + eh.E_D2 + eh.E_D3 + eh.E_D4 + eh.E_D4_DE),
@EFDHRS = SUM(eh.E_EUFDH_RS),
@EPDHRS = SUM(eh.E_EPDH_RS),
@EUDHRS = SUM(eh.E_EUFDH_RS),     -- CHECK WITH RON -- SAME AS EFDHRS??????
@EAF = SUM(eh.EAF*EH.E_PH)/SUM(EH.E_PH),
@AGE = AVG(eh.UnitAge)		
FROM GADSOS.GADSNG.EventHours eh
INNER JOIN GADSOS.GADSNG.Setup s ON s.UnitShortName = eh.UnitShortName
WHERE (DatePart(yy,TL_DateTime) = @Year1 OR DatePart(yy,TL_DateTime) = @Year2) 
AND (eh.Granularity = 'Yearly')
AND s.UtilityUnitCode = @UtilityUnitCode
 -- '330110'

-- Get counts for each event type.
SELECT 
@NUMPO = SUM(CASE WHEN ed.EventType = 'PO' THEN 1 ELSE 0 END),
@NUMUO = SUM(CASE WHEN ed.EventType IN ('MO', 'U1', 'U2', 'U3') THEN 1 ELSE 0 END),
@NUMFO = SUM(CASE WHEN ed.EventType IN ('U1', 'U2', 'U3') THEN 1 ELSE 0 END),
@NUMMO = SUM(CASE WHEN ed.EventType = 'MO' THEN 1 ELSE 0 END)
FROM GADSOS.GADSNG.EventData01 ed
WHERE (ed.Year = @Year1 OR ed.Year = @Year2)
AND ed.UtilityUnitCode = @UtilityUnitCode
AND ed.EventType IN ('PO', 'MO', 'U1', 'U2', 'U3')

-- Getn NMC, NDC, ACT_Starts, ATM_Starts.
SELECT 
@NMC = (SUM(pd.NetMaxCap * pd.PeriodHours) / SUM(pd.PeriodHours)),
@NDC = (SUM(pd.NetDepCap * pd.PeriodHours) / SUM(pd.PeriodHours)),
@ACT_Starts = SUM(ActualStarts), 
@ATM_Starts = SUM(AttemptedStarts)
from GADSOS.GADSNG.PerformanceData pd
where (pd.Year = @Year1 OR pd.Year = @Year2)
AND pd.UtilityUnitCode = @UtilityUnitCode

-- Calc factor from variables that we have loaded up.
SELECT @EFORWtFactor = @FOH + @SH + @EFDHRS
IF @EFORWtFactor > 0 
 	SELECT @EFOR = 100 * ((@FOH + @EFDH) / @EFORWtFactor)  -- F11 #24
ELSE	SELECT @EFOR = NULL

IF @PH > 0 
	BEGIN
 	SELECT @EPOF = 100 * ((@POH + @EPDH) / @PH)  		 -- F9 #18
	SELECT @EUOF = 100 * ((@UOH + @EUDH) / @PH)		 -- F10 #21
	SELECT @EUF = 100 * ((@UOH + @POH + @EUDH + @EPDH) / @PH)  -- F8 #11
--	SELECT @EAF = (@EAFx * @PH) / @PH			-- F9 #12
 	SELECT @POF = 100 * (@POH / @PH)			-- F7 #1
	SELECT @UOF = 100 * (@UOH / @PH)			-- F7 #2
	SELECT @FOF = 100 * (@FOH / @PH) 			-- F7 #3
	SELECT @MOF = 100 * (@MOH / @PH) 			-- F7 #4
	END
ELSE	BEGIN
	SELECT @EPOF = NULL
	SELECT @EUOF = NULL
	SELECT @EUF  = NULL
	SELECT @EAF = NULL
	SELECT @POF = NULL
	SELECT @UOF = NULL
	SELECT @FOF = NULL
	SELECT @MOF = NULL
	END

SELECT @EPORWtFactor = @POH + @SH + @EPDHRS
IF @EPORWtFactor > 0
 	SELECT @EPOR = 100 * ((@POH + @EPDH) / @EPORWtFactor)  -- F11 #26
ELSE 	SELECT @EPOR = NULL

SELECT @EUORWtFactor = @UOH + @SH
IF @EUORWtFactor > 0 
 	SELECT @EUOR = 100 * ((@UOH + @EUDH) / @EUORWtFactor) 		 -- F11 #28
ELSE  	SELECT @EUOR = NULL

SELECT @EORWtFactor = (@POH + @UOH + @SH + @EPDHRS + @EUDHRS)
IF @EORWtFactor > 0
  	SELECT @EOR  = 100 * ((@POH + @EPDH + @UOH + @EUDH) 
		     / (@POH + @UOH + @SH + @EPDHRS + @EUDHRS))  -- Solomon Item
ELSE SELECT @EOR = NULL
