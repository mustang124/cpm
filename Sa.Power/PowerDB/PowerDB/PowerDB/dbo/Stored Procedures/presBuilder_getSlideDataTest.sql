﻿-- =============================================
-- Author: Rodrick Blanton
-- Create date: 27 March 2017
-- Description:	Retrieves top level slide data for selection
-- =============================================
CREATE PROCEDURE [dbo].[presBuilder_getSlideDataTest] 
	@jsonString NVarchar(Max)
AS
BEGIN

	--Declare @i int = 183, @c int = 0

	--While @c < @i
	--Begin
	--	Select @jsonString
	--	Select @c = @c + 1
	--End

	Select * From
	parseJSON
	(
		   @jsonString

	)

END