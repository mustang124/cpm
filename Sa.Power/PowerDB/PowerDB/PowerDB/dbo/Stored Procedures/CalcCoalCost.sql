﻿CREATE PROC [dbo].[CalcCoalCost](@SiteID SiteID)
AS
UPDATE Coal SET
	TotMiles = ISNULL(RailMiles,0) + ISNULL(BargeMiles,0) + ISNULL(HwyMiles,0) + ISNULL(ConvLength/1760,0), 
	TotCostTon = ISNULL(TransCostTon,0) + ISNULL(MineCostTon,0) + ISNULL(OnSitePrepCostTon,0),
    MBTU = ISNULL(HeatValue*Tons/500,0), 
    MBTULHV = ISNULL(LHV*Tons/500,0)
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE Coal SET
	TotTonMiles = ISNULL(TotMiles*Tons,0),
	TransCostTM = CASE WHEN TotMiles > 0 THEN 100*TransCostTon/TotMiles END, 
    TotCostKUS = TotCostTon * Tons / 1000
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

/* Calculate total Tons, TonMiles, Average Cost/TonMile by unit for each period */
DELETE FROM CoalTotCalc WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
INSERT INTO CoalTotCalc (Refnum, TotTons, TotTonMiles, TransCostTM, MineCostTon, TransCostTon,
      OnSitePrepCostTon, TotCostTon, MBTU, MBTULHV, HeatValue, LHV,
      AshPcnt, SulfurPcnt, MoistPcnt, HardgroveGrind)
SELECT Refnum, SUM(Tons), SUM(TotTonMiles), 
	GlobalDB.dbo.WtAvg(TransCostTM, TotTonMiles),
	GlobalDB.dbo.WtAvg(MineCostTon,Tons),
	GlobalDB.dbo.WtAvg(TransCostTon,Tons),
	GlobalDB.dbo.WtAvg(OnsitePrepCostTon,Tons),
	GlobalDB.dbo.WtAvg(TotCostTon,Tons), SUM(MBTU), SUM(MBTULHV),
	GlobalDB.dbo.WtAvg(HeatValue,Tons), GlobalDB.dbo.WtAvg(LHV,Tons),
	GlobalDB.dbo.WtAvgNN(CASE WHEN MBTU > 0 THEN AshPcnt END,Tons),
	GlobalDB.dbo.WtAvgNN(CASE WHEN MBTU > 0 THEN SulfurPcnt END,Tons),
	GlobalDB.dbo.WtAvgNN(CASE WHEN MBTU > 0 THEN MoistPcnt END,Tons),
	GlobalDB.dbo.WtAvgNN(CASE WHEN MBTU > 0 THEN HardgroveGrind END,Tons)
FROM Coal WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
GROUP BY Refnum
