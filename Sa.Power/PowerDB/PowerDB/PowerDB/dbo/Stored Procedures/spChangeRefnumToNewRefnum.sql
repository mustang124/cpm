﻿-- =============================================
-- Author:		Dennis Burdett
-- Create date: Aug 5, 2008
-- Description:	Updates all the tables in Power, PowerWork, and PowerGlobal to change a RefNum
-- =============================================
CREATE PROCEDURE [dbo].[spChangeRefnumToNewRefnum] 
	@OrgRefNum Char(12) = 0, 
	@NewRefNum Char(12) = 0
AS
BEGIN
	SET NOCOUNT ON;

	if ((substring(@OrgRefNum, len(@OrgRefNum)-2, 3)) = (substring(@NewRefNum, len(@NewRefNum)-2, 3)))
	BEGIN
		-- Update all the PowerWork tables
		UPDATE PowerWork.dbo.AlternateHubs SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Ancillary SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.AssetCosts SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Auxiliaries SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.AvailableHours SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Breaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CarryoverParents SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Coal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CoalTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CogenCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CommUnavail SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.ComponentEUF SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.ComponentGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CptlMaintExp SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CptlMaintExpLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CRVCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CRVGap SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CTGData SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CTGMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.CTGSupplement SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.EGCCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Equipment SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.EventLRO SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Events SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Fuel SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.FuelProperties SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.FuelTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.GenerationAnn SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.GenerationTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.GenSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.HRVariance SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.LTSA SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.MaintEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.MaintEquipSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.MaintTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.MessageLog SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Misc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.MiscCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.NERCFactors SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.NERCTurbine SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.NonOHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Offline SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.OHEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.OHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.OHMaintCPHM SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.OpEx SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.OpExCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.OpexLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Pers SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.PersSTCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.PowerGeneration SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Rank SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.Revenue SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.RevenuePeaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.ROODs SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.StartsAnalysis SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.SteamSales SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.TSort SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.UnitGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.ValidationNotes SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.ValStat SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE PowerWork.dbo.VarCostCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum

		-- Update all the PowerWork tables
		UPDATE Power.dbo.AlternateHubs SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Ancillary SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.AssetCosts SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Auxiliaries SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Breaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Coal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CoalTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CogenCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CommUnavail SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.ComponentEUF SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.ComponentGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CptlMaintExp SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CptlMaintExpLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CRVCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CRVGap SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CTGData SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.CTGMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.EGCCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.EmissionsTons SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Equipment SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.EventLRO SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Events SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Fuel SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.FuelProperties SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.FuelTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.GenerationAnn SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.GenerationTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.GenSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.HRVariance SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.LTSA SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.MaintEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.MaintEquipSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.MaintTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.MessageLog SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Misc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.MiscCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.NERCFactors SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.NERCTurbine SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.NERCTurbine SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.NonOHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.OHEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.OHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.OHMaintCPHM SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.OpEx SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.OpExCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.OpexLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Pers SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.PersSTCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.PowerGeneration SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.Rank SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.RevenuePeaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.SteamSales SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.TSort SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.UnitGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.ValidationNotes SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.ValStat SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.ValStatRestore SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
		UPDATE Power.dbo.VarCostCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum

		-- Update all the PowerGlobal tables
		UPDATE PowerGlobal.dbo.RefList SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	END
	ELSE
		Print 'last 3 chars of refnums do NOT match'
END
