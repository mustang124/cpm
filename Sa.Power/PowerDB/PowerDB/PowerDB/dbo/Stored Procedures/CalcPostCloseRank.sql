﻿
CREATE PROC [dbo].[CalcPostCloseRank](@Refnum varchar(10))

AS

	DECLARE @Value REAL
	DECLARE @BreakValue VARCHAR(10)
	DECLARE @Breakcondition varchar(20)

	DECLARE @Listname varchar(20)
	SELECT @Listname = 'Power' + RIGHT(StudyYear,2) FROM TSort WHERE Refnum = @Refnum --is this necessarily correct? calc against the listed study year?
	--there may be times when you need to not do this, e.g. when you want to calc a unit against the prior study year
	--user will need to know that they have to change the studyyear to get it to calc that way

	--clear out old records for this unit
	DELETE FROM PostCloseRank WHERE Refnum = @Refnum --AND ListName = @Listname AND BreakCondition = @breakcondition AND BreakValue = @BreakValue


	--the following two sets are duplicates, I wasn't sure of an easier way to do this
	--maybe turn it into a table? but how to pull the correct field of data to populate it? maybe it becomes a view?

	SET @Breakcondition = 'CRV Group'
	SELECT @BreakValue = CRVGroup FROM Breaks WHERE Refnum = @Refnum


	SELECT @Value = ISNULL(HeatRate,0) FROM GenSum WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 1, @Value

	SELECT @Value = ISNULL(AnnMaintCostMWH,0) FROM MaintTotCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 6, @Value

	SELECT @Value = ISNULL(MaintOHInd,0) FROM GenSum WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 7, @Value

	SELECT @Value = ISNULL(MaintNonOHInd,0) FROM GenSum WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 8, @Value

	SELECT @Value = CASE WHEN ISNULL(TotPeakMWH,0) = 0 THEN 0 ELSE ISNULL(PeakMWHLost_Unp,0)/TotPeakMWH END * 100 FROM CommUnavail WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 46, @Value

	SELECT @Value = ISNULL(CUTI_Unp,0) FROM CommUnavail WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 47, @Value

	SELECT @Value = ISNULL(CUTI_P,0) FROM CommUnavail WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 49, @Value

	SELECT @Value = ISNULL(InternalLessScrubberPcnt,0) FROM GenerationTotCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 55, @Value

	SELECT @Value = ISNULL(TotCashLessFuelEm,0) FROM OpExCalc WHERE Refnum = @refnum AND DataType = 'EGC'
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 71, @Value

	SELECT @Value = ISNULL(ThermEff,0) FROM CogenCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 78, @Value

	SELECT @Value = ISNULL(AnnLTSACostMWH,0) FROM MaintTotCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 81, @Value

	SELECT @Value = ISNULL(TotCashLessFuelEm,0) FROM OpExCalc WHERE Refnum = @refnum AND DataType = 'MWH'
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 86, @Value

	SELECT @Value = ISNULL(TotCashLessFuelEm,0) FROM OpExCalc WHERE Refnum = @refnum AND DataType = 'KW'
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 88, @Value









	SET @Breakcondition = 'CRV Size Groups'
	SELECT @BreakValue = CRVSizeGroup FROM Breaks WHERE Refnum = @Refnum

	--DELETE FROM PostCloseRank WHERE Refnum = @Refnum AND ListName = @Listname AND BreakCondition = @breakcondition AND BreakValue = @BreakValue

	SELECT @Value = ISNULL(HeatRate,0) FROM GenSum WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 1, @Value

	SELECT @Value = ISNULL(AnnMaintCostMWH,0) FROM MaintTotCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 6, @Value

	SELECT @Value = ISNULL(MaintOHInd,0) FROM GenSum WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 7, @Value

	SELECT @Value = ISNULL(MaintNonOHInd,0) FROM GenSum WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 8, @Value

	SELECT @Value = CASE WHEN ISNULL(TotPeakMWH,0) = 0 THEN 0 ELSE ISNULL(PeakMWHLost_Unp,0)/TotPeakMWH END * 100 FROM CommUnavail WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 46, @Value

	SELECT @Value = ISNULL(CUTI_Unp,0) FROM CommUnavail WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 47, @Value

	SELECT @Value = ISNULL(CUTI_P,0) FROM CommUnavail WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 49, @Value

	SELECT @Value = ISNULL(InternalLessScrubberPcnt,0) FROM GenerationTotCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 55, @Value

	SELECT @Value = ISNULL(TotCashLessFuelEm,0) FROM OpExCalc WHERE Refnum = @refnum AND DataType = 'EGC'
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 71, @Value

	SELECT @Value = ISNULL(ThermEff,0) FROM CogenCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 78, @Value

	SELECT @Value = ISNULL(AnnLTSACostMWH,0) FROM MaintTotCalc WHERE Refnum = @refnum
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 81, @Value

	SELECT @Value = ISNULL(TotCashLessFuelEm,0) FROM OpExCalc WHERE Refnum = @refnum AND DataType = 'MWH'
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 86, @Value

	SELECT @Value = ISNULL(TotCashLessFuelEm,0) FROM OpExCalc WHERE Refnum = @refnum AND DataType = 'KW'
	EXEC SetPostCloseRank @Refnum, @Listname, @BreakCondition, @BreakValue, 88, @Value






