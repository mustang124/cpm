﻿-- =============================================
-- Author:		Dennis Burdett
-- Create date: July 15, 2008
-- Description:	Updates the Technology field in TSort table
-- =============================================
CREATE PROCEDURE dbo.spUpdateTechnology 
AS
BEGIN
	SET NOCOUNT ON;

	-- All Coal: 'COAL'
	UPDATE dbo.TSort
	SET technology='COAL'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE fuelgroup LIKE 'coal')
	 
	-- CC Electric: 'CCGT-E'
	UPDATE dbo.TSort
	SET technology='CCGT-E'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE crvsizegroup LIKE 'ce')

	-- CC Cogen: 'COGEN'
	UPDATE dbo.TSort
	SET technology='COGEN'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE (crvsizegroup LIKE 'cg1' or crvsizegroup LIKE 'cg2' or crvsizegroup LIKE 'cg3'))
	 
	-- SGO: 'SGO GAS&OIL'
	UPDATE dbo.TSort
	SET technology='SGO GAS&OIL'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE crvgroup LIKE 'gas')

	-- Multi: 'MULTI'
	UPDATE dbo.TSort
	SET technology='MULTI'
	WHERE ((Coal_NDC > 0 AND Oil_NDC > 0) 
		OR (Coal_NDC > 0 AND Gas_NDC > 0) 
		OR (Coal_NDC > 0 AND CTG_NDC > 0)
		OR (Oil_NDC > 0 AND CTG_NDC > 0)
		OR (Gas_NDC > 0 AND Oil_NDC > 0) 
		OR (Gas_NDC > 0 AND CTG_NDC >0))

	SELECT DISTINCT technology FROM tsort
END
