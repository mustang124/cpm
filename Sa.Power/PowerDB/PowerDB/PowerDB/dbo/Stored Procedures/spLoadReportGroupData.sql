﻿
CREATE PROCEDURE [dbo].[spLoadReportGroupData] @WhichData varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

--Pass in a name for a block of data to be run. Each name will run all the reports for that table.
--The program will check the choices, then attempt to run a stored proc with the name 'spLoadReportGroupXXXXData', where the XXXX is the name passed to the stored proc
--
--Possible choices are:
--Cogen
--CT
--Emissions
--EUF
--ExpenseMW
--ExpenseMWH
--Fuel
--General
--KPI
--MaintenanceMW
--MaintenanceMWH
--PersonnelCompAbsence
--PersonnelMW
--PersonnelMWH
--UnitCount

IF @WhichData IN ('Cogen', 'CT', 'Emissions', 'EUF', 'ExpenseMW', 'ExpenseMWH', 'Fuel', 'General', 'KPI', 'MaintenanceMW', 'MaintenanceMWH', 'PersonnelCompAbsence',
	'PersonnelMW', 'PersonnelMWH', 'UnitCount')
	BEGIN
		EXEC ('DECLARE @ref char(20)

		DECLARE c CURSOR FOR SELECT RefNum FROM [Power].[dbo].[ReportGroups] WHERE Listname like ''Power10%''

		OPEN c

		FETCH NEXT FROM c INTO @ref

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC [spLoadReportGroup' + @WhichData + 'Data] @ReportRefnum = @Ref
			FETCH NEXT FROM c INTO @ref
		END 
		CLOSE c
		DEALLOCATE c')
	END
ELSE
	BEGIN
		SELECT 'ERROR - there is no data group called ' + @WhichData
	END
END
