﻿

CREATE PROC [dbo].[spLoadReportGroupGeneralData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

	--remove the existing record in the table
	DELETE FROM ReportGroupGeneralData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


			INSERT ReportGroupGeneralData (RefNum, 
				ReportTitle, 
				ListName, 
				UnitCount, 
				CTGCount, 
				NMC, 
				AvgNMC, 
				PriorAdjNetGWH, 
				AdjNetMWH, 
				AvgPriorAdjNetGWH, 
				AvgAdjNetMWH, 
				FuelGroup, 
				LoadType, 
				NCF2Yr, 
				NOF2Yr,
				ServiceHours,
				Age,
				InternalUseSteamAux,
				InternalUseScrubberAux,
				InternalUseElectricAux,
				InternalUseTotal,
				AutoControlPct,
				BoilerOverhaulIntervalMonths,
				HPSOverhaulIntervalMonths,
				IPSOverhaulIntervalMonths,
				LPSOverhaulIntervalMonths,
				GeneratorOverhaulIntervalMonths,
				CTOverhaulIntervalMonths,
				HRSGOverhaulIntervalMonths,
				CashLessFuelPerRunHour,
				FixedCostPerRunHour,
				VariableCostLessFuelPerRunHour,
				MaintenanceCostPerRunHour,
				NonOverhaulCostPerRunHour,
				AnnualOverhaulCostPerRunHour,
				LTSACostPerRunHour)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				UnitCount = COUNT(t.Refnum), 
				CTGCount = SUM(t.CTGs),
				NMC = SUM(NMC), 
				AvgNMC = AVG(NMC),
				PriorAdjNetGWH = SUM(gtc.PriorAdjNetMWH)/1000, 
				AdjNetMWH = SUM(gtc.AdjNetMWH)/1000,
				AvgPriorAdjNetGWH = AVG(gtc.PriorAdjNetMWH)/1000, 
				AvgAdjNetMWH = AVG(gtc.AdjNetMWH)/1000,
				FuelGroup = GlobalDB.dbo.WhatsThere(CASE b.FuelGroup WHEN 'Coal' THEN 'C' WHEN 'Gas & Oil' THEN 'G&O' ELSE b.FuelGroup END),
				LoadType = GlobalDB.dbo.WhatsThere(CASE b.LoadType WHEN 'BASE' THEN 'B' WHEN 'PEAK' THEN 'P' WHEN 'INT' THEN 'I' ELSE b.LoadType END),
				NCF2Yr = GlobalDB.dbo.WtAvgNN(nf.NCF2Yr, nf.WPH2Yr),
				NOF2Yr = GlobalDB.dbo.WtAvgNN(nf.NOF2Yr, nf.NOF2YrWtFactor),
				ServiceHours = AVG(ISNULL(nf.ServiceHrs,0)),
				Age = AVG(CASE WHEN nf.Age <> 0 THEN nf.Age END),
				InternalUseSteamAux = GlobalDB.dbo.WtAvg(gtc.AuxiliaryPcnt, gtc.PotentialGrossMWH),
				InternalUseScrubberAux = GlobalDB.dbo.WtAvg(gtc.ScrubberPcnt, gtc.PotentialGrossMWH),
				InternalUseElectricAux = GlobalDB.dbo.WtAvg(gtc.StationSvcPcnt, gtc.PotentialGrossMWH),
				InternalUseTotal = GlobalDB.dbo.WtAvg(gtc.InternalUsagePcnt, gtc.PotentialGrossMWH),
				AutoControlPct = AVG(gtc.AutoGenCtrlPcnt),
				BoilerOverhaulIntervalMonths = SUM(BOILYrs)/SUM(BOILCount)*12,
				HPSOverhaulIntervalMonths = SUM(STGHPSYrs)/SUM(STGHPSCount)*12,
				IPSOverhaulIntervalMonths = SUM(STGIPSYrs)/SUM(STGIPSCount)*12,
				LPSOverhaulIntervalMonths = SUM(STGLPSYrs)/SUM(STGLPSCount)*12,
				GeneratorOverhaulIntervalMonths = SUM(STGGENYrs)/SUM(STGGENCount)*12,
				CTOverhaulIntervalMonths = SUM(CTGTURBYrs)/SUM(CTGTURBCount)*12,
				HRSGOverhaulIntervalMonths = SUM(HRSGYrs)/SUM(HRSGCount)*12,
				CashLessFuelPerRunHour = AVG(cprh.Total),
				FixedCostPerRunHour = AVG(cprh.Fixed),
				VariableCostLessFuelPerRunHour = AVG(cprh.VarLessFuel),
				MaintenanceCostPerRunHour = AVG(CASE WHEN mprh.Maint <> 0 THEN mprh.Maint END),
				NonOverhaulCostPerRunHour = AVG(CASE WHEN mprh.Maint > 0 THEN mprh.NonOverhaul END),
				AnnualOverhaulCostPerRunHour = AVG(CASE WHEN mprh.Maint > 0 THEN mprh.Overhaul END),
				LTSACostPerRunHour = AVG(CASE WHEN mprh.Maint > 0 THEN mprh.LTSA END)
				
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN OhMaintByRefnum omr ON omr.Refnum = t.Refnum
				INNER JOIN CostPerRunHr cprh ON cprh.Refnum = t.Refnum
				INNER JOIN MaintPerRunHr mprh ON mprh.Refnum = t.Refnum
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				
			
			
				--+ CASE WHEN rg.FuelGroup IS NOT NULL THEN ' AND (b.FuelGroup =''' + rg.FuelGroup + ''')' ELSE '' END
				--+ CASE WHEN rg.Regulated IS NOT NULL THEN ' AND (t.Regulated = ''' + rg.Regulated + ''')' ELSE '' END
				--+ CASE WHEN rg.Continent IS NOT NULL THEN ' AND (t.Continent = ''' + rg.Continent + ''')' ELSE '' END
				--+ CASE WHEN rg.region IS NOT NULL THEN ' AND (t.Region = ''' + rg.Region + ''')' ELSE '' END
				--+ CASE WHEN rg.loadtype IS NOT NULL THEN ' AND (b.LoadType = ''' + rg.LoadType + ''')' ELSE '' END
				--+ CASE WHEN rg.SteamGasOil IS NOT NULL THEN ' AND (b.SteamGasOil = ''' + rg.SteamGasOil + ''')' ELSE '' END
				--+ CASE WHEN rg.FuelType IS NOT NULL THEN ' AND (t.FuelType = ''' + rg.FuelType + ''')' ELSE '' END
				--+ CASE WHEN rg.GasOilNMCGroup IS NOT NULL THEN ' AND (b.GasOilNMCGroup = ''' + CAST(rg.GasOilNMCGroup AS VARCHAR) + ''')' ELSE '' END
				--+ CASE WHEN rg.StartsGroup IS NOT NULL THEN ' AND (st.StartsGroup = ''' + CAST(rg.StartsGroup AS VARCHAR) + ''')' ELSE '' END
				--+ CASE WHEN rg.ccNMCGroup IS NOT NULL THEN ' AND (b.CCNMCGroup = ''' + CAST(rg.ccNMCGroup AS VARCHAR) + ''')' ELSE '' END
				--+ CASE WHEN rg.LTSA IS NOT NULL THEN ' AND (b.ltsa = ''' + rg.LTSA + ''')' ELSE '' END
				--+ CASE WHEN rg.CRVSizeGroup IS NOT NULL THEN ' AND (b.CRVSizeGroup = ''' + rg.CRVSizeGroup + ''')' ELSE '' END
				--+ CASE WHEN rg.CogenElec IS NOT NULL THEN ' AND (b.CogenElec = ''' + rg.CogenElec + ''')' ELSE '' END
				--+ CASE WHEN rg.FTemp IS NOT NULL THEN ' AND (b.FTemp = ''' + CAST(rg.FTemp AS VARCHAR) + ''')' ELSE '' END
				--+ CASE WHEN rg.CTGS IS NOT NULL THEN ' AND (b.CTGS = ''' + CAST(rg.CTGS AS VARCHAR) + ''')' ELSE '' END
				--+ CASE WHEN rg.NCF2Yr IS NOT NULL THEN ' AND (b.NCF2Yr = ''' + CAST(rg.NCF2Yr AS VARCHAR) + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''

			
			--and this is the actual query being run
			EXEC ('INSERT ReportGroupGeneralData (RefNum, 
					ReportTitle, 
					ListName, 
					UnitCount, 
					CTGCount, 
					NMC, 
					AvgNMC, 
					PriorAdjNetGWH, 
					AdjNetMWH, 
					AvgPriorAdjNetGWH, 
					AvgAdjNetMWH, 
					FuelGroup, 
					LoadType, 
					NCF2Yr, 
					NOF2Yr,
					ServiceHours,
					Age,
					InternalUseSteamAux,
					InternalUseScrubberAux,
					InternalUseElectricAux,
					InternalUseTotal,
					AutoControlPct,
					BoilerOverhaulIntervalMonths,
					HPSOverhaulIntervalMonths,
					IPSOverhaulIntervalMonths,
					LPSOverhaulIntervalMonths,
					GeneratorOverhaulIntervalMonths,
					CTOverhaulIntervalMonths,
					HRSGOverhaulIntervalMonths,
					CashLessFuelPerRunHour,
					FixedCostPerRunHour,
					VariableCostLessFuelPerRunHour,
					MaintenanceCostPerRunHour,
					NonOverhaulCostPerRunHour,
					AnnualOverhaulCostPerRunHour,
					LTSACostPerRunHour)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					UnitCount = COUNT(t.Refnum), 
					CTGCount = SUM(t.CTGs),
					NMC = SUM(NMC), 
					AvgNMC = AVG(NMC),
					PriorAdjNetGWH = SUM(gtc.PriorAdjNetMWH)/1000, 
					AdjNetMWH = SUM(gtc.AdjNetMWH)/1000,
					AvgPriorAdjNetGWH = AVG(gtc.PriorAdjNetMWH)/1000, 
					AvgAdjNetMWH = AVG(gtc.AdjNetMWH)/1000,
					FuelGroup = GlobalDB.dbo.WhatsThere(CASE b.FuelGroup WHEN ''Coal'' THEN ''C'' WHEN ''Gas & Oil'' THEN ''G&O'' ELSE b.FuelGroup END),
					LoadType = GlobalDB.dbo.WhatsThere(CASE b.LoadType WHEN ''BASE'' THEN ''B'' WHEN ''PEAK'' THEN ''P'' WHEN ''INT'' THEN ''I'' ELSE b.LoadType END),
					NCF2Yr = GlobalDB.dbo.WtAvgNN(nf.NCF2Yr, nf.WPH2Yr),
					NOF2Yr = GlobalDB.dbo.WtAvgNN(nf.NOF2Yr, nf.NOF2YrWtFactor),
					ServiceHours = AVG(ISNULL(nf.ServiceHrs,0)),
					Age = AVG(CASE WHEN nf.Age <> 0 THEN nf.Age END),
					InternalUseSteamAux = GlobalDB.dbo.WtAvg(gtc.AuxiliaryPcnt, gtc.PotentialGrossMWH),
					InternalUseScrubberAux = GlobalDB.dbo.WtAvg(gtc.ScrubberPcnt, gtc.PotentialGrossMWH),
					InternalUseElectricAux = GlobalDB.dbo.WtAvg(gtc.StationSvcPcnt, gtc.PotentialGrossMWH),
					InternalUseTotal = GlobalDB.dbo.WtAvg(gtc.InternalUsagePcnt, gtc.PotentialGrossMWH),
					AutoControlPct = AVG(gtc.AutoGenCtrlPcnt),
					BoilerOverhaulIntervalMonths = SUM(BOILYrs)/SUM(BOILCount)*12,
					HPSOverhaulIntervalMonths = SUM(STGHPSYrs)/SUM(STGHPSCount)*12,
					IPSOverhaulIntervalMonths = SUM(STGIPSYrs)/SUM(STGIPSCount)*12,
					LPSOverhaulIntervalMonths = SUM(STGLPSYrs)/SUM(STGLPSCount)*12,
					GeneratorOverhaulIntervalMonths = SUM(STGGENYrs)/SUM(STGGENCount)*12,
					CTOverhaulIntervalMonths = SUM(CTGTURBYrs)/SUM(CTGTURBCount)*12,
					HRSGOverhaulIntervalMonths = SUM(HRSGYrs)/SUM(HRSGCount)*12,
					CashLessFuelPerRunHour = AVG(cprh.Total),
					FixedCostPerRunHour = AVG(cprh.Fixed),
					VariableCostLessFuelPerRunHour = AVG(cprh.VarLessFuel),
					MaintenanceCostPerRunHour = AVG(CASE WHEN mprh.Maint <> 0 THEN mprh.Maint END),
					NonOverhaulCostPerRunHour = AVG(CASE WHEN mprh.Maint > 0 THEN mprh.NonOverhaul END),
					AnnualOverhaulCostPerRunHour = AVG(CASE WHEN mprh.Maint > 0 THEN mprh.Overhaul END),
					LTSACostPerRunHour = AVG(CASE WHEN mprh.Maint > 0 THEN mprh.LTSA END)
				FROM TSort t INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					LEFT JOIN OhMaintByRefnum omr ON omr.Refnum = t.Refnum
					INNER JOIN CostPerRunHr cprh ON cprh.Refnum = t.Refnum
					INNER JOIN MaintPerRunHr mprh ON mprh.Refnum = t.Refnum
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END


