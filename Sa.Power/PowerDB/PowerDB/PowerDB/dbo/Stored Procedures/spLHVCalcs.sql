﻿CREATE PROC [dbo].[spLHVCalcs](@SiteID SiteID)
AS

DECLARE @LHVFactors TABLE (
	FuelType char(6) NOT NULL,
	HHVperLHV real NOT NULL)
INSERT @LHVFactors VALUES ('NatGas', 1.1)
INSERT @LHVFactors VALUES ('H2Gas', 1.15)
INSERT @LHVFactors VALUES ('OffGas', 1.1)
INSERT @LHVFactors VALUES ('Jet', 1.069)
INSERT @LHVFactors VALUES ('Diesel', 1.065)
INSERT @LHVFactors VALUES ('FOil', 1.058)

UPDATE Fuel
SET MBTU = MBTULHV*l.HHVPerLHV, CostMBTULocal = CostMBTULHVLocal/l.HHVPerLHV
FROM Fuel INNER JOIN @LHVFactors l ON l.FuelType = Fuel.FuelType
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND Fuel.FuelType IN ('NatGas', 'H2Gas', 'OffGas') AND ReportedLHV = 'Y'

UPDATE Fuel
SET MBTULHV = MBTU/l.HHVPerLHV, CostMBTULHVLocal = CostMBTULocal*l.HHVPerLHV
FROM Fuel INNER JOIN @LHVFactors l ON l.FuelType = Fuel.FuelType
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND Fuel.FuelType IN ('NatGas', 'H2Gas', 'OffGas') AND ReportedLHV <> 'Y'

UPDATE Fuel 
SET HeatValue = LHV*l.HHVPerLHV
FROM Fuel INNER JOIN @LHVFactors l ON l.FuelType = Fuel.FuelType
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND Fuel.FuelType IN ('Jet', 'Diesel', 'FOil') AND ReportedLHV = 'Y'

UPDATE Fuel 
SET LHV = HeatValue/l.HHVPerLHV
FROM Fuel INNER JOIN @LHVFactors l ON l.FuelType = Fuel.FuelType
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND Fuel.FuelType IN ('Jet', 'Diesel', 'FOil') AND ReportedLHV <> 'Y'

UPDATE Fuel 
SET MBTU = HeatValue * KGal, MBTULHV = LHV * KGal,
CostMBTULocal = CASE WHEN HeatValue = 0 THEN NULL ELSE CostGalLocal / (HeatValue/1000) END,
CostMBTULHVLocal = CASE WHEN LHV = 0 THEN NULL ELSE CostGalLocal / (LHV/1000) END
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND FuelType IN ('Jet', 'Diesel', 'FOil')

UPDATE Coal
SET LHV = HeatValue * CASE Biomass WHEN 'Y' THEN 0.91 ELSE 0.97 END - 10.3*MoistPcnt
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND ReportedLHV = 'N'

UPDATE Coal
SET HeatValue = (LHV + 10.3*MoistPcnt)/(CASE Biomass WHEN 'Y' THEN 0.91 ELSE 0.97 END)
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND ReportedLHV = 'Y'
