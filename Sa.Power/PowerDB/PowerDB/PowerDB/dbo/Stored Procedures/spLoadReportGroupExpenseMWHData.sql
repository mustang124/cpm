﻿



CREATE PROC [dbo].[spLoadReportGroupExpenseMWHData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

		DECLARE @temptable TABLE (totcash REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL


	
	--remove the existing record in the table
	DELETE FROM ReportGroupExpenseMWHData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it


		INSERT @temptable
		SELECT TOP 2 omwh.totcash AS totcash
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = 'MWH'
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = 'MWH'
				INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
				INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
				LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
				LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		ORDER BY omwh.TotCash ASC

		SELECT @minvalue = AVG(totcash) FROM @temptable 

		DELETE FROM @temptable 

		INSERT @temptable
		SELECT TOP 2 omwh.totcash AS totcash
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = 'MWH'
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = 'MWH'
				INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
				INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
				LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
				LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		ORDER BY omwh.TotCash DESC

		SELECT @maxvalue = AVG(totcash) FROM @temptable 
		
		
		
			INSERT ReportGroupExpenseMWHData (RefNum
			  ,ReportTitle
			  ,ListName
			  ,FixedExpMWH
			  ,VariableExpMWH
			  ,TotalCashExpMWH
			  ,TotalCashRangeMinimumMWH
			  ,TotalCashRangeMaximumMWH
			  ,OCCWagesMWH
			  ,MPSWagesMWH
			  ,SubTotalWagesMWH
			  ,OCCBenefitsMWH
			  ,MPSBenefitsMWH
			  ,SubTotalBenefitsMWH
			  ,CentralOCCWageBenMWH
			  ,CentralMPSWageBenMWH
			  ,MaintMaterialsMWH
			  ,ContractMaintLaborMWH
			  ,ContractMaintMaterialMWH
			  ,ContractMaintLumpSumMWH
			  ,OtherContractServicesMWH
			  ,OverhaulAdjMWH
			  ,LTSAAdjMWH
			  ,EnvironmentExpMWH
			  ,PropertyTaxesMWH
			  ,OtherTaxesMWH
			  ,InsuranceMWH
			  ,AllocatedAGPersCostMWH
			  ,AllocatedAGNonPersCostMWH
			  ,OtherFixedExpMWH
			  ,SubTotalFixedExpMWH
			  ,ChemicalsMWH
			  ,WaterMWH
			  ,GasMWH
			  ,LiquidMWH
			  ,SolidMWH
			  ,SubTotalFuelConsumedMWH
			  ,SO2EmissionAdjMWH
			  ,NOxEmissionAdjMWH
			  ,CO2EmissionAdjMWH
			  ,OtherVarExpMWH
			  ,SubTotalVarExpMWH
			  ,DepreciationMWH
			  ,InterestExpMWH
			  ,SupplyCarryCostMWH
			  ,FuelInvCarryCostMWH
			  ,SubTotalNonCashExpMWH
			  ,GrandTotalExpMWH
			  ,PlantSiteWagesBenefitsMWH
			  ,PlantCentralEmployeesMWH
			  ,PlantMaterialsMWH
			  ,PlantOverhaulsMWH
			  ,PlantLTSAPSAMWH
			  ,PlantOtherVarMWH
			  ,PlantMiscMWH
			  ,PlantManageExpLessFuelMWH
			  ,AncillaryRevenueMWH
			  ,AshSalesMWH
			  ,OtherChemicalSalesMWH
			  ,OtherRevenueMWH
			  ,TotalRevenueMWH
			  ,AshOperationsCostMWH
			  ,AshDisposalCostMWH
			  ,AshTotalCostMWH
			  ,ScrubberLimeCostMWH
			  ,ScrubberOtherChemCostMWH
			  ,ScrubberAuxCostMWH
			  ,ScrubberOtherNonMaintCostMWH
			  ,ScrubberTotalCostMWH
			  ,ScrubbingCostTonMWH)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				FixedExpMWH = GlobalDB.dbo.WtAvg(omwh.STFixed, gtc.AdjNetMWH),
				VariableExpMWH = GlobalDB.dbo.WtAvg(omwh.STVar, gtc.AdjNetMWH),
				TotalCashExpMWH = GlobalDB.dbo.WtAvg(omwh.TotCash, gtc.AdjNetMWH),
				TotalCashRangeMinimumMWH = @minvalue,
				TotalCashRangeMaximumMWH = @maxvalue,
				OCCWagesMWH = GlobalDB.dbo.WtAvg(omwh.OCCWages, gtc.AdjNetMWH),
				MPSWagesMWH = GlobalDB.dbo.WtAvg(omwh.MPSWages, gtc.AdjNetMWH),
				SubTotalWagesMWH = GlobalDB.dbo.WtAvg(omwh.STWages, gtc.AdjNetMWH),
				OCCBenefitsMWH = GlobalDB.dbo.WtAvg(omwh.OCCBenefits, gtc.AdjNetMWH),
				MPSBenefitsMWH = GlobalDB.dbo.WtAvg(omwh.MPSBenefits, gtc.AdjNetMWH),
				SubTotalBenefitsMWH = GlobalDB.dbo.WtAvg(omwh.STBenefits, gtc.AdjNetMWH),
				CentralOCCWageBenMWH = GlobalDB.dbo.WtAvg(omwh.CentralOCCWagesBen, gtc.AdjNetMWH),
				CentralMPSWageBenMWH = GlobalDB.dbo.WtAvg(omwh.CentralMPSWagesBen, gtc.AdjNetMWH),
				MaintMaterialsMWH = GlobalDB.dbo.WtAvg(omwh.MaintMatl, gtc.AdjNetMWH),
				ContractMaintLaborMWH = GlobalDB.dbo.WtAvg(omwh.ContMaintLabor, gtc.AdjNetMWH),
				ContractMaintMaterialMWH = GlobalDB.dbo.WtAvg(omwh.ContMaintMatl, gtc.AdjNetMWH),
				ContractMaintLumpSumMWH = GlobalDB.dbo.WtAvg(omwh.ContMaintLumpSum, gtc.AdjNetMWH),
				OtherContractServicesMWH = GlobalDB.dbo.WtAvg(omwh.OthContSvc, gtc.AdjNetMWH),
				OverhaulAdjMWH = GlobalDB.dbo.WtAvg(omwh.OverhaulAdj, gtc.AdjNetMWH),
				LTSAAdjMWH = GlobalDB.dbo.WtAvg(omwh.LTSAAdj, gtc.AdjNetMWH),
				EnvironmentExpMWH = GlobalDB.dbo.WtAvg(omwh.Envir, gtc.AdjNetMWH),
				PropertyTaxesMWH = GlobalDB.dbo.WtAvg(omwh.PropTax, gtc.AdjNetMWH),
				OtherTaxesMWH = GlobalDB.dbo.WtAvg(omwh.OthTax, gtc.AdjNetMWH),
				InsuranceMWH = GlobalDB.dbo.WtAvg(omwh.Insurance, gtc.AdjNetMWH),
				AllocatedAGPersCostMWH = GlobalDB.dbo.WtAvg(omwh.AGPers, gtc.AdjNetMWH),
				AllocatedAGNonPersCostMWH = GlobalDB.dbo.WtAvg(omwh.AGNonPers, gtc.AdjNetMWH),
				OtherFixedExpMWH = GlobalDB.dbo.WtAvg(omwh.OthFixed, gtc.AdjNetMWH),
				SubTotalFixedExpMWH = GlobalDB.dbo.WtAvg(omwh.STFixed, gtc.AdjNetMWH),
				ChemicalsMWH = GlobalDB.dbo.WtAvg(omwh.Chemicals, gtc.AdjNetMWH),
				WaterMWH = GlobalDB.dbo.WtAvg(omwh.Water, gtc.AdjNetMWH),
				GasMWH = GlobalDB.dbo.WtAvg(omwh.PurGas, gtc.AdjNetMWH),
				LiquidMWH = GlobalDB.dbo.WtAvg(omwh.PurLiquid, gtc.AdjNetMWH),
				SolidMWH = GlobalDB.dbo.WtAvg(omwh.PurSolid, gtc.AdjNetMWH),
				SubTotalFuelConsumedMWH = GlobalDB.dbo.WtAvg(omwh.STFuelCost, gtc.AdjNetMWH),
				SO2EmissionAdjMWH = GlobalDB.dbo.WtAvg(omwh.SO2EmissionAdj, gtc.AdjNetMWH),
				NOxEmissionAdjMWH = GlobalDB.dbo.WtAvg(omwh.NOxEmissionAdj, gtc.AdjNetMWH),
				COEmissionAdjMWH = GlobalDB.dbo.WtAvg(omwh.CO2EmissionAdj, gtc.AdjNetMWH),
				OtherVarExpMWH = GlobalDB.dbo.WtAvg(omwh.OthVar, gtc.AdjNetMWH),
				SubTotalVarExpMWH = GlobalDB.dbo.WtAvg(omwh.STVar, gtc.AdjNetMWH),
				DepreciationMWH = GlobalDB.dbo.WtAvg(omwh.Depreciation, gtc.AdjNetMWH),
				InterestExpMWH = GlobalDB.dbo.WtAvg(omwh.Interest, gtc.AdjNetMWH),
				SupplyCarryCostMWH = GlobalDB.dbo.WtAvg(omwh.Supply, gtc.AdjNetMWH),
				FuelInvCarryCostMWH = GlobalDB.dbo.WtAvg(omwh.FuelInven, gtc.AdjNetMWH),
				SubTotalNonCashExpMWH = GlobalDB.dbo.WtAvg(omwh.STNonCash, gtc.AdjNetMWH),
				GrandTotalExpMWH = GlobalDB.dbo.WtAvg(omwh.GrandTot, gtc.AdjNetMWH),
				PlantSiteWagesBenefitsMWH = GlobalDB.dbo.WtAvg(plnt.SiteWagesBen, gtc.AdjNetMWH),
				PlantCentralEmployeesMWH = GlobalDB.dbo.WtAvg(plnt.CentralContract, gtc.AdjNetMWH),
				PlantMaterialsMWH = GlobalDB.dbo.WtAvg(plnt.MaintMatl, gtc.AdjNetMWH),
				PlantOverhaulsMWH = GlobalDB.dbo.WtAvg(plnt.OverhaulAdj, gtc.AdjNetMWH),
				PlantLTSAPSAMWH = GlobalDB.dbo.WtAvg(plnt.LTSAAdj, gtc.AdjNetMWH),
				PlantOtherVarMWH = GlobalDB.dbo.WtAvg(plnt.OthVar, gtc.AdjNetMWH),
				PlantMiscMWH = GlobalDB.dbo.WtAvg(plnt.Misc, gtc.AdjNetMWH),
				PlantManageExpLessFuelMWH = GlobalDB.dbo.WtAvg(omwh.ActCashLessFuel, gtc.AdjNetMWH),
				AncillaryRevenueMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.TotAncillaryRevenue,0))/SUM(gtc.AdjNetMWH) END)*1000,
				AshSalesMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.RevAsh,0))/SUM(gtc.AdjNetMWH) END)*1000,
				OtherChemicalSalesMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.RevOth,0))/SUM(gtc.AdjNetMWH) END)*1000,
				OtherRevenueMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.OthNonT7Revenue,0))/SUM(gtc.AdjNetMWH) END)*1000,
				TotalRevenueMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.TotRevenueRevised,0))/SUM(gtc.AdjNetMWH) END)*1000,
				AshOperationsCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(m.AshCostOP,0))/SUM(gtc.AdjNetMWH) END)*1000,
				AshDisposalCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(m.AshCostDisp,0))/SUM(gtc.AdjNetMWH) END)*1000,
				AshTotalCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.TotAshHandling,0))/SUM(gtc.AdjNetMWH) END)*1000,
				ScrubberLimeCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrCostLime,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
				ScrubberOtherChemCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrCostChem,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
				ScrubberAuxCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrubberPowerCost,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
				ScrubberOtherNonMaintCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrCostNMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
				ScrubberTotalCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.TotScrubberNonMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
				ScrubbingCostTonMWH = GlobalDB.dbo.WtAvgNZ(so2.ScrubbingIndex, so2.TonsSO2Removed)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = 'MWH'
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = 'MWH'
				INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
				INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
				LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
				LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it
		--DECLARE @temptable TABLE (totcash REAL)
		--DECLARE @minvalue REAL
		--DECLARE @maxvalue REAL

		EXEC ('		DECLARE @temptable TABLE (totcash REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL
		
		
		INSERT @temptable
		SELECT TOP 2 omwh.totcash AS totcash
				FROM TSort t 
					INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = ''MWH''
					INNER JOIN GenerationTotCalc gtc ON gtc.refnum = t.refnum
					INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = ''MWH''
					INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
					INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
					LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
					LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
				' ORDER BY omwh.TotCash ASC 

		SELECT @minvalue =AVG(totcash) FROM @temptable 

		DELETE FROM @temptable 

		
		INSERT @temptable
		SELECT TOP 2 omwh.totcash AS totcash
				FROM TSort t 
					INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = ''MWH''
					INNER JOIN GenerationTotCalc gtc ON gtc.refnum = t.refnum
					INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = ''MWH''
					INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
					INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
					LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
					LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
				' ORDER BY omwh.TotCash DESC 

		SELECT @maxvalue = AVG(totcash)  FROM @temptable 
		
		

			
			INSERT ReportGroupExpenseMWHData (RefNum
				  ,ReportTitle
				  ,ListName
				  ,FixedExpMWH
				  ,VariableExpMWH
				  ,TotalCashExpMWH
				  ,TotalCashRangeMinimumMWH
				  ,TotalCashRangeMaximumMWH
				  ,OCCWagesMWH
				  ,MPSWagesMWH
				  ,SubTotalWagesMWH
				  ,OCCBenefitsMWH
				  ,MPSBenefitsMWH
				  ,SubTotalBenefitsMWH
				  ,CentralOCCWageBenMWH
				  ,CentralMPSWageBenMWH
				  ,MaintMaterialsMWH
				  ,ContractMaintLaborMWH
				  ,ContractMaintMaterialMWH
				  ,ContractMaintLumpSumMWH
				  ,OtherContractServicesMWH
				  ,OverhaulAdjMWH
				  ,LTSAAdjMWH
				  ,EnvironmentExpMWH
				  ,PropertyTaxesMWH
				  ,OtherTaxesMWH
				  ,InsuranceMWH
				  ,AllocatedAGPersCostMWH
				  ,AllocatedAGNonPersCostMWH
				  ,OtherFixedExpMWH
				  ,SubTotalFixedExpMWH
				  ,ChemicalsMWH
				  ,WaterMWH
				  ,GasMWH
				  ,LiquidMWH
				  ,SolidMWH
				  ,SubTotalFuelConsumedMWH
				  ,SO2EmissionAdjMWH
				  ,NOxEmissionAdjMWH
				  ,CO2EmissionAdjMWH
				  ,OtherVarExpMWH
				  ,SubTotalVarExpMWH
				  ,DepreciationMWH
				  ,InterestExpMWH
				  ,SupplyCarryCostMWH
				  ,FuelInvCarryCostMWH
				  ,SubTotalNonCashExpMWH
				  ,GrandTotalExpMWH
				  ,PlantSiteWagesBenefitsMWH
				  ,PlantCentralEmployeesMWH
				  ,PlantMaterialsMWH
				  ,PlantOverhaulsMWH
				  ,PlantLTSAPSAMWH
				  ,PlantOtherVarMWH
				  ,PlantMiscMWH
				  ,PlantManageExpLessFuelMWH
				  ,AncillaryRevenueMWH
				  ,AshSalesMWH
				  ,OtherChemicalSalesMWH
				  ,OtherRevenueMWH
				  ,TotalRevenueMWH
				  ,AshOperationsCostMWH
				  ,AshDisposalCostMWH
				  ,AshTotalCostMWH
				  ,ScrubberLimeCostMWH
				  ,ScrubberOtherChemCostMWH
				  ,ScrubberAuxCostMWH
				  ,ScrubberOtherNonMaintCostMWH
				  ,ScrubberTotalCostMWH
				  ,ScrubbingCostTonMWH)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					FixedExpMWH = GlobalDB.dbo.WtAvg(omwh.STFixed, gtc.AdjNetMWH),
					VariableExpMWH = GlobalDB.dbo.WtAvg(omwh.STVar, gtc.AdjNetMWH),
					TotalCashExpMWH = GlobalDB.dbo.WtAvg(omwh.TotCash, gtc.AdjNetMWH),
					TotalCashRangeMinimumMWH = @minvalue,
					TotalCashRangeMaximumMWH = @maxvalue,
					OCCWagesMWH = GlobalDB.dbo.WtAvg(omwh.OCCWages, gtc.AdjNetMWH),
					MPSWagesMWH = GlobalDB.dbo.WtAvg(omwh.MPSWages, gtc.AdjNetMWH),
					SubTotalWagesMWH = GlobalDB.dbo.WtAvg(omwh.STWages, gtc.AdjNetMWH),
					OCCBenefitsMWH = GlobalDB.dbo.WtAvg(omwh.OCCBenefits, gtc.AdjNetMWH),
					MPSBenefitsMWH = GlobalDB.dbo.WtAvg(omwh.MPSBenefits, gtc.AdjNetMWH),
					SubTotalBenefitsMWH = GlobalDB.dbo.WtAvg(omwh.STBenefits, gtc.AdjNetMWH),
					CentralOCCWageBenMWH = GlobalDB.dbo.WtAvg(omwh.CentralOCCWagesBen, gtc.AdjNetMWH),
					CentralMPSWageBenMWH = GlobalDB.dbo.WtAvg(omwh.CentralMPSWagesBen, gtc.AdjNetMWH),
					MaintMaterialsMWH = GlobalDB.dbo.WtAvg(omwh.MaintMatl, gtc.AdjNetMWH),
					ContractMaintLaborMWH = GlobalDB.dbo.WtAvg(omwh.ContMaintLabor, gtc.AdjNetMWH),
					ContractMaintMaterialMWH = GlobalDB.dbo.WtAvg(omwh.ContMaintMatl, gtc.AdjNetMWH),
					ContractMaintLumpSumMWH = GlobalDB.dbo.WtAvg(omwh.ContMaintLumpSum, gtc.AdjNetMWH),
					OtherContractServicesMWH = GlobalDB.dbo.WtAvg(omwh.OthContSvc, gtc.AdjNetMWH),
					OverhaulAdjMWH = GlobalDB.dbo.WtAvg(omwh.OverhaulAdj, gtc.AdjNetMWH),
					LTSAAdjMWH = GlobalDB.dbo.WtAvg(omwh.LTSAAdj, gtc.AdjNetMWH),
					EnvironmentExpMWH = GlobalDB.dbo.WtAvg(omwh.Envir, gtc.AdjNetMWH),
					PropertyTaxesMWH = GlobalDB.dbo.WtAvg(omwh.PropTax, gtc.AdjNetMWH),
					OtherTaxesMWH = GlobalDB.dbo.WtAvg(omwh.OthTax, gtc.AdjNetMWH),
					InsuranceMWH = GlobalDB.dbo.WtAvg(omwh.Insurance, gtc.AdjNetMWH),
					AllocatedAGPersCostMWH = GlobalDB.dbo.WtAvg(omwh.AGPers, gtc.AdjNetMWH),
					AllocatedAGNonPersCostMWH = GlobalDB.dbo.WtAvg(omwh.AGNonPers, gtc.AdjNetMWH),
					OtherFixedExpMWH = GlobalDB.dbo.WtAvg(omwh.OthFixed, gtc.AdjNetMWH),
					SubTotalFixedExpMWH = GlobalDB.dbo.WtAvg(omwh.STFixed, gtc.AdjNetMWH),
					ChemicalsMWH = GlobalDB.dbo.WtAvg(omwh.Chemicals, gtc.AdjNetMWH),
					WaterMWH = GlobalDB.dbo.WtAvg(omwh.Water, gtc.AdjNetMWH),
					GasMWH = GlobalDB.dbo.WtAvg(omwh.PurGas, gtc.AdjNetMWH),
					LiquidMWH = GlobalDB.dbo.WtAvg(omwh.PurLiquid, gtc.AdjNetMWH),
					SolidMWH = GlobalDB.dbo.WtAvg(omwh.PurSolid, gtc.AdjNetMWH),
					SubTotalFuelConsumedMWH = GlobalDB.dbo.WtAvg(omwh.STFuelCost, gtc.AdjNetMWH),
					SO2EmissionAdjMWH = GlobalDB.dbo.WtAvg(omwh.SO2EmissionAdj, gtc.AdjNetMWH),
					NOxEmissionAdjMWH = GlobalDB.dbo.WtAvg(omwh.NOxEmissionAdj, gtc.AdjNetMWH),
					COEmissionAdjMWH = GlobalDB.dbo.WtAvg(omwh.CO2EmissionAdj, gtc.AdjNetMWH),
					OtherVarExpMWH = GlobalDB.dbo.WtAvg(omwh.OthVar, gtc.AdjNetMWH),
					SubTotalVarExpMWH = GlobalDB.dbo.WtAvg(omwh.STVar, gtc.AdjNetMWH),
					DepreciationMWH = GlobalDB.dbo.WtAvg(omwh.Depreciation, gtc.AdjNetMWH),
					InterestExpMWH = GlobalDB.dbo.WtAvg(omwh.Interest, gtc.AdjNetMWH),
					SupplyCarryCostMWH = GlobalDB.dbo.WtAvg(omwh.Supply, gtc.AdjNetMWH),
					FuelInvCarryCostMWH = GlobalDB.dbo.WtAvg(omwh.FuelInven, gtc.AdjNetMWH),
					SubTotalNonCashExpMWH = GlobalDB.dbo.WtAvg(omwh.STNonCash, gtc.AdjNetMWH),
					GrandTotalExpMWH = GlobalDB.dbo.WtAvg(omwh.GrandTot, gtc.AdjNetMWH),
					PlantSiteWagesBenefitsMWH = GlobalDB.dbo.WtAvg(plnt.SiteWagesBen, gtc.AdjNetMWH),
					PlantCentralEmployeesMWH = GlobalDB.dbo.WtAvg(plnt.CentralContract, gtc.AdjNetMWH),
					PlantMaterialsMWH = GlobalDB.dbo.WtAvg(plnt.MaintMatl, gtc.AdjNetMWH),
					PlantOverhaulsMWH = GlobalDB.dbo.WtAvg(plnt.OverhaulAdj, gtc.AdjNetMWH),
					PlantLTSAPSAMWH = GlobalDB.dbo.WtAvg(plnt.LTSAAdj, gtc.AdjNetMWH),
					PlantOtherVarMWH = GlobalDB.dbo.WtAvg(plnt.OthVar, gtc.AdjNetMWH),
					PlantMiscMWH = GlobalDB.dbo.WtAvg(plnt.Misc, gtc.AdjNetMWH),
					PlantManageExpLessFuelMWH = GlobalDB.dbo.WtAvg(omwh.ActCashLessFuel, gtc.AdjNetMWH),
					AncillaryRevenueMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.TotAncillaryRevenue,0))/SUM(gtc.AdjNetMWH) END)*1000,
					AshSalesMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.RevAsh,0))/SUM(gtc.AdjNetMWH) END)*1000,
					OtherChemicalSalesMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.RevOth,0))/SUM(gtc.AdjNetMWH) END)*1000,
					OtherRevenueMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.OthNonT7Revenue,0))/SUM(gtc.AdjNetMWH) END)*1000,
					TotalRevenueMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.TotRevenueRevised,0))/SUM(gtc.AdjNetMWH) END)*1000,
					AshOperationsCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(m.AshCostOP,0))/SUM(gtc.AdjNetMWH) END)*1000,
					AshDisposalCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(m.AshCostDisp,0))/SUM(gtc.AdjNetMWH) END)*1000,
					AshTotalCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(mc.TotAshHandling,0))/SUM(gtc.AdjNetMWH) END)*1000,
					ScrubberLimeCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrCostLime,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
					ScrubberOtherChemCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrCostChem,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
					ScrubberAuxCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrubberPowerCost,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
					ScrubberOtherNonMaintCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.ScrCostNMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
					ScrubberTotalCostMWH = (CASE WHEN SUM(gtc.adjNetMWH) <> 0 THEN SUM(ISNULL(sc.TotScrubberNonMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else gtc.AdjNetMWH end) END)*1000,
					ScrubbingCostTonMWH = GlobalDB.dbo.WtAvgNZ(so2.ScrubbingIndex, so2.TonsSO2Removed)
				FROM TSort t 
					INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = ''MWH''
					INNER JOIN GenerationTotCalc gtc ON gtc.refnum = t.refnum
					INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = ''MWH''
					INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
					INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
					LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
					LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END





