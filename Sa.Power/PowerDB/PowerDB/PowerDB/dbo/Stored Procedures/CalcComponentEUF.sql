﻿CREATE PROC [dbo].[CalcComponentEUF] (@SiteID SiteID)
AS

	-- Delete any records in ComponentEUF for this Refnum
	Delete from ComponentEUF Where Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	DECLARE @turbHrs TABLE (
		Refnum varchar(12) NOT NULL,
		TurbineID char(6) NOT NULL,
		UtilityUnitCode char(6) NOT NULL,
		UnitType varchar(50) NOT NULL,
		UnitShortName char(10) NOT NULL,
		EvntYear smallint NOT NULL,
		E_PH float NULL
	)
	INSERT @turbHrs (Refnum, TurbineID, UtilityUnitCode, UnitType, UnitShortName, EvntYear, E_PH)
	SELECT nt.Refnum, nt.TurbineID, nt.UtilityUnitCode, s.UnitType, s.UnitShortName, t.EvntYear, eh.E_PH
	FROM dbo.TSort t INNER JOIN dbo.NERCTurbine nt on t.Refnum = nt.Refnum
	INNER JOIN GADSOS.GADSNG.Setup s on nt.UtilityUnitCode = s.UtilityUnitCode
	INNER JOIN GADSOS.GADSNG.EventHours eh ON eh.UnitShortName = s.UnitShortName AND (DatePart(yy, eh.TL_DateTime) =  t.EvntYear) AND (eh.Granularity = 'Yearly')
	WHERE t.SiteID = @SiteID AND eh.E_PH > 0

	DECLARE @CompEUFHrs TABLE (
		Refnum varchar(12) NOT NULL,
		TurbineID char(6) NOT NULL,
		SAIMajorEquip varchar(40) NULL,
		SAIMinorEquip varchar(40) NULL,
		EquipGroup char(8) NULL,
		EquivMWhrs float NULL,
		E_PH float NULL
	)
	INSERT @CompEUFHrs (Refnum, TurbineID, SAIMajorEquip, SAIMinorEquip, EquipGroup, EquivMWhrs)
	SELECT t.Refnum, t.TurbineID, cc.SAIMajorEquip, cc.SAIMinorEquip, lu.EquipGroup, SUM(e.EquivMWhrs)
	FROM @turbHrs t 
	INNER JOIN GADSOS.GADSNG.EventDetails e ON e.UnitShortName = t.UnitShortName AND (DatePart(yy, e.TL_DateTime) =  t.EvntYear) AND (e.Granularity = 'Yearly')
	INNER JOIN dbo.CauseCodes cc ON cc.Cause_Code = e.CauseCode
	INNER JOIN ComponentEUF_LU lu ON lu.UnitType = t.UnitType AND lu.SAIMajorEquip = cc.SAIMajorEquip AND lu.SAIMinorEquip = cc.SAIMinorEquip
	GROUP BY t.Refnum, t.TurbineID, cc.SAIMajorEquip, cc.SAIMinorEquip, lu.EquipGroup
	-- Equipment listed on T3 but did not find events for it
	INSERT @CompEUFHrs (Refnum, TurbineID, EquipGroup, EquivMWhrs)
	SELECT DISTINCT t.Refnum, t.TurbineID, lu.EquipGroup, 0
	FROM @turbHrs t INNER JOIN Equipment e ON e.Refnum = t.Refnum AND e.TurbineID = t.TurbineID
	INNER JOIN Component_LU lu ON lu.Component = e.EquipType
	WHERE NOT EXISTS (SELECT * FROM @CompEUFHrs c WHERE c.Refnum = t.Refnum AND c.TurbineID = t.TurbineID AND c.EquipGroup = lu.EquipGroup)
	AND EXISTS (SELECT * FROM MaintEquipCalc mec WHERE mec.Refnum = t.Refnum AND mec.EquipGroup = lu.EquipGroup AND mec.AnnMaintCostKUS > 0)
	-- Site Equipment listed on T3 but did not find events for it
	INSERT @CompEUFHrs (Refnum, TurbineID, EquipGroup, EquivMWhrs)
	SELECT t.Refnum, e.TurbineID, lu.EquipGroup, 0
	FROM TSort t INNER JOIN Equipment e ON e.Refnum = t.Refnum
	INNER JOIN Component_LU lu ON lu.Component = e.EquipType
	WHERE t.SiteID = @SiteID
	AND NOT EXISTS (SELECT * FROM @CompEUFHrs c WHERE c.Refnum = t.Refnum AND c.EquipGroup = lu.EquipGroup)
	AND EXISTS (SELECT * FROM MaintEquipCalc mec WHERE mec.Refnum = t.Refnum AND mec.EquipGroup = lu.EquipGroup AND mec.AnnMaintCostKUS > 0)
	-- Assign E_PH 
	UPDATE compEUF
	SET E_PH = ph.E_PH
	FROM @CompEUFHrs compEUF INNER JOIN @turbHrs ph ON ph.Refnum = compEUF.Refnum AND ph.TurbineID = compEUF.TurbineID
	-- Sum all E_PH for the site equipment	
	UPDATE compEUF
	SET E_PH = (SELECT SUM(ph.E_PH) FROM @turbHrs ph WHERE ph.Refnum = compEUF.Refnum)
	FROM @CompEUFHrs compEUF WHERE E_PH IS NULL
-- Here for debugging
SELECT * FROM @turbhrs
SELECT * FROM @CompEUFHrs
--
	-- ALTER the new records in ComponentEUF for this Refnum
	INSERT INTO ComponentEUF (Refnum, EquipGroup, EUF, E_PH)
	SELECT Refnum, EquipGroup, SUM(EquivMWhrs)/SUM(E_PH)*100, SUM(E_PH)
	FROM (
	SELECT Refnum, EquipGroup, TurbineID, SUM(EquivMWhrs) AS EquivMWhrs, AVG(E_PH) AS E_PH
	FROM @CompEUFHrs GROUP BY Refnum, EquipGroup, TurbineID) x
	GROUP BY Refnum, EquipGroup
