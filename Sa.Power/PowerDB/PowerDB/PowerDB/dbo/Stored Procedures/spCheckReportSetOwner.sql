﻿CREATE PROCEDURE [dbo].[spCheckReportSetOwner]
	@ReportSetID DD_ID = NULL, @ReportSetName varchar(30) = NULL,
	@CurrUser varchar(30) = NULL
AS
DECLARE @Result smallint
DECLARE @UName varchar(30), @UGroup sysname
IF @ReportSetID IS NULL AND @ReportSetName IS NULL
BEGIN
	RAISERROR('You must supply a ReportSet ID or Name.', 16, -1)
	RETURN -102
END
ELSE BEGIN
	IF @ReportSetID IS NULL
	BEGIN
		SELECT @ReportSetID = ReportSetID
		FROM ReportSets
		WHERE ReportSetName = @ReportSetName
		IF @ReportSetID IS NULL
			RETURN 1
		IF @CurrUser IS NULL
			SELECT @UName = User_Name()
		ELSE 
			SELECT @UName = @CurrUser


		IF NOT (EXISTS (SELECT * FROM sys.sysusers u INNER JOIN sys.sysmembers m ON m.memberuid = u.uid INNER JOIN sys.sysusers g ON g.uid = m.groupuid 
				WHERE u.name = @UName AND g.name in ('developer', 'Datagrp', 'db_owner')) OR @UName = 'dbo')
		BEGIN
			IF NOT EXISTS (SELECT * FROM ReportSets
				WHERE ReportSetID = @ReportSetID AND
				(Owner = @UName OR Owner IS NULL))
					RETURN -101
		END
/*		IF EXISTS (SELECT * FROM ReportQueue
				WHERE ReportSetID = @ReportSetID)
			RETURN -105
		ELSE
			RETURN 1
*/
	END
END

