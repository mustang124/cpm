﻿CREATE DEFAULT [dbo].[defBitTrue]
    AS 1;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue_dont_use].[Copy]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue_dont_use].[ClientTables]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue_dont_use].[CalcSummary]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue_dont_use].[MessageLog]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue_dont_use].[EmailNotify]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue_dont_use].[Calculate]';

