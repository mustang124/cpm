﻿CREATE DEFAULT [dbo].[defTimestamp]
    AS getdate();


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTimestamp]', @objname = N'[dbo].[ReportQueue].[TimeSubmitted]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTimestamp]', @objname = N'[dbo].[MessageLog].[MessageTime]';

