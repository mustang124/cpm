﻿CREATE RULE [dbo].[chkYorN]
    AS @Value IN ('Y', 'N') OR @Value IS NULL;


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Misc].[SNCR]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Misc].[NoNOxReduction]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[Report].[ReportGroups].[KeepUpdated]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ValStatSource].[Completed]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[Report].[ReportGroups].[Regulated]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ReportGroupsWkg].[LTSA]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[Report].[ReportGroups].[SuperCritical]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[Report].[ReportGroups].[SubCritical]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ReportGroupsWkg].[Regulated]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Misc].[DryLowNOxCombChamber]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Misc].[WaterInjection]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Misc].[SteamInjection]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Misc].[SCR]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[ValStat].[Completed]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Fuel].[ReportedLHV]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[SteamSales].[ReportedMBTU]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[SteamSales].[ExternalOrigin]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[PlantGenData].[BargainingUnit]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CTGData].[SingleShaft]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[Report].[ReportGroups].[PostProcess]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[HRSGAuxFiredBurners]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Coal].[Biomass]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Coal].[ReportedLHV]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[TSort].[Regulated]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[TSort].[CalcCommUnavail]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[Breaks].[LTSA]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompVBBParts]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompVBBMatl]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompVBBLabor]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompVBBNone]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompRotorParts]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompRotorMatl]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompRotorLabor]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CompRotorNone]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombSystemParts]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombSystemMatl]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombSystemLabor]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombSystemNone]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombVBBParts]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombVBBMatl]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombVBBLabor]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombVBBNone]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombTurbRotorParts]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombTurbRotorMatl]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombTurbRotorLabor]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[CombTurbRotorNone]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenStatorParts]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenStatorMatl]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenStatorLabor]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenStatorNone]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenRotorParts]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenRotorMatl]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenRotorLabor]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[GenRotorNone]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[LTSA].[OverheadCrane]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[YorN]';

