﻿CREATE TABLE [dbo].[SlideAnnotationText] (
    [SlideAnnotationTextID] INT            IDENTITY (1, 1) NOT NULL,
    [AnnotationText]        NVARCHAR (500) NULL
);

