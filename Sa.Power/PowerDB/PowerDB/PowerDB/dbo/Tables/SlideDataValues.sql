﻿CREATE TABLE [dbo].[SlideDataValues] (
    [SlideDataValueID] INT           IDENTITY (1, 1) NOT NULL,
    [SlideDataID]      INT           NULL,
    [SlideChartID]     INT           NULL,
    [ObjectID]         INT           NULL,
    [ParameterName]    VARCHAR (100) NOT NULL,
    [Value]            VARCHAR (255) NOT NULL,
    [IsDeleted]        BIT           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([SlideDataValueID] ASC)
);

