﻿CREATE TABLE [dbo].[UnitGaps] (
    [Refnum]                 [dbo].[Refnum] NOT NULL,
    [TargetGroup]            CHAR (12)      NOT NULL,
    [SiteWagesBenGap]        REAL           NULL,
    [CentralWagesBenGap]     REAL           NULL,
    [ContractGap]            REAL           NULL,
    [OverhaulAdjGap]         REAL           NULL,
    [MaterialsGap]           REAL           NULL,
    [TaxInsurEnvirGap]       REAL           NULL,
    [AGPersGap]              REAL           NULL,
    [OthFixedGap]            REAL           NULL,
    [OtherVarGap]            REAL           NULL,
    [OCCCompensationGap]     REAL           NULL,
    [MPSCompensationGap]     REAL           NULL,
    [OCCOVTPcntGap]          REAL           NULL,
    [MPSOVTPcntGap]          REAL           NULL,
    [InternalEnergyUsageGap] REAL           NULL,
    [HeatRateVarianceGap]    REAL           NULL,
    [HeatRateVariance]       REAL           NULL,
    [InternalEnergyUsage]    REAL           NULL,
    CONSTRAINT [PK_UnitGaps_1__15] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

