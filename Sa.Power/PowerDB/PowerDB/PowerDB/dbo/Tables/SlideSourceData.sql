﻿CREATE TABLE [dbo].[SlideSourceData] (
    [Source]  VARCHAR (50) NOT NULL,
    [Type]    VARCHAR (50) NOT NULL,
    [Filter1] VARCHAR (50) NULL,
    [Filter2] VARCHAR (50) NULL,
    [Label]   VARCHAR (50) NULL,
    [Value]   REAL         NULL
);

