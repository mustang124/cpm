﻿CREATE TABLE [dbo].[ReportGroupEmissionsData] (
    [RefNum]        CHAR (20) NOT NULL,
    [ReportTitle]   CHAR (50) NOT NULL,
    [ListName]      CHAR (20) NOT NULL,
    [SO2LbsPerMWH]  REAL      NULL,
    [NOxLbsPerMWH]  REAL      NULL,
    [CO2TonsPerMWH] REAL      NULL,
    [SO2Emitted]    REAL      NULL,
    [SO2Allowed]    REAL      NULL,
    [SO2Pur]        REAL      NULL,
    [SO2Sold]       REAL      NULL,
    [NOxEmitted]    REAL      NULL,
    [NOxAllowed]    REAL      NULL,
    [NOxPur]        REAL      NULL,
    [NOxSold]       REAL      NULL,
    [CO2Emitted]    REAL      NULL,
    [CO2Allowed]    REAL      NULL,
    [CO2Pur]        REAL      NULL,
    [CO2Sold]       REAL      NULL
);

