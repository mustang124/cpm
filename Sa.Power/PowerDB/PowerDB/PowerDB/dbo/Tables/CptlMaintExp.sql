﻿CREATE TABLE [dbo].[CptlMaintExp] (
    [Refnum]       [dbo].[Refnum]    NOT NULL,
    [TurbineID]    [dbo].[TurbineID] NOT NULL,
    [CptlCode]     VARCHAR (4)       NOT NULL,
    [SortKey]      TINYINT           NOT NULL,
    [Description]  VARCHAR (9)       NOT NULL,
    [TotCptl]      REAL              NULL,
    [CptlExcl]     REAL              NULL,
    [Energy]       REAL              NULL,
    [RegEnv]       REAL              NULL,
    [Admin]        REAL              NULL,
    [ConstrRmvl]   REAL              NULL,
    [STMaintCptl]  REAL              NULL,
    [TotMaintExp]  REAL              NULL,
    [MaintExpExcl] REAL              NULL,
    [STMaintExp]   REAL              NULL,
    [TotMMO]       REAL              NULL,
    [AllocMMO]     REAL              NULL,
    [MMOExcl]      REAL              NULL,
    [STMMO]        REAL              NULL,
    [TotMaint]     REAL              NULL,
    [InvestCptl]   REAL              NULL,
    [OtherCptl]    REAL              NULL,
    CONSTRAINT [PK_CptlMaintExp] PRIMARY KEY NONCLUSTERED ([Refnum] ASC, [TurbineID] ASC, [CptlCode] ASC)
);


GO
CREATE CLUSTERED INDEX [CptlSortKey]
    ON [dbo].[CptlMaintExp]([Refnum] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90);

