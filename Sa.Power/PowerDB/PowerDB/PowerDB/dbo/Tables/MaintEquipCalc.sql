﻿CREATE TABLE [dbo].[MaintEquipCalc] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [EquipGroup]       CHAR (8)       NOT NULL,
    [AnnOHCostKUS]     REAL           NULL,
    [AnnNonOHCostKUS]  REAL           NULL,
    [AnnMaintCostKUS]  REAL           NULL,
    [AnnOHCostMWH]     REAL           NULL,
    [AnnNonOHCostMWH]  REAL           NULL,
    [AnnMaintCostMWH]  REAL           NULL,
    [AnnOHCostMW]      REAL           NULL,
    [AnnNonOHCostMW]   REAL           NULL,
    [AnnMaintCostMW]   REAL           NULL,
    [AnnOHCostEGC]     REAL           NULL,
    [AnnNonOHCostEGC]  REAL           NULL,
    [AnnMaintCostEGC]  REAL           NULL,
    [AnnOHCostGJ]      REAL           NULL,
    [AnnNonOHCostGJ]   REAL           NULL,
    [AnnMaintCostGJ]   REAL           NULL,
    [AnnOHCostMBTU]    REAL           NULL,
    [AnnNonOHCostMBTU] REAL           NULL,
    [AnnMaintCostMBTU] REAL           NULL,
    CONSTRAINT [PK_MaintEquipCalc_1__17] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EquipGroup] ASC) WITH (FILLFACTOR = 90)
);

