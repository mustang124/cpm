﻿CREATE TABLE [dbo].[ReportGroupPersonnelMWData] (
    [RefNum]                     CHAR (20) NOT NULL,
    [ReportTitle]                CHAR (50) NOT NULL,
    [ListName]                   CHAR (20) NOT NULL,
    [TotEffPersonnel]            REAL      NULL,
    [MinimumPersonnel]           REAL      NULL,
    [MaximumPersonnel]           REAL      NULL,
    [OCCTotalOps]                REAL      NULL,
    [OCCTotalNonOvhlMaint]       REAL      NULL,
    [OCCTotalInspectors]         REAL      NULL,
    [OCCTotalLumpSumContract]    REAL      NULL,
    [OCCTotalLTSA]               REAL      NULL,
    [OCCTotalOvhlMaint]          REAL      NULL,
    [OCCTotalSubTotalMaint]      REAL      NULL,
    [OCCTotalTechnical]          REAL      NULL,
    [OCCTotalAdmin]              REAL      NULL,
    [OCCTotalSupport]            REAL      NULL,
    [OCCTotalTotal]              REAL      NULL,
    [MPSTotalOps]                REAL      NULL,
    [MPSTotalNonOvhlMaint]       REAL      NULL,
    [MPSTotalInspectors]         REAL      NULL,
    [MPSTotalLumpSumContract]    REAL      NULL,
    [MPSTotalLTSA]               REAL      NULL,
    [MPSTotalOvhlMaint]          REAL      NULL,
    [MPSTotalSubTotalMaint]      REAL      NULL,
    [MPSTotalTechnical]          REAL      NULL,
    [MPSTotalAdmin]              REAL      NULL,
    [MPSTotalSupport]            REAL      NULL,
    [MPSTotalTotal]              REAL      NULL,
    [OCCSiteOps]                 REAL      NULL,
    [OCCSiteNonOvhlMaint]        REAL      NULL,
    [OCCSiteInspectors]          REAL      NULL,
    [OCCSiteOvhlMaint]           REAL      NULL,
    [OCCSiteSubTotalMaint]       REAL      NULL,
    [OCCSiteTechnical]           REAL      NULL,
    [OCCSiteAdmin]               REAL      NULL,
    [OCCSiteSupport]             REAL      NULL,
    [OCCSiteTotal]               REAL      NULL,
    [OCCCentralNonOvhlMaint]     REAL      NULL,
    [OCCCentralInspectors]       REAL      NULL,
    [OCCCentralOvhlMaint]        REAL      NULL,
    [OCCCentralSubTotalMaint]    REAL      NULL,
    [OCCCentralTotal]            REAL      NULL,
    [OCCAGOps]                   REAL      NULL,
    [OCCAGNonOvhlMaint]          REAL      NULL,
    [OCCAGInspectors]            REAL      NULL,
    [OCCAGOvhlMaint]             REAL      NULL,
    [OCCAGSubTotalMaint]         REAL      NULL,
    [OCCAGTechnical]             REAL      NULL,
    [OCCAGAdmin]                 REAL      NULL,
    [OCCAGSupport]               REAL      NULL,
    [OCCAGTotal]                 REAL      NULL,
    [OCCContractOps]             REAL      NULL,
    [OCCContractNonOvhlMaint]    REAL      NULL,
    [OCCContractInspectors]      REAL      NULL,
    [OCCContractLumpSumContract] REAL      NULL,
    [OCCContractLTSA]            REAL      NULL,
    [OCCContractOvhlMaint]       REAL      NULL,
    [OCCContractSubTotalMaint]   REAL      NULL,
    [OCCContractTechnical]       REAL      NULL,
    [OCCContractAdmin]           REAL      NULL,
    [OCCContractSupport]         REAL      NULL,
    [OCCContractTotal]           REAL      NULL,
    [MPSSiteOps]                 REAL      NULL,
    [MPSSiteNonOvhlMaint]        REAL      NULL,
    [MPSSiteInspectors]          REAL      NULL,
    [MPSSiteOvhlMaint]           REAL      NULL,
    [MPSSiteSubTotalMaint]       REAL      NULL,
    [MPSSiteTechnical]           REAL      NULL,
    [MPSSiteAdmin]               REAL      NULL,
    [MPSSiteSupport]             REAL      NULL,
    [MPSSiteTotal]               REAL      NULL,
    [MPSCentralNonOvhlMaint]     REAL      NULL,
    [MPSCentralInspectors]       REAL      NULL,
    [MPSCentralOvhlMaint]        REAL      NULL,
    [MPSCentralSubTotalMaint]    REAL      NULL,
    [MPSCentralTotal]            REAL      NULL,
    [MPSAGOps]                   REAL      NULL,
    [MPSAGNonOvhlMaint]          REAL      NULL,
    [MPSAGInspectors]            REAL      NULL,
    [MPSAGOvhlMaint]             REAL      NULL,
    [MPSAGSubTotalMaint]         REAL      NULL,
    [MPSAGTechnical]             REAL      NULL,
    [MPSAGAdmin]                 REAL      NULL,
    [MPSAGSupport]               REAL      NULL,
    [MPSAGTotal]                 REAL      NULL,
    [MPSContractOps]             REAL      NULL,
    [MPSContractNonOvhlMaint]    REAL      NULL,
    [MPSContractInspectors]      REAL      NULL,
    [MPSContractLumpSumContract] REAL      NULL,
    [MPSContractLTSA]            REAL      NULL,
    [MPSContractOvhlMaint]       REAL      NULL,
    [MPSContractSubTotalMaint]   REAL      NULL,
    [MPSContractTechnical]       REAL      NULL,
    [MPSContractAdmin]           REAL      NULL,
    [MPSContractSupport]         REAL      NULL,
    [MPSContractTotal]           REAL      NULL
);

