﻿CREATE TABLE [dbo].[EmissionsTons] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [SO2Tons]          REAL           NULL,
    [NOxTons]          REAL           NULL,
    [CO2Tons]          REAL           NULL,
    [TotEmissionsTons] REAL           NULL,
    CONSTRAINT [PK_EmissionsTons] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

