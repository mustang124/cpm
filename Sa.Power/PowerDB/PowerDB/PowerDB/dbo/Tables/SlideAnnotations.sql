﻿CREATE TABLE [dbo].[SlideAnnotations] (
    [SlideAnnotationID]  INT IDENTITY (1, 1) NOT NULL,
    [ParentID]           INT NOT NULL,
    [AnnotationPosition] INT NOT NULL,
    [AnnotationTextID]   INT NULL
);

