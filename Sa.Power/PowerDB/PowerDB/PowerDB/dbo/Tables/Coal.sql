﻿CREATE TABLE [dbo].[Coal] (
    [Refnum]                 [dbo].[Refnum]    NOT NULL,
    [TurbineID]              [dbo].[TurbineID] NOT NULL,
    [Period]                 [dbo].[GenPeriod] NOT NULL,
    [CoalID]                 CHAR (2)          NOT NULL,
    [Name]                   CHAR (30)         NULL,
    [HeatValue]              REAL              NULL,
    [LHV]                    REAL              NULL,
    [AshPcnt]                REAL              NULL,
    [SulfurPcnt]             REAL              NULL,
    [MoistPcnt]              REAL              NULL,
    [HardgroveGrind]         REAL              NULL,
    [Tons]                   REAL              NULL,
    [MineCostTon]            REAL              NULL,
    [TransCostTon]           REAL              NULL,
    [RailMiles]              REAL              NULL,
    [BargeMiles]             REAL              NULL,
    [HwyMiles]               REAL              NULL,
    [ConvLength]             REAL              NULL,
    [OnsitePrepCostTon]      REAL              NULL,
    [NSTransMiles]           REAL              NULL,
    [MineCostTonLocal]       REAL              NULL,
    [TransCostTonLocal]      REAL              NULL,
    [OnSitePrepCostTonLocal] REAL              NULL,
    [TotMiles]               REAL              NULL,
    [TotTonMiles]            REAL              NULL,
    [TransCostTM]            REAL              NULL,
    [TotCostTon]             REAL              NULL,
    [TotCostKUS]             REAL              NULL,
    [MBTU]                   REAL              NULL,
    [MBTULHV]                REAL              NULL,
    [Biomass]                [dbo].[YorN]      NOT NULL,
    [ReportedLHV]            [dbo].[YorN]      NOT NULL,
    [DeliveredCostTon]       REAL              NULL,
    [DeliveredCostTonLocal]  REAL              NULL,
    CONSTRAINT [PK_Coal] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [Period] ASC, [CoalID] ASC) WITH (FILLFACTOR = 90)
);

