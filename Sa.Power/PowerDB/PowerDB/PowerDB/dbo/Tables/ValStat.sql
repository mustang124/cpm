﻿CREATE TABLE [dbo].[ValStat] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [IssueID]    CHAR (8)       NOT NULL,
    [IssueTitle] CHAR (50)      NOT NULL,
    [IssueText]  TEXT           NULL,
    [PostedBy]   CHAR (3)       NOT NULL,
    [PostedTime] DATETIME       NOT NULL,
    [Completed]  [dbo].[YorN]   NOT NULL,
    [SetBy]      CHAR (3)       NULL,
    [SetTime]    DATETIME       NULL,
    CONSTRAINT [PK_ValStat] PRIMARY KEY CLUSTERED ([Refnum] ASC, [IssueID] ASC) WITH (FILLFACTOR = 90)
);

