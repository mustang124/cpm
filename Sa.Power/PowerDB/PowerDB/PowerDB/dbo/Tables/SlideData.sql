﻿CREATE TABLE [dbo].[SlideData] (
    [SlideDataID]     INT          IDENTITY (1, 1) NOT NULL,
    [SlideChartID]    INT          NOT NULL,
    [AxisNumber]      BIT          NULL,
    [AxisTitle]       VARCHAR (50) NULL,
    [DataSource]      VARCHAR (50) NULL,
    [Filter1]         VARCHAR (50) NULL,
    [SlideColorGroup] INT          NULL,
    [Filter2]         VARCHAR (50) NULL,
    [Filter3]         VARCHAR (50) NULL,
    [DataType]        VARCHAR (10) NULL,
    [Filter4]         VARCHAR (50) NULL,
    [Filter5]         VARCHAR (50) NULL,
    [BarGapWidth]     REAL         NULL,
    [LineFormat]      INT          NULL,
    [Filter6]         VARCHAR (50) NULL,
    [Filter7]         VARCHAR (50) NULL,
    [Multi]           REAL         NULL
);

