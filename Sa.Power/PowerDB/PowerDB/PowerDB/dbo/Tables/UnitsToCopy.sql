﻿CREATE TABLE [dbo].[UnitsToCopy] (
    [OldRefnum] [dbo].[Refnum]    NULL,
    [NewRefnum] [dbo].[Refnum]    NOT NULL,
    [StudyYear] [dbo].[StudyYear] NULL,
    [OldSiteID] [dbo].[SiteID]    NULL,
    [NewSiteID] [dbo].[SiteID]    NULL,
    CONSTRAINT [PK_UnitsToCopy] PRIMARY KEY CLUSTERED ([NewRefnum] ASC)
);

