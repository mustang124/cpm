﻿CREATE TABLE [dbo].[CptlMaintExpLocal] (
    [Refnum]       [dbo].[Refnum]    NOT NULL,
    [TurbineID]    [dbo].[TurbineID] NOT NULL,
    [CptlCode]     VARCHAR (4)       NOT NULL,
    [SortKey]      TINYINT           NOT NULL,
    [Description]  VARCHAR (9)       NOT NULL,
    [TotCptl]      REAL              NULL,
    [CptlExcl]     REAL              NULL,
    [Energy]       REAL              NULL,
    [RegEnv]       REAL              NULL,
    [Admin]        REAL              NULL,
    [ConstrRmvl]   REAL              NULL,
    [STMaintCptl]  REAL              NULL,
    [TotMaintExp]  REAL              NULL,
    [MaintExpExcl] REAL              NULL,
    [STMaintExp]   REAL              NULL,
    [TotMMO]       REAL              NULL,
    [AllocMMO]     REAL              NULL,
    [MMOExcl]      REAL              NULL,
    [STMMO]        REAL              NULL,
    [TotMaint]     REAL              NULL,
    [InvestCptl]   REAL              NULL,
    [OtherCptl]    REAL              NULL,
    CONSTRAINT [PK_CptlMaintExpLocal] PRIMARY KEY NONCLUSTERED ([Refnum] ASC, [TurbineID] ASC, [CptlCode] ASC)
);


GO
CREATE CLUSTERED INDEX [CptlLocalSortKey]
    ON [dbo].[CptlMaintExpLocal]([Refnum] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90);


GO
CREATE TRIGGER UpdateCptlMaintExpLocal ON dbo.CptlMaintExpLocal  
FOR INSERT,UPDATE
AS
UPDATE CptlMaintExpLocal
SET RegEnv = null, OtherCptl = null
WHERE CptlCode = 'EST5' and (TotCptl = 0 or TotCptl IS NULL)
AND Refnum IN (SELECT Refnum FROM inserted WHERE CptlCode = 'EST5')
UPDATE CptlMaintExpLocal
SET RegEnv = 0
WHERE CptlCode = 'EST5' and TotCptl <> 0 and TotCptl IS NOT NULL and regenv IS NULL
AND Refnum IN (SELECT Refnum FROM inserted WHERE CptlCode = 'EST5')
UPDATE CptlMaintExpLocal
SET OtherCptl = 0
WHERE CptlCode = 'EST5' and TotCptl <> 0 and TotCptl IS NOT NULL and OtherCptl IS NULL
AND Refnum IN (SELECT Refnum FROM inserted WHERE CptlCode = 'EST5')
