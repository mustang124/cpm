﻿CREATE TABLE [dbo].[VarCostCalc] (
    [Refnum]          [dbo].[Refnum]    NOT NULL,
    [Period]          [dbo].[GenPeriod] NOT NULL,
    [StmSalesMWH]     REAL              NULL,
    [AdjNetMWH]       REAL              NULL,
    [FuelCostMWH]     REAL              NULL,
    [VarCostMWH]      REAL              NULL,
    [StmSalesMBTU]    REAL              NULL,
    [TotMBTU]         REAL              NULL,
    [FuelCostMBTU]    REAL              NULL,
    [AdjFuelCostMWH]  REAL              NULL,
    [TotMBTULHV]      REAL              NULL,
    [FuelCostMBTULHV] REAL              NULL,
    CONSTRAINT [PK_VarCostCalc_1__28] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Period] ASC) WITH (FILLFACTOR = 90)
);

