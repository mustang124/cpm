﻿CREATE TABLE [dbo].[MessageLog] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [Source]      VARCHAR (12)   NULL,
    [Severity]    CHAR (1)       NOT NULL,
    [MessageID]   INT            NOT NULL,
    [MessageText] VARCHAR (255)  NULL,
    [SysAdmin]    BIT            NOT NULL,
    [Consultant]  BIT            NOT NULL,
    [Pricing]     BIT            NOT NULL,
    [Audience4]   BIT            NOT NULL,
    [Audience5]   BIT            NOT NULL,
    [Audience6]   BIT            NOT NULL,
    [Audience7]   BIT            NOT NULL,
    [MessageTime] DATETIME       NULL
);


GO
CREATE CLUSTERED INDEX [MessageLogByTime]
    ON [dbo].[MessageLog]([MessageTime] ASC) WITH (FILLFACTOR = 90);

