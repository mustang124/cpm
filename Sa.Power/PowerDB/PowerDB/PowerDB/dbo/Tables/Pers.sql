﻿CREATE TABLE [dbo].[Pers] (
    [Refnum]            [dbo].[Refnum] NOT NULL,
    [PersCat]           CHAR (15)      NOT NULL,
    [SortKey]           INT            NOT NULL,
    [SectionID]         CHAR (4)       NULL,
    [SiteNumPers]       REAL           NULL,
    [SiteSTH]           REAL           NULL,
    [SiteOVTHrs]        REAL           NULL,
    [CentralNumPers]    REAL           NULL,
    [CentralSTH]        REAL           NULL,
    [CentralOVTHrs]     REAL           NULL,
    [AGOnSite]          REAL           NULL,
    [AGOthLoc]          REAL           NULL,
    [Contract]          REAL           NULL,
    [SiteSTEffPers]     REAL           NULL,
    [SiteOVTEffPers]    REAL           NULL,
    [SiteEffPers]       REAL           NULL,
    [SiteOVTPcnt]       REAL           NULL,
    [CentralSTEffPers]  REAL           NULL,
    [CentralOVTEffPers] REAL           NULL,
    [CentralEffPers]    REAL           NULL,
    [ContractEffPers]   REAL           NULL,
    [AGOnSiteEffPers]   REAL           NULL,
    [AGOthLocEffPers]   REAL           NULL,
    [AGEffPers]         REAL           NULL,
    [TotEffPers]        REAL           NULL,
    [TotEffPersMWH]     REAL           NULL,
    [TotEffPersMW]      REAL           NULL,
    [TotEffPersKSH]     REAL           NULL,
    [TotEffPersSHMW]    REAL           NULL,
    [TotEffPersEGC]     REAL           NULL,
    [TotEffPersGJ]      REAL           NULL,
    CONSTRAINT [PK_Pers] PRIMARY KEY NONCLUSTERED ([Refnum] ASC, [PersCat] ASC)
);


GO
CREATE CLUSTERED INDEX [PersSortKey]
    ON [dbo].[Pers]([Refnum] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90);

