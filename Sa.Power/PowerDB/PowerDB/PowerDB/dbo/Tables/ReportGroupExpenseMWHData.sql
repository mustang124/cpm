﻿CREATE TABLE [dbo].[ReportGroupExpenseMWHData] (
    [RefNum]                       CHAR (20) NOT NULL,
    [ReportTitle]                  CHAR (50) NOT NULL,
    [ListName]                     CHAR (20) NOT NULL,
    [FixedExpMWH]                  REAL      NULL,
    [VariableExpMWH]               REAL      NULL,
    [TotalCashExpMWH]              REAL      NULL,
    [TotalCashRangeMinimumMWH]     REAL      NULL,
    [TotalCashRangeMaximumMWH]     REAL      NULL,
    [OCCWagesMWH]                  REAL      NULL,
    [MPSWagesMWH]                  REAL      NULL,
    [SubTotalWagesMWH]             REAL      NULL,
    [OCCBenefitsMWH]               REAL      NULL,
    [MPSBenefitsMWH]               REAL      NULL,
    [SubTotalBenefitsMWH]          REAL      NULL,
    [CentralOCCWageBenMWH]         REAL      NULL,
    [CentralMPSWageBenMWH]         REAL      NULL,
    [MaintMaterialsMWH]            REAL      NULL,
    [ContractMaintLaborMWH]        REAL      NULL,
    [ContractMaintMaterialMWH]     REAL      NULL,
    [ContractMaintLumpSumMWH]      REAL      NULL,
    [OtherContractServicesMWH]     REAL      NULL,
    [OverhaulAdjMWH]               REAL      NULL,
    [LTSAAdjMWH]                   REAL      NULL,
    [EnvironmentExpMWH]            REAL      NULL,
    [PropertyTaxesMWH]             REAL      NULL,
    [OtherTaxesMWH]                REAL      NULL,
    [InsuranceMWH]                 REAL      NULL,
    [AllocatedAGPersCostMWH]       REAL      NULL,
    [AllocatedAGNonPersCostMWH]    REAL      NULL,
    [OtherFixedExpMWH]             REAL      NULL,
    [SubTotalFixedExpMWH]          REAL      NULL,
    [ChemicalsMWH]                 REAL      NULL,
    [WaterMWH]                     REAL      NULL,
    [GasMWH]                       REAL      NULL,
    [LiquidMWH]                    REAL      NULL,
    [SolidMWH]                     REAL      NULL,
    [SubTotalFuelConsumedMWH]      REAL      NULL,
    [SO2EmissionAdjMWH]            REAL      NULL,
    [NOxEmissionAdjMWH]            REAL      NULL,
    [CO2EmissionAdjMWH]            REAL      NULL,
    [OtherVarExpMWH]               REAL      NULL,
    [SubTotalVarExpMWH]            REAL      NULL,
    [DepreciationMWH]              REAL      NULL,
    [InterestExpMWH]               REAL      NULL,
    [SupplyCarryCostMWH]           REAL      NULL,
    [FuelInvCarryCostMWH]          REAL      NULL,
    [SubTotalNonCashExpMWH]        REAL      NULL,
    [GrandTotalExpMWH]             REAL      NULL,
    [PlantSiteWagesBenefitsMWH]    REAL      NULL,
    [PlantCentralEmployeesMWH]     REAL      NULL,
    [PlantMaterialsMWH]            REAL      NULL,
    [PlantOverhaulsMWH]            REAL      NULL,
    [PlantLTSAPSAMWH]              REAL      NULL,
    [PlantOtherVarMWH]             REAL      NULL,
    [PlantMiscMWH]                 REAL      NULL,
    [PlantManageExpLessFuelMWH]    REAL      NULL,
    [AncillaryRevenueMWH]          REAL      NULL,
    [AshSalesMWH]                  REAL      NULL,
    [OtherChemicalSalesMWH]        REAL      NULL,
    [OtherRevenueMWH]              REAL      NULL,
    [TotalRevenueMWH]              REAL      NULL,
    [AshOperationsCostMWH]         REAL      NULL,
    [AshDisposalCostMWH]           REAL      NULL,
    [AshTotalCostMWH]              REAL      NULL,
    [ScrubberLimeCostMWH]          REAL      NULL,
    [ScrubberOtherChemCostMWH]     REAL      NULL,
    [ScrubberAuxCostMWH]           REAL      NULL,
    [ScrubberOtherNonMaintCostMWH] REAL      NULL,
    [ScrubberTotalCostMWH]         REAL      NULL,
    [ScrubbingCostTonMWH]          REAL      NULL
);

