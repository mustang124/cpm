﻿CREATE TABLE [dbo].[OpexLocal] (
    [Refnum]                  [dbo].[Refnum] NOT NULL,
    [OCCWages]                REAL           NULL,
    [MPSWages]                REAL           NULL,
    [OCCBenefits]             REAL           NULL,
    [MPSBenefits]             REAL           NULL,
    [CentralOCCWagesBen]      REAL           NULL,
    [CentralMPSWagesBen]      REAL           NULL,
    [MaintMatl]               REAL           NULL,
    [LTSAAdj]                 REAL           NULL,
    [ContMaintLabor]          REAL           NULL,
    [ContMaintMatl]           REAL           NULL,
    [ContMaintLumpSum]        REAL           NULL,
    [OthContSvc]              REAL           NULL,
    [OverhaulAdj]             REAL           NULL,
    [Envir]                   REAL           NULL,
    [PropTax]                 REAL           NULL,
    [OthTax]                  REAL           NULL,
    [Insurance]               REAL           NULL,
    [AGPers]                  REAL           NULL,
    [Supply]                  REAL           NULL,
    [OthFixed]                REAL           NULL,
    [PurGas]                  REAL           NULL,
    [PurLiquid]               REAL           NULL,
    [PurSolid]                REAL           NULL,
    [FuelInven]               REAL           NULL,
    [OthVar]                  REAL           NULL,
    [AGNonPers]               REAL           NULL,
    [Depreciation]            REAL           NULL,
    [Interest]                REAL           NULL,
    [OthRevenue]              REAL           NULL,
    [OthNonT7Revenue]         REAL           NULL,
    [TotRevenueRevised]       REAL           NULL,
    [Accuracy]                CHAR (1)       NULL,
    [BookValue]               REAL           NULL,
    [FireSafetyLoss]          REAL           NULL,
    [EnvirFines]              REAL           NULL,
    [ExclOth]                 REAL           NULL,
    [TotExpExcl]              REAL           NULL,
    [Chemicals]               REAL           NULL,
    [Water]                   REAL           NULL,
    [AllocSparesInven]        REAL           NULL,
    [DemandCapacity]          REAL           NULL,
    [OCCWagesSmall]           REAL           NULL,
    [MPSWagesSmall]           REAL           NULL,
    [OCCBenefitsSmall]        REAL           NULL,
    [MPSBenefitsSmall]        REAL           NULL,
    [CentralOCCWagesBenSmall] REAL           NULL,
    [CentralMPSWagesBenSmall] REAL           NULL,
    [MaintMatlSmall]          REAL           NULL,
    [ContMaintLaborSmall]     REAL           NULL,
    [ContMaintMatlSmall]      REAL           NULL,
    [ContMaintLumpSumSmall]   REAL           NULL,
    [OthContSvcSmall]         REAL           NULL,
    CONSTRAINT [PK_OpexLocal] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

