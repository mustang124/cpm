﻿CREATE TYPE [dbo].[TrafficLightTable] AS TABLE (
    [UnitLabel] VARCHAR (100) NULL,
    [Col1]      VARCHAR (200) NULL,
    [Col2]      VARCHAR (100) NULL,
    [Col3]      VARCHAR (100) NULL,
    [Col4]      VARCHAR (100) NULL,
    [Col5]      VARCHAR (100) NULL,
    [Col6]      VARCHAR (100) NULL);

