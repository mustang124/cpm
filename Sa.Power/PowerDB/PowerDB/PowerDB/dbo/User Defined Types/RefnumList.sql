﻿CREATE TYPE [dbo].[RefnumList] AS TABLE (
    [Refnum] [dbo].[Refnum] NOT NULL,
    PRIMARY KEY CLUSTERED ([Refnum] ASC));

