﻿
CREATE VIEW [dbo].[SlideTableFormats]
AS
SELECT SlideTableFormatID, TableX, TableY, FontHeight, RowHeight, CellFill, CellColorGroup, BorderFill, BorderColorGroup, ColWidth, Notes
FROM GlobalDB.dbo.SlideTableFormats

