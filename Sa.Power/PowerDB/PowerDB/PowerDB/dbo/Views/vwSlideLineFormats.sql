﻿


CREATE VIEW [dbo].[vwSlideLineFormats]
AS

	SELECT SlideLineFormatID, Color, LineWidth, LineDashStyle, LineBeginArrow, LineEndArrow, MarkerType, MarkerSize, MarkerLastOnly
	FROM SlideLineFormats slf 
		LEFT JOIN SlideColors sc ON sc.SlideColorGroup = slf.LineColor
	WHERE sc.ColorOrder = 1

