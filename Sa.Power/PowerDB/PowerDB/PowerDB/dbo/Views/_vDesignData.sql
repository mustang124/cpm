﻿CREATE VIEW dbo._vDesignData
AS
SELECT     t.Refnum, d.Unit_Name, d.Service_Date, d.LC, d.Unit_Type, d.AE, d.Constr, d.BlrMfr, d.BlrMdl, d.BlrStmFl, d.BlrTemp, d.BlrPSIG, d.Firing, d.FurnTyp, 
                      d.Design_Fuel_1, d.Ave_Heat_1, d.__ASH_1, d.__SULFUR_1, d.__H2O_1, d.Ash_Soft_Temp_1, d.GHI_1, d._V___Ph_1, d.Design_Fuel_2, 
                      d.Ave_Heat_2, d.__ASH_2, d.__SULFUR_2, d.__H2O_2, d.Ash_Soft_Temp_2, d.GHI_2, d._V___Ph, d.BrnrMfrA, d.BrnrA, d.BrnrMfrB, d.BrnrB, 
                      d.LowNoMfr, d.LowNoTotal, d.LowNoMin, d.LowNoDate, d.CombMfr, d.BrnSMfr, d.IgMfr, d.IgFuel, d.IgTyp, d.CrshMfrA, d.StkrMfrA, d.CritCnvA, 
                      d.CrshMfrB, d.StkrMfrB, d.CritCnvB, d.FeedMfrA, d.FeedMtmA, d.FeedA, d.FeedTypA, d.FeedMfrB, d.FeedMtmB, d.FeedB, d.FeedTypB, d.PulvMfrA, 
                      d.PulvMdlA, d.PulvRtA, d.PulvA, d.PulvTot, d.PulvTypA, d.PulvMfrB, d.PulvMdlB, d.PulvRtB, d.PulvB, d.PulvMin, d.PulvTypB, d.DrftOrig, d.DrftDate, 
                      d.SootMfrA, d.SootA, d.SootTypA, d.SootQtyA, d.SootMfrB, d.SootB, d.SootTypB, d.SootQtyB, d.SootMfrC, d.SootC, d.SootTypC, d.SootQtyC, 
                      d.MPRPCMFR, d.MPRCPLOC, d.EPRCPMFR, d.EPRCPLOC, d.BGSYSMFR, d.BGFANMFR, d.BGFANMTM, d.BGFANTOT, d.BGSYSTYP, d.BGFANBLD, 
                      d.FAshMfr, d.FAshTyp, d.FGDMFR, d.FGD_Inst_Date, d.FGDORIG, d.FGDReag1, d.FGDReag2, d.TurbMfr, d.TurbOutd, d.Nameplat, d.TurbTyp, 
                      d.SHFT1MFR, d.SHFT2MFR, d.TURBCNFG, d.MAINTEMP, d.MAINPSIG, d.RHT1TEMP, d.RHT1PSIG, d.RHT2TEMP, d.RHT2PSIG, d.HPCase, d.HPCndBkp, 
                      d.HPIPCase, d.IPCase, d.IPLPCase, d.LPCase, d.LPCndBkp, d.LPBldLng, d.GENMFR, d.MAINKV, d.MAINMVA, d.MAINRPM, d.MAINPF, d.SHFT2KV, 
                      d.SHFT2MVA, d.SHFT2RPM, d.SHFT2PF, d.SHFT3KV, d.SHFT3MVA, d.SHFT3RPM, d.SHFT3PF, d.CndMfr, d.CndPass, d.CndShell, d.CndMat, d.ArMvMfr, 
                      d.ArMvTyp, d.CrcwTyp, d.CrcwOrig, d.PolshMfr, d.PolshCap, d.CpmpMfrA, d.CpmpMtmA, d.CpmpA, d.CpmpTot, d.CpmpMfrB, d.CpmpMtmB, d.CpmpB, 
                      d.CpmpMin, d.CBPMfrA, d.CBPMtmA, d.CBPmpA, d.CBPmpTot, d.CBPMfrB, d.CBPMtmB, d.CBPmpB, d.CBPmpMin, d.BFPMfrA, d.BFPrpmA, d.BFPmpA, 
                      d.BFPmpTot, d.BFPCapA, d.BFPMfrB, d.BFPrpmB, d.BFPmpB, d.BFPmpMin, d.BFPCapB, d.BFPDrvMA, d.BFPDrvTA, d.BFPCplTA, d.BFPDrvMB, 
                      d.BFPDrvTB, d.BFPCplTB, d.CTWRMFRA, d.CFANMFRA, d.CFANMTMA, d.CTWRTYPA, d.CTBPMFRA, d.CTBPMTMA, d.CTBPMPA, d.CTBPTOT, 
                      d.CTWRMFRB, d.CFANMFRB, d.CFANMTMB, d.CTWRTYPB, d.CTBPMFRB, d.CTBPMTMB, d.CTBPMPB, d.CTBPMIN, d.MXFMFRA, d.MXFA, d.MXFMVAA, 
                      d.MXFKVHA, d.MXFTYPA, d.MXFMFRB, d.MXFB, d.MXFMVAB, d.MXFKVHB, d.MXFTYPB, d.PPCMfrA, d.PPCmptrA, d.PPCLinkA, d.PPCCapA, d.PPCMfrB, 
                      d.PPCmptrB, d.PPCLinkB, d.PPCCapB, d.CT_Mfr, d.CT_Model, d.[#_of_CTs], d.CT_Gen_Mfr, d.HRSG_Mfr, d.HRSG_Model, d.[#_of_HRSGs], 
                      d.Condensing
FROM         dbo.NERCTurbine t LEFT OUTER JOIN
                      dbo.DesignData d ON t.Util_Code = d.Utility AND t.Unit_Code = d.Unit
