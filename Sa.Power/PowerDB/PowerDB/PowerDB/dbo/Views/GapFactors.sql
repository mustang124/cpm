﻿





/****** Object:  View dbo.GapFactors    Script Date: 4/18/2003 4:46:36 PM *****


*/
CREATE   VIEW [dbo].[GapFactors]
AS
SELECT     s.Refnum, c.CUTI_Unp, c.CUTI_P, c.PeakUnavail_Unp, s.HeatRate, g.InternalLessScrubberPcnt, so2.ScrubbingIndex, p.TotEffPersMW, 
                      CASE WHEN p.TotEffPers > 0 THEN (p.TotEffPers - p.AGEffPers) / p.TotEffPers * p.TotEffPersMW END AS TotLessAGEffPersMW, o.TotCashLessFuel, o.TotCashLessFuelEm, o.STVar - o.STFuelCost AS VarLessFuel, o.STVar - (o.TotCash - o.TotCashLessFuelEm) AS VarLessFuelEm, 
                      o.STFixed - (o.STWages + o.STBenefits) AS FixedLessWagesBen, o.STWages + o.STBenefits AS SiteWagesBen, s.MaintInd, s.MaintOHInd, s.MaintNonOHInd, m.AnnLTSACostMWH,
                      omw.TotCashLessFuel AS TotCashLessFuelMW, omw.TotCashLessFuelEm AS TotCashLessFuelEmMW, omw.STVar - omw.STFuelCost AS VarLessFuelMW, omw.STVar - (omw.TotCash - omw.TotCashLessFuelEm) AS VarLessFuelEmMW, omw.STFixed - (omw.STWages + omw.STBenefits) 
                      AS FixedLessWagesBenMW, omw.STWages + omw.STBenefits AS SiteWagesBenMW,
                      m.AnnMaintCostMW, m.AnnOHCostMW, m.AnnNonOHCostMW, m.AnnLTSACostMW
FROM		dbo.GenSum s INNER JOIN dbo.GenerationTotCalc g ON g.Refnum = s.Refnum 
		INNER JOIN dbo.MaintTotCalc m on s.Refnum = m.Refnum
		INNER JOIN dbo.PersSTCalc p ON p.Refnum = s.Refnum AND p.SectionID = 'TP' 
		INNER JOIN dbo.OpExCalc o ON o.Refnum = s.Refnum AND o.DataType = 'MWH' 
		INNER JOIN dbo.OpExCalc omw ON omw.Refnum = s.Refnum AND omw.DataType = 'KW' 
		LEFT OUTER JOIN dbo.CommUnavail c ON c.Refnum = s.Refnum
		LEFT OUTER JOIN dbo.SO2Removal so2 ON so2.Refnum = s.Refnum





