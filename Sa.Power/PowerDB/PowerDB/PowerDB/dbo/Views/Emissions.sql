﻿



/****** Object:  View dbo.Emissions    Script Date: 4/18/2003 4:46:35 PM ******/
CREATE  VIEW dbo.Emissions
AS
SELECT     e.Refnum, 
             CASE WHEN g.PotentialGrossMWH > 0 THEN e.SO2Tons * 2000 / g.PotentialGrossMWH ELSE NULL END AS SO2LbsPerMWH, 
             CASE WHEN g.PotentialGrossMWH > 0 THEN e.NOxTons * 2000 / g.PotentialGrossMWH ELSE NULL END AS NOxLbsPerMWH, 
             CASE WHEN g.PotentialGrossMWH > 0 THEN e.CO2Tons / g.PotentialGrossMWH ELSE NULL END AS CO2TonsPerMWH, 
             CASE WHEN g.PotentialGrossMWH > 0 THEN e.TotEmissionsTons / g.PotentialGrossMWH ELSE NULL END AS TotalTonsPerMWH, 
             e.SO2Tons, e.NOxTons, 
             e.CO2Tons, e.TotEmissionsTons
FROM         dbo.EmissionsTons e INNER JOIN
                      dbo.GenerationTotCalc g ON e.Refnum = g.Refnum




