﻿

CREATE VIEW [dbo].[R_MaintEquipCompCntByRefnum]
AS
SELECT a.Refnum, 
	CASE WHEN ASH > 0 THEN t.CompanyID ELSE NULL END AS ASH, 
	CASE WHEN BAG > 0 THEN t.CompanyID ELSE NULL END AS BAG, 
	CASE WHEN BOIL > 0 THEN t.CompanyID ELSE NULL END AS BOIL, 
	CASE WHEN BOILH2O > 0 THEN t.CompanyID ELSE NULL END AS BOILH2O, 
	CASE WHEN CA > 0 THEN t.CompanyID ELSE NULL END AS CA, 
	CASE WHEN CPHM > 0 THEN t.CompanyID ELSE NULL END AS CPHM, 
	CASE WHEN CTCOMB > 0 THEN t.CompanyID ELSE NULL END AS CTCOMB, 
	CASE WHEN CTCOMP > 0 THEN t.CompanyID ELSE NULL END AS CTCOMP, 
	CASE WHEN CTG > 0 THEN t.CompanyID ELSE NULL END AS CTG, 
	CASE WHEN CTGEN > 0 THEN t.CompanyID ELSE NULL END AS CTGEN, 
	CASE WHEN CTHRSG > 0 THEN t.CompanyID ELSE NULL END AS CTHRSG, 
	CASE WHEN CTOTH > 0 THEN t.CompanyID ELSE NULL END AS CTOTH, 
	CASE WHEN CTSCR > 0 THEN t.CompanyID ELSE NULL END AS CTSCR, 
	CASE WHEN CTTRANS > 0 THEN t.CompanyID ELSE NULL END AS CTTRANS, 
	CASE WHEN CTTURB > 0 THEN t.CompanyID ELSE NULL END AS CTTURB, 
	CASE WHEN CWF > 0 THEN t.CompanyID ELSE NULL END AS CWF, 
	CASE WHEN DGS > 0 THEN t.CompanyID ELSE NULL END AS DGS, 
	CASE WHEN DQWS > 0 THEN t.CompanyID ELSE NULL END AS DQWS, 
	CASE WHEN FHF > 0 THEN t.CompanyID ELSE NULL END AS FHF, 
	CASE WHEN GEN > 0 THEN t.CompanyID ELSE NULL END AS GEN, 
	CASE WHEN HRSG > 0 THEN t.CompanyID ELSE NULL END AS HRSG,
	CASE WHEN OTHER > 0 THEN t.CompanyID ELSE NULL END AS OTHER, 
	CASE WHEN PREC > 0 THEN t.CompanyID ELSE NULL END AS PREC, 
	CASE WHEN SCR > 0 THEN t.CompanyID ELSE NULL END AS SCR, 
	CASE WHEN TRANS > 0 THEN t.CompanyID ELSE NULL END AS TRANS, 
	CASE WHEN TURB > 0 THEN t.CompanyID ELSE NULL END AS TURB, 
	CASE WHEN VC > 0 THEN t.CompanyID ELSE NULL END AS VC, 
	CASE WHEN WASTEH2O > 0 THEN t.CompanyID ELSE NULL END AS WASTEH2O, 
	CASE WHEN WGS > 0 THEN t.CompanyID ELSE NULL END AS WGS
FROM (
	SELECT DISTINCT Refnum, 
		SUM(CASE WHEN EquipGroup = 'ASH' THEN 1 ELSE 0 END) AS ASH, 
		SUM(CASE WHEN EquipGroup = 'BAG' THEN 1 ELSE 0 END) AS BAG, 
		SUM(CASE WHEN EquipGroup = 'BOIL' THEN 1 ELSE 0 END) AS BOIL, 
		SUM(CASE WHEN EquipGroup = 'BOILH2O' THEN 1 ELSE 0 END) AS BOILH2O, 
		SUM(CASE WHEN EquipGroup = 'CA' THEN 1 ELSE 0 END) AS CA, 
		SUM(CASE WHEN EquipGroup = 'CPHM' THEN 1 ELSE 0 END) AS CPHM, 
		SUM(CASE WHEN EquipGroup = 'CTCOMB' THEN 1 ELSE 0 END) AS CTCOMB, 
		SUM(CASE WHEN EquipGroup = 'CTCOMP' THEN 1 ELSE 0 END) AS CTCOMP, 
		SUM(CASE WHEN EquipGroup = 'CTG' THEN 1 ELSE 0 END) AS CTG, 
		SUM(CASE WHEN EquipGroup = 'CTGEN' THEN 1 ELSE 0 END) AS CTGEN, 
		SUM(CASE WHEN EquipGroup = 'CTHRSG' THEN 1 ELSE 0 END) AS CTHRSG, 
		SUM(CASE WHEN EquipGroup = 'CTOTH' THEN 1 ELSE 0 END) AS CTOTH, 
		SUM(CASE WHEN EquipGroup = 'CTSCR' THEN 1 ELSE 0 END) AS CTSCR, 
		SUM(CASE WHEN EquipGroup = 'CTTRANS' THEN 1 ELSE 0 END) AS CTTRANS, 
		SUM(CASE WHEN EquipGroup = 'CTTURB' THEN 1 ELSE 0 END) AS CTTURB, 
		SUM(CASE WHEN EquipGroup = 'CWF' THEN 1 ELSE 0 END) AS CWF, 
		SUM(CASE WHEN EquipGroup = 'DGS' THEN 1 ELSE 0 END) AS DGS, 
		SUM(CASE WHEN EquipGroup = 'DQWS' THEN 1 ELSE 0 END) AS DQWS, 
		SUM(CASE WHEN EquipGroup = 'FHF' THEN 1 ELSE 0 END) AS FHF, 
		SUM(CASE WHEN EquipGroup = 'GEN' THEN 1 ELSE 0 END) AS GEN,
		SUM(CASE WHEN EquipGroup = 'HRSG' THEN 1 ELSE 0 END) AS HRSG,
		SUM(CASE WHEN EquipGroup = 'OTHER' THEN 1 ELSE 0 END) AS OTHER, 
		SUM(CASE WHEN EquipGroup = 'PREC' THEN 1 ELSE 0 END) AS PREC, 
		SUM(CASE WHEN EquipGroup = 'SCR' THEN 1 ELSE 0 END) AS SCR, 
		SUM(CASE WHEN EquipGroup = 'TRANS' THEN 1 ELSE 0 END) AS TRANS, 
		SUM(CASE WHEN EquipGroup = 'TURB' THEN 1 ELSE 0 END) AS TURB, 
		SUM(CASE WHEN EquipGroup = 'VC' THEN 1 ELSE 0 END) AS VC, 
		SUM(CASE WHEN EquipGroup = 'WASTEH2O' THEN 1 ELSE 0 END) AS WASTEH2O, 
		SUM(CASE WHEN EquipGroup = 'WGS' THEN 1 ELSE 0 END) AS WGS
	FROM dbo.R_MaintEquipCompCnt
	GROUP BY Refnum) AS a 
INNER JOIN dbo.TSort AS t ON t.Refnum = a.Refnum



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'R_MaintEquipCompCntByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'R_MaintEquipCompCntByRefnum';

