﻿


CREATE VIEW [dbo].[CTGMaintPerStart]
AS

--SELECT Refnum, AvgTotStarts = AVG(AvgTotStarts), 
--LTSAPerStart = AVG(ISNULL(LTSAAdj, 0))/AVG(AvgTotStarts),
--AnnOHPerStart = AVG(ISNULL(AnnOHCost, 0))/AVG(AvgTotStarts),
--NonOHPerStart = AVG(ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts),
--MaintCostPerStart = AVG(ISNULL(LTSAAdj, 0)+ISNULL(AnnOHCost, 0)+ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts)
--FROM CTGMaintCosts
--GROUP BY Refnum
--HAVING AVG(AvgTotStarts)>0

--SW 2/14/13 the query above was removed and the query below put in place
--I don't know why, but if you did a simple select * from CTGMaintPerStart where MaintCostPerStart > 0
--then you would get a divide by zero error. It would work fine as long as you didn't use the where clause.
--I changed it to the query below which works both ways. If you can explain this let me know.

select Refnum, 
AvgTotStarts, 
LTSAPerStart = case TotStarts when 0 then 0 else LTSAAdj/TotStarts end,
AnnOHPerStart = case TotStarts when 0 then 0 else AnnOHCost/TotStarts end,
NonOHPerStart = case TotStarts when 0 then 0 else AnnNonOHCost/TotStarts end,
MaintCostPerStart = case TotStarts when 0 then 0 else MaintCost/TotStarts end

from (
select Refnum, 
TotStarts = SUM(AvgTotStarts), 
AvgTotStarts = AVG(avgtotstarts), 
LTSAAdj = SUM(ISNULL(ltsaadj,0)),
AnnOHCost = SUM(ISNULL(AnnOHCost,0)),
AnnNonOHCost = SUM(ISNULL(AnnNonOHCost,0)),
MaintCost = SUM(ISNULL(LTSAAdj,0) + ISNULL(AnnOHCost,0) + ISNULL(AnnNonOHCost,0))
  FROM [Power].[dbo].[CTGMaintCosts]

group by Refnum) b
where TotStarts > 0


