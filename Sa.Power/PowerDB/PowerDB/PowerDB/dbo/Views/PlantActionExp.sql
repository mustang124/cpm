﻿CREATE VIEW PlantActionExp AS 
SELECT Refnum, DataType, 
	SiteWagesBen = TotCompensation,
	CentralContract = (CentralOCCWagesBen + CentralMPSWagesBen + ContMaintLabor +
			   ContMaintMatl + ContMaintLumpSum + OthContSvc),
	MaintMatl,
	OverhaulAdj,	LTSAAdj,
	OthVar = (ISNULL(OthVar,0)+ISNULL(Chemicals,0)+ISNULL(Water,0)),
	Misc = (Envir + OthFixed),
	ActCashLessFuel,
	FERCApproxExp = ActCashLessFuel	- ISNULL(OCCBenefits, 0) - ISNULL(MPSBenefits, 0),
	AEPOM = TotCompensation + CentralOCCWagesBen + CentralMPSWagesBen 
		+ ContMaintLabor + ContMaintMatl + ContMaintLumpSum + OthContSvc 
		+ MaintMatl + AGPers + OthFixed + Chemicals + OthVar + Water
FROM OpexCalc


