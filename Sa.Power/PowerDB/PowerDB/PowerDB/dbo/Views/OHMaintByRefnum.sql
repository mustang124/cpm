﻿
CREATE VIEW [dbo].[OHMaintByRefnum]
AS
--NOTE: This is only to be used for certain calculations. The Yrs part is summing the Years for a particular Refnum, and the Count is counting them,
--so we can later do some math to get the right numbers for a group of units. Do not use this individually (e.g. by dividing each row's Sum by its Count)
--as you will end up with the wrong number!
SELECT Refnum, 
	SUM(CASE WHEN Component = 'BOIL' THEN IntervalYrs END) AS BOILYrs, 
	COUNT(CASE WHEN Component = 'BOIL' THEN IntervalYrs END) AS BOILCount, 
	SUM(CASE WHEN Component = 'STG-HPS' THEN IntervalYrs END) AS STGHPSYrs, 
	COUNT(CASE WHEN Component = 'STG-HPS' THEN IntervalYrs END) AS STGHPSCount, 
	SUM(CASE WHEN Component = 'STG-IPS' THEN IntervalYrs END) AS STGIPSYrs, 
	COUNT(CASE WHEN Component = 'STG-IPS' THEN IntervalYrs END) AS STGIPSCount, 
	SUM(CASE WHEN Component = 'STG-LPS' THEN IntervalYrs END) AS STGLPSYrs, 
	COUNT(CASE WHEN Component = 'STG-LPS' THEN IntervalYrs END) AS STGLPSCount, 
	SUM(CASE WHEN Component = 'STG-GEN' OR Component = 'CTG-GEN' THEN IntervalYrs END) AS STGGENYrs, 
	COUNT(CASE WHEN Component = 'STG-GEN' OR Component = 'CTG-GEN' THEN IntervalYrs END) AS STGGENCount, 
	SUM(CASE WHEN Component = 'CTG-TURB' THEN IntervalYrs END) AS CTGTURBYrs, 
	COUNT(CASE WHEN Component = 'CTG-TURB' THEN IntervalYrs END) AS CTGTURBCount, 
	SUM(CASE WHEN Component = 'HRSG' OR Component = 'CTG-HRSG' THEN IntervalYrs END) AS HRSGYrs, 
	COUNT(CASE WHEN Component = 'HRSG' OR Component = 'CTG-HRSG' THEN IntervalYrs END) AS HRSGCount
FROM dbo.OHMaint
WHERE (ProjectID = 'OVHL')
GROUP BY Refnum


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "OHMaint"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'OHMaintByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'OHMaintByRefnum';

