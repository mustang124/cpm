﻿

CREATE        VIEW [dbo].[_ExecSummSiteView_SiteID] AS
SELECT t.SiteID, t.StudyYear,
HeatRate = SUM(g.heatrate * g.adjnetmwh)/SUM(g.AdjNetMWH),
MaintInd = SUM(g.MaintInd * gtc.AdjNetMWH2yr)/SUM(gtc.AdjNetMWH2yr),
EFOR = CASE WHEN SUM(p.EFORWtFactor)>0 THEN SUM(isnull(g.EFOR,0) * p.EFORWtFactor)/SUM(p.EFORWtFactor) ELSE AVG(p.EFOR) END,
EUOR = CASE WHEN SUM(p.EUORWtFactor)>0 THEN SUM(isnull(p.EUOR,0) * p.EUORWtFactor)/SUM(p.EUORWtFactor) ELSE AVG(p.EUOR) END,
EUF = CASE WHEN SUM(p.WPH) > 0 THEN SUM(isnull(g.EUF,0) * p.WPH)/SUM(p.WPH) ELSE AVG(g.EUF) END,
TotEffPersMW = CASE WHEN SUM(p.NMC) > 0 THEN SUM(TotEffPersMW * p.NMC)/SUM(p.NMC) ELSE NULL END,
TotEffPersMWH = SUM(TotEffPersMWH * g.AdjNetMWH)/SUM(g.AdjNetMWH),
TotCash = SUM(g.TotCash * g.AdjNetMWH)/SUM(g.AdjNetMWH),
AvgFuelCostMBTU = (SUM(o.STFuelCost*g.AdjNetMWH)/SUM(ftc.TotMBTU)),
ActCashLessFuelMWH = SUM(o.ActCashLessFuel*g.AdjNetMWH)/SUM(g.AdjNetMWH),
TotCashLessFuelMWH = SUM(o.TotCashLessFuel*g.AdjNetMWH)/SUM(g.AdjNetMWH),
TotCashLessFuelEmMWH = SUM(o.TotCashLessFuelEm*g.AdjNetMWH)/SUM(g.AdjNetMWH),
PeakUnavail_Unp = CASE WHEN SUM(c.TotPeakMWH) > 0 THEN SUM(c.PeakMWHLost_Unp)/SUM(c.TotPeakMWH)*100 ELSE NULL END,
TotEffPersEGC = CASE WHEN SUM(g.EGC) > 0 THEN SUM(TotEffPersEGC * g.EGC)/SUM(g.EGC) ELSE NULL END,
TotCashLessFuelEGC = CASE WHEN SUM(g.EGC) > 0 THEN SUM(o.TotCashLessFuelEm*g.AdjNetMWH)/SUM(g.EGC*1000) END,
ActCashLessFuelEGC = CASE WHEN SUM(g.EGC) > 0 THEN SUM(o.ActCashLessFuel*g.AdjNetMWH)/SUM(g.EGC*1000) END,
TotCashEGC = CASE WHEN SUM(g.EGC) > 0 THEN SUM(o.TotCash*g.AdjNetMWH)/SUM(g.EGC*1000) END,
MaintIndexEGC = CASE WHEN SUM(g.EGC) > 0 THEN SUM(g.MaintInd * gtc.AdjNetMWH2yr)/SUM(g.EGC*1000) END
FROM Gensum g, GenerationTotCalc gtc, NERCFactors p, tsort t, FuelTotCalc ftc, OpExCalc o, StudySites s, CommUnavail c
WHERE g.Refnum = gtc.Refnum
AND g.Refnum = p.Refnum
AND g.Refnum = t.Refnum
AND g.Refnum = ftc.Refnum
AND g.Refnum = o.Refnum
AND s.SiteID = t.SiteID
AND c.Refnum = t.Refnum
AND o.DataType = 'MWH'
AND g.AdjNetMWH > 0 AND ftc.TotMBTU>0
GROUP BY t.StudyYear, t.SiteID


