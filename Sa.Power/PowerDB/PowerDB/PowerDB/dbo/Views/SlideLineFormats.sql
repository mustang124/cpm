﻿

CREATE VIEW [dbo].[SlideLineFormats]
AS
SELECT SlideLineFormatID, LineColor, LineWidth, LineDashStyle, LineBeginArrow, LineEndArrow, MarkerType, MarkerSize, MarkerLastOnly, Notes
FROM GlobalDB.dbo.SlideLineFormats

