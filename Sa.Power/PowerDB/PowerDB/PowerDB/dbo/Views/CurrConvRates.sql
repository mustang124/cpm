﻿
CREATE  VIEW dbo.CurrConvRates
AS
SELECT     lu.CurrencyID, lu.Country, lu.Currency, c.[Year], c.ConvRate, c.CurrencyCode
FROM         dbo.Currency_LU lu INNER JOIN
                      dbo.CurrencyConv c ON lu.CurrencyCode = c.CurrencyCode

