﻿
CREATE  VIEW dbo.AbsenceCalc
AS
SELECT     dbo.TSort.Refnum, dbo.SiteAbsences.AbsCategory, dbo.SiteAbsences.SortKey, 
	OCCAbsPcnt = ISNULL(dbo.SiteAbsences.OCCAbsPcnt, 0), 
	MPSAbsPcnt = ISNULL(dbo.SiteAbsences.MPSAbsPcnt, 0),
        TotAbsPcnt = ISNULL(dbo.SiteAbsences.OCCAbsPcnt, 0) + ISNULL(dbo.SiteAbsences.MPSAbsPcnt, 0)
FROM         dbo.TSort INNER JOIN
                      dbo.SiteAbsences ON dbo.TSort.SiteID = dbo.SiteAbsences.SiteID


