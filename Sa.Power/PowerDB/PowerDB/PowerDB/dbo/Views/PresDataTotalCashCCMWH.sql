﻿

CREATE VIEW [dbo].[PresDataTotalCashCCMWH]
AS
SELECT     TOP (100) PERCENT ISNULL
                          ((SELECT     SUM(g.AdjNetMWH) AS Expr1
                              FROM         dbo.OpExCalc AS b INNER JOIN
                                                    dbo.GenSum AS g ON b.Refnum = g.Refnum
                              WHERE     (b.Refnum IN (select refnum from breaks where refnum in
                                                        (SELECT     Refnum
                                                          FROM          dbo._RL
                                                          WHERE      (ListName =
                                                                                     (SELECT     ListName
                                                                                       FROM          dbo.CurrentList))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH') AND (b.TotCash < a.TotCash) OR
                                                    (b.Refnum IN (select refnum from breaks where refnum in
                                                        (SELECT     Refnum
                                                          FROM          dbo._RL AS _RL_9
                                                          WHERE      (ListName =
                                                                                     (SELECT     ListName
                                                                                       FROM          dbo.CurrentList AS CurrentList_9))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH') AND (b.TotCash = a.TotCash) AND 
                                                    (b.STFuelCost <= a.STFuelCost)) /
                          (SELECT     SUM(g.AdjNetMWH) AS Expr1
                            FROM          dbo.OpExCalc AS b INNER JOIN
                                                   dbo.GenSum AS g ON b.Refnum = g.Refnum
                            WHERE      (b.Refnum IN (select refnum from breaks where refnum in
                                                       (SELECT     Refnum
                                                         FROM          dbo._RL AS _RL_8
                                                         WHERE      (ListName =
                                                                                    (SELECT     ListName
                                                                                      FROM          dbo.CurrentList AS CurrentList_8))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH')) * 100, 0) + (ISNULL
                          ((SELECT     SUM(g.AdjNetMWH) AS Expr1
                              FROM         dbo.OpExCalc AS b INNER JOIN
                                                    dbo.GenSum AS g ON b.Refnum = g.Refnum
                              WHERE     (b.Refnum IN (select refnum from breaks where refnum in
                                                        (SELECT     Refnum
                                                          FROM          dbo._RL AS _RL_7
                                                          WHERE      (ListName =
                                                                                     (SELECT     ListName
                                                                                       FROM          dbo.CurrentList AS CurrentList_7))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH') AND (b.TotCash < a.TotCash) OR
                                                    (b.Refnum IN (select refnum from breaks where refnum in
                                                        (SELECT     Refnum
                                                          FROM          dbo._RL AS _RL_6
                                                          WHERE      (ListName =
                                                                                     (SELECT     ListName
                                                                                       FROM          dbo.CurrentList AS CurrentList_6))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH') AND (b.TotCash = a.TotCash) AND 
                                                    (b.STFuelCost < a.STFuelCost)) /
                          (SELECT     SUM(g.AdjNetMWH) AS Expr1
                            FROM          dbo.OpExCalc AS b INNER JOIN
                                                   dbo.GenSum AS g ON b.Refnum = g.Refnum
                            WHERE      (b.Refnum IN (select refnum from breaks where refnum in
                                                       (SELECT     Refnum
                                                         FROM          dbo._RL AS _RL_5
                                                         WHERE      (ListName =
                                                                                    (SELECT     ListName
                                                                                      FROM          dbo.CurrentList AS CurrentList_5))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH')) * 100, 0) - ISNULL
                          ((SELECT     SUM(g.AdjNetMWH) AS Expr1
                              FROM         dbo.OpExCalc AS b INNER JOIN
                                                    dbo.GenSum AS g ON b.Refnum = g.Refnum
                              WHERE     (b.Refnum IN (select refnum from breaks where refnum in
                                                        (SELECT     Refnum
                                                          FROM          dbo._RL AS _RL_4
                                                          WHERE      (ListName =
                                                                                     (SELECT     ListName
                                                                                       FROM          dbo.CurrentList AS CurrentList_4))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH') AND (b.TotCash < a.TotCash) OR
                                                    (b.Refnum IN (select refnum from breaks where refnum in
                                                        (SELECT     Refnum
                                                          FROM          dbo._RL AS _RL_3
                                                          WHERE      (ListName =
                                                                                     (SELECT     ListName
                                                                                       FROM          dbo.CurrentList AS CurrentList_3))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH') AND (b.TotCash = a.TotCash) AND 
                                                    (b.STFuelCost <= a.STFuelCost)) /
                          (SELECT     SUM(g.AdjNetMWH) AS Expr1
                            FROM          dbo.OpExCalc AS b INNER JOIN
                                                   dbo.GenSum AS g ON b.Refnum = g.Refnum
                            WHERE      (b.Refnum IN (select refnum from breaks where refnum in
                                                       (SELECT     Refnum
                                                         FROM          dbo._RL AS _RL_2
                                                         WHERE      (ListName =
                                                                                    (SELECT     ListName
                                                                                      FROM          dbo.CurrentList AS CurrentList_2))) and CombinedCycle = 'Y')) AND (b.DataType = 'MWH')) * 100, 0)) / 2 AS CummPcnt, a.STFuelCost, 
                      a.TotCash AS Total
FROM         dbo.OpExCalc AS a INNER JOIN
                      dbo.GenSum AS g ON a.Refnum = g.Refnum
WHERE     (a.Refnum IN (select refnum from breaks where refnum in 
                          (SELECT     Refnum
                            FROM          dbo._RL AS _RL_1
                            WHERE      (ListName =
                                                       (SELECT     ListName
                                                         FROM          dbo.CurrentList AS CurrentList_1))) and CombinedCycle = 'Y')) AND (a.DataType = 'MWH')




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 6
               Left = 273
               Bottom = 125
               Right = 468
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1800
         Alias = 2235
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PresDataTotalCashCCMWH';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PresDataTotalCashCCMWH';

