﻿


CREATE VIEW [dbo].[_Traffic] AS

	--PostCloseRank contains rank averages for any unit that has been calced
	--a unit that has been ranked may have changed after ranking, so we should use that unit's data
	--but, what if it hasn't? 

	--what are the possibilities here?
	--pre-close calc, but not in close - this unit's rank will be nulls, right? should we ignore? it will be compared against prior year and thus be a post-close/not in close for that year
	--pre-close calc, then in close - ranks will be null before the close (?), then they will close and be ranked - should use the _RankView values
	--post-close calc, not in close - came in after close, needs to be calced and those values used
	--post-close calc, in close - was in close, then later re-calced - the final value in PostCloseRank should be used



	SELECT Refnum, Percentile, ListName, BreakCondition, BreakValue, RankVariableID
	FROM PostCloseRank

	UNION ALL

	SELECT Refnum, Percentile, ListName, BreakCondition, BreakValue, RankVariableID
	FROM _RankView r
	WHERE Refnum NOT IN (SELECT Refnum FROM PostCloseRank) --added just in case the unit gets recalced after being used in close



