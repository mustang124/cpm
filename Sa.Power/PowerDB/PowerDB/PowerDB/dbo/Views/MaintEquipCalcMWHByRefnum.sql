﻿

CREATE VIEW [dbo].[MaintEquipCalcMWHByRefnum]
AS
SELECT     Refnum, AVG(CASE WHEN EquipGroup = 'CPHM' THEN AnnMaintCostMWH ELSE NULL END) AS CPHM, 
                      AVG(CASE WHEN EquipGroup = 'BOIL' THEN AnnMaintCostMWH ELSE NULL END) AS BOIL, 
                      AVG(CASE WHEN EquipGroup = 'HRSG' THEN AnnMaintCostMWH ELSE NULL END) AS HRSG, 
                      AVG(CASE WHEN EquipGroup = 'TURB' THEN AnnMaintCostMWH ELSE NULL END) AS TURB, 
                      AVG(CASE WHEN EquipGroup = 'CA' THEN AnnMaintCostMWH ELSE NULL END) AS CA,
                      AVG(CASE WHEN EquipGroup = 'VC' THEN AnnMaintCostMWH ELSE NULL END) AS VC, 
                      AVG(CASE WHEN EquipGroup = 'GEN' THEN AnnMaintCostMWH ELSE NULL END) AS GEN, 
                      AVG(CASE WHEN EquipGroup = 'CTG' THEN AnnMaintCostMWH ELSE NULL END) AS CTG, 
                      AVG(CASE WHEN EquipGroup = 'BAG' THEN AnnMaintCostMWH ELSE NULL END) AS BAG, 
                      AVG(CASE WHEN EquipGroup = 'PREC' THEN AnnMaintCostMWH ELSE NULL END) AS PREC, 
                      AVG(CASE WHEN EquipGroup = 'WGS' THEN AnnMaintCostMWH ELSE NULL END) AS WGS, 
                      AVG(CASE WHEN EquipGroup = 'DGS' THEN AnnMaintCostMWH ELSE NULL END) AS DGS, 
                      AVG(CASE WHEN EquipGroup = 'FHF' THEN AnnMaintCostMWH ELSE NULL END) AS FHF, 
                      AVG(CASE WHEN EquipGroup = 'CWF' THEN AnnMaintCostMWH ELSE NULL END) AS CWF, 
                      AVG(CASE WHEN EquipGroup = 'ASH' THEN AnnMaintCostMWH ELSE NULL END) AS ASH, 
                      AVG(CASE WHEN EquipGroup = 'SCR' THEN AnnMaintCostMWH ELSE NULL END) AS SCR, 
                      AVG(CASE WHEN EquipGroup = 'BOILH2O' THEN AnnMaintCostMWH ELSE NULL END) AS BOILH2O, 
                      AVG(CASE WHEN EquipGroup = 'WASTEH2O' THEN AnnMaintCostMWH ELSE NULL END) AS WASTEH2O, 
                      AVG(CASE WHEN EquipGroup = 'OTHER' THEN AnnMaintCostMWH ELSE NULL END) AS OTHER,
						AVG(CASE WHEN EquipGroup = 'CTCOMB' THEN AnnMaintCostMWH ELSE NULL END) AS CTCOMB,
						AVG(CASE WHEN EquipGroup = 'CTCOMP' THEN AnnMaintCostMWH ELSE NULL END) AS CTCOMP,
						AVG(CASE WHEN EquipGroup = 'CTGEN' THEN AnnMaintCostMWH ELSE NULL END) AS CTGEN,
						AVG(CASE WHEN EquipGroup = 'CTHRSG' THEN AnnMaintCostMWH ELSE NULL END) AS CTHRSG,
						AVG(CASE WHEN EquipGroup = 'CTOTH' THEN AnnMaintCostMWH ELSE NULL END) AS CTOTH,
						AVG(CASE WHEN EquipGroup = 'CTSCR' THEN AnnMaintCostMWH ELSE NULL END) AS CTSCR,
						AVG(CASE WHEN EquipGroup = 'CTTRANS' THEN AnnMaintCostMWH ELSE NULL END) AS CTTRANS,
						AVG(CASE WHEN EquipGroup = 'CTTURB' THEN AnnMaintCostMWH ELSE NULL END) AS CTTURB,
						AVG(CASE WHEN EquipGroup = 'DQWS' THEN AnnMaintCostMWH ELSE NULL END) AS DQWS,
						AVG(CASE WHEN EquipGroup = 'TRANS' THEN AnnMaintCostMWH ELSE NULL END) AS TRANS


                      
                      
FROM         dbo.MaintEquipCalc
GROUP BY Refnum



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MaintEquipCalc"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MaintEquipCalcMWHByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'MaintEquipCalcMWHByRefnum';

