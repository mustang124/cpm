﻿
CREATE VIEW [dbo].[PresDataSGOSize]
AS
SELECT     CASE WHEN nt.NMC < 300 THEN '<300' WHEN nt.NMC >= 300 AND nt.NMC <= 500 THEN '300-500' 
				WHEN nt.NMC > 500 THEN '>500' END AS Size, CAST(CAST(COUNT(*) AS numeric) / CAST
                          ((SELECT     COUNT(*) AS Expr1
                              FROM         dbo.NERCFactors AS nt INNER JOIN
                                                    dbo.Breaks AS b ON b.Refnum = nt.Refnum
                              WHERE     (nt.Refnum IN
                                                        (SELECT     Refnum
                                                          FROM          dbo._RL
                                                          WHERE      (ListName =
                                                                                     (SELECT     ListName + ' Steam' AS Expr1
                                                                                       FROM          dbo.CurrentList)))) AND (b.FuelGroup = 'Gas & Oil')) AS numeric) * 100 AS numeric(10, 4)) AS Pct, 
                      CASE WHEN nt.NMC < 300 THEN 1 WHEN nt.NMC >= 300 AND nt.NMC <= 500 THEN 2 WHEN nt.NMC > 500 THEN 3 END AS SortOrder
FROM         dbo.NERCFactors AS nt INNER JOIN
                      dbo.Breaks AS b ON b.Refnum = nt.Refnum
WHERE     (nt.Refnum IN
                          (SELECT     Refnum
                            FROM          dbo._RL AS _RL_1
                            WHERE      (ListName =
                                                       (SELECT     ListName + ' Steam' AS Expr1
                                                         FROM          dbo.CurrentList AS CurrentList_1)))) AND (b.FuelGroup = 'Gas & Oil')
GROUP BY CASE WHEN nt.NMC < 300 THEN '<300' WHEN nt.NMC >= 300 AND nt.NMC <= 500 THEN '300-500' WHEN nt.NMC > 500 THEN '>500' END, 
		CASE WHEN nt.NMC < 300 THEN 1 WHEN nt.NMC >= 300 AND nt.NMC <= 500 THEN 2 WHEN nt.NMC > 500 THEN 3 END


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "nt"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 232
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 270
               Bottom = 125
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PresDataSGOSize';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PresDataSGOSize';

