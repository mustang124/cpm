﻿CREATE VIEW dbo.CTGSummary
AS
SELECT     c.Refnum, c.TurbineID, n.NMC,
                          (SELECT     SUM(MBTU * 1000)
                            FROM          Fuel f
                            WHERE      f.Refnum = c.Refnum AND f.TurbineID = c.TurbineID AND DATEPART(yy, Period) = t.StudyYear) /
                          (SELECT     SUM(NetMWH)
                            FROM          PowerGeneration p
                            WHERE      p.Refnum = c.Refnum AND p.TurbineID = c.TurbineID AND DATEPART(yy, Period) = t.StudyYear) AS HeatRate,
                          (SELECT     SUM(MBTULHV * 1000)
                            FROM          Fuel f
                            WHERE      f.Refnum = c.Refnum AND f.TurbineID = c.TurbineID AND DATEPART(yy, Period) = t.StudyYear) /
                          (SELECT     SUM(NetMWH)
                            FROM          PowerGeneration p
                            WHERE      p.Refnum = c.Refnum AND p.TurbineID = c.TurbineID AND DATEPART(yy, Period) = t.StudyYear) AS HeatRateLHV,
                          (SELECT     SUM(NetMWH)
                            FROM          PowerGeneration p
                            WHERE      p.Refnum = c.Refnum AND p.TurbineID = c.TurbineID AND DATEPART(yy, Period) = t.StudyYear) AS NetMWH,
                          (SELECT     SUM(NetMWH)
                            FROM          PowerGeneration p
                            WHERE      p.Refnum = c.Refnum AND p.TurbineID = c.TurbineID AND DATEPART(yy, Period) = (t.StudyYear - 1)) AS PriorNetMWH, n.ServiceHrs, 
                      n.NCF, n.NOF, n.NCF2Yr, n.NOF2Yr, n.WPH2Yr, n.NOF2YrWtFactor, s.SuccessPcnt, s.ForcedPcnt, s.MaintPcnt, s.StartupPcnt, s.DeratePcnt, 
                      s.TotalOpps
FROM         dbo.TSort t INNER JOIN
                      dbo.CTGData c ON c.Refnum = t.Refnum LEFT OUTER JOIN
                      dbo.NERCTurbine n ON n.Refnum = c.Refnum AND n.TurbineID = c.TurbineID LEFT OUTER JOIN
                      dbo.StartsAnalysis s ON s.Refnum = c.Refnum AND s.TurbineID = c.TurbineID
WHERE     ((SELECT     SUM(NetMWH)
                         FROM         PowerGeneration p
                         WHERE     p.Refnum = c.Refnum AND p.TurbineID = c.TurbineID AND DATEPART(yy, Period) = t.StudyYear) > 0)
