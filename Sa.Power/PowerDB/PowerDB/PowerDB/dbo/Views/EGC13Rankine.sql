﻿







CREATE VIEW [dbo].[EGC13Rankine]
AS
SELECT 
	e.Refnum, 
	t.CoLoc,
	t.StudyYear,
	nf.Age,
	nf.NDC,
	CTG_Count = ISNULL(nt_CTG.CTG_Count,0),
	CTG_NDC = ISNULL(nt_CTG.CTG_NDC,0),
	NumUnitsAtSite = ISNULL(p.CoalUnitNum,0) + ISNULL(p.OilGasUnitNum,0),
	Site_NDC = ISNULL(p.CoalUnitCap,0) + ISNULL(p.OilGasUnitCap,0),
	EGCRegion,
	clu.FrequencyHz,
	ftc.FuelType,
	PrecBagYN = ISNULL(ps.PrecBagYN,0),
	ScrubbersYN = ISNULL(ps.ScrubbersYN,0),
	DesignEffPct = CASE ISNULL(ps.ScrubbersYN,0) WHEN 0 THEN NULL ELSE CASE ISNULL(ms.DesignEffPct,0) WHEN 0 THEN 94 ELSE ms.DesignEffPct END END, --removes the zeroes
	SCR = ISNULL(ps.SCR,0),
	dd.BlrPSIG,
	dd.BlrTemp,
	nf.ServiceHrs,
	gtc.AdjNetMWH,
	gtc.AdjNetMWH2Yr,
	Starts = CASE ISNULL(gtc.TotStarts,0) WHEN 0 THEN ISNULL(nf.ACTStarts,0) ELSE ISNULL(gtc.TotStarts,0) END,
	mtc.AnnOHCostMW,
	mtc.AnnNonOHCostMW,
	ss.StmSalesMBTU,
	ss.ABSStmSalesMBTU,
	LightLiquidMBTU = ISNULL(DieselMBTU,0) + ISNULL(JetTurbMBTU,0),
	HeavyLiquidMBTU = ISNULL(FuelOilMBTU,0),
	NatGasMBTU = ISNULL(NatGasMBTU,0) + ISNULL(H2GasMBTU,0),
	OffGasMBTU = ISNULL(OffGasMBTU,0),
	Coal1Tons = c1.Tons, 
	Coal1MBTU = c1.MBTU, 
	Coal1AshPcnt = c1.AshPcnt, 
	Coal1SulfurPcnt = c1.SulfurPcnt, 
	Coal1MoistPcnt = c1.MoistPcnt,
	Coal2Tons = c2.Tons, 
	Coal2MBTU = c2.MBTU, 
	Coal2AshPcnt = c2.AshPcnt, 
	Coal2SulfurPcnt = c2.SulfurPcnt, 
	Coal2MoistPcnt = c2.MoistPcnt,
	Coal3Tons = c3.Tons, 
	Coal3MBTU = c3.MBTU, 
	Coal3AshPcnt = c3.AshPcnt, 
	Coal3SulfurPcnt = c3.SulfurPcnt, 
	Coal3MoistPcnt = c3.MoistPcnt,
	o.TotCashLessFuelEm
	

FROM dbo.EGC13 AS e
	LEFT JOIN TSort t ON t.Refnum = e.Refnum
	LEFT JOIN NERCFactors nf ON nf.Refnum = e.Refnum
	LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = e.Refnum
	LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = e.Refnum
	LEFT JOIN OpExCalc o ON o.Refnum = e.Refnum AND o.DataType = 'ADJ'
	LEFT JOIN Misc ms ON ms.Refnum = e.Refnum AND ms.TurbineID = 'STG'
	LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = e.Refnum
	LEFT JOIN (SELECT Refnum, CTG_Count = COUNT(*), CTG_NDC = SUM(NDC) 
				FROM NERCTurbine 
				WHERE TurbineType = 'CTG' --AND NDC IS NOT NULL 
				GROUP BY Refnum) nt_CTG ON nt_CTG.Refnum = e.Refnum
	LEFT JOIN (SELECT Refnum, 
					PrecBagYN = CASE SUM(CASE WHEN Component IN ('PREC','BAG') THEN totannohcost else 0 end) WHEN 0 THEN 0 ELSE 1 END,
					ScrubbersYN = CASE SUM(CASE WHEN Component IN ('WGS','DGS') THEN totannohcost else 0 end) WHEN 0 THEN 0 ELSE 1 END,
					SCR = CASE SUM(CASE WHEN Component IN ('SCR') THEN totannohcost else 0 end) WHEN 0 THEN 0 ELSE 1 END
				FROM (SELECT Refnum, Component, TotAnnOHCost FROM OHMaint UNION SELECT Refnum, Component, AnnNonOHCost FROM NonOHMaint UNION SELECT Refnum, 'WGS', ScrCostLime FROM Misc WHERE ScrCostLime IS NOT NULL) b -- the Misc part calcs the scrubber cost, set to 'WGS' just for it to count from
				WHERE Component IN ('BAG','PREC','WGS','DGS','SCR')
				GROUP BY Refnum) ps ON ps.Refnum = e.Refnum
	LEFT JOIN (SELECT Refnum, AshPcnt = MAX(AshPcnt), SulfurPcnt = MAX(SulfurPcnt), MoistPcnt = MAX(MoistPcnt), Tons = SUM(Tons), MBTU = SUM(MBTU) FROM Coal WHERE CoalID = '1' GROUP BY Refnum) c1 ON c1.Refnum = e.Refnum
	LEFT JOIN (SELECT Refnum, AshPcnt = MAX(AshPcnt), SulfurPcnt = MAX(SulfurPcnt), MoistPcnt = MAX(MoistPcnt), Tons = SUM(Tons), MBTU = SUM(MBTU) FROM Coal WHERE CoalID = '2' GROUP BY Refnum) c2 ON c2.Refnum = e.Refnum
	LEFT JOIN (SELECT Refnum, AshPcnt = MAX(AshPcnt), SulfurPcnt = MAX(SulfurPcnt), MoistPcnt = MAX(MoistPcnt), Tons = SUM(Tons), MBTU = SUM(MBTU) FROM Coal WHERE CoalID = '3' GROUP BY Refnum) c3 ON c3.Refnum = e.Refnum
	LEFT JOIN (SELECT nt.Refnum, BlrPSIG = MAX(d.BlrPSIG), BlrTemp = MAX(d.blrtemp) FROM NERCTurbine nt LEFT JOIN DesignData d ON d.UtilityUnitCode = nt.UtilityUnitCode GROUP BY nt.Refnum) dd ON dd.Refnum = e.Refnum
	LEFT JOIN (SELECT s.refnum, StmSalesMBTU = SUM(StmSalesMBTU), ABSStmSalesMBTU = SUM(ABS(StmSalesMBTU)) FROM SteamSales s LEFT JOIN TSort t ON t.Refnum = s.Refnum WHERE YEAR(s.Period) = t.StudyYear GROUP BY s.Refnum) ss ON ss.Refnum = e.Refnum
	LEFT JOIN PowerGlobal.dbo.Country_LU clu ON clu.Country = t.Country

WHERE e.Refnum NOT LIKE '%CC%'








GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "e"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 80
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'EGC13Rankine';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'EGC13Rankine';

