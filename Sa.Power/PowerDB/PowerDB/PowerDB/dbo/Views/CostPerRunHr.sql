﻿



CREATE   VIEW CostPerRunHr
AS
SELECT o.Refnum, CASE WHEN n.ServiceHrs>0 THEN (o.STFixed - ISNULL(o.LTSAAdj, 0) - ISNULL(o.OverhaulAdj, 0))*1000/n.ServiceHrs + m.LTSA + m.Overhaul ELSE NULL END AS Fixed, 
CASE WHEN n.ServiceHrs>0 THEN (o.STVar - o.STFuelCost)*1000/n.ServiceHrs ELSE NULL END AS VarLessFuel,
m.LTSA, m.Overhaul, Total = CASE WHEN n.ServiceHrs>0 THEN (o.TotCash - ISNULL(o.LTSAAdj, 0) - ISNULL(o.OverhaulAdj, 0) - o.STFuelCost)*1000/n.ServiceHrs + m.LTSA + m.Overhaul ELSE NULL END
FROM OpexCalc o INNER JOIN NERCFactors n ON n.Refnum = o.Refnum
INNER JOIN MaintPerRunHr m ON m.Refnum = o.Refnum
WHERE o.DataType = 'ADJ'





