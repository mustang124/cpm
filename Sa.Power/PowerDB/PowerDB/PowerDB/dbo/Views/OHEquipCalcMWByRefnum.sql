﻿





CREATE VIEW [dbo].[OHEquipCalcMWByRefnum]
AS
SELECT     Refnum, 
				SUM(CASE WHEN Component = 'CPHM' THEN TotAnnOHCostMW ELSE NULL END) AS CPHM, 
				SUM(CASE WHEN Component = 'BOIL' THEN TotAnnOHCostMW ELSE NULL END) AS BOIL, 
				SUM(CASE WHEN Component = 'HRSG' THEN TotAnnOHCostMW ELSE NULL END) AS HRSG, 
				SUM(CASE WHEN Component = 'BAG' THEN TotAnnOHCostMW ELSE NULL END) AS BAG, 
				SUM(CASE WHEN Component = 'PREC' THEN TotAnnOHCostMW ELSE NULL END) AS PREC, 
				SUM(CASE WHEN Component = 'WGS' THEN TotAnnOHCostMW ELSE NULL END) AS WGS, 
				SUM(CASE WHEN Component = 'DGS' THEN TotAnnOHCostMW ELSE NULL END) AS DGS, 
				SUM(CASE WHEN Component = 'FHF' THEN TotAnnOHCostMW ELSE NULL END) AS FHF, 
				SUM(CASE WHEN Component = 'CWF' THEN TotAnnOHCostMW ELSE NULL END) AS CWF, 
				SUM(CASE WHEN Component = 'ASH' THEN TotAnnOHCostMW ELSE NULL END) AS ASH, 
				SUM(CASE WHEN Component = 'SCR' THEN TotAnnOHCostMW ELSE NULL END) AS SCR, 
				SUM(CASE WHEN Component = 'BOILH2O' THEN TotAnnOHCostMW ELSE NULL END) AS BOILH2O, 
				SUM(CASE WHEN Component = 'WASTEH2O' THEN TotAnnOHCostMW ELSE NULL END) AS WASTEH2O, 
				SUM(CASE WHEN Component = 'OTHER' THEN TotAnnOHCostMW ELSE NULL END) AS OTHER,
				SUM(CASE WHEN Component = 'DQWS' THEN TotAnnOHCostMW ELSE NULL END) AS DQWS,
				SUM(CASE WHEN Component = 'CTG-ALL' THEN TotAnnOHCostMW ELSE NULL END) AS CTGALL,
				SUM(CASE WHEN Component = 'CTG-AUX' THEN TotAnnOHCostMW ELSE NULL END) AS CTGAUX,
				SUM(CASE WHEN Component = 'STG-IPS' THEN TotAnnOHCostMW ELSE NULL END) AS STGIPS,
				SUM(CASE WHEN Component = 'STG-LPS' THEN TotAnnOHCostMW ELSE NULL END) AS STGLPS,
				SUM(CASE WHEN Component = 'CTG-COMB' THEN TotAnnOHCostMW ELSE NULL END) AS CTGCOMB,
				SUM(CASE WHEN Component = 'CTG-COMP' THEN TotAnnOHCostMW ELSE NULL END) AS CTGCOMP,
				SUM(CASE WHEN Component = 'CTG-GEN' THEN TotAnnOHCostMW ELSE NULL END) AS CTGGEN,
				SUM(CASE WHEN Component = 'CTG-HRSG' THEN TotAnnOHCostMW ELSE NULL END) AS CTGHRSG,
				SUM(CASE WHEN Component = 'CTG-OTH' THEN TotAnnOHCostMW ELSE NULL END) AS CTGOTH,
				SUM(CASE WHEN Component = 'CTG-TRAN' THEN TotAnnOHCostMW ELSE NULL END) AS CTGTRAN,
				SUM(CASE WHEN Component = 'CTG-TURB' THEN TotAnnOHCostMW ELSE NULL END) AS CTGTURB,
				SUM(CASE WHEN Component = 'SITETRAN' THEN TotAnnOHCostMW ELSE NULL END) AS SITETRAN,
				SUM(CASE WHEN Component = 'STG-CA' THEN TotAnnOHCostMW ELSE NULL END) AS STGCA,
				SUM(CASE WHEN Component = 'STG-GEN' THEN TotAnnOHCostMW ELSE NULL END) AS STGGEN,
				SUM(CASE WHEN Component = 'STG-HPS' THEN TotAnnOHCostMW ELSE NULL END) AS STGHPS,
				SUM(CASE WHEN Component = 'STG-TURB' THEN TotAnnOHCostMW ELSE NULL END) AS STGTURB,
				SUM(CASE WHEN Component = 'STG-VC' THEN TotAnnOHCostMW ELSE NULL END) AS STGVC,
				SUM(CASE WHEN Component = 'CTG-SCR' THEN TotAnnOHCostMW ELSE NULL END) AS CTGSCR
                     
                      
FROM         dbo.OHEquipCalc
GROUP BY Refnum







GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "OHEquipCalc"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'OHEquipCalcMWByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'OHEquipCalcMWByRefnum';

