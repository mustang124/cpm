﻿

CREATE  VIEW dbo.AbsenceTotCalc
AS
SELECT     t.Refnum, SUM(ISNULL(a.OCCAbsPcnt, 0)) AS OCCAbsPcnt, SUM(ISNULL(a.MPSAbsPcnt, 0)) AS MPSAbsPcnt, SUM(ISNULL(a.TotAbsPcnt, 0)) AS TotAbsPcnt
FROM         dbo.TSort t INNER JOIN
                      dbo.SiteAbsences a ON a.SiteID = t.SiteID
GROUP BY t.Refnum


