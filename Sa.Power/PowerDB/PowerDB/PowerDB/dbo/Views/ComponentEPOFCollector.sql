﻿CREATE      VIEW [dbo].[ComponentEPOFCollector]
AS
-- ALTERd by FRS and GLC as EUF
-- Modified by SW 7/27/11 to get EPOF
-- Gives  the EPOF by Component
-- Modified by FRS to include the UnitType from Gads Setup table.
-- It will be used to determine whether turbine and generator 
-- events go to the steam turbine or the CT turbine.
SELECT nt.Refnum, s.UnitType, cc.SAIMajorEquip, cc.SAIMinorEquip,
SUM(EquivMWhrs) /
(
SELECT SUM(E_PH) 
FROM GADSOS.GADSNG.EventHours eh2
WHERE (eh2.UnitShortName = s.UnitShortName) AND (DatePart(yy,eh2.TL_DateTime) =  t.EvntYear) AND (eh2.Granularity = 'Yearly')
AND E_PH > 0 -- This line added to avoid Divide By Zero problem.
             -- Zero E_PH will now cause NULL EUF in the view instead of
             -- the view erroring due to Divide By Zero.
) * 100 AS EPOF,
(
SELECT SUM(E_PH) 
FROM GADSOS.GADSNG.EventHours eh2
WHERE (eh2.UnitShortName = s.UnitShortName) AND (DatePart(yy,eh2.TL_DateTime) =  t.EvntYear) AND (eh2.Granularity = 'Yearly')
) AS E_PH
FROM GADSOS.GADSNG.EventDetails e
INNER JOIN GADSOS.GADSNG.Setup s ON s.UnitShortName = e.UnitShortName
INNER JOIN dbo.NERCTurbine nt on nt.UtilityUnitCode = s.UtilityUnitCode
INNER JOIN dbo.TSort t on t.Refnum = nt.Refnum
INNER JOIN dbo.CauseCodes cc on cc.Cause_Code = e.CauseCode
WHERE (DatePart(yy,TL_DateTime) =  t.EvntYear) AND (e.Granularity = 'Yearly')
AND EventType like 'P%' --SW - this filter pulls only planned events
and e.EquivMWhrs > 0
GROUP BY  nt.Refnum, s.UnitType, s.UnitShortName, t.EvntYear, cc.SAIMajorEquip, cc.SAIMinorEquip
