﻿

CREATE VIEW [dbo].[vwSlideAnnotations]
AS

	SELECT SlideID = ParentID, AnnotationText, [Left], [Top], Width, Height, fontHeight, Wrap, Alignment, Color, BackColor
	FROM SlideAnnotations sa
		LEFT JOIN SlideAnnotationText st ON st.SlideAnnotationTextID = sa.AnnotationTextID
		LEFT JOIN SlideAnnotationPositions sp ON sp.AnnotationPosition = sa.AnnotationPosition
		LEFT JOIN SlideColors sc ON sc.SlideColorGroup = sp.SlideColorGroup


