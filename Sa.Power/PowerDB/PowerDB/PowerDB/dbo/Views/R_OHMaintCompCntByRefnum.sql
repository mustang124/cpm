﻿




CREATE VIEW [dbo].[R_OHMaintCompCntByRefnum]
AS
SELECT a.Refnum, 
	CASE WHEN ASH > 0 THEN t.CompanyID ELSE NULL END AS ASH, 
	CASE WHEN BAG > 0 THEN t.CompanyID ELSE NULL END AS BAG, 
	CASE WHEN BOIL > 0 THEN t.CompanyID ELSE NULL END AS BOIL, 
	CASE WHEN BOILH2O > 0 THEN t.CompanyID ELSE NULL END AS BOILH2O, 
	CASE WHEN CPHM > 0 THEN t.CompanyID ELSE NULL END AS CPHM, 
	CASE WHEN CTGALL > 0 THEN t.CompanyID ELSE NULL END AS CTGALL,
	CASE WHEN CTGAUX > 0 THEN t.CompanyID ELSE NULL END AS CTGAUX,
	CASE WHEN CTGCOMB > 0 THEN t.CompanyID ELSE NULL END AS CTGCOMB,
	CASE WHEN CTGCOMP > 0 THEN t.CompanyID ELSE NULL END AS CTGCOMP,
	CASE WHEN CTGGEN > 0 THEN t.CompanyID ELSE NULL END AS CTGGEN,
	CASE WHEN CTGHRSG > 0 THEN t.CompanyID ELSE NULL END AS CTGHRSG,
	CASE WHEN CTGOTH > 0 THEN t.CompanyID ELSE NULL END AS CTGOTH,
	CASE WHEN CTGTRAN > 0 THEN t.CompanyID ELSE NULL END AS CTGTRAN,
	CASE WHEN CTGTURB > 0 THEN t.CompanyID ELSE NULL END AS CTGTURB,
	CASE WHEN CWF > 0 THEN t.CompanyID ELSE NULL END AS CWF, 
	CASE WHEN DGS > 0 THEN t.CompanyID ELSE NULL END AS DGS, 
	CASE WHEN DQWS > 0 THEN t.CompanyID ELSE NULL END AS DQWS, 
	CASE WHEN FHF > 0 THEN t.CompanyID ELSE NULL END AS FHF, 
	CASE WHEN HRSG > 0 THEN t.CompanyID ELSE NULL END AS HRSG,
	CASE WHEN OTHER > 0 THEN t.CompanyID ELSE NULL END AS OTHER, 
	CASE WHEN PREC > 0 THEN t.CompanyID ELSE NULL END AS PREC, 
	CASE WHEN SCR > 0 THEN t.CompanyID ELSE NULL END AS SCR, 
	CASE WHEN SITETRAN > 0 THEN t.CompanyID ELSE NULL END AS SITETRAN,
	CASE WHEN STGCA > 0 THEN t.CompanyID ELSE NULL END AS STGCA,
	CASE WHEN STGGEN > 0 THEN t.CompanyID ELSE NULL END AS STGGEN,
	CASE WHEN STGHPS > 0 THEN t.CompanyID ELSE NULL END AS STGHPS,
	CASE WHEN STGIPS > 0 THEN t.CompanyID ELSE NULL END AS STGIPS,
	CASE WHEN STGLPS > 0 THEN t.CompanyID ELSE NULL END AS STGLPS,
	CASE WHEN STGTURB > 0 THEN t.CompanyID ELSE NULL END AS STGTURB,
	CASE WHEN STGVC > 0 THEN t.CompanyID ELSE NULL END AS STGVC,
	CASE WHEN WASTEH2O > 0 THEN t.CompanyID ELSE NULL END AS WASTEH2O, 
	CASE WHEN WGS > 0 THEN t.CompanyID ELSE NULL END AS WGS

FROM (
	SELECT DISTINCT Refnum, 
		SUM(CASE WHEN Component = 'ASH' THEN 1 ELSE 0 END) AS ASH, 
		SUM(CASE WHEN Component = 'BAG' THEN 1 ELSE 0 END) AS BAG, 
		SUM(CASE WHEN Component = 'BOIL' THEN 1 ELSE 0 END) AS BOIL, 
		SUM(CASE WHEN Component = 'BOILH2O' THEN 1 ELSE 0 END) AS BOILH2O, 
		SUM(CASE WHEN Component = 'CPHM' THEN 1 ELSE 0 END) AS CPHM, 
		SUM(CASE WHEN Component = 'CTG-ALL' THEN 1 ELSE 0 END) AS CTGALL,
		SUM(CASE WHEN Component = 'CTG-AUX' THEN 1 ELSE 0 END) AS CTGAUX,
		SUM(CASE WHEN Component = 'CTG-COMB' THEN 1 ELSE 0 END) AS CTGCOMB,
		SUM(CASE WHEN Component = 'CTG-COMP' THEN 1 ELSE 0 END) AS CTGCOMP,
		SUM(CASE WHEN Component = 'CTG-GEN' THEN 1 ELSE 0 END) AS CTGGEN,
		SUM(CASE WHEN Component = 'CTG-HRSG' THEN 1 ELSE 0 END) AS CTGHRSG,
		SUM(CASE WHEN Component = 'CTG-OTH' THEN 1 ELSE 0 END) AS CTGOTH,
		SUM(CASE WHEN Component = 'CTG-TRAN' THEN 1 ELSE 0 END) AS CTGTRAN,
		SUM(CASE WHEN Component = 'CTG-TURB' THEN 1 ELSE 0 END) AS CTGTURB,
		SUM(CASE WHEN Component = 'CWF' THEN 1 ELSE 0 END) AS CWF, 
		SUM(CASE WHEN Component = 'DGS' THEN 1 ELSE 0 END) AS DGS, 
		SUM(CASE WHEN Component = 'DQWS' THEN 1 ELSE 0 END) AS DQWS, 
		SUM(CASE WHEN Component = 'FHF' THEN 1 ELSE 0 END) AS FHF, 
		SUM(CASE WHEN Component = 'HRSG' THEN 1 ELSE 0 END) AS HRSG,
		SUM(CASE WHEN Component = 'OTHER' THEN 1 ELSE 0 END) AS OTHER, 
		SUM(CASE WHEN Component = 'PREC' THEN 1 ELSE 0 END) AS PREC, 
		SUM(CASE WHEN Component = 'SCR' THEN 1 ELSE 0 END) AS SCR, 
		SUM(CASE WHEN Component = 'SITETRAN' THEN 1 ELSE 0 END) AS SITETRAN,
		SUM(CASE WHEN Component = 'STG-CA' THEN 1 ELSE 0 END) AS STGCA,
		SUM(CASE WHEN Component = 'STG-GEN' THEN 1 ELSE 0 END) AS STGGEN,
		SUM(CASE WHEN Component = 'STG-HPS' THEN 1 ELSE 0 END) AS STGHPS,
		SUM(CASE WHEN Component = 'STG-IPS' THEN 1 ELSE 0 END) AS STGIPS,
		SUM(CASE WHEN Component = 'STG-LPS' THEN 1 ELSE 0 END) AS STGLPS,
		SUM(CASE WHEN Component = 'STG-TURB' THEN 1 ELSE 0 END) AS STGTURB,
		SUM(CASE WHEN Component = 'STG-VC' THEN 1 ELSE 0 END) AS STGVC,
		SUM(CASE WHEN Component = 'WASTEH2O' THEN 1 ELSE 0 END) AS WASTEH2O, 
		SUM(CASE WHEN Component = 'WGS' THEN 1 ELSE 0 END) AS WGS

	FROM dbo.R_OHMaintCompCnt
	GROUP BY Refnum) AS a 
INNER JOIN dbo.TSort AS t ON t.Refnum = a.Refnum






GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 414
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'R_OHMaintCompCntByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'R_OHMaintCompCntByRefnum';

