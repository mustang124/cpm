﻿




CREATE VIEW [dbo].[NonOHMaintMWByRefnum]
AS
SELECT     Refnum, 
				SUM(CASE WHEN Component = 'ASH' THEN AnnNonOHCostMW ELSE NULL END) AS ASH, 
				SUM(CASE WHEN Component = 'BAG' THEN AnnNonOHCostMW ELSE NULL END) AS BAG, 
				SUM(CASE WHEN Component = 'BLR-AIR' THEN AnnNonOHCostMW ELSE NULL END) AS BLRAIR,
				SUM(CASE WHEN Component = 'BLR-BOIL' THEN AnnNonOHCostMW ELSE NULL END) AS BLRBOIL,
				SUM(CASE WHEN Component = 'BLR-COND' THEN AnnNonOHCostMW ELSE NULL END) AS BLRCOND,
				SUM(CASE WHEN Component = 'BOIL' THEN AnnNonOHCostMW ELSE NULL END) AS BOIL, 
				SUM(CASE WHEN Component = 'BOILH2O' THEN AnnNonOHCostMW ELSE NULL END) AS BOILH2O, 
				SUM(CASE WHEN Component = 'CPHM' THEN AnnNonOHCostMW ELSE NULL END) AS CPHM, 
				SUM(CASE WHEN Component = 'CTG-COMB' THEN AnnNonOHCostMW ELSE NULL END) AS CTGCOMB,
				SUM(CASE WHEN Component = 'CTG-COMP' THEN AnnNonOHCostMW ELSE NULL END) AS CTGCOMP,
				SUM(CASE WHEN Component = 'CTG-GEN' THEN AnnNonOHCostMW ELSE NULL END) AS CTGGEN,
				SUM(CASE WHEN Component = 'CTG-HRSG' THEN AnnNonOHCostMW ELSE NULL END) AS CTGHRSG,
				SUM(CASE WHEN Component = 'CTG-OTH' THEN AnnNonOHCostMW ELSE NULL END) AS CTGOTH,
				SUM(CASE WHEN Component = 'CTG-SCR' THEN AnnNonOHCostMW ELSE NULL END) AS CTGSCR,
				SUM(CASE WHEN Component = 'CTG-TRAN' THEN AnnNonOHCostMW ELSE NULL END) AS CTGTRAN,
				SUM(CASE WHEN Component = 'CTG-TURB' THEN AnnNonOHCostMW ELSE NULL END) AS CTGTURB,
				SUM(CASE WHEN Component = 'CWF' THEN AnnNonOHCostMW ELSE NULL END) AS CWF, 
				SUM(CASE WHEN Component = 'DGS' THEN AnnNonOHCostMW ELSE NULL END) AS DGS, 
				SUM(CASE WHEN Component = 'DQWS' THEN AnnNonOHCostMW ELSE NULL END) AS DQWS,
				SUM(CASE WHEN Component = 'FHF' THEN AnnNonOHCostMW ELSE NULL END) AS FHF, 
				SUM(CASE WHEN Component = 'FUELDEL' THEN AnnNonOHCostMW ELSE NULL END) AS FUELDEL,
				SUM(CASE WHEN Component = 'HRSG' THEN AnnNonOHCostMW ELSE NULL END) AS HRSG, 
				SUM(CASE WHEN Component = 'OTHER' THEN AnnNonOHCostMW ELSE NULL END) AS OTHER,
				SUM(CASE WHEN Component = 'PREC' THEN AnnNonOHCostMW ELSE NULL END) AS PREC, 
				SUM(CASE WHEN Component = 'SCR' THEN AnnNonOHCostMW ELSE NULL END) AS SCR, 
				SUM(CASE WHEN Component = 'SITETRAN' THEN AnnNonOHCostMW ELSE NULL END) AS SITETRAN,
				SUM(CASE WHEN Component = 'STG-CA' THEN AnnNonOHCostMW ELSE NULL END) AS STGCA,
				SUM(CASE WHEN Component = 'STG-GEN' THEN AnnNonOHCostMW ELSE NULL END) AS STGGEN,
				SUM(CASE WHEN Component = 'STG-HPS' THEN AnnNonOHCostMW ELSE NULL END) AS STGHPS,
				SUM(CASE WHEN Component = 'STG-TURB' THEN AnnNonOHCostMW ELSE NULL END) AS STGTURB,
				SUM(CASE WHEN Component = 'STG-VC' THEN AnnNonOHCostMW ELSE NULL END) AS STGVC,
				SUM(CASE WHEN Component = 'WASTEH2O' THEN AnnNonOHCostMW ELSE NULL END) AS WASTEH2O, 
				SUM(CASE WHEN Component = 'WGS' THEN AnnNonOHCostMW ELSE NULL END) AS WGS
                     
                      
FROM         dbo.NonOHMaint
GROUP BY Refnum






GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "NonOHMaint"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'NonOHMaintMWByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'NonOHMaintMWByRefnum';

