﻿

CREATE   VIEW TransAltaCalcs
AS
SELECT c.Refnum, 1000*TotMaint/g.AdjNetMWH AS TotMaintMWH, o.ActCashLessFuel - ISNULL(o.LTSAAdj, 0) - ISNULL(o.OverhaulAdj, 0) AS PlantMngLessOvhl,
TotEffPersxOHAG_MW = (SELECT SUM(TotEffPersMW*((TotEffPers-AGEffPers)/TotEffPers)) FROM Pers
	WHERE PersCat IN (SELECT Pers_LU.PersCat FROM Pers_LU WHERE Pers_LU.CalcCat IS NOT NULL AND Pers_LU.PersCat NOT IN ('OCC-LTSA','OCC-OHAdj','MPS-LTSA','MPS-OHAdj')) AND TotEffPers > 0 
	AND Pers.Refnum = c.Refnum),
EUFxPOF2Yr = n.EUF2Yr-n.POF2Yr
FROM CptlMaintExp c INNER JOIN Gensum g ON g.Refnum = c.Refnum
INNER JOIN OpexCalc o ON o.Refnum = c.Refnum
INNER JOIN NERCFactors n ON n.Refnum = c.Refnum
WHERE c.CptlCode = 'CURR' AND o.DataType = 'MWH'


