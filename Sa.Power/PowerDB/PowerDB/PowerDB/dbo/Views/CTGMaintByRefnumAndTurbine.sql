﻿



CREATE VIEW [dbo].[CTGMaintByRefnumAndTurbine]
AS

	SELECT Refnum,
	  TurbineID,
	  CombInspHrs = SUM(CASE WHEN ProjectID = 'COMBINSP' THEN RunHrs END),
	  HGPInspHrs = SUM(CASE WHEN ProjectID = 'HGPINSP' THEN RunHrs END),
	  OverhaulInspHrs = SUM(CASE WHEN ProjectID = 'OVHL' THEN RunHrs END),
	  CombInspStarts = SUM(CASE WHEN ProjectID = 'COMBINSP' THEN TotStarts END),
	  HGPInspStarts = SUM(CASE WHEN ProjectID = 'HGPINSP' THEN TotStarts END),
	  OverhaulInspStarts = SUM(CASE WHEN ProjectID = 'OVHL' THEN TotStarts END)
  FROM CTGMaint
  GROUP BY Refnum, TurbineID 





