﻿


CREATE  VIEW dbo.SixPackAEP
AS
SELECT     s.Refnum, 
                      BoilerPeakUnavail_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Boiler' THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) 
                      AS real), 
                      TurbinePeakUnavail_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Turbine' THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) 
                      AS real), 
                      CTGPeakUnavail_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Combustion Turbine' THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END)
                       AS real), OtherPeakUnavail_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND 
                      c.SAIMinorEquip NOT IN ('Dry Gas Scrubber (DGS)', 'Wet Gas Scrubber (WGS)') THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) 
                      AS real), ScrubPeakUnavail_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip IN ('Dry Gas Scrubber (DGS)', 
                      'Wet Gas Scrubber (WGS)') THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 
                      ExternalPeakUnavail_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip NOT IN ('Boiler', 'Turbine', 'Combustion Turbine', 'Balance of Plant') 
                      THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 
                      BoilerPeakUnavail_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Boiler' AND e.EVNT_Category IN ('F', 'M') 
                      THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 
                      TurbinePeakUnavail_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Turbine' AND e.EVNT_Category IN ('F', 'M') 
                      THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 
                      CTGPeakUnavail_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Combustion Turbine' AND e.EVNT_Category IN ('F', 'M') 
                      THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 
                      OtherPeakUnavail_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip NOT IN ('Dry Gas Scrubber (DGS)', 
                      'Wet Gas Scrubber (WGS)') AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 
                      ScrubPeakUnavail_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip IN ('Dry Gas Scrubber (DGS)', 
                      'Wet Gas Scrubber (WGS)') AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 
                      ExternalPeakUnavail_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip NOT IN ('Boiler', 'Turbine', 'Combustion Turbine', 'Balance of Plant') AND 
                      e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW / cu.TotPeakMWH * 100 ELSE 0 END) AS real), 

BoilerPeakUnavail_F = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Boiler' AND e.EVNT_Category = 'F' THEN lro.PeakHrs*e.LostMW/cu.TotPeakMWH*100 ELSE 0 END) AS real),
TurbinePeakUnavail_F = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Turbine' AND e.EVNT_Category = 'F'  THEN lro.PeakHrs*e.LostMW/cu.TotPeakMWH*100 ELSE 0 END) AS real),
CTGPeakUnavail_F = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Combustion Turbine' AND e.EVNT_Category = 'F'  THEN lro.PeakHrs*e.LostMW/cu.TotPeakMWH*100 ELSE 0 END) AS real),
OtherPeakUnavail_F = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip NOT IN ('Dry Gas Scrubber (DGS)', 'Wet Gas Scrubber (WGS)') AND e.EVNT_Category = 'F' THEN lro.PeakHrs*e.LostMW/cu.TotPeakMWH*100 ELSE 0 END) AS real),
ScrubPeakUnavail_F = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip IN ('Dry Gas Scrubber (DGS)', 'Wet Gas Scrubber (WGS)') AND e.EVNT_Category = 'F' THEN lro.PeakHrs*e.LostMW/cu.TotPeakMWH*100 ELSE 0 END) AS real),
ExternalPeakUnavail_F = CAST(SUM(CASE WHEN c.SAIMajorEquip NOT IN ('Boiler', 'Turbine', 'Combustion Turbine', 'Balance of Plant') AND e.EVNT_Category = 'F' THEN lro.PeakHrs*e.LostMW/cu.TotPeakMWH*100 ELSE 0 END) AS real),
                

		      BoilerLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Boiler' THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      TurbineLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Turbine' THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      CTGLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Combustion Turbine' THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      OtherLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip NOT IN ('Dry Gas Scrubber (DGS)', 
                      'Wet Gas Scrubber (WGS)') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      ScrubLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip IN ('Dry Gas Scrubber (DGS)', 
                      'Wet Gas Scrubber (WGS)') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      ExternalLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip NOT IN ('Boiler', 'Turbine', 'Combustion Turbine', 'Balance of Plant') 
                      THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), BoilerLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Boiler' AND 
                      e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      TurbineLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Turbine' AND e.EVNT_Category IN ('F', 'M') 
                      THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), CTGLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Combustion Turbine' AND 
                      e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      OtherLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip NOT IN ('Dry Gas Scrubber (DGS)', 
                      'Wet Gas Scrubber (WGS)') AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      ScrubLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip IN ('Dry Gas Scrubber (DGS)', 
                      'Wet Gas Scrubber (WGS)') AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
                      ExternalLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip NOT IN ('Boiler', 'Turbine', 'Combustion Turbine', 'Balance of Plant') AND 
                      e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), TotPeakMWH = MIN(cu.TotPeakMWH), 
                      BoilerMaintKUS = MIN(m.MajorBoiler), TurbineMaintKUS = MIN(m.MajorTurbine), CTGMaintKUS = MIN(m.MajorCTG), 
                      OtherMaintKUS = MIN(m.MajorOther - ISNULL(m.WGS, 0) - ISNULL(m.DGS, 0)), ScrubMaintKUS = MIN(ISNULL(m.WGS, 0) + ISNULL(m.DGS, 0))
FROM         TSort s 
INNER JOIN Events e ON e.Refnum = s.Refnum 
INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = s.PricingHub
INNER JOIN CauseCodes c ON c.Cause_Code = e.Cause_Code 
INNER JOIN CommUnavail cu ON cu.Refnum = s.Refnum 
INNER JOIN MaintEquipSum m ON m.Refnum = s.Refnum
WHERE e.EVNT_Category IN ('F', 'M', 'P') AND e.Cause_Code NOT IN (7777, 0)
GROUP BY s.Refnum



