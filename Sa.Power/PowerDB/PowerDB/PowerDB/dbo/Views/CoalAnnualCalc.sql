﻿CREATE   VIEW CoalAnnualCalc AS
SELECT Refnum, CoalID, 
SUM(HeatValue*Tons)/SUM(Tons) AS HeatValue, 
SUM(LHV*Tons)/SUM(Tons) AS LHV, 
SUM(AshPcnt*Tons)/SUM(Tons*CASE WHEN AshPcnt IS NOT NULL THEN 1 ELSE 0 END) AS AshPcnt, 
SUM(SulfurPcnt*Tons)/SUM(Tons*CASE WHEN SulfurPcnt IS NOT NULL THEN 1 ELSE 0 END) AS SulfurPcnt, 
SUM(MoistPcnt*Tons)/SUM(Tons*CASE WHEN MoistPcnt IS NOT NULL THEN 1 ELSE 0 END) AS MoistPcnt, 
SUM(HardgroveGrind*Tons)/SUM(Tons*CASE WHEN HardgroveGrind IS NOT NULL THEN 1 ELSE 0 END) AS HardgroveGrind, 
SUM(Tons) AS Tons, 
SUM(MineCostTon*Tons)/SUM(Tons) AS MineCostTon, 
SUM(RailMiles*Tons)/SUM(Tons) AS RailMiles, 
SUM(BargeMiles*Tons)/SUM(Tons) AS BargeMiles, 
SUM(HwyMiles*Tons)/SUM(Tons) AS HwyMiles, 
SUM(ConvLength*Tons)/SUM(Tons) AS ConvLength, 
SUM(OnsitePrepCostTon*Tons)/SUM(Tons) AS OnsitePrepCostTon, 
SUM(NSTransMiles*Tons)/SUM(Tons) AS NSTransMiles, 
SUM(TotMiles*Tons)/SUM(Tons) AS TotMiles, 
SUM(TotTonMiles) AS TotTonMiles, 
SUM(TransCostTon*Tons)/SUM(Tons) AS TransCostTon, 
CASE SUM(TotTonMiles)
	WHEN 0 THEN NULL
	ELSE SUM(TransCostTM*TotTonMiles)/SUM(TotTonMiles) 
	END AS TransCostTM, 
SUM(TotCostKUS)*1000/SUM(Tons) AS TotCostTon, 
SUM(TotCostKUS) AS TotCostKUS, 
SUM(MBTU) AS MBTU,
SUM(MBTULHV) AS MBTULHV,
MIN(Biomass) AS Biomass,
MIN(ReportedLHV) AS ReportedLHV
FROM Coal 
GROUP BY Refnum, CoalID
HAVING SUM(Tons) > 0

