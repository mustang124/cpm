﻿




CREATE  VIEW [dbo].[TwoPack]
AS
SELECT s.Refnum, 
	BoilerMaintKUS = MIN(m.MajorBoiler), 
	TurbineMaintKUS = MIN(m.MajorTurbine), 
	CTGMaintKUS = MIN(m.MajorCTG), 
	OtherMaintKUS = MIN(m.MajorOther - ISNULL(m.WGS, 0) - ISNULL(m.DGS, 0)), 
	ScrubMaintKUS = MIN(ISNULL(m.WGS, 0) + ISNULL(m.DGS, 0))
FROM TSort s 
	INNER JOIN Events e ON e.Refnum = s.Refnum 
	--INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = s.PricingHub
	--INNER JOIN CauseCodes c ON c.Cause_Code = e.Cause_Code 
	--INNER JOIN CommUnavail cu ON cu.Refnum = s.Refnum 
	INNER JOIN MaintEquipSum m ON m.Refnum = s.Refnum
WHERE e.EVNT_Category IN ('F', 'M', 'P') AND e.Cause_Code NOT IN (7777, 0)
GROUP BY s.Refnum





