﻿
CREATE VIEW [dbo].[SlideColors]
AS
SELECT 	[SlideColorID], [SlideColorGroup], [ColorGroupName], [Color], [GradientColor], [ColorOrder]
FROM GlobalDB.[dbo].[SlideColors]

