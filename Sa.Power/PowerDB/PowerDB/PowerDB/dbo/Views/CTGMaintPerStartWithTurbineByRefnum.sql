﻿

CREATE VIEW [dbo].[CTGMaintPerStartWithTurbineByRefnum]
AS
--WARNING: this sums the totals by Refnum then applies it to only one Turbine (the MIN)
--do not use if you need the correct number for each Turbine
SELECT Refnum, MIN(TurbineID) as TurbineID, AvgTotStarts = AVG(AvgTotStarts), 
LTSAPerStart = AVG(ISNULL(LTSAAdj, 0))/AVG(AvgTotStarts),
AnnOHPerStart = AVG(ISNULL(AnnOHCost, 0))/AVG(AvgTotStarts),
NonOHPerStart = AVG(ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts),
MaintCostPerStart = AVG(ISNULL(LTSAAdj, 0)+ISNULL(AnnOHCost, 0)+ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts)
FROM CTGMaintCosts
GROUP BY Refnum
HAVING AVG(AvgTotStarts)>0



