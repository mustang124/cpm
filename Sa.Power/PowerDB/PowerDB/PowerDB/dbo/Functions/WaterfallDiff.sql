﻿


CREATE FUNCTION [dbo].[WaterfallDiff]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
--	@CRVGroup VARCHAR(30),
	@ParentList VARCHAR(30),
	@DataType VARCHAR(30), -- should be MWH or MW
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT a.UnitName, 
		a.GroupGroup, 
		'Non-OH Site Labor' = c.[Non-OH Site Labor] - a.[Non-OH Site Labor], 
		'Non-OH Materials' = c.[Non-OH Materials] - a.[Non-OH Materials], 
		'Non-OH Contract Labor' = c.[Non-OH Contract Labor] - a.[Non-OH Contract Labor],
		'OH & SP Annualization' = c.[OH & SP Annualization] - a.[OH & SP Annualization], 
		'Administrative & General' = c.[Administrative & General] - a.[Administrative & General], 
		Chemicals = c.Chemicals - a.Chemicals, 
		Water = c.Water - a.Water,
		'Taxes & Insurance' = c.[Taxes & Insurance] - a.[Taxes & Insurance], 
		'All Other' = c.[All Other] - a.[All Other]

	FROM (

		SELECT UnitName = CASE WHEN @BySite = 0 THEN z.UnitName ELSE CASE WHEN CRVGroupCount = 1 THEN z.SiteName ELSE z.UnitName END END,
			GroupGroup = CASE WHEN @BySite = 0 THEN z.CRVSizeGroup ELSE CASE WHEN CRVGroupCount = 1 THEN z.CRVGroup ELSE z.CRVSizeGroup END END,
			'Non-OH Site Labor' = GlobalDB.dbo.WtAvg([Non-OH Site Labor], AdjNetMWH),
			'Non-OH Materials' = GlobalDB.dbo.WtAvg([Non-OH Materials], AdjNetMWH),
			'Non-OH Contract Labor' = GlobalDB.dbo.WtAvg([Non-OH Contract Labor], AdjNetMWH),
			'OH & SP Annualization' = GlobalDB.dbo.WtAvg([OH & SP Annualization], AdjNetMWH),
			'Administrative & General' = GlobalDB.dbo.WtAvg([Administrative & General], AdjNetMWH),
			Chemicals = GlobalDB.dbo.WtAvg(Chemicals, AdjNetMWH),
			Water = GlobalDB.dbo.WtAvg(Water, AdjNetMWH),
			'Taxes & Insurance' = GlobalDB.dbo.WtAvg([Taxes & Insurance], AdjNetMWH),
			'All Other' = GlobalDB.dbo.WtAvg([All Other], AdjNetMWH)

			FROM (
				SELECT s.sitename, t.UnitName, B.CRVGroup, b.CRVSizeGroup,
					'Non-OH Site Labor' = (STWages + STBenefits + CentralOCCWagesBen + CentralMPSWagesBen),
					'Non-OH Materials' = (MaintMatl + ContMaintMatl),
					'Non-OH Contract Labor' = ContMaintLabor,
					'OH & SP Annualization' = (OverhaulAdj + LTSAAdj),
					'Administrative & General' = (AGPers + AGNonPers),
					Chemicals = Chemicals,
					Water = Water,
					'Taxes & Insurance' = (PropTax + OthTax + Insurance),
					'All Other' = (ContMaintLumpSum + OthContSvc + Envir + OthFixed + OthVar),
					gtc.AdjNetMWH,
					d.CRVGroupCount
				FROM TSort t
					LEFT JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StudySites s ON s.SiteID = t.SiteID
					LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
					LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					LEFT JOIN (SELECT s.SiteName, CRVGroupCount = COUNT(distinct b.CRVGroup) FROM TSort t LEFT JOIN Breaks b ON b.Refnum = t.Refnum LEFT JOIN StudySites s ON s.SiteID = t.SiteID
						WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear GROUP BY s.SiteName ) d ON d.SiteName = s.SiteName

				WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) 
					AND t.StudyYear = @StudyYear
			) z GROUP BY CASE WHEN @BySite = 0 THEN z.UnitName ELSE CASE WHEN CRVGroupCount = 1 THEN z.SiteName ELSE z.UnitName END END,
				CASE WHEN @BySite = 0 THEN z.CRVSizeGroup ELSE CASE WHEN CRVGroupCount = 1 THEN z.CRVGroup ELSE z.CRVSizeGroup END END
		) a



		--SELECT UnitName = RTRIM(CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end), 
		--	GroupGroup = CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END,
		--	'Non-OH Site Labor' = GlobalDB.dbo.WtAvg((STWages + STBenefits + CentralOCCWagesBen + CentralMPSWagesBen), gtc.AdjNetMWH),
		--	'Non-OH Materials' = GlobalDB.dbo.WtAvg((MaintMatl + ContMaintMatl), gtc.AdjNetMWH),
		--	'Non-OH Contract Labor' = GlobalDB.dbo.WtAvg(ContMaintLabor, gtc.AdjNetMWH),
		--	'OH & SP Annualization' = GlobalDB.dbo.WtAvg((OverhaulAdj + LTSAAdj), gtc.AdjNetMWH),
		--	'Administrative & General' = GlobalDB.dbo.WtAvg((AGPers + AGNonPers), gtc.AdjNetMWH),
		--	Chemicals = GlobalDB.dbo.WtAvg(Chemicals, gtc.AdjNetMWH),
		--	Water = GlobalDB.dbo.WtAvg(Water, gtc.AdjNetMWH),
		--	'Taxes & Insurance' = GlobalDB.dbo.WtAvg((PropTax + OthTax + Insurance), gtc.AdjNetMWH),
		--	'All Other' = GlobalDB.dbo.WtAvg((ContMaintLumpSum + OthContSvc + Envir + OthFixed + OthVar), gtc.AdjNetMWH)
		--	--Total = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		--FROM TSort t
		--	LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		--	LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		--	LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
		--	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		--WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) 
		--	AND t.StudyYear = @StudyYear
		--GROUP BY CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end, CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END
		--) a

	LEFT JOIN (


			SELECT GroupGroup = b.CRVGroup,
			'Non-OH Site Labor' = GlobalDB.dbo.WtAvg((STWages + STBenefits + CentralOCCWagesBen + CentralMPSWagesBen), gtc.AdjNetMWH),
			'Non-OH Materials' = GlobalDB.dbo.WtAvg((MaintMatl + ContMaintMatl), gtc.AdjNetMWH),
			'Non-OH Contract Labor' = GlobalDB.dbo.WtAvg(ContMaintLabor, gtc.AdjNetMWH),
			'OH & SP Annualization' = GlobalDB.dbo.WtAvg((OverhaulAdj + LTSAAdj), gtc.AdjNetMWH),
			'Administrative & General' = GlobalDB.dbo.WtAvg((AGPers + AGNonPers), gtc.AdjNetMWH),
			Chemicals = GlobalDB.dbo.WtAvg(Chemicals, gtc.AdjNetMWH),
			Water = GlobalDB.dbo.WtAvg(Water, gtc.AdjNetMWH),
			'Taxes & Insurance' = GlobalDB.dbo.WtAvg((PropTax + OthTax + Insurance), gtc.AdjNetMWH),
			'All Other' = GlobalDB.dbo.WtAvg((ContMaintLumpSum + OthContSvc + Envir + OthFixed + OthVar), gtc.AdjNetMWH)
			--Total = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ParentList) 
			AND t.StudyYear = @StudyYear
		GROUP BY b.CRVGroup

		UNION

		SELECT GroupGroup = b.CRVSizeGroup,
			'Non-OH Site Labor' = GlobalDB.dbo.WtAvg((STWages + STBenefits + CentralOCCWagesBen + CentralMPSWagesBen), gtc.AdjNetMWH),
			'Non-OH Materials' = GlobalDB.dbo.WtAvg((MaintMatl + ContMaintMatl), gtc.AdjNetMWH),
			'Non-OH Contract Labor' = GlobalDB.dbo.WtAvg(ContMaintLabor, gtc.AdjNetMWH),
			'OH & SP Annualization' = GlobalDB.dbo.WtAvg((OverhaulAdj + LTSAAdj), gtc.AdjNetMWH),
			'Administrative & General' = GlobalDB.dbo.WtAvg((AGPers + AGNonPers), gtc.AdjNetMWH),
			Chemicals = GlobalDB.dbo.WtAvg(Chemicals, gtc.AdjNetMWH),
			Water = GlobalDB.dbo.WtAvg(Water, gtc.AdjNetMWH),
			'Taxes & Insurance' = GlobalDB.dbo.WtAvg((PropTax + OthTax + Insurance), gtc.AdjNetMWH),
			'All Other' = GlobalDB.dbo.WtAvg((ContMaintLumpSum + OthContSvc + Envir + OthFixed + OthVar), gtc.AdjNetMWH)
			--Total = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ParentList) 
			AND t.StudyYear = @StudyYear
		GROUP BY b.CRVSizeGroup






		--SELECT GroupGroup = CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END,
		--	'Non-OH Site Labor' = GlobalDB.dbo.WtAvg((STWages + STBenefits + CentralOCCWagesBen + CentralMPSWagesBen), gtc.AdjNetMWH),
		--	'Non-OH Materials' = GlobalDB.dbo.WtAvg((MaintMatl + ContMaintMatl), gtc.AdjNetMWH),
		--	'Non-OH Contract Labor' = GlobalDB.dbo.WtAvg(ContMaintLabor, gtc.AdjNetMWH),
		--	'OH & SP Annualization' = GlobalDB.dbo.WtAvg((OverhaulAdj + LTSAAdj), gtc.AdjNetMWH),
		--	'Administrative & General' = GlobalDB.dbo.WtAvg((AGPers + AGNonPers), gtc.AdjNetMWH),
		--	Chemicals = GlobalDB.dbo.WtAvg(Chemicals, gtc.AdjNetMWH),
		--	Water = GlobalDB.dbo.WtAvg(Water, gtc.AdjNetMWH),
		--	'Taxes & Insurance' = GlobalDB.dbo.WtAvg((PropTax + OthTax + Insurance), gtc.AdjNetMWH),
		--	'All Other' = GlobalDB.dbo.WtAvg((ContMaintLumpSum + OthContSvc + Envir + OthFixed + OthVar), gtc.AdjNetMWH)
		--	--Total = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		--FROM TSort t
		--	LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		--	LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		--	LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
		--	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		--WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ParentList) 
		--	AND t.StudyYear = @StudyYear
		--GROUP BY CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END

	) c ON c.GroupGroup = a.GroupGroup



)

