﻿


CREATE FUNCTION [dbo].[NuonTrend]
(	
	@Version VARCHAR(10)

)
RETURNS TABLE 
AS
RETURN 
(

--this was done for some specific slides for Mark, and should not be used for anything else

	select top 1000 * from (
	
	select Unit = case coloc 
		when 'NUON – Diemen (D)' then 'Diemen 33'
		when 'NUON – DM33 (D)' then 'Diemen 33'
		when 'NUON – Hemweg 09 (H9)' then 'Hemweg 09'
		when 'NUON – HW08 (H8)' then 'Hemweg 08'
		when 'NUON – IJMOND01 (Y)' then 'Ijmond 01'
		when 'NUON – V24 (V24)' then 'Velsen 24'
		when 'NUON – VN25 (V25)' then 'Velsen 25'
		when 'Vattenfall – Mitte (MIT)' then 'Mitte'
		when 'Vattenfall – Moabit (M)' then 'Moabit'
		when 'Vattenfall – Reuter West (RW)' then 'Reuter West'
		when 'Vattenfall – Reuter West D (RWD)' then 'Reuter West D'
		when 'Vattenfall – Reuter West E (RWE)' then 'Reuter West E'
		when 'Vattenfall – Tiefstack Coal (TFC)' then 'Tiefstack Coal'
		when 'Vattenfall – Tiefstack GuD (TGuD)' then 'Tiefstack GuD'
	else coloc end, 
	--Label = case coloc 
	--	when 'NUON – Diemen (D)' then 'Diemen 33'
	--	when 'NUON – DM33 (D)' then 'Diemen 33'
	--	when 'NUON – Hemweg 09 (H9)' then 'Hemweg 09'
	--	when 'NUON – HW08 (H8)' then 'Hemweg 08'
	--	when 'NUON – IJMOND01 (Y)' then 'Ijmond 01'
	--	when 'NUON – V24 (V24)' then 'Velsen 24'
	--	when 'NUON – VN25 (V25)' then 'Velsen 25'
	--	when 'Vattenfall – Mitte (MIT)' then 'Mitte'
	--	when 'Vattenfall – Moabit (M)' then 'Moabit'
	--	when 'Vattenfall – Reuter West (RW)' then 'Reuter West'
	--	when 'Vattenfall – Reuter West D (RWD)' then 'Reuter West D'
	--	when 'Vattenfall – Reuter West E (RWE)' then 'Reuter West E'
	--	when 'Vattenfall – Tiefstack Coal (TFC)' then 'Tiefstack Coal'
	--	when 'Vattenfall – Tiefstack GuD (TGuD)' then 'Tiefstack GuD'
	--else coloc end, 
	Label = 'Non-OH',
	--case @Version
	--	when 'MWH' THEN 'Non-OH'
	--	when 'MW' THEN mtc.AnnMaintCostMW * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
	--	WHEN 'OpHrs' THEN mtc.AnnMaintCostOpHrs * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
	--	WHEN 'EGC' THEN mtc.AnnMaintCostEGC * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
	--	end
	cat = t.StudyYear, 
	value = case @Version
		when 'MWH' THEN mtc.AnnNonOHCostMWH * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		when 'MW' THEN mtc.AnnNonOHCostMW * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		WHEN 'OpHrs' THEN mtc.AnnNonOHCostOpHrs * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		WHEN 'EGC' THEN mtc.AnnNonOHCostEGC-- * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		end
	from TSort t
	left join MaintTotCalc mtc on mtc.Refnum = t.Refnum
	where CompanyID in ('Nuon','Vattenfall')
	and t.Refnum like '%[0-9]'
	and t.StudyYear >= 2007
	and UnitID in ('935CC','0AH119P','0AH130P','0AH122P','0AH123P','934CC','VGE100P','VGE106P','VGE120P','VGE130P','VGE4CC','VGE650P','VGE3CC','VGE101P','VGE107P')

	union

		select Unit = case coloc 
		when 'NUON – Diemen (D)' then 'Diemen 33'
		when 'NUON – DM33 (D)' then 'Diemen 33'
		when 'NUON – Hemweg 09 (H9)' then 'Hemweg 09'
		when 'NUON – HW08 (H8)' then 'Hemweg 08'
		when 'NUON – IJMOND01 (Y)' then 'Ijmond 01'
		when 'NUON – V24 (V24)' then 'Velsen 24'
		when 'NUON – VN25 (V25)' then 'Velsen 25'
		when 'Vattenfall – Mitte (MIT)' then 'Mitte'
		when 'Vattenfall – Moabit (M)' then 'Moabit'
		when 'Vattenfall – Reuter West (RW)' then 'Reuter West'
		when 'Vattenfall – Reuter West D (RWD)' then 'Reuter West D'
		when 'Vattenfall – Reuter West E (RWE)' then 'Reuter West E'
		when 'Vattenfall – Tiefstack Coal (TFC)' then 'Tiefstack Coal'
		when 'Vattenfall – Tiefstack GuD (TGuD)' then 'Tiefstack GuD'
	else coloc end, 
	--Label = case coloc 
	--	when 'NUON – Diemen (D)' then 'Diemen 33'
	--	when 'NUON – DM33 (D)' then 'Diemen 33'
	--	when 'NUON – Hemweg 09 (H9)' then 'Hemweg 09'
	--	when 'NUON – HW08 (H8)' then 'Hemweg 08'
	--	when 'NUON – IJMOND01 (Y)' then 'Ijmond 01'
	--	when 'NUON – V24 (V24)' then 'Velsen 24'
	--	when 'NUON – VN25 (V25)' then 'Velsen 25'
	--	when 'Vattenfall – Mitte (MIT)' then 'Mitte'
	--	when 'Vattenfall – Moabit (M)' then 'Moabit'
	--	when 'Vattenfall – Reuter West (RW)' then 'Reuter West'
	--	when 'Vattenfall – Reuter West D (RWD)' then 'Reuter West D'
	--	when 'Vattenfall – Reuter West E (RWE)' then 'Reuter West E'
	--	when 'Vattenfall – Tiefstack Coal (TFC)' then 'Tiefstack Coal'
	--	when 'Vattenfall – Tiefstack GuD (TGuD)' then 'Tiefstack GuD'
	--else coloc end, 
	Label = 'OH',
	--case @Version
	--	when 'MWH' THEN 'Non-OH'
	--	when 'MW' THEN mtc.AnnMaintCostMW * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
	--	WHEN 'OpHrs' THEN mtc.AnnMaintCostOpHrs * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
	--	WHEN 'EGC' THEN mtc.AnnMaintCostEGC * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
	--	end
	cat = t.StudyYear, 
	value = case @Version
		when 'MWH' THEN mtc.AnnOHCostMWH * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		when 'MW' THEN mtc.AnnOHCostMW * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		WHEN 'OpHrs' THEN mtc.AnnOHCostOpHrs * GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		WHEN 'EGC' THEN mtc.AnnOHCostEGC --* GlobalDB.dbo.CurrencyPerUSD('EUR', t.StudyYear)
		end
	from TSort t
	left join MaintTotCalc mtc on mtc.Refnum = t.Refnum
	where CompanyID in ('Nuon','Vattenfall')
	and t.Refnum like '%[0-9]'
	and t.StudyYear >= 2007
	and UnitID in ('935CC','0AH119P','0AH130P','0AH122P','0AH123P','934CC','VGE100P','VGE106P','VGE120P','VGE130P','VGE4CC','VGE650P','VGE3CC','VGE101P','VGE107P')


	) b


	order by Unit, Label, cat
)

