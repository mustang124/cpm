﻿




CREATE FUNCTION [dbo].[TrafficLighttest]
(	
	@ListName VARCHAR(30),
	@StudyYear VARCHAR(30),
	@CRVGroup VARCHAR(30),
	@RankVariableGroup INT,
	@ParentList VARCHAR(30),
	@BreakCondition VARCHAR(30)

)

RETURNS @T TABLE (UnitLabel VARCHAR(100), Col1 VARCHAR(200), Col2 VARCHAR(100), Col3 VARCHAR(100), Col4 VARCHAR(100), Col5 VARCHAR(100), Col6 VARCHAR(100))
AS
BEGIN


	declare @table TrafficLightTable
	insert @table
	select * from TestPerfSumReplace(@ListName,@StudyYear,@CRVGroup,@RankVariableGroup,@ParentList,@BreakCondition)

	insert @T
	select * from MakeTrafficLight(@table,'1') order by UnitLabel ASC, Col1 DESC



--DECLARE @RankVariable1 INT
--DECLARE @RankVariable2 INT
--DECLARE @RankVariable3 INT
--SET @RankVariable1 = case @RankVariableGroup when 1 then 46 else 0 end
--SET @RankVariable2 = case @RankVariableGroup when 1 then 47 else 0 end
--SET @RankVariable3 = case @RankVariableGroup when 1 then 49 else 0 end

--INSERT INTO @T (Unitlabel, Col1, Col2, Col3, Col4, Col5, Col6)
--select UnitLabel = '',
--	col1 = '' + CASE @RankVariable1
--					WHEN 1 THEN 'HeatRate'
--					WHEN 6 THEN 'AnnMaintCostMWH'
--					WHEN 7 THEN 'AnnOHCostMWH'
--					WHEN 8 THEN 'AnnNonOHCostMWH'
--					WHEN 46 THEN 'Unplanned Commercial Unavailability, %'
--					WHEN 47 THEN 'Unplanned Commercial Unavailability Index'
--					WHEN 49 THEN 'Planned Commercial Unavailability Index'
--					WHEN 55 THEN 'InternalLessScrubberPcnt'
--					WHEN 78 THEN 'ThermEff'
--					WHEN 81 THEN 'AnnLTSACostMWH'
--					WHEN 86 THEN 'TotCashLessFuelEm'
--					WHEN 88 THEN 'TotCashLessFuelEm'
--					ELSE '' END,
--						col2 = CASE @RankVariable2
--					WHEN 1 THEN 'HeatRate'
--					WHEN 6 THEN 'AnnMaintCostMWH'
--					WHEN 7 THEN 'AnnOHCostMWH'
--					WHEN 8 THEN 'AnnNonOHCostMWH'
--					WHEN 46 THEN 'Unplanned Commercial Unavailability, %'
--					WHEN 47 THEN 'Unplanned Commercial Unavailability Index'
--					WHEN 49 THEN 'Planned Commercial Unavailability Index'
--					WHEN 55 THEN 'InternalLessScrubberPcnt'
--					WHEN 78 THEN 'ThermEff'
--					WHEN 81 THEN 'AnnLTSACostMWH'
--					WHEN 86 THEN 'TotCashLessFuelEm'
--					WHEN 88 THEN 'TotCashLessFuelEm'
--					ELSE '' END,
--						col3 = CASE @RankVariable3
--					WHEN 1 THEN 'HeatRate'
--					WHEN 6 THEN 'AnnMaintCostMWH'
--					WHEN 7 THEN 'AnnOHCostMWH'
--					WHEN 8 THEN 'AnnNonOHCostMWH'
--					WHEN 46 THEN 'Unplanned Commercial Unavailability, %'
--					WHEN 47 THEN 'Unplanned Commercial Unavailability Index'
--					WHEN 49 THEN 'Planned Commercial Unavailability Index'
--					WHEN 55 THEN 'InternalLessScrubberPcnt'
--					WHEN 78 THEN 'ThermEff'
--					WHEN 81 THEN 'AnnLTSACostMWH'
--					WHEN 86 THEN 'TotCashLessFuelEm'
--					WHEN 88 THEN 'TotCashLessFuelEm'
--					ELSE '' END,
--					col4 = null,
--					col5 = null,
--					col6 = null

--union

--select t.UnitLabel, CAST(rv1.Percentile AS varchar), CAST(rv2.Percentile AS varchar), CAST(rv3.Percentile as varchar), null, null, null
--from TSort t
--left join _RankView rv1 on rv1.RankVariableID = @RankVariable1 and rv1.Refnum = t.Refnum and rv1.ListName = @ParentList and rv1.BreakCondition = @BreakCondition
--left join _RankView rv2 on rv2.RankVariableID = @RankVariable2 and rv2.Refnum = t.Refnum and rv2.ListName = @ParentList and rv2.BreakCondition = @BreakCondition
--left join _RankView rv3 on rv3.RankVariableID = @RankVariable3 and rv3.Refnum = t.Refnum and rv3.ListName = @ParentList and rv3.BreakCondition = @BreakCondition
--where t.Refnum in (select Refnum from _RL where ListName = @ListName)
--and t.StudyYear = @StudyYear
--and rv1.BreakValue = @CRVGroup

RETURN


END

