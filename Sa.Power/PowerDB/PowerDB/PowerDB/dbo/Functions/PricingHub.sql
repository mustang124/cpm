﻿

CREATE FUNCTION [dbo].[PricingHub]
(	

	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)
	--need to add the hour info here? different depending on location

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 100000 
		PricingHub, 
		[Date], 
		'Peak Price' = ISNULL(PeakPrice, OffPeakPrice) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),
		'Off-Peak Price' = ISNULL(OffPeakPrice, PeakPrice)  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
	FROM (
		--select [Date] = cast(cast(convert(datetime, cast(StartTime as date)) as int) as int),
		select PricingHub,
			[Date] = cast(StartTime as date),
			PeakPrice = AVG(CASE WHEN DATEPART(HOUR, StartTime) BETWEEN 6 AND 21 THEN Price END), 
			OffPeakPrice = AVG(CASE WHEN DATEPART(HOUR, StartTime) NOT BETWEEN 6 AND 21 THEN Price END)
		FROM Prices 
		WHERE PricingHub IN (SELECT DISTINCT PricingHub FROM TSort WHERE Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND StudyYear = @StudyYear) AND YEAR(StartTime) = @StudyYear
		group by PricingHub, cast(StartTime as date)
		) b
		order by PricingHub, [Date]

	
)


