﻿



CREATE FUNCTION [dbo].[PlantAction]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@DataType VARCHAR(3),
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	--cleaner way to do the group and pacesetter?
	SELECT UnitLabel = Label,
		'Site Wages & Benefits' = SUM(SiteWagesBen) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Contract & Centralized Employees' = SUM(Central) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Materials' = SUM(MaintMatl) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Overhauls & Projects' = SUM(OHAdj) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'LTSA/CSA' = SUM(LTSAAdj) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Other Variable' = SUM(Other) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Miscellaneous' = SUM(Misc) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END

		FROM (
		SELECT Label = 'Group Avg', SiteWagesBen = Value, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'SiteWagesBen'
		UNION
		SELECT Label = 'Group Avg', SiteWagesBen = NULL, Central = Value, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Central'
		UNION
		SELECT Label = 'Group Avg', SiteWagesBen = NULL, Central = NULL, MaintMatl = Value, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'MaintMatl'
		UNION
		SELECT Label = 'Group Avg', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = Value, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'OHAdj'
		UNION
		SELECT Label = 'Group Avg', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = Value, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'LTSAAdj'
		UNION
		SELECT Label = 'Group Avg', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = Value, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Other'
		UNION
		SELECT Label = 'Group Avg', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Misc') b
	GROUP BY Label


	UNION ALL


		SELECT UnitLabel = Label,
		'Site Wages & Benefits' = SUM(SiteWagesBen) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Contract & Centralized Employees' = SUM(Central) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Materials' = SUM(MaintMatl) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Overhauls & Projects' = SUM(OHAdj) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'LTSA/CSA' = SUM(LTSAAdj) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Other Variable' = SUM(Other) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Miscellaneous' = SUM(Misc) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END

		FROM (
		SELECT Label = 'Pace- setter', SiteWagesBen = Value, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'SiteWagesBen_PS'
		UNION
		SELECT Label = 'Pace- setter', SiteWagesBen = NULL, Central = Value, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Central_PS'
		UNION
		SELECT Label = 'Pace- setter', SiteWagesBen = NULL, Central = NULL, MaintMatl = Value, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'MaintMatl_PS'
		UNION
		SELECT Label = 'Pace- setter', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = Value, LTSAAdj = NULL, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'OHAdj_PS'
		UNION
		SELECT Label = 'Pace- setter', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = Value, Other = NULL, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'LTSAAdj_PS'
		UNION
		SELECT Label = 'Pace- setter', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = Value, Misc = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Other_PS'
		UNION
		SELECT Label = 'Pace- setter', SiteWagesBen = NULL, Central = NULL, MaintMatl = NULL, OHAdj = NULL, LTSAAdj = NULL, Other = NULL, Misc = Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PlantAction' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Misc_PS') b
	GROUP BY Label


	UNION ALL

	SELECT UnitLabel, [Site Wages & Benefits], [Contract & Centralized Employees], [Materials], [Overhauls & Projects], [LTSA/CSA], [Other Variable], [Miscellaneous] FROM (
		SELECT UnitLabel = 'Co Avg',
			'Site Wages & Benefits' = GlobalDB.dbo.WtAvg(o.SiteWagesBen, CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'Contract & Centralized Employees' = GlobalDB.dbo.WtAvg(o.CentralContract, CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'Materials' = GlobalDB.dbo.WtAvg(o.MaintMatl, CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'Overhauls & Projects' = GlobalDB.dbo.WtAvg(o.OverhaulAdj, CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'LTSA/CSA' = GlobalDB.dbo.WtAvg(o.LTSAAdj, CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'Other Variable' = GlobalDB.dbo.WtAvg(o.OthVar, CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'Miscellaneous' = GlobalDB.dbo.WtAvg(o.Misc, CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			COUNT(*) AS cnt
		FROM TSort t
			LEFT JOIN PlantActionExp o ON o.Refnum = t.Refnum and o.DataType = @DataType
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN GenSum g ON g.Refnum = t.Refnum
		WHERE t.StudyYear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) AND t.Refnum NOT LIKE '%G'
			AND (
				(@CRVGroup = 'C1' AND b.CoalNMCGroup2 = 1) OR 
				(@CRVGroup = 'C2' AND b.CoalNMCGroup2 = 2) OR 
				(@CRVGroup = 'C3' AND b.CoalNMCGroup2 = 3) OR 
				(@CRVGroup = 'GAS' AND b.SteamGasOil = 'Y') OR
				(b.CRVSizeGroup = @CRVGroup AND b.CombinedCycle = 'Y') OR
				(@CRVGroup = 'ALL') --returns all
				)
	) b
	WHERE cnt > 1

	UNION ALL

	SELECT UnitLabel, [Site Wages & Benefits], [Contract & Centralized Employees],[Materials],[Overhauls & Projects],[LTSA/CSA],[Other Variable],[Miscellaneous]
	FROM PowerGlobal.dbo.Numbers

	LEFT JOIN (
	SELECT TOP 1000 UnitLabel = RTRIM(UnitLabel), --TOP 1000 here so that the ORDER BY will work in a TVF
		'Site Wages & Benefits' = o.SiteWagesBen * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Contract & Centralized Employees' = o.CentralContract * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Materials' = o.MaintMatl * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Overhauls & Projects' = o.OverhaulAdj * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'LTSA/CSA' = o.LTSAAdj * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Other Variable' = o.OthVar * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Miscellaneous' = o.Misc * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		Rank = RANK() OVER (ORDER BY CASE WHEN t.Refnum LIKE '%G' THEN 1 ELSE 0 END, o.ActCashLessFuel DESC)
	FROM TSort t
		LEFT JOIN PlantActionExp o ON o.Refnum = t.Refnum and o.DataType = @DataType
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenSum g ON g.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND (
			((@CRVGroup = 'C1' AND b.CoalNMCGroup2 = 1) OR 
			(@CRVGroup = 'C2' AND b.CoalNMCGroup2 = 2) OR 
			(@CRVGroup = 'C3' AND b.CoalNMCGroup2 = 3) OR 
			(@CRVGroup = 'GAS' AND b.SteamGasOil = 'Y') OR
			(b.CRVSizeGroup = @CRVGroup AND b.CombinedCycle = 'Y') OR
			(@CRVGroup = 'ALL') --returns all
			)
			)--OR t.Refnum LIKE '%G')
	--ORDER BY o.ActCashLessFuel DESC
	) c ON Num = Rank
	WHERE Num <= 9 OR Rank IS NOT NULL


)
