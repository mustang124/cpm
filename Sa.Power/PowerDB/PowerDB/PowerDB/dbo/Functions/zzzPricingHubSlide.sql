﻿
CREATE FUNCTION [dbo].[zzzPricingHubSlide]
(	

	@ListName VARCHAR(30),
	@StudyYear INT
	--need to add the hour info here? different depending on location

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 100000 --PricingHub, 
		[Date], PeakPrice = ISNULL(PeakPrice, OffPeakPrice), OffPeakPrice = ISNULL(OffPeakPrice, PeakPrice) FROM (
		--select [Date] = cast(cast(convert(datetime, cast(StartTime as date)) as int) as int),
		select --PricingHub,
			[Date] = cast(StartTime as date),
			PeakPrice = AVG(CASE WHEN DATEPART(HOUR, StartTime) BETWEEN 6 AND 21 THEN Price END), 
			OffPeakPrice = AVG(CASE WHEN DATEPART(HOUR, StartTime) NOT BETWEEN 6 AND 21 THEN Price END)
		FROM Prices 
		WHERE PricingHub IN (SELECT DISTINCT PricingHub FROM TSort WHERE Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)) AND YEAR(StartTime) = @StudyYear
		group by --PricingHub, 
		cast(StartTime as date)
		) b
		order by --PricingHub, 
		[Date]
)

