﻿


CREATE FUNCTION [dbo].[HeatRate]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@Metric BIT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 UnitLabel = rtrim(t.UnitLabel), HeatRate = CASE WHEN @Metric = 1 THEN GlobalDB.dbo.UnitsConv(o.HeatRate,'BTU','MJ') ELSE o.HeatRate END
	FROM TSort t 
		INNER JOIN GenerationTotCalc o ON o.Refnum = t.Refnum 
	WHERE t.studyyear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
	ORDER BY o.HeatRate DESC



)



