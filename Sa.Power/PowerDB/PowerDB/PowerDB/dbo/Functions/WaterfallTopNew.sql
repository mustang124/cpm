﻿



CREATE FUNCTION [dbo].[WaterfallTopNew]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10),
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(


	SELECT 
	DISTINCT Title = CASE WHEN @BySite = 0 THEN a.UnitName ELSE CASE WHEN CRVGroupCount = 1 THEN a.SiteName ELSE a.UnitName END END, 
		Title2 = CASE WHEN @ParentList LIKE '%Pacesetter%' THEN 'Pacesetter' ELSE 'Peer Group' END,
		BeginCell = CASE WHEN @BySite = 0 THEN c.BeginCell ELSE CASE WHEN CRVGroupCount = 1 THEN b.BeginCell ELSE c.BeginCell END END

	FROM (
		--this part just gets the possible site/unit name and crv and size groups to join to
		SELECT s.SiteName, t.UnitName, b.CRVGroup, b.CRVSizeGroup
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		) a

	LEFT JOIN (
		--this part gets the values for the different CRV Groups, which will be chosen from later
		SELECT s.sitename,
			BeginCell = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		FROM TSort t
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		GROUP BY s.SiteName
		) b ON b.SiteName = a.SiteName

	LEFT JOIN (
		--this part gets the values for the different CRV Size Groups, which will be chosen from later
		SELECT t.UnitName, 
			BeginCell = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		FROM TSort t
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		GROUP BY t.UnitName
		) c ON c.UnitName = a.UnitName

	LEFT JOIN (
		--this part counts the number of CRV Groups for that sitename - in the query at the top we use this to decide whether to use CRV Group or CRV Size Group (if the count = 1 we use CRV Group)
		SELECT s.SiteName, CRVGroupCount = COUNT(distinct b.CRVGroup)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		GROUP BY s.SiteName
		) d ON d.SiteName = a.SiteName







	--old version replaced 8/31/16 SW - see WaterfallBotNew for details on why

	--SELECT Title = RTRIM(CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end), 
	--	--Title2 = RTRIM(CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end), 
	--	[BeginCell] = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
	--	--Gap1 = null, 
	--	----Label = null, 
	--	--Down = null, 
	--	--Up = null, 
	--	--[EndCell] = null, 
	--	--sort = 1
	--FROM TSort t
	--	LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--	LEFT JOIN StudySites s on s.SiteID = t.SiteID
	--	LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
	--	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	--WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
	--GROUP BY CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end, CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END

)




