﻿

CREATE FUNCTION [dbo].[zzzPersBarCoal]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CoalNMCGroup VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(
	SELECT UnitLabel = 'Group Avg',
		'Site' = GlobalDB.dbo.WtAvg(o.SiteEffPers*100/nf.NMC, nf.NMC),
		'Centralized' = GlobalDB.dbo.WtAvg(o.CentralEffPers*100/nf.NMC, nf.NMC),
		'Non-Overhaul Contract' = GlobalDB.dbo.WtAvg((isnull(o.ContractEffPers,0) - isnull(p.ContractEffPers,0))*100/nf.NMC,nf.NMC),
		'Overhaul Contract' = GlobalDB.dbo.WtAvg(isnull(p.ContractEffPers,0)*100/nf.NMC,nf.NMC),
		'A&G' = GlobalDB.dbo.WtAvg(o.AGEffPers*100/nf.NMC,nf.NMC)
	FROM TSort t
		LEFT JOIN PersSTCalc o ON o.Refnum = t.Refnum and o.SectionID = 'TP'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, ContractEffPers = SUM(ContractEffPers) FROM Pers WHERE PersCat IN ('OCC-OHAdj', 'MPS-OHAdj','OCC-LTSA','MPS-LTSA') GROUP BY Refnum) p ON p.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

		SELECT UnitLabel = 'Pace- setter',
		'Site' = GlobalDB.dbo.WtAvg(o.SiteEffPers*100/nf.NMC, nf.NMC),
		'Centralized' = GlobalDB.dbo.WtAvg(o.CentralEffPers*100/nf.NMC, nf.NMC),
		'Non-Overhaul Contract' = GlobalDB.dbo.WtAvg((isnull(o.ContractEffPers,0) - isnull(p.ContractEffPers,0))*100/nf.NMC,nf.NMC),
		'Overhaul Contract' = GlobalDB.dbo.WtAvg(isnull(p.ContractEffPers,0)*100/nf.NMC,nf.NMC),
		'A&G' = GlobalDB.dbo.WtAvg(o.AGEffPers*100/nf.NMC,nf.NMC)
	FROM TSort t
		LEFT JOIN PersSTCalc o ON o.Refnum = t.Refnum and o.SectionID = 'TP'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, ContractEffPers = SUM(ContractEffPers) FROM Pers WHERE PersCat IN ('OCC-OHAdj', 'MPS-OHAdj','OCC-LTSA','MPS-LTSA') GROUP BY Refnum) p ON p.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13 Pacesetters 2')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT UnitLabel = 'Co Avg',
		'Site' = GlobalDB.dbo.WtAvg(o.SiteEffPers*100/nf.NMC, nf.NMC),
		'Centralized' = GlobalDB.dbo.WtAvg(o.CentralEffPers*100/nf.NMC, nf.NMC),
		'Non-Overhaul Contract' = GlobalDB.dbo.WtAvg((isnull(o.ContractEffPers,0) - isnull(p.ContractEffPers,0))*100/nf.NMC,nf.NMC),
		'Overhaul Contract' = GlobalDB.dbo.WtAvg(isnull(p.ContractEffPers,0)*100/nf.NMC,nf.NMC),
		'A&G' = GlobalDB.dbo.WtAvg(o.AGEffPers*100/nf.NMC,nf.NMC)
	FROM TSort t
		LEFT JOIN PersSTCalc o ON o.Refnum = t.Refnum and o.SectionID = 'TP'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, ContractEffPers = SUM(ContractEffPers) FROM Pers WHERE PersCat IN ('OCC-OHAdj', 'MPS-OHAdj','OCC-LTSA','MPS-LTSA') GROUP BY Refnum) p ON p.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT TOP 1000 UnitLabel, --TOP 1000 here so that the ORDER BY will work in a TVF
		Site = o.SiteEffPers*100/nf.NMC,
		Central = o.CentralEffPers*100/nf.NMC,
		ContractNonOH =  (isnull(o.ContractEffPers,0) - isnull(p.ContractEffPers,0))*100/nf.NMC,
		ContractOH = isnull(p.ContractEffPers,0)*100/nf.NMC,
		AG =  o.AGEffPers*100/nf.NMC
		--, g.NMC
	FROM TSort t
		LEFT JOIN PersSTCalc o ON o.Refnum = t.Refnum and o.SectionID = 'TP'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, ContractEffPers = SUM(ContractEffPers) FROM Pers WHERE PersCat IN ('OCC-OHAdj', 'MPS-OHAdj','OCC-LTSA','MPS-LTSA') GROUP BY Refnum) p ON p.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup
	ORDER BY o.TotEffPers/nf.NMC DESC

)


