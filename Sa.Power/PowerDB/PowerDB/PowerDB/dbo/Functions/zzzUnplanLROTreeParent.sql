﻿

CREATE FUNCTION [dbo].[zzzUnplanLROTreeParent]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30), --"", "Tech", "Coal", "SGO", "Elec","Cogen"
	@Major VARCHAR(30), --blank or anything, must match value from UnplanLROTreeSource or you will get 0
	@Minor VARCHAR(30) --blank or anything, must match value from UnplanLROTreeSource or you will get 0
)
RETURNS TABLE 
AS
RETURN 
(

	--compares a record to its top level parent, returns percentages

	SELECT
		Name = 
			CASE WHEN @Minor = '' THEN CASE WHEN @Major = '' THEN @TreeType ELSE @Major END ELSE @Minor END, -- minor if not blank, then major if not blank, then treetype
		LRO = ISNULL(child.LRO/parent.LRO * 100,0), 
		LostMWh = ISNULL(child.LostMWh/parent.LostMWh * 100,0) 
	FROM UnplanLROTree (@ListName,@StudyYear,CASE WHEN @Major = '' THEN '' ELSE @TreeType END,'','') parent
		LEFT JOIN (SELECT * FROM UnplanLROTree (@ListName,@StudyYear,@TreeType,@Major,@Minor)) child ON 1=1

)


