﻿


CREATE FUNCTION [dbo].[MaintCostvsUCU]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@PeerGroup VARCHAR(30),
	@Version VARCHAR(30), --MWH or MW
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(
	--need to add the lines

	SELECT t.UnitLabel, 
		MaintIndex = CASE @Version WHEN 'MWH' THEN mtc.AnnMaintCostMWH WHEN 'MW' THEN mtc.AnnMaintCostMW ELSE 0 END * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear), 
		UnplannedCommUnavail = ISNULL(c.PeakUnavail_F,0) + ISNULL(c.PeakUnavail_M,0)
	FROM TSort t 
		INNER join Breaks b on b.Refnum = t.Refnum 
		INNER JOIN CommUnavail c ON t.Refnum = c.Refnum
		INNER join MainttotCalc mtc ON mtc.Refnum = t.Refnum
	WHERE t.studyyear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) 
		AND ((
			(@PeerGroup = 'C3' AND b.CoalNMCGroup2 = 3) OR 
			(@PeerGroup = 'C2' AND b.CoalNMCGroup2 = 2) OR
			(@PeerGroup = 'C1' AND b.CoalNMCGroup2 = 1) OR
			(@PeerGroup = 'SGO' AND b.SteamGasOil = 'Y') OR
			(@PeerGroup = b.CRVSizeGroup)
			OR t.Refnum LIKE '%G')

		)
)



