﻿




CREATE FUNCTION [dbo].[GetEGCColors]
(	
	@ShortListName VARCHAR(30),
	@ColorNumber INT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 Label, Value 
	FROM SlideSourceData 
	WHERE Source = @ShortListName 
		AND Type = 'EGCColors' 
		AND Filter1 = @ColorNumber 
	ORDER BY CAST(Filter2 AS int)

)





