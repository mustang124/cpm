﻿
CREATE FUNCTION [dbo].[zzzLineTest]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

--SELECT c1= 'Sheet1!$E$1',c2='1Q/2Q Break',c3='4FC774',c4='Sheet1!$E$2:$E$15',c5='14',c6='6.57',c7='6.57',c8='6.57',c9='6.57',c10='6.57',c11='6.57',c12='6.57',c13='6.57',c14='6.57',c15='6.57',c16='6.57',c17='6.57',c18='6.57',c19='6.57'


select 'rownum' = 1,'1Q/2Q' = 6.57, '2Q/3Q' = 9.16, '3Q/4Q' = 16.5
union all
select 2, 6.57, 9.16, 16.5
union  all
select 3, 6.57, 9.16, 16.5
union  all
select 4, 6.57, 9.16, 16.5
union  all
select 5, 6.57, 9.16, 16.5
union  all
select 6, 6.57, 9.16, 16.5
union  all
select 7, 6.57, 9.16, 16.5
union  all
select 8, 6.57, 9.16, 16.5
union  all
select 9, 6.57, 9.16, 16.5
union  all
select 10, 6.57, 9.16, 16.5
union  all
select 11, 6.57, 9.16, 16.5
union  all
select 12, 6.57, 9.16, 16.5
union  all
select 13, 6.57, 9.16, 16.5
union  all
select 14, 6.57, 9.16, 16.5

)

