﻿


CREATE FUNCTION [dbo].[GapTable]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT SiteLabel = '<FMT colWidth=200 rowHeight=50>',
		RevenueGap = '<FMT rowHeight=50>Revenue Gap',
		FuelOperGap = '<FMT rowHeight=50>Operations/Fuels Gap',
		OpexGap = '<FMT rowHeight=50>Expenditures<sub>' + @CRVGroup + '</sub> Gap'

	UNION ALL

	SELECT '<FMT colWidth=72 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(ss.sitelabel) + '</b>', 
		RevenueGapKUS = '<FMT Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(RevenueGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))),
		FuelOperGapKUS = '<FMT Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(FuelOperGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))),
		OpexGapKUS = '<FMT Align=Center>' + 
			Power.dbo.GetDollarsText(SUM(ISNULL(
				CASE @CRVGroup WHEN 'MWH' THEN OpexEmmGapKUS 
							WHEN 'MW' THEN OpexEmmGapKUSMW 
							WHEN 'EGC' THEN OpexEmmGapKUSEGC END
			, 0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)))
	FROM CRVGap, Tsort t
		INNER JOIN StudySites ss on t.SiteID = ss.SiteID
	WHERE crvgap.refnum = t.refnum
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND t.studyyear = @StudyYear
	GROUP BY ss.sitelabel

)



