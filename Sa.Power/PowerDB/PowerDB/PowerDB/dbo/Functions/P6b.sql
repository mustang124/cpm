﻿



CREATE FUNCTION [dbo].[P6b]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30),
	@ProcessType VARCHAR(30), -- should be Boiler, CTG, Other, or Turbine
	@ShortListName VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT UnitLabel, ROSMaint = 0, Maint = 0, ROSPeakUnavail_Tot, PeakUnavail_Tot, ROSPeakUnavail_Unp, PeakUnavail_Unp FROM P6New (@ListName, @StudyYear, @FuelGroup, @ProcessType, @ShortListName, '')

	--SELECT TOP 1000 UnitLabel = CASE n.Num WHEN 2 THEN 'Study' WHEN 3 THEN 'Company' WHEN 5 THEN 'Study' WHEN 6 THEN 'Company' ELSE '' END,
	--	ROSMaint2 = 0, --filler for the chart
	--	Maint2 = 0, --filler for the chart
	--	ROSPeakUnavail_Tot2 = CASE WHEN n.Num = 4 THEN SUM(CASE WHEN x.ROS = 'Y' THEN 
	--					CASE LEFT(@ProcessType,3)
	--						WHEN 'Boi' THEN s.BoilerLostPeakMWH_Tot WHEN 'CTG' THEN s.CTGLostPeakMWH_Tot WHEN 'Oth' THEN s.OtherLostPeakMWH_Tot WHEN 'Tur' THEN s.TurbineLostPeakMWH_Tot
	--					END 
	--				ELSE 0 END)/SUM(CASE WHEN x.ROS = 'Y' THEN s.TotPeakMWH ELSE 0.001 END)*100
	--				ELSE 0 END,
	--	PeakUnavail_Tot2 = CASE WHEN n.Num = 5 THEN SUM(CASE WHEN x.ROS = 'N' THEN 
	--					CASE LEFT(@ProcessType,3)
	--						WHEN 'Boi' THEN s.BoilerLostPeakMWH_Tot WHEN 'CTG' THEN s.CTGLostPeakMWH_Tot WHEN 'Oth' THEN s.OtherLostPeakMWH_Tot WHEN 'Tur' THEN s.TurbineLostPeakMWH_Tot
	--					END 
	--				ELSE 0 END)/SUM(CASE WHEN x.ROS = 'N' THEN s.TotPeakMWH ELSE 0.001 END)*100
	--				ELSE 0 END,
	--	ROSPeakUnavail_Unp2 = CASE WHEN n.Num = 7 THEN SUM(CASE WHEN x.ROS = 'Y' THEN 
	--					CASE LEFT(@ProcessType,3)
	--						WHEN 'Boi' THEN s.BoilerLostPeakMWH_Unp WHEN 'CTG' THEN s.CTGLostPeakMWH_Unp WHEN 'Oth' THEN s.OtherLostPeakMWH_Unp WHEN 'Tur' THEN s.TurbineLostPeakMWH_Unp
	--					END 
	--				ELSE 0 END)/SUM(CASE WHEN x.ROS = 'Y' THEN s.TotPeakMWH ELSE 0.001 END)*100
	--				ELSE 0 END,
	--	PeakUnavail_Unp2 = CASE WHEN n.Num = 8 THEN SUM(CASE WHEN x.ROS = 'N' THEN 
	--					CASE LEFT(@ProcessType,3)
	--						WHEN 'Boi' THEN s.BoilerLostPeakMWH_Unp WHEN 'CTG' THEN s.CTGLostPeakMWH_Unp WHEN 'Oth' THEN s.OtherLostPeakMWH_Unp WHEN 'Tur' THEN s.TurbineLostPeakMWH_Unp
	--					END 
	--				ELSE 0 END)/SUM(CASE WHEN x.ROS = 'N' THEN s.TotPeakMWH ELSE 0.001 END)*100
	--				ELSE 0 END

	--FROM PowerGlobal.dbo.Numbers n
	--	LEFT JOIN FourPack s ON 1=1
	--	INNER JOIN TSort t ON t.Refnum = s.Refnum
	--	INNER JOIN NERCFactors g ON g.Refnum = s.Refnum
	--	INNER JOIN Breaks b ON b.Refnum = s.Refnum
	--	INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = s.Refnum
	--	INNER JOIN 
	--		(SELECT Refnum, ROS = 'Y' FROM _RL WHERE ListName = @ShortListName AND Refnum NOT IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
	--		UNION
	--		SELECT Refnum, ROS = 'N' FROM _RL WHERE ListName = @ListName AND Refnum IN (SELECT Refnum FROM TSort WHERE StudyYear = @StudyYear)) x ON x.Refnum = s.Refnum
	--WHERE n.Num <= 8 AND 
	--(
	--	(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
	--	(@FuelGroup = 'Gas' AND b.SteamGasOil = 'Y') OR
	--	(@FuelGroup = 'CE' AND b.CRVSizeGroup LIKE 'CE%') OR
	--	(@FuelGroup = 'CG' AND b.CRVSizeGroup LIKE 'CG%') OR
	--	(@FuelGroup = 'SC' AND b.CRVSizeGroup = 'SC')

	--	)

	--	group by n.Num
	--	order by n.Num


)
