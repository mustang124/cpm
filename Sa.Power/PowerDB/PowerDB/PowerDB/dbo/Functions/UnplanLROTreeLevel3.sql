﻿


CREATE FUNCTION [dbo].[UnplanLROTreeLevel3]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30) --"", "Tech", "Coal", "SGO", "Elec","Cogen", Tech and blank return the same
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT AGS = 'Air/Gas/Slag', 
		AGSLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Air/Gas/Slag' THEN LRO ELSE 0 END),0), 
		AGSMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Air/Gas/Slag' THEN LostMWH ELSE 0 END),0),
		VC = 'Valves & Controls', 
		VCLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Valves & Controls (VC)' THEN LRO ELSE 0 END), 0),
		VCMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Valves & Controls (VC)' THEN LostMWH ELSE 0 END),0),
		Comb = 'Fuel/ Combustors', 
		CombLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Combustion' THEN LRO ELSE 0 END), 0),
		CombMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Combustion' THEN LostMWH ELSE 0 END),0),
		CWS = 'Cooling Water', 
		CWSLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Cooling Water Facilities (CWF)' THEN LRO ELSE 0 END), 0),
		CWSMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Cooling Water Facilities (CWF)' THEN LostMWH ELSE 0 END),0),
		FD = 'Fuel Delivery', 
		FDLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Fuel Delivery System (FHF)' THEN LRO ELSE 0 END), 0),
		FDMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Fuel Delivery System (FHF)' THEN LostMWH ELSE 0 END),0),
		rownum = 3
	FROM UnplanLROTreeSource (@ListName, @StudyYear)
	WHERE (
		(@TreeType = '' OR @TreeType = 'Tech') OR
		(@TreeType = 'Coal' AND FuelGroup = 'Coal' AND CombinedCycle = 'N') OR
		(@TreeType = 'SGO' AND FuelGroup = 'Gas & Oil' AND CombinedCycle = 'N') OR
		(@TreeType = 'Elec' AND CombinedCycle = 'Y' AND CogenElec = 'Elec') OR
		(@TreeType = 'Cogen' AND CombinedCycle = 'Y' AND CogenElec = 'Cogen')
		)
		AND SAIMinorEquip IN ('Air/Gas/Slag','Valves & Controls (VC)','Cooling Water Facilities (CWF)','Fuel Delivery System (FHF)','Combustion')

	UNION

	SELECT AGS = 'Tube Leaks', 
		AGSLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Tube Leaks' THEN LRO ELSE 0 END), 0),
		AGSMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Tube Leaks' THEN LostMWH ELSE 0 END),0),
		VC = 'Steam Turbine', 
		VCLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Turbine' THEN LRO ELSE 0 END), 0),
		VCMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Turbine' THEN LostMWH ELSE 0 END),0),
		Comb = 'Inlet/ Compressor', 
		CombLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Inlet' THEN LRO ELSE 0 END), 0),
		CombMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Inlet' THEN LostMWH ELSE 0 END),0),
		CWS = 'Emission Controls', 
		CWSLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Emission Controls' THEN LRO ELSE 0 END), 0),
		CWSMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Emission Controls' THEN LostMWH ELSE 0 END),0),
		FD = 'BOP Other', 
		FDLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Other' THEN LRO ELSE 0 END), 0),
		FDMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Other' THEN LostMWH ELSE 0 END),0),
		rownum = 4
	FROM UnplanLROTreeSource (@ListName, @StudyYear)
	WHERE (
		(@TreeType = '' OR @TreeType = 'Tech') OR
		(@TreeType = 'Coal' AND FuelGroup = 'Coal' AND CombinedCycle = 'N') OR
		(@TreeType = 'SGO' AND FuelGroup = 'Gas & Oil' AND CombinedCycle = 'N') OR
		(@TreeType = 'Elec' AND CombinedCycle = 'Y' AND CogenElec = 'Elec') OR
		(@TreeType = 'Cogen' AND CombinedCycle = 'Y' AND CogenElec = 'Cogen')
		)
		AND (SAIMinorEquip IN ('Tube Leaks','Inlet','Emission Controls') OR (SAIMinorEquip = 'Turbine' AND SAIMajorEquip = 'Turbine') OR (SAIMinorEquip = 'Other' AND SAIMajorEquip = 'Balance of Plant'))

	UNION

	SELECT AGS = 'Steam/ Feedwater', 
		AGSLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Steam/Feedwater' THEN LRO ELSE 0 END),0), 
		AGSMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Steam/Feedwater' THEN LostMWH ELSE 0 END),0),
		VC = 'Condenser & Auxiliary', 
		VCLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Condenser & Auxiliaries (CA)' THEN LRO ELSE 0 END), 0),
		VCMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Condenser & Auxiliaries (CA)' THEN LostMWH ELSE 0 END),0),
		Comb = 'Turbine', 
		CombLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Turbine' THEN LRO ELSE 0 END), 0),
		CombMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Turbine' THEN LostMWH ELSE 0 END),0),
		CWS = 'Pulverizer', 
		CWSLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Coal Pulverizers/Hammer Mills (CPHM)' THEN LRO ELSE 0 END), 0),
		CWSMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Coal Pulverizers/Hammer Mills (CPHM)' THEN LostMWH ELSE 0 END),0),
		FD = '', 
		FDLRO = '', 
		FDMWH = '', 
		rownum = 5
	FROM UnplanLROTreeSource (@ListName, @StudyYear)
	WHERE (
		(@TreeType = '' OR @TreeType = 'Tech') OR
		(@TreeType = 'Coal' AND FuelGroup = 'Coal' AND CombinedCycle = 'N') OR
		(@TreeType = 'SGO' AND FuelGroup = 'Gas & Oil' AND CombinedCycle = 'N') OR
		(@TreeType = 'Elec' AND CombinedCycle = 'Y' AND CogenElec = 'Elec') OR
		(@TreeType = 'Cogen' AND CombinedCycle = 'Y' AND CogenElec = 'Cogen')
		)
		AND (SAIMinorEquip IN ('Steam/Feedwater','Condenser & Auxiliaries (CA)','Coal Pulverizers/Hammer Mills (CPHM)') OR (SAIMinorEquip = 'Turbine' AND SAIMajorEquip = 'Combustion Turbine'))

	UNION

	SELECT AGS = 'Boiler Other', 
		AGSLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Boiler Misc' THEN LRO ELSE 0 END), 0),
		AGSMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Boiler Misc' THEN LostMWH ELSE 0 END),0),
		VC = 'Generator', 
		VCLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Generator (Gen)' THEN LRO ELSE 0 END), 0),
		VCMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Generator (Gen)' THEN LostMWH ELSE 0 END),0),
		Comb = 'CTG Other', 
		CombLRO = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Other' THEN LRO ELSE 0 END), 0),
		CombMWH = ISNULL(SUM(CASE WHEN SAIMinorEquip = 'Other' THEN LostMWH ELSE 0 END),0),
		CWS = '', 
		CWSLRO = '', 
		CWSMWH = '',
		FD = '', 
		FDLRO = '', 
		FDMWH = '', 
		rownum = 6
	FROM UnplanLROTreeSource (@ListName, @StudyYear)
	WHERE (
		(@TreeType = '' OR @TreeType = 'Tech') OR
		(@TreeType = 'Coal' AND FuelGroup = 'Coal' AND CombinedCycle = 'N') OR
		(@TreeType = 'SGO' AND FuelGroup = 'Gas & Oil' AND CombinedCycle = 'N') OR
		(@TreeType = 'Elec' AND CombinedCycle = 'Y' AND CogenElec = 'Elec') OR
		(@TreeType = 'Cogen' AND CombinedCycle = 'Y' AND CogenElec = 'Cogen')
		)
		AND (SAIMinorEquip IN ('Boiler Misc','Generator (Gen)') OR (SAIMinorEquip = 'Other' AND SAIMajorEquip = 'Combustion Turbine'))














)



