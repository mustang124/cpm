﻿


CREATE FUNCTION [dbo].[zzzRepEventsByValueUnit]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)
	--@CRVGroup VARCHAR(30) --coal or gas & oil

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT TOP 1000 
		b.UnitLabel, 
		TotalDolperLostMWH = ISNULL(TotalDolperLostMWH,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
	FROM PowerGlobal.dbo.Numbers n
		left join (select distinct UnitLabel = SiteLabel FROM RepEventsSource (@ListName, @StudyYear) ) c on 1=1
		LEFT JOIN (
			SELECT UnitLabel = SiteLabel,
				TotalDolperLostMWH = CASE WHEN SUM(LOSTMWH) > 0 THEN (SUM(LRO)/SUM(LOSTMWH))*1000 END,
				Rank = RANK() OVER (PARTITION BY SiteLabel ORDER BY SiteLabel, CASE WHEN SUM(LOSTMWH) > 0 THEN (SUM(LRO)/SUM(LOSTMWH))*1000 END DESC)
			FROM RepEventsSource (@ListName, @StudyYear)
			--WHERE FuelGroup = 'Gas & Oil'
				
			GROUP BY SiteLabel, CAUSE_CODE, PresDescription
			HAVING COUNT(DISTINCT RefEventNum) >1
		) b ON b.Rank = n.Num and b.UnitLabel = c.UnitLabel
	WHERE n.Num <= 7
		and b.UnitLabel is not null
	ORDER BY UnitLabel, ISNULL(TotalDolperLostMWH,0) DESC




	----Using CurrencyID just to get a number from 1 to 7
	--SELECT TOP 1000 c.SiteLabel, TotalDolperLostMWH = TotalDolperLostMWH * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) FROM (
	--	SELECT SiteLabel, Num
	--	FROM PowerGlobal.dbo.Numbers n
	--		FULL OUTER JOIN (
			
	--			SELECT DISTINCT ss.SiteLabel FROM TSort t 
	--				INNER JOIN StudySites ss on t.SiteID = ss.SiteID
	--				INNER JOIN Events e ON e.Refnum = t.Refnum 
	--				INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
	--				INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
	--			WHERE lro.LRO >0 
	--				AND e.CAUSE_CODE <> 7777 
	--				AND t.studyyear = @StudyYear
	--				AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
	--				AND e.EVNT_Category in ('F', 'M')
	--			GROUP BY e.CAUSE_CODE, ss.SiteLabel, PresDescription
	--			HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) > 1
	--			) b ON 1=1
	--	WHERE Num <= 7
	--	) c
	--LEFT JOIN (
	--	SELECT TOP 1000 SiteLabel, 
	--	TotalDolperLostMWH , Rank

	--	FROM (
	--		SELECT e.CAUSE_CODE, 
	--			COUNT(DISTINCT e.EVNT_NO) AS NumEvents, 
	--			--SUM(lro.LRO) AS TotalLRO2, 

	--			ss.SiteLabel, 
	--			COUNT(DISTINCT t.Refnum) AS NumUnits, 
	--			PresDescription, 
	--			SUM(lro.LRO) AS TotalLRO, 
	--		CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN (SUM(lro.LRO)/SUM(e.LOSTMW*e.DURATION))*1000 END AS TotalDolperLostMWH, 
	--			RANK() OVER (PARTITION BY ss.SiteLabel ORDER BY CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN (SUM(lro.LRO)/SUM(e.LOSTMW*e.DURATION))*1000 END DESC) AS Rank
	--		FROM TSort t 
	--			INNER JOIN StudySites ss on t.SiteID = ss.SiteID
	--			INNER JOIN Events e ON e.Refnum = t.Refnum 
	--			INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
	--			INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
	--		WHERE lro.LRO >0 
	--			AND e.CAUSE_CODE <> 7777 
	--			AND t.studyyear = @StudyYear
	--			AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) 
	--			AND e.EVNT_Category in ('F', 'M')
	--		GROUP BY e.CAUSE_CODE, ss.SiteLabel, PresDescription
	--				HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) >1
	--		) b 
	--	WHERE Rank <= 7
	--	) d ON d.SiteLabel = c.SiteLabel AND d.Rank = c.Num
	--ORDER BY c.SiteLabel ASC, TotalDolperLostMWH DESC






)



