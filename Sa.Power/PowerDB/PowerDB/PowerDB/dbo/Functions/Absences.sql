﻿

CREATE FUNCTION [dbo].[Absences]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30)
)
RETURNS TABLE 
AS
RETURN 
(

--UnitLabel		Vac & Hol	Other
--OCC Study		123			456
--OCC Company	234			345
--MPS Study		345			546
--MPS Company	123			464

	SELECT TOP 1000 UnitLabel, [Vacation and Holidays], Other FROM (

	SELECT UnitLabel = 'OCC Study', 'Vacation and Holidays' = SUM (vac), Other = SUM(oth), SortOrder = 1 FROM (
		SELECT vac = CASE WHEN Filter2 = 'VAC' THEN Value ELSE 0 END,
			oth = CASE WHEN Filter2 = 'OTH' THEN Value ELSE 0 END
		FROM SlideSourceData 
		WHERE Type = 'Absences' 
			AND Label = 'OCC' 
			AND Source = @ShortListName
			AND (
				(@CRVGroup = 'Coal' AND Filter1 = 'Coal') OR 
				(@CRVGroup = 'Gas & Oil' AND Filter1 = 'Gas') OR
				(@CRVGroup = 'CC' AND Filter1 = 'CC') OR
				(@CRVGroup = 'Cogen' AND Filter1 = 'Cogen')
				)
		) b

	UNION

		SELECT 'OCC Company',
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) end,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) end, SortOrder = 2
		FROM TSort t
			INNER JOIN PersSTCalc p ON p.Refnum = t.Refnum AND p.SectionID = 'TOCC' 
			INNER JOIN (SELECT Refnum, SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN OCCAbsPcnt ELSE 0 END) AS VacHol,
				SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN 0 ELSE OCCAbsPcnt END) AS Other FROM AbsenceCalc a GROUP BY Refnum) x ON x.Refnum = t.Refnum
			LEFT JOIN Breaks b on b.Refnum = t.Refnum 
		WHERE t.studyyear = @StudyYear
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND (
				(@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR 
				(@CRVGroup = 'Gas & Oil' AND b.FuelGroup = 'Gas & Oil' AND b.CombinedCycle = 'N') OR
				(@CRVGroup = 'CC' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec = 'Elec') OR
				(@CRVGroup = 'Cogen' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec= 'Cogen')
				)

	UNION

	SELECT UnitLabel = 'MPS Study', 'Vacation and Holidays' = SUM (vac), Other = SUM(oth), SortOrder = 3 FROM (
		SELECT vac = CASE WHEN Filter2 = 'VAC' THEN Value ELSE 0 END,
			oth = CASE WHEN Filter2 = 'OTH' THEN Value ELSE 0 END
		FROM SlideSourceData 
		WHERE Type = 'Absences' 
			AND Label = 'MPS' 
			AND Source = @ShortListName
			AND (
				(@CRVGroup = 'Coal' AND Filter1 = 'Coal') OR 
				(@CRVGroup = 'Gas & Oil' AND Filter1 = 'Gas') OR
				(@CRVGroup = 'CC' AND Filter1 = 'CC') OR
				(@CRVGroup = 'Cogen' AND Filter1 = 'Cogen')
				)
		) b

	UNION

		SELECT 'MPS Company',
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.VacHol*p.SiteSTH)/SUM(p.SiteSTH) end,
			CASE WHEN SUM(p.SiteSTH) = 0 THEN 0 ELSE SUM(x.Other*p.SiteSTH)/SUM(p.SiteSTH) end, SortOrder = 4
		FROM TSort t
			INNER JOIN PersSTCalc p ON p.Refnum = t.Refnum AND p.SectionID = 'TMPS' 
			INNER JOIN (SELECT Refnum, SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN MPSAbsPcnt ELSE 0 END) AS VacHol,
				SUM(CASE WHEN AbsCategory IN ('HOL', 'HOLVAC', 'VAC') THEN 0 ELSE MPSAbsPcnt END) AS Other FROM AbsenceCalc a GROUP BY Refnum) x ON x.Refnum = t.Refnum
			LEFT JOIN Breaks b on b.Refnum = t.Refnum 
		WHERE t.studyyear = @StudyYear
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND (
				(@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR 
				(@CRVGroup = 'Gas & Oil' AND b.FuelGroup = 'Gas & Oil' AND b.CombinedCycle = 'N') OR
				(@CRVGroup = 'CC' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec = 'Elec') OR
				(@CRVGroup = 'Cogen' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec= 'Cogen')
				)

	) c ORDER BY SortOrder

)


