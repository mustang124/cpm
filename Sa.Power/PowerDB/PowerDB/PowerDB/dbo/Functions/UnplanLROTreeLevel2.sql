﻿


CREATE FUNCTION [dbo].[UnplanLROTreeLevel2]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30) --"", "Tech", "Coal", "SGO", "Elec","Cogen", Tech and blank return the same
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT Boiler = 'Boiler', 
		BoilerLRO = SUM(CASE WHEN SAIMajorEquip = 'Boiler' THEN LRO ELSE 0 END), 
		BoilerMWH = SUM(CASE WHEN SAIMajorEquip = 'Boiler' THEN LostMWH ELSE 0 END),
		STG = 'STG', 
		STGLRO = SUM(CASE WHEN SAIMajorEquip = 'Turbine' THEN LRO ELSE 0 END), 
		STGMWH = SUM(CASE WHEN SAIMajorEquip = 'Turbine' THEN LostMWH ELSE 0 END),
		Ext = 'External', 
		ExternalLRO = SUM(CASE WHEN SAIMajorEquip = 'External' THEN LRO ELSE 0 END), 
		ExternalMWH = SUM(CASE WHEN SAIMajorEquip = 'External' THEN LostMWH ELSE 0 END),
		CTG = 'CTG', 
		CTGLRO = SUM(CASE WHEN SAIMajorEquip = 'Combustion Turbine' THEN LRO ELSE 0 END), 
		CTGMWH = SUM(CASE WHEN SAIMajorEquip = 'Combustion Turbine' THEN LostMWH ELSE 0 END),
		BOP = 'Balance of Plant', 
		BalanceLRO = SUM(CASE WHEN SAIMajorEquip = 'Balance of Plant' THEN LRO ELSE 0 END), 
		BalanceMWH = SUM(CASE WHEN SAIMajorEquip = 'Balance of Plant' THEN LostMWH ELSE 0 END)
	FROM UnplanLROTreeSource (@ListName, @StudyYear)
	WHERE (
		(@TreeType = '' OR @TreeType = 'Tech') OR
		(@TreeType = 'Coal' AND FuelGroup = 'Coal' AND CombinedCycle = 'N') OR
		(@TreeType = 'SGO' AND FuelGroup = 'Gas & Oil' AND CombinedCycle = 'N') OR
		(@TreeType = 'Elec' AND CombinedCycle = 'Y' AND CogenElec = 'Elec') OR
		(@TreeType = 'Cogen' AND CombinedCycle = 'Y' AND CogenElec = 'Cogen')
		)


)



