﻿

CREATE FUNCTION [dbo].[zzzPricingHubOffPeak]
(	

	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)

	--need to add the hour info here? different depending on location

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT --PricingHub, 
	[Date], OffPeakPrice = OffPeakPrice * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) FROM (
		SELECT * FROM PricingHubSlide (@ListName,@StudyYear)) b

)


