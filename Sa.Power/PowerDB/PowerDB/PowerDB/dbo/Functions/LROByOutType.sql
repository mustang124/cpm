﻿
CREATE FUNCTION [dbo].[LROByOutType]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30), --Brayton or Rankine
	@ShortListName VARCHAR(30) --Power13 Steam or Power13 CC

)
RETURNS TABLE 
AS
RETURN 
(

select top 1000 unitlabel, Forced, Forced2, Maint, Maint2, Planned, Planned2 from (

SELECT UnitLabel = 'Study', Forced = case when (Forcedval + Maintval + Plannedval) = 0 then 0 else (Forcedval / (Forcedval + Maintval + Plannedval) * 100) end, Forced2 = 0, Maint = 0, Maint2 = 0, Planned = 0, Planned2 = 0, sort = 1
FROM (
SELECT SUM(CommUnavail.LRO_F)/1000 AS Forcedval, 
SUM(CommUnavail.LRO_M)/1000 AS Maintval,
SUM(CommUnavail.LRO_P)/1000 AS Plannedval
FROM CommUnavail left join TSort t on t.refnum = CommUnavail.refnum
WHERE t.Refnum IN (SELECT refnum FROM _rl WHERE listname = @ShortListName)
) b

union

SELECT 'Company', Forced = 0, Forced2 = case when (Forcedval + Maintval + Plannedval) = 0 then 0 else (Forcedval / (Forcedval + Maintval + Plannedval) * 100) end, Maint = 0, Maint2 = 0, Planned = 0, Planned2 = 0, sort = 2
 FROM (select 
Sum(cu.LRO_F)/1000 AS Forcedval, 
Sum(cu.LRO_M)/1000 AS Maintval,
Sum(cu.LRO_P)/1000 AS Plannedval
FROM CommUnavail cu, Tsort t
WHERE cu.Refnum = t.Refnum and t.studyyear = @StudyYear
and cu.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) AND cu.Refnum NOT LIKE '%G'
and ((@CRVGroup = 'Rankine' and cu.Refnum IN (Select refnum from Breaks where FuelGroup = 'Coal' OR CombinedCycle = 'N'))
or (@CRVGroup = 'Brayton' and cu.Refnum IN (Select refnum from Breaks where CombinedCycle = 'Y')))
) c

union

select '',0,0,0,0,0,0,3

union

SELECT UnitLabel = 'Study', Forced = 0, Forced2 = 0, Maint = case when (Forcedval + Maintval + Plannedval) = 0 then 0 else (Maintval / (Forcedval + Maintval + Plannedval) * 100) end, Maint2 = 0, Planned = 0, Planned2 = 0, sort = 4
FROM (
SELECT SUM(CommUnavail.LRO_F)/1000 AS Forcedval, 
SUM(CommUnavail.LRO_M)/1000 AS Maintval,
SUM(CommUnavail.LRO_P)/1000 AS Plannedval
FROM CommUnavail left join TSort t on t.refnum = CommUnavail.refnum
WHERE t.Refnum IN (SELECT refnum FROM _rl WHERE listname = @ShortListName)
) b

union

SELECT 'Company', Forced = 0, Forced2 = 0, Maint =0, Maint2 = case when (Forcedval + Maintval + Plannedval) = 0 then 0 else (Maintval / (Forcedval + Maintval + Plannedval) * 100) end, Planned = 0, Planned2 = 0, sort = 5
 
 FROM (select 
Sum(cu.LRO_F)/1000 AS Forcedval, 
Sum(cu.LRO_M)/1000 AS Maintval,
Sum(cu.LRO_P)/1000 AS Plannedval
FROM CommUnavail cu, Tsort t
WHERE cu.Refnum = t.Refnum and t.studyyear = @StudyYear
and cu.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) AND cu.Refnum NOT LIKE '%G'
and ((@CRVGroup = 'Rankine' and cu.Refnum IN (Select refnum from Breaks where FuelGroup = 'Coal' OR CombinedCycle = 'N'))
or (@CRVGroup = 'Brayton' and cu.Refnum IN (Select refnum from Breaks where CombinedCycle = 'Y')))

) c

union

select '',0,0,0,0,0,0,6

union

SELECT UnitLabel = 'Study', Forced = 0, Forced2 = 0, Maint =0, Maint2 = 0, Planned = case when (Forcedval + Maintval + Plannedval) = 0 then 0 else (Plannedval / (Forcedval + Maintval + Plannedval) * 100) end, Planned2 = 0, sort = 7
FROM (
SELECT SUM(CommUnavail.LRO_F)/1000 AS Forcedval, 
SUM(CommUnavail.LRO_M)/1000 AS Maintval,
SUM(CommUnavail.LRO_P)/1000 AS Plannedval
FROM CommUnavail left join TSort t on t.refnum = CommUnavail.refnum
WHERE t.Refnum IN (SELECT refnum FROM _rl WHERE listname = @ShortListName)
) b

union

SELECT 'Company', Forced = 0, Forced2 = 0, Maint =0, Maint2 = 0, Planned = 0, Planned2 = case when (Forcedval + Maintval + Plannedval) = 0 then 0 else (Plannedval / (Forcedval + Maintval + Plannedval) * 100) end, sort = 8
 FROM (select 
Sum(cu.LRO_F)/1000 AS Forcedval, 
Sum(cu.LRO_M)/1000 AS Maintval,
Sum(cu.LRO_P)/1000 AS Plannedval
FROM CommUnavail cu, Tsort t
WHERE cu.Refnum = t.Refnum and t.studyyear = @StudyYear
and cu.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) AND cu.Refnum NOT LIKE '%G'
and ((@CRVGroup = 'Rankine' and cu.Refnum IN (Select refnum from Breaks where FuelGroup = 'Coal' OR CombinedCycle = 'N'))
or (@CRVGroup = 'Brayton' and cu.Refnum IN (Select refnum from Breaks where CombinedCycle = 'Y')))

) c

) d
order by sort

--SELECT UnitLabel = 'Study', value = (Maintval / (Forcedval + Maintval + Plannedval) * 100)
--FROM (
--SELECT SUM(CommUnavail.LRO_F)/1000 AS Forcedval, 
--SUM(CommUnavail.LRO_M)/1000 AS Maintval,
--SUM(CommUnavail.LRO_P)/1000 AS Plannedval
--FROM CommUnavail left join TSort t on t.refnum = CommUnavail.refnum
--WHERE t.Refnum IN (SELECT refnum FROM _rl WHERE listname = @ShortListName)
--) b

--union

--SELECT UnitLabel = 'Study', value = (Plannedval / (Forcedval + Maintval + Plannedval) * 100)
--FROM (
--SELECT SUM(CommUnavail.LRO_F)/1000 AS Forcedval, 
--SUM(CommUnavail.LRO_M)/1000 AS Maintval,
--SUM(CommUnavail.LRO_P)/1000 AS Plannedval
--FROM CommUnavail left join TSort t on t.refnum = CommUnavail.refnum
--WHERE t.Refnum IN (SELECT refnum FROM _rl WHERE listname = @ShortListName)
--) b





--SELECT SUM(CommUnavail.LRO_F)/1000 AS Forced, 
--SUM(CommUnavail.LRO_P)/1000 AS Planned, 
--SUM(CommUnavail.LRO_M)/1000 AS Maint
--FROM CommUnavail left join TSort t on t.refnum = CommUnavail.refnum
--WHERE t.Refnum IN (SELECT refnum FROM _rl WHERE listname = 'Power13 CC')








--SELECT 
--Sum(cu.LRO_F)/1000 AS F, 
--Sum(cu.LRO_M)/1000 AS M,
--Sum(cu.LRO_P)/1000 AS P
--FROM CommUnavail cu, Tsort t
--WHERE cu.Refnum = t.Refnum and t.studyyear = @StudyYear
--and cu.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
--and cu.Refnum IN (case when @CRVGroup = 'Rankine' THEN (Select refnum from Breaks where FuelGroup = 'Coal' OR CombinedCycle = 'N') else (Select Refnum from Breaks where CombinedCycle = 'Y') end)


--SELECT 
--Sum(cu.LRO_F)/1000 AS F, 
--Sum(cu.LRO_M)/1000 AS M,
--Sum(cu.LRO_P)/1000 AS P
--FROM CommUnavail cu, Tsort t
--WHERE cu.Refnum = t.Refnum and t.studyyear = 2013
--and cu.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = 'Power13: WWW')
--and cu.refnum in (select refnum from breaks where combinedcycle = 'Y')



	----Group and Pacesetter values
	--SELECT UnitLabel = Label, 
	--	'Equivalent Unplanned Outage Factor, 2Yr' = SUM(EUOF), 
	--	'Equivalent Planned Outage Factor, 2Yr' = SUM(EPOF) FROM (
	--	SELECT Label, EUOF = Value, EPOF = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'TechUnavail' AND Filter1 = @CRVGroup AND Filter2 = 'EUOF'
	--	UNION
	--	SELECT Label, EUOF = NULL, EPOF = Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'TechUnavail' AND Filter1 = @CRVGroup AND Filter2 = 'EPOF') b
	--GROUP BY Label

	--UNION ALL

	----Company average values
	--SELECT 'Co Avg',
	--	EUOF = GlobalDB.dbo.WtAvg(EUOF2Yr, WPH2Yr),
	--	EPOF = GlobalDB.dbo.WtAvg(EPOF2Yr, WPH2Yr)
	--FROM TSort t
	--	LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--	LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	--WHERE t.StudyYear = @StudyYear 
	--	AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
	--	AND b.CRVGroup = @CRVGroup

	--UNION ALL

	----Unit Values
	--SELECT TOP 1000 UnitLabel, --TOP 1000 here so that the ORDER BY will work in a TVF
	--	EUOF = ISNULL(nf.EUOF2Yr,0),
	--	EPOF = ISNULL(nf.EPOF2Yr,0)
	--	--WPH = ISNULL(nf.WPH2Yr,0)
	--FROM TSort t
	--	LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--	LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	--WHERE t.StudyYear = @StudyYear 
	--	AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
	--	AND b.CRVGroup = @CRVGroup
	--ORDER BY ISNULL(nf.EUOF2yr,0) DESC

)

