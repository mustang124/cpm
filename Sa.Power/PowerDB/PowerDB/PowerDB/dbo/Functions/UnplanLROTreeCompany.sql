﻿


CREATE FUNCTION [dbo].[UnplanLROTreeCompany]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)
	--@TreeType VARCHAR(30) --"", "Tech", "Coal", "SGO", "Elec","Cogen", Tech and blank return the same
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 row1 = '', row2 = '', row3 = '', row4 = '<FMT rowHeight=10 fontHeight=1>', row5 = '<FMT rowHeight=10 fontHeight=1>', row6 = '', row7 = ''--, row8 = '<FMT rowHeight=10 fontHeight=1>', row9 = '', row10 = '', row11 = '<FMT rowHeight=10 fontHeight=1>', row12 = '', row13 = '', row14 = '<FMT rowHeight=10 fontHeight=1>', row15 = '', row16 = '', row17 = '<FMT rowHeight=10 fontHeight=1>', row18 = '', row19 = ''

	UNION ALL

	SELECT TOP 1000 row1, row2, row3, row4, row5, row6, row7=''--, row8='<FMT fontHeight=1>', row9='', row10='', row11='<FMT fontHeight=1>', row12='', row13='', row14='<FMT fontHeight=1>', row15='', row16='', row17='<FMT fontHeight=1>', row18='', row19='' 
	FROM (


	select Num, rownum, row1='',row2= ISNULL(company.row2,''),row3='',row4='<FMT fontHeight=1' + case when num = 27 then ' RightBorder=1' else '' end + case when num between 10 and 45 then ' BottomBorder=1' else '' end + '>',row5='<FMT fontHeight=1' + case when num in (9,21,33,45) then ' RightBorder=1' else '' end + '>', row6 = ISNULL(coal.row2,isnull(gas.row2,isnull(ctg.row2,isnull(cogen.row2,''))))

	from PowerGlobal.dbo.Numbers n
	left join (select rownum = 23, row2 = '<FMT Merge=10,2 Align=Center Color=660066 ColorFont=FFFFFF>Total LRO<br />k ' + RTRIM(@CurrencyCode) + ' ' + REPLACE(CONVERT(varchar, CAST(ROUND(ISNULL(SUM(LRO * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)),0),0) AS money), 1),'.00','') + '<br />(' + REPLACE(CONVERT(varchar, CAST(ROUND(ISNULL(SUM(LostMWH),0),0) AS money), 1),'.00','') + ' MWh)'
	 from UnplanLROTreeTop(@ListName,@StudyYear,'')) company on rownum = Num

	left join (select coalrownum = 6, row2 = '<FMT Merge=8,2 Align=Center Color=' + CASE WHEN a.LostMWh/b.LostMWh > 0.1 THEN '660066' ELSE '0380CD' END + ' ColorFont=FFFFFF>Rankine (Coal) ' + ISNULL(CAST(ROUND(a.LRO/b.LRO*100,0) AS varchar),0) + '% (' + ISNULL(CAST(ROUND(a.lostmwh/b.lostmwh*100,0) AS varchar),0) + '%)'
	 from UnplanLROTreeTop(@ListName,@StudyYear,'Coal') a
	left join (select * from UnplanLROTreeTop(@ListName,@StudyYear,'')) b on 1=1) coal on coalrownum = Num

	left join (select gasrownum = 18, row2 = '<FMT Merge=8,2 Align=Center Color=' + CASE WHEN a.LostMWh/b.LostMWh > 0.1 THEN '660066' ELSE '0380CD' END + ' ColorFont=FFFFFF>Rankine (Gas & Oil) ' + ISNULL(CAST(ROUND(a.LRO/b.LRO*100,0) AS varchar),0) + '% (' + ISNULL(CAST(ROUND(a.lostmwh/b.lostmwh*100,0) AS varchar),0) + '%)'
	 from UnplanLROTreeTop(@ListName,@StudyYear,'SGO') a
	left join (select * from UnplanLROTreeTop(@ListName,@StudyYear,'')) b on 1=1) gas on gasrownum = Num

	left join (select ctgrownum = 30, row2 = '<FMT Merge=8,2 Align=Center Color=' + CASE WHEN a.LostMWh/b.LostMWh > 0.1 THEN '660066' ELSE '0380CD' END + ' ColorFont=FFFFFF>Combustion Turbine Blocks ' + ISNULL(CAST(ROUND(a.LRO/b.LRO*100,0) AS varchar),0) + '% (' + ISNULL(CAST(ROUND(a.lostmwh/b.lostmwh*100,0) AS varchar),0) + '%)'
	 from UnplanLROTreeTop(@ListName,@StudyYear,'Elec') a
	left join (select * from UnplanLROTreeTop(@ListName,@StudyYear,'')) b on 1=1) ctg on ctgrownum = Num

	left join (select cogenrownum = 42, row2 = '<FMT Merge=8,2 Align=Center Color=' + CASE WHEN a.LostMWh/b.LostMWh > 0.1 THEN '660066' ELSE '0380CD' END + ' ColorFont=FFFFFF>Cogeneration ' + ISNULL(CAST(ROUND(a.LRO/b.LRO*100,0) AS varchar),0) + '% (' + ISNULL(CAST(ROUND(a.lostmwh/b.lostmwh*100,0) AS varchar),0) + '%)'
	 from UnplanLROTreeTop(@ListName,@StudyYear,'Cogen') a
	left join (select * from UnplanLROTreeTop(@ListName,@StudyYear,'')) b on 1=1) cogen on cogenrownum = Num

	where n.Num <= 58 and n.Num > 1
	) b
	ORDER BY Num

)



