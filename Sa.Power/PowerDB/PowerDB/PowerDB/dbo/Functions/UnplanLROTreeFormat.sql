﻿


CREATE FUNCTION [dbo].[UnplanLROTreeFormat]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30), --"", "Tech", "Coal", "SGO", "Elec","Cogen", Tech and blank return the same
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 f1,f2,f3,f4,f5, rownum FROM (

	SELECT f1='',f2='',
	f3='<FMT Merge=10,2 Align=Center Color=660066 ColorFont=FFFFFF>Total LRO<br />k ' + RTRIM(@CurrencyCode) + ' ' + REPLACE(CONVERT(varchar, CAST(ROUND(ISNULL(SUM(LRO * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)),0),0) AS money), 1),'.00','') + '<br />(' + REPLACE(CONVERT(varchar, CAST(ROUND(ISNULL(SUM(LostMWH),0),0) AS money), 1),'.00','') + ' MWh)',
	f4='',f5='', rownum = 1 FROM UnplanLROTreeTop (@ListName, @StudyYear, @TreeType)

	UNION

--cast(ROUND(SUM(u.lro)/max(b.LRO) * 100,0) as varchar)



	SELECT '<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(BoilerLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + Boiler + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(BoilerLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(BoilerMWH,0)/ISNULL(LostMWh,0) END *100,0) AS varchar) + '%)',
		'<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(STGLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + STG + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(STGLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(STGMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)',
		'<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(ExternalLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + Ext + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(ExternalLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(ExternalMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)',
		'<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(CTGLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + CTG + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(CTGLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(CTGMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)', 
		'<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(BalanceLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + BOP + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(BalanceLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(BalanceMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)',
		rownum = 2
	FROM UnplanLROTreeLevel2 (@ListName, @StudyYear, @TreeType) a
		left join (SELECT * FROM UnplanLROTreeTop (@ListName, @StudyYear, @TreeType)) b ON 1=1

	UNION 

	SELECT '<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(AGSLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + AGS + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(AGSLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(AGSMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)',
		'<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(VCLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + VC + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(VCLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(VCMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)',
		'<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(CombLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + Comb + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(CombLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(CombMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)',
		CASE WHEN CWS ='' THEN '' ELSE '<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(CWSLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + CWS + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(CWSLRO,0)/ISNULL(LRO,0) END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(CWSMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)' END, 
		CASE WHEN FD = '' THEN '' ELSE '<FMT Merge=6,2 Align=Center Color=' + CASE WHEN ISNULL(LRO,0) = 0 THEN '0380CD' ELSE CASE WHEN ISNULL(FDLRO,0)/ISNULL(LRO,0) > 0.1 THEN '660066' ELSE '0380CD' END END + ' ColorFont=FFFFFF>' + FD + ' ' + CAST(ROUND(CASE WHEN ISNULL(LRO,0) = 0 THEN 0 ELSE ISNULL(FDLRO,0)/ISNULL(LRO,0)END * 100,0) AS varchar) + '% (' + CAST(ROUND(CASE WHEN ISNULL(LostMWH,0) = 0 THEN 0 ELSE ISNULL(FDMWH,0)/ISNULL(LostMWh,0) END*100,0) AS varchar) + '%)' END,
		rownum

	FROM UnplanLROTreeLevel3 (@ListName, @StudyYear, @TreeType) a
		left join (SELECT * FROM UnplanLROTreeTop (@ListName, @StudyYear, @TreeType)) b ON 1=1

	) b
	ORDER BY rownum
)



