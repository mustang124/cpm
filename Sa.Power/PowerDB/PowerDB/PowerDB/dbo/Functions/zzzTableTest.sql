﻿

CREATE FUNCTION [dbo].[zzzTableTest]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

--SELECT c1= 'Sheet1!$E$1',c2='1Q/2Q Break',c3='4FC774',c4='Sheet1!$E$2:$E$15',c5='14',c6='6.57',c7='6.57',c8='6.57',c9='6.57',c10='6.57',c11='6.57',c12='6.57',c13='6.57',c14='6.57',c15='6.57',c16='6.57',c17='6.57',c18='6.57',c19='6.57'


select '' as SiteLabel,
'RevenueGap' as RevenueGap,
'Operations/Fuels Gap' as FuelOperGap,
OpexGap = CASE @CRVGroup
	WHEN 'MWH' THEN 'ExpendituresMWh Gap'
	WHEN 'MW' THEN 'ExpendituresMW Gap'
	WHEN 'EGC' THEN 'ExpendituresEGC Gap'
	END

union all

Select ss.sitelabel, 
cast(sum(ISNULL(RevenueGapKUS,0)) as varchar) as RevenueGapKUS, 
cast(sum(ISNULL(FuelOperGapKUS,0)) as varchar) as FuelOperGapKUS,

OpexGapKUS = CASE @CRVGroup
	WHEN 'MWH' THEN cast(sum(ISNULL(OpexEmmGapKUS,0)) as varchar)
	WHEN 'MW' THEN cast(sum(ISNULL(OpexEmmGapKUSMW,0)) as varchar)
	WHEN 'EGC' THEN cast(sum(ISNULL(OpexEmmGapKUSEGC,0)) as varchar)
	END


--cast(sum(ISNULL(OpexEmmGapKUS,0)) as varchar) as OpexGapKUS
from CRVGap, Tsort t
INNER JOIN StudySites ss on t.SiteID = ss.SiteID
where crvgap.refnum = t.refnum
and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) and t.studyyear = @StudyYear
group by ss.sitelabel

)


