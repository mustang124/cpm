﻿


CREATE FUNCTION [dbo].[zzzRepEventsByCountUnitTable]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)
	--@CRVGroup VARCHAR(30) --coal or gas & oil

)
RETURNS TABLE 
AS
RETURN 
(

	select 'Site' AS SiteLabel,
	'<FMT colWidth=106 LeftMargin=7 ColorFont=98002E><b>Cause Code</b>' as CauseCode,
	'<FMT LeftMargin=7># Events' as NumEvents,
	'<FMT LeftMargin=7>Total LRO, k ' + RTRIM(@CurrencyCode) as TotalLRO,
	'<FMT LeftMargin=7># Plants' as NumSites,
	'<FMT LeftMargin=7># Units' as NumUnits,
	'' as PresDescription

	union all

	select c.SiteLabel, '<FMT LeftMargin=7 ColorFont=98002E>' + '<b>' + d.CAUSE_CODE + '</b>', '<FMT LeftMargin=7>' + d.NumEvents, '<FMT LeftMargin=7>' + d.TotalLRO, '<FMT LeftMargin=7>' + CASE WHEN ISNULL(d.CAUSE_CODE,'') = '' THEN '' ELSE '1' END , '<FMT LeftMargin=7>' + d.NumUnits, '<FMT LeftMargin=7>' + d.PresDescription from (
		SELECT *--SiteLabel, CurrencyID
		FROM PowerGlobal.dbo.Numbers n
			FULL OUTER JOIN (
			
				SELECT DISTINCT ss.SiteLabel FROM TSort t 
					INNER JOIN StudySites ss on t.SiteID = ss.SiteID
					INNER JOIN Events e ON e.Refnum = t.Refnum 
					INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
					INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
				WHERE --lro.LRO >0 
					e.CAUSE_CODE <> 7777 
					AND t.studyyear = @StudyYear
					AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
					AND e.EVNT_Category in ('F', 'M')
				GROUP BY e.CAUSE_CODE, ss.SiteLabel, PresDescription
				HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) > 1
				) b ON 1=1
		WHERE Num <= 7
		) c
	LEFT JOIN (

		SELECT *
		FROM (

			SELECT CAST(e.CAUSE_CODE AS VARCHAR) AS CAUSE_CODE, 
				CAST(COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) AS VARCHAR) AS NumEvents, 
				TotalLRO = CAST(LEFT(CONVERT(varchar(50), CAST(SUM(ISNULL(lro.LRO,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)) AS money),1), LEN(CONVERT(varchar(50), CAST(SUM(ISNULL(lro.LRO,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)) AS money),1)) -3) AS varchar),
				ss.SiteLabel, 
				CAST(COUNT(DISTINCT t.Refnum) AS VARCHAR) AS NumUnits, 
				PresDescription,
				CAST(CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN (SUM(lro.LRO * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))/SUM(e.LOSTMW*e.DURATION))*1000 END AS VARCHAR) AS TotalDolperLostMWH, 
				RANK() OVER (PARTITION BY ss.SiteLabel ORDER BY COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) DESC, SUM(lro.LRO) DESC, e.CAUSE_CODE) AS Rank
			FROM TSort t 
				INNER JOIN StudySites ss on t.SiteID = ss.SiteID
				INNER JOIN Events e ON e.Refnum = t.Refnum 
				INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
				INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
			WHERE e.CAUSE_CODE <> 7777 
				AND t.studyyear = @StudyYear 
				AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) 
				AND e.EVNT_Category in ('F', 'M')
			GROUP BY e.CAUSE_CODE, ss.SiteLabel, PresDescription
			HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) >1

			) b 
		WHERE Rank <= 7

		) d ON d.SiteLabel = c.SiteLabel AND d.Rank = c.Num

)



