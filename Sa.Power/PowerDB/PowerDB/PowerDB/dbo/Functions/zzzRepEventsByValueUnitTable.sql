﻿



CREATE FUNCTION [dbo].[zzzRepEventsByValueUnitTable]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)
	--@CRVGroup VARCHAR(30) --coal or gas & oil

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT 'Site' AS SiteLabel,
		'<FMT colWidth=106 LeftMargin=7 ColorFont=98002E><b>Cause Code</b>' AS CauseCode,
		'<FMT LeftMargin=7># Events' AS NumEvents,
		'<FMT LeftMargin=7>Total LRO, k ' + RTRIM(@CurrencyCode) AS TotalLRO,
		'<FMT LeftMargin=7># Plants' AS NumSites,
		'<FMT LeftMargin=7># Units' AS NumUnits,
		'' AS PresDescription

	UNION ALL

	SELECT TOP 1000 c.SiteLabel, '<FMT LeftMargin=7 ColorFont=98002E>' + '<b>' + ISNULL(CauseCode,'') + '</b>', '<FMT LeftMargin=7>' + NumEvents, '<FMT LeftMargin=7>' + TotalLRO, '<FMT LeftMargin=7>' + NumSites, '<FMT LeftMargin=7>' + NumUnits, '<FMT LeftMargin=7>' + PresDescription
	FROM (
		SELECT SiteLabel, Num
		FROM PowerGlobal.dbo.Numbers n
			FULL OUTER JOIN (SELECT DISTINCT SiteLabel FROM StudySites 
							WHERE SiteID IN (SELECT SiteID FROM TSort WHERE Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) 
								AND Refnum IN (SELECT Refnum FROM Events WHERE EVNT_Category IN ('F','M'))
								AND Refnum IN (SELECT Refnum FROM EventLRO WHERE LRO > 0)
							AND StudyYear = @StudyYear)
							) b ON 1=1
		WHERE Num <= 7
		) c

		LEFT JOIN (

			SELECT TOP 1000 SiteLabel, 
				CauseCode = CAUSE_CODE, 
				NumEvents, 
				TotalLRO = LEFT(CONVERT(varchar(50), CAST((ISNULL(TotalLRO,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)) AS money),1), LEN(CONVERT(varchar(50), CAST((ISNULL(TotalLRO,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)) AS money),1)) -3),
				NumSites = '1', 
				NumUnits, 
				PresDescription,
				Rank
			FROM (

				SELECT CAST(e.CAUSE_CODE AS VARCHAR) AS CAUSE_CODE, 
					CAST(COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) AS VARCHAR) AS NumEvents, 
					CAST(SUM(lro.LRO) AS VARCHAR) AS TotalLRO, 
					ss.SiteLabel, 
					CAST(COUNT(DISTINCT t.Refnum) AS VARCHAR) AS NumUnits, 
					PresDescription,
					CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN (SUM(lro.LRO)/SUM(e.LOSTMW*e.DURATION))*1000 END AS TotalDolperLostMWH, 
					CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN SUM(lro.LRO)/SUM(e.LOSTMW*e.DURATION) END as SortBy, 
					RANK() OVER (PARTITION BY ss.SiteLabel ORDER BY CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN SUM(lro.LRO)/SUM(e.LOSTMW*e.DURATION) END DESC) AS Rank
				FROM TSort t 
					INNER JOIN StudySites ss on t.SiteID = ss.SiteID
					INNER JOIN Events e ON e.Refnum = t.Refnum 
					INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
					INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
				WHERE lro.LRO > 0
					AND e.CAUSE_CODE <> 7777
					AND t.studyyear = @StudyYear 
					AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) 
					AND e.EVNT_Category in ('F', 'M')
				GROUP BY e.CAUSE_CODE, ss.SiteLabel, PresDescription
				HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) > 1

				) c --b --ON b.Rank = num.Num
				)	d ON d.SiteLabel = c.SiteLabel AND d.Rank = c.Num
	ORDER BY c.SiteLabel ASC, CASE WHEN Rank IS NULL THEN 99999 ELSE Rank END ASC


)

