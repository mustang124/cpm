﻿CREATE FUNCTION PricingHubByRefnum
	(@Refnum Refnum)
RETURNS char(12)
AS
BEGIN
	DECLARE @PricingHub char(12)
	SELECT @PricingHub = PricingHub FROM TSort WHERE Refnum = @Refnum
	RETURN @PricingHub
END
