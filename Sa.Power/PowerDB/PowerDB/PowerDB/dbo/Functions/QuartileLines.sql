﻿

CREATE FUNCTION [dbo].[QuartileLines]
(	
	@ShortListName VARCHAR(30),
	@DataType VARCHAR(30),
	@CRVGroup VARCHAR(30),
	@Filter2 VARCHAR(30) = '',
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT '' AS Num, 
		Q12 = CASE WHEN @DataType IN ('PlantAction','MaintIndex') THEN b.Q12 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE b.Q12 END,
		Q23 = CASE WHEN @DataType IN ('PlantAction','MaintIndex') THEN b.Q23 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE b.Q23 END,
		Q34 = CASE WHEN @DataType IN ('PlantAction','MaintIndex') THEN b.Q34 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE b.Q34 END

	FROM RefList_LU 
		LEFT JOIN (SELECT [Q12],[Q23],[Q34] FROM (
			SELECT Label, Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = @DataType AND Filter1 = @CRVGroup AND (Filter2 = @Filter2 AND @Filter2 <> '') AND Label IN ('Q12','Q23','Q34')) src
				PIVOT (SUM(Value) FOR Label IN ([Q12],[Q23],[Q34])) pvt
				) b on 1 = 1
	WHERE RefListNo <= 12 --because we need 12 rows



)


