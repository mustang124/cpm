﻿

CREATE FUNCTION [dbo].[LROByOutTypeFuelWithHeader]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30) -- Coal or Gas

)
RETURNS TABLE 
AS
RETURN 
(

			SELECT LROType = 'Forced',
					Sum(CommUnavail.LRO_F)/1000 as Forced, 
					0 as Maint,
					0 as Planned
				FROM CommUnavail, TSort t, Breaks b
				WHERE t.Refnum = CommUnavail.Refnum 
					and t.refnum = b.refnum 
					and ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
					and t.studyyear = @StudyYear
					and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)

	union

			SELECT LROType = 'Maintenance (Prescheduled)',
					0 as Forced, 
					Sum(CommUnavail.LRO_M)/1000 as Maint,
					0 as Planned
				FROM CommUnavail, TSort t, Breaks b
				WHERE t.Refnum = CommUnavail.Refnum 
					and t.refnum = b.refnum 
					and ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
					and t.studyyear = @StudyYear
					and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)

	union

			SELECT LROType = 'Planned',
					0 as Forced, 
					0 as Maint,
					Sum(CommUnavail.LRO_P)/1000 as Planned
				FROM CommUnavail, TSort t, Breaks b
				WHERE t.Refnum = CommUnavail.Refnum 
					and t.refnum = b.refnum 
					and ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
					and t.studyyear = @StudyYear
					and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)



)


