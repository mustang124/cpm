﻿

CREATE FUNCTION [dbo].[zzzRepEventsByValue]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30), --coal or gas & oil
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 
		UnitLabel = '', 
		TotalDolperLostMWH = ISNULL(TotalDolperLostMWH,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
	FROM PowerGlobal.dbo.Numbers n
		LEFT JOIN (
			SELECT TOP 7 UnitLabel = '',
				TotalDolperLostMWH = CASE WHEN SUM(LOSTMWH) > 0 THEN (SUM(LRO)/SUM(LOSTMWH))*1000 END,
				Rank = RANK() OVER (ORDER BY CASE WHEN SUM(LOSTMWH) > 0 THEN (SUM(LRO)/SUM(LOSTMWH))*1000 END DESC)
			FROM RepEventsSource (@ListName, @StudyYear)
			WHERE FuelGroup = @FuelGroup
			GROUP BY CAUSE_CODE, PresDescription
			HAVING COUNT(DISTINCT RefEventNum) >1
		) b ON b.Rank = n.Num
	WHERE n.Num <= 7
	ORDER BY ISNULL(TotalDolperLostMWH,0) DESC

)


