﻿





CREATE FUNCTION [dbo].[WaterfallNew]
(	
	@ListName VARCHAR(30),
	@StudyYear INT =2015,
	@ShortListName VARCHAR(30),
	@DataType VARCHAR(10),
	@BySite BIT =1, -- 0 if by unit, 1 if by site
	@CurrencyCode VARCHAR(4),
	@IsPacesetter BIT --if 1 then add " Pacesetters" to the end of the ParentList

)


RETURNS TABLE 
AS
RETURN 
(


--holy cow this works but it is ugly. clean if you ever have the time and patience

	SELECT TOP 1000 
		Title = Title + ' vs ' + CASE WHEN @IsPacesetter = 1 THEN 'Pacesetters' ELSE 'Peer Group' END, 
		Title2, 
		--BeginCell = CAST(ROUND(BeginCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		--Gap1 = CAST(ROUND(Gap1b * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		--Down = CAST(ROUND(Down * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		--Up = CAST(ROUND(Up * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		--EndCell = CAST(ROUND(EndCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money) 
		BeginCell = ROUND(BeginCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),4), 
		Gap1 = ROUND(Gap1b * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),4), 
		Down = ROUND(Down * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),4), 
		Up = ROUND(Up * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),4), 
		EndCell = ROUND(EndCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),4)
	FROM (

		SELECT d.Title, d.Title2, d.BeginCell, d.Gap1, d.Down, d.Up, d.EndCell, d.sort, Gap1b = ISNULL(d.BeginCell2,0) - ISNULL(d.Down2,0) + SUM(ISNULL(u.Up,0))
		FROM (

			SELECT b.Title, b.Title2, b.BeginCell, Gap1 = b.Gap1, b.Down, b.Up, b.EndCell, b.sort, BeginCell2 = topleft.BeginCell, Down2 = SUM(dn.down)
		
			 FROM (

				SELECT Title, Title2, BeginCell = EndCell, Gap1 = NULL, Down = NULL, Up = NULL, EndCell = NULL, sort = 1 
					FROM WaterfallBotNew (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)

				UNION ALL

				SELECT Title, Title2 = Title, BeginCell = NULL, Gap1 = NULL, Down = NULL, Up = NULL, EndCell = BeginCell, sort = 999 
					FROM WaterfallTopNew (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)

				UNION ALL

				SELECT title = wes.unitname, 
					Title2 = wes.title, 
					BeginCell = null, 
					Gap1 = null,
					Down = case when Value >= 0 then ABS(value) else null end, 
					Up = case when Value < 0 then ABS(value) else null end, 
					EndCell = null, 
					Sort = wes.rankorder + 1
				FROM WaterfallEventSummaryNew(@ListName,@StudyYear,@ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite) wes

				) b

			left join (
				SELECT Title, BeginCell = EndCell
				FROM WaterfallBotNew (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)) topleft on topleft.Title = b.Title and b.sort between 2 and 998

			left join (
				SELECT title = unitname, 
				Down = case when Value >= 0 then ABS(value) else null end,
				Sort = rankorder + 1
			FROM WaterfallEventSummaryNew(@ListName,@StudyYear,@ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)) dn 
				on dn.title = b.Title and dn.Down is not null and dn.Sort <= b.sort and b.sort < 999

			group by b.Title, b.Title2, b.BeginCell, b.Gap1, b.Down, b.Up, b.EndCell, b.sort, topleft.BeginCell
			) d

		left join (
						SELECT title = unitname, 
			Up = case when Value < 0 then ABS(value) else null end, 
			Sort = rankorder + 1
		FROM WaterfallEventSummaryNew(@ListName,@StudyYear,@ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)) u 
		on u.title = d.Title and u.Sort < d.sort and d.sort < 999 and u.Up is not null

		group by d.Title, d.Title2, d.BeginCell, d.Gap1, d.Down, d.Up, d.EndCell, d.sort, d.BeginCell2, d.Down2
		) e

	ORDER BY e.title, e.Sort






----------------the following was just a test of the waterfall, making sure the squeezing worked correctly
----------------saved here for posterity, just in case it is needed again some day
----------------select top 100 Title, Title2, BeginCell, Gap1, Down, Up, EndCell from (

----------------SELECT Title = 'Emporia 7FA vs Peer Group', Title2 = 'Peer Group', BeginCell = 74.33, Gap1 = 0.00, Down = NULL, Up = NULL, EndCell = NULL, SortOrder = 1
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Non-OH Site Labor',NULL,74.33,NULL,6.53,NULL, 2
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Non-OH Materials',NULL,80.86,NULL,.26,NULL, 3
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Non-OH Contract Labor',NULL,81.12,NULL,.92,NULL, 4
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','OH & SP Annual ization',NULL,82.04,NULL,9.23,NULL, 5
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Admin & General',NULL,91.27,NULL,4.20,NULL, 6
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Chemicals',NULL,95.47,NULL,.20,NULL, 7
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Water',NULL,94.67,1.20,NULL,NULL, 8
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Taxes & Insurance',NULL,94.67,NULL,.68,NULL, 9
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','All Other',NULL,95.35,NULL,.70,NULL, 10
----------------UNION
----------------SELECT 'Emporia 7FA vs Peer Group','Emporia 7FA',NULL,0.00,NULL,NULL,96.05, 11

----------------) b order by SortOrder











--OLD version, which was horizontal in the presentation (the version above is vertical, so required less data)


--	SELECT TOP 1000 
--		Title, 
--		Title2, 
--		BeginCell = CAST(ROUND(BeginCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
--		Gap1 = CAST(ROUND(Gap1 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
--		--Label = CAST(ROUND(Label * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
--		Down = CAST(ROUND(Down * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
--		Up = CAST(ROUND(Up * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
--		EndCell = CAST(ROUND(EndCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money) 
--	FROM (

--		SELECT Title, Title2, BeginCell, Gap1 = Gap1, Down, Up, EndCell, sort FROM (

--				SELECT Title, Title2 = Title, BeginCell, Gap1 = NULL, Down = NULL, Up = NULL, EndCell = NULL, sort = 1 
--					FROM WaterfallTopNew (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)

--				UNION ALL

--				SELECT Title, Title2, BeginCell = NULL, Gap1 = NULL, Down = NULL, Up = NULL, EndCell, sort = 999 -- ExtraBeginCell = NULL 
--					FROM WaterfallBotNew (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)

--				UNION ALL

--				SELECT title = wes.unitname, 
--					Title2 = wes.title, 
--					BeginCell = null, 
--					Gap1 = ABS(Value), 
--					Down = case when Value < 0 then ABS(value) else null end, 
--					Up = case when Value >= 0 then ABS(value) else null end, 
--					EndCell = null, 
--					Sort = wes.rankorder + 1--, ExtraBeginCell = wtn.BeginCell
--				FROM WaterfallEventSummaryNew(@ListName,@StudyYear,@ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite) wes
--				--	left join (SELECT * FROM Waterfallleftjoin (@ListName,@StudyYear,@ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)) wlj on wlj.title2 = wes.unitname and wlj.title = wes.title
----					left join (SELECT * FROM WaterfallTopNew (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)) wtn on wtn.Title = wes.unitname

--				) b
--		) c

--	ORDER BY title, Sort



)






