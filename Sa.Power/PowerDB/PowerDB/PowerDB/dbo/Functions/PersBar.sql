﻿
CREATE FUNCTION [dbo].[PersBar]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30)
)
RETURNS TABLE 
AS
RETURN 
(

	--Group and Pacesetter values
	SELECT UnitLabel = Label, 
		'Site' = SUM(Site), 
		'Centralized' = SUM(Central),
		'Non-Overhaul Contract' = SUM(ContractNonOH),
		'Overhaul Contract' = SUM(ContractOH),
		'A&G' = SUM(AG) FROM (
		SELECT Label, Site = Value, Central = NULL, ContractNonOH = NULL, ContractOH = NULL, AG = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PersBar' AND Filter1 = @CRVGroup AND Filter2 = 'Site'
		UNION
		SELECT Label, Site = NULL, Central = Value, ContractNonOH = NULL, ContractOH = NULL, AG = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PersBar' AND Filter1 = @CRVGroup AND Filter2 = 'Central'
		UNION
		SELECT Label, Site = NULL, Central = NULL, ContractNonOH = Value, ContractOH = NULL, AG = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PersBar' AND Filter1 = @CRVGroup AND Filter2 = 'ContractNonOH'
		UNION
		SELECT Label, Site = NULL, Central = NULL, ContractNonOH = NULL, ContractOH = Value, AG = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PersBar' AND Filter1 = @CRVGroup AND Filter2 = 'ContractOH'
		UNION
		SELECT Label, Site = NULL, Central = NULL, ContractNonOH = NULL, ContractOH = NULL, AG = Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'PersBar' AND Filter1 = @CRVGroup AND Filter2 = 'AG') b
	GROUP BY Label

	UNION ALL

	-- need to remove Co Avg when there is only one unit
	SELECT UnitLabel, [Site], Centralized, [Non-Overhaul Contract], [Overhaul Contract], [A&G] FROM (
		SELECT UnitLabel = 'Co Avg',
			'Site' = GlobalDB.dbo.WtAvg(o.SiteEffPers*100/nf.NMC, nf.NMC),
			'Centralized' = GlobalDB.dbo.WtAvg(o.CentralEffPers*100/nf.NMC, nf.NMC),
			'Non-Overhaul Contract' = GlobalDB.dbo.WtAvg((isnull(o.ContractEffPers,0) - isnull(p.ContractEffPers,0))*100/nf.NMC,nf.NMC),
			'Overhaul Contract' = GlobalDB.dbo.WtAvg(isnull(p.ContractEffPers,0)*100/nf.NMC,nf.NMC),
			'A&G' = GlobalDB.dbo.WtAvg(o.AGEffPers*100/nf.NMC,nf.NMC),
			COUNT(*) AS cnt
		FROM TSort t
			LEFT JOIN PersSTCalc o ON o.Refnum = t.Refnum and o.SectionID = 'TP'
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
			LEFT JOIN (SELECT Refnum, ContractEffPers = SUM(ContractEffPers) FROM Pers WHERE PersCat IN ('OCC-OHAdj', 'MPS-OHAdj','OCC-LTSA','MPS-LTSA') GROUP BY Refnum) p ON p.Refnum = t.Refnum
		WHERE t.StudyYear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND (b.CRVGroup = @CRVGroup OR b.CoalNMCGroup2 = CASE @CRVGroup WHEN 'C3' THEN 3 WHEN 'C2' THEN 2 WHEN 'C1' THEN 1 ELSE 0 END OR b.CRVSizeGroup = @CRVGroup)
			AND t.Refnum NOT LIKE '%G'
		) b
	WHERE cnt > 1

	UNION ALL

	SELECT UnitLabel, Site, Central, ContractNonOH, ContractOH, AG FROM (
	SELECT TOP 1000 UnitLabel = RTRIM(UnitLabel), --TOP 1000 here so that the ORDER BY will work in a TVF
		Site = o.SiteEffPers*100/nf.NMC,
		Central = o.CentralEffPers*100/nf.NMC,
		ContractNonOH =  (isnull(o.ContractEffPers,0) - isnull(p.ContractEffPers,0))*100/nf.NMC,
		ContractOH = isnull(p.ContractEffPers,0)*100/nf.NMC,
		AG =  o.AGEffPers*100/nf.NMC,
		SortOrder = CASE WHEN t.Refnum LIKE '%G' THEN 1 ELSE 0 END
		--, g.NMC
	FROM TSort t
		LEFT JOIN PersSTCalc o ON o.Refnum = t.Refnum and o.SectionID = 'TP'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, ContractEffPers = SUM(ContractEffPers) FROM Pers WHERE PersCat IN ('OCC-OHAdj', 'MPS-OHAdj','OCC-LTSA','MPS-LTSA') GROUP BY Refnum) p ON p.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND ((b.CRVGroup = @CRVGroup OR b.CoalNMCGroup2 = CASE @CRVGroup WHEN 'C3' THEN 3 WHEN 'C2' THEN 2 WHEN 'C1' THEN 1 ELSE 0 END OR b.CRVSizeGroup = @CRVGroup)
			)--OR t.Refnum LIKE '%G')
	ORDER BY SortOrder, o.TotEffPers/nf.NMC DESC) d

)

