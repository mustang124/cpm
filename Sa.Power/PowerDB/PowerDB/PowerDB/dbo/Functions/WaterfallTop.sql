﻿


CREATE FUNCTION [dbo].[WaterfallTop]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10),
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT Title = RTRIM(CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end), 
		Title2 = RTRIM(CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end), 
		[BeginCell] = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH), 
		Gap1 = null, 
		Label = null, 
		Down = null, 
		Up = null, 
		[EndCell] = null, 
		sort = 1
	FROM TSort t
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN StudySites s on s.SiteID = t.SiteID
		LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
	GROUP BY CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end, CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END

)



