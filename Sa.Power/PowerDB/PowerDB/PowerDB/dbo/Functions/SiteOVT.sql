﻿

CREATE FUNCTION [dbo].[SiteOVT]
(	

	@ListName VARCHAR(30),
	@StudyYear INT,
	@PeerGroup VARCHAR(30), --Coal, Gas & Oil, CC, Cogen
	@ShortListName VARCHAR(30)
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 SiteLabel = RTRIM(sitelabel), MPS = MPSPct, OCC = OCCPct 
	FROM (

		SELECT *
			FROM Currency_LU clu
			LEFT JOIN (


			SELECT Sort = '', SiteLabel = 'Group Avg', MPSPct = SUM (MPSPct), OCCPct = SUM(OCCPct), sortorder = 0 from (
				SELECT
					MPSPct = CASE WHEN SectionID = 'TMPS' THEN SUM(SiteOVTPcnt * SiteSTH)/SUM(SiteSTH) ELSE 0 END, 
					OCCPct = CASE WHEN SectionID = 'TOCC' THEN SUM(SiteOVTPcnt * SiteSTH)/SUM(SiteSTH) ELSE 0 END
				FROM PersSTCalc p
					LEFT JOIN TSort t ON t.Refnum = p.Refnum
					LEFT JOIN StudySites s ON s.SiteID = t.SiteID
					LEFT JOIN Breaks b ON b.Refnum = t.Refnum
				WHERE p.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ShortListName) 
					AND t.StudyYear = @StudyYear
					AND SectionID IN ('TMPS','TOCC')
					AND (
						(@PeerGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
						(@PeerGroup = 'Gas & Oil' AND b.FuelGroup = 'Gas & Oil' AND b.CombinedCycle = 'N') OR 
						(@PeerGroup = 'CC' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec = 'Elec') OR
						(@PeerGroup = 'Cogen' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec = 'Cogen')
						)
				GROUP BY SectionID
				) b

			UNION

			SELECT Sort = SiteLabel, SiteLabel , MPSPct = SUM (MPSPct), OCCPct = SUM(OCCPct), sortorder = RANK() OVER (ORDER BY SUM(MPSPct) DESC, SUM(OCCPct) DESC, SiteLabel) from (
				SELECT s.SiteLabel,
					MPSPct = CASE WHEN SectionID = 'TMPS' THEN SUM(SiteOVTPcnt * SiteSTH)/SUM(SiteSTH) ELSE 0 END, 
					OCCPct = CASE WHEN SectionID = 'TOCC' THEN SUM(SiteOVTPcnt * SiteSTH)/SUM(SiteSTH) ELSE 0 END
				FROM PersSTCalc p
					LEFT JOIN TSort t ON t.Refnum = p.Refnum
					LEFT JOIN StudySites s ON s.SiteID = t.SiteID
					LEFT JOIN Breaks b ON b.Refnum = t.Refnum
				WHERE p.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) 
					AND t.StudyYear = @StudyYear
					AND SectionID IN ('TMPS','TOCC')
					AND (
						(@PeerGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
						(@PeerGroup = 'Gas & Oil' AND b.FuelGroup = 'Gas & Oil' AND b.CombinedCycle = 'N') OR 
						(@PeerGroup = 'CC' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec = 'Elec') OR
						(@PeerGroup = 'Cogen' AND b.FuelGroup = 'Gas & Oil' AND b.CogenElec = 'Cogen')
						)
					GROUP BY SiteLabel, SectionID
				) b GROUP BY SiteLabel 

		) d on clu.CurrencyID = sortorder + 1
	) c

	WHERE CurrencyID <= 12 OR sortorder IS NOT NULL 
	ORDER BY CurrencyID

)


