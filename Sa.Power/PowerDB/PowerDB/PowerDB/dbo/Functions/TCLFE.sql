﻿CREATE FUNCTION [dbo].[TCLFE]
(	
	--this function retuns several TCLFE values for the presentation
	--depending on how UnitOrGroup is set, it will return different groups of values, either units (1) or CRVSizeGroups (0)

	-- ListName is the name of the list, can be a company name (e.g. "Power13: Westar") or any list (e.g. "Power13 Pacesetters")
	-- StudyYear is the year you want to limit it to (because some of the lists have multiple years for trends)
	-- DataType is the type which will be used from OpexCalc (generally will be "MWH" or "KW" or "EGC")
	-- UnitOrGroup is the data that will return, if it is 1 it will return each Unit individually, if 0 it will group them all by CRVSizeGroup)
	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@DataType CHAR(5),
	@UnitOrGroup bit
)
RETURNS TABLE 
AS
RETURN 
(
	
	SELECT UnitLabel = CASE WHEN @UnitOrGroup = 1 THEN RTRIM(UnitLabel) ELSE RTRIM(CRVSizeGroup) END, 
		CRVSizeGroup = RTRIM(CRVSizeGroup), 
		'Plant Manageable' = GlobalDB.dbo.WtAvg(o.ActCashLessFuel, gtc.AdjNetMWH), 
		Taxes = GlobalDB.dbo.WtAvg(o.PropTax + o.OthTax, gtc.AdjNetMWH), 
		Insurance = GlobalDB.dbo.WtAvg(o.Insurance, gtc.AdjNetMWH), 
		'A&G Personnel' = GlobalDB.dbo.WtAvg(o.AGPers, gtc.AdjNetMWH), 
		'A&G Non-Personnel' = GlobalDB.dbo.WtAvg(o.AGNonPers, gtc.AdjNetMWH),
		AdjNetMWH = CASE WHEN @UnitOrGroup = 1 THEN gtc.AdjNetMWH ELSE 0 END
	FROM TSort t
		LEFT JOIN OpexCalc o ON o.Refnum = t.Refnum AND o.DataType = @DataType
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
	GROUP BY CRVSizeGroup, 
		CASE WHEN @UnitOrGroup = 1 THEN RTRIM(UnitLabel) ELSE RTRIM(CRVSizeGroup) END,
		CASE WHEN @UnitOrGroup = 1 THEN gtc.AdjNetMWH ELSE 0 END

)
