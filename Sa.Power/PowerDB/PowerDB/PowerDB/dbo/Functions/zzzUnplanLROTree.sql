﻿

CREATE FUNCTION [dbo].[zzzUnplanLROTree]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30), --"", "Tech", "Coal", "SGO", "Elec","Cogen"
	@Major VARCHAR(30),
	@Minor VARCHAR(30)
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT LRO = SUM(LRO),LostMWh = SUM(LostMWH)
	FROM UnplanLROTreeSource (@ListName, @StudyYear)
	WHERE (
		(@TreeType = '' OR @TreeType = 'Tech') OR
		(@TreeType = 'Coal' AND FuelGroup = 'Coal' AND CombinedCycle = 'N') OR
		(@TreeType = 'SGO' AND FuelGroup = 'Gas & Oil' AND CombinedCycle = 'N') OR
		(@TreeType = 'Elec' AND CombinedCycle = 'Y' AND CogenElec = 'Elec') OR
		(@TreeType = 'Cogen' AND CombinedCycle = 'Y' AND CogenElec = 'Cogen')
		)
		AND (
		(@Major = '') OR 
		(@Major <> '' AND SAIMajorEquip = @Major)
		)
		AND (
		(@Minor = '') OR
		(@Minor <> '' AND SAIMinorEquip = @Minor) OR
		(LEFT(@Minor, 10) = 'Coal Pulve' AND LEFT(SAIMinorEquip,10) = 'Coal Pulve') --Why do this? when I get to the ( in 'Coal Pulverizers/Hammer Mills (CPHM)' it nulls, WHY?
		--if i do the full phrase it nulls, if i substring it works up until that opening paren, then it nulls.
		)

)


