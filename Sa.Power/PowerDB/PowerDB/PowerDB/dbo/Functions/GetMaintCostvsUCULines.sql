﻿

CREATE FUNCTION [dbo].[GetMaintCostvsUCULines]
(	
	@ShortListName VARCHAR(30),
	@Type VARCHAR(30), -- should be MaintCost or Unplan
	@PeerGroup VARCHAR(30),
	@DataType VARCHAR(30), -- begins with Q- or H-, for Quartile or Half, ends with MWH or MW, so e.g. Q-MWH or H-MW
	@ListName VARCHAR(30),
	@CurrencyCode VARCHAR(4),
	@StudyYear INT

)
RETURNS TABLE 
AS
RETURN 
(

	--note the line in the select mc = MAX part looks backwards (type = MaintCost then UCU else MaintIndex; it's because the lines are reversed to show the max
	SELECT TOP 1000 
		[Percent] = CAST(CASE WHEN @Type = 'MaintCost' THEN CAST(Label AS real) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE CASE WHEN CAST(Label AS real) = 0 THEN 0 ELSE CEILING((CASE WHEN mc > halfval THEN mc ELSE halfval END)/10)*10 END END AS real), 
		Value = CASE WHEN @Type = 'MaintCost' THEN case when Value = 0 then value else CEILING((CASE WHEN mc > halfval THEN mc ELSE halfval END)/10)*10 end else value end--,*
	FROM SlideSourceData s
		LEFT JOIN (SELECT mc = MAX(CASE WHEN @Type = 'MaintCost' THEN UnplannedCommUnavail Else MaintIndex END) from MaintCostvsUCU (@ListName,(SELECT DISTINCT StudyYear FROM TSort WHERE refnum IN (SELECT refnum FROM _RL WHERE ListName = @ShortListName)),CASE @PeerGroup WHEN 'GAS' THEN 'SGO' ELSE @PeerGroup END,RIGHT(@DataType,LEN(@DataType)-2),@CurrencyCode)) m on 1=1
		LEFT JOIN (SELECT halfval = MAX(CASE WHEN @Type = 'MaintCost' THEN Value ELSE Label END) FROM SlideSourceData s WHERE Source = @ShortListName AND Type = CASE WHEN @Type = 'MaintCost' THEN 'Unplan' ELSE 'MaintCost' END AND Filter1 = @PeerGroup AND Filter2 = REPLACE (@DataType,'Q-','H-')) maxhalf ON 1=1
	WHERE Source = @ShortListName
		AND Type = @Type
		AND Filter1 = @PeerGroup
		AND Filter2 = @DataType

)


