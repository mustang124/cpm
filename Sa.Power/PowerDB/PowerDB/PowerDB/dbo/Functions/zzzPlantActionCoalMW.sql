﻿



CREATE FUNCTION [dbo].[zzzPlantActionCoalMW]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CoalNMCGroup VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT UnitLabel = 'Group Avg',
		'Site Wages & Benefits' = GlobalDB.dbo.WtAvg(o.SiteWagesBen, g.AdjNetMWH),
		'Contract & Centralized Employees' = GlobalDB.dbo.WtAvg(o.CentralContract, g.AdjNetMWH),
		'Materials' = GlobalDB.dbo.WtAvg(o.MaintMatl, g.AdjNetMWH),
		'Overhauls & Projects' = GlobalDB.dbo.WtAvg(o.OverhaulAdj, g.AdjNetMWH),
		'Other Variable' = GlobalDB.dbo.WtAvg(o.OthVar, g.AdjNetMWH),
		'Miscellaneous' = GlobalDB.dbo.WtAvg(o.Misc, g.AdjNetMWH)
	FROM TSort t
		LEFT JOIN PlantActionExp o ON o.Refnum = t.Refnum and o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenSum g ON g.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

		SELECT UnitLabel = 'Pace- setter',
		'Site Wages & Benefits' = GlobalDB.dbo.WtAvg(o.SiteWagesBen, g.AdjNetMWH),
		'Contract & Centralized Employees' = GlobalDB.dbo.WtAvg(o.CentralContract, g.AdjNetMWH),
		'Materials' = GlobalDB.dbo.WtAvg(o.MaintMatl, g.AdjNetMWH),
		'Overhauls & Projects' = GlobalDB.dbo.WtAvg(o.OverhaulAdj, g.AdjNetMWH),
		'Other Variable' = GlobalDB.dbo.WtAvg(o.OthVar, g.AdjNetMWH),
		'Miscellaneous' = GlobalDB.dbo.WtAvg(o.Misc, g.AdjNetMWH)
	FROM TSort t
		LEFT JOIN PlantActionExp o ON o.Refnum = t.Refnum and o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenSum g ON g.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13 Pacesetters 2')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT UnitLabel = 'Co Avg',
		'Site Wages & Benefits' = GlobalDB.dbo.WtAvg(o.SiteWagesBen, g.AdjNetMWH),
		'Contract & Centralized Employees' = GlobalDB.dbo.WtAvg(o.CentralContract, g.AdjNetMWH),
		'Materials' = GlobalDB.dbo.WtAvg(o.MaintMatl, g.AdjNetMWH),
		'Overhauls & Projects' = GlobalDB.dbo.WtAvg(o.OverhaulAdj, g.AdjNetMWH),
		'Other Variable' = GlobalDB.dbo.WtAvg(o.OthVar, g.AdjNetMWH),
		'Miscellaneous' = GlobalDB.dbo.WtAvg(o.Misc, g.AdjNetMWH)
	FROM TSort t
		LEFT JOIN PlantActionExp o ON o.Refnum = t.Refnum and o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenSum g ON g.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT TOP 1000 UnitLabel, --TOP 1000 here so that the ORDER BY will work in a TVF
		'Site Wages & Benefits' = o.SiteWagesBen,
		'Contract & Centralized Employees' = o.CentralContract,
		'Materials' = o.MaintMatl,
		'Overhauls & Projects' = o.OverhaulAdj,
		'Other Variable' = o.OthVar,
		'Miscellaneous' = o.Misc
	FROM TSort t
		LEFT JOIN PlantActionExp o ON o.Refnum = t.Refnum and o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenSum g ON g.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup
	ORDER BY o.ActCashLessFuel DESC

)




