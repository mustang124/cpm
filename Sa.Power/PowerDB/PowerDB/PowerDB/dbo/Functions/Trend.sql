﻿


CREATE FUNCTION [dbo].[Trend]
(	
	@ListName VARCHAR(30),
	@PeerGroup VARCHAR(30),
	@RankVariableID VARCHAR(30),
	@ShortListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4),
	@Metric BIT


)
RETURNS TABLE 
AS
RETURN 
(

	SELECT line, cat, value = CASE WHEN @RankVariableID = 1 AND @Metric = 1 THEN GlobalDB.dbo.UnitsConv(value,'BTU','MJ') ELSE value END
	FROM (
		--get the first quartile line
		select line, cat, value FROM (
		SELECT top 100 line = 'Q1', cat = [Year], value = q12 * CASE WHEN @RankVariableID IN (21, 86, 6) THEN GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE 1 END
		FROM (SELECT * FROM SlideQuartiles WHERE ListName = @ShortListName AND PeerGroup = @PeerGroup AND RankVariableID = @RankVariableID) b order by cat
		) bb

		UNION ALL
		--get the fourth quartile line
		select line, cat, value FROM (
		SELECT top 100 line = 'Q4', cat = [Year], value = q34 * CASE WHEN @RankVariableID IN (21, 86, 6) THEN GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE 1 END
		FROM (SELECT * FROM SlideQuartiles WHERE ListName = @ShortListName AND PeerGroup = @PeerGroup AND RankVariableID = @RankVariableID) c order by cat
		) cc

		UNION ALL

		--unit data broken into three parts:
		--1. get the year (to make sure all fields are filled)
		--need to fix the date part
		SELECT TOP 1000 b.SiteLabel, a.StudyYear, c.value * CASE WHEN @RankVariableID IN (21, 86, 6) THEN GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE 1 END
		FROM (SELECT DISTINCT t.studyyear FROM TSort t WHERE t.StudyYear BETWEEN 
			--2008 
			(SELECT DISTINCT StudyYear-5 FROM TSort where refnum IN (SELECT refnum FROM _RL where listname = @ShortListName))
			AND 
			--2013 
			(SELECT DISTINCT StudyYear FROM TSort where refnum IN (SELECT refnum FROM _RL where listname = @ShortListName))
	
		) a

		--2. get the units (to make sure all fields are filled)
		LEFT JOIN (
			SELECT SiteLabel =RTRIM(s.SiteLabel)
			FROM NERCFactors c 
				LEFT JOIN TSort t ON t.Refnum = c.Refnum 
				LEFT JOIN StudySites s ON s.SiteID = t.SiteID 
				LEFT JOIN Breaks b ON b.Refnum = t.Refnum 
			WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear >= (SELECT DISTINCT StudyYear-5 FROM TSort where refnum IN (SELECT refnum FROM _RL where listname = @ShortListName))
				AND (
					(@PeerGroup = 'Coal' AND b.FuelGroup = 'Coal') OR 
					(@PeerGroup = 'Gas & Oil' AND b.FuelGroup <> 'Coal' AND b.CombinedCycle = 'N') OR
					(@PeerGroup = 'CE' AND b.FuelGroup <> 'Coal' AND b.CombinedCycle = 'Y' AND b.CogenElec = 'Elec') OR 
					(@PeerGroup = 'CG' AND b.FuelGroup <> 'Coal' AND b.CombinedCycle = 'Y' AND b.CogenElec <> 'Elec')
					)
			GROUP BY s.SiteLabel ) b on 1 = 1

		--3. get the actual data
		LEFT JOIN (
			SELECT SiteLabel = RTRIM(s.SiteLabel), t.studyyear, 
				value = CASE @RankVariableID
					WHEN 1 THEN SUM(CASE WHEN gs.HeatRate*gs.AdjNetMWH <> 0 THEN gs.HeatRate*gs.AdjNetMWH END)/SUM(CASE WHEN gs.HeatRate*gs.AdjNetMWH <> 0 THEN gs.AdjNetMWH END) -- 1 = HeatRate
					WHEN 6 THEN CASE WHEN SUM(gtc.AdjNetMWH2Yr) <> 0 THEN SUM(ISNULL(mtc.AnnMaintCostMWH*gtc.AdjNetMWH2Yr,0))/SUM(gtc.AdjNetMWH2Yr) END -- 6 = MaintIndex MWH
					WHEN 12 THEN MAX(gs.OSHARate) -- 12 = OSHARate
					WHEN 17 THEN CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(gs.TotEffPersMW*nf.NMC,0))/SUM(nf.NMC) END -- 17 = TEP 100MW
					WHEN 21 THEN CASE WHEN SUM(gtc.AdjNetMWH) <> 0 THEN SUM(ISNULL(omwh.ActCashLessFuel*gtc.AdjNetMWH,0))/SUM(gtc.AdjNetMWH) END -- 21 = PlantMan MWH
					WHEN 23 THEN CASE WHEN SUM(nf.EFORWtFactor) <> 0 THEN SUM(ISNULL(nf.EFOR * nf.EFORWtFactor,0)) / SUM(nf.EFORWtFactor) END -- 23 = EFOR
					WHEN 24 THEN CASE WHEN SUM(nf.WPH) <> 0 THEN SUM(ISNULL(nf.EUF*nf.WPH,0))/SUM(nf.WPH) END -- 24 = EUF
					WHEN 46 THEN CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Unp,0))/SUM(cu.TotPeakMWH) END*100 -- 46 = Unplanned CU %
					WHEN 71 THEN CASE WHEN SUM(gtc.EGC) <> 0 THEN SUM(ISNULL(oadj.TotCashLessFuelEm,0))/SUM(gtc.EGC) END -- 71 = TCLFE EGC
					WHEN 72 THEN CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(gs.TotEffPersMW*nf.NMC,0))/SUM(nf.NMC) END -- 72 = TEP 100MW
					WHEN 74 THEN CASE WHEN SUM(gtc.EGC) <> 0 THEN SUM(ISNULL(oegc.ActCashLessFuel*gtc.EGC,0))/SUM(gtc.EGC) END -- 74 = PlantMan EGC
					WHEN 75 THEN CASE WHEN SUM(gtc.EGC) <> 0 THEN SUM(ISNULL(mtc.AnnMaintCostEGC*gtc.EGC,0))/SUM(gtc.EGC) END -- 75 = MaintIndex EGC
					WHEN 78 THEN SUM(CASE WHEN cc.ThermEff*cc.TotInputMBTU <> 0 THEN cc.ThermEff*cc.TotInputMBTU END)/SUM(CASE WHEN cc.ThermEff*cc.TotInputMBTU <> 0 THEN cc.TotInputMBTU END) -- 78 = ThermEff
					WHEN 86 THEN CASE WHEN SUM(gtc.AdjNetMWH) <> 0 THEN SUM(ISNULL(omwh.TotCashLessFuelEm*gtc.AdjNetMWH,0))/SUM(gtc.AdjNetMWH) END -- 86 = TCLFE MWH
					WHEN 92 THEN CASE WHEN SUM(nf.WPH) <> 0 THEN SUM(ISNULL(nf.EUOF * nf.WPH,0)) / SUM(nf.WPH) END -- 92 = EUOF

					END
			FROM NERCFactors nf 
				LEFT JOIN TSort t ON t.Refnum = nf.Refnum 
				LEFT JOIN StudySites s ON s.SiteID = t.SiteID 
				LEFT JOIN Breaks b ON b.Refnum = t.Refnum 
				LEFT JOIN GenSum gs ON gs.Refnum = t.Refnum
				LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN OpExCalc omwh ON omwh.Refnum = t.Refnum AND omwh.DataType = 'MWH'
				LEFT JOIN OpExCalc oadj ON oadj.Refnum = t.Refnum AND oadj.DataType = 'ADJ'
				LEFT JOIN OpExCalc oegc ON oegc.Refnum = t.Refnum AND oegc.DataType = 'EGC'
				LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
				LEFT JOIN CommUnavail cu ON cu.Refnum = t.Refnum
				LEFT JOIN CogenCalc cc ON cc.Refnum = t.Refnum
			WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear >= (SELECT DISTINCT StudyYear-5 FROM TSort where refnum IN (SELECT refnum FROM _RL where listname = @ShortListName))
				AND (
				(@PeerGroup = 'Coal' AND b.FuelGroup = 'Coal') OR 
				(@PeerGroup = 'Gas & Oil' AND b.FuelGroup <> 'Coal' AND b.CombinedCycle = 'N') OR
				(@PeerGroup = 'CE' AND b.FuelGroup <> 'Coal' AND b.CombinedCycle = 'Y' AND b.CogenElec = 'Elec') OR 
				(@PeerGroup = 'CG' AND b.FuelGroup <> 'Coal' AND b.CombinedCycle = 'Y' AND b.CogenElec <> 'Elec')
				)
			GROUP BY s.SiteLabel , t.StudyYear, b.FuelGroup, b.CombinedCycle, b.CogenElec) c on b.SiteLabel = c.SiteLabel and c.StudyYear = a.StudyYear

		ORDER BY b.SiteLabel, a.StudyYear
	) b
)

