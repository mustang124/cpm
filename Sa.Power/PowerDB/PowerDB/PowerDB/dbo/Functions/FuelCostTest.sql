﻿


CREATE FUNCTION [dbo].[FuelCostTest]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30),
	@Location VARCHAR(30),
	@CurrencyCode VARCHAR(4),
	@Metric BIT
	--@PeerGroup1 VARCHAR(50),
	--@PeerGroup2 VARCHAR(50),
	--@PeerGroup3 VARCHAR(50)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT Label = 'test1', UnitLabel, FuelCost = CASE @Metric WHEN 1 THEN GlobalDB.dbo.UnitsConv(FuelCost,'MBTU','GJ') ELSE FuelCost END
	FROM (
		SELECT t.UnitLabel, FuelCost = (CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
			LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
		WHERE t.studyyear = @StudyYear 
			AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND (
				(@Location = 'N America' AND t.Continent = 'N America') OR
				(@Location = 'Europe' AND t.Continent = 'Europe') OR
				(@Location = 'World' AND t.Continent NOT IN ('N America','Europe'))
				)
			AND (
				(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
				(@FuelGroup <> 'Coal' AND f.FuelType IN ('Gas','Oil'))
				)
	) b

union

	SELECT Label = 'test2', UnitLabel, FuelCost = CASE @Metric WHEN 1 THEN GlobalDB.dbo.UnitsConv(FuelCost,'MBTU','GJ') ELSE FuelCost END
	FROM (
		SELECT t.UnitLabel, FuelCost = (CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
			LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
		WHERE t.studyyear = @StudyYear 
			AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND (
				(@Location = 'N America' AND t.Continent = 'N America') OR
				(@Location = 'Europe' AND t.Continent = 'Europe') OR
				(@Location = 'World' AND t.Continent NOT IN ('N America','Europe'))
				)
			AND (
				(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
				(@FuelGroup <> 'Coal' AND f.FuelType IN ('Gas','Oil'))
				)
	) b


--union
--select Label = 'Test3', 'T3',FuelCost = null





)

