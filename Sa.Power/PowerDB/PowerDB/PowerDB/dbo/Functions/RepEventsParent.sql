﻿



CREATE FUNCTION [dbo].[RepEventsParent]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ByUnit BIT, -- all units (0) or each unit individually (1)
	@Filter VARCHAR(10), -- Coal, Gas & Oil, or ''
	@ResultType INT, -- total LRO (0) or LRO/MWh (1) or Count (2)
	@RepetitiveEvents BIT -- all events (0) or only repetitive (1) (repetitive is defined as >3 of the same cause code)

)
RETURNS TABLE 
AS
RETURN 
(

	-- although the following code looks complicated, it is relatively simple
	-- it is making groups of numbers depending on the parameters passed to it
	-- we use the numbers table just as a sorting and grouping mechanism
	-- the first join pulls out the appropriate units, by calcing if they should belong within the output
	-- the second join sums the data, and passes it back
	-- and the where makes sure that we're only passing back 7 records to the Bar7 charts

	SELECT TOP 1000 * FROM PowerGlobal.dbo.Numbers n
		LEFT JOIN (
			SELECT DISTINCT SiteLabel2 = CASE WHEN @ByUnit = 0 THEN '' ELSE SiteLabel END
			FROM (SELECT SiteLabel = CASE WHEN @ByUnit = 0 THEN '' ELSE SiteLabel END
				FROM RepEventsSource (@ListName, @StudyYear) 
				WHERE FuelGroup = CASE WHEN @Filter = '' THEN FuelGroup ELSE @Filter END
					AND LRO > @RepetitiveEvents * -1 -- * -1 because we get a 0 or 1, and LRO needs to be > 0 if repetitive events = 0 or >= 0 if repevents = 1
				GROUP BY CASE WHEN @ByUnit = 0 THEN '' ELSE SiteLabel END, Cause_Code 
				HAVING COUNT(DISTINCT refeventnum) > CASE WHEN @RepetitiveEvents = 0 THEN 0 ELSE 3 END) e
			) c ON 1=1
		LEFT JOIN (
			SELECT SiteLabel = CASE WHEN @ByUnit = 0 THEN '' ELSE SiteLabel END, 
				Cause_Code, 
				NumEvents = COUNT(DISTINCT RefEventNum), 
				LRO = SUM(LRO), 
				NumPlants = COUNT(DISTINCT SiteLabel), 
				NumUnits = COUNT(DISTINCT UnitLabel), 
				PresDescription,
				LROMWh = CASE WHEN SUM(LOSTMWH) = 0 THEN 0 ELSE SUM(LRO)/SUM(LOSTMWH)*1000 END,
				Rank = ROW_NUMBER() OVER (PARTITION BY CASE WHEN @ByUnit = 0 THEN '' ELSE SiteLabel END ORDER BY CASE WHEN @ResultType = 2 THEN COUNT(DISTINCT RefEventNum) WHEN @ResultType = 1 THEN CASE WHEN SUM(LOSTMWH) = 0 THEN 0 ELSE SUM(LRO)/SUM(LOSTMWH)*1000 END ELSE SUM(LRO) END DESC)
			FROM RepEventsSource (@ListName, @StudyYear) 
			WHERE FuelGroup = CASE WHEN @Filter = '' THEN FuelGroup ELSE @Filter END
				AND LRO > @RepetitiveEvents * -1
			GROUP BY CASE WHEN @ByUnit = 0 THEN '' ELSE SiteLabel END, Cause_Code, PresDescription
			HAVING COUNT(DISTINCT RefEventNum) > CASE WHEN @RepetitiveEvents = 0 THEN 0 ELSE 3 END
			) b ON n.Num = b.Rank AND b.SiteLabel = c.SiteLabel2
	WHERE Num <= 7
	ORDER BY SiteLabel2, Num, Rank

)


