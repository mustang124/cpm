﻿



CREATE FUNCTION [dbo].[zzzMaintCostCoalMW]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CoalNMCGroup VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT UnitLabel = 'Group Avg',
		'Non-Overhaul' = GlobalDB.dbo.WtAvg(o.AnnNonOHCostMW, nf.NMC2Yr),
		'Overhaul & Special Projects <15 yrs' = GlobalDB.dbo.WtAvg(o.AnnOHCostMW - o.AnnOHCostOver15MW, nf.NMC2Yr),
		'Special Projects >= 15 yrs' = GlobalDB.dbo.WtAvg(o.annOHCostover15MW, nf.NMC2Yr)
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

		SELECT UnitLabel = 'Pace- setter',
		'Non-Overhaul' = GlobalDB.dbo.WtAvg(o.AnnNonOHCostMW, nf.NMC2Yr),
		'Overhaul & Special Projects <15 yrs' = GlobalDB.dbo.WtAvg(o.AnnOHCostMW - o.AnnOHCostOver15MW, nf.NMC2Yr),
		'Special Projects >= 15 yrs' = GlobalDB.dbo.WtAvg(o.annOHCostover15MW, nf.NMC2Yr)
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13 Pacesetters 2')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT UnitLabel = 'Co Avg',
		'Non-Overhaul' = GlobalDB.dbo.WtAvg(o.AnnNonOHCostMW, nf.NMC2Yr),
		'Overhaul & Special Projects <15 yrs' = GlobalDB.dbo.WtAvg(o.AnnOHCostMW - o.AnnOHCostOver15MW, nf.NMC2Yr),
		'Special Projects >= 15 yrs' = GlobalDB.dbo.WtAvg(o.annOHCostover15MW, nf.NMC2Yr)
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT TOP 1000 UnitLabel, --TOP 1000 here so that the ORDER BY will work in a TVF
		'Non-Overhaul' = o.AnnNonOHCostMW,
		'Overhaul & Special Projects <15 yrs' = o.AnnOHCostMW - o.AnnOHCostOver15MW,
		'Special Projects >= 15 yrs' = o.annOHCostover15MW
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup
	ORDER BY o.AnnMaintCostMW DESC


)




