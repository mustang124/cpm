﻿CREATE FUNCTION [dbo].[Quartiles]
(	

	-- this function returns a table of the average of the highest Q1 value and lowest Q2 value, and highest Q2/lowest Q3, and highest Q3/lowest Q4
	-- you end up with the break points for each of the CRVSizeGroups in the variable and rank list you chose
	@ListName VARCHAR(30),
	@StudyYear INT,
	@BreakCondition VARCHAR(30),
	@RankVariableID SMALLINT
)
RETURNS TABLE 
AS
RETURN 
(

	--get pivoted totals
	SELECT CRVSizeGroup = BreakValue, Q12 = [1],Q23 = [2],Q34 = [3] FROM (
		SELECT BreakValue, Tile, Value = AVG(Value) FROM (
			--first part of union gets the highest value from quartiles 1-3
			SELECT Value = MAX(CASE WHEN Tile >= NumTiles THEN NULL ELSE Value END), Tile, NumTiles, BreakValue
				FROM _RankView 
				WHERE RankVariableID = @RankVariableID
					AND ListName = @ListName
					AND BreakCondition = @BreakCondition
					AND Tile < 4
				GROUP BY Tile, NumTiles, BreakValue
			UNION
			--second part of union gets lowest value from quartiles 2-4
				SELECT MIN(Value), Tile-1, NumTiles, BreakValue --we reduce the Tile by 1 so the AVG will work on the correct values
				FROM _RankView 
				WHERE RankVariableID = @RankVariableID
					AND ListName = @ListName
					AND BreakCondition = @BreakCondition
					AND Tile > 1
				GROUP BY Variable, Tile, NumTiles, BreakValue) b
		GROUP BY BreakValue, Tile) src
	PIVOT (MAX(Value) FOR Tile IN ([1],[2],[3])) pvt

)
