﻿

CREATE FUNCTION [dbo].[zzzRepEventsByCountUnit]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)
	--@CRVGroup VARCHAR(30) --coal or gas & oil

)
RETURNS TABLE 
AS
RETURN 
(


	--Using CurrencyID just to get a number from 1 to 7
	SELECT TOP 1000 c.SiteLabel, TotalDolperLostMWH = TotalDolperLostMWH * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) FROM (
		SELECT SiteLabel, Num
		FROM PowerGlobal.dbo.Numbers n
			FULL OUTER JOIN (
			
				SELECT DISTINCT ss.SiteLabel FROM TSort t 
					INNER JOIN StudySites ss on t.SiteID = ss.SiteID
					INNER JOIN Events e ON e.Refnum = t.Refnum 
					INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
					INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
				WHERE e.CAUSE_CODE <> 7777 
					AND t.studyyear = @StudyYear
					AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
					AND e.EVNT_Category in ('F', 'M')
				GROUP BY e.CAUSE_CODE, ss.SiteLabel, PresDescription
				HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) > 1
				) b ON 1=1
		WHERE Num <= 7
		) c
	LEFT JOIN (



		SELECT TOP 1000 SiteLabel, TotalDolperLostMWH, NumEvents, Rank
		FROM (


		SELECT e.CAUSE_CODE, 
			COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) AS NumEvents, 
			ss.SiteLabel,
			CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN (SUM(lro.LRO)/SUM(e.LOSTMW*e.DURATION))*1000 END AS TotalDolperLostMWH,
			RANK() OVER (PARTITION BY ss.SiteLabel ORDER BY COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) DESC, SUM(lro.LRO) DESC, e.CAUSE_CODE) AS Rank
		FROM TSort t
			INNER JOIN StudySites ss on t.SiteID = ss.SiteID
			INNER JOIN Breaks b ON b.Refnum = t.Refnum
			INNER JOIN Events e ON e.Refnum = t.Refnum 
			INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
			INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
		WHERE e.CAUSE_CODE <> 7777 
			AND t.studyyear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) 
			AND e.EVNT_Category in ('F', 'M')
		GROUP BY e.CAUSE_CODE, ss.SiteLabel, PresDescription
		HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) > 1

			) b 
		WHERE Rank <= 7

	) d ON d.SiteLabel = c.SiteLabel AND d.Rank = c.Num
	ORDER BY c.SiteLabel ASC, NumEvents DESC



)


