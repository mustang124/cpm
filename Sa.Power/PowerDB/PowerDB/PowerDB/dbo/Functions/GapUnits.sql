﻿



CREATE FUNCTION [dbo].[GapUnits]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT TOP 1000 UnitLabel, RevenueGapKUS, FuelOperGapKUS, OpexGapKUS

	FROM (

				--SW 9/26/16
				--this is the original top part, which grabbed all units and put them in the output
				--below is the new top part, which grabs all units where there is more than 1 unit at a site and puts them in the output (because otherwise you are just repeating the data)
				--SELECT 
				--	SiteLabel = '<FMT colWidth=72 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(ss.sitelabel) + '</b>',
				--	UnitLabel = '<FMT colWidth=72 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(UnitLabel) + '</b>', 
				--	RevenueGapKUS = '<FMT RightMargin=15 Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(RevenueGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))),
				--	FuelOperGapKUS = '<FMT RightMargin=15 Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(FuelOperGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))),
				--	OpexGapKUS = '<FMT RightMargin=15 Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(OpexEmmGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)))
				--	,sortorder = 1
				--FROM CRVGap, Tsort t
				--INNER JOIN StudySites ss on t.SiteID = ss.SiteID
				--where CRVGap.refnum = t.refnum
				--and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) and t.studyyear = @StudyYear
				--GROUP BY ss.SiteLabel, UnitLabel

		SELECT 
			SiteLabel = '<FMT colWidth=72 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(ss.sitelabel) + '</b>',
			UnitLabel = '<FMT colWidth=72 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(UnitLabel) + '</b>', 
			RevenueGapKUS = '<FMT RightMargin=15 Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(RevenueGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))),
			FuelOperGapKUS = '<FMT RightMargin=15 Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(FuelOperGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))),
			OpexGapKUS = '<FMT RightMargin=15 Align=Center>' + Power.dbo.GetDollarsText(SUM(ISNULL(OpexEmmGapKUS,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)))
			,sortorder = 1
		FROM CRVGap, Tsort t
		INNER JOIN StudySites ss on t.SiteID = ss.SiteID
		LEFT JOIN (--added in this part to count the units at a site
			SELECT Sitel, COUNT(*) AS UnitCount FROM (
				SELECT SiteL = '<FMT colWidth=72 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(ss.sitelabel) + '</b>'
				FROM CRVGap, Tsort t
				INNER JOIN StudySites ss on t.SiteID = ss.SiteID
				WHERE CRVGap.refnum = t.refnum
					AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) and t.studyyear = @StudyYear
				GROUP BY ss.SiteLabel, UnitLabel
			) b GROUP BY SiteL
		) c ON c.SiteL = '<FMT colWidth=72 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(ss.sitelabel) + '</b>'
		WHERE CRVGap.refnum = t.refnum
			AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) and t.studyyear = @StudyYear
			AND UnitCount > 1
		GROUP BY ss.SiteLabel, UnitLabel, UnitCount

	union
	--then we add in the sites for the totals
	select SiteLabel, *,2 from GapTable(@ListName,@StudyYear,'MWH', @CurrencyCode)
	) b

	order by SiteLabel, sortorder, UnitLabel

)




