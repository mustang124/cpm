﻿


CREATE FUNCTION [dbo].[zzzWaterfall2]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10),
	@BySite BIT, -- 0 if by unit, 1 if by site
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 
		Title, 
		Title2, 
		BeginCell = CAST(ROUND(BeginCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		--Gap1 = CAST(ROUND(Gap1 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		Label = CAST(ROUND(Label * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		Down = CAST(ROUND(Down * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		Up = CAST(ROUND(Up * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		EndCell = CAST(ROUND(EndCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money) 
	FROM (
	SELECT Title, Title2, BeginCell, Label, Down, Up, EndCell, sort FROM WaterfallTop (@ListName, @StudyYear, @ParentList, @DataType, @BySite)
	union

	select b.Title, b.Title2, b.BeginCell, Label = SUM(isnull(c.Label,0) + isnull(c.BeginCell,0)) - ISNULL(b.down,0), b.Down, b.Up, b.EndCell, b.sort from (
	SELECT * FROM WaterfallTop (@ListName, @StudyYear, @ParentList, @DataType, @BySite)
	union
	select title = unitname, Title2 = title, BeginCell = null, Gap1 = null, Label = Value, down = case when Value < 0 then ABS(value) else null end,
	Up = case when Value >= 0 then ABS(value) else null end, EndCell = null, sort = Rank
	 from WaterfallEventSummary2(@ListName, @StudyYear, @ParentList, @DataType, @BySite)
	--order by unitname, Rank
	) b
	inner join (SELECT * FROM WaterfallTop (@ListName, @StudyYear, @ParentList, @DataType, @BySite)
	union
	select title = unitname, Title2 = title, BeginCell = null, Gap1 = null, Label = Value, down = case when Value < 0 then ABS(value) else null end,
	Up = case when Value >= 0 then ABS(value) else null end, EndCell = null, sort = Rank
	 from WaterfallEventSummary2(@ListName, @StudyYear, @ParentList, @DataType, @BySite)
	--order by unitname, Rank
	) c on c.Title = b.Title and c.sort < b.sort
	--order by Title, sort
	group by b.Title, b.Title2, b.BeginCell, b.Down, b.Up, b.EndCell, b.sort
	union 
	SELECT Title, Title2, BeginCell, Label, Down, Up, EndCell, sort = 999 FROM WaterfallBot (@ListName, @StudyYear, @ParentList, @DataType, @BySite)
	) d
	order by Title, sort





	--SELECT TOP 1000 
	--	Title, 
	--	Title2, 
	--	BeginCell = CAST(ROUND(BeginCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
	--	Gap1 = CAST(ROUND(Gap1 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
	--	--Label = CAST(ROUND(Label * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
	--	Down = CAST(ROUND(Down * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
	--	Up = CAST(ROUND(Up * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
	--	EndCell = CAST(ROUND(EndCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money) 
	--FROM (
	--	SELECT * FROM WaterfallTop (@ListName, @StudyYear, @ParentList, @DataType, @BySite)

	--	UNION ALL

	--	SELECT Title, Title2, BeginCell, Gap1, Label, Down, Up, EndCell, sort = 999 FROM WaterfallBot (@ListName, @StudyYear, @ParentList, @DataType, @BySite)

	--	UNION ALL

	--	SELECT title = wes.unitname, Title2 = wes.title, null, wes.Value, ABS(Value),case when Value < 0 then ABS(value) else null end,case when Value >= 0 then ABS(value) else null end, null, wes.rank + 1 
	--		from WaterfallEventSummary2(@ListName,@StudyYear,@ParentList, @DataType, @BySite) wes
	--		--left join (SELECT * FROM Waterfallleftjoin (@ListName,@StudyYear,@ParentList, @DataType, @BySite)) wlj on wlj.title2 = wes.unitname and wlj.title = wes.title
	--	) b
	--ORDER BY title, Sort



)



