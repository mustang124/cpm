﻿
CREATE FUNCTION [dbo].[zzzUnplanLROByCauseCode]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30), --coal or gas & oil
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT UnitName = '', TotalLRO = ROUND(((TotalLRO) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)),0)
	FROM PowerGlobal.dbo.Numbers n
		LEFT JOIN (
			SELECT CAUSE_CODE, TotalLRO = SUM(LRO), Rank = RANK() OVER (ORDER BY SUM(LRO) DESC) 
			FROM UnplanLRO
			WHERE Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
				AND FuelGroup = @CRVGroup 
			GROUP BY CAUSE_CODE
			) u on u.Rank = n.Num
	WHERE n.Num <= 7



	--SELECT UnitName = '', 
	--	TotalLRO = CAST(REPLACE(REPLACE(TotalLRO,',',''),'<FMT LeftMargin=7>','') as float)
	--FROM UnplanLROByCauseCodeTable(@ListName, @StudyYear, @CRVGroup, @CurrencyCode)
	--WHERE CauseCode not like '%Cause Code%'

)

