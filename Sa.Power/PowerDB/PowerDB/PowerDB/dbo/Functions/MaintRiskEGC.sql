﻿


CREATE FUNCTION [dbo].[MaintRiskEGC]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT t.UnitLabel, nf.EUOF2Yr, mtc.AnnNonOHCostEGC
	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		AND (b.CRVSizeGroup = @CRVGroup OR t.Refnum LIKE '%G')

	UNION

	SELECT 'Peer', GlobalDB.dbo.WtAvgNZ(nf.EUOF2Yr, nf.WPH2Yr), GlobalDB.dbo.WtAvg( mtc.AnnNonOHCostEGC, gtc.EGC)
	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ShortListName) AND t.StudyYear = @StudyYear
		AND b.CRVSizeGroup = @CRVGroup

	UNION

	SELECT 'PS', GlobalDB.dbo.WtAvgNZ(nf.EUOF2Yr, nf.WPH2Yr), GlobalDB.dbo.WtAvg( mtc.AnnNonOHCostEGC, gtc.EGC)
	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = (@ShortListName + ' Pacesetters')) AND t.StudyYear = @StudyYear
		AND b.CRVSizeGroup = @CRVGroup
)



