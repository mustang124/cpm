﻿

CREATE FUNCTION [dbo].[LROByTech]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@AllLRO BIT, -- if 1 then function returns LRO_Tot, else it returns Unplanned LRO
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 -- TOP 1000 here so we can sort it the way we want
		LROType = CASE LROType
			WHEN 'Coal' THEN 'Rankine (Coal)'
			WHEN 'GasOil' THEN 'Rankine (Gas & Oil)'
			WHEN 'CC' THEN 'Simple & Combined Cycle'
			WHEN 'Cogen' THEN 'Cogeneration'
			END,
		LROTotal = [1] * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
	FROM
	(
		SELECT 
			Total, 
			LROType, 
			value, 
			sortorder = CASE lrotype WHEN 'Coal' THEN 1 WHEN 'GasOil' THEN 2 WHEN 'CC' THEN 3 WHEN 'Cogen' THEN 4 END --to sort them the way we want
		FROM (
				SELECT Total = 1,
					Coal = SUM(CASE WHEN b.FuelGroup = 'Coal' THEN CASE @AllLRO WHEN 1 THEN cu.LRO_Tot ELSE (cu.LRO_M + cu.LRO_F) END / 1000 ELSE 0 END),
					GasOil = SUM(CASE WHEN b.SteamGasOil = 'Y' THEN CASE @AllLRO WHEN 1 THEN cu.LRO_Tot ELSE (cu.LRO_M + cu.LRO_F) END / 1000 ELSE 0 END),
					CC = SUM(CASE WHEN b.CogenElec = 'Elec' THEN CASE @AllLRO WHEN 1 THEN cu.LRO_Tot ELSE (cu.LRO_M + cu.LRO_F) END / 1000 ELSE 0 END),
					Cogen = SUM(CASE WHEN b.CogenElec = 'Cogen' THEN CASE @AllLRO WHEN 1 THEN cu.LRO_Tot ELSE (cu.LRO_M + cu.LRO_F) END / 1000 ELSE 0 END)
				FROM CommUnavail cu, Breaks b, Tsort t
				WHERE b.Refnum = cu.Refnum 
					AND cu.Refnum = t.Refnum 
					AND t.studyyear = @StudyYear
					AND cu.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
			) c

		UNPIVOT (
			value FOR LROType IN (Coal, GasOil, CC, Cogen)
			) unpiv
		) src

		PIVOT (
			SUM (value) FOR Total IN ([1])
		) piv

	ORDER BY sortorder

)


