﻿
CREATE FUNCTION [dbo].[GetDistList]
(	
	@ShortListName VARCHAR(30), -- Short List Name e.g. 'Power15'
	@Group1 VARCHAR(30),
	@Group2 VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4),
	@Metric BIT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT [Percent], Value = 
		CASE WHEN @Metric = 1 AND @Group1 IN ('Coal','Gas','Oil') THEN GlobalDB.dbo.UnitsConv(Value,'MBTU','GJ') 
			WHEN @Metric = 1 AND @Group1 = 'HeatRate' THEN GlobalDB.dbo.UnitsConv(Value,'BTU','MJ') 
			ELSE Value END
	FROM (
		SELECT TOP 1000 [Percent], 
			Value = Value * CASE WHEN @Group1 IN ('Coal','Gas','Oil') THEN GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE 1 END
		FROM DistLists
		WHERE ListName = @ShortListName
			AND Group1 = @Group1
			AND Group2 = @Group2
		ORDER BY [Percent] ASC
	) b

)

