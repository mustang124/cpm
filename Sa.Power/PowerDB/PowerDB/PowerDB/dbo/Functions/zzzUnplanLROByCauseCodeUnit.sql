﻿

CREATE FUNCTION [dbo].[zzzUnplanLROByCauseCodeUnit]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CurrencyCode VARCHAR(4)
	--@CRVGroup VARCHAR(30) --coal or gas & oil

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT UnitName = SiteLabel, UnitName2 = SiteLabel, --two unitnames for the multi
		TotalLRO = CAST(REPLACE(REPLACE(ISNULL(TotalLRO,0),',',''),'<FMT LeftMargin=7>','') AS float)
	FROM UnplanLROByCauseCodeUnitTable(@ListName, @StudyYear, @CurrencyCode)
	WHERE CauseCode not like '%Cause Code%'

)
