﻿



CREATE  PROC [Report].[UpdateReportBatch](@Batch varchar(10))
AS
DECLARE @Refnum dbo.ReportRefnum
DECLARE cReports CURSOR LOCAL FAST_FORWARD
FOR	SELECT Refnum FROM Report.ReportGroups WHERE Batch = @Batch
OPEN cReports
FETCH NEXT FROM cReports INTO @Refnum
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC Report.UpdateReportGroup @Refnum
	FETCH NEXT FROM cReports INTO @Refnum
END
CLOSE cReports
DEALLOCATE cReports




