﻿
CREATE PROCEDURE [Report].[UpdateAllReportsForUnit](@Refnum dbo.Refnum)
AS
IF EXISTS (SELECT * FROM dbo.TSort WHERE Refnum = @Refnum)
	EXEC Report.UpdateReportGroup @Refnum

DECLARE @ReportRefnum ReportRefnum
DECLARE cRefs CURSOR LOCAL FAST_FORWARD
FOR	SELECT ReportRefnum
	FROM Report.ReportGroupRefs rgr
	WHERE rgr.Refnum = @Refnum
	AND EXISTS (SELECT * FROM Report.ReportGroups rg WHERE rg.Refnum = rgr.ReportRefnum AND ISNULL(rg.KeepUpdated, 'N') = 'Y')
OPEN cRefs
FETCH NEXT FROM cRefs INTO @ReportRefnum
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC Report.UpdateReportGroup @ReportRefnum
	FETCH NEXT FROM cRefs INTO @ReportRefnum
END
CLOSE cRefs
DEALLOCATE cRefs



