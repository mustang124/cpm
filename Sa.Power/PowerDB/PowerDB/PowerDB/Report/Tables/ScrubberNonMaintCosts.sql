﻿CREATE TABLE [Report].[ScrubberNonMaintCosts] (
    [Refnum]              [dbo].[ReportRefnum] NOT NULL,
    [DataType]            VARCHAR (6)          NOT NULL,
    [ScrCostLime]         REAL                 NULL,
    [ScrCostChem]         REAL                 NULL,
    [ScrubberPowerCost]   REAL                 NULL,
    [ScrCostNMaint]       REAL                 NULL,
    [TotScrubberNonMaint] REAL                 NULL,
    [tsUpdate]            DATETIME             CONSTRAINT [DF__ScrubberN__tsUpd__7E62A77F] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ScrubberNonMaintCosts] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC)
);

