﻿CREATE TABLE [Report].[FuelCost] (
    [Refnum]   [dbo].[ReportRefnum] NOT NULL,
    [HVBasis]  VARCHAR (3)          NOT NULL,
    [Gas]      REAL                 NULL,
    [Liquid]   REAL                 NULL,
    [Coal]     REAL                 NULL,
    [Total]    REAL                 NULL,
    [tsUpdate] DATETIME             CONSTRAINT [DF_FuelCost_tsUpdate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FuelCost] PRIMARY KEY CLUSTERED ([Refnum] ASC, [HVBasis] ASC)
);

