﻿CREATE TABLE [Report].[AshHandlingCosts] (
    [Refnum]         [dbo].[ReportRefnum] NOT NULL,
    [DataType]       VARCHAR (6)          NOT NULL,
    [AshCostOp]      REAL                 NULL,
    [AshCostDisp]    REAL                 NULL,
    [TotAshHandling] REAL                 NULL,
    [tsUpdate]       DATETIME             CONSTRAINT [DF__AshHandli__tsUpd__7D6E8346] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AshHandlingCosts] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC)
);

