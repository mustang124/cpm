﻿CREATE TABLE [Report].[EquipOHIntervalRaw] (
    [Refnum]       [dbo].[ReportRefnum] NOT NULL,
    [Component]    VARCHAR (8)          NOT NULL,
    [IntervalMo]   NUMERIC (4, 1)       NOT NULL,
    [UnitCount]    SMALLINT             NOT NULL,
    [SiteCount]    SMALLINT             NOT NULL,
    [CompanyCount] SMALLINT             NOT NULL,
    [tsUpdate]     DATETIME             CONSTRAINT [DF__OHInterva__tsUpd__0603C947] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OHInterval] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Component] ASC)
);

