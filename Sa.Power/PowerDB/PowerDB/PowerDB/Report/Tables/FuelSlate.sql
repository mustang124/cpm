﻿CREATE TABLE [Report].[FuelSlate] (
    [Refnum]           [dbo].[ReportRefnum] NOT NULL,
    [TotGasPcntHHV]    NUMERIC (5, 2)       NULL,
    [NatGasPcntHHV]    NUMERIC (5, 2)       NULL,
    [OffGasPcntHHV]    NUMERIC (5, 2)       NULL,
    [H2GasPcntHHV]     NUMERIC (5, 2)       NULL,
    [JetTurbPcntHHV]   NUMERIC (5, 2)       NULL,
    [DieselPcntHHV]    NUMERIC (5, 2)       NULL,
    [FuelOilPcntHHV]   NUMERIC (5, 2)       NULL,
    [CoalPcntHHV]      NUMERIC (5, 2)       NULL,
    [TotGasPcntLHV]    NUMERIC (5, 2)       NULL,
    [NatGasPcntLHV]    NUMERIC (5, 2)       NULL,
    [OffGasPcntLHV]    NUMERIC (5, 2)       NULL,
    [H2GasPcntLHV]     NUMERIC (5, 2)       NULL,
    [JetTurbPcntLHV]   NUMERIC (5, 2)       NULL,
    [DieselPcntLHV]    NUMERIC (5, 2)       NULL,
    [FuelOilPcntLHV]   NUMERIC (5, 2)       NULL,
    [CoalPcntLHV]      NUMERIC (5, 2)       NULL,
    [FuelOilHeatValue] NUMERIC (6, 2)       NULL,
    [FuelOilLHV]       NUMERIC (6, 2)       NULL,
    [tsUpdate]         DATETIME             CONSTRAINT [DF__FuelSlate__tsUpd__78A9CE29] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FuelSlate] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

