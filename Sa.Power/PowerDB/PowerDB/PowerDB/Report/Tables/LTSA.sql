﻿CREATE TABLE [Report].[LTSA] (
    [Refnum]                [dbo].[ReportRefnum] NOT NULL,
    [EquivStartsFactor]     REAL                 NULL,
    [CurrEquivRunHrsFactor] REAL                 NULL,
    [tsUpdate]              DATETIME             CONSTRAINT [DF__LTSA__tsUpdate__6F2063EF] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LTSA_1] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

