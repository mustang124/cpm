﻿CREATE TABLE [Report].[EquipNonOHCostRaw] (
    [Refnum]           [dbo].[ReportRefnum] NOT NULL,
    [Component]        VARCHAR (8)          NOT NULL,
    [AnnNonOHCostMWH]  REAL                 NULL,
    [AnnNonOHCostMW]   REAL                 NULL,
    [AnnNonOHCostMBTU] REAL                 NULL,
    [UnitCount]        SMALLINT             NOT NULL,
    [SiteCount]        SMALLINT             NOT NULL,
    [CompanyCount]     SMALLINT             NOT NULL,
    [tsUpdate]         DATETIME             CONSTRAINT [DF__EquipNonO__tsUpd__07EC11B9] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EquipNonOHCost] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Component] ASC)
);

