﻿CREATE TABLE [Report].[ReportGroupRefWeighting] (
    [ReportRefnum] [dbo].[ReportRefnum] NOT NULL,
    [Refnum]       [dbo].[Refnum]       NOT NULL,
    [Weighting]    REAL                 NOT NULL,
    CONSTRAINT [PK_ReportGroupRefWeighting] PRIMARY KEY CLUSTERED ([ReportRefnum] ASC, [Refnum] ASC)
);

