﻿CREATE TABLE [Report].[AbsencePcnt] (
    [Refnum]     [dbo].[ReportRefnum] NOT NULL,
    [EmpType]    VARCHAR (3)          CONSTRAINT [DF_AbsencePcnt_EmpType] DEFAULT ('OCC') NOT NULL,
    [TotAbsPcnt] NUMERIC (5, 2)       NULL,
    [HOL]        NUMERIC (5, 2)       NULL,
    [VAC]        NUMERIC (5, 2)       NULL,
    [HOLVAC]     NUMERIC (5, 2)       NULL,
    [ONJOBC]     NUMERIC (5, 2)       NULL,
    [ONJOB]      NUMERIC (5, 2)       NULL,
    [SICK]       NUMERIC (5, 2)       NULL,
    [LEGAL]      NUMERIC (5, 2)       NULL,
    [OthExc]     NUMERIC (5, 2)       NULL,
    [Unexc]      NUMERIC (5, 2)       NULL,
    [Other]      NUMERIC (5, 2)       NULL,
    [tsUpdate]   DATETIME             CONSTRAINT [DF_AbsencePcnt_tsUpdate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_AbsencePcnt] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EmpType] ASC)
);

