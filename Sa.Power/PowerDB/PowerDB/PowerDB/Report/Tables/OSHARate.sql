﻿CREATE TABLE [Report].[OSHARate] (
    [Refnum]    [dbo].[ReportRefnum] NOT NULL,
    [OSHARate]  NUMERIC (6, 3)       NULL,
    [tsUpdated] DATETIME             CONSTRAINT [DF_OSHARate_tsUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OSHARate] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

