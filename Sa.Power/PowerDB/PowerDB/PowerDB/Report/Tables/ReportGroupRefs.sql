﻿CREATE TABLE [Report].[ReportGroupRefs] (
    [ReportRefnum] [dbo].[ReportRefnum] NOT NULL,
    [Refnum]       [dbo].[Refnum]       NOT NULL,
    [Weighting]    REAL                 CONSTRAINT [DF_ReportGroupRefs_Weighting] DEFAULT ((1)) NOT NULL,
    [tsUpdate]     DATETIME             DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ReportGroupRefs] PRIMARY KEY CLUSTERED ([ReportRefnum] ASC, [Refnum] ASC) WITH (FILLFACTOR = 70)
);

