﻿CREATE TABLE [Report].[StartsAnalysis] (
    [Refnum]      [dbo].[ReportRefnum] NOT NULL,
    [SuccessPcnt] NUMERIC (5, 2)       NULL,
    [ForcedPcnt]  NUMERIC (5, 2)       NULL,
    [MaintPcnt]   NUMERIC (5, 2)       NULL,
    [StartupPcnt] NUMERIC (5, 2)       NULL,
    [DeratePcnt]  NUMERIC (5, 2)       NULL,
    [tsUpdate]    DATETIME             CONSTRAINT [DF__StartsAna__tsUpd__7108AC61] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_StartsAnalysis] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

