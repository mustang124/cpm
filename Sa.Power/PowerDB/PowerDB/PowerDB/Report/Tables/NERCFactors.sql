﻿CREATE TABLE [Report].[NERCFactors] (
    [Refnum]           [dbo].[ReportRefnum] NOT NULL,
    [SumNMC]           FLOAT (53)           NULL,
    [AvgNMC]           REAL                 NULL,
    [ServiceHrs]       REAL                 NULL,
    [NCF]              NUMERIC (6, 3)       NULL,
    [NOF]              NUMERIC (6, 3)       NULL,
    [EOR]              NUMERIC (6, 3)       NULL,
    [EUOR]             NUMERIC (6, 3)       NULL,
    [EFOR]             NUMERIC (6, 3)       NULL,
    [EPOR]             NUMERIC (6, 3)       NULL,
    [EUF]              NUMERIC (6, 3)       NULL,
    [EUOF]             NUMERIC (6, 3)       NULL,
    [EPOF]             NUMERIC (6, 3)       NULL,
    [MSTUO]            REAL                 NULL,
    [MSTFO]            REAL                 NULL,
    [MSTMO]            REAL                 NULL,
    [ServiceHrs2YrAvg] REAL                 NULL,
    [NCF2Yr]           NUMERIC (6, 3)       NULL,
    [NOF2Yr]           NUMERIC (6, 3)       NULL,
    [EOR2Yr]           NUMERIC (6, 3)       NULL,
    [EUOR2Yr]          NUMERIC (6, 3)       NULL,
    [EFOR2Yr]          NUMERIC (6, 3)       NULL,
    [EPOR2Yr]          NUMERIC (6, 3)       NULL,
    [EUF2Yr]           NUMERIC (6, 3)       NULL,
    [EUOF2Yr]          NUMERIC (6, 3)       NULL,
    [EPOF2Yr]          NUMERIC (6, 3)       NULL,
    [Age]              NUMERIC (4, 1)       NULL,
    [tsUpdate]         DATETIME             CONSTRAINT [DF__NERCFacto__tsUpd__6C43F744] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_NERCFactors_1] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

