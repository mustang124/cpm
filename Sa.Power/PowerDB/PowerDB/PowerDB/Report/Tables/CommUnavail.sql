﻿CREATE TABLE [Report].[CommUnavail] (
    [Refnum]              [dbo].[ReportRefnum] NOT NULL,
    [CommUnavail]         NUMERIC (6, 3)       NULL,
    [CUTI_Tot]            NUMERIC (6, 3)       NULL,
    [CommUnavail_Unp]     NUMERIC (6, 3)       NULL,
    [CUTI_Unp]            NUMERIC (6, 3)       NULL,
    [CommUnavail_F]       NUMERIC (6, 3)       NULL,
    [CommUnavail_M]       NUMERIC (6, 3)       NULL,
    [CommUnavail_P]       NUMERIC (6, 3)       NULL,
    [CUTI_F]              NUMERIC (6, 3)       NULL,
    [CUTI_M]              NUMERIC (6, 3)       NULL,
    [CUTI_P]              NUMERIC (6, 3)       NULL,
    [CommUnavail_Tot_Win] NUMERIC (6, 3)       NULL,
    [CUTI_Tot_Win]        NUMERIC (6, 3)       NULL,
    [CommUnavail_F_Win]   NUMERIC (6, 3)       NULL,
    [CUTI_F_Win]          NUMERIC (6, 3)       NULL,
    [CommUnavail_Tot_Sum] NUMERIC (6, 3)       NULL,
    [CUTI_Tot_Sum]        NUMERIC (6, 3)       NULL,
    [CommUnavail_F_Sum]   NUMERIC (6, 3)       NULL,
    [CUTI_F_Sum]          NUMERIC (6, 3)       NULL,
    [tsUpdate]            DATETIME             CONSTRAINT [DF__CommUnava__tsUpd__70148828] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CommUnavail] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

