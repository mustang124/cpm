﻿CREATE TABLE [Report].[CTGMaintCostPerStart] (
    [Refnum]            [dbo].[ReportRefnum] NOT NULL,
    [AvgTotStarts]      NUMERIC (6, 1)       NULL,
    [LTSAPerStart]      REAL                 NULL,
    [AnnOHPerStart]     REAL                 NULL,
    [NonOHPerStart]     REAL                 NULL,
    [MaintCostPerStart] REAL                 NULL,
    [tsUpdate]          DATETIME             CONSTRAINT [DF_CTGMaintCostPerStart_tsUpdate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTGMaintCostPerStart] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

