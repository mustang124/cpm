﻿CREATE TABLE [Report].[Cogen] (
    [Refnum]                 [dbo].[ReportRefnum] NOT NULL,
    [ThermPcnt]              NUMERIC (5, 2)       NULL,
    [StmReturnPcnt]          NUMERIC (5, 2)       NULL,
    [TotExportMBTU]          REAL                 NULL,
    [ThermEff]               NUMERIC (5, 2)       NULL,
    [CTGThermEff]            NUMERIC (5, 2)       NULL,
    [ElecHeatRate]           NUMERIC (6, 1)       NULL,
    [ThermNCF]               NUMERIC (5, 2)       NULL,
    [ThermEUF]               NUMERIC (5, 2)       NULL,
    [ElecNCF]                NUMERIC (5, 2)       NULL,
    [ElecNOF]                NUMERIC (6, 3)       NULL,
    [ElecEFOR]               NUMERIC (6, 3)       NULL,
    [TotCashOpexMBTU]        REAL                 NULL,
    [TotCashLessFuelEmmMBTU] REAL                 NULL,
    [PlantMngMBTU]           REAL                 NULL,
    [AnnMaintCostMBTU]       REAL                 NULL,
    [tsUpdate]               DATETIME             CONSTRAINT [DF__Cogen__tsUpdate__75CD617E] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Cogen] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

