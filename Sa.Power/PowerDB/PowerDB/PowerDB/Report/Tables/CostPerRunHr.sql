﻿CREATE TABLE [Report].[CostPerRunHr] (
    [Refnum]              [dbo].[ReportRefnum] NOT NULL,
    [TotalCostPerRunHr]   REAL                 NULL,
    [FixedCostPerRunHr]   REAL                 NULL,
    [VarLessFuelPerRunHr] REAL                 NULL,
    [TotMaintPerRunHr]    REAL                 NULL,
    [NonOHMaintPerRunHr]  REAL                 NULL,
    [OHMaintPerRunHr]     REAL                 NULL,
    [LTSAPerRunHr]        REAL                 NULL,
    [tsUpdate]            DATETIME             CONSTRAINT [DF__CostPerRu__tsUpd__76C185B7] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CostPerRunHr] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

