﻿CREATE TABLE [Report].[CTGData] (
    [Refnum]      [dbo].[ReportRefnum] NOT NULL,
    [NMC]         REAL                 NULL,
    [PriorNetGWH] REAL                 NULL,
    [NetGWH]      REAL                 NULL,
    [ServiceHrs]  REAL                 NULL,
    [NCF2Yr]      NUMERIC (6, 3)       NULL,
    [NOF2Yr]      NUMERIC (6, 3)       NULL,
    [SuccessPcnt] NUMERIC (5, 2)       NULL,
    [ForcedPcnt]  NUMERIC (5, 2)       NULL,
    [MaintPcnt]   NUMERIC (5, 2)       NULL,
    [StartupPcnt] NUMERIC (5, 2)       NULL,
    [DeratePcnt]  NUMERIC (5, 2)       NULL,
    [HeatRate]    NUMERIC (6, 1)       NULL,
    [HeatRateLHV] NUMERIC (6, 1)       NULL,
    [FiringTemp]  NUMERIC (6, 1)       NULL,
    [tsUpdate]    DATETIME             CONSTRAINT [DF__CTGData__tsUpdat__6E2C3FB6] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTGData_1] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

