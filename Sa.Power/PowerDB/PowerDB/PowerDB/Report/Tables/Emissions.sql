﻿CREATE TABLE [Report].[Emissions] (
    [Refnum]       [dbo].[ReportRefnum] NOT NULL,
    [EmissionType] VARCHAR (3)          NOT NULL,
    [LbsPerMWH]    REAL                 NULL,
    [TonsPerMWH]   REAL                 NULL,
    [TonsEmitted]  REAL                 NULL,
    [TonsAllow]    REAL                 NULL,
    [tsUpdate]     DATETIME             CONSTRAINT [DF__Emissions__tsUpd__7B863AD4] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Emissions] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EmissionType] ASC)
);

