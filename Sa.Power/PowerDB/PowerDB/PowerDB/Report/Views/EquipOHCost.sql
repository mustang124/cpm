﻿CREATE VIEW Report.EquipOHCost
AS
SELECT Refnum, DataType, [CPHM], Boilers = [BOIL], STG_HPS = [STG-HPS], STG_IPS = [STG-IPS], STG_TURB = [STG-TURB], STG_LPS = [STG-LPS], STG_GEN = [STG-GEN], STG_VC = [STG-VC], STG_CA = [STG-CA], 
[BAG],[PREC],[WGS],[DGS],[SCR],
CTG_HGPInsp = [CTG-TURB.HGPINSP], CTG_TURB_OH = [CTG-TURB.OVHL], CTG_TURB_OthProj = [CTG-TURB.PROJ], CTG_TURB = [CTG-TURB], CTG_COMB = [CTG-COMB], CTG_COMP = [CTG-COMP], CTG_GEN = [CTG-GEN], CTG_TRAN = [CTG-TRAN], CTG_HRSG = [ALL-HRSG], CTG_OTH = [CTG-OTH],
[ALL-FUEL],[CWF],[ASH],WSS = [ALL-WSS],[SITETRAN],[WASTEH2O],[OTHER]
FROM (SELECT e.Refnum, rd.DataType, CompProj = CASE WHEN e.ProjectType = 'OVHL+PROJ' THEN e.Component ELSE RTRIM(e.Component) + '.' + e.ProjectType END, rd.AnnOHCost
	FROM Report.EquipOHCostRaw e LEFT JOIN Report.ReportGroups rg ON rg.Refnum = e.Refnum
	CROSS APPLY (VALUES (CASE WHEN ISNULL(rg.PostProcess,'N') = 'N' OR (e.UnitCount >= 1 AND e.CompanyCount >= 1) THEN 1.0 END)) ds(ReportData)
	CROSS APPLY (VALUES ('$/MWH', e.AnnOHCostMWH*ds.ReportData),('K$/MW', e.AnnOHCostMW*ds.ReportData),('$/MBTU', e.AnnOHCostMBTU*ds.ReportData)
		) rd(DataType, AnnOHCost)
	) src
PIVOT (AVG(AnnOHCost) FOR CompProj IN ([CPHM],[BOIL],[STG-HPS],[STG-IPS],[STG-LPS],[STG-TURB],[STG-GEN],[STG-VC],[STG-CA],
[BAG],[PREC],[WGS],[DGS],[SCR],
[CTG-TURB.HGPINSP],[CTG-TURB.OVHL],[CTG-TURB.PROJ],[CTG-TURB],[CTG-COMB],[CTG-COMP],[CTG-GEN],[CTG-TRAN],[ALL-HRSG],[CTG-OTH],
[ALL-FUEL],[CWF],[ASH],[ALL-WSS],[SITETRAN],[WASTEH2O],[OTHER])) pvt