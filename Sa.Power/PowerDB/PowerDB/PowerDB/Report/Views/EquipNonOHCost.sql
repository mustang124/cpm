﻿CREATE VIEW Report.EquipNonOHCost
AS
SELECT Refnum, DataType, [CPHM], Boilers = [BOIL], STG_TURB = [STG-TURB], STG_GEN = [STG-GEN], STG_VC = [STG-VC], STG_CA = [STG-CA], 
[BAG],[PREC],[WGS],[DGS],[SCR],
CTG_TURB = [CTG-TURB], CTG_COMB = [CTG-COMB], CTG_GEN = [CTG-GEN], CTG_TRAN = [CTG-TRAN], CTG_HRSG = [CTG-HRSG], CTG_OTH = [CTG-OTH],
[FUELDEL],[CWF],[ASH],[WSS],[SITETRAN],[WASTEH2O],[OTHER]
FROM (SELECT e.Refnum, rd.DataType, e.Component, rd.AnnNonOHCost
	FROM Report.EquipNonOHCostRaw e LEFT JOIN Report.ReportGroups rg ON rg.Refnum = e.Refnum
	CROSS APPLY (VALUES (CASE WHEN ISNULL(rg.PostProcess,'N') = 'N' OR (e.UnitCount >= 1 AND e.CompanyCount >= 1) THEN 1.0 END)) ds(ReportData)
	CROSS APPLY (VALUES ('$/MWH', e.AnnNonOHCostMWH*ds.ReportData),('K$/MW', e.AnnNonOHCostMW*ds.ReportData),('$/MBTU', e.AnnNonOHCostMBTU*ds.ReportData)
		) rd(DataType, AnnNonOHCost)
	) src
PIVOT (AVG(AnnNonOHCost) FOR Component IN ([CPHM],[BOIL],[STG-TURB],[STG-GEN],[STG-VC],[STG-CA],[BAG],[PREC],[WGS],[DGS],[SCR],
[CTG-TURB],[CTG-COMB],[CTG-GEN],[CTG-TRAN],[CTG-HRSG],[CTG-OTH],
[FUELDEL],[CWF],[ASH],[WSS],[SITETRAN],[WASTEH2O],[OTHER])) pvt
