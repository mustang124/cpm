﻿CREATE FUNCTION [Report].[AggOHInterval](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID, oh.Component
		, IntervalMo = GlobalDB.dbo.WtAvg(oh.IntervalYrs, r.Weighting)*12
		, COUNT(DISTINCT r.Refnum) AS UnitCount
		, COUNT(DISTINCT t.SiteID) AS SiteCount
		, COUNT(DISTINCT t.CompanyID) AS CompanyCount
	FROM @Refnums r INNER JOIN dbo.OHMaint oh ON oh.Refnum = r.Refnum
	INNER JOIN dbo.TSort t ON t.Refnum = r.Refnum
	WHERE oh.ProjectID = 'OVHL' AND oh.IntervalYrs > 0
	GROUP BY r.SubGroupType, r.SubGroupID, oh.Component
	UNION
	SELECT r.SubGroupType, r.SubGroupID, cc.ComponentGroup
		, IntervalMo = GlobalDB.dbo.WtAvg(oh.IntervalYrs, r.Weighting)*12
		, COUNT(DISTINCT r.Refnum) AS UnitCount
		, COUNT(DISTINCT t.SiteID) AS SiteCount
		, COUNT(DISTINCT t.CompanyID) AS CompanyCount
	FROM @Refnums r INNER JOIN dbo.OHMaint oh ON oh.Refnum = r.Refnum
	INNER JOIN dbo.TSort t ON t.Refnum = r.Refnum
	INNER JOIN (VALUES ('STG-GEN','ALL-GEN'), ('CTG-GEN','ALL-GEN')
		, ('HRSG','ALL-HRSG'), ('CTG-HRSG','ALL-HRSG')
	) cc(Component, ComponentGroup) ON cc.Component = oh.Component
	WHERE oh.ProjectID = 'OVHL' AND oh.IntervalYrs > 0
	GROUP BY r.SubGroupType, r.SubGroupID, cc.ComponentGroup
)

