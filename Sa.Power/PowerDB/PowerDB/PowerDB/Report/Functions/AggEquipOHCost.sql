﻿CREATE FUNCTION [Report].[AggEquipOHCost](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT ac.SubGroupType, ac.SubGroupID, ac.Component, ac.ProjectType
		, AnnOHCostMWH = CASE WHEN SUM(gtc.AdjNetMWH2Yr) > 0 THEN SUM(ac.TotAnnOHCost*ac.Weighting)/SUM(gtc.AdjNetMWH2Yr*ac.Weighting) END*1000
		, AnnOHCostMW = CASE WHEN SUM(gtc.AdjNetMWH2Yr) > 0 THEN SUM(ac.TotAnnOHCost*ac.Weighting)/SUM(nf.NMC2Yr*ac.Weighting) END
		, AnnOHCostMBTU = CASE WHEN SUM(gtc.AdjNetMWH2Yr) > 0 THEN SUM(ac.TotAnnOHCost*ac.Weighting)/SUM(gtc.TotalOutputMBTU2Yr*ac.Weighting) END*1000
		, COUNT(DISTINCT t.Refnum) AS UnitCount
		, COUNT(DISTINCT t.SiteID) AS SiteCount
		, COUNT(DISTINCT t.CompanyID) AS CompanyCount
	FROM (
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, oh.Component, ProjectType = 'OVHL+PROJ'
			, TotAnnOHCost = SUM(oh.TotAnnOHCost)
		FROM @Refnums r INNER JOIN dbo.OHMaint oh ON oh.Refnum = r.Refnum
		WHERE oh.TotAnnOHCost > 0
		GROUP BY r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, oh.Component
		UNION
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, oh.Component, ProjectType = CASE WHEN oh.ProjectID IN ('OVHL','HGPINSP') THEN oh.ProjectID ELSE 'PROJ' END
			, TotAnnOHCost = SUM(oh.TotAnnOHCost)
		FROM @Refnums r INNER JOIN dbo.OHMaint oh ON oh.Refnum = r.Refnum
		WHERE oh.TotAnnOHCost > 0 AND oh.Component IN ('CTG-TURB')
		GROUP BY r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, oh.Component, CASE WHEN oh.ProjectID IN ('OVHL','HGPINSP') THEN oh.ProjectID ELSE 'PROJ' END
		UNION
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, cc.ComponentGroup, ProjectType = 'OVHL+PROJ'
			, TotAnnOHCost = SUM(oh.TotAnnOHCost)
		FROM @Refnums r INNER JOIN dbo.OHMaint oh ON oh.Refnum = r.Refnum
		INNER JOIN (VALUES ('STG-GEN','ALL-GEN'), ('CTG-GEN','ALL-GEN')
			, ('HRSG','ALL-HRSG'), ('CTG-HRSG','ALL-HRSG')
			, ('DQWS','ALL-WSS'), ('BOILH2O','ALL-WSS')
			, ('FUELDEL','ALL-FUEL'), ('FHF','ALL-FUEL')
		) cc(Component, ComponentGroup) ON cc.Component = oh.Component
		WHERE oh.TotAnnOHCost > 0
		GROUP BY r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, cc.ComponentGroup
	) ac
	INNER JOIN dbo.TSort t ON t.Refnum = ac.Refnum
	INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	INNER JOIN dbo.NERCFactors nf ON nf.Refnum = t.Refnum
	GROUP BY ac.SubGroupType, ac.SubGroupID, ac.Component, ac.ProjectType)

