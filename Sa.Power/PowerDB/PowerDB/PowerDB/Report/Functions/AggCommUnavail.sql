﻿CREATE FUNCTION [Report].[AggCommUnavail](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, CommUnavail = SUM(cu.PeakMWHLost_Tot)/SUM(cu.TotPeakMWH)*100
		, CUTI_Tot = GlobalDB.dbo.WtAvg(cu.CUTI_Tot, r.Weighting)
		, CommUnavail_Unp = SUM(cu.PeakMWHLost_Unp)/SUM(cu.TotPeakMWH)*100
		, CUTI_Unp = GlobalDB.dbo.WtAvg(cu.CUTI_Unp, r.Weighting)
		, CommUnavail_F = SUM(cu.PeakMWHLost_F)/SUM(cu.TotPeakMWH)*100
		, CommUnavail_M = SUM(cu.PeakMWHLost_M)/SUM(cu.TotPeakMWH)*100
		, CommUnavail_P = SUM(cu.PeakMWHLost_P)/SUM(cu.TotPeakMWH)*100
		, CUTI_F = GlobalDB.dbo.WtAvg(cu.CUTI_F, r.Weighting)
		, CUTI_M = GlobalDB.dbo.WtAvg(cu.CUTI_M, r.Weighting)
		, CUTI_P = GlobalDB.dbo.WtAvg(cu.CUTI_P, r.Weighting)
		, CommUnavail_Tot_Win = SUM(cu.PeakMWHLost_Tot_Win)/SUM(cu.TotPeakMWH_Win)*100
		, CUTI_Tot_Win = GlobalDB.dbo.WtAvg(cu.CUTI_Tot_Win, r.Weighting)
		, CommUnavail_F_Win = SUM(cu.PeakMWHLost_F_Win)/SUM(cu.TotPeakMWH_Win)*100
		, CUTI_F_Win = GlobalDB.dbo.WtAvg(cu.CUTI_F_Win, r.Weighting)
		, CommUnavail_Tot_Sum = SUM(cu.PeakMWHLost_Tot_Sum)/SUM(cu.TotPeakMWH_Sum)*100
		, CUTI_Tot_Sum = GlobalDB.dbo.WtAvg(cu.CUTI_Tot_Sum, r.Weighting)
		, CommUnavail_F_Sum = SUM(cu.PeakMWHLost_F_Sum)/SUM(cu.TotPeakMWH_Sum)*100
		, CUTI_F_Sum = GlobalDB.dbo.WtAvg(cu.CUTI_F_Sum, r.Weighting)
	FROM @Refnums r INNER JOIN dbo.CommUnavail cu ON cu.Refnum = r.Refnum
	WHERE r.Weighting > 0
	GROUP BY r.SubGroupType, r.SubGroupID
)

