﻿CREATE FUNCTION [Report].[AggSiteUnits](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, TotExclSCNum = GlobalDB.dbo.WtAvgNZ(ugd.TotExclSCNum, r.Weighting)
		, TotExclSCCap = GlobalDB.dbo.WtAvgNZ(ugd.TotExclSCCap, r.Weighting)
		, TotUnitNum = GlobalDB.dbo.WtAvgNZ(ugd.TotUnitNum, r.Weighting)
		, TotCap = GlobalDB.dbo.WtAvgNZ(ugd.TotCap, r.Weighting)
		, TotExclCTNum = GlobalDB.dbo.WtAvg(ugd.TotExclCTNum, CASE WHEN ugd.TotUnitNum > 0 THEN r.Weighting ELSE 0 END)
		, TotExclCTCap = GlobalDB.dbo.WtAvgNZ(ugd.TotExclCTCap, CASE WHEN ugd.TotUnitNum > 0 THEN r.Weighting ELSE 0 END)
	FROM @refnums r INNER JOIN dbo.UnitGenData ugd ON ugd.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

