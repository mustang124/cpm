﻿CREATE FUNCTION [Report].[AggOpexRanges](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	WITH RangeData AS (
		SELECT r.SubGroupType, r.SubGroupID, opex.DataType, opex.TotCash, opex.TotCashLessFuel, opex.TotCashLessFuelEm, opex.ActCashLessFuel
		FROM @Refnums r INNER JOIN dbo.OpExCalc opex ON opex.Refnum = r.Refnum
		WHERE r.Weighting > 0 AND opex.DataType <> 'ADJ')
	SELECT DISTINCT SubGroupType, SubGroupID, DataType
		, TotCash_Min2 = (SELECT AVG(TotCash) FROM (SELECT TOP 2 TotCash FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.TotCash ASC) x)
		, TotCash_Max2 = (SELECT AVG(TotCash) FROM (SELECT TOP 2 TotCash FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.TotCash DESC) x)
		, TotCashLessFuel_Min2 = (SELECT AVG(TotCashLessFuel) FROM (SELECT TOP 2 TotCashLessFuel FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.TotCashLessFuel ASC) x)
		, TotCashLessFuel_Max2 = (SELECT AVG(TotCashLessFuel) FROM (SELECT TOP 2 TotCashLessFuel FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.TotCashLessFuel DESC) x)
		, TotCashLessFuelEm_Min2 = (SELECT AVG(TotCashLessFuelEm) FROM (SELECT TOP 2 TotCashLessFuelEm FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.TotCashLessFuelEm ASC) x)
		, TotCashLessFuelEm_Max2 = (SELECT AVG(TotCashLessFuelEm) FROM (SELECT TOP 2 TotCashLessFuelEm FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.TotCashLessFuelEm DESC) x)
		, ActCashLessFuel_Min2 = (SELECT AVG(ActCashLessFuel) FROM (SELECT TOP 2 ActCashLessFuel FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.ActCashLessFuel ASC) x)
		, ActCashLessFuel_Max2 = (SELECT AVG(ActCashLessFuel) FROM (SELECT TOP 2 ActCashLessFuel FROM RangeData rd WHERE rd.SubGroupType = t.SubGroupType AND rd.SubGroupID = t.SubGroupID AND rd.DataType = t.DataType ORDER BY rd.ActCashLessFuel DESC) x)
	FROM RangeData t
)

