﻿CREATE FUNCTION [Report].[AggCoal](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, HeatValue = GlobalDB.dbo.WtAvgNZ(c.HeatValue, c.Tons*r.Weighting)
		, LHV = GlobalDB.dbo.WtAvgNZ(c.LHV, c.Tons*r.Weighting)
		, AshPcnt = GlobalDB.dbo.WtAvgNZ(c.AshPcnt, c.Tons*r.Weighting)
		, SulfurPcnt = GlobalDB.dbo.WtAvgNZ(c.SulfurPcnt, c.Tons*r.Weighting)
		, MoistPcnt = GlobalDB.dbo.WtAvgNZ(c.MoistPcnt, c.Tons*r.Weighting)
		, DeliveredCostTon = GlobalDB.dbo.WtAvg(c.DeliveredCostTon, c.Tons*r.Weighting)
		, OnsitePrepCostTon = GlobalDB.dbo.WtAvg(c.OnsitePrepCostTon, c.Tons*r.Weighting)
		, TotCostTon = GlobalDB.dbo.WtAvg(c.TotCostTon, c.Tons*r.Weighting)
	FROM @refnums r INNER JOIN dbo.Coal c ON c.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

