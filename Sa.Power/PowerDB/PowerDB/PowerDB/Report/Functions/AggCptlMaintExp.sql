﻿CREATE FUNCTION [Report].[AggCptlMaintExp](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT cme.SubGroupType, cme.SubGroupID, dx.DataType
		, STMaintCptl = cme.STMaintCptl/dx.DivValue
		, STMaintExp = cme.STMaintExp/dx.DivValue
		, STMMO = cme.STMMO/dx.DivValue
		, Energy = cme.Energy/dx.DivValue
		, RegEnv = cme.RegEnv/dx.DivValue
		, Admin = cme.Admin/dx.DivValue
		, ConstrRmvl = cme.ConstrRmvl/dx.DivValue
		, InvestCptl = cme.InvestCptl/dx.DivValue
	FROM (
		SELECT r.SubGroupType, r.SubGroupID
			, STMaintCptl = SUM(cm.STMaintCptl)
			, STMaintExp = SUM(cm.STMaintExp)
			, STMMO = SUM(cm.STMMO)
			, Energy = SUM(cm.Energy)
			, RegEnv = SUM(cm.RegEnv)
			, Admin = SUM(cm.Admin)
			, ConstrRmvl = SUM(cm.ConstrRmvl)
			, InvestCptl = SUM(cm.InvestCptl)
		FROM @refnums r INNER JOIN dbo.CptlMaintExp cm ON cm.Refnum = r.Refnum AND cm.CptlCode = 'CURR'
		GROUP BY r.SubGroupType, r.SubGroupID) cme
	INNER JOIN (
		SELECT r.SubGroupType, r.SubGroupID, AdjNetMWH = SUM(gtc.AdjNetMWH), TotalOutputMBTU = SUM(gtc.TotalOutputMBTU), NMC = SUM(NMC)
		FROM @refnums r INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
		INNER JOIN dbo.NERCFactors nf ON nf.Refnum = r.Refnum
		GROUP BY r.SubGroupType, r.SubGroupID) d ON d.SubGroupType = cme.SubGroupType AND d.SubGroupID = cme.SubGroupID
	CROSS APPLY (VALUES
		('$/MWH', d.AdjNetMWH/1000),
		('K$/MW', d.NMC),
		('$/MBTU', d.TotalOutputMBTU/1000)
	) dx(DataType, DivValue)
)

