﻿CREATE FUNCTION [Report].[AggPersOvertimePcnt](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT SubGroupType, SubGroupID, 
		OCCOps = [OCC-Oper], OCCSTNOHMaint = [OCC-NOHMaint+Insp], OCCNOHMaint = [OCC-NOHMaint], OCCInsp = [OCC-Insp], OCCOverhaul = [OCC-OHAdj], OCCSTMaint = [OCCM],
		OCCTech = [OCC-Tech], OCCAdmin = [OCC-Admin], OCCNonMaint = [OCC-NonMaint], OCCTotal = [TOCC],
		MPSOps = [MPS-Oper], MPSSTNOHMaint = [MPS-NOHMaint+Insp], MPSNOHMaint = [MPS-NOHMaint], MPSInsp = [MPS-Insp], MPSOverhaul = [MPS-OHAdj], MPSSTMaint = [MPSM],
		MPSTech = [MPS-Tech], MPSAdmin = [MPS-Admin], MPSNonMaint = [MPS-NonMaint], MPSTotal = [TMPS],
		TotalPers = [TP]
	FROM (
	SELECT r.SubGroupType, r.SubGroupID, p.PersCat, OVTPcnt = SUM(p.SiteOVTHrs*r.Weighting)/SUM(p.SiteSTH*r.Weighting)*100
	FROM @refnums r INNER JOIN dbo.Pers p ON p.Refnum = r.Refnum
	WHERE p.SiteSTH > 0 AND p.PersCat NOT IN ('OCC-Maint%', 'MPS-Maint%')
	GROUP BY r.SubGroupType, r.SubGroupID, p.PersCat
	UNION
	SELECT r.SubGroupType, r.SubGroupID, noh.STCat, OVTPcnt = SUM(p.SiteOVTHrs*r.Weighting)/SUM(p.SiteSTH*r.Weighting)*100
	FROM @refnums r INNER JOIN dbo.Pers p ON p.Refnum = r.Refnum
	INNER JOIN (VALUES ('OCC-NOHMaint+Insp','OCC-NOHMaint'),('OCC-NOHMaint+Insp','OCC-Insp')
					,  ('MPS-NOHMaint+Insp','MPS-NOHMaint'),('MPS-NOHMaint+Insp','MPS-Insp')) noh(STCat, PersCat) ON noh.PersCat = p.PersCat 
	WHERE p.SiteSTH > 0
	GROUP BY r.SubGroupType, r.SubGroupID, noh.STCat
	UNION
	SELECT r.SubGroupType, r.SubGroupID, p.SectionID, SiteOVTPcnt = GlobalDB.dbo.WtAvg(p.SiteOVTPcnt, p.SiteSTH*r.Weighting)
	FROM @refnums r INNER JOIN dbo.PersSTCalc p ON p.Refnum = r.Refnum
	WHERE p.SiteSTH > 0
	GROUP BY r.SubGroupType, r.SubGroupID, p.SectionID) src
	PIVOT (AVG(OVTPcnt) FOR PersCat IN (
		[OCC-Oper],[OCC-NOHMaint+Insp],[OCC-NOHMaint],[OCC-Insp],[OCC-OHAdj],[OCCM],[OCC-Tech],[OCC-Admin],[OCC-NonMaint],[TOCC],
		[MPS-Oper], [MPS-NOHMaint+Insp],[MPS-NOHMaint],[MPS-Insp],[MPS-OHAdj],[MPSM],[MPS-Tech],[MPS-Admin],[MPS-NonMaint],[TMPS],
		[TP])) pvt
)

