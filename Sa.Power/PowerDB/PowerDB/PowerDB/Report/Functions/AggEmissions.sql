﻿CREATE FUNCTION [Report].[AggEmissions](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT g.SubGroupType, g.SubGroupID, ex.EmissionType, ex.LbsPerMWH, ex.TonsPerMWH, ex.TonsEmitted, ex.TonsAllow
	FROM (SELECT DISTINCT SubGroupType, SubGroupID FROM @Refnums) g
	LEFT JOIN (
		SELECT r.SubGroupType, r.SubGroupID
			, SO2LbsPerMWH = GlobalDB.dbo.WtAvgNZ(e.SO2LbsPerMWH, gtc.PotentialGrossMWH*r.Weighting)
			, NOxLbsPerMWH = GlobalDB.dbo.WtAvgNZ(e.NOxLbsPerMWH, gtc.PotentialGrossMWH*r.Weighting)
			, CO2TonsPerMWH = GlobalDB.dbo.WtAvgNZ(e.CO2TonsPerMWH, gtc.PotentialGrossMWH*r.Weighting)
		FROM @refnums r INNER JOIN dbo.Emissions e ON e.Refnum = r.Refnum
		INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
		GROUP BY r.SubGroupType, r.SubGroupID) e ON e.SubGroupType = g.SubGroupType AND e.SubGroupID = g.SubGroupID
	LEFT JOIN (
		SELECT r.SubGroupType, r.SubGroupID
			, SO2Emitted = GlobalDB.dbo.WtAvg(misc.SO2Emitted, CASE WHEN misc.SO2Emitted>0 THEN r.Weighting ELSE 0 END)
			, SO2Allow = GlobalDB.dbo.WtAvg(misc.SO2Allow, CASE WHEN misc.SO2Emitted>0 THEN r.Weighting ELSE 0 END)
			, CO2Emitted = GlobalDB.dbo.WtAvg(misc.CO2Emitted, CASE WHEN misc.CO2Emitted>0 THEN r.Weighting ELSE 0 END)
			, CO2Allow = GlobalDB.dbo.WtAvg(misc.CO2Allow, CASE WHEN misc.CO2Emitted>0 THEN r.Weighting ELSE 0 END)
			, NOxEmitted = GlobalDB.dbo.WtAvg(misc.NOxEmitted, CASE WHEN misc.NOxEmitted>0 THEN r.Weighting ELSE 0 END)
			, NOxAllow = GlobalDB.dbo.WtAvg(misc.NOxAllow, CASE WHEN misc.NOxEmitted>0 THEN r.Weighting ELSE 0 END)
		FROM @refnums r
		INNER JOIN (SELECT Refnum, TurbineID, SO2Emitted = SUM(SO2Emitted), SO2Allow = SUM(SO2Allow), CO2Emitted = SUM(CO2Emitted), CO2Allow = SUM(CO2Allow)
			, NOxEmitted = SUM(NOxEmitted), NOxAllow = SUM(NOxAllow), AshCostOP = SUM(AshCostOp), AshCostDisp = SUM(AshCostDisp)
			FROM dbo.Misc WHERE Refnum IN (SELECT Refnum FROM @Refnums) GROUP BY Refnum, TurbineID) misc ON misc.Refnum = r.Refnum
		GROUP BY r.SubGroupType, r.SubGroupID) ea ON ea.SubGroupType = g.SubGroupType AND ea.SubGroupID = g.SubGroupID
	CROSS APPLY (VALUES
			('SO2', e.SO2LbsPerMWH, e.SO2LbsPerMWH/2000, ea.SO2Emitted, ea.SO2Allow),
			('CO2', e.CO2TonsPerMWH*2000, e.CO2TonsPerMWH, ea.CO2Emitted, ea.CO2Allow),
			('NOx', e.NOxLbsPerMWH, e.NOxLbsPerMWH/2000, ea.NOxEmitted, ea.NOxAllow)
		) ex(EmissionType, LbsPerMWH, TonsPerMWH, TonsEmitted, TonsAllow)
)

