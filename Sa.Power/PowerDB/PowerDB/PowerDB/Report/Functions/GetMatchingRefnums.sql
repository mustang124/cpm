﻿CREATE FUNCTION [Report].[GetMatchingRefnums](@ListName varchar(30), @UserGroup varchar(5), @StudyYear smallint
	, @TileRankVariable varchar(128), @TileRankBreak varchar(128), @TileBreakValue varchar(12), @TileTile tinyint
	, @CompanyID varchar(15), @SiteID dbo.SiteID, @Region varchar(15), @Continent varchar(20), @Country varchar(50), @State varchar(20)
	, @FuelGroup varchar(9), @FuelType varchar(8), @LoadType varchar(4), @LoadType2 varchar(3), @CRVGroup varchar(5), @CRVSizeGroup varchar(6), @CRVSizeGroup2 varchar(5)
	, @CoalNMCGroup int, @CoalNMCGroup2 int, @CoalNMCOverlap int, @CoalNMCOverlap2 tinyint, @CoalAshGroup int, @CoalHVGroup int, @CoalSulfurGroup varchar(2)
	, @Scrubbers varchar(1), @ScrubberSize varchar(2), @BaseCoal varchar(1), @SuperCritical dbo.YorN, @SubCritical dbo.YorN
	, @SteamGasOil varchar(1), @CombinedCycle varchar(1), @CogenElec varchar(5), @GasOilNMCGroup int, @CCNMCGroup tinyint
	, @NCF2YrGroup tinyint, @FiringTempGroup tinyint, @CTGS tinyint, @LTSA varchar(1), @Regulated dbo.YorN, @StartsGroup smallint
	, @MinNMC real, @MaxNMC real)
RETURNS @Results TABLE(Refnum dbo.Refnum NOT NULL)
AS
BEGIN
	IF @TileTile IS NOT NULL
		INSERT @Results (Refnum)
		SELECT Refnum FROM Ranking.RankView 
		WHERE ListName = @ListName AND RankVariable = @TileRankVariable 
		AND RankBreak = @TileRankBreak AND BreakValue = @TileBreakValue 
		AND Tile = @TileTile
	ELSE
		INSERT @Results (Refnum)
		SELECT rl.Refnum
		FROM dbo._RL rl INNER JOIN dbo.TSort t ON t.Refnum = rl.Refnum
		INNER JOIN dbo.Breaks b ON b.Refnum = rl.Refnum
		LEFT JOIN dbo.FuelTotCalc ftc ON ftc.Refnum = rl.Refnum
		WHERE rl.ListName = @ListName
		AND (@UserGroup IS NULL OR rl.UserGroup = @UserGroup)
		AND (@StudyYear IS NULL OR t.StudyYear = @StudyYear)
		AND (@CompanyID IS NULL OR t.CompanyID = @CompanyID)
		AND (@SiteID IS NULL OR t.SiteID = @SiteID)
		AND (@Region IS NULL OR t.Region = @Region)
		AND (@Continent IS NULL OR t.Continent = @Continent)
		AND (@Country IS NULL OR t.Country = @Country)
		AND (@State IS NULL OR t.[State] = @State)
		AND (@FuelGroup IS NULL OR b.FuelGroup = @FuelGroup)
		AND (@FuelType IS NULL OR ftc.FuelType = @FuelType)
		AND (@LoadType IS NULL OR b.LoadType = @LoadType)
		AND (@LoadType2 IS NULL OR b.LoadType2 = @LoadType2)
		AND (@CRVGroup IS NULL OR b.CRVGroup = @CRVGroup)
		AND (@CRVSizeGroup IS NULL OR b.CRVSizeGroup = @CRVSizeGroup)
		AND (@CRVSizeGroup2 IS NULL OR b.CRVSizeGroup2 = @CRVSizeGroup2)
		AND (@CoalNMCGroup IS NULL OR b.CoalNMCGroup = @CoalNMCGroup)
		AND (@CoalNMCGroup2 IS NULL OR b.CoalNMCGroup2 = @CoalNMCGroup2)
		AND (@CoalNMCOverlap IS NULL OR b.CoalNMCOverlap = @CoalNMCOverlap)
		AND (@CoalNMCOverlap2 IS NULL OR b.CoalNMCOverlap2 = @CoalNMCOverlap2)
		AND (@CoalAshGroup IS NULL OR b.CoalAshGroup = @CoalAshGroup)
		AND (@CoalHVGroup IS NULL OR b.CoalHVGroup = @CoalHVGroup)
		AND (@CoalSulfurGroup IS NULL OR b.CoalSulfurGroup = @CoalSulfurGroup)
		AND (@Scrubbers IS NULL OR b.Scrubbers = @Scrubbers)
		AND (@ScrubberSize IS NULL OR b.ScrubberSize = @ScrubberSize)
		AND (@BaseCoal IS NULL OR b.BaseCoal = @BaseCoal)
		AND (@SuperCritical IS NULL OR (@SuperCritical = 'Y' AND b.CRVGroup LIKE '%SC') OR (@SuperCritical = 'N' AND b.CRVGroup NOT LIKE '%SC'))
		/* SubCritical is not right. Need definition. */
		AND (@SubCritical IS NULL OR (@SubCritical = 'Y' AND b.CRVGroup NOT LIKE '%SC') OR (@SubCritical = 'N' AND b.CRVGroup LIKE '%SC'))
		AND (@SteamGasOil IS NULL OR b.SteamGasOil = @SteamGasOil)
		AND (@CombinedCycle IS NULL OR b.CombinedCycle = @CombinedCycle)
		AND (@CogenElec IS NULL OR b.CogenElec = @CogenElec)
		AND (@GasOilNMCGroup IS NULL OR b.GasOilNMCGroup = @GasOilNMCGroup)
		AND (@CCNMCGroup IS NULL OR b.CCNMCGroup = @CCNMCGroup)
		AND (@NCF2YrGroup IS NULL OR b.NCF2Yr = @NCF2YrGroup)
		AND (@FiringTempGroup IS NULL OR b.FTEMP = @FiringTempGroup)
		AND (@CTGS IS NULL OR b.CTGS = @CTGS)
		AND (@LTSA IS NULL OR b.LTSA = @LTSA)
		AND (@Regulated IS NULL OR t.Regulated = @Regulated)
		AND (@StartsGroup IS NULL OR EXISTS (SELECT 1 FROM dbo.StartsAnalysis sa WHERE sa.Refnum = rl.Refnum AND sa.StartsGroup = @StartsGroup))
		AND (@MinNMC IS NULL OR EXISTS (SELECT 1 FROM dbo.NERCFactors n WHERE n.Refnum = rl.Refnum AND n.NMC >= @MinNMC))
		AND (@MaxNMC IS NULL OR EXISTS (SELECT 1 FROM dbo.NERCFactors n WHERE n.Refnum = rl.Refnum AND n.NMC <= @MaxNMC))

	RETURN
END
