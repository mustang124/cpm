﻿

CREATE FUNCTION [Report].[AggGeneration](@Refnums Report.GroupedRefnumList READONLY)
RETURNS @results TABLE (SubGroupType varchar(15) NOT NULL, SubGroupID varchar(25) NOT NULL
		, SumPriorAdjNetGWH real NULL
		, SumAdjNetGWH real NULL
		, AvgPriorAdjNetGWH real NULL
		, AvgAdjNetGWH real NULL
		, TotStarts real NULL
		, MeanRunTime real NULL
		, AuxiliaryPcnt real NULL
		, ScrubberPcnt real NULL
		, StationSvcPcnt real NULL
		, InternalUsagePcnt real NULL
		, HeatRate real NULL, HeatRateLHV real NULL
		, HeatRate_Min2 real NULL, HeatRate_Max2 real NULL
		, HeatRateLHV_Min2 real NULL, HeatRateLHV_Max2 real NULL
	, PRIMARY KEY 
	(
		SubGroupType ASC,
		SubGroupID ASC
	)
	)
AS
BEGIN
	INSERT @results(SubGroupType, SubGroupID, SumPriorAdjNetGWH, SumAdjNetGWH, AvgPriorAdjNetGWH, AvgAdjNetGWH
		, TotStarts, MeanRunTime, AuxiliaryPcnt, ScrubberPcnt, StationSvcPcnt, InternalUsagePcnt, HeatRate, HeatRateLHV)
	SELECT r.SubGroupType, r.SubGroupID
		, SumPriorAdjNetGWH = SUM(gtc.PriorAdjNetMWH)*0.001
		, SumAdjNetGWH = SUM(gtc.AdjNetMWH)*0.001
		, AvgPriorAdjNetGWH = GlobalDB.dbo.WtAvgNZ(gtc.PriorAdjNetMWH, r.Weighting)*0.001
		, AvgAdjNetGWH = GlobalDB.dbo.WtAvgNZ(gtc.AdjNetMWH, r.Weighting)*0.001
		, TotStarts = GlobalDB.dbo.WtAvgNN(gtc.TotStarts, r.Weighting)
		, MeanRunTime = GlobalDB.dbo.WtAvgNZ(gtc.MeanRunTime, gtc.TotStarts*r.Weighting)
		, AuxiliaryPcnt = GlobalDB.dbo.WtAvg(gtc.AuxiliaryPcnt, gtc.PotentialGrossMWH*r.Weighting)
		, ScrubberPcnt = GlobalDB.dbo.WtAvg(gtc.ScrubberPcnt, gtc.PotentialGrossMWH*r.Weighting)
		, StationSvcPcnt = GlobalDB.dbo.WtAvg(gtc.StationSvcPcnt, gtc.PotentialGrossMWH*r.Weighting)
		, InternalUsagePcnt = GlobalDB.dbo.WtAvg(gtc.InternalUsagePcnt, gtc.PotentialGrossMWH*r.Weighting)
		, HeatRate = GlobalDB.dbo.WtAvgNZ(gtc.HeatRate, gtc.AdjNetMWH*r.Weighting)
		, HeatRateLHV = GlobalDB.dbo.WtAvgNZ(gtc.HeatRateLHV, gtc.AdjNetMWH*r.Weighting)
	FROM @Refnums r INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID

	; WITH RangeData (SubGroupType, SubGroupID, HeatRate, HeatRateLHV)
	AS (SELECT r.SubGroupType, r.SubGroupID, c.HeatRate, c.HeatRateLHV
		FROM @Refnums r INNER JOIN dbo.GenerationTotCalc c ON c.Refnum = r.Refnum)
	UPDATE results
	SET HeatRate_Min2 = (SELECT AVG(HeatRate) FROM (SELECT TOP 2 HeatRate FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY HeatRate ASC) x)
	, HeatRate_Max2 = (SELECT AVG(HeatRate) FROM (SELECT TOP 2 HeatRate FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY HeatRate DESC) x)
	, HeatRateLHV_Min2 = (SELECT AVG(HeatRateLHV) FROM (SELECT TOP 2 HeatRateLHV FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY HeatRateLHV ASC) x)
	, HeatRateLHV_Max2 = (SELECT AVG(HeatRateLHV) FROM (SELECT TOP 2 HeatRateLHV FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY HeatRateLHV DESC) x)
	FROM @results results

	RETURN
END


