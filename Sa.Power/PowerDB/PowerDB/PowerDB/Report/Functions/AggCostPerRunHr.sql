﻿CREATE FUNCTION [Report].[AggCostPerRunHr](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, TotalCostPerRunHr = GlobalDB.dbo.WtAvg(c.Total, r.Weighting)
		, FixedCostPerRunHr = GlobalDB.dbo.WtAvg(c.Fixed, r.Weighting)
		, VarLessFuelPerRunHr = GlobalDB.dbo.WtAvg(c.VarLessFuel, r.Weighting)
		, TotMaintPerRunHr = GlobalDB.dbo.WtAvgNN(m.Maint, r.Weighting)
		, NonOHMaintPerRunHr = GlobalDB.dbo.WtAvgNN(m.NonOverhaul, r.Weighting)
		, OHMaintPerRunHr = GlobalDB.dbo.WtAvgNN(m.Overhaul, r.Weighting)
		, LTSAPerRunHr = GlobalDB.dbo.WtAvgNN(m.LTSA, r.Weighting)
	FROM @refnums r INNER JOIN dbo.CostPerRunHr c ON c.Refnum = r.Refnum
	LEFT JOIN dbo.MaintPerRunHr m ON m.Refnum = r.Refnum AND m.Maint > 0
	GROUP BY r.SubGroupType, r.SubGroupID
)
