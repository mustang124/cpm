﻿

CREATE FUNCTION [Report].[AggMaintTotCalc](@Refnums Report.GroupedRefnumList READONLY)
RETURNS @results TABLE (SubGroupType varchar(15) NOT NULL, SubGroupID varchar(25) NOT NULL
		, AnnMaintCostMWH real NULL
		, AnnMaintCostMWH_Min2 real NULL, AnnMaintCostMWH_Max2 real NULL
		, AnnNonOHCostMWH real NULL
		, AnnOHCostMWH real NULL
		, AnnOHProjCostMWH real NULL
		, AnnLTSACostMWH real NULL
		, AnnLTSACostMWH_NZ real NULL
		, AnnMaintCostMW real NULL
		, AnnMaintCostMW_Min2 real NULL, AnnMaintCostMW_Max2 real NULL
		, AnnNonOHCostMW real NULL
		, AnnOHCostMW real NULL
		, AnnOHProjCostMW real NULL
		, AnnLTSACostMW real NULL
		, AnnLTSACostMW_NZ real NULL
		, AnnMaintCostMBTU real NULL
		, AnnMaintCostMBTU_Min2 real NULL, AnnMaintCostMBTU_Max2 real NULL
		, AnnNonOHCostMBTU real NULL
		, AnnOHCostMBTU real NULL
		, AnnOHProjCostMBTU real NULL
		, AnnLTSACostMBTU real NULL
		, AnnLTSACostMBTU_NZ real NULL
		, AnnMaintCostEGC real NULL
		, AnnMaintCostEGC_Min2 real NULL, AnnMaintCostEGC_Max2 real NULL
		, AnnNonOHCostEGC real NULL
		, AnnOHCostEGC real NULL
		, AnnOHProjCostEGC real NULL
		, AnnLTSACostEGC real NULL
		, AnnLTSACostEGC_NZ real NULL
	, PRIMARY KEY 
	(
		SubGroupType ASC,
		SubGroupID ASC
	)
	)
AS
BEGIN
	INSERT @results(SubGroupType, SubGroupID
		, AnnMaintCostMWH, AnnNonOHCostMWH, AnnOHCostMWH, AnnOHProjCostMWH, AnnLTSACostMWH, AnnLTSACostMWH_NZ
		, AnnMaintCostMW, AnnNonOHCostMW, AnnOHCostMW, AnnOHProjCostMW, AnnLTSACostMW, AnnLTSACostMW_NZ
		, AnnMaintCostMBTU, AnnNonOHCostMBTU, AnnOHCostMBTU, AnnOHProjCostMBTU, AnnLTSACostMBTU, AnnLTSACostMBTU_NZ
		, AnnMaintCostEGC, AnnNonOHCostEGC, AnnOHCostEGC, AnnOHProjCostEGC, AnnLTSACostEGC, AnnLTSACostEGC_NZ)
	SELECT r.SubGroupType, r.SubGroupID
		, AnnMaintCostMWH = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostMWH, gtc.AdjNetMWH2Yr*r.Weighting)
		, AnnNonOHCostMWH = GlobalDB.dbo.WtAvg(mtc.AnnNonOHCostMWH, gtc.AdjNetMWH2Yr*r.Weighting)
		, AnnOHCostMWH = GlobalDB.dbo.WtAvg(mtc.AnnOHCostMWH, gtc.AdjNetMWH2Yr*r.Weighting)
		, AnnOHProjCostMWH = GlobalDB.dbo.WtAvg(mtc.AnnOHProjCostMWH, gtc.AdjNetMWH2Yr*r.Weighting)
		, AnnLTSACostMWH = GlobalDB.dbo.WtAvg(mtc.AnnLTSACostMWH, gtc.AdjNetMWH2Yr*r.Weighting)
		, AnnLTSACostMWH = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostMWH, gtc.AdjNetMWH2Yr*r.Weighting)
		, AnnMaintCostMW = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostMW, nf.NMC2Yr*r.Weighting)
		, AnnNonOHCostMW = GlobalDB.dbo.WtAvg(mtc.AnnNonOHCostMW, nf.NMC2Yr*r.Weighting)
		, AnnOHCostMW = GlobalDB.dbo.WtAvg(mtc.AnnOHCostMW, nf.NMC2Yr*r.Weighting)
		, AnnOHProjCostMW = GlobalDB.dbo.WtAvg(mtc.AnnOHProjCostMW, nf.NMC2Yr*r.Weighting)
		, AnnLTSACostMW = GlobalDB.dbo.WtAvg(mtc.AnnLTSACostMW, nf.NMC2Yr*r.Weighting)
		, AnnLTSACostMW = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostMW, nf.NMC2Yr*r.Weighting)
		, AnnMaintCostMBTU = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostMBTU, gtc.TotalOutputMBTU2Yr*r.Weighting)
		, AnnNonOHCostMBTU = GlobalDB.dbo.WtAvg(mtc.AnnNonOHCostMBTU, gtc.TotalOutputMBTU2Yr*r.Weighting)
		, AnnOHCostMBTU = GlobalDB.dbo.WtAvg(mtc.AnnOHCostMBTU, gtc.TotalOutputMBTU2Yr*r.Weighting)
		, AnnOHProjCostMBTU = GlobalDB.dbo.WtAvg(mtc.AnnOHProjCostMBTU, gtc.TotalOutputMBTU2Yr*r.Weighting)
		, AnnLTSACostMBTU = GlobalDB.dbo.WtAvg(mtc.AnnLTSACostMBTU, gtc.TotalOutputMBTU2Yr*r.Weighting)
		, AnnLTSACostMBTU = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostMBTU, gtc.TotalOutputMBTU2Yr*r.Weighting)
		, AnnMaintCostEGC = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostEGC, gs.EGC*r.Weighting)
		, AnnNonOHCostEGC = GlobalDB.dbo.WtAvg(mtc.AnnNonOHCostEGC, gs.EGC*r.Weighting)
		, AnnOHCostEGC = GlobalDB.dbo.WtAvg(mtc.AnnOHCostEGC, gs.EGC*r.Weighting)
		, AnnOHProjCostEGC = GlobalDB.dbo.WtAvg(mtc.AnnOHProjCostEGC, gs.EGC*r.Weighting)
		, AnnLTSACostEGC = GlobalDB.dbo.WtAvg(mtc.AnnLTSACostEGC, gs.EGC*r.Weighting)
		, AnnLTSACostEGC = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostEGC, gs.EGC*r.Weighting)
	FROM @refnums r INNER JOIN dbo.MaintTotCalc mtc ON mtc.Refnum = r.Refnum
	INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
	INNER JOIN dbo.NERCFactors nf ON nf.Refnum = r.Refnum
	LEFT JOIN dbo.Gensum gs ON gs.Refnum = r.Refnum AND gs.EGC > 0
	GROUP BY r.SubGroupType, r.SubGroupID

	; WITH RangeData (SubGroupType, SubGroupID, AnnMaintCostMWH, AnnMaintCostMW, AnnMaintCostMBTU, AnnMaintCostEGC)
	AS (SELECT r.SubGroupType, r.SubGroupID, c.AnnMaintCostMWH, c.AnnMaintCostMW, c.AnnMaintCostMBTU, c.AnnMaintCostEGC
		FROM @Refnums r INNER JOIN dbo.MaintTotCalc c ON c.Refnum = r.Refnum)
	UPDATE results
	SET AnnMaintCostMW_Min2 = (SELECT AVG(AnnMaintCostMW) FROM (SELECT TOP 2 AnnMaintCostMW FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostMW ASC) x)
	, AnnMaintCostMW_Max2 = (SELECT AVG(AnnMaintCostMW) FROM (SELECT TOP 2 AnnMaintCostMW FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostMW DESC) x)
	, AnnMaintCostMBTU_Min2 = (SELECT AVG(AnnMaintCostMBTU) FROM (SELECT TOP 2 AnnMaintCostMBTU FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostMBTU ASC) x)
	, AnnMaintCostMBTU_Max2 = (SELECT AVG(AnnMaintCostMBTU) FROM (SELECT TOP 2 AnnMaintCostMBTU FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostMBTU DESC) x)
	, AnnMaintCostMWH_Min2 = (SELECT AVG(AnnMaintCostMWH) FROM (SELECT TOP 2 AnnMaintCostMWH FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostMWH ASC) x)
	, AnnMaintCostMWH_Max2 = (SELECT AVG(AnnMaintCostMWH) FROM (SELECT TOP 2 AnnMaintCostMWH FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostMWH DESC) x)
	, AnnMaintCostEGC_Min2 = (SELECT AVG(AnnMaintCostEGC) FROM (SELECT TOP 2 AnnMaintCostEGC FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostEGC ASC) x)
	, AnnMaintCostEGC_Max2 = (SELECT AVG(AnnMaintCostEGC) FROM (SELECT TOP 2 AnnMaintCostEGC FROM RangeData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID ORDER BY AnnMaintCostEGC DESC) x)
	FROM @results results

	RETURN
END


