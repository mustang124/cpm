﻿CREATE FUNCTION [Report].[AggNERCFactors](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, SumNMC = SUM(n.NMC), AvgNMC = GlobalDB.dbo.WtAvgNZ(n.NMC, r.Weighting)
		, ServiceHrs = GlobalDB.dbo.WtAvg(n.ServiceHrs, r.Weighting)
		, NCF = GlobalDB.dbo.WtAvg(n.NCF, n.WPH*r.Weighting)
		, NOF = GlobalDB.dbo.WtAvg(n.NOF, n.NOFWtFactor*r.Weighting)
		, EOR = GlobalDB.dbo.WtAvg(n.EOR, n.EORWtFactor*r.Weighting)
		, EUOR = GlobalDB.dbo.WtAvg(n.EUOR, n.EUORWtFactor*r.Weighting)
		, EFOR = GlobalDB.dbo.WtAvg(n.EFOR, n.EFORWtFactor*r.Weighting)
		, EPOR = GlobalDB.dbo.WtAvg(n.EPOR, n.EPORWtFactor*r.Weighting)
		, EUF = GlobalDB.dbo.WtAvg(n.EUF, n.WPH*r.Weighting)
		, EUOF = GlobalDB.dbo.WtAvg(n.EUOF, n.WPH*r.Weighting)
		, EPOF = GlobalDB.dbo.WtAvg(n.EPOF, n.WPH*r.Weighting)
		, MSTUO = GlobalDB.dbo.WtAvgNZ(n.MSTUO, n.NumUO*r.Weighting)
		, MSTFO = GlobalDB.dbo.WtAvgNZ(n.MSTFO, n.NumFO*r.Weighting)
		, MSTMO = GlobalDB.dbo.WtAvgNZ(n.MSTMO, n.NumMO*r.Weighting)
		, ServiceHrs2YrAvg = GlobalDB.dbo.WtAvg(n.ServiceHrs2YrAvg, r.Weighting)
		, NCF2Yr = GlobalDB.dbo.WtAvgNN(n.NCF2Yr, n.WPH2Yr*r.Weighting)
		, NOF2Yr = GlobalDB.dbo.WtAvgNN(n.NOF2Yr, n.NOF2YrWtFactor*r.Weighting)
		, EOR2Yr = GlobalDB.dbo.WtAvg(n.EOR2Yr, n.EOR2YrWtFactor*r.Weighting)
		, EUOR2Yr = GlobalDB.dbo.WtAvg(n.EUOR2Yr, n.EUOR2YrWtFactor*r.Weighting)
		, EFOR2Yr = GlobalDB.dbo.WtAvg(n.EFOR2Yr, n.EFOR2YrWtFactor)
		, EPOR2Yr = GlobalDB.dbo.WtAvg(n.EPOR2Yr, n.EPOR2YrWtFactor*r.Weighting)
		, EUF2Yr = GlobalDB.dbo.WtAvg(n.EUF2Yr, n.WPH2Yr*r.Weighting)
		, EUOF2Yr = GlobalDB.dbo.WtAvg(n.EUOF2Yr, n.WPH2Yr*r.Weighting)
		, EPOF2Yr = GlobalDB.dbo.WtAvg(n.EPOF2Yr, n.WPH2Yr*r.Weighting)
		, Age = GlobalDB.dbo.WtAvgNZ(n.Age, r.Weighting)
	FROM @Refnums r INNER JOIN dbo.NERCFactors n ON n.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

