﻿CREATE FUNCTION [Report].[AggAshHandlingCosts](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT d.SubGroupType, d.SubGroupID, d.DataType, AshCostOp = ash.AshCostOp/d.DivValue, AshCostDisp = ash.AshCostDisp/d.DivValue, TotAshHandling = rev.TotAshHandling/d.DivValue
	FROM (
		SELECT r.SubGroupType, r.SubGroupID, dx.DataType, DivValue = SUM(dx.DivValue)
		FROM @Refnums r
		INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
		INNER JOIN dbo.NERCFactors nf ON nf.Refnum = r.Refnum
		CROSS APPLY (VALUES 
			('$/MWH', gtc.AdjNetMWH/1000),
			('K$/MW', nf.NMC),
			('$/MBTU', gtc.TotalOutputMBTU/1000)
			) dx(DataType, DivValue)
		GROUP BY r.SubGroupType, r.SubGroupID, dx.DataType) d
	LEFT JOIN (
		SELECT r.SubGroupType, r.SubGroupID, TotAshHandling = SUM(mc.TotAshHandling)
		FROM @refnums r INNER JOIN dbo.MiscCalc mc ON mc.Refnum = r.Refnum
		GROUP BY r.SubGroupType, r.SubGroupID) rev ON rev.SubGroupType = d.SubGroupType AND rev.SubGroupID = d.SubGroupID
	LEFT JOIN (
		SELECT r.SubGroupType, r.SubGroupID, AshCostOp = SUM(misc.AshCostOP), AshCostDisp = SUM(misc.AshCostDisp)
		FROM @refnums r INNER JOIN dbo.Misc misc ON misc.Refnum = r.Refnum
		GROUP BY r.SubGroupType, r.SubGroupID) ash ON ash.SubGroupType = d.SubGroupType AND ash.SubGroupID = d.SubGroupID
	WHERE d.DivValue > 0
)

