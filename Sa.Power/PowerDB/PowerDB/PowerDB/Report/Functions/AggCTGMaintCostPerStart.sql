﻿CREATE FUNCTION [Report].[AggCTGMaintCostPerStart](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, AvgTotStarts = GlobalDB.dbo.WtAvg(m.AvgTotStarts, r.Weighting)
		, LTSAPerStart = GlobalDB.dbo.WtAvg(m.LTSAPerStart, m.AvgTotStarts*r.Weighting)
		, AnnOHPerStart = GlobalDB.dbo.WtAvg(m.AnnOHPerStart, m.AvgTotStarts*r.Weighting)
		, NonOHPerStart = GlobalDB.dbo.WtAvg(m.NonOHPerStart, m.AvgTotStarts*r.Weighting)
		, MaintCostPerStart = GlobalDB.dbo.WtAvg(m.MaintCostPerStart, m.AvgTotStarts*r.Weighting)
	FROM @refnums r INNER JOIN dbo.CTGMaintPerStart m ON m.Refnum = r.Refnum
	WHERE m.AvgTotStarts > 0 AND m.MaintCostPerStart > 0
	GROUP BY r.SubGroupType, r.SubGroupID
)

