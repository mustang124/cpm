﻿CREATE FUNCTION [Report].[AggAbsenceOCC](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT SubGroupType, SubGroupID, TotOCCAbsPcnt = GlobalDB.dbo.WtAvg(TotOCCAbsPcnt, SiteSTH), OCCAbsPcnt_HOL = GlobalDB.dbo.WtAvg(HOL, SiteSTH), OCCAbsPcnt_VAC = GlobalDB.dbo.WtAvg(VAC, SiteSTH), OCCAbsPcnt_HOLVAC = GlobalDB.dbo.WtAvg(HOLVAC, SiteSTH)
		, OCCAbsPcnt_ONJOBC = GlobalDB.dbo.WtAvg(ONJOBC, SiteSTH), OCCAbsPcnt_ONJOB = GlobalDB.dbo.WtAvg(ONJOB, SiteSTH), OCCAbsPcnt_SICK = GlobalDB.dbo.WtAvg(SICK, SiteSTH), OCCAbsPcnt_LEGAL = GlobalDB.dbo.WtAvg(LEGAL, SiteSTH)
		, OCCAbsPcnt_OthExc = GlobalDB.dbo.WtAvg(OTHEXC, SiteSTH), OCCAbsPcnt_Unexc = GlobalDB.dbo.WtAvg(UNEXC, SiteSTH), OCCAbsPcnt_Other = GlobalDB.dbo.WtAvg(OTHER, SiteSTH)
	FROM (
	SELECT pvt.SubGroupType, pvt.SubGroupID, pvt.Refnum, pvt.TotOCCAbsPcnt, [HOL],[VAC],[HOLVAC],[ONJOBC],[ONJOB],[SICK],[LEGAL],[OTHEXC],[UNEXC],[OTHER],p.SiteSTH
	FROM (SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, TotOCCAbsPcnt = t.OCCAbsPcnt, a.AbsCategory, a.OCCAbsPcnt FROM @Refnums r INNER JOIN dbo.AbsenceTotCalc t ON t.Refnum = r.Refnum INNER JOIN dbo.AbsenceCalc a ON a.Refnum = t.Refnum) src
	PIVOT (SUM(OCCAbsPcnt) FOR AbsCategory IN ([HOLVAC],[ONJOB],[OTHER],[UNEXC],[ONJOBC],[HOL],[LEGAL],[VAC],[SICK],[OTHEXC])) pvt
	INNER JOIN dbo.PersSTCalc p ON p.Refnum = pvt.Refnum AND p.SectionID = 'TOCC'
	) occ GROUP BY SubGroupType, SubGroupID
)

