﻿CREATE FUNCTION [Report].[AggCTGData](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, NMC = GlobalDB.dbo.WtAvgNZ(ctg.NMC, r.Weighting)
		, PriorNetGWH = GlobalDB.dbo.WtAvgNZ(ctg.PriorNetMWH, r.Weighting)/1000
		, NetGWH = GlobalDB.dbo.WtAvgNZ(ctg.NetMWH, r.Weighting)/1000
		, ServiceHrs = GlobalDB.dbo.WtAvgNZ(ctg.ServiceHrs, r.Weighting)
		, NCF2Yr = GlobalDB.dbo.WtAvg(ctg.NCF2Yr, ctg.WPH2Yr*r.Weighting)
		, NOF2Yr = GlobalDB.dbo.WtAvg(ctg.NOF2Yr, ctg.NOF2YrWtFactor*r.Weighting)
		, SuccessPcnt = GlobalDB.dbo.WtAvg(ctg.SuccessPcnt, ctg.TotalOpps*r.Weighting)
		, ForcedPcnt = GlobalDB.dbo.WtAvg(ctg.ForcedPcnt, ctg.TotalOpps*r.Weighting)
		, MaintPcnt = GlobalDB.dbo.WtAvg(ctg.MaintPcnt, ctg.TotalOpps*r.Weighting)
		, StartupPcnt = GlobalDB.dbo.WtAvg(ctg.StartupPcnt, ctg.TotalOpps*r.Weighting)
		, DeratePcnt = GlobalDB.dbo.WtAvg(ctg.DeratePcnt, ctg.TotalOpps*r.Weighting)
		, HeatRate = GlobalDB.dbo.WtAvgNZ(ctg.HeatRate, ctg.NetMWH*r.Weighting)
		, HeatRateLHV = GlobalDB.dbo.WtAvgNZ(ctg.HeatRateLHV, ctg.NetMWH*r.Weighting)
		, FiringTemp = (SELECT GlobalDB.dbo.WtAvgNN(CTGData.FiringTemp, r2.Weighting) FROM @Refnums r2 INNER JOIN dbo.CTGData CTGData ON CTGData.Refnum = r2.Refnum WHERE r2.SubGroupType = r.SubGroupType AND r2.SubGroupID = r.SubGroupID)
	FROM @Refnums r INNER JOIN dbo.CTGSummary ctg ON ctg.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

