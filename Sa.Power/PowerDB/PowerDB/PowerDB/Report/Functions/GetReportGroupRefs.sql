﻿
CREATE FUNCTION [Report].[GetReportGroupRefs](@ReportRefnum ReportRefnum)
RETURNS TABLE
AS

	RETURN 
	(
		SELECT Refnum FROM Report.ReportGroupRefs WHERE ReportRefnum = @ReportRefnum
		UNION
		SELECT Refnum FROM dbo.TSort WHERE Refnum = @ReportRefnum
	)


