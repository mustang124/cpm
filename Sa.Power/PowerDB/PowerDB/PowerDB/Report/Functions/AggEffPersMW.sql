﻿

CREATE FUNCTION [Report].[AggEffPersMW](@Refnums Report.GroupedRefnumList READONLY)
RETURNS @results TABLE (SubGroupType varchar(15) NOT NULL, SubGroupID varchar(25) NOT NULL
	, PersType varchar(20) NOT NULL
	, OCCOps real NULL
	, OCCSTNOHMaint real NULL
	, OCCNOHMaint real NULL
	, OCCInsp real NULL
	, OCCLumpSum real NULL
	, OCCLTSA real NULL
	, OCCOverhaul real NULL
	, OCCSTMaint real NULL
	, OCCTech real NULL
	, OCCAdmin real NULL
	, OCCNonMaint real NULL
	, OCCTotal real NULL
	, MPSOps real NULL
	, MPSSTNOHMaint real NULL
	, MPSNOHMaint real NULL
	, MPSInsp real NULL
	, MPSLumpSum real NULL
	, MPSLTSA real NULL
	, MPSOverhaul real NULL
	, MPSSTMaint real NULL
	, MPSTech real NULL
	, MPSAdmin real NULL
	, MPSNonMaint real NULL
	, MPSTotal real NULL
	, TotalPers real NULL
	, PRIMARY KEY (SubGroupType, SubGroupID, PersType)
)
AS
BEGIN
	DECLARE @RefValues TABLE (
		SubGroupType varchar(25) NOT NULL,
		SubGroupID varchar(25) NOT NULL,
		Refnum dbo.Refnum NOT NULL,
		Weighting real NOT NULL,
		PersCat varchar(20) NOT NULL,
		NMC real NULL,
		SiteEffPersMW real NULL,
		CentralEffPersMW real NULL,
		AGEffPersMW real NULL,
		ContractEffPersMW real NULL,
		TotEffPersMW real NULL,
		PRIMARY KEY( SubGroupType, SubGroupID, Refnum, PersCat))
	INSERT @RefValues (SubGroupType, SubGroupID, Refnum, Weighting, PersCat, NMC, SiteEffPersMW, CentralEffPersMW, AGEffPersMW, ContractEffPersMW, TotEffPersMW)
	SELECT pc.SubGroupType, pc.SubGroupID, pc.Refnum, pc.Weighting, pc.PersCat, nf.NMC
		, SiteEffPersMW = pc.SiteEffPers/nf.NMC*100, CentralEffPersMW = pc.CentralEffPers/nf.NMC*100, AGEffPersMW = pc.AGEffPers/nf.NMC*100
		, ContractEffPersMW = pc.ContractEffPers/nf.NMC*100, TotEffPersMW = pc.TotEffPers/nf.NMC*100
	FROM (
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, p.PersCat, p.SiteEffPers, p.CentralEffPers, p.AGEffPers, p.ContractEffPers, p.TotEffPers
		FROM @refnums r INNER JOIN dbo.Pers p ON p.Refnum = r.Refnum
		WHERE p.PersCat NOT IN ('OCC-Maint%', 'MPS-Maint%')
		UNION
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, noh.STCat, 
			SiteEffPers = SUM(p.SiteEffPers*r.Weighting), CentralEffPers = SUM(p.CentralEffPers*r.Weighting), AGEffPers = SUM(p.AGEffPers*r.Weighting), 
			ContractEffPers = SUM(p.ContractEffPers*r.Weighting), TotEffPers = SUM(p.TotEffPers*r.Weighting)
		FROM @refnums r INNER JOIN dbo.Pers p ON p.Refnum = r.Refnum
		INNER JOIN (VALUES ('OCC-NOHMaint+Insp','OCC-NOHMaint'),('OCC-NOHMaint+Insp','OCC-Insp')
						,  ('MPS-NOHMaint+Insp','MPS-NOHMaint'),('MPS-NOHMaint+Insp','MPS-Insp')) noh(STCat, PersCat) ON noh.PersCat = p.PersCat 
		GROUP BY r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, noh.STCat
		UNION
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, p.SectionID, p.SiteEffPers, p.CentralEffPers, p.AGEffPers, p.ContractEffPers, p.TotEffPers
		FROM @refnums r INNER JOIN dbo.PersSTCalc p ON p.Refnum = r.Refnum
	) pc INNER JOIN dbo.NERCFactors nf ON nf.Refnum = pc.Refnum
	WHERE nf.NMC > 0

	INSERT @results (SubGroupType, SubGroupID, PersType
		, OCCOps, OCCSTNOHMaint, OCCNOHMaint, OCCInsp, OCCLumpSum, OCCLTSA, OCCOverhaul, OCCSTMaint, OCCTech, OCCAdmin, OCCNonMaint, OCCTotal
		, MPSOps, MPSSTNOHMaint, MPSNOHMaint, MPSInsp, MPSLumpSum, MPSLTSA, MPSOverhaul, MPSSTMaint, MPSTech, MPSAdmin, MPSNonMaint, MPSTotal
		, TotalPers)
	SELECT SubGroupType, SubGroupID, PersType,
		OCCOps = [OCC-Oper], OCCSTNOHMaint = [OCC-NOHMaint+Insp], OCCNOHMaint = [OCC-NOHMaint], OCCInsp = [OCC-Insp], 
		OCCLumpSum = [OCC-LSCAdj], OCCLTSA = [OCC-LTSA], OCCOverhaul = [OCC-OHAdj], OCCSTMaint = [OCCM],
		OCCTech = [OCC-Tech], OCCAdmin = [OCC-Admin], OCCNonMaint = [OCC-NonMaint], OCCTotal = [TOCC],
		MPSOps = [MPS-Oper], MPSSTNOHMaint = [MPS-NOHMaint+Insp], MPSNOHMaint = [MPS-NOHMaint], MPSInsp = [MPS-Insp],
		MPSLumpSum = [MPS-LSCAdj], MPSLTSA = [MPS-LTSA], MPSOverhaul = [MPS-OHAdj], MPSSTMaint = [MPSM],
		MPSTech = [MPS-Tech], MPSAdmin = [MPS-Admin], MPSNonMaint = [MPS-NonMaint], MPSTotal = [TMPS],
		TotalPers = [TP]
	FROM (
		SELECT unp.SubGroupType, unp.SubGroupID, unp.PersCat, unp.PersType, EffPersMW
		FROM (
			SELECT SubGroupType, SubGroupID, PersCat, 
				[Site] = GlobalDB.dbo.WtAvg(SiteEffPersMW, NMC*Weighting), 
				Central = GlobalDB.dbo.WtAvg(CentralEffPersMW, NMC*Weighting),
				AG = GlobalDB.dbo.WtAvg(AGEffPersMW, NMC*Weighting),
				[Contract] = GlobalDB.dbo.WtAvg(ContractEffPersMW, NMC*Weighting),
				Total = GlobalDB.dbo.WtAvg(TotEffPersMW, NMC*Weighting)
			FROM @RefValues
			GROUP BY SubGroupType, SubGroupID, PersCat) src
		UNPIVOT (EffPersMW FOR PersType IN ([Site], Central, AG, [Contract], Total)) unp
	) c
	PIVOT (AVG(EffPersMW) FOR PersCat IN (
		[OCC-Oper],[OCC-NOHMaint+Insp],[OCC-NOHMaint],[OCC-Insp],[OCC-LSCAdj],[OCC-LTSA],[OCC-OHAdj],[OCCM],[OCC-Tech],[OCC-Admin],[OCC-NonMaint],[TOCC],
		[MPS-Oper],[MPS-NOHMaint+Insp],[MPS-NOHMaint],[MPS-Insp],[MPS-LSCAdj],[MPS-LTSA],[MPS-OHAdj],[MPSM],[MPS-Tech],[MPS-Admin],[MPS-NonMaint],[TMPS],
		[TP])) pvt

	RETURN
END


