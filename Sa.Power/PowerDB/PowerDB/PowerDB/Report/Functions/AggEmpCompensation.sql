﻿CREATE FUNCTION [Report].[AggEmpCompensation](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, OCCAnnComp = GlobalDB.dbo.WtAvgNZ(GenSum.OCCAnnComp, r.Weighting)*0.001
		, OCCBenPcnt = GlobalDB.dbo.WtAvg(GenSum.OCCBenPcnt, OpExCalc.OCCWages*r.Weighting)
		, MPSAnnComp = GlobalDB.dbo.WtAvgNZ(GenSum.MPSAnnComp, r.Weighting)*0.001
		, MPSBenPcnt = GlobalDB.dbo.WtAvg(GenSum.MPSBenPcnt, OpExCalc.MPSWages*r.Weighting)
	FROM @refnums r INNER JOIN dbo.Gensum Gensum ON Gensum.Refnum = r.Refnum
	INNER JOIN dbo.OpexCalc OpexCalc ON OpexCalc.Refnum = r.Refnum AND OpexCalc.DataType = 'ADJ'
	GROUP BY r.SubGroupType, r.SubGroupID
)

