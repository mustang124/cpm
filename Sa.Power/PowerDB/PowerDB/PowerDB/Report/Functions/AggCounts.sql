﻿CREATE FUNCTION [Report].[AggCounts](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, UnitCount = COUNT(DISTINCT t.Refnum)
		, CTGCount = COUNT(n.TurbineID)
		, SiteCount = COUNT(DISTINCT t.SiteID)
		, CompanyCount = COUNT(DISTINCT t.CompanyID)
	FROM @refnums r INNER JOIN dbo.TSort t ON t.Refnum = r.Refnum
	LEFT JOIN dbo.NERCTurbine n ON n.Refnum = r.Refnum AND n.TurbineType IN ('CTG','CC')
	GROUP BY r.SubGroupType, r.SubGroupID
)

