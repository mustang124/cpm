﻿CREATE FUNCTION [Report].[AggScrubberNonMaintCosts](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID, dx.DataType
		, ScrCostLime = SUM(s.ScrCostLime)/SUM(dx.DivValue), ScrCostChem = SUM(s.ScrCostChem)/SUM(dx.DivValue), ScrubberPowerCost = SUM(s.ScrubberPowerCost)/SUM(dx.DivValue)
		, ScrCostNMaint = SUM(s.ScrCostNMaint)/SUM(dx.DivValue), TotScrubberNonMaint = SUM(s.TotScrubberNonMaint)/SUM(dx.DivValue)
	FROM @refnums r INNER JOIN dbo.ScrubberCosts s ON s.Refnum = r.Refnum
	INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
	INNER JOIN dbo.NERCFactors nf ON nf.Refnum = r.Refnum
	CROSS APPLY (VALUES 
			('$/MWH', gtc.AdjNetMWH/1000),
			('K$/MW', nf.NMC),
			('$/MBTU', gtc.TotalOutputMBTU/1000)
			) dx(DataType, DivValue)
	WHERE s.TotScrubberNonMaint > 0
	GROUP BY r.SubGroupType, r.SubGroupID, dx.DataType
)

