﻿CREATE FUNCTION [Report].[AggEquipNonOHCost](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT ac.SubGroupType, ac.SubGroupID, ac.Component
		, AnnNonOHCostMWH = GlobalDB.dbo.WtAvg(ac.AnnNonOHCostMWH, gtc.AdjNetMWH2Yr*ac.Weighting)
		, AnnNonOHCostMW = GlobalDB.dbo.WtAvg(ac.AnnNonOHCostMW, nf.NMC2Yr*ac.Weighting)
		, AnnNonOHCostMBTU = GlobalDB.dbo.WtAvg(ac.AnnNonOHCostMBTU, gtc.TotalOutputMBTU2Yr*ac.Weighting)
		, COUNT(DISTINCT t.Refnum) AS UnitCount
		, COUNT(DISTINCT t.SiteID) AS SiteCount
		, COUNT(DISTINCT t.CompanyID) AS CompanyCount
	FROM (
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, Component = ISNULL(cc.MapToComponent,noh.Component)
			, AnnNonOHCostMWH = SUM(noh.AnnNonOHCostMWH)
			, AnnNonOHCostMW = SUM(noh.AnnNonOHCostMW)
			, AnnNonOHCostMBTU = SUM(noh.AnnNonOHCostMBTU)
		FROM @Refnums r INNER JOIN dbo.NonOHMaint noh ON noh.Refnum = r.Refnum
		LEFT JOIN (VALUES ('STG', 'STG-GEN')
			, ('BLR-AIR','BOIL'), ('BLR-BOIL','BOIL'), ('BLR-COND','BOIL')
			, ('CTG-SCR','SCR')
			, ('HRSG','CTG-HRSG')
			, ('DQWS','WSS'), ('BOILH2O','WSS')
			, ('FHF','FUELDEL')
		) cc(Component, MapToComponent) ON cc.Component = noh.Component
		--WHERE noh.AnnNonOHCost > 0
		GROUP BY r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, ISNULL(cc.MapToComponent,noh.Component)
	) ac
	INNER JOIN dbo.TSort t ON t.Refnum = ac.Refnum
	INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	INNER JOIN dbo.NERCFactors nf ON nf.Refnum = t.Refnum
	GROUP BY ac.SubGroupType, ac.SubGroupID, ac.Component
)

