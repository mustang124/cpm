﻿

CREATE FUNCTION [Report].[AggTotEffPers](@Refnums Report.GroupedRefnumList READONLY)
RETURNS @results TABLE (SubGroupType varchar(15) NOT NULL, SubGroupID varchar(25) NOT NULL
		, DataType varchar(3) NOT NULL
		, TotEffPers real NULL
		, TotEffPers_Min2 real NULL
		, TotEffPers_Max2 real NULL
	, PRIMARY KEY 
	(
		SubGroupType ASC,
		SubGroupID ASC,
		DataType ASC
	)
)
AS
BEGIN
	DECLARE @UnitData TABLE (
		SubGroupType varchar(15) NOT NULL, 
		SubGroupID varchar(25) NOT NULL,
		Refnum dbo.Refnum NOT NULL,
		Weighting real NOT NULL,
		DataType varchar(3) NOT NULL,
		TotEffPers real NULL,
		Divisor real NULL,
		PRIMARY KEY (SubGroupType ASC, SubGroupID ASC, Refnum ASC, DataType ASC)
	)
	INSERT @UnitData (SubGroupType, SubGroupID, Refnum, Weighting, DataType, TotEffPers, Divisor)
	SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, px.DataType, px.TotEffPers, px.Divisor
	FROM @refnums r INNER JOIN dbo.PersSTCalc p ON p.Refnum = r.Refnum AND p.SectionID = 'TP'
	INNER JOIN dbo.GenSum g ON g.Refnum = p.Refnum
	INNER JOIN dbo.NERCFactors n ON n.Refnum = r.Refnum
	CROSS APPLY (VALUES
		('MW', g.TotEffPersMW, n.NMC),
		('EGC', g.TotEffPersEGC, g.EGC),
		('MWH', g.TotEffPersMWH, g.AdjNetMWH)
	) px(DataType, TotEffPers, Divisor)
	WHERE px.TotEffPers > 0

	INSERT @results(SubGroupType, SubGroupID, DataType, TotEffPers)
	SELECT SubGroupType, SubGroupID, DataType, GlobalDb.dbo.WtAvgNZ(TotEffPers, Divisor*Weighting)
	FROM @UnitData
	GROUP BY SubGroupType, SubGroupID, DataType

	UPDATE results
	SET TotEffPers_Min2 = (SELECT AVG(TotEffPers) FROM (SELECT TOP 2 TotEffPers FROM @UnitData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID AND rd.DataType = results.DataType ORDER BY TotEffPers ASC) x)
	, TotEffPers_Max2 = (SELECT AVG(TotEffPers) FROM (SELECT TOP 2 TotEffPers FROM @UnitData rd WHERE rd.SubGroupType = results.SubGroupType AND rd.SubGroupID = results.SubGroupID AND rd.DataType = results.DataType ORDER BY TotEffPers DESC) x)
	FROM @results results

	RETURN
END


