﻿namespace Power_Calcs
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtSiteID = new System.Windows.Forms.TextBox();
			this.lblSiteID = new System.Windows.Forms.Label();
			this.cbCalculations = new System.Windows.Forms.CheckBox();
			this.cbCopyClean = new System.Windows.Forms.CheckBox();
			this.cbClientTables = new System.Windows.Forms.CheckBox();
			this.cbSumCalcs = new System.Windows.Forms.CheckBox();
			this.btnCalculate = new System.Windows.Forms.Button();
			this.btnAddToQueue = new System.Windows.Forms.Button();
			this.btnCalcQueue = new System.Windows.Forms.Button();
			this.lblStatus = new System.Windows.Forms.Label();
			this.cbValidatedInput = new System.Windows.Forms.CheckBox();
			this.btnGetStatus = new System.Windows.Forms.Button();
			this.lblGADSDate = new System.Windows.Forms.Label();
			this.lblTimelineDate = new System.Windows.Forms.Label();
			this.lblROODSDate = new System.Windows.Forms.Label();
			this.GADSDate = new System.Windows.Forms.Label();
			this.TimelineDate = new System.Windows.Forms.Label();
			this.ROODSDate = new System.Windows.Forms.Label();
			this.lblFileDate = new System.Windows.Forms.Label();
			this.lblUploadDate = new System.Windows.Forms.Label();
			this.lblPricingHub = new System.Windows.Forms.Label();
			this.lblHubDate = new System.Windows.Forms.Label();
			this.FileDate = new System.Windows.Forms.Label();
			this.UploadDate = new System.Windows.Forms.Label();
			this.PricingHub = new System.Windows.Forms.Label();
			this.HubDate = new System.Windows.Forms.Label();
			this.lblCalcsDate = new System.Windows.Forms.Label();
			this.CalcsDate = new System.Windows.Forms.Label();
			this.lblStatusTitle = new System.Windows.Forms.Label();
			this.btnUpdateTimeline = new System.Windows.Forms.Button();
			this.btnUploadFile = new System.Windows.Forms.Button();
			this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
			this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
			this.SuspendLayout();
			// 
			// txtSiteID
			// 
			this.txtSiteID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtSiteID.Location = new System.Drawing.Point(90, 10);
			this.txtSiteID.Name = "txtSiteID";
			this.txtSiteID.Size = new System.Drawing.Size(200, 26);
			this.txtSiteID.TabIndex = 0;
			// 
			// lblSiteID
			// 
			this.lblSiteID.AutoSize = true;
			this.lblSiteID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSiteID.Location = new System.Drawing.Point(14, 13);
			this.lblSiteID.Name = "lblSiteID";
			this.lblSiteID.Size = new System.Drawing.Size(58, 20);
			this.lblSiteID.TabIndex = 1;
			this.lblSiteID.Text = "SiteID:";
			// 
			// cbCalculations
			// 
			this.cbCalculations.AutoSize = true;
			this.cbCalculations.Checked = true;
			this.cbCalculations.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbCalculations.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbCalculations.Location = new System.Drawing.Point(364, 12);
			this.cbCalculations.Name = "cbCalculations";
			this.cbCalculations.Size = new System.Drawing.Size(114, 24);
			this.cbCalculations.TabIndex = 4;
			this.cbCalculations.Text = "Calculations";
			this.cbCalculations.UseVisualStyleBackColor = true;
			// 
			// cbCopyClean
			// 
			this.cbCopyClean.AutoSize = true;
			this.cbCopyClean.Checked = true;
			this.cbCopyClean.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbCopyClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbCopyClean.Location = new System.Drawing.Point(364, 42);
			this.cbCopyClean.Name = "cbCopyClean";
			this.cbCopyClean.Size = new System.Drawing.Size(154, 24);
			this.cbCopyClean.TabIndex = 5;
			this.cbCopyClean.Text = "Copy to Clean DB";
			this.cbCopyClean.UseVisualStyleBackColor = true;
			// 
			// cbClientTables
			// 
			this.cbClientTables.AutoSize = true;
			this.cbClientTables.Checked = true;
			this.cbClientTables.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbClientTables.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbClientTables.Location = new System.Drawing.Point(364, 72);
			this.cbClientTables.Name = "cbClientTables";
			this.cbClientTables.Size = new System.Drawing.Size(119, 24);
			this.cbClientTables.TabIndex = 6;
			this.cbClientTables.Text = "Client Tables";
			this.cbClientTables.UseVisualStyleBackColor = true;
			// 
			// cbSumCalcs
			// 
			this.cbSumCalcs.AutoSize = true;
			this.cbSumCalcs.Checked = true;
			this.cbSumCalcs.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbSumCalcs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbSumCalcs.Location = new System.Drawing.Point(364, 102);
			this.cbSumCalcs.Name = "cbSumCalcs";
			this.cbSumCalcs.Size = new System.Drawing.Size(100, 24);
			this.cbSumCalcs.TabIndex = 7;
			this.cbSumCalcs.Text = "SumCalcs";
			this.cbSumCalcs.UseVisualStyleBackColor = true;
			// 
			// btnCalculate
			// 
			this.btnCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCalculate.Location = new System.Drawing.Point(18, 62);
			this.btnCalculate.Name = "btnCalculate";
			this.btnCalculate.Size = new System.Drawing.Size(140, 42);
			this.btnCalculate.TabIndex = 1;
			this.btnCalculate.Text = "Calculate";
			this.btnCalculate.UseVisualStyleBackColor = true;
			this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
			// 
			// btnAddToQueue
			// 
			this.btnAddToQueue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnAddToQueue.Location = new System.Drawing.Point(164, 62);
			this.btnAddToQueue.Name = "btnAddToQueue";
			this.btnAddToQueue.Size = new System.Drawing.Size(140, 42);
			this.btnAddToQueue.TabIndex = 2;
			this.btnAddToQueue.Text = "Add To Queue";
			this.btnAddToQueue.UseVisualStyleBackColor = true;
			this.btnAddToQueue.Click += new System.EventHandler(this.btnAddToQueue_Click);
			// 
			// btnCalcQueue
			// 
			this.btnCalcQueue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnCalcQueue.Location = new System.Drawing.Point(164, 114);
			this.btnCalcQueue.Name = "btnCalcQueue";
			this.btnCalcQueue.Size = new System.Drawing.Size(140, 42);
			this.btnCalcQueue.TabIndex = 3;
			this.btnCalcQueue.Text = "Calculate Queue";
			this.btnCalcQueue.UseVisualStyleBackColor = true;
			this.btnCalcQueue.Click += new System.EventHandler(this.btnCalcQueue_Click);
			// 
			// lblStatus
			// 
			this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStatus.Location = new System.Drawing.Point(12, 322);
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(991, 60);
			this.lblStatus.TabIndex = 8;
			// 
			// cbValidatedInput
			// 
			this.cbValidatedInput.AutoSize = true;
			this.cbValidatedInput.Checked = true;
			this.cbValidatedInput.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cbValidatedInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cbValidatedInput.Location = new System.Drawing.Point(364, 132);
			this.cbValidatedInput.Name = "cbValidatedInput";
			this.cbValidatedInput.Size = new System.Drawing.Size(136, 24);
			this.cbValidatedInput.TabIndex = 8;
			this.cbValidatedInput.Text = "Validated Input";
			this.cbValidatedInput.UseVisualStyleBackColor = true;
			// 
			// btnGetStatus
			// 
			this.btnGetStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnGetStatus.Location = new System.Drawing.Point(758, 5);
			this.btnGetStatus.Name = "btnGetStatus";
			this.btnGetStatus.Size = new System.Drawing.Size(140, 31);
			this.btnGetStatus.TabIndex = 9;
			this.btnGetStatus.Text = "Update Status";
			this.btnGetStatus.UseVisualStyleBackColor = true;
			this.btnGetStatus.Click += new System.EventHandler(this.btnGetStatus_Click);
			// 
			// lblGADSDate
			// 
			this.lblGADSDate.AutoSize = true;
			this.lblGADSDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblGADSDate.Location = new System.Drawing.Point(566, 42);
			this.lblGADSDate.Name = "lblGADSDate";
			this.lblGADSDate.Size = new System.Drawing.Size(99, 20);
			this.lblGADSDate.TabIndex = 16;
			this.lblGADSDate.Text = "GADS Date:";
			// 
			// lblTimelineDate
			// 
			this.lblTimelineDate.AutoSize = true;
			this.lblTimelineDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTimelineDate.Location = new System.Drawing.Point(566, 72);
			this.lblTimelineDate.Name = "lblTimelineDate";
			this.lblTimelineDate.Size = new System.Drawing.Size(110, 20);
			this.lblTimelineDate.TabIndex = 17;
			this.lblTimelineDate.Text = "Timeline Date:";
			// 
			// lblROODSDate
			// 
			this.lblROODSDate.AutoSize = true;
			this.lblROODSDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblROODSDate.Location = new System.Drawing.Point(566, 102);
			this.lblROODSDate.Name = "lblROODSDate";
			this.lblROODSDate.Size = new System.Drawing.Size(111, 20);
			this.lblROODSDate.TabIndex = 18;
			this.lblROODSDate.Text = "ROODS Date:";
			// 
			// GADSDate
			// 
			this.GADSDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.GADSDate.Location = new System.Drawing.Point(697, 42);
			this.GADSDate.Name = "GADSDate";
			this.GADSDate.Size = new System.Drawing.Size(170, 20);
			this.GADSDate.TabIndex = 19;
			// 
			// TimelineDate
			// 
			this.TimelineDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TimelineDate.Location = new System.Drawing.Point(697, 72);
			this.TimelineDate.Name = "TimelineDate";
			this.TimelineDate.Size = new System.Drawing.Size(170, 20);
			this.TimelineDate.TabIndex = 20;
			// 
			// ROODSDate
			// 
			this.ROODSDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ROODSDate.Location = new System.Drawing.Point(697, 102);
			this.ROODSDate.Name = "ROODSDate";
			this.ROODSDate.Size = new System.Drawing.Size(170, 20);
			this.ROODSDate.TabIndex = 21;
			// 
			// lblFileDate
			// 
			this.lblFileDate.AutoSize = true;
			this.lblFileDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFileDate.Location = new System.Drawing.Point(566, 154);
			this.lblFileDate.Name = "lblFileDate";
			this.lblFileDate.Size = new System.Drawing.Size(118, 20);
			this.lblFileDate.TabIndex = 22;
			this.lblFileDate.Text = "Input File Date:";
			// 
			// lblUploadDate
			// 
			this.lblUploadDate.AutoSize = true;
			this.lblUploadDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblUploadDate.Location = new System.Drawing.Point(566, 185);
			this.lblUploadDate.Name = "lblUploadDate";
			this.lblUploadDate.Size = new System.Drawing.Size(103, 20);
			this.lblUploadDate.TabIndex = 23;
			this.lblUploadDate.Text = "Upload Date:";
			// 
			// lblPricingHub
			// 
			this.lblPricingHub.AutoSize = true;
			this.lblPricingHub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPricingHub.Location = new System.Drawing.Point(566, 251);
			this.lblPricingHub.Name = "lblPricingHub";
			this.lblPricingHub.Size = new System.Drawing.Size(94, 20);
			this.lblPricingHub.TabIndex = 24;
			this.lblPricingHub.Text = "Pricing Hub:";
			// 
			// lblHubDate
			// 
			this.lblHubDate.AutoSize = true;
			this.lblHubDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHubDate.Location = new System.Drawing.Point(566, 284);
			this.lblHubDate.Name = "lblHubDate";
			this.lblHubDate.Size = new System.Drawing.Size(133, 20);
			this.lblHubDate.TabIndex = 25;
			this.lblHubDate.Text = "Pricing Hub Date:";
			// 
			// FileDate
			// 
			this.FileDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FileDate.Location = new System.Drawing.Point(697, 154);
			this.FileDate.Name = "FileDate";
			this.FileDate.Size = new System.Drawing.Size(170, 20);
			this.FileDate.TabIndex = 26;
			// 
			// UploadDate
			// 
			this.UploadDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.UploadDate.Location = new System.Drawing.Point(697, 185);
			this.UploadDate.Name = "UploadDate";
			this.UploadDate.Size = new System.Drawing.Size(170, 20);
			this.UploadDate.TabIndex = 27;
			// 
			// PricingHub
			// 
			this.PricingHub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.PricingHub.Location = new System.Drawing.Point(697, 251);
			this.PricingHub.Name = "PricingHub";
			this.PricingHub.Size = new System.Drawing.Size(170, 20);
			this.PricingHub.TabIndex = 28;
			// 
			// HubDate
			// 
			this.HubDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.HubDate.Location = new System.Drawing.Point(697, 284);
			this.HubDate.Name = "HubDate";
			this.HubDate.Size = new System.Drawing.Size(170, 20);
			this.HubDate.TabIndex = 29;
			// 
			// lblCalcsDate
			// 
			this.lblCalcsDate.AutoSize = true;
			this.lblCalcsDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCalcsDate.Location = new System.Drawing.Point(566, 219);
			this.lblCalcsDate.Name = "lblCalcsDate";
			this.lblCalcsDate.Size = new System.Drawing.Size(91, 20);
			this.lblCalcsDate.TabIndex = 30;
			this.lblCalcsDate.Text = "Calcs Date:";
			// 
			// CalcsDate
			// 
			this.CalcsDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CalcsDate.Location = new System.Drawing.Point(697, 219);
			this.CalcsDate.Name = "CalcsDate";
			this.CalcsDate.Size = new System.Drawing.Size(170, 20);
			this.CalcsDate.TabIndex = 31;
			// 
			// lblStatusTitle
			// 
			this.lblStatusTitle.AutoSize = true;
			this.lblStatusTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStatusTitle.Location = new System.Drawing.Point(656, 10);
			this.lblStatusTitle.Name = "lblStatusTitle";
			this.lblStatusTitle.Size = new System.Drawing.Size(83, 20);
			this.lblStatusTitle.TabIndex = 32;
			this.lblStatusTitle.Text = "STATUS:";
			// 
			// btnUpdateTimeline
			// 
			this.btnUpdateTimeline.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnUpdateTimeline.Location = new System.Drawing.Point(873, 67);
			this.btnUpdateTimeline.Name = "btnUpdateTimeline";
			this.btnUpdateTimeline.Size = new System.Drawing.Size(140, 31);
			this.btnUpdateTimeline.TabIndex = 33;
			this.btnUpdateTimeline.Text = "Update Timeline";
			this.btnUpdateTimeline.UseVisualStyleBackColor = true;
			// 
			// btnUploadFile
			// 
			this.btnUploadFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnUploadFile.Location = new System.Drawing.Point(873, 180);
			this.btnUploadFile.Name = "btnUploadFile";
			this.btnUploadFile.Size = new System.Drawing.Size(140, 31);
			this.btnUploadFile.TabIndex = 34;
			this.btnUploadFile.Text = "Upload Input File";
			this.btnUploadFile.UseVisualStyleBackColor = true;
			// 
			// shapeContainer1
			// 
			this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
			this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
			this.shapeContainer1.Name = "shapeContainer1";
			this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
			this.shapeContainer1.Size = new System.Drawing.Size(1020, 391);
			this.shapeContainer1.TabIndex = 35;
			this.shapeContainer1.TabStop = false;
			// 
			// lineShape1
			// 
			this.lineShape1.Name = "lineShape1";
			this.lineShape1.X1 = 538;
			this.lineShape1.X2 = 538;
			this.lineShape1.Y1 = 8;
			this.lineShape1.Y2 = 305;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1020, 391);
			this.Controls.Add(this.btnUploadFile);
			this.Controls.Add(this.btnUpdateTimeline);
			this.Controls.Add(this.lblStatusTitle);
			this.Controls.Add(this.CalcsDate);
			this.Controls.Add(this.lblCalcsDate);
			this.Controls.Add(this.HubDate);
			this.Controls.Add(this.PricingHub);
			this.Controls.Add(this.UploadDate);
			this.Controls.Add(this.FileDate);
			this.Controls.Add(this.lblHubDate);
			this.Controls.Add(this.lblPricingHub);
			this.Controls.Add(this.lblUploadDate);
			this.Controls.Add(this.lblFileDate);
			this.Controls.Add(this.ROODSDate);
			this.Controls.Add(this.TimelineDate);
			this.Controls.Add(this.GADSDate);
			this.Controls.Add(this.lblROODSDate);
			this.Controls.Add(this.lblTimelineDate);
			this.Controls.Add(this.lblGADSDate);
			this.Controls.Add(this.btnGetStatus);
			this.Controls.Add(this.cbValidatedInput);
			this.Controls.Add(this.lblStatus);
			this.Controls.Add(this.btnCalcQueue);
			this.Controls.Add(this.btnAddToQueue);
			this.Controls.Add(this.btnCalculate);
			this.Controls.Add(this.cbSumCalcs);
			this.Controls.Add(this.cbClientTables);
			this.Controls.Add(this.cbCopyClean);
			this.Controls.Add(this.cbCalculations);
			this.Controls.Add(this.lblSiteID);
			this.Controls.Add(this.txtSiteID);
			this.Controls.Add(this.shapeContainer1);
			this.Name = "Form1";
			this.Text = "Power Calcs";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtSiteID;
		private System.Windows.Forms.Label lblSiteID;
		private System.Windows.Forms.CheckBox cbCalculations;
		private System.Windows.Forms.CheckBox cbCopyClean;
		private System.Windows.Forms.CheckBox cbClientTables;
		private System.Windows.Forms.CheckBox cbSumCalcs;
		private System.Windows.Forms.Button btnCalculate;
		private System.Windows.Forms.Button btnAddToQueue;
		private System.Windows.Forms.Button btnCalcQueue;
		private System.Windows.Forms.Label lblStatus;
		private System.Windows.Forms.CheckBox cbValidatedInput;
		private System.Windows.Forms.Button btnGetStatus;
		private System.Windows.Forms.Label lblGADSDate;
		private System.Windows.Forms.Label lblTimelineDate;
		private System.Windows.Forms.Label lblROODSDate;
		private System.Windows.Forms.Label GADSDate;
		private System.Windows.Forms.Label TimelineDate;
		private System.Windows.Forms.Label ROODSDate;
		private System.Windows.Forms.Label lblFileDate;
		private System.Windows.Forms.Label lblUploadDate;
		private System.Windows.Forms.Label lblPricingHub;
		private System.Windows.Forms.Label lblHubDate;
		private System.Windows.Forms.Label FileDate;
		private System.Windows.Forms.Label UploadDate;
		private System.Windows.Forms.Label PricingHub;
		private System.Windows.Forms.Label HubDate;
		private System.Windows.Forms.Label lblCalcsDate;
		private System.Windows.Forms.Label CalcsDate;
		private System.Windows.Forms.Label lblStatusTitle;
		private System.Windows.Forms.Button btnUpdateTimeline;
		private System.Windows.Forms.Button btnUploadFile;
		private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
		private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
	}
}

