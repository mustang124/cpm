﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Power_Calcs
{
	public partial class Form1 : Form
	{

		public Form1()
		{
			InitializeComponent();

		}

		private void btnCalculate_Click(object sender, EventArgs e)
		{
			//clear the status box
			lblStatus.Text = "Calculating...";
			Form1.ActiveForm.Refresh();

			PowerCalc calc = new PowerCalc(txtSiteID.Text.Trim());
			lblStatus.Text = calc.Calc(cbCalculations.Checked, cbCopyClean.Checked, cbClientTables.Checked, cbSumCalcs.Checked, cbValidatedInput.Checked);
			
			this.ActiveControl = txtSiteID;

		}

		private void btnAddToQueue_Click(object sender, EventArgs e)
		{
			//clear the status box
			lblStatus.Text = "";
			Form1.ActiveForm.Refresh();
			
			PowerCalc calc = new PowerCalc(txtSiteID.Text.Trim());
			lblStatus.Text = calc.AddToQueue(cbCalculations.Checked, cbCopyClean.Checked, cbClientTables.Checked, cbSumCalcs.Checked, cbValidatedInput.Checked);

			this.ActiveControl = txtSiteID;
		}

		private void btnCalcQueue_Click(object sender, EventArgs e)
		{
			//clear the status box
			lblStatus.Text = "Processing queue...";
			Form1.ActiveForm.Refresh();

			PowerCalc calc = new PowerCalc("");
			lblStatus.Text = calc.ProcessQueue();// calc.Calc(cbCalculations.Checked, cbCopyClean.Checked, cbClientTables.Checked, cbSumCalcs.Checked, cbValidatedInput.Checked);

			this.ActiveControl = txtSiteID;
		}

		private void btnGetStatus_Click(object sender, EventArgs e)
		{
			if (txtSiteID.Text == "")
			{
				lblStatus.Text = "Enter a SiteID.";
				Form1.ActiveForm.Refresh();
				return;
			}

			PowerCalc calc = new PowerCalc(txtSiteID.Text.Trim());
			Status status = calc.GetStatus();

			GADSDate.Text = status.GADSDate;
			TimelineDate.Text = status.TimelineDate;
			ROODSDate.Text = status.ROODsDate;
			UploadDate.Text = status.UploadDate;
			FileDate.Text = status.FileDate;
			PricingHub.Text = status.PricingHub;
			HubDate.Text = status.PricingHubDate;
			CalcsDate.Text = status.CalcsDate;

			lblStatus.Text = status.statusErrors;
			
		}

	}
}
