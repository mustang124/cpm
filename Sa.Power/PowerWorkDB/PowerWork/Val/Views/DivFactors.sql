﻿CREATE View [Val].[DivFactors] as
/*Select Refnum = t.RefineryID, g.Coloc
, EDC = EDC / CASE WHEN T.RefineryType = 'FUELS' THEN 1869019.0 ELSE 469281.0 END
, NumUnits = x.NumUnits / CASE WHEN T.RefineryType = 'FUELS' THEN 26.0 ELSE 9.5 END
, PartUnits = x.NumUnits / CASE WHEN T.RefineryType = 'FUELS' THEN 12.0 ELSE 4.0 END
From GenSum g JOIN Tsort t on t.refnum=g.Refnum and g.Scenario = 'base' and (IncludesVAC = 'N' or IncludesVAC IS NULL)
JOIN (Select refnum, NumUnits = Count(*) from Config where UnitID < 90000 Group by Refnum) x on x.refnum=g.refnum
JOIN (Select refnum, NumCrudes = Count(*) from Crude Group by Refnum) c on c.refnum=g.refnum*/
SELECT t.Refnum, EDC = 1, NumUnits = 1, PartUnits = 1
FROM TSort t
