﻿-- =============================================
-- Author:		Rodrick Blanton
-- Create date: 09 May 2017
-- Description:	Get Events by Unit from GADSOS
-- =============================================
CREATE PROCEDURE [PowerTesting].[GetGADSEvents]
	-- Add the parameters for the stored procedure here
	@searchRefNum VARCHAR(30) 
	, @Year INT = 2016
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT --top 1000
			tSort.Refnum
			, tSort.SiteID
			, tsort.CompanyID
			, tsort.CompanyName
			, tsort.UnitName
			, tsort.UnitLabel
			, tsort.UnitID
			, tsort.CoLoc
			, evntD.EventNumber
			--, CASE WHEN LEN(ISNULL(evnt1.VerbalDesc2,''))>0 THEN evnt1.VerbalDesc2
			--	   WHEN LEN(ISNULL(evnt2.VerbalDesc2,''))>0 THEN evnt2.VerbalDesc2
			--	   ELSE NULL
			--   END AS [VerbalDesc2]
			, nercT.UtilityUnitCode
			, evntd.EventType
			, evntR.StartDateTime
			, evntr.EndDateTime
			, YEAR(evntR.StartDateTime) [Year]
			, evntr.CauseCode
			, evntr.VerbalDesc86
			, evntr.CauseCodeExt
			, evntr.NetAvailCapacity
		FROM  PowerWork.dbo.TSort tsort
		INNER JOIN PowerWork.dbo.NercTurbine nercT ON tsort.Refnum = nercT.Refnum
		LEFT JOIN  GADSOS.GADSNG.EventDetails evntD ON nercT.UtilityUnitCode = evntD.UtilityUnitCode
		LEFT JOIN GADSOS.GADSNG.EventData01 evnt1 ON nercT.UtilityUnitCode = evnt1.UtilityUnitCode and  evntD.EventNumber = evnt1.EventNumber
		LEFT JOIN GADSOS.GADSNG.EventData01AR evnt1AR ON nercT.UtilityUnitCode = evnt1AR.UtilityUnitCode and  evntD.EventNumber = evnt1AR.EventNumber
		LEFT JOIN GADSOS.GADSNG.EventData02 evnt2 ON nercT.UtilityUnitCode = evnt2.UtilityUnitCode and  evntD.EventNumber = evnt2.EventNumber
		LEFT JOIN GADSOS.GADSNG.EventData02AR evnt2AR ON nercT.UtilityUnitCode = evnt2AR.UtilityUnitCode and  evntD.EventNumber = evnt2AR.EventNumber
		LEFT JOIN GADSOS.GADSNG.EventDataCHP evntCHP ON nercT.UtilityUnitCode = evntCHP.UtilityUnitCode and  evntD.EventNumber = evntCHP.EventNumber
		LEFT JOIN GADSOS.GADSNG.EventDataCHPAR evntCHPAR ON nercT.UtilityUnitCode = evntCHPAR.UtilityUnitCode and  evntD.EventNumber = evntCHPAR.EventNumber
		LEFT JOIN GADSOS.GADSNG.EventRecords evntR ON evntD.UnitShortName = evntR.UnitShortName and evntD.EventNumber = evntR.EventNumber and evntD.EventType = evntr.EventType
		WHERE tsort.Refnum Like @searchRefNum
		and YEAR(evntR.StartDateTime) IN (2015, 2012)
		ORDER BY
			tsort.Refnum
			, tsort.SiteID
			, nercT.UtilityUnitCode
			, [Year]
			, evntD.EventNumber


END
