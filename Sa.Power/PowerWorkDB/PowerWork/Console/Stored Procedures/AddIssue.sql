﻿CREATE PROCEDURE [Console].[AddIssue] 
	@RefNum dbo.Refnum,
	@IssueID nvarchar(8),
	@IssueTitle nvarchar(50),
	@IssueText text,
	@UserName nvarchar(20)
AS
BEGIN
INSERT INTO Val.Checklist
Values(@RefNum, @IssueID, @IssueTitle, @IssueText, @UserName, GETDATE(),'N', null,null)
END
