﻿CREATE PROCEDURE [Console].[GetValidatorData]

	@RefNum dbo.Refnum,
	@Consultant varchar(5)

AS
BEGIN

	SELECT t.Consultant, /*rtrim(t.RefID) + ltrim(convert(varchar,t.StudyYear))*/ t.SiteID as [Ref Num], revNo as [Revision], RTRIM(t.CompanyID) + ' - ' + t.SiteName as [Company], Starttime as Start, stoptime as [End], convert(varchar,ValTime) +'s' as [Validation Time] 
	FROM [Val].[ValTime] v INNER JOIN StudySites t ON t.SiteID = v.Refnum
    WHERE t.Consultant=@Consultant and t.SiteID = @RefNum

END
