﻿CREATE PROCEDURE [Console].[GetMessageLog]
   	@SiteID varchar(10),
   	@MessageID varchar(3)

AS
BEGIN

SELECT MAX(MessageTime) as MessageTime
FROM MessageLog l INNER JOIN TSort t ON t.Refnum = l.Refnum
WHERE t.SiteID = @SiteID AND MessageID = @MessageID

	
END
