﻿CREATE PROC [Console].[WriteLog]
	@UserName nvarchar(20),
	@LogType nvarchar(20),
	@Message nvarchar(2000)
	
	
AS
	
	INSERT INTO Console.ConsoleLog
	VALUES(@UserName,@LogType,@Message,GETDATE())
