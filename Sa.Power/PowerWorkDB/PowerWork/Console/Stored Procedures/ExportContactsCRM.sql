﻿CREATE PROCEDURE [Console].[ExportContactsCRM]

AS

BEGIN

     
      
		SELECT	'' as Salutation,
	SUBSTRING(CoordName,1,CHARINDEX(' ',CoordName,1)) as [First Name],
	' ' as [Middle Name],
	SUBSTRING(CoordName,CHARINDEX(' ',CoordName,1),LEN(CoordName)) as [Last Name],
	CoordTitle as Title,
	'' as [Parent Customer],
	CoordPhone as [Business Phone],
	'' as [Home Phone],
	'' as [Mobile Phone],
	CoordFax as [Fax],
	CoordEMail as [E-mail],
	'' as Practice,
	[CoordAddr1] as [Address 1],
			[CoordAddr2] as [Address 2] ,
			'' as [Address 3],
			'PRIMARY' as [Address Type],
			'' as [Name],
			[CoordCity] as [City],
			[CoordState] as [State],
			[CoordZip] as [Zip],
			'' as [Country],
	'',0,0,'',0,1,'','','','','','','','','','','','','','','','','','','','','','',0,0,0,0,0,'','','','','',''	 
	 FROM ClientInfo 
END