﻿CREATE PROCEDURE [Console].[UpdateValidationNotes]

	@RefNum dbo.Refnum,
	@Notes text

AS
BEGIN

	UPDATE Val.Notes SET ValidationNotes = @Notes
	WHERE Refnum=@RefNum 

END
