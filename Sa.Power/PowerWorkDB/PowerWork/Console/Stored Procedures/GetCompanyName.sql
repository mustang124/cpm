﻿CREATE PROCEDURE [Console].[GetCompanyName] 
	@RefNum dbo.Refnum
AS
BEGIN
	SELECT CoLoc = RTRIM(CompanyID) + ' - ' + SiteName FROM dbo.StudySites WHERE SiteID = @RefNum
END
