﻿CREATE PROCEDURE [Console].[GetContactEmails] 
	@RefNum nvarchar(12),
	@CompanyID nvarchar(50)
AS
DECLARE @CompanyCoordEmail nvarchar(50)
DECLARE @PlantCoordEmail nvarchar(50)
BEGIN

select 
@CompanyCoordEmail=Email 
 from company_LU where companyid=@CompanyID

 
 SELECT @PlantCoordEmail=CoordEmail
	 FROM ClientInfo WHERE SiteID = @RefNum
	 
	 SELECT @CompanyCoordEmail as CompanyCoordEmail, @PlantCoordEmail as PlantCoordEmail
 
 END