﻿CREATE PROCEDURE [Console].[GetIssues]
	@RefNum dbo.Refnum,
	@Completed nvarchar(1)
AS
BEGIN

	SELECT * FROM ValStat
	WHERE Refnum = @RefNum 
	AND (@Completed = 'A' OR Completed = @Completed)

END
