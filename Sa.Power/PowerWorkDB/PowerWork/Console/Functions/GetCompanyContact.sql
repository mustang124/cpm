﻿CREATE FUNCTION [Console].[GetCompanyContact](@Refnum dbo.Refnum, @ContactType varchar(5))
RETURNS TABLE
AS
	RETURN 
	(
		SELECT FirstName, LastName, ContactType, Email, JobTitle
			, Phone, PhoneSecondary, Fax
			, MailAddr1, MailAddr2, MailAddr3, MailCity, MailState, MailZip, MailCountry
			, StrAddr1, StrAddr2, StrAdd3, StrCity, StrState, StrZip, StrCountry
			, SendMethod, Password = dbo.GetCompanyPassword(@Refnum), Comment, SANumber = 0
		FROM dbo.CoContactInfo
		WHERE ContactType = @ContactType OR @ContactType IS NULL
		--AND SANumber = dbo.GetSANumber(@Refnum)
		UNION
		SELECT AltFirstName, AltLastName, ContactType = 'Alt', AltEmail, JobTitle = NULL
			, Phone = NULL, PhoneSecondary = NULL, Fax = NULL
			, MailAddr1 = NULL, MailAddr2 = NULL, MailAddr3 = NULL, MailCity = NULL, MailState = NULL, MailZip = NULL, MailCountry = NULL
			, StrAddr1 = NULL, StrAddr2 = NULL, StrAdd3 = NULL, StrCity = NULL, StrState = NULL, StrZip = NULL, StrCountry = NULL
			, SendMethod, Password = dbo.GetCompanyPassword(@Refnum), Comment, SANumber = 0
		FROM dbo.CoContactInfo
		WHERE @ContactType = 'Alt'
		--AND SANumber = dbo.GetSANumber(@Refnum)
	)
