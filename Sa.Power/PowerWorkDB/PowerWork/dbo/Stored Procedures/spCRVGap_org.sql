﻿



CREATE  PROC [spCRVGap_org](@Refnum Refnum) AS
DECLARE @StudyYear smallint, @PricingHub varchar(12), 
	@CRVSizeGroup varchar(6), @CRVSizeGroup2 varchar(6)
DECLARE @AvgLRO_Unp real
DECLARE @tPeakUnavail_Unp real, @tHeatRate real, @tTotCashLessFuel real
DECLARE @LostPeakMWH_Unp real, @PeakUnavail_Unp real, @TotPeakMWH real, 
	@HeatRate real, @TotCashLessFuel real, @AvgFuelCost real, @AdjNetMWH real
DECLARE @ScrubberCost real, @ScrubberMWH real
SELECT @StudyYear = dbo.StudyYear(@Refnum), @PricingHub = dbo.PricingHubByRefnum(@Refnum)
SELECT @CRVSizeGroup = CRVSizeGroup, @CRVSizeGroup2 = CRVSizeGroup2
FROM Breaks
WHERE Refnum = @Refnum
DELETE FROM CRVGap WHERE Refnum = @Refnum
--SELECT @AvgLRO_Unp = AVG(Price)
--FROM Prices
--WHERE DATEPART(yy, StartTime) = @StudyYear
--AND PricingHub = @PricingHub
SELECT @AvgLRO_Unp = LRO_Unp*1000/PeakMWHLost_Unp 
FROM CommUnavail
WHERE Refnum = @Refnum and PeakMWHLost_Unp > 0
IF @AvgLRO_Unp IS NULL
	SELECT @AvgLRO_Unp = 0
SELECT @PeakUnavail_Unp = PeakUnavail_Unp, @TotPeakMWH = TotPeakMWH
FROM CommUnavail WHERE Refnum = @Refnum
SELECT @LostPeakMWH_Unp = SUM(tl.PeakHrs*tl.LostMW)
FROM TSort INNER JOIN NERCTurbine n ON n.Refnum = TSort.Refnum
INNER JOIN EVNTTL tl ON  tl.UtilityUnitCode = n.UtilityUnitCode
	AND tl.Evnt_Year = TSort.EvntYear
INNER JOIN EvntRpts e2 ON e2.UNITABBR = n.UNITABBR
	AND DATEPART(yy, e2.Start_Date) = tl.EVNT_YEAR AND e2.EVNT_NO = tl.EVNT_NO
INNER JOIN CauseCodes c ON c.Cause_Code = e2.Cause_Code
WHERE tl.EVNT_Category IN ('F','M') AND c.SAIMajorEquip IN ('Boiler','Turbine','Balance Of Plant', 'Combustion Turbine')
AND c.SAIMinorEquip <> 'Coal Pulverizers/Hammer Mills (CPHM)'
AND tl.PeakHrs*tl.LostMW>0 AND e2.EVNT_NO <> 7777
and TSort.Refnum = @Refnum
IF @TotPeakMWH > 0 
BEGIN
	IF @LostPeakMWH_Unp*100/@TotPeakMWH < @PeakUnavail_Unp
		SELECT @PeakUnavail_Unp = @LostPeakMWH_Unp*100/@TotPeakMWH
END
SELECT @TotCashLessFuel = TotCashLessFuel FROM OpexCalc 
WHERE Refnum = @Refnum AND DataType = 'MWH'
SELECT @HeatRate = HeatRate, @AvgFuelCost = AvgFuelCostMBTU, @AdjNetMWH = AdjNetMWH
FROM GenSum WHERE Refnum = @Refnum
SELECT @ScrubberCost = c.TotScrubber-c.ScrubberPowerCost, @ScrubberMWH = ISNULL(ScrConsMWH,0)
FROM MiscCalc c INNER JOIN Misc m ON m.Refnum = c.Refnum
WHERE c.Refnum = @Refnum
IF @ScrubberCost > 0
	SELECT @TotCashLessFuel = @TotCashLessFuel - @ScrubberCost*1000/@AdjNetMWH
IF @ScrubberMWH > 0 
	SELECT @HeatRate = @HeatRate*(@AdjNetMWH/(@AdjNetMWH+@ScrubberMWH))
-- Calculate gaps
DECLARE @RevenueGap real, @FuelOperGap real, @OpexGap real, @TotGap real

SELECT 	@tPeakUnavail_Unp = PeakUnavail_Unp, @tHeatRate = HeatRate, 
	@tTotCashLessFuel = TotCashLessFuelMWH
FROM CRVGapFactors f
WHERE StudyYear = @StudyYear AND CRVSizeGroup = @CRVSizeGroup

SELECT @RevenueGap = (@PeakUnavail_Unp - @tPeakUnavail_Unp)/100 * @AvgLRO_Unp * @TotPeakMWH / 1000
SELECT @FuelOperGap = (@HeatRate - @tHeatRate) * @AvgFuelCost * @AdjNetMWH / 1000000
SELECT @OpexGap = (@TotCashLessFuel - @tTotCashLessFuel) * @AdjNetMWH / 1000
SELECT @TotGap = CASE WHEN @RevenueGap>0 THEN @RevenueGap ELSE 0 END 
		+ CASE WHEN @FuelOperGap>0 THEN @FuelOperGap ELSE 0 END 
		+ CASE WHEN @OpexGap>0 THEN @OpexGap ELSE 0 END
INSERT INTO CRVGap (Refnum, CRVSizeGroup, AdjNetMWH,
	PeakUnavail_Unp, TargetPeakUnavail_Unp, MktAvgPrice, RevenueGapKUS,
	HeatRate, TargetHeatRate, AvgFuelCostMBTU, FuelOperGapKUS,
	TotCashLessFuelMWH, TargetTotCashLessFuelMWH, OpexGapKUS, TotGapKUS)
VALUES (@Refnum, @CRVSizeGroup, @AdjNetMWH,
	@PeakUnavail_Unp, @tPeakUnavail_Unp, @AvgLRO_Unp, @RevenueGap,
	@HeatRate, @tHeatRate, @AvgFuelCost, @FuelOperGap,
	@TotCashLessFuel, @tTotCashLessFuel, @OpexGap, @TotGap)

-- Calculate gaps for overlapping CRV Size Group
SELECT @tPeakUnavail_Unp = NULL, @tHeatRate = NULL, @tTotCashLessFuel = NULL
SELECT 	@tPeakUnavail_Unp = PeakUnavail_Unp, @tHeatRate = HeatRate, 
	@tTotCashLessFuel = TotCashLessFuelMWH
FROM CRVGapFactors f
WHERE StudyYear = @StudyYear AND CRVSizeGroup = @CRVSizeGroup2

IF @tPeakUnavail_Unp IS NOT NULL
BEGIN
SELECT @RevenueGap = (@PeakUnavail_Unp - @tPeakUnavail_Unp)/100 * @AvgLRO_Unp * @TotPeakMWH / 1000
SELECT @FuelOperGap = (@HeatRate - @tHeatRate) * @AvgFuelCost * @AdjNetMWH / 1000000
SELECT @OpexGap = (@TotCashLessFuel - @tTotCashLessFuel) * @AdjNetMWH / 1000
SELECT @TotGap = CASE WHEN @RevenueGap>0 THEN @RevenueGap ELSE 0 END 
		+ CASE WHEN @FuelOperGap>0 THEN @FuelOperGap ELSE 0 END 
		+ CASE WHEN @OpexGap>0 THEN @OpexGap ELSE 0 END
INSERT INTO CRVGap (Refnum, CRVSizeGroup, AdjNetMWH,
	PeakUnavail_Unp, TargetPeakUnavail_Unp, MktAvgPrice, RevenueGapKUS,
	HeatRate, TargetHeatRate, AvgFuelCostMBTU, FuelOperGapKUS,
	TotCashLessFuelMWH, TargetTotCashLessFuelMWH, OpexGapKUS, TotGapKUS)
VALUES (@Refnum, @CRVSizeGroup2, @AdjNetMWH,
	@PeakUnavail_Unp, @tPeakUnavail_Unp, @AvgLRO_Unp, @RevenueGap,
	@HeatRate, @tHeatRate, @AvgFuelCost, @FuelOperGap,
	@TotCashLessFuel, @tTotCashLessFuel, @OpexGap, @TotGap)
END




