﻿CREATE PROCEDURE spDDLookups;1
	@ObjName sysname,
	@LookupID DD_ID OUTPUT
AS
IF EXISTS (SELECT * FROM DD_Lookups WHERE ObjectName = @ObjName)
	SELECT @LookupID = LookupID
	FROM DD_Lookups
	WHERE ObjectName = @ObjName
ELSE
	SELECT @LookupID = -1
