﻿
CREATE PROCEDURE [dbo].[spInflateCarryoverFuel](@Refnum char(12), @FuelType char(6), @InflFactor real)
AS

UPDATE Fuel
SET CostMBTULocal = CostMBTULocal * @InflFactor,
	CostMBTU = CostMBTU * @InflFactor,
	CostMBTULHVLocal = CostMBTULHVLocal * @InflFactor,
	CostMBTULHV = CostMBTULHV * @InflFactor,
	CostGalLocal = CostGalLocal * @InflFactor,
	CostGal = CostGal * @InflFactor,
	TotCostKUS = TotCostKUS * @InflFactor
WHERE Refnum = @Refnum AND FuelType = @FuelType 

UPDATE OpexLocal
SET PurGas = (SELECT SUM(CASE WHEN ReportedLHV = 'Y' THEN CostMBTULHVLocal*MBTULHV ELSE CostMBTULocal*MBTU END/1000) FROM Fuel WHERE Fuel.Refnum = OpexLocal.Refnum AND FuelType IN ('NatGas','OffGas','H2Gas')),
PurLiquid = (SELECT SUM(CostGalLocal*KGal) FROM Fuel WHERE Fuel.Refnum = OpexLocal.Refnum AND FuelType IN ('Jet','FOil','Diesel'))
WHERE Refnum = @Refnum
