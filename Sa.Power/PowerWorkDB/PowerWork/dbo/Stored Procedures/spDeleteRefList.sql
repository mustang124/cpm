﻿CREATE PROCEDURE spDeleteRefList @ListName RefListName 
AS
DECLARE @Access smallint
EXEC @Access = spCheckListOwner @ListName
IF @Access > 0
BEGIN
	DELETE FROM RefList
	WHERE RefListNo = (SELECT RefListNo
				FROM RefList_LU
				WHERE ListName = @ListName)
	DELETE FROM RefList_LU
	WHERE ListName = @ListName
END
ELSE
	RETURN @Access
