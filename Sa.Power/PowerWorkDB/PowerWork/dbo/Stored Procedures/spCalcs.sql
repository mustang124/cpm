﻿
CREATE PROC [dbo].[spCalcs](@SiteID SiteID)
AS

BEGIN TRY

	DECLARE @RefCount INT
	SELECT @RefCount = COUNT(*) FROM TSort WHERE SiteID = @SiteID

	IF @RefCount > 0
	BEGIN



		DECLARE @result int, @msgSource varchar(12), @BreakCount real
		SET NOCOUNT ON
		--PRINT '1-madeit'
		EXEC CalcInitial @SiteID
		--PRINT '2-madeit'
		EXEC CalcTurbineNERCFactors @SiteID
		--PRINT '3-madeit'
		EXEC CalcComponentEUF @SiteID
		--PRINT '4-madeit'
		EXEC spLHVCalcs @SiteID -- can be called anytime before this point
		--PRINT '5-madeit'
		EXEC spAllocateSpares @SiteID -- can be called anytime before this point
		--PRINT '6-madeit'
		EXEC spCurrencyConv @SiteID
		--PRINT '7-madeit'
		EXEC CalcEmissionsTons @SiteID
		--PRINT '8-madeit'
		EXEC CalcMaintExp @SiteID
		--PRINT '9-madeit'
		EXEC CalcCoalCost @SiteID
		--PRINT '10-madeit'
		EXEC CalcSteam @SiteID
		--PRINT '11-madeit'
		EXEC CalcMaint @SiteID
		--PRINT '12-madeit'
		EXEC CalcPers @SiteID
		--PRINT '13-madeit'
		EXEC CalcCost @SiteID
		--PRINT '14-madeit'
		EXEC CalcVarCost @SiteID
		--PRINT '15-madeit'
		EXEC CalcSummary @SiteID
		--PRINT '16-madeit'

		-- These procedures are unit based and complex, 
		-- so we will calculate them one unit at a time.
		DECLARE cUnits CURSOR LOCAL FAST_FORWARD
		FOR	SELECT Refnum FROM TSort WHERE SiteID = @SiteID
		DECLARE @Refnum Refnum
		OPEN cUnits
		FETCH NEXT FROM cUnits INTO @Refnum
		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC spCTGClass @Refnum -- SW added 8/25/14 hope this fills in the CTG class properly
			EXEC spUpdateBreaks @Refnum
			EXEC spCogenCalc @Refnum
			EXEC spCommUnavail @Refnum
			EXEC spEGCCalcs @Refnum
			EXEC spUpdateGaps @Refnum
			EXEC spComponentEUFPlus @Refnum -- SW added 9/5/13 to make sure this table is populated
			EXEC CalcPostCloseRank @Refnum --SW added 9/6/16 to calc this table's values automatically

			FETCH NEXT FROM cUnits INTO @Refnum
		END
		CLOSE cUnits
		DEALLOCATE cUnits

		EXEC CalcMaintOver15 @SiteID -- SW 8/26/14 we do this at the end because it needs to be after EGC
	

		--we delete any old error message
		DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'Calculations' and MessageID = 37

		EXEC sp_InsertMessage @siteid, 44, 'Calculations', 'Calculations'

	END
	ELSE
	BEGIN
		RAISERROR ('spCalcs failed.',16,1)
	END
	
END TRY
BEGIN CATCH

	--we delete any old error message
	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'Calculations' and MessageID = 37

	EXEC sp_InsertMessage @siteid, 37, 'Calculations', 'Calculations'

	RAISERROR ('spCalcs failed.',16,1)

END CATCH