﻿

/****** Object:  Stored Procedure dbo.spCommUnavail    Script Date: 4/18/2003 4:39:04 PM ******/
CREATE PROCEDURE [dbo].[spCommUnavail] (@Refnum Refnum) AS
DECLARE @StartPeriod smallDateTime, @EndPeriod smallDateTime
DECLARE @Period smalldatetime, @VarCostMWH real
DECLARE @StudyYear integer, @EventYear integer, @PricingHub varchar(15) 
DECLARE @UtilCode integer, @UnitCode integer
DECLARE @TotPeakMWH real, @TotMRO real
DECLARE @PeakMWHLost_P real, @LRO_P real
DECLARE @PeakMWHLost_M real, @LRO_M real
DECLARE @PeakMWHLost_F real, @LRO_F real
DECLARE @PeakMWHLost_Tot real, @LRO_Tot real
DECLARE @LROP_P real, @LROP_M real, @LROP_F real, @LROP_Tot real
DECLARE @CUTI_P real, @CUTI_M real, @CUTI_F real, @CUTI_Tot real
DECLARE @PeakUnavail_P real, @PeakUnavail_M real, 
	@PeakUnavail_F real, @PeakUnavail_Tot real
DECLARE @TotPeakMWH_Win real, @TotMRO_Win real
DECLARE @PeakMWHLost_P_Win real, @LRO_P_Win real
DECLARE @PeakMWHLost_M_Win real, @LRO_M_Win real
DECLARE @PeakMWHLost_F_Win real, @LRO_F_Win real
DECLARE @PeakMWHLost_Tot_Win real, @LRO_Tot_Win real
DECLARE @LROP_P_Win real, @LROP_M_Win real, @LROP_F_Win real, @LROP_Tot_Win real
DECLARE @CUTI_P_Win real, @CUTI_M_Win real, @CUTI_F_Win real, @CUTI_Tot_Win real
DECLARE @PeakUnavail_P_Win real, @PeakUnavail_M_Win real, 
	@PeakUnavail_F_Win real, @PeakUnavail_Tot_Win real
DECLARE @TotPeakMWH_Sum real, @TotMRO_Sum real
DECLARE @PeakMWHLost_P_Sum real, @LRO_P_Sum real
DECLARE @PeakMWHLost_M_Sum real, @LRO_M_Sum real
DECLARE @PeakMWHLost_F_Sum real, @LRO_F_Sum real
DECLARE @PeakMWHLost_Tot_Sum real, @LRO_Tot_Sum real
DECLARE @LROP_P_Sum real, @LROP_M_Sum real, @LROP_F_Sum real, @LROP_Tot_Sum real
DECLARE @CUTI_P_Sum real, @CUTI_M_Sum real, @CUTI_F_Sum real, @CUTI_Tot_Sum real
DECLARE @PeakUnavail_P_Sum real, @PeakUnavail_M_Sum real, 
	@PeakUnavail_F_Sum real, @PeakUnavail_Tot_Sum real
DECLARE @PeakMWHLost_Unp real, @LRO_Unp real, 
	@LROP_Unp real, @PeakUnavail_Unp real, @CUTI_Unp real
DECLARE @PeakMWHLost_Unp_Sum real, @LRO_Unp_Sum real, 
	@LROP_Unp_Sum real, @PeakUnavail_Unp_Sum real, @CUTI_Unp_Sum real
DECLARE @PeakMWHLost_Unp_Win real, @LRO_Unp_Win real, 
	@LROP_Unp_Win real, @PeakUnavail_Unp_Win real, @CUTI_Unp_Win real
IF EXISTS (SELECT * FROM TSort WHERE Refnum = @Refnum AND CalcCommUnavail = 'N')
	RETURN
SELECT @StudyYear = StudyYear, @EventYear = EvntYear, @PricingHub = PricingHub FROM TSort WHERE Refnum = @Refnum
DELETE FROM CommUnavail WHERE Refnum = @Refnum

EXEC spLoadEvents @Refnum

-- Load RevenuePeaks
DELETE FROM RevenuePeaks WHERE Refnum = @Refnum
INSERT INTO RevenuePeaks (Refnum, PricingHub, StartTime, EndTime, Price)
SELECT @Refnum, PricingHub, StartTime, EndTime, Price
FROM Prices
WHERE PricingHub IN (SELECT PricingHub FROM TSort WHERE Refnum = @Refnum UNION SELECT PricingHub FROM AlternateHubs WHERE Refnum = @Refnum)
AND DATEPART(yy, StartTime) = @EventYear

IF @StudyYear <> @EventYear 
	UPDATE RevenuePeaks
	SET Price = (SELECT AVG(Price) FROM Prices
		WHERE Prices.PricingHub = RevenuePeaks.PricingHub
		AND DATEPART(mm, Prices.StartTime) = DATEPART(mm, RevenuePeaks.StartTime)
		AND DATEPART(yy, Prices.StartTime) = @StudyYear)
	WHERE Refnum = @Refnum

DECLARE cVarCost CURSOR
FOR SELECT Period, VarCostMWH FROM VarCostCalc WHERE Refnum = @Refnum ORDER BY Period DESC
OPEN cVarCost
FETCH NEXT FROM cVarCost INTO @Period, @VarCostMWH
WHILE (@@FETCH_STATUS <> -1)
BEGIN
	IF (@@FETCH_STATUS <> -2)
	BEGIN
		IF @StudyYear <> @EventYear
			SELECT @Period = DATEADD(yy, @EventYear-@StudyYear, @Period) 
		UPDATE RevenuePeaks
		SET VarCost = @VarCostMWH, Penalty = Price-@VarCostMWH
		WHERE Refnum = @Refnum 
		AND (StartTime <= @Period OR DATEPART(dy, StartTime) <= DATEPART(dy, @Period))
	END
	FETCH NEXT FROM cVarCost INTO @Period, @VarCostMWH
END
CLOSE cVarCost
DEALLOCATE cVarCost

DELETE FROM RevenuePeaks WHERE Refnum = @Refnum AND Penalty < 0

UPDATE RevenuePeaks
SET MRO = n.NDC * Penalty * (DATEDIFF(mi, StartTime, EndTime)/60.0) / 1000,
PotentialNetMWH = n.NDC * (DATEDIFF(mi, StartTime, EndTime)/60.0)
FROM RevenuePeaks, NERCFactors n
WHERE RevenuePeaks.Refnum = @Refnum
AND RevenuePeaks.Refnum = n.Refnum 


-- Calculate LRO of Events and store in EventLRO
DELETE FROM EventLRO WHERE Refnum = @Refnum

INSERT INTO EventLRO (Refnum, TurbineID, PricingHub, EVNT_NO, Phase, PeakHrs, LRO)
SELECT Refnum, TurbineID, PricingHub, EVNT_NO, Phase, 0, 0
FROM Events, (SELECT PricingHub FROM TSort WHERE Refnum = @Refnum UNION SELECT PricingHub FROM AlternateHubs WHERE Refnum = @Refnum) x
WHERE Events.Refnum = @Refnum

UPDATE EventLRO
SET PeakHrs = x.PeakHrs, LRO = x.LRO
FROM EventLRO, (SELECT e.Refnum, e.TurbineID, p.PricingHub, e.Evnt_No, e.Phase,
	PeakHrs = SUM(CASE 
		WHEN e.Start_Time <= p.StartTime AND e.End_Time >= p.EndTime THEN DATEDIFF(mi, p.StartTime, DATEADD(mi, 1, p.EndTime))/60.0 --SW 10/4/16 adding a minute to endtime because otherwise it counts an hour as 59 minutes (10:00 to 10:59, datediff is 59 minutes not 60 as it should be)
		WHEN e.Start_Time <= p.StartTime AND e.End_Time < p.EndTime THEN DATEDIFF(mi, p.StartTime, DATEADD(mi, 1, e.End_Time))/60.0
		WHEN e.Start_Time > p.StartTime AND e.End_Time >= p.EndTime THEN DATEDIFF(mi, e.Start_Time, DATEADD(mi, 1, p.EndTime))/60.0
		WHEN e.Start_Time > p.StartTime AND e.End_Time < p.EndTime THEN DATEDIFF(mi, e.Start_Time, DATEADD(mi, 1, e.End_Time))/60.0
		ELSE 0 END),
	LRO = SUM(CASE 
		WHEN e.Start_Time <= p.StartTime AND e.End_Time >= p.EndTime THEN DATEDIFF(mi, p.StartTime, DATEADD(mi, 1, p.EndTime))/60.0
		WHEN e.Start_Time <= p.StartTime AND e.End_Time < p.EndTime THEN DATEDIFF(mi, p.StartTime, DATEADD(mi, 1, e.End_Time))/60.0
		WHEN e.Start_Time > p.StartTime AND e.End_Time >= p.EndTime THEN DATEDIFF(mi, e.Start_Time, DATEADD(mi, 1, p.EndTime))/60.0
		WHEN e.Start_Time > p.StartTime AND e.End_Time < p.EndTime THEN DATEDIFF(mi, e.Start_Time, DATEADD(mi, 1, e.End_Time))/60.0
		ELSE 0 END * p.Penalty * e.LostMW)/1000
	FROM Events e, RevenuePeaks p
	WHERE e.Refnum = p.Refnum AND e.Refnum = @Refnum AND p.EndTime > e.Start_Time AND p.StartTime < e.End_Time 
	GROUP BY e.Refnum, e.TurbineID, p.PricingHub, e.Evnt_No, e.Phase) x	
WHERE x.Refnum = EventLRO.Refnum AND x.TurbineID = EventLRO.TurbineID AND x.Evnt_no = EventLRO.Evnt_no AND x.Phase = EventLRO.Phase
AND EventLRO.Refnum = @Refnum

-- Calculate commercial unavailability indicators
-- Total Year
SELECT @StartPeriod = '1/1/' + convert(char(4), @EventYear),
	@EndPeriod = '1/1/' + convert(char(4), @EventYear+1)
EXEC spGetCUComponents @Refnum, @PricingHub, @StartPeriod, @EndPeriod,
	@TotPeakMWH OUTPUT, @TotMRO OUTPUT,
	@PeakMWHLost_P OUTPUT, @PeakMWHLost_M OUTPUT, 
	@PeakMWHLost_F OUTPUT, @PeakMWHLost_Tot OUTPUT,
	@LRO_P OUTPUT, @LRO_M OUTPUT, @LRO_F OUTPUT, @LRO_Tot OUTPUT
EXEC spCUCalcs
	@TotPeakMWH, @TotMRO, @PeakMWHLost_P, 
	@PeakMWHLost_M, @PeakMWHLost_F, @PeakMWHLost_Tot,
	@LRO_P, @LRO_M, @LRO_F, @LRO_Tot, 
	@LROP_P OUTPUT, @LROP_M OUTPUT, @LROP_F OUTPUT, @LROP_Tot OUTPUT,
	@PeakUnavail_P OUTPUT, @PeakUnavail_M OUTPUT, 
	@PeakUnavail_F OUTPUT, @PeakUnavail_Tot OUTPUT,
	@CUTI_P OUTPUT, @CUTI_M OUTPUT, @CUTI_F OUTPUT, @CUTI_Tot OUTPUT,
	@PeakMWHLost_Unp OUTPUT, @LRO_Unp OUTPUT, @LROP_Unp OUTPUT,
	@PeakUnavail_Unp OUTPUT, @CUTI_Unp OUTPUT
-- Winter (Jan - Feb)
SELECT @StartPeriod = '1/1/' + convert(char(4), @EventYear),
	@EndPeriod = '3/1/' + convert(char(4), @EventYear)
EXEC spGetCUComponents @Refnum, @PricingHub, @StartPeriod, @EndPeriod,
	@TotPeakMWH_Win OUTPUT, @TotMRO_Win OUTPUT,
	@PeakMWHLost_P_Win OUTPUT, @PeakMWHLost_M_Win OUTPUT, 
	@PeakMWHLost_F_Win OUTPUT, @PeakMWHLost_Tot_Win OUTPUT,
	@LRO_P_Win OUTPUT, @LRO_M_Win OUTPUT, 
	@LRO_F_Win OUTPUT, @LRO_Tot_Win OUTPUT
EXEC spCUCalcs
	@TotPeakMWH_Win, @TotMRO_Win, @PeakMWHLost_P_Win, 
	@PeakMWHLost_M_Win, @PeakMWHLost_F_Win, @PeakMWHLost_Tot_Win,
	@LRO_P_Win, @LRO_M_Win, @LRO_F_Win, @LRO_Tot_Win, 
	@LROP_P_Win OUTPUT, @LROP_M_Win OUTPUT, @LROP_F_Win OUTPUT, @LROP_Tot_Win OUTPUT,
	@PeakUnavail_P_Win OUTPUT, @PeakUnavail_M_Win OUTPUT, 
	@PeakUnavail_F_Win OUTPUT, @PeakUnavail_Tot_Win OUTPUT,
	@CUTI_P_Win OUTPUT, @CUTI_M_Win OUTPUT, @CUTI_F_Win OUTPUT, @CUTI_Tot_Win OUTPUT,
	@PeakMWHLost_Unp_Win OUTPUT, @LRO_Unp_Win OUTPUT, @LROP_Unp_Win OUTPUT,
	@PeakUnavail_Unp_Win OUTPUT, @CUTI_Unp_Win OUTPUT
-- Summer (June - Aug)
SELECT @StartPeriod = '6/1/' + convert(char(4), @EventYear),
	@EndPeriod = '9/1/' + convert(char(4), @EventYear)
EXEC spGetCUComponents @Refnum, @PricingHub, @StartPeriod, @EndPeriod,
	@TotPeakMWH_Sum OUTPUT, @TotMRO_Sum OUTPUT,
	@PeakMWHLost_P_Sum OUTPUT, @PeakMWHLost_M_Sum OUTPUT, 
	@PeakMWHLost_F_Sum OUTPUT, @PeakMWHLost_Tot_Sum OUTPUT,
	@LRO_P_Sum OUTPUT, @LRO_M_Sum OUTPUT, 
	@LRO_F_Sum OUTPUT, @LRO_Tot_Sum OUTPUT
EXEC spCUCalcs
	@TotPeakMWH_Sum, @TotMRO_Sum, @PeakMWHLost_P_Sum, 
	@PeakMWHLost_M_Sum, @PeakMWHLost_F_Sum, @PeakMWHLost_Tot_Sum,
	@LRO_P_Sum, @LRO_M_Sum, @LRO_F_Sum, @LRO_Tot_Sum, 
	@LROP_P_Sum OUTPUT, @LROP_M_Sum OUTPUT, @LROP_F_Sum OUTPUT, @LROP_Tot_Sum OUTPUT,
	@PeakUnavail_P_Sum OUTPUT, @PeakUnavail_M_Sum OUTPUT, 
	@PeakUnavail_F_Sum OUTPUT, @PeakUnavail_Tot_Sum OUTPUT,
	@CUTI_P_Sum OUTPUT, @CUTI_M_Sum OUTPUT, @CUTI_F_Sum OUTPUT, @CUTI_Tot_Sum OUTPUT,
	@PeakMWHLost_Unp_Sum OUTPUT, @LRO_Unp_Sum OUTPUT, @LROP_Unp_Sum OUTPUT,
	@PeakUnavail_Unp_Sum OUTPUT, @CUTI_Unp_Sum OUTPUT

INSERT INTO CommUnavail (Refnum, TotPeakMWH, TotMRO,
PeakMWHLost_P, PeakMWHLost_M, PeakMWHLost_F, PeakMWHLost_Tot,
LRO_P, LRO_M, LRO_F, LRO_Tot, LROP_P, LROP_M, LROP_F, LROP_Tot,
PeakUnavail_P, PeakUnavail_M, PeakUnavail_F, PeakUnavail_Tot,
CUTI_P, CUTI_M, CUTI_F, CUTI_Tot,
TotPeakMWH_Win, TotMRO_Win, 
PeakMWHLost_P_Win, PeakMWHLost_M_Win, PeakMWHLost_F_Win, PeakMWHLost_Tot_Win,
LRO_P_Win, LRO_M_Win, LRO_F_Win, LRO_Tot_Win, 
LROP_P_Win, LROP_M_Win, LROP_F_Win, LROP_Tot_Win,
PeakUnavail_P_Win, PeakUnavail_M_Win, PeakUnavail_F_Win, PeakUnavail_Tot_Win,
CUTI_P_Win, CUTI_M_Win, CUTI_F_Win, CUTI_Tot_Win,
TotPeakMWH_Sum, TotMRO_Sum, 
PeakMWHLost_P_Sum, PeakMWHLost_M_Sum, PeakMWHLost_F_Sum, PeakMWHLost_Tot_Sum,
LRO_P_Sum, LRO_M_Sum, LRO_F_Sum, LRO_Tot_Sum, 
LROP_P_Sum, LROP_M_Sum, LROP_F_Sum, LROP_Tot_Sum,
PeakUnavail_P_Sum, PeakUnavail_M_Sum, PeakUnavail_F_Sum, PeakUnavail_Tot_Sum,
CUTI_P_Sum, CUTI_M_Sum, CUTI_F_Sum, CUTI_Tot_Sum,
PeakMWHLost_Unp, LRO_Unp, LROP_Unp, PeakUnavail_Unp, CUTI_Unp,
PeakMWHLost_Unp_Win, LRO_Unp_Win, LROP_Unp_Win, PeakUnavail_Unp_Win, CUTI_Unp_Win,
PeakMWHLost_Unp_Sum, LRO_Unp_Sum, LROP_Unp_Sum, PeakUnavail_Unp_Sum, CUTI_Unp_Sum)
VALUES (@Refnum, @TotPeakMWH, @TotMRO, 
@PeakMWHLost_P, @PeakMWHLost_M, @PeakMWHLost_F, @PeakMWHLost_Tot,
@LRO_P, @LRO_M, @LRO_F, @LRO_Tot, 
@LROP_P, @LROP_M, @LROP_F, @LROP_Tot,
@PeakUnavail_P, @PeakUnavail_M, @PeakUnavail_F, @PeakUnavail_Tot,
@CUTI_P, @CUTI_M, @CUTI_F, @CUTI_Tot,
@TotPeakMWH_Win, @TotMRO_Win, 
@PeakMWHLost_P_Win, @PeakMWHLost_M_Win, @PeakMWHLost_F_Win, @PeakMWHLost_Tot_Win,
@LRO_P_Win, @LRO_M_Win, @LRO_F_Win, @LRO_Tot_Win, 
@LROP_P_Win, @LROP_M_Win, @LROP_F_Win, @LROP_Tot_Win,
@PeakUnavail_P_Win, @PeakUnavail_M_Win, @PeakUnavail_F_Win, @PeakUnavail_Tot_Win,
@CUTI_P_Win, @CUTI_M_Win, @CUTI_F_Win, @CUTI_Tot_Win,
@TotPeakMWH_Sum, @TotMRO_Sum, 
@PeakMWHLost_P_Sum, @PeakMWHLost_M_Sum, @PeakMWHLost_F_Sum, @PeakMWHLost_Tot_Sum,
@LRO_P_Sum, @LRO_M_Sum, @LRO_F_Sum, @LRO_Tot_Sum, 
@LROP_P_Sum, @LROP_M_Sum, @LROP_F_Sum, @LROP_Tot_Sum,
@PeakUnavail_P_Sum, @PeakUnavail_M_Sum, @PeakUnavail_F_Sum, @PeakUnavail_Tot_Sum,
@CUTI_P_Sum, @CUTI_M_Sum, @CUTI_F_Sum, @CUTI_Tot_Sum,
@PeakMWHLost_Unp, @LRO_Unp, @LROP_Unp, @PeakUnavail_Unp, @CUTI_Unp,
@PeakMWHLost_Unp_Win, @LRO_Unp_Win, @LROP_Unp_Win, @PeakUnavail_Unp_Win, @CUTI_Unp_Win,
@PeakMWHLost_Unp_Sum, @LRO_Unp_Sum, @LROP_Unp_Sum, @PeakUnavail_Unp_Sum, @CUTI_Unp_Sum)



