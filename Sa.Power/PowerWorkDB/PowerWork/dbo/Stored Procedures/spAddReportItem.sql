﻿

CREATE PROCEDURE spAddReportItem 
	@ReportID DD_ID = NULL,
	@ReportName varchar(30) = NULL,
	@DisplayOrder smallint = NULL,
	@ReportItemID DD_ID = 1,
	@ItemType ItemType,
	@LabelOverride varchar(50) = NULL,
	@FormatOverride tinyint = NULL,
	@DisplayUnitsOverride varchar(20) = NULL,
	@RptItemScenario Scenario = NULL,
	@RptItemSQL varchar(255) = NULL,
	@RptItemCurrency varchar(5) = NULL
AS
IF @ReportID IS NULL AND @ReportName IS NULL
BEGIN
	RAISERROR ('You must specify a ReportID or ReportName.',16,-1)
	RETURN -100
END
IF @ReportID IS NULL
	SELECT @ReportID = ReportID 
	FROM ReportDef 
	WHERE ReportName = @ReportName
IF @ReportID IS NULL
BEGIN
	RAISERROR ('Invalid ReportName',16,-1)
	RETURN -100
END
IF @ItemType = 'I'
BEGIN
	IF NOT EXISTS (SELECT * FROM ReportItemDef 
			WHERE ReportItemID = @ReportItemID)
	BEGIN
		RAISERROR ('Invalid ReportItem',16,-1)
		RETURN -101
	END
END
ELSE BEGIN
	IF NOT EXISTS (SELECT * FROM ReportDef
			WHERE ReportID = @ReportItemID)
	BEGIN
		RAISERROR ('Invalid ReportItem',16,-1)
		RETURN -101
	END
END	
IF @DisplayOrder IS NULL 
	SELECT @DisplayOrder = ISNULL(MAX(DisplayOrder), 0)+1 
	FROM ReportItems 
	WHERE ReportID = @ReportID
INSERT INTO ReportItems(ReportID, DisplayOrder, ReportItemID, ItemType,
	LabelOverride, FormatOverride, DisplayUnitsOverride, 
	RptItemScenario, RptItemSQL, RptItemCurrency)
VALUES (@ReportID, @DisplayOrder, @ReportItemID, @ItemType,
	@LabelOverride, @FormatOverride, @DisplayUnitsOverride, 
	@RptItemScenario, @RptItemSQL, @RptItemCurrency)


