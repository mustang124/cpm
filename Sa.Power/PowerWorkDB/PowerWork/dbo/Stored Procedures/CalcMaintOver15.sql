﻿
CREATE PROCEDURE CalcMaintOver15(@SiteID SiteID)

AS
BEGIN
	SET NOCOUNT ON
	
	--SW 8/26/14
	--made this to calculate the maintenance cost over 15 years old, which now goes in the presentation
	--this is mostly built from some temp code I wrote to get working, and hasn't had much cleanup
	--we simply calculate the difference between the overhaul date and previous overhaul date, and if it's >= 15 we count it

	--get the refnums we're interested in
	DECLARE @Units TABLE (Refnum char(12) NOT NULL)
	INSERT @Units (Refnum)
	SELECT Refnum FROM TSort WHERE SiteID = @SiteID

	--now calculate the MWH and MW versions

	UPDATE MaintTotCalc SET AnnOHCostOver15MWH = AnnOHCost15MWH, AnnOHCostOver15MW = AnnOHCost15MW
	FROM(
		SELECT t.Refnum, 
			AnnOHCost15MWH = oh15.AnnOHCost15*1000/gtc.AdjNetMWH2Yr, 
			AnnOHCost15MW = oh15.AnnOHCost15*1000/(nf.NMC2Yr*1000)
		FROM TSort t
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
			LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
			LEFT JOIN (SELECT Refnum, AnnOHCost = SUM(TotAnnOHCost) FROM OHMaint GROUP BY Refnum) oh ON oh.Refnum = t.Refnum
			LEFT JOIN (SELECT o.Refnum, AnnOHCost15 = SUM(TotAnnOHCost) FROM OHMaint o LEFT JOIN TSort t on t.Refnum = o.Refnum
				WHERE o.Refnum IN (SELECT Refnum FROM @Units) AND DATEDIFF(yy,o.PrevOHDate, o.OHDate) >= 15 
				GROUP BY o.Refnum) oh15 ON oh15.Refnum = t.Refnum
			LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM @Units)) b
	WHERE MaintTotCalc.Refnum = b.Refnum

	--then do the EGC version (which requires the MW version because we calc off it, and also requires EGC to be done)

	UPDATE MaintTotCalc SET annohcostover15egc = OHCostover15egc
	FROM(
		SELECT m.Refnum, 
			OHCostOver15EGC = (m.AnnOHCostover15MW * nf.NMC2Yr)/gtc.EGC
		FROM MaintTotCalc m 
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = m.Refnum
			LEFT JOIN NERCFactors nf ON nf.Refnum = m.Refnum
		WHERE m.Refnum IN (SELECT Refnum FROM @Units)) b
	WHERE MaintTotCalc.Refnum = b.Refnum

END
