﻿CREATE PROC [dbo].[spTimelineAddEvent] (@EventNo SMALLINT, @EventType CHAR(2), @EventGroup CHAR(2), @StartTime SMALLDATETIME,
@StopTime SMALLDATETIME, @AC DECIMAL(18,2), @Phase TINYINT, @UUC CHAR(6))

AS

	DECLARE @i AS INT
	DECLARE @Overlap AS FLOAT
	DECLARE @StartOverlap AS DATETIME
	DECLARE @StopOverlap AS DATETIME
	DECLARE @MWLost AS FLOAT
	
	
	DECLARE @cMWAvail AS FLOAT
	DECLARE @cStart AS DATETIME
	DECLARE @cStop AS DATETIME
	DECLARE @CalcIndex AS INTEGER

	DECLARE @CalcEvnt_No SMALLINT
	DECLARE @CalcEvnt_Type CHAR(2)
	DECLARE @CalcEvnt_Group CHAR(2)
	DECLARE @CalcPhase TINYINT
	DECLARE @CalcStartTime SMALLDATETIME
	DECLARE @CalcEndTime SMALLDATETIME
	DECLARE @CalcAvailCap SMALLINT

	DECLARE @NextID INT

    --print CAST(@UUC AS VARCHAR) + ' ' + CAST(@EventNo AS VARCHAR) + ' ' + CAST(@StopTime AS VARCHAR)
	IF @AC = 0
	BEGIN
		SELECT @MWLost = AvailCap FROM TimelineCalc WHERE IDNum = 2
		SET @i = 3
		SELECT @CalcIndex = MAX(IDNum) FROM TimelineCalc 
		WHILE @i <= @CalcIndex 
		BEGIN
			SELECT @cStart = StartTime FROM TimelineCalc WHERE IDNum = @i
			SELECT @cStop = EndTime FROM TimelineCalc WHERE IDNum = @i
			IF @cStart < @StartTime AND @cStop > @StartTime 
			BEGIN
				IF @cStop <= @StopTime
					UPDATE TimelineCalc SET EndTime = @StartTime WHERE IDNum = @i
				ELSE
					IF @cStop > @StopTime 
					BEGIN
--							--do another addevent here 
						SELECT @CalcEvnt_No		= Evnt_No FROM TimelineCalc WHERE IDNum = @i
						SELECT @CalcEvnt_Type	= Evnt_Type FROM TimelineCalc WHERE IDNum = @i
						SELECT @CalcEvnt_Group	= Evnt_Group FROM TimelineCalc WHERE IDNum = @i
						SET	   @CalcStartTime	= @StopTime
						SET    @CalcEndTime		= @cStop
						SELECT @CalcAvailCap	= AvailCap FROM TimelineCalc WHERE IDNum = @i
						SELECT @CalcPhase		= (Phase + 1) FROM TimelineCalc WHERE IDNum = @i
    --print CAST(@UUC AS VARCHAR) + ' ' + CAST(@CalcEvnt_No AS VARCHAR) + ' ' + CAST(@CalcEndTime AS VARCHAR)

						EXEC spTimelineAddEvent @CalcEvnt_No, @CalcEvnt_Type, @CalcEvnt_Group, @CalcStartTime, @CalcEndTime, @CalcAvailCap, @CalcPhase, @UUC
						UPDATE TimelineCalc SET EndTime = @StartTime WHERE IDNum = @i
					END
			END
			SET @i = @i + 1
		END
	END
	ELSE
	BEGIN
		IF @EventNo = 0
			SET @MWLost = 0
		ELSE
		BEGIN
			IF @Phase = 1
			BEGIN
				SELECT @i = MAX(IDNum) FROM TimelineCalc 
				--print cast(@uuc as varchar) + ' ' + cast(@EventNo as varchar) + ' ' + cast(@i as varchar)
				WHILE @i >= 2
				BEGIN
					SELECT @cMWAvail = ISNULL(AvailCap,0) FROM TimelineCalc WHERE IDNum = @i
					--print cast(@uuc as varchar) + ' ' + cast(@EventNo as varchar) + ' ' + cast(@cMWAvail as varchar) + ' ' + cast(@AC as varchar)
					IF @cMWAvail > 0
					BEGIN
						SET @MWLost = cast(@cMWAvail as integer) - cast(@AC as integer)
						--print cast(@MWLost as varchar)
						SET @i = -1
					END
					SET @i = @i - 1
				END
			END
			ELSE
			BEGIN
				SELECT @i = MAX(IDNum) FROM TimelineCalc 
				WHILE @i >= 2
				BEGIN
					SELECT @CalcEvnt_No	= Evnt_No FROM TimelineCalc WHERE IDNum = @i
					IF @CalcEvnt_No = @EventNo 
					BEGIN
						SELECT @MWLost = LostMW FROM TimelineCalc WHERE IDNum = @i
						SET @i = -1 
					END
					SET @i = @i - 1
				END
			END
			IF @MWLost < 0 
				SET @MWLost = 0
		END
	END

	SELECT @NextID = ISNULL(MAX(IDNum) + 1,2) from TimelineCalc

	INSERT TimelineCalc ([IDNum], [UtilityUnitCode], [Evnt_Year], [Evnt_No], [Evnt_Type], [Evnt_Group], 
		[Phase], [StartTime], [EndTime], [AvailCap], [LostMW])
	VALUES (@NextID, @UUC, YEAR(@StartTime), @EventNo, @EventType, @EventGroup,
		@Phase, @StartTime, @StopTime, @AC, @MWLost)

