﻿
CREATE     PROC [dbo].[spCogenCalc](@Refnum Refnum)
AS
--SW 6/21/12 The following line was removed and the line after was added
--Tony requested that both Brayton and Rankine units get Cogen calcs. Simplest way was to ignore the Breaks line below, and add a new line which
--looked at whether the unit had steam sales or not. If it did, it gets past this and populates the tables required
--IF NOT EXISTS (SELECT * FROM Breaks WHERE Refnum = @Refnum AND CogenElec = 'COGEN')
IF NOT EXISTS (SELECT * FROM GenerationTotCalc WHERE Refnum = @Refnum AND ISNULL(StmSales,0) <> 0)
	RETURN 0

UPDATE CTGData
SET FuelMBTU = ISNULL((SELECT SUM(MBTU) FROM Fuel WHERE Fuel.Refnum = CTGData.Refnum AND Fuel.TurbineID = CTGData.TurbineID), 0)
	+  ISNULL((SELECT SUM(MBTU) FROM Coal WHERE Coal.Refnum = CTGData.Refnum AND Coal.TurbineID = CTGData.TurbineID), 0)
, ElecMBTU = (SELECT SUM(ElecMBTU) FROM PowerGeneration e INNER JOIN TSort t ON t.Refnum = e.Refnum AND t.StudyYear = DATEPART(yy, e.Period) WHERE e.Refnum = CTGData.Refnum AND e.TurbineID = CTGData.TurbineID)
WHERE Refnum = @Refnum

UPDATE CTGData
SET ThermEff = ElecMBTU/FuelMBTU*100
WHERE FuelMBTU>0
AND Refnum = @Refnum

IF NOT EXISTS (SELECT * FROM CogenCalc WHERE REfnum = @Refnum)
	INSERT INTO CogenCalc (Refnum) VALUES (@Refnum)
-- FRS per JJC 7/2006 changed
-- < to <= on StmReturnKLB. 
-- and >= to > on StmPassthroughKLB
-- Need to review further.
UPDATE CogenCalc
SET StmExportMBTU = (SELECT SUM(StmSalesMBTU) FROM SteamSales s INNER JOIN TSort t ON t.Refnum = s.Refnum AND t.StudyYear = DATEPART(yy, s.Period) WHERE s.Refnum = CogenCalc.Refnum AND s.StmSalesMBTU>0)
, StmExportKLB = (SELECT SUM(StmSales) FROM SteamSales s INNER JOIN TSort t ON t.Refnum = s.Refnum AND t.StudyYear = DATEPART(yy, s.Period) WHERE s.Refnum = CogenCalc.Refnum AND s.StmSales>0)
, StmReturnMBTU = (SELECT -SUM(StmSalesMBTU) FROM SteamSales s INNER JOIN TSort t ON t.Refnum = s.Refnum AND t.StudyYear = DATEPART(yy, s.Period) WHERE s.Refnum = CogenCalc.Refnum AND s.StmSalesMBTU<0)
, StmReturnKLB = (SELECT -SUM(StmSales) FROM SteamSales s INNER JOIN TSort t ON t.Refnum = s.Refnum AND t.StudyYear = DATEPART(yy, s.Period) WHERE s.Refnum = CogenCalc.Refnum AND s.StmSales<0 AND s.StmPress <= (SELECT MIN(StmPress) FROM SteamSales e WHERE e.Refnum = s.Refnum AND e.StmSales>0))
, StmPassthroughKLB = (SELECT -SUM(StmSales) FROM SteamSales s INNER JOIN TSort t ON t.Refnum = s.Refnum AND t.StudyYear = DATEPART(yy, s.Period) WHERE s.Refnum = CogenCalc.Refnum AND s.StmSales<0 AND s.StmPress > (SELECT MIN(StmPress) FROM SteamSales e WHERE e.Refnum = s.Refnum AND e.StmSales>0))
, StmNetMBTU = (SELECT SUM(StmSalesMBTU) FROM SteamSales s INNER JOIN TSort t ON t.Refnum = s.Refnum AND t.StudyYear = DATEPART(yy, s.Period) WHERE s.Refnum = CogenCalc.Refnum)
, ElecMBTU = (SELECT SUM(ElecMBTU) FROM PowerGeneration e INNER JOIN TSort t ON t.Refnum = e.Refnum AND t.StudyYear = DATEPART(yy, e.Period) WHERE e.Refnum = CogenCalc.Refnum)
, TotInputMBTU = ISNULL((SELECT SUM(MBTU) FROM Fuel WHERE Fuel.Refnum = CogenCalc.Refnum), 0)
	+  ISNULL((SELECT SUM(MBTU) FROM Coal WHERE Coal.Refnum = CogenCalc.Refnum), 0)
, CTGFuelMBTU = (SELECT SUM(FuelMBTU) FROM CTGData WHERE CTGData.Refnum = CogenCalc.Refnum)
, CTGElecMBTU = (SELECT SUM(ElecMBTU) FROM CTGData WHERE CTGData.Refnum = CogenCalc.Refnum)
WHERE Refnum = @Refnum

UPDATE CogenCalc
SET TotExportMBTU = ISNULL(StmExportMBTU, 0) + ISNULL(ElecMBTU, 0),
    NetExportMBTU = ISNULL(StmNetMBTU, 0) + ISNULL(ElecMBTU, 0),
    TotInputMBTU = ISNULL(TotInputMBTU, 0) + ISNULL(StmReturnMBTU, 0),
    StmReturnPcnt = 100 * ISNULL(StmReturnKLB, 0) / CASE WHEN (ISNULL(StmExportKLB, 0) - ISNULL(StmPassthroughKLB, 0)) = 0 THEN NULL ELSE (ISNULL(StmExportKLB, 0) - ISNULL(StmPassthroughKLB, 0)) END,
    NetStmExportKLb = ISNULL(StmExportKLB, 0) - ISNULL(StmPassthroughKLB, 0)
WHERE Refnum = @Refnum

UPDATE CogenCalc
SET ThermPcnt = CASE WHEN NetExportMBTU > 0 THEN StmNetMBTU/NetExportMBTU*100 ELSE NULL END
, ThermEff = (TotExportMBTU/TotInputMBTU)*100
, CTGThermEff = 100 * ISNULL(CTGElecMBTU, 0)/CASE WHEN CTGFuelMBTU = 0 THEN NULL ELSE CTGFuelMBTU END
WHERE Refnum = @Refnum

UPDATE CogenCalc
SET ElecHeatRate = 1000*(f.TotMBTU - ISNULL(CogenCalc.StmNetMBTU, 0))/g.NetMWH
FROM CogenCalc INNER JOIN GenerationTotCalc g ON g.Refnum = CogenCalc.Refnum
INNER JOIN FuelTotCalc f ON f.REfnum = CogenCalc.Refnum
WHERE CogenCalc.Refnum = @Refnum

DECLARE @NetMWH real, @NetMWH2Yr real
SELECT @NetMWH = NetMWH, @NetMWH2Yr = NetMWH + ISNULL(PriorNetMWH, 0)
FROM GenerationTotCalc WHERE Refnum = @Refnum

DECLARE @ElecNMC real, @MaxElecMWH real, @MaxElecMWSH real, @EUF real, @EAF real, @EFOR real

SELECT @ElecNMC = SUM(NMC), @MaxElecMWH = SUM(NMC*PeriodHrs), @MaxElecMWSH = SUM(NMC*ServiceHrs), 
@EUF = SUM(EUF*NMC*PeriodHrs)/SUM(NMC*PeriodHrs), @EAF = SUM(EAF*NMC*PeriodHrs)/SUM(NMC*PeriodHrs), @EFOR = SUM(EFOR*EFORWtFactor)/SUM(EFORWtFactor)
FROM NERCTurbine
WHERE Refnum = @Refnum AND TurbineType IN ('CTG', 'CST', 'STG')

UPDATE CogenCalc
SET ElecNMC = @ElecNMC, MaxElecMWH = @MaxElecMWH, ElecEFOR = @EFOR,
ElecNCF = (@NetMWH/@MaxElecMWH)*100, ElecNOF = (@NetMWH/@MaxElecMWSH)*100
WHERE Refnum = @Refnum

SELECT @MaxElecMWH = SUM(NMC2Yr*PeriodHrs2Yr), @MaxElecMWSH = SUM(NMC2Yr*ServiceHrs2Yr), 
@EUF = SUM(EUF2Yr*NMC2Yr*PeriodHrs2Yr)/SUM(NMC2Yr*PeriodHrs2Yr), @EAF = SUM(EAF2Yr*NMC2Yr*PeriodHrs2Yr)/SUM(NMC2Yr*PeriodHrs2Yr), @EFOR = SUM(EFOR2Yr*EFOR2YrWtFactor)/SUM(EFOR2YrWtFactor)
FROM NERCTurbine
WHERE Refnum = @Refnum AND TurbineType IN ('CTG', 'CST', 'STG')

UPDATE CogenCalc
SET MaxElecMWH2Yr = @MaxElecMWH, ElecEFOR2Yr = @EFOR,
ElecNCF2Yr = (@NetMWH2Yr/@MaxElecMWH)*100, ElecNOF2Yr = (@NetMWH2Yr/@MaxElecMWSH)*100
WHERE Refnum = @Refnum

DECLARE @TotCash real, @TotCashLessFuel real, @ActCashLessFuel real, @MaintCost real, @TotCashLessFuelEmm real
SELECT @TotCash = TotCash, @TotCashLessFuel = TotCashLessFuel, @ActCashLessFuel = ActCashLessFuel, 
@MaintCost = (SELECT AnnMaintCost FROM MaintTotCalc WHERE MaintTotCalc.Refnum = OpexCalc.Refnum),
@TotCashLessFuelEmm = TotCashLessFuelEm
FROM OpexCalc
WHERE Refnum = @Refnum AND DataType = 'ADJ'

UPDATE CogenCalc
SET TotCashOpexMBTU = 1000*@TotCash/NetExportMBTU,
TotCashLessFuelMBTU = 1000*@TotCashLessFuel/NetExportMBTU,
TotCashLessFuelEmmMBTU = 1000*@TotCashLessFuelEmm/NetExportMBTU,
PlantMngMBTU = 1000*@ActCashLessFuel/NetExportMBTU,
AnnMaintCostMBTU = 1000*@MaintCost/NetExportMBTU
WHERE Refnum = @Refnum

UPDATE CogenCalc
SET MaxMBTUExport = ISNULL((SELECT SUM(NMC_MBTU*PeriodHrs) FROM NERCTurbine WHERE TurbineType IN ('SS') AND NERCTurbine.Refnum = CogenCalc.Refnum), 0),
	ThermEUF = (SELECT SUM(NMC_MBTU*EUF)/SUM(NMC_MBTU) FROM NERCTurbine WHERE TurbineType IN ('SS') AND NERCTurbine.Refnum = CogenCalc.Refnum),
	TotStmMakeMBTU = ISNULL(StmNetMBTU, 0) + ISNULL((SELECT SUM(GrossMWH)*12.5 FROM PowerGeneration e INNER JOIN TSort t ON t.Refnum = e.Refnum AND t.StudyYear = DATEPART(yy, e.Period) WHERE e.Refnum = CogenCalc.Refnum AND e.TurbineID = 'Site'), 0)
WHERE Refnum = @Refnum

IF EXISTS (SELECT * FROM NERCTurbine WHERE REfnum = @Refnum AND TurbineType IN ('BLR','HRSG'))
BEGIN
	UPDATE CogenCalc
	SET ThermNMC = (SELECT SUM(NMC_MBTU) FROM NERCTurbine WHERE TurbineType IN ('HRSG', 'BLR') AND NERCTurbine.Refnum = CogenCalc.Refnum),
	MaxStmMakeMBTU = (SELECT SUM(NMC_MBTU*PeriodHrs) FROM NERCTurbine WHERE TurbineType IN ('HRSG', 'BLR') AND NERCTurbine.Refnum = CogenCalc.Refnum)
	WHERE Refnum = @Refnum
END
ELSE BEGIN
	UPDATE CogenCalc
	SET ThermNMC = ISNULL((SELECT SUM(NMC_MBTU) FROM NERCTurbine WHERE TurbineType = 'SS' AND NERCTurbine.Refnum = CogenCalc.Refnum), 0)
		+ ISNULL((SELECT SUM(NMC)*12.5 FROM NERCTurbine WHERE TurbineType IN ('CST', 'STG') AND NERCTurbine.Refnum = CogenCalc.Refnum), 0),
	MaxStmMakeMBTU = ISNULL((SELECT SUM(NMC_MBTU*PeriodHrs) FROM NERCTurbine WHERE TurbineType = 'SS' AND NERCTurbine.Refnum = CogenCalc.Refnum), 0)
		+ ISNULL((SELECT SUM(NMC*PeriodHrs)*12.5 FROM NERCTurbine WHERE TurbineType IN ('CST', 'STG') AND NERCTurbine.Refnum = CogenCalc.Refnum), 0)
	WHERE Refnum = @Refnum
END

UPDATE CogenCalc
SET 	ThermNCF = TotStmMakeMBTU/MaxStmMakeMBTU*100, 
	StmRedFactor = CASE WHEN MaxMBTUExport > 0 THEN MaxStmMakeMBTU/MaxMBTUExport END
WHERE MaxStmMakeMBTU > 0 AND Refnum = @Refnum

UPDATE CogenCalc
SET StmRedFactor = 1
WHERE Refnum = @Refnum AND StmRedFactor IS NULL AND MaxMBTUExport > 0





