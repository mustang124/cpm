﻿CREATE PROCEDURE spAddReportItemDef
	@FactorA DD_ID, @FactorB DD_ID, @Method tinyint,
	@Multiplier real = 1, @Label varchar(50) = '',
	@Format tinyint = 0, @SectionID tinyint = 0,
	@Scenario Scenario, @SQL varchar(255), @Join varchar(255),
	@Currency varchar(5) = NULL,
	@ID DD_ID OUTPUT
AS
IF EXISTS (SELECT * FROM ReportItemDef WHERE Label = @Label)
	RETURN -101
ELSE
BEGIN
	INSERT INTO ReportItemDef (FactorA, FactorB, Method,
		Multiplier, Label, Format, SectionID, ItemDefScenario,
		ItemDefSQL, JoinOverride, ItemDefCurrency)
	VALUES (@FactorA, @FactorB, @Method, @Multiplier, @Label,
		@Format, @SectionID, @Scenario, @SQL, @Join, @Currency)
	SELECT @ID = ReportItemID FROM ReportItemDef
	WHERE Label = @Label
END

