﻿

CREATE PROC [dbo].[spCopyClean](@SiteID SiteID)
AS

BEGIN TRY

		DECLARE @sql nvarchar(500)

		DECLARE cUnits CURSOR LOCAL FAST_FORWARD
		FOR	SELECT Refnum FROM TSort WHERE SiteID = @SiteID
		DECLARE @Refnum Refnum
		OPEN cUnits
		FETCH NEXT FROM cUnits INTO @Refnum
		WHILE @@FETCH_STATUS = 0
		BEGIN

			DECLARE RefnumTables CURSOR LOCAL FAST_FORWARD FOR
				SELECT ObjectName FROM DD_Objects WHERE CopyData = 1 AND TableType IN ('I','O')
			DECLARE @table varchar(50)
			OPEN RefnumTables
			FETCH NEXT FROM RefnumTables INTO @table
			WHILE @@FETCH_STATUS = 0
			BEGIN

				SET @sql = 'DELETE FROM Power.dbo.' + rtrim(@table) + ' WHERE Refnum = ''' + rtrim(@Refnum) + ''''
				exec sp_executesql @sql
				--print @sql
				SET @sql = 'INSERT Power.dbo.' + rtrim(@table) + ' SELECT * FROM PowerWork.dbo.' + rtrim(@table) + ' WHERE Refnum = ''' + rtrim(@Refnum) + ''''
				exec sp_executesql @sql
				--print @sql

				FETCH NEXT FROM RefnumTables INTO @table

			END
			CLOSE RefnumTables
			DEALLOCATE RefnumTables

			FETCH NEXT FROM cUnits INTO @Refnum
		END
		CLOSE cUnits
		DEALLOCATE cUnits


		DECLARE SiteTables CURSOR LOCAL FAST_FORWARD FOR
			SELECT ObjectName FROM DD_Objects WHERE CopyData = 1 AND TableType IN ('P')
		--DECLARE @table varchar(50)
		OPEN SiteTables
		FETCH NEXT FROM SiteTables INTO @table
		WHILE @@FETCH_STATUS = 0
		BEGIN

			SET @sql = 'DELETE FROM Power.dbo.' + rtrim(@table) + ' WHERE SiteID = ''' + rtrim(@SiteID) + ''''
			exec sp_executesql @sql
			--print @sql
			SET @sql = 'INSERT Power.dbo.' + rtrim(@table) + ' SELECT * FROM PowerWork.dbo.' + rtrim(@table) + ' WHERE SiteID = ''' + rtrim(@SiteID) + ''''
			exec sp_executesql @sql
			--print @sql

			FETCH NEXT FROM SiteTables INTO @table

		END
		CLOSE SiteTables
		DEALLOCATE SiteTables

		--we delete any old error message
		DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CopyData'
		
END TRY
BEGIN CATCH

	--we delete any old error message
	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CopyData'

	EXEC sp_InsertMessage @siteid, 37, 'CopyData', 'CopyData'

	RAISERROR ('spCopyClean failed.',16,1)

END CATCH
--GO


