﻿
-- =============================================
-- Author:		Steve West
-- Create date: 06/06/2012
-- Description:	Calculates EGC based ON Rick's new method
-- Based off spEGCCalcs2007, then modified to use the new calcs
-- =============================================
CREATE PROCEDURE [dbo].[spEGCCalcs2011] 
	@refnum varchar(12) = null,
	@year int = 0, 
	--CAUTION! This is the year it will be calced against. That means you can pass a potentially invalid unit to the stored proc.
	--for example, if you pass unit 123456P09 with year 2012, then that unit's EGC fields will be calced against 2012 coefficients, and put
	--into the database that way. You should really check this carefully.
	--(Why did I do it this way? To make it easier to test old units in the EGC spreadsheets from Rick)
	--Normally you you should pass the same @year value that is in the TSort.StudyYear field, and should only use a different year 
	--when you are running in DEBUG mode.
	@DEBUG bit = 0
	--if you run with @debug set to 0 (the default) it will calc the unit and populate various tables
	--if you set @debug to 1 it will print messages and not write anything to tables

AS
BEGIN
	SET NOCOUNT ON
	
	IF @year < 2013 --SW 5/14/14 added this so that it would switch to EGCCalcs2013 when appropriate
					--if year is less than listed value it will execute, otherwise it will go to the else
					--at the bottom, which sends it on to EGCCalcs2013

	BEGIN
	
		SET @refnum = rtrim(@refnum)
		
		IF @year < 2011 -- and this makes sure it doesn't calc any year earlier than it should
			return

		DECLARE @EGC_Total REAL

	----------COGENS----------
	--SW 6/14/13 it was decided that Cogens should be treated differently for 2012
	--the ultimate decision was that any unit that had non-zero steam output (i.e. a cogen) should be split into the cogen group 
	--(other possibility was that at least 20% of production had to be steam)
	--so now we need to pull out cogens first, then look at Rankine units, then CC

	--step 1 is to determine if the unit had non-zero steam
	--simple query of the GenerationTotCalc table
		DECLARE @IsCogen bit
		
		SELECT @IsCogen = CASE StmSales WHEN 0 THEN 0 ELSE 1 END
			FROM GenerationTotCalc 
			WHERE Refnum = @refnum

		IF @year < 2012 --because this wasn't used before 2012
			SET @IsCogen = 0

		--IF @refnum = '146CC12' --Tarragona test
		--TURNING OFF THE COGEN MODEL FOR 2012, BECAUSE WE DECIDED IT WASN'T WORKING RIGHT
		--SW 8/1/13 WE NEED TO RESET THIS FLAG (OR DELETE IT AND CHANGE THE IF @Year ABOVE) IN 2014
		--REALLY HOPE WE REMEMBER TO DO THAT
		--AND YES THIS IS ALL CAPS FOR A REASON
		SET @IsCogen = 0

		IF @IsCogen = 1
		BEGIN

	----------------------------------------------------------------------------
	----------------------------------------------------------------------------
	----------------------------------------------------------------------------
			
			--Okay, it's a Cogen, let's process it
			DECLARE @EGC_Cogen_HRSG REAL
			DECLARE @EGC_Cogen_Coal_NDC REAL
			DECLARE @EGC_Cogen_CTG_NDC REAL
			DECLARE @EGC_Cogen_AdjNetMWh2Yr REAL
			DECLARE @EGC_Cogen_StmSales REAL
			DECLARE @EGC_Cogen_StmSalesMWh REAL
			DECLARE @EGC_Cogen_TotStarts REAL
			DECLARE @EGC_Cogen_CTGAvgTotStarts REAL
			DECLARE @EGC_Cogen_HeatRate REAL
			DECLARE @EGC_Cogen_FiringTemp REAL
			DECLARE @EGC_Cogen_Age REAL
			DECLARE @EGC_Cogen_NatGasMBTU REAL
			DECLARE @EGC_Cogen_OffGasMBTU REAL
			DECLARE @EGC_Cogen_DieselFuelOilMBTU REAL
			DECLARE @EGC_Cogen_JetTurbMBTU REAL
			DECLARE @EGC_Cogen_CoalMBTU REAL
			DECLARE @EGC_Cogen_Gas2YrMWh REAL
			
			
			----------Cogen_HRSG----------

			SELECT @EGC_Cogen_HRSG = 
				CASE ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0))
					WHEN 0 THEN 0
					ELSE POWER(nf.NDC - ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0)), egc.Pwr_Cogen_HRSG)
					END
				* egc.Cogen_HRSG
			FROM TSort t
				LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ISNULL(ndc,0)) FROM NERCTurbine WHERE turbinetype = 'CTG' GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
				LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_HRSG = ' + CAST(@EGC_Cogen_HRSG AS VARCHAR)


			----------Cogen_Coal_NDC----------

			SELECT @EGC_Cogen_Coal_NDC = 
				POWER(CASE ftc.FuelType WHEN 'Gas' THEN 0 ELSE nf.NDC END, egc.Pwr_Cogen_Coal_NDC)
				* egc.Cogen_Coal_NDC
			FROM TSort t
				LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_Coal_NDC = ' + CAST(@EGC_Cogen_Coal_NDC AS VARCHAR)


			----------Cogen_CTG_NDC----------
			
			SELECT @EGC_Cogen_CTG_NDC = 
				POWER(ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0)), egc.Pwr_Cogen_CTG_NDC)
				* egc.Cogen_CTG_NDC
			FROM TSort t
				LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ISNULL(ndc,0)) FROM NERCTurbine WHERE turbinetype = 'CTG' GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_CTG_NDC = ' + CAST(@EGC_Cogen_CTG_NDC AS VARCHAR)
				
						
			----------Cogen_AdjNetMWh2Yr----------

			SELECT @EGC_Cogen_AdjNetMWh2Yr = 
				POWER((gtc.AdjNetMWH2Yr/1000) + 0.56, egc.Pwr_Cogen_AdjNetMWh2Yr) -- yet another magic number
				* egc.Cogen_AdjNetMWh2Yr
			FROM TSort t
				LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_AdjNetMWh2Yr = ' + CAST(@EGC_Cogen_AdjNetMWh2Yr AS VARCHAR)
				
				
			----------Cogen_StmSales----------

			SELECT @EGC_Cogen_StmSales = 
				POWER((gtc.StmSales + 63450)/1000, egc.Pwr_Cogen_StmSales) -- yet another magic number
				* egc.Cogen_StmSales
			FROM TSort t
				LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_StmSales = ' + CAST(@EGC_Cogen_StmSales AS VARCHAR)
				
							
			----------Cogen_StmSalesMWh----------

			SELECT @EGC_Cogen_StmSalesMWh = 
				POWER((gtc.StmSalesMWH + 364469)/1000, egc.Pwr_Cogen_StmSalesMWh) -- yet another magic number
				* egc.Cogen_StmSalesMWh
			FROM TSort t
				LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_StmSalesMWh = ' + CAST(@EGC_Cogen_StmSalesMWh AS VARCHAR)
				

			----------Cogen_TotStarts----------

			SELECT @EGC_Cogen_TotStarts = 
				POWER(gtc.TotStarts, egc.Pwr_Cogen_TotStarts)
				* egc.Cogen_TotStarts
			FROM TSort t
				LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_TotStarts = ' + CAST(@EGC_Cogen_TotStarts AS VARCHAR)


			----------Cogen_CTGAvgTotStarts----------

			SELECT @EGC_Cogen_CTGAvgTotStarts = 
				POWER(ISNULL(ctgm.AvgTotStarts,0), egc.Pwr_Cogen_CTGAvgTotStarts)
				* egc.Cogen_CTGAvgTotStarts
			FROM TSort t
				LEFT JOIN CTGMaintPerStart ctgm ON ctgm.Refnum = t.Refnum 
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_CTGAvgTotStarts = ' + CAST(@EGC_Cogen_CTGAvgTotStarts AS VARCHAR)


			----------Cogen_HeatRate----------

			SELECT @EGC_Cogen_HeatRate = 
				POWER(LOG(gs.HeatRate), egc.Pwr_Cogen_HeatRate)
				* egc.Cogen_HeatRate
			FROM TSort t
				LEFT JOIN GenSum gs ON gs.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_HeatRate = ' + CAST(@EGC_Cogen_HeatRate AS VARCHAR)


			----------Cogen_FiringTemp----------
			
			SELECT @EGC_Cogen_FiringTemp =
				POWER(ISNULL(ctgd.FiringTemp,0)/1000, egc.Pwr_Cogen_FiringTemp)
				* Cogen_FiringTemp
			FROM TSort t
				LEFT JOIN (SELECT refnum, firingtemp = MAX(ISNULL(FiringTemp,0)) FROM CTGData GROUP BY Refnum) ctgd ON ctgd.Refnum = t.Refnum 
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_FiringTemp = ' + CAST(@EGC_Cogen_FiringTemp AS VARCHAR)

			----------Cogen_Age----------

			SELECT @EGC_Cogen_Age = 
				POWER(nf.Age, egc.Pwr_Cogen_Age)
				* egc.Cogen_Age
			FROM TSort t
				LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_Age = ' + CAST(@EGC_Cogen_Age AS VARCHAR)


			----------Cogen_NatGasMBTU----------
			
			SELECT @EGC_Cogen_NatGasMBTU = 
				POWER(ftc.NatGasMBTU/1000, egc.Pwr_Cogen_NatGasMBTU)
				* egc.Cogen_NatGasMBTU
			FROM TSort t
				LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_NatGasMBTU = ' + CAST(@EGC_Cogen_NatGasMBTU AS VARCHAR)


			----------Cogen_OffGasMBTU----------

			SELECT @EGC_Cogen_OffGasMBTU = 
				POWER(ftc.OffGasMBTU/1000, egc.Pwr_Cogen_OffGasMBTU)
				* egc.Cogen_OffGasMBTU
			FROM TSort t
				LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_OffGasMBTU = ' + CAST(@EGC_Cogen_OffGasMBTU AS VARCHAR)


			----------Cogen_DieselFuelOilMBTU----------

			SELECT @EGC_Cogen_DieselFuelOilMBTU = 
				POWER(((ftc.DieselMBTU + ftc.FuelOilMBTU)/1000), egc.Pwr_Cogen_DieselFuelOilMBTU)
				* egc.Cogen_DieselFuelOilMBTU
			FROM TSort t
				LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_DieselFuelOilMBTU = ' + CAST(@EGC_Cogen_DieselFuelOilMBTU AS VARCHAR)
				
				
			----------Cogen_JetTurbMBTU----------

			SELECT @EGC_Cogen_JetTurbMBTU = 
				POWER(ftc.JetTurbMBTU/1000, egc.Pwr_Cogen_JetTurbMBTU)
				* egc.Cogen_JetTurbMBTU
			FROM TSort t
				LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_JetTurbMBTU = ' + CAST(@EGC_Cogen_JetTurbMBTU AS VARCHAR)
				
				
			----------Cogen_CoalMBTU----------

			SELECT @EGC_Cogen_CoalMBTU = 
				POWER((ftc.CoalMBTU/1000), egc.Pwr_Cogen_CoalMBTU)
				* egc.Cogen_CoalMBTU
			FROM TSort t
				LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_CoalMBTU = ' + CAST(@EGC_Cogen_CoalMBTU AS VARCHAR)
				

			----------Cogen_Gas2YrMWh----------

			SELECT @EGC_Cogen_Gas2YrMWh = 
				CASE ftc.FuelType
					WHEN 'Gas' THEN --if it is a Gas unit
						CASE ISNULL(nt.CTGCount,0) -- and it doesn't have any CTGs
							WHEN 0 THEN 
								POWER((gtc.AdjNetMWH2Yr/1000) + 0.56, egc.Pwr_Cogen_Gas2yrMWh)
							ELSE 0
							END
					ELSE 0
					END
				* egc.Cogen_Gas2yrMwh
			FROM TSort t
				LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				LEFT JOIN (select refnum, CTGCount = COUNT(*) from NERCTurbine where turbinetype = 'CTG' group by Refnum) nt on nt.Refnum = t.Refnum 
				LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
			WHERE t.Refnum = @refnum

			IF @debug = 1
				PRINT 'EGC_Cogen_Gas2YrMWh = ' + CAST(@EGC_Cogen_Gas2YrMWh AS VARCHAR)


			SET @EGC_Total = 
				@EGC_Cogen_HRSG 
				+ @EGC_Cogen_Coal_NDC
				+ @EGC_Cogen_CTG_NDC
				+ @EGC_Cogen_AdjNetMWh2Yr
				+ @EGC_Cogen_StmSales
				+ @EGC_Cogen_StmSalesMWh
				+ @EGC_Cogen_TotStarts
				+ @EGC_Cogen_CTGAvgTotStarts
				+ @EGC_Cogen_HeatRate
				+ @EGC_Cogen_FiringTemp
				+ @EGC_Cogen_Age
				+ @EGC_Cogen_NatGasMBTU
				+ @EGC_Cogen_OffGasMBTU
				+ @EGC_Cogen_DieselFuelOilMBTU
				+ @EGC_Cogen_JetTurbMBTU
				+ @EGC_Cogen_CoalMBTU
				+ @EGC_Cogen_Gas2YrMWh
			
			
				IF @debug = 1
					PRINT 'EGC_Total = ' + CAST(@EGC_Total AS VARCHAR)
						
		
		END --end of Cogen calculation


	----------------------------------------------------------------------------
	----------------------------------------------------------------------------
	----------------------------------------------------------------------------


		
		ELSE -- else is not cogen
		BEGIN
	----------------------------------------------------------------------------
			IF @refnum NOT LIKE '%CC%' --for the Coal units
			BEGIN
				
				DECLARE @EGC_SCR REAL
				DECLARE @EGC_WGS REAL
				DECLARE @EGC_DGS REAL
				DECLARE @EGC_PrecBag REAL
				DECLARE @EGC_OtherSiteCapCoalOil REAL
				DECLARE @EGC_OtherSiteCapGas REAL
				DECLARE @EGC_OtherSiteCap1 REAL --this term added SW 6/12/13 for Rick's new model
				DECLARE @EGC_OtherSiteCap2 REAL --this term added SW 6/12/13 for Rick's new model
				DECLARE @EGC_SteamSalesCoalOil REAL
				DECLARE @EGC_SteamSalesGas REAL
				DECLARE @EGC_Coal_NDC REAL
				--DECLARE @EGC_Oil_NDC REAL
				DECLARE @EGC_Gas_NDC REAL
				DECLARE @EGC_Geothermal_NDC REAL -- added 6/12/13 SW
				--DECLARE @EGC_CTG_NDC REAL
				DECLARE @EGC_Coal1 REAL
				DECLARE @EGC_Coal2 REAL
				DECLARE @EGC_Coal3 REAL
				DECLARE @EGC_GasTotalNonGasUnits REAL
				DECLARE @EGC_GasTotalGasUnits REAL
				DECLARE @EGC_LiquidFuelsGasUnits REAL
				DECLARE @EGC_LiquidFuelsNoSulfurCoal REAL

				----------Coal calculations----------
				--we start with some calcs on the coal to get the data we need
				--basically what this is doing is getting the tons of coal for each coal#, and removing the ash percentage, then summing it for the outputs
				CREATE TABLE #coaltmp (refnum char(12), coalID char(2), Name char(30) NULL, AshPcnt real NULL, Tons real NULL, TonsLessAsh real NULL, CoalType char(15) NULL)
				CREATE TABLE #coal (refnum char(12), CoalType char(15), coalID char(2), Tons real NULL, TonsLessAsh real NULL, AshTons real NULL)

				INSERT #coaltmp  SELECT Refnum, CoalID, Name, AshPcnt, Tons, Tons * ((100 - isnull(ashpcnt,0))/100), '' from Coal where Refnum = @refnum 

				--update the types based on the name
				--Rick varied the calculations based on what type of fuel it was
				--This code cleans up those names a little (does this need continual adjustment?)
				UPDATE #coaltmp SET CoalType = 'Biomass' WHERE Name LIKE '%bio%' OR Name LIKE '%meal%' or Name like '%SUBSTITUTION FUEL%'
				UPDATE #coaltmp SET CoalType = 'Coal Dust' WHERE Name LIKE '%dust%'
				UPDATE #coaltmp SET CoalType = 'Petcoke' WHERE Name LIKE '%petcoke%' OR Name LIKE '%pet coke%'
				UPDATE #coaltmp SET CoalType = 'Sludge' WHERE Name LIKE '%sludge%'
				UPDATE #coaltmp SET CoalType = 'Woodgas' WHERE Name LIKE '%woodgass%' OR Name LIKE '%wood gass%'
				UPDATE #coaltmp SET CoalType = 'Tires' WHERE Name LIKE '%tire%'
				UPDATE #coaltmp SET CoalType = 'Coal Gassified' WHERE Name LIKE '%coal gass%'
				UPDATE #coaltmp SET CoalType = 'Landfill' WHERE Name LIKE '%sklad%'
				UPDATE #coaltmp SET CoalType = 'Coal' WHERE CoalType = ''

				INSERT #coal SELECT refnum, coaltype, coalid, SUM(Tons), SUM(tonslessash), AshTons = SUM(Tons - TonsLessAsh) FROM #coaltmp GROUP BY refnum, CoalType, coalID 

				DROP TABLE #coaltmp
				--we don't need #coaltmp any more, but we keep #coal for a couple of calculations below


				----------SCR----------
				--check NonOHMaint and OHMaint to see if the unit has SCR spending, then multiply by other vars
				SELECT @EGC_SCR =
					ISNULL((CASE WHEN (ISNULL(AnnNonOHCost,0) + ISNULL(TotAnnOHCost,0)) <> 0 THEN 1 ELSE 0 END),0) --if it has any spending then it exists
					* ISNULL(((CASE @year WHEN 2011 THEN gtc.AdjNetMWH2Yr ELSE gtc.AdjNetMWH END)/1000000),0)
					--SW 6/12/13 changed above line, Rick's original 2011 EGC model used AdjNetMWH2Yr but he changed the model in 2012 to use AdjNetMWH
					* ISNULL(egc.Pwr_SCR,0) --multiplying, not really a POWER, but it was in an original version of the model
					* ISNULL(egc.SCR,0)
				FROM TSort t 
					LEFT JOIN (SELECT refnum, AnnNonOHCost = SUM(ISNULL(CurrCptlLocal,0) + ISNULL(CurrMMOCostLocal,0) + ISNULL(CurrExpLocal,0) + ISNULL(PrevCptlLocal,0) + ISNULL(PrevMMOCostLocal,0) + ISNULL(PrevExpLocal,0)) FROM NonOHMaint WHERE Component = 'SCR' GROUP BY Refnum) noh ON noh.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, TotAnnOHCost = SUM(ISNULL(directcostlocal,0) + ISNULL(ammocostlocal,0) + ISNULL(lumpsumcostlocal,0)) FROM OHMaint WHERE Component = 'SCR' GROUP BY Refnum) oh ON oh.Refnum = t.Refnum
					LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_SCR = ' + CAST(@EGC_SCR AS VARCHAR)


				----------WGS----------
				--check NonOHMaint and OHMaint to see if the unit has WGS spending, then multiply by other vars
				SELECT @EGC_WGS = 
					POWER((
						(CASE @year WHEN 2011 THEN 0.001 ELSE 0.001 END) --magic number?
						--SW 6/12/13 changed above line, Rick's original 2011 EGC model multiplied by 0.001 but 2012 model removed this multiplier
						--so I changed it to multiply by 1 to keep the term in the model for 2011
						* ISNULL(CASE WHEN (ISNULL(AnnNonOHCost,0) + ISNULL(TotAnnOHCost,0)) <> 0 THEN 1 ELSE 0 END,0) --if it has any spending then it exists
						* ISNULL(SulfurTons,0))
						,egc.Pwr_WGS)
					* egc.WGS
				FROM TSort t
					LEFT JOIN (SELECT refnum, AnnNonOHCost = SUM(ISNULL(CurrCptlLocal,0) + ISNULL(CurrMMOCostLocal,0) + ISNULL(CurrExpLocal,0)) FROM NonOHMaint WHERE Component = 'WGS' GROUP BY Refnum) n ON n.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, TotAnnOHCost = SUM(ISNULL(directcostlocal,0) + ISNULL(ammocostlocal,0) + ISNULL(lumpsumcostlocal,0)) FROM OHMaint WHERE Component = 'WGS' GROUP BY Refnum) o ON o.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, SulfurTons = SUM(ISNULL(Tons,0) * ISNULL(SulfurPcnt,0)) /100 FROM Coal GROUP BY Refnum) coalsulfur ON coalsulfur.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_WGS = ' + CAST(@EGC_WGS AS VARCHAR)


				----------DGS----------
				--check NonOHMaint and OHMaint to see if the unit has DGS spending, then multiply by other vars
				SELECT @EGC_DGS = 
					POWER((
						(CASE @year WHEN 2011 THEN 0.001 ELSE 0.001 END) --magic number?
						--SW 6/12/13 changed above line, Rick's original 2011 EGC model multiplied by 0.001 but 2012 model removed this multiplier
						--so I changed it to multiply by 1 to keep the term in the model for 2011
						* ISNULL(CASE WHEN (ISNULL(AnnNonOHCost,0) + ISNULL(TotAnnOHCost,0)) <> 0 THEN 1 ELSE 0 END,0) --if it has any spending then it exists
						--* ISNULL(CASE WHEN (AnnNonOHCost + TotAnnOHCost) <> 0 THEN 1 ELSE 0 END,0) --if it has any spending then it exists
						* ISNULL(SulfurTons,0))
						,egc.Pwr_DGS)
					* egc.DGS
				FROM TSort t
					LEFT JOIN (SELECT refnum, AnnNonOHCost = SUM(ISNULL(CurrCptlLocal,0) + ISNULL(CurrMMOCostLocal,0) + ISNULL(CurrExpLocal,0)) FROM NonOHMaint WHERE Component = 'DGS' GROUP BY Refnum) n ON n.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, TotAnnOHCost = SUM(ISNULL(directcostlocal,0) + ISNULL(ammocostlocal,0) + ISNULL(lumpsumcostlocal,0)) FROM OHMaint WHERE Component = 'DGS' GROUP BY Refnum) o ON o.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, SulfurTons = SUM(ISNULL(Tons,0) * ISNULL(SulfurPcnt,0)) /100 FROM Coal GROUP BY Refnum) coalsulfur ON coalsulfur.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_DGS = ' + CAST(@EGC_DGS AS VARCHAR)


				----------PrecBag----------
				--using calcs from the #coal which was calculated above
				SELECT @EGC_PrecBag = 
					POWER(ISNULL(coalash.AshTons,0)/1000, egc.Pwr_PrecBag)
					* egc.PrecBag
				FROM TSort t
					LEFT JOIN (SELECT refnum, AshTons = SUM(Ashtons) FROM #Coal WHERE CoalType = 'Coal' GROUP BY Refnum) coalash ON coalash.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_PrecBag = ' + CAST(@EGC_PrecBag AS VARCHAR)


				----------Model Change----------
				IF @year = 2011
				-- SW 6/12/13 Model changed from 2012 to 2013
				-- the OtherSiteCap was no longer distinguished between coal/oil/gas
				-- instead Rick used two separate terms for OtherSiteCap
				-- so i added this IF statement, if we're doing 2011 it will do this, ELSE it will do the 2012 version (and whatever happens in the future)

				BEGIN

					--start by setting the new model terms to 0 so the calcs will continue to work below
					SET @EGC_OtherSiteCap1 = 0
					SET @EGC_OtherSiteCap2 = 0
				
					----------OtherSiteCapCoalOil----------
					--8/21/12 SW Changed OtherSiteCap to CoalOil and Gas versions, adjusted calculations
					--MAGIC NUMBERS ALERT! Where did those numbers come from and why aren't they in the Coeffs table?
					SELECT @EGC_OtherSiteCapCoalOil = 
						CASE ftc.FuelType 
							WHEN 'Gas' THEN 0
							ELSE 
								CASE ISNULL(p.TotUnitNum,ISNULL(t.NumUnitsAtSite,1))
									WHEN 1 THEN 0
									ELSE 91.6442499651945 * POWER((cast((ISNULL(p.TotCap,0) - ISNULL(nf.NDC,0)) as real)/(ISNULL(p.TotUnitNum,ISNULL(t.NumUnitsAtSite,1))-1)),0.610759682791099) - 382.799775241381 * POWER(cast((ISNULL(p.TotCap,0) - ISNULL(nf.NDC,0)) as real), 0.333) - 495.435453279966 * POWER((ISNULL(p.TotCap,0)/ISNULL(nf.NDC,0)),0.333) 
									END
							END
						* egc.OtherSiteCapCoalOil
					FROM TSort t
						LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
						--left join (select Refnum = @refnum, SiteNDC = SUM(ndc) from NERCFactors nf where Refnum in (select Refnum from TSort where SiteID in (select SiteID from TSort where Refnum = @refnum))) SiteTot ON SiteTot.refnum = T.Refnum
						LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
						LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
						LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
					WHERE t.Refnum = @refnum

					IF @debug = 1
						PRINT 'EGC_OtherSiteCapCoalOil = ' + CAST(@EGC_OtherSiteCapCoalOil AS VARCHAR)
						

					----------OtherSiteCapGas----------
					--MAGIC NUMBERS ALERT! Where did those numbers come from and why aren't they in the Coeffs table?
					SELECT @EGC_OtherSiteCapGas = 
						CASE ftc.FuelType 
							WHEN 'Gas' THEN 
								CASE ISNULL(p.TotUnitNum,ISNULL(t.NumUnitsAtSite,1))
									WHEN 1 THEN 0
									ELSE 91.6442499651945 * POWER((cast((ISNULL(p.TotCap,0) - ISNULL(nf.NDC,0)) as real)/(ISNULL(p.TotUnitNum,ISNULL(t.NumUnitsAtSite,1))-1)),0.610759682791099) - 382.799775241381 * POWER(cast((ISNULL(p.TotCap,0) - ISNULL(nf.NDC,0)) as real), 0.333) - 495.435453279966 * POWER((ISNULL(p.TotCap,0)/ISNULL(nf.NDC,0)),0.333) 
									END
							ELSE 0
							END
						* egc.OtherSiteCapGas
					FROM TSort t
						LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
						--left join (select Refnum = @refnum, SiteNDC = SUM(ndc) from NERCFactors nf where Refnum in (select Refnum from TSort where SiteID in (select SiteID from TSort where Refnum = @refnum))) SiteTot ON SiteTot.refnum = T.Refnum
						LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
						LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
						LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
					WHERE t.Refnum = @refnum

					IF @debug = 1
						PRINT 'EGC_OtherSiteCapGas = ' + CAST(@EGC_OtherSiteCapGas AS VARCHAR)
				
				END

				ELSE
				----------this is where the new terms were added for OtherSiteCap 1 & 2 from 2012 on - SW 6/12/13
				BEGIN

					--start by setting the 2011 model terms to 0 so the calcs will continue to work below
					SET @EGC_OtherSiteCapCoalOil = 0
					SET @EGC_OtherSiteCapGas = 0

					----------OtherSiteCap1----------
					SELECT @EGC_OtherSiteCap1 = 
						SIGN(((ISNULL(p.TotCap,0)/ISNULL(p.TotUnitNum,ISNULL(t.NumUnitsAtSite,1)))- ISNULL(nf.NDC,0)))
						--SIGN(((ISNULL(SiteTot.SiteNDC,0)/ISNULL(t.NumUnitsAtSite,1))- ISNULL(nf.NDC,0)))
						* POWER(ABS(((ISNULL(p.TotCap,0)/ISNULL(p.TotUnitNum,ISNULL(t.NumUnitsAtSite,1)))- ISNULL(nf.NDC,0))), egc.Pwr_OtherSiteCap1)
						--* POWER(ABS(((ISNULL(SiteTot.SiteNDC,0)/ISNULL(t.NumUnitsAtSite,1))- ISNULL(nf.NDC,0))), egc.Pwr_OtherSiteCap1)
						* egc.OtherSiteCap1
					FROM TSort t
						LEFT JOIN (SELECT Refnum = @refnum, SiteNDC = SUM(ndc) from NERCFactors nf where Refnum in (select Refnum from TSort where SiteID in (select SiteID from TSort where Refnum = @refnum))) SiteTot ON SiteTot.refnum = T.Refnum
						LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
						LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
						LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
					WHERE t.Refnum = @refnum

					IF @debug = 1
						PRINT 'EGC_OtherSiteCap1 = ' + CAST(@EGC_OtherSiteCap1 AS VARCHAR)


					----------OtherSiteCap2----------
					SELECT @EGC_OtherSiteCap2 = 
						((ISNULL(nf.NDC,0)/ISNULL(p.TotCap,0)) - 0.5)
						--((ISNULL(nf.NDC,0)/ISNULL(SiteTot.SiteNDC,0)) - 0.5)
						* egc.OtherSiteCap2
					FROM TSort t
						LEFT JOIN (SELECT Refnum = @refnum, SiteNDC = SUM(ndc) from NERCFactors nf where Refnum in (select Refnum from TSort where SiteID in (select SiteID from TSort where Refnum = @refnum))) SiteTot ON SiteTot.refnum = T.Refnum
						LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
						LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
						LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
					WHERE t.Refnum = @refnum
					
					IF @debug = 1
						PRINT 'EGC_OtherSiteCap2 = ' + CAST(@EGC_OtherSiteCap2 AS VARCHAR)
				
				END


				----------SteamSalesCoalOil----------

				SELECT @EGC_SteamSalesCoalOil = 
					--SW 6/12/13 calculation was changed in 2012
					CASE ftc.FuelType 
						WHEN 'Gas' THEN 0 
						ELSE POWER(CASE @year WHEN 2011 THEN gtc.StmSalesMWH ELSE gtc.StmSales END/CASE @year WHEN 2011 THEN 1000000 ELSE 1000 END, Pwr_SteamSalesCoalOil)
						END
					* SteamSalesCoalOil
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
					LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_SteamSalesCoalOil = ' + CAST(@EGC_SteamSalesCoalOil AS VARCHAR)


				----------SteamSalesGas----------

				SELECT @EGC_SteamSalesGas = 
					CASE ftc.FuelType 
						WHEN 'Gas' THEN 
							CASE @year
								WHEN 2011 THEN POWER(gtc.StmSalesMWH/1000000, Pwr_SteamSalesGas) --SW 6/12/13 changed calc for 2012
								ELSE POWER(gtc.StmSales/1000, Pwr_SteamSalesGas)
								END
						ELSE 0
						END
					* SteamSalesGas
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
					LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_SteamSalesGas = ' + CAST(@EGC_SteamSalesGas AS VARCHAR)
					
					
				----------Coal_NDC----------

				SELECT @EGC_Coal_NDC = 
					--CASE @year 
						--WHEN 2011 THEN POWER(CASE ftc.FuelType WHEN 'Gas' THEN 0 ELSE nf.NDC END, egc.Pwr_Coal_NDC)
						POWER(CASE ftc.FuelType WHEN 'Gas' THEN 0 ELSE nf.NDC END, egc.Pwr_Coal_NDC)
						--ELSE POWER(CASE ftc.FuelType WHEN 'Coal' THEN nf.NDC ELSE 0 END, egc.Pwr_Coal_NDC) --SW 6/12/13 change for 2012
						--END
					* egc.Coal_NDC
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_Coal_NDC = ' + CAST(@EGC_Coal_NDC AS VARCHAR)


				----------Oil_NDC----------
				--Oil_NDC removed from model, set at 0	

				--SELECT @EGC_Oil_NDC = 
				--	POWER(CASE ftc.FuelType WHEN 'Gas' THEN 0 ELSE nf.NDC END, egc.Pwr_Oil_NDC)
				--	* egc.Oil_NDC
				--FROM TSort t
				--	LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
				--	LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				--	LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				--WHERE t.Refnum = @refnum

				--IF @debug = 1
				--	PRINT 'EGC_Oil_NDC = ' + CAST(@EGC_Oil_NDC AS VARCHAR)


				----------Gas_NDC----------

				SELECT @EGC_Gas_NDC = 
					POWER(CASE ftc.FuelType WHEN 'Gas' THEN nf.NDC ELSE 0 END, egc.Pwr_Gas_NDC)
					* egc.Gas_NDC
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_Gas_NDC = ' + CAST(@EGC_Gas_NDC AS VARCHAR)


				----------Geothermal_NDC---------- SW 6/12/13 added to model
				-- which is baloney because the FuelType is not Geo anywhere in the database
				-- apparently Rick just made it up because I didn't supply it to him that way
				-- so essentially this piece of code is never going to be used, and therefore it has not been checked properly
				
				SELECT @EGC_Geothermal_NDC = 
					POWER(CASE ftc.FuelType WHEN 'Geo' THEN nf.NDC ELSE 0 END, egc.Pwr_Geothermal_NDC)
					* egc.Geothermal_NDC
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_Geothermal_NDC = ' + CAST(@EGC_Geothermal_NDC AS VARCHAR)


				----------Coal1----------
				
				--8/21/12 SW coal 1 removed from the model, not sure why the model wouldn't count it
				--6/12/13 SW coal 1 put back in the model. trust in the validity of the model eroding as it changes randomly
				
				IF @year = 2011
					SET @EGC_Coal1 = 0
				ELSE
				BEGIN
					SELECT @EGC_Coal1 = 
						ISNULL(POWER(c.TonsLessAsh/1000, egc.Pwr_Coal1),0)
						* egc.Coal1
					FROM TSort t
						LEFT JOIN #coal c ON c.CoalType = 'Coal' AND c.coalID like '%1'
						LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
					WHERE t.Refnum = @refnum
				END

				IF @debug = 1
					PRINT 'EGC_Coal1 = ' + CAST(@EGC_Coal1 AS VARCHAR)
				
				----------Coal2----------
				
				SELECT @EGC_Coal2 = 
					ISNULL(POWER(c.TonsLessAsh/1000, egc.Pwr_Coal2),0) --note 2011 divisor = 1000
					* egc.Coal2
				FROM TSort t
					LEFT JOIN #coal c ON c.CoalType = 'Coal' AND c.coalID like '%2'
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_Coal2 = ' + CAST(@EGC_Coal2 AS VARCHAR)
				
				----------Coal3----------
				
				SELECT @EGC_Coal3 = 
					ISNULL(POWER(c.TonsLessAsh/1000, egc.Pwr_Coal3),0) --note 2011 divisor = 1000
					* egc.Coal3
				FROM TSort t
					LEFT JOIN #coal c ON c.CoalType = 'Coal' AND c.coalID like '%3'
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_Coal3 = ' + CAST(@EGC_Coal3 AS VARCHAR)

				
				----------GasTotalNonGasUnits----------
				
				SELECT @EGC_GasTotalNonGasUnits = 
					CASE ftc.fueltype WHEN 'Gas' THEN 0 ELSE POWER(ISNULL(ftc.TotGasMBTU,0)/1000, egc.Pwr_GasTotalNonGasUnits) END
					* egc.GasTotalNonGasUnits
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum			
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_GasTotalNonGasUnits = ' + CAST(@EGC_GasTotalNonGasUnits AS VARCHAR)
				

				----------GasTotalGasUnits----------

				SELECT @EGC_GasTotalGasUnits = 
					CASE ftc.fueltype WHEN 'Gas' THEN POWER(ISNULL(ftc.TotGasMBTU,0)/1000, egc.Pwr_GasTotalGasUnits) ELSE 0 END
					* egc.GasTotalGasUnits
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum			
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_GasTotalGasUnits = ' + CAST(@EGC_GasTotalGasUnits AS VARCHAR)


				----------LiquidFuelsGasUnits----------

				SELECT @EGC_LiquidFuelsGasUnits = 
					CASE ftc.fueltype 
						WHEN 'Gas' THEN --model change in 2012 divided the # of gallons by 1000 SW 6/12/13
							POWER((ISNULL(ftc.JetTurbKGal,0) + ISNULL(ftc.DieselKGal,0) + ISNULL(ftc.FuelOilKGal,0))/(CASE @year WHEN 2011 THEN 1 ELSE 1000 END), egc.Pwr_LiquidFuelsGasUnits) 
						ELSE 0 
						END
					* egc.LiquidFuelsGasUnits
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum			
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_LiquidFuelsGasUnits = ' + CAST(@EGC_LiquidFuelsGasUnits AS VARCHAR)


				----------LiquidFuelsNoSulfurCoal----------

				--IF @year = 2011
				--change in the model dropped this variable after 2011 SW 6/12/13
				--BEGIN
					SELECT @EGC_LiquidFuelsNoSulfurCoal = 
						CASE ftc.fueltype WHEN 'Gas' THEN 0 ELSE POWER(((ISNULL(ftc.JetTurbKGal,0) * 0.9985) + (ISNULL(ftc.DieselKGal,0) * 0.9965) + (ISNULL(ftc.FuelOilKGal,0) * ((100 - ISNULL(fp.Sulfur,0))/100))), egc.Pwr_LiquidFuelsNoSulfurCoal) END
						* egc.LiquidFuelsNoSulfurCoal
					FROM TSort t
						LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
						LEFT JOIN FuelProperties fp on fp.Refnum = t.Refnum		
						LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
					WHERE t.Refnum = @refnum
				--END
				
				--ELSE
				--BEGIN
				--	SET @EGC_LiquidFuelsNoSulfurCoal = 0
				--END
				
				IF @debug = 1
					PRINT 'EGC_LiquidFuelsNoSulfurCoal = ' + CAST(@EGC_LiquidFuelsNoSulfurCoal AS VARCHAR)
				
				
				----------LiquidFuelsSulfurCoal----------	
				--no longer used
					
				--now we can drop the coal table we created
				DROP TABLE #coal


				SET @EGC_Total = 
					@EGC_SCR 
					+ @EGC_WGS 
					+ @EGC_DGS 
					+ @EGC_PrecBag 
					+ @EGC_OtherSiteCapCoalOil 
					+ @EGC_OtherSiteCapGas 
					+ @EGC_OtherSiteCap1 --SW added 6/12/13
					+ @EGC_OtherSiteCap2 --SW added 6/12/13
					+ @EGC_SteamSalesCoalOil 
					+ @EGC_SteamSalesGas 
					+ @EGC_Coal_NDC 
					+ @EGC_Gas_NDC 
					+ @EGC_Geothermal_NDC --SW added 6/12/13
					+ @EGC_Coal1
					+ @EGC_Coal2 
					+ @EGC_Coal3 
					+ @EGC_GasTotalNonGasUnits 
					+ @EGC_GasTotalGasUnits 
					+ @EGC_LiquidFuelsGasUnits 
					+ @EGC_LiquidFuelsNoSulfurCoal 
				
				IF @debug = 1
					PRINT 'EGC_Total = ' + CAST(@EGC_Total AS VARCHAR)

			----------end of PN----------
			END


			----------------------------------------------------------------------------
			----------------------------------------------------------------------------
			----------------------------------------------------------------------------

			
			--now the CC units
			IF @refnum LIKE '%CC%'
			BEGIN

				DECLARE @EGC_Age REAL
				DECLARE @EGC_CTG_NDC REAL
				DECLARE @EGC_FiringTemp REAL
				--DECLARE @EGC_CTG_Starts REAL
				DECLARE @EGC_CTG_AvgCTGSizeUnder150 REAL
				DECLARE @EGC_CTG_AvgCTGSizeOver150 REAL
				DECLARE @EGC_CTG_SiteEffect REAL
				DECLARE @EGC_CTG_SCR_NDC REAL
				DECLARE @EGC_CTG_SteamSales REAL
				DECLARE @EGC_CTG_HRSGCapN REAL
				DECLARE @EGC_CTG_HRSGCap REAL
				DECLARE @EGC_CTG_GasMBTU REAL
				DECLARE @EGC_CTG_LiquidMBTU REAL

			
			
			
				----------Age----------
				
				SELECT @EGC_Age = 
					CASE WHEN nf.Age < 5 
						THEN 0 
						ELSE POWER(ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0)) / FLOOR(nf.Age/5) , egc.Pwr_CTG_Age)
						END
					* egc.CTG_Age
				FROM TSort t
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_Age = ' + CAST(@EGC_Age AS VARCHAR)


				----------CTG_NDC----------
				
				SELECT @EGC_CTG_NDC = 
					POWER((ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0)) / nf.NDC), egc.Pwr_CTG_NDC)
					* egc.CTG_NDC
				FROM TSort t
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_NDC = ' + CAST(@EGC_CTG_NDC AS VARCHAR)		
				
				
				----------FiringTemp----------
				
				SELECT @EGC_FiringTemp = 
					POWER((ISNULL(ctgd.firingtemp,0) * ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0)) / 1000), egc.Pwr_CTG_FiringTemp)
					* egc.CTG_FiringTemp
				FROM TSort t
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN (SELECT refnum, firingtemp = MAX(firingtemp) FROM CTGData GROUP BY Refnum ) ctgd ON ctgd.Refnum = t.Refnum 
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_FiringTemp = ' + CAST(@EGC_FiringTemp AS VARCHAR)			
				
				
				----------CTG_Starts----------
				--updated 8/10/12 SW - removed from model
				
				
				----------CTG_AvgCTGSizeUnder150----------
				
				SELECT @EGC_CTG_AvgCTGSizeUnder150 = 
					CASE 
						WHEN ISNULL(nt.CTG_NDC, t.CTG_NDC)/ISNULL(nt.CTGCount, t.CTGs) > 150 THEN 0 
						ELSE POWER(ISNULL(nf.NDC, 0), egc.Pwr_CTG_AvgSizeUnder150) 
						END
					* egc.CTG_AvgSizeUnder150
				FROM TSort t
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc), CTGCount = COUNT(*) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_AvgCTGSizeUnder150 = ' + CAST(@EGC_CTG_AvgCTGSizeUnder150 AS VARCHAR)	


				----------CTG_AvgCTGSizeOver150----------

				SELECT @EGC_CTG_AvgCTGSizeOver150 = 
					CASE 
						WHEN ISNULL(nt.CTG_NDC, t.CTG_NDC)/ISNULL(nt.CTGCount, t.CTGs) <= 150 THEN 0 
						ELSE POWER(ISNULL(nf.NDC, 0), egc.Pwr_CTG_AvgSizeOver150) 
						END
					* egc.CTG_AvgSizeOver150
				FROM TSort t
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc), CTGCount = COUNT(*) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_AvgCTGSizeOver150 = ' + CAST(@EGC_CTG_AvgCTGSizeOver150 AS VARCHAR)	


				----------CTG_SiteEffect----------

				SELECT @EGC_CTG_SiteEffect = 
					POWER((0.0074885512878783 * POWER(p.TotCap/ISNULL(p.TotUnitNum, t.NumUnitsAtSite), 2) + POWER(nf.NDC, 1.10029714213866)), egc.Pwr_CTG_SiteEffect)
					 --beware magic numbers from the spreadsheet
					* egc.CTG_SiteEffect
				FROM TSort t
					--LEFT JOIN (SELECT siteid, SiteNDC = SUM(nf.ndc) FROM TSort t LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum GROUP BY SiteID) b ON b.SiteID = t.SiteID
					LEFT JOIN NERCFactors nf on nf.Refnum = t.Refnum
					LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_SiteEffect = ' + CAST(@EGC_CTG_SiteEffect AS VARCHAR)

				----------CTG_SCR_NDC----------
				
				SELECT @EGC_CTG_SCR_NDC = 
					ISNULL(scrcalc.SCR,0)
					* POWER(gtc.AdjNetMWH/ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0)), egc.Pwr_CTG_SCRNDC)
					* egc.CTG_SCRNDC
				FROM TSort t
					LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN (SELECT DISTINCT Refnum, SCR = 1 FROM (
						SELECT DISTINCT Refnum FROM NonOHMaint WHERE Component = 'CTG-SCR'
							UNION SELECT DISTINCT Refnum FROM Misc WHERE SCR = 'Y'
							UNION SELECT DISTINCT Refnum FROM OHMaint WHERE Component = 'SCR'
							) b) scrcalc on scrcalc.Refnum = t.Refnum 
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_SCR_NDC = ' + CAST(@EGC_CTG_SCR_NDC AS VARCHAR)

				
				----------CTG_SteamSales----------
				SELECT @EGC_CTG_SteamSales = 
					POWER(ABS(CASE @year WHEN 2011 THEN gtc.StmSalesMWH ELSE gtc.StmSales END), egc.Pwr_CTG_SteamSales)
					--SW 6/14/13 made change above based on Rick's new model
					* egc.CTG_SteamSales

				FROM TSort t
					LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_SteamSales = ' + CAST(@EGC_CTG_SteamSales AS VARCHAR)


				----------CTG_HRSGCapN----------
				
				SELECT @EGC_CTG_HRSGCapN = 
					POWER((nf.NDC - ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0)))/ISNULL(nt.CTGCount,1), egc.Pwr_CTG_HRSGCapNDC) --ctgcount could be NULL? set it to 1 SW 6/14/13
					* egc.CTG_HRSGCapNDC
				FROM TSort t
					LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc), CTGCount = COUNT(*) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_HRSGCapN = ' + CAST(@EGC_CTG_HRSGCapN AS VARCHAR)

				
				----------CTG_HRSGCap----------

				SELECT @EGC_CTG_HRSGCap = 
					POWER((nf.NDC - ISNULL(nt.CTG_NDC, ISNULL(t.CTG_NDC,0))), egc.Pwr_CTG_HRSGCap)
					* egc.CTG_HRSGCap
				FROM TSort t
					LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum
					LEFT JOIN (SELECT refnum, CTG_NDC = SUM(ndc) FROM NERCTurbine WHERE turbinetype IN ('CTG','CC') GROUP BY Refnum) nt ON nt.Refnum = t.Refnum 
					LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum = @refnum

				IF @debug = 1
					PRINT 'EGC_CTG_HRSGCap = ' + CAST(@EGC_CTG_HRSGCap AS VARCHAR)


				----------CTG_GasMBTU----------

				SELECT @EGC_CTG_GasMBTU = 
					POWER(ISNULL(ftc.NatGasMBTU,0) + ISNULL(ftc.OffGasMBTU,0) + ISNULL(ftc.H2GasMBTU,0) , egc.Pwr_CTG_GasMBTU)
					* egc.CTG_GasMBTU
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum=@refnum
				
				IF @debug = 1
					PRINT 'EGC_CTG_GasMBTU = ' + CAST(@EGC_CTG_GasMBTU AS VARCHAR)


				----------CTG_LiquidMBTU----------
				
				SELECT @EGC_CTG_LiquidMBTU = 
					POWER(ISNULL(ftc.DieselKGal,0) + ISNULL(ftc.FuelOilKGal,0) + ISNULL(ftc.JetTurbKGal,0) , egc.Pwr_CTG_LiquidMBTU)
					* egc.CTG_LiquidMBTU
				FROM TSort t
					LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
					LEFT JOIN PowerGlobal.dbo.EGCCoefficients2011 egc ON egc.Year = @year
				WHERE t.Refnum=@refnum
				
				IF @debug = 1
					PRINT 'EGC_CTG_LiquidMBTU = ' + CAST(@EGC_CTG_LiquidMBTU AS VARCHAR)

				SET @EGC_Total = 
					@EGC_Age
					+ @EGC_CTG_NDC 
					+ @EGC_FiringTemp 
					+ @EGC_CTG_AvgCTGSizeUnder150 
					+ @EGC_CTG_AvgCTGSizeOver150 
					+ @EGC_CTG_SiteEffect 
					+ @EGC_CTG_SCR_NDC 
					+ @EGC_CTG_SteamSales 
					+ @EGC_CTG_HRSGCapN 
					+ @EGC_CTG_HRSGCap 
					+ @EGC_CTG_GasMBTU 
					+ @EGC_CTG_LiquidMBTU 

				IF @debug = 1
					PRINT 'EGC_Total = ' + CAST(@EGC_Total AS VARCHAR)


			----------end of CC----------

			END

		END -- End is not Cogen


		----------------------------------------------------------------------------
		----------------------------------------------------------------------------
		----------------------------------------------------------------------------



		-------------------------------------------------------------------------------------------------
		--now we get a little tricky
		--the following isn't really necessary for the new calculation, but it is legacy from the old calculation
		--basically we're filling in a bunch of fields in the EGCCalc table, which may or may not be needed later

		--next, we're just doing a straight rip of the old code to populate the appropriate variables, so we can insert them
		IF (@debug = 0)
		--note that debug can be set to 1 at the start if you just want to look at the totals, not populate the database with it
		BEGIN
		
			--first declare some of the fields we'll be calcing
			DECLARE @AshPcnt real, @CoalPcnt real, @OilPcnt real, @GasPcnt real, @Sulfur real
			--and of course our total
			DECLARE @calc_EGC real

			DECLARE @EGCTechnology varchar(15)
			DECLARE @EGCRegion varchar(20)
			DECLARE @CTG_NDC real
			DECLARE @NDC real 
			DECLARE @FuelType varchar(8)
			DECLARE @CoalMBTU real
			DECLARE @CoalSulfur real 
			DECLARE @CoalAsh real 
			DECLARE @OilSulfur real
			DECLARE @OilAsh real 
			DECLARE @NumUnitsAtSite real
			DECLARE @OilMBTU real
			DECLARE @GasMBTU real
			DECLARE @CoalNDC real 
			DECLARE @OilNDC real
			DECLARE @GasNDC real
			DECLARE @CTGs int 
			DECLARE @ScrubbersYN int
			DECLARE @PrecBagYN int 
			DECLARE @AdjNetMWH2Yr real 
			----@TotCashLessFuelEm real = null,
			DECLARE @Age real 
			DECLARE @ElecGenStarts real 

			-- run stored proc to try to fill in the EGCTechnology field in CASE this is the 1st time the unit has been calc'd
			EXEC spUPDATEEGCTechnologyForRefnum @Refnum = @refnum

			-- get the base required fields
			SELECT @EGCRegion=rtrim(EGCRegion), @EGCTechnology=rtrim(EGCTechnology) 
				FROM dbo.tsort 
				WHERE refnum=@refnum

			IF RIGHT(@Refnum,1) = 'P' AND @year < 2010 --oops, this will never be called now (since we're 2011 or higher). just leaving it in for posterity
			BEGIN
				SET @EGCTechnology='CARRYOVER'
			END

			SELECT @CTGs = ISNULL(ctgs,0), @CTG_NDC = ISNULL(ctg_ndc,0), @NDC = ISNULL(ndc,0), 
					@CoalNDC = ISNULL(Coal_NDC,0), @OilNDC = ISNULL(Oil_NDC,0), @GasNDC = ISNULL(Gas_NDC,0), 
					@PrecBagYN = ISNULL(PrecBagYN,0), @ScrubbersYN = ISNULL(ScrubbersYN,0)
				FROM dbo.tsort 
				WHERE Refnum=@refnum

			SELECT @NumUnitsAtSite = TotUnitNum
				FROM dbo.PlantGenData
				WHERE SiteID=(SELECT SiteID FROM TSort WHERE Refnum = @refnum)

			IF @NumUnitsAtSite IS NULL
				SELECT @NumUnitsAtSite = ISNULL(NumUnitsAtSite,0)
				FROM TSort
				WHERE Refnum = @refnum

			IF @NDC = 0
			BEGIN
			
				SELECT @NDC = ISNULL(nf.NDC,0)
				FROM NERCFactors nf 
				WHERE nf.Refnum = @refnum
			
			END


			SELECT @FuelType=fueltype 
				FROM dbo.FuelTotCalc  
				WHERE refnum=@refnum

			SELECT @Age=max(age) 
				FROM dbo.nercturbine 
				WHERE refnum=@refnum

			SELECT @ElecGenStarts=ISNULL(SUM(TotalOpps),0) 
				FROM dbo.StartsAnalysis sa 
				INNER JOIN NERCTurbine nt ON nt.Refnum = sa.Refnum AND nt.TurbineID = sa.TurbineID 
				WHERE nt.TurbineType IN ('CTG', 'STG', 'CST') AND sa.Refnum = @Refnum
			SELECT @AdjNetMWH2Yr = ISNULL(AdjNetMWH2Yr,0)/1000000 
				FROM dbo.GenerationTotCalc 
				WHERE Refnum = @Refnum

			SELECT @CoalSulfur = ISNULL(SulfurPcnt,0), @CoalAsh = ISNULL(AshPcnt,0)
				FROM CoalTotCalc
				WHERE Refnum = @Refnum

			SELECT @OilSulfur = ISNULL(Sulfur,0), @OilAsh = 0.1 
				FROM FuelProperties 
				WHERE Refnum = @Refnum



			--more calcs to fill in old data
			IF (@CTG_NDC IS NULL)
				SET @CTG_NDC = 0
			ELSE
				BEGIN
					IF (@CTGs > 0)
						SET @CTG_NDC = @CTG_NDC --/ @CTGs
				END

			IF @OilSulfur IS NULL
				SELECT @OilSulfur = 2.3, @OilAsh = 0.1

			SELECT @Sulfur = (ISNULL(@CoalSulfur,0)*@CoalNDC + ISNULL(@OilSulfur,0)*@OilNDC)/@NDC
			SELECT @AshPcnt = (ISNULL(@CoalAsh,0)*@CoalNDC + ISNULL(@OilAsh,0)*@OilNDC)/@NDC


			SELECT @OilMBTU = SUM(MBTU) 
				FROM Fuel 
				WHERE Refnum = @Refnum AND FuelType IN ('Jet', 'FOil', 'Diesel') AND TurbineID IN ('STG','Site')
			IF @OilMBTU IS NULL 
				SELECT @OilMBTU = 0

			SELECT @GasMBTU = SUM(MBTU) 
				FROM Fuel 
				WHERE Refnum = @Refnum AND FuelType IN ('NatGas', 'OffGas', 'H2Gas') AND TurbineID IN ('STG','Site')
			IF @GasMBTU IS NULL 
				SELECT @GasMBTU = 0

			SELECT @CoalMBTU = SUM(MBTU) 
				FROM Coal 
				WHERE Refnum = @Refnum

		
			IF (@OilMBTU + @GasMBTU) > 0
				SELECT @OilPcnt = @OilMBTU/(@OilMBTU + @GasMBTU)
			ELSE
				SELECT @OilPcnt = 0

			IF @OilPcnt < 0.1
				SELECT @OilPcnt = 0

			IF @OilPcnt > 0.9
				SELECT @OilPcnt = 1

			IF (@OilMBTU + @GasMBTU) > 0
				SELECT @GasPcnt = @GasMBTU/(@OilMBTU + @GasMBTU)
			ELSE
				SELECT @GasPcnt = 0

			IF @GasPcnt < 0.1
				SELECT @GasPcnt = 0

			IF @GasPcnt > 0.9
				SELECT @GasPcnt = 1

			IF @CoalMBTU > 0
				SELECT @CoalPcnt = @CoalMBTU/(@CoalMBTU + @OilMBTU + @GasMBTU)
			ELSE
				SELECT @CoalPcnt = 0

			IF @CoalPcnt < 0.1
				SELECT @CoalPcnt = 0

			IF @CoalPcnt > 0.9
				SELECT @CoalPcnt = 1

			--get the EGC value we came up with
			SELECT @calc_EGC = @EGC_Total --FROM #tmp WHERE refnum = @refnum 
		
			--Build the "REAL" EGCCalc record. The one that will be used.
			--if DEBUG THEN ONLY display the value do NOT UPDATE any database records
			--note the 2011 in the Values is the EGC Version, not the year
			DELETE FROM EGCCalc WHERE Refnum = @Refnum
			INSERT INTO EGCCalc (Refnum, Method, Technology, EGC, CTGs, CTG_NDC, NDC, AdjNetMWH, Sulfur, AshPcnt, 
				FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt, GasPcnt, CoalPcnt, Age, NumUnitsAtSite, 
				ScrubbersYN,  PrecBagYN)
			VALUES (@Refnum, '2011', @EGCTechnology, @calc_EGC, @CTGs, @CTG_NDC, @NDC, @AdjNetMWH2Yr, @Sulfur, @AshPcnt,
				@FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt, @GasPcnt, @CoalPcnt, @Age, @NumUnitsAtSite, 
				@ScrubbersYN, @PrecBagYN)

	--		-- Go UPDATE all of the EGC fields spread thoughout the database.
			UPDATE GenerationTotCalc SET EGC = @calc_EGC WHERE Refnum = @Refnum
			UPDATE Gensum SET EGC = @calc_EGC WHERE Refnum = @Refnum

			UPDATE NonOHMaint SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE OHEquipCalc SET TotAnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN TotAnnOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE MaintEquipCalc SET AnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintEquipCalc SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintEquipCalc SET AnnMaintCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE MaintTotCalc SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnMaintCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnOHProjCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHProjCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnOHExclProjEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHExclProj/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnMaintExclProjEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintExclProj/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnLTSACostEGC = CASE WHEN @calc_EGC > 0 THEN AnnLTSACost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE Pers SET TotEffPersEGC = CASE WHEN @calc_EGC > 0 THEN TotEffPers*1000/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE PersSTCalc SET TotEffPersEGC = CASE WHEN @calc_EGC > 0 THEN TotEffPers*1000/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE GenSum SET TotEffPersEGC = (SELECT TotEffPersEGC FROM PersSTCalc WHERE PersSTCalc.Refnum = Gensum.Refnum AND PersSTCalc.SectionID = 'TP') WHERE Refnum = @Refnum

			EXEC spAddOpexCalc @Refnum = @Refnum, @DataType = 'EGC', @DivFactor = @calc_EGC

		END -- this is the end of if @debug = 0
		
		ELSE
		BEGIN

			--and if we're just running in DEBUG, all we do is select the values we calculated
			IF @IsCogen = 1
			BEGIN
				SELECT Refnum = @refnum, EGC_Total = @EGC_Total, EGC_Cogen_HRSG = @EGC_Cogen_HRSG, EGC_Cogen_Coal_NDC = @EGC_Cogen_Coal_NDC,
					EGC_Cogen_CTG_NDC = @EGC_Cogen_CTG_NDC, EGC_Cogen_AdjNetMWh2Yr = @EGC_Cogen_AdjNetMWh2Yr,
					EGC_Cogen_StmSales = @EGC_Cogen_StmSales, EGC_Cogen_StmSalesMWh = @EGC_Cogen_StmSalesMWh,
					EGC_Cogen_TotStarts = @EGC_Cogen_TotStarts, EGC_Cogen_CTGAvgTotStarts = @EGC_Cogen_CTGAvgTotStarts,
					EGC_Cogen_HeatRate = @EGC_Cogen_HeatRate, EGC_Cogen_FiringTemp = @EGC_Cogen_FiringTemp,
					EGC_Cogen_Age = @EGC_Cogen_Age, EGC_Cogen_NatGasMBTU = @EGC_Cogen_NatGasMBTU,
					EGC_Cogen_OffGasMBTU = @EGC_Cogen_OffGasMBTU, EGC_Cogen_DieselFuelOilMBTU = @EGC_Cogen_DieselFuelOilMBTU,
					EGC_Cogen_JetTurbMBTU = @EGC_Cogen_JetTurbMBTU, EGC_Cogen_CoalMBTU = @EGC_Cogen_CoalMBTU,
					EGC_Cogen_Gas2YrMWh = @EGC_Cogen_Gas2YrMWh
				
			END
			ELSE

				IF @refnum NOT LIKE '%CC%'

					SELECT Refnum = @refnum, EGC_Total = @EGC_Total, EGC_SCR = @EGC_SCR, EGC_WGS = @EGC_WGS, WGC_DGS = @EGC_DGS, EGC_PrecBag = @EGC_PrecBag, 
						EGC_OtherSiteCapCoalOil = @EGC_OtherSiteCapCoalOil, EGC_OtherSiteCapGas = @EGC_OtherSiteCapGas,
						EGC_OtherSiteCap1 = @EGC_OtherSiteCap1, EGC_OtherSiteCap2 = @EGC_OtherSiteCap2, -- SW 6/12/13 added these two terms
						EGC_SteamSalesCoalOil = @EGC_SteamSalesCoalOil, EGC_SteamSalesGas = @EGC_SteamSalesGas, EGC_Coal_NDC = @EGC_Coal_NDC, 
						EGC_Gas_NDC = @EGC_Gas_NDC, 
						EGC_Geothermal_NDC = @EGC_Geothermal_NDC, --SW 6/12/13 added to model
						EGC_Coal1 = @EGC_Coal1, EGC_Coal2 = @EGC_Coal2, EGC_Coal3 = @EGC_Coal3, EGC_GasTotalNonGasUnits = @EGC_GasTotalNonGasUnits,
						EGC_GasTotalGasUnits = @EGC_GasTotalGasUnits, EGC_LiquidFuelsGasUnits = @EGC_LiquidFuelsGasUnits, 
						EGC_LiquidFuelsNoSulfurCoal = @EGC_LiquidFuelsNoSulfurCoal
						
				ELSE
					SELECT Refnum = @refnum, EGC_Total = @EGC_Total, EGC_Age = @EGC_Age, EGC_CTG_NDC = @EGC_CTG_NDC, EGC_FiringTemp = @EGC_FiringTemp, 
						EGC_CTG_AvgCTGSizeUnder150 = @EGC_CTG_AvgCTGSizeUnder150, EGC_CTG_AvgCTGSizeOver150 = @EGC_CTG_AvgCTGSizeOver150,
						EGC_CTG_SiteEffect = @EGC_CTG_SiteEffect, EGC_CTG_SCR_NDC = @EGC_CTG_SCR_NDC, EGC_CTG_SteamSales = @EGC_CTG_SteamSales, 
						EGC_CTG_HRSGCapNDC = @EGC_CTG_HRSGCapN, EGC_CTG_HRSGCap = @EGC_CTG_HRSGCap, EGC_CTG_GasMBTU = @EGC_CTG_GasMBTU, 
						EGC_CTG_LiquidMBTU = @EGC_CTG_LiquidMBTU
							
		END
		
	END --end of if < 2013 - meaning 2011/12 were dealt with, now we need to work on 2013+
	ELSE
	BEGIN
		EXEC spEGCCalcs2013 @refnum, @year, @DEBUG
	END
	
	
END