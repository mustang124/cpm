﻿CREATE PROC procDelRelationship @Object1 sysname, @Object2 sysname AS
DECLARE @Obj1ID DD_ID, @Obj2ID DD_ID
EXEC procGetObjectID @Object1, @Obj1ID OUTPUT
EXEC procGetObjectID @Object2, @Obj2ID OUTPUT
DELETE FROM DD_Relationships
WHERE (ObjectA = @Obj1ID AND ObjectB = @Obj2ID)
 OR (ObjectA = @Obj2ID AND ObjectB = @Obj1ID)
