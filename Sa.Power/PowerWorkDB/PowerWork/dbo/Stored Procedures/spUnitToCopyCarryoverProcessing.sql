﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- This set of SQL statements will take the units listed
-- in UnitsToCopy and build the carryover data for them
-- in the new year.
-- Other operations need to take place after this to complete
-- the process, including inflation and fuel adjustments.

-- NOTE: Be sure that StudyYear in UnitsToCopy
-- is set to the new study year before 
-- running this SQL.
-- If not, the Period field in several IMPORTANT 
-- tables will not get updated.
-- =============================================
CREATE PROCEDURE [dbo].[spUnitToCopyCarryoverProcessing] 

AS
--BEGIN
	SET NOCOUNT ON;

	SELECT * 
	INTO #TSort
	FROM TSort
	WHERE Refnum IN (SELECT OldRefnum FROM UnitsToCopy)

	UPDATE #TSort
	SET Refnum = s.NewRefnum , SiteID = s.NewSiteID, StudyYear = s.StudyYear, CalcCommUnavail = 'N'
	FROM #TSort INNER JOIN UnitsToCopy s ON s.OldRefnum = #TSort.Refnum

	DELETE FROM TSort
	WHERE Refnum IN (SELECT NewRefnum FROM UnitsToCopy)

	INSERT INTO TSort 
	SELECT *
	FROM #TSort

	DROP TABLE #TSort

	DECLARE cTables CURSOR
	READ_ONLY
	FOR 	select name from sysobjects
		where type = 'u' and (id in (select id from syscolumns where name = 'SiteID')
		or id in (select id from syscolumns where name = 'Refnum') or name = 'ROODs') and name NOT IN ('TSort', 'Rank', 'RevenuePeaks', 'RefList', 'MessageLog', 'ValStat', 'ValidationNotes', 'AvailableHours', 'ProcessSteps'
		, 'ValTime', 'DCScores','Checklist','Versions','Notes') -- SW 5/30/13 added these 5 tablenames, they were part of Console but give errors in Carryovers
		or name = 'EmissionsTons'

	DECLARE @name sysname
	DECLARE @qryCopyToTemp varchar(200), @qryDel varchar(200)
	DECLARE @qryUpdateRefnum varchar(200), @qryUpdateSiteID varchar(200), @qryUpdateStudyYear varchar(200)
	DECLARE @qryUpdatePeriod varchar(500)
	OPEN cTables

	FETCH NEXT FROM cTables INTO @name
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			PRINT @name
			IF EXISTS (SELECT * FROM syscolumns c INNER JOIN sysobjects o ON o.id = c.id WHERE o.Name = @name AND c.Name = 'Refnum')
			BEGIN
				SELECT @qryUpdateRefnum = 'UPDATE #Copy SET Refnum = (SELECT NewRefnum FROM UnitsToCopy WHERE OldRefnum = #Copy.Refnum);'
				SELECT @qryCopyToTemp = 'SELECT * INTO #Copy FROM ' + @name + ' WHERE Refnum IN (SELECT OldRefnum FROM UnitsToCopy);'
				SELECT @qryDel = 'DELETE FROM ' + @name + ' WHERE Refnum IN (SELECT NewRefnum FROM UnitsToCopy);'
			END
			ELSE
				SELECT @qryUpdateRefnum = ''
			IF EXISTS (SELECT * FROM syscolumns c INNER JOIN sysobjects o ON o.id = c.id WHERE o.Name = @name AND c.Name = 'SiteID')
			BEGIN
				IF @qryUpdateRefnum = ''
				BEGIN
					SELECT @qryUpdateSiteID = 'UPDATE #Copy SET SiteID = (SELECT DISTINCT NewSiteID FROM UnitsToCopy WHERE OldSiteID = SiteID);'
					SELECT @qryCopyToTemp = 'SELECT * INTO #Copy FROM ' + @name + ' WHERE SiteID IN (SELECT OldSiteID FROM UnitsToCopy);'
					SELECT @qryDel = 'DELETE FROM ' + @name + ' WHERE SiteID IN (SELECT NewSiteID FROM UnitsToCopy);'
				END
				ELSE
					SELECT @qryUpdateSiteID = 'UPDATE #Copy SET SiteID = (SELECT NewSiteID FROM UnitsToCopy WHERE NewRefnum = Refnum);'
			END
			ELSE
				SELECT @qryUpdateSiteID = ''
			IF EXISTS (SELECT * FROM syscolumns c INNER JOIN sysobjects o ON o.id = c.id WHERE o.Name = @name AND c.Name = 'StudyYear')
				SELECT @qryUpdateStudyYear = 'UPDATE #Copy SET StudyYear = (SELECT DISTINCT StudyYear FROM UnitsToCopy WHERE NewSiteID = SiteID);'
			ELSE
				SELECT @qryUpdateStudyYear = ''
			IF EXISTS (SELECT * FROM syscolumns c INNER JOIN sysobjects o ON o.id = c.id WHERE o.Name = @name AND c.Name = 'Period')
				SELECT @qryUpdatePeriod = 'UPDATE #Copy SET Period = DATEADD(yy, u.StudyYear - t.StudyYear, Period) FROM #Copy INNER JOIN UnitsToCopy u ON u.NewRefnum = #Copy.Refnum INNER JOIN TSort t ON t.Refnum = u.OldRefnum;'
			ELSE
				SELECT @qryUpdatePeriod = ''
			EXEC (@qryCopyToTemp
				+ @qryUpdateRefnum
				+ @qryUpdateSiteID
				+ @qryUpdateStudyYear
				+ @qryUpdatePeriod
				+ @qryDel
				+ ' INSERT INTO ' + @name + ' SELECT * FROM #Copy;'
				+ ' DROP TABLE #Copy;')

	--		EXEC (@qryDel)
		END
		FETCH NEXT FROM cTables INTO @name
	END

	CLOSE cTables
	DEALLOCATE cTables

	UPDATE StudySites
	SET SiteDirectory = rtrim('\\DALLAS2\DATA\Data\Study\Power\' + CONVERT(varchar(4), StudyYear) + 'P\Plant\' + SiteID)
	WHERE SiteID IN (SELECT NewSiteID FROM UnitsToCopy)

	UPDATE OHMaint
	SET OHDate = DATEADD(yy, u.StudyYear - t.StudyYear, OHDate),
		PrevOHDate = DATEADD(yy, u.StudyYear - t.StudyYear, PrevOHDate)
	FROM OHMaint 
		INNER JOIN UnitsToCopy u ON u.NewRefnum = OHMaint.Refnum 
		INNER JOIN TSort t ON t.Refnum = u.OldRefnum

	UPDATE CTGMaint
	SET InspDate = DATEADD(yy, u.StudyYear - t.StudyYear, InspDate),
		PrevInspDate = DATEADD(yy, u.StudyYear - t.StudyYear, PrevInspDate)
	FROM CTGMaint 
		INNER JOIN UnitsToCopy u ON u.NewRefnum = CTGMaint.Refnum 
		INNER JOIN TSort t ON t.Refnum = u.OldRefnum

	UPDATE PowerGeneration
	Set Period = DATEADD(yy, 0, Period)
	FROM PowerGeneration 
		INNER JOIN UnitsToCopy u on u.NewRefnum = PowerGeneration.Refnum 


