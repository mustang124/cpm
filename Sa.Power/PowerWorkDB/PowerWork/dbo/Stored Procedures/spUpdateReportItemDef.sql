﻿
CREATE PROCEDURE spUpdateReportItemDef
	@ItemID DD_ID = -2 OUTPUT, @ItemName varchar(50),
	@FactorA DD_ID = NULL, @ObjectA sysname = NULL, @ColumnA sysname = NULL, 
	@FactorB DD_ID = NULL, @ObjectB sysname = NULL, @ColumnB sysname = NULL, 
	@Method tinyint = NULL, @MethodName varchar(30) = NULL,
	@Multiplier real = 1, @Label varchar(50) = '',
	@Format tinyint = 0, 
	@SectionID tinyint = 0, @SectionName varchar(30) = NULL,
	@Scenario Scenario = NULL, @SQL varchar(255) = NULL,
	@Currency varchar(5) = NULL
AS
DECLARE	@ProcResult integer
IF EXISTS (SELECT * FROM ReportItemDef WHERE ItemName = @ItemName AND ReportItemID <> @ItemID)
	RETURN -101
IF  (@FactorA IS NULL) AND (@ObjectA IS NOT NULL AND @ColumnA IS NOT NULL)
BEGIN
	EXEC @ProcResult = spGetColumnID @ObjectName = @ObjectA, @ColumnName = @ColumnA, @ColumnID = @FactorA OUTPUT
	IF @ProcResult <> 0
		SELECT @FactorA = NULL
END
IF  (@FactorB IS NULL) AND (@ObjectB IS NOT NULL AND @ColumnB IS NOT NULL)
BEGIN
	EXEC @ProcResult = spGetColumnID @ObjectName = @ObjectB, @ColumnName = @ColumnB, @ColumnID = @FactorB OUTPUT
	IF @ProcResult <> 0
		SELECT @FactorB = NULL
END
IF @SectionName IS NOT NULL
	SELECT @SectionID = SectionID FROM DD_Sections 
	WHERE SectionName = @SectionName
IF @SectionID IS NULL
	SELECT @SectionID = 0
IF @MethodName IS NOT NULL
	SELECT @Method = MethodNo FROM Method_LU
	WHERE Name = @MethodName
IF @ItemID = -2
BEGIN
	INSERT INTO ReportItemDef (ItemName, FactorA,FactorB, Method, 
		Multiplier, Label, Format, SectionID, 
		ItemDefScenario, ItemDefSQL, ItemDefCurrency)
	VALUES (@ItemName, @FactorA, @FactorB, @Method, @Multiplier, @Label,
		@Format, @SectionID, @Scenario, @SQL, @Currency)
	SELECT @ItemID = ReportItemID FROM ReportItemDef
	WHERE ItemName = @ItemName
END
ELSE
BEGIN
	UPDATE ReportItemDef
	SET ItemName = @ItemName, FactorA = @FactorA, FactorB = @FactorB, 
		Method = @Method, Multiplier = @Multiplier, 
		Label = @Label, Format = @Format, SectionID = @SectionID,
		ItemDefScenario = @Scenario, ItemDefSQL = @SQL,
		ItemDefCurrency = @Currency
	WHERE ReportItemID = @ItemID
END

