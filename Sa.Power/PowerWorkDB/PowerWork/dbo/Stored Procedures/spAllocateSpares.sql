﻿CREATE PROC spAllocateSpares(@SiteID SiteID)
AS
DECLARE @SparesInven real, @SiteSupply real
SELECT @SparesInven = ISNULL(SparesInvenCentralLocal, 0) + ISNULL(SparesInvenOnSiteLocal, 0) + ISNULL(CurrSparesInvenLocal, 0)
FROM PlantGenData WHERE SiteID = @SiteID

IF @SparesInven > 0
BEGIN
	SELECT @SiteSupply = SUM(Supply) FROM OpexLocal
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	IF @SiteSupply > 0
		UPDATE OpexLocal
		SET AllocSparesInven = @SparesInven * Supply/@SiteSupply
		WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
	ELSE BEGIN
	      	/*** Code for those units that did not report any Materials and Supply Carrying Costs.
        	 Allocate based on number of units. ***/
		SELECT @SiteSupply = COUNT(*) FROM TSort
 		WHERE SiteID = @SiteID
	        IF @SiteSupply > 0
			UPDATE OpexLocal
			SET AllocSparesInven = @SparesInven / @SiteSupply
			WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
	END
END


