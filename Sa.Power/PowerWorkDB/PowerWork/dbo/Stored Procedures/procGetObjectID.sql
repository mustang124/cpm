﻿CREATE PROC procGetObjectID @ObjName sysname, @ObjID DD_ID OUTPUT 
AS
SELECT @ObjID = ObjectID FROM DD_Objects WHERE ObjectName = @ObjName
