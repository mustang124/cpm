﻿CREATE PROC [dbo].[spComponentEUF] (@Refnum varchar(10))
AS
DECLARE @E_PH float
	-- Delete any records in ComponentEUF for this Refnum
	Delete from ComponentEUF Where Refnum = @Refnum

	DECLARE @turbHrs TABLE (
		Refnum varchar(12) NOT NULL,
		TurbineID char(6) NOT NULL,
		UtilityUnitCode char(6) NOT NULL,
		UnitType varchar(50) NOT NULL,
		UnitShortName char(10) NOT NULL,
		EvntYear smallint NOT NULL,
		E_PH float NULL
	)
	INSERT @turbHrs (Refnum, TurbineID, UtilityUnitCode, UnitType, UnitShortName, EvntYear, E_PH)
	SELECT nt.Refnum, nt.TurbineID, nt.UtilityUnitCode, s.UnitType, s.UnitShortName, t.EvntYear, eh.E_PH
	FROM dbo.TSort t INNER JOIN dbo.NERCTurbine nt on t.Refnum = nt.Refnum
	INNER JOIN GADSOS.GADSNG.Setup s on nt.UtilityUnitCode = s.UtilityUnitCode
	INNER JOIN GADSOS.GADSNG.EventHours eh ON eh.UnitShortName = s.UnitShortName AND (DatePart(yy, eh.TL_DateTime) =  t.EvntYear) AND (eh.Granularity = 'Yearly')
	WHERE t.Refnum = @Refnum AND eh.E_PH > 0
	
	SELECT @E_PH = SUM(E_PH) FROM @turbHrs
	
	DECLARE @CompEUFHrs TABLE (
		Refnum varchar(12) NOT NULL,
		--TurbineID char(6) NOT NULL,
		SAIMajorEquip varchar(40) NULL,
		SAIMinorEquip varchar(40) NULL,
		EquipGroup char(8) NULL,
		EquivMWhrs float NULL,
		E_PH float NULL
	)
	INSERT @CompEUFHrs (Refnum, SAIMajorEquip, SAIMinorEquip, EquipGroup, EquivMWhrs)
	SELECT t.Refnum, /*t.TurbineID,*/ cc.SAIMajorEquip, cc.SAIMinorEquip, lu.EquipGroup, SUM(e.EquivMWhrs)
	FROM @turbHrs t 
	INNER JOIN GADSOS.GADSNG.EventDetails e ON e.UnitShortName = t.UnitShortName AND (DatePart(yy, e.TL_DateTime) =  t.EvntYear) AND (e.Granularity = 'Yearly')
	INNER JOIN dbo.CauseCodes cc ON cc.Cause_Code = e.CauseCode
	INNER JOIN ComponentEUF_LU lu ON lu.UnitType = t.UnitType AND lu.SAIMajorEquip = cc.SAIMajorEquip AND lu.SAIMinorEquip = cc.SAIMinorEquip
	GROUP BY t.Refnum, cc.SAIMajorEquip, cc.SAIMinorEquip, lu.EquipGroup
	
	IF NOT EXISTS (SELECT * FROM @CompEUFHrs)
		RETURN 1
		
	-- Equipment listed on T3 but did not find events for it
	INSERT @CompEUFHrs (Refnum, EquipGroup, EquivMWhrs)
	SELECT DISTINCT mec.Refnum, mec.EquipGroup, 0
	FROM MaintEquipCalc mec 
	WHERE mec.Refnum = @Refnum AND mec.AnnMaintCostKUS > 0 AND NOT EXISTS (SELECT * FROM @CompEUFHrs c WHERE c.Refnum = mec.Refnum AND c.EquipGroup = mec.EquipGroup)
	-- Assign E_PH 
	UPDATE compEUF
	SET E_PH = @E_PH
	FROM @CompEUFHrs compEUF
-- Here for debugging
--SELECT * FROM @turbhrs
--SELECT * FROM @CompEUFHrs
--
	-- ALTER the new records in ComponentEUF for this Refnum
	INSERT INTO ComponentEUF (Refnum, EquipGroup, EUF, E_PH)
	SELECT Refnum, EquipGroup, SUM(EquivMWhrs)/AVG(E_PH)*100, AVG(E_PH) AS E_PH
	FROM @CompEUFHrs WHERE E_PH > 0
	GROUP BY Refnum, EquipGroup
