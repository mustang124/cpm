﻿CREATE     PROCEDURE [dbo].[spPersCalcs] (@Refnum Refnum)
AS
SET NOCOUNT ON
DECLARE @OCCAbsPcnt real, @MPSAbsPcnt real
DECLARE @OCCSTH real, @MPSSTH real, @SiteID SiteID, @StudyYear smallint
DECLARE @MWH100K real, @KSH real, @MW100 real, @GJ100K real
SELECT @MWH100K = AdjNetMWH/100000, @GJ100K = TotalOutputGJ/100000 FROM GenerationTotCalc 
WHERE Refnum = @Refnum
SELECT @KSH = ServiceHrs/1000, @MW100 = NMC/100
FROM NERCFactors WHERE Refnum = @Refnum
SELECT @SiteID = SiteID, @StudyYear = StudyYear
FROM TSort WHERE Refnum = @Refnum
DECLARE @OHOCCSTH real, @OHMPSSTH real
SELECT 	@OHOCCSTH = SUM(o.SiteOCCSTH), 
	@OHMPSSTH = SUM(o.SiteMPSSTH)
FROM OHMaint o
WHERE DATEPART(YY, o.OHDate) = @StudyYear
AND Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
SELECT 	@OCCSTH = SUM(SiteSTH*CASE WHEN IncludeInAbsence = 'O' THEN 1 ELSE 0 END), 
@MPSSTH = SUM(SiteSTH*CASE WHEN IncludeInAbsence = 'M' THEN 1 ELSE 0 END)
FROM PersSiteCalc p INNER JOIN Pers_LU l ON p.PersCat = l.PersCat
WHERE p.SiteID = @SiteID
SELECT 	@OCCSTH = ISNULL(@OCCSTH, 0) + ISNULL(@OHOCCSTH, 0),
	@MPSSTH = ISNULL(@MPSSTH, 0) + ISNULL(@OHMPSSTH, 0)
UPDATE SiteAbsences
SET 	OCCAbsPcnt = CASE WHEN @OCCSTH > 0 THEN ISNULL(OCCAbs, 0)*100/@OCCSTH ELSE NULL END,
	MPSAbsPcnt = CASE WHEN @MPSSTH > 0 THEN ISNULL(MPSAbs, 0)*100/@MPSSTH ELSE NULL END,
	TotAbsPcnt = CASE WHEN (@OCCSTH + @MPSSTH)> 0 THEN (ISNULL(OCCAbs, 0)+ISNULL(MPSAbs, 0))*100/(@OCCSTH + @MPSSTH) ELSE NULL END,
	TotAbs = ISNULL(OCCAbs, 0) + ISNULL(MPSAbs, 0)
WHERE SiteID = @SiteID
SELECT @OCCAbsPcnt = OCCAbsPcnt, @MPSAbsPcnt = MPSAbsPcnt
FROM AbsenceTotCalc WHERE Refnum = @Refnum
/* Effective Personnel Calcs */
UPDATE Pers
SET SiteSTEffPers = ISNULL(SiteSTH, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
SiteOVTEffPers = ISNULL(SiteOVTHrs, 0)/1800,
CentralSTEffPers = ISNULL(CentralSTH, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
CentralOVTEffPers = ISNULL(CentralOVTHrs, 0)/1800,
AGOnSiteEffPers = ISNULL(AGOnSite, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
AGOthLocEffPers = ISNULL(AGOthLoc, 0)/1800*(CASE WHEN Pers.PersCat IN ('MPS-LSCAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'OCC-OHAdj') THEN 1 WHEN l.CalcCat = 'OCC' THEN 1-@OCCAbsPcnt/100 WHEN l.CalcCat = 'MPS' THEN 1-@MPSAbsPcnt/100 ELSE 1 END),
ContractEffPers = ISNULL(Contract, 0)/1800,
SiteOVTPcnt = CASE WHEN SiteSTH > 0 THEN 100*SiteOVTHrs/SiteSTH ELSE 0 END,
SectionID = l.SectionID
FROM Pers INNER JOIN Pers_LU l ON l.PersCat = Pers.PersCat
WHERE Refnum = @Refnum AND Pers.PersCat NOT IN ('OCC-Maint%', 'MPS-Maint%')
UPDATE Pers
SET 	SiteEffPers = SiteSTEffPers + SiteOVTEffPers, 
	CentralEffPers = CentralSTEffPers + CentralOVTEffPers, 
	AGEffPers = AGOnSiteEffPers + AGOthLocEffPers,
	TotEffPers = SiteSTEffPers + SiteOVTEffPers 
			+ CentralSTEffPers + CentralOVTEffPers 
			+ AGOnSiteEffPers + AGOthLocEffPers + ContractEffPers
WHERE Refnum = @Refnum
UPDATE Pers
SET	TotEffPersMWH = CASE WHEN @MWH100K > 0 THEN TotEffPers/@MWH100K ELSE NULL END,
	TotEffPersMW = CASE WHEN @MW100 > 0 THEN TotEffPers/@MW100 ELSE NULL END,
	TotEffPersGJ = CASE WHEN @GJ100K > 0 THEN TotEffPers/@GJ100K ELSE NULL END
WHERE Refnum = @Refnum
/* Sub-totals and Totals */
DELETE FROM PersSTCalc WHERE Refnum = @Refnum
INSERT INTO PersSTCalc (Refnum, SectionID, SiteNumPers, SiteSTH, SiteOVTHrs,
	CentralNumPers, CentralSTH, CentralOVTHrs, 
	AGOnSite, AGOthLoc, Contract, 
	SiteSTEffPers, SiteOVTEffPers, SiteEffPers, 
	CentralSTEffPers, CentralOVTEffPers, CentralEffPers, 
	ContractEffPers, AGOnSiteEffPers, AGOthLocEffPers, AGEffPers, 
	TotEffPers, SiteOVTPcnt, TotEffPersMWH, TotEffPersMW, TotEffPersGJ)
SELECT Refnum, SectionID, SUM(SiteNumPers), SUM(SiteSTH), SUM(SiteOVTHrs),
	SUM(CentralNumPers), SUM(CentralSTH), SUM(CentralOVTHrs),
	SUM(AGOnSite), SUM(AGOthLoc), SUM(Contract),
	SUM(SiteSTEffPers), SUM(SiteOVTEffPers), SUM(SiteEffPers),
	SUM(CentralSTEffPers), SUM(CentralOVTEffPers), SUM(CentralEffPers),
	SUM(ContractEffPers), SUM(AGOnSiteEffPers), SUM(AGOthLocEffPers), SUM(AGEffPers),
	SUM(TotEffPers), CASE WHEN SUM(SiteSTH) > 0 THEN 100*SUM(SiteOVTHrs)/SUM(SiteSTH) ELSE NULL END, SUM(TotEffPersMWH), SUM(TotEffPersMW), SUM(TotEffPersGJ)
FROM Pers
WHERE Refnum = @Refnum AND SectionID IS NOT NULL AND PersCat NOT IN ('OCC-Maint%', 'MPS-Maint%')
GROUP BY Refnum, SectionID
INSERT INTO PersSTCalc (Refnum, SectionID, SiteNumPers, SiteSTH, SiteOVTHrs,
	CentralNumPers, CentralSTH, CentralOVTHrs, AGOnSite, AGOthLoc, Contract,
	SiteSTEffPers, SiteOVTEffPers, SiteEffPers, 
	CentralSTEffPers, CentralOVTEffPers, CentralEffPers, ContractEffPers,
	AGOnSiteEffPers, AGOthLocEffPers, AGEffPers, TotEffPers, SiteOVTPcnt,
	TotEffPersMWH, TotEffPersMW, TotEffPersGJ)
SELECT Refnum, 'TOCC', SUM(SiteNumPers), SUM(SiteSTH), SUM(SiteOVTHrs),
	SUM(CentralNumPers), SUM(CentralSTH), SUM(CentralOVTHrs),
	SUM(AGOnSite), SUM(AGOthLoc), SUM(Contract),
	SUM(SiteSTEffPers), SUM(SiteOVTEffPers), SUM(SiteEffPers),
	SUM(CentralSTEffPers), SUM(CentralOVTEffPers), SUM(CentralEffPers),
	SUM(ContractEffPers), SUM(AGOnSiteEffPers), SUM(AGOthLocEffPers), SUM(AGEffPers),
	SUM(TotEffPers), CASE WHEN SUM(SiteSTH)>0 THEN 100*SUM(SiteOVTHrs)/SUM(SiteSTH) ELSE NULL END,
    	SUM(TotEffPersMWH), SUM(TotEffPersMW), SUM(TotEffPersGJ)
FROM Pers 
WHERE Refnum = @Refnum AND SectionID IN ('OCCO', 'OCCM', 'OCCT', 'OCCA')
GROUP BY Refnum 
INSERT INTO PersSTCalc (Refnum, SectionID, SiteNumPers, SiteSTH, SiteOVTHrs,
	CentralNumPers, CentralSTH, CentralOVTHrs, AGOnSite, AGOthLoc, Contract,
	SiteSTEffPers, SiteOVTEffPers, SiteEffPers, 
	CentralSTEffPers, CentralOVTEffPers, CentralEffPers, ContractEffPers,
	AGOnSiteEffPers, AGOthLocEffPers, AGEffPers, TotEffPers, SiteOVTPcnt,
	TotEffPersMWH, TotEffPersMW, TotEffPersGJ)
SELECT Refnum, 'TMPS', SUM(SiteNumPers), SUM(SiteSTH), SUM(SiteOVTHrs),
	SUM(CentralNumPers), SUM(CentralSTH), SUM(CentralOVTHrs),
	SUM(AGOnSite), SUM(AGOthLoc), SUM(Contract),
	SUM(SiteSTEffPers), SUM(SiteOVTEffPers), SUM(SiteEffPers),
	SUM(CentralSTEffPers), SUM(CentralOVTEffPers), SUM(CentralEffPers),
	SUM(ContractEffPers), SUM(AGOnSiteEffPers), SUM(AGOthLocEffPers), SUM(AGEffPers),
	SUM(TotEffPers), CASE WHEN SUM(SiteSTH)>0 THEN 100*SUM(SiteOVTHrs)/SUM(SiteSTH) ELSE NULL END,
    	SUM(TotEffPersMWH), SUM(TotEffPersMW), SUM(TotEffPersGJ)
FROM Pers 
WHERE Refnum = @Refnum AND SectionID IN ('MPSO', 'MPSM', 'MPST', 'MPSA')
GROUP BY Refnum 
INSERT INTO PersSTCalc (Refnum, SectionID, SiteNumPers, SiteSTH, SiteOVTHrs,
	CentralNumPers, CentralSTH, CentralOVTHrs, AGOnSite, AGOthLoc, Contract,
	SiteSTEffPers, SiteOVTEffPers, SiteEffPers, 
	CentralSTEffPers, CentralOVTEffPers, CentralEffPers, ContractEffPers,
	AGOnSiteEffPers, AGOthLocEffPers, AGEffPers, TotEffPers, SiteOVTPcnt,
	TotEffPersMWH, TotEffPersMW, TotEffPersGJ)
SELECT Refnum, 'TP', SUM(SiteNumPers), SUM(SiteSTH), SUM(SiteOVTHrs),
	SUM(CentralNumPers), SUM(CentralSTH), SUM(CentralOVTHrs),
	SUM(AGOnSite), SUM(AGOthLoc), SUM(Contract),
	SUM(SiteSTEffPers), SUM(SiteOVTEffPers), SUM(SiteEffPers),
	SUM(CentralSTEffPers), SUM(CentralOVTEffPers), SUM(CentralEffPers),
	SUM(ContractEffPers), SUM(AGOnSiteEffPers), SUM(AGOthLocEffPers), SUM(AGEffPers),
	SUM(TotEffPers), CASE WHEN SUM(SiteSTH)>0 THEN 100*SUM(SiteOVTHrs)/SUM(SiteSTH) ELSE NULL END,
    	SUM(TotEffPersMWH), SUM(TotEffPersMW), SUM(TotEffPersGJ)
FROM Pers 
WHERE Refnum = @Refnum AND SectionID IN ('OCCO', 'OCCM', 'OCCT', 'OCCA', 'MPSO', 'MPSM', 'MPST', 'MPSA')
GROUP BY Refnum 
UPDATE PersSTCalc
SET 	CompNumPers = ISNULL(SiteNumPers, 0) + ISNULL(CentralNumPers, 0), 
	CompSTH = ISNULL(SiteSTH, 0) + ISNULL(CentralSTH, 0), 
	CompSTEffPers = ISNULL(SiteSTEffPers, 0) + ISNULL(CentralSTEffPers, 0), 
	CompOVTEffPers = ISNULL(SiteOVTEffPers, 0) + ISNULL(CentralOVTEffPers, 0), 
	CompEffPers = ISNULL(SiteEffPers, 0) + ISNULL(CentralEffPers, 0),
	CompOVTPcnt = CASE WHEN (ISNULL(SiteSTH, 0) + ISNULL(CentralSTH, 0)) > 0 THEN 100*(ISNULL(SiteOVTHrs, 0) + ISNULL(CentralOVTHrs, 0))/(ISNULL(SiteSTH, 0) + ISNULL(CentralSTH, 0)) END
WHERE Refnum = @Refnum
