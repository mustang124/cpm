﻿-- =============================================
-- Author:		Dennis Burdett
-- Create date: July 17, 2008
-- Description:	Updates the EGCTechnology field in TSort table for a single Company
-- Modified: 02/19/09 Dennis - changed the name of the Technology field and now looks to see if
--   the EGCManualTech is True or False
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateEGCTechnologyForCompanyID] 
	@CompanyID char(12)
AS
BEGIN
	SET NOCOUNT ON;

	-- All Coal: 'COAL'
	UPDATE dbo.TSort
	SET EGCTechnology='COAL'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE fuelgroup LIKE 'coal') 
		AND refnum IN ( SELECT refnum FROM tsort WHERE CompanyID=@CompanyID AND EGCManualTech=0)
	 
	-- CC Electric: 'CCGT-E'
	UPDATE dbo.TSort
	SET EGCTechnology='CCGT-E'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE crvsizegroup LIKE 'ce')
		AND refnum IN ( SELECT refnum FROM tsort WHERE CompanyID=@CompanyID AND EGCManualTech=0)

	-- CC Cogen: 'COGEN'
	UPDATE dbo.TSort
	SET EGCTechnology='COGEN'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE (crvsizegroup LIKE 'cg1' or crvsizegroup LIKE 'cg2' or crvsizegroup LIKE 'cg3'))
	 	AND refnum IN ( SELECT refnum FROM tsort WHERE CompanyID=@CompanyID AND EGCManualTech=0)

	-- SGO: 'SGO GAS&OIL'
	UPDATE dbo.TSort
	SET EGCTechnology='SGO GAS&OIL'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE crvgroup LIKE 'gas')
		AND refnum IN ( SELECT refnum FROM tsort WHERE CompanyID=@CompanyID AND EGCManualTech=0)

	-- Multi: 'MULTI'
	UPDATE dbo.TSort
	SET EGCTechnology='MULTI'
	WHERE ((Coal_NDC > 0 AND Oil_NDC > 0) 
		OR (Coal_NDC > 0 AND Gas_NDC > 0) 
		OR (Coal_NDC > 0 AND CTG_NDC > 0)
		OR (Oil_NDC > 0 AND CTG_NDC > 0)
		OR (Gas_NDC > 0 AND Oil_NDC > 0) 
		OR (Gas_NDC > 0 AND CTG_NDC >0))
		AND refnum IN ( SELECT refnum FROM tsort WHERE CompanyID=@CompanyID AND EGCManualTech=0)

END
