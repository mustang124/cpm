﻿CREATE         PROCEDURE [dbo].[spUpdateBreaks] (@Refnum Refnum) 
AS
SET NOCOUNT ON
DECLARE @StudyYear smallint, @LoadType varchar(5), @FuelType varchar(10)
DECLARE @ServiceHrs2Yr real, @NMC real, @TotStarts real, @HeatValue real, @NOF2Yr real, @BlrPSIG real
DECLARE @EquipCnt int
DECLARE @FuelGroup varchar(9), @CoalNMCGroup int, @CoalNMCGroup2 int, @CoalNMCOverlap int, @CoalNMCOverlap2 int, 
	@CoalHVGroup int, @Scrubbers varchar(1), @CoalNMCScrub int, @ScrubberSize varchar(2), @BaseCoal char(1), 
	@GasOilNMCGroup int, @CCNMCGroup int,
	@CombinedCycle char(1), @LoadType2 varchar(3), @SteamGO varchar(1)
DECLARE @CRVGroup char(5), @CoalSulfurGroup char(2), @CoalSulfur real
DECLARE @LTSA YorN, @CogenElec varchar(5)
DECLARE @ScrubbersYN int
-- 06/29/09 Dennis added new fields to handle new Power Blocks in 2008 output
DECLARE @CTGS tinyint, @FTEMP tinyint, @FiringTemp real, @NCF2Y tinyint, @NCF2Yr real
DECLARE @CoalAsh real, @CoalAshGroup int

DELETE FROM Breaks 
	WHERE Refnum = @Refnum

SELECT @StudyYear = dbo.StudyYear(@Refnum)

SELECT @BlrPSIG = BlrPSIG 
	FROM _vDesignData 
	WHERE Refnum = @Refnum AND BlrPSIG IS NOT NULL

-- 11/11/2008 Dennis
-- special code to FORCE Copesul steam site to be Coal
IF @Refnum like '250101P%' or @Refnum like '250102P%' or @Refnum like '250103P%'
BEGIN
print 'copesul'
	UPDATE FuelTotCalc set FuelType = 'Coal' WHERE Refnum = @Refnum
END

-- 11/29/2011 Steve
-- special code to FORCE Nuon Buggenum site to be Gas (this is a Coal Gasified unit)
IF @Refnum LIKE '944CC%'
BEGIN
PRINT 'Nuon Buggenum'
	UPDATE FuelTotCalc SET FuelType = 'Gas' WHERE Refnum = @Refnum
END

SELECT @LoadType = LoadType, @FuelType = FuelType 
	FROM FuelTotCalc 
	WHERE Refnum = @Refnum

SELECT @NMC = NMC, @ServiceHrs2Yr = ServiceHrs2YrAVG, @NOF2Yr = NOF2Yr, @NCF2Yr = ISNULL(NCF2Yr, NCF)
	FROM NERCFactors
	WHERE Refnum = @Refnum

SELECT @TotStarts = TotStarts
	FROM GenerationTotCalc
	WHERE Refnum = @Refnum

SELECT @HeatValue = HeatValue, @CoalSulfur = SulfurPcnt, @CoalAsh = AshPcnt
	FROM CoalTotCalc
	WHERE Refnum = @Refnum

SELECT @FuelGroup = CASE @FuelType
	WHEN 'Coal' THEN 'Coal'
	WHEN 'Oil' THEN 'Gas & Oil'
	WHEN 'Gas' THEN 'Gas & Oil'
	END

IF @StudyYear = 1997 
	SELECT @LoadType = CASE 
		WHEN @LoadType = '1' THEN 'I'
		WHEN @LoadType = '2' THEN 'II'
		WHEN @LoadType = '3' THEN 'III'
		END
ELSE 
	SELECT @LoadType = CASE
		WHEN @ServiceHrs2Yr < 3000 THEN 'Peak' 
		WHEN @ServiceHrs2Yr >= 3000 AND @ServiceHrs2Yr < 6000 AND @TotStarts >= 50 THEN 'Peak' 
		WHEN @ServiceHrs2Yr >= 6000 AND @TotStarts < 50 THEN 'Base' 
		ELSE 'Int'
		END

IF @LoadType = 'Base'
	BEGIN
		IF @NOF2Yr >= 85
			SELECT @LoadType2 = 'HLB'
		ELSE
			SELECT @LoadType2 = 'LFB'
	END

IF rtrim(@FuelType) = 'Coal'
	BEGIN

		SELECT @CoalNMCGroup = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'Coal' 
		AND MinCap <= @NMC AND MaxCap > @NMC

		SELECT @CoalNMCGroup2 = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'Coal2' 
		AND MinCap <= @NMC AND MaxCap > @NMC

		SELECT @CoalNMCOverlap = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'CoalX' 
		AND MinCap <= @NMC AND MaxCap > @NMC

		SELECT @CoalNMCOverlap2 = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'C2X' 
		AND MinCap <= @NMC AND MaxCap > @NMC

		SELECT @CoalNMCScrub = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'Scrub' 
		AND MinCap <= @NMC AND MaxCap > @NMC

		SELECT @CoalHVGroup = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'CHV' 
		AND MinCap <= @HeatValue AND MaxCap > @HeatValue

		SELECT @CoalAshGroup = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'Ash' 
		AND MinCap <= @CoalAsh AND MaxCap > @CoalAsh


		IF @LoadType = 'Base' 
			SELECT @BaseCoal = 'Y'
		ELSE
			SELECT @BaseCoal = 'N'
			
			
			
		-- 01/16/09 DB change scrubber flag to use tsort NOT maint cost field

		-- 6/28/13 SW update scrubber flag in tsort based on if there is maint cost in the data
		DECLARE @wgs REAL

		SELECT @wgs = ISNULL(SUM(ISNULL(CurrCptlLocal,0) + ISNULL(CurrMMOCostLocal,0) + ISNULL(CurrExpLocal,0)) ,0)
		FROM NonOHMaint 
		WHERE Component = 'WGS' AND refnum = @refnum

		SELECT @wgs = @wgs + ISNULL(SUM(ISNULL(directcostlocal,0) + ISNULL(ammocostlocal,0) + ISNULL(lumpsumcostlocal,0)) ,0)
		FROM OHMaint WHERE Component = 'WGS' and Refnum = @Refnum

		IF @wgs = 0 -- one final check of equipment table 
		BEGIN
			SELECT @wgs = COUNT(*) from Equipment where Refnum = 'TJS130P15' and EquipType IN ('WGS','DGS')
		END

		UPDATE TSort SET ScrubbersYN = CASE @wgs WHEN 0 THEN 0 ELSE 1 END WHERE Refnum = @Refnum

		--6/28/13 SW then continue with original code
		
		SELECT @ScrubbersYN = ScrubbersYN 
			FROM tsort  
			WHERE refnum = @Refnum
		SELECT @Scrubbers = CASE
			when @ScrubbersYN = 1 then 'Y'
			--WHEN ScrubberAnnMaintCost > 0 AND TotScrubberNonMaint > 0 THEN 'Y'
			ELSE 'N'
			END
		FROM MiscCalc WHERE Refnum = @Refnum

		SELECT @ScrubberSize = @Scrubbers + CONVERT(char(1), @CoalNMCScrub)


		IF @StudyYear < 2012
		BEGIN 
			SELECT @CRVGroup = CASE WHEN @HeatValue <= 10250 THEN 'LB' WHEN @HeatValue > 10250 THEN 'E' END

			IF @CRVGroup = 'E'
				SELECT @CRVGroup = RTRIM(@CRVGRoup) + CASE WHEN @BlrPSIG < 3000 THEN 'D' WHEN @BlrPSIG >= 3000 THEN 'SC' ELSE '' END
		END
		ELSE
		BEGIN
			--2012 or later we changed the calcs a little
			SELECT @CRVGroup = 
				CASE WHEN @HeatValue <= 10250 THEN
					CASE WHEN @BlrPSIG >= 3200 THEN 'LSC' ELSE 'LB' END --if PSIG is NULL they default to LB
				ELSE
					CASE WHEN @BlrPSIG >= 3200 THEN 'ESC' ELSE 'ED' END --if PSIG is NULL they default to ED
				END
		END

		IF @StudyYear < 2012
		BEGIN
			IF @CoalSulfur <= 1.2 
				SELECT @CoalSulfurGroup = 'Lo'
			IF @CoalSulfur > 1.2
				SELECT @CoalSulfurGroup = 'Hi'
		END
		ELSE
		BEGIN
			IF @CoalSulfur <= 0.4
				SELECT @CoalSulfurGroup = 'Lo'
			IF @CoalSulfur > 0.8
				SELECT @CoalSulfurGroup = 'Hi'
			IF @CoalSulfur > 0.4 AND @CoalSulfur <= 0.8
				SELECT @CoalSulfurGroup = 'Md'
		END
	END
ELSE 
	BEGIN
		SELECT @GasOilNMCGroup = ClassNum
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = 'GO'
		AND MinCap <= @NMC AND MaxCap > @NMC
	END
SELECT @CombinedCycle = 'N'
IF EXISTS (SELECT * FROM Equipment WHERE Refnum = @Refnum AND EquipType LIKE 'CTG%')
	BEGIN
	--	IF EXISTS (SELECT * FROM MaintEquipCalc WHERE Refnum = @Refnum AND EquipGroup IN ('CTG-HRSG', 'CTG-GEN') AND AnnMaintCostKUS > 0)
	--	BEGIN
			SELECT @CombinedCycle = 'Y'	
			SELECT @CCNMCGroup = ClassNum

			FROM Class_LU
			WHERE StudyYear = @StudyYear AND ClassType = 'CC'
			AND MinCap <= @NMC AND MaxCap > @NMC
	--	END
	END
IF @FuelType = 'Gas' OR @FuelType = 'Oil'
	BEGIN
		IF @CombinedCycle = 'Y'
			BEGIN
				SELECT @SteamGO = 'N' , @CRVGroup = 'CE'

				--SW 8/18/14 two lines below to change the CRV Group to CG (added the CE/CG split in the 2013 study
				IF EXISTS (SELECT * FROM GenerationTotCalc WHERE Refnum = @Refnum AND StmSalesMWH/AdjNetMWH<>0.00)
					SELECT @CRVGroup = 'CG'
				
				--SW 3/5/13 two lines below added to distinguish a Simple Cycle from a Combined Cycle
				--using the rule that an SC only has CTGs in NERCTurbine
				IF EXISTS (select * from NERCTurbine where Refnum = @Refnum and refnum in (select refnum from nercturbine where TurbineType = 'CTG') and Refnum not in (select Refnum from NERCTurbine where TurbineType <> 'CTG'))
					SELECT @CRVGroup = 'SC'
			END
		ELSE
			SELECT @SteamGO = 'Y' , @CRVGroup = 'GAS'
	END

IF @CombinedCycle = 'Y'
BEGIN 
	SELECT @LTSA = 'N', @CogenElec = 'Elec'
	-- 05/06/09 Dennis - changed LTSA flag to be 'Y' if it has Contract money OR LTSA Payment money
	--IF EXISTS (SELECT * FROM LTSA WHERE Refnum = @Refnum AND TotCostKLocal>0)
	IF EXISTS (SELECT * FROM LTSA WHERE Refnum = @Refnum AND (TotCostKLocal>0 OR PaymentCurrKLocal>0))
		SELECT @LTSA = 'Y'
	-- 09/23/08 DB changed the >0.01 to <>0.00 per Rod
	IF EXISTS (SELECT * FROM GenerationTotCalc WHERE Refnum = @Refnum AND StmSalesMWH/AdjNetMWH<>0.00)
		SELECT @CogenElec = 'Cogen'
	--SW 9/26/14 removed the following lines, because we got a Nuon unit that is not a cogen, and it screwed up the presentation
	---- Reliant-Europe/NUON have always been Cogen units so we are overriding
	--IF EXISTS (SELECT * FROM TSort WHERE (Refnum = @Refnum AND CompanyID IN ('NUON', 'Reliant-Euro'))) 
	--	SELECT @CogenElec = 'Cogen'
	
	-- 06/29/09 Dennis added new fields to handle new Power Blocks in 2008 output
	IF @StudyYear < 2012
	BEGIN
		IF @NCF2Yr>0 and @NCF2Yr<40
			SET @NCF2Y=1
		ELSE IF @NCF2Yr>39 and @NCF2Yr<61
			SET @NCF2Y=2
		ELSE IF @NCF2Yr>60
			SET @NCF2Y=3
	END
	ELSE
	-- 7/8/13 SW split to 5 groups in 2012, at 20% intervals
	BEGIN
		IF @NCF2Yr > 0 AND @NCF2Yr <= 20
			SET @NCF2Y = 1
		ELSE IF @NCF2Yr > 20 and @NCF2Yr <= 40
			SET @NCF2Y = 2
		ELSE IF @NCF2Yr > 40 AND @NCF2Yr <= 60
			SET @NCF2Y = 3
		ELSE IF @NCF2Yr > 60 AND @NCF2Yr <= 80
			SET @NCF2Y = 4
		ELSE IF @NCF2Yr > 80
			SET @NCF2Y = 5
	END
	
	-- 06/29/09 Dennis added new fields to handle new Power Blocks in 2008 output
	--SELECT @CTGS = CTGS
	--	FROM TSort
	--	WHERE Refnum = @Refnum
	--8/10/15 SW changed calc because we no longer use TSort as data source
	SELECT @CTGS = COUNT(TurbineType) FROM NERCTurbine WHERE Refnum = @Refnum AND TurbineType = 'CTG'

	IF @CTGS = 0
	BEGIN
		--SW updates 4/25/16
		--first we attempt to get the total from CTGData, which for newer units (>=2014) should be correct based on what they entered into the input form
		SELECT @CTGS = COUNT(*) FROM CTGData WHERE Refnum = @Refnum

		-- but if we still don't have a number, we will set it to 1
		IF @CTGS = 0
			SET @CTGS = 1 --this is not necessarily correct but it is as close as we can reasonably get
	END

	IF @CTGS>4
		SET @CTGS=4
		
	-- 06/29/09 Dennis added new fields to handle new Power Blocks in 2008 output
	SELECT @FiringTemp = AVG(FiringTemp)
		FROM CTGData
		WHERE Refnum = @Refnum
	IF @FiringTemp>0 and @FiringTemp<2282
		SET @FTEMP=1
	ELSE IF @FiringTemp>=2282
		SET @FTEMP=2
END

INSERT INTO Breaks (Refnum, FuelGroup, LoadType, CoalNMCGroup, CoalNMCGroup2, GasOilNMCGroup, CoalHVGroup, Scrubbers, ScrubberSize, CombinedCycle, 
		BaseCoal, LoadType2, SteamGasOil, CRVGroup, CoalNMCOverlap, CoalNMCOverlap2, CoalSulfurGroup, CCNMCGroup, LTSA, CogenElec, 
		NCF2Yr, CTGS, FTEMP, CoalAshGroup)
	SELECT @Refnum, @FuelGroup, @LoadType, @CoalNMCGroup, @CoalNMCGroup2, @GasOilNMCGroup, @CoalHVGroup, @Scrubbers, @ScrubberSize, @CombinedCycle, 
		@BaseCoal, @LoadType2, @SteamGO, @CRVGroup, @CoalNMCOverlap, @CoalNMCOverlap2, @CoalSulfurGroup, @CCNMCGroup, @LTSA, @CogenElec,
		@NCF2Y, @CTGS, @FTEMP, @CoalAshGroup

--GRANT  EXECUTE  ON [dbo].[spUpdateBreaks]  TO [Datagrp]
--GO

--GRANT  EXECUTE  ON [dbo].[spUpdateBreaks]  TO [Developer]
--GO






