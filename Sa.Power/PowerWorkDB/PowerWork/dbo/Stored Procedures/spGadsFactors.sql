﻿CREATE               PROC [dbo].[spGadsFactors] (
 	@UtilityUnitCode varchar(6), 
	@Year1 int, 
	@Year2 int,
	@NMC real output,
	@NDC real output,
	@EFOR real output,	-- F11 #24
	@EFORWtFactor real output,
	@EPOF real output,	-- F9 #18
	@EUOF real output,	-- F10 #21
	@EPOR real output,	-- F11 #26
	@EPORWtFactor real output,
	@EUOR real output,	-- F11 #28
	@EUORWtFactor real output,
	@EUF  real output,	-- F8 #11
	@EAF  real output,	-- F9 #12
	@POF  real output,	-- F7 #1
	@EOR  real output,	-- Solomon Item
	@EORWtFactor real output,
	@FOF  real output,	-- F7 #3
	@MOF  real output,	-- F7 #4
	@NUMFO real output,	-- F5 #16
	@NUMMO real output,	-- F5 #17
	@NUMPO real output,	-- F4 #14
	@NUMUO real output, 	-- F4 #15
	@PH    real output,	-- F2 #11
	@SH    real output, 	-- F1 #1
	@UOF   real output,	-- F7 #2
	@PH_Unweighted real output,
	@SH_Unweighted real output,
	@AGE 	real output,
	@ACT_Starts real output,
	@ATM_Starts real output,
	-- 01/21/09 DB new table fields and weight factors for Rod
	@WEFOF real output, 
	--@EFOFWtFactor real output, 
	@WUOR real output, 
	@UORWtFactor real output,
	@WFOR real output, 
	@FORWtFactor real output,
	@WPOR real output, 
	@PORWtFactor real output,
	@RSHours real = NULL output,
	@RSEvents real = NULL output,
	@RS_Unweighted real = NULL output,
	@AF real = NULL output,
	@YearsCounted real = NULL output --SW 9/18/13 added
)
AS

--                      Gads appendix F Page 1 and 2 definition number:
--DECLARE		@SH real   	--  1
DECLARE		@AH  real	--  4
DECLARE		@POH real   	--  5
DECLARE		@UOH real   	--  6
DECLARE		@FOH real   	--  7
DECLARE		@MOH real 	--  8
--DECLARE		@PH real   	--  11
DECLARE		@EFDH real   	--  13A
DECLARE		@EPDH real   	--  13B
DECLARE		@EUDH real   	--  13C
DECLARE		@EFDHRS real   	--  13D
DECLARE		@EPDHRS real   	--  13E
DECLARE		@EUDHRS real  	--  ??
--DECLARE		@NUMFO real 	-- F5 #16
--DECLARE		@NUMMO real 	-- F5 #17
--DECLARE		@NUMPO real 	-- F4 #14
--DECLARE		@NUMUO real  	-- F4 #15
DECLARE 	@EAFx real -- unweigthed interim var
			-- to avoid having to get NMC.

-- Load up the items
SELECT 
@SH = SUM(eh.E_SH),
@SH_Unweighted = SUM(eh.SH),
@POH = SUM(eh.E_PO + eh.E_PO_SE),
@UOH = SUM(eh.E_U1 + eh.E_U2 + eh.E_U3 + eh.E_SF + eh.E_MO + eh.E_MO_SE),
@FOH = SUM(eh.E_U1 + eh.E_U2 + eh.E_U3 + eh.E_SF),
@MOH = SUM(eh.E_MO + eh.E_MO_SE),
@PH = SUM(eh.E_PH),         
@PH_Unweighted = SUM(eh.PH),                      
@EFDH = SUM(eh.E_D1 + eh.E_D2 + eh.E_D3),
@EPDH = SUM(eh.E_PD + eh.E_PD_DE),
@EUDH = SUM(eh.E_D1 + eh.E_D2 + eh.E_D3 + eh.E_D4 + eh.E_D4_DE),
@EFDHRS = SUM(eh.E_EUFDH_RS),
@EPDHRS = SUM(eh.E_EPDH_RS),
@EUDHRS = SUM(eh.E_EUFDH_RS),     -- CHECK WITH RON -- SAME AS EFDHRS??????
@EAF = SUM(eh.EAF*EH.E_PH)/SUM(EH.E_PH),
@AGE = AVG(eh.UnitAge),
@RSHours = SUM(eh.E_RS),
@RS_Unweighted = SUM(eh.RS),
@AF = SUM(eh.AF*EH.E_PH)/SUM(EH.E_PH),
@YearsCounted = COUNT(*) --SW 9/18/13 added as a check for there only being one year of data 
FROM GADSOS.GADSNG.EventHours eh
INNER JOIN GADSOS.GADSNG.Setup s ON s.UnitShortName = eh.UnitShortName
WHERE (DatePart(yy,TL_DateTime) = @Year1 OR DatePart(yy,TL_DateTime) = @Year2) 
AND (eh.Granularity = 'Yearly')
AND eh.E_PH <> 0
AND s.UtilityUnitCode = @UtilityUnitCode
 -- '330110'

-- Get counts for each event type.
SELECT 
@NUMPO = SUM(CASE WHEN ed.EventType = 'PO' THEN 1 ELSE 0 END),
@NUMUO = SUM(CASE WHEN ed.EventType IN ('MO', 'U1', 'U2', 'U3') THEN 1 ELSE 0 END),
@NUMFO = SUM(CASE WHEN ed.EventType IN ('U1', 'U2', 'U3') THEN 1 ELSE 0 END),
@NUMMO = SUM(CASE WHEN ed.EventType = 'MO' THEN 1 ELSE 0 END),
@RSEvents = SUM(CASE WHEN ed.EventType = 'RS' THEN 1 ELSE 0 END)
FROM GADSOS.GADSNG.EventData01 ed
WHERE (ed.Year = @Year1 OR ed.Year = @Year2)
AND ed.UtilityUnitCode = @UtilityUnitCode
AND ed.EventType IN ('PO', 'MO', 'U1', 'U2', 'U3', 'RS')

-- Get NMC, NDC, ACT_Starts, ATM_Starts.
SELECT 
@NMC = (SUM(pd.NetMaxCap * pd.PeriodHours) / SUM(pd.PeriodHours)),
@NDC = (SUM(pd.NetDepCap * pd.PeriodHours) / SUM(pd.PeriodHours)),
@ACT_Starts = SUM(ActualStarts), 
@ATM_Starts = SUM(AttemptedStarts)
from GADSOS.GADSNG.PerformanceData pd
where (pd.Year = @Year1 OR pd.Year = @Year2)
AND pd.UtilityUnitCode = @UtilityUnitCode

-- Calc factor from variables that we have loaded up.
SELECT @EFORWtFactor = @FOH + @SH + @EFDHRS
IF @EFORWtFactor > 0 
 	SELECT @EFOR = 100 * ((@FOH + @EFDH) / @EFORWtFactor)  -- F11 #24
ELSE	SELECT @EFOR = NULL

IF @PH > 0 
	BEGIN
 		SELECT @EPOF = 100 * ((@POH + @EPDH) / @PH)  		 -- F9 #18
		SELECT @EUOF = 100 * ((@UOH + @EUDH) / @PH)		 -- F10 #21
		SELECT @EUF = 100 * ((@UOH + @POH + @EUDH + @EPDH) / @PH)  -- F8 #11		--	SELECT @EAF = (@EAFx * @PH) / @PH			-- F9 #12
 		SELECT @POF = 100 * (@POH / @PH)			-- F7 #1
		SELECT @UOF = 100 * (@UOH / @PH)			-- F7 #2
		SELECT @FOF = 100 * (@FOH / @PH) 			-- F7 #3
		SELECT @MOF = 100 * (@MOH / @PH) 			-- F7 #4
		-- 01/21/09 DB new table fields and weight factors for Rod
		SELECT @WEFOF = 100 * ((@FOH + @EFDH) / @PH)
	END
ELSE	
	BEGIN
		SELECT @EPOF = NULL
		SELECT @EUOF = NULL
		SELECT @EUF  = NULL
		SELECT @EAF = NULL
		SELECT @POF = NULL
		SELECT @UOF = NULL
		SELECT @FOF = NULL
		SELECT @MOF = NULL
		-- 01/21/09 DB new table fields and weight factors for Rod
		SELECT @WEFOF = NULL
		SELECT @AF = NULL
	END

SELECT @EPORWtFactor = @POH + @SH + @EPDHRS
IF @EPORWtFactor > 0
 	SELECT @EPOR = 100 * ((@POH + @EPDH) / @EPORWtFactor)  -- F11 #26
ELSE 	SELECT @EPOR = NULL

SELECT @EUORWtFactor = @UOH + @SH + @EUDHRS -- 6/13/2006 FRS ADDED "+ @EUDHRS"
IF @EUORWtFactor > 0 
 	SELECT @EUOR = 100 * ((@UOH + @EUDH) / @EUORWtFactor) 		 -- F11 #28
ELSE  	SELECT @EUOR = NULL

SELECT @EORWtFactor = (@POH + @UOH + @SH + @EPDHRS + @EUDHRS)
IF @EORWtFactor > 0
  	SELECT @EOR  = 100 * ((@POH + @EPDH + @UOH + @EUDH) 
		     / (@POH + @UOH + @SH + @EPDHRS + @EUDHRS))  -- Solomon Item
ELSE SELECT @EOR = NULL

-- 01/21/09 DB new table fields and weight factors for Rod
SELECT @UORWtFactor = @UOH + @SH
IF @UORWtFactor > 0
	SELECT @WUOR = 100 * (@UOH / @UORWtFactor)
ELSE SELECT @WUOR = NULL

-- 01/21/09 DB new table fields and weight factors for Rod
SELECT @FORWtFactor = @FOH + @SH
IF @FORWtFactor > 0
	SELECT @WFOR = 100 * (@FOH / @FORWtFactor)
ELSE SELECT @WFOR = NULL

-- 01/21/09 DB new table fields and weight factors for Rod
SELECT @PORWtFactor = @POH + @SH
IF @PORWtFactor > 0
	SELECT @WPOR = 100 * (@FOH / @PORWtFactor)
ELSE SELECT @WPOR = NULL

-- 4/20/09 GLC - if no event records then service hours and period hours are not in EventHours
-- pull from PerformanceRecords
IF @PH_Unweighted IS NULL
BEGIN
	SELECT @PH_Unweighted = SUM(PeriodHours), @SH_Unweighted = SUM(ServiceHours), @RS_Unweighted = SUM(RSHours)
	FROM GADSOS.GADSNG.PerformanceRecords p 
	INNER JOIN GADSOS.GADSNG.Setup s ON s.UnitShortName = p.UnitShortName
	WHERE (p.Year = @Year1 OR p.Year = @Year2) AND (p.Period = 'YR')
	AND s.UtilityUnitCode = @UtilityUnitCode
	
END
