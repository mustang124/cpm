﻿CREATE PROCEDURE [dbo].[CalcMaint](@SiteID SiteID)
AS

BEGIN TRY

	DECLARE @OHLumpSumOCCPcnt real; SET @OHLumpSumOCCPcnt = 0.90
	DECLARE @OHLumpSumMPSPcnt real; SELECT @OHLumpSumMPSPcnt = 1 - @OHLumpSumOCCPcnt
	DECLARE @CTGOCCPcnt real; SELECT @CTGOCCPcnt = 7/8.0
	DECLARE @CTGMPSPcnt real; SELECT @CTGMPSPcnt = 1 - @CTGOCCPcnt
	DECLARE @ContOCCPcnt real; SET @ContOCCPcnt = 0.90
	DECLARE @ContMPSPcnt real; SELECT @ContMPSPcnt = 1 - @ContOCCPcnt
	DECLARE @HrsPerEffPers real; SELECT @HrsPerEffPers = 1800
	DECLARE @StudyYear StudyYear; SELECT @StudyYear = StudyYear FROM StudySites WHERE SiteID = @SiteID
		
Print 'insert Units'
	DECLARE @Units TABLE (Refnum char(12) NOT NULL, MWH real NULL, MW real NULL, GJ real NULL, OpHrs real NULL, MBTU real NULL)
	INSERT @Units (Refnum, MWH, MW, GJ, OpHrs, MBTU)
	SELECT Refnum, 
		MWH = (SELECT AdjNetMWH2Yr FROM GenerationTotCalc WHERE GenerationTotCalc.Refnum = u.Refnum),
		MW = (SELECT NMC2Yr*1000 FROM NERCFactors WHERE NERCFactors.Refnum = u.Refnum),
		GJ = (SELECT TotalOutputGJ2Yr FROM GenerationTotCalc WHERE GenerationTotCalc.Refnum = u.Refnum), --SW 4/17/13
		OpHrs = (SELECT ServiceHrs FROM NERCFactors WHERE NERCFactors.Refnum = u.Refnum), -- SW 4/18/13
		MBTU = (SELECT TotalOutputMBTU2Yr FROM GenerationTotCalc WHERE GenerationTotCalc.Refnum = u.Refnum) -- SW 8/2/13
	FROM TSort u WHERE SiteID = @SiteID

Print 'Select Labor Rates'
	-- Get labor rates from YearlyFactors
	DECLARE @ContractorRate real, @CTGContractorRate real, @CCNonOHLumpSumRate real
	SELECT @ContractorRate = ContractorRate, @CTGContractorRate = CTGContractorRate, @CCNonOHLumpSumRate = CCNonOHLumpSumRate
	FROM YearlyFactors WHERE Year = @StudyYear
	
Print 'Select InflationFactors'
	-- Get inflation factors to be applied to overhaul costs
	SELECT Year, InflFactor = InflationFactor/ISNULL((SELECT InflationFactor FROM YearlyFactors WHERE Year = @StudyYear),(SELECT InflationFactor FROM YearlyFactors WHERE Year = (SELECT MAX(Year) FROM YearlyFactors)))
	INTO #Infl
	FROM YearlyFactors
	WHERE Year <= @StudyYear

	DECLARE @LTSAAdj real
Print 'Delete records from MainTotCalc, OHEquipCalc, MaintEquipCalc, MaintEquipSum'
	DELETE FROM MaintTotCalc WHERE Refnum IN (SELECT Refnum FROM @Units)
	DELETE FROM OHEquipCalc WHERE Refnum IN (SELECT Refnum FROM @Units)
	DELETE FROM MaintEquipCalc WHERE Refnum IN (SELECT Refnum FROM @Units)
	DELETE FROM MaintEquipSum WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	--    Update AMMO Costs
	DECLARE @CurrNonOH real, @PrevNonOH real, @CurrOH real, @PrevOH real,
			@CurrAMMO real, @PrevAMMO real, @CurrAMMOPcnt real, @PrevAMMOPcnt real
	SELECT @CurrNonOH = SUM(ISNULL(CurrCptl,0) + ISNULL(CurrExp,0) + ISNULL(CurrLaborCost,0) + ISNULL(CurrMMOCost,0))
		, @PrevNonOH = SUM(ISNULL(PrevCptl,0) + ISNULL(PrevExp,0) + ISNULL(PrevLaborCost,0) + ISNULL(PrevMMOCost,0))
	FROM NonOHMaint
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	SELECT @CurrOH = SUM(ISNULL(DirectCost,0) + ISNULL(LumpSumCost,0))
	FROM OHMaint
	WHERE Refnum IN (SELECT Refnum FROM @Units)
	AND DATEPART(yy, OHDate) = @StudyYear

	SELECT @PrevOH = SUM(ISNULL(DirectCost,0) + ISNULL(LumpSumCost,0))
	FROM OHMaint
	WHERE Refnum IN (SELECT Refnum FROM @Units)
	AND DATEPART(yy, OHDate) = @StudyYear-1

	SELECT @CurrAMMO = PlantCurrMMOACost, @PrevAMMO = PlantPrevMMOACost
	FROM PlantGenData WHERE SiteID = @SiteID

	SELECT	@CurrAMMO = ISNULL(@CurrAMMO,0), @CurrNonOH = ISNULL(@CurrNonOH,0), @CurrOH = ISNULL(@CurrOH,0),
			@PrevAMMO = ISNULL(@PrevAMMO,0), @PrevNonOH = ISNULL(@PrevNonOH,0), @PrevOH = ISNULL(@PrevOH,0)

	SELECT @CurrAMMOPcnt = CASE WHEN (@CurrNonOH + @CurrOH) > 0 THEN @CurrAMMO / (@CurrNonOH + @CurrOH) ELSE 0 END
	SELECT @PrevAMMOPcnt = CASE WHEN (@PrevNonOH + @PrevOH) > 0 THEN @PrevAMMO / (@PrevNonOH + @PrevOH) ELSE 0 END

Print 'Update NonOHMaint'
	UPDATE NonOHMaint
	SET CurrAMMOCost = (ISNULL(CurrCptl,0) + ISNULL(CurrExp,0) + ISNULL(CurrLaborCost,0) + ISNULL(CurrMMOCost,0))*@CurrAMMOPcnt, 
		CurrTotNonOHCost = (ISNULL(CurrCptl,0) + ISNULL(CurrExp,0) + ISNULL(CurrLaborCost,0) + ISNULL(CurrMMOCost,0))*(1+@CurrAMMOPcnt),
		PrevAMMOCost = (ISNULL(PrevCptl,0) + ISNULL(PrevExp,0) + ISNULL(PrevLaborCost,0) + ISNULL(PrevMMOCost,0))*@PrevAMMOPcnt, 
		PrevTotNonOHCost = (ISNULL(PrevCptl,0) + ISNULL(PrevExp,0) + ISNULL(PrevLaborCost,0) + ISNULL(PrevMMOCost,0))*(1+@PrevAMMOPcnt)
	WHERE Refnum IN (SELECT Refnum FROM @Units)

Print 'Update OHMaint 1'
	UPDATE OHMaint
	SET AMMOCost = (ISNULL(DirectCost,0) + ISNULL(LumpSumCost,0))*@CurrAMMOPcnt
	WHERE Refnum IN (SELECT Refnum FROM @Units)
	AND DATEPART(yy, OHDate) = @StudyYear

Print 'Update OHMaint 2'
	UPDATE OHMaint
	SET AMMOCost = (ISNULL(DirectCost,0) + ISNULL(LumpSumCost,0))*@PrevAMMOPcnt
	WHERE Refnum IN (SELECT Refnum FROM @Units)
	AND DATEPART(yy, OHDate) = @StudyYear - 1

Print 'Select Complexity and Contractor Rate'
	--    AnnualOHCalc;
	-- Get OH data
	SELECT *, OHComplexityMult = CAST(1.0 as real), OHYear = ISNULL(DATEPART(yy, OHDate), @StudyYear), ContractorRate = CAST(NULL as real)
	INTO #OH
	FROM OHMaint 
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	UPDATE oh
	SET OHComplexityMult = CAST(Quantity as real)/Repaired
	FROM #OH oh INNER JOIN OHMaintCPHM cphm ON cphm.Refnum = oh.Refnum
	WHERE oh.Component = 'CPHM' AND oh.ProjectID = 'OVHL' AND cphm.Quantity > 0 AND cphm.Repaired > 0

	UPDATE oh
	SET InflationFactor = (SELECT InflFactor FROM #Infl WHERE Year = oh.OHYear)
	FROM #OH oh

	IF EXISTS (SELECT * FROM #OH WHERE OHYear < (SELECT MIN(Year) FROM #Infl))
		UPDATE oh
		SET InflationFactor = (SELECT InflFactor FROM #Infl WHERE Year = (SELECT MIN(Year) FROM #Infl))
		FROM #OH oh
		WHERE OHYear < (SELECT MIN(Year) FROM #Infl)
		
	IF EXISTS (SELECT * FROM #OH WHERE OHYear > (SELECT MAX(Year) FROM #Infl))
		UPDATE oh
		SET InflationFactor = (SELECT InflFactor FROM #Infl WHERE Year = (SELECT MAX(Year) FROM #Infl))
		FROM #OH oh
		WHERE OHYear > (SELECT MAX(Year) FROM #Infl)

	UPDATE #OH
	SET InflationFactor = 1
	WHERE InflationFactor IS NULL OR InflationFactor = 0

	UPDATE oh
	SET ContractorRate = CASE WHEN Component IN ('CTG-TURB', 'CTG-COMB', 'CTG-COMP') THEN ISNULL(yf.CTGContractorRate, 225) ELSE ISNULL(yf.ContractorRate, 60) END
	FROM #OH oh LEFT JOIN YearlyFactors yf ON yf.Year = oh.OHYear

	UPDATE #OH
	SET TotOHCost = (ISNULL(DirectCost,0) + ISNULL(AMMOCost,0) + ISNULL(LumpSumCost,0)) * OHComplexityMult

	UPDATE #OH
	SET IntervalDays = CASE WHEN TotOHCost = 0 THEN NULL WHEN OHDate IS NULL OR PrevOHDate IS NULL OR OHDate <= PrevOHDate THEN 730.5 ELSE DATEDIFF(hh, PrevOHDate, OHDate)/24.0 END

	UPDATE #OH
	SET IntervalYrs = IntervalDays / 365.25

Print 'Personel Overhaul'
	UPDATE #OH SET
		LumpSumHrs = ISNULL(LumpSumCost,0)*1000/ContractorRate,
		AnnSiteOCCSTH = ISNULL(SiteOCCSTH,0)/IntervalYrs*OHComplexityMult, 
		AnnSiteOCCOVT = ISNULL(SiteOCCOVT,0)/IntervalYrs*OHComplexityMult,
		AnnSiteMPSSTH = ISNULL(SiteMPSSTH,0)/IntervalYrs*OHComplexityMult, 
		AnnSiteMPSOVT = ISNULL(SiteMPSOVT,0)/IntervalYrs*OHComplexityMult,
		AnnCentralOCCSTH = ISNULL(CentralOCCSTH,0)/IntervalYrs*OHComplexityMult,
		AnnCentralOCCOVT = ISNULL(CentralOCCOVT,0)/IntervalYrs*OHComplexityMult,
		AnnCentralMPSSTH = ISNULL(CentralMPSSTH,0)/IntervalYrs*OHComplexityMult,
		AnnCentralMPSOVT = ISNULL(CentralMPSOVT,0)/IntervalYrs*OHComplexityMult,
		AnnAGOCC = ISNULL(AGOCC,0)/IntervalYrs*OHComplexityMult,
		AnnAGMPS = ISNULL(AGMPS,0)/IntervalYrs*OHComplexityMult,
		AnnContractOCC = ISNULL(ContractOCC,0)/IntervalYrs*OHComplexityMult,
		AnnContractMPS = ISNULL(ContractMPS,0)/IntervalYrs*OHComplexityMult

Print 'Total Effort'
	UPDATE #OH SET 
		TotEffort = ISNULL(SiteOCCSTH,0) + ISNULL(SiteOCCOVT,0) + ISNULL(SiteMPSSTH,0) + ISNULL(SiteMPSOVT,0)
				+ ISNULL(CentralOCCSTH,0) + ISNULL(CentralOCCOVT,0) + ISNULL(CentralMPSSTH,0) + ISNULL(CentralMPSOVT,0)
				+ ISNULL(AGOCC,0) + ISNULL(AGMPS,0) + ISNULL(ContractOCC,0) + ISNULL(ContractMPS,0) + LumpSumHrs

Print 'Update OHMaint'
	UPDATE OHMaint SET 
		InflationFactor = x.InflationFactor,
		IntervalYrs = x.IntervalYrs, 
		IntervalDays = x.IntervalDays,
		AnnSiteOCCSTH = x.AnnSiteOCCSTH,
		AnnSiteOCCOVT = x.AnnSiteOCCOVT,
		AnnSiteMPSSTH = x.AnnSiteMPSSTH,
		AnnSiteMPSOVT = x.AnnSiteMPSOVT,
		AnnCentralOCCSTH = x.AnnCentralOCCSTH,
		AnnCentralOCCOVT = x.AnnCentralOCCOVT,
		AnnCentralMPSSTH = x.AnnCentralMPSSTH,
		AnnCentralMPSOVT = x.AnnCentralMPSOVT,
		AnnAGOCC = x.AnnAGOCC,
		AnnAGMPS = x.AnnAGMPS,
		AnnContractOCC = x.AnnContractOCC,
		AnnContractMPS = x.AnnContractMPS,
		SiteOCCSTEffPers = x.AnnSiteOCCSTH/@HrsPerEffPers, 
		SiteOCCOVTEffPers = x.AnnSiteOCCOVT/@HrsPerEffPers,
		SiteMPSSTEffPers = x.AnnSiteMPSSTH/@HrsPerEffPers, 
		SiteMPSOVTEffPers = x.AnnSiteMPSOVT/@HrsPerEffPers,
		CentralOCCSTEffPers = x.AnnCentralOCCSTH/@HrsPerEffPers, 
		CentralOCCOVTEffPers = x.AnnCentralOCCOVT/@HrsPerEffPers,
		CentralMPSSTEffPers = x.AnnCentralMPSSTH/@HrsPerEffPers, 
		CentralMPSOVTEffPers = x.AnnCentralMPSOVT/@HrsPerEffPers,
		AGOCCEffPers = x.AnnAGOCC/@HrsPerEffPers, 
		AGMPSEffPers = x.AnnAGMPS/@HrsPerEffPers,
		ContOCCEffPers = x.AnnContractOCC/@HrsPerEffPers, 
		ContMPSEffPers = x.AnnContractMPS/@HrsPerEffPers,
		LumpSumEffPers = (x.LumpSumHrs/x.IntervalYrs*x.OHComplexityMult)/@HrsPerEffPers, 
		LumpSumHrs = x.LumpSumHrs, 
		AnnLumpSumHrs = x.LumpSumHrs/x.IntervalYrs*x.OHComplexityMult,
		TotOHCost = x.TotOHCost, 
		TotAnnOHCost = x.TotOHCost*x.InflationFactor/x.IntervalYrs,
		TotEffort = x.TotEffort, 
		TotAnnEffPers = (x.TotEffort/x.IntervalYrs*x.OHComplexityMult)/@HrsPerEffPers
	FROM OHMaint INNER JOIN #OH x ON x.Refnum = OHMaint.Refnum AND x.TurbineID = OHMaint.TurbineID AND x.ID = OHMaint.ID

	DROP TABLE #OH
	DROP TABLE #Infl

	--    OHEquipCalc;
	Print 'Inserting OHEquipCalc'
	INSERT INTO OHEquipCalc (Refnum, EquipID, Component, TurbineID,
		SiteOCCSTEffPers, SiteOCCOVTEffPers, SiteMPSStEffPers,
		SiteMPSOvtEffPers, CentralOCCStEffPers, CentralOCCOvtEffPers, CentralMPSStEffPers,
		CentralMPSOvtEffPers, AGOCCEffPers, AGMPSEffPers, ContOCCEffPers,
		ContMPSEffPers, LumpSumEffPers, TotAnnOHCost, TotAnnEffPers, TotAnnOHCostMWH, TotAnnOHCostMW, TotAnnOHCostGJ, TotAnnOHCostMBTU)
	SELECT oh.Refnum, EquipID, Component, TurbineID,
		SUM(SiteOCCStEffPers), SUM(SiteOCCOvtEffPers), SUM(SiteMPSStEffPers),
		SUM(SiteMPSOvtEffPers), SUM(CentralOCCStEffPers), SUM(CentralOCCOvtEffPers),
		SUM(CentralMPSStEffPers), SUM(CentralMPSOvtEffPers), SUM(AGOCCEffPers),
		SUM(AGMPSEffPers), SUM(ContOCCEffPers), SUM(ContMPSEffPers),
		SUM(LumpSumEffPers), SUM(TotAnnOHCost), SUM(TotAnnEffPers),
		CASE WHEN u.MWH > 0 THEN SUM(TotAnnOHCost)/(u.MWH/1000) END,
		CASE WHEN u.MW > 0 THEN SUM(TotAnnOHCost)/(u.MW/1000) END,
		CASE WHEN u.GJ > 0 THEN SUM(TotAnnOHCost)/(u.GJ/1000) END,
		CASE WHEN u.MBTU > 0 THEN SUM(TotAnnOHCost)/(u.MBTU/1000) END
	FROM OHMaint oh INNER JOIN @Units u ON u.Refnum = oh.Refnum
	GROUP BY oh.Refnum, EquipID, Component, TurbineID, u.MWH, u.MW, u.GJ, u.MBTU


	--    NonOHMaintCalc;
Print 'Updating NonOHMaint'
	UPDATE NonOHMaint
	SET AnnCptl = (ISNULL(CurrCptl,0) + ISNULL(PrevCptl,0))/2, 
		AnnExp = (ISNULL(CurrExp,0) + ISNULL(PrevExp,0))/2, 
		AnnLaborCost = (ISNULL(CurrLaborCost,0) + ISNULL(PrevLaborCost,0))/2, 
		AnnMMOCost = (ISNULL(CurrMMOCost,0) + ISNULL(PrevMMOCost,0))/2,
		AnnAMMOCost = (ISNULL(CurrAMMOCost,0) + ISNULL(PrevAMMOCost,0))/2, 
		AnnNonOHCost = (ISNULL(CurrTotNonOHCost,0) + ISNULL(PrevTotNonOHCost,0))/2,
		AnnNonOHCostMWH = CASE WHEN u.MWH > 0 THEN (ISNULL(CurrTotNonOHCost,0) + ISNULL(PrevTotNonOHCost,0))/2*1000/u.MWH END, 
		AnnNonOHCostMW = CASE WHEN u.MW > 0 THEN (ISNULL(CurrTotNonOHCost,0) + ISNULL(PrevTotNonOHCost,0))/2*1000/u.MW END,
		AnnNonOHCostGJ = CASE WHEN u.GJ > 0 THEN (ISNULL(CurrTotNonOHCost,0) + ISNULL(PrevTotNonOHCost,0))/2*1000/u.GJ END,
		AnnNonOHCostMBTU = CASE WHEN u.MBTU > 0 THEN (ISNULL(CurrTotNonOHCost,0) + ISNULL(PrevTotNonOHCost,0))/2*1000/u.MBTU END
	FROM NonOHMaint INNER JOIN @Units u ON u.Refnum = NonOHMaint.Refnum

	--    EquipGroupCalc;
Print 'Units'
	SELECT * FROM @UNITS

Print 'Insert MaintEquipCalc'
	INSERT INTO MaintEquipCalc (Refnum, EquipGroup, AnnOHCostKUS, AnnNonOHCostKUS, AnnMaintCostKUS)
	SELECT Refnum, EquipGroup, AnnOHCostKUS = SUM(ISNULL(AnnOHCostKUS,0)), AnnNonOHCostKUS = SUM(ISNULL(AnnNonOHCostKUS,0)), AnnMaintCostKUS = SUM(ISNULL(AnnOHCostKUS,0)+ISNULL(AnnNonOHCostKUS,0))
	FROM (
		SELECT Refnum, EquipGroup, AnnOHCostKUS = TotAnnOHCost, AnnNonOHCostKUS = CAST(NULL as real)
		FROM OHMaint oh INNER JOIN Component_LU lu ON lu.Component = oh.Component
		WHERE oh.Refnum IN (SELECT Refnum FROM @Units)
		UNION ALL --SW added ALL 11/15/13 because it was removing duplicate rows from the component totals
		SELECT Refnum, EquipGroup, AnnOHCostKUS = NULL, AnnNonOHCostKUS = AnnNonOHCost
		FROM NonOHMaint noh INNER JOIN Component_LU lu ON lu.Component = noh.Component
		WHERE noh.Refnum IN (SELECT Refnum FROM @Units)) x
	GROUP BY Refnum, EquipGroup

Print 'Update MaintEquipCalc'
	UPDATE MaintEquipCalc SET
		AnnOHCostMWH = CASE WHEN u.MWH > 0 THEN AnnOHCostKUS*1000/u.MWH END, 
		AnnNonOHCostMWH = CASE WHEN u.MWH > 0 THEN AnnNonOHCostKUS*1000/u.MWH END, 
		AnnMaintCostMWH = CASE WHEN u.MWH > 0 THEN AnnMaintCostKUS*1000/u.MWH END, 
		AnnOHCostMW = CASE WHEN u.MW > 0 THEN AnnOHCostKUS*1000/u.MW END, 
		AnnNonOHCostMW = CASE WHEN u.MW > 0 THEN AnnNonOHCostKUS*1000/u.MW END, 
		AnnMaintCostMW = CASE WHEN u.MW > 0 THEN AnnMaintCostKUS*1000/u.MW END,
		AnnOHCostGJ = CASE WHEN u.GJ > 0 THEN AnnOHCostKUS*1000/u.GJ END, 
		AnnNonOHCostGJ = CASE WHEN u.GJ > 0 THEN AnnNonOHCostKUS*1000/u.GJ END, 
		AnnMaintCostGJ = CASE WHEN u.GJ > 0 THEN AnnMaintCostKUS*1000/u.GJ END,
		AnnOHCostMBTU = CASE WHEN u.MBTU > 0 THEN AnnOHCostKUS*1000/u.MBTU END, 
		AnnNonOHCostMBTU = CASE WHEN u.MBTU > 0 THEN AnnNonOHCostKUS*1000/u.MBTU END, 
		AnnMaintCostMBTU = CASE WHEN u.MBTU > 0 THEN AnnMaintCostKUS*1000/u.MBTU END
	FROM MaintEquipCalc INNER JOIN @Units u ON u.Refnum = MaintEquipCalc.Refnum


	--    CalcLTSAAdj;
	Print 'Insert LTSA'
	SELECT Refnum, TurbineID, CTAvgPayment2Yr = CASE WHEN ISNULL(PaymentPrevKUS,0) = 0 THEN ISNULL(PaymentCurrKUS,0)
							WHEN ISNULL(PaymentCurrKUS,0) = 0 THEN ISNULL(PaymentPrevKUS,0)
							ELSE (PaymentCurrKUS + PaymentPrevKUS)/2 END
		, CTAnnContCost = CASE WHEN LengthYears > 0 THEN ISNULL(TotCostKUS,0)/LengthYears ELSE 0 END
		, LifeRemaining = CASE WHEN LengthYears > 0 THEN DATEDIFF(dd, DATEADD(yy, LengthYears, StartDate), CAST('12/31/'+CAST(@StudyYear as varchar(4)) as datetime))/365.25 ELSE 0 END
		, AvgTotRunHrs = CASE WHEN PrevTotRunHrs IS NULL THEN ISNULL(CurrTotRunHrs,0) ELSE (ISNULL(CurrTotRunHrs,0) + PrevTotRunHrs)/2 END
		, AvgEquivRunHrs = CASE WHEN PrevEquivRunHrs IS NULL THEN ISNULL(CurrEquivRunHrs,0) ELSE (ISNULL(CurrEquivRunHrs,0) + PrevEquivRunHrs)/2 END
		, AvgTotStarts = CASE WHEN PrevTotStarts IS NULL THEN ISNULL(CurrTotStarts,0) ELSE (ISNULL(CurrTotStarts,0) + PrevTotStarts)/2 END
		, AvgEquivStarts = CASE WHEN PrevEquivStarts IS NULL THEN ISNULL(CurrEquivStarts,0) ELSE (ISNULL(CurrEquivStarts,0) + PrevEquivStarts)/2 END
		, LTSAAdj = CAST(NULL as real)
	INTO #LTSA
	FROM LTSA WHERE Refnum IN (SELECT Refnum FROM @Units)

	Print 'Update LTSA 1'
	UPDATE #LTSA
	SET LifeRemaining = CASE WHEN LifeRemaining > 0 THEN LifeRemaining END,
		LTSAAdj = CASE WHEN CTAnnContCost = 0 THEN
					CASE WHEN CTAvgPayment2Yr > 0 THEN CTAvgPayment2Yr END
				WHEN ABS(1 - CTAvgPayment2Yr/CTAnnContCost) > 0.2 THEN CTAnnContCost
				ELSE CTAvgPayment2Yr END
	
	Print 'Update LTSA 2'
	UPDATE LTSA 
	SET LTSAAdj = ISNULL(x.LTSAAdj,0),
		AvgTotRunHrs = x.AvgTotRunHrs, AvgEquivRunHrs = x.AvgEquivRunHrs,
		AvgTotStarts = x.AvgTotStarts, AvgEquivStarts = x.AvgEquivStarts,
		LTSAAdjTotRunHr = CASE WHEN x.AvgTotRunHrs > 0 THEN 1000*x.LTSAAdj/x.AvgTotRunHrs END, 
		LTSAAdjEquivRunHr = CASE WHEN x.AvgEquivRunHrs > 0 THEN 1000*x.LTSAAdj/x.AvgEquivRunHrs END,
		LTSAAdjTotStart = CASE WHEN x.AvgTotStarts > 0 THEN 1000*x.LTSAAdj/x.AvgTotStarts END, 
		LTSAAdjEquivStart = CASE WHEN x.AvgEquivStarts > 0 THEN 1000*x.LTSAAdj/x.AvgEquivStarts END,
		LifeRemaining = x.LifeRemaining
	FROM LTSA INNER JOIN #LTSA x ON x.Refnum = LTSA.Refnum AND x.TurbineID = LTSA.TurbineID

	DROP TABLE #LTSA


	--    CalcMaintCostRun;
	Print 'Update TotStarts in CTGMaint 1'
	UPDATE CTGMaint
	SET TotStarts = ISNULL(HotStarts, 0) + ISNULL(ColdStarts, 0)
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND NOT (HotStarts IS NULL AND ColdStarts IS NULL)

	Print 'Update TotStarts in CTGMaint 2'
	UPDATE CTGMaint
	SET TotStarts = EquivStarts
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND HotStarts IS NULL AND ColdStarts IS NULL

	Print 'Update EquivStarts in CTGMaint 1'
	UPDATE CTGMaint
	SET EquivStarts = TotStarts
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND EquivStarts IS NULL

	Print 'Update EquivRunHrs in CTGMaint 1'
	UPDATE CTGMaint
	SET RunHrs = EquivRunHrs
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND RunHrs IS NULL

	Print 'Update EquivRunHrs in CTGMaint 2'
	UPDATE CTGMaint
	SET EquivRunHrs = RunHrs
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND EquivRunHrs IS NULL

	Print 'Update EquivRunHrs in CTGMaint 3'
	UPDATE OHMaint
	SET TotStarts = NULL, EquivStarts = NULL, TotRunHrs = NULL, EquivRunHrs = NULL
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	Print 'Update OHMaint 6'
	UPDATE OHMaint
	SET TotStarts = CASE DATEDIFF(dd, c.PrevInspDate, c.InspDate) WHEN 0 THEN 0 ELSE ISNULL(c.TotStarts, 0)*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25) END,
	EquivStarts = CASE DATEDIFF(dd, c.PrevInspDate, c.InspDate) WHEN 0 THEN 0 ELSE ISNULL(c.EquivStarts,0)*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25) END,
	TotRunHrs = CASE DATEDIFF(dd, c.PrevInspDate, c.InspDate) WHEN 0 THEN 0 ELSE ISNULL(c.RunHrs,0)*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25) END,
	EquivRunHrs = CASE DATEDIFF(dd, c.PrevInspDate, c.InspDate) WHEN 0 THEN 0 ELSE ISNULL(c.EquivRunHrs,0)*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25) END
	FROM OHMaint INNER JOIN CTGMaint c ON c.Refnum = OHMaint.Refnum
	AND c.TurbineID = OHMaint.TurbineID AND c.ProjectID = OHMaint.ProjectID
	WHERE OHMaint.Refnum IN (SELECT Refnum FROM @Units) AND OHMaint.Component = 'CTG-TURB'

	Print 'Update OHMaint  7'
	UPDATE OHMaint
	SET TotStarts = ISNULL(c.TotStarts, 0),
	EquivStarts = c.EquivStarts, TotRunHrs = c.RunHrs, EquivRunHrs = c.EquivRunHrs
	FROM OHMaint INNER JOIN CTGMaint c ON c.Refnum = OHMaint.Refnum AND c.TurbineID = OHMaint.TurbineID
	WHERE OHMaint.Refnum IN (SELECT Refnum FROM @Units) AND OHMaint.OHDate = c.InspDate AND OHMaint.PrevOHDate = c.PrevInspDate

	Print 'Update OHMaint  8'
	UPDATE OHMaint
	SET TotStarts = c.TotStarts, EquivStarts = c.EquivStarts, TotRunHrs = c.TotRunHrs, EquivRunHrs = c.EquivRunHrs
	FROM OHMaint INNER JOIN OHMaint c ON OHMaint.Refnum = c.Refnum and OHMaint.TurbineID = c.TurbineID
	AND c.Component = 'CTG-TURB' and c.ProjectID IN ('OVHL','HGPINSP')
	WHERE OHMaint.Refnum IN (SELECT Refnum FROM @Units)
	AND (NOT (OHMaint.Component = 'CTG-TURB' and OHMaint.ProjectID IN ('OVHL','HGPINSP')))
	AND (OHMaint.OHDate = c.OHDate AND OHMaint.PrevOHDate = c.PrevOHDate)

	Print 'Update OHMaint  9'
	UPDATE OHMaint
	SET TotStarts = c.TotStarts*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25),
	EquivStarts = c.EquivStarts*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25),
	TotRunHrs = c.RunHrs*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25),
	EquivRunHrs = c.EquivRunHrs*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25)
	FROM OHMaint INNER JOIN CTGMaint c ON c.Refnum = OHMaint.Refnum AND c.TurbineID = OHMaint.TurbineID
	WHERE OHMaint.Refnum IN (SELECT Refnum FROM @Units) AND c.ProjectID = 'OVHL'
	AND OHMaint.TotStarts = NULL AND OHMaint.EquivStarts = NULL
	AND OHMaint.TotRunHrs = NULL AND OHMaint.EquivRunHrs = NULL

	Print 'Update OHMaint  10'
	UPDATE OHMaint
	SET TotStarts = c.TotStarts*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25),
	EquivStarts = c.EquivStarts*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25),
	TotRunHrs = c.RunHrs*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25),
	EquivRunHrs = c.EquivRunHrs*OHMaint.IntervalYrs/(DATEDIFF(dd, c.PrevInspDate, c.InspDate)/365.25)
	FROM OHMaint INNER JOIN CTGMaint c ON c.Refnum = OHMaint.Refnum AND c.TurbineID = OHMaint.TurbineID
	WHERE OHMaint.Refnum IN (SELECT Refnum FROM @Units) AND c.ProjectID = 'HGPINSP'
	AND OHMaint.TotStarts = NULL AND OHMaint.EquivStarts = NULL
	AND OHMaint.TotRunHrs = NULL AND OHMaint.EquivRunHrs = NULL

	UPDATE OHMaint SET EquivRunHrs = TotRunHrs
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND (EquivRunHrs IS NULL OR EquivRunHrs = 0)
	UPDATE OHMaint SET EquivStarts = TotStarts
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND (EquivStarts IS NULL OR EquivStarts = 0)

	UPDATE OHMaint
	SET OHCostTotRunHr = CASE WHEN TotRunHrs > 0 THEN 1000*TotOHCost/TotRunHrs ELSE NULL END,
	OHCostEquivRunHr = CASE WHEN EquivRunHrs > 0 THEN 1000*TotOHCost/EquivRunHrs WHEN TotRunHrs > 0 THEN 1000*TotOHCost/TotRunHrs ELSE NULL END,
	OHCostTotStart = CASE WHEN TotStarts > 0 THEN 1000*TotOHCost/TotStarts ELSE NULL END,
	OHCostEquivStart = CASE WHEN EquivStarts > 0 THEN 1000*TotOHCost/EquivStarts WHEN TotStarts > 0 THEN 1000*TotOHCost/TotStarts ELSE NULL END
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	UPDATE NonOHMaint
	SET NonOHCostTotRunHr = CASE WHEN LTSA.AvgTotRunHrs = 0 THEN NULL ELSE 1000*AnnNonOHCost/LTSA.AvgTotRunHrs END,
	NonOHCostEquivRunHr = CASE WHEN LTSA.AvgEquivRunHrs = 0 THEN NULL ELSE 1000*AnnNonOHCost/LTSA.AvgEquivRunHrs END,
	NonOHCostTotStart = CASE WHEN LTSA.AvgTotStarts = 0 THEN NULL ELSE 1000*AnnNonOHCost/LTSA.AvgTotStarts END,
	NonOHCostEquivStart = CASE WHEN LTSA.AvgEquivStarts = 0 THEN NULL ELSE 1000*AnnNonOHCost/LTSA.AvgEquivStarts END
	FROM NonOHMaint INNER JOIN LTSA ON LTSA.Refnum = NonOHMaint.Refnum AND LTSA.TurbineID = NonOHMaint.TurbineID
	WHERE NonOHMaint.Refnum IN (SELECT Refnum FROM @Units)

	/* Non-overhaul and overhaul cost per run-hour for the site
	  (based on 2-year average service hrs) */
	UPDATE NonOHMaint
	SET NonOHCostTotRunHr = CASE WHEN ISNULL(nf.ServiceHrs2YrAvg,0) = 0 THEN NULL ELSE 1000*AnnNonOHCost/ServiceHrs2YrAvg END,
	NonOHCostEquivRunHr = CASE WHEN ISNULL(nf.ServiceHrs2YrAvg,0) = 0 THEN NULL ELSE 1000*AnnNonOHCost/ServiceHrs2YrAvg END
	FROM NonOHMaint INNER JOIN NERCFactors nf ON nf.Refnum = NonOHMaint.Refnum
	WHERE NonOHMaint.Refnum IN (SELECT Refnum FROM @Units)
	AND NOT EXISTS (SELECT * FROM LTSA WHERE LTSA.Refnum = NonOHMaint.Refnum AND LTSA.TurbineID = NonOHMaint.TurbineID AND LTSA.AvgTotRunHrs>0)

	UPDATE OHMaint
	SET TotRunHrs = ServiceHrs2YrAvg * IntervalYrs,
	EquivRunHrs = ServiceHrs2YrAvg * IntervalYrs,
	OHCostTotRunHr = CASE WHEN ISNULL(nf.ServiceHrs2YrAvg,0) = 0 THEN NULL ELSE 1000*TotAnnOHCost/ServiceHrs2YrAvg END,
	OHCostEquivRunHr = CASE WHEN ISNULL(nf.ServiceHrs2YrAvg,0) = 0 THEN NULL ELSE 1000*TotAnnOHCost/ServiceHrs2YrAvg END
	FROM OHMaint INNER JOIN NERCFactors nf ON nf.Refnum = OHMaint.Refnum
	WHERE OHMaint.Refnum IN (SELECT Refnum FROM @Units)
	AND NOT EXISTS (SELECT * FROM LTSA WHERE LTSA.Refnum = OHMaint.Refnum AND LTSA.TurbineID = OHMaint.TurbineID AND LTSA.AvgTotRunHrs>0)


	--    MaintTotCalc;
	-- SW 4/17/13 added fields for GJ and OpHrs
	INSERT INTO MaintTotCalc (Refnum, CurrNonOHCost, PrevNonOHCost,
		CurrOHCost, PrevOHCost, AvgMWH2Yr,
		AnnNonOHCost, AnnNonOHCostMWH, AnnNonOHCostMW, AnnNonOHCostGJ, AnnNonOHCostOpHrs, AnnNonOHCostMBTU,
		AnnOHCost, AnnOHCostMWH, AnnOHCostMW, AnnOHCostGJ, AnnOHCostOpHrs, AnnOHCostMBTU,
		AnnOHProjCost, AnnOHProjCostMWH, AnnOHProjCostMW, AnnOHProjCostGJ, AnnOHProjCostOpHrs, AnnOHProjCostMBTU,
		AnnOHExclProj, AnnOHExclProjMWH, AnnOHExclProjMW,
		AnnLTSACost, AnnLTSACostMWH, AnnLTSACostMW, AnnLTSACostGJ, AnnLTSACostOpHrs, AnnLTSACostMBTU,
		AnnMaintCost, AnnMaintCostMWH, AnnMaintCostMW, AnnMaintCostGJ, AnnMaintCostOpHrs, AnnMaintCostMBTU,
		AnnMaintExclProj, AnnMaintExclProjMWH, AnnMaintExclProjMW)
	SELECT Refnum, CurrNonOHCost, PrevNonOHCost,
		CurrOHCost, PrevOHCost, MWH,
		AnnNonOHCost, AnnNonOHCost*1000/MWH, AnnNonOHCost*1000/MW, AnnNonOHCost*1000/GJ, AnnNonOHCost*1000/OpHrs, AnnNonOHCost*1000/MBTU,
		AnnOHCost, AnnOHCost*1000/MWH, AnnOHCost*1000/MW, AnnOHCost*1000/GJ, AnnOHCost*1000/OpHrs, AnnOHCost*1000/MBTU,
		AnnOHProjCost, AnnOHProjCost*1000/MWH, AnnOHProjCost*1000/MW, AnnOHProjCost*1000/GJ, AnnOHProjCost*1000/OpHrs, AnnOHProjCost*1000/MBTU,
		ISNULL(AnnOHCost,0) - ISNULL(AnnOHProjCost,0), (ISNULL(AnnOHCost,0) - ISNULL(AnnOHProjCost,0))*1000/MWH, (ISNULL(AnnOHCost,0) - ISNULL(AnnOHProjCost,0))*1000/MW,
		LTSAAdj, LTSAAdj*1000/MWH, LTSAAdj*1000/MW, LTSAAdj*1000/GJ, LTSAAdj*1000/OpHrs, LTSAAdj*1000/MBTU,
		ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0), (ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0))*1000/MWH, (ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0))*1000/MW, (ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0))*1000/GJ, (ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0))*1000/OpHrs, (ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0))*1000/MBTU, --SW 4/18/13 added GJ, changed the second MWH to MW (I think this is correct), SW 8/2/13 added MBTU
		ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0) - ISNULL(AnnOHProjCost,0), (ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0) - ISNULL(AnnOHProjCost,0))*1000/MWH, (ISNULL(AnnNonOHCost, 0) + ISNULL(AnnOHCost,0) + ISNULL(LTSAAdj,0) - ISNULL(AnnOHProjCost,0))*1000/MW
	FROM (
		SELECT Refnum, MWH = CASE WHEN MWH > 0 THEN MWH END, MW = CASE WHEN MW > 0 THEN MW END, GJ = CASE WHEN GJ > 0 THEN GJ END, OpHrs = CASE WHEN OpHrs > 0 THEN OpHrs END, MBTU = CASE WHEN MBTU > 0 THEN MBTU END
			, CurrNonOHCost = (SELECT SUM(CurrTotNonOHCost) FROM NonOHMaint WHERE NonOHMaint.Refnum = u.Refnum)
			, PrevNonOHCost = (SELECT SUM(PrevTotNonOHCost) FROM NonOHMaint WHERE NonOHMaint.Refnum = u.Refnum)
			, AnnNonOHCost = (SELECT SUM(ISNULL(CurrTotNonOHCost,0)+ISNULL(PrevTotNonOHCost,0))/2 FROM NonOHMaint WHERE NonOHMaint.Refnum = u.Refnum)
			, CurrOHCost = (SELECT SUM(TotOHCost) FROM OHMaint WHERE OHMaint.Refnum = u.Refnum AND DATEPART(yy, OHDate) = @StudyYear)
			, PrevOHCost = (SELECT SUM(TotOHCost) FROM OHMaint WHERE OHMaint.Refnum = u.Refnum AND DATEPART(yy, OHDate) = @StudyYear-1)
			, AnnOHCost = (SELECT SUM(TotAnnOHCost) FROM OHMaint WHERE OHMaint.Refnum = u.Refnum)
			, AnnOHProjCost = (SELECT SUM(TotAnnOHCost) FROM OHMaint WHERE OHMaint.Refnum = u.Refnum AND ProjectID <> 'OVHL')
			, LTSAAdj = (SELECT SUM(LTSAAdj) FROM LTSA WHERE LTSA.Refnum = u.Refnum)
		FROM @Units u) x

	-- Maintenance by Equipment category
	set ansi_warnings off --we do this just because we know this next query is going to have NULLs and we don't want the warning
	INSERT INTO MaintEquipSum (Refnum, CPHM, BOIL, HRSG, TURB, CA, VC, GEN, CTG,
		BAG, PREC, WGS, DGS, FHF, CWF, ASH, SCR, BOILH2O, WASTEH2O, Other)
	SELECT Refnum,
	CPHM = AVG(CASE WHEN EquipGroup = 'CPHM' THEN AnnMaintCostKUS ELSE NULL END),
	BOIL = AVG(CASE WHEN EquipGroup = 'BOIL' THEN AnnMaintCostKUS ELSE NULL END), 
	HRSG = AVG(CASE WHEN EquipGroup = 'HRSG' THEN AnnMaintCostKUS ELSE NULL END),
	TURB = AVG(CASE WHEN EquipGroup = 'TURB' THEN AnnMaintCostKUS ELSE NULL END),
	CA = AVG(CASE WHEN EquipGroup = 'CA' THEN AnnMaintCostKUS ELSE NULL END),
	VC = AVG(CASE WHEN EquipGroup = 'VC' THEN AnnMaintCostKUS ELSE NULL END),
	GEN = AVG(CASE WHEN EquipGroup = 'GEN' THEN AnnMaintCostKUS ELSE NULL END),
	CTG = AVG(CASE WHEN EquipGroup = 'CTG' THEN AnnMaintCostKUS ELSE NULL END),
	BAG = AVG(CASE WHEN EquipGroup = 'BAG' THEN AnnMaintCostKUS ELSE NULL END),
	PREC = AVG(CASE WHEN EquipGroup = 'PREC' THEN AnnMaintCostKUS ELSE NULL END),
	WGS = AVG(CASE WHEN EquipGroup = 'WGS' THEN AnnMaintCostKUS ELSE NULL END),
	DGS = AVG(CASE WHEN EquipGroup = 'DGS' THEN AnnMaintCostKUS ELSE NULL END),
	FHF = AVG(CASE WHEN EquipGroup = 'FHF' THEN AnnMaintCostKUS ELSE NULL END),
	CWF = AVG(CASE WHEN EquipGroup = 'CWF' THEN AnnMaintCostKUS ELSE NULL END),
	ASH = AVG(CASE WHEN EquipGroup = 'ASH' THEN AnnMaintCostKUS ELSE NULL END),
	SCR = AVG(CASE WHEN EquipGroup = 'SCR' THEN AnnMaintCostKUS ELSE NULL END),
	BOILH2O = AVG(CASE WHEN EquipGroup = 'BOILH2O' THEN AnnMaintCostKUS ELSE NULL END),
	WASTEH2O = AVG(CASE WHEN EquipGroup = 'WASTEH2O' THEN AnnMaintCostKUS ELSE NULL END),
	OTHER = AVG(CASE WHEN EquipGroup = 'OTHER' THEN AnnMaintCostKUS ELSE NULL END)
	FROM MaintEquipCalc
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
	GROUP BY Refnum
	set ansi_warnings on --turn the warnings back on

	UPDATE MaintEquipSum
	SET MajorBoiler = ISNULL((SELECT SUM(AnnMaintCostKUS) FROM MaintIndexDetails WHERE MaintIndexDetails.Refnum = MaintEquipSum.Refnum AND EquipGroup2 = 'BOILER'), 0),
		MajorTurbine = ISNULL((SELECT SUM(AnnMaintCostKUS) FROM MaintIndexDetails WHERE MaintIndexDetails.Refnum = MaintEquipSum.Refnum AND EquipGroup2 = 'TURBINE'), 0),
		MajorCTG = ISNULL((SELECT SUM(AnnMaintCostKUS) FROM MaintIndexDetails WHERE MaintIndexDetails.Refnum = MaintEquipSum.Refnum AND EquipGroup2 = 'CTG'), 0),
		MajorOther = ISNULL((SELECT SUM(AnnMaintCostKUS) FROM MaintIndexDetails WHERE MaintIndexDetails.Refnum = MaintEquipSum.Refnum AND EquipGroup2 = 'OTHER'), 0)
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	--    PersAdj;
	DELETE FROM Pers 
	WHERE Refnum IN (SELECT Refnum FROM @Units) 
	AND PersCat IN ('OCC-OHAdj', 'MPS-OHAdj', 'OCC-LSCAdj', 'MPS-LSCAdj', 'OCC-LTSA', 'MPS-LTSA')

	INSERT INTO Pers (Refnum, PersCat, SortKey, SectionID, 
		SiteSTH, SiteOVTHrs, CentralSTH, CentralOVTHrs,
		AGOnSite, Contract)
	SELECT Refnum, 'OCC-OHAdj', 30, 'OCCM',  
		SUM(AnnSiteOCCSTH), SUM(AnnSiteOCCOVT), 
		SUM(AnnCentralOCCSTH), SUM(AnnCentralOCCOVT),
		SUM(AnnAGOCC), 
		SUM(ISNULL(AnnContractOCC,0)) + SUM(ISNULL(AnnLumpSumHrs,0)*CASE WHEN Component IN ('CTG-TURB', 'CTG-COMB') THEN @CTGOCCPcnt ELSE @OHLumpSumOCCPcnt END)
	FROM OHMaint WHERE Refnum IN (SELECT Refnum FROM @Units)
	GROUP BY Refnum

	INSERT INTO Pers (Refnum, PersCat, SortKey, SectionID, 
		SiteSTH, SiteOVTHrs, CentralSTH, CentralOVTHrs,
		AGOnSite, Contract)
	SELECT Refnum, 'MPS-OHAdj', 110, 'MPSM',  
		SUM(AnnSiteMPSSTH), SUM(AnnSiteMPSOVT),
		SUM(AnnCentralMPSSTH), SUM(AnnCentralMPSOVT),
		SUM(AnnAGMPS),
		SUM(ISNULL(AnnContractMPS,0)) + SUM(ISNULL(AnnLumpSumHrs,0)*CASE WHEN Component IN ('CTG-TURB', 'CTG-COMB') THEN @CTGMPSPcnt ELSE @OHLumpSumMPSPcnt END)
	FROM OHMaint WHERE Refnum IN (SELECT Refnum FROM @Units)
	GROUP BY Refnum

	/* Calculate personnel for Lump sum contracts by dividing 
		Contract Maintenance Lump Sum expenses from Opex by Contractor rate from YearlyFactors.
		Rate is determined by the existence of a CTG */
	SELECT Refnum, LSHours = ContMaintLumpSum*1000/CASE WHEN EXISTS (SELECT * FROM Equipment e WHERE e.Refnum = Opex.Refnum AND e.EquipType = 'CTG') THEN @CCNonOHLumpSumRate ELSE @ContractorRate END
	INTO #LSContHrs
	FROM OpEx WHERE Refnum IN (SELECT Refnum FROM @Units)

	INSERT INTO Pers (Refnum, PersCat, SortKey, SectionID, Contract)
	SELECT u.Refnum, 'OCC-LSCAdj', 180, 'OCCM', ISNULL(ls.LSHours,0) * @ContOCCPcnt
	FROM @Units u LEFT JOIN #LSContHrs ls ON ls.Refnum = u.Refnum
	INSERT INTO Pers (Refnum, PersCat, SortKey, SectionID, Contract)
	SELECT u.Refnum, 'MPS-LSCAdj', 170, 'MPSM', ISNULL(ls.LSHours,0) * @ContMPSPcnt
	FROM @Units u LEFT JOIN #LSContHrs ls ON ls.Refnum = u.Refnum

	DROP TABLE #LSContHrs

	/* LTSA Personnel adjustment */
	DECLARE @LTSAContLaborRate real
	SELECT @LTSAContLaborRate = ISNULL(@CTGContractorRate, 300)/3.0 /* Changed 5/28/04 - Labor should be 1/3, materials 2/3, default rate was 225 */

	--SW 9/11/15 the following adjusted for 2014 study to automatically calculate a value, since we decided that if they have an LTSA then they will have those parts/labor/matl (~2% of units said N)
	BEGIN
		SELECT Refnum, TotLTSA = SUM(LTSAAdj),
			LTSALabor = SUM(LTSAAdj * 
				CASE WHEN @StudyYear >= 2014 THEN 0.33 
				ELSE CASE WHEN ISNULL(CombSystemLabor,'N') = 'Y' OR ISNULL(CombVBBLabor,'N') = 'Y' THEN 0.33 ELSE 0 END 
				END),
			LTSAParts = SUM(LTSAAdj * 
				CASE WHEN @StudyYear >= 2014 THEN 0.67 
				ELSE (CASE WHEN ISNULL(CombSystemParts,'N') = 'Y' OR ISNULL(CombVBBParts,'N') = 'Y' THEN 0.62 ELSE 0 END + CASE WHEN ISNULL(CombVBBMatl,'N') = 'Y' THEN 0.05 ELSE 0 END) 
				END),
			AdjLTSALabor = CAST(0 as real), LTSAHrs = CAST(NULL as real)
		INTO #LTSAAdj
		FROM LTSA WHERE Refnum IN (SELECT Refnum FROM @Units) AND LTSAAdj > 0
		GROUP BY Refnum
	END


	UPDATE #LTSAAdj
	SET AdjLTSALabor = TotLTSA * ISNULL(LTSALabor,0)/(ISNULL(LTSALabor,0)+ISNULL(LTSAParts,0))
	WHERE ISNULL(LTSALabor,0)+ISNULL(LTSAParts,0) > 0

	UPDATE #LTSAAdj SET LTSAHrs = AdjLTSALabor*1000/@LTSAContLaborRate

	INSERT INTO Pers (Refnum, PersCat, SortKey, SectionID, Contract)
	SELECT u.Refnum, 'OCC-LTSA', 26, 'OCCM', ISNULL(l.LTSAHrs,0)*@ContOCCPcnt
	FROM @Units u LEFT JOIN #LTSAAdj l ON l.Refnum = u.Refnum

	INSERT INTO Pers (Refnum, PersCat, SortKey, SectionID, Contract)
	SELECT u.Refnum, 'MPS-LTSA', 126, 'MPSM', ISNULL(l.LTSAHrs,0)*@ContMPSPcnt
	FROM @Units u LEFT JOIN #LTSAAdj l ON l.Refnum = u.Refnum

	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcMaint'
	
	EXEC sp_InsertMessage @siteid, 35, 'CalcMaint', 'CalcMaint'

END TRY
BEGIN CATCH

	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcMaint'

	EXEC sp_InsertMessage @siteid, 36, 'CalcMaint', 'CalcMaint'

END CATCH