﻿CREATE PROCEDURE spDDLookups;4
	@LookupID DD_ID,
	@ObjName sysname ,
	@ColName sysname ,
	@DescField sysname ,
	@Desc varchar(100) = NULL
AS
IF NOT EXISTS (SELECT * FROM DD_Lookups 
	WHERE LookupID = @LookupID)
	RETURN -101
ELSE
	UPDATE DD_Lookups
	SET ObjectName = @ObjName, ColumnName = @ColName, 
	DescField = @DescField, Description = @Desc
	WHERE LookupID = @LookupID
