﻿CREATE PROCEDURE sp_InsertMessage @Refnum Refnum, @MsgID int,
 @Source varchar(12), @P1 varchar(255) = '', @P2 varchar(255) = '',
 @P3 varchar(255) = '', @P4 varchar(255) = ''
AS
DECLARE @MsgStr varchar(255), @Severity char(1), @SysAdmin bit,
 @Consultant bit, @Pricing bit, @Audience4 bit, @Audience5 bit,
 @Audience6 bit, @Audience7 bit
SELECT @MsgStr = MessageText, @Severity = Severity, @SysAdmin = SysAdmin,
	@Consultant = Consultant, @Pricing = Pricing, @Audience4 = Audience4,
	@Audience5 = Audience5, @Audience6 = Audience6, @Audience7 = Audience7
FROM Message_LU
WHERE MessageID = @MsgID
if CHARINDEX('@P1', @MsgStr) > 0
	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P1', @MsgStr), 3, @P1)
if CHARINDEX('@P2', @MsgStr) > 0
	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P2', @MsgStr), 3, @P2)
if CHARINDEX('@P3', @MsgStr) > 0
	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P3', @MsgStr), 3, @P3)
if CHARINDEX('@P4', @MsgStr) > 0
	SELECT @MsgStr = STUFF(@MsgStr, CHARINDEX('@P4', @MsgStr), 3, @P4)
INSERT INTO MessageLog (Refnum, MessageID, Severity, Source, MessageText,
	SysAdmin, Consultant, Pricing, Audience4, Audience5,
	Audience6, Audience7)
VALUES (@Refnum, @MsgID, @Severity, @Source, @MsgStr, @SysAdmin,
	@Consultant, @Pricing, @Audience4, @Audience5, @Audience6,
	@Audience7)
