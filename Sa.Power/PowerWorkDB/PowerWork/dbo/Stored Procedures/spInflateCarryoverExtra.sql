﻿CREATE PROCEDURE [dbo].[spInflateCarryoverExtra](@SiteID varchar(10), @InflFactor real)
AS

	--SW 10/23/12 - commented out this code, because it already exists in spInflateCarryover, and we don't want
	--to run the risk of running it twice and doing a double update
	--and commented rather than deleting the stored proc in case it is called elsewhere
	
	--UPDATE OpexLocal
	--SET PropTax=PropTax*@InflFactor,
	--OthTax=OthTax*@InflFactor,
	--Insurance=Insurance*@InflFactor,
	--OthVar=OthVar*@InflFactor,
	--AGNonPers=AGNonPers*@InflFactor,
	--Water=Water*@InflFactor
	--WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	--UPDATE LTSA
	--SET PaymentCurrKLocal = PaymentCurrKLocal*@InflFactor,
	--PaymentPrevKLocal = PaymentPrevKLocal*@InflFactor,
	--TotCostKLocal = TotCostKLocal*@InflFactor
	--WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

