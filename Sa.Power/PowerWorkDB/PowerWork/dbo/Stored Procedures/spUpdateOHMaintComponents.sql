﻿
-- =============================================
-- Author:		Steve West
-- Create date: 06/24/2016
-- Description:	Updates OHMaint Component and ProjectID fields based on what was input
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateOHMaintComponents] 
	@refnum varchar(12)
AS
BEGIN
	SET NOCOUNT ON
	SET @refnum = rtrim(@refnum)

	DECLARE @TurbineID CHAR(6)
	DECLARE @ID INT
	DECLARE @EquipID INT
	DECLARE @OldEquipID INT
	DECLARE @Component CHAR(8)
	DECLARE @ProjectID CHAR(15)
	DECLARE @ComponentText VARCHAR(250)
	DECLARE @ProjectIDText VARCHAR(250)
	DECLARE @RorB CHAR(1)
	DECLARE @EquipType CHAR(8)
	DECLARE @OldEquipType CHAR(8)
	DECLARE @EquipName VARCHAR(250)
	DECLARE @NewComponentText VARCHAR(250)


	--go through the Equipment table
	DECLARE equipment CURSOR FOR 
		SELECT TurbineID, EquipID, EquipType, EquipName FROM Equipment
		WHERE Refnum = @refnum

	OPEN equipment
	FETCH NEXT FROM equipment INTO @TurbineID, @OldEquipID, @OldEquipType, @EquipName

	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SELECT @EquipID = EquipID, @EquipType = EquipType FROM Equipment_LU WHERE EquipName = @EquipName

		IF @EquipID IS NOT NULL AND @EquipType IS NOT NULL
		BEGIN
			UPDATE Equipment SET EquipID = @EquipID, EquipType = @EquipType
			WHERE Refnum = @refnum AND TurbineID = @TurbineID AND EquipID = @OldEquipID AND EquipType = @OldEquipType AND EquipName = @EquipName
		END

		SET @EquipID = NULL
		SET @EquipType = NULL

		FETCH NEXT FROM equipment INTO @TurbineID, @OldEquipID, @OldEquipType, @EquipName
	END
	CLOSE equipment
	DEALLOCATE equipment


	--go through the NonOHMaint table
	DECLARE nonoh CURSOR FOR 
		SELECT TurbineID, EquipID, Component, ComponentDesc FROM NonOHMaint
		WHERE Refnum = @refnum

	OPEN nonoh
	FETCH NEXT FROM nonoh INTO @TurbineID, @OldEquipID, @OldEquipType, @ComponentText --just using OldEquipType as a field here

	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SET @NewComponentText = @ComponentText
		IF @NewComponentText LIKE '%([0-9])'
		BEGIN
			SET @NewComponentText = LEFT(@NewComponentText, LEN(RTRIM(@NewComponentText)) - 3)
		END
		ELSE
		IF @NewComponentText LIKE '%([0-9][0-9])'
		BEGIN
			SET @NewComponentText = LEFT(@NewComponentText, LEN(RTRIM(@NewComponentText)) - 4)
		END

		--print @NewComponentText

		--cleanups; needs to be a better way to do this

		--type specific components
		IF @refnum NOT LIKE '%CC%' OR (@refnum LIKE '%CC%' AND @TurbineID = 'Site') --this is an STG tab
		BEGIN
			IF @NewComponentText = 'Generator'
				SET @NewComponentText = 'Steam Turbine Generator'

		END
		ELSE -- this is a CTG tab
		BEGIN
			IF @NewComponentText = 'Generator'
				SET @NewComponentText = 'CTG Generator'
			IF @NewComponentText = 'Transformer'
				SET @NewComponentText = 'CTG Transformer'
			IF @NewComponentText = 'Selective Catalytic Reduction (SCR) System'
				SET @NewComponentText = 'CTG SCR'
			IF @NewComponentText = 'Other'
				SET @NewComponentText = 'CTG Other'

		END

		SELECT @EquipID = EquipID, @EquipType = EquipType FROM Equipment_LU WHERE EquipName = @NewComponentText
		--print @EquipID
		--print @EquipType

		IF @EquipID IS NOT NULL AND @EquipType IS NOT NULL
		BEGIN
			UPDATE NonOHMaint SET EquipID = @EquipID, Component = @EquipType
			WHERE Refnum = @refnum AND TurbineID = @TurbineID AND EquipID = @OldEquipID AND Component = @OldEquipType AND ComponentDesc = @ComponentText
		END

		SET @EquipID = NULL
		SET @EquipType = NULL

		FETCH NEXT FROM nonoh INTO @TurbineID, @OldEquipID, @OldEquipType, @ComponentText
	END
	CLOSE nonoh
	DEALLOCATE nonoh


	--go through the OHMaint table

	DELETE FROM OHMaint WHERE Refnum = @refnum 
		AND ComponentText IS NULL AND ProjectIDText IS NULL AND OHDate IS NULL and DirectCostLocal IS NULL 
		AND SiteOCCSTH IS NULL AND AdditionalInfo IS NULL --client put a blank row with the first column populated. Hopefully checking these fields is enough to make sure it really is blank

	DECLARE overhauls CURSOR FOR 
		SELECT TurbineID, ID,  ComponentText, ProjectIDText FROM OHMaint
		WHERE Refnum = @refnum

	OPEN overhauls
	FETCH NEXT FROM overhauls INTO @TurbineID, @ID, @ComponentText, @ProjectIDText

	WHILE @@FETCH_STATUS = 0 
	BEGIN

		--refnum and turbineid
		--look up different items depending on which tab it is coming from
		IF @refnum NOT LIKE '%CC%' OR (@refnum LIKE '%CC%' AND @TurbineID = 'Site') -- is a Rankine unit, or CC unit Site tab
		BEGIN
			set @RorB = 'R'
		END
		ELSE
		BEGIN
			-- is a CTG tab
			set @RorB = 'B'
		END

		SELECT @EquipID = EquipID, @Component = Component, @ProjectID = ProjectID FROM OHEquipment_LU 
		WHERE ComponentText = @ComponentText AND ProjectIDText = @ProjectIDText AND RankineOrBrayton = @RorB

		IF @EquipID IS NOT NULL AND @Component IS NOT NULL AND @ProjectID IS NOT NULL --has to actually find something
		BEGIN
			UPDATE OHMaint SET EquipID = @EquipID, Component = @Component, ProjectID = @ProjectID
			WHERE Refnum = @refnum AND TurbineID = @TurbineID AND ID = @ID AND ComponentText = @ComponentText AND ProjectIDText = @ProjectIDText
		END
		ELSE -- didn't find the appropriate values, set some defaults
		BEGIN
			--try it with just 'Other' as the project, this will see if it gets the equipment
			DECLARE @IDText CHAR(15)
			SET @IDText = 'Other'
			IF @ComponentText = 'Combustion Turbine' OR @ComponentText = 'Steam Turbine Generator'
				SET @IDText = 'Turbine Other' -- this is not strictly accurate but close enough for our purposes

			SELECT @EquipID = EquipID, @Component = Component, @ProjectID = ProjectID FROM OHEquipment_LU 
			WHERE ComponentText = @ComponentText AND ProjectIDText = @IDText AND RankineOrBrayton = @RorB

			IF @EquipID IS NOT NULL AND @Component IS NOT NULL AND @ProjectID IS NOT NULL --has to actually find something
			BEGIN
				UPDATE OHMaint SET EquipID = @EquipID, Component = @Component, ProjectID = @ProjectID
				WHERE Refnum = @refnum AND TurbineID = @TurbineID AND ID = @ID AND ComponentText = @ComponentText AND ProjectIDText = @ProjectIDText
			END
			ELSE -- and this is a catchall for everything else, just set it to other
			BEGIN
				
				SELECT @EquipID = EquipID, @Component = Component, @ProjectID = ProjectID FROM OHEquipment_LU 
				WHERE ComponentText = 'Other' AND ProjectIDText = 'Other' AND RankineOrBrayton = @RorB

				UPDATE OHMaint SET EquipID = @EquipID, Component = @Component, ProjectID = @ProjectID
				WHERE Refnum = @refnum AND TurbineID = @TurbineID AND ID = @ID AND ComponentText = @ComponentText AND ProjectIDText = @ProjectIDText

			END

		END

		--and reset
		SET @EquipID = NULL
		SET @Component = NULL
		SET @ProjectID = NULL



		FETCH NEXT FROM overhauls INTO @TurbineID, @ID, @ComponentText, @ProjectIDText
	END
	CLOSE overhauls
	DEALLOCATE overhauls
	
	
										
END

