﻿CREATE PROC [dbo].[spComponentEUFPlus] (@Refnum varchar(10))
AS
DECLARE @E_PH float
	-- Delete any records in ComponentEUF for this Refnum
	Delete from ComponentEUFPlus Where Refnum = @Refnum

	DECLARE @turbHrs TABLE (
		Refnum varchar(12) NOT NULL,
		TurbineID char(6) NOT NULL,
		UtilityUnitCode char(6) NOT NULL,
		UnitType varchar(50) NOT NULL,
		UnitShortName char(10) NOT NULL,
		EvntYear smallint NOT NULL,
		E_PH float NULL
	)
	INSERT @turbHrs (Refnum, TurbineID, UtilityUnitCode, UnitType, UnitShortName, EvntYear, E_PH)
	SELECT nt.Refnum, nt.TurbineID, nt.UtilityUnitCode, s.UnitType, s.UnitShortName, t.EvntYear, eh.E_PH
	FROM dbo.TSort t 
	INNER JOIN dbo.NERCTurbine nt on t.Refnum = nt.Refnum
	INNER JOIN GADSOS.GADSNG.Setup s on nt.UtilityUnitCode = s.UtilityUnitCode
	INNER JOIN GADSOS.GADSNG.EventHours eh ON eh.UnitShortName = s.UnitShortName AND (DatePart(yy, eh.TL_DateTime) =  t.EvntYear) AND (eh.Granularity = 'Yearly')
	WHERE t.Refnum = @Refnum AND eh.E_PH > 0
	
	SELECT @E_PH = SUM(E_PH) FROM @turbHrs
	
	DECLARE @CompEUFHrs TABLE (
		Refnum varchar(12) NOT NULL,
		--TurbineID char(6) NOT NULL,
		SAIMajorEquip varchar(40) NULL,
		SAIMinorEquip varchar(40) NULL,
		EquipGroup char(8) NULL,
		EquivMWhrs float NULL,
		EquivMWhrsEUOF float NULL,
		EquivMWhrsEPOF float NULL,
		E_PH float NULL
	)
	INSERT @CompEUFHrs (Refnum, SAIMajorEquip, SAIMinorEquip, EquipGroup, EquivMWhrs, EquivMWhrsEUOF, EquivMWhrsEPOF)
	SELECT t.Refnum, /*t.TurbineID,*/ cc.SAIMajorEquip, cc.SAIMinorEquip, lu.EquipGroup, SUM(e.EquivMWhrs),
		SUM(CASE WHEN e.EventType NOT LIKE 'P%' AND e.EventType NOT LIKE 'RS' THEN e.EquivMWhrs ELSE 0 END),
		SUM(CASE WHEN e.EventType LIKE 'P%' THEN e.EquivMWhrs ELSE 0 END)
	FROM @turbHrs t 
	INNER JOIN GADSOS.GADSNG.EventDetails e ON e.UnitShortName = t.UnitShortName AND (DatePart(yy, e.TL_DateTime) =  t.EvntYear) AND (e.Granularity = 'Yearly')
	INNER JOIN dbo.CauseCodes cc ON cc.Cause_Code = e.CauseCode
	INNER JOIN ComponentEUF_LU lu ON lu.UnitType = t.UnitType AND lu.SAIMajorEquip = cc.SAIMajorEquip AND lu.SAIMinorEquip = cc.SAIMinorEquip
	GROUP BY t.Refnum, cc.SAIMajorEquip, cc.SAIMinorEquip, lu.EquipGroup
	
	IF NOT EXISTS (SELECT * FROM @CompEUFHrs)
		RETURN 1
		
	-- Equipment listed on T3 but did not find events for it
	INSERT @CompEUFHrs (Refnum, EquipGroup, EquivMWhrs, EquivMWhrsEUOF, EquivMWhrsEPOF)
	SELECT DISTINCT mec.Refnum, mec.EquipGroup, 0, 0, 0
	FROM MaintEquipCalc mec 
	WHERE mec.Refnum = @Refnum AND mec.AnnMaintCostKUS > 0 AND NOT EXISTS (SELECT * FROM @CompEUFHrs c WHERE c.Refnum = mec.Refnum AND c.EquipGroup = mec.EquipGroup)
	-- Assign E_PH 
	UPDATE compEUF
	SET E_PH = @E_PH
	FROM @CompEUFHrs compEUF
-- Here for debugging
--SELECT * FROM @turbhrs
--SELECT * FROM @CompEUFHrs
--
	-- ALTER the new records in ComponentEUF for this Refnum
	INSERT INTO ComponentEUFPlus (Refnum, EquipGroup, EUF, EUOF, EPOF, E_PH)
	SELECT Refnum, EquipGroup, SUM(EquivMWhrs)/AVG(E_PH)*100, SUM(EquivMWhrsEUOF)/AVG(E_PH)*100, SUM(EquivMWhrsEPOF)/AVG(E_PH)*100, AVG(E_PH) AS E_PH
	FROM @CompEUFHrs WHERE E_PH > 0
	GROUP BY Refnum, EquipGroup



--------------------------------
--SW 8/18/14
-- the section below was added to populate the EUFTotals table, which is used to calculate various things in the study disk
DELETE FROM EUFTotals WHERE Refnum = @Refnum


INSERT EUFTotals (Refnum, BoilerEPOF, STGEPOF, CTGEPOF, PollutionEPOF, OtherEPOF, E_PH, CPHM_EUOF, BOIL_EUOF, TURB_EUOF, GEN_EUOF, VC_EUOF, 
	CA_EUOF, BAG_EUOF, PREC_EUOF, WGS_EUOF, DGS_EUOF, SCR_EUOF, CTTURB_EUOF, CTGEN_EUOF, CTTRANS_EUOF, CTHRSG_EUOF, CTOTH_EUOF, FHF_EUOF, 
	CWF_EUOF, ASH_EUOF, DQWS_EUOF, TRANS_EUOF, WASTEH2O_EUOF, OTHER_EUOF)
	SELECT Refnum
		,BoilerEPOF = SUM(CASE WHEN EquipGroup IN ('CPHM','BOIL') THEN EPOF ELSE 0 END)
		,STGEPOF = SUM(CASE WHEN EquipGroup IN ('TURB','GEN','VC','CA') THEN EPOF ELSE 0 END)
		,CTGEPOF = SUM(CASE WHEN EquipGroup IN ('CTTURB','CTGEN','CTTRANS','CTHRSG','CTOTH','CTCOMB','CTCOMP','CTSCR') THEN EPOF ELSE 0 END)
		,PollutionEPOF = SUM(CASE WHEN EquipGroup IN ('BAG','PREC','WGS','DGS','SCR') THEN EPOF ELSE 0 END)
		,Oth_EPOF = SUM(CASE WHEN EquipGroup IN ('FHF','CWF','ASH','DQWS','BOILH2O','TRANS','WASTEH2O','OTHER') THEN EPOF ELSE 0 END)
		,E_PH = MAX(E_PH)
		,CPHM_EUOF = SUM(CASE WHEN EquipGroup = 'CPHM' THEN EUOF ELSE 0 END)
		,BOIL_EUOF = SUM(CASE WHEN EquipGroup = 'BOIL' THEN EUOF ELSE 0 END)

		,TURB_EUOF = SUM(CASE WHEN EquipGroup = 'TURB' THEN EUOF ELSE 0 END)
		,GEN_EUOF = SUM(CASE WHEN EquipGroup = 'GEN' THEN EUOF ELSE 0 END)
		,VC_EUOF = SUM(CASE WHEN EquipGroup = 'VC' THEN EUOF ELSE 0 END)
		,CA_EUOF = SUM(CASE WHEN EquipGroup = 'CA' THEN EUOF ELSE 0 END)

		,BAG_EUOF = SUM(CASE WHEN EquipGroup = 'BAG' THEN EUOF ELSE 0 END)
		,PREC_EUOF = SUM(CASE WHEN EquipGroup = 'PREC' THEN EUOF ELSE 0 END)
		,WGS_EUOF = SUM(CASE WHEN EquipGroup = 'WGS' THEN EUOF ELSE 0 END)
		,DGS_EUOF = SUM(CASE WHEN EquipGroup = 'DGS' THEN EUOF ELSE 0 END)
		,SCR_EUOF = SUM(CASE WHEN EquipGroup = 'SCR' THEN EUOF ELSE 0 END)

		,CTTURB_EUOF = SUM(CASE WHEN EquipGroup = 'CTTURB' THEN EUOF ELSE 0 END)
		,CTGEN_EUOF = SUM(CASE WHEN EquipGroup = 'CTGEN' THEN EUOF ELSE 0 END)
		,CTTRANS_EUOF = SUM(CASE WHEN EquipGroup = 'CTTRANS' THEN EUOF ELSE 0 END)
		,CTHRSG_EUOF = SUM(CASE WHEN EquipGroup = 'CTHRSG' THEN EUOF ELSE 0 END)
		,CTOTH_EUOF = SUM(CASE WHEN EquipGroup IN ('CTOTH','CTCOMB','CTCOMP','CTSCR') THEN EUOF ELSE 0 END)
		
		,FHF_EUOF = SUM(CASE WHEN EquipGroup = 'FHF' THEN EUOF ELSE 0 END)
		,CWF_EUOF = SUM(CASE WHEN EquipGroup = 'CWF' THEN EUOF ELSE 0 END)
		,ASH_EUOF = SUM(CASE WHEN EquipGroup = 'ASH' THEN EUOF ELSE 0 END)
		,DQWS_EUOF = SUM(CASE WHEN EquipGroup IN ('DQWS','BOILH2O') THEN EUOF ELSE 0 END)
		,TRANS_EUOF = SUM(CASE WHEN EquipGroup = 'TRANS' THEN EUOF ELSE 0 END)
		,WASTEH2O_EUOF = SUM(CASE WHEN EquipGroup = 'WASTEH2O' THEN EUOF ELSE 0 END)
		,OTHER_EUOF = SUM(CASE WHEN EquipGroup = 'OTHER' THEN EUOF ELSE 0 END)

	FROM ComponentEUFPlus
	WHERE Refnum = @Refnum
	GROUP BY Refnum
