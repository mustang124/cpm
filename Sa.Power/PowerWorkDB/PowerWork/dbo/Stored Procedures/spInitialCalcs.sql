﻿CREATE PROC [dbo].[spInitialCalcs](@Refnum Refnum)
AS

UPDATE OHMaint SET Component = 'SITETRAN', ProjectID = CASE WHEN ID = 600110 THEN 'OVHL' ELSE 'OTHER' END
FROM OHMaint 
	INNER JOIN Equipment e ON e.Refnum = OHMaint.Refnum AND e.TurbineID = OHMaint.TurbineID AND e.EquipID = OHMaint.EquipID
WHERE OHMaint.Refnum = @Refnum AND e.EquipType = 'SITETRAN' AND OHMaint.Component = 'BOIL'


UPDATE OHMaint SET ProjectID = 'OTHER'
WHERE Refnum = @Refnum AND Component = 'SITETRAN' AND ProjectID = 'FWHEAT'


UPDATE CTGData SET FiringTemp = CTGSupplement.FiringTemp
FROM CTGData 
	INNER JOIN CTGSupplement ON CTGData.Refnum = CTGSupplement.Refnum AND CTGData.TurbineID = CTGSupplement.TurbineID
WHERE CTGData.FiringTemp IS NULL AND CTGData.Refnum = @Refnum


-- SW 6/10/13 cleaning up some CTGData naming conventions to improve ability to find records when grouping
-- this should be added to as time goes by and we see what clients do in these fields

UPDATE CTGData SET Manufacturer = 'ALSTOM', NameplateOEM = 'ABB' WHERE Manufacturer = 'ABB' AND Refnum = @Refnum --correct manuf for ABB and put ABB into Nameplate
UPDATE CTGData SET Manufacturer = 'ALSTOM' WHERE Manufacturer = 'ALSTOM' AND Refnum = @Refnum -- just capitalizes the manuf name
UPDATE CTGData SET Manufacturer = 'GENERAL ELECTRIC', NameplateOEM = 'GE' WHERE Manufacturer = 'GE' AND Refnum = @Refnum --correct GE name and move old name into Nameplate
UPDATE CTGData SET Manufacturer = 'ROLLS ROYCE', NameplateOEM = 'Rolls Royce' WHERE Manufacturer = 'ROLLS ROYCE' AND Refnum = @Refnum --capitalize and move name into Nameplate
UPDATE CTGData SET Manufacturer = 'SIEMENS' WHERE Manufacturer = 'SIEMENS' AND Refnum = @Refnum -- just capitalizes the manuf name

UPDATE CTGData SET FrameModel = 'GT26B 2.2' WHERE Manufacturer = 'ALSTOM' AND FrameModel = 'GT26B2.2' AND Refnum = @Refnum
UPDATE CTGData SET FrameModel = 'GT11N2' WHERE Manufacturer = 'ALSTOM' AND FrameModel = '11N2' AND Refnum = @Refnum
UPDATE CTGData SET FrameModel = 'GT11N2' WHERE Manufacturer = 'ALSTOM' AND FrameModel = '11N2 GAS TURBINE' AND Refnum = @Refnum
UPDATE CTGData SET FrameModel = '9FA+E' WHERE Manufacturer = 'GENERAL ELECTRIC' AND FrameModel = '9 FA+e' AND Refnum = @Refnum



UPDATE PowerGeneration SET NetMWH = ISNULL(GrossMWH, 0) - ISNULL(StationSvcElec, 0)
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE Refnum = @Refnum AND StudyYear >= 1999)


UPDATE PowerGeneration SET ElecMBTU = NetMWH * 3.41214
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE Refnum = @Refnum AND StudyYear >= 1999)

