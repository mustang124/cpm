﻿

-- =============================================
-- Author:		Steve West
-- Create date: 05/14/2014
-- Description:	Calculates EGC based ON Rick's new method
-- Based off spEGCCalcs2011, THEN modified to use the new calcs
-- =============================================
CREATE PROCEDURE [dbo].[spEGCCalcs2013] 
	@refnum varchar(12) = null,
	@year int = 0, 
	--CAUTION! This is the year it will be calced against. That means you can pass a potentially invalid unit to the stored proc.
	--for example, if you pass unit 123456P09 with year 2012, THEN that unit's EGC fields will be calced against 2012 coefficients, and put
	--into the database that way. You should really check this carefully.
	--(Why did I do it this way? To make it easier to test old units in the EGC spreadsheets from Rick)
	--Normally you you should pass the same @year value that is in the TSort.StudyYear field, and should only use a different year 
	--when you are running in DEBUG mode.
	--if you pass nothing the value in TSort.StudyYear will be used
	@DEBUG bit = 0
	--if you run with @debug set to 0 (the default) it will calc the unit and populate various tables
	--if you set @debug to 1 it will print messages and not write anything to tables

AS
BEGIN
	SET NOCOUNT ON
	SET @refnum = rtrim(@refnum)

	IF @year < 2013
		return

	-----------------------DECLARE all the variables used for the EGC totals-----------------------
	DECLARE @EGC_Total REAL
	DECLARE @EGC_Scrubber REAL
	DECLARE @EGC_PrecBag REAL
	DECLARE @EGC_SCR REAL
	DECLARE @EGC_Frequency50 REAL
	DECLARE @EGC_Frequency60 REAL
	DECLARE @EGC_Starts REAL
	DECLARE @EGC_SiteEffect REAL
	DECLARE @EGC_OtherSiteCap REAL
	DECLARE @EGC_SteamSales REAL
	DECLARE @EGC_SolidFuel REAL
	DECLARE @EGC_NatGasH2Gas REAL
	DECLARE @EGC_OffGas REAL
	DECLARE @EGC_DieselJet REAL
	DECLARE @EGC_HeavyFuel REAL
	DECLARE @EGC_Age REAL
	DECLARE @EGC_EquivBoiler REAL
	DECLARE @EGC_Supercritical REAL
	DECLARE @EGC_CTGGas REAL
	DECLARE @EGC_SiteFuel REAL
	DECLARE @EGC_ETech REAL
	DECLARE @EGC_FTech REAL
	DECLARE @EGC_AeroTech REAL
	DECLARE @EGC_NumSTs REAL
	DECLARE @EGC_NumGens REAL
	DECLARE @EGC_TotEquip REAL
	DECLARE @EGC_Subcritical REAL
	DECLARE @EGC_MWH REAL
	DECLARE @EGC_Taxes REAL
	DECLARE @EGC_POHours REAL
	DECLARE @EGC_ServiceHours REAL
	DECLARE @EGC_HRSGCap REAL
	DECLARE @EGC_HRSGCapN REAL
	DECLARE @EGC_CTGLiquidFuel REAL
	
	
	----------------DECLARE all the variables used to hold the unit data----------------------	
	DECLARE @Age REAL
	DECLARE @NDC REAL
	DECLARE @CTG_Count REAL
	DECLARE @CTG_NDC REAL
	DECLARE @NumUnitsAtSite REAL
	DECLARE @Site_NDC REAL
	DECLARE @Frequency REAL
	DECLARE @FuelType CHAR(8)
	DECLARE @ScrubbersYN REAL
	DECLARE @PrecBagYN REAL
	DECLARE @DesignEffPct REAL
	DECLARE @SCR REAL
	DECLARE @BlrPSIG REAL
	DECLARE @AdjNetMWH REAL
	DECLARE @AdjNetMWH2Yr REAL
	DECLARE @ServiceHrs REAL
	DECLARE @Starts REAL
	DECLARE @SteamSalesMBTU REAL
	DECLARE @DieselJet REAL
	DECLARE @HeavyFuel REAL
	DECLARE @NatGasMBTU REAL
	DECLARE @OffGasMBTU REAL
	DECLARE @Coal1Tons REAL
	DECLARE @Coal1AshPcnt REAL
	DECLARE @Coal1SulfurPcnt REAL
	DECLARE @Coal1MoisturePcnt REAL
	DECLARE @Coal2Tons REAL
	DECLARE @Coal2AshPcnt REAL
	DECLARE @Coal2SulfurPcnt REAL
	DECLARE @Coal2MoisturePcnt REAL
	DECLARE @Coal3Tons REAL
	DECLARE @Coal3AshPcnt REAL
	DECLARE @Coal3SulfurPcnt REAL
	DECLARE @Coal3MoisturePcnt REAL
	DECLARE @NumBoilers REAL
	DECLARE @BoilerSizeFactor REAL
	DECLARE @NumSTs REAL
	DECLARE @TotNumMajEqpt REAL
	DECLARE @Class CHAR(4)
	DECLARE @SingleShaft REAL
	DECLARE @CTGGasMBTU REAL
	DECLARE @CTGLiquidMBTU REAL
	DECLARE @SiteMBTU REAL
	DECLARE @Taxes REAL
	DECLARE @IsCE REAL
	DECLARE @IsCG REAL
	DECLARE @IsSC REAL
	DECLARE @IsESC REAL
	DECLARE @CTGAge REAL -- added 2/3/16 SW
	DECLARE @POHours REAL -- added 7/15/16 SW
	DECLARE @RefNumRegEx VarChar(14)
	
	SELECT @RefNumRegEx = CASE WHEN @refnum LIKE '%C[0-9][0-9]' THEN '%C[0-9][0-9]'
							   WHEN @refnum LIKE '%C[0-9][0-9]P' THEN '%C[0-9][0-9]P'
						  ELSE
							  '%CC%'	
						  END

	print @RefNumRegEx

	--------------------Initial Data Grab----------------------------
	
	SELECT --t.Refnum, t.CoLoc, 
		--@Age = MIN(nt_CTGAge.CTGAge, nf.Age), 
		@Age = CASE WHEN nt_CTGAge.CTGAge < nf.Age THEN nt_CTGAge.CTGAge ELSE Age END,
		@NDC = nf.NDC,
		@CTG_Count = CASE WHEN t.Refnum LIKE @RefNumRegEx THEN ISNULL(ISNULL(nt_CTG2.CTG_Count,nt_CTG.CTG_Count),1) ELSE 0 END, --theory is that this is set up as a single shaft unit, so 1 CTG if NULL
		@CTG_NDC = CASE WHEN t.refnum LIKE @RefNumRegEx THEN ROUND(ISNULL(nt_CTG.CTG_NDC, nf.NDC * 2/3),1) ELSE 0 END ,-- if it is a single shaft the CTG is producing 2/3 and the STG 1/3
		@NumUnitsAtSite = CASE WHEN t.Refnum LIKE @RefNumRegEx THEN ISNULL(p.CCUnitNum,0) + ISNULL(p.SCNum,0) + ISNULL(p.OthUnitNum,0) ELSE ISNULL(p.CoalUnitNum,0) + ISNULL(p.OilGasUnitNum,0) END,
		@Site_NDC = CASE WHEN t.Refnum LIKE @RefNumRegEx THEN ROUND(ISNULL(p.CCUnitCap,0) + ISNULL(p.SCCap,0) + ISNULL(p.OthUnitCap,0),1) ELSE ISNULL(p.CoalUnitCap,0) + ISNULL(p.OilGasUnitCap,0) END,
		@Frequency = clu.FrequencyHz,
		@FuelType = ftc.FuelType,
		@PrecBagYN = ISNULL(ps.PrecBagYN,0),
		@ScrubbersYN = ISNULL(ps.ScrubbersYN,0),
		@DesignEffPct = CASE ISNULL(ps.ScrubbersYN,0) WHEN 0 THEN 0 ELSE CASE ISNULL(ms.DesignEffPct,0) WHEN 0 THEN 94 ELSE ISNULL(ms.DesignEffPct,0) END END, --removes the zeroes, 94 decided by consultants as the default value after looking at data
		@SCR = CASE WHEN scrs.HasSCR IS NULL THEN ISNULL(pscc.SCR,0) ELSE CASE scrs.HasSCR WHEN 0 THEN 0 ELSE 1 END END,
		@BlrPSIG = ISNULL(ec.BoilerPressure, ISNULL(dd.BlrPSIG,0)),
		@ServiceHrs = nf.ServiceHrs,
		@AdjNetMWH = gtc.AdjNetMWH,
		@AdjNetMWH2Yr = gtc.AdjNetMWH2Yr,
		@Starts = CASE ISNULL(nf.ACTStarts,0) WHEN 0 THEN ISNULL(gtc.TotStarts,0) ELSE ISNULL(nf.ACTStarts,0) END,
		@SteamSalesMBTU = ISNULL(ss.StmSalesMBTU,0),
		@DieselJet = ISNULL(DieselMBTU,0) + ISNULL(JetTurbMBTU,0),
		@HeavyFuel = ISNULL(FuelOilMBTU,0),
		@NatGasMBTU = ISNULL(NatGasMBTU,0) + ISNULL(H2GasMBTU,0),
		@OffGasMBTU = ISNULL(OffGasMBTU,0),
		@Coal1Tons = ISNULL(c1.Tons, 0),
		@Coal1AshPcnt = ISNULL(c1.AshPcnt, 0),
		@Coal1SulfurPcnt = ISNULL(c1.SulfurPcnt, 0),
		@Coal1MoisturePcnt = ISNULL(c1.MoistPcnt,0),
		@Coal2Tons = ISNULL(c2.Tons, 0),
		@Coal2AshPcnt = ISNULL(c2.AshPcnt, 0),
		@Coal2SulfurPcnt = ISNULL(c2.SulfurPcnt, 0),
		@Coal2MoisturePcnt = ISNULL(c2.MoistPcnt,0),
		@Coal3Tons = ISNULL(c3.Tons, 0),
		@Coal3AshPcnt = ISNULL(c3.AshPcnt, 0),
		@Coal3SulfurPcnt = ISNULL(c3.SulfurPcnt, 0),
		@Coal3MoisturePcnt = ISNULL(c3.MoistPcnt, 0),
		@NumBoilers = ISNULL(ec.NumBoilers, ISNULL(blr.NumBoilers,0)),
		@BoilerSizeFactor = CASE 
								WHEN ISNULL(c.HeatValue,0) = 0 THEN 0
								WHEN ISNULL(c.HeatValue,0) < 8300 THEN 2.03 --magic numbers determined by Tony and Rick 5/14
								WHEN ISNULL(c.HeatValue,0) > 9500 THEN 1
								ELSE 1.21 -- 8300-9500
							END,
		@NumSTs = ISNULL(ec.NumSTGs, ISNULL(sts.NumSTs,0)),
		@TotNumMajEqpt = CASE WHEN t.Refnum LIKE @RefNumRegEx THEN 
				(ISNULL(ctgs.NumCTGs,0) * 2) + ISNULL(blr.NumBoilers,0) + (ISNULL(sts.NumSTs,0) * 2) + ISNULL(hrsgs.NumHRSGs,0) - ISNULL(t.SingleShaft,0)
			 ELSE ISNULL(ctgs.NumCTGs,0) + ISNULL(blr.NumBoilers,0) + (ISNULL(sts.NumSTs,0) * 2) END,

		@Class = ctgd.Class,
		@SingleShaft = ISNULL(t.SingleShaft,0), -- this needs to be fixed somehow
		@CTGGasMBTU = fuel.CTGGasMBTU,
		@CTGLiquidMBTU = fuel.CTGLiquidMBTU,
		@SiteMBTU = fuel.SiteMBTU,
		@Taxes = o.OthTax + o.PropTax, --2014 addition
		@IsCE = CASE WHEN b.CRVGroup = 'CE' THEN 1.19 ELSE 1.0 END, --2014 addition
		@IsCG = CASE WHEN b.CRVGroup = 'CG' THEN 1.35 ELSE 1.0 END, --2015 addition
		@IsSC = CASE WHEN b.CRVGroup = 'SC' THEN 1.20 ELSE 1.0 END, --2015 addition
		@IsESC = CASE WHEN b.CRVGroup = 'ESC' THEN 1.20 ELSE 1.0 END, --2015 addition
		@POHours = CASE WHEN PO.eventyear = t.evntyear THEN PO.POHours ELSE 0 END -- 2015 addition

	FROM TSort t
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, CTG_Count = COUNT(*), CTG_NDC = SUM(CASE WHEN TurbineType = 'CC' THEN (NDC * 2/3) ELSE NDC END) -- trying to handle blocks
					FROM NERCTurbine 
					WHERE TurbineType IN( 'CTG','CC') AND NDC IS NOT NULL 
					GROUP BY Refnum) nt_CTG ON nt_CTG.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, CTGAge = MAX(Age) FROM NERCTurbine WHERE TurbineType = 'CTG' GROUP BY Refnum) nt_CTGAge ON nt_CTGAge.Refnum = t.Refnum
						
		LEFT JOIN (SELECT Refnum, CTG_Count = COUNT(*) FROM NERCTurbine WHERE TurbineType = 'CTG' AND NDC IS NULL GROUP BY Refnum) nt_CTG2 ON nt_CTG2.Refnum = t.Refnum
		--this one is trying to handle the (very rare, perhaps only Capital Bridgeport?) unit that has a CC set up with an NDC, and CTGs set up but NULL, to get the correct CTG count
		LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
		LEFT JOIN PowerGlobal.dbo.Country_LU clu ON clu.Country = t.Country
		LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
		LEFT JOIN Misc ms ON ms.Refnum = t.Refnum AND ms.TurbineID = 'STG'
		LEFT JOIN CoalTotCalc c ON c.Refnum = t.Refnum
		LEFT JOIN (select Refnum, NumBoilers = SUM(NumBoilers), NumSTGs = SUM(NumSTGs), BoilerPressure = MAX(BoilerPressure) from EquipCount group by Refnum) ec ON ec.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, FiringTemp = MAX(FiringTemp), Class FROM CTGData GROUP BY Refnum, Class) ctgd ON ctgd.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, --SW 6/10/14 going back to looking at Equipment table, trusting consultants to check it is correct
						PrecBagYN = CASE SUM(CASE WHEN EquipType IN ('BAG','PREC') THEN 1 ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END,
						ScrubbersYN = CASE SUM(CASE WHEN EquipType IN ('WGS','DGS') THEN 1 ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END--,
						--SCR = CASE SUM(CASE WHEN EquipType IN ('SCR','CTG-SCR') THEN 1 ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END
					--FROM (SELECT DISTINCT Refnum, EquipType FROM Equipment WHERE EquipType IN ('BAG','CTG-SCR','DGS','PREC','SCR','WGS')) b GROUP BY Refnum) ps ON ps.Refnum = t.Refnum
					FROM (SELECT DISTINCT Refnum, EquipType FROM Equipment WHERE EquipType IN ('BAG','DGS','PREC','WGS')) b GROUP BY Refnum) ps ON ps.Refnum = t.Refnum

		--LEFT JOIN (SELECT Refnum, 
		--				PrecBagYN = CASE SUM(CASE WHEN Component IN ('PREC','BAG') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END,
		--				ScrubbersYN = CASE SUM(CASE WHEN Component IN ('WGS','DGS') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END,
		--				SCR = CASE SUM(CASE WHEN Component IN ('SCR') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END
		--			FROM (SELECT Refnum, Component, TotAnnOHCost = ISNULL(TotAnnOHCost,0) FROM OHMaint UNION SELECT Refnum, Component, AnnNonOHCost = ISNULL(AnnNonOHCost,0) FROM NonOHMaint UNION SELECT Refnum, 'WGS', ScrCostLime FROM Misc WHERE ScrCostLime IS NOT NULL) b -- the Misc part calcs the scrubber cost, set to 'WGS' just for it to count from
		--			WHERE Component IN ('BAG','PREC','WGS','DGS','SCR')
		--			GROUP BY Refnum) ps ON ps.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, 
						SCR = CASE SUM(CASE WHEN Component IN ('SCR','CTG-SCR') THEN totannohcost ELSE 0 END) WHEN 0 THEN 0 ELSE 1 END
					FROM (SELECT Refnum, Component, TotAnnOHCost = ISNULL(TotAnnOHCost,0) FROM OHMaint UNION SELECT Refnum, Component, AnnNonOHCost = ISNULL(AnnNonOHCost,0) FROM NonOHMaint) b
					WHERE Component IN ('CTG-SCR','SCR')
					GROUP BY Refnum) pscc ON pscc.Refnum = t.Refnum
					
		LEFT JOIN (SELECT nt.Refnum, BlrPSIG = MAX(ISNULL(d.BlrPSIG,0)) FROM NERCTurbine nt LEFT JOIN DesignData d ON d.UtilityUnitCode = nt.UtilityUnitCode GROUP BY nt.Refnum) dd ON dd.Refnum = t.Refnum
									
		LEFT JOIN (SELECT s.refnum, StmSalesMBTU = SUM(StmSalesMBTU), ABSStmSalesMBTU = SUM(ABS(StmSalesMBTU)) FROM SteamSales s LEFT JOIN TSort t ON t.Refnum = s.Refnum WHERE YEAR(s.Period) = t.StudyYear GROUP BY s.Refnum) ss ON ss.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, AshPcnt = MAX(ISNULL(AshPcnt,0)), SulfurPcnt = MAX(ISNULL(SulfurPcnt,0)), MoistPcnt = MAX(ISNULL(MoistPcnt,0)), Tons = SUM(ISNULL(Tons,0)), MBTU = SUM(ISNULL(MBTU,0)) FROM Coal WHERE CoalID = '1' GROUP BY Refnum) c1 ON c1.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, AshPcnt = MAX(ISNULL(AshPcnt,0)), SulfurPcnt = MAX(ISNULL(SulfurPcnt,0)), MoistPcnt = MAX(ISNULL(MoistPcnt,0)), Tons = SUM(ISNULL(Tons,0)), MBTU = SUM(ISNULL(MBTU,0)) FROM Coal WHERE CoalID = '2' GROUP BY Refnum) c2 ON c2.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, AshPcnt = MAX(ISNULL(AshPcnt,0)), SulfurPcnt = MAX(ISNULL(SulfurPcnt,0)), MoistPcnt = MAX(ISNULL(MoistPcnt,0)), Tons = SUM(ISNULL(Tons,0)), MBTU = SUM(ISNULL(MBTU,0)) FROM Coal WHERE CoalID = '3' GROUP BY Refnum) c3 ON c3.Refnum = t.Refnum

		LEFT JOIN (SELECT Refnum, NumBoilers = COUNT(*) FROM Equipment WHERE EquipType = 'BOIL' GROUP BY Refnum) blr ON blr.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, NumSTs = COUNT(*) FROM Equipment WHERE EquipType = 'STG' GROUP BY Refnum) sts ON sts.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, NumCTGs = COUNT(*) FROM Equipment WHERE EquipType = 'CTG-GEN' GROUP BY Refnum) ctgs ON ctgs.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, NumHRSGs = COUNT(*) FROM Equipment WHERE EquipType = 'HRSG' GROUP BY Refnum) hrsgs ON hrsgs.Refnum = t.Refnum


		--LEFT JOIN (SELECT Refnum, NumGens = COUNT(*) FROM Equipment WHERE EquipType = 'CTG-GEN' GROUP BY Refnum) ctgs ON ctgs.Refnum = t.Refnum
		LEFT JOIN (SELECT Refnum, HasSCR = COUNT(*) FROM Equipment WHERE EquipType = 'SCR' GROUP BY Refnum) scrs ON scrs.Refnum = t.Refnum --updated for new equipment lists in 2014 study
		
		LEFT JOIN (SELECT Refnum,
							SiteMBTU = SUM(CASE TurbineID WHEN 'Site' THEN MBTU ELSE 0 END),
							CTGLiquidMBTU = SUM(CASE TurbineID WHEN 'Site' THEN 0 ELSE CASE WHEN FuelType IN ('Diesel','Jet','FOil') THEN MBTU ELSE 0 END END),
							CTGGasMBTU = SUM(CASE TurbineID WHEN 'Site' THEN 0 ELSE CASE WHEN FuelType IN ('Diesel','Jet','FOil') THEN 0 ELSE MBTU END END)
					FROM Fuel
					GROUP BY Refnum) fuel ON fuel.Refnum = t.Refnum AND t.Refnum LIKE @RefNumRegEx

		LEFT JOIN (SELECT Refnum, PropTax = ISNULL(PropTax,0), OthTax = ISNULL(OthTax,0) FROM OpExCalc WHERE DataType = 'ADJ') o ON o.Refnum = t.Refnum

		LEFT JOIN (SELECT nt.Refnum, eventyear = YEAR(TL_DateTime), POHours = SUM(PO)/COUNT(*)
					FROM NERCTurbine nt
						LEFT JOIN GADSOS.GADSNG.Setup s ON s.UtilityUnitCode = nt.UtilityUnitCode
						LEFT JOIN GADSOS.GADSNG.EventHours e on e.UnitShortName = s.UnitShortName AND e.Granularity = 'Yearly'
					GROUP BY nt.Refnum, year(TL_DateTime)) PO ON PO.Refnum = t.Refnum and PO.eventyear = t.EvntYear

	WHERE t.Refnum = @refnum
	
	-- added 2/3/16 SW
	-- Tony's theory that a repowered unit should have a younger age
	-- this applies to CTGs that are younger than the overall unit
	-- for many (most?) units they will be the same or very similar
	-- for a few units, the CTG was repowered and the age may be very different
	-- we find the oldest CTG, compare it to the overall age of the unit, and change the overall age if it is younger
	--SELECT @CTGAge = MAX(Age) FROM NERCTurbine WHERE TurbineType = 'CTG' AND Refnum = @Refnum -- added 2/3/16 SW

	--IF @CTGAge IS NOT NULL -- added 2/3/16 SW
	--BEGIN
	--	IF @CTGAge < @Age
	--		SET @Age = @CTGAge
	--END

Print 'made it'
--print @Age
--print @NDC
--print rtrim(@Class)

	---------------------------DECLARE some more setup variables
	DECLARE @Model CHAR(12) = ''
	DECLARE @IsRankine BIT = 0
	DECLARE @RankineCoal BIT = 0
	DECLARE @RankineGas BIT = 0
	DECLARE @CC BIT = 0
	DECLARE @SC BIT = 0

	----------------------populate the setup variables
	--IF @refnum NOT LIKE '%CC%  SOME REFNUM ONLY HAS ONE C FOLLOWED BY yy OR yyP'
	IF @refnum NOT LIKE   @RefNumRegEx
	BEGIN 
	PRINT @Refnum
		SET @IsRankine = 1
		IF @FuelType = 'Coal'
		BEGIN
			SET @RankineCoal = 1
			SET @Model = 'RankineCoal'
		END
		IF @FuelType <> 'Coal'
		BEGIN
			SET @RankineGas = 1
			SET @Model = 'RankineGas'
		END
	END
	ELSE
	BEGIN
		SET @IsRankine = 0
		IF @NDC - @CTG_NDC < 1 -- should be the same, but slight fudge factor for rounding
		BEGIN
			SET @SC = 1
			SET @Model = 'SC'
		END
		ELSE
		BEGIN
			SET @CC = 1
			SET @Model = 'CC'
		END
	END

--print 'NDC ' PRINT @NDC
--print 'CTG ' PRINT @CTG_NDC
--print 'MODEL ' PRINT  @Model
--Print 'CC ' PRINT @CC

	--IF @refnum NOT LIKE '%CC%  SOME REFNUM ONLY HAS ONE C FOLLOWED BY yy OR yyP'
	IF (@refnum NOT LIKE @RefNumRegEx)
	BEGIN
		IF @FuelType = 'Oil'
		BEGIN
			---- MODEL DOES NOT WORK FOR OIL UNITS YET, GET OUT OF HERE! SW 7/7/14
			--RETURN
			--update 7/22/15 changed it so it will fire the Gas model for oil units, this may produce whackadoodle results though
			SET @RankineGas = 1
			SET @Model = 'RankineGas'
		END
	END
	
	---------------DECLARE all the variables used to hold the Power and Multiplier values---------------
	DECLARE @Pwr_Scrubber REAL
	DECLARE @Mult_Scrubber REAL
	DECLARE @Pwr_PrecBag REAL
	DECLARE @Mult_PrecBag REAL
	DECLARE @Pwr_SCR REAL
	DECLARE @Mult_SCR REAL
	DECLARE @Pwr_Freq50 REAL
	DECLARE @Mult_Freq50 REAL
	DECLARE @Pwr_Freq60 REAL
	DECLARE @Mult_Freq60 REAL
	DECLARE @Pwr_Starts REAL
	DECLARE @Mult_Starts REAL
	DECLARE @Pwr_SiteEffect REAL
	DECLARE @Mult_SiteEffect REAL
	DECLARE @Pwr_OtherSiteCap REAL
	DECLARE @Mult_OtherSiteCap REAL
	DECLARE @Pwr_SteamSalesMBTU REAL
	DECLARE @Mult_SteamSalesMBTU REAL
	DECLARE @Pwr_SolidFuel REAL
	DECLARE @Mult_SolidFuel REAL
	DECLARE @Pwr_NatGas REAL
	DECLARE @Mult_NatGas REAL
	DECLARE @Pwr_OffGas REAL
	DECLARE @Mult_OffGas REAL
	DECLARE @Pwr_DieselJet REAL
	DECLARE @Mult_DieselJet REAL
	DECLARE @Pwr_HeavyFuel REAL
	DECLARE @Mult_HeavyFuel REAL
	DECLARE @Pwr_Age REAL
	DECLARE @Mult_Age REAL
	DECLARE @Pwr_EquivBoilers REAL
	DECLARE @Mult_EquivBoilers REAL
	DECLARE @Pwr_Supercritical REAL
	DECLARE @Mult_Supercritical REAL
	DECLARE @Pwr_CTGGas REAL
	DECLARE @Mult_CTGGas REAL
	DECLARE @Pwr_SiteFuel REAL
	DECLARE @Mult_SiteFuel REAL
	DECLARE @Pwr_ETech REAL
	DECLARE @Mult_ETech REAL
	DECLARE @Pwr_FTech REAL
	DECLARE @Mult_FTech REAL
	DECLARE @Pwr_AeroTech REAL
	DECLARE @Mult_AeroTech REAL
	DECLARE @Pwr_NumSTs REAL
	DECLARE @Mult_NumSTs REAL
	DECLARE @Pwr_NumGens REAL
	DECLARE @Mult_NumGens REAL
	DECLARE @Pwr_TotEquip REAL
	DECLARE @Mult_TotEquip REAL
	DECLARE @Pwr_MWH REAL
	DECLARE @Mult_MWH REAL
	DECLARE @Pwr_Subcritical REAL
	DECLARE @Mult_Subcritical REAL
	DECLARE	@Pwr_POHours REAL
	DECLARE	@Mult_POHours REAL
	DECLARE	@Pwr_ServiceHours REAL
	DECLARE	@Mult_ServiceHours REAL
	DECLARE	@Pwr_HRSGCap REAL
	DECLARE	@Mult_HRSGCap REAL
	DECLARE	@Pwr_HRSGCapN REAL
	DECLARE	@Mult_HRSGCapN REAL
	DECLARE	@Pwr_CTGLiquidFuel REAL
	DECLARE	@Mult_CTGLiquidFuel REAL
	
	-------------------------populate the Power and Multiplier variables
	SELECT @Pwr_Scrubber = Pwr_Scrubber,
		@Mult_Scrubber = Mult_Scrubber,
		@Pwr_PrecBag = Pwr_PrecBag,
		@Mult_PrecBag = Mult_PrecBag,
		@Pwr_SCR = Pwr_SCR,
		@Mult_SCR = Mult_SCR,
		@Pwr_Freq50 = Pwr_Freq50,
		@Mult_Freq50 = Mult_Freq50,
		@Pwr_Freq60 = Pwr_Freq60,
		@Mult_Freq60 = Mult_Freq60,
		@Pwr_Starts = Pwr_Starts,
		@Mult_Starts = Mult_Starts,
		@Pwr_SiteEffect = Pwr_SiteEffect,
		@Mult_SiteEffect = Mult_SiteEffect,
		@Pwr_OtherSiteCap = Pwr_OtherSiteCap,
		@Mult_OtherSiteCap = Mult_OtherSiteCap,
		@Pwr_SteamSalesMBTU = Pwr_SteamSalesMBTU,
		@Mult_SteamSalesMBTU = Mult_SteamSalesMBTU,
		@Pwr_SolidFuel = Pwr_SolidFuel,
		@Mult_SolidFuel = Mult_SolidFuel,
		@Pwr_NatGas = Pwr_NatGas,
		@Mult_NatGas = Mult_NatGas,
		@Pwr_OffGas = Pwr_OffGas,
		@Mult_OffGas = Mult_OffGas,
		@Pwr_DieselJet = Pwr_DieselJet,
		@Mult_DieselJet = Mult_DieselJet,
		@Pwr_HeavyFuel = Pwr_HeavyFuel,
		@Mult_HeavyFuel = Mult_HeavyFuel,
		@Pwr_Age = Pwr_Age,
		@Mult_Age = Mult_Age,
		@Pwr_EquivBoilers = Pwr_EquivBoilers,
		@Mult_EquivBoilers = Mult_EquivBoilers,
		@Pwr_Supercritical = Pwr_Supercritical,
		@Mult_Supercritical = Mult_Supercritical,
		@Pwr_CTGGas = Pwr_CTGGas,
		@Mult_CTGGas = Mult_CTGGas,
		@Pwr_SiteFuel = Pwr_SiteFuel,
		@Mult_SiteFuel = Mult_SiteFuel,
		@Pwr_ETech = Pwr_ETech,
		@Mult_ETech = Mult_ETech,
		@Pwr_FTech = Pwr_FTech,
		@Mult_FTech = Mult_FTech,
		@Pwr_AeroTech = Pwr_AeroTech,
		@Mult_AeroTech = Mult_AeroTech,
		@Pwr_NumSTs = Pwr_NumSTs,
		@Mult_NumSTs = Mult_NumSTs,
		@Pwr_NumGens = Pwr_NumGens,
		@Mult_NumGens = Mult_NumGens,
		@Pwr_TotEquip = Pwr_TotEquip,
		@Mult_TotEquip = Mult_TotEquip,
		@Pwr_MWH = Pwr_MWH,
		@Mult_MWH = Mult_MWH,
		@Pwr_Subcritical = Pwr_Subcritical,
		@Mult_Subcritical = Mult_Subcritical,
		@Pwr_POHours = Pwr_POHours,
		@Mult_POHours = Mult_POHours,
		@Pwr_ServiceHours = Pwr_ServiceHours,
		@Mult_ServiceHours = Mult_ServiceHours,
		@Pwr_HRSGCap = Pwr_HRSGCap,
		@Mult_HRSGCap = Mult_HRSGCap
	FROM PowerGlobal.dbo.EGCCoefficients2013
	WHERE Year = @year AND model = @Model
	
	--these are set to 0 for 2015, might need to revise in future
	SET @Pwr_HRSGCapN = 0
	SET @Mult_HRSGCapN = 0
	SET @Pwr_CTGLiquidFuel = 0
	SET @Mult_CTGLiquidFuel = 0

	
	
	-----------------------------------------------------
	---special exception code for 2015 - an extra model for CC units using mostly liquid fuel
	--print @year
	--print @model
	--print @CTGLiquidMBTU
	--print @CTGGasMBTU

	---
	--IF @year = 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75) --75% liquid fuel in the CTG
	IF @year >= 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75) --75% liquid fuel in the CTG
	BEGIN

		SET @Mult_Freq50 = 6.108330994
		SET @Mult_SCR = 0.0000156333675814135
		SET @Pwr_Freq60 = 1
		SET @Mult_Freq60 = 3.861546616
		SET @Mult_Starts = 10.42526612 --should be CTG starts, is there something separate?
		SET @Mult_SiteEffect = 95.01138866
		SET @Mult_SteamSalesMBTU = 1013.416787
		SET @Mult_Age = 1.009603897
		SET @Mult_CTGGas = 0
		SET @Mult_SiteFuel = 2.295009898
		SET @Mult_ETech = 0.837543396
		SET @Mult_FTech = 13.80182103
		SET @Mult_AeroTech = 0.0000046614735533391
		SET @Mult_NumSTs = 0
		SET @Mult_NumGens = 0
		SET @Mult_TotEquip = 0
		SET @Mult_POHours = 12.12086212
		SET @Mult_ServiceHours = 0.198440634
		SET @Mult_HRSGCapN = 23.52182008 --this number and the hrsgcap below are switched, i screwed up the calc and it was easier just to plug these in backwards than to do a whole recalc
		SET @Pwr_HRSGCapN = 1
		SET @Mult_HRSGCap = 9.671174771
		SET @Pwr_CTGLiquidFuel = 1
		SET @Mult_CTGLiquidFuel = 0.43063066
		
	END
	------------------------------------------------------
	
	
	
	
	
--print @refnum
--print @model
	
	
	

	--------Scrubbers--------
	SET @EGC_Scrubber =
		(POWER(
			@ScrubbersYN * -- if they have scrubbers
			@DesignEffPct * -- scrubber efficiency
			((@Coal1Tons * @Coal1SulfurPcnt) + (@Coal2Tons * @Coal2SulfurPcnt) + (@Coal3Tons * @Coal3SulfurPcnt))/1000000 -- sulfur tons
			,@Pwr_Scrubber)
			* @Mult_Scrubber)

--print @egc_scrubber
--select @ScrubbersYN, @DesignEffPct, @Coal1Tons, @Coal1SulfurPcnt, @Coal2Tons, @Coal2SulfurPcnt, @Coal3Tons, @Coal3SulfurPcnt, @Pwr_Scrubber, @Mult_Scrubber

	--------PrecBag--------
	SET @EGC_PrecBag =
		(POWER(
			@PrecBagYN * -- if they have precbags
			CASE WHEN ((@Coal1Tons * @Coal1AshPcnt) + (@Coal2Tons * @Coal2AshPcnt) + (@Coal3Tons * @Coal3AshPcnt)) = 0 THEN 0 ELSE 
				LOG((@Coal1Tons * @Coal1AshPcnt) + (@Coal2Tons * @Coal2AshPcnt) + (@Coal3Tons * @Coal3AshPcnt)) END -- ash tons
			,@Pwr_PrecBag)
			* @Mult_PrecBag)

--print @egc_precbag  


--print @AdjNetMWH
--print @scr

	--------SCR--------
	SET @EGC_SCR = 
		(POWER(
			(@SCR *
				CASE @Model WHEN 'SC' THEN @AdjNetMWH ELSE @AdjNetMWH2Yr END / CASE @Model WHEN 'SC' THEN CASE WHEN @year = 2013 THEN 1 ELSE 1000 END WHEN 'CC' THEN CASE WHEN @year < 2015 THEN 1000000 ELSE 1000 END ELSE 1000000 END) --change 2014
			,@Pwr_SCR)
			*@Mult_SCR)

--print @SCR
--print @Model
--print @AdjNetMWH2Yr
--print @Pwr_SCR
--print @Mult_SCR
--print @egc_scr
	
	--------Frequency50--------

	SET @EGC_Frequency50 = 
--		@IsRankine -- doesn't apply to CC/SC units
		 CASE @Frequency WHEN 50 THEN 1 ELSE 0 END  -- this is Freq60
		* POWER(CASE @Model WHEN 'CC' THEN case WHEN @year < 2015 THEN @AdjNetMWH/1000000 ELSE @NDC END ELSE @NDC END, @Pwr_Freq50)
		* @Mult_Freq50

--print @model
	
--print @EGC_Frequency50

	--------Frequency60--------

	SET @EGC_Frequency60 = 
		@IsRankine -- doesn't apply to CC/SC units
		* CASE @Frequency WHEN 60 THEN 1 ELSE 0 END  -- this is Freq60
		* POWER(@NDC, @Pwr_Freq60)
		* @Mult_Freq60

	--a different multiplier for freq60 when it is liquid model
	--IF @year = 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	IF @year >= 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	BEGIN
		SET @EGC_Frequency60 = --doesnt apply to CC/SC units except for liquid ctg, which is this case
			CASE @Frequency WHEN 60 THEN 1 ELSE 0 END  -- this is Freq60
			* POWER(@NDC, @Pwr_Freq60)
			* @Mult_Freq60
	END


	
--print @EGC_Frequency60

	--------Starts--------
	SET @EGC_Starts =
		CASE WHEN @IsRankine = 0 THEN 1 ELSE 0 END -- Rankine units get nothing, non-Rankine units get calced
		* POWER((@ServiceHrs/CASE WHEN @Starts > 0 THEN @Starts ELSE 1 END), @Pwr_Starts) -- case stmt to fix anyone with 0 starts
		* @Mult_Starts


	--a different multiplier for starts when it is liquid model
	--IF @year = 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	IF @year >= 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	BEGIN
		SET @EGC_Starts = 
			POWER(@Starts/@CTG_Count,@Pwr_Starts)
			* @Mult_Starts

	END





--print @starts
--print @servicehrs
--print @pwr_starts
--print @mult_starts

--print @EGC_Starts
--print 'here'


	--------Site Effect--------
	SET @EGC_SiteEffect =
		CASE WHEN @IsRankine = 1 THEN SIGN(@NDC/@Site_NDC-0.5) ELSE SIGN(@NDC - @Site_NDC/@NumUnitsAtSite) END
		* POWER(ABS(CASE WHEN @IsRankine = 1 THEN @NDC/@Site_NDC-0.5 ELSE @NDC - @Site_NDC/@NumUnitsAtSite END), @Pwr_SiteEffect)
		* @Mult_SiteEffect

	---site effect changed for CC in 2015
	IF @CC = 1 AND @year >= 2015
	BEGIN
		SET @EGC_SiteEffect =
		POWER(@NDC/(@Site_NDC/@NumUnitsAtSite),  @Pwr_SiteEffect)
		* @Mult_SiteEffect
	END



--print 'here'
--select @NDC, @Site_NDC, @NumUnitsAtSite
--print @EGC_SiteEffect

	--------Other Site Cap--------
	SET @EGC_OtherSiteCap = 
		@IsRankine
		* SIGN(@NDC - @Site_NDC/@NumUnitsAtSite) 
		* POWER(ABS(@NDC - @Site_NDC/@NumUnitsAtSite), @Pwr_OtherSiteCap)
		* @Mult_OtherSiteCap
	
	IF @RankineGas = 1 AND @year >= 2015 -- change to OtherSiteCap for Rankine Gas in 2015, this will overwrite the value just above for the appropriate units
	BEGIN
		SET @EGC_OtherSiteCap =
		POWER ((@NDC/(@Site_NDC/@NumUnitsAtSite)), @Pwr_OtherSiteCap)
		* @Mult_OtherSiteCap
	END

--print @EGC_OtherSiteCap

	--------Steam Sales--------
	SET @EGC_SteamSales = 
		CASE WHEN @CC = 1 THEN POWER(LOG(1 + ABS(@SteamSalesMBTU/case when @year <2015 then 1000000 else 1000 end)), @Pwr_SteamSalesMBTU)
			ELSE POWER(@SteamSalesMBTU/(CASE WHEN @IsRankine = 1 THEN 1000 ELSE 1000000 END), @Pwr_SteamSalesMBTU) END
		* @Mult_SteamSalesMBTU

--print @steamsalesmbtu
--print @CC
--print @IsRankine
--print '**'
--print @Pwr_steamsalesmbtu
--print @mult_steamsalesmbtu
--print '**'	
--print @EGC_SteamSales

	--------Solid Fuel--------
	SET @EGC_SolidFuel = 
		@RankineCoal * --only applies to Coal

		POWER((@Coal1Tons * (1 - (@Coal1AshPcnt + @Coal1SulfurPcnt + @Coal1MoisturePcnt)/100) +
				@Coal2Tons * (1 - (@Coal2AshPcnt + @Coal2SulfurPcnt + @Coal2MoisturePcnt)/100) +
				@Coal3Tons * (1 - (@Coal3AshPcnt + @Coal3SulfurPcnt + @Coal3MoisturePcnt)/100))/1000
			,@Pwr_SolidFuel)
		* @Mult_SolidFuel

--print @EGC_SolidFuel

	--------NatGas + H2Gas--------
	SET @EGC_NatGasH2Gas =
		@IsRankine *
		POWER(@NatGasMBTU/1000, @Pwr_NatGas)
		* @Mult_NatGas

--print @EGC_NatGasH2Gas

	--------OffGas--------
	SET @EGC_OffGas =
		(CASE WHEN @year = 2013 THEN @RankineGas ELSE @IsRankine END) *
		POWER(@OffGasMBTU/1000, @Pwr_OffGas)
		* @Mult_OffGas

--print @IsRankine
--print @OffGasMBTU
--print @Pwr_OffGas
--print @Mult_OffGas
	
--print @EGC_OffGas

	--------Diesel + Jet--------
	SET @EGC_DieselJet = 
		(CASE WHEN @year = 2013 THEN @RankineCoal ELSE @IsRankine END) * --changed 2014
		POWER(@DieselJet/1000, @Pwr_DieselJet)
		* @Mult_DieselJet

--print @EGC_DieselJet

	--------HeavyFuel--------
	SET @EGC_HeavyFuel = 
		@RankineCoal *
		POWER(@HeavyFuel/1000, @Pwr_HeavyFuel)
		* @Mult_HeavyFuel

--print @EGC_HeavyFuel

	--------Age--------
		--Rankine Coal has one equation, CC has another, the other two variants return 0.
		--We add the Rankine Coal and CC together to get a result we can multiply both of them by, since they will only return a number for their own equation
		--MAGIC NUMBER ALERT!
	SET @EGC_Age =
		(CASE WHEN @RankineCoal = 1 THEN POWER(@Age, @Pwr_Age) ELSE 0 END
		+ CASE WHEN @CC = 1 THEN POWER(CASE WHEN @year = 2013 THEN 1.38898405948581 ELSE 1.91429722806757 END,(FLOOR(@Age/8)))* POWER(@NDC,@Pwr_Age) ELSE 0 END  -- changed 2014
		+ CASE WHEN @SC = 1 THEN CASE @year WHEN 2013 THEN 0 WHEN 2014 THEN POWER (POWER(1.3000, (FLOOR(@Age/8))) * @CTG_NDC, @Pwr_Age) ELSE POWER(@Age, @Pwr_Age) END ELSE 0 END --changed 2014, 2015
		)
		* @Mult_Age

	--a different multiplier for age when it is liquid model
	--IF @year = 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	IF @year >= 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	BEGIN
		SET @EGC_Age = 
			POWER(1.91429722806757,(FLOOR(@Age/5)))* POWER(@NDC,@Pwr_Age)
			* @Mult_Age

	END












--print @RankineCoal
--print @Age
--print @Pwr_Age
--print @CC
--print @year
--print @NDC
--print @SC
--print @CTG_NDC


--print @EGC_Age

	--------Equiv Boiler--------
	SET @EGC_EquivBoiler = 
		@RankineCoal *
		POWER(@NumBoilers * @BoilerSizeFactor,@Pwr_EquivBoilers)
		*@Mult_EquivBoilers

--print @EGC_EquivBoiler

	--------Supercritical--------
	SET @EGC_Supercritical = 
		@RankineCoal *
		POWER(CASE WHEN @BlrPSIG >= 3200 THEN @NDC ELSE 0 END, @Pwr_Supercritical)
		* @Mult_Supercritical

--print @EGC_Supercritical

	--------Subcritical--------
	SET @EGC_Subcritical = 
	(CASE WHEN @year = 2013 THEN 0 ELSE @IsRankine END) * --added 2014
		ISNULL(POWER(CASE WHEN @BlrPSIG < 3200 THEN @NDC/(CASE WHEN @RankineCoal = 1 THEN 1 WHEN @RankineGas = 1 THEN @Frequency ELSE 1 END) ELSE 0 END, ISNULL(@Pwr_Subcritical,0)),0)
		* ISNULL(@Mult_Subcritical,0)

	IF @RankineGas = 1 AND @year >= 2015 -- change to Subcritical for Rankine Gas in 2015, this will overwrite the value just above for the appropriate units
	BEGIN
		SET @EGC_Subcritical =
		ISNULL(POWER (CASE WHEN @BlrPSIG < 3200 THEN @NDC ELSE 0 END, @Pwr_Subcritical),0)
		* ISNULL(@Mult_Subcritical,0)
	END

--print @EGC_Supercritical
--print @year
--print @IsRankine
--print @BlrPSIG
--print @NDC
--print @RankineCoal
--print @RankineGas
--print @Frequency
--print @Pwr_Subcritical
--print @Mult_Subcritical


	--------MWH--------
	SET @EGC_MWH = 
		(CASE WHEN @year = 2013 THEN 0 ELSE @RankineGas END ) * --added 2014
		POWER(@AdjNetMWH2Yr/1000000, ISNULL(@Pwr_MWH,0))
		* ISNULL(@Mult_MWH,0)

--print @EGC_MWH


	--------Taxes--------
	--in 2014 we calced the model against the non-Tax TCLFE total, but we need to add the value of taxes back in to compare against the taxed TCLFE total
	--we simply add the tax amount, because it will count 100% against EGC
	SET @EGC_Taxes = CASE WHEN @year = 2013 THEN 0 ELSE @Taxes END


	---------PO Hours----------------
	--in 2015 we added PO Hours as a variable

	SET @EGC_POHours =
		(CASE WHEN @year < 2015 THEN 0 ELSE 1 END) * --added for 2015
		POWER(@POHours/168, ISNULL(@Pwr_POHours,0)) --divide by 168 because we want the number of weeks
		* ISNULL(@Mult_POHours,0)

	------ Service Hours
	SET @EGC_ServiceHours = 0
	IF @CC = 1 AND @year >= 2015
	BEGIN
		SET @EGC_ServiceHours =
		POWER(@ServiceHrs, @Pwr_ServiceHours)
		* @Mult_ServiceHours
	END

	-- HRSG Capacity
	SET @EGC_HRSGCap = 0
	IF @CC = 1 AND @year >= 2015
	BEGIN
		SET @EGC_HRSGCap = 
			POWER(((@NDC - @CTG_NDC)/@CTG_Count), @Pwr_HRSGCap)
			* @Mult_HRSGCap
	END

	--print 'blah'
	--print @ndc
	--print @ctg_ndc
	--print @ctg_count
	--print @pwr_hrsgcap
	--print @mult_hrsgcap
	--print @egc_hrsgcap


	-- HRSG Capacity N - added 2015 for liquid fuel units only
	SET @EGC_HRSGCapN = 0
	--IF @year = 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	IF @year >= 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	BEGIN
		SET @EGC_HRSGCapN = 
			POWER((100 * ((@NDC - @CTG_NDC)/@NDC)), @Pwr_HRSGCapN)
			* @Mult_HRSGCapN
	END

	--CTG Liquid fuel, added 2015 for liquid fuel units only
	SET @EGC_CTGLiquidFuel = 0
	--IF @year = 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	IF @year >= 2015 AND @Model = 'CC' AND (@CTGLiquidMBTU/(@CTGLiquidMBTU + @CTGGasMBTU) > 0.75)
	BEGIN
		SET @EGC_CTGLiquidFuel = 
			POWER((@CTGLiquidMBTU/1000), @Pwr_CTGLiquidFuel)
			* @Mult_CTGLiquidFuel

	END



	IF @IsRankine = 1
		BEGIN
			-- the following variables are only used for CC/SC units, so we set them to 0 here for Rankine units
		
			SET @EGC_CTGGas = 0
			SET @EGC_SiteFuel = 0
			SET @EGC_ETech = 0
			SET @EGC_FTech = 0
			SET @EGC_AeroTech = 0
			SET @EGC_NumSTs = 0
			SET @EGC_NumGens = 0
			SET @EGC_TotEquip = 0

		END
	ELSE
		BEGIN
			-- and here we do the calculations for the CC/SC units

			--------CTG Gas--------
			SET @EGC_CTGGas = 
				POWER(CASE WHEN @year = 2013 THEN @CTGGasMBTU ELSE (@NatGasMBTU + @OffGasMBTU) END /CASE WHEN @year < 2015 THEN 1000000 ELSE 1000 END, @Pwr_CTGGas) --change 2014
				* @Mult_CTGGas

	--print @EGC_CTGGas

			--------Site Fuel--------
			SET @EGC_SiteFuel = 
				POWER(@SiteMBTU/CASE WHEN @year < 2015 THEN 1000000 ELSE 1000 END, @Pwr_SiteFuel)
				* @Mult_SiteFuel

	--print @EGC_SiteFuel

			
			--------E Tech--------
			SET @EGC_ETech = 
				CASE WHEN @Class = 'E' THEN 
					POWER(CASE WHEN @Model = 'CC' THEN @CTG_NDC/@CTG_Count ELSE @CTG_NDC END, @Pwr_ETech)
				ELSE 0 END
				* @Mult_ETech

	--print @EGC_ETech
		
			--------F Tech--------
			SET @EGC_FTech = 
				CASE WHEN @Class = 'F' THEN 
					POWER(CASE @Model WHEN 'CC' THEN @CTG_NDC/@CTG_Count WHEN 'SC' THEN CASE @year WHEN 2013 THEN 0 ELSE @CTG_NDC END ELSE 0 END, @Pwr_FTech)
				ELSE 0 END
				* @Mult_FTech
	--print @CTG_NDC
	--print @CTG_Count
	--print @Class
	--print @EGC_FTech

			--------Aero Tech--------
			SET @EGC_AeroTech = 
				CASE WHEN @Class = 'Aero' THEN 
					POWER(
						CASE WHEN @year = 2013 THEN 
							CASE WHEN @Model = 'SC' THEN @CTG_NDC ELSE 0 END 
						ELSE 
							CASE @Model WHEN 'SC' THEN @CTG_NDC WHEN 'CC' THEN @CTG_NDC/@CTG_Count ELSE 0 END --change 2014
						END, @Pwr_AeroTech)
				ELSE 0 END
				* @Mult_AeroTech

	--print @EGC_AeroTech

			--------Num STs--------
			SET @EGC_NumSTs = 
				@CC 
				* POWER(@NumSTs, @Pwr_NumSTs)
				* @Mult_NumSTs
			
	--print @EGC_NumSTs

			--------Num Gens--------
			SET @EGC_NumGens = 
				@CC 
				* POWER(CASE WHEN @SingleShaft = 1 THEN @CTG_Count ELSE (@CTG_Count + @NumSTs) END, @Pwr_NumGens)
				* @Mult_NumGens
			
	--print @EGC_NumGens
		
			--------Tot Equip--------
			SET @EGC_TotEquip = 
				CASE WHEN @year < 2015 THEN 
						@SC
						* POWER(
							CASE @TotNumMajEqpt
								WHEN 6 THEN 3.45787518340236
								WHEN 8 THEN 4.76322719381692
								ELSE 2 END
							,@Pwr_TotEquip)
						* @Mult_TotEquip
				ELSE
					POWER(@TotNumMajEqpt, @Pwr_TotEquip)
						* @Mult_TotEquip
				END

	--=(IF(CI226=6,3.45787518340236,IF(CI226=8,4.76322719381692,2))^$AJ$224)*$AJ$225

	--print @TotNumMajEqpt
	--print @Pwr_TotEquip
	--print @Mult_TotEquip
	--print @EGC_TotEquip





		END
	

	SET @EGC_Total = 
		  @EGC_Scrubber 
		+ @EGC_PrecBag 
		+ @EGC_SCR 
		+ @EGC_Frequency50 
		+ @EGC_Frequency60 
		+ @EGC_Starts 
		+ @EGC_SiteEffect 
		+ @EGC_OtherSiteCap 
		+ @EGC_SteamSales 
		+ @EGC_SolidFuel 
		+ @EGC_NatGasH2Gas 
		+ @EGC_OffGas 
		+ @EGC_DieselJet 
		+ @EGC_HeavyFuel 
		+ @EGC_Age 
		+ @EGC_EquivBoiler 
		+ @EGC_Supercritical 
		+ @EGC_CTGGas 
		+ @EGC_SiteFuel 
		+ @EGC_ETech 
		+ @EGC_FTech 
		+ @EGC_AeroTech 
		+ @EGC_NumSTs 
		+ @EGC_NumGens 
		+ @EGC_TotEquip 
		+ @EGC_Subcritical
		+ @EGC_MWH
		+ @EGC_Taxes
		+ @EGC_POHours
		+ @EGC_ServiceHours
		+ @EGC_HRSGCap
		+ @EGC_HRSGCapN
		+ @EGC_CTGLiquidFuel

		 Print @EGC_Scrubber 
		 Print @EGC_PrecBag 
		 Print @EGC_SCR 
		 Print @EGC_Frequency50 
		 Print @EGC_Frequency60 
		 Print @EGC_Starts 
		 Print @EGC_SiteEffect 
		 Print @EGC_OtherSiteCap 
		 Print @EGC_SteamSales 
		 Print @EGC_SolidFuel 
		 Print @EGC_NatGasH2Gas 
		 Print @EGC_OffGas 
		 Print @EGC_DieselJet 
		 Print @EGC_HeavyFuel 
		 Print @EGC_Age 
		 Print @EGC_EquivBoiler 
		 Print @EGC_Supercritical 
		 Print @EGC_CTGGas 
		 Print @EGC_SiteFuel 
		 Print @EGC_ETech 
		 Print @EGC_FTech 
		 Print @EGC_AeroTech 
		 Print @EGC_NumSTs 
		 Print @EGC_NumGens 
		 Print @EGC_TotEquip 
		 Print @EGC_Subcritical
		 Print @EGC_MWH
		 Print @EGC_Taxes
		 Print @EGC_POHours
		 Print @EGC_ServiceHours
		 Print @EGC_HRSGCap
		 Print @EGC_HRSGCapN
		 Print @EGC_CTGLiquidFuel
		 Print 'EGC Stuff'



	--2014 addition, CE units were skewing too high so we threw in a fudge factor
	IF @year = 2014
	BEGIN
		SET @EGC_Total = @EGC_Total * @IsCE
	END

	--2015 addition, CG units were skewing too low so we threw in a fudge factor
	--IF @year = 2015
	IF @year >= 2015
	BEGIN
		SET @EGC_Total = @EGC_Total / @IsCG
		SET @EGC_Total = @EGC_Total / @IsSC
		SET @EGC_Total = @EGC_Total / @IsESC
	END


	IF @debug = 1
	BEGIN
		--PRINT @refnum + ' EGC_Total = ' + CAST(@EGC_Total AS VARCHAR)

		SELECT EGC_Total = @EGC_Total,
			EGC_Scrubber = @EGC_Scrubber,
			EGC_PrecBag = @EGC_PrecBag,
			EGC_SCR = @EGC_SCR,
			EGC_Frequency50 = @EGC_Frequency50,
			EGC_Frequency60 = @EGC_Frequency60,
			EGC_Starts = @EGC_Starts,
			EGC_SiteEffect = @EGC_SiteEffect,
			EGC_OtherSiteCap = @EGC_OtherSiteCap,
			EGC_SteamSales= @EGC_SteamSales,
			EGC_SolidFuel = @EGC_SolidFuel,
			EGC_NatGasH2Gas = @EGC_NatGasH2Gas,
			EGC_OffGas = @EGC_OffGas,
			EGC_DieselJet = @EGC_DieselJet,
			EGC_HeavyFuel = @EGC_HeavyFuel,
			EGC_Age = @EGC_Age,
			EGC_EquivBoiler = @EGC_EquivBoiler,
			EGC_Supercritical = @EGC_Supercritical,
			EGC_CTGGas = @EGC_CTGGas,
			EGC_SiteFuel = @EGC_SiteFuel,
			EGC_ETech = @EGC_ETech,
			EGC_FTech = @EGC_FTech,
			EGC_AeroTech = @EGC_AeroTech,
			EGC_NumSTs = @EGC_NumSTs,
			EGC_NumGens = @EGC_NumGens,
			EGC_TotEquip = @EGC_TotEquip,
			EGC_Subcritical = @EGC_Subcritical,
			EGC_MWH = @EGC_MWH,
			EGC_Taxes = @EGC_Taxes,
			IsCE = @IsCE,
			IsCG = @IsCG,
			IsSC = @IsSC,
			IsESC = @IsESC,
			EGC_POHours = @EGC_POHours,
			EGC_ServiceHours = @EGC_ServiceHours,
			EGC_HRSGCap = @EGC_HRSGCap,
			EGC_HRSGCapN = @EGC_HRSGCapN,
			EGC_CTGLiquidFuel = @EGC_CTGLiquidFuel
		
		--print @numBoilers
		
		
		--print @EGC_Scrubber 
		--print @EGC_PrecBag 
		--print @EGC_SCR 
		--print @EGC_Frequency50 
		--print @EGC_Frequency60 
		--print @EGC_Starts 
		--print @EGC_SiteEffect 
		--print @EGC_OtherSiteCap 
		--print @EGC_SteamSales 
		--print @EGC_SolidFuel 
		--print @EGC_NatGasH2Gas 
		--print @EGC_OffGas 
		--print @EGC_DieselJet 
		--print @EGC_HeavyFuel 
		--print @EGC_Age 
		--print @EGC_EquivBoiler 
		--print @EGC_Supercritical 
		--print @EGC_CTGGas 
		--print @EGC_SiteFuel 
		--print @EGC_ETech 
		--print @EGC_FTech 
		--print @EGC_AeroTech 
		--print @EGC_NumSTs 
		--print @EGC_NumGens 
		--print @EGC_TotEquip 
		
		--print @numBoilers
		
	END


	----------------------------------------------------------------------------
	----------------------------------------------------------------------------
	----------------------------------------------------------------------------



	-------------------------------------------------------------------------------------------------
	--now we get a little tricky
	--the following isn't really necessary for the new calculation, but it is legacy from the old calculation
	--basically we're filling in a bunch of fields in the EGCCalc table, which may or may not be needed later

	----next, we're just doing a straight rip of the old code to populate the appropriate variables, so we can insert them
	IF @debug = 0 --note that debug can be set to 1 at the start if you just want to look at the totals, not populate the database with it
	BEGIN
	
					--	--first declare some of the fields we'll be calcing
					--	DECLARE @AshPcnt real, @CoalPcnt real, @OilPcnt real, @GasPcnt real, @Sulfur real
					--	--and of course our total
					--	DECLARE @calc_EGC real

					--	DECLARE @EGCTechnology varchar(15)
					--	DECLARE @EGCRegion varchar(20)
					--	DECLARE @CTG_NDC real
					--	DECLARE @NDC real 
					--	DECLARE @FuelType varchar(8)
					--	DECLARE @CoalMBTU real
					--	DECLARE @CoalSulfur real 
					--	DECLARE @CoalAsh real 
					--	DECLARE @OilSulfur real
					--	DECLARE @OilAsh real 
					--	DECLARE @NumUnitsAtSite real
					--	DECLARE @OilMBTU real
					--	DECLARE @GasMBTU real
					--	DECLARE @CoalNDC real 
					--	DECLARE @OilNDC real
					--	DECLARE @GasNDC real
					--	DECLARE @CTGs int 
					--	DECLARE @ScrubbersYN int
					--	DECLARE @PrecBagYN int 
					--	DECLARE @AdjNetMWH2Yr real 
					--	----@TotCashLessFuelEm real = null,
					--	DECLARE @Age real 
					--	DECLARE @ElecGenStarts real 

				--		-- run stored proc to try to fill in the EGCTechnology field in CASE this is the 1st time the unit has been calc'd
				--		EXEC spUPDATEEGCTechnologyForRefnum @Refnum = @refnum

				--		-- get the base required fields
				--		SELECT @EGCRegion=rtrim(EGCRegion), @EGCTechnology=rtrim(EGCTechnology) 
				--			FROM dbo.tsort 
				--			WHERE refnum=@refnum

				--		IF RIGHT(@Refnum,1) = 'P' AND @year < 2010 --oops, this will never be called now (since we're 2011 or higher). just leaving it in for posterity
				--		BEGIN
				--			SET @EGCTechnology='CARRYOVER'
				--		END

				--		SELECT @CTGs = ISNULL(ctgs,0), @CTG_NDC = ISNULL(ctg_ndc,0), @NDC = ISNULL(ndc,0), 
				--				@CoalNDC = ISNULL(Coal_NDC,0), @OilNDC = ISNULL(Oil_NDC,0), @GasNDC = ISNULL(Gas_NDC,0), 
				--				@PrecBagYN = ISNULL(PrecBagYN,0), @ScrubbersYN = ISNULL(ScrubbersYN,0)
				--			FROM dbo.tsort 
				--			WHERE Refnum=@refnum

				--		SELECT @NumUnitsAtSite = TotUnitNum
				--			FROM dbo.PlantGENData
				--			WHERE SiteID=(SELECT SiteID FROM TSort WHERE Refnum = @refnum)

				--		IF @NumUnitsAtSite IS NULL
				--			SELECT @NumUnitsAtSite = ISNULL(NumUnitsAtSite,0)
				--			FROM TSort
				--			WHERE Refnum = @refnum

				--		IF @NDC = 0
				--		BEGIN
						
				--			SELECT @NDC = ISNULL(nf.NDC,0)
				--			FROM NERCFactors nf 
				--			WHERE nf.Refnum = @refnum
						
				--		END


				--		SELECT @FuelType=fueltype 
				--			FROM dbo.FuelTotCalc  
				--			WHERE refnum=@refnum

				--		SELECT @Age=max(age) 
				--			FROM dbo.nercturbine 
				--			WHERE refnum=@refnum

				--		SELECT @ElecGenStarts=ISNULL(SUM(TotalOpps),0) 
				--			FROM dbo.StartsAnalysis sa 
				--			INNER JOIN NERCTurbine nt ON nt.Refnum = sa.Refnum AND nt.TurbineID = sa.TurbineID 
				--			WHERE nt.TurbineType IN ('CTG', 'STG', 'CST') AND sa.Refnum = @Refnum
				--		SELECT @AdjNetMWH2Yr = ISNULL(AdjNetMWH2Yr,0)/1000000 
				--			FROM dbo.GenerationTotCalc 
				--			WHERE Refnum = @Refnum

				--		SELECT @CoalSulfur = ISNULL(SulfurPcnt,0), @CoalAsh = ISNULL(AshPcnt,0)
				--			FROM CoalTotCalc
				--			WHERE Refnum = @Refnum

				--		SELECT @OilSulfur = ISNULL(Sulfur,0), @OilAsh = 0.1 
				--			FROM FuelProperties 
				--			WHERE Refnum = @Refnum



				--		--more calcs to fill in old data
				--		IF (@CTG_NDC IS NULL)
				--			SET @CTG_NDC = 0
				--		ELSE
				--			BEGIN
				--				IF (@CTGs > 0)
				--					SET @CTG_NDC = @CTG_NDC --/ @CTGs
				--			END

				--		IF @OilSulfur IS NULL
				--			SELECT @OilSulfur = 2.3, @OilAsh = 0.1

				--		SELECT @Sulfur = (ISNULL(@CoalSulfur,0)*@CoalNDC + ISNULL(@OilSulfur,0)*@OilNDC)/@NDC
				--		SELECT @AshPcnt = (ISNULL(@CoalAsh,0)*@CoalNDC + ISNULL(@OilAsh,0)*@OilNDC)/@NDC


				--		SELECT @OilMBTU = SUM(MBTU) 
				--			FROM Fuel 
				--			WHERE Refnum = @Refnum AND FuelType IN ('Jet', 'FOil', 'Diesel') AND TurbineID IN ('STG','Site')
				--		IF @OilMBTU IS NULL 
				--			SELECT @OilMBTU = 0

				--		SELECT @GasMBTU = SUM(MBTU) 
				--			FROM Fuel 
				--			WHERE Refnum = @Refnum AND FuelType IN ('NatGas', 'OffGas', 'H2Gas') AND TurbineID IN ('STG','Site')
				--		IF @GasMBTU IS NULL 
				--			SELECT @GasMBTU = 0

				--		SELECT @CoalMBTU = SUM(MBTU) 
				--			FROM Coal 
				--			WHERE Refnum = @Refnum

					
				--		IF (@OilMBTU + @GasMBTU) > 0
				--			SELECT @OilPcnt = @OilMBTU/(@OilMBTU + @GasMBTU)
				--		ELSE
				--			SELECT @OilPcnt = 0

				--		IF @OilPcnt < 0.1
				--			SELECT @OilPcnt = 0

				--		IF @OilPcnt > 0.9
				--			SELECT @OilPcnt = 1

				--		IF (@OilMBTU + @GasMBTU) > 0
				--			SELECT @GasPcnt = @GasMBTU/(@OilMBTU + @GasMBTU)
				--		ELSE
				--			SELECT @GasPcnt = 0

				--		IF @GasPcnt < 0.1
				--			SELECT @GasPcnt = 0

				--		IF @GasPcnt > 0.9
				--			SELECT @GasPcnt = 1

				--		IF @CoalMBTU > 0
				--			SELECT @CoalPcnt = @CoalMBTU/(@CoalMBTU + @OilMBTU + @GasMBTU)
				--		ELSE
				--			SELECT @CoalPcnt = 0

				--		IF @CoalPcnt < 0.1
				--			SELECT @CoalPcnt = 0

				--		IF @CoalPcnt > 0.9
				--			SELECT @CoalPcnt = 1

				--		--get the EGC value we came up with
				--		SELECT @calc_EGC = @EGC_Total --FROM #tmp WHERE refnum = @refnum 
					
				--		--Build the "REAL" EGCCalc record. The one that will be used.
				--		--if DEBUG THEN ONLY display the value do NOT UPDATE any database records
				--		--note the 2011 in the Values is the EGC Version, not the year
				--		DELETE FROM EGCCalc WHERE Refnum = @Refnum
				--		INSERT INTO EGCCalc (Refnum, Method, Technology, EGC, CTGs, CTG_NDC, NDC, AdjNetMWH, Sulfur, AshPcnt, 
				--			FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt, GasPcnt, CoalPcnt, Age, NumUnitsAtSite, 
				--			ScrubbersYN,  PrecBagYN)
				--		VALUES (@Refnum, '2011', @EGCTechnology, @calc_EGC, @CTGs, @CTG_NDC, @NDC, @AdjNetMWH2Yr, @Sulfur, @AshPcnt,
				--			@FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt, @GasPcnt, @CoalPcnt, @Age, @NumUnitsAtSite, 
				--			@ScrubbersYN, @PrecBagYN)

	-- Go UPDATE all of the EGC fields spread thoughout the database.
		UPDATE GenerationTotCalc SET EGC = @EGC_Total WHERE Refnum = @Refnum
		UPDATE Gensum SET EGC = @EGC_Total WHERE Refnum = @Refnum

		UPDATE NonOHMaint SET AnnNonOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnNonOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE OHEquipCalc SET TotAnnOHCostEGC = CASE WHEN @EGC_Total > 0 THEN TotAnnOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

		UPDATE MaintEquipCalc SET AnnOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHCostKUS/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintEquipCalc SET AnnNonOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnNonOHCostKUS/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintEquipCalc SET AnnMaintCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnMaintCostKUS/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

		UPDATE MaintTotCalc SET AnnNonOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnNonOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintTotCalc SET AnnOHCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintTotCalc SET AnnMaintCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnMaintCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintTotCalc SET AnnOHProjCostEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHProjCost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintTotCalc SET AnnOHExclProjEGC = CASE WHEN @EGC_Total > 0 THEN AnnOHExclProj/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintTotCalc SET AnnMaintExclProjEGC = CASE WHEN @EGC_Total > 0 THEN AnnMaintExclProj/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE MaintTotCalc SET AnnLTSACostEGC = CASE WHEN @EGC_Total > 0 THEN AnnLTSACost/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

		UPDATE Pers SET TotEffPersEGC = CASE WHEN @EGC_Total > 0 THEN TotEffPers*1000/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum
		UPDATE PersSTCalc SET TotEffPersEGC = CASE WHEN @EGC_Total > 0 THEN TotEffPers*1000/@EGC_Total ELSE NULL END WHERE Refnum = @Refnum

		UPDATE GenSum SET TotEffPersEGC = (SELECT TotEffPersEGC FROM PersSTCalc WHERE PersSTCalc.Refnum = Gensum.Refnum AND PersSTCalc.SectionID = 'TP') WHERE Refnum = @Refnum

		EXEC spAddOpexCalc @Refnum = @Refnum, @DataType = 'EGC', @DivFactor = @EGC_Total

	END -- this is the END of if @debug = 0
					
				--	ELSE
				--	BEGIN

				--		--and if we're just running in DEBUG, all we do is select the values we calculated
				--		IF @IsCogen = 1
				--		BEGIN
				--			SELECT Refnum = @refnum, EGC_Total = @EGC_Total, EGC_Cogen_HRSG = @EGC_Cogen_HRSG, EGC_Cogen_Coal_NDC = @EGC_Cogen_Coal_NDC,
				--				EGC_Cogen_CTG_NDC = @EGC_Cogen_CTG_NDC, EGC_Cogen_AdjNetMWh2Yr = @EGC_Cogen_AdjNetMWh2Yr,
				--				EGC_Cogen_StmSales = @EGC_Cogen_StmSales, EGC_Cogen_StmSalesMWh = @EGC_Cogen_StmSalesMWh,
				--				EGC_Cogen_TotStarts = @EGC_Cogen_TotStarts, EGC_Cogen_CTGAvgTotStarts = @EGC_Cogen_CTGAvgTotStarts,
				--				EGC_Cogen_HeatRate = @EGC_Cogen_HeatRate, EGC_Cogen_FiringTemp = @EGC_Cogen_FiringTemp,
				--				EGC_Cogen_Age = @EGC_Cogen_Age, EGC_Cogen_NatGasMBTU = @EGC_Cogen_NatGasMBTU,
				--				EGC_Cogen_OffGasMBTU = @EGC_Cogen_OffGasMBTU, EGC_Cogen_DieselFuelOilMBTU = @EGC_Cogen_DieselFuelOilMBTU,
				--				EGC_Cogen_JetTurbMBTU = @EGC_Cogen_JetTurbMBTU, EGC_Cogen_CoalMBTU = @EGC_Cogen_CoalMBTU,
				--				EGC_Cogen_Gas2YrMWh = @EGC_Cogen_Gas2YrMWh
							
				--		END
				--		ELSE

				--			IF @refnum NOT LIKE @RefNumRegEx

				--				SELECT Refnum = @refnum, EGC_Total = @EGC_Total, EGC_SCR = @EGC_SCR, EGC_WGS = @EGC_WGS, WGC_DGS = @EGC_DGS, EGC_PrecBag = @EGC_PrecBag, 
				--					EGC_OtherSiteCapCoalOil = @EGC_OtherSiteCapCoalOil, EGC_OtherSiteCapGas = @EGC_OtherSiteCapGas,
				--					EGC_OtherSiteCap1 = @EGC_OtherSiteCap1, EGC_OtherSiteCap2 = @EGC_OtherSiteCap2, -- SW 6/12/13 added these two terms
				--					EGC_SteamSalesCoalOil = @EGC_SteamSalesCoalOil, EGC_SteamSalesGas = @EGC_SteamSalesGas, EGC_Coal_NDC = @EGC_Coal_NDC, 
				--					EGC_Gas_NDC = @EGC_Gas_NDC, 
				--					EGC_Geothermal_NDC = @EGC_Geothermal_NDC, --SW 6/12/13 added to model
				--					EGC_Coal1 = @EGC_Coal1, EGC_Coal2 = @EGC_Coal2, EGC_Coal3 = @EGC_Coal3, EGC_GasTotalNonGasUnits = @EGC_GasTotalNonGasUnits,
				--					EGC_GasTotalGasUnits = @EGC_GasTotalGasUnits, EGC_LiquidFuelsGasUnits = @EGC_LiquidFuelsGasUnits, 
				--					EGC_LiquidFuelsNoSulfurCoal = @EGC_LiquidFuelsNoSulfurCoal
									
				--			ELSE
				--				SELECT Refnum = @refnum, EGC_Total = @EGC_Total, EGC_Age = @EGC_Age, EGC_CTG_NDC = @EGC_CTG_NDC, EGC_FiringTemp = @EGC_FiringTemp, 
				--					EGC_CTG_AvgCTGSizeUnder150 = @EGC_CTG_AvgCTGSizeUnder150, EGC_CTG_AvgCTGSizeOver150 = @EGC_CTG_AvgCTGSizeOver150,
				--					EGC_CTG_SiteEffect = @EGC_CTG_SiteEffect, EGC_CTG_SCR_NDC = @EGC_CTG_SCR_NDC, EGC_CTG_SteamSales = @EGC_CTG_SteamSales, 
				--					EGC_CTG_HRSGCapNDC = @EGC_CTG_HRSGCapN, EGC_CTG_HRSGCap = @EGC_CTG_HRSGCap, EGC_CTG_GasMBTU = @EGC_CTG_GasMBTU, 
				--					EGC_CTG_LiquidMBTU = @EGC_CTG_LiquidMBTU
											
END
