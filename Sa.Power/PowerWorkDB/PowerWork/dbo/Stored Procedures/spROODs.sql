﻿CREATE PROCEDURE [dbo].[spROODs]
	@SiteID VARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	--initial setup
	CREATE TABLE #tmp (ID INT IDENTITY(1,1),
		Refnum CHAR(12), 
		TurbineID CHAR(6), 
		UnitName VARCHAR(50), 
		CauseCode SMALLINT, 
		VerbalDesc86 VARCHAR(86), 
		EventType CHAR(2), 
		StartDateTime DATETIME, 
		EndDateTime DATETIME,
		Result CHAR(12))

	CREATE TABLE #rngOutput (ID INT,
		Refnum CHAR(12), 
		TurbineID CHAR(6), 
		UnitName VARCHAR(50), 
		CauseCode SMALLINT, 
		VerbalDesc86 VARCHAR(86), 
		EventType CHAR(2), 
		StartDateTime DATETIME, 
		EndDateTime DATETIME,
		Result CHAR(12))
		
	DECLARE @inputID INT
	DECLARE @inputRefnum CHAR(12)
	DECLARE @inputTurbineID CHAR(6)
	DECLARE @inputUnitName VARCHAR(50)
	DECLARE @inputCauseCode SMALLINT
	DECLARE @inputVerbalDesc86 VARCHAR(86)
	DECLARE @inputEventType CHAR(2)
	DECLARE @inputStartDateTime DATETIME
	DECLARE @inputEndDateTime DATETIME

	DECLARE @pintCurrentOutageRow INT
	DECLARE @strStartupResult CHAR(12)
	DECLARE @intInRow INT
	DECLARE @datEndDate DATETIME
	DECLARE @intRow INT
		
	DECLARE @inputPlusID INT
	--DECLARE @inputPlusRefnum CHAR(12)
	--DECLARE @inputPlusTurbineID CHAR(6)
	--DECLARE @inputPlusUnitName VARCHAR(50)
	--DECLARE @inputPlusCauseCode SMALLINT
	--DECLARE @inputPlusVerbalDesc86 VARCHAR(86)
	DECLARE @inputPlusEventType CHAR(2)
	DECLARE @inputPlusStartDateTime DATETIME
	DECLARE @inputPlusEndDateTime DATETIME
	
	--SELECT * INTO #rngOutput FROM #tmp

    --get the raw Unit data
	INSERT #tmp (Refnum, TurbineID, UnitName, CauseCode, VerbalDesc86, EventType, StartDateTime, EndDateTime)
		SELECT t.Refnum, nt.TurbineID, t.UnitName, e.CauseCode, e.VerbalDesc86, e.EventType, e.StartDateTime, e.EndDateTime
		FROM TSort t
			LEFT JOIN NERCTurbine nt ON nt.Refnum = t.Refnum
			LEFT JOIN GADSOS.GADSNG.EventData01 e ON e.UtilityUnitCode = nt.UtilityUnitCode
		WHERE t.SiteID = @SiteID
			AND YEAR(e.StartDateTime)= t.StudyYear
			AND e.RevisionCard01 <> 'X' --these are deleted records we do not want to use
			AND nt.TurbineID <> 'STEAM' --steam rows dropped 
		ORDER BY t.SiteID, t.UnitName, nt.TurbineID, e.StartDateTime, e.EndDateTime

	--process the raw Unit data
	
	
--	Sub ProcessRawData()
    
--    ' Using the data in the range 'OutageEventsRaw' as input, build the range 'OutageEvents'.
    
--    Dim blnInitialOutage As Boolean
--  '  Dim blnSubsequentOutage As Boolean
    
--    'Dim rngInput As Range, rngReview As Range, rngOutput As Range
--    ' strEventType is the event type code from the data
--    Dim strEventType As String, strStartupResult As String
    
--    Dim intRowIn As Integer ', intColIn As Integer
--    Dim intRowOut As Integer ', intColOut As Integer
    
--    Dim intRow As Integer 'not sure what this is needed for - SW 1/17/13


--    Set rngInput = ThisWorkbook.Names("OutageEventsRaw").RefersToRange
--    Set rngReview = ThisWorkbook.Names("Review").RefersToRange
--    Set rngOutput = ThisWorkbook.Names("OutageEvents").RefersToRange
    
--    ' Clear out the area we will use for logging (temporary?)
--    rngInput.Cells(1, 1).Offset(0, colEND_TIME).EntireColumn.ClearContents
    
--    ' Clear the areas where we will put the results.
--    rngReview.ClearContents
--    rngReview.Cells(1, 1).Offset(0, colEND_TIME).EntireColumn.ClearContents
--    rngReview.Cells(1, 1).Offset(0, colEND_TIME + 1).EntireColumn.ClearContents
--    rngOutput.ClearContents
    
--    ' Clear out any unit markers left from last time.
--    rngReview.Borders.LineStyle = xlNone
    
--    ' Put some headings back out there.
--    rngReview.Cells(1, 1).Offset(-1, colEND_TIME).Value = "Computer"
--    rngReview.Cells(1, 1).Offset(-1, colEND_TIME + 1).Value = "Human"
    
--    intRowOut = 1
--    pstrUnitLabel = rngInput(1, colUnitLabel)
--    rngOutput(intRowOut, 1) = rngInput(1, colRefnum)
--    rngOutput(intRowOut, 2) = pstrUnitLabel
--    pintNextReviewLine = 0
    
--    For intRowIn = 1 To rngInput.Rows.Count

	DECLARE @rowToFind INT
	
	DECLARE @strEventType CHAR(2)
	DECLARE @nextrowID INT
	DECLARE @nextrowstarttime DATETIME
	DECLARE @nextrowEventType CHAR(2)

	DECLARE rngInputPlus CURSOR SCROLL STATIC FOR 
		SELECT ID, EventType, StartDateTime, EndDateTime FROM #tmp
	OPEN rngInputPlus

	DECLARE rngInput CURSOR FOR 
		SELECT ID, Refnum, TurbineID, UnitName, CauseCode, VerbalDesc86, EventType, StartDateTime, EndDateTime FROM #tmp
	OPEN rngInput
	FETCH NEXT FROM rngInput INTO @inputID, @inputRefnum, @inputTurbineID, @inputUnitName, @inputCauseCode, @inputVerbalDesc86, @inputEventType, @inputStartDateTime, @inputEndDateTime
	WHILE @@FETCH_STATUS = 0
	BEGIN
	--print 'here 0 - ' + CAST(@inputID AS CHAR)

		SET @strEventType = @inputEventType		--            strEventType = rngInput.Cells(intRowIn, colEVNT_TYPE)

		IF @strEventType IN ('PO', 'RS', 'MO', 'SF', 'U1', 'U2', 'U3', 'U4') -- IsOutage
		BEGIN

--                ' Skip any consecutive events. If the end time of this event equal the start time of the next event,
--                ' and the next event is not a Derate (Never let a derate be the initial outage.)
--                ' and the next event is not a StartUp Failure (Don't let the StartUp be the inital outage, let the original outage
--                ' be the initial. The SetStartupResult logic will catch the Startup as being the subsequent failure.)
--                ' then consider it a consecutive event and make it the Initial Outage.
			SET @rowToFind = @inputID + 1
			FETCH ABSOLUTE @rowToFind FROM rngInputPlus INTO @inputPlusID, @inputPlusEventType, @inputPlusStartDateTime, @inputPlusEndDateTime

			WHILE @inputEndDateTime = @inputPlusStartDateTime
				AND LEFT(@inputPlusEventType,1) <> 'D'
				AND @inputPlusEventType <> 'SF'
			BEGIN
				INSERT #rngOutput SELECT ID, Refnum, TurbineID, UnitName, CauseCode, VerbalDesc86, EventType, StartDateTime, EndDateTime, Result = 'c' FROM #tmp WHERE ID = @inputID
				FETCH NEXT FROM rngInput INTO @inputID, @inputRefnum, @inputTurbineID, @inputUnitName, @inputCauseCode, @inputVerbalDesc86, @inputEventType, @inputStartDateTime, @inputEndDateTime
				SET @rowToFind = @inputID + 1
				FETCH ABSOLUTE @rowToFind FROM rngInputPlus INTO @inputPlusID, @inputPlusEventType, @inputPlusStartDateTime, @inputPlusEndDateTime
			END

--                ' intRowIn is now pointing to the row number that we will consider to be the Initial Outage
--                ' Capture the row number of the outage. If we end up finding an outage or derate during the
--                ' following week, we will need to have this row number to put into the review range.
--                pintCurrentOutageRow = intRowIn
			SET @pintCurrentOutageRow = @inputID
                
--                ' Output this outage to the review range.
--                OutputReviewLine rngInput, rngReview, pintCurrentOutageRow, "" ' Did have Out here.
			INSERT #rngOutput SELECT ID, Refnum, TurbineID, UnitName, CauseCode, VerbalDesc86, EventType, StartDateTime, EndDateTime, Result = '' FROM #tmp WHERE ID = @inputID
                
--                ' Determine the start up result
--                strStartupResult = SetStartupResult(rngInput, intRowIn)
			SET @strStartupResult = ''



-------------------------------------------------------
----------Function SetStartupResult(rngInput As Range, intInRow As Integer) As String

--rngInput = rngInputPlus
--intInRow = @inputID
			SET @intInRow = @inputID

----------    ' Given a starting row in the input, look one week into the
----------    ' future and return a string indicating the result of the startup effort
----------    ' as follows:
----------    ' Successful, Derate, Forced Outage, Maint Outage, Startup failure
----------    Dim datEndDate As Date, sngEndTime As Single
----------    Dim blnHaveOutage As Boolean, blnHaveDerate As Boolean, blnHaveInitialDerate As Boolean
----------    Dim intRow As Integer
----------    Dim intRowIn As Integer ' ? SW
----------    Dim blnSubsequentOutage As Boolean
    
----------    datEndDate = rngInput(intInRow, colEND_TIME)
			SET @rowToFind = @intInRow
			FETCH ABSOLUTE @rowToFind FROM rngInputPlus INTO @inputPlusID, @inputPlusEventType, @inputPlusStartDateTime, @inputPlusEndDateTime
			SET @datEndDate = @inputPlusEndDateTime


    
----------    'DEBUG FRS 6/3/04
----------    'If rngInput(intRow, colUnitLabel) = "GT2" Then Stop
----------    blnHaveInitialDerate = False
----------    intRow = intInRow + 1
			SET @intRow = @intInRow + 1
----------    ' Look 1 week (10,080 minutes, to be exact) into the future for another outage.
----------    Do While intRow < rngInput.Rows.Count And _
----------            DateDiff("n", datEndDate, rngInput(intRow, colSTART_TIME)) < 10080 _
----------            And rngInput(intRow, colUnitLabel) = rngInput(intRow - 1, colUnitLabel) _
----------            And rngInput(intRow, colUnitName) = rngInput(intRow - 1, colUnitName)
----------        ' Check for outage. If it was a planned outage or a reserve shutdown, then stop looking for outages, and let
----------        ' the logic take you to the Derate search. The Derate logic will find the PO or RS and count it as successful.
----------        ' 8/12/02
----------        If (rngInput(intRow, colEVNT_TYPE) = "PO" Or rngInput(intRow, colEVNT_TYPE) = "RS") Then
----------            Exit Do
----------        Else
----------            ' if the start date-time of this line = the end date-time of the previous line, don't consider this a new event, it is
----------            ' just a continuation of the current one. and same event type? (does the cause code need to be the same ... or similar?)
----------            If Not (rngInput(intRowIn, colEND_TIME) = rngInput(intRowIn + 1, colSTART_TIME) _
----------                        And Left(rngInput(intRowIn + 1, colEVNT_TYPE), 1) <> "D" _
----------                        And rngInput(intRowIn + 1, colEVNT_TYPE) <> "SF") Then
    
----------                ' If it was a Startup Failure, count it (regardless of cause code). Otherwise, count if it was a "preventable" outage.
----------                blnSubsequentOutage = IsOutage(rngInput(intRow, colEVNT_TYPE)) And Preventable(rngInput(intRow, colCAUSE_CODE))
----------                'SW 1/29/13 removed date from IsOutage function: blnSubsequentOutage = IsOutage(rngInput(intRow, colEVNT_TYPE), rngInput(intRow, colSTART_TIME)) And Preventable(rngInput(intRow, colCAUSE_CODE))
----------                If rngInput(intRow, colEVNT_TYPE) = "SF" Or blnSubsequentOutage Then
----------                    blnHaveOutage = True
----------                    Exit Do
----------                End If
    
----------                'FOR NEXT YEAR.........   FRS 10/4/02
----------                ' I THINK WE SHOULD PUT THE FOLLOWING STATEMENT IN. BUT NEED TO RESEARCH AND TEST. ALSO WILL NEED
----------                ' TO CHANGE CODE ELSEWHERE SO THAT THIS WOULD NOT MAKE THAT PART GET ERROR.
----------                ' ELSE STRSTARTUPRESULT = ""
            
----------            End If
----------        End If
----------        intRow = intRow + 1
----------    Loop

----------    If Not blnHaveOutage Then
----------        ' Look 1 week into the future for a Derate.
----------        intRow = intInRow + 1
----------        Do While DateDiff("n", datEndDate, rngInput(intRow, colSTART_TIME)) < 10080 _
----------            And rngInput(intRow, colUnitLabel) = rngInput(intRow - 1, colUnitLabel) _
----------            And rngInput(intRow, colUnitName) = rngInput(intRow - 1, colUnitName)
----------            ' 8/12/02
----------            ' If you find a PO or RS before you find a derate,
----------            ' get out and let the logic at the end count it as successful.
----------            If rngInput(intRow, colEVNT_TYPE) = "PO" Or rngInput(intRow, colEVNT_TYPE) = "RS" Then
----------                Exit Do
----------            End If
                
----------            If Left(rngInput(intRow, colEVNT_TYPE), 1) = "D" And Preventable(rngInput(intRow, colCAUSE_CODE)) Then
----------                blnHaveDerate = True
----------                ' output this derate to the review range
----------                OutputReviewLine rngInput, rngReview, intRow, "Derate"
----------                If blnHaveInitialDerate Then
                
----------                    ' "Computer" column
----------                    rngReview.Cells(pintNextReviewLine, colEND_TIME + 1) = "xtrD"
----------                    ' "Human" column
----------                    rngReview.Cells(pintNextReviewLine, colEND_TIME + 2) = "xtrD"
    
----------                End If
----------                MarkRawData rngInput, intRow, "Derate"
----------                'Exit Do ' Commenting this line (should?) make the program keep looking for more derates during the week window.
----------                blnHaveInitialDerate = True
----------            End If
----------            If rngInput(intRow, colRefnum) = "" Then Exit Do
----------            intRow = intRow + 1
----------        Loop
----------    End If
----------    'DEBUG FRS 6/3/04
----------    'If Trim(rngInput(intRow, colUnitLabel)) = "GT2" Then Stop
    
----------    pstrUnitLabel = rngInput(intRow - 1, colUnitLabel)
    
----------    If blnHaveOutage Then
----------        Select Case rngInput(intRow, colEVNT_TYPE)
----------            Case "MO"
----------                SetStartupResult = "Maint"
----------            Case "U1", "U2", "U3"
----------                SetStartupResult = "Forced"
----------            Case "SF"
----------                SetStartupResult = "Startup"
----------            Case Else
----------                MsgBox "Event type " & rngInput(intRow, colEVNT_TYPE)
----------        End Select
----------        ' output this outage to the review range.
----------        OutputReviewLine rngInput, rngReview, intRow, SetStartupResult
        
----------        MarkRawData rngInput, intRow, SetStartupResult
----------        pintOutageNextRow = intRow
----------        Exit Function
----------    End If
    
----------    If blnHaveDerate Then
----------        SetStartupResult = "Derate"
----------        ' NEXT ROW ADDED 8/12/02 TO CORRECT PROBLEM OF DERATES
----------        ' GETTING PROCESSED MULTIPLE TIMES WHEN PRECEEDED BY A LOT OF RS's.
----------        ' BUT DID NOT WORK.
----------        'pintOutageNextRow = intRow
----------        Exit Function
----------    End If
    
----------    'Successful startup
----------    SetStartupResult = "Successful"
----------    'pintNextReviewLine = pintNextReviewLine - 1
----------    'rngReview.Rows(pintNextReviewLine).ClearContents

----------End Function
-------------------------------------------------
























			
--                ' Did we have another outage during the week?
--                Do Until strStartupResult = "Successful" Or strStartupResult = "Derate" Or pstrUnitLabel <> rngInput(intRowIn, colUnitLabel)
--                    ' YES, WE HAD AN OUTAGE DURING THE STARTUP WEEK.
                    
--                    ' Set the row we found the outage on as the next starting point.
--                    intRowIn = pintOutageNextRow
--                    ' If we have hit the next unit, capture the label and bump the row number that we are writing to
--                    ' in the output range. And call this one a success.
--                    If pstrUnitLabel <> rngInput(intRowIn, colUnitLabel) Then
--                        pstrUnitLabel = rngInput(intRowIn, colUnitLabel)
--                        intRowOut = intRowOut + 1
--                        rngOutput(intRowOut, 1) = rngInput(intRowIn, colRefnum)
--                        rngOutput(intRowOut, 2) = pstrUnitLabel
--                        strStartupResult = "Successful"
--                    Else
--                        ' Skip any consecutive events. If the end time of this event equal the start time of the next event,
--                        ' and the next event is not a Derate (Never let a derate be the initial outage.)
--                        ' and the next event is not a StartUp Failure,
--                        '       (Don't let the StartUp be the inital outage, let the original outage be the initial.
--                        '       The SetStartupResult logic will catch the Startup as being the subsequent failure.)
--                        ' then consider it a consecutive event and make it the Intial Outage.
--                        Do While (rngInput(intRowIn, colEND_TIME) = rngInput(intRowIn + 1, colSTART_TIME) _
--                            And Left(rngInput(intRowIn + 1, colEVNT_TYPE), 1) <> "D" _
--                            And rngInput(intRowIn + 1, colEVNT_TYPE) <> "SF")
                            
--                            ' Go ahead and output this consecutive event so the consultant can see it.
--                            OutputReviewLine rngInput, rngReview, intRowIn, "c"
--                            intRowIn = intRowIn + 1
--                        Loop
                        
--                        strStartupResult = SetStartupResult(rngInput, intRowIn)
--                    End If
--                Loop
                
--                ' If we have hit the next unit, capture the label and bump the row number that we are writing to
--                ' in the output range.
--                ' I don't think we need to do this again here. THEN AGAIN, MAYBE WE DO??
--                If pstrUnitLabel <> rngInput(intRowIn, colUnitLabel) Then
--                    pstrUnitLabel = rngInput(intRowIn, colUnitLabel)
--                    intRowOut = intRowOut + 1
--                    rngOutput(intRowOut, 1) = rngInput(intRowIn, colRefnum)
--                    rngOutput(intRowOut, 2) = pstrUnitLabel
--                End If
                
--                Select Case strStartupResult
                    
--                    Case "Successful"
--                        If rngReview.Cells(pintNextReviewLine, colEND_TIME + 1).Value = "" Then
--                            rngReview.Cells(pintNextReviewLine, colEND_TIME + 1).Value = "OK"
--                            rngReview.Cells(pintNextReviewLine, colEND_TIME + 2).Value = "OK"
--                        Else
--                            OutputReviewLine rngInput, rngReview, intRowIn, "OK"
--                        End If
                    
--                    Case "Derate"
--                        'NEXT ROW ADDED 8/12/02.
--                        intRow = pintOutageNextRow 'not sure what this intRow is used for, should be intRowIn or intRowOut? - SW 1/17/13
                    
--                    Case "Maint"
--                        'OutputReviewLine rngInput, rngReview, intRowIn, "Maint"
--                        Stop
                    
--                    Case "Forced"
--                        'OutputReviewLine rngInput, rngReview, intRowIn, "Forced"
--                        Stop
                    
--                    Case Else
--                        MsgBox "Invalid logic? " & strStartupResult
                
--                End Select

		END
		FETCH NEXT FROM rngInput INTO @inputID, @inputRefnum, @inputTurbineID, @inputUnitName, @inputCauseCode, @inputVerbalDesc86, @inputEventType, @inputStartDateTime, @inputEndDateTime
	END

	CLOSE rngInput
	DEALLOCATE rngInput
	CLOSE rngInputPlus
	DEALLOCATE rngInputPlus

--select * from #tmp
select * from #rngOutput

--    ' Format the end time column
--        Columns("T:T").Select
--        Selection.NumberFormat = "m/d/yy h:mm;@"
        
--    rngReview.Columns(7).NumberFormat = "m/d/yy h:mm"
--    MarkUnPreventables rngReview.Columns(colCAUSE_CODE)
--    rngOutput.Worksheet.Calculate
--    RebuildSlideData
--    rngReview.Activate
    
--End Sub


	--and clean up
	DROP TABLE #tmp
	DROP Table #rngOutput
	
END
