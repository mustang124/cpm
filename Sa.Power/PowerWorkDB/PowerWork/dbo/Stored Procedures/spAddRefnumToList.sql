﻿

/****** Object:  Stored Procedure dbo.spAddRefnumToList    Script Date: 4/18/2003 4:24:18 PM ******/


CREATE  PROCEDURE spAddRefnumToList
	@ListName RefListName = NULL, @RefListNo RefListNo = NULL, 
	@Refnum Refnum, @UserGroup char(5) = ''
AS
DECLARE @Access integer
IF @ListName IS NULL AND @RefListNo IS NULL 
	RAISERROR ('No refinery list supplied.', 16, -1)
IF @RefListNo IS NULL
	SELECT @RefListNo = RefListNo FROM RefList_LU
	WHERE ListName = @ListName
ELSE 
	SELECT @RefListNo = RefListNo FROM RefList_LU
	WHERE RefListNo = @RefListNo
IF @RefListNo IS NULL 
	RAISERROR ('Invalid refinery list.', 16, -1)
EXEC @Access = spCheckListOwner @ListNo = @RefListNo
IF @Access < 0
	RAISERROR ('Insufficient rights to modify list', 16, -1)
IF @UserGroup IS NULL
	SELECT @UserGroup = ''
INSERT INTO RefList (RefListNo, Refnum, UserGroup)
VALUES (@RefListNo, @Refnum, @UserGroup)


