﻿CREATE PROCEDURE dbo.sp_CalcMissingPrices(@ph Varchar(8)) AS

select  distinct 
      a.Starttime as OmittedDates
     
into #Temp
from revenueprices a 
where 
	datepart(dd,a.Starttime) < Dateadd(dd,-1, Dateadd(mm,1 ,Cast(Month(a.starttime) as varchar(2)) + '/1/'+ Cast(Year(a.starttime) as varchar(4))))
	and NOT EXISTS 
       (   select * 
	   from revenueprices b 
	   where 
	        datepart(mm,b.starttime)=datepart(mm,a.starttime) 
		and datepart(yy,b.starttime)=datepart(yy,a.starttime)
		and datepart(dd,b.starttime)=datepart(dd,a.starttime)
		and b.pricinghub=@ph
	)
and a.starttime >= ( select Min(starttime) from revenueprices where pricinghub=@ph) 
    
insert into RevenuePrices (price,pricinghub,starttime,endtime) 
/*select distinct Omitteddates from #Temp */
select  (yesterday.price+tomorrow.price)/2 as price, 
         yesterday.pricinghub,    
         Dateadd(dd,1,yesterday.starttime) as starttime,
         Dateadd(dd,1,yesterday.endtime) as endtime 
from 
     RevenuePrices yesterday,
     RevenuePrices tomorrow,
     #Temp t
 
where 
       yesterday.pricinghub=@ph and
       tomorrow.pricinghub=yesterday.pricinghub and 
       yesterday.starttime=Dateadd(dd,-1,t.OmittedDates) and 
       tomorrow.starttime=Dateadd(dd,1,t.OmittedDates)
order by yesterday.starttime       

Drop Table #Temp

