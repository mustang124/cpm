﻿CREATE PROC spMaintEquipSum(@Refnum Refnum) AS 
DECLARE @BOILER real, @TURBINE real, @CTG real, @OTHER real
DELETE FROM MaintEquipSum
WHERE Refnum = @Refnum
INSERT INTO MaintEquipSum (Refnum, CPHM, BOIL, HRSG, TURB, CA, VC, GEN, CTG,
	BAG, PREC, WGS, DGS, FHF, CWF, ASH, SCR, BOILH2O, WASTEH2O, Other)
SELECT @Refnum,
CPHM = AVG(CASE WHEN EquipGroup = 'CPHM' THEN AnnMaintCostKUS ELSE NULL END),
BOIL = AVG(CASE WHEN EquipGroup = 'BOIL' THEN AnnMaintCostKUS ELSE NULL END), 
HRSG = AVG(CASE WHEN EquipGroup = 'HRSG' THEN AnnMaintCostKUS ELSE NULL END),
TURB = AVG(CASE WHEN EquipGroup = 'TURB' THEN AnnMaintCostKUS ELSE NULL END),
CA = AVG(CASE WHEN EquipGroup = 'CA' THEN AnnMaintCostKUS ELSE NULL END),
VC = AVG(CASE WHEN EquipGroup = 'VC' THEN AnnMaintCostKUS ELSE NULL END),
GEN = AVG(CASE WHEN EquipGroup = 'GEN' THEN AnnMaintCostKUS ELSE NULL END),
CTG = AVG(CASE WHEN EquipGroup = 'CTG' THEN AnnMaintCostKUS ELSE NULL END),
BAG = AVG(CASE WHEN EquipGroup = 'BAG' THEN AnnMaintCostKUS ELSE NULL END),
PREC = AVG(CASE WHEN EquipGroup = 'PREC' THEN AnnMaintCostKUS ELSE NULL END),
WGS = AVG(CASE WHEN EquipGroup = 'WGS' THEN AnnMaintCostKUS ELSE NULL END),
DGS = AVG(CASE WHEN EquipGroup = 'DGS' THEN AnnMaintCostKUS ELSE NULL END),
FHF = AVG(CASE WHEN EquipGroup = 'FHF' THEN AnnMaintCostKUS ELSE NULL END),
CWF = AVG(CASE WHEN EquipGroup = 'CWF' THEN AnnMaintCostKUS ELSE NULL END),
ASH = AVG(CASE WHEN EquipGroup = 'ASH' THEN AnnMaintCostKUS ELSE NULL END),
SCR = AVG(CASE WHEN EquipGroup = 'SCR' THEN AnnMaintCostKUS ELSE NULL END),
BOILH2O = AVG(CASE WHEN EquipGroup = 'BOILH2O' THEN AnnMaintCostKUS ELSE NULL END),
WASTEH2O = AVG(CASE WHEN EquipGroup = 'WASTEH2O' THEN AnnMaintCostKUS ELSE NULL END),
OTHER = AVG(CASE WHEN EquipGroup = 'OTHER' THEN AnnMaintCostKUS ELSE NULL END)
FROM MaintEquipCalc
WHERE Refnum = @Refnum
SELECT @BOILER = SUM(CASE WHEN EquipGroup2 = 'BOILER' THEN AnnMaintCostKUS ELSE 0 END),
@TURBINE = SUM(CASE WHEN EquipGroup2 = 'TURBINE' THEN AnnMaintCostKUS ELSE 0 END),
@CTG = SUM(CASE WHEN EquipGroup2 = 'CTG' THEN AnnMaintCostKUS ELSE 0 END),
@OTHER = SUM(CASE WHEN EquipGroup2 = 'OTHER' THEN AnnMaintCostKUS ELSE 0 END)
FROM MaintIndexDetails
WHERE Refnum = @Refnum
UPDATE MaintEquipSum
SET MajorBoiler = @BOILER, MajorTurbine = @TURBINE, MajorCTG = @CTG, MajorOther = @OTHER
WHERE Refnum = @Refnum
GRANT EXEC ON spMaintEquipSum TO Datagrp
GRANT EXEC ON spMaintEquipSum TO Developer
