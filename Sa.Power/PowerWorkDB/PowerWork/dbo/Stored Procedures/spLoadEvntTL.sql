﻿




/****** Object:  Stored Procedure dbo.spLoadEvntTL    Script Date: 4/18/2003 4:29:24 PM ******/

/****** Object:  Stored Procedure dbo.spLoadEvntTL    Script Date: 12/28/2001 7:33:58 AM ******/
CREATE    PROCEDURE spLoadEvntTL (@UtilUnitCode char(6))

AS

DELETE FROM EvntTL WHERE UtilityUnitCode = @UtilUnitCode
AND EXISTS (SELECT * FROM LoadEvntTL x 
	WHERE x.UtilityUnitCode = @UtilUnitCode
	AND x.Evnt_Year = EvntTL.Evnt_Year)

INSERT INTO EVNTTL
SELECT * FROM LoadEVNTTL
WHERE UtilityUnitCode = @UtilUnitCode

DELETE FROM LoadEvntTL WHERE UtilityUnitCode = @UtilUnitCode







