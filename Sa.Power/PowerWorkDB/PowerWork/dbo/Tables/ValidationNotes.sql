﻿CREATE TABLE [dbo].[ValidationNotes] (
    [Refnum]                  [dbo].[Refnum] NOT NULL,
    [ValidationNotes]         TEXT           NULL,
    [ConsultingOpportunities] TEXT           NULL,
    CONSTRAINT [PK_ValidationNotes] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

