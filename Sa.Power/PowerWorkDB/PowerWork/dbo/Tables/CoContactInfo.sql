﻿CREATE TABLE [dbo].[CoContactInfo] (
    [SANumber]       INT           NOT NULL,
    [ContactType]    CHAR (5)      NOT NULL,
    [FirstName]      VARCHAR (25)  NULL,
    [LastName]       VARCHAR (25)  NULL,
    [JobTitle]       VARCHAR (75)  NULL,
    [PersonalTitle]  VARCHAR (5)   NULL,
    [Phone]          VARCHAR (40)  NULL,
    [PhoneSecondary] VARCHAR (40)  NULL,
    [Fax]            VARCHAR (40)  NULL,
    [Email]          VARCHAR (255) NULL,
    [StrAddr1]       VARCHAR (75)  NULL,
    [StrAddr2]       VARCHAR (50)  NULL,
    [StrAdd3]        VARCHAR (50)  NULL,
    [StrCity]        VARCHAR (30)  NULL,
    [StrState]       VARCHAR (30)  NULL,
    [StrZip]         VARCHAR (30)  NULL,
    [StrCountry]     VARCHAR (30)  NULL,
    [MailAddr1]      VARCHAR (75)  NULL,
    [MailAddr2]      VARCHAR (50)  NULL,
    [MailAddr3]      VARCHAR (50)  NULL,
    [MailCity]       VARCHAR (30)  NULL,
    [MailState]      VARCHAR (30)  NULL,
    [MailZip]        VARCHAR (30)  NULL,
    [MailCountry]    VARCHAR (30)  NULL,
    [AltFirstName]   VARCHAR (20)  NULL,
    [AltLastName]    VARCHAR (25)  NULL,
    [AltEmail]       VARCHAR (255) NULL,
    [CCAlt]          [dbo].[YorN]  CONSTRAINT [DF_CoContactInfo_CCAlt] DEFAULT ((1)) NULL,
    [SendMethod]     CHAR (3)      NULL,
    [Comment]        VARCHAR (255) NULL,
    CONSTRAINT [PK_CoContactInfo] PRIMARY KEY CLUSTERED ([SANumber] ASC, [ContactType] ASC) WITH (FILLFACTOR = 70)
);

