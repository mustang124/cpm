﻿CREATE TABLE [dbo].[SteamSales] (
    [Refnum]           [dbo].[Refnum]    NOT NULL,
    [SteamID]          TINYINT           NOT NULL,
    [Period]           [dbo].[GenPeriod] NOT NULL,
    [StmSales]         REAL              NULL,
    [StmPress]         REAL              NULL,
    [StmTemp]          REAL              NULL,
    [Enthalpy]         REAL              NULL,
    [StmSalesMWH]      FLOAT (53)        NULL,
    [StmSalesMBTU]     FLOAT (53)        NULL,
    [MarketValueLocal] REAL              NULL,
    [MarketValue]      REAL              NULL,
    [ReportedMBTU]     [dbo].[YorN]      NULL,
    [ExternalOrigin]   [dbo].[YorN]      NULL,
    CONSTRAINT [PK_SteamSales_1__13] PRIMARY KEY CLUSTERED ([Refnum] ASC, [SteamID] ASC, [Period] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'UOM is millions of pounds - FRS 10/2006', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SteamSales', @level2type = N'COLUMN', @level2name = N'StmSales';

