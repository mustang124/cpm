﻿CREATE TABLE [dbo].[CTGSupplement] (
    [Refnum]     [dbo].[Refnum]    NOT NULL,
    [TurbineID]  [dbo].[TurbineID] NOT NULL,
    [FiringTemp] REAL              NOT NULL,
    CONSTRAINT [PK_CTGSupplement] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC)
);

