﻿CREATE TABLE [dbo].[FuelProperties] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [FuelType]    CHAR (6)       NOT NULL,
    [Sulfur]      REAL           NULL,
    [Viscosity]   REAL           NULL,
    [Gravity]     REAL           NULL,
    [PourPt]      REAL           NULL,
    [Vanadium]    REAL           NULL,
    [Asphaltenes] REAL           NULL,
    CONSTRAINT [PK_FuelProperties] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FuelType] ASC)
);

