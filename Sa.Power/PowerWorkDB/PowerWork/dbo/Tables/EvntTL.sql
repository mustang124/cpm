﻿CREATE TABLE [dbo].[EvntTL] (
    [UtilityUnitCode] CHAR (6)      NOT NULL,
    [EVNT_YEAR]       SMALLINT      NOT NULL,
    [EVNT_NO]         SMALLINT      NOT NULL,
    [PHASE]           TINYINT       NOT NULL,
    [EVNT_TYPE]       CHAR (2)      NULL,
    [EVNT_GROUP]      CHAR (2)      NULL,
    [START_TIME]      SMALLDATETIME NULL,
    [END_TIME]        SMALLDATETIME NULL,
    [AVAIL_CAP]       SMALLINT      NULL,
    [LOSTMW]          SMALLINT      NULL,
    [DURATION]        REAL          NULL,
    [EVNT_Category]   CHAR (1)      NULL,
    [LRO]             REAL          NULL,
    [PeakHrs]         REAL          NULL,
    CONSTRAINT [PK_EvntTL] PRIMARY KEY CLUSTERED ([UtilityUnitCode] ASC, [EVNT_YEAR] ASC, [EVNT_NO] ASC, [PHASE] ASC)
);

