﻿CREATE TABLE [dbo].[Offline] (
    [Refnum]      VARCHAR (15)  NULL,
    [TurbineID]   VARCHAR (10)  NULL,
    [Util_Code]   CHAR (3)      NULL,
    [Unit_Code]   CHAR (3)      NULL,
    [evnt_type]   VARCHAR (8)   NULL,
    [event_start] SMALLDATETIME NULL,
    [event_end]   SMALLDATETIME NULL,
    [NAC]         INT           NULL,
    [GAC]         INT           NULL
);

