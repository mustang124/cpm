﻿CREATE TABLE [dbo].[WorkTblPriceCalc] (
    [UtilityUnitCode] CHAR (6)   NOT NULL,
    [UTIL_CODE]       CHAR (3)   NOT NULL,
    [UNIT_CODE]       CHAR (3)   NOT NULL,
    [EVNT_YEAR]       SMALLINT   NOT NULL,
    [EVNT_NO]         SMALLINT   NOT NULL,
    [PHASE]           TINYINT    NOT NULL,
    [PeakHrs]         FLOAT (53) NULL,
    [LRO]             FLOAT (53) NULL
);

