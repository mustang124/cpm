﻿CREATE TABLE [dbo].[CalcQueue] (
    [ListName]     [dbo].[RefListName] NOT NULL,
    [Calculate]    BIT                 NOT NULL,
    [Compare]      BIT                 NOT NULL,
    [Copy]         BIT                 NOT NULL,
    [ClientTables] BIT                 NOT NULL,
    [CalcSummary]  BIT                 NOT NULL,
    [MessageLog]   BIT                 NOT NULL,
    [EmailNotify]  BIT                 NOT NULL,
    [EMailMessage] VARCHAR (255)       NULL,
    CONSTRAINT [PK_CalcQueue] PRIMARY KEY CLUSTERED ([ListName] ASC)
);

