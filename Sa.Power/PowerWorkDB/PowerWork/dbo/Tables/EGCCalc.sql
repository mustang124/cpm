﻿CREATE TABLE [dbo].[EGCCalc] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [Method]         CHAR (10)      NOT NULL,
    [Technology]     CHAR (15)      NULL,
    [EGC]            REAL           NULL,
    [STGs]           TINYINT        NULL,
    [STG_NDC]        REAL           NULL,
    [CTGs]           TINYINT        NULL,
    [CTG_NDC]        REAL           NULL,
    [NDC]            REAL           NULL,
    [Scrubbers]      TINYINT        NULL,
    [PrecBag]        TINYINT        NULL,
    [AdjNetMWH]      REAL           NULL,
    [Sulfur]         REAL           NULL,
    [AshPcnt]        REAL           NULL,
    [FuelType]       VARCHAR (8)    NULL,
    [Coal_NDC]       REAL           NULL,
    [Oil_NDC]        REAL           NULL,
    [Gas_NDC]        REAL           NULL,
    [Starts]         REAL           NULL,
    [OilPcnt]        REAL           NULL,
    [GasPcnt]        REAL           NULL,
    [CoalPcnt]       REAL           NULL,
    [Age]            REAL           NULL,
    [NumUnitsAtSite] REAL           NULL,
    [ScrubbersYN]    TINYINT        NULL,
    [PrecYN]         TINYINT        NULL,
    [PrecBagYN]      TINYINT        NULL,
    CONSTRAINT [PK_EGCCalc] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Method] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Count of scrubbers', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EGCCalc', @level2type = N'COLUMN', @level2name = N'Scrubbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Count of Prec and Bag', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EGCCalc', @level2type = N'COLUMN', @level2name = N'PrecBag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Coal Sulfur', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EGCCalc', @level2type = N'COLUMN', @level2name = N'Sulfur';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Coal Ash', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EGCCalc', @level2type = N'COLUMN', @level2name = N'AshPcnt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'From Breaks table', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'EGCCalc', @level2type = N'COLUMN', @level2name = N'FuelType';

