﻿CREATE TABLE [dbo].[AvailableHours] (
    [Period]  DATETIME  NOT NULL,
    [Unavail] SMALLINT  NULL,
    [Refnum]  CHAR (10) NOT NULL,
    [Price]   MONEY     NULL,
    CONSTRAINT [PK_AvailableHours] PRIMARY KEY CLUSTERED ([Period] ASC, [Refnum] ASC)
);

