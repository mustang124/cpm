﻿CREATE TABLE [dbo].[ComponentEUFPlus] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [EquipGroup] CHAR (8)       NOT NULL,
    [EUF]        REAL           NULL,
    [EUOF]       REAL           NULL,
    [EPOF]       REAL           NULL,
    [E_PH]       REAL           NULL,
    CONSTRAINT [PK_ComponentEUFPlus] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EquipGroup] ASC)
);

