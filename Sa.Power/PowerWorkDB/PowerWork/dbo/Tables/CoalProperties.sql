﻿CREATE TABLE [dbo].[CoalProperties] (
    [Refnum]                 [dbo].[Refnum]    NOT NULL,
    [TurbineID]              [dbo].[TurbineID] NOT NULL,
    [CoalID]                 CHAR (2)          NOT NULL,
    [Name]                   CHAR (30)         NULL,
    [AshPcnt]                REAL              NULL,
    [SulfurPcnt]             REAL              NULL,
    [MoistPcnt]              REAL              NULL,
    [OnSitePrepCostTonLocal] REAL              NULL,
    CONSTRAINT [PK_CoalProperties] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [CoalID] ASC) WITH (FILLFACTOR = 90)
);

