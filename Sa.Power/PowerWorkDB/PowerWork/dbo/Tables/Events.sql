﻿CREATE TABLE [dbo].[Events] (
    [Refnum]        [dbo].[Refnum]    NOT NULL,
    [TurbineID]     [dbo].[TurbineID] NOT NULL,
    [EVNT_NO]       SMALLINT          NOT NULL,
    [EVNT_TYPE]     CHAR (2)          NOT NULL,
    [EVNT_GROUP]    CHAR (2)          NULL,
    [EVNT_Category] CHAR (1)          NULL,
    [PHASE]         TINYINT           NOT NULL,
    [START_TIME]    SMALLDATETIME     NULL,
    [END_TIME]      SMALLDATETIME     NULL,
    [AVAIL_CAP]     SMALLINT          NULL,
    [LOSTMW]        SMALLINT          NULL,
    [DURATION]      REAL              NULL,
    [CAUSE_CODE]    SMALLINT          NULL,
    CONSTRAINT [PK_Events] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [EVNT_NO] ASC, [PHASE] ASC)
);

