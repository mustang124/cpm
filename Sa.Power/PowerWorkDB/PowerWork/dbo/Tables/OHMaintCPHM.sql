﻿CREATE TABLE [dbo].[OHMaintCPHM] (
    [Refnum]   [dbo].[Refnum] NOT NULL,
    [Quantity] SMALLINT       NULL,
    [Repaired] SMALLINT       NULL,
    CONSTRAINT [PK___2__10] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

