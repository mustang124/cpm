﻿CREATE TABLE [dbo].[TSort] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [SiteID]           [dbo].[SiteID] NOT NULL,
    [CompanyID]        CHAR (15)      NOT NULL,
    [CompanyName]      CHAR (50)      NULL,
    [UnitName]         VARCHAR (50)   NOT NULL,
    [UnitLabel]        CHAR (5)       NULL,
    [UnitID]           CHAR (8)       NOT NULL,
    [CoLoc]            VARCHAR (100)  NULL,
    [StudyYear]        INT            NOT NULL,
    [EvntYear]         INT            NULL,
    [Continent]        VARCHAR (20)   NULL,
    [Country]          VARCHAR (50)   NULL,
    [Region]           CHAR (15)      NULL,
    [State]            CHAR (20)      NULL,
    [PricingHub]       CHAR (15)      NULL,
    [Regulated]        [dbo].[YorN]   NULL,
    [CalcCommUnavail]  [dbo].[YorN]   CONSTRAINT [DF_TSort_CalcCommUnavail] DEFAULT ('Y') NOT NULL,
    [EGCRegion]        CHAR (15)      NULL,
    [EGCTechnology]    VARCHAR (15)   NULL,
    [EGCManualTech]    BIT            NULL,
    [CTGs]             TINYINT        NULL,
    [CTG_NDC]          REAL           NULL,
    [NDC]              REAL           NULL,
    [Coal_NDC]         REAL           NULL,
    [Oil_NDC]          REAL           NULL,
    [Gas_NDC]          REAL           NULL,
    [PrecBagYN]        TINYINT        NULL,
    [ScrubbersYN]      TINYINT        NULL,
    [NumUnitsAtSite]   REAL           NULL,
    [FuelType]         VARCHAR (8)    NULL,
    [Metric]           BIT            NULL,
    [HHV]              BIT            NULL,
    [CurrencyID]       INT            NULL,
    [FormerCurrencyID] INT            NULL,
    [SingleShaft]      REAL           NULL,
    [NumSTs]           REAL           NULL,
    [DryCoolTower]     REAL           NULL,
    CONSTRAINT [PK_TSort] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE  trigger [dbo].[TSortCoLoc] on [dbo].[TSort] for insert, update 
AS
IF UPDATE(CompanyID) OR UPDATE(UnitName) OR UPDATE(UnitLabel)
	UPDATE TSort
	SET CoLoc = (rtrim([CompanyID]) + ' ' + char(150) + ' ' + rtrim([UnitName]) + ' (' + rtrim(isnull([UnitLabel],'')) + ')')
	WHERE Refnum IN (SELECT Refnum FROM inserted)

