﻿CREATE TABLE [dbo].[Equipment] (
    [Refnum]           [dbo].[Refnum]      NOT NULL,
    [TurbineID]        [dbo].[TurbineID]   NOT NULL,
    [EquipID]          [dbo].[EquipmentID] NOT NULL,
    [EquipType]        CHAR (8)            NOT NULL,
    [HRSGFiredBoilers] CHAR (1)            NULL,
    [EquipName]        VARCHAR (250)       NULL,
    CONSTRAINT [PK_Equipment_1__10] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [EquipID] ASC) WITH (FILLFACTOR = 90)
);

