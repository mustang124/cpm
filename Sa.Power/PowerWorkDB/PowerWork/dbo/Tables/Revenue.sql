﻿CREATE TABLE [dbo].[Revenue] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [Period]     SMALLDATETIME  NOT NULL,
    [AdjNetMWH]  REAL           NULL,
    [AvgPrice]   REAL           NULL,
    [RevenueKUS] REAL           NULL,
    CONSTRAINT [PK_Revenue] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Period] ASC)
);

