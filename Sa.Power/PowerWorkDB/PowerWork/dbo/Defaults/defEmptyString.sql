﻿CREATE DEFAULT [dbo].[defEmptyString]
    AS '';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defEmptyString]', @objname = N'[dbo].[ClientInfo].[PlantName]';

