﻿CREATE DEFAULT [dbo].[defReportedLHV]
    AS 'N';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defReportedLHV]', @objname = N'[dbo].[Fuel].[ReportedLHV]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defReportedLHV]', @objname = N'[dbo].[Coal].[ReportedLHV]';

