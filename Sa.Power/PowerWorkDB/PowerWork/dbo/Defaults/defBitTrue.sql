﻿CREATE DEFAULT [dbo].[defBitTrue]
    AS 1;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue].[Copy]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue].[ClientTables]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue].[CalcSummary]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue].[MessageLog]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitTrue]', @objname = N'[dbo].[CalcQueue].[Calculate]';

