﻿-------------------------------------------------------------------------------------------------------
--PURPOSE:    To Return Parts of the RefNum providing First Part is Numeric and Second Part is Alphabetic
--DATE:       10/27/2012
--WRITTEN BY: Greg Vestal
-------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[FormatRefNum] 
(
	@RefNum dbo.Refnum,
	@Mode int -- 0=Return Full RefNum, 1=Return RefNum Cycle, 2=Return Study, 3=Return Year
)
RETURNS varchar(12)
AS

BEGIN
	DECLARE @FirstPart varchar(4)
	DECLARE @SecondPart varchar(3)
	DECLARE @ThirdPart varchar(4)
	DECLARE @Part2Start tinyint, @Part2Size tinyint
	DECLARE @result varchar(12)
	
	SELECT @SecondPart = Study, @Part2Size = StudyCodeSize, @Part2Start = CHARINDEX(StudyCode, @Refnum, 1)
	FROM Console.RefnumStudyCodes
	WHERE CHARINDEX(StudyCode, @Refnum, 1) > 0
	
	SET @result = 'INVALID MODE'
	IF @Part2Start > 0
	BEGIN
		IF @SecondPart = 'CC'
		BEGIN
			IF SUBSTRING(@Refnum, @Part2Start + 1, 2) = 'CC'
				SET @Part2Start = @Part2Start + 1
		END
		SELECT @FirstPart = LEFT(@Refnum, @Part2Start-1), @ThirdPart = SUBSTRING(@Refnum, @Part2Start + @Part2Size, 4)
	
		IF @Mode=0
			SET @result =  @FirstPart + @SecondPart + @ThirdPart
			
		ELSE IF @Mode=1
			SET @result =  @FirstPart
		
		ELSE IF @Mode=2
			SET @result =  @SecondPart
			
		ELSE IF @Mode=3
			SET @result =  @ThirdPart
	END
	ELSE
		SET @result = 'INVALID REF'
		
	RETURN @result
END
