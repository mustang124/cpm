﻿


CREATE FUNCTION [dbo].[GetStageData]
(	
	@Refnum VARCHAR(12),
	@TabType CHAR(10),
	@ValueToFind VARCHAR(250),
	@XPlus INT,
	@YPlus INT
)
RETURNS TABLE 
AS
RETURN 
(
--clunky how it does the worksheet part 3 times. how to fix?
--needs the second and third because otherwise it may find @ValueToFind on a non-correct worksheet, and then pull the wrong numbers
--put the second and third in as a temporary fix (which means it will probably still be here ten years from now - today is 1/14/16)

	SELECT WorkSheet, CellX = CAST(CellX AS int), CellY = CAST(CellY AS int), Value
	FROM StageInput 
	WHERE Refnum = @Refnum
		AND WorkSheet IN (
			SELECT WorkSheet 
			FROM StageInput 
			WHERE (
				(@TabType = 'Plant' and Value = 'Plant Information') or --these are hardcoded from the study file
				(@TabType = 'STG' and Value = 'Steam Turbine Generator Information') or 
				(@TabType = 'CTG' and Value = 'Combustion Turbine Generator Information') or 
				(@TabType = 'OH' and Value = 'Table 3B - Scheduled Overhaul Maintenance Records')
				)
			)
		AND CellX IN (SELECT CellX + @XPlus FROM StageInput WHERE Refnum = @Refnum AND Value LIKE @ValueToFind AND WorkSheet IN (
			SELECT WorkSheet 
			FROM StageInput 
			WHERE (
				(@TabType = 'Plant' and Value = 'Plant Information') or --these are hardcoded from the study file
				(@TabType = 'STG' and Value = 'Steam Turbine Generator Information') or 
				(@TabType = 'CTG' and Value = 'Combustion Turbine Generator Information') or 
				(@TabType = 'OH' and Value = 'Table 3B - Scheduled Overhaul Maintenance Records')
				)
			))
		AND CellY IN (SELECT CellY + @YPlus FROM StageInput WHERE Refnum = @Refnum AND Value LIKE @ValueToFind AND WorkSheet IN (
			SELECT WorkSheet 
			FROM StageInput 
			WHERE (
				(@TabType = 'Plant' and Value = 'Plant Information') or --these are hardcoded from the study file
				(@TabType = 'STG' and Value = 'Steam Turbine Generator Information') or 
				(@TabType = 'CTG' and Value = 'Combustion Turbine Generator Information') or 
				(@TabType = 'OH' and Value = 'Table 3B - Scheduled Overhaul Maintenance Records')
				)
			))

)



