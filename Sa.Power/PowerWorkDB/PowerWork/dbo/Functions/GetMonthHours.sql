﻿-- =============================================
-- Author:		Mike L. Brown
-- Create date: 16 May 2017
-- Description:	Provide the number of hours in a month
--				starting from a given date
-- =============================================
CREATE FUNCTION [dbo].[GetMonthHours]
(
	-- Add the parameters for the function here
	@fromDate DATETIME
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @dtDate DATETIME
	,@periodHours int


	-- Add the T-SQL statements to compute the return value here
	SELECT @dtDate= dateadd(mm,datediff(mm,0,@fromDate),0)
	SELECT @periodHours = datediff(dd,@dtDate,dateadd(mm,1,@dtDate))* 24

	-- Return the result of the function
	RETURN @periodHours

END
