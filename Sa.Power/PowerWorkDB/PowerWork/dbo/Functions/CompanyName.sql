﻿
CREATE FUNCTION CompanyName
	(@CompanyID char(12))
RETURNS char(50)
AS
BEGIN
	DECLARE @CompanyName char(50)
	SELECT @CompanyName = CompanyName FROM Company_LU WHERE CompanyID = @CompanyID
	RETURN @CompanyName
END



