﻿
create  FUNCTION [dbo].[SiteIDBase](@SiteID SiteID)
RETURNS varchar(12)
AS
BEGIN 
	DECLARE @SiteIDBase varchar(12)

    SELECT @SiteIDBase = 
		CASE 
			WHEN SUBSTRING(@SiteID, LEN(RTRIM(@SiteID)),1)='P' THEN 
				 SUBSTRING(@SiteID, 1, LEN(RTRIM(@SiteID))-3) 
		ELSE 
			SUBSTRING(@SiteID, 1, LEN(RTRIM(@SiteID))-2) 
		END

    RETURN @SiteIDBase
END

