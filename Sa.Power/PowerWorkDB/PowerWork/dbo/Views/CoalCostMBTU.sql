﻿


/****** Object:  View dbo.CoalCostMBTU    Script Date: 5/3/01 2:21:43 PM ******/
CREATE  VIEW CoalCostMBTU AS 
SELECT	Refnum, MineCostMBTU = MineCostTon*(TotTons/MBTU), 
	TransCostMBTU = Isnull(TransCostTon*(TotTons/MBTU),0), 
	OnSitePrepCostMBTU = OnSitePrepCostTon*(TotTons/MBTU),
	TotCostMBTU = MineCostTon*(TotTons/MBTU) + Isnull(TransCostTon*(TotTons/MBTU),0),	MBTU,
	MineCostMBTULHV = MineCostTon*(TotTons/MBTULHV), 
	TransCostMBTULHV = Isnull(TransCostTon*(TotTons/MBTULHV),0), 
	OnSitePrepCostMBTULHV = OnSitePrepCostTon*(TotTons/MBTULHV),
	TotCostMBTULHV = MineCostTon*(TotTons/MBTU) + Isnull(TransCostTon*(TotTons/MBTULHV),0),	MBTULHV
from coaltotcalc



