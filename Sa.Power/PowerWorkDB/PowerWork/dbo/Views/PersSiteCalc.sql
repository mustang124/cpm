﻿


/****** Object:  View dbo.PersSiteCalc    Script Date: 4/18/2003 4:38:59 PM ******/
/******
MESSAGE: Microsoft SQL-DMO (ODBC SQLState: 42S02)
DESCRIPTION: [Microsoft][ODBC SQL Server Driver][SQL Server]Invalid object name 'dbo._vPerHrs'.
sysmessages error: '208', SCODE_SEVERITY: '1'
******/

-- ********** Statement #348 FAILED **********

/****** Object:  View dbo.PersSiteCalc    Script Date: 12/28/2001 7:35:16 AM ******/
/****** Object:  View dbo.PersSiteCalc    Script Date: 5/3/01 2:21:43 PM ******/
CREATE VIEW PersSiteCalc AS 
SELECT t.SiteID, p.PersCat, l.SortKey, Sum(p.SiteNumPers) AS SiteNumPers, Sum(p.SiteSTH) AS SiteSTH,
Sum(p.SiteOVTHrs) AS SiteOVTHrs, Sum(p.CentralNumPers) AS CentralNumPers, 
Sum(p.CentralSTH) AS CentralSTH, Sum(p.CentralOVTHrs) AS CentralOVTHrs, 
Sum(p.AGOnSite) AS AGOnSite, Sum(p.AGOthLoc) AS AGOthLoc, Sum(p.Contract) AS Contract
FROM Pers p, Pers_LU l, TSort t
WHERE p.Refnum = t.Refnum AND l.PersCat = p.PersCat
GROUP BY t.SiteID, p.PersCat, l.SortKey



