﻿




CREATE   VIEW [dbo].[_vEvntTL] AS

SELECT t.Refnum, n.TurbineID, EVNT_YEAR, EVNT_NO, PHASE, EVNT_TYPE, EVNT_GROUP, 
START_TIME, END_TIME, AVAIL_CAP, LOSTMW, DURATION, 
EVNT_Category, LRO, PeakHrs
FROM EVNTTL p 
INNER JOIN NERCTurbine n ON n.UtilityUnitCode = p.UtilityUnitCode
INNER JOIN TSort t ON t.Refnum = n.Refnum
AND p.EVNT_YEAR = t.EvntYear






