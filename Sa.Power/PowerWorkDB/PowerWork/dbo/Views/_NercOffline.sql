﻿
CREATE  VIEW dbo._NercOffline
AS
SELECT     dbo.NERCTurbine.Refnum, dbo.NERCTurbine.TurbineID, dbo.NERCTurbine.Util_Code, dbo.NERCTurbine.Unit_Code, dbo.Evnt01.EVNT_TYPE , 
                      DATEADD(mi, 60 * dbo.Evnt01.TIME_START, dbo.Evnt01.DATE_START) AS Event_Start , DATEADD(mi, 60 * dbo.Evnt01.TIME_END, 
                      dbo.Evnt01.DATE_END) AS Event_End,dbo.Evnt01.NAC,dbo.Evnt01.GAC
FROM         dbo.NERCTurbine INNER JOIN
                      dbo.TSort ON dbo.NERCTurbine.Refnum = dbo.TSort.Refnum INNER JOIN
                      dbo.Evnt01 ON dbo.NERCTurbine.Util_Code = dbo.Evnt01.UTIL_CODE AND dbo.NERCTurbine.Unit_Code = dbo.Evnt01.UNIT_CODE
WHERE    (dbo.Evnt01.EVNT_TYPE = 'RS') OR
               (ISNULL(dbo.Evnt01.NAC, 0) + ISNULL(dbo.Evnt01.GAC,0)) = 0


