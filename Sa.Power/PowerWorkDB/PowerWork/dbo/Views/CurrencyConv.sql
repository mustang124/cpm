﻿/****** Object:  View dbo.CurrencyConv    Script Date: 4/18/2003 4:38:54 PM *****
***** Object:  View dbo.CurrencyConv    Script Date: 12/28/2001 7:35:14 AM ******/
CREATE VIEW dbo.CurrencyConv
AS
SELECT     dbo.Currency_LU.CurrencyID, GlobalDB.dbo.CurrencyConv.[Year], GlobalDB.dbo.CurrencyConv.ConvRate, GlobalDB.dbo.CurrencyConv.SaveDate, 
                      GlobalDB.dbo.CurrencyConv.CurrencyCode
FROM         GlobalDB.dbo.CurrencyConv INNER JOIN
                      dbo.Currency_LU ON GlobalDB.dbo.CurrencyConv.CurrencyCode = dbo.Currency_LU.CurrencyCode
