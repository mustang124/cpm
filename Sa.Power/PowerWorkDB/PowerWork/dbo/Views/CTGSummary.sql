﻿CREATE VIEW dbo.CTGSummary
AS
SELECT     c.Refnum, c.TurbineID, n.NMC,
                          (SELECT     SUM(MBTU * 1000) AS Expr1
                            FROM          dbo.Fuel AS f
                            WHERE      (Refnum = c.Refnum) AND (TurbineID = c.TurbineID) AND (DATEPART(yy, Period) = t.StudyYear)) /
                          (SELECT     SUM(NetMWH) AS Expr1
                            FROM          dbo.PowerGeneration AS p
                            WHERE      (Refnum = c.Refnum) AND (TurbineID = c.TurbineID) AND (DATEPART(yy, Period) = t.StudyYear)) AS HeatRate,
                          (SELECT     SUM(MBTULHV * 1000) AS Expr1
                            FROM          dbo.Fuel AS f
                            WHERE      (Refnum = c.Refnum) AND (TurbineID = c.TurbineID) AND (DATEPART(yy, Period) = t.StudyYear)) /
                          (SELECT     SUM(NetMWH) AS Expr1
                            FROM          dbo.PowerGeneration AS p
                            WHERE      (Refnum = c.Refnum) AND (TurbineID = c.TurbineID) AND (DATEPART(yy, Period) = t.StudyYear)) AS HeatRateLHV,
                          (SELECT     SUM(NetMWH) AS Expr1
                            FROM          dbo.PowerGeneration AS p
                            WHERE      (Refnum = c.Refnum) AND (TurbineID = c.TurbineID) AND (DATEPART(yy, Period) = t.StudyYear)) AS NetMWH,
                          (SELECT     SUM(NetMWH) AS Expr1
                            FROM          dbo.PowerGeneration AS p
                            WHERE      (Refnum = c.Refnum) AND (TurbineID = c.TurbineID) AND (DATEPART(yy, Period) = t.StudyYear - 1)) AS PriorNetMWH, n.ServiceHrs, n.NCF,
                       n.NOF, n.NCF2Yr, n.NOF2Yr, n.WPH2Yr, n.NOF2YrWtFactor, s.SuccessPcnt, s.ForcedPcnt, s.MaintPcnt, s.StartupPcnt, s.DeratePcnt, s.TotalOpps
FROM         dbo.TSort AS t INNER JOIN
                      dbo.CTGData AS c ON c.Refnum = t.Refnum LEFT OUTER JOIN
                      dbo.NERCTurbine AS n ON n.Refnum = c.Refnum AND n.TurbineID = c.TurbineID LEFT OUTER JOIN
                      dbo.StartsAnalysis AS s ON s.Refnum = c.Refnum AND s.TurbineID = c.TurbineID
WHERE     ((SELECT     SUM(NetMWH) AS Expr1
                         FROM         dbo.PowerGeneration AS p
                         WHERE     (Refnum = c.Refnum) AND (TurbineID = c.TurbineID) AND (DATEPART(yy, Period) = t.StudyYear)) > 0)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[43] 4[18] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 203
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 64
               Left = 368
               Bottom = 179
               Right = 520
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "n"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 63
               Left = 679
               Bottom = 178
               Right = 831
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CTGSummary';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'CTGSummary';

