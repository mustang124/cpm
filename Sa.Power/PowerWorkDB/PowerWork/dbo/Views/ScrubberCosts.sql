﻿CREATE VIEW ScrubberCosts
AS
SELECT c.Refnum, SUM(m.ScrCostLime) AS ScrCostLime, SUM(m.ScrCostChem) AS ScrCostChem, c.ScrubberPowerCost, SUM(m.ScrCostNMaint) AS ScrCostNMaint, c.TotScrubberNonMaint
FROM Misc m INNER JOIN MiscCalc c ON m.Refnum = c.Refnum
WHERE c.TotScrubberNonMaint>0
GROUP BY c.Refnum, c.ScrubberPowerCost, c.TotScrubberNonMaint
