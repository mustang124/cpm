﻿

CREATE VIEW _vPedigree AS

SELECT t.Refnum, t.TurbineID, n.UNITABBR, n.UNITNAME, n.GMC, n.NMC,
n.COMMDATE, n.RETIREDATE, n.SHMETHOD, n.GRP01, n.GRP02, n.GRP03,
n.GRP04, n.GRP05, n.GRP06, n.GRP07, n.GRP08, n.GRP09, n.GRP10,
n.GRP11, n.GRP12, n.GRP13, n.GRP14, n.GRP15, n.GRP16, n.GRP17,
n.GRP18, n.GRP19, n.GRP20, n.GRP21, n.GRP22, n.GRP23, n.GRP24,
n.GRP25, n.GRP26, n.GRP27, n.GRP28, n.GRP29, n.GRP30, n.GRP31,
n.PERFDATE, n.PERFSTRT
FROM NERCTurbine t LEFT JOIN Pedigree n
ON t.Util_Code = n.Util_Code AND t.Unit_Code = n.Unit_Code



