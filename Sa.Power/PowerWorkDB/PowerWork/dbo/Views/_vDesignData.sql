﻿CREATE VIEW dbo._vDesignData
AS
SELECT     t.Refnum, d.Unit_Name, d.Service_Date, d.LC, d.Unit_Type, d.AE, d.Constr, d.BlrMfr, d.BlrMdl, d.BlrStmFl, d.BlrTemp, d.BlrPSIG, d.Firing, d.FurnTyp, 
                      d.Design_Fuel_1, d.Ave_Heat_1, d.__ASH_1, d.__SULFUR_1, d.__H2O_1, d.Ash_Soft_Temp_1, d.GHI_1, d._V___Ph_1, d.Design_Fuel_2, 
                      d.Ave_Heat_2, d.__ASH_2, d.__SULFUR_2, d.__H2O_2, d.Ash_Soft_Temp_2, d.GHI_2, d._V___Ph, d.BrnrMfrA, d.BrnrA, d.BrnrMfrB, d.BrnrB, 
                      d.LowNoMfr, d.LowNoTotal, d.LowNoMin, d.LowNoDate, d.CombMfr, d.BrnSMfr, d.IgMfr, d.IgFuel, d.IgTyp, d.CrshMfrA, d.StkrMfrA, d.CritCnvA, 
                      d.CrshMfrB, d.StkrMfrB, d.CritCnvB, d.FeedMfrA, d.FeedMtmA, d.FeedA, d.FeedTypA, d.FeedMfrB, d.FeedMtmB, d.FeedB, d.FeedTypB, d.PulvMfrA, 
                      d.PulvMdlA, d.PulvRtA, d.PulvA, d.PulvTot, d.PulvTypA, d.PulvMfrB, d.PulvMdlB, d.PulvRtB, d.PulvB, d.PulvMin, d.PulvTypB, d.DrftOrig, d.DrftDate, 
                      d.SootMfrA, d.SootA, d.SootTypA, d.SootQtyA, d.SootMfrB, d.SootB, d.SootTypB, d.SootQtyB, d.SootMfrC, d.SootC, d.SootTypC, d.SootQtyC, 
                      d.MPRPCMFR, d.MPRCPLOC, d.EPRCPMFR, d.EPRCPLOC, d.BGSYSMFR, d.BGFANMFR, d.BGFANMTM, d.BGFANTOT, d.BGSYSTYP, d.BGFANBLD, 
                      d.FAshMfr, d.FAshTyp, d.FGDMFR, d.FGD_Inst_Date, d.FGDORIG, d.FGDReag1, d.FGDReag2, d.TurbMfr, d.TurbOutd, d.Nameplat, d.TurbTyp, 
                      d.SHFT1MFR, d.SHFT2MFR, d.TURBCNFG, d.MAINTEMP, d.MAINPSIG, d.RHT1TEMP, d.RHT1PSIG, d.RHT2TEMP, d.RHT2PSIG, d.HPCase, d.HPCndBkp, 
                      d.HPIPCase, d.IPCase, d.IPLPCase, d.LPCase, d.LPCndBkp, d.LPBldLng, d.GENMFR, d.MAINKV, d.MAINMVA, d.MAINRPM, d.MAINPF, d.SHFT2KV, 
                      d.SHFT2MVA, d.SHFT2RPM, d.SHFT2PF, d.SHFT3KV, d.SHFT3MVA, d.SHFT3RPM, d.SHFT3PF, d.CndMfr, d.CndPass, d.CndShell, d.CndMat, d.ArMvMfr, 
                      d.ArMvTyp, d.CrcwTyp, d.CrcwOrig, d.PolshMfr, d.PolshCap, d.CpmpMfrA, d.CpmpMtmA, d.CpmpA, d.CpmpTot, d.CpmpMfrB, d.CpmpMtmB, d.CpmpB, 
                      d.CpmpMin, d.CBPMfrA, d.CBPMtmA, d.CBPmpA, d.CBPmpTot, d.CBPMfrB, d.CBPMtmB, d.CBPmpB, d.CBPmpMin, d.BFPMfrA, d.BFPrpmA, d.BFPmpA, 
                      d.BFPmpTot, d.BFPCapA, d.BFPMfrB, d.BFPrpmB, d.BFPmpB, d.BFPmpMin, d.BFPCapB, d.BFPDrvMA, d.BFPDrvTA, d.BFPCplTA, d.BFPDrvMB, 
                      d.BFPDrvTB, d.BFPCplTB, d.CTWRMFRA, d.CFANMFRA, d.CFANMTMA, d.CTWRTYPA, d.CTBPMFRA, d.CTBPMTMA, d.CTBPMPA, d.CTBPTOT, 
                      d.CTWRMFRB, d.CFANMFRB, d.CFANMTMB, d.CTWRTYPB, d.CTBPMFRB, d.CTBPMTMB, d.CTBPMPB, d.CTBPMIN, d.MXFMFRA, d.MXFA, d.MXFMVAA, 
                      d.MXFKVHA, d.MXFTYPA, d.MXFMFRB, d.MXFB, d.MXFMVAB, d.MXFKVHB, d.MXFTYPB, d.PPCMfrA, d.PPCmptrA, d.PPCLinkA, d.PPCCapA, d.PPCMfrB, 
                      d.PPCmptrB, d.PPCLinkB, d.PPCCapB, d.CT_Mfr, d.CT_Model, d.[#_of_CTs], d.CT_Gen_Mfr, d.HRSG_Mfr, d.HRSG_Model, d.[#_of_HRSGs], 
                      d.Condensing
FROM         dbo.NERCTurbine AS t LEFT OUTER JOIN
                      dbo.DesignData AS d ON t.UtilityUnitCode = d.UtilityUnitCode

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 121
               Right = 415
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'_vDesignData';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'_vDesignData';

