﻿
CREATE VIEW [dbo].[_RankView] AS
SELECT r.Refnum, ts.CoLoc, Variable = v.Description, r.Value,
r.Rank, s.DataCount, r.Percentile, r.Tile, s.NumTiles, 
l.ListName, BreakCondition = b.Description, s.BreakValue,
s.RankSpecsID, s.BreakID, s.RankVariableID /*, v.VariableID, v.Scenario, s.RefListNo*/
FROM Power.dbo.Rank r, Power.dbo.RankSpecs_LU s, Power.dbo.RankBreaks b, Power.dbo.RankVariables v,
	Power.dbo.RefList_LU l, Tsort ts
WHERE r.RankSpecsID = s.RankSpecsID
and s.BreakID = b.BreakID 
and s.RankVariableID = v.RankVariableID
and s.RefListNo = l.RefListNo
and r.Refnum = ts.Refnum



