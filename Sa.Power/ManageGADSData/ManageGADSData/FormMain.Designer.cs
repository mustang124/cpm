﻿namespace PowerCPA
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.ClockTimer = new System.Windows.Forms.Timer(this.components);
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusBarStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusFiller = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusBarLastProcess = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusBarProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.statusBarClock = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlBottom = new System.Windows.Forms.Panel();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnCreateLocalDatabase = new System.Windows.Forms.Button();
            this.btnUploadLocalDataToCPA = new System.Windows.Forms.Button();
            this.btnImportNonNERCExcel = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.lblMainStartupMessage = new System.Windows.Forms.Label();
            this.statusBar.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClockTimer
            // 
            this.ClockTimer.Tick += new System.EventHandler(this.OnTickClockTimer);
            // 
            // statusBar
            // 
            this.statusBar.AutoSize = false;
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusBarStatus,
            this.toolStripStatusFiller,
            this.statusBarLastProcess,
            this.statusBarProgressBar,
            this.statusBarClock});
            this.statusBar.Location = new System.Drawing.Point(0, 494);
            this.statusBar.MinimumSize = new System.Drawing.Size(200, 0);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(842, 22);
            this.statusBar.SizingGrip = false;
            this.statusBar.TabIndex = 19;
            // 
            // statusBarStatus
            // 
            this.statusBarStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.statusBarStatus.Name = "statusBarStatus";
            this.statusBarStatus.Size = new System.Drawing.Size(390, 17);
            this.statusBarStatus.Spring = true;
            this.statusBarStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusFiller
            // 
            this.toolStripStatusFiller.AutoSize = false;
            this.toolStripStatusFiller.Name = "toolStripStatusFiller";
            this.toolStripStatusFiller.Size = new System.Drawing.Size(5, 17);
            // 
            // statusBarLastProcess
            // 
            this.statusBarLastProcess.AutoSize = false;
            this.statusBarLastProcess.Name = "statusBarLastProcess";
            this.statusBarLastProcess.Size = new System.Drawing.Size(250, 17);
            this.statusBarLastProcess.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // statusBarProgressBar
            // 
            this.statusBarProgressBar.AutoSize = false;
            this.statusBarProgressBar.Name = "statusBarProgressBar";
            this.statusBarProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // statusBarClock
            // 
            this.statusBarClock.AutoSize = false;
            this.statusBarClock.Name = "statusBarClock";
            this.statusBarClock.Size = new System.Drawing.Size(80, 17);
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.btnAbout);
            this.pnlBottom.Controls.Add(this.btnCreateLocalDatabase);
            this.pnlBottom.Controls.Add(this.btnUploadLocalDataToCPA);
            this.pnlBottom.Controls.Add(this.btnImportNonNERCExcel);
            this.pnlBottom.Controls.Add(this.btnClose);
            this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBottom.Location = new System.Drawing.Point(0, 0);
            this.pnlBottom.Name = "pnlBottom";
            this.pnlBottom.Size = new System.Drawing.Size(842, 27);
            this.pnlBottom.TabIndex = 24;
            // 
            // btnAbout
            // 
            this.btnAbout.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAbout.Location = new System.Drawing.Point(571, 2);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(75, 23);
            this.btnAbout.TabIndex = 10;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.OnClickAbout);
            // 
            // btnCreateLocalDatabase
            // 
            this.btnCreateLocalDatabase.Location = new System.Drawing.Point(121, 2);
            this.btnCreateLocalDatabase.Name = "btnCreateLocalDatabase";
            this.btnCreateLocalDatabase.Size = new System.Drawing.Size(150, 23);
            this.btnCreateLocalDatabase.TabIndex = 5;
            this.btnCreateLocalDatabase.Text = "Create Local Database";
            this.btnCreateLocalDatabase.UseVisualStyleBackColor = true;
            this.btnCreateLocalDatabase.Click += new System.EventHandler(this.OnClickCreateLocalDatabase);
            // 
            // btnUploadLocalDataToCPA
            // 
            this.btnUploadLocalDataToCPA.Location = new System.Drawing.Point(421, 2);
            this.btnUploadLocalDataToCPA.Name = "btnUploadLocalDataToCPA";
            this.btnUploadLocalDataToCPA.Size = new System.Drawing.Size(150, 23);
            this.btnUploadLocalDataToCPA.TabIndex = 4;
            this.btnUploadLocalDataToCPA.Text = "Upload Local Data to CPA";
            this.btnUploadLocalDataToCPA.UseVisualStyleBackColor = true;
            this.btnUploadLocalDataToCPA.Click += new System.EventHandler(this.OnClickUploadLocalDataToCPA);
            // 
            // btnImportNonNERCExcel
            // 
            this.btnImportNonNERCExcel.Location = new System.Drawing.Point(271, 2);
            this.btnImportNonNERCExcel.Name = "btnImportNonNERCExcel";
            this.btnImportNonNERCExcel.Size = new System.Drawing.Size(150, 23);
            this.btnImportNonNERCExcel.TabIndex = 3;
            this.btnImportNonNERCExcel.Text = "Import Non-NERC Excel";
            this.btnImportNonNERCExcel.UseVisualStyleBackColor = true;
            this.btnImportNonNERCExcel.Click += new System.EventHandler(this.OnClickImportNonNERCExcel);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(646, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.OnClickClose);
            // 
            // pnlTop
            // 
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 27);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(842, 1);
            this.pnlTop.TabIndex = 26;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.lblMainStartupMessage);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 28);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(842, 466);
            this.pnlMain.TabIndex = 27;
            // 
            // lblMainStartupMessage
            // 
            this.lblMainStartupMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMainStartupMessage.Location = new System.Drawing.Point(25, 200);
            this.lblMainStartupMessage.Name = "lblMainStartupMessage";
            this.lblMainStartupMessage.Size = new System.Drawing.Size(793, 67);
            this.lblMainStartupMessage.TabIndex = 0;
            this.lblMainStartupMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 516);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlBottom);
            this.Controls.Add(this.statusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(850, 550);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "GADS Data Maintenance";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OnClosing);
            this.Resize += new System.EventHandler(this.OnResizeFormMain);
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.pnlBottom.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer ClockTimer;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusBarStatus;
        private System.Windows.Forms.ToolStripStatusLabel statusBarClock;
        private System.Windows.Forms.Panel pnlBottom;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Button btnCreateLocalDatabase;
        private System.Windows.Forms.Button btnUploadLocalDataToCPA;
        private System.Windows.Forms.Button btnImportNonNERCExcel;
        private System.Windows.Forms.Panel pnlMain;
        protected internal System.Windows.Forms.ToolStripProgressBar statusBarProgressBar;
        private System.Windows.Forms.Label lblMainStartupMessage;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.ToolStripStatusLabel statusBarLastProcess;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusFiller;

    }
}

