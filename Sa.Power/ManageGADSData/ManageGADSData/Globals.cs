using System;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Text;

using ComUtilsAlias = Common.Utility;
using ComUIUtilsAlias = Common.UI;
using DBUtilsAlias = Common.Database;

namespace PowerCPA
{
    internal partial class Globals
    {
        #region enums
        public enum Roles
        {
            Developer = 1000,
            Administrator = 500,
            PowerUser = 400,
            Consultant = 300
        };
        public enum DateComparisonResult
        {
            Earlier = -1,
            Later = 1,
            TheSame = 0
        };

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// For guidelines regarding the creation of new enum types, see
        ///     http://msdn2.microsoft.com/en-us/library/ms229058.aspx
        /// and
        ///     http://msdn2.microsoft.com/en-us/library/4x252001.aspx
        /// </remarks>
        public enum ProcessCtrlID
        {
            Empty = -1,
            ImportNonNERC,
            CreateLocalDatabase,
            UploadLocalDataToCPA
        }
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <remarks>
        ///// For guidelines regarding the creation of new enum types, see
        /////     http://msdn2.microsoft.com/en-us/library/ms229058.aspx
        ///// and
        /////     http://msdn2.microsoft.com/en-us/library/4x252001.aspx
        ///// </remarks>
        //public enum DatabaseType
        //{
        //    Access,
        //    SQL
        //}
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// For guidelines regarding the creation of new enum types, see
        ///     http://msdn2.microsoft.com/en-us/library/ms229058.aspx
        /// and
        ///     http://msdn2.microsoft.com/en-us/library/4x252001.aspx
        /// </remarks>
        public enum SetupRecordCheckReturn
        {
            Match,
            LocalGreater,
            LocalSmaller,
            NotAllValidated,
            AllLocalRecordsNotInSQL,
            ShortnameExistAlready,
            UUCExistAlready,
            SameNumberButDontMatch,
            NotSet
        }
        #endregion enums

        #region EventHandlers

        #region OnStatusMessageChanged
        public static event EventHandler StatusMessageChanged;
        /// <summary>
        /// Triggers the StatusMessageChanged event.
        /// </summary>
        public static void OnStatusMessageChanged()
        {
            if (StatusMessageChanged != null)
                StatusMessageChanged(Globals.StatusMessageChanged, new EventArgs());
        }
        #endregion OnStatusMessageChanged

        #region OnStatusBarLastProcessChanged
        public static event EventHandler StatusBarLastProcessChanged;
        /// <summary>
        /// Triggers the StatusBarLastProcessChanged event.
        /// </summary>
        public static void OnStatusBarLastProcessChanged()
        {
            if (StatusBarLastProcessChanged != null)
                StatusBarLastProcessChanged(Globals.StatusBarLastProcessChanged, new EventArgs());
        }
        #endregion OnStatusBarLastProcessChanged


        #region OnStatusBarProgressBarValueChanged
        public static event EventHandler StatusBarProgressBarValueChanged;
        /// <summary>
        /// Triggers the StatusBarProgressBarValueChanged event.
        /// </summary>
        public static void OnStatusBarProgressBarValueChanged()
        {
            if (StatusBarProgressBarValueChanged != null)
                StatusBarProgressBarValueChanged(Globals.StatusBarProgressBarValueChanged, new EventArgs());
        }
        #endregion OnStatusBarProgressBarValueChanged

        #region OnStatusBarProgressBarValueChanged
        public static event EventHandler StatusBarProgressBarMaxChanged;
        /// <summary>
        /// Triggers the StatusBarProgressBarValueChanged event.
        /// </summary>
        public static void OnStatusBarProgressBarMaxChanged()
        {
            if (StatusBarProgressBarMaxChanged != null)
                StatusBarProgressBarMaxChanged(Globals.StatusBarProgressBarMaxChanged, new EventArgs());
        }
        #endregion OnStatusBarProgressBarValueChanged


        #endregion EventHandlers

        #region Help Provider
        public HelpProvider helpProvider;

        #endregion Help Provider

        #region Constants
        public const string STR_MicrosoftJetOLEDB40 = "Microsoft.Jet.OLEDB.4.0";
        public const string STR_Admin = "Admin";
        public const string PROJECTDIRECTORY = @"XXCOMPANYIDXX\";

        #region Filenames

        #endregion Filenames

        #region Tablenames
        // setup type tables

        #endregion Tablenames

        #region encryption keys
        public static readonly string EncryptionKey = "F3l1u4e1g5g9e2G6r5o3u5p9";
        public static readonly string EncryptionIV = "3F1l4u1e5g9g2e6G5r3o5u9p";
        public static readonly string EncryptSelect = "S@Se1ect";
        public static readonly string EncryptExec = "S@3xec";
        #endregion encryption keys

        #endregion Constants

        #region Global Variables
        public static SqlConnectionStringBuilder sqlConnGD = new SqlConnectionStringBuilder();
        public static OleDbConnectionStringBuilder accessConnGD = new OleDbConnectionStringBuilder();

        public static string CurrentHubName = string.Empty;
        public static DataTable dtGADSNG = new DataTable("GADSNG");
        public static DataLayer.GADSNG dsGADSNG = new PowerCPA.DataLayer.GADSNG();
        public static DataTable dtErrors = new DataTable("Errors");
        public static DataTable dtErrSqlStatements = new DataTable("ErrorsSqlStatements");
        public static bool bFormCancel = false;
        public static bool bInitialLoad = true;
        public static bool bRunningLocal = false;
        public static bool BuildDefPerfRecords = true;
        public static List<string> Developers = new List<string>();
        public static List<string> Administrators = new List<string>();
        public static List<string> PowerUsers = new List<string>();

        public static string GADSDirectory = string.Empty;
        public static string GADSMasterDirectory = string.Empty;

        public static List<WorkListEvents> WorkListEvents = new List<WorkListEvents>();
        public static List<PerformanceData> WorkListPerfs = new List<PerformanceData>();
        public static string ExcelDataFile = string.Empty;
        public static string AccessDataFile = string.Empty;
        public static ProcessCtrlID CurrentProcessControl = ProcessCtrlID.Empty;
        public static StringBuilder SETUP_INSERT_BASE = new StringBuilder();
        public static StringBuilder EVENTDATA01_INSERT_BASE = new StringBuilder();
        public static StringBuilder EVENTDATA02_INSERT_BASE = new StringBuilder();
        public static StringBuilder PERFORMANCEDATA_INSERT_BASE = new StringBuilder();
        public static SetupRecordCheckReturn SetupCompareStatus = SetupRecordCheckReturn.Match;

        //private List<UniqueUtilityByYear> lstUniqueUtilityByYear = new List<UniqueUtilityByYear>();
        public static List<UniqueUtilityUnitsByYear> ListUniqueUtilityUnitsByYear = new List<UniqueUtilityUnitsByYear>();

        // common to both Events & Performance records
        public static DateTime TIMESTAMP;

        // used by Events only
        //public static string CAUSECODEEXT;
        public static byte CONTRIBCODE;
        public static bool PRIMARYALERT;
        public static short MANHOURSWORKED;
        public static DateTime REVMONTHCARD01;
        public static DateTime REVMONTHCARD02;
        public static DateTime REVMONTHCARD03;
        public static bool DOMINANTDERATE;
        public static char REVISIONCARD01;
        public static char REVISIONCARD02;
        public static char REVISIONCARD03;
        public static bool CARRYOVERLASTYEAR;
        public static bool CARRYOVERNEXTYEAR;
        public static string VERBALDESC1;

        // used by Performance only
        public static decimal GROSSMAXCAP;
        public static decimal GROSSDEPCAP;
        public static decimal GROSSGEN;
        public static int ATTEMPTEDSTARTS;
        public static int ACTUALSTARTS;
        public static byte TYPUNITLOADING;
        public static int PRIBTUS;
        public static int SECBTUS;
        public static int TERBTUS;
        public static int QUABTUS;
        public static DateTime REVMONTHCARD1;
        public static DateTime REVMONTHCARD2;
        public static DateTime REVMONTHCARD3;
        public static DateTime REVMONTHCARD4;
        public static int INACTIVEHOURS;
        public static decimal RSHOURS;
        public static decimal PUMPINGHOURS;
        public static decimal SYNCHCONDHOURS;
        public static decimal PLANNEDOUTAGEHOURS;
        public static decimal FORCEDOUTAGEHOURS;
        public static decimal MAINTOUTAGEHOURS;
        public static decimal EXTOFSCHEDOUTAGES;
        public static char REVISIONCARD1;
        public static char REVISIONCARD2;
        public static char REVISIONCARD3;
        public static char REVISIONCARD4;

        #region Column names for Events Template

        // Column names for Events Template
        public const string CN_EVENTNUMBER = "EVENTNUMBER";
        public const string CN_EVENTTYPE = "EVENTTYPE";
        public const string CN_STARTDATETIME = "STARTDATETIME";
        public const string CN_ENDDATETIME = "ENDDATETIME";
        public const string CN_CAUSECODE = "CAUSECODE";
        public const string CN_CAUSECODEEXT = "CAUSECODEEXT";
        public const string CN_VERBALDESC1 = "VERBALDESC1";
        public const string CN_VERBALDESC2 = "VERBALDESC2";
        public const string CN_VERBALDESC86 = "VERBALDESC86";
        public const string CN_VERBALDESCFULL = "VERBALDESCFULL";
        public const string CN_NETAVAILCAPACITY = "NETAVAILCAPACITY";
        public const string CN_NETAVAILCAPAC = "NETAVAILCAPAC";
        #endregion

        // Event data 
        // columns in the CPA version of the file
        // EventNumber	VerbalDesc2	UtilityUnitCode	EventType	StartDateTime	EndDateTime	Year	CauseCode	VerbalDesc86	NetAvailCapacity

        // columns in the Ron version of the file
        // UtilityUnitCode	UnitShortName	Year	EventNumber	EventType	StartDateTime	EndDateTime	GrossAvailCapacity	NetAvailCapacity	CauseCode	
        //  VerbalDesc86

        // Performance data
        // columns 
        // UtilityUnitCode	UnitShortName	Year	Period	GrossMaxCap	GrossDepCap	GrossGen	NetMaxCap	NetDepCap	NetGen	AttemptedStarts	ActualStarts	TypUnitLoading	
        //  VerbalDesc	ServiceHours	PeriodHours	PriFuelCode	PriQtyBurned	PriAvgHeatContent	PriBtus	PriPercentAsh	PriPercentMoisture	PriPercentSulfur	PriPercentAlkalines	
        //  PriGrindIndexVanad	PriAshSoftTemp	SecFuelCode	SecQtyBurned	SecAvgHeatContent	TerFuelCode	TerQtyBurned	TerAvgHeatContent	QuaFuelCode	QuaQtyBurned	QuaAvgHeatContent	
        //  QuaBtus	QuaPercentAsh	QuaPercentMoisture	QuaPercentSulfur	QuaPercentAlkalines	QuaGrindIndexVanad	QuaAshSoftTemp	RevMonthCard1	RevMonthCard2	RevMonthCard3	RevMonthCard4	
        //  TimeStamp	InactiveHours	RSHours	PumpingHours	SynchCondHours	PlannedOutageHours	ForcedOutageHours	MaintOutageHours	ExtofSchedOutages	
        //  RevisionCard1	RevisionCard2	RevisionCard3	RevisionCard4


        #region default Column number for Events Template
        // default Column number for Events Template
        public static int col_eventnumber = 0;
        public static int col_verbaldesc2 = 1;
        public static int col_utilityunitcode = 2;
        public static int col_eventtype = 3;
        public static int col_startdatetime = 4;
        public static int col_enddatetime = 5;
        public static int col_year = 6;
        public static int col_causecode = 7;
        public static int col_verbaldesc86 = 8;
        public static int col_netavailcapacity = 9;

        #endregion

        public static int headerRow = 20;

        // Column names common to both Templates
        public const string CN_UTILITYUNITCODE = "UTILITYUNITCODE";
        public const string CN_YEAR = "YEAR";

        #region Column names for Performance Template
        // Column names for Performance Template
        public const string CN_UNITSHORTNAME = "UNITSHORTNAME";
        public const string CN_PERIOD = "PERIOD";
        public const string CN_GROSSMAXCAP = "GROSSMAXCAP";
        public const string CN_GROSSDEPCAP = "GROSSDEPCAP";
        public const string CN_GROSSGEN = "GROSSGEN";
        public const string CN_NETMAXCAP = "NETMAXCAP";
        public const string CN_NETDEPCAP = "NETDEPCAP";
        public const string CN_NETGEN = "NETGEN";
        public const string CN_ATTEMPTEDSTARTS = "ATTEMPTEDSTARTS";
        public const string CN_ACTUALSTARTS = "ACTUALSTARTS";
        public const string CN_TYPUNITLOADING = "TYPUNITLOADING";
        public const string CN_VERBALDESC = "VERBALDESC";
        public const string CN_SERVICEHOURS = "SERVICEHOURS";
        public const string CN_PERIODHOURS = "PERIODHOURS";
        public const string CN_PRIFUELCODE = "PRIFUELCODE";
        public const string CN_PRIQTYBURNED = "PRIQTYBURNED";
        public const string CN_PRIAVGHEATCONTENT = "PRIAVGHEATCONTENT";
        public const string CN_PRIBTUS = "PRIBTUS";
        public const string CN_PRIPERCENTASH = "PRIPERCENTASH";
        public const string CN_PRIPERCENTMOISTURE = "PRIPERCENTMOISTURE";
        public const string CN_PRIPERCENTSULFUR = "PRIPERCENTSULFUR";
        public const string CN_PRIPERCENTALKALINES = "PRIPERCENTALKALINES";
        public const string CN_PRIGRINDINDEXVANAD = "PRIGRINDINDEXVANAD";
        public const string CN_PRIASHSOFTTEMP = "PRIASHSOFTTEMP";
        public const string CN_SECFUELCODE = "SECFUELCODE";
        public const string CN_SECQTYBURNED = "SECQTYBURNED";
        public const string CN_SECAVGHEATCONTENT = "SECAVGHEATCONTENT";
        public const string CN_SECBTUS = "SecBtus";
        public const string CN_SECPERCENTASH = "SecPercentAsh";
        public const string CN_SECPERCENTMOISTURE = "SecPercentMoisture";
        public const string CN_SECPERCENTSULFUR = "SecPercentSulfur";
        public const string CN_SECPERCENTALKALINES = "SecPercentAlkalines";
        public const string CN_SECGRINDINDEXVANAD = "SecGrindIndexVanad";
        public const string CN_SECASHSOFTTEMP = "SecAshSoftTemp";
        public const string CN_TERFUELCODE = "TerFuelCode";
        public const string CN_TERQTYBURNED = "TerQtyBurned";
        public const string CN_TERAVGHEATCONTENT = "TerAvgHeatContent";
        public const string CN_TERBTUS = "TerBtus";
        public const string CN_TERPERCENTASH = "TerPercentAsh";
        public const string CN_TERPERCENTMOISTURE = "TerPercentMoisture";
        public const string CN_TERPERCENTSULFUR = "TerPercentSulfur";
        public const string CN_TERPERCENTALKALINES = "TerPercentAlkalines";
        public const string CN_TERGRINDINDEXVANAD = "TerGrindIndexVanad";
        public const string CN_TERASHSOFTTEMP = "TerAshSoftTemp";
        public const string CN_QUAFUELCODE = "QuaFuelCode";
        public const string CN_QUAQTYBURNED = "QuaQtyBurned";
        public const string CN_QUAAVGHEATCONTENT = "QuaAvgHeatContent";
        public const string CN_QUABTUS = "QuaBtus";
        public const string CN_QUAPERCENTASH = "QuaPercentAsh";
        public const string CN_QUAPERCENTMOISTURE = "QuaPercentMoisture";
        public const string CN_QUAPERCENTSULFUR = "QuaPercentSulfur";
        public const string CN_QUAPERCENTALKALINES = "QuaPercentAlkalines";
        public const string CN_QUAGRINDINDEXVANAD = "QuaGrindIndexVanad";
        public const string CN_QUAASHSOFTTEMP = "QuaAshSoftTemp";
        public const string CN_REVMONTHCARD1 = "RevMonthCard1";
        public const string CN_REVMONTHCARD2 = "RevMonthCard2";
        public const string CN_REVMONTHCARD3 = "RevMonthCard3";
        public const string CN_REVMONTHCARD4 = "RevMonthCard4";
        public const string CN_TIMESTAMP = "TimeStamp";
        public const string CN_INACTIVEHOURS = "InactiveHours";
        public const string CN_RSHOURS = "RSHours";
        public const string CN_PUMPINGHOURS = "PumpingHours";
        public const string CN_SYNCHCONDHOURS = "SynchCondHours";
        public const string CN_PLANNEDOUTAGEHOURS = "PlannedOutageHours";
        public const string CN_FORCEDOUTAGEHOURS = "ForcedOutageHours";
        public const string CN_MAINTOUTAGEHOURS = "MaintOutageHours";
        public const string CN_EXTOFSCHEDOUTAGES = "ExtofSchedOutages";
        public const string CN_REVISIONCARD1 = "RevisionCard1";
        public const string CN_REVISIONCARD2 = "RevisionCard2";
        public const string CN_REVISIONCARD3 = "RevisionCard3";
        public const string CN_REVISIONCARD4 = "RevisionCard4";
        #endregion

        #endregion Global Variables

        #region Local Variables
        private static string _currentUser = That.User.Name;
        private static int _userRole = (int)Roles.Consultant;
        private static string _currentYear;
        private static string _initialDirectory = string.Empty;
        private static bool _FuelUpdates;
        private static bool _UnitsToCopy;
        private static bool _InflationFactor;
        private static bool _CalcDatabase;
        private static string _StatusMessageText = string.Empty;
        private static string _StatusBarLastProcessText = string.Empty;
        private static int _StatusBarProgressBarValue = 0;
        private static int _StatusBarProgressBarMax = 0;
        #endregion Local Variables

        #region Public Properties
        public static string CurrentUser
        {
            get { return (_currentUser); }
            set
            {
                _currentUser = value;
                // now set the UserRole
                if (Administrators.Contains(_currentUser))
                    _userRole = (int)Roles.Administrator;
                else if (Developers.Contains(_currentUser))
                    _userRole = (int)Roles.Developer;
                else if (PowerUsers.Contains(_currentUser))
                    _userRole = (int)Roles.PowerUser;
            }
        }
        public static int UserRole
        {
            get { return _userRole; }
            set { _userRole = value; }
        }
        public static string CurrentYear
        {
            get { return _currentYear; }
            set { _currentYear = value; }
        }
        public static string InitialDirectory
        {
            get { return _initialDirectory; }
            set
            {
                _initialDirectory = value;
                if (!_initialDirectory.EndsWith(@"\"))
                {
                    _initialDirectory += @"\";
                }
            }
        }

        public static bool UnitsToCopy
        {
            get { return _UnitsToCopy; }
            set { _UnitsToCopy = value; }
        }
        public static bool InflationFactor
        {
            get { return _InflationFactor; }
            set { _InflationFactor = value; }
        }
        public static bool FuelUpdates
        {
            get { return _FuelUpdates; }
            set { _FuelUpdates = value; }
        }
        public static bool CalcDatabase
        {
            get { return _CalcDatabase; }
            set { _CalcDatabase = value; }
        }
        /// <summary>
        /// Gets or sets the StatusMessageText.
        /// </summary>
        /// <value>The StatusMessageText.</value>
        public static string StatusMessageText
        {
            get
            {
                return _StatusMessageText;
            }
            set
            {
                _StatusMessageText = value;
                Globals.OnStatusMessageChanged();
            }
        }

        /// <summary>
        /// Gets or sets the StatusBarLastProcessText.
        /// </summary>
        /// <value>The StatusBarLastProcessText.</value>
        public static string StatusBarLastProcessText
        {
            get
            {
                return _StatusBarLastProcessText;
            }
            set
            {
                _StatusBarLastProcessText = value;
                Globals.OnStatusBarLastProcessChanged();
            }
        }       /// <summary>
        /// Gets or sets the StatusBarProgressBarValue.
        /// </summary>
        /// <value>The StatusBarProgressBarValue.</value>
        public static int StatusBarProgressBarValue
        {
            get
            {
                return _StatusBarProgressBarValue;
            }
            set
            {
                _StatusBarProgressBarValue = value;
                Globals.OnStatusBarProgressBarValueChanged();
            }
        }
        /// <summary>
        /// Gets or sets the StatusBarProgressBarValue.
        /// </summary>
        /// <value>The StatusBarProgressBarValue.</value>
        public static int StatusBarProgressBarMax
        {
            get
            {
                return _StatusBarProgressBarMax;
            }
            set
            {
                _StatusBarProgressBarMax = value;
                Globals.OnStatusBarProgressBarMaxChanged();
            }
        }

        #endregion Public Properties

        #region Constructors
        internal Globals()
        {
            this.helpProvider = new System.Windows.Forms.HelpProvider();
        }
        #endregion Constructors

        #region Events Raised

        #endregion Events Raised

        #region Public Methods
        public static void AddErrorToTable(string _uuc, int _row, string _desc)
        {
            DataRow workRow;
            workRow = Globals.dtErrors.NewRow();
            workRow["UtilityUnitCode"] = _uuc;
            workRow["Line"] = _row + 1;
            workRow["Error"] = _desc;
            Globals.dtErrors.Rows.Add(workRow);
        }
        public static void AddErrorToTable(string _tabname, string _uuc, int _row, string _desc)
        {
            DataRow workRow;
            workRow = Globals.dtErrors.NewRow();
            workRow["TabName"] = _tabname;
            workRow["UtilityUnitCode"] = _uuc;
            workRow["Line"] = _row ;
            workRow["Error"] = _desc;
            Globals.dtErrors.Rows.Add(workRow);
        }
        public static void AddErrorToSQLErrorTable(string _sql)
        {
            DataRow workRow;
            workRow = Globals.dtErrSqlStatements.NewRow();
            workRow["SqlStatement"] = _sql;
            Globals.dtErrSqlStatements.Rows.Add(workRow);
        }

        #endregion Public Methods

        #region Private Methods

        #endregion Private Methods

    }
}
