﻿namespace PowerCPA.UserControls
{
    partial class ctrlImportNonNERC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlTopImportType = new System.Windows.Forms.Panel();
            this.pnlTopButtons = new System.Windows.Forms.Panel();
            this.btnSelectTabs = new System.Windows.Forms.Button();
            this.btnImportData = new System.Windows.Forms.Button();
            this.btnViewData = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkBuildDefaultPerfRecords = new System.Windows.Forms.CheckBox();
            this.radPerfData = new System.Windows.Forms.RadioButton();
            this.radEventData = new System.Windows.Forms.RadioButton();
            this.pnlTopFilename = new System.Windows.Forms.Panel();
            this.cmbCompanyID = new System.Windows.Forms.ComboBox();
            this.lblCompanyID = new System.Windows.Forms.Label();
            this.txtAccessFile = new Common.CustomControls.MyTextBox();
            this.txtExcelFile = new Common.CustomControls.MyTextBox();
            this.lblExcelFile = new System.Windows.Forms.Label();
            this.btnFindDataFile = new System.Windows.Forms.Button();
            this.lblControlTitle = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlExcelCheckList = new System.Windows.Forms.Panel();
            this.lstbxExcelCheckList = new System.Windows.Forms.ListBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.datagridGADSData = new System.Windows.Forms.DataGridView();
            this.pnlTop.SuspendLayout();
            this.pnlTopImportType.SuspendLayout();
            this.pnlTopButtons.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlTopFilename.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlExcelCheckList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridGADSData)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.pnlTopImportType);
            this.pnlTop.Controls.Add(this.pnlTopFilename);
            this.pnlTop.Controls.Add(this.lblControlTitle);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(842, 125);
            this.pnlTop.TabIndex = 24;
            // 
            // pnlTopImportType
            // 
            this.pnlTopImportType.Controls.Add(this.pnlTopButtons);
            this.pnlTopImportType.Controls.Add(this.groupBox1);
            this.pnlTopImportType.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopImportType.Location = new System.Drawing.Point(0, 69);
            this.pnlTopImportType.Name = "pnlTopImportType";
            this.pnlTopImportType.Size = new System.Drawing.Size(842, 55);
            this.pnlTopImportType.TabIndex = 23;
            // 
            // pnlTopButtons
            // 
            this.pnlTopButtons.Controls.Add(this.btnSelectTabs);
            this.pnlTopButtons.Controls.Add(this.btnImportData);
            this.pnlTopButtons.Controls.Add(this.btnViewData);
            this.pnlTopButtons.Location = new System.Drawing.Point(409, 1);
            this.pnlTopButtons.Name = "pnlTopButtons";
            this.pnlTopButtons.Size = new System.Drawing.Size(385, 52);
            this.pnlTopButtons.TabIndex = 24;
            // 
            // btnSelectTabs
            // 
            this.btnSelectTabs.Enabled = false;
            this.btnSelectTabs.Location = new System.Drawing.Point(11, 17);
            this.btnSelectTabs.Name = "btnSelectTabs";
            this.btnSelectTabs.Size = new System.Drawing.Size(121, 23);
            this.btnSelectTabs.TabIndex = 21;
            this.btnSelectTabs.Text = "Select Excel Tabs";
            this.btnSelectTabs.UseVisualStyleBackColor = true;
            this.btnSelectTabs.Click += new System.EventHandler(this.OnClickSelectTabs);
            // 
            // btnImportData
            // 
            this.btnImportData.Enabled = false;
            this.btnImportData.Location = new System.Drawing.Point(253, 17);
            this.btnImportData.Name = "btnImportData";
            this.btnImportData.Size = new System.Drawing.Size(121, 23);
            this.btnImportData.TabIndex = 0;
            this.btnImportData.Text = "Save to Local GADS";
            this.btnImportData.UseVisualStyleBackColor = true;
            this.btnImportData.Click += new System.EventHandler(this.OnClickImportData);
            // 
            // btnViewData
            // 
            this.btnViewData.Enabled = false;
            this.btnViewData.Location = new System.Drawing.Point(132, 17);
            this.btnViewData.Name = "btnViewData";
            this.btnViewData.Size = new System.Drawing.Size(121, 23);
            this.btnViewData.TabIndex = 20;
            this.btnViewData.Text = "Read Excel Data";
            this.btnViewData.UseVisualStyleBackColor = true;
            this.btnViewData.Click += new System.EventHandler(this.OnClickViewData);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkBuildDefaultPerfRecords);
            this.groupBox1.Controls.Add(this.radPerfData);
            this.groupBox1.Controls.Add(this.radEventData);
            this.groupBox1.Location = new System.Drawing.Point(48, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(361, 46);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Select Type of Data to Import ";
            // 
            // chkBuildDefaultPerfRecords
            // 
            this.chkBuildDefaultPerfRecords.Checked = true;
            this.chkBuildDefaultPerfRecords.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBuildDefaultPerfRecords.ForeColor = System.Drawing.Color.Red;
            this.chkBuildDefaultPerfRecords.Location = new System.Drawing.Point(220, 12);
            this.chkBuildDefaultPerfRecords.Name = "chkBuildDefaultPerfRecords";
            this.chkBuildDefaultPerfRecords.Size = new System.Drawing.Size(134, 30);
            this.chkBuildDefaultPerfRecords.TabIndex = 2;
            this.chkBuildDefaultPerfRecords.Text = "Build Default Performance Records";
            this.chkBuildDefaultPerfRecords.UseVisualStyleBackColor = true;
            this.chkBuildDefaultPerfRecords.CheckedChanged += new System.EventHandler(this.OnCheckedChangedBuildDefaultPerfRecords);
            // 
            // radPerfData
            // 
            this.radPerfData.AutoSize = true;
            this.radPerfData.Enabled = false;
            this.radPerfData.Location = new System.Drawing.Point(102, 20);
            this.radPerfData.Name = "radPerfData";
            this.radPerfData.Size = new System.Drawing.Size(111, 17);
            this.radPerfData.TabIndex = 1;
            this.radPerfData.TabStop = true;
            this.radPerfData.Text = "Performance Data";
            this.radPerfData.UseVisualStyleBackColor = true;
            this.radPerfData.Click += new System.EventHandler(this.OnClickPerfData);
            // 
            // radEventData
            // 
            this.radEventData.AutoSize = true;
            this.radEventData.Location = new System.Drawing.Point(15, 20);
            this.radEventData.Name = "radEventData";
            this.radEventData.Size = new System.Drawing.Size(79, 17);
            this.radEventData.TabIndex = 0;
            this.radEventData.TabStop = true;
            this.radEventData.Text = "Event Data";
            this.radEventData.UseVisualStyleBackColor = true;
            this.radEventData.Click += new System.EventHandler(this.OnClickEventData);
            // 
            // pnlTopFilename
            // 
            this.pnlTopFilename.Controls.Add(this.cmbCompanyID);
            this.pnlTopFilename.Controls.Add(this.lblCompanyID);
            this.pnlTopFilename.Controls.Add(this.txtAccessFile);
            this.pnlTopFilename.Controls.Add(this.txtExcelFile);
            this.pnlTopFilename.Controls.Add(this.lblExcelFile);
            this.pnlTopFilename.Controls.Add(this.btnFindDataFile);
            this.pnlTopFilename.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopFilename.Location = new System.Drawing.Point(0, 23);
            this.pnlTopFilename.Name = "pnlTopFilename";
            this.pnlTopFilename.Size = new System.Drawing.Size(842, 46);
            this.pnlTopFilename.TabIndex = 22;
            // 
            // cmbCompanyID
            // 
            this.cmbCompanyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCompanyID.FormattingEnabled = true;
            this.cmbCompanyID.Location = new System.Drawing.Point(98, 23);
            this.cmbCompanyID.Name = "cmbCompanyID";
            this.cmbCompanyID.Size = new System.Drawing.Size(196, 21);
            this.cmbCompanyID.TabIndex = 23;
            this.cmbCompanyID.SelectedIndexChanged += new System.EventHandler(this.OnSelectedIndexChangedCompanyID);
            // 
            // lblCompanyID
            // 
            this.lblCompanyID.Location = new System.Drawing.Point(13, 23);
            this.lblCompanyID.Name = "lblCompanyID";
            this.lblCompanyID.Size = new System.Drawing.Size(85, 20);
            this.lblCompanyID.TabIndex = 22;
            this.lblCompanyID.Text = "CompanyID";
            this.lblCompanyID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAccessFile
            // 
            this.txtAccessFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessFile.Location = new System.Drawing.Point(300, 23);
            this.txtAccessFile.Name = "txtAccessFile";
            this.txtAccessFile.ReadOnly = true;
            this.txtAccessFile.Size = new System.Drawing.Size(530, 20);
            this.txtAccessFile.TabIndex = 20;
            // 
            // txtExcelFile
            // 
            this.txtExcelFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExcelFile.Location = new System.Drawing.Point(98, 1);
            this.txtExcelFile.Name = "txtExcelFile";
            this.txtExcelFile.ReadOnly = true;
            this.txtExcelFile.Size = new System.Drawing.Size(693, 20);
            this.txtExcelFile.TabIndex = 17;
            // 
            // lblExcelFile
            // 
            this.lblExcelFile.Location = new System.Drawing.Point(13, 1);
            this.lblExcelFile.Name = "lblExcelFile";
            this.lblExcelFile.Size = new System.Drawing.Size(85, 20);
            this.lblExcelFile.TabIndex = 16;
            this.lblExcelFile.Text = "Excel File:";
            this.lblExcelFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnFindDataFile
            // 
            this.btnFindDataFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindDataFile.Location = new System.Drawing.Point(791, 1);
            this.btnFindDataFile.Name = "btnFindDataFile";
            this.btnFindDataFile.Size = new System.Drawing.Size(39, 20);
            this.btnFindDataFile.TabIndex = 18;
            this.btnFindDataFile.Text = "Find";
            this.btnFindDataFile.UseVisualStyleBackColor = true;
            this.btnFindDataFile.Click += new System.EventHandler(this.OnClickFindExcelFile);
            // 
            // lblControlTitle
            // 
            this.lblControlTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblControlTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControlTitle.Location = new System.Drawing.Point(0, 0);
            this.lblControlTitle.Name = "lblControlTitle";
            this.lblControlTitle.Size = new System.Drawing.Size(842, 23);
            this.lblControlTitle.TabIndex = 30;
            this.lblControlTitle.Text = "ControlTitle";
            this.lblControlTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlExcelCheckList);
            this.pnlMain.Controls.Add(this.datagridGADSData);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 125);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(842, 336);
            this.pnlMain.TabIndex = 26;
            // 
            // pnlExcelCheckList
            // 
            this.pnlExcelCheckList.Controls.Add(this.lstbxExcelCheckList);
            this.pnlExcelCheckList.Controls.Add(this.txtNote);
            this.pnlExcelCheckList.Location = new System.Drawing.Point(102, 4);
            this.pnlExcelCheckList.Name = "pnlExcelCheckList";
            this.pnlExcelCheckList.Size = new System.Drawing.Size(638, 348);
            this.pnlExcelCheckList.TabIndex = 24;
            // 
            // lstbxExcelCheckList
            // 
            this.lstbxExcelCheckList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstbxExcelCheckList.FormattingEnabled = true;
            this.lstbxExcelCheckList.Items.AddRange(new object[] {
            "Be sure row 21 is not hidden (this row contains the database field names)",
            "",
            "Be sure there are no gaps in the data because the software will stop reading when" +
                " it hits a blank row.",
            "",
            "Do a quick check to make sure no one has modified or deleted any of the following" +
                " cols:",
            "  EventNumber",
            "  VerbalDesc2",
            "  UtilityUnitCode",
            "  EventType",
            "  StartDateTime",
            "  EndDateTime",
            "  Year",
            "  CauseCode",
            "  VerbalDesc86",
            "  VerbalDescFull (optional)",
            "  NetAvailCapacity (some of the forms being use have this one chopped off)",
            "",
            "Col order does not matter as long as the following 2 restrictions are met",
            "- EventNumber (must be the first column in the section)",
            "- NetAvailCapacity (must be the last column in the section)",
            ""});
            this.lstbxExcelCheckList.Location = new System.Drawing.Point(0, 41);
            this.lstbxExcelCheckList.Name = "lstbxExcelCheckList";
            this.lstbxExcelCheckList.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lstbxExcelCheckList.Size = new System.Drawing.Size(638, 290);
            this.lstbxExcelCheckList.TabIndex = 3;
            // 
            // txtNote
            // 
            this.txtNote.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNote.ForeColor = System.Drawing.Color.Red;
            this.txtNote.Location = new System.Drawing.Point(0, 0);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ReadOnly = true;
            this.txtNote.Size = new System.Drawing.Size(638, 41);
            this.txtNote.TabIndex = 2;
            this.txtNote.Text = "Be sure to check the following in the Excel file before trying to import it";
            this.txtNote.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // datagridGADSData
            // 
            this.datagridGADSData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridGADSData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagridGADSData.Location = new System.Drawing.Point(0, 0);
            this.datagridGADSData.Name = "datagridGADSData";
            this.datagridGADSData.Size = new System.Drawing.Size(842, 336);
            this.datagridGADSData.TabIndex = 23;
            // 
            // ctrlImportNonNERC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlTop);
            this.Name = "ctrlImportNonNERC";
            this.Load += new System.EventHandler(this.OnLoad);
            this.Resize += new System.EventHandler(this.OnResizeControl);
            this.pnlTop.ResumeLayout(false);
            this.pnlTopImportType.ResumeLayout(false);
            this.pnlTopButtons.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlTopFilename.ResumeLayout(false);
            this.pnlTopFilename.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.pnlExcelCheckList.ResumeLayout(false);
            this.pnlExcelCheckList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridGADSData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlTopImportType;
        private System.Windows.Forms.Panel pnlTopButtons;
        private System.Windows.Forms.Button btnImportData;
        private System.Windows.Forms.Button btnViewData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radPerfData;
        private System.Windows.Forms.RadioButton radEventData;
        private System.Windows.Forms.Panel pnlTopFilename;
        private Common.CustomControls.MyTextBox txtExcelFile;
        private System.Windows.Forms.Label lblExcelFile;
        private System.Windows.Forms.Button btnFindDataFile;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.DataGridView datagridGADSData;
        private Common.CustomControls.MyTextBox txtAccessFile;
        private System.Windows.Forms.ComboBox cmbCompanyID;
        private System.Windows.Forms.Label lblCompanyID;
        private System.Windows.Forms.Button btnSelectTabs;
        private System.Windows.Forms.Panel pnlExcelCheckList;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.ListBox lstbxExcelCheckList;
        private System.Windows.Forms.Label lblControlTitle;
        private System.Windows.Forms.CheckBox chkBuildDefaultPerfRecords;
    }
}
