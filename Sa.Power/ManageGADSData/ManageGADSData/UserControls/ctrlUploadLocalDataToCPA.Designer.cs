﻿namespace PowerCPA.UserControls
{
    partial class ctrlUploadLocalDataToCPA
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlTopButtons = new System.Windows.Forms.Panel();
            this.pnlCompanyID = new System.Windows.Forms.Panel();
            this.cmbCompanyID = new System.Windows.Forms.ComboBox();
            this.lblCompanyID = new System.Windows.Forms.Label();
            this.btnUploadData = new System.Windows.Forms.Button();
            this.btnGetData = new System.Windows.Forms.Button();
            this.pnlTopFilename = new System.Windows.Forms.Panel();
            this.txtAccessFile = new Common.CustomControls.MyTextBox();
            this.lblAccessFile = new System.Windows.Forms.Label();
            this.lblControlTitle = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splitContainerGADSData = new System.Windows.Forms.SplitContainer();
            this.datagridLocalData = new System.Windows.Forms.DataGridView();
            this.lblLocalGADS = new System.Windows.Forms.Label();
            this.datagridSQLData = new System.Windows.Forms.DataGridView();
            this.lblSQLGADS = new System.Windows.Forms.Label();
            this.pnlTop.SuspendLayout();
            this.pnlTopButtons.SuspendLayout();
            this.pnlCompanyID.SuspendLayout();
            this.pnlTopFilename.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.splitContainerGADSData.Panel1.SuspendLayout();
            this.splitContainerGADSData.Panel2.SuspendLayout();
            this.splitContainerGADSData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridLocalData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datagridSQLData)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.pnlTopButtons);
            this.pnlTop.Controls.Add(this.pnlTopFilename);
            this.pnlTop.Controls.Add(this.lblControlTitle);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(842, 96);
            this.pnlTop.TabIndex = 28;
            // 
            // pnlTopButtons
            // 
            this.pnlTopButtons.Controls.Add(this.pnlCompanyID);
            this.pnlTopButtons.Controls.Add(this.btnUploadData);
            this.pnlTopButtons.Controls.Add(this.btnGetData);
            this.pnlTopButtons.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopButtons.Location = new System.Drawing.Point(0, 51);
            this.pnlTopButtons.Name = "pnlTopButtons";
            this.pnlTopButtons.Size = new System.Drawing.Size(842, 42);
            this.pnlTopButtons.TabIndex = 23;
            // 
            // pnlCompanyID
            // 
            this.pnlCompanyID.Controls.Add(this.cmbCompanyID);
            this.pnlCompanyID.Controls.Add(this.lblCompanyID);
            this.pnlCompanyID.Location = new System.Drawing.Point(150, 4);
            this.pnlCompanyID.Name = "pnlCompanyID";
            this.pnlCompanyID.Size = new System.Drawing.Size(301, 34);
            this.pnlCompanyID.TabIndex = 27;
            // 
            // cmbCompanyID
            // 
            this.cmbCompanyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCompanyID.FormattingEnabled = true;
            this.cmbCompanyID.Location = new System.Drawing.Point(94, 7);
            this.cmbCompanyID.Name = "cmbCompanyID";
            this.cmbCompanyID.Size = new System.Drawing.Size(196, 21);
            this.cmbCompanyID.TabIndex = 3;
            this.cmbCompanyID.SelectedIndexChanged += new System.EventHandler(this.OnSelectedIndexChangedCompanyID);
            // 
            // lblCompanyID
            // 
            this.lblCompanyID.Location = new System.Drawing.Point(10, 7);
            this.lblCompanyID.Name = "lblCompanyID";
            this.lblCompanyID.Size = new System.Drawing.Size(78, 20);
            this.lblCompanyID.TabIndex = 2;
            this.lblCompanyID.Text = "CompanyID";
            this.lblCompanyID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUploadData
            // 
            this.btnUploadData.Enabled = false;
            this.btnUploadData.Location = new System.Drawing.Point(572, 10);
            this.btnUploadData.Name = "btnUploadData";
            this.btnUploadData.Size = new System.Drawing.Size(121, 23);
            this.btnUploadData.TabIndex = 0;
            this.btnUploadData.Text = "Save to SQL";
            this.btnUploadData.UseVisualStyleBackColor = true;
            this.btnUploadData.Click += new System.EventHandler(this.OnClickUploadData);
            // 
            // btnGetData
            // 
            this.btnGetData.Enabled = false;
            this.btnGetData.Location = new System.Drawing.Point(451, 10);
            this.btnGetData.Name = "btnGetData";
            this.btnGetData.Size = new System.Drawing.Size(121, 23);
            this.btnGetData.TabIndex = 20;
            this.btnGetData.Text = "Get Local Data";
            this.btnGetData.UseVisualStyleBackColor = true;
            this.btnGetData.Click += new System.EventHandler(this.OnClickGetData);
            // 
            // pnlTopFilename
            // 
            this.pnlTopFilename.Controls.Add(this.txtAccessFile);
            this.pnlTopFilename.Controls.Add(this.lblAccessFile);
            this.pnlTopFilename.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopFilename.Location = new System.Drawing.Point(0, 23);
            this.pnlTopFilename.Name = "pnlTopFilename";
            this.pnlTopFilename.Size = new System.Drawing.Size(842, 28);
            this.pnlTopFilename.TabIndex = 22;
            // 
            // txtAccessFile
            // 
            this.txtAccessFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessFile.Location = new System.Drawing.Point(117, 4);
            this.txtAccessFile.Name = "txtAccessFile";
            this.txtAccessFile.ReadOnly = true;
            this.txtAccessFile.Size = new System.Drawing.Size(693, 20);
            this.txtAccessFile.TabIndex = 20;
            // 
            // lblAccessFile
            // 
            this.lblAccessFile.Location = new System.Drawing.Point(32, 4);
            this.lblAccessFile.Name = "lblAccessFile";
            this.lblAccessFile.Size = new System.Drawing.Size(85, 20);
            this.lblAccessFile.TabIndex = 19;
            this.lblAccessFile.Text = "GADS Data File:";
            this.lblAccessFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblControlTitle
            // 
            this.lblControlTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblControlTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControlTitle.Location = new System.Drawing.Point(0, 0);
            this.lblControlTitle.Name = "lblControlTitle";
            this.lblControlTitle.Size = new System.Drawing.Size(842, 23);
            this.lblControlTitle.TabIndex = 30;
            this.lblControlTitle.Text = "ControlTitle";
            this.lblControlTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.splitContainerGADSData);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 96);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(842, 365);
            this.pnlMain.TabIndex = 29;
            // 
            // splitContainerGADSData
            // 
            this.splitContainerGADSData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerGADSData.Location = new System.Drawing.Point(0, 0);
            this.splitContainerGADSData.Name = "splitContainerGADSData";
            this.splitContainerGADSData.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerGADSData.Panel1
            // 
            this.splitContainerGADSData.Panel1.CausesValidation = false;
            this.splitContainerGADSData.Panel1.Controls.Add(this.datagridLocalData);
            this.splitContainerGADSData.Panel1.Controls.Add(this.lblLocalGADS);
            // 
            // splitContainerGADSData.Panel2
            // 
            this.splitContainerGADSData.Panel2.CausesValidation = false;
            this.splitContainerGADSData.Panel2.Controls.Add(this.datagridSQLData);
            this.splitContainerGADSData.Panel2.Controls.Add(this.lblSQLGADS);
            this.splitContainerGADSData.Size = new System.Drawing.Size(842, 365);
            this.splitContainerGADSData.SplitterDistance = 180;
            this.splitContainerGADSData.TabIndex = 24;
            // 
            // datagridLocalData
            // 
            this.datagridLocalData.AllowUserToAddRows = false;
            this.datagridLocalData.AllowUserToDeleteRows = false;
            this.datagridLocalData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridLocalData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagridLocalData.Location = new System.Drawing.Point(0, 13);
            this.datagridLocalData.Name = "datagridLocalData";
            this.datagridLocalData.ReadOnly = true;
            this.datagridLocalData.Size = new System.Drawing.Size(842, 167);
            this.datagridLocalData.TabIndex = 23;
            // 
            // lblLocalGADS
            // 
            this.lblLocalGADS.BackColor = System.Drawing.Color.Black;
            this.lblLocalGADS.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLocalGADS.ForeColor = System.Drawing.Color.White;
            this.lblLocalGADS.Location = new System.Drawing.Point(0, 0);
            this.lblLocalGADS.Name = "lblLocalGADS";
            this.lblLocalGADS.Size = new System.Drawing.Size(842, 13);
            this.lblLocalGADS.TabIndex = 1;
            this.lblLocalGADS.Text = "Local GADS Setup Records";
            this.lblLocalGADS.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // datagridSQLData
            // 
            this.datagridSQLData.AllowUserToAddRows = false;
            this.datagridSQLData.AllowUserToDeleteRows = false;
            this.datagridSQLData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridSQLData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagridSQLData.Location = new System.Drawing.Point(0, 13);
            this.datagridSQLData.Name = "datagridSQLData";
            this.datagridSQLData.ReadOnly = true;
            this.datagridSQLData.Size = new System.Drawing.Size(842, 168);
            this.datagridSQLData.TabIndex = 0;
            // 
            // lblSQLGADS
            // 
            this.lblSQLGADS.BackColor = System.Drawing.Color.Black;
            this.lblSQLGADS.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSQLGADS.ForeColor = System.Drawing.Color.White;
            this.lblSQLGADS.Location = new System.Drawing.Point(0, 0);
            this.lblSQLGADS.Name = "lblSQLGADS";
            this.lblSQLGADS.Size = new System.Drawing.Size(842, 13);
            this.lblSQLGADS.TabIndex = 24;
            this.lblSQLGADS.Text = "SQL Server GADS Setup Records";
            this.lblSQLGADS.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ctrlUploadLocalDataToCPA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlTop);
            this.Name = "ctrlUploadLocalDataToCPA";
            this.Load += new System.EventHandler(this.OnLoad);
            this.Resize += new System.EventHandler(this.OnResizeMainControl);
            this.pnlTop.ResumeLayout(false);
            this.pnlTopButtons.ResumeLayout(false);
            this.pnlCompanyID.ResumeLayout(false);
            this.pnlTopFilename.ResumeLayout(false);
            this.pnlTopFilename.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.splitContainerGADSData.Panel1.ResumeLayout(false);
            this.splitContainerGADSData.Panel2.ResumeLayout(false);
            this.splitContainerGADSData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagridLocalData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datagridSQLData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlTopButtons;
        private System.Windows.Forms.Panel pnlTopFilename;
        private Common.CustomControls.MyTextBox txtAccessFile;
        private System.Windows.Forms.Label lblAccessFile;
        private System.Windows.Forms.Button btnUploadData;
        private System.Windows.Forms.Button btnGetData;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.DataGridView datagridLocalData;
        private System.Windows.Forms.Panel pnlCompanyID;
        private System.Windows.Forms.ComboBox cmbCompanyID;
        private System.Windows.Forms.Label lblCompanyID;
        private System.Windows.Forms.SplitContainer splitContainerGADSData;
        private System.Windows.Forms.DataGridView datagridSQLData;
        private System.Windows.Forms.Label lblControlTitle;
        private System.Windows.Forms.Label lblLocalGADS;
        private System.Windows.Forms.Label lblSQLGADS;

    }
}
