﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using DataLayerAlias = PowerCPA.DataLayer.DataLayer;
using ComUtilsAlias = Common.Utility;
using ComUIUtilsAlias = Common.UI;
using DBUtilsAlias = Common.Database;

namespace PowerCPA.UserControls
{
    public partial class ctrlCreateLocalDatabase : BaseUserControl
    {
        #region Variables
        #region Constants

        #endregion Constants

        #region Local Variables
        private string m_CompanyID = string.Empty;
        private string m_GADSMasterFile = Globals.GADSMasterDirectory + @"GADSNG.MDB";
        private string m_ProjectDirectory = string.Empty;
        private string m_ProjectFilename = string.Empty;
        #endregion Local Variables

        #region Global Variables

        #endregion Global Variables

        #endregion Variables

        #region Public Properties

        #endregion Public Properties

        #region Constructors
        public ctrlCreateLocalDatabase()
        {
            InitializeComponent();
            lblControlTitle.Text = "Create a local *.mdb file with all the current data from the server GADS database for selected company";
            Globals.bInitialLoad = true;

            // set up restrictions on the datagrid so user can NOT make any changes
            datagridGADSData.AllowUserToOrderColumns = true;
            datagridGADSData.AllowUserToAddRows = false;
            datagridGADSData.AllowUserToDeleteRows = false;
            datagridGADSData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;

        }

        #endregion Constructors

        #region Form Load
        private void OnLoad(object sender, EventArgs e)
        {
            Globals.bFormCancel = !Globals.PromptForSQLLogin();

            if (Globals.bFormCancel)
            {
                this.Enabled = false;
                Globals.UserRole = 0;
            }
            if (Globals.UserRole < (int)Globals.Roles.Consultant)
            {
                MessageBox.Show("You are not authorized to run this application");
                return;
            }
            else
            {
                Globals.sqlConnGD = Globals.InitializeSQLConnectionStringBuilder("GadsOS", 90);
                this.Enabled = true;
                try
                {
                    cmbCompanyID.DataSource = DataLayerAlias.GetSQLCompanyList(m_CompanyID);
                    cmbCompanyID.DisplayMember = "companyid";
                    cmbCompanyID.ValueMember = "companyid";
                    cmbCompanyID.SelectedIndex = -1;
                    Globals.bInitialLoad = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem retrieving list of companies from SQL.\n" + ex.Message);
                }
            }
        }

        #endregion Form Load

        #region Events
        #region Form Events
        private void OnResizeControl(object sender, EventArgs e)
        {
            ComUIUtilsAlias.Utils.CenterControlItems(this.pnlTopInfoPrompt);
            ComUIUtilsAlias.Utils.CenterControlItems(this.pnlTopFilename);
        }

        #endregion Form Events

        #region Click Events
        private void OnSelectedIndexChangedCompanyID(object sender, EventArgs e)
        {
            datagridGADSData.DataSource = null;
            if (!Globals.bInitialLoad)
            {
                m_CompanyID = cmbCompanyID.SelectedValue.ToString().Trim();
                m_ProjectDirectory = Globals.GADSDirectory + Globals.PROJECTDIRECTORY.Replace("XXCOMPANYIDXX", m_CompanyID);
                m_ProjectFilename = m_ProjectDirectory + @"GADSNG.MDB";

                txtAccessFile.Text = m_ProjectFilename;
                Globals.AccessDataFile = m_ProjectFilename;

                DirectoryInfo di = new DirectoryInfo(m_ProjectDirectory);
                if (!di.Exists)
                {
                    try
                    {
                        di.Create();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem creating project directory [" + m_ProjectDirectory + "]\n" + ex.Message);
                        return;
                    }
                }
                FileInfo fi = new FileInfo(m_ProjectFilename);
                if (!fi.Exists)
                {
                    FileInfo fiMaster = new FileInfo(m_GADSMasterFile);
                    if (fiMaster.Exists)
                    {
                        fiMaster.CopyTo(m_ProjectFilename, true);
                    }
                    else
                    {
                        MessageBox.Show("GADS Master database file [" + m_GADSMasterFile + "] missing.");
                        return;
                    }
                }
                btnGetALLData.Enabled = true;
                btnGetSetupOnlyData.Enabled = true;
            }
        }
        private void OnClickGetAllData(object sender, EventArgs e)
        {
            GetSQLData(true);
        }
        private void OnClickGetSetupOnlyData(object sender, EventArgs e)
        {
            GetSQLData(false);
        }

        private void GetSQLData(bool _All)
        {
                Globals.dsGADSNG = DataLayerAlias.GetSQLGADSData(m_CompanyID, _All);

            if (Globals.dsGADSNG == null)
            {
                datagridGADSData.DataSource = null;
                btnSaveData.Enabled = false;
            }
            else
            {
                datagridGADSData.DataSource = Globals.dsGADSNG.Tables["Setup"];
                Globals.StatusMessageText = "Setup rows=[" + Globals.dsGADSNG.Tables["Setup"].Rows.Count.ToString() +
                    "] Event01 rows=[" + Globals.dsGADSNG.Tables["EventData01"].Rows.Count.ToString() +
                    "] Event02 rows=[" + Globals.dsGADSNG.Tables["EventData02"].Rows.Count.ToString() +
                    "] PerfData rows=[" + Globals.dsGADSNG.Tables["PerformanceData"].Rows.Count.ToString() + "]";
                btnSaveData.Enabled = true;
            }
        }
        private void OnClickSaveData(object sender, EventArgs e)
        {
            DataLayerAlias.SetupConnectStringForAccessDatabase();

            // clear out the local ACCESS records
                DataLayerAlias.ClearAccessSetupRecords(m_CompanyID);
                DataLayerAlias.ClearAccessEventData01Records(m_CompanyID);
                DataLayerAlias.ClearAccessEventData02Records(m_CompanyID);
                DataLayerAlias.ClearAccessPerformanceDataRecords(m_CompanyID);

            if (Globals.dsGADSNG.Tables["Setup"].Rows.Count > 0)
            {
                Globals.SetupCompareStatus = Globals.SetupRecordCheckReturn.NotSet;
                DataLayerAlias.UploadSetupRecordsToAccess();
            }
            if (Globals.dsGADSNG.Tables["EventData01"].Rows.Count > 0)
            {
                DataLayerAlias.UploadEventData01RecordsToAccess();
            }
            if (Globals.dsGADSNG.Tables["EventData02"].Rows.Count > 0)
            {
                DataLayerAlias.UploadEventData02RecordsToAccess();
            }
            if (Globals.dsGADSNG.Tables["PerformanceData"].Rows.Count > 0)
            {
                DataLayerAlias.UploadPerformanceDataRecordsToAccess();
            }
            if (Globals.dtErrors.Rows.Count > 0)
            {
                Globals.StatusMessageText = "Error - review data listed above.";
                this.datagridGADSData.DataSource = Globals.dtErrors;
                if (Globals.dtErrSqlStatements.Rows.Count > 0)
                    Globals.dtErrSqlStatements.WriteXml("AccessErrorSqlStatements.xml");
            }
            else
            {
                Globals.StatusMessageText = "Ready";
                Globals.StatusBarLastProcessText = "Created " + m_CompanyID + " local database - " +
                    DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            }
        }

        #endregion Click Events

        #endregion Events

        #region Methods

        #endregion Methods


    }
}
