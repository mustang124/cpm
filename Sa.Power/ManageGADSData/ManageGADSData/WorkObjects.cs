﻿using System;

namespace PowerCPA
{
    class WorkListEvents
    {
        public short EventNumber { get; set; }
        //public string UnitShortName { get; set; }
        public string UtilityUnitCode { get; set; }
        public string EventType { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public short Year { get; set; }
        public short CauseCode { get; set; }
        public string CauseCodeExt { get; set; }
        public string VerbalDesc1 { get; set; }
        public string VerbalDesc2 { get; set; }
        public string VerbalDesc86 { get; set; }
        public string VerbalDescFull { get; set; }
        public decimal NetAvailCapacity { get; set; }
    }

    class UniqueUtilityUnitsByYear
    {
        public string UtilityUnitCode { get; set; }
        public string UnitShortName { get; set; }
        public short Year { get; set; }
        public bool Insert { get; set; }
        public int ExcelEventCount { get; set; }
        public int UploadedEventCount { get; set; }
    }
}
