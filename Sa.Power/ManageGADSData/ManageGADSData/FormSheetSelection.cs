﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PowerCPA
{
    public partial class FormSheetSelection : Form
    {

        #region public Dictionary<string, string> SheetNames
        private Dictionary<string, string> _sheetNames = new Dictionary<string, string>();
        private Dictionary<string, string> _SelectedSheets = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the SelectedSheets.
        /// </summary>
        /// <value>The SelectedSheets.</value>
        public Dictionary<string, string> SelectedSheets
        {
            get
            {
                return _SelectedSheets;
            }
        }
        /// <summary>
        /// Gets or sets the SheetNames.
        /// </summary>
        /// <value>The SheetNames.</value>
        public Dictionary<string, string> SheetNames
        {
            set
            {
                _sheetNames = value;
                foreach (KeyValuePair<string, string> kvp in _sheetNames)
                {
                    chkboxSheets.Items.Add(kvp.Value);
                }
            }
        }
        #endregion

        public FormSheetSelection()
        {
            InitializeComponent();
            chkboxSheets.CheckOnClick = true;
        }

        private void OnClickOK(object sender, EventArgs e)
        {
            if (chkboxSheets.CheckedItems.Count == 0)
            {
            }
            else
            {
                _SelectedSheets = new Dictionary<string, string>();
                CheckedListBox.CheckedItemCollection SelectedObjects = chkboxSheets.CheckedItems;
                for (int x = 0; x <= chkboxSheets.CheckedItems.Count - 1; x++)
                {
                    _SelectedSheets.Add(chkboxSheets.CheckedItems[x].ToString() + "$", chkboxSheets.CheckedItems[x].ToString());
                }
            }
        }
        private void OnClickCancel(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
