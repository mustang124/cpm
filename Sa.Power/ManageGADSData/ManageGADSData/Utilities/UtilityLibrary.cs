﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Windows.Forms;

namespace PowerCPA.Utilities
{
    public class UtilityLibrary
    {
        public static DateTime ReturnNextSunday(DateTime dtTemp)
        {
            switch (dtTemp.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    // Trying to find Sunday
                    return dtTemp;
                case DayOfWeek.Monday:
                    return dtTemp.AddDays(6);
                case DayOfWeek.Tuesday:
                    return dtTemp.AddDays(5);
                case DayOfWeek.Wednesday:
                    return dtTemp.AddDays(4);
                case DayOfWeek.Thursday:
                    return dtTemp.AddDays(3);
                case DayOfWeek.Friday:
                    return dtTemp.AddDays(2);
                case DayOfWeek.Saturday:
                    return dtTemp.AddDays(1);
                default:
                    return dtTemp;
            }
        }
        public static decimal GetHrsInMonth(int _yr, int _mth, int _dstValueGadsSetup)
        {
            //int HrsInMonth = Common.Utility.Time.GetHrsInMonth(_yr, _mth, false);
            DateTime start = new DateTime(_yr, _mth, 1, 0, 0, 0);
            DateTime end = new DateTime(_yr, _mth, Common.Utility.Time.GetDaysInMonth(_yr, _mth), 23, 59, 59);
            double adjDST = CalculateHoursBetween2DateTimes(start, end, _dstValueGadsSetup);
            return Convert.ToDecimal(adjDST);
        }
        public static double CalculateHoursBetween2DateTimes(DateTime BeginDateTime, DateTime EndingDateTime, int DaylightSavingTime)
        {
            long longInterval;
            double douInterval = 0;

            if ((null == BeginDateTime) || (null == EndingDateTime))
            {
                return 0;
            }

            if ((BeginDateTime.Hour == 23) && (BeginDateTime.Minute == 59) && (BeginDateTime.Second == 59))
            {
                BeginDateTime = BeginDateTime.AddSeconds(1);
            }

            if ((EndingDateTime.Hour == 23) && (EndingDateTime.Minute == 59) && (EndingDateTime.Second == 59))
            {
                EndingDateTime = EndingDateTime.AddSeconds(1);
            }

            // Calculate the "unadjusted" time difference between the two date/times in fractions of an hour
            try
            {
                longInterval = Microsoft.VisualBasic.DateAndTime.DateDiff(DateInterval.Second, BeginDateTime,
                    EndingDateTime, FirstDayOfWeek.System, FirstWeekOfYear.System);
                // douInterval = Math.Round((longInterval / 3600.0), 6)
                douInterval = Convert.ToDouble((longInterval / 3600.0));
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }

            DateTime dtBegin1;
            DateTime dtEnd1;

            DateTime dtBegin2;
            DateTime dtEnd2;

            DateTime dtTemp;
            DateTime dtDate;

            //  -------------------------------------------------
            //  China and Japan do not observe DST
            //  Fiji -- stopped in 2000
            //  -------------------------------------------------
            switch (DaylightSavingTime)
            {
                case 0: //  DaylightSavingTime = 0
                    //  NO DAYLIGHT SAVING TIME ADJUSTMENT
                    return douInterval;

                //  =====================
                //   NORTHERN HEMISPHERE
                //  =====================
                case 1: //  DaylightSavingTime = 1;

                    //  North America (Canada, US and Mexico) First Sunday in April through last Sunday in October at 2:00 AM local time
                    //  In April 2:00 AM -> 3:00 AM
                    //  In October 2:00 AM -> 1:00 AM

                    //  In 1987 daylight saving time was moved to the first Sunday in April; it used to start the LAST Sunday in April

                    //  It still ends on the last Sunday in October.

                    if (Application.CurrentCulture.Name == "en-US")
                    {
                        if (BeginDateTime.Year >= 2007)
                        {
                            //  Second Sunday in March
                            dtTemp = System.DateTime.Parse("03/08/" + BeginDateTime.Year.ToString());
                        }
                        else if ((BeginDateTime.Year >= 1987) && (BeginDateTime.Year < 2007))
                        {
                            // First Sunday in April
                            dtTemp = System.DateTime.Parse("04/01/" + BeginDateTime.Year.ToString());
                        }
                        else
                        {
                            // LAST Sunday in April
                            dtTemp = System.DateTime.Parse("04/24/" + BeginDateTime.Year.ToString());
                        }
                    }
                    else if (Application.CurrentCulture.Name == "en-CA")
                    {
                        if (BeginDateTime.Year >= 2007)
                        {
                            // Second Sunday in March
                            dtTemp = System.DateTime.Parse("08/03/" + BeginDateTime.Year.ToString());
                        }
                        else if ((BeginDateTime.Year >= 1987) && (BeginDateTime.Year < 2007))
                        {
                            // First Sunday in April
                            dtTemp = System.DateTime.Parse("01/04/" + BeginDateTime.Year.ToString());
                        }
                        else
                        {
                            // LAST Sunday in April
                            dtTemp = System.DateTime.Parse("24/04/" + BeginDateTime.Year.ToString());
                        }
                    }
                    else
                    {
                        // Mexico did NOT change along with USA and Canada in 2007
                        if (BeginDateTime.Year >= 1987)
                        {
                            // First Sunday in April
                            dtTemp = System.DateTime.Parse("01/04/" + BeginDateTime.Year.ToString());
                        }
                        else
                        {
                            // LAST Sunday in April
                            dtTemp = System.DateTime.Parse("24/04/" + BeginDateTime.Year.ToString());
                        }
                    }

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");

                    // Last Sunday in October
                    if ((EndingDateTime.Year >= 2007) && (Application.CurrentCulture.Name != "es-MX"))
                    {
                        if (Application.CurrentCulture.Name == "en-US")
                        {
                            dtTemp = System.DateTime.Parse("11/01/" + EndingDateTime.Year.ToString());
                        }
                        else
                        {
                            dtTemp = System.DateTime.Parse("01/11/" + EndingDateTime.Year.ToString());
                        }
                    }
                    else
                    {
                        if (Application.CurrentCulture.Name == "en-US")
                        {
                            dtTemp = System.DateTime.Parse("10/25/" + EndingDateTime.Year.ToString());
                        }
                        else
                        {
                            dtTemp = System.DateTime.Parse("25/10/" + EndingDateTime.Year.ToString());
                        }
                    }

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 01:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    break;

                case 2: // DaylightSavingTime = 2

                    // The EU version of Daylight Saving Time runs from the last Sunday in March through the last Sunday in October
                    // at 1:00 AM UTC.

                    // UK (England, Ireland, Scotland and Wales), Iceland, Morocco and Portugal are 1 hour behind European time.

                    // In March at 1:00 AM local time -> 2:00 AM
                    // In October at 2:00 AM local time -> 1:00 AM

                    // Last Sunday in March
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("03/25/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/03/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 01:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");

                    // Last Sunday in October
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("10/25/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/10/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 01:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    break;

                case 3: // DaylightSavingTime = 3
                    // The EU version of Daylight Saving Time runs from the last Sunday in March through the last Sunday in October
                    // at 1:00 AM UTC.

                    // Portions of Europe:  Belgium, Italy, France, Spain, Switzerland, Hungary, Germany, Poland, Austria, Sweden, Netherlands,
                    //                      Denmark, Yugoslavia - Serbia, Croatia, Czech Republic, Norway, Gibraltar, Slovak Republic, Macedonia,
                    //                      Luxembourg, Albania, Montenegro, et al.

                    // In March at 2:00 AM local time -> 3:00 AM
                    // In October at 3:00 AM local time -> 2:00 AM
                    // Local time that is 2:00 AM STD to DST and 3:00 AM DST to STD
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("03/25/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/03/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");

                    //  Last Sunday in October
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("10/25/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/10/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    break;

                case 4:  // DaylightSavingTime = 4

                    // The EU version of Daylight Saving Time runs from the last Sunday in March through the last Sunday in October
                    // at 1:00 AM UTC.

                    // Other portions of Europe:  Romania, Greece, Bulgaria, Turkey, Ukraine, Estonia, Finland, Cyprus, Lithuania, Latvia,
                    //                            Moldova

                    // In March at 3:00 AM local time -> 4:00 AM
                    // In October at 4:00 AM local time -> 3:00 AM
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("03/25/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/03/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 04:00 AM");

                    // Last Sunday in October
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("10/25/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/10/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 04:00 AM");
                    break;

                case 5: // DaylightSavingTime = 5

                    // Russia last Sunday in March at 2:00 AM LOCAL TIME through last Sunday in October at 2:00 LOCAL TIME
                    // In March at 2:00 AM local time -> 3:00 AM
                    // In October at 3:00 AM local time -> 2:00 AM
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("03/25/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/03/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");

                    // Last Sunday in October
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("10/25/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/10/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    break;

                // ==================================================================================
                //  IN SOUTHERN HEMISPHERE DST STARTS IN October/Fall AND ENDS in March/April/Spring
                // ==================================================================================

                case 6: // DaylightSavingTime = 6

                    // Australia - South Australia, Victoria, Australian Capital Territory, New South Wales, Lord Howe Island
                    // Last Sunday in October through last Sunday in March, BUT THEY DO THE OPPOSITE

                    // In October at 2:00 AM clocks are turned forward one hour to read 3:00 AM
                    // In March at 3:00 AM clocks are turned back one hour to read 2:00 AM

                    // Last Sunday in October
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("10/25/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/10/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");

                    // Last Sunday in March
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("03/25/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/03/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    break;

                case 7: // DaylightSavingTime = 7

                    // Australia - Tasmania 1st Sunday in October through last Sunday in March

                    // In October at 2:00 AM clocks are turned forward one hour to read 3:00 AM
                    // In March at 3:00 AM clocks are turned back one hour to read 2:00 AM

                    // First Sunday in October
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("10/01/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("01/10/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");

                    // Last Sunday in March
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("03/25/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/03/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    break;

                case 8: // DaylightSavingTime = 8

                    // New Zealand, Chatham 1st Sunday in October through 3rd Sunday in March

                    // In October at 2:00 AM clocks are turned forward one hour to read 3:00 AM
                    // In March at 3:00 AM clocks are turned back one hour to read 2:00 AM

                    // First Sunday in October
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("10/01/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("01/10/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");

                    // Third Sunday in March
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("03/15/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("15/03/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    break;

                case 9: // DaylightSavingTime = 9

                    // Tonga 1st Sunday in November through last Sunday in January

                    // First Sunday in November
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("11/01/" + BeginDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("01/11/" + BeginDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");

                    // Last Sunday in January
                    if (Application.CurrentCulture.Name == "en-US")
                        dtTemp = System.DateTime.Parse("01/25/" + EndingDateTime.Year.ToString());
                    else
                        dtTemp = System.DateTime.Parse("25/01/" + EndingDateTime.Year.ToString());

                    dtDate = ReturnNextSunday(dtTemp);

                    dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 02:00 AM");
                    dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString() + " 03:00 AM");
                    break;
                default:
                    return douInterval;
            }

            // BeginDateTime, EndingDateTime
            if ((DateTime.Compare(BeginDateTime, dtBegin1) <= 0) &&
                (DateTime.Compare(EndingDateTime, dtEnd1) >= 0) &&
                (DateTime.Compare(EndingDateTime, dtBegin2) <= 0))
            {
                // The event straddles the change to DST time from Standard time
                return douInterval - 1;
            }
            else if ((DateTime.Compare(BeginDateTime, dtBegin2) <= 0) && (DateTime.Compare(EndingDateTime, dtEnd2) >= 0) && (
             DateTime.Compare(BeginDateTime, dtEnd1) >= 0))
            {
                // The event straddles the change from DST time to Standard time
                return douInterval + 1;
            }

            return douInterval;
        }
        public static string CleanDatabaseString(string str)
        {
            str = str.Replace("'", "");
            str = str.Replace('"', ' ');
            str = str.Replace(';', ' ');
            return str;
        }
    
    }
}
