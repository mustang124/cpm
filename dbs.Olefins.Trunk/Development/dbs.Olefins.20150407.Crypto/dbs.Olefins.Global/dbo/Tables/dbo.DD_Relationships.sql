﻿CREATE TABLE [dbo].[DD_Relationships] (
    [ObjectA] [dbo].[DD_ID] NOT NULL,
    [ObjectB] [dbo].[DD_ID] NOT NULL,
    [ColumnA] [dbo].[DD_ID] NOT NULL,
    [ColumnB] [dbo].[DD_ID] NOT NULL,
    CONSTRAINT [UniqueItemA] UNIQUE NONCLUSTERED ([ObjectA] ASC, [ObjectB] ASC, [ColumnA] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqueItemB] UNIQUE NONCLUSTERED ([ObjectA] ASC, [ObjectB] ASC, [ColumnB] ASC) WITH (FILLFACTOR = 90)
);

