﻿CREATE TABLE [dbo].[DD_Columns] (
    [ColumnID]     [dbo].[DD_ID] IDENTITY (1, 1) NOT NULL,
    [ObjectID]     [dbo].[DD_ID] NOT NULL,
    [ColumnName]   [sysname]     NOT NULL,
    [StoredUnits]  [dbo].[UOM]   NULL,
    [DisplayUnits] [dbo].[UOM]   NULL,
    [HelpID]       VARCHAR (30)  NULL,
    [LookupID]     [dbo].[DD_ID] NULL,
    CONSTRAINT [PK_DD_Columns_1__14] PRIMARY KEY CLUSTERED ([ColumnID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [ItemObjectRI] FOREIGN KEY ([ObjectID]) REFERENCES [dbo].[DD_Objects] ([ObjectID]),
    CONSTRAINT [UniqueColumn] UNIQUE NONCLUSTERED ([ObjectID] ASC, [ColumnName] ASC) WITH (FILLFACTOR = 90)
);

