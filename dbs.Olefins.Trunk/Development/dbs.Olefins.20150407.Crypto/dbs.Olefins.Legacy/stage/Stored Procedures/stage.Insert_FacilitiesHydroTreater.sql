﻿CREATE PROCEDURE [stage].[Insert_FacilitiesHydroTreater]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@Quantity_kBSD			FLOAT,
	@HydroTreaterTypeId		INT,
	@Pressure_PSIg			FLOAT	= NULL,
	@Processed_kMT			FLOAT	= NULL,
	@Processed_Pcnt			FLOAT,
	@Density_SG				FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[FacilitiesHydroTreater]([SubmissionId], [FacilityId], [Quantity_kBSD], [HydroTreaterTypeId], [Pressure_PSIg], [Processed_kMT], [Processed_Pcnt], [Density_SG])
	SELECT
		@SubmissionId,
		@FacilityId,
		@Quantity_kBSD,
		@HydroTreaterTypeId,
		@Pressure_PSIg,
		@Processed_kMT,
		@Processed_Pcnt,
		@Density_SG
	WHERE	@Quantity_kBSD		>= 0.0
		OR	@HydroTreaterTypeId	>= 1
		OR	@Pressure_PSIg		>= 0.0
		OR	@Processed_kMT		>= 0.0
		OR	@Processed_Pcnt		>= 0.0
		OR	@Density_SG			>= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'		+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @FacilityId:'			+ CONVERT(VARCHAR, @FacilityId))
			+ COALESCE(', @Quantity_kBSD:'		+ CONVERT(VARCHAR, @Quantity_kBSD),			'')
			+ COALESCE(', @HydroTreaterTypeId:'	+ CONVERT(VARCHAR, @HydroTreaterTypeId),	'')
			+ COALESCE(', @Pressure_PSIg:'		+ CONVERT(VARCHAR, @Pressure_PSIg),			'')
			+ COALESCE(', @Processed_kMT:'		+ CONVERT(VARCHAR, @Processed_kMT),			'')
			+ COALESCE(', @Processed_Pcnt:'		+ CONVERT(VARCHAR, @Processed_Pcnt),		'')
			+ COALESCE(', @Density_SG:'			+ CONVERT(VARCHAR, @Density_SG),			'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;