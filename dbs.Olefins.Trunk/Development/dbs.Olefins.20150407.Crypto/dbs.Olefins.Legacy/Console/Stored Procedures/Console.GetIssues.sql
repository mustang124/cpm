﻿CREATE PROCEDURE [Console].[GetIssues]
      @RefNum dbo.Refnum,
      @Completed nvarchar(1)
AS
BEGIN

      SELECT * FROM ValStat
      WHERE Refnum = dbo.FormatRefNum(@RefNum,0) 
      AND (@Completed = 'A' OR Completed = @Completed)

END

