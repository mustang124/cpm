﻿CREATE PROCEDURE [Console].[GetRefNumsByCompany]
@CoLoc nvarchar(80),
@Study varchar(3),
@StudyYear nvarchar(4)

AS
BEGIN

	SELECT RefNum FROM dbo.TSort WHERE CoLoc = @CoLoc AND StudyYear = @StudyYear

END
