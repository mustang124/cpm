﻿CREATE PROCEDURE [ante].[Merge_ComponentEnergy]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[ComponentEnergy] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_ComponentId('H2'),			1951.0),
			(@MethodologyId, dim.Return_ComponentId('CH4'),			1951.0),
			(@MethodologyId, dim.Return_ComponentId('C2H4'),		1882.0),
			(@MethodologyId, dim.Return_ComponentId('C3H6'),		1467.0),
			(@MethodologyId, dim.Return_ComponentId('C4H6'),		 947.0),
			(@MethodologyId, dim.Return_ComponentId('C4H8'),		 947.0),
			(@MethodologyId, dim.Return_ComponentId('C4H10'),		 947.0),
			(@MethodologyId, dim.Return_ComponentId('C6H6'),		 150.0),
			(@MethodologyId, dim.Return_ComponentId('Other'),		 150.0),
			(@MethodologyId, dim.Return_ComponentId('PyroGasOil'),	 150.0),
			(@MethodologyId, dim.Return_ComponentId('PyroFuelOil'),  150.0),
			(@MethodologyId, dim.Return_ComponentId('Inerts'),		 947.0),
			(@MethodologyId, dim.Return_ComponentId('C2H6'),		1882.0),
			(@MethodologyId, dim.Return_ComponentId('C3H8'),		1467.0)
	)
	AS Source([MethodologyId], [ComponentId], [StandardEnergy_BtuLb])
	ON	Target.[MethodologyId]		= Source.[MethodologyId]
	AND	Target.[ComponentId]		= Source.[ComponentId]
	WHEN MATCHED THEN UPDATE SET
		Target.[StandardEnergy_BtuLb]	= Source.[StandardEnergy_BtuLb]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [ComponentId], [StandardEnergy_BtuLb])
		VALUES([MethodologyId], [ComponentId], [StandardEnergy_BtuLb])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;