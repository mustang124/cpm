﻿CREATE TABLE [ante].[MapRecycledBalance] (
    [MethodologyId]  INT                NOT NULL,
    [RecStreamId]    INT                NOT NULL,
    [RecComponentId] INT                NOT NULL,
    [BalStreamId]    INT                NULL,
    [BalComponentId] INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapRecycledBalance_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_MapRecycledBalance_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_MapRecycledBalance_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_MapRecycledBalance_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapRecycledBalance] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [RecStreamId] ASC),
    CONSTRAINT [CV_MapRecycledBalance_Mapping] CHECK (NOT ([RecStreamId]=[BalStreamId] AND [RecComponentId]=[BalComponentId])),
    CONSTRAINT [FK_MapRecycledBalance_BalComponent_LookUp] FOREIGN KEY ([BalComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_MapRecycledBalance_BalStream_LookUp] FOREIGN KEY ([BalStreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_MapRecycledBalance_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_MapRecycledBalance_RecComponent_LookUp] FOREIGN KEY ([RecComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_MapRecycledBalance_RecStream_LookUp] FOREIGN KEY ([RecStreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_MapRecycledBalance_u]
	ON [ante].[MapRecycledBalance]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapRecycledBalance]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapRecycledBalance].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapRecycledBalance].[RecStreamId]	= INSERTED.[RecStreamId];

END;