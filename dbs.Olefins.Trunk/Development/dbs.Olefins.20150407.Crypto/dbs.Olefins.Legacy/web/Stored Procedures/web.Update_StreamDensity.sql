﻿CREATE PROCEDURE [web].[Update_StreamDensity]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@Density_SG				FLOAT	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[StreamDensity] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamNumber] = @StreamNumber)
	BEGIN
		SET @Density_SG = COALESCE(@Density_SG, 0.0);
		EXECUTE [stage].[Update_StreamDensity] @SubmissionId, @StreamNumber, @Density_SG;
	END
	ELSE
	BEGIN
		IF(@Density_SG >= 0.0)
		EXECUTE [stage].[Insert_StreamDensity] @SubmissionId, @StreamNumber, @Density_SG;
	END;

END;