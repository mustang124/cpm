﻿CREATE TABLE [calc].[StreamComposition] (
    [MethodologyId]     INT                NOT NULL,
    [SubmissionId]      INT                NOT NULL,
    [StreamNumber]      INT                NOT NULL,
    [StreamId]          INT                NOT NULL,
    [ComponentId]       INT                NOT NULL,
    [Component_WtPcnt]  FLOAT (53)         NOT NULL,
    [Component_Dur_kMT] FLOAT (53)         NOT NULL,
    [Component_Ann_kMT] FLOAT (53)         NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_StreamComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamComposition] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [StreamNumber] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_StreamComposition_Component_Ann_kMT_MinIncl_0.0] CHECK ([Component_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamComposition_Component_Dur_kMT_MinIncl_0.0] CHECK ([Component_Dur_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MaxIncl_100.0] CHECK ([Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MinIncl_0.0] CHECK ([Component_WtPcnt]>=(0.0)),
    CONSTRAINT [FK_StreamComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StreamComposition_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StreamComposition_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_StreamComposition_StreamAttributes] FOREIGN KEY ([SubmissionId], [StreamNumber], [StreamId]) REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber], [StreamId]),
    CONSTRAINT [FK_StreamComposition_StreamQuantity] FOREIGN KEY ([SubmissionId], [StreamNumber]) REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber]),
    CONSTRAINT [FK_StreamComposition_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_StreamComposition_u]
	ON [calc].[StreamComposition]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StreamComposition]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StreamComposition].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StreamComposition].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[StreamComposition].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[calc].[StreamComposition].[ComponentId]	= INSERTED.[ComponentId];

END;