﻿CREATE PROCEDURE [calc].[Insert_HydrogenPurification]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @hStreamId		INT	= [dim].[Return_StreamId]('Hydrogen');
	DECLARE @hComponentId	INT	= [dim].[Return_ComponentId]('H2');

	DECLARE @uStreamId		INT	= [dim].[Return_StreamId]('ProdOlefins');
	DECLARE @uComponentId	INT	= [dim].[Return_ComponentId]('ProdOlefins');

	DECLARE @FacilityId		INT	= [dim].[Return_FacilityId]('HydroPur');

	INSERT INTO [calc].[HydrogenPurification]([MethodologyId], [SubmissionId], [FacilityId], [StreamNumber], [StreamId], [ComponentId], [Component_WtPcnt], [Component_kMT], [Utilization_Pcnt], [H2Unit_Count], [Duration_Days])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		[calc].[Return_HydroPurFacilityId](@MethodologyId, @SubmissionId),
		c.[StreamNumber],
		c.[StreamId],
		c.[ComponentId],
		c.[Component_WtPcnt],
		c.[Component_Dur_kMT],
		u.[_Utilization_Pcnt],
		f.[Unit_Count],
		s.[_Duration_Days]
	FROM [calc].[StreamComposition]					c
	INNER JOIN [calc].[Utilization]					u
		ON	u.[MethodologyId]	= c.[MethodologyId]
		AND	u.[SubmissionId]	= c.[SubmissionId]
	INNER JOIN [fact].[Facilities_HydroPur]			f WITH (NOEXPAND)
		ON	f.[SubmissionId]	= c.[SubmissionId]
	INNER JOIN [fact].[Submissions]					s
		ON	s.[SubmissionId]	= c.[SubmissionId]
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
		AND	c.[StreamId]		= @hStreamId
		AND	c.[ComponentId]		= @hComponentId
		AND	u.[StreamId]		= @uStreamId
		AND	u.[ComponentId]		= @uComponentId
		AND	f.[FacilityId]		= @FacilityId;
			
END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;