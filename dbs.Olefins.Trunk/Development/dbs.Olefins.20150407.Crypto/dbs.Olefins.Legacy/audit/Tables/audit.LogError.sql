﻿CREATE TABLE [audit].[LogError] (
    [ErrorNumber]     INT                NOT NULL,
    [XActState]       SMALLINT           NOT NULL,
    [ProcedureSchema] [sysname]          NOT NULL,
    [ProcedureName]   VARCHAR (128)      NOT NULL,
    [ProcedureLine]   INT                NOT NULL,
    [ProcedureParam]  VARCHAR (4000)     NULL,
    [ErrorMessage]    VARCHAR (MAX)      NOT NULL,
    [ErrorSeverity]   INT                NOT NULL,
    [ErrorState]      INT                NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_LogError_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (128)     CONSTRAINT [DF_LogError_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (128)     CONSTRAINT [DF_LogError_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (128)     CONSTRAINT [DF_LogError_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_LogError] PRIMARY KEY CLUSTERED ([tsModified] DESC, [tsModifiedRV] DESC),
    CONSTRAINT [CL_LogError_ErrorMessage] CHECK ([ErrorMessage]<>''),
    CONSTRAINT [CL_LogError_ProcedureName] CHECK ([ProcedureName]<>''),
    CONSTRAINT [CL_LogError_ProcedureParam] CHECK ([ProcedureParam]<>''),
    CONSTRAINT [CL_LogError_ProcedureSchema] CHECK ([ProcedureSchema]<>''),
    CONSTRAINT [CR_LogError_ProcedureLine] CHECK ([ProcedureLine]>(0))
);


GO

CREATE TRIGGER [audit].[t_LogError_u]
	ON [audit].[LogError]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [audit].[LogError]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[audit].[LogError].[tsModifiedRV]		= INSERTED.[tsModifiedRV];
		
END;