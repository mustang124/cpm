﻿CREATE FUNCTION [audit].[ReplaceParameters]
(
	@String			NVARCHAR(MAX),
	@Parameters		NVARCHAR(MAX),
	@Delimiter		NCHAR(1)		= N'|'
)
RETURNS NVARCHAR(MAX)
AS
BEGIN

	SELECT	@String = REPLACE(@String, N'@(' + CONVERT(NVARCHAR(3), p.[Id]) + N')', p.[Item])
	FROM	[audit].[ParseString](@Parameters, @Delimiter) p;

	RETURN @String;

END