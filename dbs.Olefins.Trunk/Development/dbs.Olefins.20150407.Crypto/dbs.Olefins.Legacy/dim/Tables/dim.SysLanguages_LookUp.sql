﻿CREATE TABLE [dim].[SysLanguages_LookUp] (
    [langid]         SMALLINT           NOT NULL,
    [dateformat]     NCHAR (3)          NOT NULL,
    [datefirst]      TINYINT            NOT NULL,
    [upgrade]        INT                NOT NULL,
    [name]           [sysname]          NOT NULL,
    [alias]          [sysname]          NOT NULL,
    [months]         NVARCHAR (372)     NOT NULL,
    [shortmonths]    NVARCHAR (132)     NOT NULL,
    [days]           NVARCHAR (217)     NOT NULL,
    [lcid]           INT                NOT NULL,
    [msglangid]      SMALLINT           NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_SysLanguages_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_SysLanguages_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_SysLanguages_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_SysLanguages_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SysLanguages_LookUp] PRIMARY KEY CLUSTERED ([langid] ASC),
    CONSTRAINT [CL_SysLanguages_LookUp_alias] CHECK ([alias]<>''),
    CONSTRAINT [CL_SysLanguages_LookUp_name] CHECK ([name]<>''),
    CONSTRAINT [UK_SysLanguages_LookUp_alias] UNIQUE NONCLUSTERED ([alias] ASC),
    CONSTRAINT [UK_SysLanguages_LookUp_lcid] UNIQUE NONCLUSTERED ([lcid] ASC),
    CONSTRAINT [UK_SysLanguages_LookUp_name] UNIQUE NONCLUSTERED ([name] ASC)
);


GO

CREATE TRIGGER [dim].[t_SysLanguages_LookUp_u]
	ON [dim].[SysLanguages_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[SysLanguages_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[SysLanguages_LookUp].[langid]	= INSERTED.[langid];

END;