﻿CREATE FUNCTION [dim].[Return_StandardId]
(
	@StandardTag	VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[StandardId]
	FROM [dim].[Standard_LookUp]	l
	WHERE l.[StandardTag] = @StandardTag;

	RETURN @Id;

END;