﻿CREATE PROCEDURE [dim].[Merge_Standard]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [dim].[Standard_LookUp] AS Target
	USING
	(
		VALUES
			('kEdc', 'Total EDC', 'Total EDC'),
			('StdEnergy', 'EII Standard Energy (MBtu/d)', 'Energy Intensity Index Standard Energy (MBtu/d)'),
			('Pers', 'Personnel Efficiency (K Hours)', 'Personnel Efficiency Standard (K Hours)'),
			('PersMaint', 'Maintenance (K Hours)', 'Personnel Efficiency Maintenance Standard (K Hours)'),
			('PersNonMaint', 'Non-Maintenance (K Hours)', 'Personnel Efficiency Non-Maintenance Standard (K Hours)'),
			('Mes', 'Maintenance', 'Maintenance Efficiency Standard (K USD)'),
			('NonEnergy', 'Non-Energy', 'Non-Energy Efficiency Standard (K USD)')
	)
	AS Source([StandardTag], [StandardName], [StandardDetail])
	ON	Target.[StandardTag]		= Source.[StandardTag]
	WHEN MATCHED THEN UPDATE SET
		Target.[StandardName]		= Source.[StandardName],
		Target.[StandardDetail]	= Source.[StandardDetail]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([StandardTag], [StandardName], [StandardDetail])
		VALUES([StandardTag], [StandardName], [StandardDetail])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	MERGE INTO [dim].[Standard_Parent] AS Target
	USING
	(
		SELECT
			m.MethodologyId,
			l.StandardId,
			p.StandardId,
			t.Operator,
			t.SortKey,
			'/'
		FROM (VALUES
			('kEdc', 'kEdc',			'+', 1),
			('StdEnergy', 'StdEnergy',	'+', 2),
			('Pers', 'Pers',			'+', 6),
			('PersMaint', 'Pers',		'+', 4),
			('PersNonMaint', 'Pers',	'+', 5),
			('Mes', 'Mes',				'+', 7),
			('NonEnergy', 'NonEnergy',	'+', 8)
			)	t(StandardTag, ParentTag, Operator, SortKey)
		INNER JOIN [dim].[Standard_LookUp]			l
			ON	l.StandardTag = t.StandardTag
		INNER JOIN [dim].[Standard_LookUp]			p
			ON	p.StandardTag = t.ParentTag
		INNER JOIN [ante].[Methodology]				m
			ON	m.[MethodologyTag] = '2013'
	)
	AS Source([MethodologyId], [StandardId], [ParentId], [Operator], [SortKey], [Hierarchy])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StandardId]	= Source.[StandardId]
	WHEN MATCHED THEN UPDATE SET
		Target.[ParentId]		= Source.[ParentId],
		Target.[Operator]		= Source.[Operator],
		Target.[SortKey]		= Source.[SortKey],
		Target.[Hierarchy]		= Source.[Hierarchy]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StandardId], [ParentId], [Operator], [SortKey], [Hierarchy])
		VALUES([MethodologyId], [StandardId], [ParentId], [Operator], [SortKey], [Hierarchy])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	EXECUTE dim.Update_Parent 'dim', 'Standard_Parent', 'MethodologyId', 'StandardId', 'ParentId', 'SortKey', 'Hierarchy';
	EXECUTE dim.Merge_Bridge 'dim', 'Standard_Parent', 'dim', 'Standard_Bridge', 'MethodologyId', 'StandardId', 'SortKey', 'Hierarchy', 'Operator';

END;