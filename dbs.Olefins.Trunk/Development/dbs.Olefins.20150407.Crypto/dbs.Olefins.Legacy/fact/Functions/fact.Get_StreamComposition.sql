﻿CREATE FUNCTION [fact].[Get_StreamComposition]
(
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		c.[SubmissionId],
		c.[StreamNumber],
		c.[ComponentId],
		c.[Component_WtPcnt]
	FROM [fact].[StreamComposition]		c
	WHERE	c.[SubmissionId] = @SubmissionId
);