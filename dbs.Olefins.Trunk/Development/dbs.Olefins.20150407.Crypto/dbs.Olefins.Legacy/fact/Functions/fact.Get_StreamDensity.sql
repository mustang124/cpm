﻿CREATE FUNCTION [fact].[Get_StreamDensity]
(
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		d.[SubmissionId],
		d.[StreamNumber],
		d.[Density_SG]
	FROM [fact].[StreamDensity]		d
	WHERE	d.[SubmissionId] = @SubmissionId
);