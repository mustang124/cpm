﻿CREATE PROCEDURE [fact].[Insert_SubmissionComments]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[SubmissionComments]([SubmissionId], [SubmissionComment])
	SELECT
		c.[SubmissionId],
		c.[SubmissionComment]
	FROM [stage].[SubmissionComments]		c
	INNER JOIN [stage].[Submissions]		z
		ON	z.[SubmissionId]		= c.[SubmissionId]
	WHERE	c.[SubmissionId]		= @SubmissionId
		AND	c.[SubmissionComment]	IS NOT NULL;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;