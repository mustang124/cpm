﻿CREATE PROCEDURE [fact].[Insert_StreamRecycled]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[StreamRecycled]([SubmissionId], [ComponentId], [Recycled_WtPcnt])
	SELECT
		r.[SubmissionId],
		r.[ComponentId],
		r.[Recycled_WtPcnt]
	FROM [stage].[StreamRecycled]		r
	INNER JOIN [stage].[Submissions]	z
		ON	z.[SubmissionId]	= r.[SubmissionId]
	WHERE	r.[SubmissionId]	= @SubmissionId
		AND	r.[Recycled_WtPcnt]	> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;