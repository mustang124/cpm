﻿CREATE TABLE [dbo].[Rank] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [RankSpecsID] INT            NOT NULL,
    [Rank]        SMALLINT       NULL,
    [Percentile]  REAL           NULL,
    [Tile]        TINYINT        NULL,
    [Value]       FLOAT (53)     NULL,
    CONSTRAINT [PK_Rank_1] PRIMARY KEY CLUSTERED ([Refnum] ASC, [RankSpecsID] ASC) WITH (FILLFACTOR = 50)
);

