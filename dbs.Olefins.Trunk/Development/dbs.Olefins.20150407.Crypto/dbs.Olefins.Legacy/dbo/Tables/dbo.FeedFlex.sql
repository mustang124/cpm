﻿CREATE TABLE [dbo].[FeedFlex] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [FeedProdID] VARCHAR (20)   NOT NULL,
    [PlantCap]   REAL           NULL,
    [ActCap]     REAL           NULL,
    CONSTRAINT [PK___7__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

