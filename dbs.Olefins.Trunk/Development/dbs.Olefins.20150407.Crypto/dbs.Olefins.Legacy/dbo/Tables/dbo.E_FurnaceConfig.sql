﻿CREATE TABLE [dbo].[E_FurnaceConfig] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [FurnRpt]        SMALLINT       NULL,
    [FFd_Furn]       SMALLINT       NULL,
    [FFd_FurnType]   SMALLINT       NULL,
    [Rec_Furn]       SMALLINT       NULL,
    [EthRec]         SMALLINT       NULL,
    [ProRec]         SMALLINT       NULL,
    [ButRec]         SMALLINT       NULL,
    [HPS]            SMALLINT       NULL,
    [LPS]            SMALLINT       NULL,
    [Specialty1]     SMALLINT       NULL,
    [Specialty2]     SMALLINT       NULL,
    [TotFurn]        SMALLINT       NULL,
    [FFdAvgCap]      REAL           NULL,
    [FFdWtAvgEff]    REAL           NULL,
    [BoilerAvgCap]   REAL           NULL,
    [BoilerWtAvgEff] REAL           NULL,
    CONSTRAINT [PK___E_FurnaceConfig] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

