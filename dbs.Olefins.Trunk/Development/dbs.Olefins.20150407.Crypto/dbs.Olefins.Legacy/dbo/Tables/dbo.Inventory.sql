﻿CREATE TABLE [dbo].[Inventory] (
    [Refnum]   [dbo].[Refnum] NOT NULL,
    [TankType] CHAR (2)       NOT NULL,
    [KMT]      REAL           NULL,
    [YEIL]     REAL           NULL,
    CONSTRAINT [PK___3__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TankType] ASC)
);

