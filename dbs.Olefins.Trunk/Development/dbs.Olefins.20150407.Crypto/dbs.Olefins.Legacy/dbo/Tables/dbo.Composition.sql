﻿CREATE TABLE [dbo].[Composition] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [FeedProdID] VARCHAR (20)   NOT NULL,
    [H2]         REAL           NULL,
    [CH4]        REAL           NULL,
    [C2H2]       REAL           NULL,
    [C2H6]       REAL           NULL,
    [C2H4]       REAL           NULL,
    [C3H6]       REAL           NULL,
    [C3H8]       REAL           NULL,
    [BUTAD]      REAL           NULL,
    [C4S]        REAL           NULL,
    [C4H10]      REAL           NULL,
    [BZ]         REAL           NULL,
    [PYGAS]      REAL           NULL,
    [PYOIL]      REAL           NULL,
    [INERT]      REAL           NULL,
    CONSTRAINT [PK___Composition] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

