﻿CREATE TABLE [dbo].[RefList] (
    [RefListNo] INT            NOT NULL,
    [Refnum]    [dbo].[Refnum] NOT NULL,
    [UserGroup] CHAR (5)       CONSTRAINT [DF__RefList__UserGro__1FCDBCEB] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_RefList_1__18] PRIMARY KEY CLUSTERED ([RefListNo] ASC, [UserGroup] ASC, [Refnum] ASC)
);

