﻿CREATE TABLE [dbo].[MetaOpex] (
    [Refnum] [dbo].[Refnum] NOT NULL,
    [Type]   CHAR (10)      NOT NULL,
    [NVE]    REAL           NULL,
    [Vol]    REAL           NULL,
    CONSTRAINT [PK_MetaOpex] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Type] ASC)
);

