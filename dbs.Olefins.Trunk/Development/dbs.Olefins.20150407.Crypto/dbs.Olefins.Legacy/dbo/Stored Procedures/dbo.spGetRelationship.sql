﻿
/****** Object:  Stored Procedure dbo.spGetRelationship    Script Date: 4/18/2003 4:32:54 PM ******/

/****** Object:  Stored Procedure dbo.spGetRelationship    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.spGetRelationship    Script Date: 04/13/2000 8:32:50 AM ******/
CREATE PROCEDURE spGetRelationship @ObjNameA varchar(30), @ObjNameB varchar(30)
AS
DECLARE @ObjIDA integer, @ObjIDB integer, @TempObj integer
EXEC procGetObjectID @ObjNameA, @ObjIDA OUTPUT
EXEC procGetObjectID @ObjNameB, @ObjIDB OUTPUT
IF @ObjIDB < @ObjIDA 
BEGIN
	SELECT @TempObj = @ObjIDA
	SELECT @ObjIDA = @ObjIDB
	SELECT @ObjIDB = @TempObj
END
SELECT a.ObjectName ObjectA, b.ObjectName ObjectB, c.ColumnName ColumnA, d.ColumnName ColumnB
FROM DD_Objects a, DD_Objects b, DD_Columns c, DD_Columns d, DD_Relationships r
WHERE a.ObjectID = c.ObjectID AND b.ObjectID = d.ObjectID
AND ((r.ObjectA = a.ObjectID AND r.ColumnA = c.ColumnID)
AND (r.ObjectB = b.ObjectID AND r.ColumnB = d.ColumnID))
AND r.ObjectA = @ObjIDA AND r.ObjectB = @ObjIDB
