﻿

CREATE PROCEDURE spRankInsert;2
@Refnum Refnum,
@RankSpecsID integer,
@Rank smallint,
@Percentile real,
@Tile Tinyint,
@Value float
AS
INSERT Rank (Refnum, RankSpecsID, Rank, Percentile, Tile, Value)
VALUES (@Refnum, @RankSpecsID, @Rank, @Percentile, @Tile, @Value)

