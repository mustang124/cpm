﻿
/****** Object:  Stored Procedure dbo.spDeleteReportSet    Script Date: 4/18/2003 4:32:54 PM ******/


/****** Object:  Stored Procedure dbo.spDeleteReportSet    Script Date: 12/28/2001 7:34:43 AM ******/
CREATE PROCEDURE spDeleteReportSet 
	@ReportSetID integer = NULL, @ReportSetName varchar(30) = NULL
AS
DECLARE @Result integer, @ErrMsg varchar(255)
IF @ReportSetID IS NOT NULL
BEGIN
	EXECUTE @Result = spCheckReportSetOwner @ReportSetID, NULL, NULL
	IF @Result >= 0
	BEGIN
		DELETE FROM ReportOptions
		WHERE ReportSetID = @ReportSetID
		DELETE FROM ReportSets
		WHERE ReportSetID = @ReportSetID
	END
	
	ELSE BEGIN
		SELECT @ErrMsg = 'Insufficient rights to delete ReportSet ' +
				CONVERT(varchar(20), @ReportSetID)
		RAISERROR(@ErrMsg, 11, -1)
	END	
END
ELSE BEGIN
	IF @ReportSetName IS NOT NULL
	BEGIN
		EXECUTE @Result = spCheckReportSetOwner NULL, @ReportSetName, NULL
		IF @Result >= 0
		BEGIN
			DELETE FROM ReportOptions
			WHERE ReportSetID = (SELECT ReportSetID FROM ReportSets WHERE ReportSetName = @ReportSetName)
	
			DELETE FROM ReportSets
			WHERE ReportSetName = @ReportSetName
		END
		ELSE BEGIN
			SELECT @ErrMsg = 'Insufficient rights to delete ' + @ReportSetName
			RAISERROR(@ErrMsg, 11, -1)
		END
	END
END


