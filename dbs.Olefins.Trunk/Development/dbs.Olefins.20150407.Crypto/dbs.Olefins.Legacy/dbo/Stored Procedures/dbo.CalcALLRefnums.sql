﻿
CREATE PROCEDURE [dbo].[CalcALLRefnums] 
	@MethodologyYear smallint
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Refnum dbo.Refnum;
	
	BEGIN DECLARE curTemp CURSOR LOCAL FAST_FORWARD FOR
	SELECT	Refnum
	FROM	dbo._RL
	WHERE	UPPER(ListName) in ('03PCH','05PCH','07PCH','09PCH','11PCH')
	ORDER BY Refnum ASC	END;
	
	OPEN curTemp;
	FETCH NEXT FROM curTemp INTO @Refnum;
	WHILE @@FETCH_STATUS = 0
	BEGIN

		PRINT @Refnum;
		
		EXECUTE dbo.CalcEDC @Refnum, @MethodologyYear;
		EXECUTE dbo.CalcEII @Refnum, @MethodologyYear;
		--EXECUTE [dbo].[RefreshNewFactors] @Refnum	-- RRH 20131015: commented for late participants; new factors are still being developed
		
		--EXECUTE CalcFurnRely @Refnum
		--EXECUTE CalcUnitOPEX @Refnum

	FETCH NEXT FROM curTemp INTO @Refnum;
	END;

	-- Free variables and memory
	CLOSE curTemp;
	DEALLOCATE curTemp;
	
END


