﻿CREATE PROCEDURE [dbo].[Get_FactorsAndStandardsRefnum]
(
	@Refnum	VARCHAR(42)
)
AS
BEGIN

	DECLARE	@MethodologyId			INT;
	DECLARE	@SubmissionId			INT;

	SET @MethodologyId = IDENT_CURRENT('[ante].[Methdoology]');

	SET @SubmissionId = [auth].[Return_SubmissionId]('Solomon Plant', @Refnum);

	EXECUTE [web].[Get_FactorsAndStandards] @MethodologyId, @SubmissionId;

END;
