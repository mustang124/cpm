﻿CREATE PROCEDURE [dbo].[Insert_StreamRecycled]
(
	@SubmissionId			INT,
	@ComponentId			INT,

	@Recycled_WtPcnt		FLOAT
)
AS
BEGIN

	IF(@Recycled_WtPcnt >= 0.0)
	EXECUTE [stage].[Insert_StreamRecycled] @SubmissionId, @ComponentId, @Recycled_WtPcnt;

END;
