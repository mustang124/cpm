﻿ 

/****** Object:  Stored Procedure dbo.CalcFurnRely    Script Date: 6/19/2008 1:55:04 PM ******/
 
/****** Object:  Stored Procedure dbo.CalcFurnRely    Script Date: 6/10/2008 12:52:15 PM ******/
 

CREATE     PROC [dbo].[CalcFurnRely] (@Refnum Refnum) AS
Declare @DaysInYear Real
Select @DaysInYear = 365
--
-- Special Calcs for Frunace Calculations
--  First, force categories for REPORTS
--
 
--  5-16-2012  
--OtherMajorCost and TotCostMT where using FeedQtyKMTA, changed to FeedCapMTD
UPDATE FurnRely
Set FurnFeedBreak = CASE FurnFeed  
 WHEN 'Ethane' THEN 'ETHANE'
 WHEN 'Propane' THEN 'PROPANE'
 WHEN 'Butane' THEN 'BUTANE'
 WHEN 'Naphtha' THEN 'NAPHTHA'
 WHEN 'Gasoil<390' THEN 'GASOIL'
 WHEN 'GasOil>390' THEN 'GASOIL'
 ELSE 'Other'
 END
Where Refnum = @Refnum
--
-- Now Lets Annualize and normalize the $
--
UPDATE FurnRely
Set  AnnRetubeCostMatlMT  = RetubeCostMatl * 1000 / (RetubeInterval/12.0) / (FeedCapMTD * @DaysInYear)
 ,AnnRetubeCostLaborMT  = RetubeCostLabor * 1000 / (RetubeInterval/12.0) / (FeedCapMTD * @DaysInYear)
 ,AnnRetubeCostMT  = (ISNULL(RetubeCostMatl,0)+ISNULL(RetubeCostLabor,0)) * 1000 / (RetubeInterval/12.0) / (FeedCapMTD * @DaysInYear)
Where Refnum = @Refnum 
 and FeedCapMTD IS NOT NULL and FeedCapMTD > 0
 and RetubeInterval IS NOT NULL and RetubeInterval > 0
--
-- Don't need to worry about the interval here
--
UPDATE FurnRely
Set  OtherMajorCostMT  = OtherMajorCost * 1000 / (FeedCapMTD * @DaysInYear)
 ,TotCostMT   = TotCost * 1000 / (FeedCapMTD * @DaysInYear)
Where Refnum = @Refnum 
 and FeedCapMTD IS NOT NULL and FeedCapMTD > 0
--
-- FuelType - for reports
--
UPDATE FurnRely
Set FuelTypeYN = CASE WHEN FuelType IS NULL THEN 0 ELSE 1 END
   ,FuelTypeFG = CASE WHEN FuelType = 'FG' THEN 1 ELSE 0 END
   ,FuelTypeFO = CASE WHEN FuelType = 'FO' THEN 1 ELSE 0 END
Where Refnum = @Refnum
--
 
 
 
 
 
