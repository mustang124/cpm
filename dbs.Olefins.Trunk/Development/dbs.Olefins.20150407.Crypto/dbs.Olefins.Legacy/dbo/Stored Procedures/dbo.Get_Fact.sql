﻿CREATE PROCEDURE [dbo].[Get_Fact]
(
	@SubmissionId			INT
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Submissions
	DECLARE	@SubmissionName					NVARCHAR(42);
	DECLARE	@DateBeg						DATE;
	DECLARE	@DateEnd						DATE;

	SELECT
		@SubmissionName = s.[SubmissionName],
		@DateBeg = s.[DateBeg],
		@DateEnd = s.[DateEnd]
	FROM [fact].[Get_Submission](@SubmissionId)	s;

	DECLARE	@SubmissionComment				NVARCHAR(MAX);

	SELECT
		@SubmissionComment = s.[SubmissionComment]
	FROM [fact].[Get_SubmissionComments](@SubmissionId)	s;

	--	Capacity
	DECLARE	@Ethylene_StreamDay_MTSD		FLOAT;	--	[87]
	DECLARE	@Propylene_StreamDay_MTSD		FLOAT;	--	[90]
	DECLARE	@Olefins_StreamDay_MTSD			FLOAT;	--	[86]

	SELECT
		@Ethylene_StreamDay_MTSD	= u.[87],
		@Propylene_StreamDay_MTSD	= u.[90],
		@Olefins_StreamDay_MTSD		= u.[86]
	FROM (
		SELECT	
			c.[SubmissionId],
			c.[StreamId],
			c.[StreamDay_MTSD]
		FROM	[fact].[Get_Capacity](@SubmissionId)	c
		) p
		PIVOT (
		MAX(p.[StreamDay_MTSD]) FOR p.[StreamId] IN ([87], [90], [86])
		) u;

	IF(@Olefins_StreamDay_MTSD IS NULL)
	SET @Olefins_StreamDay_MTSD = @Ethylene_StreamDay_MTSD + COALESCE(@Propylene_StreamDay_MTSD, 0.0);

	IF(@Propylene_StreamDay_MTSD IS NULL)
	SET @Propylene_StreamDay_MTSD = @Olefins_StreamDay_MTSD - @Ethylene_StreamDay_MTSD;

	--	Facility Count
	DECLARE	@FracFeed_UnitCount				INT;	--	[28]
	DECLARE	@BoilHP_UnitCount				INT;	--	[66]
	DECLARE	@BoilLP_UnitCount				INT;	--	[67]
	DECLARE	@ElecGen_UnitCount				INT;	--	[70]
	DECLARE	@HydroPurCryogenic_UnitCount	INT;	--	[54]
	DECLARE	@HydroPurPSA_UnitCount			INT;	--	[55]
	DECLARE	@HydroPurMembrane_UnitCount		INT;	--	[56]
	DECLARE	@TowerPyroGasHT_UnitCount		INT;	--	[41]

	SELECT
		@FracFeed_UnitCount				= [28],
		@BoilHP_UnitCount				= [66],
		@BoilLP_UnitCount				= [67],
		@ElecGen_UnitCount				= [70],
		@HydroPurCryogenic_UnitCount	= [54],
		@HydroPurPSA_UnitCount			= [55],
		@HydroPurMembrane_UnitCount		= [56],
		@TowerPyroGasHT_UnitCount		= [41]
	FROM (
		SELECT	
			f.[SubmissionId],
			f.[FacilityId],
			f.[Unit_Count]
		FROM	[fact].[Get_Facilities](@SubmissionId)	f
		) p
		PIVOT (
		MAX(p.[Unit_Count]) FOR p.[FacilityId] IN ([28], [66], [67], [70], [54], [55], [56], [41])
		) u;

	DECLARE	@Trains_UnitCount				INT;

	SELECT
		@Trains_UnitCount = t.Train_Count
	FROM [fact].[Get_FacilitiesTrains](@SubmissionId)	t;

	--	Facility - Boilers
	DECLARE	@BoilHP_Rate_kLbHr				FLOAT;
	DECLARE	@BoilLP_Rate_kLbHr				FLOAT;
	
	SELECT
		@BoilHP_Rate_kLbHr	= u.[66],
		@BoilLP_Rate_kLbHr	= u.[67]
	FROM (
		SELECT	
			f.[SubmissionId],
			f.[FacilityId],
			f.[Rate_kLbHr]
		FROM	[fact].[Get_FacilitiesBoilers](@SubmissionId)	f
		) p
		PIVOT (
		MAX(p.[Rate_kLbHr]) FOR p.[FacilityId] IN ([66], [67])
		) u;

	--	Facility - Electricity Generation
	DECLARE	@Capacity_MW					FLOAT;

	SELECT
		@Capacity_MW = f.[Capacity_MW]
	FROM [fact].[Get_FacilitiesElecGeneration](@SubmissionId)	f;

	--	Facility - FracFeed
	DECLARE	@FracFeed_Quantity_kBSD			FLOAT;
	DECLARE	@FracFeed_StreamId				INT;
	DECLARE	@FracFeed_Throughput_kMT		FLOAT;
	DECLARE	@FracFeed_Density_SG			FLOAT;

	SELECT
		@FracFeed_Quantity_kBSD		= f.[Quantity_kBSD],
		@FracFeed_StreamId			= f.[StreamId],
		@FracFeed_Throughput_kMT	= f.[Throughput_kMT],
		@FracFeed_Density_SG		= f.[Density_SG]
	FROM [fact].[Get_FacilitiesFractionator](@SubmissionId)	f;

	--	Facility - HydroTreater
	DECLARE	@HT_Quantity_kBSD				FLOAT;
	DECLARE	@HT_HydroTreaterTypeId			INT;
	DECLARE	@HT_Pressure_PSIg				FLOAT;
	DECLARE	@HT_Processed_kMT				FLOAT;
	DECLARE	@HT_Processed_Pcnt				FLOAT;
	DECLARE	@HT_Density_SG					FLOAT;

	SELECT
		@HT_Quantity_kBSD			= f.[Quantity_kBSD],
		@HT_HydroTreaterTypeId		= f.[HydroTreaterTypeId],
		@HT_Pressure_PSIg			= f.[Pressure_PSIg],
		@HT_Processed_kMT			= f.[Processed_kMT],
		@HT_Processed_Pcnt			= f.[Processed_Pcnt],
		@HT_Density_SG				= f.[Density_SG]
	FROM [fact].[Get_FacilitiesHydroTreater](@SubmissionId)	f;

	-- Stream - Quantity
	BEGIN

	DECLARE	@St_1001_kMT					FLOAT;
	DECLARE	@St_1002_kMT					FLOAT;
	DECLARE	@St_1003_kMT					FLOAT;
	DECLARE	@St_1004_kMT					FLOAT;
	DECLARE	@St_1005_kMT					FLOAT;
	DECLARE	@St_1006_kMT					FLOAT;

	DECLARE	@St_2001_kMT					FLOAT;
	DECLARE	@St_2002_kMT					FLOAT;
	DECLARE	@St_2003_kMT					FLOAT;
	DECLARE	@St_2004_kMT					FLOAT;
	DECLARE	@St_2005_kMT					FLOAT;
	DECLARE	@St_2006_kMT					FLOAT;
	DECLARE	@St_2007_kMT					FLOAT;
	DECLARE	@St_2008_kMT					FLOAT;
	DECLARE	@St_2009_kMT					FLOAT;
	DECLARE	@St_2010_kMT					FLOAT;
	DECLARE	@St_2011_kMT					FLOAT;
	DECLARE	@St_2012_kMT					FLOAT;

	DECLARE	@St_4001_kMT					FLOAT;
	DECLARE	@St_4002_kMT					FLOAT;
	DECLARE	@St_4003_kMT					FLOAT;
	DECLARE	@St_4004_kMT					FLOAT;

	DECLARE	@St_4005_kMT					FLOAT;
	DECLARE	@St_4006_kMT					FLOAT;
	DECLARE	@St_4007_kMT					FLOAT;
	DECLARE	@St_4008_kMT					FLOAT;
	DECLARE	@St_4009_kMT					FLOAT;
	DECLARE	@St_4010_kMT					FLOAT;
	DECLARE	@St_4011_kMT					FLOAT;
	DECLARE	@St_4012_kMT					FLOAT;

	DECLARE	@St_4013_kMT					FLOAT;
	DECLARE	@St_4014_kMT					FLOAT;
	DECLARE	@St_4015_kMT					FLOAT;
	DECLARE	@St_4016_kMT					FLOAT;
	DECLARE	@St_4017_kMT					FLOAT;
	DECLARE	@St_4018_kMT					FLOAT;
	DECLARE	@St_4019_kMT					FLOAT;
	DECLARE	@St_4020_kMT					FLOAT;
	DECLARE	@St_4021_kMT					FLOAT;
	DECLARE	@St_4022_kMT					FLOAT;
	DECLARE	@St_4023_kMT					FLOAT;
	DECLARE	@St_4024_kMT					FLOAT;
	DECLARE	@St_4025_kMT					FLOAT;
	DECLARE	@St_4026_kMT					FLOAT;

	DECLARE	@St_5001_kMT					FLOAT;
	DECLARE	@St_5002_kMT					FLOAT;
	DECLARE	@St_5003_kMT					FLOAT;
	DECLARE	@St_5004_kMT					FLOAT;
	DECLARE	@St_5005_kMT					FLOAT;
	DECLARE	@St_5006_kMT					FLOAT;
	DECLARE	@St_5007_kMT					FLOAT;
	DECLARE	@St_5008_kMT					FLOAT;
	DECLARE	@St_5009_kMT					FLOAT;
	DECLARE	@St_5010_kMT					FLOAT;
	DECLARE	@St_5011_kMT					FLOAT;
	DECLARE	@St_5012_kMT					FLOAT;
	DECLARE	@St_5013_kMT					FLOAT;
	DECLARE	@St_5014_kMT					FLOAT;
	DECLARE	@St_5015_kMT					FLOAT;
	DECLARE	@St_5016_kMT					FLOAT;
	DECLARE	@St_5017_kMT					FLOAT;
	DECLARE	@St_5018_kMT					FLOAT;
	DECLARE	@St_5019_kMT					FLOAT;
	DECLARE	@St_5020_kMT					FLOAT;
	DECLARE	@St_5022_kMT					FLOAT;
	DECLARE	@St_5023_kMT					FLOAT;
	DECLARE	@St_5024_kMT					FLOAT;
	
	SELECT
		@St_1001_kMT	= u.[1001],
		@St_1002_kMT	= u.[1002],
		@St_1003_kMT	= u.[1003],
		@St_1004_kMT	= u.[1004],
		@St_1005_kMT	= u.[1005],
		@St_1006_kMT	= u.[1006],

		@St_2001_kMT	= u.[2001],
		@St_2002_kMT	= u.[2002],
		@St_2003_kMT	= u.[2003],
		@St_2004_kMT	= u.[2004],
		@St_2005_kMT	= u.[2005],
		@St_2006_kMT	= u.[2006],
		@St_2007_kMT	= u.[2007],
		@St_2008_kMT	= u.[2008],
		@St_2009_kMT	= u.[2009],
		@St_2010_kMT	= u.[2010],
		@St_2011_kMT	= u.[2011],
		@St_2012_kMT	= u.[2012],

		@St_4001_kMT	= u.[4001],
		@St_4002_kMT	= u.[4002],
		@St_4003_kMT	= u.[4003],
		@St_4004_kMT	= u.[4004],

		@St_4005_kMT	= u.[4005],
		@St_4006_kMT	= u.[4006],
		@St_4007_kMT	= u.[4007],
		@St_4008_kMT	= u.[4008],
		@St_4009_kMT	= u.[4009],
		@St_4010_kMT	= u.[4010],
		@St_4011_kMT	= u.[4011],
		@St_4012_kMT	= u.[4012],

		@St_4013_kMT	= u.[4013],
		@St_4014_kMT	= u.[4014],
		@St_4015_kMT	= u.[4015],
		@St_4016_kMT	= u.[4016],
		@St_4017_kMT	= u.[4017],
		@St_4018_kMT	= u.[4018],
		@St_4019_kMT	= u.[4019],
		@St_4020_kMT	= u.[4020],
		@St_4021_kMT	= u.[4021],
		@St_4022_kMT	= u.[4022],
		@St_4023_kMT	= u.[4023],
		@St_4024_kMT	= u.[4024],
		@St_4025_kMT	= u.[4025],
		@St_4026_kMT	= u.[4026],

		@St_5001_kMT	= u.[5001],
		@St_5002_kMT	= u.[5002],
		@St_5003_kMT	= u.[5003],
		@St_5004_kMT	= u.[5004],
		@St_5005_kMT	= u.[5005],
		@St_5006_kMT	= u.[5006],
		@St_5007_kMT	= u.[5007],
		@St_5008_kMT	= u.[5008],
		@St_5009_kMT	= u.[5009],
		@St_5010_kMT	= u.[5010],
		@St_5011_kMT	= u.[5011],
		@St_5012_kMT	= u.[5012],
		@St_5013_kMT	= u.[5013],
		@St_5014_kMT	= u.[5014],
		@St_5015_kMT	= u.[5015],
		@St_5016_kMT	= u.[5016],
		@St_5017_kMT	= u.[5017],
		@St_5018_kMT	= u.[5018],
		@St_5019_kMT	= u.[5019],
		@St_5020_kMT	= u.[5020],
		@St_5022_kMT	= u.[5022],
		@St_5023_kMT	= u.[5023],
		@St_5024_kMT	= u.[5024]
	FROM (
		SELECT	
			c.[SubmissionId],
			c.[StreamNumber],
			c.[Quantity_kMT]
		FROM	[fact].[Get_StreamQuantity](@SubmissionId)	c
		) p
		PIVOT (
		MAX(p.[Quantity_kMT]) FOR p.[StreamNumber] IN (
			[1001], [1002], [1003], [1004], [1005], [1006],
			[2001], [2002], [2003], [2004], [2005], [2006], [2007], [2008], [2009], [2010], [2011], [2012],
			[4001], [4002], [4003], [4004],
			[4005], [4006], [4007], [4008], [4009], [4010], [4011], [4012],
			[4013], [4014], [4015], [4016], [4017], [4018], [4019], [4020], [4021], [4022], [4023], [4024], [4025], [4026],
			[5001], [5002], [5003], [5004], [5005], [5006], [5007], [5008], [5009], [5010], [5011], [5012],
			[5013], [5014], [5015], [5016], [5017], [5018], [5019], [5020], [5022], [5023], [5024]
			)
		) u;

	END;

	--	Stream - Composition
	DECLARE	@St_1001_CH4_WtPcnt				FLOAT;
	DECLARE	@St_1001_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_1001_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_1001_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_1001_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_1001_NBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1001_IBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1001_B2_WtPcnt				FLOAT;
	DECLARE	@St_1001_B1_WtPcnt				FLOAT;
	DECLARE	@St_1001_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_1001_NC5_WtPcnt				FLOAT;
	DECLARE	@St_1001_IC5_WtPcnt				FLOAT;
	DECLARE	@St_1001_NC6_WtPcnt				FLOAT;
	DECLARE	@St_1001_C6ISO_WtPcnt			FLOAT;
	DECLARE	@St_1001_C7H16_WtPcnt			FLOAT;
	DECLARE	@St_1001_C8H18_WtPcnt			FLOAT;
	DECLARE	@St_1001_CO_CO2_WtPcnt			FLOAT;
	DECLARE	@St_1001_H2_WtPcnt				FLOAT;
	DECLARE	@St_1001_S_WtPcnt				FLOAT;

	SELECT
		@St_1001_CH4_WtPcnt		= [17],
		@St_1001_C2H6_WtPcnt	= [18],
		@St_1001_C2H4_WtPcnt	= [15],
		@St_1001_C3H8_WtPcnt	= [19],
		@St_1001_C3H6_WtPcnt	= [16],
		@St_1001_NBUTA_WtPcnt	= [22],
		@St_1001_IBUTA_WtPcnt	= [23],
		@St_1001_B2_WtPcnt		= [27],
		@St_1001_B1_WtPcnt		= [26],
		@St_1001_C4H6_WtPcnt	= [5],
		@St_1001_NC5_WtPcnt		= [98],
		@St_1001_IC5_WtPcnt		= [99],
		@St_1001_NC6_WtPcnt		= [114],
		@St_1001_C6ISO_WtPcnt	= [115],
		@St_1001_C7H16_WtPcnt	= [116],
		@St_1001_C8H18_WtPcnt	= [119],
		@St_1001_CO_CO2_WtPcnt	= [29],
		@St_1001_H2_WtPcnt		= [7],
		@St_1001_S_WtPcnt		= [32]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 1001
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[17], [18], [15], [19], [16], [22], [23], [27], [26], [5], [98], [99], [114], [115], [116], [119], [29], [7], [32])
		) p;

	DECLARE	@St_1002_CH4_WtPcnt				FLOAT;
	DECLARE	@St_1002_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_1002_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_1002_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_1002_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_1002_NBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1002_IBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1002_B2_WtPcnt				FLOAT;
	DECLARE	@St_1002_B1_WtPcnt				FLOAT;
	DECLARE	@St_1002_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_1002_NC5_WtPcnt				FLOAT;
	DECLARE	@St_1002_IC5_WtPcnt				FLOAT;
	DECLARE	@St_1002_NC6_WtPcnt				FLOAT;
	DECLARE	@St_1002_C6ISO_WtPcnt			FLOAT;
	DECLARE	@St_1002_C7H16_WtPcnt			FLOAT;
	DECLARE	@St_1002_C8H18_WtPcnt			FLOAT;
	DECLARE	@St_1002_CO_CO2_WtPcnt			FLOAT;
	DECLARE	@St_1002_H2_WtPcnt				FLOAT;
	DECLARE	@St_1002_S_WtPcnt				FLOAT;

	SELECT
		@St_1002_CH4_WtPcnt		= [17],
		@St_1002_C2H6_WtPcnt	= [18],
		@St_1002_C2H4_WtPcnt	= [15],
		@St_1002_C3H8_WtPcnt	= [19],
		@St_1002_C3H6_WtPcnt	= [16],
		@St_1002_NBUTA_WtPcnt	= [22],
		@St_1002_IBUTA_WtPcnt	= [23],
		@St_1002_B2_WtPcnt		= [27],
		@St_1002_B1_WtPcnt		= [26],
		@St_1002_C4H6_WtPcnt	= [5],
		@St_1002_NC5_WtPcnt		= [98],
		@St_1002_IC5_WtPcnt		= [99],
		@St_1002_NC6_WtPcnt		= [114],
		@St_1002_C6ISO_WtPcnt	= [115],
		@St_1002_C7H16_WtPcnt	= [116],
		@St_1002_C8H18_WtPcnt	= [119],
		@St_1002_CO_CO2_WtPcnt	= [29],
		@St_1002_H2_WtPcnt		= [7],
		@St_1002_S_WtPcnt		= [32]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 1002
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[17], [18], [15], [19], [16], [22], [23], [27], [26], [5], [98], [99], [114], [115], [116], [119], [29], [7], [32])
		) p;

	DECLARE	@St_1003_CH4_WtPcnt				FLOAT;
	DECLARE	@St_1003_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_1003_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_1003_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_1003_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_1003_NBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1003_IBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1003_B2_WtPcnt				FLOAT;
	DECLARE	@St_1003_B1_WtPcnt				FLOAT;
	DECLARE	@St_1003_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_1003_NC5_WtPcnt				FLOAT;
	DECLARE	@St_1003_IC5_WtPcnt				FLOAT;
	DECLARE	@St_1003_NC6_WtPcnt				FLOAT;
	DECLARE	@St_1003_C6ISO_WtPcnt			FLOAT;
	DECLARE	@St_1003_C7H16_WtPcnt			FLOAT;
	DECLARE	@St_1003_C8H18_WtPcnt			FLOAT;
	DECLARE	@St_1003_CO_CO2_WtPcnt			FLOAT;
	DECLARE	@St_1003_H2_WtPcnt				FLOAT;
	DECLARE	@St_1003_S_WtPcnt				FLOAT;

	SELECT
		@St_1003_CH4_WtPcnt		= [17],
		@St_1003_C2H6_WtPcnt	= [18],
		@St_1003_C2H4_WtPcnt	= [15],
		@St_1003_C3H8_WtPcnt	= [19],
		@St_1003_C3H6_WtPcnt	= [16],
		@St_1003_NBUTA_WtPcnt	= [22],
		@St_1003_IBUTA_WtPcnt	= [23],
		@St_1003_B2_WtPcnt		= [27],
		@St_1003_B1_WtPcnt		= [26],
		@St_1003_C4H6_WtPcnt	= [5],
		@St_1003_NC5_WtPcnt		= [98],
		@St_1003_IC5_WtPcnt		= [99],
		@St_1003_NC6_WtPcnt		= [114],
		@St_1003_C6ISO_WtPcnt	= [115],
		@St_1003_C7H16_WtPcnt	= [116],
		@St_1003_C8H18_WtPcnt	= [119],
		@St_1003_CO_CO2_WtPcnt	= [29],
		@St_1003_H2_WtPcnt		= [7],
		@St_1003_S_WtPcnt		= [32]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 1003
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[17], [18], [15], [19], [16], [22], [23], [27], [26], [5], [98], [99], [114], [115], [116], [119], [29], [7], [32])
		) p;

	DECLARE	@St_1004_CH4_WtPcnt				FLOAT;
	DECLARE	@St_1004_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_1004_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_1004_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_1004_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_1004_NBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1004_IBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1004_B2_WtPcnt				FLOAT;
	DECLARE	@St_1004_B1_WtPcnt				FLOAT;
	DECLARE	@St_1004_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_1004_NC5_WtPcnt				FLOAT;
	DECLARE	@St_1004_IC5_WtPcnt				FLOAT;
	DECLARE	@St_1004_NC6_WtPcnt				FLOAT;
	DECLARE	@St_1004_C6ISO_WtPcnt			FLOAT;
	DECLARE	@St_1004_C7H16_WtPcnt			FLOAT;
	DECLARE	@St_1004_C8H18_WtPcnt			FLOAT;
	DECLARE	@St_1004_CO_CO2_WtPcnt			FLOAT;
	DECLARE	@St_1004_H2_WtPcnt				FLOAT;
	DECLARE	@St_1004_S_WtPcnt				FLOAT;

	SELECT
		@St_1004_CH4_WtPcnt		= [17],
		@St_1004_C2H6_WtPcnt	= [18],
		@St_1004_C2H4_WtPcnt	= [15],
		@St_1004_C3H8_WtPcnt	= [19],
		@St_1004_C3H6_WtPcnt	= [16],
		@St_1004_NBUTA_WtPcnt	= [22],
		@St_1004_IBUTA_WtPcnt	= [23],
		@St_1004_B2_WtPcnt		= [27],
		@St_1004_B1_WtPcnt		= [26],
		@St_1004_C4H6_WtPcnt	= [5],
		@St_1004_NC5_WtPcnt		= [98],
		@St_1004_IC5_WtPcnt		= [99],
		@St_1004_NC6_WtPcnt		= [114],
		@St_1004_C6ISO_WtPcnt	= [115],
		@St_1004_C7H16_WtPcnt	= [116],
		@St_1004_C8H18_WtPcnt	= [119],
		@St_1004_CO_CO2_WtPcnt	= [29],
		@St_1004_H2_WtPcnt		= [7],
		@St_1004_S_WtPcnt		= [32]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 1004
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[17], [18], [15], [19], [16], [22], [23], [27], [26], [5], [98], [99], [114], [115], [116], [119], [29], [7], [32])
		) p;

	DECLARE	@St_1005_CH4_WtPcnt				FLOAT;
	DECLARE	@St_1005_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_1005_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_1005_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_1005_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_1005_NBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1005_IBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1005_B2_WtPcnt				FLOAT;
	DECLARE	@St_1005_B1_WtPcnt				FLOAT;
	DECLARE	@St_1005_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_1005_NC5_WtPcnt				FLOAT;
	DECLARE	@St_1005_IC5_WtPcnt				FLOAT;
	DECLARE	@St_1005_NC6_WtPcnt				FLOAT;
	DECLARE	@St_1005_C6ISO_WtPcnt			FLOAT;
	DECLARE	@St_1005_C7H16_WtPcnt			FLOAT;
	DECLARE	@St_1005_C8H18_WtPcnt			FLOAT;
	DECLARE	@St_1005_CO_CO2_WtPcnt			FLOAT;
	DECLARE	@St_1005_H2_WtPcnt				FLOAT;
	DECLARE	@St_1005_S_WtPcnt				FLOAT;

	SELECT
		@St_1005_CH4_WtPcnt		= [17],
		@St_1005_C2H6_WtPcnt	= [18],
		@St_1005_C2H4_WtPcnt	= [15],
		@St_1005_C3H8_WtPcnt	= [19],
		@St_1005_C3H6_WtPcnt	= [16],
		@St_1005_NBUTA_WtPcnt	= [22],
		@St_1005_IBUTA_WtPcnt	= [23],
		@St_1005_B2_WtPcnt		= [27],
		@St_1005_B1_WtPcnt		= [26],
		@St_1005_C4H6_WtPcnt	= [5],
		@St_1005_NC5_WtPcnt		= [98],
		@St_1005_IC5_WtPcnt		= [99],
		@St_1005_NC6_WtPcnt		= [114],
		@St_1005_C6ISO_WtPcnt	= [115],
		@St_1005_C7H16_WtPcnt	= [116],
		@St_1005_C8H18_WtPcnt	= [119],
		@St_1005_CO_CO2_WtPcnt	= [29],
		@St_1005_H2_WtPcnt		= [7],
		@St_1005_S_WtPcnt		= [32]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 1005
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[17], [18], [15], [19], [16], [22], [23], [27], [26], [5], [98], [99], [114], [115], [116], [119], [29], [7], [32])
		) p;

	DECLARE	@St_1006_CH4_WtPcnt				FLOAT;
	DECLARE	@St_1006_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_1006_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_1006_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_1006_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_1006_NBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1006_IBUTA_WtPcnt			FLOAT;
	DECLARE	@St_1006_B2_WtPcnt				FLOAT;
	DECLARE	@St_1006_B1_WtPcnt				FLOAT;
	DECLARE	@St_1006_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_1006_NC5_WtPcnt				FLOAT;
	DECLARE	@St_1006_IC5_WtPcnt				FLOAT;
	DECLARE	@St_1006_NC6_WtPcnt				FLOAT;
	DECLARE	@St_1006_C6ISO_WtPcnt			FLOAT;
	DECLARE	@St_1006_C7H16_WtPcnt			FLOAT;
	DECLARE	@St_1006_C8H18_WtPcnt			FLOAT;
	DECLARE	@St_1006_CO_CO2_WtPcnt			FLOAT;
	DECLARE	@St_1006_H2_WtPcnt				FLOAT;
	DECLARE	@St_1006_S_WtPcnt				FLOAT;

	SELECT
		@St_1006_CH4_WtPcnt		= [17],
		@St_1006_C2H6_WtPcnt	= [18],
		@St_1006_C2H4_WtPcnt	= [15],
		@St_1006_C3H8_WtPcnt	= [19],
		@St_1006_C3H6_WtPcnt	= [16],
		@St_1006_NBUTA_WtPcnt	= [22],
		@St_1006_IBUTA_WtPcnt	= [23],
		@St_1006_B2_WtPcnt		= [27],
		@St_1006_B1_WtPcnt		= [26],
		@St_1006_C4H6_WtPcnt	= [5],
		@St_1006_NC5_WtPcnt		= [98],
		@St_1006_IC5_WtPcnt		= [99],
		@St_1006_NC6_WtPcnt		= [114],
		@St_1006_C6ISO_WtPcnt	= [115],
		@St_1006_C7H16_WtPcnt	= [116],
		@St_1006_C8H18_WtPcnt	= [119],
		@St_1006_CO_CO2_WtPcnt	= [29],
		@St_1006_H2_WtPcnt		= [7],
		@St_1006_S_WtPcnt		= [32]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 1006
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[17], [18], [15], [19], [16], [22], [23], [27], [26], [5], [98], [99], [114], [115], [116], [119], [29], [7], [32])
		) p;

	DECLARE	@St_2001_P_WtPcnt				FLOAT;
	DECLARE	@St_2001_I_WtPcnt				FLOAT;
	DECLARE	@St_2001_N_WtPcnt				FLOAT;
	DECLARE	@St_2001_O_WtPcnt				FLOAT;
	DECLARE	@St_2001_A_WtPcnt				FLOAT;

	SELECT
		@St_2001_P_WtPcnt	= [169],
		@St_2001_I_WtPcnt	= [170],
		@St_2001_N_WtPcnt	= [171],
		@St_2001_O_WtPcnt	= [172],
		@St_2001_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2001
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2002_P_WtPcnt				FLOAT;
	DECLARE	@St_2002_I_WtPcnt				FLOAT;
	DECLARE	@St_2002_N_WtPcnt				FLOAT;
	DECLARE	@St_2002_O_WtPcnt				FLOAT;
	DECLARE	@St_2002_A_WtPcnt				FLOAT;

	SELECT
		@St_2002_P_WtPcnt	= [169],
		@St_2002_I_WtPcnt	= [170],
		@St_2002_N_WtPcnt	= [171],
		@St_2002_O_WtPcnt	= [172],
		@St_2002_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2002
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2003_P_WtPcnt				FLOAT;
	DECLARE	@St_2003_I_WtPcnt				FLOAT;
	DECLARE	@St_2003_N_WtPcnt				FLOAT;
	DECLARE	@St_2003_O_WtPcnt				FLOAT;
	DECLARE	@St_2003_A_WtPcnt				FLOAT;

	SELECT
		@St_2003_P_WtPcnt	= [169],
		@St_2003_I_WtPcnt	= [170],
		@St_2003_N_WtPcnt	= [171],
		@St_2003_O_WtPcnt	= [172],
		@St_2003_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2003
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2004_P_WtPcnt				FLOAT;
	DECLARE	@St_2004_I_WtPcnt				FLOAT;
	DECLARE	@St_2004_N_WtPcnt				FLOAT;
	DECLARE	@St_2004_O_WtPcnt				FLOAT;
	DECLARE	@St_2004_A_WtPcnt				FLOAT;
	
	SELECT
		@St_2004_P_WtPcnt	= [169],
		@St_2004_I_WtPcnt	= [170],
		@St_2004_N_WtPcnt	= [171],
		@St_2004_O_WtPcnt	= [172],
		@St_2004_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2004
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2005_P_WtPcnt				FLOAT;
	DECLARE	@St_2005_I_WtPcnt				FLOAT;
	DECLARE	@St_2005_N_WtPcnt				FLOAT;
	DECLARE	@St_2005_O_WtPcnt				FLOAT;
	DECLARE	@St_2005_A_WtPcnt				FLOAT;
	
	SELECT
		@St_2005_P_WtPcnt	= [169],
		@St_2005_I_WtPcnt	= [170],
		@St_2005_N_WtPcnt	= [171],
		@St_2005_O_WtPcnt	= [172],
		@St_2005_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2005
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2006_P_WtPcnt				FLOAT;
	DECLARE	@St_2006_I_WtPcnt				FLOAT;
	DECLARE	@St_2006_N_WtPcnt				FLOAT;
	DECLARE	@St_2006_O_WtPcnt				FLOAT;
	DECLARE	@St_2006_A_WtPcnt				FLOAT;
	
	SELECT
		@St_2006_P_WtPcnt	= [169],
		@St_2006_I_WtPcnt	= [170],
		@St_2006_N_WtPcnt	= [171],
		@St_2006_O_WtPcnt	= [172],
		@St_2006_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2006
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2010_P_WtPcnt				FLOAT;
	DECLARE	@St_2010_I_WtPcnt				FLOAT;
	DECLARE	@St_2010_N_WtPcnt				FLOAT;
	DECLARE	@St_2010_O_WtPcnt				FLOAT;
	DECLARE	@St_2010_A_WtPcnt				FLOAT;
	
	SELECT
		@St_2010_P_WtPcnt	= [169],
		@St_2010_I_WtPcnt	= [170],
		@St_2010_N_WtPcnt	= [171],
		@St_2010_O_WtPcnt	= [172],
		@St_2010_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2010
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2011_P_WtPcnt				FLOAT;
	DECLARE	@St_2011_I_WtPcnt				FLOAT;
	DECLARE	@St_2011_N_WtPcnt				FLOAT;
	DECLARE	@St_2011_O_WtPcnt				FLOAT;
	DECLARE	@St_2011_A_WtPcnt				FLOAT;
	
	SELECT
		@St_2011_P_WtPcnt	= [169],
		@St_2011_I_WtPcnt	= [170],
		@St_2011_N_WtPcnt	= [171],
		@St_2011_O_WtPcnt	= [172],
		@St_2011_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2011
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_2012_P_WtPcnt				FLOAT;
	DECLARE	@St_2012_I_WtPcnt				FLOAT;
	DECLARE	@St_2012_N_WtPcnt				FLOAT;
	DECLARE	@St_2012_O_WtPcnt				FLOAT;
	DECLARE	@St_2012_A_WtPcnt				FLOAT;

	SELECT
		@St_2012_P_WtPcnt	= [169],
		@St_2012_I_WtPcnt	= [170],
		@St_2012_N_WtPcnt	= [171],
		@St_2012_O_WtPcnt	= [172],
		@St_2012_A_WtPcnt	= [173]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 2012
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[169], [170], [171], [172], [173]
			)
		) p;

	DECLARE	@St_5001_H2_WtPcnt				FLOAT;
	DECLARE	@St_5002_CH4_WtPcnt				FLOAT;
	DECLARE	@St_5004_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_5005_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_5006_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_5007_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_5008_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_5009_C3H8_WtPcnt			FLOAT;

	SELECT
		@St_5001_H2_WtPcnt			= [5001_07],
		@St_5002_CH4_WtPcnt			= [5002_17],
		@St_5004_C2H4_WtPcnt		= [5004_15],
		@St_5005_C2H4_WtPcnt		= [5005_15],
		@St_5006_C3H6_WtPcnt		= [5006_16],
		@St_5007_C3H6_WtPcnt		= [5007_16],
		@St_5008_C3H6_WtPcnt		= [5008_16],
		@St_5009_C3H8_WtPcnt		= [5009_19]
	FROM (
		SELECT
			c.[SubmissionId],
			CONVERT(CHAR(4), c.[StreamNumber]) + '_' + CONVERT(VARCHAR(3), c.[ComponentId]) [Stream_Comp],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	>= 5001
			AND	c.[StreamNumber]	<= 5009
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[Stream_Comp] IN (
			[5001_07], [5002_17], [5004_15], [5005_15], [5006_16], [5007_16], [5008_16], [5009_19])
		) p;

	DECLARE	@St_5018_H2_WtPcnt				FLOAT;
	DECLARE	@St_5018_CH4_WtPcnt				FLOAT;
	DECLARE	@St_5018_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_5018_C2H6_WtPcnt			FLOAT;

	SELECT
		@St_5018_H2_WtPcnt		= [7],
		@St_5018_CH4_WtPcnt		= [17],
		@St_5018_C2H4_WtPcnt	= [15],
		@St_5018_C2H6_WtPcnt	= [18]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 5018
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[7], [17], [15], [18])
		) p;

	DECLARE	@St_5019_H2_WtPcnt				FLOAT;
	DECLARE	@St_5019_CH4_WtPcnt				FLOAT;
	DECLARE	@St_5019_C2H2_WtPcnt			FLOAT;
	DECLARE	@St_5019_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_5019_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_5019_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_5019_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_5019_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_5019_C4H8_WtPcnt			FLOAT;
	DECLARE	@St_5019_C4H10_WtPcnt			FLOAT;
	DECLARE	@St_5019_C6H6_WtPcnt			FLOAT;
	DECLARE	@St_5019_PyroGasOil_WtPcnt		FLOAT;
	DECLARE	@St_5019_PyroFuelOil_WtPcnt		FLOAT;
	DECLARE	@St_5019_Inerts_WtPcnt			FLOAT;

	SELECT
		@St_5019_H2_WtPcnt			= [7],
		@St_5019_CH4_WtPcnt			= [17],
		@St_5019_C2H2_WtPcnt		= [9],
		@St_5019_C2H4_WtPcnt		= [15],
		@St_5019_C2H6_WtPcnt		= [18],
		@St_5019_C3H6_WtPcnt		= [16],
		@St_5019_C3H8_WtPcnt		= [19],
		@St_5019_C4H6_WtPcnt		= [5],
		@St_5019_C4H8_WtPcnt		= [24],
		@St_5019_C4H10_WtPcnt		= [21],
		@St_5019_C6H6_WtPcnt		= [6],
		@St_5019_PyroGasOil_WtPcnt	= [162],
		@St_5019_PyroFuelOil_WtPcnt	= [163],
		@St_5019_Inerts_WtPcnt		= [164]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 5019
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[7], [17], [9], [15], [18], [16], [19], [5], [24], [21], [6], [162], [163], [164])
		) p;
		
	DECLARE	@St_5020_H2_WtPcnt				FLOAT;
	DECLARE	@St_5020_CH4_WtPcnt				FLOAT;
	DECLARE	@St_5020_C2H2_WtPcnt			FLOAT;
	DECLARE	@St_5020_C2H4_WtPcnt			FLOAT;
	DECLARE	@St_5020_C2H6_WtPcnt			FLOAT;
	DECLARE	@St_5020_C3H6_WtPcnt			FLOAT;
	DECLARE	@St_5020_C3H8_WtPcnt			FLOAT;
	DECLARE	@St_5020_C4H6_WtPcnt			FLOAT;
	DECLARE	@St_5020_C4H8_WtPcnt			FLOAT;
	DECLARE	@St_5020_C4H10_WtPcnt			FLOAT;
	DECLARE	@St_5020_C6H6_WtPcnt			FLOAT;
	DECLARE	@St_5020_PyroGasOil_WtPcnt		FLOAT;
	DECLARE	@St_5020_PyroFuelOil_WtPcnt		FLOAT;
	DECLARE	@St_5020_Inerts_WtPcnt			FLOAT;

	SELECT
		@St_5020_H2_WtPcnt			= [7],
		@St_5020_CH4_WtPcnt			= [17],
		@St_5020_C2H2_WtPcnt		= [9],
		@St_5020_C2H4_WtPcnt		= [15],
		@St_5020_C2H6_WtPcnt		= [18],
		@St_5020_C3H6_WtPcnt		= [16],
		@St_5020_C3H8_WtPcnt		= [19],
		@St_5020_C4H6_WtPcnt		= [5],
		@St_5020_C4H8_WtPcnt		= [24],
		@St_5020_C4H10_WtPcnt		= [21],
		@St_5020_C6H6_WtPcnt		= [6],
		@St_5020_PyroGasOil_WtPcnt	= [162],
		@St_5020_PyroFuelOil_WtPcnt	= [163],
		@St_5020_Inerts_WtPcnt		= [164]
	FROM (
		SELECT
			c.[SubmissionId],
			c.[StreamNumber],
			c.[ComponentId],
			c.[Component_WtPcnt]
		FROM [fact].[Get_StreamComposition](@SubmissionId)	c
		WHERE	c.[StreamNumber]	= 5020
		) u
		PIVOT (
		MAX(u.[Component_WtPcnt]) FOR u.[ComponentId] IN (
			[7], [17], [9], [15], [18], [16], [19], [5], [24], [21], [6], [162], [163], [164])
		) p;

	--	Stream - Composition (Molar)
	BEGIN

	DECLARE	@St_5001_H2_MolPcnt				FLOAT;
	DECLARE	@St_5002_CH4_MolPcnt			FLOAT;
	
	SELECT
		@St_5001_H2_MolPcnt		= u.[5001],
		@St_5002_CH4_MolPcnt	= u.[5002]
	FROM (
		SELECT	
			c.[SubmissionId],
			c.[StreamNumber],
			c.[Component_MolPcnt]
		FROM	[fact].[Get_StreamCompositionMol](@SubmissionId)	c
		) p
		PIVOT (
		MAX(p.[Component_MolPcnt]) FOR p.[StreamNumber] IN (
			[5001], [5002]
			)
		) u;

	END;

	--	Stream - Density
	BEGIN

	DECLARE	@St_2001_Density_SG				FLOAT;
	DECLARE	@St_2002_Density_SG				FLOAT;
	DECLARE	@St_2003_Density_SG				FLOAT;
	DECLARE	@St_2004_Density_SG				FLOAT;
	DECLARE	@St_2005_Density_SG				FLOAT;
	DECLARE	@St_2006_Density_SG				FLOAT;
	DECLARE	@St_2007_Density_SG				FLOAT;
	DECLARE	@St_2008_Density_SG				FLOAT;
	DECLARE	@St_2009_Density_SG				FLOAT;
	DECLARE	@St_2010_Density_SG				FLOAT;
	DECLARE	@St_2011_Density_SG				FLOAT;
	DECLARE	@St_2012_Density_SG				FLOAT;
	
	SELECT
		@St_2001_Density_SG	= u.[2001],
		@St_2002_Density_SG	= u.[2002],
		@St_2003_Density_SG	= u.[2003],
		@St_2004_Density_SG	= u.[2004],
		@St_2005_Density_SG	= u.[2005],
		@St_2006_Density_SG	= u.[2006],

		@St_2007_Density_SG	= u.[2007],
		@St_2008_Density_SG	= u.[2008],
		@St_2009_Density_SG	= u.[2009],
		@St_2010_Density_SG	= u.[2010],
		@St_2011_Density_SG	= u.[2011],
		@St_2012_Density_SG	= u.[2012]
	FROM (
		SELECT	
			c.[SubmissionId],
			c.[StreamNumber],
			c.[Density_SG]
		FROM	[fact].[Get_StreamDensity](@SubmissionId)	c
		) p
		PIVOT (
		MAX(p.[Density_SG]) FOR p.[StreamNumber] IN (
			[2001], [2002], [2003], [2004], [2005], [2006],
			[2007], [2008], [2009], [2010], [2011], [2012]
			)
		) u;

	END;

	--	Stream - Description
	BEGIN

	DECLARE	@St_1006_Desc					VARCHAR(256);

	DECLARE	@St_2010_Desc					VARCHAR(256);
	DECLARE	@St_2011_Desc					VARCHAR(256);
	DECLARE	@St_2012_Desc					VARCHAR(256);

	DECLARE	@St_5019_Desc					VARCHAR(256);
	DECLARE	@St_5020_Desc					VARCHAR(256);
	
	SELECT
		@St_1006_Desc	= u.[1006],
		@St_2010_Desc	= u.[2010],
		@St_2011_Desc	= u.[2011],
		@St_2012_Desc	= u.[2012],
		@St_5019_Desc	= u.[5019],
		@St_5020_Desc	= u.[5020]
	FROM (
		SELECT	
			c.[SubmissionId],
			c.[StreamNumber],
			c.[StreamDescription]
		FROM	[fact].[Get_StreamDescription](@SubmissionId)	c
		) p
		PIVOT (
		MAX(p.[StreamDescription]) FOR p.[StreamNumber] IN (
			[1006],
			[2010], [2011], [2012],
			[5019], [5020]
			)
		) u;

	END;

	--	Stream - Recovered
	BEGIN

	DECLARE	@St_4001_Recovered_WtPcnt					FLOAT;
	DECLARE	@St_4002_Recovered_WtPcnt					FLOAT;
	DECLARE	@St_4003_Recovered_WtPcnt					FLOAT;
	DECLARE	@St_4004_Recovered_WtPcnt					FLOAT;

	DECLARE	@St_4008_Recovered_WtPcnt					FLOAT;
	DECLARE	@St_4010_Recovered_WtPcnt					FLOAT;

	DECLARE	@St_4016_Recovered_WtPcnt					FLOAT;
	DECLARE	@St_4018_Recovered_WtPcnt					FLOAT;
	DECLARE	@St_4021_Recovered_WtPcnt					FLOAT;
	DECLARE	@St_4022_Recovered_WtPcnt					FLOAT;
	
	SELECT
		@St_4001_Recovered_WtPcnt	= u.[4001],
		@St_4002_Recovered_WtPcnt	= u.[4002],
		@St_4003_Recovered_WtPcnt	= u.[4003],
		@St_4004_Recovered_WtPcnt	= u.[4004],

		@St_4008_Recovered_WtPcnt	= u.[4008],
		@St_4010_Recovered_WtPcnt	= u.[4010],
		
		@St_4016_Recovered_WtPcnt	= u.[4016],
		@St_4018_Recovered_WtPcnt	= u.[4018],
		@St_4021_Recovered_WtPcnt	= u.[4021],
		@St_4022_Recovered_WtPcnt	= u.[4022]

	FROM (
		SELECT	
			c.[SubmissionId],
			c.[StreamNumber],
			c.[Recovered_WtPcnt]
		FROM	[fact].[Get_StreamRecovered](@SubmissionId)	c
		) p
		PIVOT (
		MAX(p.[Recovered_WtPcnt]) FOR p.[StreamNumber] IN (
			[4001], [4002], [4003], [4004],
			[4008], [4010],
			[4016], [4018], [4021], [4022]
			)
		) u;

	END;

	--	Recycled
	BEGIN

	DECLARE	@Recycled_C2H6_WtPcnt			FLOAT;
	DECLARE	@Recycled_C3H8_WtPcnt			FLOAT;
	DECLARE	@Recycled_C4H10_WtPcnt			FLOAT;

	SELECT
		@Recycled_C2H6_WtPcnt	=	u.[18],
		@Recycled_C3H8_WtPcnt	=	u.[19],
		@Recycled_C4H10_WtPcnt	=	u.[21]
	FROM (
		SELECT
			s.[SubmissionId],
			s.[ComponentId],
			s.[Recycled_WtPcnt]
		FROM [fact].[Get_StreamRecycled](@SubmissionId)		s
		WHERE s.[SubmissionId] = @SubmissionId
		) p
		PIVOT (
		MAX(p.[Recycled_WtPcnt]) FOR p.[ComponentId] IN ([18], [19], [21])
		) u;

	END;

	--	Results

	SELECT
		--	Submissions
		@SubmissionId					[SubmissionId],
		@SubmissionName					[SubmissionName],
		@DateBeg						[DateBeg],
		@DateEnd						[DateEnd],
		@SubmissionComment				[SubmissionComment],

		--	Capacity
		@Ethylene_StreamDay_MTSD		[Ethylene_StreamDay_MTSD],
		@Propylene_StreamDay_MTSD		[Propylene_StreamDay_MTSD],
		@Olefins_StreamDay_MTSD			[Olefins_StreamDay_MTSD],

		--	Facility Count
		@FracFeed_UnitCount				[FracFeed_UnitCount],
		@BoilHP_UnitCount				[BoilHP_UnitCount],
		@BoilLP_UnitCount				[BoilLP_UnitCount],
		@ElecGen_UnitCount				[ElecGen_UnitCount],
		@HydroPurCryogenic_UnitCount	[HydroPurCryogenic_UnitCount],
		@HydroPurPSA_UnitCount			[HydroPurPSA_UnitCount],
		@HydroPurMembrane_UnitCount		[HydroPurMembrane_UnitCount],
		@TowerPyroGasHT_UnitCount		[TowerPyroGasHT_UnitCount],

		@Trains_UnitCount				[Trains_UnitCount],

		--	Facility - Boilers
		@BoilHP_Rate_kLbHr				[BoilHP_Rate_kLbHr],
		@BoilLP_Rate_kLbHr				[BoilLP_Rate_kLbHr],
			
		--	Facility - Electricity Generation
		@Capacity_MW					[Capacity_MW],

		--	Facility - FracFeed
		@FracFeed_Quantity_kBSD			[FracFeed_Quantity_kBSD],
		@FracFeed_StreamId				[FracFeed_StreamId],
		@FracFeed_Throughput_kMT		[FracFeed_Throughput_kMT],
		@FracFeed_Density_SG			[FracFeed_Density_SG],

		--	Facility - HydroTreater
		@HT_Quantity_kBSD				[HT_Quantity_kBSD],
		@HT_HydroTreaterTypeId			[HT_HydroTreaterTypeId],
		@HT_Pressure_PSIg				[HT_Pressure_PSIg],
		@HT_Processed_kMT				[HT_Processed_kMT],
		@HT_Processed_Pcnt				[HT_Processed_Pcnt],
		@HT_Density_SG					[HT_Density_SG],
		
		--	Stream - Quanity
		@St_1001_kMT					[St_1001_kMT],
		@St_1002_kMT					[St_1002_kMT],
		@St_1003_kMT					[St_1003_kMT],
		@St_1004_kMT					[St_1004_kMT],
		@St_1005_kMT					[St_1005_kMT],
		@St_1006_kMT					[St_1006_kMT],

		@St_2001_kMT					[St_2001_kMT],
		@St_2002_kMT					[St_2002_kMT],
		@St_2003_kMT					[St_2003_kMT],
		@St_2004_kMT					[St_2004_kMT],
		@St_2005_kMT					[St_2005_kMT],
		@St_2006_kMT					[St_2006_kMT],
		@St_2007_kMT					[St_2007_kMT],
		@St_2008_kMT					[St_2008_kMT],
		@St_2009_kMT					[St_2009_kMT],
		@St_2010_kMT					[St_2010_kMT],
		@St_2011_kMT					[St_2011_kMT],
		@St_2012_kMT					[St_2012_kMT],

		@St_4001_kMT					[St_4001_kMT],
		@St_4002_kMT					[St_4002_kMT],
		@St_4003_kMT					[St_4003_kMT],
		@St_4004_kMT					[St_4004_kMT],

		@St_4005_kMT					[St_4005_kMT],
		@St_4006_kMT					[St_4006_kMT],
		@St_4007_kMT					[St_4007_kMT],
		@St_4008_kMT					[St_4008_kMT],
		@St_4009_kMT					[St_4009_kMT],
		@St_4010_kMT					[St_4010_kMT],
		@St_4011_kMT					[St_4011_kMT],
		@St_4012_kMT					[St_4012_kMT],

		@St_4013_kMT					[St_4013_kMT],
		@St_4014_kMT					[St_4014_kMT],
		@St_4015_kMT					[St_4015_kMT],
		@St_4016_kMT					[St_4016_kMT],
		@St_4017_kMT					[St_4017_kMT],
		@St_4018_kMT					[St_4018_kMT],
		@St_4019_kMT					[St_4019_kMT],
		@St_4020_kMT					[St_4020_kMT],
		@St_4021_kMT					[St_4021_kMT],
		@St_4022_kMT					[St_4022_kMT],
		@St_4023_kMT					[St_4023_kMT],
		@St_4024_kMT					[St_4024_kMT],
		@St_4025_kMT					[St_4025_kMT],
		@St_4026_kMT					[St_4026_kMT],

		@St_5001_kMT					[St_5001_kMT],
		@St_5002_kMT					[St_5002_kMT],
		@St_5003_kMT					[St_5003_kMT],
		@St_5004_kMT					[St_5004_kMT],
		@St_5005_kMT					[St_5005_kMT],
		@St_5006_kMT					[St_5006_kMT],
		@St_5007_kMT					[St_5007_kMT],
		@St_5008_kMT					[St_5008_kMT],
		@St_5009_kMT					[St_5009_kMT],
		@St_5010_kMT					[St_5010_kMT],
		@St_5011_kMT					[St_5011_kMT],
		@St_5012_kMT					[St_5012_kMT],
		@St_5013_kMT					[St_5013_kMT],
		@St_5014_kMT					[St_5014_kMT],
		@St_5015_kMT					[St_5015_kMT],
		@St_5016_kMT					[St_5016_kMT],
		@St_5017_kMT					[St_5017_kMT],
		@St_5018_kMT					[St_5018_kMT],
		@St_5019_kMT					[St_5019_kMT],
		@St_5020_kMT					[St_5020_kMT],
		@St_5022_kMT					[St_5022_kMT],
		@St_5023_kMT					[St_5023_kMT],
		@St_5024_kMT					[St_5024_kMT],

		--	Stream - Composition
		@St_1001_CH4_WtPcnt 			[St_1001_CH4_WtPcnt],
		@St_1001_C2H6_WtPcnt 			[St_1001_C2H6_WtPcnt],
		@St_1001_C2H4_WtPcnt 			[St_1001_C2H4_WtPcnt],
		@St_1001_C3H8_WtPcnt 			[St_1001_C3H8_WtPcnt],
		@St_1001_C3H6_WtPcnt 			[St_1001_C3H6_WtPcnt],
		@St_1001_NBUTA_WtPcnt 			[St_1001_NBUTA_WtPcnt],
		@St_1001_IBUTA_WtPcnt 			[St_1001_IBUTA_WtPcnt],
		@St_1001_B2_WtPcnt 				[St_1001_B2_WtPcnt],
		@St_1001_B1_WtPcnt 				[St_1001_B1_WtPcnt],
		@St_1001_C4H6_WtPcnt 			[St_1001_C4H6_WtPcnt],
		@St_1001_NC5_WtPcnt 			[St_1001_NC5_WtPcnt],
		@St_1001_IC5_WtPcnt 			[St_1001_IC5_WtPcnt],
		@St_1001_NC6_WtPcnt 			[St_1001_NC6_WtPcnt],
		@St_1001_C6ISO_WtPcnt 			[St_1001_C6ISO_WtPcnt],
		@St_1001_C7H16_WtPcnt 			[St_1001_C7H16_WtPcnt],
		@St_1001_C8H18_WtPcnt 			[St_1001_C8H18_WtPcnt],
		@St_1001_CO_CO2_WtPcnt 			[St_1001_CO_CO2_WtPcnt],
		@St_1001_H2_WtPcnt 				[St_1001_H2_WtPcnt],
		@St_1001_S_WtPcnt 				[St_1001_S_WtPcnt],

		@St_1002_CH4_WtPcnt 			[St_1002_CH4_WtPcnt],
		@St_1002_C2H6_WtPcnt 			[St_1002_C2H6_WtPcnt],
		@St_1002_C2H4_WtPcnt 			[St_1002_C2H4_WtPcnt],
		@St_1002_C3H8_WtPcnt 			[St_1002_C3H8_WtPcnt],
		@St_1002_C3H6_WtPcnt 			[St_1002_C3H6_WtPcnt],
		@St_1002_NBUTA_WtPcnt 			[St_1002_NBUTA_WtPcnt],
		@St_1002_IBUTA_WtPcnt 			[St_1002_IBUTA_WtPcnt],
		@St_1002_B2_WtPcnt 				[St_1002_B2_WtPcnt],
		@St_1002_B1_WtPcnt 				[St_1002_B1_WtPcnt],
		@St_1002_C4H6_WtPcnt 			[St_1002_C4H6_WtPcnt],
		@St_1002_NC5_WtPcnt 			[St_1002_NC5_WtPcnt],
		@St_1002_IC5_WtPcnt 			[St_1002_IC5_WtPcnt],
		@St_1002_NC6_WtPcnt 			[St_1002_NC6_WtPcnt],
		@St_1002_C6ISO_WtPcnt 			[St_1002_C6ISO_WtPcnt],
		@St_1002_C7H16_WtPcnt 			[St_1002_C7H16_WtPcnt],
		@St_1002_C8H18_WtPcnt 			[St_1002_C8H18_WtPcnt],
		@St_1002_CO_CO2_WtPcnt 			[St_1002_CO_CO2_WtPcnt],
		@St_1002_H2_WtPcnt 				[St_1002_H2_WtPcnt],
		@St_1002_S_WtPcnt 				[St_1002_S_WtPcnt],

		@St_1003_CH4_WtPcnt 			[St_1003_CH4_WtPcnt],
		@St_1003_C2H6_WtPcnt 			[St_1003_C2H6_WtPcnt],
		@St_1003_C2H4_WtPcnt 			[St_1003_C2H4_WtPcnt],
		@St_1003_C3H8_WtPcnt 			[St_1003_C3H8_WtPcnt],
		@St_1003_C3H6_WtPcnt 			[St_1003_C3H6_WtPcnt],
		@St_1003_NBUTA_WtPcnt 			[St_1003_NBUTA_WtPcnt],
		@St_1003_IBUTA_WtPcnt 			[St_1003_IBUTA_WtPcnt],
		@St_1003_B2_WtPcnt 				[St_1003_B2_WtPcnt],
		@St_1003_B1_WtPcnt 				[St_1003_B1_WtPcnt],
		@St_1003_C4H6_WtPcnt 			[St_1003_C4H6_WtPcnt],
		@St_1003_NC5_WtPcnt 			[St_1003_NC5_WtPcnt],
		@St_1003_IC5_WtPcnt 			[St_1003_IC5_WtPcnt],
		@St_1003_NC6_WtPcnt 			[St_1003_NC6_WtPcnt],
		@St_1003_C6ISO_WtPcnt 			[St_1003_C6ISO_WtPcnt],
		@St_1003_C7H16_WtPcnt 			[St_1003_C7H16_WtPcnt],
		@St_1003_C8H18_WtPcnt 			[St_1003_C8H18_WtPcnt],
		@St_1003_CO_CO2_WtPcnt 			[St_1003_CO_CO2_WtPcnt],
		@St_1003_H2_WtPcnt 				[St_1003_H2_WtPcnt],
		@St_1003_S_WtPcnt 				[St_1003_S_WtPcnt],

		@St_1004_CH4_WtPcnt 			[St_1004_CH4_WtPcnt],
		@St_1004_C2H6_WtPcnt 			[St_1004_C2H6_WtPcnt],
		@St_1004_C2H4_WtPcnt 			[St_1004_C2H4_WtPcnt],
		@St_1004_C3H8_WtPcnt 			[St_1004_C3H8_WtPcnt],
		@St_1004_C3H6_WtPcnt 			[St_1004_C3H6_WtPcnt],
		@St_1004_NBUTA_WtPcnt 			[St_1004_NBUTA_WtPcnt],
		@St_1004_IBUTA_WtPcnt 			[St_1004_IBUTA_WtPcnt],
		@St_1004_B2_WtPcnt 				[St_1004_B2_WtPcnt],
		@St_1004_B1_WtPcnt 				[St_1004_B1_WtPcnt],
		@St_1004_C4H6_WtPcnt 			[St_1004_C4H6_WtPcnt],
		@St_1004_NC5_WtPcnt 			[St_1004_NC5_WtPcnt],
		@St_1004_IC5_WtPcnt 			[St_1004_IC5_WtPcnt],
		@St_1004_NC6_WtPcnt 			[St_1004_NC6_WtPcnt],
		@St_1004_C6ISO_WtPcnt 			[St_1004_C6ISO_WtPcnt],
		@St_1004_C7H16_WtPcnt 			[St_1004_C7H16_WtPcnt],
		@St_1004_C8H18_WtPcnt 			[St_1004_C8H18_WtPcnt],
		@St_1004_CO_CO2_WtPcnt 			[St_1004_CO_CO2_WtPcnt],
		@St_1004_H2_WtPcnt 				[St_1004_H2_WtPcnt],
		@St_1004_S_WtPcnt 				[St_1004_S_WtPcnt],

		@St_1005_CH4_WtPcnt 			[St_1005_CH4_WtPcnt],
		@St_1005_C2H6_WtPcnt 			[St_1005_C2H6_WtPcnt],
		@St_1005_C2H4_WtPcnt 			[St_1005_C2H4_WtPcnt],
		@St_1005_C3H8_WtPcnt 			[St_1005_C3H8_WtPcnt],
		@St_1005_C3H6_WtPcnt 			[St_1005_C3H6_WtPcnt],
		@St_1005_NBUTA_WtPcnt 			[St_1005_NBUTA_WtPcnt],
		@St_1005_IBUTA_WtPcnt 			[St_1005_IBUTA_WtPcnt],
		@St_1005_B2_WtPcnt 				[St_1005_B2_WtPcnt],
		@St_1005_B1_WtPcnt 				[St_1005_B1_WtPcnt],
		@St_1005_C4H6_WtPcnt 			[St_1005_C4H6_WtPcnt],
		@St_1005_NC5_WtPcnt 			[St_1005_NC5_WtPcnt],
		@St_1005_IC5_WtPcnt 			[St_1005_IC5_WtPcnt],
		@St_1005_NC6_WtPcnt 			[St_1005_NC6_WtPcnt],
		@St_1005_C6ISO_WtPcnt 			[St_1005_C6ISO_WtPcnt],
		@St_1005_C7H16_WtPcnt 			[St_1005_C7H16_WtPcnt],
		@St_1005_C8H18_WtPcnt 			[St_1005_C8H18_WtPcnt],
		@St_1005_CO_CO2_WtPcnt 			[St_1005_CO_CO2_WtPcnt],
		@St_1005_H2_WtPcnt 				[St_1005_H2_WtPcnt],
		@St_1005_S_WtPcnt 				[St_1005_S_WtPcnt],

		@St_1006_CH4_WtPcnt 			[St_1006_CH4_WtPcnt],
		@St_1006_C2H6_WtPcnt 			[St_1006_C2H6_WtPcnt],
		@St_1006_C2H4_WtPcnt 			[St_1006_C2H4_WtPcnt],
		@St_1006_C3H8_WtPcnt 			[St_1006_C3H8_WtPcnt],
		@St_1006_C3H6_WtPcnt 			[St_1006_C3H6_WtPcnt],
		@St_1006_NBUTA_WtPcnt 			[St_1006_NBUTA_WtPcnt],
		@St_1006_IBUTA_WtPcnt 			[St_1006_IBUTA_WtPcnt],
		@St_1006_B2_WtPcnt 				[St_1006_B2_WtPcnt],
		@St_1006_B1_WtPcnt 				[St_1006_B1_WtPcnt],
		@St_1006_C4H6_WtPcnt 			[St_1006_C4H6_WtPcnt],
		@St_1006_NC5_WtPcnt 			[St_1006_NC5_WtPcnt],
		@St_1006_IC5_WtPcnt 			[St_1006_IC5_WtPcnt],
		@St_1006_NC6_WtPcnt 			[St_1006_NC6_WtPcnt],
		@St_1006_C6ISO_WtPcnt 			[St_1006_C6ISO_WtPcnt],
		@St_1006_C7H16_WtPcnt 			[St_1006_C7H16_WtPcnt],
		@St_1006_C8H18_WtPcnt 			[St_1006_C8H18_WtPcnt],
		@St_1006_CO_CO2_WtPcnt 			[St_1006_CO_CO2_WtPcnt],
		@St_1006_H2_WtPcnt 				[St_1006_H2_WtPcnt],
		@St_1006_S_WtPcnt 				[St_1006_S_WtPcnt],

		@St_2001_P_WtPcnt 				[St_2001_P_WtPcnt],
		@St_2001_I_WtPcnt 				[St_2001_I_WtPcnt],
		@St_2001_N_WtPcnt 				[St_2001_N_WtPcnt],
		@St_2001_O_WtPcnt 				[St_2001_O_WtPcnt],
		@St_2001_A_WtPcnt 				[St_2001_A_WtPcnt],
		
		@St_2002_P_WtPcnt 				[St_2002_P_WtPcnt],
		@St_2002_I_WtPcnt 				[St_2002_I_WtPcnt],
		@St_2002_N_WtPcnt 				[St_2002_N_WtPcnt],
		@St_2002_O_WtPcnt 				[St_2002_O_WtPcnt],
		@St_2002_A_WtPcnt 				[St_2002_A_WtPcnt],
		
		@St_2003_P_WtPcnt 				[St_2003_P_WtPcnt],
		@St_2003_I_WtPcnt 				[St_2003_I_WtPcnt],
		@St_2003_N_WtPcnt 				[St_2003_N_WtPcnt],
		@St_2003_O_WtPcnt 				[St_2003_O_WtPcnt],
		@St_2003_A_WtPcnt 				[St_2003_A_WtPcnt],
		
		@St_2004_P_WtPcnt 				[St_2004_P_WtPcnt],
		@St_2004_I_WtPcnt 				[St_2004_I_WtPcnt],
		@St_2004_N_WtPcnt 				[St_2004_N_WtPcnt],
		@St_2004_O_WtPcnt 				[St_2004_O_WtPcnt],
		@St_2004_A_WtPcnt 				[St_2004_A_WtPcnt],
		
		@St_2005_P_WtPcnt 				[St_2005_P_WtPcnt],
		@St_2005_I_WtPcnt 				[St_2005_I_WtPcnt],
		@St_2005_N_WtPcnt 				[St_2005_N_WtPcnt],
		@St_2005_O_WtPcnt 				[St_2005_O_WtPcnt],
		@St_2005_A_WtPcnt 				[St_2005_A_WtPcnt],
		
		@St_2006_P_WtPcnt 				[St_2006_P_WtPcnt],
		@St_2006_I_WtPcnt 				[St_2006_I_WtPcnt],
		@St_2006_N_WtPcnt 				[St_2006_N_WtPcnt],
		@St_2006_O_WtPcnt 				[St_2006_O_WtPcnt],
		@St_2006_A_WtPcnt 				[St_2006_A_WtPcnt],
		
		@St_2010_P_WtPcnt 				[St_2010_P_WtPcnt],
		@St_2010_I_WtPcnt 				[St_2010_I_WtPcnt],
		@St_2010_N_WtPcnt 				[St_2010_N_WtPcnt],
		@St_2010_O_WtPcnt 				[St_2010_O_WtPcnt],
		@St_2010_A_WtPcnt 				[St_2010_A_WtPcnt],
		
		@St_2011_P_WtPcnt 				[St_2011_P_WtPcnt],
		@St_2011_I_WtPcnt 				[St_2011_I_WtPcnt],
		@St_2011_N_WtPcnt 				[St_2011_N_WtPcnt],
		@St_2011_O_WtPcnt 				[St_2011_O_WtPcnt],
		@St_2011_A_WtPcnt 				[St_2011_A_WtPcnt],
		
		@St_2012_P_WtPcnt 				[St_2012_P_WtPcnt],
		@St_2012_I_WtPcnt 				[St_2012_I_WtPcnt],
		@St_2012_N_WtPcnt 				[St_2012_N_WtPcnt],
		@St_2012_O_WtPcnt 				[St_2012_O_WtPcnt],
		@St_2012_A_WtPcnt 				[St_2012_A_WtPcnt],

		@St_5001_H2_WtPcnt				[St_5001_H2_WtPcnt],
		@St_5002_CH4_WtPcnt				[St_5002_CH4_WtPcnt],
		@St_5004_C2H4_WtPcnt			[St_5004_C2H4_WtPcnt],
		@St_5005_C2H4_WtPcnt			[St_5005_C2H4_WtPcnt],
		@St_5006_C3H6_WtPcnt			[St_5006_C3H6_WtPcnt],
		@St_5007_C3H6_WtPcnt			[St_5007_C3H6_WtPcnt],
		@St_5008_C3H6_WtPcnt			[St_5008_C3H6_WtPcnt],
		@St_5009_C3H8_WtPcnt			[St_5009_C3H8_WtPcnt],

		@St_5018_H2_WtPcnt 				[St_5018_H2_WtPcnt],
		@St_5018_CH4_WtPcnt 			[St_5018_CH4_WtPcnt],
		@St_5018_C2H4_WtPcnt 			[St_5018_C2H4_WtPcnt],
		@St_5018_C2H6_WtPcnt 			[St_5018_C2H6_WtPcnt],

		@St_5019_H2_WtPcnt 				[St_5019_H2_WtPcnt],
		@St_5019_CH4_WtPcnt 			[St_5019_CH4_WtPcnt],
		@St_5019_C2H2_WtPcnt 			[St_5019_C2H2_WtPcnt],
		@St_5019_C2H4_WtPcnt 			[St_5019_C2H4_WtPcnt],
		@St_5019_C2H6_WtPcnt 			[St_5019_C2H6_WtPcnt],
		@St_5019_C3H6_WtPcnt 			[St_5019_C3H6_WtPcnt],
		@St_5019_C3H8_WtPcnt 			[St_5019_C3H8_WtPcnt],
		@St_5019_C4H6_WtPcnt 			[St_5019_C4H6_WtPcnt],
		@St_5019_C4H8_WtPcnt 			[St_5019_C4H8_WtPcnt],
		@St_5019_C4H10_WtPcnt 			[St_5019_C4H10_WtPcnt],
		@St_5019_C6H6_WtPcnt 			[St_5019_C6H6_WtPcnt],
		@St_5019_PyroGasOil_WtPcnt 		[St_5019_PyroGasOil_WtPcnt],
		@St_5019_PyroFuelOil_WtPcnt 	[St_5019_PyroFuelOil_WtPcnt],
		@St_5019_Inerts_WtPcnt 			[St_5019_Inerts_WtPcnt],

		@St_5020_H2_WtPcnt 				[St_5020_H2_WtPcnt],
		@St_5020_CH4_WtPcnt 			[St_5020_CH4_WtPcnt],
		@St_5020_C2H2_WtPcnt 			[St_5020_C2H2_WtPcnt],
		@St_5020_C2H4_WtPcnt 			[St_5020_C2H4_WtPcnt],
		@St_5020_C2H6_WtPcnt 			[St_5020_C2H6_WtPcnt],
		@St_5020_C3H6_WtPcnt 			[St_5020_C3H6_WtPcnt],
		@St_5020_C3H8_WtPcnt 			[St_5020_C3H8_WtPcnt],
		@St_5020_C4H6_WtPcnt 			[St_5020_C4H6_WtPcnt],
		@St_5020_C4H8_WtPcnt 			[St_5020_C4H8_WtPcnt],
		@St_5020_C4H10_WtPcnt 			[St_5020_C4H10_WtPcnt],
		@St_5020_C6H6_WtPcnt 			[St_5020_C6H6_WtPcnt],
		@St_5020_PyroGasOil_WtPcnt 		[St_5020_PyroGasOil_WtPcnt],
		@St_5020_PyroFuelOil_WtPcnt 	[St_5020_PyroFuelOil_WtPcnt],
		@St_5020_Inerts_WtPcnt 			[St_5020_Inerts_WtPcnt],

		--	Stream - Composition (Molar)
		@St_5001_H2_MolPcnt				[St_5001_H2_MolPcnt],
		@St_5002_CH4_MolPcnt			[St_5002_CH4_MolPcnt],

		--	Stream - Density
		@St_2001_Density_SG				[St_2001_Density_SG],
		@St_2002_Density_SG				[St_2002_Density_SG],
		@St_2003_Density_SG				[St_2003_Density_SG],
		@St_2004_Density_SG				[St_2004_Density_SG],
		@St_2005_Density_SG				[St_2005_Density_SG],
		@St_2006_Density_SG				[St_2006_Density_SG],

		@St_2007_Density_SG				[St_2007_Density_SG],
		@St_2008_Density_SG				[St_2008_Density_SG],
		@St_2009_Density_SG				[St_2009_Density_SG],
		@St_2010_Density_SG				[St_2010_Density_SG],
		@St_2011_Density_SG				[St_2011_Density_SG],
		@St_2012_Density_SG				[St_2012_Density_SG],

		--	Stream - Description
		@St_1006_Desc					[St_1006_Desc],

		@St_2010_Desc					[St_2010_Desc],
		@St_2011_Desc					[St_2011_Desc],
		@St_2012_Desc					[St_2012_Desc],

		@St_5019_Desc					[St_5019_Desc],
		@St_5020_Desc					[St_5020_Desc],

		--	Stream - Recovered
		@St_4001_Recovered_WtPcnt		[St_4001_Recovered_WtPcnt],
		@St_4002_Recovered_WtPcnt		[St_4002_Recovered_WtPcnt],
		@St_4003_Recovered_WtPcnt		[St_4003_Recovered_WtPcnt],
		@St_4004_Recovered_WtPcnt		[St_4004_Recovered_WtPcnt],

		@St_4008_Recovered_WtPcnt		[St_4008_Recovered_WtPcnt],
		@St_4010_Recovered_WtPcnt		[St_4010_Recovered_WtPcnt],

		@St_4016_Recovered_WtPcnt		[St_4016_Recovered_WtPcnt],
		@St_4018_Recovered_WtPcnt		[St_4018_Recovered_WtPcnt],
		@St_4021_Recovered_WtPcnt		[St_4021_Recovered_WtPcnt],
		@St_4022_Recovered_WtPcnt		[St_4022_Recovered_WtPcnt],

		--	Recycled
		@Recycled_C2H6_WtPcnt			[Recycled_C2H6_WtPcnt],
		@Recycled_C3H8_WtPcnt			[Recycled_C3H8_WtPcnt],
		@Recycled_C4H10_WtPcnt			[Recycled_C4H10_WtPcnt];

END;
