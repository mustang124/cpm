﻿



CREATE  PROC GetBreak(@ListName varchar(30), @RankVariableID smallint, @BreakCondition varchar(50), @BreakValue varchar(15), @Percentile real)
AS
SET NOCOUNT ON
CREATE TABLE #Data (VarValue real NOT NULL)

insert into #Data
select TOP 1 Value
from _RankView
where ListName = @ListName and RankVariableID = @RankVariableID and BreakCondition = @BreakCondition AND BreakValue = @BreakValue 
and Percentile >= @Percentile
order by Percentile ASC

insert into #Data
select TOP 1 Value
from _RankView
where ListName = @ListName and RankVariableID = @RankVariableID and BreakCondition = @BreakCondition AND BreakValue = @BreakValue 
and Percentile < @Percentile
order by Percentile DESC

DECLARE @Avg real
SELECT @Avg = AVG(VarValue) FROM #Data
DECLARE @VarDesc varchar(50)
SELECT @VarDesc = Description FROM RankVariables WHERE RankVariableID = @RankVariableID

SELECT ListName = @ListName, RankVariableID = @RankVariableID, BreakCondition = @BreakCondition, BreakValue = @BreakValue, Percentile = @Percentile, Value = @Avg

DROP TABLE #Data




