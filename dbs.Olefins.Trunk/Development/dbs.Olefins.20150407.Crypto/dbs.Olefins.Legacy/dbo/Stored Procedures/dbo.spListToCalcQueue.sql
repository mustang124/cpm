﻿
/****** Object:  Stored Procedure dbo.spListToCalcQueue    Script Date: 4/18/2003 4:32:53 PM ******/

/****** Object:  Stored Procedure dbo.spListToCalcQueue    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.spListToCalcQueue    Script Date: 04/13/2000 8:32:50 AM ******/
CREATE PROCEDURE spListToCalcQueue 
	@ListName varchar(30),
	@Calculate bit = 1, @Compare bit = 1, @Copy bit = 1,
	@ClientTables bit = 1, @CalcSummary bit = 1,
	@MessageLog bit = 1, @EmailNotify bit = 1,
	@EMailMessage varchar(255) = NULL
AS
IF EXISTS (SELECT * FROM CalcQueue WHERE ListName = @ListName)
	UPDATE CalcQueue
	SET Calculate = @Calculate, Compare = @Compare, Copy = @Copy,
	ClientTables = @ClientTables, CalcSummary = @CalcSummary,
	MessageLog = @MessageLog, EmailNotify = @EmailNotify,
	EMailMessage = @EMailMessage 
	WHERE ListName = @ListName
ELSE
	INSERT INTO CalcQueue (ListName, Calculate, Compare, Copy,
	ClientTables, CalcSummary, MessageLog, EmailNotify, EMailMessage)
	VALUES (@ListName, @Calculate, @Compare, @Copy, @ClientTables, 
	@CalcSummary, @MessageLog, @EmailNotify, @EMailMessage)
	


