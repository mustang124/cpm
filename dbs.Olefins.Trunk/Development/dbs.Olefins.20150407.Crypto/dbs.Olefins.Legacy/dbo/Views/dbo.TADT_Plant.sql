﻿


CREATE view [dbo].[TADT_Plant] AS
Select Refnum, 
PlanInterval = Sum(PlanInterval * PctTotEthylCap) / sum(PctTotEthylCap), 
PlanDT = Sum(PlanDT * PctTotEthylCap) / sum(PctTotEthylCap), 
ActInterval = Sum(ActInterval * PctTotEthylCap) / sum(PctTotEthylCap), 
ActDT = Sum(ActDT * PctTotEthylCap) / sum(PctTotEthylCap), 
TACost = Sum(TACost), 
TAManHrs = Sum(TAManHrs), 
AnnTACost = Sum(TACost/ActInterval*12), 
AnnTAManHrs = Sum(TAManHrs/ActInterval*12), 
TotPcnt = sum(PctTotEthylCap),
NumTrain = Count(*),
SingleTrain = CASE Count(*) WHEN 1 THEN 1 ELSE 0 END,
DualTrain = CASE Count(*) WHEN 2 THEN 1 ELSE 0 END,
ThreeTrain = CASE Count(*) WHEN 3 THEN 1 ELSE 0 END,
TAMini = CASE WHEN SUM(CASE WHEN LEFT(TAMini, 1) = 'Y' THEN 1 ELSE 0 END) > 0 THEN 100 ELSE 0 END
FROM TADT
Where PctTotEthylCap > 0 
--and Refnum = '05pch089u'
Group by Refnum



