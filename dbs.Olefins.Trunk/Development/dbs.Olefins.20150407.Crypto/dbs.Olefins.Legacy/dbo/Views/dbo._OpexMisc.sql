﻿


/****** Object:  View dbo._RankView    Script Date: 4/18/2003 4:32:51 PM ******/

/****** Object:  View dbo._RankView    Script Date: 12/28/2001 7:34:24 AM ******/
/****** Object:  View dbo._RankView    Script Date: 04/13/2000 7:55:23 AM ******/

/* NonEnergyOPEX as defined by OMV 9/28/04*/
/*Changed 2/8/08 to be included in the study results, added PurOth */
CREATE  VIEW dbo._OpexMisc AS
SELECT t.Refnum, 
 'NonEnergyOPEX' = SUM(ISNULL(f.FixExpAmt, 0) + ISNULL(o.Chemicals, 0) + ISNULL(o.Catalysts, 0) +
   ISNULL(o.Royalties, 0) + ISNULL(f.AnnTAExp, 0) + ISNULL(o.PurOth, 0)+ ISNULL(o.OthVol, 0))/SUM(p.HVChemDiv)
FROM TSort t
INNER JOIN Opex o ON o.Refnum = t.Refnum
INNER JOIN FinancialTot f ON f.Refnum = t.Refnum
INNER JOIN Production p ON p.Refnum = o.Refnum
GROUP BY t.Refnum



