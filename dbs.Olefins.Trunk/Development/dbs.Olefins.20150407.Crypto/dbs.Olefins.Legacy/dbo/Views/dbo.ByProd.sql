﻿



/****** Object:  View dbo.ByProd    Script Date: 8/29/2006 7:23:33 AM ******/

/****** Object:  View dbo.ByProd    Script Date: 8/29/2006 7:22:44 AM ******/
/**Claire,
DBB recommended revising the XOM follow-up work to be consistent with the main Olefin study. There is a discrepancy that the main study counts the ethylene in OtherProd as Product but XOM ByProd View counts the entire OtherProd as ByProd. 
Henry and I traced the XOM ByProd View macro (used in XOM follow-up work). Unfortunately, XOM ByProd's macro for ByProd Ethylene cannot be revised easily to account for the ethylene in OtherProd. We'd have to split OtherProd and define the ethylene portion - very messy - some data needed to do this are not uploaded.
We came up with an easier revision to the XOM follow-up work to make it consistent with the main study.     For the next study, we will revise CALCS's [output] row 634 to be consistent with the main study definition for ByProd and replace the ByProd Value in XOM BYPROD view with this value uploaded from CALCS [Output] to db table [FINQTR] ByProdEthylene.
By the way, we traced the data in the presentation's Ethylene ByProd Credit slide.  The Ethylene ByProd Credit was calculated in Presentation.XLS via a formula: Cash Cost-RMC-Opex.  So the presentation data does not use FINQRT's ByProd Credit.  We dld not find any other reference in the olefin db that uses this FINQRT's ByProd Credit.
For 2005, I will manually correct the XOM plants affected by this discrepancy.  Later, for the next study, we can review the revisions to CALCs required.
I have documented this also in  K:\STUDY\Olefins\2005\Correspondence\Company Follow-up\ExxonMobil US\SETUP 05. xls [View]
/jeanne
**/

CREATE    VIEW [dbo].[ByProd] AS
SELECT f.Refnum, ByProdBasis = MIN('C2'), TotVal = Sum(TotVal), 
	ByProdKMT = sum(AnnFeedProd), DivFactor = min(C2Div),
	ProdVal = min(GPV/1000)-Sum(TotVal), GPV = min(GPV/1000)
From FeedProdVal f JOIN FeedProd_LU l on l.FeedProdID=f.FeedProdID
INNER JOIN Quantity q on q.Refnum=f.Refnum and f.FeedProdID=q.FeedProdID
INNER JOIN FinancialTot t on t.Refnum=q.Refnum
INNER JOIN Production p on p.Refnum=q.Refnum
WHERE C2Prod = 'BYPROD'
GROUP BY f.Refnum
UNION
SELECT f.Refnum, ByProdBasis = MIN('OLE'), TotVal = Sum(TotVal), 
	ByProdKMT = sum(AnnFeedProd), DivFactor = min(C2C3Div),
	ProdVal = min(GPV/1000)-Sum(TotVal), GPV = min(GPV/1000)
From FeedProdVal f JOIN FeedProd_LU l on l.FeedProdID=f.FeedProdID
INNER JOIN Quantity q on q.Refnum=f.Refnum and f.FeedProdID=q.FeedProdID
INNER JOIN FinancialTot t on t.Refnum=q.Refnum
INNER JOIN Production p on p.Refnum=q.Refnum
WHERE OleProd = 'BYPROD'
GROUP BY f.Refnum
UNION
SELECT f.Refnum, ByProdBasis = MIN('HVC'), TotVal = Sum(TotVal), 
	ByProdKMT = sum(AnnFeedProd), DivFactor = min(HVChemDiv),
	ProdVal = min(GPV/1000)-Sum(TotVal), GPV = min(GPV/1000)
From FeedProdVal f JOIN FeedProd_LU l on l.FeedProdID=f.FeedProdID
INNER JOIN Quantity q on q.Refnum=f.Refnum and f.FeedProdID=q.FeedProdID
INNER JOIN FinancialTot t on t.Refnum=q.Refnum
INNER JOIN Production p on p.Refnum=q.Refnum
WHERE HVCProd = 'BYPROD'
GROUP BY f.Refnum
--UNION
--SELECT f.Refnum, ByProdBasis = MIN('OLECpby'), TotVal = Sum(f.TotVal), 
--	ByProdKMT = sum(q.AnnFeedProd), DivFactor = min(ISNULL(p.OlefinsCapMTD,p.OlefinsMaxCapMTD)*365/1000),
--	ProdVal = min(t.GPV/1000)-Sum(f.TotVal), GPV = min(t.GPV/1000)
--From OlefinsLegacy.dbo.FeedProdVal f JOIN OlefinsLegacy.dbo.FeedProd_LU l on l.FeedProdID=f.FeedProdID
--INNER JOIN OlefinsLegacy.dbo.Quantity q on q.Refnum=f.Refnum and f.FeedProdID=q.FeedProdID
--INNER JOIN OlefinsLegacy.dbo.FinancialTot t on t.Refnum=q.Refnum
--INNER JOIN OlefinsLegacy.dbo.Capacity p on p.Refnum=q.Refnum
--WHERE l.OleProd = 'BYPROD' and ISNULL(p.OlefinsCapMTD,p.OlefinsMaxCapMTD)>0
--GROUP BY f.Refnum







