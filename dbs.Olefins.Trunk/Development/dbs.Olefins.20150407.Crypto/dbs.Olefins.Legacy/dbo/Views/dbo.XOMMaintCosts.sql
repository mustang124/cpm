﻿


CREATE VIEW XOMMaintCosts AS
Select m.Refnum, 
StudyYear,
ByProdBasis,
DivFactor, 
(r.TotReplVal *1000 * MaintCostIndex * (CASE WHEN StudyYear <=1993 THEN 100 ELSE 1 END) / 100 / DivFactor) as TotMaintCost,
(r.TotReplVal *1000 * MaintCostIndex  * (CASE WHEN StudyYear<=1993 THEN 100 ELSE 1 END) / 100 - f.AnnTAExp) / DivFactor as RoutMaintCost,
 f.AnnTAExp / DivFactor as AnnTAMaintCost
FROM ByProd b INNER JOIN FinancialTot f on f.Refnum=b.Refnum
INNER JOIN MaintInd m on m.Refnum=f.Refnum
INNER JOIN Replacement r on r.Refnum=f.Refnum
INNER JOIN TSort t on t.Refnum=f.Refnum

