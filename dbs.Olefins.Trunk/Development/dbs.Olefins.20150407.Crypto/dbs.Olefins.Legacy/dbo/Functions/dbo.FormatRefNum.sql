﻿-------------------------------------------------------------------------------------------------------
--PURPOSE:    To Return Parts of the Refnum providing First Part is Numeric and Second Part is Alphabetic
--            Also Reformats LIV and LEV to LUB  
--DATE:       10/27/2012
--WRITTEN BY: Greg Vestal
-------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[FormatRefNum] 
(
	@Refnum dbo.Refnum,
	@Mode int -- 0=Return Full Refnum, 1=Return Refnum Cycle, 2=Return Study, 3=Return Year
)
RETURNS varchar(12)
AS

BEGIN
	DECLARE @FirstPart varchar(4)
	DECLARE @SecondPart varchar(3)
	DECLARE @ThirdPart varchar(4)
	DECLARE @Part2Start tinyint, @Part2Size tinyint
	DECLARE @result varchar(12)
	
	SELECT @SecondPart = Study, @Part2Size = StudyCodeSize, @Part2Start = CHARINDEX(StudyCode, @Refnum, 1)
	FROM Console.RefnumStudyCodes
	WHERE CHARINDEX(StudyCode, @Refnum, 1) > 0
	
	SET @result = 'INVALID MODE'
	IF @Part2Start > 0
	BEGIN
		SELECT @FirstPart = LEFT(@Refnum, @Part2Start-1), @ThirdPart = SUBSTRING(@Refnum, @Part2Start + @Part2Size, 4)
	
		IF @Mode=0
			SET @result =  @FirstPart + @SecondPart + @ThirdPart
			
		ELSE IF @Mode=1
			SET @result =  @FirstPart
		
		ELSE IF @Mode=2
			SET @result =  @SecondPart
			
		ELSE IF @Mode=3
			SET @result =  @ThirdPart
	END
	ELSE
		SET @result = 'INVALID REF'
		
	RETURN @result
END
