﻿
CREATE function dbo.UnitsConv(@valToConv float, @FromUnits varchar(255), @ToUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @DivLocTo int, @DivLocFrom int
	DECLARE @UOMGroupFrom varchar(20), @FactorFrom float
	DECLARE @UOMGroupTo varchar(20), @FactorTo float
	DECLARE @Result float

	IF @ToUnits = @FromUnits 
		SET @Result = @valToConv
	ELSE IF CHARINDEX('/', @ToUnits) > 0 
	BEGIN
		SELECT 	@DivLocTo = CHARINDEX('/', @ToUnits),
			@DivLocFrom = CHARINDEX('/', @FromUnits)
		SELECT @Result = dbo.UnitsConv(@valToConv, SUBSTRING(@FromUnits, 1, @DivLocFrom-1), SUBSTRING(@ToUnits, 1, @DivLocTo-1))/
                    		dbo.UnitsConv(1, SUBSTRING(@FromUnits, @DivLocFrom+1, LEN(@FromUnits)), SUBSTRING(@ToUnits, @DivLocTo+1, LEN(@ToUnits)))
	END
	ELSE IF dbo.CaseSensitiveSearch(@ToUnits, 'p', 1) > 0
	BEGIN
		SELECT 	@DivLocTo = dbo.CaseSensitiveSearch(@ToUnits, 'p', 1),
			@DivLocFrom = dbo.CaseSensitiveSearch(@FromUnits, 'p', 1)
		SELECT @Result = dbo.UnitsConv(@valToConv, SUBSTRING(@FromUnits, 1, @DivLocFrom-1), SUBSTRING(@ToUnits, 1, @DivLocTo-1))
	END
	ELSE IF CHARINDEX('*', @ToUnits) > 0
	BEGIN
		SELECT 	@DivLocTo = CHARINDEX('*', @ToUnits),
			@DivLocFrom = CHARINDEX('*', @FromUnits)
		SELECT @Result = dbo.UnitsConv(@valToConv, SUBSTRING(@FromUnits, 1, @DivLocFrom-1), SUBSTRING(@ToUnits, 1, @DivLocTo-1))*
				dbo.UnitsConv(1, SUBSTRING(@FromUnits,@DivLocFrom+1, LEN(@FromUnits)), SUBSTRING(@ToUnits, @DivLocTo+1, LEN(@ToUnits)))
	END
	ELSE BEGIN
		SELECT @UOMGroupFrom = UOMGroup, @FactorFrom = Factor
		FROM UOM WHERE UOMCode = @FromUnits
		SELECT @UOMGroupTo = UOMGroup, @FactorTo = Factor
		FROM UOM WHERE UOMCode = @ToUnits

		SELECT @Result = CASE ISNULL(@UOMGroupFrom, '')
			WHEN '' THEN @valToConv
			WHEN 'Gravity' THEN dbo.ConvGravity(@valToConv, @FromUnits, @ToUnits)
			WHEN 'Temperature' THEN dbo.ConvTemp(@valToConv, @FromUnits, @ToUnits)
			WHEN 'Viscosity' THEN dbo.ConvViscosity(@valToConv, @FromUnits, @ToUnits)
			ELSE @valToConv * (@FactorTo/@FactorFrom) END
	END
	RETURN @Result
END


