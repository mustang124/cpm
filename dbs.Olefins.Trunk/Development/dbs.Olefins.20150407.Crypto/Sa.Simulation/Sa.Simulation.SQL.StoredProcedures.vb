﻿
Namespace SQL

    ''' <summary>
    ''' SQL Stored Procedures used when inserting, updating, and deleting records.
    ''' </summary>
    ''' <remarks>
    ''' <para>
    ''' Copyright © 2012 HSB Solomon Associates LLC
    ''' Solomon Associates
    ''' Two Galleria Tower, Suite 1500
    ''' 13455 Noel Road
    ''' Dallas, Texas 75240, United States of America
    ''' </para>
    ''' </remarks>
    <System.Runtime.Remoting.Contexts.Synchronization()>
    Public Class StoredProcedures

        Private Const nFactorSetID As System.Int32 = 12
        Private Const nRefnum As System.Int32 = 18
        Private Const nSimModelID As System.Int32 = 12
        Private Const nOpCondID As System.Int32 = 12
        Private Const nStreamID As System.Int32 = 36
        Private Const nStreamDescription As System.Int32 = 128

        ''' <summary>
        ''' Determines if a field exists a SqlDataReader Row
        ''' </summary>
        ''' <param name="rdr">Provides a way of reading a forward-only stream of rows from a SQL Server database.</param>
        ''' <param name="FieldName">Field name as string</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Shared Function FieldExists(
            ByVal rdr As SqlClient.SqlDataReader,
            ByVal FieldName As String) As Boolean

            Dim bFieldExists As Boolean = False

            If Not rdr Is Nothing Then

                rdr.GetSchemaTable().DefaultView.RowFilter = "ColumnName= '" + FieldName + "'"
                bFieldExists = CType(rdr.GetSchemaTable().DefaultView.Count = 1, Boolean)

            End If

            Return bFieldExists

        End Function

        ''' <summary>
        ''' Execute the SQL Command against the database.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="cmd">SQLcommand to ececute.</param>
        ''' <remarks>
        ''' This Sub is included to provide a standard method of executing SQL Commands against a database.  This method is to handle SQL locking.
        ''' </remarks>
        <System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)>
        Friend Shared Function SaCommitTransaction(
            ByVal cnString As String,
            ByVal cmd As System.Data.SqlClient.SqlCommand,
            ByVal ShowDialog As Boolean) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cn As New System.Data.SqlClient.SqlConnection(cnString)

                cmd.Connection = cn
                cn.Open()

                Try
                    r = cmd.ExecuteNonQuery()

                Catch ex As System.Data.SqlClient.SqlException
                    Sa.Log.Write("Error with " + cmd.CommandText + ControlChars.CrLf + ControlChars.CrLf + ex.Message, EventLogEntryType.Error, ShowDialog, Sa.Log.LogName, 0)

                End Try

            End Using

            Return r

        End Function

#Region " Delete Results "

        ''' <summary>
        ''' Delete simulation results..
        ''' </summary>
        ''' <param name="CnString">Sets the connection string used to open a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function SimulationResultsDelete(
                ByVal CnString As String,
                ByVal SimModelID As String,
                ByVal FactorSetID As String,
                ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            r = Sa.Simulation.SQL.StoredProcedures.YieldCompositionPlantDelete(CnString, SimModelID, FactorSetID, Refnum)
            r = r + Sa.Simulation.SQL.StoredProcedures.YieldCompositionStreamDelete(CnString, SimModelID, FactorSetID, Refnum)
            r = r + Sa.Simulation.SQL.StoredProcedures.EnergyConsumptionStreamDelete(CnString, SimModelID, FactorSetID, Refnum)
            r = r + Sa.Simulation.SQL.StoredProcedures.EnergyConsumptionPlantDelete(CnString, SimModelID, FactorSetID, Refnum)

            Return r

        End Function

        ''' <summary>
        ''' Delete rows from sim.EnergyConsumptionPlant
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function EnergyConsumptionPlantDelete(
            ByVal CnString As String,
            ByVal SimModelID As String,
            ByVal FactorSetID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.EnergyConsumptionPlant_Delete")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Delete rows from sim.YieldCompositionPlant.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function YieldCompositionPlantDelete(
            ByVal CnString As String,
            ByVal SimModelID As String,
            ByVal FactorSetID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.YieldCompositionPlant_Delete")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Delete rows from sim.Yield.
        ''' </summary>
        ''' <param name="CnString">Sets the connection string used to open a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function EnergyConsumptionStreamDelete(
            ByVal cnString As String,
            ByVal SimModelID As String,
            ByVal FactorSetID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.StreamEnergy_Delete")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID
                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(cnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Delete rows from sim.YieldComposition.
        ''' </summary>
        ''' <param name="CnString">Sets the connection string used to open a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function YieldCompositionStreamDelete(
            ByVal cnString As String,
            ByVal SimModelID As String,
            ByVal FactorSetID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.StreamYield_Delete")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID
                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(cnString, cmd, False)

            End Using

            Return r

        End Function

#End Region

#Region " Insert Results "

        ''' <summary>
        ''' Insert the yield header into...
        ''' </summary>
        ''' <param name="CnString">Sets the connection string used to open a SQL Server database.</param>
        ''' <param name="QueueID">System.Int64 identifying the parameters that filter the simulation items to be processed (sim.SimQueue).</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="rdr">Provides a way of reading a forward-only stream of rows from a SQL Server database.</param>
        ''' <param name="nErrorID">Error value retuned by the simulation model (dim.SimErrorMessagesLu).</param>
        ''' <param name="nEnergy">Amount of energy used calculated by the simulation model.</param>
        ''' <remarks></remarks>
        Public Shared Function PutYieldHeader(
            ByVal CnString As String,
            ByVal QueueID As System.Int64,
            ByVal SimModelID As String,
            ByVal rdr As SqlClient.SqlDataReader,
            ByVal nErrorID As System.Int32,
            ByVal nEnergy As Single) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cn As New System.Data.SqlClient.SqlConnection(CnString)

                cn.Open()

                Dim FurnaceTypeID As String = Nothing

                If rdr.GetOrdinal("FurnaceTypeID") > 0 Then

                    If Not rdr.IsDBNull(rdr.GetOrdinal("FurnaceTypeID")) Then

                        FurnaceTypeID = rdr.GetString(rdr.GetOrdinal("FurnaceTypeID"))

                    End If

                End If

                r = Sa.Simulation.SQL.StoredProcedures.EnergyConsumptionStreamInsert(CnString,
                    QueueID,
                    SimModelID,
                    rdr,
                    CType(nErrorID, String),
                    FurnaceTypeID,
                    nEnergy
                    )

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Insert rows into sim.Yield.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="QueueID">System.Int64 identifying the parameters that filter the simulation items to be processed (sim.SimQueue).</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="rdr">Provides a way of reading a forward-only stream of rows from a SQL Server database.</param>
        ''' <param name="ErrorID">Error value retuned by the simulation model (dim.SimErrorMessagesLu).</param>
        ''' <param name="FurnaceTypeID">String representing the furnace type used in the simulation (dim.SimFurnaceTypeLu).</param>
        ''' <param name="SimEnergy_kCalkg">Amount of energy used calculated by the simulation model.</param>
        ''' <remarks></remarks>
        Public Shared Function EnergyConsumptionStreamInsert(
            ByVal CnString As String,
            ByVal QueueID As System.Int64,
            ByVal SimModelID As String,
            ByVal rdr As SqlClient.SqlDataReader,
            ByVal ErrorID As String,
            Optional ByVal FurnaceTypeID As String = Nothing,
            Optional ByVal SimEnergy_kCalkg As Single = Nothing) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.StreamEnergy_Insert")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@QueueID", SqlDbType.VarChar, 12).Value = QueueID
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("FactorSetID"))
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, Sa.Simulation.SQL.StoredProcedures.nRefnum).Value = rdr.GetString(rdr.GetOrdinal("Refnum"))
                cmd.Parameters.Add("@CalDateKey", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("CalDateKey"))
                cmd.Parameters.Add("@StreamID", SqlDbType.VarChar, 36).Value = rdr.GetString(rdr.GetOrdinal("StreamID"))
                cmd.Parameters.Add("@StreamDescription", SqlDbType.NVarChar, 128).Value = rdr.GetString(rdr.GetOrdinal("StreamDescription"))
                cmd.Parameters.Add("@OpCondID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("OpCondID"))
                cmd.Parameters.Add("@RecycleID", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("RecycleID"))

                cmd.Parameters.Add("@ErrorID", SqlDbType.VarChar, 12).Value = ErrorID

                If FurnaceTypeID <> Nothing Then cmd.Parameters.Add("@FurnaceTypeID", SqlDbType.VarChar, 12).Value = FurnaceTypeID
                If SimEnergy_kCalkg <> Nothing Then cmd.Parameters.Add("@SimEnergy_kCalkg", SqlDbType.Real).Value = SimEnergy_kCalkg

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Update rows in sim.Yield.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="QueueID_Init">System.Int64 identifying the parameters that filter the simulation items to be processed (sim.SimQueue).</param>
        ''' <param name="SimModelID_Init">String used to identify the simulation model to delete.</param>
        ''' <param name="rdr">Provides a way of reading a forward-only stream of rows from a SQL Server database.</param>
        ''' <param name="ErrorID">Error value retuned by the simulation model (dim.SimErrorMessagesLu).</param>
        ''' <param name="FurnaceTypeID">String representing the furnace type used in the simulation (dim.SimFurnaceTypeLu).</param>
        ''' <param name="SimEnergy_kCalkg">Amount of energy used calculated by the simulation model.</param>
        ''' <remarks></remarks>
        Public Shared Function EnergyConsumptionStreamUpdate(
            ByVal CnString As String,
            ByVal QueueID_Init As System.Int64,
            ByVal SimModelID_Init As String,
            ByVal rdr As SqlClient.SqlDataReader,
            Optional ByVal ErrorID As String = Nothing,
            Optional ByVal FurnaceTypeID As String = Nothing,
            Optional ByVal SimEnergy_kCalkg As Single = Nothing) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.StreamEnergy_Update")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@QueueID_init", SqlDbType.VarChar, 12).Value = QueueID_Init
                cmd.Parameters.Add("@SimModelID_init", SqlDbType.VarChar, 12).Value = SimModelID_Init
                cmd.Parameters.Add("@FactorSetID_init", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("FactorSetID"))
                cmd.Parameters.Add("@Refnum_init", SqlDbType.VarChar, 18).Value = rdr.GetString(rdr.GetOrdinal("Refnum"))
                cmd.Parameters.Add("@CalDateKey_init", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("CalDateKey"))
                cmd.Parameters.Add("@StreamID_init", SqlDbType.VarChar, 36).Value = rdr.GetString(rdr.GetOrdinal("StreamID"))
                cmd.Parameters.Add("@StreamDescription_init", SqlDbType.NVarChar, 128).Value = rdr.GetString(rdr.GetOrdinal("StreamDescription"))
                cmd.Parameters.Add("@OpCondID_init", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("OpCondID"))
                cmd.Parameters.Add("@RecycleID_init", SqlDbType.VarChar, 36).Value = rdr.GetInt32(rdr.GetOrdinal("RecycleID"))

                If ErrorID <> Nothing Then cmd.Parameters.Add("@ErrorID", SqlDbType.VarChar, 12).Value = ErrorID
                If FurnaceTypeID <> Nothing Then cmd.Parameters.Add("@FurnaceTypeID", SqlDbType.VarChar, 12).Value = FurnaceTypeID
                If SimEnergy_kCalkg <> Nothing Then cmd.Parameters.Add("@SimEnergy_kCalkg", SqlDbType.Real).Value = SimEnergy_kCalkg

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Insert rows into sim.EnergyConsumptionPlant
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function EnergyConsumptionPlantInsert(
            ByVal CnString As String,
            ByVal FactorSetID As String,
            ByVal SimModelID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.EnergyConsumptionPlant_Insert")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Insert rows into sim.YieldComposition.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="QueueID">System.Int64 identifying the parameters that filter the simulation items to be processed (sim.SimQueue).</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="rdr">Provides a way of reading a forward-only stream of rows from a SQL Server database.</param>
        ''' <param name="ComponentID">String representing the composition component (dim.CompositionLu).</param>
        ''' <param name="Component_WtPcnt">Real number representing the wieght percent of the composition component [0, 100].</param>
        ''' <remarks></remarks>
        Public Shared Function YieldCompositionStreamInsert(
            ByVal CnString As String,
            ByVal QueueID As System.Int64,
            ByVal SimModelID As String,
            ByVal rdr As SqlClient.SqlDataReader,
            ByVal ComponentID As String,
            ByVal Component_WtPcnt As Single) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.StreamYield_Insert")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@QueueID", SqlDbType.VarChar, 12).Value = QueueID
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("FactorSetID"))
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = rdr.GetString(rdr.GetOrdinal("Refnum"))
                cmd.Parameters.Add("@CalDateKey", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("CalDateKey"))
                cmd.Parameters.Add("@StreamID", SqlDbType.VarChar, 36).Value = rdr.GetString(rdr.GetOrdinal("StreamID"))
                cmd.Parameters.Add("@StreamDescription", SqlDbType.NVarChar, 128).Value = rdr.GetString(rdr.GetOrdinal("StreamDescription"))
                cmd.Parameters.Add("@OpCondID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("OpCondID"))
                cmd.Parameters.Add("@RecycleID", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("RecycleID"))

                cmd.Parameters.Add("@ComponentID", SqlDbType.VarChar, 24).Value = ComponentID
                cmd.Parameters.Add("@Component_WtPcnt", SqlDbType.Real).Value = Component_WtPcnt

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Insert rows into sim.YieldCompositionPlant.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function YieldCompositionPlantPypsInsert(
            ByVal CnString As String,
            ByVal FactorSetID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.YieldCompositionPlantPyps_Insert")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Insert rows into sim.YieldCompositionPlant.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function YieldCompositionPlantInsert(
            ByVal CnString As String,
            ByVal FactorSetID As String,
            ByVal SimModelID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.YieldCompositionPlant_Insert")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

#End Region

#Region " SPSL "

        ''' <summary>
        ''' Delete rows from sim.SpslOut.  This table contains the raw output from the SPSL simulation.
        ''' </summary>
        ''' <param name="CnString">Sets the connection string used to open a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function SpslOutDelete(
            ByVal cnString As String,
            ByVal SimModelID As String,
            ByVal FactorSetID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.SpslOut_Delete")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID
                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(cnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Insert rows into sim.SpslOut.  This table contains the raw output from the SPSL simulation.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="QueueID">System.Int64 identifying the parameters that filter the simulation items to be processed (sim.SimQueue).</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="rdr">Provides a way of reading a forward-only stream of rows from a SQL Server database.</param>
        ''' <param name="DataSetID">String representing whether the data is used for input or output.</param>
        ''' <param name="FieldID">String representing the field.</param>
        ''' <param name="Value">Real number representing the value in the field.</param>
        ''' <remarks></remarks>
        Public Shared Function SpslOutInsert(
            ByVal CnString As String,
            ByVal QueueID As System.Int64,
            ByVal SimModelID As String,
            ByVal rdr As SqlClient.SqlDataReader,
            ByVal DataSetID As String,
            ByVal FieldID As String,
            ByVal Value As Single) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.SpslOut_Insert")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@QueueID", SqlDbType.VarChar, 12).Value = QueueID
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("FactorSetID"))
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = rdr.GetString(rdr.GetOrdinal("Refnum"))
                cmd.Parameters.Add("@CalDateKey", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("CalDateKey"))
                cmd.Parameters.Add("@StreamID", SqlDbType.VarChar, 36).Value = rdr.GetString(rdr.GetOrdinal("StreamID"))
                cmd.Parameters.Add("@StreamDescription", SqlDbType.NVarChar, 128).Value = rdr.GetString(rdr.GetOrdinal("StreamDescription"))
                cmd.Parameters.Add("@OpCondID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("OpCondID"))
                cmd.Parameters.Add("@RecycleID", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("RecycleID"))

                cmd.Parameters.Add("@DataSetID", SqlDbType.VarChar, 6).Value = DataSetID
                cmd.Parameters.Add("@FieldID", SqlDbType.VarChar, 12).Value = FieldID
                cmd.Parameters.Add("@Value", SqlDbType.Real).Value = Value

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Delete rows from sim.SpslIn.  This table contains the raw input used for the SPSL simulation.
        ''' </summary>
        ''' <param name="CnString">Sets the connection string used to open a SQL Server database.</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="FactorSetID">String representing the factor set and associated calculations and models (dim.FactorSetsLu).</param>
        ''' <param name="Refnum">String representing a plant, the study year, and study (dim.SubscriptionsAssets, dim.TSortSolomon, dim.TSortClient).</param>
        ''' <remarks></remarks>
        Public Shared Function SpslInDelete(
            ByVal cnString As String,
            ByVal SimModelID As String,
            ByVal FactorSetID As String,
            ByVal Refnum As String) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.SpslIn_Delete")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID
                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = FactorSetID
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = Refnum

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(cnString, cmd, False)

            End Using

            Return r

        End Function

        ''' <summary>
        ''' Insert rows into sim.SpslIn.  This table contains the raw input used for the SPSL simulation.
        ''' </summary>
        ''' <param name="CnString">Represents an open connection to a SQL Server database.</param>
        ''' <param name="QueueID">System.Int64 identifying the parameters that filter the simulation items to be processed (sim.SimQueue).</param>
        ''' <param name="SimModelID">String representing the simulation model (dim.SimModelLu).</param>
        ''' <param name="rdr">Provides a way of reading a forward-only stream of rows from a SQL Server database.</param>
        ''' <param name="DataSetID">String representing whether the data is used for input or output.</param>
        ''' <param name="FieldID">String representing the field.</param>
        ''' <param name="Value">Real number representing the value in the field.</param>
        ''' <remarks>These values may not match the values submitted to the SPSL simulation engine.  Errors cause the values in some fields to step.  This stepping is used to remove simulation errors and the values should be compared to the input.</remarks>
        Public Shared Function SpslInInsert(
            ByVal CnString As String,
            ByVal QueueID As System.Int64,
            ByVal SimModelID As String,
            ByVal rdr As SqlClient.SqlDataReader,
            ByVal DataSetID As String,
            ByVal FieldID As String,
            ByVal Value As Single) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("sim.SpslIn_Insert")

                cmd.CommandType = CommandType.StoredProcedure

                cmd.Parameters.Add("@QueueID", SqlDbType.VarChar, 12).Value = QueueID
                cmd.Parameters.Add("@SimModelID", SqlDbType.VarChar, 12).Value = SimModelID

                cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("FactorSetID"))
                cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = rdr.GetString(rdr.GetOrdinal("Refnum"))
                cmd.Parameters.Add("@CalDateKey", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("CalDateKey"))
                cmd.Parameters.Add("@StreamID", SqlDbType.VarChar, 36).Value = rdr.GetString(rdr.GetOrdinal("StreamID"))
                cmd.Parameters.Add("@StreamDescription", SqlDbType.NVarChar, 128).Value = rdr.GetString(rdr.GetOrdinal("StreamDescription"))
                cmd.Parameters.Add("@OpCondID", SqlDbType.VarChar, 12).Value = rdr.GetString(rdr.GetOrdinal("OpCondID"))
                cmd.Parameters.Add("@RecycleID", SqlDbType.Int).Value = rdr.GetInt32(rdr.GetOrdinal("RecycleID"))

                cmd.Parameters.Add("@DataSetID", SqlDbType.VarChar, 6).Value = DataSetID
                cmd.Parameters.Add("@FieldID", SqlDbType.VarChar, 12).Value = FieldID
                cmd.Parameters.Add("@Value", SqlDbType.Real).Value = Value

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

#End Region

#Region " SimQueue "

        ''' <summary>
        ''' Update a row in sim.SimQueue to show that it has been queued into the simulation engine.
        ''' </summary>
        ''' <param name="CnString">String representing an connection connection string to a SQL Server database.</param>
        ''' <param name="QueueID">System.Int64 representing the QueueId to be updated</param>
        ''' <param name="tsQueued">DateTimeOffset representing the the time when the item is queued into the simulation engine.</param>
        ''' <returns>System.Int32 representing the number of records updated in sim.SimQueue.</returns>
        ''' <remarks></remarks>
        Public Shared Function SimQueueUpdate(
            ByVal CnString As String,
            ByVal QueueID As System.Int64,
            Optional ByVal tsQueued As DateTimeOffset = Nothing,
            Optional ByVal tsStart As DateTimeOffset = Nothing,
            Optional ByVal tsComplete As DateTimeOffset = Nothing,
            Optional ByVal ItemsQueued As System.Int32 = Nothing,
            Optional ByVal ItemsProcessed As System.Int32 = Nothing,
            Optional ByVal ErrorID As String = Nothing
            ) As System.Int32

            Dim r As System.Int32 = Nothing

            Using cmd As New System.Data.SqlClient.SqlCommand("q.Simulation_Update")

                cmd.CommandType = CommandType.StoredProcedure

                If tsQueued = Nothing Then tsQueued = DateTimeOffset.Now

                cmd.Parameters.Add("@QueueID_init", SqlDbType.BigInt).Value = QueueID

                If tsQueued <> Nothing Then cmd.Parameters.Add("@tsQueued", SqlDbType.DateTimeOffset, 7).Value = tsQueued
                If tsStart <> Nothing Then cmd.Parameters.Add("@tsStart", SqlDbType.DateTimeOffset, 7).Value = tsStart
                If tsComplete <> Nothing Then cmd.Parameters.Add("@tsComplete", SqlDbType.DateTimeOffset, 7).Value = tsComplete

                If ItemsQueued <> Nothing Then cmd.Parameters.Add("@ItemsQueued", SqlDbType.Int).Value = ItemsQueued
                If ItemsProcessed <> Nothing Then cmd.Parameters.Add("@ItemsProcessed", SqlDbType.Int).Value = ItemsProcessed
                If ErrorID <> Nothing Then cmd.Parameters.Add("@ErrorID", SqlDbType.VarChar, 36).Value = ErrorID

                r = Sa.Simulation.SQL.StoredProcedures.SaCommitTransaction(CnString, cmd, False)

            End Using

            Return r

        End Function

#End Region

    End Class

End Namespace
