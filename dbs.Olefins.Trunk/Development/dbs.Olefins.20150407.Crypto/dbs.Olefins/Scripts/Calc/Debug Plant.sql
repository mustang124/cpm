﻿--SELECT DATEDIFF(d, '1/1/2000', SYSDATETIME())

DECLARE @Refnum			VARCHAR(18) = '2013PCH033';
DECLARE @sRefnum		VARCHAR(18) = RIGHT(@Refnum, LEN(@Refnum) - 2);
DECLARE @FactorSetID	VARCHAR(4)	= '2013'; -- LEFT(@Refnum, 4);
DECLARE @StudyId		VARCHAR(4)	= 'PCH';

SET NOCOUNT ON;

DECLARE @ProcedureDesc	NVARCHAR(4000);

DECLARE @fpl	[calc].[FoundationPlantList];

INSERT INTO @fpl
(
	[FactorSetID],
	[FactorSetName],
	[FactorSet_AnnDateKey],
	[FactorSet_QtrDateKey],
	[FactorSet_QtrDate],
	[Refnum],
	[Plant_AnnDateKey],
	[Plant_QtrDateKey],
	[Plant_QtrDate],
	[CompanyID],
	[AssetID],
	[AssetName],
	[SubscriberCompanyName],
	[PlantCompanyName],
	[SubscriberAssetName],
	[PlantAssetName],
	[CountryID],
	[StateName],
	[EconRegionID],
	[UomID],
	[CurrencyFcn],
	[CurrencyRpt],
	[AssetPassWord_PlainText],
	[StudyYear],
	[DataYear],
	[Consultant],
	[StudyYearDifference],
	[CalQtr]
)
SELECT
	tsq.[FactorSetID],
	tsq.[FactorSetName],
	tsq.[FactorSet_AnnDateKey],
	tsq.[FactorSet_QtrDateKey],
	tsq.[FactorSet_QtrDate],
	tsq.[Refnum],
	tsq.[Plant_AnnDateKey],
	tsq.[Plant_QtrDateKey],
	tsq.[Plant_QtrDate],
	tsq.[CompanyID],
	tsq.[AssetID],
	tsq.[AssetName],
	tsq.[SubscriberCompanyName],
	tsq.[PlantCompanyName],
	tsq.[SubscriberAssetName],
	tsq.[PlantAssetName],
	tsq.[CountryID],
	tsq.[StateName],
	tsq.[EconRegionID],
	tsq.[UomID],
	tsq.[CurrencyFcn],
	tsq.[CurrencyRpt],
	tsq.[AssetPassWord_PlainText],
	tsq.[StudyYear],
	tsq.[DataYear],
	tsq.[Consultant],
	tsq.[StudyYearDifference],
	tsq.[CalQtr]
FROM [calc].[FoundationPlantListSource]		tsq
WHERE	tsq.FactorSetID		= @FactorSetID
	AND	tsq.Refnum			= @Refnum;

		SELECT
			[m].[Refnum],
			[m].[CurrencyRpt],
			[d].[DataType],
			[d].[DivisorValue] * 1000.0,
			[m].[MarginAnalysis],

			[RawMaterialCost]			= -SUM(CASE WHEN [m].[MarginId] = 'PlantFeed' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[GrossProductValue]			= SUM(CASE WHEN [m].[MarginId] = 'Prod' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[GrossMargin]				= SUM(CASE WHEN [m].[MarginId] = 'MarginGross' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[TotCashOpEx]				= -SUM(CASE WHEN [m].[MarginId] = 'TotCashOpEx' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProductionCost]			= (-SUM(CASE WHEN [m].[MarginId] = 'PlantFeed' THEN [m].[Amount_Cur] ELSE 0.0 END) - SUM(CASE WHEN [m].[MarginId] = 'TotCashOpEx' THEN [m].[Amount_Cur] ELSE 0.0 END)) / [d].[DivisorValue],
			[NetCashMargin]				= SUM(CASE WHEN [m].[MarginId] = 'MarginNetCash' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[NetCashMarginTAAdj]		= SUM(CASE WHEN [m].[MarginId] = 'MarginNetCash' THEN m.TaAdj_Amount_Cur ELSE 0.0 END) / [d].[DivisorValue],
			[NetMargin]					= (SUM(CASE WHEN [m].[MarginId] = 'MarginNetCash' THEN [m].[Amount_Cur] ELSE 0.0 END) + SUM(CASE WHEN [m].[MarginId] = 'NonCash' THEN [m].[Amount_Cur] ELSE 0.0 END)) / [d].[DivisorValue],
			[NonCash]					= SUM(CASE WHEN [m].[MarginId] = 'NonCash' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[STTaExp]					= SUM(CASE WHEN [m].[MarginId] = 'STTaExp' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[STNonVol]					= SUM(CASE WHEN [m].[MarginId] = 'STNonVol' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[STNonVolTA]				= SUM(CASE WHEN [m].[MarginId] = 'STNonVolTA' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[MetaNonVol]				= SUM(CASE WHEN [m].[MarginId] = 'MetaNonVol' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[STVol]						= SUM(CASE WHEN [m].[MarginId] = 'STVol' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[MetaSTVol]					= SUM(CASE WHEN [m].[MarginId] = 'MetaSTVol' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[TotalCashOpEx]				= SUM(CASE WHEN [m].[MarginId] = 'TotalCashOpEx' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[FeedProdLossBal]			= SUM(CASE WHEN [m].[MarginId] = 'FeedProdLossBal' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[EthyleneCG]				= SUM(CASE WHEN [m].[MarginId] = 'EthyleneCG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[EthylenePG]				= SUM(CASE WHEN [m].[MarginId] = 'EthylenePG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Ethylene]					= SUM(CASE WHEN [m].[MarginId] = 'Ethylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProdLoss]					= SUM(CASE WHEN [m].[MarginId] = 'ProdLoss' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCOther]					= SUM(CASE WHEN [m].[MarginId] = 'PPFCOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCPyroFuelOil]			= SUM(CASE WHEN [m].[MarginId] = 'PPFCPyroFuelOil' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCPyroGasOil]			= SUM(CASE WHEN [m].[MarginId] = 'PPFCPyroGasOil' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCPyroNaphtha]			= SUM(CASE WHEN [m].[MarginId] = 'PPFCPyroNaphtha' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCButane]				= SUM(CASE WHEN [m].[MarginId] = 'PPFCButane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCPropane]				= SUM(CASE WHEN [m].[MarginId] = 'PPFCPropane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCEthane]				= SUM(CASE WHEN [m].[MarginId] = 'PPFCEthane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCLiquid]				= SUM(CASE WHEN [m].[MarginId] = 'PPFCLiquid' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFCFuelGas]				= SUM(CASE WHEN [m].[MarginId] = 'PPFCFuelGas' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PPFC]						= SUM(CASE WHEN [m].[MarginId] = 'PPFC' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProLPG]					= SUM(CASE WHEN [m].[MarginId] = 'ProLPG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[NRC]						= SUM(CASE WHEN [m].[MarginId] = 'NRC' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PyroGasOil]				= SUM(CASE WHEN [m].[MarginId] = 'PyroGasOil' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PyroFuelOil]				= SUM(CASE WHEN [m].[MarginId] = 'PyroFuelOil' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PropaneC3Resid]			= SUM(CASE WHEN [m].[MarginId] = 'PropaneC3Resid' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PyroGasoline]				= SUM(CASE WHEN [m].[MarginId] = 'PyroGasoline' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[C4Oth]						= SUM(CASE WHEN [m].[MarginId] = 'C4Oth' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[IsoButylene]				= SUM(CASE WHEN [m].[MarginId] = 'IsoButylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Methane]					= SUM(CASE WHEN [m].[MarginId] = 'Methane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[AcidGas]					= SUM(CASE WHEN [m].[MarginId] = 'AcidGas' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProdMisc]					= SUM(CASE WHEN [m].[MarginId] = 'ProdMisc' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProdOtherMargin]			= SUM(CASE WHEN [m].[MarginId] = 'ProdOtherMargin' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProdOther]					= SUM(CASE WHEN [m].[MarginId] = 'ProdOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Hydrogen]					= SUM(CASE WHEN [m].[MarginId] = 'Hydrogen' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Butadiene]					= SUM(CASE WHEN [m].[MarginId] = 'Butadiene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Benzene]					= SUM(CASE WHEN [m].[MarginId] = 'Benzene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Acetylene]					= SUM(CASE WHEN [m].[MarginId] = 'Acetylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PropyleneRG]				= SUM(CASE WHEN [m].[MarginId] = 'PropyleneRG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PropyleneCG]				= SUM(CASE WHEN [m].[MarginId] = 'PropyleneCG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PropylenePG]				= SUM(CASE WHEN [m].[MarginId] = 'PropylenePG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Propylene]					= SUM(CASE WHEN [m].[MarginId] = 'Propylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProdOlefins]				= SUM(CASE WHEN [m].[MarginId] = 'ProdOlefins' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ProdHVC]					= SUM(CASE WHEN [m].[MarginId] = 'ProdHVC' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Prod]						= SUM(CASE WHEN [m].[MarginId] = 'Prod' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ByProdAdjustmentProdOther]	= SUM(CASE WHEN [m].[MarginId] = 'ByProdAdjustmentProdOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ByProdCredit]				= SUM(CASE WHEN [m].[MarginId] = 'ByProdCredit' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppNitrogen]				= SUM(CASE WHEN [m].[MarginId] = 'SuppNitrogen' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppOther]					= SUM(CASE WHEN [m].[MarginId] = 'SuppOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppWashOil]				= SUM(CASE WHEN [m].[MarginId] = 'SuppWashOil' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppGasOil]				= SUM(CASE WHEN [m].[MarginId] = 'SuppGasOil' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppMisc]					= SUM(CASE WHEN [m].[MarginId] = 'SuppMisc' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilMoGas]					= SUM(CASE WHEN [m].[MarginId] = 'DilMoGas' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilPropane]				= SUM(CASE WHEN [m].[MarginId] = 'DilPropane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilMethane]				= SUM(CASE WHEN [m].[MarginId] = 'DilMethane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilHydrogen]				= SUM(CASE WHEN [m].[MarginId] = 'DilHydrogen' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilEthylene]				= SUM(CASE WHEN [m].[MarginId] = 'DilEthylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilEthane]					= SUM(CASE WHEN [m].[MarginId] = 'DilEthane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilButylene]				= SUM(CASE WHEN [m].[MarginId] = 'DilButylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilButane]					= SUM(CASE WHEN [m].[MarginId] = 'DilButane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilButadiene]				= SUM(CASE WHEN [m].[MarginId] = 'DilButadiene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilPropylene]				= SUM(CASE WHEN [m].[MarginId] = 'DilPropylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[DilBenzene]				= SUM(CASE WHEN [m].[MarginId] = 'DilBenzene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppDil]					= SUM(CASE WHEN [m].[MarginId] = 'SuppDil' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGPropylene]				= SUM(CASE WHEN [m].[MarginId] = 'ROGPropylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGPropane]				= SUM(CASE WHEN [m].[MarginId] = 'ROGPropane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGMethane]				= SUM(CASE WHEN [m].[MarginId] = 'ROGMethane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGInerts]					= SUM(CASE WHEN [m].[MarginId] = 'ROGInerts' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGHydrogen]				= SUM(CASE WHEN [m].[MarginId] = 'ROGHydrogen' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGEthylene]				= SUM(CASE WHEN [m].[MarginId] = 'ROGEthylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGEthane]					= SUM(CASE WHEN [m].[MarginId] = 'ROGEthane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ROGC4Plus]					= SUM(CASE WHEN [m].[MarginId] = 'ROGC4Plus' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppROG]					= SUM(CASE WHEN [m].[MarginId] = 'SuppROG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ConcPropylene]				= SUM(CASE WHEN [m].[MarginId] = 'ConcPropylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ConcEthylene]				= SUM(CASE WHEN [m].[MarginId] = 'ConcEthylene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ConcButadiene]				= SUM(CASE WHEN [m].[MarginId] = 'ConcButadiene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[ConcBenzene]				= SUM(CASE WHEN [m].[MarginId] = 'ConcBenzene' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppConc]					= SUM(CASE WHEN [m].[MarginId] = 'SuppConc' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppToRec]					= SUM(CASE WHEN [m].[MarginId] = 'SuppToRec' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppToPyro]				= SUM(CASE WHEN [m].[MarginId] = 'SuppToPyro' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[SuppTot]					= SUM(CASE WHEN [m].[MarginId] = 'SuppTot' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[FeedLiqOther]				= SUM(CASE WHEN [m].[MarginId] = 'FeedLiqOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[LiqHeavyOther]				= SUM(CASE WHEN [m].[MarginId] = 'LiqHeavyOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[GasOilHt]					= SUM(CASE WHEN [m].[MarginId] = 'GasOilHt' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[GasOilHv]					= SUM(CASE WHEN [m].[MarginId] = 'GasOilHv' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[EpGT390]					= SUM(CASE WHEN [m].[MarginId] = 'EpGT390' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Diesel]					= SUM(CASE WHEN [m].[MarginId] = 'Diesel' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[EpLT390]					= SUM(CASE WHEN [m].[MarginId] = 'EpLT390' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[LiqHeavy]					= SUM(CASE WHEN [m].[MarginId] = 'LiqHeavy' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[LiqLightOther]				= SUM(CASE WHEN [m].[MarginId] = 'LiqLightOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Condensate]				= SUM(CASE WHEN [m].[MarginId] = 'Condensate' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Raffinate]					= SUM(CASE WHEN [m].[MarginId] = 'Raffinate' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[NaphthaHv]					= SUM(CASE WHEN [m].[MarginId] = 'NaphthaHv' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[NaphthaFr]					= SUM(CASE WHEN [m].[MarginId] = 'NaphthaFr' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[NaphthaLt]					= SUM(CASE WHEN [m].[MarginId] = 'NaphthaLt' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Naphtha]					= SUM(CASE WHEN [m].[MarginId] = 'Naphtha' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[HeavyNGL]					= SUM(CASE WHEN [m].[MarginId] = 'HeavyNGL' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[LiqC5C6]					= SUM(CASE WHEN [m].[MarginId] = 'LiqC5C6' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[LiqLight]					= SUM(CASE WHEN [m].[MarginId] = 'LiqLight' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Liquid]					= SUM(CASE WHEN [m].[MarginId] = 'Liquid' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[FeedLtOther]				= SUM(CASE WHEN [m].[MarginId] = 'FeedLtOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[IsoButane]					= SUM(CASE WHEN [m].[MarginId] = 'IsoButane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Butane]					= SUM(CASE WHEN [m].[MarginId] = 'Butane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[LPG]						= SUM(CASE WHEN [m].[MarginId] = 'LPG' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Propane]					= SUM(CASE WHEN [m].[MarginId] = 'Propane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Ethane]					= SUM(CASE WHEN [m].[MarginId] = 'Ethane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[EPMix]						= SUM(CASE WHEN [m].[MarginId] = 'EPMix' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[EthanePropane]				= SUM(CASE WHEN [m].[MarginId] = 'EthanePropane' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[Light]						= SUM(CASE WHEN [m].[MarginId] = 'Light' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[FreshPyroFeed]				= SUM(CASE WHEN [m].[MarginId] = 'FreshPyroFeed' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[PlantFeed]					= SUM(CASE WHEN [m].[MarginId] = 'PlantFeed' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[FeedOther]					= SUM(CASE WHEN [m].[MarginId] = 'FeedOther' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[FeedProdLoss]				= SUM(CASE WHEN [m].[MarginId] = 'FeedProdLoss' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[MarginGross]				= SUM(CASE WHEN [m].[MarginId] = 'MarginGross' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[MarginNetCash]				= SUM(CASE WHEN [m].[MarginId] = 'MarginNetCash' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue],
			[TotRefExp]					= SUM(CASE WHEN [m].[MarginId] = 'TotRefExp' THEN m.OpEx_Cur ELSE 0.0 END) / [d].[DivisorValue],
			[PreTaxIncome]				= SUM(CASE WHEN [m].[MarginId] = 'PreTaxIncome' THEN [m].[Amount_Cur] ELSE 0.0 END) / [d].[DivisorValue]
	FROM
		[calc].[MarginMetaAggregate]	[m]
	INNER JOIN (
		SELECT
			[dmp].[FactorSetID],
			[dmp].[Refnum],
			[DataType] = CASE	[dmp].[ComponentId]
						WHEN 'C2H4'			THEN 'ETHDiv_MT'
						WHEN 'ProdHVC'		THEN 'HVC_MT'
						WHEN 'ProdOlefins'	THEN 'OLEProd_MT'
						END,
			[DivisorValue] = [dmp].[Production_kMT] / 1000.0
		FROM [calc].[DivisorsMetaProduction] [dmp]
		WHERE	[dmp].[ComponentID] IN ('C2H4', 'ProdHVC', 'ProdOlefins')
			AND	[dmp].[Production_kMT] > 0.0
		UNION ALL
		SELECT
			[dmp].[FactorSetID],
			[dmp].[Refnum],
			[DataType] = CASE	[dmp].[ComponentId]
						WHEN 'ProdHVC'		THEN 'HVC_LB'
						END,
			[DivisorValue] = [dmp].[Production_kMT] / 1000.0 * [GlobalDB].[dbo].[UnitsConv](1.0, 'MT', 'kLB')
		FROM [calc].[DivisorsMetaProduction] [dmp]
		WHERE	[dmp].[ComponentID] = 'ProdHVC'
			AND	[dmp].[Production_kMT] > 0.0
		UNION ALL
		SELECT ALL
			@FactorSetId,
			@Refnum,
			'ADJ',
			1.0
		) [d]
		ON	[d].[FactorSetID]	= [m].[FactorSetID]
		AND	[d].[Refnum]		= [m].[Refnum]
	WHERE	[m].[FactorSetId]	= @FactorSetId
		AND	[m].[Refnum]		= @Refnum
	GROUP BY
		[m].[Refnum],
		[m].[FactorSetId],
		[m].[CurrencyRpt],
		[d].[DataType],
		[d].[DivisorValue],
		[m].[MarginAnalysis];