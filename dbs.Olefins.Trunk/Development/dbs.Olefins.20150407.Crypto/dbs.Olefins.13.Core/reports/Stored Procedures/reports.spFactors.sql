﻿


CREATE              PROC [reports].[spFactors](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DELETE FROM reports.Factors WHERE GroupId = @GroupId
Insert into reports.Factors (GroupId
, DataType
, FreshPyroFeed
, SuppTot
, PlantFeed
, FreshPyroFeedPur
, TowerPyroGasHT
, HydroPur
, Auxiliary
, RedPropyleneCG
, RedPropyleneRG
, RedPropylene
, RedEthyleneCG
, RedCrackedGasTrans
, Reduction
, ProcessUnits
, BoilFiredSteam
, ElecGen
, Utilities
, Total
, Ethane
, LPG
, Liquid
, SuppEthane
, SuppLPG
, SuppLiquid
, TowerNAPS
, TowerPyroGasB
, TowerPyroGasSS
, TowerDeethanizer
, TowerDepropanizer
, HydroPurPSA
, HydroPurMembrane
, HydroPurCryogenic
, ElecGenDist
)

SELECT GroupId = @GroupId
, DataType					= f.DataType
, FreshPyroFeed = [$(DbGlobal)].dbo.WtAvg(f.FreshPyroFeed, 1)
, SuppTot = [$(DbGlobal)].dbo.WtAvg(f.SuppTot, 1)
, PlantFeed = [$(DbGlobal)].dbo.WtAvg(f.PlantFeed, 1)
, FreshPyroFeedPur = [$(DbGlobal)].dbo.WtAvg(f.FreshPyroFeedPur, 1)
, TowerPyroGasHT = [$(DbGlobal)].dbo.WtAvg(f.TowerPyroGasHT, 1)
, HydroPur = [$(DbGlobal)].dbo.WtAvg(f.HydroPur, 1)
, Auxiliary = [$(DbGlobal)].dbo.WtAvg(f.Auxiliary, 1)
, RedPropyleneCG = [$(DbGlobal)].dbo.WtAvg(f.RedPropyleneCG, 1)
, RedPropyleneRG = [$(DbGlobal)].dbo.WtAvg(f.RedPropyleneRG, 1)
, RedPropylene = [$(DbGlobal)].dbo.WtAvg(f.RedPropylene, 1)
, RedEthyleneCG = [$(DbGlobal)].dbo.WtAvg(f.RedEthyleneCG, 1)
, RedCrackedGasTrans = [$(DbGlobal)].dbo.WtAvg(f.RedCrackedGasTrans, 1)
, Reduction = [$(DbGlobal)].dbo.WtAvg(f.Reduction, 1)
, ProcessUnits = [$(DbGlobal)].dbo.WtAvg(f.ProcessUnits, 1)
, BoilFiredSteam = [$(DbGlobal)].dbo.WtAvg(f.BoilFiredSteam, 1)
, ElecGen = [$(DbGlobal)].dbo.WtAvg(f.ElecGen, 1)
, Utilities = [$(DbGlobal)].dbo.WtAvg(f.Utilities, 1)
, Total = [$(DbGlobal)].dbo.WtAvg(f.Total, 1)
, Ethane = [$(DbGlobal)].dbo.WtAvg(f.Ethane, 1)
, LPG = [$(DbGlobal)].dbo.WtAvg(f.LPG, 1)
, Liquid = [$(DbGlobal)].dbo.WtAvg(f.Liquid, 1)
, SuppEthane = [$(DbGlobal)].dbo.WtAvg(f.SuppEthane, 1)
, SuppLPG = [$(DbGlobal)].dbo.WtAvg(f.SuppLPG, 1)
, SuppLiquid = [$(DbGlobal)].dbo.WtAvg(f.SuppLiquid, 1)
, TowerNAPS = [$(DbGlobal)].dbo.WtAvg(f.TowerNAPS, 1)
, TowerPyroGasB = [$(DbGlobal)].dbo.WtAvg(f.TowerPyroGasB, 1)
, TowerPyroGasSS = [$(DbGlobal)].dbo.WtAvg(f.TowerPyroGasSS, 1)
, TowerDeethanizer = [$(DbGlobal)].dbo.WtAvg(f.TowerDeethanizer, 1)
, TowerDepropanizer = [$(DbGlobal)].dbo.WtAvg(f.TowerDepropanizer, 1)
, HydroPurPSA = [$(DbGlobal)].dbo.WtAvg(f.HydroPurPSA, 1)
, HydroPurMembrane = [$(DbGlobal)].dbo.WtAvg(f.HydroPurMembrane, 1)
, HydroPurCryogenic = [$(DbGlobal)].dbo.WtAvg(f.HydroPurCryogenic, 1)
, ElecGenDist = [$(DbGlobal)].dbo.WtAvg(f.ElecGenDist, 1)
FROM @MyGroup r JOIN dbo.Factors f on f.Refnum=r.Refnum 
GROUP BY f.DataType


SET NOCOUNT OFF

















