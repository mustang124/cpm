﻿


CREATE              PROC [reports].[spAPCAppCount](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.APCAppCount WHERE GroupId = @GroupId
Insert into reports.APCAppCount (GroupId
, PyroFurnIndivid
, PyroFurn
, TowerQuench
, CompGas
, TowerOther
, TowerDepropDebut
, TowerPropylene
, SteamHeat
, OtherOnSite)

SELECT GroupId = @GroupId
, PyroFurnIndivid	= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcPyroFurnIndivid' THEN 1 ELSE 0 END)
, PyroFurn			= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcFurnFeed' THEN 1 ELSE 0 END)
, TowerQuench		= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcTowerQuench' THEN 1 ELSE 0 END)
, CompGas			= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcTowerComp' THEN 1 ELSE 0 END)
, TowerOther		= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcTowerOther' THEN 1 ELSE 0 END)
, TowerDepropDebut	= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcTowerDepropDebut' THEN 1 ELSE 0 END)
, TowerPropylene	= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcTowerPropylene' THEN 1 ELSE 0 END)
, SteamHeat			= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcSteamHeat' THEN 1 ELSE 0 END)
, OtherOnSite		= [$(DbGlobal)].dbo.WtAvg(Controller_Count, CASE WHEN ApcId = 'ApcOtherOnSite' THEN 1 ELSE 0 END)
FROM @MyGroup r JOIN fact.ApcControllers a on a.Refnum=r.Refnum

UPDATE reports.APCAppCount Set Total = ISNULL(PyroFurnIndivid, 0) + ISNULL(PyroFurn, 0) + ISNULL(TowerQuench, 0) + ISNULL(CompGas, 0) +
ISNULL(TowerOther, 0) + ISNULL(TowerDepropDebut, 0) + ISNULL(TowerPropylene, 0) + ISNULL(SteamHeat, 0) + ISNULL(OtherOnSite, 0) 
WHERE GroupId = @GroupId

SET NOCOUNT OFF

--select * from fact.ApcControllers where Refnum = '2011PCH194'

















