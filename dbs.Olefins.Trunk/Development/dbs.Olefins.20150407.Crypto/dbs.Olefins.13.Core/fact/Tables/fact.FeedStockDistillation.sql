﻿CREATE TABLE [fact].[FeedStockDistillation] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [DistillationID]    VARCHAR (4)        NOT NULL,
    [Temp_C]            REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_FeedStockDistillation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistillation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistillation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_FeedStockDistillation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedStockDistillation] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC, [DistillationID] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_FeedStockDistillation_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_FeedStockDistillation_Temp_C] CHECK ([Temp_C]>=(0.0)),
    CONSTRAINT [FK_FeedStockDistillation_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedStockDistillation_FeedStockDistillation_LookUp] FOREIGN KEY ([DistillationID]) REFERENCES [dim].[FeedStockDistillation_LookUp] ([DistillationId]),
    CONSTRAINT [FK_FeedStockDistillation_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FeedStockDistillation_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedStockDistillation_u]
	ON [fact].[FeedStockDistillation]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedStockDistillation]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedStockDistillation].Refnum				= INSERTED.Refnum
		AND [fact].[FeedStockDistillation].StreamId				= INSERTED.StreamId
		AND [fact].[FeedStockDistillation].StreamDescription	= INSERTED.StreamDescription
		AND [fact].[FeedStockDistillation].DistillationID		= INSERTED.DistillationID
		AND [fact].[FeedStockDistillation].CalDateKey			= INSERTED.CalDateKey;

END;