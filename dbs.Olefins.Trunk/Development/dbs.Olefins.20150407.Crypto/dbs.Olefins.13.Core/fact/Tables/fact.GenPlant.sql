﻿CREATE TABLE [fact].[GenPlant] (
    [Refnum]                    VARCHAR (25)       NOT NULL,
    [CalDateKey]                INT                NOT NULL,
    [EthyleneCarrier_Bit]       BIT                NULL,
    [ExtractButadiene_Bit]      BIT                NULL,
    [ExtractAromatics_Bit]      BIT                NULL,
    [MTBE_Bit]                  BIT                NULL,
    [Isobutylene_Bit]           BIT                NULL,
    [IsobutyleneHighPurity_Bit] BIT                NULL,
    [Butene1_Bit]               BIT                NULL,
    [Isoprene_Bit]              BIT                NULL,
    [CycloPentadiene_Bit]       BIT                NULL,
    [OtherC5_Bit]               BIT                NULL,
    [PolyPlantShare_Bit]        BIT                NULL,
    [OlefinsDerivatives_Bit]    BIT                NULL,
    [IntegrationRefinery_Bit]   BIT                NULL,
    [CapCreepAnn_Pcnt]          REAL               NULL,
    [LocationFactor]            REAL               NULL,
    [OSIMPrep_Hrs]              REAL               NULL,
    [tsModified]                DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]            NVARCHAR (168)     CONSTRAINT [DF_GenPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]            NVARCHAR (168)     CONSTRAINT [DF_GenPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]             NVARCHAR (168)     CONSTRAINT [DF_GenPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlant] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlant_CapCreepAnn_Pcnt] CHECK ([CapCreepAnn_Pcnt]>=(0.0) AND [CapCreepAnn_Pcnt]<=(120.0)),
    CONSTRAINT [CR_GenPlant_Location_Factor] CHECK ([LocationFactor]>=(0.0)),
    CONSTRAINT [CR_GenPlant_OSIMPrep_Hrs] CHECK ([OSIMPrep_Hrs]>=(0.0)),
    CONSTRAINT [FK_GenPlant_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlant_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlant_u]
	ON [fact].[GenPlant]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlant]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlant].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlant].CalDateKey	= INSERTED.CalDateKey;

END;