﻿CREATE TABLE [fact].[ReliabilityCritPathNameOther] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [FacilityName]   NVARCHAR (96)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityCritPathNameOther_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCritPathNameOther_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCritPathNameOther_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCritPathNameOther_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityCritPathNameOther] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_ReliabilityCritPathNameOther_FacilityName] CHECK ([FacilityName]<>''),
    CONSTRAINT [FK_ReliabilityCritPathNameOther_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityCritPathNameOther_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_ReliabilityCritPathNameOther_ReliabilityCritPath] FOREIGN KEY ([Refnum], [FacilityId], [CalDateKey]) REFERENCES [fact].[ReliabilityCritPath] ([Refnum], [FacilityId], [CalDateKey]),
    CONSTRAINT [FK_ReliabilityCritPathNameOther_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityCritPathNameOther_u]
	ON [fact].[ReliabilityCritPathNameOther]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityCritPathNameOther]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityCritPathNameOther].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityCritPathNameOther].FacilityId	= INSERTED.FacilityId
		AND [fact].[ReliabilityCritPathNameOther].CalDateKey	= INSERTED.CalDateKey;

END;