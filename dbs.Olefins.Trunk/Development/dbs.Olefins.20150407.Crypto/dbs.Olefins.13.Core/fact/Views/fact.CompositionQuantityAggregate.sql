﻿CREATE VIEW fact.CompositionQuantityAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	c.StreamId,
	c.StreamDescription,
	b.ComponentId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c.Component_WtPcnt * q.Tot_kMT 
		WHEN '-' THEN - c.Component_WtPcnt * q.Tot_kMT
		END) / 100.0								[Component_kMT],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + c.Component_WtPcnt
		WHEN '-' THEN - c.Component_WtPcnt
		END)										[Component_WtPcnt],
	ROW_NUMBER() OVER (PARTITION BY c.Refnum, c.CalDateKey, c.StreamId
		ORDER BY SUM(
		CASE WHEN b.ComponentId <> 'Tot' AND m.PYPS IS NOT NULL THEN
			CASE b.DescendantOperator
			WHEN '+' THEN + c.Component_WtPcnt
			WHEN '-' THEN - c.Component_WtPcnt
			END END) DESC)							[PYPSComposition_Rank],
	ROW_NUMBER() OVER (PARTITION BY c.Refnum, c.CalDateKey, c.StreamId
		ORDER BY SUM(
		CASE WHEN b.ComponentId <> 'Tot' AND m.SPSL IS NOT NULL THEN
			CASE b.DescendantOperator
			WHEN '+' THEN + c.Component_WtPcnt
			WHEN '-' THEN - c.Component_WtPcnt
			END END) DESC)							[SPSLComposition_Rank]
FROM dim.Component_Bridge					b
LEFT OUTER JOIN ante.MapComponentToInput	m
	ON	m.FactorSetId = b.FactorSetId
	AND	m.ComponentId = b.DescendantId
INNER JOIN fact.CompositionQuantity			c
	ON	c.ComponentId = b.DescendantId
INNER JOIN fact.QuantityPivot				q
	ON	q.Refnum = c.Refnum
	AND	q.CalDateKey = c.CalDateKey
	AND	q.StreamId = c.StreamId
	AND	q.StreamDescription = c.StreamDescription
GROUP BY
	 b.FactorSetId,
	 c.Refnum,
	 c.CalDateKey,
	 c.StreamId,
	 c.StreamDescription,
	 b.ComponentId;