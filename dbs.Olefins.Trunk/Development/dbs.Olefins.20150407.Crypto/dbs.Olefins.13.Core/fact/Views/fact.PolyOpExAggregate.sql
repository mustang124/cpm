﻿CREATE VIEW fact.PolyOpExAggregate
WITH SCHEMABINDING
AS
SELECT
	b.[FactorSetId],
	o.[AssetId],
	o.[CalDateKey], 
	o.[CurrencyRpt], 
	b.[AccountId],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + o.[Amount_Cur]
		WHEN '-' THEN - o.[Amount_Cur]
		ELSE 0.0
		END)			[Amount_Cur],
	COUNT_BIG(*)		[IndexItems]
FROM [dim].[Account_Bridge]			b
INNER JOIN [fact].[PolyOpEx]		o
	ON	o.[AccountId] = b.[DescendantId]
GROUP BY
	b.[FactorSetId],
	o.[AssetId],
	o.[CalDateKey], 
	o.[CurrencyRpt], 
	b.[AccountId];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_PolyOpExAggregate]
    ON [fact].[PolyOpExAggregate]([FactorSetId] ASC, [AssetId] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC);

