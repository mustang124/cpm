﻿CREATE VIEW fact.TSort
WITH SCHEMABINDING
AS
SELECT
	tc.Refnum,
	tc.CalDateKey,
	c.CompanyId,
	a.AssetId,
	a.AssetName,
	c.CompanyId + ' - ' + a.AssetName	[CoLoc],
	
	sc.SubscriberCompanyName,
	tc.PlantCompanyName,
	sa.SubscriberAssetName,
	tc.PlantAssetName,
	
	g.RegionId,
	g.RegionId	[Region],
	r.EconRegionID,
	a.CountryId,
	cl.CountryName,
	a.StateName,

	tc.UomId,
	tc.CurrencyFcn,
	tc.CurrencyRpt,
	
	sa.PassWord_PlainText				[AssetPassWord_PlainText],
	ts._StudyYear						[StudyYear],
	tc._DataYear						[DataYear]

FROM fact.TSortClient							tc
LEFT OUTER JOIN cons.TSortSolomon				ts
	ON	ts.Refnum		= tc.Refnum
LEFT OUTER JOIN cons.SubscriptionsAssets		sa
	ON	sa.Refnum		= tc.Refnum
LEFT OUTER JOIN cons.SubscriptionsCompanies		sc
	ON	sc.CompanyId	= sa.CompanyId
	AND	sc.StudyId		= sa.StudyId
	AND	sc.CalDateKey	= sa.CalDateKey
LEFT OUTER JOIN cons.Assets						a
	ON	a.AssetId		= sa.AssetId
LEFT OUTER JOIN dim.Company_LookUp				c
	ON	c.CompanyId		= sa.CompanyId
LEFT OUTER JOIN ante.CountryEconRegion			r
	ON	r.FactorSetId	= ts._StudyYear
	AND	r.CountryId		= a.CountryId
LEFT OUTER JOIN ante.CountryRegion_Geo			g
	ON	g.CountryId		= a.CountryId
LEFT OUTER JOIN dim.Country_LookUp				cl
	ON	cl.CountryId	= a.CountryId
