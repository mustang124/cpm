﻿CREATE PROCEDURE [fact].[Insert_ReliabilityPyroFurnPlantLimit]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityPyroFurnPlantLimit(Refnum, CalDateKey, OppLossId, PlantFurnLimit_Pcnt)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, 'FurnaceMech'												[OppLossId]
			, m.PctFurnRelyLimit * 100.0								[PlantFurnLimit_Pcnt]
		
		FROM stgFact.Misc m
		INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
		WHERE m.PctFurnRelyLimit IS NOT NULL
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;