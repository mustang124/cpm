﻿CREATE PROCEDURE [fact].[Insert_Polymer]
(
	@Refnum		VARCHAR(18)
)
AS
BEGIN
	
	SET NOCOUNT ON;
		
	PRINT NCHAR(9) + 'INSERT INTO cons.Assets (Polymer)';
	
	INSERT INTO cons.Assets(AssetIdPri, AssetIdSec, AssetName, AssetDetail, CountryId)
	SELECT DISTINCT
		  etl.ConvAssetPCH(f.Refnum)				[AssetIdPri]
		, 'T' + CONVERT(VARCHAR(2), f.PlantId)		[AssetIdSec]
		, MIN(f.Name)
		, MIN(f.Name + ISNULL(' (' + f.City, '') + CASE WHEN LEN(f.[State]) >= 1 THEN ISNULL(', ' + f.[State], '') ELSE '' END + CASE WHEN LEN(f.City) >= 2 THEN ')' ELSE '' END)
		, MIN(etl.ConvCountryID(f.Refnum, ISNULL(f.Country, t.PlantCountry)))
	FROM stgFact.PolyFacilities f
	INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
	WHERE f.Name <> '0'
		AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		AND etl.ConvAssetPCH(f.Refnum) + '-' + 'T' + CONVERT(VARCHAR(2), f.PlantId)	NOT IN (SELECT AssetId FROM cons.Assets)
	GROUP BY
		  etl.ConvAssetPCH(f.Refnum)
		, 'T' + CONVERT(VARCHAR(2), f.PlantId);
		
	PRINT NCHAR(9) + 'INSERT INTO cons.AssetsParents (Polymer)';
		
	INSERT INTO cons.AssetsParents(CalDateKey, AssetId, Parent, Operator, Hierarchy)
	SELECT DISTINCT
		etl.ConvDateKey(t.StudyYear),				
		etl.ConvAssetPCH(f.Refnum) + '-T' + CONVERT(VARCHAR(2), f.PlantId)	[AssetId],
		etl.ConvAssetPCH(f.Refnum)				[AssetParent],
		'+',
		'/'
	FROM stgFact.PolyFacilities			f
	INNER JOIN stgFact.TSort			t
		ON	t.Refnum = f.Refnum
	LEfT OUTER JOIN cons.AssetsParents ap
		ON	ap.CalDateKey = etl.ConvDateKey(t.StudyYear)
		AND	ap.AssetId = etl.ConvAssetPCH(f.Refnum) + '-T' + CONVERT(VARCHAR(2), f.PlantId)
		AND	ap.Parent = etl.ConvAssetPCH(f.Refnum)
	WHERE	f.Name <> '0'
		AND	ap.AssetId IS NULL
		AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;
	
	PRINT NCHAR(9) + 'INSERT INTO fact.PolyFacilities';
	
	INSERT INTO fact.PolyFacilities(
		  Refnum
		, CalDateKey
		, AssetId
		
		, PolyName
		, PolyCity
		, PolyState
		
		, Capacity_kMT
		
		, Stream_MTd
		, ReactorTrains_Count
		, StartUp_Year
			
		, PolymerFamilyID
		, PolymerTechID
		, PrimeResin_Pcnt
		)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvAssetPCH(p.Refnum)
			+ '-T' + CONVERT(VARCHAR(2), p.PlantId)	[AssetId]
		, p.Name
		, p.City
		, p.[State]
		, p.CapKMTA
		, p.CapMTD
		, p.ReactorTrainCnt
		, CASE WHEN p.ReactorStartup > 1900 THEN p.ReactorStartup END
		, etl.ConvPolymerFamily(p.Product)
		, etl.ConvPolymerTech(p.Technology)
		, CASE WHEN p.ReactorResinPct > 100.0 THEN 100.0 ELSE p.ReactorResinPct END
	FROM stgFact.PolyFacilities p
	INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	WHERE p.Name <> '0'
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;
		
	PRINT NCHAR(9) + 'INSERT INTO fact.PolyOpEx';
	
	INSERT INTO fact.PolyOpEx(Refnum, CalDateKey, AssetId, AccountId, CurrencyRpt, Amount_Cur)
	SELECT
			u.Refnum
		, u.CalDateKey
		, u.AssetId
		, etl.ConvAccountID(u.AccountId)
		, 'USD'
		, u.AmountRPT
	FROM(
		SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvAssetPCH(p.Refnum)
				+ '-T' + CONVERT(VARCHAR(2), p.PlantId)				[AssetId]
			, p.NonTAMaint
			, p.TA
			, p.OthNonVol
			, p.Catalysts
			, p.Chemicals	[ChemOth]
			, p.Additives
			, p.Royalties
			, p.Energy
			, p.STVol
				- ISNULL(p.Catalysts, 0.0)
				- ISNULL(p.Chemicals, 0.0)
				- ISNULL(p.Additives, 0.0)
				- ISNULL(p.Royalties, 0.0)
				- ISNULL(p.Energy, 0.0)
				- ISNULL(p.Packaging, 0.0)
				- ISNULL(p.OthVol, 0.0)
							[PurOth]
			, p.Packaging
			, p.OthVol
			, p.STVol
		FROM stgFact.PolyOpEx p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	p.TotCashOpEx <> 0.0
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) t
		UNPIVOT(
		AmountRPT FOR AccountId IN (
				NonTAMaint
			, TA
			, OthNonVol
		
			, Catalysts
			, ChemOth
			, Additives
			, Royalties
			, Energy
			, PurOth
			, Packaging
			, OthVol
			)
		) u
		WHERE u.AmountRPT > 0.0
	
	PRINT NCHAR(9) + 'INSERT INTO fact.PolyQuantity';
	
	INSERT INTO fact.PolyQuantity(Refnum, CalDateKey, AssetId, StreamId, Quantity_kMT)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, u.AssetId
		, etl.ConvStreamIDPoly(u.StreamId)	[StreamId]
		, u.kMT
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvAssetPCH(p.Refnum)
				+ '-T' + CONVERT(VARCHAR(2), p.TrainId)				[AssetId]
			, p.Hydrogen
			, p.Ethylene_Plant
			, p.Ethylene_Other
			, p.Propylene_Plant
			, p.Propylene_Other
			, p.Butene
			, p.Hexene
			, p.Octene
			, p.OthMono1
			, p.OthMono2
			, p.OthMono3
			, p.Additives
			, p.Catalysts
			, p.OtherFeed
			--, p.TotalRawMatl
			--, p.OthMono1Desc
			--, p.OthMono2Desc
			--, p.OthMono3Desc
			, p.LDPE_Liner
			, p.LDPE_Clarity
			, p.LDPE_Blow
			, p.LDPE_GP
			, p.LDPE_LidResin
			, p.LDPE_EVA
			, p.LDPE_Other
			, p.LDPE_Offspec
			, p.LLDPE_ButeneLiner
			, p.LLDPE_ButeneMold
			, p.LLDPE_HAOLiner
			, p.LLDPE_HAOMeltIndex
			, p.LLDPE_HAOInject
			, p.LLDPE_HAOLidResin
			, p.LLDPE_HAORoto
			, p.LLDPE_Other
			, p.LLDPE_Offspec
			, p.HDPE_Homopoly
			, p.HDPE_Copoly
			, p.HDPE_Film
			, p.HDPE_GP
			, p.HDPE_Pipe
			, p.HDPE_Drum
			, p.HDPE_Roto
			, p.HDPE_Other
			, p.HDPE_Offspec
			, p.POLY_Homo
			, p.POLY_Copoly
			, p.POLY_CopolyBlow
			, p.POLY_CopolyInject
			, p.POLY_CopolyBlock
			, p.POLY_AtacticPP
			, p.POLY_Other
			, p.POLY_Offspec
			, p.ByProdOth1
			, p.ByProdOth2
			, p.ByProdOth3
			, p.ByProdOth4
			, p.ByProdOth5
			--, p.TotProducts
			--, p.ByProdOth1Desc
			--, p.ByProdOth2Desc
			--, p.ByProdOth3Desc
			--, p.ByProdOth4Desc
			--, p.ByProdOth5Desc
			, p.ReactorProd
			--, p.MonomerConsumed
			--, p.MonomerUtil
		FROM stgFact.PolyMatlBalance p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		INNER JOIN stgFact.PolyFacilities f ON f.Refnum = p.Refnum AND f.PlantId = p.TrainId
		WHERE	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT (
		kMT FOR StreamId IN (
			  p.Hydrogen
			, p.Ethylene_Plant
			, p.Ethylene_Other
			, p.Propylene_Plant
			, p.Propylene_Other
			, p.Butene
			, p.Hexene
			, p.Octene
			, p.OthMono1
			, p.OthMono2
			, p.OthMono3
			, p.Additives
			, p.Catalysts
			, p.OtherFeed
			--, p.TotalRawMatl
			--, p.OthMono1Desc
			--, p.OthMono2Desc
			--, p.OthMono3Desc
			, p.LDPE_Liner
			, p.LDPE_Clarity
			, p.LDPE_Blow
			, p.LDPE_GP
			, p.LDPE_LidResin
			, p.LDPE_EVA
			, p.LDPE_Other
			, p.LDPE_Offspec
			, p.LLDPE_ButeneLiner
			, p.LLDPE_ButeneMold
			, p.LLDPE_HAOLiner
			, p.LLDPE_HAOMeltIndex
			, p.LLDPE_HAOInject
			, p.LLDPE_HAOLidResin
			, p.LLDPE_HAORoto
			, p.LLDPE_Other
			, p.LLDPE_Offspec
			, p.HDPE_Homopoly
			, p.HDPE_Copoly
			, p.HDPE_Film
			, p.HDPE_GP
			, p.HDPE_Pipe
			, p.HDPE_Drum
			, p.HDPE_Roto
			, p.HDPE_Other
			, p.HDPE_Offspec
			, p.POLY_Homo
			, p.POLY_Copoly
			, p.POLY_CopolyBlow
			, p.POLY_CopolyInject
			, p.POLY_CopolyBlock
			, p.POLY_AtacticPP
			, p.POLY_Other
			, p.POLY_Offspec
			, p.ByProdOth1
			, p.ByProdOth2
			, p.ByProdOth3
			, p.ByProdOth4
			, p.ByProdOth5
			--, p.TotProducts
			--, p.ByProdOth1Desc
			--, p.ByProdOth2Desc
			--, p.ByProdOth3Desc
			--, p.ByProdOth4Desc
			--, p.ByProdOth5Desc
			, p.ReactorProd
			--, p.MonomerConsumed
			--, p.MonomerUtil
			)
		)u
	INNER JOIN cons.Assets a ON a.AssetId = u.AssetId;
	
	PRINT NCHAR(9) + 'INSERT INTO fact.PolyQuantityNameOther';
	
	INSERT INTO fact.PolyQuantityNameOther(Refnum, CalDateKey, AssetId, StreamId, StreamName)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, u.AssetId
		, etl.ConvStreamIDPoly(u.StreamId)	[StreamId]
		, CASE 
			WHEN LEN(u.StreamDetail) = 0 THEN etl.ConvStreamIDPoly(u.StreamId)
			WHEN u.StreamDetail = '0' THEN etl.ConvStreamIDPoly(u.StreamId)
			ELSE u.StreamDetail
			END
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, etl.ConvAssetPCH(p.Refnum)
				+ '-T' + CONVERT(VARCHAR(2), p.TrainId)					[AssetId]
			, p.OthMono1Desc											[OthMono1]
			, CASE WHEN p.OthMono2Desc <> '' THEN p.OthMono2Desc END	[OthMono2]
			, p.OthMono3Desc											[OthMono3]
			, p.ByProdOth1Desc		[ByProdOth1]
			, p.ByProdOth2Desc		[ByProdOth2]
			, p.ByProdOth3Desc		[ByProdOth3]
			, p.ByProdOth4Desc		[ByProdOth4]
			, p.ByProdOth5Desc		[ByProdOth5]
		FROM stgFact.PolyMatlBalance p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		INNER JOIN stgFact.PolyFacilities f ON f.Refnum = p.Refnum AND f.PlantId = p.TrainId
		WHERE f.Name <> '0'
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT (
		StreamDetail FOR StreamId IN (
			  p.OthMono1
			, p.OthMono2
			, p.OthMono3
			, p.ByProdOth1
			, p.ByProdOth2
			, p.ByProdOth3
			, p.ByProdOth4
			, p.ByProdOth5
			)
		)u
	INNER JOIN fact.PolyQuantity	pq
		ON	pq.Refnum = u.Refnum
		AND	pq.AssetId = u.AssetId
		AND	pq.StreamId = etl.ConvStreamIDPoly(u.StreamId)
	INNER JOIN cons.Assets a ON a.AssetId = u.AssetId;
	
END;