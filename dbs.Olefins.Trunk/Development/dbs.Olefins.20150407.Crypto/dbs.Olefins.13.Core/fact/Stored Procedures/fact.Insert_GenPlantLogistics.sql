﻿CREATE PROCEDURE [fact].[Insert_GenPlantLogistics]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantLogistics(Refnum, CalDateKey, AccountId, StreamId, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, 'Logistics'
			, u.StreamId
			, 'USD'
			, u.Amount_Cur
		FROM(
			SELECT 
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, p.FeedLogCost	[FreshPyroFeed]
				, p.EtyLogCost	[Ethylene]
				, p.PryLogCost	[Propylene]
			
			FROM stgFact.Prac p
			INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
			WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) t
			UNPIVOT (
			Amount_Cur FOR StreamId IN(
				  FreshPyroFeed
				, Ethylene
				, Propylene
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;