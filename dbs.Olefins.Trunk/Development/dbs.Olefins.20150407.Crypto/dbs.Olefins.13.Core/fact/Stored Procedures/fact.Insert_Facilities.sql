﻿CREATE PROCEDURE [fact].[Insert_Facilities]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Facilities(Refnum, CalDateKey, FacilityId, Unit_Count)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvFacilityID(LEFT(u.FacilityId, LEN(u.FacilityId) - 3))	[FacilityId]
			, u.UnitCount
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
				, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
				, FracFeedCnt
				, PyrFurnCnt
				, QTowerCnt
				, QWaterSysCnt
				, GasFracCnt
				, ProcDryerCnt
				, GasCompCnt
				--, GasCompStageCnt
				, CTowerCnt
				, MethSysCnt
				, DemethTowerCnt
				, DeethTowerCnt
				, AcetylCnt
				, EthylFracCnt
				, DepropTowerCnt
				, PropylFracCnt
				, DebutTowerCnt
				, PropCompCnt
				, EthyCompCnt
				, MethCompCnt
				, HPBoilCnt
				, LPBoilCnt
				, CONVERT(TINYINT, CASE t.Refnum
					WHEN '03PCH03B' THEN 1
					WHEN '03PCH182' THEN 1
					WHEN '03PCH029' THEN 1
					WHEN '03PCH166' THEN 1
					WHEN '03PCH147' THEN 1
					ELSE SteamHeatCnt
					END)							[SteamHeatCnt]
				, ElecGenCnt
				, CoolTowerCnt
				, HydroCryoCnt
				, HydroPSACnt
				, HydroMembCnt
				, FeedStorCnt
				, AboveStorCnt
				, UnderStorCnt
				, EthyLiqCnt
				, PGasHydroCnt
				, PipeCnt
				, RailCnt
				, MarineCnt
				, AirCompCnt
				, ElecTransfCnt
				, FlareCnt
				, WaterTreatCnt
				, BldgCnt
				, RGRCnt
				--, UnitCnt
				, OthCnt1		[Oth1Cnt]
				, OthCnt2		[Oth2Cnt]
				, OthCnt3		[Oth3Cnt]
			FROM stgFact.Facilities f
			INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
			WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT(
			UnitCount FOR FacilityId IN(
				  FracFeedCnt
				, PyrFurnCnt
				, QTowerCnt
				, QWaterSysCnt
				, GasFracCnt
				, ProcDryerCnt
				, GasCompCnt
				--, GasCompStageCnt
				, CTowerCnt
				, MethSysCnt
				, DemethTowerCnt
				, DeethTowerCnt
				, AcetylCnt
				, EthylFracCnt
				, DepropTowerCnt
				, PropylFracCnt
				, DebutTowerCnt
				, PropCompCnt
				, EthyCompCnt
				, MethCompCnt
				, HPBoilCnt
				, LPBoilCnt
				, SteamHeatCnt
				, ElecGenCnt
				, CoolTowerCnt
				, HydroCryoCnt
				, HydroPSACnt
				, HydroMembCnt
				, FeedStorCnt
				, AboveStorCnt
				, UnderStorCnt
				, EthyLiqCnt
				, PGasHydroCnt
				, PipeCnt
				, RailCnt
				, MarineCnt
				, AirCompCnt
				, ElecTransfCnt
				, FlareCnt
				, WaterTreatCnt
				, BldgCnt
				, RGRCnt
				--, UnitCnt
				, Oth1Cnt
				, Oth2Cnt
				, Oth3Cnt
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;