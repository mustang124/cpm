﻿CREATE TABLE [Ranking].[RankSpecsStats] (
    [RankSpecsID] INT           NOT NULL,
    [LastTime]    SMALLDATETIME CONSTRAINT [DF__RankSpecs__LastT__5090EFD7] DEFAULT (getdate()) NOT NULL,
    [NumTiles]    TINYINT       NULL,
    [DataCount]   SMALLINT      NULL,
    [Top2]        FLOAT (53)    NULL,
    [Tile1Break]  FLOAT (53)    NULL,
    [Tile2Break]  FLOAT (53)    NULL,
    [Tile3Break]  FLOAT (53)    NULL,
    [Btm2]        FLOAT (53)    NULL,
    CONSTRAINT [PK_RankSpecsStats] PRIMARY KEY CLUSTERED ([RankSpecsID] ASC) WITH (FILLFACTOR = 70)
);

