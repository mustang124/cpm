﻿CREATE TYPE [Ranking].[RankedData] AS TABLE (
    [Refnum]     VARCHAR (25) NOT NULL,
    [Rank]       SMALLINT     NULL,
    [Percentile] REAL         NULL,
    [Tile]       TINYINT      NULL,
    [Value]      FLOAT (53)   NULL,
    PRIMARY KEY CLUSTERED ([Refnum] ASC));

