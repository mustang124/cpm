﻿CREATE TABLE [dim].[SimFieldDefinitions_LookUp] (
    [DataSetId]      VARCHAR (6)        NOT NULL,
    [ModelId]        VARCHAR (12)       NOT NULL,
    [FieldId]        VARCHAR (12)       NOT NULL,
    [FieldDetail]    VARCHAR (256)      NULL,
    [ComponentId]    VARCHAR (42)       NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_SimFieldDefinitions_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_SimFieldDefinitions_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_SimFieldDefinitions_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_SimFieldDefinitions_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SimFieldDefinitions_LookUp] PRIMARY KEY CLUSTERED ([DataSetId] ASC, [ModelId] ASC, [FieldId] ASC),
    CONSTRAINT [CL_SimFieldDefinitions_LookUp_FieldDetail] CHECK ([FieldDetail]<>''),
    CONSTRAINT [CL_SimFieldDefinitions_LookUp_FieldId] CHECK ([FieldId]<>''),
    CONSTRAINT [CV_SimFieldDefinitions_LookUp_DataSetId] CHECK ([DataSetId]='Input' OR [DataSetId]='Output'),
    CONSTRAINT [FK_SimFieldDefinitions_LookUp_CompositionLu] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_SimFieldDefinitions_LookUp_SimModelLu] FOREIGN KEY ([ModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId])
);


GO

CREATE TRIGGER [dim].[t_SimFieldDefinitions_LookUp_u]
	ON [dim].[SimFieldDefinitions_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[SimFieldDefinitions_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[SimFieldDefinitions_LookUp].[DataSetId]	= INSERTED.[DataSetId]
		AND	[dim].[SimFieldDefinitions_LookUp].[ModelId]	= INSERTED.[ModelId]
		AND	[dim].[SimFieldDefinitions_LookUp].[FieldId]	= INSERTED.[FieldId];
		
END;
