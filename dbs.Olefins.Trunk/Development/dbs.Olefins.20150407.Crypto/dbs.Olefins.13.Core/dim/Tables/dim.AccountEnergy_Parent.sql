﻿CREATE TABLE [dim].[AccountEnergy_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [AccountId]      VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_AccountEnergy_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_AccountEnergy_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_AccountEnergy_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_AccountEnergy_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_AccountEnergy_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_AccountEnergy_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] DESC, [AccountId] ASC),
    CONSTRAINT [CR_AccountEnergy_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_AccountEnergy_Parent_FactorSetLu] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_AccountEnergy_Parent_LookUp_Accounts] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_AccountEnergy_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_AccountEnergy_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[AccountEnergy_Parent] ([FactorSetId], [AccountId])
);


GO

CREATE TRIGGER [dim].[t_AccountEnergy_Parent_u]
ON [dim].[AccountEnergy_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[AccountEnergy_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[AccountEnergy_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[AccountEnergy_Parent].[AccountId]		= INSERTED.[AccountId];

END;
