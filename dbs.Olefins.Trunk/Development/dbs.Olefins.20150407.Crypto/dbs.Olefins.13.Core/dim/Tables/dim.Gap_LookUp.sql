﻿CREATE TABLE [dim].[Gap_LookUp] (
    [GapId]          VARCHAR (42)       NOT NULL,
    [GapName]        NVARCHAR (84)      NOT NULL,
    [GapDetail]      NVARCHAR (348)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Gap_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Gap_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Gap_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Gap_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Gap_LookUp] PRIMARY KEY CLUSTERED ([GapId] ASC),
    CONSTRAINT [CL_Gap_LookUp_GapDetail] CHECK ([GapDetail]<>''),
    CONSTRAINT [CL_Gap_LookUp_GapId] CHECK ([GapId]<>''),
    CONSTRAINT [CL_Gap_LookUp_GapName] CHECK ([GapName]<>''),
    CONSTRAINT [UK_Gap_LookUp_GapDetail] UNIQUE NONCLUSTERED ([GapDetail] ASC),
    CONSTRAINT [UK_Gap_LookUp_GapName] UNIQUE NONCLUSTERED ([GapName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Gap_LookUp_u]
	ON [dim].[Gap_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Gap_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Gap_LookUp].[GapId]		= INSERTED.[GapId];
		
END;
