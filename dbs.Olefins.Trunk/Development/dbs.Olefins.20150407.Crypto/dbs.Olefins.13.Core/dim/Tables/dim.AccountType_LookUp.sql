﻿CREATE TABLE [dim].[AccountType_LookUp] (
    [AccountTypeId]     VARCHAR (12)       NOT NULL,
    [AccountTypeName]   NVARCHAR (84)      NOT NULL,
    [AccountTypeDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_AccountType_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_AccountType_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_AccountType_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_AccountType_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_AccountType_LookUp] PRIMARY KEY CLUSTERED ([AccountTypeId] ASC),
    CONSTRAINT [CL_AccountType_LookUp_AccountTypeDetail] CHECK ([AccountTypeDetail]<>''),
    CONSTRAINT [CL_AccountType_LookUp_AccountTypeID] CHECK ([AccountTypeId]<>''),
    CONSTRAINT [CL_AccountType_LookUp_AccountTypeName] CHECK ([AccountTypeName]<>''),
    CONSTRAINT [UK_AccountType_LookUp_AccountTypeDetail] UNIQUE NONCLUSTERED ([AccountTypeDetail] ASC),
    CONSTRAINT [UK_AccountType_LookUp_AccountTypeName] UNIQUE NONCLUSTERED ([AccountTypeName] ASC)
);


GO

CREATE TRIGGER [dim].[t_AccountType_LookUp_u]
	ON [dim].[AccountType_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[AccountType_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[AccountType_LookUp].[AccountTypeId]		= INSERTED.[AccountTypeId];
		
END;
