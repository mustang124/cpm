﻿CREATE TABLE [inter].[PricingPrimaryCompositionCountry] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [CountryId]             CHAR (3)           NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [ComponentId]           VARCHAR (42)       NOT NULL,
    [Proximity_RegionID]    VARCHAR (5)        NULL,
    [Raw_Amount_Cur]        REAL               NOT NULL,
    [Prx_Amount_Cur]        REAL               NULL,
    [Adj_MatlBal_Cur]       REAL               NULL,
    [Adj_Proximity_Cur]     REAL               NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],coalesce([MatlBal_Supersede_Cur],(coalesce([Prx_Amount_Cur],[Raw_Amount_Cur])+coalesce([Adj_MatlBal_Cur],(0.0)))+coalesce([Adj_Proximity_Cur],(0.0))),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPrimaryCompositionCountry_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryCompositionCountry_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryCompositionCountry_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryCompositionCountry_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingPrimaryCompositionCountry] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [CountryId] ASC, [StreamId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingPrimaryCompositionCountry_MatlBal_Supersede_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPrimaryCompositionCountry_Prox_Amount_Cur] CHECK ([Prx_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPrimaryCompositionCountry_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingPrimaryCompositionCountry_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPrimaryCompositionCountry_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingPrimaryCompositionCountry_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingPrimaryCompositionCountry_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingPrimaryCompositionCountry_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingPrimaryCompositionCountry_Region_LookUp] FOREIGN KEY ([Proximity_RegionID]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingPrimaryCompositionCountry_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingPrimaryCompositionCountry_u
	ON inter.PricingPrimaryCompositionCountry
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingPrimaryCompositionCountry
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingPrimaryCompositionCountry.FactorSetId		= INSERTED.FactorSetId
		AND inter.PricingPrimaryCompositionCountry.CountryId		= INSERTED.CountryId
		AND inter.PricingPrimaryCompositionCountry.CalDateKey		= INSERTED.CalDateKey
		AND inter.PricingPrimaryCompositionCountry.CurrencyRpt		= INSERTED.CurrencyRpt
		AND inter.PricingPrimaryCompositionCountry.StreamId			= INSERTED.StreamId
		AND inter.PricingPrimaryCompositionCountry.ComponentId		= INSERTED.ComponentId;

END;