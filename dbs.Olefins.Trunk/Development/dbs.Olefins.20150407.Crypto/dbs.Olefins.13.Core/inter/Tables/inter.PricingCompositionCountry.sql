﻿CREATE TABLE [inter].[PricingCompositionCountry] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [PricingMethodID]       VARCHAR (42)       NOT NULL,
    [CountryId]             CHAR (3)           NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [ComponentId]           VARCHAR (42)       NOT NULL,
    [MatlBal_Cur]           REAL               NOT NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],isnull([MatlBal_Supersede_Cur],[MatlBal_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingCompositionCountry_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionCountry_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionCountry_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionCountry_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingCompositionCountry] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PricingMethodID] ASC, [CurrencyRpt] ASC, [CountryId] ASC, [StreamId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingCompositionCountry_MatlBal_Cur] CHECK ([MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingCompositionCountry_MatlBal_Supersedet_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingCompositionCountry_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingCompositionCountry_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingCompositionCountry_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingCompositionCountry_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingCompositionCountry_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingCompositionCountry_PricingMethod] FOREIGN KEY ([PricingMethodID]) REFERENCES [dim].[PricingMethodLu] ([MethodID]),
    CONSTRAINT [FK_PricingCompositionCountry_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO
CREATE NONCLUSTERED INDEX [IX_PricingCompositionCountry_PlantPricing]
    ON [inter].[PricingCompositionCountry]([StreamId] ASC, [ComponentId] ASC)
    INCLUDE([FactorSetId], [PricingMethodID], [CountryId], [CalDateKey], [CurrencyRpt], [_MatlBal_Amount_Cur]);


GO
CREATE NONCLUSTERED INDEX [IX_PricingCompositionCountry_PlantPricing_Other]
    ON [inter].[PricingCompositionCountry]([StreamId] ASC)
    INCLUDE([FactorSetId], [CountryId], [CalDateKey], [CurrencyRpt], [ComponentId], [MatlBal_Cur], [MatlBal_Supersede_Cur], [_MatlBal_Amount_Cur]);


GO

CREATE TRIGGER [inter].t_PricingCompositionCountry_u
	ON [inter].[PricingCompositionCountry]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [inter].[PricingCompositionCountry]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].[PricingCompositionCountry].FactorSetId		= INSERTED.FactorSetId
		AND [inter].[PricingCompositionCountry].CountryId		= INSERTED.CountryId
		AND [inter].[PricingCompositionCountry].CalDateKey		= INSERTED.CalDateKey
		AND [inter].[PricingCompositionCountry].ComponentId		= INSERTED.ComponentId
		AND [inter].[PricingCompositionCountry].CurrencyRpt		= INSERTED.CurrencyRpt;

END;