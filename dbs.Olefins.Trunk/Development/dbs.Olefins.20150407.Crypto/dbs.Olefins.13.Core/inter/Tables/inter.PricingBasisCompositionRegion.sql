﻿CREATE TABLE [inter].[PricingBasisCompositionRegion] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [RegionId]       VARCHAR (5)        NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [Raw_Amount_Cur] REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PricingBasisCompositionRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PricingBasisCompositionRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PricingBasisCompositionRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PricingBasisCompositionRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingBasisCompositionRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingBasisCompositionRegion_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingBasisCompositionRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingBasisCompositionRegion_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingBasisCompositionRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingBasisCompositionRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingBasisCompositionRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingBasisCompositionRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingBasisCompositionRegion_u
	ON inter.PricingBasisCompositionRegion
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingBasisCompositionRegion
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingBasisCompositionRegion.FactorSetId		= INSERTED.FactorSetId
		AND inter.PricingBasisCompositionRegion.RegionId		= INSERTED.RegionId
		AND inter.PricingBasisCompositionRegion.CalDateKey		= INSERTED.CalDateKey
		AND inter.PricingBasisCompositionRegion.CurrencyRpt		= INSERTED.CurrencyRpt
		AND inter.PricingBasisCompositionRegion.StreamId		= INSERTED.StreamId
		AND inter.PricingBasisCompositionRegion.ComponentId		= INSERTED.ComponentId;

END;