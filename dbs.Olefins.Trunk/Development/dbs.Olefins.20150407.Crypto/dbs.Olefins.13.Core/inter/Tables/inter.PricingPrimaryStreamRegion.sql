﻿CREATE TABLE [inter].[PricingPrimaryStreamRegion] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [RegionId]              VARCHAR (5)        NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [Raw_Amount_Cur]        REAL               NOT NULL,
    [Adj_MatlBal_Cur]       REAL               NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [Report_Supersede_Cur]  REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],coalesce([MatlBal_Supersede_Cur],[Raw_Amount_Cur]+coalesce([Adj_MatlBal_Cur],(0.0))),(1))) PERSISTED NOT NULL,
    [_Report_Amount_Cur]    AS                 (CONVERT([real],coalesce([Report_Supersede_Cur],[Raw_Amount_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPrimaryStreamRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryStreamRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryStreamRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryStreamRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingPrimaryStreamRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingPrimaryStreamRegion_MatlBal_Supersede_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPrimaryStreamRegion_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPrimaryStreamRegion_Report_Supersede_Cur] CHECK ([Report_Supersede_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingPrimaryStreamRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPrimaryStreamRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingPrimaryStreamRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingPrimaryStreamRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingPrimaryStreamRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingPrimaryStreamRegion_u
	ON inter.PricingPrimaryStreamRegion
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingPrimaryStreamRegion
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingPrimaryStreamRegion.FactorSetId	= INSERTED.FactorSetId
		AND inter.PricingPrimaryStreamRegion.RegionId		= INSERTED.RegionId
		AND inter.PricingPrimaryStreamRegion.CalDateKey		= INSERTED.CalDateKey
		AND inter.PricingPrimaryStreamRegion.StreamId		= INSERTED.StreamId
		AND inter.PricingPrimaryStreamRegion.CurrencyRpt	= INSERTED.CurrencyRpt;

END;