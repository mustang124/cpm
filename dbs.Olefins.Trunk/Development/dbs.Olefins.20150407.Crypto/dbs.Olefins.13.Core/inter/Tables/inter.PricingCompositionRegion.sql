﻿CREATE TABLE [inter].[PricingCompositionRegion] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [PricingMethodID]       VARCHAR (42)       NOT NULL,
    [RegionId]              VARCHAR (5)        NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [ComponentId]           VARCHAR (42)       NOT NULL,
    [MatlBal_Cur]           REAL               NOT NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],isnull([MatlBal_Supersede_Cur],[MatlBal_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingCompositionRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingCompositionRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PricingMethodID] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingCompositionRegion_MatlBal_Cur] CHECK ([MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingCompositionRegion_MatlBal_Supersedet_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingCompositionRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingCompositionRegion_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingCompositionRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingCompositionRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingCompositionRegion_PricingMethod] FOREIGN KEY ([PricingMethodID]) REFERENCES [dim].[PricingMethodLu] ([MethodID]),
    CONSTRAINT [FK_PricingCompositionRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingCompositionRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [inter].t_PricingCompositionRegion_u
	ON [inter].PricingCompositionRegion
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [inter].PricingCompositionRegion
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].PricingCompositionRegion.FactorSetId		= INSERTED.FactorSetId
		AND [inter].PricingCompositionRegion.RegionId			= INSERTED.RegionId
		AND [inter].PricingCompositionRegion.CalDateKey		= INSERTED.CalDateKey
		AND [inter].PricingCompositionRegion.ComponentId		= INSERTED.ComponentId
		AND [inter].PricingCompositionRegion.CurrencyRpt		= INSERTED.CurrencyRpt;

END;