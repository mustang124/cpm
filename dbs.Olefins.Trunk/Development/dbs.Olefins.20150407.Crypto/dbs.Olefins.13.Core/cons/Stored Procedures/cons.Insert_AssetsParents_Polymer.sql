﻿CREATE PROCEDURE [cons].[Insert_AssetsParents_Polymer]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO cons.AssetsParents(CalDateKey, AssetId, Parent, Operator, Hierarchy)
		SELECT DISTINCT
			etl.ConvDateKey(t.StudyYear),				
			etl.ConvAssetPCH(f.Refnum) + '-T' + CONVERT(VARCHAR(2), f.PlantId)	[AssetId],
			etl.ConvAssetPCH(f.Refnum)				[AssetParent],
			'+',
			'/'
		FROM stgFact.PolyFacilities			f
		INNER JOIN stgFact.TSort			t
			ON	t.Refnum = f.Refnum
		LEfT OUTER JOIN cons.AssetsParents ap
			ON	ap.CalDateKey = etl.ConvDateKey(t.StudyYear)
			AND	ap.AssetId = etl.ConvAssetPCH(f.Refnum) + '-T' + CONVERT(VARCHAR(2), f.PlantId)
			AND	ap.Parent = etl.ConvAssetPCH(f.Refnum)
		WHERE	f.Name <> '0'
			AND	ap.AssetId IS NULL
			AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;