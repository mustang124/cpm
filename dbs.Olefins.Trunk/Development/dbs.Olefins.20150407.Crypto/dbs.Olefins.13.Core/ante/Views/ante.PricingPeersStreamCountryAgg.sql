﻿CREATE VIEW ante.PricingPeersStreamCountryAgg
WITH SCHEMABINDING
AS
SELECT
	c.FactorSetId,
	c.CountryId,
	c.CalDateKey,
	'LogisticsTransportAdj'			[AccountId],
	c.CurrencyRpt,
	c.OverRide_RegionID,
	c.StreamId,
	SUM(c.Adj_Proximity_Cur)			[Adj_Proximity_Cur]
FROM ante.PricingPeersStreamCountry c
GROUP BY
	c.FactorSetId,
	c.CountryId,
	c.CalDateKey,
	c.CurrencyRpt,
	c.OverRide_RegionID,
	c.StreamId;
