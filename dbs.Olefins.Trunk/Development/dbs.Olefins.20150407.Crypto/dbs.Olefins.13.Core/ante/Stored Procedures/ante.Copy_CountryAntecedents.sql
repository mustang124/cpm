﻿CREATE PROCEDURE [ante].[Copy_CountryAntecedents]
(
	@FrFactorSetId	VARCHAR (12),
	@FrCountryId	CHAR (3),

	@ToFactorSetId	VARCHAR (12),
	@ToCountryId	CHAR (3)
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [ante].[ApiGhgEmissions](FactorSetId, CountryId, ComponentId, Emissions_MTMWHr)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.ComponentId,
		a.Emissions_MTMWHr
	FROM [ante].[ApiGhgEmissions]					a
	LEFT OUTER JOIN [ante].[ApiGhgEmissions]		n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
		AND	n.[ComponentId]	= a.[ComponentId]
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL
		AND	n.[ComponentId]	IS NULL;

	INSERT INTO [ante].[CountryEconRegion](FactorSetId, CountryId, EconRegionID)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.EconRegionID
	FROM [ante].[CountryEconRegion]					a
	LEFT OUTER JOIN [ante].[CountryEconRegion]		n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL;

	INSERT INTO [ante].[EmissionsFactorElectricity](FactorSetId, CountryId, EmissionsId, CEF_MTCO2_MBtu)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.EmissionsId,
		a.CEF_MTCO2_MBtu
	FROM [ante].[EmissionsFactorElectricity]				a
	LEFT OUTER JOIN [ante].[EmissionsFactorElectricity]		n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
		AND	n.[EmissionsId]	= a.[EmissionsId]
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL
		AND	n.[EmissionsId]	IS NULL;

	INSERT INTO [ante].[LocationFactor](FactorSetId, CountryId, LocationFactor)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.LocationFactor
	FROM [ante].[LocationFactor]				a
	LEFT OUTER JOIN [ante].[LocationFactor]		n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL;

	INSERT INTO [ante].[PricingAdjDutiesStreamCountry](FactorSetId, CountryId, CalDateKey, AccountId, CurrencyRpt, StreamId, Amount_Cur)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.CalDateKey,
		a.AccountId,
		a.CurrencyRpt,
		a.StreamId,
		a.Amount_Cur
	FROM [ante].[PricingAdjDutiesStreamCountry]				a
	LEFT OUTER JOIN [ante].[PricingAdjDutiesStreamCountry]	n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
		AND	n.[CalDateKey]	= a.[CalDateKey]
		AND	n.[AccountId]	= a.[AccountId]
		AND	n.[CurrencyRpt]	= a.[CurrencyRpt]
		AND	n.[StreamId]	= a.[StreamId]
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL
		AND	n.[CalDateKey]	IS NULL
		AND	n.[AccountId]	IS NULL
		AND	n.[CurrencyRpt]	IS NULL
		AND	n.[StreamId]	IS NULL;

	INSERT INTO [ante].[PricingMultiplierStreamCountry](FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, Amount_Cur)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.CalDateKey,
		a.CurrencyRpt,
		a.StreamId,
		a.Amount_Cur
	FROM [ante].[PricingMultiplierStreamCountry]	a
	LEFT OUTER JOIN [ante].[PricingMultiplierStreamCountry]	n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
		AND	n.[CalDateKey]	= a.[CalDateKey]
		AND	n.[CurrencyRpt]	= a.[CurrencyRpt]
		AND	n.[StreamId]	= a.[StreamId]
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL
		AND	n.[CalDateKey]	IS NULL
		AND	n.[CurrencyRpt]	IS NULL
		AND	n.[StreamId]	IS NULL;

	INSERT INTO [ante].[PricingPeersCompositionCountry](FactorSetId, CountryId, CalDateKey, CurrencyRpt, OverRide_RegionID, AccountId, StreamId, ComponentId, Adj_Proximity_Cur)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.CalDateKey,
		a.CurrencyRpt,
		a.OverRide_RegionID,
		a.AccountId,
		a.StreamId,
		a.ComponentId,
		a.Adj_Proximity_Cur
	FROM [ante].[PricingPeersCompositionCountry]	a
	LEFT OUTER JOIN [ante].[PricingPeersCompositionCountry]	n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
		AND	n.[CalDateKey]	= a.[CalDateKey]
		AND	n.[CurrencyRpt]	= a.[CurrencyRpt]
		AND	n.[OverRide_RegionID]	= a.[OverRide_RegionID]
		AND	n.[AccountId]	= a.[AccountId]
		AND	n.[StreamId]	= a.[StreamId]
		AND	n.[ComponentId]	= a.[ComponentId]
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL
		AND	n.[CalDateKey]	IS NULL
		AND	n.[CurrencyRpt]	IS NULL
		AND	n.[OverRide_RegionID]	IS NULL
		AND	n.[AccountId]	IS NULL
		AND	n.[StreamId]	IS NULL
		AND	n.[ComponentId]	IS NULL;

	INSERT INTO [ante].[PricingPeersStreamCountry](FactorSetId, CountryId, CalDateKey, CurrencyRpt, OverRide_RegionID, AccountId, StreamId, Adj_Proximity_Cur)
	SELECT
		@ToFactorSetId,
		@ToCountryId,
		a.CalDateKey,
		a.CurrencyRpt,
		a.OverRide_RegionID,
		a.AccountId,
		a.StreamId,
		a.Adj_Proximity_Cur
	FROM [ante].[PricingPeersStreamCountry]	a
	LEFT OUTER JOIN [ante].[PricingPeersStreamCountry]	n
		ON	n.[FactorSetId]	= @ToFactorSetId
		AND	n.[CountryId]	= @ToCountryId
		AND	n.[CalDateKey]	= a.[CalDateKey]
		AND	n.[CurrencyRpt]	= a.[CurrencyRpt]
		AND	n.[OverRide_RegionID]	= a.[OverRide_RegionID]
		AND	n.[AccountId]	= a.[AccountId]
		AND	n.[StreamId]	= a.[StreamId]
	WHERE	a.[FactorSetId] = @FrFactorSetId
		AND	a.[CountryId]	= @FrCountryId
		AND	n.[FactorSetId]	IS NULL
		AND	n.[CountryId]	IS NULL
		AND	n.[CalDateKey]	IS NULL
		AND	n.[CurrencyRpt]	IS NULL
		AND	n.[OverRide_RegionID]	IS NULL
		AND	n.[AccountId]	IS NULL
		AND	n.[StreamId]	IS NULL;

END;