﻿CREATE TABLE [ante].[SimFurnaceConfigSpsl] (
    [FactorSetId]              VARCHAR (12)       NOT NULL,
    [StreamId]                 VARCHAR (42)       NOT NULL,
    [FurnaceTypeId]            VARCHAR (12)       NOT NULL,
    [SeverityConversion]       TINYINT            NULL,
    [CoilInletTemp_Min_C]      REAL               NULL,
    [CoilInletTemp_Max_C]      REAL               NULL,
    [CoilOutletPressure_kgcm2] REAL               NULL,
    [CoilInletPressure_kgcm2]  REAL               NULL,
    [CoilOutletTemp_C]         REAL               NULL,
    [CoilInletTemp_C]          REAL               NULL,
    [RadiantWallTemp_C]        REAL               NULL,
    [FlowRate_KgHr]            REAL               NULL,
    [SteamHydrocarbon_Ratio]   REAL               NULL,
    [HydrogenCarbon_Ratio]     REAL               NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_SimFurnaceConfigSpsl_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceConfigSpsl_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceConfigSpsl_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_SimFurnaceConfigSpsl_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SimFurnaceConfigSpsl] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_CoilInletPressure_kgcm2] CHECK ([CoilInletPressure_kgcm2]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_CoilInletTemp_C] CHECK ([CoilInletTemp_C]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_CoilInletTemp_Max_C] CHECK ([CoilInletTemp_Max_C]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_CoilInletTemp_Min_C] CHECK ([CoilInletTemp_Min_C]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_CoilOutletPressure_kgcm2] CHECK ([CoilOutletPressure_kgcm2]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_CoilOutletTemp_C] CHECK ([CoilOutletTemp_C]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_FlowRate_kghr] CHECK ([FlowRate_KgHr]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_HydrogenCarbon_Ratio] CHECK ([HydrogenCarbon_Ratio]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_RadiantWallTemp_C] CHECK ([RadiantWallTemp_C]>=(0.0)),
    CONSTRAINT [CR_SimFurnaceConfigSpsl_SteamHydrocarbon_Ratio] CHECK ([SteamHydrocarbon_Ratio]>=(0.0)),
    CONSTRAINT [CV_SimFurnaceConfigSpsl_CoilInletTemp] CHECK ([CoilInletTemp_Min_C]<=[CoilInletTemp_Max_C]),
    CONSTRAINT [FK_SimFurnaceConfigSpsl_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_SimFurnaceConfigSpsl_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_SimFurnaceConfigSpsl_u]
	ON [ante].[SimFurnaceConfigSpsl]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[SimFurnaceConfigSpsl]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[SimFurnaceConfigSpsl].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[SimFurnaceConfigSpsl].StreamId		= INSERTED.StreamId;

END;