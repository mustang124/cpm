﻿CREATE TABLE [ante].[PeerGroupFeedClass] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [PeerGroup]      TINYINT            NOT NULL,
    [LimitLower_SG]  REAL               NOT NULL,
    [LimitUpper_SG]  REAL               NOT NULL,
    [_LimitRange]    AS                 (((('['+CONVERT([varchar](32),round([LimitLower_SG],(2)),(0)))+', ')+CONVERT([varchar](32),round([LimitUpper_SG],(2)),(0)))+')') PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupFeedClass_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PeerGroupFeedClass] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PeerGroup] ASC),
    CONSTRAINT [CR_PeerGroupFeedClass_FeedClass] CHECK ([PeerGroup]=(5) OR [PeerGroup]=(4) OR [PeerGroup]=(3) OR [PeerGroup]=(2) OR [PeerGroup]=(1)),
    CONSTRAINT [FK_PeerGroupFeedClass_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_PeerGroupFeedClass_u]
	ON [ante].[PeerGroupFeedClass]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PeerGroupFeedClass]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PeerGroupFeedClass].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[PeerGroupFeedClass].PeerGroup	= INSERTED.PeerGroup;

END;