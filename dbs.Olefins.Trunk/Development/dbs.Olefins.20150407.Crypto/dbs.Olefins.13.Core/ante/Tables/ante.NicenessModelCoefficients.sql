﻿CREATE TABLE [ante].[NicenessModelCoefficients] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [Yield]          REAL               NOT NULL,
    [Energy]         REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessModelCoefficients_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_NicenessModelCoefficients_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_NicenessModelCoefficients_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_NicenessModelCoefficients_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_NicenessModelCoefficients] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CONSTRAINT [FK_NicenessModelCoefficients_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_NicenessModelCoefficients_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_NicenessModelCoefficients_u]
	ON [ante].[NicenessModelCoefficients]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessModelCoefficients]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[NicenessModelCoefficients].FactorSetId		= INSERTED.FactorSetId
		AND	[ante].[NicenessModelCoefficients].StreamId			= INSERTED.StreamId;

END;
