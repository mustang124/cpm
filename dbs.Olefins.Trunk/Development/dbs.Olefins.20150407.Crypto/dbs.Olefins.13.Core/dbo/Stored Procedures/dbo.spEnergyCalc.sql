﻿






CREATE              PROC [dbo].[spEnergyCalc](@Refnum varchar(25), @FactorSetId varchar(12))
AS
SET NOCOUNT ON

DELETE FROM dbo.EnergyCalc WHERE Refnum = @Refnum
Insert into dbo.EnergyCalc (Refnum
, Currency
, DataType
, AccountId
, AccountName
, EnergyParent
, Stream_Value_Cur
, Amount_MBtu
, Amount_GJ	
, CostPerMBtu
, CostPerGJ	
, CostkWh
, DivisorValue)
SELECT dt.Refnum
, ISNULL(ep.CurrencyRpt, 'USD')  -- only seems to be a null where there is not a price.
, dt.DataType
, ep.AccountId
, l.AccountName
, p.ParentId
, ep.Stream_Value_Cur
--, Amount_MBtu = ep.LHValue_MBtu/CASE WHEN dt.DataType in ('UEDC','EDC') THEN dt.DivisorValue*1000.0 ELSE dt.DivisorValue END
--, Amount_GJ =  ep.LHValue_GJ/CASE WHEN dt.DataType in ('UEDC','EDC') THEN dt.DivisorValue*1000.0 ELSE dt.DivisorValue END
, Amount_MBtu = ep.LHValue_MBtu/(CASE DataType WHEN 'UEDC' THEN 1000.0 WHEN 'EDC' then 1000.0 WHEN 'EII' THEN 365.0/100.0 ELSE 1.0 END * dt.DivisorValue)
, Amount_GJ =  ep.LHValue_GJ/(CASE DataType WHEN 'UEDC' THEN 1000.0 WHEN 'EDC' then 1000.0 WHEN 'EII' THEN 365.0/100.0 ELSE 1.0 END * dt.DivisorValue)
, CostPerMBtu = CASE 
            WHEN ep.AccountId in ('PurElec','ExportElectric') THEN NULL 
            WHEN ep.AccountId like 'PPFC%' THEN ep.Stream_Value_Cur * 1000 / ep.LHValue_MBtu
            ELSE ep.Amount_Cur 
      END
, CostPerGJ = CASE WHEN ep.AccountId in ('PurElec','ExportElectric') THEN NULL 
            WHEN ep.AccountId like 'PPFC%' THEN ep.Stream_Value_Cur * 1000 / ep.LHValue_GJ
            ELSE [$(DbGlobal)].dbo.UnitsConv(ep.Amount_Cur, '1/MBtu','1/GJ') 
      END
, CostkWh = CASE WHEN ep.AccountId in ('PurElec','ExportElectric') THEN ep.Amount_Cur ELSE NULL END
, DivisorValue = CASE DataType 
				WHEN 'UEDC' THEN 1000.0 
				WHEN 'EDC' then 1000.0 
				WHEN 'EII' THEN 365.0/100.0 
				ELSE 1.0 
				END * dt.DivisorValue
FROM (Select Refnum, 
			DataType = CASE d.DataType
			WHEN 'HVC_MT'		THEN	'kBtuPerHVCMt'
			WHEN 'HVC_LB'		THEN	'BtuPerHVCLb'
			WHEN 'ETHDiv_MT'	THEN	'kBtuPerETHMt'
			WHEN 'OLEDiv_MT'	THEN	'kBtuPerOLEMt'
			WHEN 'ADJ'			THEN	'MBtu'
			ELSE d.DataType
			END,
			DivisorValue FROM dbo.DivisorsUnPivot d where Refnum = @Refnum 
		 AND d.DataType in ('UEDC','EDC','HVC_MT','HVC_LB','ETHDiv_MT','ADJ','OLEDiv_MT','EII')) dt
INNER JOIN (
            SELECT e.Refnum, pps.FactorSetId, p.CurrencyRpt, e.AccountId, e.LHValue_MBtu, e.LHValue_GJ, p.Amount_Cur, pps.Stream_Value_Cur
            FROM fact.EnergyLHValueAggregate e LEFT OUTER JOIN fact.EnergyPrice p on p.Refnum=e.Refnum and p.AccountId=e.AccountId
            LEFT OUTER JOIN calc.PricingPlantStreamAggregate pps on pps.Refnum=e.Refnum and e.AccountId=pps.StreamId and pps.FactorSetId=e.FactorSetId
            WHERE e.LHValue_MBtu IS NOT NULL and e.FactorSetId = @FactorSetId
            ) ep ON ep.Refnum = dt.Refnum and ISNULL(ep.FactorSetId, @FactorSetId) = @FactorSetId  -- This is really strange, but if not a PPF, then there is not a factorsetID
INNER JOIN dim.Account_LookUp l on l.AccountId = ep.AccountId
INNER JOIN dim.AccountEnergy_Parent p on p.AccountId=l.AccountId and p.FactorSetId = @FactorSetId
and dt.Refnum = @Refnum
SET NOCOUNT OFF



















