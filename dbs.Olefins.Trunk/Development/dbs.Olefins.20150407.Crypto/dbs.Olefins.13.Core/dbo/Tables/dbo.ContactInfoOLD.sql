﻿CREATE TABLE [dbo].[ContactInfoOLD] (
    [Refnum]              VARCHAR (11) NOT NULL,
    [CoordName]           VARCHAR (40) NULL,
    [CoordTitle]          VARCHAR (75) NULL,
    [POBox]               VARCHAR (35) NULL,
    [Street]              VARCHAR (60) NULL,
    [City]                VARCHAR (40) NULL,
    [State]               VARCHAR (25) NULL,
    [Country]             VARCHAR (30) NULL,
    [Zip]                 VARCHAR (20) NULL,
    [Telephone]           VARCHAR (30) NULL,
    [Fax]                 VARCHAR (30) NULL,
    [WWW]                 VARCHAR (65) NULL,
    [PricingContact]      VARCHAR (40) NULL,
    [PricingContactEmail] VARCHAR (65) NULL,
    [DCContact]           VARCHAR (40) NULL,
    [DCContactEmail]      VARCHAR (65) NULL
);

