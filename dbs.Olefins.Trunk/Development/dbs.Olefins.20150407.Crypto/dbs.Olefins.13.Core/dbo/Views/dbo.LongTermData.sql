﻿
CREATE view [dbo].[LongTermData] as
Select Refnum = CASE WHEN StudyYear < 2000 THEN '19' ELSE '20' END + t.Refnum
, t.CoLoc
, t.StudyYear,
UnadjC2Util,
TAAdjC2Util,
TAAdjC2C3Util,
UnAdjC2C3Util,
ePYPS.BTULbHVChem,
ePYPS.BTUuEDC,
HVChemTEPmt,
EDCTotWHr,
HVChemTotWHrmt,
PersCostTA_MTHVC,
PersCostTA_kEDC,
MaintCostIndex,
MaintCostEDC,
NewMaintEffInd,
NewMaintEffEDC,
ReliabInd,
yPYPS.PyroC2YPcnt,
yPYPS.PyroHVCYPcnt,
yPYPS.YIRTPSC2,
yPYPS.YIRTPSHVC,
yPYPS.YIRTHVC,
yPYPS.YIRTC2,
yPYPS.SIC2,
yPYPS.PValPcntOS,
PValPcntMSPYPS = yPYPS.PValPcntMS,
PValPcntMSSPSL = ySPSL.PValPcntMS,
PValPcntOSPPYPS = yPYPS.PValPcntOSP,
PValPcntOSPSPSL = ySPSL.PValPcntOSP,
HVChemCash,
UEDCCash,
EDCNEOpex,
TotHVChemExp,
MfgC2,
MfgC2C3,
MfgHVC,
MargnNetC2,
MargnNetC2C3,
MargnNetHVC,
GrossROI = Gross,
NetROI = Net,
TAAdjNet,
HVChemNEOpex,
yPYPS.Loss_PctTotalFreshFeed,
TotalLoss_PctFreshFeed = CASE WHEN qTotProd.AnnFeedProd = 0 then 0 ELSE qSTLoss.AnnFeedProd/qTotProd.AnnFeedProd*100 end,
HVChemDiv, EthylCapKMTA, TotReplVal,
NewEEIHVChemPYPS = ePYPS.NewEEIHVChem,
NewEEIHVChemSPSL = eSPSL.NewEEIHVChem ,
EII,
kEDC,
kUEDC, 
StdEnergy
From [$(DbOlefinsLegacy)].dbo.CapUtil c
INNER JOIN [$(DbOlefinsLegacy)].dbo.TSort t on t.Refnum=c.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.Production p on p.Refnum=t.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.Capacity cap on cap.Refnum=t.Refnum
 
INNER JOIN [$(DbOlefinsLegacy)].dbo.EEI ePYPS on ePYPS.Refnum=t.Refnum AND ePYPS.Model = 'PYPS'
left outer JOIN [$(DbOlefinsLegacy)].dbo.EEI eSPSL on eSPSL.Refnum=t.Refnum AND eSPSL.Model = 'SPSL'
 
left outer JOIN [$(DbOlefinsLegacy)].dbo.EII on EII.Refnum=t.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.Productivity pd on pd.Refnum=t.Refnum
left outer JOIN [$(DbOlefinsLegacy)].dbo.PersCost pc on pc.Refnum=t.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.MaintInd m on m.Refnum=t.Refnum
 
INNER JOIN [$(DbOlefinsLegacy)].dbo.Yield yPYPS on yPYPS.Refnum=t.Refnum AND yPYPS.Model = 'PYPS'
left outer JOIN [$(DbOlefinsLegacy)].dbo.Yield ySPSL on ySPSL.Refnum=t.Refnum AND ySPSL.Model = 'SPSL'
 
INNER JOIN [$(DbOlefinsLegacy)].dbo.UnitOpex u on u.Refnum=t.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.ROI r on r.Refnum=t.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.Replacement rep on rep.Refnum=t.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.QuantitySTCalc qSTLoss on qSTLoss.Refnum=t.Refnum AND qSTLoss.Category = 'Loss'
INNER JOIN [$(DbOlefinsLegacy)].dbo.QuantityTotCalc qTotProd on qTotProd.Refnum=t.Refnum AND qTotProd.[Type] = 'Prod'
left outer JOIN [$(DbOlefinsLegacy)].dbo.EDC ON EDC.Refnum = t.Refnum
Where t.StudyYear < 2007



 
 








