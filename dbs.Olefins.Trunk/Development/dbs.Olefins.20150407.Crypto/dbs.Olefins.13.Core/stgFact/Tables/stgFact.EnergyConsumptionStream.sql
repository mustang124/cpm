﻿CREATE TABLE [stgFact].[EnergyConsumptionStream] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [SimModelId]       VARCHAR (12)       NOT NULL,
    [OpCondId]         VARCHAR (12)       NOT NULL,
    [StreamId]         VARCHAR (42)       NOT NULL,
    [SimEnergy_kCalkg] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_EnergyCompositionStream_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyCompositionStream_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyCompositionStream_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyCompositionStream_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_EnergyCompositionStream] PRIMARY KEY CLUSTERED ([Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC),
    CONSTRAINT [FK_stgFact_EnergyConsumptionStream_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_stgFact_EnergyConsumptionStream_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId])
);


GO

CREATE TRIGGER [stgFact].[t_EnergyConsumptionStream_u]
	ON [stgFact].[EnergyConsumptionStream]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[EnergyConsumptionStream]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[EnergyConsumptionStream].Refnum		= INSERTED.Refnum
		AND	[stgFact].[EnergyConsumptionStream].SimModelId	= INSERTED.SimModelId
		AND	[stgFact].[EnergyConsumptionStream].OpCondId	= INSERTED.OpCondId
		AND	[stgFact].[EnergyConsumptionStream].StreamId	= INSERTED.StreamId;

END;