﻿CREATE TABLE [stgFact].[PracTA] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CGC]            CHAR (1)           NULL,
    [RC]             CHAR (1)           NULL,
    [TLE]            CHAR (1)           NULL,
    [OHX]            CHAR (1)           NULL,
    [Tower]          CHAR (1)           NULL,
    [Furnace]        CHAR (1)           NULL,
    [Util]           CHAR (1)           NULL,
    [CapProj]        CHAR (1)           NULL,
    [Other]          CHAR (1)           NULL,
    [OthCPMDesc]     VARCHAR (75)       NULL,
    [TimeFact]       CHAR (1)           NULL,
    [OthTFDesc]      VARCHAR (75)       NULL,
    [TAPrepTime]     REAL               NULL,
    [TaStartUpTime]  REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PracTA_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracTA_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracTA_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracTA_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PracTA] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PracTA_u]
	ON [stgFact].[PracTA]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PracTA]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PracTA].Refnum		= INSERTED.Refnum;

END;