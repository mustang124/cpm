﻿CREATE TABLE [stgFact].[PracRely] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [RBICurPcnt]     REAL               NULL,
    [RBIFutPcnt]     REAL               NULL,
    [RBIMethPcnt]    REAL               NULL,
    [CompPhil]       CHAR (1)           NULL,
    [RTCGComp]       REAL               NULL,
    [RTCGTurb]       REAL               NULL,
    [RTC3Comp]       REAL               NULL,
    [RTC3Turb]       REAL               NULL,
    [RTC2Comp]       REAL               NULL,
    [RTC2Turb]       REAL               NULL,
    [FreqVib]        REAL               NULL,
    [FreqComp]       REAL               NULL,
    [FreqTurb]       REAL               NULL,
    [FreqStat]       REAL               NULL,
    [CompLife]       INT                NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PracRely_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracRely_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracRely_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracRely_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PracRely] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PracRely_u]
	ON [stgFact].[PracRely]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PracRely]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PracRely].Refnum		= INSERTED.Refnum;

END;