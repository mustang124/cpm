﻿CREATE TABLE [stgFact].[PolyFacilities] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [PlantId]         TINYINT            NOT NULL,
    [Name]            NVARCHAR (50)      NULL,
    [City]            NVARCHAR (40)      NULL,
    [State]           NVARCHAR (25)      NULL,
    [Country]         NVARCHAR (30)      NULL,
    [CapMTD]          REAL               NULL,
    [CapKMTA]         REAL               NULL,
    [ReactorTrainCnt] INT                NULL,
    [ReactorStartup]  SMALLINT           NULL,
    [Product]         VARCHAR (20)       NULL,
    [Technology]      VARCHAR (20)       NULL,
    [ReactorResinPct] REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PolyFacilities_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyFacilities_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyFacilities_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyFacilities_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PolyFacilities] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PlantId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PolyFacilities_u]
	ON [stgFact].[PolyFacilities]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PolyFacilities]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PolyFacilities].Refnum		= INSERTED.Refnum
		AND	[stgFact].[PolyFacilities].PlantId		= INSERTED.PlantId;

END;