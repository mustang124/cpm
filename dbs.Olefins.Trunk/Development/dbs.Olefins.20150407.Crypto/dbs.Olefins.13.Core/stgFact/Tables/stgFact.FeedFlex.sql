﻿CREATE TABLE [stgFact].[FeedFlex] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdID]     VARCHAR (20)       NOT NULL,
    [PlantCap]       REAL               NULL,
    [ActCap]         REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_FeedFlex_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedFlex_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedFlex_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedFlex_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_FeedFlex] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_FeedFlex_u]
	ON [stgFact].[FeedFlex]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[FeedFlex]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[FeedFlex].Refnum			= INSERTED.Refnum
		AND	[stgFact].[FeedFlex].FeedProdID		= INSERTED.FeedProdID;

END;