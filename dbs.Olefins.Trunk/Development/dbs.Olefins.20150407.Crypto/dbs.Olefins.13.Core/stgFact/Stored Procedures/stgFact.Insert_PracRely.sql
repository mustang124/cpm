﻿CREATE PROCEDURE [stgFact].[Insert_PracRely]
(
	@Refnum			VARCHAR (25),

	@RBICurPcnt		REAL		= NULL,
	@RBIFutPcnt		REAL		= NULL,
	@RBIMethPcnt	REAL		= NULL,
	@CompPhil		CHAR (1)	= NULL,
	@RTCGComp		REAL		= NULL,
	@RTCGTurb		REAL		= NULL,
	@RTC3Comp		REAL		= NULL,
	@RTC3Turb		REAL		= NULL,
	@RTC2Comp		REAL		= NULL,
	@RTC2Turb		REAL		= NULL,
	@FreqVib		REAL		= NULL,
	@FreqComp		REAL		= NULL,
	@FreqTurb		REAL		= NULL,
	@FreqStat		REAL		= NULL,
	@CompLife		INT			= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PracRely]([Refnum], [RBICurPcnt], [RBIFutPcnt], [RBIMethPcnt], [CompPhil], [RTCGComp], [RTCGTurb], [RTC3Comp], [RTC3Turb], [RTC2Comp], [RTC2Turb], [FreqVib], [FreqComp], [FreqTurb], [FreqStat], [CompLife])
	VALUES(@Refnum, @RBICurPcnt, @RBIFutPcnt, @RBIMethPcnt, @CompPhil, @RTCGComp, @RTCGTurb, @RTC3Comp, @RTC3Turb, @RTC2Comp, @RTC2Turb, @FreqVib, @FreqComp, @FreqTurb, @FreqStat, @CompLife);

END;
