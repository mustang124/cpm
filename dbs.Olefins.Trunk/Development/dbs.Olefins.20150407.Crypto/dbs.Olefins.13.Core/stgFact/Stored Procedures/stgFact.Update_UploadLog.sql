﻿CREATE PROCEDURE [stgFact].[Update_UploadLog]
(
	@Refnum				VARCHAR (25),
	@FileType			VARCHAR (20),
	@FilePath			VARCHAR (512),
	@TimeBeg			DATETIMEOFFSET(7),

	@TimeEnd			DATETIMEOFFSET(7),
	@Error_Count		INT,
	@Msg				VARCHAR (MAX)
)
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[UploadLog]
	SET	[Archive] = 1
	WHERE	[Refnum]	= @Refnum
		AND	[FileType]	= @FileType
		AND	[FilePath]	= @FilePath
		AND	[TimeEnd]	IS NOT NULL
		AND	[Msg]		IS NOT NULL
		AND	[Archive]	= 0;

	UPDATE [stgFact].[UploadLog]
	SET	[TimeEnd]		= @TimeEnd,
		[Error_Count]	= @Error_Count,
		[Msg]			= @Msg
	WHERE	[Refnum]	= @Refnum
		AND	[FileType]	= @FileType
		AND	[FilePath]	= @FilePath
		AND	[TimeBeg]	= @TimeBeg
		AND	[Archive]	= 0;

END;