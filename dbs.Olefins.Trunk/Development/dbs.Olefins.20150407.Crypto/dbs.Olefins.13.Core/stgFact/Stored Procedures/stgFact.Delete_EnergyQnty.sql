﻿CREATE PROCEDURE [stgFact].[Delete_EnergyQnty]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[EnergyQnty]
	WHERE [Refnum] = @Refnum;

END;