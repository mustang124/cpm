﻿CREATE PROCEDURE [stgFact].[Insert_CapitalExp]
(
	@Refnum			VARCHAR (25),
	@CptlCode		VARCHAR (3),

	@Onsite			REAL	= NULL,
	@Offsite		REAL	= NULL,
	@Exist			REAL	= NULL,
	@EnergyCons		REAL	= NULL,
	@Envir			REAL	= NULL,
	@Safety			REAL	= NULL,
	@Computer		REAL	= NULL,
	@Maint			REAL	= NULL,
	@Total			REAL	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[CapitalExp]([Refnum], [CptlCode], [Onsite], [Offsite], [Exist], [EnergyCons], [Envir], [Safety], [Computer], [Maint], [Total])
	VALUES(@Refnum, @CptlCode, @Onsite, @Offsite, @Exist, @EnergyCons, @Envir, @Safety, @Computer, @Maint, @Total);

END