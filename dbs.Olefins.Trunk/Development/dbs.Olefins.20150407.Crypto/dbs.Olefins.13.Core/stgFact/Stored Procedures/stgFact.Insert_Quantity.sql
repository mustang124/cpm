﻿CREATE PROCEDURE [stgFact].[Insert_Quantity]
(
	@Refnum      VARCHAR (25),
	@FeedProdID  VARCHAR (20),

	@Q1Feed      REAL         = NULL,
	@Q2Feed      REAL         = NULL,
	@Q3Feed      REAL         = NULL,
	@Q4Feed      REAL         = NULL,
	@AnnFeedProd REAL         = NULL,
	@RecPcnt     REAL         = NULL,
	@MiscFeed    VARCHAR (35) = NULL,
	@OthProdDesc VARCHAR (35) = NULL,
	@MiscProd1   VARCHAR (50) = NULL,
	@MiscProd2   VARCHAR (50) = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Quantity]([Refnum], [FeedProdID], [Q1Feed], [Q2Feed], [Q3Feed], [Q4Feed], [AnnFeedProd], [RecPcnt], [MiscFeed], [OthProdDesc], [MiscProd1], [MiscProd2])
	VALUES(@Refnum, @FeedProdID, @Q1Feed, @Q2Feed, @Q3Feed, @Q4Feed, @AnnFeedProd, @RecPcnt, @MiscFeed, @OthProdDesc, @MiscProd1, @MiscProd2);

END;