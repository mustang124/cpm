﻿
CREATE FUNCTION etl.ConvCompanyID
(
	@CompanyId	VARCHAR(25),
	@CoName		VARCHAR(100),
	@Co			VARCHAR(40)
)
RETURNS VARCHAR(42)
WITH SCHEMABINDING
BEGIN
	
	DECLARE @ResultVar VARCHAR(42) =
	CASE @CompanyId
		WHEN 'ATOFINA'							THEN 'Atofina'
		WHEN 'BASELL' THEN
			CASE @CoName
			WHEN 'BASELL'						THEN 'Basell'
			WHEN 'Basell Olefine GmbH'			THEN 'Basell'
			WHEN 'Elenac'						THEN 'Elenac'
			WHEN 'LyondellBasell'				THEN 'LyondellBasell'
			END
		WHEN 'BASF'								THEN 'BASF'
		WHEN 'BASF FINA'						THEN 'BASF Fina'
		WHEN 'BASF_ATOFINA'						THEN 'BASF Fina'
		WHEN 'BASF_FINA'						THEN 'BASF Fina'
		WHEN 'BASF-YPC'							THEN 'BASF YPC'
		WHEN 'BASF YPC'							THEN 'BASF YPC'
		WHEN 'BOREALIS'							THEN 'Borealis'
		WHEN 'BP' THEN
			CASE @CoName
			WHEN 'BP AMOCO'						THEN 'BP AMOCO'
			WHEN 'BP Grangemouth'				THEN 'BP Grangemouth'
			WHEN 'BP Koeln GmbH'				THEN 'BP Koeln'
			WHEN 'BP Köln Krackbetriebe'		THEN 'BP Koeln'
			WHEN 'BP'							THEN 'BP'
			WHEN 'RUHR OEL GmbH'				THEN 'RuhrOel'
			END
		WHEN 'BOROUGE'							THEN 'Borouge'
		WHEN 'BRASKEM'							THEN 'Braskem'
		WHEN 'CARMEL'							THEN 'Carmel'
		WHEN 'CHEVRON PHILLIPS'					THEN 'CPChem'
		WHEN 'CHEVRONPHILL'						THEN 'CPChem'
		WHEN 'COPENE'							THEN 'Copene'
		WHEN 'COPESUL'							THEN 'Copesul'
		WHEN 'CPC'								THEN 'CPC'
		WHEN 'CSPC'								THEN 'CSPC'
		WHEN 'DEA'								THEN 'ShellDEA'
		WHEN 'DOW' THEN
			CASE @CoName
			WHEN ' DOW Chemical'				THEN 'Dow'
			WHEN 'DOW'							THEN 'Dow'
			WHEN 'DOW Chemical'					THEN 'Dow'
			WHEN 'DOW CHEMICAL CANADA INC.'		THEN 'Dow Canada'
			END
		WHEN 'DUPONT'							THEN 'du Pont'
		WHEN 'EQUATE'							THEN 'Equate'
		WHEN 'EQUISTAR'							THEN 'Equistar'
		WHEN 'ETHMALAYSIA'						THEN 'EthMalaysia'
		WHEN 'EXXONMOBIL' THEN
			CASE @CoName
			WHEN 'EXXONMOBIL'							THEN 'ExxonMobil'
			WHEN 'EXXONMOBIL  CHEMICAL LTD'				THEN 'ExxonMobil'
			WHEN 'ExxonMobil Chem Op Pvt Lmt'			THEN 'ExxonMobil ChemOpPvtLmt'
			WHEN 'EXXONMOBIL CHEMICAL COMPANY'			THEN 'ExxonMobil'
			WHEN 'ExxonMobil Chemical Op PrvtLmt'		THEN 'ExxonMobil ChemOpPvtLmt'
			WHEN 'ExxonMobil Singapore'					THEN 'ExxonMobil SGP'
			WHEN 'EXXONMOBILASIA'						THEN 'ExxonMobil Asia'
			ELSE
				CASE @Co
				WHEN 'EXXONMOBIL'						THEN 'ExxonMobil'
				END
			END
		WHEN 'FAO'								THEN 'FAO'
		WHEN 'FHR'								THEN 'FlintHillsResources'
		WHEN 'FLINT HILLS'						THEN 'FlintHillsResources'
		WHEN 'FORMOSA'							THEN 'Formosa'
		WHEN 'FREP'								THEN 'FREP'
		WHEN 'GAIL'								THEN 'GAIL'
		WHEN 'HUNTSMAN'							THEN 'Huntsman'
		WHEN 'IDEMITSU'							THEN 'Idemitsu'
		WHEN 'IOCL'								THEN 'IOCL'
		WHEN 'IRPC'								THEN 'IRPC'
		WHEN 'LG'								THEN 'LGChem'
		WHEN 'LUKOIL'							THEN 'Lukoil'
		WHEN 'KNH'								THEN 'KNH'
		WHEN 'LYONDELL'							THEN 'Lyondell'
		WHEN 'LYONDELLBASELL'					THEN 'LyondellBasell'
		WHEN 'Maruzen'							THEN 'Maruzen'
		WHEN 'MITSUBISHI'						THEN 'Mitsubishi'
		WHEN 'MIZUSHIMA'						THEN 'Mitsubishi'
		WHEN 'MOC'								THEN 'MOC'
		WHEN 'NAPHTACHIMIE'						THEN 'Naphtachimie'
		WHEN 'NIPPON'							THEN 'Nippon'
		WHEN 'NORETYL'							THEN 'Noretyl'
		WHEN 'NOVA'								THEN 'Nova'
		WHEN 'NPC'								THEN 'NPC'
		WHEN 'OMV'								THEN 'OMV'
		WHEN 'OPTIMAL OLEFINS'					THEN 'Optimal'
		WHEN 'OSAKA'							THEN 'Osaka'
		WHEN 'PCS'								THEN 'PCS'
		WHEN 'PETROCHINA' THEN
			CASE @CoName
			WHEN 'JILIN  CHEMICAL'				THEN 'CNPC Jilin'
			END
		WHEN 'PETRONAS'							THEN 'Petronas'
		WHEN 'PEMEX'							THEN 'Pemex'
		WHEN 'PETKIM'							THEN 'Petkim'
		WHEN 'PETRO RABIGH'						THEN 'PetroRabigh'
		WHEN 'PETROMONT'						THEN 'Petromont'
		WHEN 'PKN' THEN								
			CASE @CoName
			WHEN 'PKN'							THEN 'PKN'
			WHEN 'PKN Orlen'					THEN 'PKN'
			WHEN 'UNIPETROL'					THEN 'PKN Unipetrol'
			END
		WHEN 'POLIMERI'							THEN 'Polimeri'
		WHEN 'PQU'								THEN 'PqU'
		WHEN 'PTT'								THEN 'PTT'
		WHEN 'QUATTOR'							THEN 'Quattor'
		WHEN 'RAYONG'							THEN 'Rayong'
		WHEN 'RELIANCE'							THEN 'Reliance'
		WHEN 'REPSOL'							THEN 'Repsol'
		WHEN 'RIOPOL'							THEN 'RioPol'
		WHEN 'RLOC'								THEN 'RLOC'
		WHEN 'ROC'								THEN 'ROC'
		WHEN 'RUHROEL'							THEN 'RuhrOel'
		WHEN 'SABIC' THEN
			CASE @CoName
			WHEN 'Al Jubail Petrochemical Company'		THEN 'SabicAJ'
			WHEN 'Jubail United Petrochemical Company'	THEN 'SabicUnited'
			WHEN 'PETROKEMYA'							THEN 'SabicPetrokemya'
			WHEN 'SABIC' THEN
				CASE @Co
				WHEN 'SABIC'								THEN 'SABIC'
				WHEN 'SABIC EUR'							THEN 'SabicEur'
				END 
			WHEN 'SABIC EUR'							THEN 'SabicEur'
			WHEN 'SABIC-Europe'							THEN 'SabicEur'
			WHEN 'SABIC-PETROKEMYA'						THEN 'SabicPetrokemya'
			WHEN 'SABIC, PETROKEMYA'					THEN 'SabicPetrokemya'
			WHEN 'Kemya Olefins Plant'					THEN 'Sabickemya'
			WHEN 'PETROKEMYA'							THEN 'SabicPetrokemya'
			ELSE
				CASE @Co
				WHEN 'SABIC'								THEN 'SABIC'
				END
			END
		WHEN 'SABIC EUR'						THEN 'SabicEur'
		WHEN 'SADAF'							THEN 'SabicSadaf'
		
		WHEN 'SAMSUNG'							THEN 'Samsung'
		WHEN 'SASOL' THEN
			CASE @CoName
			WHEN 'Sasol NA'							THEN 'Sasol NA'
			WHEN 'SASOL POLYMERS'					THEN 'Sasol Pol'
			END
		WHEN 'SASOL POLYMERS'					THEN 'Sasol Pol'
		WHEN 'SECCO'							THEN 'SECCO'
		WHEN 'SHELL' THEN
			CASE @CoName
			WHEN 'Basell Olefine GmbH'				THEN 'Basell'
			WHEN 'SHELL'							THEN 'Shell'
			WHEN 'SHELL CHEMICALS'					THEN 'Shell'
			WHEN 'Shell Deutschland Oil GmbH'		THEN 'Shell Deutschland'
			WHEN 'SHELL INTERNATIONAL'				THEN 'Shell Intl'
			END
		WHEN 'SHOWADENKO'						THEN 'ShowaDenko'
		WHEN 'SIBUR'							THEN 'Sibur'
		WHEN 'SINOPEC' THEN
			CASE @CoName
			WHEN 'SINOPEC'							THEN 'Sinopec'
			WHEN 'SHANGHAI PETROCHEMICALS'			THEN 'Sinopec'
			WHEN 'MPCC Ethylene Industry Company'	THEN 'Sinopec Maoming'
			WHEN 'SINOPEC (YPC)'					THEN 'Sinopec YPC'
			ELSE
				CASE @Co WHEN 'SINOPEC'				THEN 'Sinopec'	END
			END
		WHEN 'SK'								THEN 'SK'
		WHEN 'TASNEE'							THEN 'Tasnee'
		WHEN 'TITANGROUP'						THEN 'Titan'
		WHEN 'TOTAL'							THEN 'Total'
		WHEN 'TOC'								THEN 'TOC'
		WHEN 'WESTLAKE'							THEN 'Westlake'
		WHEN 'WILLIAMS'							THEN 'Williams'
		WHEN 'TONEN'							THEN 'ExxonMobil Tonen'
		WHEN 'YNCC'								THEN 'YNCC'
		
		WHEN 'SOLOMON'							THEN 'Solomon'
		WHEN 'TEST'								THEN 'Solomon'
	ELSE
		CASE @CoName
			WHEN 'ALBERTA GAS ETHYLENE'					THEN 'AlbGas'
			WHEN 'ATOCHEM'								THEN 'Atochem'
			WHEN 'ATOFINA'								THEN 'Atofina'
			WHEN 'BASF'									THEN 'BASF'
			WHEN 'BOREALIS'								THEN 'Borealis'
			WHEN 'BOREALIS POLIMEROS'					THEN 'BorealisPolimeros'
			WHEN 'BP'									THEN 'BP'
			WHEN 'BP AMOCO'								THEN 'BP AMOCO'
			WHEN 'BSL OLEFINVERBUND GMBH'				THEN 'BSL'
			WHEN 'BSL OLEFINVERBUND'					THEN 'BSL'
			WHEN 'CAIN CHEMICAL'						THEN 'Cain'
			WHEN 'CARMEL OLEFINS'						THEN 'Carmel'
			WHEN 'CHEVRON'								THEN 'CPChem'
			WHEN 'CNPC'									THEN 'CNPC'
			WHEN 'CONDEA VISTA'							THEN 'CONDEAVista'
			WHEN 'COPENE'								THEN 'Copene'
			WHEN 'COPESUL'								THEN 'Copesul'
			WHEN 'DAQING  PETROCHEMICAL COMPLEX'		THEN 'CNPC Daqing'
			WHEN 'DEA MINERALOEL AG'					THEN 'DEA'
			WHEN 'DEA MINERALOEL'						THEN 'DEA'
			WHEN 'DOW BENELUX'							THEN 'Dow Benelux'
			WHEN 'DOW BENELUX N.V.'						THEN 'Dow Benelux'
			WHEN 'DOW CENTRAL GERMANY'					THEN 'Dow Germany'
			WHEN 'DOW CHEMICAL IBERICA S.A.'			THEN 'Dow Iberica'
			WHEN 'DOW EUROPE'							THEN 'Dow Europe'
			WHEN 'DOW IBERICA'							THEN 'Dow Iberica'
			WHEN 'DOW USA'								THEN 'Dow USA'
			WHEN 'DSM'									THEN 'DSM'
			WHEN 'DUPONT'								THEN 'du Pont'
			WHEN 'DUSHANZI PETROCHEMICAL COMPANY'		THEN 'CNPC Dushanzi'
			WHEN 'EASTMAN CHEMICAL'						THEN 'Eastman'
			WHEN 'EASTMAN CHEMICAL CO'					THEN 'Eastman'
			WHEN 'ECOPETROL'							THEN 'Ecopetrol'
			WHEN 'Elenac'								THEN 'Elenac'
			WHEN 'Elenac GmbH'							THEN 'Elenac'
			WHEN 'ELF ATOCHEM'							THEN 'Elf Atochem'
			WHEN 'ENICHEM'								THEN 'Enichem'
			WHEN 'ENIMONT'								THEN 'Enimont'
			WHEN 'EQUATE'								THEN 'Equate'
			WHEN 'EQUISTAR'								THEN 'Equistar'
			WHEN 'ERDOELCHEMIE'							THEN 'ErdoelChimie'
			WHEN 'ETHYLENE MALAYSIA'					THEN 'EthMalaysia'
			WHEN 'EXXON CHEMICAL LTD'					THEN 'Exxon Chemical'
			WHEN 'EXXONMOBIL CHEMICAL - PEQUIVEN'		THEN 'ExxonMobil Pequiven'
			WHEN 'EXXONMOBIL CHEMICAL COMPANY'			THEN 'ExxonMobil'
			WHEN 'FINA ANTWERP OLEFINS'					THEN 'FAO'
			WHEN 'FINA REFINING'						THEN 'Fina'
			WHEN 'FINANESTE'							THEN 'NesteOy'					-- ?
			WHEN 'FORMOSA PLASTICS'						THEN 'Formosa'
			WHEN 'HALDIA PETROCHEMICALS LIMITED'		THEN 'Haldia'
			WHEN 'HUNTSMAN'								THEN 'Huntsman'
			WHEN 'HUNTSMAN CHEMICAL'					THEN 'Huntsman'
			WHEN 'HUNTSMAN CHEMICAL COMPANY'			THEN 'Huntsman'
			WHEN 'HUNTSMAN POLYMERS CORPORATION'		THEN 'Huntsman'
			WHEN 'ICI C&P LIMITED'						THEN 'ICI'
			WHEN 'IDEMITSU PETROCHEMICAL'				THEN 'Idemitsu'
			WHEN 'IMPERIAL'								THEN 'Imperial'
			WHEN 'IMPERIAL OIL'							THEN 'Imperial'
			WHEN 'INDIAN PETROCHEMICALS'				THEN 'IPCL'
			WHEN 'KEMCOR AUSTRALIA'						THEN 'Kemcor'
			WHEN 'LYONDELL'								THEN 'Lyondell'
			WHEN 'LYONDELL PETROCHEMICAL'				THEN 'Lyondell'
			WHEN 'MISTUI PETROCHEMICAL'					THEN 'Mitsui'
			WHEN 'MITSUBISHI '							THEN 'Mitsubishi'
			WHEN 'MITSUBISHI CHEMICAL'					THEN 'Mitsubishi'
			WHEN 'MITSUI'								THEN 'Mitsui'
			WHEN 'MONTELL'								THEN 'Montell'
			WHEN 'MPCC Ethylene Industry Company'		THEN 'Sinopec Maoming'
			WHEN 'NAPHTACHIMIE'							THEN 'Naphtachimie'
			WHEN 'NESTE OY'								THEN 'NesteOy'
			WHEN 'NORSK HYDRO'							THEN 'Norsk Hydro'
			WHEN 'NOVA CHEMICAL'						THEN 'Nova Chemicals'
			WHEN 'NOVA CHEMICALS'						THEN 'Nova Chemicals'
			WHEN 'NOVACOR'								THEN 'Novacor'
			WHEN 'NPC'									THEN 'NPC'
			WHEN 'OccIDENTAL'							THEN 'OxyChem'
			WHEN 'OMV'									THEN 'OMV'
			WHEN 'OSAKA PETROCHEMICALS'					THEN 'Osaka'
			WHEN 'PBB'									THEN 'DowPBB'
			WHEN 'PCHEM OF SINGAPORE'					THEN 'PCS'
			WHEN 'PCS'									THEN 'PCS'
			WHEN 'PEMEX'								THEN 'Pemex'
			WHEN 'PEMEX PETROQUIMICA'					THEN 'Pemex'
			WHEN 'PETROMONT'							THEN 'Petromont'
			WHEN 'PETROQUIMICA BAHIA'					THEN 'DowPBB'
			WHEN 'PETROX'								THEN 'Petrox'
			WHEN 'PHILLIPS CHEMICAL COMPANY'			THEN 'Phillips'
			WHEN 'POLIFIN'								THEN 'Polyfin'
			WHEN 'POLIMERI EUROPA'						THEN 'Polimeri'
			WHEN 'POLYSAR'								THEN 'Polysar'
			WHEN 'PQU'									THEN 'PqU'
			WHEN 'PUSA'									THEN 'PUSA'
			WHEN 'QAPCO'								THEN 'Qapco'
			WHEN 'QENOS'								THEN 'Qenos'
			WHEN 'QUANTUM CHEM'							THEN 'Quantum'
			WHEN 'QUANTUM CHEMICAL'						THEN 'Quantum'
			WHEN 'Rayong Olefins Company Limited'		THEN 'Rayong'
			WHEN 'RELIANCE INDUSTRIES, LTD.'			THEN 'Reliance'
			WHEN 'REPSOL'								THEN 'Repsol'
			WHEN 'REPSOL PETROLEO'						THEN 'RepsolPetro'
			WHEN 'REPSOL PETROLEO, S.A.'				THEN 'RepsolPetro'
			WHEN 'RUHR OEL'								THEN 'RuhrOel'
			WHEN 'RUHR OEL GmbH'						THEN 'RuhrOel'
			WHEN 'SADAF'								THEN 'SabicSadaf'
			WHEN 'SANYO PETROCHEMICAL'					THEN 'Sanyo'
			WHEN 'SASOL SYN. FUELS'						THEN 'Sasol SynFuels'
			WHEN 'SHANGHAI PETROCHEMICALS'				THEN 'Sinopec'
			WHEN 'SHELL'								THEN 'Shell'
			WHEN 'SHELL CHEMICALS'						THEN 'Shell'
			WHEN 'SHELL INTERNATIONAL'					THEN 'Shell Intl'
			WHEN 'SHELL INTL.'							THEN 'Shell Intl'
			WHEN 'SICM'									THEN 'Shell Intl CM'
			WHEN 'Singapore EDBI'						THEN 'EDBI'
			WHEN 'SINOPEC'								THEN 'Sinopec'
			WHEN 'SK CORPORATION'						THEN 'SK'
			WHEN 'SONYO'								THEN 'Sanyo'
			WHEN 'STATOIL'								THEN 'Statoil'
			WHEN 'SUMITOMO CHEMICAL'					THEN 'Sumitomo'
			WHEN 'SUMITOMO CHEMICAL CO.LTD.'			THEN 'Sumitomo'
			WHEN 'SUNOCO'								THEN 'Sunoco'
			WHEN 'SYNTHOL SYNTHETIC FUELS'				THEN 'Synthol'
			WHEN 'TEXACO'								THEN 'Texaco'
			WHEN 'THAI OLEFINS'							THEN 'TOC'
			WHEN 'TONEN CHEMICAL CORP.'					THEN 'ExxonMobil Tonen'
			WHEN 'UNION CARBIDE'						THEN 'UnionCarbide'
			WHEN 'UNION TEXAS'							THEN 'UnionTexas'
			WHEN 'WESTLAKE GROUP'						THEN 'Westlake'
			WHEN 'WILLIAMS'								THEN 'Williams'
	
			WHEN 'SOLOMON'								THEN 'Solomon'

		END
	END;
	
	RETURN @ResultVar;
	
END;