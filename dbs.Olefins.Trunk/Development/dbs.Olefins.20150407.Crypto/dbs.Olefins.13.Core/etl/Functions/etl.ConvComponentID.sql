﻿
CREATE FUNCTION etl.ConvComponentID
(
	@ComponentId	NVARCHAR(24)
)
RETURNS NVARCHAR(24)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(24) =
	CASE @ComponentId
		WHEN 'Butane'					THEN 'C4H10'
		WHEN 'Butene'					THEN 'C4H8'
		WHEN 'BUTAD'					THEN 'C4H6'
		WHEN 'XYETB'					THEN 'C8H10'
		WHEN 'BENZ'						THEN 'C6H6'
		WHEN 'TOLuO'					THEN 'C7H8'
		WHEN 'STYR'						THEN 'C8H8'
		WHEN 'PyroOtherGas'				THEN 'PyroGasoline'
		WHEN 'CO2'						THEN 'CO_CO2'
		
		--WHEN 'nButane'				THEN 'NBUTA'
		--WHEN 'iButane'				THEN 'IBUTA'
		--WHEN 'Isobutylene'			THEN 'IB'
		--WHEN 'Butene1'				THEN 'B1'
		WHEN 'Butadiene'				THEN 'C4H6'
		--WHEN 'nPentane'				THEN 'NC5'
		--WHEN 'iPentane'				THEN 'IC5'
		--WHEN 'nHexane'				THEN 'NC6'
		--WHEN 'iHexane'				THEN 'C6ISO'
		WHEN 'Septane'					THEN 'C7H16'
		WHEN 'Octane'					THEN 'C8H18'
		--WHEN 'CO2'					THEN 'CO2'
		--WHEN 'Sulfur'					THEN 'S'

		-- PYPS Results
        WHEN 'HYDROGEN'					THEN 'H2'
        WHEN 'METHANE'					THEN 'CH4'
        WHEN 'ACETYLENE'				THEN 'C2H2'
        WHEN 'ETHYLENE'					THEN 'C2H4'
        WHEN 'ETHANE'					THEN 'C2H6'
        WHEN 'MA + PD'					THEN 'C3H4'
        WHEN 'MA_PD'					THEN 'C3H4'
        WHEN 'MAPD'						THEN 'C3H4'
        WHEN 'PROPYLENE'				THEN 'C3H6'
        WHEN 'PROPANE'					THEN 'C3H8'
        WHEN 'BUTADIENE'				THEN 'C4H6'
        WHEN 'BUTENE'					THEN 'C4H8'
        WHEN 'BUTANE'					THEN 'C4H10'
        WHEN 'C5-S'						THEN 'C5S'
        WHEN 'C5_S'						THEN 'C5S'
        WHEN 'C6-C8 NA'					THEN 'C6C8NA'
        WHEN 'Naphtha'					THEN 'C6C8NA'
        WHEN 'BENZENE'					THEN 'C6H6'
        WHEN 'TOLUENE'					THEN 'C7H8'
        WHEN 'XY + ETB'					THEN 'C8H10'
        WHEN 'Xy_EB'					THEN 'C8H10'
        WHEN 'STYRENE'					THEN 'C8H8'

		-- C9-400F and FUEL OIL transformation defined by Calcs!YIRCalc (Yield Prediction)

        WHEN 'C9-400F'					THEN 'PyroGasOil'
        WHEN 'C9400F'					THEN 'PyroGasOil'
        WHEN 'C9_400F'					THEN 'PyroGasOil'

		WHEN 'FUEL OIL'					THEN 'PyroFuelOil'
		WHEN 'FuelOil'					THEN 'PyroFuelOil'
			
		-- Pricing
		--WHEN 'NATURAL_GAS'				THEN NULL

		ELSE @ComponentId
		
	END;
		
	RETURN @ResultVar;

END