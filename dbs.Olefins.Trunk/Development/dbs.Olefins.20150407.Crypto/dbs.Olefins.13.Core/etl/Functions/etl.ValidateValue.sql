﻿CREATE FUNCTION [etl].[ValidateValue]
(
	@Old				REAL, 
	@New				REAL,
	@ErrorRelative		REAL,
	@ErrorAbsolute		REAL,
	@AllowZeroToNull	BIT
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @r BIT = 0;

	IF
	(
		(
			@Old = @New
		)
		OR
		(
			@Old IS NULL AND @New IS NULL
		)
		OR
		(
			ABS(@Old - @New) <= @ErrorAbsolute
			AND
			CASE WHEN @Old <> 0.0 THEN ABS(@Old - @New) / @Old ELSE 0.0 END * 100.0 <= @ErrorRelative
		)
		OR
		(
			@AllowZeroToNull = 1 AND @Old = 0.0 AND @New IS NULL
		)

	)
		SET @r = 1;
	ELSE
		SET @r = 0;
	
	RETURN @r;

END;