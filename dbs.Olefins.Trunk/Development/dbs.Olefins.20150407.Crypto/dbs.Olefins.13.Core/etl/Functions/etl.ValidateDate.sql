﻿CREATE FUNCTION [etl].[ValidateDate]
(
	@Old				DATETIME,
	@New				DATETIME,
	@AllowNullToText	BIT
)
RETURNS BIT
AS
BEGIN

	DECLARE @r BIT = 0;

	IF (DATEDIFF(DAY, @Old, @New) = 0
		OR (@Old IS NULL AND @New IS NULL))
		SET @r = 1;
	ELSE
		SET @r = 0;

	RETURN @r;

END