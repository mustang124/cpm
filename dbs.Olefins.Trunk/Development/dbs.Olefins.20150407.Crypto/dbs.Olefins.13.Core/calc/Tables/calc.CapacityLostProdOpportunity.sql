﻿CREATE TABLE [calc].[CapacityLostProdOpportunity] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [StreamAvg_kMT]  REAL               NULL,
    [Production_kMT] REAL               NOT NULL,
    [Loss_kMT]       REAL               NOT NULL,
    [_ProdLoss_kMT]  AS                 (isnull([Production_kMT],(0.0))+isnull([Loss_kMT],(0.0))) PERSISTED NOT NULL,
    [_ErrAbs]        AS                 (CONVERT([real],([StreamAvg_kMT]-[Production_kMT])-[Loss_kMT],(1))) PERSISTED,
    [_ErrRel]        AS                 (CONVERT([real],case when [StreamAvg_kMT]<>(0.0) then ((([StreamAvg_kMT]-[Production_kMT])-[Loss_kMT])/[StreamAvg_kMT])*(100.0)  end,(1))) PERSISTED,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityLostProdOpportunity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_CapacityLostProdOpportunity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_CapacityLostProdOpportunity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_CapacityLostProdOpportunity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CapacityLostProdOpportunity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CapacityLostProdOpportunity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CapacityLostProdOpportunity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CapacityLostProdOpportunity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_CapacityLostProdOpportunity_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_LostProdOpportunity_u
	ON  calc.[CapacityLostProdOpportunity]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[CapacityLostProdOpportunity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[CapacityLostProdOpportunity].FactorSetId	= INSERTED.FactorSetId
		AND calc.[CapacityLostProdOpportunity].Refnum		= INSERTED.Refnum
		AND calc.[CapacityLostProdOpportunity].CalDateKey	= INSERTED.CalDateKey
		AND calc.[CapacityLostProdOpportunity].StreamId		= INSERTED.StreamId;
			
END