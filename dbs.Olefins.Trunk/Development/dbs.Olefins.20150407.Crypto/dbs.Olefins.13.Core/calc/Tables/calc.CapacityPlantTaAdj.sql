﻿CREATE TABLE [calc].[CapacityPlantTaAdj] (
    [FactorSetId]             VARCHAR (12)       NOT NULL,
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [SchedId]                 CHAR (1)           NOT NULL,
    [StreamId]                VARCHAR (42)       NOT NULL,
    [Study_Date]              DATE               NOT NULL,
    [Expansion_Date]          DATE               NULL,
    [Ann_TurnAroundLoss_Pcnt] REAL               NULL,
    [Stream_kMT]              REAL               NULL,
    [StreamPrev_kMT]          REAL               NULL,
    [StreamAvg_kMT]           REAL               NULL,
    [Capacity_kMT]            REAL               NULL,
    [CapacityPrev_kMT]        REAL               NULL,
    [CapacityAvg_kMT]         REAL               NULL,
    [_TaStream_kMT]           AS                 (CONVERT([real],([Stream_kMT]*[Ann_TurnAroundLoss_Pcnt])/(100.0),(1))) PERSISTED,
    [_TaStreamPrev_kMT]       AS                 (CONVERT([real],([StreamPrev_kMT]*[Ann_TurnAroundLoss_Pcnt])/(100.0),(1))) PERSISTED,
    [_TaStreamAvg_kMT]        AS                 (CONVERT([real],([StreamAvg_kMT]*[Ann_TurnAroundLoss_Pcnt])/(100.0),(1))) PERSISTED,
    [_TaCapacity_kMT]         AS                 (CONVERT([real],([Capacity_kMT]*[Ann_TurnAroundLoss_Pcnt])/(100.0),(1))) PERSISTED,
    [_TaCapacityPrev_kMT]     AS                 (CONVERT([real],([CapacityPrev_kMT]*[Ann_TurnAroundLoss_Pcnt])/(100.0),(1))) PERSISTED,
    [_TaCapacityAvg_kMT]      AS                 (CONVERT([real],([CapacityAvg_kMT]*[Ann_TurnAroundLoss_Pcnt])/(100.0),(1))) PERSISTED,
    [_TaAdj_Stream_kMT]       AS                 (CONVERT([real],([Stream_kMT]*((100.0)-[Ann_TurnAroundLoss_Pcnt]))/(100.0),(1))) PERSISTED,
    [_TaAdj_StreamPrev_kMT]   AS                 (CONVERT([real],([StreamPrev_kMT]*((100.0)-[Ann_TurnAroundLoss_Pcnt]))/(100.0),(1))) PERSISTED,
    [_TaAdj_StreamAvg_kMT]    AS                 (CONVERT([real],([StreamAvg_kMT]*((100.0)-[Ann_TurnAroundLoss_Pcnt]))/(100.0),(1))) PERSISTED,
    [_TaAdj_Capacity_kMT]     AS                 (CONVERT([real],([Capacity_kMT]*((100.0)-[Ann_TurnAroundLoss_Pcnt]))/(100.0),(1))) PERSISTED,
    [_TaAdj_CapacityPrev_kMT] AS                 (CONVERT([real],([CapacityPrev_kMT]*((100.0)-[Ann_TurnAroundLoss_Pcnt]))/(100.0),(1))) PERSISTED,
    [_TaAdj_CapacityAvg_kMT]  AS                 (CONVERT([real],([CapacityAvg_kMT]*((100.0)-[Ann_TurnAroundLoss_Pcnt]))/(100.0),(1))) PERSISTED,
    [TaCapacityDerating_Pcnt] AS                 (CONVERT([real],([Ann_TurnAroundLoss_Pcnt]/((100.0)-[Ann_TurnAroundLoss_Pcnt]))*(100.0),(1))) PERSISTED,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityPlantTaAdj_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_CapacityPlantTaAdj_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_CapacityPlantTaAdj_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_CapacityPlantTaAdj_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CapacityPlantTaAdj] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SchedId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CapacityPlantTaAdj_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CapacityPlantTaAdj_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CapacityPlantTaAdj_Schedule_LookUp] FOREIGN KEY ([SchedId]) REFERENCES [dim].[Schedule_LookUp] ([ScheduleId]),
    CONSTRAINT [FK_calc_CapacityPlantTaAdj_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_CapacityPlantTaAdj_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_CapacityPlantTaAdj_u
	ON  calc.CapacityPlantTaAdj
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.CapacityPlantTaAdj
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.CapacityPlantTaAdj.FactorSetId	= INSERTED.FactorSetId
		AND calc.CapacityPlantTaAdj.Refnum		= INSERTED.Refnum
		AND calc.CapacityPlantTaAdj.CalDateKey	= INSERTED.CalDateKey
		AND calc.CapacityPlantTaAdj.SchedId		= INSERTED.SchedId
		AND calc.CapacityPlantTaAdj.StreamId	= INSERTED.StreamId;
				
END