﻿CREATE TABLE [calc].[MeiLosses] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [CurrencyRpt]      VARCHAR (4)        NOT NULL,
    [Stream_Value_Cur] REAL               NOT NULL,
    [Quantity_kMT]     REAL               NOT NULL,
    [Component_kMT]    REAL               NOT NULL,
    [Losses_Index]     REAL               NOT NULL,
    [Losses_PcntRv]    REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_MeiLosses_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_MeiLosses_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_MeiLosses_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_MeiLosses_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_MeiLosses] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_MeiLosses_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_MeiLosses_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_MeiLosses_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_MeiLosses_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_MeiLosses_u
	ON  calc.MeiLosses
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.MeiLosses
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MeiLosses.FactorSetId		= INSERTED.FactorSetId
		AND calc.MeiLosses.Refnum			= INSERTED.Refnum
		AND calc.MeiLosses.CalDateKey		= INSERTED.CalDateKey
		AND calc.MeiLosses.CurrencyRpt		= INSERTED.CurrencyRpt;
				
END