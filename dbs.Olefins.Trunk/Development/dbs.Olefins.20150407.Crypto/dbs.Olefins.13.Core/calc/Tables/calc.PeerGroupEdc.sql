﻿CREATE TABLE [calc].[PeerGroupEdc] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PeerGroup]      TINYINT            NOT NULL,
    [kEdc]           REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupEdc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupEdc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_PeerGroupEdc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_PeerGroupEdc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PeerGroupEdc] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PeerGroupEdc_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PeerGroupEdc_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PeerGroupEdc_PeerGroup] FOREIGN KEY ([FactorSetId], [PeerGroup]) REFERENCES [ante].[PeerGroupEdc] ([FactorSetId], [PeerGroup]),
    CONSTRAINT [FK_calc_PeerGroupEdc_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_PeerGroupEdc_u
ON  calc.PeerGroupEdc
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE calc.PeerGroupEdc
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PeerGroupEdc.FactorSetId		= INSERTED.FactorSetId
		AND calc.PeerGroupEdc.Refnum			= INSERTED.Refnum
		AND calc.PeerGroupEdc.CalDateKey		= INSERTED.CalDateKey;
		
END;