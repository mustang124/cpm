﻿CREATE TABLE [calc].[EmissionsCarbonDirect] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [SimModelId]             VARCHAR (12)       NOT NULL,
    [EmissionsId]            VARCHAR (42)       NOT NULL,
    [Quantity_MBtu]          REAL               NOT NULL,
    [_CefGwp]                AS                 (CONVERT([real],case when [Quantity_MBtu]<>(0.0) then [EmissionsCarbon_MTCO2e]/[Quantity_MBtu]  end,(0))) PERSISTED,
    [EmissionsCarbon_MTCO2e] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsCarbonDirect_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonDirect_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonDirect_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonDirect_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EmissionsCarbonDirect] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [EmissionsId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_EmissionsCarbonDirect_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EmissionsCarbonDirect_Emissions_LookUp] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsCarbonDirect_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_EmissionsCarbonDirect_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_EmissionsCarbonDirect_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_EmissionsCarbonDirect_u
	ON  calc.EmissionsCarbonDirect
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.EmissionsCarbonDirect
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.EmissionsCarbonDirect.FactorSetId	= INSERTED.FactorSetId
		AND	calc.EmissionsCarbonDirect.Refnum		= INSERTED.Refnum
		AND calc.EmissionsCarbonDirect.CalDateKey	= INSERTED.CalDateKey
		AND calc.EmissionsCarbonDirect.EmissionsId	= INSERTED.EmissionsId
		AND calc.EmissionsCarbonDirect.SimModelId	= INSERTED.SimModelId;
	
END;