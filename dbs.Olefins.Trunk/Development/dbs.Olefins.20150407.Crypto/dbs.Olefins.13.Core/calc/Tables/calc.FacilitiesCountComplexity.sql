﻿CREATE TABLE [calc].[FacilitiesCountComplexity] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [FacilityId]      VARCHAR (42)       NOT NULL,
    [Unit_Count]      INT                NOT NULL,
    [Unit_Complexity] REAL               NOT NULL,
    [Adj_Count]       REAL               NOT NULL,
    [Adj_Complexity]  REAL               NOT NULL,
    [Replacement_Wt]  REAL               NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesCountComplexity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_FacilitiesCountComplexity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_FacilitiesCountComplexity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_FacilitiesCountComplexity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_FacilitiesCountComplexity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_FacilitiesCountComplexity_Adj_Complexity] CHECK ([Adj_Complexity]>=(0)),
    CONSTRAINT [CR_calc_FacilitiesCountComplexity_Adj_Count] CHECK ([Adj_Count]>(0)),
    CONSTRAINT [CR_calc_FacilitiesCountComplexity_Unit_Complexity] CHECK ([Unit_Complexity]>=(0)),
    CONSTRAINT [CR_calc_FacilitiesCountComplexity_Unit_Count] CHECK ([Unit_Count]>=(0)),
    CONSTRAINT [FK_calc_FacilitiesCountComplexity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_FacilitiesCountComplexity_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_calc_FacilitiesCountComplexity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_FacilitiesCountComplexity_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_FacilitiesCountComplexity_u
	ON  calc.FacilitiesCountComplexity
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.FacilitiesCountComplexity
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.FacilitiesCountComplexity.FactorSetId		= INSERTED.FactorSetId
		AND calc.FacilitiesCountComplexity.Refnum			= INSERTED.Refnum
		AND calc.FacilitiesCountComplexity.CalDateKey		= INSERTED.CalDateKey
		AND calc.FacilitiesCountComplexity.FacilityId		= INSERTED.FacilityId;
			
END