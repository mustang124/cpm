﻿CREATE VIEW [calc].[MarginManufacturingCost]
WITH SCHEMABINDING
AS
SELECT
	m.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	m.CurrencyRpt,
	m.MarginAnalysis,
	m.MarginId,
	m.Amount_Cur * 1000.0	[Margin_Cur],
	o.AccountId,
	o.Amount_Cur			[OpEx_Cur],
	ISNULL(o.Amount_Cur, 0.0) - ISNULL(m.Amount_Cur * 1000.0, 0.0)	[ManuCost_Cur]
FROM calc.MarginAggregate				m
INNER JOIN calc.OpExAggregate			o
	ON	o.FactorSetId = m.FactorSetId
	AND	o.Refnum = m.Refnum
	AND	o.CalDateKey = m.CalDateKey
	AND	o.CurrencyRpt = m.CurrencyRpt
	AND	o.AccountId = 'TotRefExp'
WHERE	m.MarginAnalysis = 'None'
	AND	m.MarginId = 'PlantFeed'
