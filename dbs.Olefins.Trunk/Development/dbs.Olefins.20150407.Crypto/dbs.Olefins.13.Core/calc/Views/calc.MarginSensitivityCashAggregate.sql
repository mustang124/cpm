﻿CREATE VIEW [calc].[MarginSensitivityCashAggregate]
WITH SCHEMABINDING
AS
SELECT
	ms.[FactorSetId],
	ms.[Refnum],
	ms.[CalDateKey],
	ms.[CurrencyRpt],
	ms.[MarginAnalysis],
	SUM(ms.[Amount_Cur])					[Amount_Cur],
	SUM(ms.[Amount_SensEDC])				[Amount_SensEDC],
	SUM(ms.[Amount_SensProd_MT])			[Amount_SensProd_MT],
	SUM(ms.[Amount_SensProd_Cents])			[Amount_SensProd_Cents],
	SUM(ms.[TaAdj_Amount_Cur])				[TaAdj_Amount_Cur],
	SUM(ms.[TAAdj_Amount_SensEDC])			[TAAdj_Amount_SensEDC],
	SUM(ms.[TaAdj_Amount_SensProd_MT])		[TaAdj_Amount_SensProd_MT],
	SUM(ms.[TAAdj_Amount_SensProd_Cents])	[TAAdj_Amount_SensProd_Cents]
FROM calc.[MarginSensitivity]	ms
WHERE	ms.[MarginId]			IN ('STVol', 'STNonVol', 'STTaExp')
GROUP BY
	ms.[FactorSetId],
	ms.[Refnum],
	ms.[CalDateKey],
	ms.[CurrencyRpt],
	ms.[MarginAnalysis];