﻿CREATE VIEW [calc].[PricingYieldComposition_SOA]
WITH SCHEMABINDING
AS
SELECT
	m.FactorSetId,
	m.ScenarioId,
	m.Refnum,
	MAX(m.CalDateKey)	[CalDateKey],
	m.CurrencyRpt,
	m.SimModelId,
	m.OpCondId,
	m.RecycleId,
	p._Ann_Calc_Amount_Cur / SUM(m._Ann_Calc_Amount_Cur) * 100.0 [PyroVal_Pcnt]
FROM calc.PricingYieldComposition					m
INNER JOIN calc.PricingYieldCompositionAggregate	p
	ON	p.FactorSetId = m.FactorSetId
	AND	p.ScenarioId = m.ScenarioId
	AND	p.Refnum = m.Refnum
	AND	p.CurrencyRpt = m.CurrencyRpt
	AND	p.RecycleId = m.RecycleId
	AND	p.SimModelId = 'Plant'
WHERE m.SimModelId <> 'Plant'
GROUP BY
	m.FactorSetId,
	m.ScenarioId,
	m.Refnum,
	m.CurrencyRpt,
	m.SimModelId,
	m.OpCondId,
	m.RecycleId,
	p._Ann_Calc_Amount_Cur;