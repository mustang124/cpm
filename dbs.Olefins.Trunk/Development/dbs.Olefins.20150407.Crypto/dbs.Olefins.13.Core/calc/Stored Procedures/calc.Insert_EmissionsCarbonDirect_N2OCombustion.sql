﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonDirect_N2OCombustion]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonDirect(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_MBtu, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			'PYPS',
			fpl.Plant_QtrDateKey,
			n2o.EmissionsId,
			COALESCE(n2o.Quantity_MBtu, 0.0),
			n2o.EmissionsCarbon_MTCO2e * dir.Quantity_MBtu / tot.Quantity_MBtu
		FROM @fpl											fpl
		INNER JOIN calc.EmissionsCarbonPlantAggregate		dir
			ON	dir.FactorSetId		= fpl.FactorSetId
			AND	dir.Refnum			= fpl.Refnum
			AND	dir.CalDateKey		= fpl.Plant_QtrDateKey
			AND	dir.EmissionsId		= 'DirectCombustion'
		INNER JOIN calc.EmissionsCarbonPlantAggregate		tot
			ON	tot.FactorSetId		= fpl.FactorSetId
			AND	tot.Refnum			= fpl.Refnum
			AND	tot.CalDateKey		= fpl.Plant_QtrDateKey
			AND	tot.EmissionsId		= 'TotalEmissions'
		INNER JOIN calc.EmissionsCarbonStandard				n2o
			ON	n2o.FactorSetId		= fpl.FactorSetId
			AND	n2o.Refnum			= fpl.Refnum
			AND	n2o.CalDateKey		= fpl.Plant_QtrDateKey
			AND	n2o.EmissionsId		IN (SELECT b.DescendantId FROM dim.EmissionsStandard_Bridge b WHERE b.FactorSetId = fpl.FactorSetId AND b.EmissionsId = 'N2OCombustion')
			AND	n2o.EmissionsCarbon_MTCO2e	> 0.0
		WHERE	fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END