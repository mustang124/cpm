﻿CREATE PROCEDURE [calc].[Insert_Roi_MarginSensitivity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Roi(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MarginAnalysis, MarginId, Roi, TaAdj_Roi, Loc_Roi, TaAdj_Loc_Roi)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			ms.MarginAnalysis,
			ms.MarginId,
			ms.Amount_SensProd_MT		/ rv.ReplacementValue		* ec.Capacity_kMT / 10.0	[Roi],
			ms.TaAdj_Amount_SensProd_MT	/ rv.ReplacementValue		* ec.Capacity_kMT / 10.0	[TaAdj_Roi],
			ms.Amount_SensProd_MT		/ rv.ReplacementLocation	* ec.Capacity_kMT / 10.0	[Loc_Roi],
			ms.TaAdj_Amount_SensProd_MT	/ rv.ReplacementLocation	* ec.Capacity_kMT / 10.0	[TaAdj_Loc_Roi]
		FROM @fpl									fpl
		INNER JOIN fact.Capacity					ec
			ON	ec.Refnum = fpl.Refnum
			AND	ec.CalDateKey = fpl.Plant_QtrDateKey
			AND	ec.StreamId = 'Ethylene'
		INNER JOIN calc.MarginSensitivity			ms
			ON	ms.FactorSetId = fpl.FactorSetId
			AND	ms.Refnum = fpl.Refnum
			AND	ms.CalDateKey = fpl.Plant_QtrDateKey
			AND	ms.CurrencyRpt = fpl.CurrencyRpt
			AND	ms.MarginAnalysis = 'Ethylene'
			AND	ms.MarginId IN ('MarginGross', 'MarginNetCash', 'PreTaxIncome')
		INNER JOIN calc.ReplacementValueAggregate	rv
			ON	rv.FactorSetId = fpl.FactorSetId
			AND	rv.Refnum = fpl.Refnum
			AND	rv.CalDateKey = fpl.Plant_QtrDateKey
			AND	rv.CurrencyRpt = fpl.CurrencyRpt
			AND	rv.ReplacementValueId = 'UscgReplacement'
			AND	rv.ReplacementLocation IS NOT NULL
		ORDER BY ms.Amount_Cur;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END