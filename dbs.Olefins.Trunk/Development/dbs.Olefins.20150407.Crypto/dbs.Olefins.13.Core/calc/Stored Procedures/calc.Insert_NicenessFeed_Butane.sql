﻿CREATE PROCEDURE [calc].[Insert_NicenessFeed_Butane]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessFeed]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [NicenessYield_kMT], [NicenessEnergy_kMT])
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			'Butane'						[StreamId],
			sqa.Quantity_kMT - COALESCE(SUM(cs.Component_kMT), 0.0) - COALESCE(SUM(s.Quantity_kMT * r.Recycled_WtPcnt) / 100.0, 0.0)	[Stream_kMT],
			sqa.Quantity_kMT - COALESCE(SUM(cs.Component_kMT), 0.0) - COALESCE(SUM(s.Quantity_kMT * r.Recycled_WtPcnt) / 100.0, 0.0)	[NicenessYield_kMT],
			sqa.Quantity_kMT - COALESCE(SUM(cs.Component_kMT), 0.0) - COALESCE(SUM(s.Quantity_kMT * r.Recycled_WtPcnt) / 100.0, 0.0)	[NicenessEnergy_kMT]
		FROM @fpl									fpl
		INNER JOIN fact.StreamQuantityAggregate		sqa	WITH (NOEXPAND)
			ON	sqa.FactorSetId	= fpl.FactorSetId
			AND	sqa.Refnum		= fpl.Refnum
			AND	sqa.StreamId	= 'Light'
		INNER JOIN dim.Stream_Bridge				b
			ON	b.FactorSetId	= fpl.FactorSetId
			AND	b.StreamId		= 'PlantFeed'

		LEFT OUTER JOIN calc.CompositionStream		cs
			ON	cs.FactorSetId	= fpl.FactorSetId
			AND	cs.Refnum		= fpl.Refnum
			AND	cs.CalDateKey	= fpl.Plant_QtrDateKey
			AND	cs.SimModelId	= 'Plant'
			AND	cs.OpCondId		= 'PlantFeed'
			AND	cs.ComponentId	IN ('CH4', 'C2H4', 'C2H6', 'CO_CO2', 'H2', 'S', 'C3H6', 'C3H8')
			AND	cs.StreamId		= b.DescendantId
			AND	cs.StreamId		IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'Light')

		LEFT OUTER JOIN fact.Quantity				s
			ON	s.Refnum		= fpl.Refnum
			AND	s.CalDateKey	= fpl.Plant_QtrDateKey
			AND	s.StreamId		= b.DescendantId
			AND	s.StreamId		IN ('ROGC4Plus', 'DilButane')
		LEFT OUTER JOIN [ante].[MapRecyle]			m
			ON	m.StreamId		= s.StreamId
		LEFT OUTER JOIN fact.QuantitySuppRecycled	r
			ON	r.Refnum		= fpl.Refnum
			AND	r.CalDateKey	= fpl.Plant_AnnDateKey
			AND	r.StreamId		= m.RecycleId
		WHERE	fpl.[FactorSet_AnnDateKey]	> 20130000
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			sqa.Quantity_kMT;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;