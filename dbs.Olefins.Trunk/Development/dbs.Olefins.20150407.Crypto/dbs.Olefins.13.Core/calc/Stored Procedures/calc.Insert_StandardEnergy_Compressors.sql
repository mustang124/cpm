﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Compressors]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			m.ModelId,
			f.FacilityId,
			f.FacilityId,
			k.Value * Power(f.Power_BHP * ABS(q.Quantity_kMT) / c.Capacity_kMT, e.Value)
		FROM @fpl												tsq
		INNER JOIN fact.FacilitiesCompressors					f
			ON	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN fact.StreamQuantityAggregate					q	WITH (NOEXPAND)
			ON	q.FactorSetId = tsq.FactorSetId
			AND	q.Refnum = tsq.Refnum
			AND q.StreamId = 'FreshPyroFeed'
		INNER JOIN fact.Capacity								c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = q.StreamId
		INNER JOIN ante.StandardEnergyCoefficients				k
			ON k.FactorSetId = tsq.FactorSetId
			AND	k.StandardEnergyId = f.FacilityId
		INNER JOIN ante.StandardEnergyExponents					e
			ON e.FactorSetId = tsq.FactorSetId
			AND	e.StandardEnergyId = f.FacilityId
			AND	e.FactorSetId = k.FactorSetId
		INNER JOIN dim.SimModel_LookUp							m
			ON	m.ModelId	<>	'Plant'
		WHERE	f.Power_BHP > 0.0
			AND	tsq.FactorSet_AnnDateKey	<= 20130000
			AND	q.Quantity_kMT > 0.0
			AND	c.Capacity_kMT > 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;