﻿CREATE PROCEDURE [calc].[Delete_CalculatePlant]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		BEGIN	--	DELETE STATEMENTS

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Efficiency';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Efficiency						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PeerGroupCapacity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PeerGroupCapacity					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PeerGroupCoGen';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PeerGroupCoGen					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PeerGroupEdc';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PeerGroupEdc						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PeerGroupTech';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PeerGroupTech						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Divisors';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Divisors							t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.EmissionsCarbonDirect';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.EmissionsCarbonDirect				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.EmissionsCarbonStandard';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.EmissionsCarbonStandard			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.EmissionsCarbonPlant';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.EmissionsCarbonPlant				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PeerGroupFeedClass';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PeerGroupFeedClass				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ContainedPyroFeedLiquid';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ContainedPyroFeedLiquid			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ContainedPyroFeedLight';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ContainedPyroFeedLight			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MaintExpenseOccSwb';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MaintExpenseOccSwb				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MaintExpenseMpsSwb';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MaintExpenseMpsSwb				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MaintExpenseLaborMatl';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MaintExpenseLaborMatl				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MaintExpenseCostCategory';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MaintExpenseCostCategory			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MaintCostRoutine';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MaintCostRoutine					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MaintCostAverage';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MaintCostAverage					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MaintPersAverage';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MaintPersAverage					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.EnergyNormal';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.EnergyNormal						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Roi';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Roi								t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Mei';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Mei								t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MeiMaintenanceCostIndex';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MeiMaintenanceCostIndex			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MeiLosses';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MeiLosses							t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MeiUnreliability';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MeiUnreliability					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MarginSensitivity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MarginSensitivity					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Margin';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Margin							t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.MarginAggregate';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.MarginAggregate					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ReliabilityIndicator';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ReliabilityIndicator				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.ReliabilityProductLost';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.ReliabilityProductLost			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ReliabilityOpLossAnnualized';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ReliabilityOpLossAnnualized		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ReliabilityOpLossAvailability';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ReliabilityOpLossAvailability		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ReplacementValue';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ReplacementValue					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ReplValOtherOffSite';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ReplValOtherOffSite				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ReplValSuppFeedAdjustment';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ReplValSuppFeedAdjustment			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ReplValPyroFlexibility';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ReplValPyroFlexibility			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.ReplValPyroCapacityEstimate';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.ReplValPyroCapacityEstimate		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.ReplValPyroCapacity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.ReplValPyroCapacity				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.ReplValEncEstimate';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.ReplValEncEstimate				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PersCost';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PersCost							t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PersSupervisoryRatioTaAdj';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PersSupervisoryRatioTaAdj			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PersAggregateAdj';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PersAggregateAdj					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PersMaintTa';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PersMaintTa						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.OpEx';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.OpEx								t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingPPFCFuelGas';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.PricingPPFCFuelGas				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PricingPlantStreamQuarters';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PricingPlantStreamQuarters		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PricingPlantComposition';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.[PricingPlantComposition]			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PricingPlantStream';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.[PricingPlantStream]				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc =NCHAR(9) + 'DELETE FROM calc.PricingPlantAccount';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.[PricingPlantAccount]				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PricingYieldComposition';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PricingYieldComposition			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Eei';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Eei								t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.EnergySeparation';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.EnergySeparation					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ApcIndex';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ApcIndex							t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ApcIndexApplications';
		PRINT @ProcedureDesc;
		
		DELETE FROM t
		FROM calc.ApcIndexApplications				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ApcIndexOnLineModel';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ApcIndexOnLineModel				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ApcIndexPyroFurnIndivid';
		PRINT @ProcedureDesc;
		
		DELETE FROM t
		FROM calc.ApcIndexPyroFurnIndivid			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ApcIndexSteamOther';
		PRINT @ProcedureDesc;
		
		DELETE FROM t
		FROM calc.ApcIndexSteamOther				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.ApcAbsenceCorrection';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.ApcAbsenceCorrection				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Furnaces';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Furnaces							t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.FacilitiesCountComplexity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.FacilitiesCountComplexity			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.NicenessFeed';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.NicenessFeed						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.NicenessEnergy';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.NicenessEnergy					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergy';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergy					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergyFreshFeed';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergyFreshFeed			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergyFracFeed';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergyFracFeed			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergyHydroPur';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergyHydroPur			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergyHydroTreat';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergyHydroTreat			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergyRedCrackedGas';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergyRedCrackedGas		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergyRedEthylene';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergyRedEthylene			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergyRedPropylene';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergyRedPropylene		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.StandardEnergySupplemental';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.StandardEnergySupplemental		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.Edc';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.Edc								t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.SimulationCompositionYieldImprovement';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.SimulationCompositionYieldImprovement		t
		INNER JOIN @fpl										fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CompositionYieldRatio';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.CompositionYieldRatio				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.YIRProduct';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.YIRProduct						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.YIRSupplemental';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.YIRSupplemental					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CompositionYieldPlant';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.CompositionYieldPlant				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CapacityLostProdOpportunity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.CapacityLostProdOpportunity		t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.QuantityRecycled';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.QuantityRecycled					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CapacityUtilization';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.CapacityUtilization				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CapacityProductionRatio';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.CapacityProductionRatio			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CapacityPlantTaAdj';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.CapacityPlantTaAdj				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CapacityPlant';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.CapacityPlant						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CapacityGrowthUtilization';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.[CapacityGrowthAggregate]			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.DivisorsProduction';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.DivisorsProduction				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.DivisorsFeed';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.DivisorsFeed						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.TaAdjRatio';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.TaAdjRatio						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.TaAdjPers';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.TaAdjPers							t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.TaAdjReliability';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.TaAdjReliability					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.TaAdjCapacityAggregate';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.TaAdjCapacityAggregate			t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.TaAdjCapacity';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.TaAdjCapacity						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.TaAdjStudyYear';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.TaAdjStudyYear					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.DivisorsProduction';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.DivisorsProduction				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.CompositionStream';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.[CompositionStream]				t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.CompositionCalc';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM inter.CompositionCalc					t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM calc.PlantRecycles';
		PRINT @ProcedureDesc;

		DELETE FROM t
		FROM calc.PlantRecycles						t
		INNER JOIN @fpl								fpl
			ON	fpl.Refnum = t.Refnum;

		END;
	
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;