﻿CREATE PROCEDURE [calc].[Insert_SimulationCompositionYieldImprovement_C2H2]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[SimulationCompositionYieldImprovement]([FactorSetId], [Refnum], [CalDateKey], [SimModelId], [OpCondId], [RecycleId], [ComponentId], [Plant_kMT])
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			s.ModelId,
			o.OpCondId,
			p.RecycleId,
			b.DescendantId,
			p.Component_kMT
		FROM @fpl											fpl

		INNER JOIN dim.SimModel_LookUp						s
			ON	s.ModelId <> 'Plant'
		INNER JOIN dim.SimOpCond_LookUp						o
			ON	o.OpCondId <> 'PlantFeed'
		INNER JOIN dim.Component_Bridge						b
			ON	b.FactorSetId = fpl.FactorSetId
			AND	b.ComponentId = 'ProdHVC'
		INNER JOIN calc.CompositionYieldRatio				p
			ON	p.FactorSetId = fpl.FactorSetId
			AND	p.Refnum = fpl.Refnum
			AND	p.CalDateKey = fpl.Plant_QtrDateKey
			AND	p.SimModelId = 'Plant'
			AND	p.OpCondId = 'OSOP'
			AND	p.ComponentId = b.DescendantId
			AND	p.ComponentId = 'C2H2'
		WHERE	p.Component_kMT IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;