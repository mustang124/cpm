﻿CREATE PROCEDURE calc.Insert_CapacityGrowthAggregate
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	--	Insert Records	-----------------------------------------------------------------
	BEGIN TRY

		INSERT INTO calc.[CapacityGrowthAggregate](FactorSetId, Refnum, CalDateKey, StreamId, Prod_kMT, Capacity_kMT, StudyYearDifference)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			g.CalDateKey,
			ISNULL(g.StreamId, 'ProdOlefins'),
			SUM(g.Prod_kMT),
			SUM(g.Capacity_kMT),
			g._StudyYearDifference
		FROM @fpl						fpl
		INNER JOIN fact.CapacityGrowth	g
			ON	g.Refnum = fpl.Refnum
		WHERE	fpl.CalQtr = 4
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			g.CalDateKey,
			g._StudyYearDifference,
			ROLLUP(g.StreamId);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;