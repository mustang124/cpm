﻿CREATE PROCEDURE [calc].[Insert_MeiUnreliability]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.MeiUnreliability(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MarginAnalysis, Margin_CurkMT, Unreliability_Index, Unreliability_PcntRv)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			m.MarginAnalysis,
			SUM(m.Amount_Cur) / p.Production_kMT * 1000.0											[Margin_CurkMT],
			SUM(m.Amount_Cur) / p.Production_kMT * l.Loss_MTYr									[Unreliability_Index],
			SUM(m.Amount_Cur) / p.Production_kMT * l.Loss_MTYr / rv.ReplacementLocation / 10.0	[Unreliability_PcntRv]
		FROM @fpl										fpl
		LEFT OUTER JOIN calc.MarginAggregate			m
			ON	m.FactorSetId = fpl.FactorSetId
			AND	m.Refnum = fpl.Refnum
			AND	m.CalDateKey = fpl.Plant_QtrDateKey
			AND	m.CurrencyRpt = fpl.CurrencyRpt
			AND	m.MarginAnalysis = 'Ethylene'
			AND	m.MarginId IN ('MarginGross', 'STVol')
		INNER JOIN calc.DivisorsProduction				p
			ON	p.FactorSetId = fpl.FactorSetId
			AND	p.Refnum = fpl.Refnum
			AND	p.CalDateKey = fpl.Plant_QtrDateKey
			AND	p.ComponentId = 'C2H4'
		INNER JOIN calc.ReplacementValueAggregate		rv
			ON	rv.FactorSetId = fpl.FactorSetId
			AND	rv.Refnum = fpl.Refnum
			AND	rv.CalDateKey = fpl.Plant_QtrDateKey
			AND	rv.CurrencyRpt = fpl.CurrencyRpt
			AND	rv.ReplacementValueId = 'UscgReplacement'
			AND	rv.ReplacementLocation IS NOT NULL
		INNER JOIN inter.ReliabilityProductLost			l
			ON	l.FactorSetId = fpl.FactorSetId
			AND	l.Refnum = fpl.Refnum
			AND	l.CalDateKey = fpl.Plant_QtrDateKey
		WHERE fpl.CalQtr = 4
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			m.MarginAnalysis,
			p.Production_kMT,
			rv.ReplacementLocation,
			l.Loss_MTYr;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END