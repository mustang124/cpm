﻿CREATE PROCEDURE [calc].[Insert_PricingPlantStream_Metathesis]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @t TABLE
		(
			MetaStreamID	VARCHAR(42)	NOT	NULL,
			PricedAs		VARCHAR(42)	NOT	NULL,
			PRIMARY KEY CLUSTERED(MetaStreamID ASC)
		);

		INSERT INTO @t(MetaStreamID, PricedAs) VALUES
			('MetaOCEthylene', 'EthylenePG'),
			('MetaOCButanes', 'C4Oth'),
			('MetaOCButene1', 'C4Oth'),
			('MetaOCButene2', 'C4Oth'),
			('MetaOCOther', 'IsoButylene'),

			('MetaPurchEthylene', 'ConcEthylene'),
			('MetaPurchButane', 'DilButane'),
			('MetaPurchButene1', 'DilButylene'),
			('MetaPurchButene2', 'DilButylene'),
			('MetaPurchOther', 'DilButylene'),

			('MetaProdPropylenePG', 'PropylenePG'),
			('MetaProdUnReactLightPurge', 'PropaneC3Resid'),
			('MetaProdUnReactButane', 'C4Oth'),
			('MetaProdC5', 'PyroGasoline');

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT,
			Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Adj_CompPurity_Cur,
			Supersede_Amount_Cur,
			Client_Amount_Cur,
			Region_Amount_Cur,
			Country_Amount_Cur,
			Location_Amount_Cur,
			Composition_Amount_Cur,
			Energy_Amount_Cur
		)
		SELECT
			p.FactorSetId,
			p.ScenarioId,
			p.Refnum,
			p.CalDateKey,
			p.CurrencyRpt,

			q.StreamId,
			q.StreamId,
			q.Quantity_kMT,

			p.Adj_NonStdStream_Coeff,
			p.Adj_Logistics_Cur,
			p.Adj_CompPurity_Cur,
			p.Supersede_Amount_Cur,

			p.Client_Amount_Cur,
			p.Region_Amount_Cur,
			p.Country_Amount_Cur,
			p.Location_Amount_Cur,
			p.Composition_Amount_Cur,
			p.Supersede_Amount_Cur

		FROM @fpl												tsq
		INNER JOIN calc.[PricingPlantStream]					p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN @t											t
			ON	t.PricedAs = p.StreamId
		INNER JOIN fact.MetaQuantity							q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
			AND	q.StreamId = t.MetaStreamID;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;