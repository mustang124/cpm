﻿CREATE PROCEDURE [calc].[Insert_NicenessFeed_LightEP]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[NicenessFeed]([FactorSetId], [Refnum], [CalDateKey], [StreamId], [Stream_kMT], [NicenessYield_kMT], [NicenessEnergy_kMT])
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			CASE
				WHEN cs.[ComponentId] IN ('CH4', 'C2H4', 'C2H6', 'CO_CO2', 'H2', 'S') THEN 'Ethane'
				WHEN cs.[ComponentId] IN ('C3H6', 'C3H8') THEN 'Propane'
				END			[StreamId],
			SUM(cs.Component_kMT),
			SUM(cs.Component_kMT),
			SUM(cs.Component_kMT)
		FROM @fpl							fpl
		INNER JOIN calc.CompositionStream	cs
			ON	cs.FactorSetId	= fpl.FactorSetId
			AND	cs.Refnum		= fpl.Refnum
			AND	cs.CalDateKey	= fpl.Plant_QtrDateKey
			AND	cs.SimModelId	= 'Plant'
			AND	cs.OpCondId		= 'PlantFeed'
			AND	cs.StreamId		IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'Light')
		WHERE	cs.[ComponentId] IN ('CH4', 'C2H4', 'C2H6', 'CO_CO2', 'H2', 'S', 'C3H6', 'C3H8')
			AND	fpl.[FactorSet_AnnDateKey]	> 20130000
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			CASE
				WHEN cs.ComponentId IN ('CH4', 'C2H4', 'C2H6', 'CO_CO2', 'H2', 'S') THEN 'Ethane'
				WHEN cs.ComponentId IN ('C3H6', 'C3H8')								THEN 'Propane'
				END;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;