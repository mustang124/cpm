﻿CREATE TABLE [Console].[Console] (
    [StudyType]        NVARCHAR (50)  NULL,
    [DataConnection]   NVARCHAR (200) NULL,
    [CurrentStudyYear] NVARCHAR (4)   NULL,
    [StudyDrive]       NVARCHAR (200) NULL,
    [TempDrive]        NVARCHAR (50)  NULL,
    [StudyRegion]      NVARCHAR (50)  NULL
);

