﻿CREATE PROCEDURE sim.InputPyps
(	
	@Refnum			VARCHAR(25)	= NULL,
	@FactorSetId	VARCHAR(12)	= NULL
)
AS
BEGIN (
SELECT 1
	--SELECT
	--	f.FactorSetId,
	--	q.Refnum,
	--	q.CalDateKey,
		
	--	q.StreamId,
	--	q.StreamDescription,
	--	o.OpCondId,
		
	--	rt.RecycleId,
	--	rt.RecycleId,
	--	CONVERT(BIT, ISNULL(s.Active, 0))			[Supersede_Bit],
		
	--	NULL										[FurnaceTypeId],
		
	--	ISNULL(s.[SevInd],
	--		CASE
	--		WHEN p.FeedConv_WtPcnt > 0.1			THEN 3
	--		WHEN p.PropyleneMethane_Ratio > 0.1		THEN 4 
	--		WHEN p.PropyleneEthylene_Ratio > 0.02	THEN 2
	--		WHEN p.EthyleneYield_WtPcnt > 0.1		THEN 1
	--		END)									[SevInd],
		
	--	COUNT(1) OVER()							[Items],
	--	ISNULL(s.[1], ISNULL(c.[1], 0))			  [1],	-- Ethane
	--	ISNULL(s.[2], ISNULL(c.[2], 0))			  [2],	-- Propane
	--	ISNULL(s.[3], ISNULL(c.[3], 0))			  [3],	-- n.BUTANE
	--	ISNULL(s.[4], ISNULL(c.[4], 0))			  [4],	-- i.BUTANE
	--	ISNULL(s.[5], ISNULL(c.[5], 0))			  [5],	-- n.PENTANE
	--	ISNULL(s.[6], ISNULL(c.[6], 0))			  [6],	-- i.PENTANE
	--	ISNULL(s.[7], ISNULL(c.[7], 0))			  [7],	-- Propylene
	--	ISNULL(s.[8], ISNULL(c.[8], 0))			  [8],	-- 1-BUTENE
	--	ISNULL(s.[9], ISNULL(c.[9], 0))			  [9],	-- i-BUTENE
		
	--	ISNULL(s.[10], CASE WHEN f.PYPSFeedType = 0 THEN ISNULL(c.[10], 0) ELSE 0 END)
	--												 [10],	-- Methane
		
	--	ISNULL(s.[15], ISNULL(c.[15], 0))			 [15],	-- Hexanes
	--	ISNULL(s.[16], ISNULL(c.[16], 0))			 [16],	-- Ethylene
		
	--	ISNULL(s.[11], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	1										ELSE 0 END)	[11],
	--	ISNULL(s.[12], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	1										ELSE 0 END)	[12],
	--	ISNULL(s.[13], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	1										ELSE 0 END)	[13],
	--	ISNULL(s.[14], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	1										ELSE 0 END) [14],
		
	--	ISNULL(s.[81], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(p.Density_SG, f.PypsSG)			ELSE 0 END)	[81],
	--	ISNULL(s.[82], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(s.[82], d.D000)					ELSE 0 END)	[82],
	--	ISNULL(s.[83], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(s.[83], d.D010)					ELSE 0 END)	[83],
	--	ISNULL(s.[84], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(s.[84], d.D030)					ELSE 0 END)	[84],
	--	ISNULL(s.[85], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(s.[85], d.D050)					ELSE 0 END)	[85],
	--	ISNULL(s.[86], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(s.[86], d.D070)					ELSE 0 END)	[86],
	--	ISNULL(s.[87], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(s.[87], d.D090)					ELSE 0 END)	[87],
	--	ISNULL(s.[88], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(s.[88], ISNULL(d.D100, d.D095))	ELSE 0 END)	[88],
		
	--	ISNULL(s.[89], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	ISNULL(k.H2 * 100.0, 0)					ELSE 0 END)	[89],
		
	--	ISNULL(s.[90], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN ISNULL(k.P, 0.0) + ISNULL(k.I, 0.0)
	--															ELSE ISNULL(f.PypsP, 0.0) + ISNULL(f.PypsI, 0.0)
	--															END																ELSE 0 END)	[90],
	--	ISNULL(s.[91], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN ISNULL(k.N, 0.0) + ISNULL(k.O, 0.0)
	--															ELSE ISNULL(f.PypsN, 0.0) + ISNULL(f.PypsO, 0.0)
	--															END																ELSE 0 END)	[91],
	--	ISNULL(s.[92], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN ISNULL(k.A, 0)
	--															ELSE ISNULL(f.PypsA, 0)
	--															END																ELSE 0 END)	[92],
	--	ISNULL(s.[93], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 11 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN CASE WHEN k.P <> 0 THEN ISNULL(k.I, 0) / k.P ELSE 0 END
	--															ELSE CASE WHEN f.PypsP <> 0 THEN ISNULL(f.PypsI, 0) / f.PypsP ELSE 0 END
	--															END																ELSE 0 END)	[93],
																
	--	ISNULL(s.[94], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(p.Density_SG, f.PypsSG)			ELSE 0 END)	[94],
	--	ISNULL(s.[95], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(s.[95], d.D000)					ELSE 0 END)	[95],
	--	ISNULL(s.[96], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(s.[96], d.D010)					ELSE 0 END)	[96],
	--	ISNULL(s.[97], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(s.[97], d.D030)					ELSE 0 END)	[97],
	--	ISNULL(s.[98], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(s.[98], d.D050)					ELSE 0 END)	[98],
	--	ISNULL(s.[99], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType)	= 12 THEN	ISNULL(s.[99], d.D070)					ELSE 0 END)	[99],
	--	ISNULL(s.[100], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(s.[100], d.D090)					ELSE 0 END) [100],
	--	ISNULL(s.[101], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(s.[101], ISNULL(d.D100, d.D095))	ELSE 0 END) [101],
	--	ISNULL(s.[102], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 12 THEN	ISNULL(k.H2	* 100.0, 0)					ELSE 0 END) [102],
		
	--	ISNULL(s.[103], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(p.Density_SG, f.PypsSG)			ELSE 0 END) [103],
	--	ISNULL(s.[104], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(s.[104], d.D000)					ELSE 0 END) [104],
	--	ISNULL(s.[105], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(s.[105], d.D010)					ELSE 0 END) [105],
	--	ISNULL(s.[106], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(s.[106], d.D030)					ELSE 0 END) [106],
	--	ISNULL(s.[107], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(s.[107], d.D050)					ELSE 0 END) [107],
	--	ISNULL(s.[108], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(s.[108], d.D070)					ELSE 0 END) [108],
	--	ISNULL(s.[109], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(s.[109], d.D090)					ELSE 0 END) [109],
	--	ISNULL(s.[110], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(s.[110], ISNULL(d.D100, d.D095))	ELSE 0 END) [110],
	--	ISNULL(s.[111], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	ISNULL(k.H2 * 100.0, 0)					ELSE 0 END) [111],
		
	--	ISNULL(s.[112], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN ISNULL(k.P, 0.0) + ISNULL(k.I, 0.0)
	--															ELSE ISNULL(f.PypsP, 0.0) + ISNULL(f.PypsI, 0.0)
	--															END																ELSE 0 END)	[112],
	--	ISNULL(s.[113], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN ISNULL(k.N, 0.0) + ISNULL(k.O, 0.0)
	--															ELSE ISNULL(f.PypsN, 0.0) + ISNULL(f.PypsO, 0.0)
	--															END																ELSE 0 END)	[113],
	--	ISNULL(s.[114], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN ISNULL(k.A, 0)
	--															ELSE ISNULL(f.PypsA, 0)
	--															END																ELSE 0 END)	[114],
	--	ISNULL(s.[115], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	CASE WHEN k.Tot_WtPcnt = 100.0
	--															THEN CASE WHEN k.P <> 0 THEN ISNULL(k.I, 0) / k.P ELSE 0 END
	--															ELSE CASE WHEN f.PypsP <> 0 THEN ISNULL(f.PypsI, 0) / f.PypsP ELSE 0 END
	--															END																ELSE 0 END)	[115],
		
	--	ISNULL(s.[116], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(p.Density_SG, f.PypsSG)			ELSE 0 END) [116],
	--	ISNULL(s.[117], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(s.[117], d.D000)					ELSE 0 END) [117],
	--	ISNULL(s.[118], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(s.[118], d.D010)					ELSE 0 END) [118],
	--	ISNULL(s.[119], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(s.[119], d.D030)					ELSE 0 END) [119],
	--	ISNULL(s.[120], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(s.[120], d.D050)					ELSE 0 END) [120],
	--	ISNULL(s.[121], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(s.[121], d.D070)					ELSE 0 END) [121],
	--	ISNULL(s.[122], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(s.[122], d.D090)					ELSE 0 END) [122],
	--	ISNULL(s.[123], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(s.[123], ISNULL(d.D100, d.D095))	ELSE 0 END) [123],
	--	ISNULL(s.[124], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	ISNULL(k.H2 * 100.0, 0)					ELSE 0 END) [124],
		
	--	ISNULL(s.[125], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 13 THEN	sim.PypsLiquidType(1, ISNULL(p.Density_SG, f.PypsSG), d.D100, q.StreamId)	ELSE 0 END) [125],
	--	ISNULL(s.[126], CASE WHEN ISNULL(k.LiquidFeedType, f.PypsFeedType) = 14 THEN	sim.PypsLiquidType(0, ISNULL(p.Density_SG, f.PypsSG), d.D100, q.StreamId)	ELSE 0 END) [126],
		
	--	LOG(rt.RecycleId) / LOG(2) + 1								[127],
		
	--	ISNULL(s.[128],
	--		ISNULL(o.CoilOutletPressure_kgcm2,
	--		ISNULL(p.CoilOutletPressure_PSIa * 0.07030696,			--	PSIa to kg/cm�
	--		ISNULL(f.PypsCoilOutletPressure_kgcm2, 0)))) 			[128],
			
	--	ISNULL(s.[129], 100.0)										[129],
		
	--	ISNULL(s.[130],
	--		ISNULL(p.SteamHydrocarbon_Ratio,
	--		ISNULL(f.PypsSteamHC_Ratio, 0)))						[130],
			
	--	ISNULL(s.[131],
	--		ISNULL(
	--			CASE s.[SevInd]
	--				WHEN 1 THEN p.EthyleneYield_WtPcnt
	--				WHEN 2 THEN - p.PropyleneEthylene_Ratio
	--				WHEN 3 THEN p.FeedConv_WtPcnt
	--				WHEN 4 THEN p.PropyleneMethane_Ratio
	--				ELSE
	--					CASE
	--						WHEN p.FeedConv_WtPcnt > 0.1			THEN   p.FeedConv_WtPcnt
	--						WHEN p.PropyleneMethane_Ratio > 0.1		THEN   p.PropyleneMethane_Ratio 
	--						WHEN p.PropyleneEthylene_Ratio > 0.02	THEN - p.PropyleneEthylene_Ratio
	--						WHEN p.EthyleneYield_WtPcnt > 0.1		THEN   p.EthyleneYield_WtPcnt
	--					END
	--			END, ISNULL(f.PypsFeedConv, 0)))					[131],
				
	--	ISNULL(s.[132], CASE WHEN ISNULL(s.[SevInd], -1) = 3
	--							OR	CASE
	--									WHEN p.FeedConv_WtPcnt > 0.1			THEN   p.FeedConv_WtPcnt
	--									WHEN p.PropyleneMethane_Ratio > 0.1		THEN   p.PropyleneMethane_Ratio 
	--									WHEN p.PropyleneEthylene_Ratio > 0.02	THEN - p.PropyleneEthylene_Ratio
	--									WHEN p.EthyleneYield_WtPcnt > 0.1		THEN   p.EthyleneYield_WtPcnt
	--								END = p.FeedConv_WtPcnt
	--							THEN ISNULL(c.FeedConvID, ISNULL(f.PypsFeedConvID, 0)) ELSE 0 END)
	--																[132],
																	
	--	-- PYPS Reserved 149 - 158
	--	0.95	[149],
	--	3		[150],
	--	5		[151],
	--	2		[152],
	--	0		[153],
	--	0.5		[154],
	--	0.9		[155],
	--	4		[156],
	--	0		[157],
	--	1		[158]
		
	--FROM fact.QuantityPivot									q
	
	--INNER JOIN cons.TSortSolomon							t
	--	ON	t.Refnum = q.Refnum
		
	--INNER JOIN ante.[SimFeedConfig]								f
	--	ON	f.StreamId = q.StreamId
		
	--INNER JOIN ante.[SimOpCondConfig]							o
	--	ON	o.FactorSetId = f.FactorSetId
	--	AND o.SimModelId = 'PYPS'
		
	--LEFT OUTER JOIN fact.FeedStockCrackingParameters		p
	--	ON	p.Refnum = q.Refnum
	--	AND p.CalDateKey = q.CalDateKey
	--	AND	p.StreamId = q.StreamId
	--	AND p.StreamDescription = q.StreamDescription
		
	--INNER JOIN calc.PlantRecycles(1)						rt
	--	ON	rt.FactorSetId =  f.FactorSetId
	--	AND	rt.Refnum = q.Refnum
	--	AND	rt.CalDateKey = q.CalDateKey

	--LEFT OUTER JOIN sim.PypsComposition						c
	--	ON	c.Refnum = q.Refnum
	--	AND c.CalDateKey = q.CalDateKey
	--	AND	c.StreamId = q.StreamId
	--	AND c.StreamDescription = q.StreamDescription
	--	AND	c.Tot_WtPcnt >= 90.0
		
	--LEFT OUTER JOIN sim.Distillation(@FactorSetId, @Refnum)	d
	--	ON	d.Refnum = q.Refnum
	--	AND d.CalDateKey = q.CalDateKey
	--	AND	d.StreamId = q.StreamId
	--	AND d.StreamDescription = q.StreamDescription
		
	--LEFT OUTER JOIN sim.PIANO								k
	--	ON	k.Refnum = q.Refnum
	--	AND k.CalDateKey = q.CalDateKey
	--	AND	k.StreamId = q.StreamId
	--	AND k.StreamDescription = q.StreamDescription
	--	AND	k.Tot_WtPcnt >= 90.0
		
	--LEFT OUTER JOIN(
	--	SELECT
	--		s.FieldId,
	--		s.Value,
	--		s.Active,
			
	--		s.FactorSetId,
	--		s.OpCondId,
			
	--		s.Refnum,
	--		s.CalDateKey,
	--		s.StreamId,
	--		s.StreamDescription
	--	FROM sim.Supersede s
	--	WHERE	s.DataSetId = 'Input'
	--		AND	s.SimModelId = 'PYPS'
	--		AND	s.Active = 1
	--	) u
	--	PIVOT (
	--	MAX(u.Value) FOR u.FieldId IN (
	--		[1], [2], [3], [4], [5], [6], [7], [8], [9],
	--		[10], [11], [12], [13], [14], [15], [16],
	--			  [81], [82], [83], [84], [85], [86], [87], [88], [89],
	--		[90], [91], [92], [93], [94], [95], [96], [97], [98], [99],
	--		[100], [101], [102], [103], [104], [105], [106], [107], [108], [109],
	--		[110], [111], [112], [113], [114], [115], [116], [117], [118], [119],
	--		[120], [121], [122], [123], [124], [125], [126],		[128], [129], -- 127 is RecycleType, all types are simulated
	--		[130], [131], [132],
	--		[SevInd]
	--		)
	--	) s
	--	ON	s.FactorSetId = f.FactorSetId
	--	AND ISNULL(s.OpCondId, o.OpCondId) = o.OpCondId
	--	AND	ISNULL(s.Refnum, q.Refnum) = q.Refnum
	--	AND ISNULL(s.CalDateKey, q.CalDateKey) = q.CalDateKey
	--	AND	ISNULL(s.StreamId, q.StreamId) = q.StreamId
	--	AND	ISNULL(s.StreamDescription, q.StreamDescription) = q.StreamDescription
		
	--WHERE	f.FactorSetId = @FactorSetId 
	--	AND	q.Refnum = @Refnum 
	--
);
END;