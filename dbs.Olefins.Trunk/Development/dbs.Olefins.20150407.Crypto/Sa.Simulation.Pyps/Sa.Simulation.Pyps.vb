﻿
''' <summary>
''' Extends the simulation error codes to PYPS.
''' </summary>
''' <remarks></remarks>
Friend Class ErrorCode
    Inherits Sa.Simulation.ErrorCode

    Public Const ProcessPlatform As System.Int32 = -25

    Public Const NapInReset As System.Int32 = -26
    Public Const MDLOUT As System.Int32 = -27
    Public Const MdlOutErr As System.Int32 = -28

    Public Const DistTemp As System.Int32 = 1

    ''' <summary>
    ''' USRSOL.EXE exited improperly.
    ''' </summary>
    ''' <remarks>USRSOL.EXE returns the the exit code <c>129</c> when failing.</remarks>
    Public Const UsrSolExit As System.Int32 = 129

    ''' <summary>
    ''' USRSOL.EXE enountered a <c>divide by zero</c> operation.
    ''' </summary>
    ''' <remarks>USRSOL.EXE returns the exit code <c>131</c> when a <c>divide by zero</c> error occurs</remarks>
    Public Const UsrSolDiv As System.Int32 = 131

End Class

''' <summary>
''' Runs the Lummus PYPS Simulation.
''' </summary>
''' <remarks>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </remarks>
<System.Runtime.Remoting.Contexts.Synchronization()>
Friend NotInheritable Class Simulation

    <System.ThreadStatic()> Private Items As System.Int32 = 0
    <System.ThreadStatic()> Private BgwOr As Sa.Simulation.Pyps.BackgroundWorker

    Private Const SimModelID As String = "PYPS"
    Private Const fCheckSum As String = "Hash\Sa.Simulation.Pyps.SHA512.txt"

    Friend Sub New(ByVal BGWs As Sa.Simulation.Pyps.BackgroundWorker)
        Me.BgwOr = BGWs
    End Sub

    ''' <summary>
    ''' Runs an instance of the Lummus PYPS simulation.
    ''' </summary>
    ''' <param name="a">Argument to pass variables to filter records to be simulated</param>
    ''' <remarks></remarks>
    <System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)>
    Friend Sub StartSimulation(
        ByVal a As Sa.Simulation.Threading.Argument)

        If Me.BgwOr.CancellationPending Then Exit Sub

        Dim PathEngine As String = Sa.AssemblyInfo.PathApplication + SimModelID + "\"
        Dim EngineCheckSum As String = Sa.AssemblyInfo.PathApplication + fCheckSum

        Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Verify|" + fCheckSum)

        ' Verify orignal files
        If Sa.FileOps.VerifySourceFiles(EngineCheckSum, PathEngine) Then

            Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Starting|Deleting previous results")

            Sa.Simulation.SQL.StoredProcedures.SimQueueUpdate(a.ConnectionString, a.QueueID, , DateTimeOffset.Now())
            Sa.Simulation.SQL.StoredProcedures.SimulationResultsDelete(a.ConnectionString, SimModelID, a.FactorSetID, a.Refnum)

            Using cn As New System.Data.SqlClient.SqlConnection(a.ConnectionString)

                Using cmd As New System.Data.SqlClient.SqlCommand("sim.InputPyps", cn)

                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.Add("@FactorSetID", SqlDbType.VarChar, 12).Value = a.FactorSetID
                    cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = a.Refnum

                    cn.Open()

                    Dim sError As String = "0" 'Nothing

                    Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Starting|Retrieving records to process")

                    Using rdr As System.Data.SqlClient.SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleResult)

                        If rdr.HasRows Then sError = Me.LoopFeeds(a.ConnectionString, a.QueueID, a.Refnum, a.FactorSetID, rdr, a.DeleteFolder)

                    End Using

                    If sError = CType(Sa.Simulation.Pyps.ErrorCode.None, String) Then

                        Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.None, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Calculating|Calculating Plant")

                        Dim r As System.Int32 = 0

                        r = Sa.Simulation.SQL.StoredProcedures.YieldCompositionPlantInsert(a.ConnectionString, a.FactorSetID, SimModelID, a.Refnum)

                        If r <= 0 Then
                            Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.PlantComposition, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Error|Calculating Plant Composition")
                            sError = "Plant Composition"
                        End If

                        r = Sa.Simulation.SQL.StoredProcedures.EnergyConsumptionPlantInsert(a.ConnectionString, a.FactorSetID, SimModelID, a.Refnum)

                        If r <= 0 Then
                            Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.PlantComposition, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Error|Calculating Plant Energy Consumption")
                            sError = "Plant Energy Consumption"
                        End If

                    End If

                    Sa.Simulation.SQL.StoredProcedures.SimQueueUpdate(a.ConnectionString, a.QueueID, , , DateTimeOffset.Now(), , Me.Items, sError)

                    Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.Finished, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Finished|Simulation Exit Code: " + sError)

                End Using

            End Using

        Else

            Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.SourceFiles, a.QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + a.Refnum + "|" + a.FactorSetID + "|Error|Stopping simulation... Corrupted Source Files")

            Sa.Log.Write(SimModelID + " Source files in " + PathEngine + ControlChars.CrLf + "are corrupted or could not be verified." + ControlChars.CrLf + ControlChars.CrLf + "Verify these files against " + EngineCheckSum + ".", EventLogEntryType.Error, True, Sa.Log.LogName, 0)

        End If

    End Sub

    ''' <summary>
    ''' Prepares feeds for processing.
    ''' </summary>
    ''' <param name="CnString">Connection String</param>
    ''' <param name="QueueID">QueueID (sim.SimQueue)</param>
    ''' <param name="rdr">SqlDataReader for providing the values used in the simulation.</param>
    ''' <param name="DeleteFolder">Flag to delete the simulation folder: True - Deletes the folder; False - Keeps the folder</param>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Private Function LoopFeeds(
            ByVal CnString As String,
            ByVal QueueID As System.Int64,
            ByVal Refnum As String,
            ByVal FactorSetID As String,
            ByVal rdr As System.Data.SqlClient.SqlDataReader,
            ByVal DeleteFolder As Boolean) As String

        Dim sLoopFeeds As String = CType(Sa.Simulation.Pyps.ErrorCode.Initilization, String)

        Dim PathEngine As String = Sa.AssemblyInfo.PathApplication + SimModelID + "\"
        Dim PathEngineTarget As String = Sa.AssemblyInfo.SystemDrive + "PyrolysisSimulations\" + SimModelID + "\"

        Dim pSimulation As String = Sa.Simulation.Actions.CopyEngine(PathEngine, PathEngineTarget, System.Guid.NewGuid.ToString())

        Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.None, QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + Refnum + "|" + FactorSetID + "|Path|" + pSimulation)

        If Sa.FileOps.VerifyFolder(PathEngine, pSimulation) Then

            Dim aError As System.Int32 = Sa.Simulation.Pyps.ErrorCode.Initilization
            Dim iError As System.Int32 = Sa.Simulation.Pyps.ErrorCode.Initilization

            Dim PypsApp As New PypsApplication

            Do While rdr.Read()

                If Me.BgwOr.CancellationPending Then aError = Sa.Simulation.Pyps.ErrorCode.Cancel : Exit Do

                Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.None, QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + rdr.GetString(rdr.GetOrdinal("Refnum")) + "|" + FactorSetID + "|Running|" + rdr.GetString(rdr.GetOrdinal("StreamID")) + " (" + rdr.GetString(rdr.GetOrdinal("StreamDescription")) + "): " + rdr.GetString(rdr.GetOrdinal("OpCondID")))

                iError = PypsApp.SimulateFeed(CnString, QueueID, rdr, pSimulation)

                If iError <> Sa.Simulation.Pyps.ErrorCode.None Then

                    Me.BgwOr.ReportProgress(Sa.Simulation.Pyps.ErrorCode.FeedError, QueueID.ToString(System.Globalization.NumberFormatInfo.InvariantInfo) + "|" + SimModelID + "|" + rdr.GetString(rdr.GetOrdinal("Refnum")) + "|" + FactorSetID + "|Error (" + CType(iError, String) + ")|" + rdr.GetString(rdr.GetOrdinal("StreamID")) + " (" + rdr.GetString(rdr.GetOrdinal("StreamDescription")) + "): " + rdr.GetString(rdr.GetOrdinal("OpCondID")))
                    Sa.Log.Write(SimModelID + " Error simulating feed:" + ControlChars.CrLf + rdr.GetString(rdr.GetOrdinal("Refnum")) + ControlChars.CrLf + FactorSetID + ControlChars.CrLf + rdr.GetString(rdr.GetOrdinal("StreamID")) + "(" + rdr.GetString(rdr.GetOrdinal("StreamDescription")) + ")" + ControlChars.CrLf + rdr.GetString(rdr.GetOrdinal("OpCondID")), EventLogEntryType.Error, False, Sa.Log.LogName, 0)

                End If

                If aError = Sa.Simulation.Pyps.ErrorCode.None Or aError = Sa.Simulation.Pyps.ErrorCode.Initilization Then
                    aError = iError
                Else
                    If aError <> iError And iError <> Sa.Simulation.Pyps.ErrorCode.None Then aError = Sa.Simulation.Pyps.ErrorCode.Notice
                End If

                Me.Items = Me.Items + 1

            Loop

            PypsApp = Nothing

            sLoopFeeds = CType(aError, String)

        Else

            sLoopFeeds = CType(Sa.Simulation.Pyps.ErrorCode.SourceFiles, String)

        End If

        If DeleteFolder Then Sa.FileOps.DeleteFolder(pSimulation)

        Return sLoopFeeds

    End Function

End Class

''' <summary>
''' Lummus PYPS Simulation Engine.
''' </summary>
''' <remarks>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </remarks>
<System.Runtime.Remoting.Contexts.Synchronization()>
Friend NotInheritable Class PypsApplication

    Private Const SimModelID As String = "PYPS"
    Private Const TimeOut As System.Int32 = 5000

#Region " Simulation Variables "

    Private Const eNapInN As System.Int32 = 11
    Private Const eNapInG As System.Int32 = 12
    Private Const eNapIn1 As System.Int32 = 13
    Private Const eNapIn2 As System.Int32 = 14

    Private Const fMain As String = "USRSOL.EXE"

    Private Const fInput As String = "SOLINP.DAT"
    Private Const fYield As String = "MDLOUT.DAT"
    Private Const fTemp As String = "TEMP1.DAT"
    Private Const fCoeyld As String = "COEYLD.DAT"
    Private Const fNapIn As String = "NAPIN.DAT"

    Private Const PypsFieldCount As System.Int32 = 158

#End Region

    ''' <summary>
    ''' Sequentially executes the simulation for a set of feeds.
    ''' </summary>
    ''' <param name="CnString">Connection String</param>
    ''' <param name="QueueID">QueueID (sim.SimQueue)</param>
    ''' <param name="rdr">SqlDataReader for providing the values used in the simulation.</param>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Friend Function SimulateFeed(
        ByVal CnString As String,
        ByVal QueueID As System.Int64,
        ByVal rdr As System.Data.SqlClient.SqlDataReader,
        ByVal pSimulation As String) As System.Int32

        Dim nErrorID As System.Int32 = Sa.Simulation.Pyps.ErrorCode.None
        Dim SolInp() As String = Nothing

        If nErrorID = Sa.Simulation.Pyps.ErrorCode.None Then nErrorID = Me.PrepareFiles(pSimulation)

        SolInp = Me.PrepareSolInp(rdr)

        If nErrorID = Sa.Simulation.Pyps.ErrorCode.None Then nErrorID = Me.WriteSolInp(pSimulation, SolInp)

        If nErrorID = Sa.Simulation.Pyps.ErrorCode.None Then nErrorID = Me.ProcessPyps(pSimulation)

        If nErrorID = Sa.Simulation.Pyps.ErrorCode.None Then nErrorID = Me.VerifyMdlOut(pSimulation)

        If nErrorID = Sa.Simulation.Pyps.ErrorCode.None AndAlso Me.IsLiquid(SolInp) Then

            If nErrorID = Sa.Simulation.Pyps.ErrorCode.None Then nErrorID = Me.VerifyNapIn(pSimulation, SolInp)

            If nErrorID <> Sa.Simulation.Pyps.ErrorCode.None Then nErrorID = Me.ResetNapIn(pSimulation, SolInp)

            If nErrorID = Sa.Simulation.Pyps.ErrorCode.None Then nErrorID = Me.VerifyMdlOut(pSimulation)

        End If

        nErrorID = Me.PutYield(CnString, QueueID, rdr, pSimulation, nErrorID)

        Return nErrorID

    End Function

    ''' <summary>
    ''' Delete and copy files used during simulation.
    ''' </summary>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Friend Function PrepareFiles(
            ByVal pSimulation As String) As System.Int32

        Dim iPrepareFiles As System.Int32 = Sa.Simulation.Pyps.ErrorCode.SourceFiles

        Sa.FileOps.DeleteFile(pSimulation + fInput)
        Sa.FileOps.DeleteFile(pSimulation + fYield)
        Sa.FileOps.DeleteFile(pSimulation + fTemp)

        Sa.FileOps.DeleteFile(pSimulation + fCoeyld)
        Sa.FileOps.DeleteFile(pSimulation + fNapIn)

        Sa.FileOps.CopyFile(Sa.AssemblyInfo.PathApplication + SimModelID + "\" + fCoeyld, pSimulation + fCoeyld)
        Sa.FileOps.CopyFile(Sa.AssemblyInfo.PathApplication + SimModelID + "\" + fNapIn, pSimulation + fNapIn)

        If Not IO.File.Exists(pSimulation + fInput) AndAlso
            Not IO.File.Exists(pSimulation + fYield) AndAlso
            Not IO.File.Exists(pSimulation + fTemp) AndAlso
            IO.File.Exists(pSimulation + fCoeyld) AndAlso
            IO.File.Exists(pSimulation + fNapIn) AndAlso
            Sa.FileOps.VerifyFile(Sa.AssemblyInfo.PathApplication + SimModelID + "\" + fCoeyld, pSimulation + fCoeyld) AndAlso
            Sa.FileOps.VerifyFile(Sa.AssemblyInfo.PathApplication + SimModelID + "\" + fNapIn, pSimulation + fNapIn) Then iPrepareFiles = Sa.Simulation.Pyps.ErrorCode.None

        Return iPrepareFiles

    End Function

    ''' <summary>
    ''' Creates the input array to be used when creating the input file.
    ''' </summary>
    ''' <param name="rdr">SqlDataReader for providing the values used in the simulation.</param>
    ''' <returns>String()</returns>
    ''' <remarks></remarks>
    Private Function PrepareSolInp(
        ByVal rdr As System.Data.SqlClient.SqlDataReader) As String()

        Dim arr(PypsFieldCount) As String

        Dim fName As String = Nothing

        For j As System.Int32 = 1 To PypsFieldCount Step 1

            fName = CType(j, String)

            If Sa.Simulation.SQL.StoredProcedures.FieldExists(rdr, fName) AndAlso Not rdr.GetValue(rdr.GetOrdinal(fName)) Is DBNull.Value Then
                arr(j) = rdr.GetFloat(rdr.GetOrdinal(fName)).ToString(System.Globalization.NumberFormatInfo.InvariantInfo)
            Else
                arr(j) = "0"
            End If

        Next j

        Return arr

    End Function

    ''' <summary>
    ''' Writes the input array to the SOLINPT.DAT file.
    ''' </summary>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <param name="aSolInp">String array of the input data.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Friend Function WriteSolInp(
        ByVal pSimulation As String,
        ByVal aSolInp() As String) As System.Int32

        Dim iWriteSolInput As System.Int32 = Sa.Simulation.Pyps.ErrorCode.Prepare

        Dim fValue As String = ""

        For i As System.Int32 = 1 To aSolInp.GetUpperBound(0) Step 1
            fValue = fValue + " " + aSolInp(i) + " "
        Next i

        System.IO.File.WriteAllText(pSimulation + fInput, fValue, System.Text.Encoding.ASCII)

        If IO.File.Exists(pSimulation + fInput) Then iWriteSolInput = Sa.Simulation.Pyps.ErrorCode.None

        Return iWriteSolInput

    End Function

    ''' <summary>
    ''' Prepares the USRSOL.EXE process for execution
    ''' </summary>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks>Sets process parameters and verifys the process completed properly.  If USRSOL.EXE exits improperly, the process is run again.</remarks>
    Friend Function ProcessPyps(
        ByVal pSimulation As String) As System.Int32

        Dim iProcessPyps As System.Int32 = Sa.Simulation.Pyps.ErrorCode.Process

        Using proc As New System.Diagnostics.Process

            proc.EnableRaisingEvents = True

            proc.StartInfo.FileName = pSimulation + fMain
            proc.StartInfo.CreateNoWindow = True
            proc.StartInfo.UseShellExecute = False
            proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            proc.StartInfo.WorkingDirectory = pSimulation

            iProcessPyps = Me.RunPyps(proc, TimeOut)

            ' Re-run PYPS if improper exit.  Occasionally, PYPS will improperly exit but will normally run when executed a second time.
            If iProcessPyps = Sa.Simulation.Pyps.ErrorCode.UsrSolExit Then
                iProcessPyps = Me.RunPyps(proc, TimeOut)
            End If

        End Using

        Return iProcessPyps

    End Function

    ''' <summary>
    ''' Executes USRSOL.EXE and verifies is exit code.
    ''' </summary>
    ''' <param name="proc">Defined process object for USRSOL.EXE</param>
    ''' <param name="TimeOut">Number of milliseconds to pass before exiting USRSOL.EXE.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks>Starts USRSOL.EXE and waits for execution to complete.  If execution does not complete, the process is killed.
    ''' Killing the process removes possible file locks.</remarks>
    Friend Function RunPyps(
        ByVal proc As System.Diagnostics.Process,
        ByVal TimeOut As System.Int32) As System.Int32

        Dim iRunPyps As System.Int32 = Sa.Simulation.Pyps.ErrorCode.Worker

        Try

            proc.Start()
            proc.WaitForExit(TimeOut)

            If proc.HasExited Then
                iRunPyps = proc.ExitCode
            Else
                Try
                    proc.Kill()
                Catch ex As InvalidOperationException
                    Sa.Log.Write("Could not kill " & proc.ProcessName & " (" & proc.Id & ") process", EventLogEntryType.Error, False, Sa.Log.LogName, 0)
                End Try
            End If

        Catch ex As System.ComponentModel.Win32Exception

            iRunPyps = Sa.Simulation.Pyps.ErrorCode.ProcessPlatform

        End Try

        Return iRunPyps

    End Function

    ''' <summary>
    ''' Determines if the feed is a liquid feed.
    ''' </summary>
    ''' <param name="aSolInpt">String array of the input data.</param>
    ''' <returns>Boolean</returns>
    ''' <remarks>Examines the four feed type indicators to determine if the feed is a liquid feed.</remarks>
    Friend Function IsLiquid(
        ByVal aSolInpt() As String) As Boolean

        Dim bIsLiquid As Boolean = False

        If CType(aSolInpt(eNapInN), System.Int32) = 1 Or
            CType(aSolInpt(eNapInG), System.Int32) = 1 Or
            CType(aSolInpt(eNapIn1), System.Int32) = 1 Or
            CType(aSolInpt(eNapIn2), System.Int32) = 1 Then

            bIsLiquid = True

        End If

        Return bIsLiquid

    End Function

    ''' <summary>
    ''' Verifies distillation temperatures in NAPIN.DAT match those in the input.
    ''' </summary>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <param name="aSolInpt">String array of the input data.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks>NAPIN.DAT stores the distallation temperatures used when simulating liquid feeds.
    ''' The NAPIN.DAT seed file has all values set to zero (0).  This seed file should be updated when simulating a liquid feed.</remarks>
    Friend Function VerifyNapIn(
        ByVal pSimulation As String,
        ByVal aSolInpt() As String) As System.Int32

        Dim iVerifyNapIn As System.Int32 = Me.IBP(aSolInpt)

        Dim iIBP As System.Int32 = iVerifyNapIn

        Dim sNapIn As String = System.IO.File.ReadAllText(pSimulation + fNapIn, System.Text.Encoding.ASCII).Replace(Chr(32), ControlChars.CrLf)
        Dim aNapIn() As String = sNapIn.Split(ControlChars.CrLf.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        Dim e As System.Int32 = 0

        ' Verify that all seven (7) distillation temperatures have been updated by USERSOL.EXE
        For j As System.Int32 = 5 To 11 Step 1

            If Single.Parse(aNapIn(j), System.Globalization.NumberFormatInfo.InvariantInfo) = Single.Parse(aSolInpt(j + iIBP - 5), System.Globalization.NumberFormatInfo.InvariantInfo) Then
                e = e + 1
            End If

        Next j

        If e = 7 Then iVerifyNapIn = Sa.Simulation.Pyps.ErrorCode.None

        Return iVerifyNapIn

    End Function

    ''' <summary>
    ''' Gets the field index (Base 1) of the Initial Boiling Point.
    ''' </summary>
    ''' <param name="aSolInpt">String array of the input data.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Friend Function IBP(
        ByVal aSolInpt() As String) As System.Int32

        Dim iIBP As System.Int32 = 0

        Try

            Select Case 1
                Case CType(aSolInpt(eNapInN), System.Int32)
                    iIBP = 82
                Case CType(aSolInpt(eNapInG), System.Int32)
                    iIBP = 95
                Case CType(aSolInpt(eNapIn1), System.Int32)
                    iIBP = 104
                Case CType(aSolInpt(eNapIn2), System.Int32)
                    iIBP = 117
            End Select

        Catch ex As IndexOutOfRangeException

        End Try

        Return iIBP

    End Function

    ''' <summary>
    ''' Attempts to reset NAPIN.DAT, a file used to simulate liquid feeds.
    ''' </summary>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <param name="aSolInpt">String array of the input data.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks>
    ''' NAPIN.DAT contains distillation temperatures for the last liquid feed run.
    ''' This procedure changes the IBP and specific gravity, then runs the simulation.
    ''' After the running the simulation, the original distallation is used and runs the simulation again.
    ''' </remarks>
    Friend Function ResetNapIn(
        ByVal pSimulation As String,
        ByVal aSolInpt() As String) As System.Int32

        Dim iResetNapIn As System.Int32 = Sa.Simulation.Pyps.ErrorCode.NapInReset

        Dim tSolInpt(aSolInpt.GetUpperBound(0)) As String

        Array.Copy(aSolInpt, tSolInpt, aSolInpt.Length)

        Dim n As Single = Nothing
        Dim iIBP As System.Int32 = Me.IBP(tSolInpt)

        ' Change the initial boiling point
        n = Single.Parse(tSolInpt(iIBP), System.Globalization.NumberFormatInfo.InvariantInfo) - CType(5.0, Single)
        tSolInpt(iIBP) = n.ToString(System.Globalization.NumberFormatInfo.InvariantInfo)

        ' Change the Specific Gravity
        n = Single.Parse(tSolInpt(iIBP - 1), System.Globalization.NumberFormatInfo.InvariantInfo) + CType(0.1, Single)
        tSolInpt(iIBP - 1) = n.ToString(System.Globalization.NumberFormatInfo.InvariantInfo)

        Dim e As System.Int32 = Me.PrepareFiles(pSimulation)

        ' Delete the original input and yield files
        Sa.FileOps.DeleteFile(pSimulation + fInput)
        Sa.FileOps.DeleteFile(pSimulation + fYield)

        ' Write the temporary input file and run the simulation
        If e = Sa.Simulation.Pyps.ErrorCode.None Then e = Me.WriteSolInp(pSimulation, tSolInpt)
        If e = Sa.Simulation.Pyps.ErrorCode.None Then e = Me.ProcessPyps(pSimulation)

        ' Delete the temporary input and yield files
        Sa.FileOps.DeleteFile(pSimulation + fInput)
        Sa.FileOps.DeleteFile(pSimulation + fYield)

        ' Write the original input file and run the simulation
        If e = Sa.Simulation.Pyps.ErrorCode.None Then e = Me.WriteSolInp(pSimulation, aSolInpt)
        If e = Sa.Simulation.Pyps.ErrorCode.None Then e = Me.ProcessPyps(pSimulation)

        ' Verify the simulation properly ran
        If e = Sa.Simulation.Pyps.ErrorCode.None Then e = Me.VerifyNapIn(pSimulation, aSolInpt)

        If e = Sa.Simulation.Pyps.ErrorCode.None Then iResetNapIn = Sa.Simulation.Pyps.ErrorCode.None

        Return iResetNapIn

    End Function

    ''' <summary>
    ''' Verifies MDLOUT.DAT was saved to the working directory.
    ''' </summary>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Friend Function VerifyMdlOut(
        ByVal pSimulation As String) As System.Int32

        Dim iVerifyMdlOut As System.Int32 = Sa.Simulation.Pyps.ErrorCode.MDLOUT

        If IO.File.Exists(pSimulation + fYield) Then iVerifyMdlOut = Sa.Simulation.Pyps.ErrorCode.None

        Return iVerifyMdlOut

    End Function

    ''' <summary>
    ''' Prepares the yield from MDLOUT.DAT for input to the database.
    ''' </summary>
    ''' <param name="CnString">Connection String</param>
    ''' <param name="QueueID">QueueID (sim.SimQueue) </param>
    ''' <param name="rdr"></param>
    ''' <param name="pSimulation">Path of the working directory.</param>
    ''' <param name="nErrorID">Error (dim.SimErrorMessagesLu)</param>
    ''' <returns>System.Int32</returns>
    ''' <remarks></remarks>
    Private Function PutYield(
        ByVal CnString As String,
        ByVal QueueID As System.Int64,
        ByVal rdr As System.Data.SqlClient.SqlDataReader,
        ByVal pSimulation As String,
        ByVal nErrorID As System.Int32) As System.Int32

        Dim iPutYield As System.Int32 = Sa.Simulation.Pyps.ErrorCode.EnergyAndComposition

        If nErrorID = Sa.Simulation.Pyps.ErrorCode.None Then

            Dim sYield As String = System.IO.File.ReadAllText(pSimulation + fYield, System.Text.Encoding.ASCII)
            Dim aYield() As String = sYield.Split(ControlChars.CrLf.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

            If aYield.Length = 25 Then

                Dim sEnergy As String = Nothing
                Dim nEnergy As Single = Nothing
                Dim eErrorID As System.Int32 = Sa.Simulation.Pyps.ErrorCode.Energy

                Dim strOpCondID As String = rdr.GetString(rdr.GetOrdinal("OpCondID"))

                If strOpCondID <> "MS25" Then
                    sEnergy = aYield(24).Substring(20, 10).Trim
                Else
                    sEnergy = aYield(24).Substring(31, aYield(24).Length - 31).Trim
                End If

                If sEnergy <> "**********" AndAlso IsNumeric(sEnergy) Then
                    nEnergy = Single.Parse(sEnergy, System.Globalization.NumberFormatInfo.InvariantInfo)
                Else
                    sEnergy = Nothing
                End If

                If nEnergy > 0 Then eErrorID = Sa.Simulation.Pyps.ErrorCode.None Else eErrorID = Sa.Simulation.Pyps.ErrorCode.Energy

                Sa.Simulation.SQL.StoredProcedures.PutYieldHeader(CnString, QueueID, SimModelID, rdr, eErrorID, nEnergy)

                Dim sComp As String = Nothing

                Dim sPcnt As String = Nothing
                Dim nPcnt As Single = Nothing

                Dim CompErr As Boolean = False

                For j As System.Int32 = 4 To aYield.Count - 3 Step 1

                    sComp = Me.ConvComponentId(aYield(j).Substring(1, 14).Trim)

                    If strOpCondID <> "MS25" Then
                        sPcnt = aYield(j).Substring(18, 10).Trim
                    Else
                        sPcnt = aYield(j).Substring(31, aYield(j).Length - 31).Trim
                    End If

                    If sPcnt <> "**********" AndAlso IsNumeric(sPcnt) Then
                        nPcnt = Single.Parse(sPcnt, System.Globalization.NumberFormatInfo.InvariantInfo)
                    Else
                        sPcnt = Nothing
                    End If

                    Sa.Simulation.SQL.StoredProcedures.YieldCompositionStreamInsert(CnString, QueueID, SimModelID, rdr, sComp, nPcnt)

                    If CompErr = False And (nPcnt < 0.0 Or nPcnt > 100.0) Then CompErr = True

                Next j

                If CompErr = True Then

                    If eErrorID = Sa.Simulation.Pyps.ErrorCode.Energy Then iPutYield = Sa.Simulation.Pyps.ErrorCode.EnergyAndComposition Else iPutYield = Sa.Simulation.Pyps.ErrorCode.Composition

                    Sa.Simulation.SQL.StoredProcedures.EnergyConsumptionStreamUpdate(CnString, QueueID, SimModelID, rdr, CType(iPutYield, String), Nothing, nEnergy)

                Else

                    If eErrorID = Sa.Simulation.Pyps.ErrorCode.Energy Then iPutYield = Sa.Simulation.Pyps.ErrorCode.Energy Else iPutYield = Sa.Simulation.Pyps.ErrorCode.None

                End If

            Else

                iPutYield = Sa.Simulation.Pyps.ErrorCode.MdlOutErr

            End If

        Else

            iPutYield = nErrorID

            Sa.Simulation.SQL.StoredProcedures.PutYieldHeader(CnString, QueueID, SimModelID, rdr, iPutYield, Nothing)

        End If

        Return iPutYield

    End Function

    ''' <summary>
    ''' Converts the MDLOUT.DAT composition names to the Solomon ComponentID (dim.CompositionLu)
    ''' </summary>
    ''' <param name="sComponent">MDLOUT.DAT compsition name.</param>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' C9-400F and FUEL OIL transformation defined by Calcs!YIRCalc (Yield Prediction)
    ''' </remarks>
    Friend Function ConvComponentId(
        ByVal sComponent As String) As String
        Dim sConvComponentId As String = Nothing

        Select Case sComponent
            Case "HYDROGEN" : sConvComponentId = "H2"
            Case "METHANE" : sConvComponentId = "CH4"
            Case "ACETYLENE" : sConvComponentId = "C2H2"
            Case "ETHYLENE" : sConvComponentId = "C2H4"
            Case "ETHANE" : sConvComponentId = "C2H6"
            Case "MA + PD" : sConvComponentId = "C3H4"
            Case "PROPYLENE" : sConvComponentId = "C3H6"
            Case "PROPANE" : sConvComponentId = "C3H8"
            Case "BUTADIENE" : sConvComponentId = "C4H6"
            Case "BUTENE" : sConvComponentId = "C4H8"
            Case "BUTANE" : sConvComponentId = "C4H10"
            Case "C5-S" : sConvComponentId = "C5S"
            Case "C6-C8 NA" : sConvComponentId = "C6C8NA"
            Case "BENZENE" : sConvComponentId = "C6H6"
            Case "TOLUENE" : sConvComponentId = "C7H8"
            Case "XY + ETB" : sConvComponentId = "C8H10"
            Case "STYRENE" : sConvComponentId = "C8H8"
            Case "C9-400F" : sConvComponentId = "PyroGasOil"      ' Per YIR Calcs Transform of yields
            Case "FUEL OIL" : sConvComponentId = "PyroFuelOil"    ' Per YIR Calcs Transform of yields
        End Select

        Return sConvComponentId

    End Function

End Class


