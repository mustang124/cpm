﻿CREATE TABLE [stgFact].[PracMPC] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Tbl9020]        TINYINT            NULL,
    [Tbl9021]        TINYINT            NULL,
    [Tbl9022]        TINYINT            NULL,
    [Tbl9023]        TINYINT            NULL,
    [Tbl9024]        TINYINT            NULL,
    [Tbl9025]        TINYINT            NULL,
    [Tbl9026]        TINYINT            NULL,
    [Tbl9027]        TINYINT            NULL,
    [Tbl9028]        TINYINT            NULL,
    [Tbl9029]        TINYINT            NULL,
    [Tbl9030]        REAL               NULL,
    [Tbl9031]        REAL               NULL,
    [Tbl9032]        REAL               NULL,
    [Tbl9033]        TINYINT            NULL,
    [Tbl9034]        TINYINT            NULL,
    [Tbl9035]        SMALLINT           NULL,
    [Tbl9036]        TINYINT            NULL,
    [Tbl9037]        SMALLINT           NULL,
    [Tot9020_28]     TINYINT            NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PracMPC_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracMPC_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracMPC_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracMPC_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PracMPC] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PracMPC_u]
	ON [stgFact].[PracMPC]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PracMPC]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PracMPC].Refnum		= INSERTED.Refnum;

END;