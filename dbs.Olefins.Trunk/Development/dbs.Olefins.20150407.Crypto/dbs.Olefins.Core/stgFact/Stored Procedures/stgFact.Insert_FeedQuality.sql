﻿CREATE PROCEDURE [stgFact].[Insert_FeedQuality]
(
	@Refnum					VARCHAR (25),
	@FeedProdID				VARCHAR (20),

	@Methane				REAL					= NULL,
	@Ethane					REAL					= NULL,
	@Ethylene				REAL					= NULL,
	@Propane				REAL					= NULL,
	@Propylene				REAL					= NULL,
	@nButane				REAL					= NULL,
	@iButane				REAL					= NULL,
	@Isobutylene			REAL					= NULL,
	@Butene1				REAL					= NULL,
	@Butadiene				REAL					= NULL,
	@nPentane				REAL					= NULL,
	@iPentane				REAL					= NULL,
	@nHexane				REAL					= NULL,
	@iHexane				REAL					= NULL,
	@Septane				REAL					= NULL,
	@Octane					REAL					= NULL,
	@CO2					REAL					= NULL,
	@Hydrogen				REAL					= NULL,
	@Sulfur					REAL					= NULL,
	@CoilOutletPressure		REAL					= NULL,
	@SteamHCRatio			REAL					= NULL,
	@CoilOutletTemp			REAL					= NULL,
	@EthyleneYield			REAL					= NULL,
	@PropylEthylRatio		REAL					= NULL,
	@FeedConv				REAL					= NULL,
	@PropylMethaneRatio		REAL					= NULL,
	@ConstrYear				INT						= NULL,
	@ResTime				REAL					= NULL,
	@SulfurPPM				REAL					= NULL,
	@ASTMethod				VARCHAR (10)			= NULL,
	@IBP					REAL					= NULL,
	@D5pcnt					REAL					= NULL,
	@D10Pcnt				REAL					= NULL,
	@D30Pcnt				REAL					= NULL,
	@D50Pcnt				REAL					= NULL,
	@D70Pcnt				REAL					= NULL,
	@D90Pcnt				REAL					= NULL,
	@D95Pcnt				REAL					= NULL,
	@EP						REAL					= NULL,
	@NParaffins				REAL					= NULL,
	@IsoParaffins			REAL					= NULL,
	@Naphthenes				REAL					= NULL,
	@Olefins				REAL					= NULL,
	@Aromatics				REAL					= NULL,
	@SG						REAL					= NULL,
	@OthLiqFeedDESC			VARCHAR (50)			= NULL,
	@OthLiqFeedPriceBasis	REAL					= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FeedQuality]([Refnum], [FeedProdID], [Methane], [Ethane], [Ethylene], [Propane], [Propylene], [nButane], [iButane], [Isobutylene], [Butene1], [Butadiene], [nPentane], [iPentane], [nHexane], [iHexane], [Septane], [Octane], [CO2], [Hydrogen], [Sulfur], [CoilOutletPressure], [SteamHCRatio], [CoilOutletTemp], [EthyleneYield], [PropylEthylRatio], [FeedConv], [PropylMethaneRatio], [ConstrYear], [ResTime], [SulfurPPM], [ASTMethod], [IBP], [D5pcnt], [D10Pcnt], [D30Pcnt], [D50Pcnt], [D70Pcnt], [D90Pcnt], [D95Pcnt], [EP], [NParaffins], [IsoParaffins], [Naphthenes], [Olefins], [Aromatics], [SG], [OthLiqFeedDESC], [OthLiqFeedPriceBasis])
	VALUES(@Refnum, @FeedProdID, @Methane, @Ethane, @Ethylene, @Propane, @Propylene, @nButane, @iButane, @Isobutylene, @Butene1, @Butadiene, @nPentane, @iPentane, @nHexane, @iHexane, @Septane, @Octane, @CO2, @Hydrogen, @Sulfur, @CoilOutletPressure, @SteamHCRatio, @CoilOutletTemp, @EthyleneYield, @PropylEthylRatio, @FeedConv, @PropylMethaneRatio, @ConstrYear, @ResTime, @SulfurPPM, @ASTMethod, @IBP, @D5pcnt, @D10Pcnt, @D30Pcnt, @D50Pcnt, @D70Pcnt, @D90Pcnt, @D95Pcnt, @EP, @NParaffins, @IsoParaffins, @Naphthenes, @Olefins, @Aromatics, @SG, @OthLiqFeedDESC, @OthLiqFeedPriceBasis)

END;