﻿CREATE PROCEDURE [stgFact].[Insert_Maint01]
(
	@Refnum         VARCHAR (25),
	@ProjectID      VARCHAR (15),

	@RoutMaintMatl  REAL         = NULL,
	@RoutMaintLabor REAL         = NULL,
	@RoutMaintTot   REAL         = NULL,
	@TAMatl         REAL         = NULL,
	@TALabor        REAL         = NULL,
	@TATot          REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Maint01]([Refnum], [ProjectID], [RoutMaintMatl], [RoutMaintLabor], [RoutMaintTot], [TAMatl], [TALabor], [TATot])
	VALUES(@Refnum, @ProjectID, @RoutMaintMatl, @RoutMaintLabor, @RoutMaintTot, @TAMatl, @TALabor, @TATot);

END;
