﻿CREATE PROCEDURE [stgFact].[Insert_Prac]
(
    @Refnum            VARCHAR (25),

    @FeedLogCost       REAL         = NULL,
    @EtyLogCost        REAL         = NULL,
    @PryLogCost        REAL         = NULL,
    @AffGasPcnt        REAL         = NULL,
    @AffEtyPcnt        REAL         = NULL,
    @AffPryPcnt        REAL         = NULL,
    @AffOthPcnt        REAL         = NULL,
    @EtyCarrier        CHAR (1)     = NULL,
    @ButaExtract       CHAR (1)     = NULL,
    @AromExtract       CHAR (1)     = NULL,
    @MTBE              CHAR (1)     = NULL,
    @IsoMTBE           CHAR (1)     = NULL,
    @IsoHighPurity     CHAR (1)     = NULL,
    @Butene1           CHAR (1)     = NULL,
    @Isoprene          CHAR (1)     = NULL,
    @Cyclopent         CHAR (1)     = NULL,
    @OthC5             CHAR (1)     = NULL,
    @InletTemp         REAL         = NULL,
    @FurnPreheatPcnt   REAL         = NULL,
    @FurnOutputPcnt    REAL         = NULL,
    @StmPress          REAL         = NULL,
    @TLE2Pcnt          REAL         = NULL,
    @TurbDriver        CHAR (1)     = NULL,
    @FurnTemp          REAL         = NULL,
    @OxyContent        REAL         = NULL,
    @OutletTemp        REAL         = NULL,
    @Deterioration     REAL         = NULL,
    @CapCreep          REAL         = NULL,
    @LocFactor         REAL         = NULL,
    @PrepHrs           REAL         = NULL,
    @AffChmPcnt        REAL         = NULL,
    @SteamReq          REAL         = NULL,
    @AffGasPcntNonDisc REAL         = NULL,
    @NonAffGasPcnt     REAL         = NULL,
    @SteamPcnt         REAL         = NULL,
    @TotFDCost         REAL         = NULL,
    @TotProdRev        REAL         = NULL,
    @CW                REAL         = NULL,
    @CWSupplyTemp      REAL         = NULL,
    @CWReturnTemp      REAL         = NULL,
    @HeatFF            REAL         = NULL,
    @ProcGasCompr      REAL         = NULL,
    @ComprEffDesc      VARCHAR (40) = NULL,
    @ExtractSteam      REAL         = NULL,
    @CondSteam         REAL         = NULL,
    @SurfCondVac       REAL         = NULL,
    @DutyPPHE          REAL         = NULL,
    @QuenchOilHM       REAL         = NULL,
    @QuenchOilDS       REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Prac]([Refnum], [FeedLogCost], [EtyLogCost], [PryLogCost], [AffGasPcnt], [AffEtyPcnt], [AffPryPcnt], [AffOthPcnt], [EtyCarrier], [ButaExtract], [AromExtract], [MTBE], [IsoMTBE], [IsoHighPurity], [Butene1], [Isoprene], [Cyclopent], [OthC5], [InletTemp], [FurnPreheatPcnt], [FurnOutputPcnt], [StmPress], [TLE2Pcnt], [TurbDriver], [FurnTemp], [OxyContent], [OutletTemp], [Deterioration], [CapCreep], [LocFactor], [PrepHrs], [AffChmPcnt], [SteamReq], [AffGasPcntNonDisc], [NonAffGasPcnt], [SteamPcnt], [TotFDCost], [TotProdRev], [CW], [CWSupplyTemp], [CWReturnTemp], [HeatFF], [ProcGasCompr], [ComprEffDesc], [ExtractSteam], [CondSteam], [SurfCondVac], [DutyPPHE], [QuenchOilHM], [QuenchOilDS])
	VALUES(@Refnum, @FeedLogCost, @EtyLogCost, @PryLogCost, @AffGasPcnt, @AffEtyPcnt, @AffPryPcnt, @AffOthPcnt, @EtyCarrier, @ButaExtract, @AromExtract, @MTBE, @IsoMTBE, @IsoHighPurity, @Butene1, @Isoprene, @Cyclopent, @OthC5, @InletTemp, @FurnPreheatPcnt, @FurnOutputPcnt, @StmPress, @TLE2Pcnt, @TurbDriver, @FurnTemp, @OxyContent, @OutletTemp, @Deterioration, @CapCreep, @LocFactor, @PrepHrs, @AffChmPcnt, @SteamReq, @AffGasPcntNonDisc, @NonAffGasPcnt, @SteamPcnt, @TotFDCost, @TotProdRev, @CW, @CWSupplyTemp, @CWReturnTemp, @HeatFF, @ProcGasCompr, @ComprEffDesc, @ExtractSteam, @CondSteam, @SurfCondVac, @DutyPPHE, @QuenchOilHM, @QuenchOilDS)

END;