﻿CREATE PROCEDURE [stgFact].[Delete_PracOrg]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracOrg]
	WHERE [Refnum] = @Refnum;

END;