﻿CREATE PROCEDURE [stgFact].[Delete_ProdLoss_Availability]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[ProdLoss_Availability]
	WHERE [Refnum] = @Refnum;

END;