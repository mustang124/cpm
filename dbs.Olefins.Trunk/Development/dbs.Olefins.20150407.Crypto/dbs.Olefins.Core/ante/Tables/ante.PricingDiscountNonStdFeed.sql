﻿CREATE TABLE [ante].[PricingDiscountNonStdFeed] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [Coefficient]    REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PricingDiscountNonStdFeed_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingDiscountNonStdFeed] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingDiscountNonStdFeed_Coefficient] CHECK ([Coefficient]>=(0.0)),
    CONSTRAINT [FK_PricingDiscountNonStdFeed_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingDiscountNonStdFeed_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingDiscountNonStdFeed_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_PricingDiscountNonStdFeed_u]
	ON [ante].[PricingDiscountNonStdFeed]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingDiscountNonStdFeed]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingDiscountNonStdFeed].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[PricingDiscountNonStdFeed].CalDateKey		= INSERTED.CalDateKey;

END;