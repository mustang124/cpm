﻿CREATE PROCEDURE sim.StreamYield_Insert(
	@QueueId			BIGINT,
	@FactorSetId		VARCHAR(12),
	@SimModelId			VARCHAR(12),

	@Refnum				VARCHAR(25),
	@CalDateKey			INT,
	
	@StreamId			VARCHAR(42),
	@StreamDescription	NVARCHAR(256),
	@OpCondId			VARCHAR(12),
	@RecycleId			VARCHAR(42),
	@ComponentId		VARCHAR(42),
	@Component_WtPcnt	REAL
	)
AS
	INSERT INTO sim.[YieldCompositionStream](QueueId, FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, StreamDescription, RecycleId, ComponentId, Component_WtPcnt)
	VALUES(@QueueId, @FactorSetId, @Refnum, @CalDateKey, @SimModelId, @OpCondId, @StreamId, @StreamDescription, @RecycleId, @ComponentId, @Component_WtPcnt)
	;