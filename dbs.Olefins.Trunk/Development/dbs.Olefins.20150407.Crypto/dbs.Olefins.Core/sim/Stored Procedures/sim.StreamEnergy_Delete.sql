﻿CREATE PROCEDURE sim.StreamEnergy_Delete
(
	@FactorSetId	VARCHAR(12)	= NULL,
	@Refnum			VARCHAR(25)	= NULL,
	@SimModelId		VARCHAR(12)	= NULL
)
AS
BEGIN

	DELETE FROM sim.EnergyConsumptionStream
	WHERE	FactorSetId		= @FactorSetId
		AND	Refnum			= @Refnum
		AND	SimModelId		= @SimModelId;

END