﻿CREATE PROCEDURE sim.StreamEnergy_Update(
	@ErrorID					VARCHAR(12)	= NULL,
	@FurnaceTypeId				VARCHAR(12)	= NULL,
	@SimEnergy_kCalkg			REAL		= NULL,
	----------------------------------------------
	@QueueID_init				BIGINT,
	@FactorSetId_init			VARCHAR(12),
	  
	@Refnum_init				VARCHAR(25),
	@CalDateKey_init			INT,
	@SimModelID_init			VARCHAR(12),
	@OpCondID_init				VARCHAR(12),
	
	@StreamID_init				VARCHAR(42),
	@StreamDescription_init		NVARCHAR(256),
	@RecycleID_init				INT
	)
AS
	UPDATE sim.[EnergyConsumptionStream]
	SET	  ErrorID			= ISNULL(@ErrorID, ErrorID)
		, FurnaceTypeId		= ISNULL(@FurnaceTypeId, FurnaceTypeId)
		, SimEnergy_kCalkg	= ISNULL(@SimEnergy_kCalkg, SimEnergy_kCalkg)
	WHERE QueueId			= @QueueID_init
	AND FactorSetId			= @FactorSetId_init
	AND	Refnum				= @Refnum_init
	AND	CalDateKey			= @CalDateKey_init
	AND	StreamId			= @StreamID_init
	AND	StreamDescription	= @StreamDescription_init
	AND	OpCondId			= @OpCondID_init
	AND	RecycleId			= @RecycleID_init
	;