﻿CREATE PROC [Ranking].[GetRankSpecsID](@ListId varchar(42), @RankBreak sysname, @BreakValue varchar(12), @RankVariable sysname, @RankSpecsID int OUTPUT)
AS
BEGIN
	SET @RankSpecsID = NULL
	IF @ListId IS NULL OR @RankBreak IS NULL OR @BreakValue IS NULL OR @RankVariable IS NULL
		RETURN 1
	ELSE BEGIN
		DECLARE @ListName nvarchar(84)
		SELECT @ListName = ListName FROM cons.RefListLu WHERE ListId = @ListId
		IF @ListName IS NULL
			RETURN 2
		ELSE BEGIN
			SELECT @RankSpecsID = RankSpecsID 
			FROM Ranking.RankSpecsLu WHERE ListId = @ListId AND RankBreak = @RankBreak AND BreakValue = @BreakValue AND RankVariable = @RankVariable
		
			IF @RankSpecsID IS NULL
			BEGIN
				INSERT Ranking.RankSpecsLu(ListId, RankBreak, BreakValue, RankVariable, ListName)
				SELECT @ListId, @RankBreak, @BreakValue, @RankVariable, @ListName

				SELECT @RankSpecsID = @@IDENTITY
			END
		END
	END
END
