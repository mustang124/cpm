﻿CREATE TABLE [calc].[StandardEnergyHydroPur] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [StandardEnergyId]       VARCHAR (42)       NOT NULL,
    [Quantity_kMT]           REAL               NOT NULL,
    [Component_WtPcnt]       REAL               NOT NULL,
    [kSCF]                   REAL               NOT NULL,
    [kSCFDay]                REAL               NOT NULL,
    [StandardEnergy_MBtuDay] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyHydroPur_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyHydroPur_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyHydroPur_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyHydroPur_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyHydroPur] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StandardEnergyId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_StandardEnergyHydroPur_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyHydroPur_kSCF] CHECK ([kSCF]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyHydroPur_kSCFDay] CHECK ([kSCFDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyHydroPur_Quantity_kMT] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [CR_StandardEnergyHydroPur_StandardEnergy_MBtuDay] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [FK_StandardEnergyHydroPur_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_StandardEnergyHydroPur_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergyHydroPur_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergyHydroPur_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_CalcStandardEnergyHydroPur_u
	ON  calc.StandardEnergyHydroPur
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.StandardEnergyHydroPur
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.StandardEnergyHydroPur.[FactorSetId]		= INSERTED.[FactorSetId]
		AND	calc.StandardEnergyHydroPur.[Refnum]			= INSERTED.[Refnum]
		AND calc.StandardEnergyHydroPur.[StandardEnergyId]	= INSERTED.[StandardEnergyId]
		AND calc.StandardEnergyHydroPur.[CalDateKey]		= INSERTED.[CalDateKey];
				
END;