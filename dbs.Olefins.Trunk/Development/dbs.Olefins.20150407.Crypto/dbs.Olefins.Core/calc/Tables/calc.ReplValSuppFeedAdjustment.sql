﻿CREATE TABLE [calc].[ReplValSuppFeedAdjustment] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [CurrencyRpt]            VARCHAR (4)        NOT NULL,
    [Quantity_kMT]           REAL               NOT NULL,
    [Amount_kMT]             REAL               NOT NULL,
    [InflationFactor]        REAL               NOT NULL,
    [SupplementalAdjustment] REAL               NOT NULL,
    [RGRSuperseede]          REAL               NULL,
    [_SuppFeedAdjustment]    AS                 (CONVERT([real],isnull([RGRSuperseede],[SupplementalAdjustment]),(0))) PERSISTED NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_ReplValSuppFeedAdjustment_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_ReplValSuppFeedAdjustment_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_ReplValSuppFeedAdjustment_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_ReplValSuppFeedAdjustment_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ReplValSuppFeedAdjustment] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ReplValSuppFeedAdjustment_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ReplValSuppFeedAdjustment_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_ReplValSuppFeedAdjustment_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ReplValSuppFeedAdjustment_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ReplValSuppFeedAdjustment_u
	ON  calc.ReplValSuppFeedAdjustment
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ReplValSuppFeedAdjustment
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ReplValSuppFeedAdjustment.FactorSetId		= INSERTED.FactorSetId
		AND	calc.ReplValSuppFeedAdjustment.Refnum			= INSERTED.Refnum
		AND calc.ReplValSuppFeedAdjustment.CalDateKey		= INSERTED.CalDateKey
		AND calc.ReplValSuppFeedAdjustment.CurrencyRpt		= INSERTED.CurrencyRpt;
				
END