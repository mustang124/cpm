﻿CREATE TABLE [calc].[MaintCost] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CalDateKeyPrev] INT                NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [MaintPrev_Cur]  REAL               NOT NULL,
    [Maint_Cur]      REAL               NOT NULL,
    [MaintAvg_Cur]   REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MaintCost_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MaintCost_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MaintCost_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MaintCost_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_MaintCost] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_MaintCost_Maint_Cur] CHECK ([Maint_Cur]>=(0.0)),
    CONSTRAINT [CR_calc_MaintCost_MaintAvg_Cur] CHECK ([MaintAvg_Cur]>=(0.0)),
    CONSTRAINT [CR_calc_MaintCost_MaintPrevCur] CHECK ([MaintPrev_Cur]>=(0.0)),
    CONSTRAINT [FK_calc_MaintCost_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_MaintCost_Calendar_LookUp_Prev] FOREIGN KEY ([CalDateKeyPrev]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_MaintCost_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_MaintCost_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_MaintCost_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [calc].[t_MaintCost_u]
	ON [calc].[MaintCost]
	AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.MaintCost
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MaintCost.FactorSetId		= INSERTED.FactorSetId
		AND	calc.MaintCost.Refnum			= INSERTED.Refnum
		AND calc.MaintCost.CalDateKey		= INSERTED.CalDateKey
		AND calc.MaintCost.CurrencyRpt		= INSERTED.CurrencyRpt;

END