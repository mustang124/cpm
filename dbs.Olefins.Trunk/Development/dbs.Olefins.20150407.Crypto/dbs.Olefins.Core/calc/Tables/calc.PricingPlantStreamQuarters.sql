﻿CREATE TABLE [calc].[PricingPlantStreamQuarters] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [ScenarioId]        VARCHAR (42)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NULL,
    [Q1]                REAL               NOT NULL,
    [Q2]                REAL               NOT NULL,
    [Q3]                REAL               NOT NULL,
    [Q4]                REAL               NOT NULL,
    [Total]             REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPlantStreamQuarters_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_PricingPlantStreamQuarters_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_PricingPlantStreamQuarters_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_PricingPlantStreamQuarters_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [FK_calc_PricingPlantStreamQuarters_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PricingPlantStreamQuarters_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_PricingPlantStreamQuarters_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PricingPlantStreamQuarters_Scenarios] FOREIGN KEY ([FactorSetId], [ScenarioId]) REFERENCES [ante].[PricingScenarioConfig] ([FactorSetId], [ScenarioId]),
    CONSTRAINT [FK_calc_PricingPlantStreamQuarters_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_PricingPlantStreamQuarters_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE UNIQUE CLUSTERED INDEX [UX_PricingPlantStreamQuarters_PK]
    ON [calc].[PricingPlantStreamQuarters]([FactorSetId] ASC, [Refnum] ASC, [ScenarioId] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC);


GO
CREATE TRIGGER calc.t_PricingPlantStreamQuarters_u
	ON  calc.[PricingPlantStreamQuarters]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[PricingPlantStreamQuarters]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[PricingPlantStreamQuarters].FactorSetId		= INSERTED.FactorSetId
		AND calc.[PricingPlantStreamQuarters].ScenarioId		= INSERTED.ScenarioId
		AND calc.[PricingPlantStreamQuarters].Refnum			= INSERTED.Refnum
		AND calc.[PricingPlantStreamQuarters].CalDateKey		= INSERTED.CalDateKey
		AND calc.[PricingPlantStreamQuarters].CurrencyRpt		= INSERTED.CurrencyRpt
		AND calc.[PricingPlantStreamQuarters].StreamId			= INSERTED.StreamId
		AND calc.[PricingPlantStreamQuarters].StreamDescription	= INSERTED.StreamDescription;
	
END;