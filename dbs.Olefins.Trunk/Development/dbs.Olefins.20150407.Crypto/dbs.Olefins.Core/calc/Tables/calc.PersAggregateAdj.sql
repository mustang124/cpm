﻿CREATE TABLE [calc].[PersAggregateAdj] (
    [FactorSetId]                    VARCHAR (12)       NOT NULL,
    [Refnum]                         VARCHAR (25)       NOT NULL,
    [CalDateKey]                     INT                NOT NULL,
    [PersId]                         VARCHAR (34)       NOT NULL,
    [Personnel_Count]                REAL               NULL,
    [StraightTime_Hrs]               REAL               NULL,
    [OverTime_Hrs]                   REAL               NULL,
    [Company_Hrs]                    REAL               NULL,
    [Contract_Hrs]                   REAL               NULL,
    [Tot_Hrs]                        REAL               NULL,
    [Company_Fte]                    REAL               NULL,
    [Contract_Fte]                   REAL               NULL,
    [Tot_Fte]                        REAL               NULL,
    [_OtSt_Pcnt]                     AS                 (case when [StraightTime_Hrs]<>(0.0) then CONVERT([real],([OverTime_Hrs]/[StraightTime_Hrs])*(100.0),(1))  end) PERSISTED,
    [_StraightTime_Pcnt]             AS                 (case when [Tot_Hrs]<>(0.0) then CONVERT([real],([StraightTime_Hrs]/[Tot_Hrs])*(100.0),(1))  end) PERSISTED,
    [_OverTime_Pcnt]                 AS                 (case when [Tot_Hrs]<>(0.0) then CONVERT([real],([OverTime_Hrs]/[Tot_Hrs])*(100.0),(1))  end) PERSISTED,
    [_Company_Pcnt]                  AS                 (case when [Tot_Hrs]<>(0.0) then CONVERT([real],([Company_Hrs]/[Tot_Hrs])*(100.0),(1))  end) PERSISTED,
    [_Contract_Pcnt]                 AS                 (case when [Tot_Hrs]<>(0.0) then CONVERT([real],([Contract_Hrs]/[Tot_Hrs])*(100.0),(1))  end) PERSISTED,
    [_CompanyFte_Pcnt]               AS                 (case when [Tot_Fte]<>(0.0) then CONVERT([real],([Company_Fte]/[Tot_Fte])*(100.0),(1))  end) PERSISTED,
    [_ContractFte_Pcnt]              AS                 (case when [Tot_Fte]<>(0.0) then CONVERT([real],([Contract_Fte]/[Tot_Fte])*(100.0),(1))  end) PERSISTED,
    [InflTaAdjAnn_Company_Hrs]       REAL               NULL,
    [InflTaAdjAnn_Contract_Hrs]      REAL               NULL,
    [InflTaAdjAnn_Tot_Hrs]           REAL               NULL,
    [InflTaAdjAnn_Company_Fte]       REAL               NULL,
    [InflTaAdjAnn_Contract_Fte]      REAL               NULL,
    [InflTaAdjAnn_Tot_Fte]           REAL               NULL,
    [_InflTaAdjAnn_CompanyHrs_Pcnt]  AS                 (case when [InflTaAdjAnn_Tot_Hrs]<>(0.0) then CONVERT([real],([InflTaAdjAnn_Company_Hrs]/[InflTaAdjAnn_Tot_Hrs])*(100.0),(1))  end) PERSISTED,
    [_InflTaAdjAnn_ContractHrs_Pcnt] AS                 (case when [InflTaAdjAnn_Tot_Hrs]<>(0.0) then CONVERT([real],([InflTaAdjAnn_Contract_Hrs]/[InflTaAdjAnn_Tot_Hrs])*(100.0),(1))  end) PERSISTED,
    [_InflTaAdjAnn_CompanyFte_Pcnt]  AS                 (case when [InflTaAdjAnn_Tot_Fte]<>(0.0) then CONVERT([real],([InflTaAdjAnn_Company_Fte]/[InflTaAdjAnn_Tot_Fte])*(100.0),(1))  end) PERSISTED,
    [_InflTaAdjAnn_ContractFte_Pcnt] AS                 (case when [InflTaAdjAnn_Tot_Fte]<>(0.0) then CONVERT([real],([InflTaAdjAnn_Contract_Fte]/[InflTaAdjAnn_Tot_Fte])*(100.0),(1))  end) PERSISTED,
    [WorkWeek_Hrs]                   REAL               NULL,
    [Contract_InflTaAdjAnnPcnt]      REAL               NULL,
    [tsModified]                     DATETIMEOFFSET (7) CONSTRAINT [DF_PersAggregateAdj_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                 NVARCHAR (168)     CONSTRAINT [DF_PersAggregateAdj_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                 NVARCHAR (168)     CONSTRAINT [DF_PersAggregateAdj_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                  NVARCHAR (168)     CONSTRAINT [DF_PersAggregateAdj_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PersAggregateAdj] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [PersId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PersAggregateAdj_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PersAggregateAdj_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PersAggregateAdj_Pers_LookUp] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_calc_PersAggregateAdj_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_PersAggregateAdj_u
	ON  calc.PersAggregateAdj
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.PersAggregateAdj
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PersAggregateAdj.FactorSetId	= INSERTED.FactorSetId
		AND calc.PersAggregateAdj.Refnum		= INSERTED.Refnum
		AND calc.PersAggregateAdj.CalDateKey	= INSERTED.CalDateKey
		AND calc.PersAggregateAdj.PersId		= INSERTED.PersId;
	
END