﻿CREATE TABLE [calc].[EnergyNormal] (
    [FactorSetId]                    VARCHAR (12)       NOT NULL,
    [Refnum]                         VARCHAR (25)       NOT NULL,
    [CalDateKey]                     INT                NOT NULL,
    [CoolingWaterDeltaTemp_C]        REAL               NULL,
    [CoolingWaterEnergy_BtuLb]       REAL               NULL,
    [FinFanEnergy_BtuLb]             REAL               NULL,
    [SteamExtraction_MtMtHVC]        REAL               NULL,
    [SteamCondensing_MtMtHVC]        REAL               NULL,
    [ProcessedHeatExchanged_MtMtHVC] REAL               NULL,
    [tsModified]                     DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyNormal_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                 NVARCHAR (168)     CONSTRAINT [DF_EnergyNormal_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                 NVARCHAR (168)     CONSTRAINT [DF_EnergyNormal_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                  NVARCHAR (168)     CONSTRAINT [DF_EnergyNormal_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_EnergyNormal] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_EnergyNormal_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_EnergyNormal_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_EnergyNormal_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_EnergyNormal_u
   ON  calc.EnergyNormal
   AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.EnergyNormal
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.EnergyNormal.FactorSetId	= INSERTED.FactorSetId
		AND calc.EnergyNormal.Refnum		= INSERTED.Refnum
		AND calc.EnergyNormal.CalDateKey	= INSERTED.CalDateKey;
				
END