﻿CREATE PROCEDURE [calc].[Insert_ApcIndexApplications_Specific]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ApcIndexApplications]([FactorSetId], [Refnum], [CalDateKey], [ApcId], [AbsenceCorrectionFactor], [OnLine_Pcnt], [Mpc_Int], [Sep_Int], [Mpc_Value], [Sep_Value])
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			b.DescendantId										[ApcId],
			c._AbsenceCorrectionFactor,
			COALESCE(o.OnLine_Pcnt, 0.0)						[OnLine_Pcnt],

			COALESCE(CONVERT(INT, k.Mpc_Bit), 0)	* COALESCE(CONVERT(FLOAT, f._Unit_Bit), 1.0)	[Mpc_Int],
			COALESCE(CONVERT(INT, k.Sep_Bit), 0)	* COALESCE(CONVERT(FLOAT, f._Unit_Bit), 1.0)	[Sep_Int],
			
			w.Mpc_Value								* COALESCE(CONVERT(FLOAT, f._Unit_Bit), 1.0)	[Mpc_Value],
			w.Sep_Value								* COALESCE(CONVERT(FLOAT, f._Unit_Bit), 1.0)	[Sep_Value]
		FROM @fpl												tsq
		INNER JOIN dim.Apc_Bridge								b
			ON	b.FactorSetId	= tsq.FactorSetId
			AND	b.ApcId			IN ('PyroFurn', 'Comp', 'ProdRecovery')
		INNER JOIN calc.ApcAbsenceCorrection					c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_QtrDateKey
		INNER JOIN ante.ApcWeighting							w
			ON	w.FactorSetId	= tsq.FactorSetId
			AND	w.ApcId			= b.DescendantId
		LEFT OUTER JOIN fact.ApcExistance						k
			ON	k.Refnum		= tsq.Refnum
			AND	k.CalDateKey	= tsq.Plant_QtrDateKey
			AND	k.ApcId			= b.DescendantId
		LEFT OUTER JOIN fact.ApcOnLinePcnt						o
			ON	o.Refnum		= tsq.Refnum
			AND	o.CalDateKey	= tsq.Plant_QtrDateKey
			AND	o.ApcId			= b.ApcId
		LEFT OUTER JOIN fact.Facilities							f
			ON	f.Refnum		= tsq.Refnum
			AND	f.CalDateKey	= tsq.Plant_QtrDateKey
			AND	f.FacilityId	= 'TowerPropylene'
			AND w.ApcId			IN('ApcC3Recovery', 'ApcC3RecoveryThroughput', 'ApcSteamNaphthaComposition', 'ApcSteamNaphthaThroughput')
		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;