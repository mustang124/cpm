﻿CREATE PROCEDURE [calc].[Insert_StandardEnergyHydroTreat]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		-------------------------------------------------------------------------------
		--	F325: HydroTreat 

		INSERT INTO [calc].[StandardEnergyHydroTreat]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [Quantity_kMT], [FeedProcessed_Pcnt], [StandardEnergy_MBtuDay])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			sqa.[StreamId]			[StandardEnergyId],

			ABS(sqa.[Quantity_kMT])	[Quantity_kMT],
			[calc].[MinValue](fht.[FeedProcessed_kMT] / ABS(SUM(sqa.[Quantity_kMT]) OVER(PARTITION BY fpl.[FactorSetId], fpl.[Refnum], fpl.[Plant_AnnDateKey])), 1.0)	[FeedProcessed_Pcnt],

			ABS(sqa.[Quantity_kMT])
			* [calc].[MinValue](fht.[FeedProcessed_kMT] / ABS(SUM(sqa.[Quantity_kMT]) OVER(PARTITION BY fpl.[FactorSetId], fpl.[Refnum], fpl.[Plant_AnnDateKey])), 1.0)
			* 7.16
			/ 365.0
			* map.Energy_kBtuBBL	[StandardEnergy_MBtuDay]
		FROM @fpl											fpl
		INNER JOIN [fact].[StreamQuantityAggregate]			sqa	WITH(NOEXPAND)
			ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
			AND	sqa.[Refnum]			= fpl.[Refnum]
			AND	sqa.[StreamId]			IN ('Benzene', 'PyroGasoline')
			AND	sqa.[Quantity_kMT]		<> 0.0
		INNER JOIN [fact].[FacilitiesHydroTreat]			fht
			ON	fht.[Refnum]			= fpl.[Refnum]
			AND	fht.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	fht.[FeedProcessed_kMT]	<> 0.0
		INNER JOIN (VALUES
			('B', 100),
			('DS', 80),
			('SS', 55)
			)											map ([HTTypeId], [Energy_kBtuBBL])
			ON map.[HTTypeId]		= COALESCE(fht.[HTTypeID], 'SS') 
		WHERE	fpl.[CalQtr]		= 4
			AND	fpl.[FactorSet_AnnDateKey] > 20130000;
	
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;