﻿CREATE PROCEDURE calc.Insert_ApcIndex
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.ApcIndex (ApcIndexApplications)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.ApcIndex(FactorSetId, Refnum, CalDateKey, ApcId,
			Mpc_Value,
			Sep_Value,
			Mpc_Int,
			Sep_Int,
			OnLine_Pcnt,
			Apc_Index,
			ApcOnLine_Index)
		SELECT a.FactorSetId, a.Refnum, a.CalDateKey, a.ApcId,
			a._Abs_Mpc_Value,
			a._Abs_Sep_Value,
			a.Mpc_Int,
			a.Sep_Int,
			a.OnLine_Pcnt,
			a._Apc_Index,
			a._ApcOnLine_Index
		FROM @fpl									tsq
		INNER JOIN calc.ApcIndexApplications		a
			ON	a.FactorSetId	= tsq.FactorSetId
			AND	a.Refnum		= tsq.Refnum
			AND	a.CalDateKey	= tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr		= 4;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.ApcIndex (ApcIndexPyroFurnIndivid)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.ApcIndex(FactorSetId, Refnum, CalDateKey, ApcId,
			Mpc_Value,
			Mpc_Int,
			Sep_Int,
			OnLine_Pcnt,
			Apc_Index,
			ApcOnLine_Index)
		SELECT a.FactorSetId, a.Refnum, a.CalDateKey, a.ApcId,
			a._Abs_Mpc_Value,
			1,
			0,
			a.OnLine_Pcnt,
			a.Apc_Index,
			a._ApcOnLine_Index
		FROM @fpl									tsq
		INNER JOIN calc.ApcIndexPyroFurnIndivid		a
			ON	a.FactorSetId	= tsq.FactorSetId
			AND	a.Refnum		= tsq.Refnum
			AND	a.CalDateKey	= tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr		= 4;
					
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.ApcIndex (Steam/Other)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.ApcIndex(FactorSetId, Refnum, CalDateKey, ApcId,
			Mpc_Value,
			Mpc_Int,
			Sep_Int,
			OnLine_Pcnt,
			Apc_Index,
			ApcOnLine_Index)
		SELECT a.FactorSetId, a.Refnum, a.CalDateKey, a.ApcId,
			a._Abs_Mpc_Value,
			COALESCE(a.Mpc_Int, 0),
			0,
			a.OnLine_Pcnt,
			a.Apc_Index,
			a._ApcOnLine_Index
		FROM @fpl									tsq
		INNER JOIN calc.ApcIndexSteamOther			a
			ON	a.FactorSetId	= tsq.FactorSetId
			AND	a.Refnum		= tsq.Refnum
			AND	a.CalDateKey	= tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr		= 4;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.ApcIndex (ApcIndexOnLineModel)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.ApcIndex(FactorSetId, Refnum, CalDateKey, ApcId,
			Mpc_Value,
			Mpc_Int,
			Sep_Int,
			OnLine_Pcnt,
			Apc_Index,
			ApcOnLine_Index)
		SELECT a.FactorSetId, a.Refnum, a.CalDateKey, a.ApcId,
			a.Abs_Mpc_Value,
			COALESCE(a.Mpc_Int, 0),
			0,
			COALESCE(a.OnLine_Pcnt, 0.0),
			COALESCE(a._Apc_Index, 0.0),
			COALESCE(a._ApcOnLine_Index, 0.0)
		FROM @fpl									tsq
		INNER JOIN calc.ApcIndexOnLineModel			a
			ON	a.FactorSetId	= tsq.FactorSetId
			AND	a.Refnum		= tsq.Refnum
			AND	a.CalDateKey	= tsq.Plant_QtrDateKey
		WHERE	tsq.CalQtr		= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;