﻿CREATE PROCEDURE [calc].[Insert_PricingPlantStream]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	--	Create Pricing Foundation	-----------------------------------------------------
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'CREATE TABLE @Foundation';
		PRINT @ProcedureDesc;

		DECLARE @Foundation TABLE
		(
			[FactorSetId]				VARCHAR (12)	NOT	NULL,
			[ScenarioId]				VARCHAR (42)	NOT	NULL,
			[Refnum]					VARCHAR (25)	NOT	NULL,
			[EconRegionId]				VARCHAR (5)		NOT	NULL,
			[CountryId]					CHAR (3)		NOT	NULL,
			[CurrencyRpt]				VARCHAR (4)		NOT	NULL,
			[PricingMethodId]			VARCHAR (42)	NOT	NULL,
			[FactorSet_AnnDateKey]		INT				NOT	NULL,
			[FactorSet_QtrDateKey]		INT				NOT	NULL,
			[Plant_AnnDateKey]			INT				NOT	NULL,
			[Plant_QtrDateKey]			INT				NOT	NULL,
			[StreamId]					VARCHAR (42)	NOT	NULL,
			[StreamDescription]			NVARCHAR (256)	NOT	NULL,
			[Quantity_kMT]				REAL			NOT	NULL,
			[Adj_NonStdStream_Coeff]	REAL				NULL,
			[Adj_Logistics_Cur]			REAL				NULL,
			[Supersede_Amount_Cur]		REAL				NULL,
			PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ScenarioId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [StreamDescription] ASC, [Plant_QtrDateKey] ASC)
		);

		INSERT INTO @Foundation
		(
			FactorSetId, ScenarioId, Refnum,
			EconRegionId, CountryId, CurrencyRpt, PricingMethodId,
			FactorSet_AnnDateKey, FactorSet_QtrDateKey,
			Plant_AnnDateKey, Plant_QtrDateKey,
			StreamId, StreamDescription,
			Quantity_kMT,
			Adj_NonStdStream_Coeff, Adj_Logistics_Cur,
			Supersede_Amount_Cur
		)
		SELECT
			tsq.FactorSetId,
			psc.ScenarioId,
			tsq.Refnum,
			COALESCE(psp.RegionId, psc.RegionId, tsq.EconRegionId),
			COALESCE(psp.CountryId, psc.CountryId, tsq.CountryId),
			tsq.CurrencyRpt,
			psc.PricingMethodId,
			tsq.FactorSet_AnnDateKey,
			tsq.FactorSet_QtrDateKey,
			tsq.Plant_AnnDateKey,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			q.StreamDescription,
			q.Quantity_kMT,
			coeff.Coefficient,
			lg.Amount_Cur,
			super.Amount_Cur
		FROM @fpl													tsq
		INNER JOIN fact.Quantity									q
			ON	q.Refnum			= tsq.Refnum
			AND	q.CalDateKey		= tsq.Plant_QtrDateKey
		INNER JOIN ante.PricingScenarioConfig						psc
			ON	psc.FactorSetId		= tsq.FactorSetId
		LEFT OUTER JOIN ante.PricingScenarioPlant					psp
			ON	psp.FactorSetId		= psc.FactorSetId
			AND	psp.ScenarioId		= psc.FactorSetId
			AND psp.Refnum			= tsq.Refnum
		LEFT OUTER JOIN ante.PricingAdjDutiesStreamCountryAgg		lg
			ON	lg.FactorSetId		= tsq.FactorSetId
			AND	lg.CountryId		= COALESCE(psp.CountryId, psc.CountryId, tsq.CountryId)
			AND	lg.CalDateKey		= tsq.FactorSet_AnnDateKey
			AND	lg.CurrencyRpt		= tsq.CurrencyRpt
			AND	lg.StreamId			= q.StreamId
		LEFT OUTER JOIN	ante.PricingDiscountNonStdFeed				coeff
			ON	coeff.FactorSetId	= tsq.FactorSetId
			AND coeff.Refnum		= tsq.Refnum
			AND coeff.CalDateKey	= tsq.FactorSet_AnnDateKey
			AND	coeff.StreamId		= q.StreamId
		LEFT OUTER JOIN super.QuantityValue							super
			ON	super.Refnum		= tsq.Refnum
			AND	super.CalDateKey	= tsq.Plant_QtrDateKey
			AND	super.CurrencyRpt	= tsq.CurrencyRpt
			AND	super.StreamId		= q.StreamId
			AND	super.StreamDescription = q.StreamDescription

		WHERE	q.Quantity_kMT IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	--	Insert Records - Stream	Based (Feed) --------------------------------------------
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Supplemental: DilEthylene)';
		PRINT @ProcedureDesc;
		
		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			CASE WHEN
					CASE WHEN s.Quantity_kMT <> 0.0 THEN ISNULL(a.Quantity_kMT, 0.0) / s.Quantity_kMT ELSE 0.0 END > 0.4 AND a.Quantity_kMT > 12.0
				THEN
					(ce._MatlBal_Amount_Cur +  de._MatlBal_Amount_Cur) / 2.0
				ELSE
					de._MatlBal_Amount_Cur
				END											[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN fact.StreamQuantityAggregate						a WITH (NOEXPAND)
			ON	a.FactorSetId = fnd.FactorSetId
			AND	a.Refnum = fnd.Refnum
			AND	a.StreamId = fnd.StreamId

		INNER JOIN fact.StreamQuantityAggregate						s WITH (NOEXPAND)
			ON	s.FactorSetId = fnd.FactorSetId
			AND	s.Refnum = fnd.Refnum
			AND	s.StreamId = 'SuppTot'

		LEFT OUTER JOIN [inter].PricingStreamCountry				ce
			ON	/*ce.FactorSetId = fnd.FactorSetId
			AND	*/ce.PricingMethodId = fnd.PricingMethodId
			AND	ce.CountryId = fnd.CountryId
			AND ce.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	ce.CurrencyRpt = fnd.CurrencyRpt
			AND ce.StreamId = 'ConcEthylene'

		LEFT OUTER JOIN [inter].PricingStreamCountry				de
			ON	/*de.FactorSetId = fnd.FactorSetId
			AND */de.PricingMethodId = fnd.PricingMethodId
			AND	de.CountryId = fnd.CountryId
			AND	de.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	de.CurrencyRpt = fnd.CurrencyRpt
			AND	de.StreamId = 'DilEthane'

		WHERE	fnd.StreamId = 'DilEthylene'
			AND CASE WHEN 
					CASE WHEN s.Quantity_kMT <> 0.0 THEN ISNULL(a.Quantity_kMT, 0.0) / s.Quantity_kMT ELSE 0.0 END > 0.4 AND a.Quantity_kMT > 12.0
				THEN
					(ce._MatlBal_Amount_Cur +  de._MatlBal_Amount_Cur) / 2.0
				ELSE
					de._MatlBal_Amount_Cur
				END		IS NOT NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Supplemental: DilPropylene)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			CASE WHEN
					CASE WHEN s.Quantity_kMT <> 0.0 THEN ISNULL(a.Quantity_kMT, 0.0) / s.Quantity_kMT ELSE 0.0 END > 0.4 AND a.Quantity_kMT > 12.0
				THEN
					(cp._MatlBal_Amount_Cur + dp._MatlBal_Amount_Cur) / 2.0
				ELSE
					dp._MatlBal_Amount_Cur
				END											[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN fact.StreamQuantityAggregate						a	WITH (NOEXPAND)
			ON	a.FactorSetId = fnd.FactorSetId
			AND	a.Refnum = fnd.Refnum
			AND	a.StreamId = fnd.StreamId

		INNER JOIN fact.StreamQuantityAggregate						s	WITH (NOEXPAND)
			ON	s.FactorSetId = fnd.FactorSetId
			AND	s.Refnum = fnd.Refnum
			AND	s.StreamId = 'SuppTot'

		LEFT OUTER JOIN [inter].PricingStreamCountry				cp
			ON	/*cp.FactorSetId = fnd.FactorSetId
			AND */cp.PricingMethodId = fnd.PricingMethodId
			AND	cp.CountryId = fnd.CountryId
			AND cp.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	cp.CurrencyRpt = fnd.CurrencyRpt
			AND cp.StreamId = 'ConcPropylene'

		LEFT OUTER JOIN [inter].PricingStreamCountry				dp
			ON	/*dp.FactorSetId = fnd.FactorSetId
			AND */dp.PricingMethodId = fnd.PricingMethodId
			AND	dp.CountryId = fnd.CountryId
			AND dp.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	dp.CurrencyRpt = fnd.CurrencyRpt
			AND dp.StreamId = 'DilPropane'

		WHERE	fnd.StreamId = 'DilPropylene'
			AND CASE WHEN 
					CASE WHEN s.Quantity_kMT <> 0.0 THEN ISNULL(a.Quantity_kMT, 0.0) / s.Quantity_kMT ELSE 0.0 END > 0.4 AND a.Quantity_kMT > 12.0
				THEN
					(cp._MatlBal_Amount_Cur + dp._MatlBal_Amount_Cur) / 2.0
				ELSE
					dp._MatlBal_Amount_Cur
				END	IS NOT NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Supplemental: ROG)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Energy_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur * m.Coefficient * h.NHValue_MBtuLb / 21500.0

		FROM @Foundation											fnd

		INNER JOIN ante.PricingMultiplierROG						m
			ON	m.FactorSetId	= fnd.FactorSetId
			AND m.CalDateKey	= fnd.FactorSet_AnnDateKey

		INNER JOIN ante.PricingNetHeatingValueStream				h
			ON	h.FactorSetId	= fnd.FactorSetId
			AND h.StreamId		= fnd.StreamId

		INNER JOIN [inter].PricingStreamCountry						k
			ON	/*k.FactorSetId = fnd.FactorSetId
			AND */k.PricingMethodId = fnd.PricingMethodId
			AND	k.CountryId		= fnd.CountryId
			AND	k.CalDateKey	= fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND k.CurrencyRpt	= fnd.CurrencyRpt
			AND	k.StreamId		= 'DilMethane';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Liquid Feeds: Raffinate, Condensate, HeavyNGL; Products: Acetylene, Butadiene, Benzene, IsoButylene, C4Oth, PyroFuelOil, PyroGasOil)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur								[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId = fnd.PricingMethodId
			AND k.CountryId = fnd.CountryId
			AND	k.CalDateKey = fnd.Plant_QtrDateKey
			AND k.CurrencyRpt = fnd.CurrencyRpt
			AND	k.StreamId = fnd.StreamId

		WHERE	fnd.StreamId IN ('Raffinate', 'Condensate',								-- Fresh Pyro
					'Acetylene', 'Butadiene', 'Benzene',								-- Products
					'C4Oth', 'HeavyNGL', 'IsoButylene', 'PyroFuelOil', 'PyroGasOil');	-- Material Balance

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (MetaOCButanes, MetaOCButene1, MetaOCButene2, MetaProdUnReactButane)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur									[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId	= fnd.PricingMethodId
			AND k.CountryId			= fnd.CountryId
			AND	k.CalDateKey		= fnd.Plant_QtrDateKey
			AND k.CurrencyRpt		= fnd.CurrencyRpt
			AND	k.StreamId			= 'C4Oth'

		WHERE	fnd.StreamId		IN ('MetaOCButanes', 'MetaOCButene1', 'MetaOCButene2', 'MetaProdUnReactButane');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (MetaOCOther)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur									[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId	= fnd.PricingMethodId
			AND k.CountryId			= fnd.CountryId
			AND	k.CalDateKey		= fnd.Plant_QtrDateKey
			AND k.CurrencyRpt		= fnd.CurrencyRpt
			AND	k.StreamId			= 'IsoButylene'

		WHERE	fnd.StreamId		= 'MetaOCOther';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (GasOilHt, GasOilHv, Diesel)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Region_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur * 
				CASE WHEN a.Density_SG = 0.0
				THEN 1.0
				ELSE (d.DensityStandard_SG / a.Density_SG) - 0.005
				END												[Region_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN fact.FeedStockLiquidAttributes					a
			ON	a.FactorSetId	= fnd.FactorSetId
			AND	a.Refnum		= fnd.Refnum
			AND	a.CalDateKey	= fnd.Plant_AnnDateKey
			AND a.StreamId		= fnd.StreamId
			AND	a.StreamDescription = fnd.StreamDescription

		LEFT OUTER JOIN ante.PricingStdDensityStreamRegion			d
			ON	d.RegionId		= fnd.[EconRegionId]
			AND	d.FactorSetId	= fnd.FactorSetId
			AND	d.StreamId		= fnd.StreamId

		INNER JOIN [inter].PricingStreamCountry						k
			ON	/*k.FactorSetId = fnd.FactorSetId
			AND */k.PricingMethodId = fnd.PricingMethodId
			AND k.CountryId		= fnd.CountryId
			AND	k.CalDateKey	= fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND k.CurrencyRpt	= fnd.CurrencyRpt
			AND	k.StreamId		= fnd.StreamId

		WHERE	fnd.StreamId IN ('GasOilHt', 'GasOilHv')
			OR(	fnd.StreamId = 'Diesel' AND fnd.Plant_QtrDateKey < 20090000);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PyroGasoline, MetaProdC5)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur								[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN fact.Facilities									f
			ON	f.Refnum = fnd.Refnum
			AND	f.FacilityId = 'TowerPyroGasHT'
		
		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId = fnd.PricingMethodId
			AND k.CountryId = fnd.CountryId
			AND	k.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND k.CurrencyRpt = fnd.CurrencyRpt
			AND	k.StreamId = CASE WHEN f._Unit_Bit = 1 THEN 'PyroGasOilHt' ELSE 'PyroGasOilUt' END

		WHERE	fnd.StreamId IN ('PyroGasoline', 'MetaProdC5');

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	--	Insert Records - Stream Based (Supplemental)	---------------------------------
	BEGIN TRY
		
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Supplemental)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur	[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId = fnd.PricingMethodId
			AND k.CountryId = fnd.CountryId
			AND	k.CalDateKey = fnd.Plant_QtrDateKey
			AND k.CurrencyRpt = fnd.CurrencyRpt
			AND	k.StreamId = fnd.StreamId

		WHERE	fnd.StreamId		IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fnd.FactorSetId AND d.StreamId  = 'SuppTot')
			AND	fnd.StreamId NOT	IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fnd.FactorSetId AND d.StreamId  = 'SuppROG')
			AND fnd.StreamId NOT	IN ('DilEthylene', 'DilPropylene', 'SuppOther');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (MetaPurchButene1, MetaPurchButene2, MetaPurchOther)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur	[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId	= fnd.PricingMethodId
			AND k.CountryId			= fnd.CountryId
			AND	k.CalDateKey		= fnd.Plant_QtrDateKey
			AND k.CurrencyRpt		= fnd.CurrencyRpt
			AND	k.StreamId			= 'DilButylene'

		WHERE	fnd.StreamId		IN ('MetaPurchButene1', 'MetaPurchButene2', 'MetaPurchOther');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (MetaPurchEthylene)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur	[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId	= fnd.PricingMethodId
			AND k.CountryId			= fnd.CountryId
			AND	k.CalDateKey		= fnd.Plant_QtrDateKey
			AND k.CurrencyRpt		= fnd.CurrencyRpt
			AND	k.StreamId			= 'ConcEthylene'

		WHERE	fnd.StreamId		= 'MetaPurchEthylene';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (MetaPurchButane)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur	[Country_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.PricingMethodId	= fnd.PricingMethodId
			AND k.CountryId			= fnd.CountryId
			AND	k.CalDateKey		= fnd.Plant_QtrDateKey
			AND k.CurrencyRpt		= fnd.CurrencyRpt
			AND	k.StreamId			= 'DilButane'

		WHERE	fnd.StreamId		= 'MetaPurchButane';

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	--	Insert Records - Composition Based (Feed)	-------------------------------------
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Light Feeds: Other Light Feed)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Client_Amount_Cur,
			Composition_Amount_Cur,
			Energy_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			v.Amount_Cur									[Client_Amount_Cur],

			SUM(qc.Component_WtPcnt * k._MatlBal_Amount_Cur) / 100.0
															[Composition_Amount_Cur],

			SUM(qc.Component_WtPcnt * h.NHValue_MBtuLb * [CH4]._MatlBal_Amount_Cur) / 100.0 / 21500.0 * 1.15
															[Energy_Amount_Cur (Methane/Heating Value Based)]

		FROM @Foundation											fnd

		LEFT OUTER JOIN fact.QuantityValue							v
			ON	v.Refnum			= fnd.Refnum
			AND	v.CalDateKey		= fnd.Plant_AnnDateKey
			AND	v.StreamId			= fnd.StreamId
			AND	v.StreamId			= 'FeedLtOther'

		INNER JOIN fact.CompositionQuantity							qc
			ON	qc.Refnum			= fnd.Refnum
			AND qc.CalDateKey		= fnd.Plant_AnnDateKey
			AND qc.StreamId			= fnd.StreamId
			AND qc.StreamId			= 'FeedLtOther'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			k
			ON	/*k.FactorSetId = fnd.FactorSetId
			AND */k.PricingMethodId = fnd.PricingMethodId
			AND	k.CountryId			= fnd.CountryId
			AND k.CalDateKey		= fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND k.CurrencyRpt		= fnd.CurrencyRpt
			AND	k.StreamId			= 'Light'
			AND k.ComponentId		= [qc].ComponentId

		INNER JOIN ante.PricingNetHeatingValueComposition			h
			ON	h.FactorSetId		= fnd.FactorSetId
			AND	h.CalDateKey		= fnd.FactorSet_AnnDateKey
			AND	h.ComponentId		= [qc].ComponentId

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[CH4]	
			ON	/*[CH4].FactorSetId = fnd.FactorSetId
			AND */[CH4].PricingMethodId = fnd.PricingMethodId
			AND	[CH4].CountryId		= fnd.CountryId
			AND [CH4].CalDateKey	= fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [CH4].CurrencyRpt	= fnd.CurrencyRpt
			AND	[CH4].StreamId		= 'Light'
			AND [CH4].ComponentId	= 'CH4'

		WHERE	fnd.StreamId = 'FeedLtOther'

		GROUP BY
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			v.Amount_Cur

		HAVING	fnd.Supersede_Amount_Cur	IS NOT NULL
			OR	v.Amount_Cur				IS NOT NULL
			OR	SUM(qc.Component_WtPcnt * k._MatlBal_Amount_Cur) IS NOT NULL
			OR	SUM(qc.Component_WtPcnt * h.NHValue_MBtuLb * [CH4]._MatlBal_Amount_Cur)	IS NOT NULL

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Light Feeds: EPMix)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			(ISNULL(qc.C3H6, 0.0) + ISNULL(qc.C3H8, 0.0) + ISNULL(qc.NBUTA, 0.0) + ISNULL(qc.IBUTA, 0.0) + ISNULL(qc.IB, 0.0) + ISNULL(qc.B1, 0.0) +
				ISNULL(qc.C4H6, 0.0) + ISNULL(qc.NC5, 0.0) + ISNULL(qc.IC5, 0.0) + ISNULL(qc.NC6, 0.0) + ISNULL(qc.C6ISO, 0.0) + ISNULL(qc.C7H16, 0.0) + ISNULL(qc.C8H18, 0.0))
				--	20121203	NC7
				--	20121203	NC8
				/ 100.0
				* ISNULL([C3H8]._MatlBal_Amount_Cur, 0.0) 
		
			+ CASE WHEN ISNULL(qc.CH4, 0.0) < 1.5
				THEN ISNULL(qc.CH4, 0.0)														/ 100.0 * ISNULL([C2H6]._MatlBal_Amount_Cur, 0.0)
				ELSE CASE WHEN ISNULL(qc.CH4, 0.0) < 2.5 THEN 1.5 ELSE ISNULL(qc.CH4, 0.0) END	/ 100.0 * ISNULL([CH4]._MatlBal_Amount_Cur, 0.0)
				END

			+ (ISNULL(qc.C2H4, 0.0) + ISNULL(qc.C2H6, 0.0)) / 100.0
				* ISNULL([EPMix]._Pricing_Amount_Cur, 0.0) 												[Composition_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN fact.CompositionQuantityPivot					qc
			ON	qc.Refnum = fnd.Refnum
			AND	qc.StreamId = fnd.StreamId
			AND	qc.StreamDescription = fnd.StreamDescription

		INNER JOIN [inter].PricingCompositionCountry				[C3H8]
			ON	/*[C3H8].FactorSetId = fnd.FactorSetId
			AND */[C3H8].PricingMethodId = fnd.PricingMethodId
			AND	[C3H8].CountryId = fnd.CountryId
			AND [C3H8].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [C3H8].CurrencyRpt = fnd.CurrencyRpt
			AND	[C3H8].StreamId = 'Light'
			AND [C3H8].ComponentId = 'C3H8'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[CH4]
			ON	/*[CH4].FactorSetId = fnd.FactorSetId
			AND */[CH4].PricingMethodId = fnd.PricingMethodId
			AND	[CH4].CountryId = fnd.CountryId
			AND [CH4].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [CH4].CurrencyRpt = fnd.CurrencyRpt
			AND	[CH4].StreamId = 'Light'
			AND [CH4].ComponentId = 'CH4'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[C2H6]
			ON	/*[C2H6].FactorSetId = fnd.FactorSetId
			AND */[C2H6].PricingMethodId = fnd.PricingMethodId
			AND	[C2H6].CountryId = fnd.CountryId
			AND [C2H6].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [C2H6].CurrencyRpt = fnd.CurrencyRpt
			AND	[C2H6].StreamId = 'Light'
			AND [C2H6].ComponentId = 'C2H6'

		LEFT OUTER JOIN [inter].PricingStreamCountry				[EPMix]
			ON	/*[EPMix].FactorSetId = fnd.FactorSetId
			AND */[EPMix].PricingMethodId = fnd.PricingMethodId
			AND	[EPMix].CountryId = fnd.CountryId
			AND [EPMix].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [EPMix].CurrencyRpt = fnd.CurrencyRpt
			AND	[EPMix].StreamId = fnd.StreamId
			AND	[EPMix].StreamId = 'EPMix'

		WHERE fnd.StreamId = 'EPMix';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Light Feeds: Ethane, Propane, LPG, Butane)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Composition_Amount_Cur,
			Adj_CompPurity_Cur
		)
		SELECT
			t.FactorSetId,
			t.ScenarioId,
			t.Refnum,
			t.Plant_QtrDateKey,
			t.CurrencyRpt,
			t.StreamId,
			t.StreamDescription,
			t.Quantity_kMT,
			t.Adj_NonStdStream_Coeff,
			t.Adj_Logistics_Cur,
			t.Supersede_Amount_Cur,

			t.Composition_Amount_Cur,

			CASE t.StreamId
				WHEN 'Ethane'	THEN CASE WHEN ISNULL(p.C2H4, 0.0) + ISNULL(p.C2H6, 0.0) > 90.0 THEN  NULL
								ELSE CASE WHEN ISNULL(p.C2H4, 0.0) + ISNULL(p.C2H6, 0.0) > 80.0 THEN -7.0
									ELSE -14.0
									END
								END
								
				WHEN 'Propane'	THEN CASE WHEN (ISNULL(p.C3H6, 0.0) + ISNULL(p.C3H8, 0.0)) > 90.0 THEN  NULL
								ELSE CASE WHEN (ISNULL(p.C3H6, 0.0) + ISNULL(p.C3H8, 0.0)) > 80.0 THEN -10.0
									ELSE -20.0
									END
								END
								
				WHEN 'LPG'		THEN CASE WHEN (calc.MaxValue(ISNULL(p.C3H6, 0.0) + ISNULL(p.C3H8, 0.0), ISNULL(p.NBUTA, 0.0) + ISNULL(p.IBUTA, 0.0) + ISNULL(p.IB, 0.0) + ISNULL(p.B1, 0.0))) > 90.0 THEN  NULL
								ELSE CASE WHEN (calc.MaxValue(ISNULL(p.C3H6, 0.0) + ISNULL(p.C3H8, 0.0), ISNULL(p.NBUTA, 0.0) + ISNULL(p.IBUTA, 0.0) + ISNULL(p.IB, 0.0) + ISNULL(p.B1, 0.0))) > 80.0 THEN -10.0
									ELSE -20.0
									END
								END
								
				WHEN 'Butane'	THEN CASE WHEN (ISNULL(p.NBUTA, 0.0) + ISNULL(p.IBUTA, 0.0) + ISNULL(p.IB, 0.0) + ISNULL(p.B1, 0.0)) > 90.0 THEN  NULL
								ELSE CASE WHEN (ISNULL(p.NBUTA, 0.0) + ISNULL(p.IBUTA, 0.0) + ISNULL(p.IB, 0.0) + ISNULL(p.B1, 0.0)) > 80.0 THEN -9.0
									ELSE -18.0
									END
								END
				END											[Composition_AdjPurity_Cur]
		FROM (
			SELECT
				fnd.FactorSetId,
				fnd.ScenarioId,
				fnd.Refnum,
				fnd.Plant_QtrDateKey,
				fnd.CurrencyRpt,
				fnd.StreamId,
				fnd.StreamDescription,
				fnd.Quantity_kMT,
				fnd.Adj_NonStdStream_Coeff,
				fnd.Adj_Logistics_Cur,
				fnd.Supersede_Amount_Cur,

				SUM(qc.Component_WtPcnt * k._MatlBal_Amount_Cur) / 100.0
																		[Composition_Amount_Cur]

			FROM @Foundation											fnd

			INNER JOIN fact.CompositionQuantity							qc
				ON	qc.Refnum = fnd.Refnum
				AND qc.StreamId = fnd.StreamId
				AND qc.StreamDescription = fnd.StreamDescription

			INNER JOIN [inter].PricingCompositionCountry				k
				ON	/*k.FactorSetId = fnd.FactorSetId
				AND */k.PricingMethodId = fnd.PricingMethodId
				AND	k.CountryId = fnd.CountryId
				AND k.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
				AND k.CurrencyRpt = fnd.CurrencyRpt
				AND	k.StreamId = 'Light'
				AND k.ComponentId = qc.ComponentId

			WHERE	fnd.StreamId IN ('Ethane', 'Propane', 'LPG', 'Butane')
				AND	k._MatlBal_Amount_Cur	IS NOT NULL
				AND	qc.Component_WtPcnt		IS NOT NULL

			GROUP BY
				fnd.FactorSetId,
				fnd.ScenarioId,
				fnd.Refnum,
				fnd.Plant_QtrDateKey,
				fnd.CurrencyRpt,
				fnd.StreamId,
				fnd.StreamDescription,
				fnd.Quantity_kMT,
				fnd.Adj_NonStdStream_Coeff,
				fnd.Adj_Logistics_Cur,
				fnd.Supersede_Amount_Cur
			)										t
		INNER JOIN calc.CompositionUnPivot			p
			ON	p.FactorSetId = t.FactorSetId
			AND	p.Refnum = t.Refnum
			AND	p.StreamId = t.StreamId
			AND	p.StreamDescription = t.StreamDescription;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Liquid Feeds: FeedLiqOther)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Client_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			v.Amount_Cur											[Client_Amount_Cur]

		FROM @Foundation											fnd

		LEFT OUTER JOIN fact.QuantityValue							v
			ON	v.Refnum = fnd.Refnum
			AND	v.StreamId = fnd.StreamId
			AND	v.StreamDescription = fnd.StreamDescription

		WHERE	fnd.StreamId = 'FeedLiqOther'
			AND	(fnd.Supersede_Amount_Cur IS NOT NULL
				OR v.Amount_Cur IS NOT NULL);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Liquid Feeds: NaphthaFr, NaphthaHv, NaphthaLt, Diesel)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			CASE WHEN a.Density_SG > 0.0
				THEN a.C5C6Weight_Fraction * [C5C6]._Pricing_Amount_Cur + a.HeavyWeight_Fraction * [Nap]._Pricing_Amount_Cur
				ELSE [Nap]._Pricing_Amount_Cur
				END													[Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN fact.FeedStockLiquidAttributes					a
			ON	a.FactorSetId = fnd.FactorSetId
			AND	a.Refnum = fnd.Refnum
			AND	a.CalDateKey = fnd.Plant_AnnDateKey
			AND a.StreamId = fnd.StreamId
			AND	a.StreamDescription = fnd.StreamDescription

		INNER JOIN [inter].PricingStreamCountry						[C5C6]
			ON	/*[C5C6].FactorSetId = fnd.FactorSetId
			AND */[C5C6].PricingMethodId = fnd.PricingMethodId
			AND [C5C6].CountryId = fnd.CountryId
			AND	[C5C6].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [C5C6].CurrencyRpt = fnd.CurrencyRpt
			AND	[C5C6].StreamId = 'LiqC5C6'

		LEFT OUTER JOIN [inter].PricingStreamCountry				[Nap]
			ON	/*[Nap].FactorSetId = fnd.FactorSetId
			AND */[Nap].PricingMethodId = fnd.PricingMethodId
			AND [Nap].CountryId = fnd.CountryId
			AND	[Nap].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [Nap].CurrencyRpt = fnd.CurrencyRpt
			AND	[Nap].StreamId = 'Naphtha'

		WHERE	fnd.StreamId IN ('NaphthaFr', 'NaphthaHv', 'NaphthaLt')
			OR(	fnd.StreamId = 'Diesel' AND fnd.Plant_QtrDateKey >= 20090000);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	--	Insert Records - Composition Based (Product)	---------------------------------
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Prod: Hydrogen)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur,
			Adj_CompPurity_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			[Hydrogen]._Pricing_Amount_Cur						[Country_Amount_Cur],
																
			(qr.H2 * [H2]._MatlBal_Amount_Cur + qr.CH4 * [CH4]._MatlBal_Amount_Cur) / 100.0
																[Composition_Amount_Cur],
			CASE WHEN qr.H2 < 52.8
				THEN - qr.H2 * calc.MinValue(100.0, (52.8 - qr.H2) * 2.0) / 100.0 * calc.MinValue(250.0, [H2]._MatlBal_Amount_Cur - 51600.0 / 21500.0 * [CH4]._MatlBal_Amount_Cur)
				END / 100.0										[Adj_CompPurity_Cur]

		FROM @Foundation											fnd

		INNER JOIN calc.CompositionUnPivot							qr
			ON	qr.FactorSetId = fnd.FactorSetId
			AND	qr.Refnum = fnd.Refnum
			AND qr.CalDateKey = fnd.Plant_AnnDateKey
			AND qr.StreamId = fnd.StreamId
			AND qr.StreamDescription = fnd.StreamDescription

		INNER JOIN [inter].PricingStreamCountry						[Hydrogen]
			ON	/*[Hydrogen].FactorSetId = fnd.FactorSetId
			AND	*/[Hydrogen].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [Hydrogen].CountryId = fnd.CountryId
			AND [Hydrogen].CurrencyRpt = fnd.CurrencyRpt
			AND	[Hydrogen].StreamId = fnd.StreamId

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[H2]
			ON	/*[H2].FactorSetId = fnd.FactorSetId
			AND	*/[H2].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [H2].CountryId = fnd.CountryId
			AND [H2].CurrencyRpt = fnd.CurrencyRpt
			AND	[H2].StreamId = 'Prod'
			AND	[H2].ComponentId = 'H2'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[CH4]
			ON	/*[CH4].FactorSetId = fnd.FactorSetId
			AND	*/[CH4].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [CH4].CountryId = [Hydrogen].CountryId
			AND [CH4].CurrencyRpt = fnd.CurrencyRpt
			AND	[CH4].StreamId = 'Prod'
			AND	[CH4].ComponentId = 'CH4'

		WHERE	fnd.StreamId = 'Hydrogen';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (Prod: Methane)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			[Methane]._Pricing_Amount_Cur							[Country_Amount_Cur],

			[CH4]._MatlBal_Amount_Cur * (qr.CH4 + (100.0 - qr.CH4) * 51.6 / 21.5) / 100.0
																	[Composition_Amount_Cur]

		FROM @Foundation											fnd
		
		INNER JOIN calc.CompositionUnPivot							qr
			ON	qr.FactorSetId = fnd.FactorSetId
			AND	qr.Refnum = fnd.Refnum
			AND qr.CalDateKey = fnd.Plant_AnnDateKey
			AND qr.StreamId = fnd.StreamId
			AND qr.StreamDescription = fnd.StreamDescription

		INNER JOIN [inter].PricingStreamCountry						[Methane]
			ON	/*[Methane].FactorSetId = fnd.FactorSetId
			AND	*/[Methane].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [Methane].CountryId = fnd.CountryId
			AND [Methane].CurrencyRpt = fnd.CurrencyRpt
			AND	[Methane].StreamId = 'Methane'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[CH4]
			ON	/*[CH4].FactorSetId = fnd.FactorSetId
			AND	*/[CH4].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [CH4].CountryId = fnd.CountryId
			AND [CH4].CurrencyRpt = fnd.CurrencyRpt
			AND	[CH4].StreamId = 'Prod'
			AND [CH4].ComponentId = 'CH4'

		WHERE	fnd.StreamId = 'Methane';

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	--	Insert Records - Stream Based (Product)	-----------------------------------------
	BEGIN TRY
  
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (EthylenePG, MetaOCEthylene)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur									[Country_Amount_Cur],
																
			calc.MinValue(1.0, COALESCE(qr.C2H4, 0.0) / sp.Purity_WtPcnt) * k._MatlBal_Amount_Cur
																	[Composition_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN ante.PricingStreamPurityLimits					sp
			ON	sp.FactorSetId		= fnd.FactorSetId
			AND	sp.StreamId			= 'EthylenePG'
			AND	sp.ComponentId		= 'C2H4'
		
		LEFT OUTER JOIN calc.CompositionUnPivot						qr
			ON	qr.FactorSetId		= fnd.FactorSetId
			AND	qr.Refnum			= fnd.Refnum
			AND qr.CalDateKey		= fnd.Plant_AnnDateKey
			AND qr.StreamId			= sp.StreamId
		
		INNER JOIN [inter].PricingStreamCountry						k
			ON	k.CalDateKey		= fnd.Plant_QtrDateKey
			AND k.CountryId			= fnd.CountryId
			AND k.CurrencyRpt		= fnd.CurrencyRpt
			AND	k.StreamId			= sp.StreamId
		
		WHERE	fnd.StreamId IN ('EthylenePG', 'MetaOCEthylene');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (EthyleneCG)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			[EthyleneCG]._MatlBal_Amount_Cur						[Country_Amount_Cur],
		
			CASE WHEN qr.C2H4 > sp.Purity_WtPcnt
				THEN [EthyleneCG]._MatlBal_Amount_Cur
				ELSE (qr.C2H4 * [EthyleneCG]._MatlBal_Amount_Cur + (100.0 - qr.C2H4) * [Ethane]._MatlBal_Amount_Cur) / 100.0
				END													[Composition_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN ante.PricingStreamPurityLimits					sp
			ON	sp.FactorSetId	= fnd.FactorSetId
			AND	sp.StreamId		= fnd.StreamId
			AND	sp.ComponentId	= 'C2H4'
		
		INNER JOIN calc.CompositionUnPivot							qr
			ON	qr.FactorSetId	= fnd.FactorSetId
			AND	qr.Refnum		= fnd.Refnum
			AND qr.CalDateKey	= fnd.Plant_AnnDateKey
			AND qr.StreamId		= fnd.StreamId
			AND qr.StreamDescription = fnd.StreamDescription
		
		INNER JOIN [inter].PricingStreamCountry						[EthyleneCG]
			ON	/*[EthyleneCG].FactorSetId = fnd.FactorSetId
			AND	*/[EthyleneCG].CalDateKey	= fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [EthyleneCG].CountryId		= fnd.CountryId
			AND [EthyleneCG].CurrencyRpt	= fnd.CurrencyRpt
			AND	[EthyleneCG].StreamId		= fnd.StreamId
		
		LEFT OUTER JOIN [inter].PricingStreamCountry				[Ethane]
			ON	/*[Ethane].FactorSetId = fnd.FactorSetId
			AND	*/[Ethane].CalDateKey	= fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [Ethane].CountryId		= fnd.CountryId
			AND [Ethane].CurrencyRpt	= fnd.CurrencyRpt
			AND	[Ethane].StreamId		= 'Ethane'
		
		WHERE	fnd.StreamId = 'EthyleneCG';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PropylenePG, MetaProdPropylenePG)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			[PropylenePG]._MatlBal_Amount_Cur					[Country_Amount_Cur],
		
			[PropylenePG]._MatlBal_Amount_Cur +
			  ([PropylenePG]._MatlBal_Amount_Cur - [PropyleneCG]._MatlBal_Amount_Cur)
				* CASE WHEN coalesce(qr.C3H6, 0.0) > sp.PurityUpper_WtPcnt
					THEN 0.0
					ELSE  calc.MaxValue(-1.0, -(sp.PurityUpper_WtPcnt - qr.C3H6)/(sp.PurityUpper_WtPcnt - sp.PurityLower_WtPcnt))
					END											[Composition_Amount_Cur]
		FROM @Foundation											fnd
		
		INNER JOIN ante.PricingStreamPurityLimits					sp
			ON	sp.FactorSetId = fnd.FactorSetId
			AND	sp.StreamId = 'PropylenePG'
			AND	sp.ComponentId = 'C3H6'
		
		LEFT OUTER JOIN calc.CompositionUnPivot						qr
			ON	qr.FactorSetId = fnd.FactorSetId
			AND	qr.Refnum = fnd.Refnum
			AND qr.CalDateKey = fnd.Plant_AnnDateKey
			AND qr.StreamId = sp.StreamId
		
		INNER JOIN [inter].PricingStreamCountry						[PropylenePG]
			ON	[PropylenePG].CalDateKey = fnd.Plant_QtrDateKey
			AND [PropylenePG].CountryId = fnd.CountryId
			AND [PropylenePG].CurrencyRpt = fnd.CurrencyRpt
			AND	[PropylenePG].StreamId = sp.StreamId
		
		INNER JOIN [inter].PricingStreamCountry						[PropyleneCG]
			ON	[PropyleneCG].CalDateKey = fnd.Plant_QtrDateKey
			AND [PropyleneCG].CountryId = fnd.CountryId
			AND [PropyleneCG].CurrencyRpt = fnd.CurrencyRpt
			AND	[PropyleneCG].StreamId = 'PropyleneCG'

		WHERE	fnd.StreamId IN ('PropylenePG', 'MetaProdPropylenePG');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PropyleneCG)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			[PropyleneCG]._MatlBal_Amount_Cur					[Country_Amount_Cur],
		
			CASE WHEN fnd.[EconRegionId] = 'EUR'
				THEN qr.C3H6 * [PropyleneCG]._MatlBal_Amount_Cur / 100.0
				ELSE calc.MinValue(0.99 * [PropylenePG]._MatlBal_Amount_Cur,
					CASE WHEN qr.C3H6 >= sp.PurityUpper_WtPcnt
					THEN	  (qr.C3H6 - sp.PurityUpper_WtPcnt)/(pp.PurityUpper_WtPcnt - sp.PurityUpper_WtPcnt)		* [PropylenePG]._MatlBal_Amount_Cur
					 + (1.0 - (qr.C3H6 - sp.PurityUpper_WtPcnt)/(pp.PurityUpper_WtPcnt - sp.PurityUpper_WtPcnt))	* [PropyleneCG]._MatlBal_Amount_Cur
					ELSE
						CASE WHEN qr.C3H6 > sp.PurityLower_WtPcnt
						THEN [PropyleneCG]._MatlBal_Amount_Cur
						ELSE (qr.C3H6 * [PropyleneCG]._MatlBal_Amount_Cur + (100.0 - qr.C3H6) * [Propane]._MatlBal_Amount_Cur) / 100.0
						END
					END)
				END												[Composition_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN ante.PricingStreamPurityLimits					sp
			ON	sp.FactorSetId = fnd.FactorSetId
			AND	sp.StreamId = fnd.StreamId
			AND	sp.ComponentId = 'C3H6'

		INNER JOIN calc.CompositionUnPivot							qr
			ON	qr.FactorSetId = fnd.FactorSetId
			AND	qr.Refnum = fnd.Refnum
			AND qr.CalDateKey = fnd.Plant_AnnDateKey
			AND qr.StreamId = fnd.StreamId
			AND qr.StreamDescription = fnd.StreamDescription

		INNER JOIN ante.PricingStreamPurityLimits					pp
			ON	pp.FactorSetId = fnd.FactorSetId
			AND	pp.StreamId = 'PropylenePG'
			AND	pp.ComponentId = 'C3H6'

		INNER JOIN [inter].PricingStreamCountry						[PropyleneCG]
			ON	/*[PropyleneCG].FactorSetId = fnd.FactorSetId
			AND	*/[PropyleneCG].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [PropyleneCG].CountryId = fnd.CountryId
			AND [PropyleneCG].CurrencyRpt = fnd.CurrencyRpt
			AND	[PropyleneCG].StreamId = 'PropyleneCG'

		LEFT OUTER JOIN [inter].PricingStreamCountry				[PropylenePG]
			ON	/*[PropylenePG].FactorSetId = fnd.FactorSetId
			AND	*/[PropylenePG].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [PropylenePG].CountryId = fnd.CountryId
			AND [PropylenePG].CurrencyRpt = fnd.CurrencyRpt
			AND	[PropylenePG].StreamId = 'PropylenePG'

		LEFT OUTER JOIN [inter].PricingStreamCountry				[Propane]
			ON	/*[Propane].FactorSetId = fnd.FactorSetId
			AND	*/[Propane].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [Propane].CountryId = fnd.CountryId
			AND [Propane].CurrencyRpt = fnd.CurrencyRpt
			AND	[Propane].StreamId = 'Propane'

		WHERE	fnd.StreamId = 'PropyleneCG';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PropyleneRG)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			[PropyleneRG]._MatlBal_Amount_Cur					[Country_Amount_Cur],

			CASE WHEN qr.C3H6 > 80.0
				THEN (0.9 * [PropyleneCG]._MatlBal_Amount_Cur * qr.C3H6 + [Propane]._MatlBal_Amount_Cur * (100.0 - qr.C3H6)) / 100.0
				ELSE
					CASE WHEN qr.C3H6 < 70.0
					THEN ([PropyleneRG]._MatlBal_Amount_Cur * qr.C3H6 + [Propane]._MatlBal_Amount_Cur * (100.0 - qr.C3H6)) / 100.0
					ELSE [PropyleneRG]._MatlBal_Amount_Cur
					END
				END												[Composition_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN ante.PricingStreamPurityLimits					sp
			ON	sp.FactorSetId = fnd.FactorSetId
			AND	sp.StreamId = fnd.StreamId
			AND	sp.ComponentId = 'C3H6'

		INNER JOIN calc.CompositionUnPivot							qr
			ON	qr.FactorSetId = fnd.FactorSetId
			AND	qr.Refnum = fnd.Refnum
			AND qr.CalDateKey = fnd.Plant_AnnDateKey
			AND qr.StreamId = fnd.StreamId
			AND qr.StreamDescription = fnd.StreamDescription

		INNER JOIN [inter].PricingStreamCountry						[PropyleneRG]
			ON	/*[PropyleneRG].FactorSetId = fnd.FactorSetId
			AND	*/[PropyleneRG].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [PropyleneRG].CountryId = fnd.CountryId
			AND [PropyleneRG].CurrencyRpt = fnd.CurrencyRpt
			AND	[PropyleneRG].StreamId = 'PropyleneRG'

		LEFT OUTER JOIN [inter].PricingStreamCountry					[PropyleneCG]
			ON	/*[PropyleneCG].FactorSetId = fnd.FactorSetId
			AND	*/[PropyleneCG].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [PropyleneCG].CountryId = fnd.CountryId
			AND [PropyleneCG].CurrencyRpt = fnd.CurrencyRpt
			AND	[PropyleneCG].StreamId = 'PropyleneCG'

		LEFT OUTER JOIN [inter].PricingStreamCountry					[Propane]
			ON	/*[Propane].FactorSetId = fnd.FactorSetId
			AND	*/[Propane].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [Propane].CountryId = fnd.CountryId
			AND [Propane].CurrencyRpt = fnd.CurrencyRpt
			AND	[Propane].StreamId = 'Propane'

		WHERE	fnd.StreamId = 'PropyleneRG';

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PropaneC3Resid, MetaProdUnReactLightPurge)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			[Propane]._MatlBal_Amount_Cur						[Country_Amount_Cur],
													
			CASE WHEN COALESCE(qr.C3H8, 0.0) > 90.0
				THEN [Propane]._MatlBal_Amount_Cur
				ELSE calc.MinValue([Propane]._MatlBal_Amount_Cur, [LPG]._MatlBal_Amount_Cur)
				END												[Composition_Amount_Cur]

		FROM @Foundation											fnd

		INNER JOIN ante.PricingStreamPurityLimits					sp
			ON	sp.FactorSetId = fnd.FactorSetId
			AND	sp.StreamId = 'PropaneC3Resid'
			AND	sp.ComponentId = 'C3H8'

		LEFT OUTER JOIN calc.CompositionUnPivot						qr
			ON	qr.FactorSetId = fnd.FactorSetId
			AND	qr.Refnum = fnd.Refnum
			AND qr.CalDateKey = fnd.Plant_AnnDateKey
			AND qr.StreamId = sp.StreamId

		INNER JOIN [inter].PricingStreamCountry						[Propane]
			ON	[Propane].CalDateKey = fnd.Plant_QtrDateKey
			AND [Propane].CountryId = fnd.CountryId
			AND [Propane].CurrencyRpt = fnd.CurrencyRpt
			AND	[Propane].StreamId = 'Propane'

		INNER JOIN [inter].PricingStreamCountry						[LPG]
			ON	[LPG].CalDateKey = fnd.Plant_QtrDateKey
			AND [LPG].CountryId = fnd.CountryId
			AND [LPG].CurrencyRpt = fnd.CurrencyRpt
			AND	[LPG].StreamId = 'LPG'

		WHERE	fnd.StreamId IN ('PropaneC3Resid', 'MetaProdUnReactLightPurge');

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (AcidGas, Losses)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			fnd.Quantity_kMT,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			0.0										[Country_Amount_Cur]

		FROM @Foundation											fnd
		--	Task 141: Implement Tri-Tables in calculations
		--WHERE	fnd.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('AcidGas,Loss', 1, ',') d);
		WHERE	fnd.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fnd.FactorSetId AND d.StreamId IN ('AcidGas', 'Loss'));

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	--	Insert Records - ProdOther, SuppOther	-----------------------------------------
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (ProdOther, SuppOther)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			t.FactorSetId,
			t.ScenarioId,
			t.Refnum,
			t.Plant_QtrDateKey,
			t.CurrencyRpt,
			t.StreamId,
			t.StreamDescription,
			t.Quantity_kMT,
			t.Adj_NonStdStream_Coeff,
			t.Adj_Logistics_Cur,
			t.Supersede_Amount_Cur,

			SUM(t.Component_WtPcnt * t._MatlBal_Amount_Cur) / 100.0		[Country_Amount_Cur],
			SUM(t.Component_WtPcnt * t.Composition_Amount_Cur) / 100.0	[Composition_Amount_Cur]

		FROM (

			SELECT
				fnd.FactorSetId,
				fnd.ScenarioId,
				fnd.Refnum,
				fnd.Plant_QtrDateKey,
				fnd.CurrencyRpt,
				fnd.StreamId,
				fnd.StreamDescription,
				fnd.Quantity_kMT,
				fnd.Adj_NonStdStream_Coeff,
				fnd.Adj_Logistics_Cur,
				fnd.Supersede_Amount_Cur,

				c.ComponentId,
				c.Component_WtPcnt,
				k._MatlBal_Amount_Cur,
			
				CASE c.ComponentId
					WHEN 'H2'		THEN
						CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [CH4]._MatlBal_Amount_Cur * 51500.0 / 21500.0 END
					WHEN 'C2H2'		THEN
						CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt
							THEN CASE WHEN [C2H4_Comp].Component_WtPcnt > [C2H4_ppt].Purity_WtPcnt THEN [C2H4_price]._MatlBal_Amount_Cur ELSE [CH4]._MatlBal_Amount_Cur  END
							ELSE [CH4]._MatlBal_Amount_Cur
							END
					WHEN 'C2H4'		THEN
						CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [CH4]._MatlBal_Amount_Cur END
					WHEN 'C3H6'		THEN
						CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE
							CASE WHEN
								SUM(CASE WHEN c.ComponentId IN ('C3H6', 'C3H8', 'C4H6', 'C4H8', 'C4H10') THEN c.Component_WtPcnt END) OVER(PARTITION BY
									  fnd.FactorSetId
									, fnd.ScenarioId
									, fnd.Refnum
									, fnd.Plant_QtrDateKey
									, fnd.CurrencyRpt	
									, fnd.StreamId
									, fnd.StreamDescription
									) > 75.0
							THEN [C3H8]._MatlBal_Amount_Cur
							ELSE [CH4]._MatlBal_Amount_Cur
							END
						END
					WHEN 'C4H6'		THEN
						CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [C3H8]._MatlBal_Amount_Cur END
					WHEN 'C6H6'		THEN
						CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [PyroGasOil]._MatlBal_Amount_Cur END
					WHEN 'Inerts'	THEN
						0.0
					ELSE
						k._MatlBal_Amount_Cur
					END													[Composition_Amount_Cur]

			FROM @Foundation											fnd

			INNER JOIN calc.CompositionStream							c
				ON	c.FactorSetId		= fnd.FactorSetId
				AND	c.Refnum			= fnd.Refnum
				AND	c.CalDateKey		= fnd.Plant_QtrDateKey
				AND	c.StreamId			= fnd.StreamId
				AND	c.StreamDescription = fnd.StreamDescription

			INNER JOIN [inter].PricingCompositionCountry				k
				ON	/*k.FactorSetId = fnd.FactorSetId
				AND	*/k.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
				AND k.CountryId = fnd.CountryId
				AND k.CurrencyRpt = fnd.CurrencyRpt
				AND	k.StreamId = 'Prod'
				AND	k.ComponentId = [c].ComponentId

			LEFT OUTER JOIN ante.PricingStreamPurityLimits				ppt
				ON	ppt.FactorSetId = fnd.FactorSetId
				AND ppt.StreamId = fnd.StreamId
				AND ppt.ComponentId = [c].ComponentId

			LEFT OUTER JOIN [inter].PricingCompositionCountry			[CH4]
				ON	/*[CH4].FactorSetId = fnd.FactorSetId
				AND	*/[CH4].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
				AND [CH4].CountryId = fnd.CountryId
				AND [CH4].CurrencyRpt = fnd.CurrencyRpt
				AND	[CH4].StreamId = 'Prod'
				AND	[CH4].ComponentId = 'CH4'

			LEFT OUTER JOIN [inter].PricingCompositionCountry			[C3H8]
				ON	/*[C3H8].FactorSetId = fnd.FactorSetId
				AND	*/[C3H8].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
				AND [C3H8].CountryId = fnd.CountryId
				AND [C3H8].CurrencyRpt = fnd.CurrencyRpt
				AND	[C3H8].StreamId = 'Prod'
				AND	[C3H8].ComponentId = 'C3H8'

			LEFT OUTER JOIN [inter].PricingCompositionCountry			[PyroGasOil]
				ON	/*[PyroGasOil].FactorSetId = fnd.FactorSetId
				AND	*/[PyroGasOil].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
				AND [PyroGasOil].CountryId = fnd.CountryId
				AND [PyroGasOil].CurrencyRpt = fnd.CurrencyRpt
				AND	[PyroGasOil].StreamId = 'Prod'
				AND	[PyroGasOil].ComponentId = 'PyroGasOil'

			LEFT OUTER JOIN ante.PricingStreamPurityLimits				[C2H4_ppt]
				ON	[C2H4_ppt].FactorSetId = fnd.FactorSetId
				AND [C2H4_ppt].StreamId = fnd.StreamId
				AND [C2H4_ppt].ComponentId = 'C2H4'

			LEFT OUTER JOIN calc.CompositionStream						[C2H4_Comp]
				ON	[C2H4_Comp].FactorSetId = fnd.FactorSetId
				AND	[C2H4_Comp].Refnum = fnd.Refnum
				AND	[C2H4_Comp].CalDateKey = fnd.Plant_QtrDateKey
				AND	[C2H4_Comp].StreamId = fnd.StreamId
				AND	[C2H4_Comp].StreamDescription = fnd.StreamDescription
				AND [C2H4_Comp].ComponentId = 'C2H4'

			LEFT OUTER JOIN [inter].PricingCompositionCountry			[C2H4_price]
				ON	/*[C2H4_price].FactorSetId = fnd.FactorSetId
				AND	*/[C2H4_price].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
				AND [C2H4_price].CountryId = fnd.CountryId
				AND [C2H4_price].CurrencyRpt = fnd.CurrencyRpt
				AND	[C2H4_price].StreamId = 'Prod'
				AND	[C2H4_price].ComponentId = 'C2H4'

			WHERE	fnd.StreamId IN ('ProdOther', 'SuppOther')
		)		t
		GROUP BY
			t.FactorSetId,
			t.ScenarioId,
			t.Refnum,
			t.Plant_QtrDateKey,
			t.CurrencyRpt,
			t.StreamId,
			t.StreamDescription,
			t.Quantity_kMT,
			t.Adj_NonStdStream_Coeff,
			t.Adj_Logistics_Cur,
			t.Supersede_Amount_Cur;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantComposition (ProdOther)';
		PRINT @ProcedureDesc;

		INSERT INTO [calc].[PricingPlantComposition](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, ComponentId, Component_kMT, Component_WtPcnt, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Supersede_Amount_Cur,
			Country_Amount_Cur,
			Composition_Amount_Cur
		)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			fnd.StreamId,
			fnd.StreamDescription,
			c.ComponentId,
			fnd.Quantity_kMT * c.Component_WtPcnt / 100.0		[Quantity_kMT],
			c.Component_WtPcnt,
			fnd.Adj_NonStdStream_Coeff,
			fnd.Adj_Logistics_Cur,
			fnd.Supersede_Amount_Cur,

			k._MatlBal_Amount_Cur,

			CASE c.ComponentId
				WHEN 'H2'		THEN
					CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [CH4]._MatlBal_Amount_Cur * 51500.0 / 21500.0 END
				WHEN 'C2H2'		THEN
					CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt
						THEN CASE WHEN [C2H4_Comp].Component_WtPcnt > [C2H4_ppt].Purity_WtPcnt THEN [C2H4_price]._MatlBal_Amount_Cur ELSE [CH4]._MatlBal_Amount_Cur  END
						ELSE [CH4]._MatlBal_Amount_Cur
						END
				WHEN 'C2H4'		THEN
					CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [CH4]._MatlBal_Amount_Cur END
				WHEN 'C3H6'		THEN
					CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE
						CASE WHEN
							SUM(CASE WHEN c.ComponentId IN ('C3H6', 'C3H8', 'C4H6', 'C4H8', 'C4H10') THEN c.Component_WtPcnt END) OVER(PARTITION BY
									fnd.FactorSetId
								, fnd.ScenarioId
								, fnd.Refnum
								, fnd.Plant_QtrDateKey
								, fnd.CurrencyRpt	
								, fnd.StreamId
								, fnd.StreamDescription
								) > 75.0
						THEN [C3H8]._MatlBal_Amount_Cur
						ELSE [CH4]._MatlBal_Amount_Cur
						END
					END
				WHEN 'C4H6'		THEN
					CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [C3H8]._MatlBal_Amount_Cur END
				WHEN 'C6H6'		THEN
					CASE WHEN c.Component_WtPcnt > ppt.Purity_WtPcnt THEN k._MatlBal_Amount_Cur ELSE [PyroGasOil]._MatlBal_Amount_Cur END
				WHEN 'Inerts'	THEN
					0.0
				ELSE 
					0.0
				END													[Composition_Amount_Cur]

		FROM @Foundation											fnd
				
		INNER JOIN fact.CompositionQuantity							c
			ON	c.Refnum = fnd.Refnum
			AND	c.StreamId = fnd.StreamId
			AND	c.CalDateKey = fnd.Plant_AnnDateKey
			AND	c.StreamDescription = fnd.StreamDescription

		INNER JOIN [inter].PricingCompositionCountry				k
			ON	/*k.FactorSetId = fnd.FactorSetId
			AND	*/k.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND k.CountryId = fnd.CountryId
			AND k.CurrencyRpt = fnd.CurrencyRpt
			AND	k.StreamId = 'Prod'
			AND	k.ComponentId = [c].ComponentId

		LEFT OUTER JOIN ante.PricingStreamPurityLimits				ppt
			ON	ppt.FactorSetId = fnd.FactorSetId
			AND ppt.StreamId = fnd.StreamId
			AND ppt.ComponentId = [c].ComponentId

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[CH4]
			ON	/*[CH4].FactorSetId = fnd.FactorSetId
			AND	*/[CH4].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [CH4].CountryId = fnd.CountryId
			AND [CH4].CurrencyRpt = fnd.CurrencyRpt
			AND	[CH4].StreamId = 'Prod'
			AND	[CH4].ComponentId = 'CH4'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[C3H8]
			ON	/*[C3H8].FactorSetId = fnd.FactorSetId
			AND	*/[C3H8].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [C3H8].CountryId = fnd.CountryId
			AND [C3H8].CurrencyRpt = fnd.CurrencyRpt
			AND	[C3H8].StreamId = 'Prod'
			AND	[C3H8].ComponentId = 'C3H8'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[PyroGasOil]
			ON	/*[PyroGasOil].FactorSetId = fnd.FactorSetId
			AND	*/[PyroGasOil].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [PyroGasOil].CountryId = fnd.CountryId
			AND [PyroGasOil].CurrencyRpt = fnd.CurrencyRpt
			AND	[PyroGasOil].StreamId = 'Prod'
			AND	[PyroGasOil].ComponentId = 'PyroGasOil'

		LEFT OUTER JOIN ante.PricingStreamPurityLimits				[C2H4_ppt]
			ON	[C2H4_ppt].FactorSetId = fnd.FactorSetId
			AND [C2H4_ppt].StreamId = fnd.StreamId
			AND [C2H4_ppt].ComponentId = 'C2H4'

		LEFT OUTER JOIN fact.CompositionQuantity					[C2H4_Comp]
			ON	[C2H4_Comp].Refnum = fnd.Refnum
			AND	[C2H4_Comp].StreamId = fnd.StreamId
			AND	[C2H4_Comp].StreamDescription = fnd.StreamDescription
			AND [C2H4_Comp].ComponentId = 'C2H4'

		LEFT OUTER JOIN [inter].PricingCompositionCountry			[C2H4_price]
			ON	/*[C2H4_price].FactorSetId = fnd.FactorSetId
			AND	*/[C2H4_price].CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND [C2H4_price].CountryId = fnd.CountryId
			AND [C2H4_price].CurrencyRpt = fnd.CurrencyRpt
			AND	[C2H4_price].StreamId = 'Prod'
			AND	[C2H4_price].ComponentId = 'C2H4'

		WHERE	fnd.StreamId = 'ProdOther';

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	--	Insert Records - PPFC	---------------------------------------------------------
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PPFC)';
		PRINT @ProcedureDesc;

		DECLARE @PPFCQuantity TABLE(
			Refnum						VARCHAR (25)		NOT NULL	CHECK(Refnum <> ''),
			AccountId					VARCHAR (42)		NOT	NULL	CHECK(AccountId <> ''),
			Quantity_kMT				REAL				NOT	NULL	CHECK(Quantity_kMT >= 0.0)
			PRIMARY KEY CLUSTERED(Refnum ASC, AccountId ASC)
			);

		DECLARE @PPFCLiquid TABLE(
			FactorSetId					VARCHAR (12)		NOT	NULL	CHECK(FactorSetId <> ''),
			ScenarioId					VARCHAR (42)		NOT	NULL	CHECK(ScenarioId <> ''),
			Refnum						VARCHAR (25)		NOT NULL	CHECK(Refnum <> ''),
			CountryId					CHAR (3)			NOT	NULL	CHECK(CountryId <> ''),
			AnnDateKey					INT					NOT	NULL	CHECK(AnnDateKey > 19000101 AND AnnDateKey < 99991231),
			CalDateKey					INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			CurrencyRpt					VARCHAR (4)			NOT	NULL	CHECK(CurrencyRpt <> ''),
			AccountId					VARCHAR (42)		NOT	NULL	CHECK(AccountId <> ''),

			Quantity_kMT				REAL				NOT	NULL	CHECK(Quantity_kMT >= 0.0),
			Composition_Amount_Cur		REAL				NOT	NULL	CHECK(Composition_Amount_Cur >= 0.0),

			_MatlBal_Amount_Cur			AS CONVERT(REAL, Quantity_kMT * Composition_Amount_Cur, 1)
										PERSISTED			NOT	NULL	CHECK(_MatlBal_Amount_Cur >= 0.0),

			Supersede_Cur				REAL					NULL	CHECK(Supersede_Cur >= 0.0),
			_Supersede_Amount_Cur		AS CONVERT(REAL, Quantity_kMT * Supersede_Cur, 1)
										PERSISTED						CHECK(_Supersede_Amount_Cur >= 0.0),
			
			PRIMARY KEY CLUSTERED(FactorSetId ASC, ScenarioId ASC, Refnum ASC, CurrencyRpt ASC, AccountId ASC, CalDateKey ASC)
			);

		DECLARE @PPFCLiquidAggregate TABLE(
			FactorSetId					VARCHAR (12)		NOT	NULL	CHECK(FactorSetId <> ''),
			ScenarioId					VARCHAR (42)		NOT	NULL	CHECK(ScenarioId <> ''),
			Refnum						VARCHAR (25)		NOT NULL	CHECK(Refnum <> ''),
			CountryId					CHAR (3)			NOT	NULL	CHECK(CountryId <> ''),
			AnnDateKey					INT					NOT	NULL	CHECK(AnnDateKey > 19000101 AND AnnDateKey < 99991231),
			CurrencyRpt					VARCHAR (4)			NOT	NULL	CHECK(CurrencyRpt <> ''),
			AccountId					VARCHAR (42)		NOT	NULL	CHECK(AccountId <> ''),

			Quantity_kMT				REAL				NOT	NULL	CHECK(Quantity_kMT >= 0.0)

			PRIMARY KEY CLUSTERED(FactorSetId ASC, ScenarioId ASC, Refnum ASC, CurrencyRpt ASC, AccountId ASC, AnnDateKey DESC)
			);

		DECLARE @PPFCQuantityComp TABLE(
			FactorSetId					VARCHAR (12)		NOT	NULL	CHECK(FactorSetId <> ''),
			Refnum						VARCHAR (25)		NOT NULL	CHECK(Refnum <> ''),
			CalDateKey					INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			StreamId					VARCHAR (42)		NOT	NULL	CHECK(StreamId = 'PPFC'),
			Component_kMT				REAL				NOT	NULL	CHECK(Component_kMT >= 0.0),

			PRIMARY KEY CLUSTERED(FactorSetId ASC, Refnum ASC, CalDateKey ASC)
			);

		DECLARE @PPFCPricingCountryAvg TABLE(
			FactorSetId					VARCHAR (12)		NOT	NULL	CHECK(FactorSetId <> ''),
			CalDateKey					INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			CountryId					CHAR (3)			NOT	NULL	CHECK(CountryId <> ''),
			CurrencyRpt					VARCHAR (4)			NOT	NULL	CHECK(CurrencyRpt <> ''),
			StreamId					VARCHAR (42)		NOT	NULL	CHECK(StreamId = 'PricingAdj'),
			MatlBal_Amount_Cur			REAL				NOT	NULL	CHECK(MatlBal_Amount_Cur >= 0.0),
			MatlBal_AmountAvg_Cur		REAL				NOT	NULL	CHECK(MatlBal_AmountAvg_Cur >= 0.0),
			_MatlBal_Amount_Pcnt		AS CONVERT(REAL, MatlBal_Amount_Cur / MatlBal_AmountAvg_Cur)
										PERSISTED			NOT NULL	CHECK(_MatlBal_Amount_Pcnt >= 0.0),

			PRIMARY KEY CLUSTERED(FactorSetId ASC, CountryId ASC, CurrencyRpt ASC, StreamId ASC, CalDateKey ASC)
			);

		DECLARE @PPFCFuelGas TABLE(
			FactorSetId					VARCHAR (12)		NOT	NULL	CHECK(FactorSetId <> ''),
			ScenarioId					VARCHAR (42)		NOT	NULL	CHECK(ScenarioId <> ''),
			Refnum						VARCHAR (25)		NOT NULL	CHECK(Refnum <> ''),
			CountryId					CHAR (3)			NOT	NULL	CHECK(LEN(CountryId) = 3),
			AnnDateKey					INT					NOT	NULL	CHECK(AnnDateKey > 19000101 AND AnnDateKey < 99991231),
			CalDateKey					INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			CurrencyRpt					VARCHAR (4)			NOT	NULL	CHECK(CurrencyRpt <> ''),
			AccountId					VARCHAR (42)		NOT	NULL	CHECK(AccountId <> ''),

			Quantity_kMT				REAL				NOT	NULL	CHECK(Quantity_kMT >= 0.0),
			LHValue_MBTU				REAL				NOT	NULL	CHECK(LHValue_MBTU >= 0.0),
			Composition_Amount_Cur		REAL				NOT	NULL	CHECK(Composition_Amount_Cur >= 0.0),

			_MatlBal_Amount_Cur			AS CONVERT(REAL, Quantity_kMT * Composition_Amount_Cur, 1)
										PERSISTED			NOT	NULL	CHECK(_MatlBal_Amount_Cur >= 0.0),

			Supersede_Cur				REAL					NULL	CHECK(Supersede_Cur >= 0.0),
			_Supersede_Amount_Cur		AS CONVERT(REAL, Quantity_kMT * Supersede_Cur, 1)
										PERSISTED						CHECK(_Supersede_Amount_Cur >= 0.0),

			PRIMARY KEY CLUSTERED(FactorSetId ASC, ScenarioId ASC, Refnum ASC, CurrencyRpt ASC, AccountId ASC, CalDateKey ASC)
			);

		INSERT INTO @PPFCQuantity (Refnum, AccountId, Quantity_kMT)
		SELECT
			q.Refnum,
			q.StreamId,
			SUM(q.Quantity_kMT)		[Quantity_kMT]
		FROM fact.Quantity	q
		WHERE	q.StreamId = 'PPFC'
			AND	q.Quantity_kMT <> 0.0
			AND	q.Refnum IN (SELECT tsq.Refnum FROM @fpl					 tsq)
		GROUP BY
			q.Refnum,
			q.StreamId;

		INSERT INTO @PPFCLiquid(FactorSetId, ScenarioId, Refnum, CountryId, AnnDateKey, CalDateKey, CurrencyRpt, AccountId, Quantity_kMT, Composition_Amount_Cur, Supersede_Cur)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.CountryId,
			fnd.Plant_AnnDateKey,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			h.AccountId,

			h.LHValue_MBTU / v.NHValue_MBtuLb / 2.204623 / qp.Quantity_kMT * fnd.Quantity_kMT		[Quantity_kMT],
			p._MatlBal_Amount_Cur																	[Composition_Amount_Cur],
			fnd.Supersede_Amount_Cur
			
		FROM @Foundation											fnd

		INNER JOIN @PPFCQuantity									qp
			ON	qp.Refnum = fnd.Refnum
			AND	qp.Quantity_kMT <> 0.0

		INNER JOIN fact.EnergyLHValue								h
			ON	h.Refnum = fnd.Refnum
			AND	h.CalDateKey = fnd.Plant_AnnDateKey
			AND	h.AccountId IN ('PPFCEthane', 'PPFCPropane', 'PPFCButane', 'PPFCPyroNaphtha', 'PPFCPyroGasOil', 'PPFCPyroFuelOil')
			AND	h.LHValue_MBTU <> 0.0

		INNER JOIN ante.PricingNetHeatingValueStream				v
			ON	v.FactorSetId = fnd.FactorSetId
			AND	v.StreamId = h.AccountId
			AND v.CalDateKey = fnd.FactorSet_AnnDateKey
			AND	v.NHValue_MBtuLb <> 0.0

		INNER JOIN [inter].PricingStreamCountry						p
			ON	/*p.FactorSetId = fnd.FactorSetId
			AND	*/p.PricingMethodId = fnd.PricingMethodId
			AND	p.CountryId = fnd.CountryId
			AND	p.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	p.CurrencyRpt = fnd.CurrencyRpt
			AND	p.StreamId = [h].AccountId
			AND	p._MatlBal_Amount_Cur <> 0.0

		WHERE fnd.StreamId = 'PPFC';

		INSERT INTO @PPFCLiquidAggregate(FactorSetId, ScenarioId, Refnum, CountryId, AnnDateKey, CurrencyRpt, AccountId, Quantity_kMT)
		SELECT
			o.FactorSetId,
			o.ScenarioId,
			o.Refnum,
			o.CountryId,
			o.AnnDateKey, 
			o.CurrencyRpt,
			'PPFCLiquid',
			SUM(o.Quantity_kMT)		[Quantity_kMT]
		FROM @PPFCLiquid	o
		GROUP BY
			o.FactorSetId,
			o.ScenarioId,
			o.Refnum,
			o.CountryId,
			o.AnnDateKey, 
			o.CurrencyRpt;

		INSERT INTO @PPFCQuantityComp(FactorSetId, Refnum, CalDateKey, StreamId, Component_kMT)
		SELECT
			c.FactorSetId,
			c.Refnum,
			c.CalDateKey,
			c.StreamId,
			SUM(c.Component_kMT)
		FROM calc.CompositionStream		c
		LEFT OUTER JOIN @PPFCLiquidAggregate a
			ON	a.FactorSetId = c.FactorSetId
			AND	a.Refnum = c.Refnum
		WHERE	c.StreamId = 'PPFC'
			AND c.ComponentId IN ('H2', 'CH4', 'C2H4', 'Inerts')
			AND c.Component_kMT <> 0.0
			AND	c.Refnum IN (SELECT tsq.Refnum FROM @fpl					 tsq)
		GROUP BY
			c.FactorSetId,
			c.Refnum,
			c.CalDateKey,
			c.StreamId,
			a.Quantity_kMT;

		INSERT INTO @PPFCPricingCountryAvg(FactorSetId, CalDateKey, CountryId, CurrencyRpt, StreamId, MatlBal_Amount_Cur, MatlBal_AmountAvg_Cur)
		SELECT
			pr.FactorSetId,
			pr.CalDateKey,
			pr.CountryId,
			pr.CurrencyRpt, 
			pr.StreamId,
			pr._MatlBal_Amount_Cur,
			AVG(pr._MatlBal_Amount_Cur) OVER(PARTITION BY pr.FactorSetId,
					pr.CountryId,
					pr.CurrencyRpt, 
					pr.StreamId)
		FROM [inter].PricingStreamCountry							pr
		WHERE	pr.StreamId = 'PricingAdj'
			AND	pr._MatlBal_Amount_Cur <> 0.0;

		INSERT INTO @PPFCFuelGas(FactorSetId, ScenarioId, Refnum, CountryId, AnnDateKey, CalDateKey, CurrencyRpt, AccountId,
			Quantity_kMT, LHValue_MBTU, Composition_Amount_Cur, Supersede_Cur)
		SELECT
			fnd.FactorSetId,
			fnd.ScenarioId,
			fnd.Refnum,
			fnd.CountryId,
			fnd.Plant_AnnDateKey,
			fnd.Plant_QtrDateKey,
			fnd.CurrencyRpt,
			b.AccountId,
			
			comp.Component_kMT											[Quantity_kMT],
			b.LHValue_MBTU,

			b.LHValue_MBTU / (q.Quantity_kMT - ISNULL(o.Quantity_kMT, 0.0))
				* pm.Amount_Cur
				* pr._MatlBal_Amount_Pcnt / 1000.0						[Composition_Amount_Cur],

			fnd.Supersede_Amount_Cur

		FROM @Foundation											fnd

		INNER JOIN fact.EnergyLHValue								b
			ON	b.Refnum = fnd.Refnum
			AND	b.CalDateKey = fnd.Plant_AnnDateKey
			AND b.AccountId = 'PPFCFuelGas'
			AND	b.LHValue_MBTU <> 0.0

		INNER JOIN @PPFCQuantityComp								[comp]
			ON	[comp].FactorSetId = fnd.FactorSetId
			AND	[comp].Refnum = fnd.Refnum
			AND	[comp].CalDateKey = fnd.Plant_QtrDateKey
			AND	[comp].StreamId = fnd.StreamId

		INNER JOIN @PPFCQuantity									q
			ON	q.Refnum = fnd.Refnum
			AND	q.AccountId = fnd.StreamId

		LEFT OUTER JOIN @PPFCLiquidAggregate						o
			ON	o.FactorSetId = fnd.FactorSetId
			AND	o.ScenarioId = fnd.ScenarioId
			AND	o.Refnum = fnd.Refnum
			AND	o.CountryId = fnd.CountryId
			AND	o.AnnDateKey = fnd.Plant_AnnDateKey
			AND	o.CurrencyRpt = fnd.CurrencyRpt		

		INNER JOIN ante.PricingMultiplierStreamCountry				pm
			ON	/*pm.FactorSetId = fnd.FactorSetId
			AND	*/pm.CalDateKey = fnd.Plant_AnnDateKey	--	fnd.FactorSet_AnnDateKey
			AND	pm.CountryId = fnd.CountryId
			AND	pm.CurrencyRpt = fnd.CurrencyRpt
			AND pm.StreamId = 'PricingAdj'
			AND pm.Amount_Cur <> 0.0

		INNER JOIN @PPFCPricingCountryAvg							pr
			ON	/*pr.FactorSetId = fnd.FactorSetId
			AND	*/pr.CalDateKey = fnd.Plant_QtrDateKey	--	fnd.FactorSet_QtrDateKey
			AND	pr.CountryId = fnd.CountryId
			AND	pr.CurrencyRpt = fnd.CurrencyRpt
			AND pr.StreamId = 'PricingAdj'

		WHERE	fnd.StreamId = 'PPFC';

		INSERT INTO [inter].[PricingPPFCFuelGas](FactorSetId, ScenarioId, Refnum, CountryId, AnnDateKey, CalDateKey, CurrencyRpt, AccountId,
			Quantity_kMT, LHValue_MBTU, Composition_Amount_Cur, Supersede_Cur)
		SELECT FactorSetId, ScenarioId, Refnum, CountryId, AnnDateKey, CalDateKey, CurrencyRpt, AccountId,
			Quantity_kMT, LHValue_MBTU, Composition_Amount_Cur, Supersede_Cur
		FROM @PPFCFuelGas;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PPFCLiquid)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription
			, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Composition_Amount_Cur, Supersede_Amount_Cur)
		SELECT
			t.FactorSetId,
			t.ScenarioId,
			t.Refnum,
			t.CalDateKey,
			t.CurrencyRpt,
			t.AccountId,
			t.AccountId,

			t.Quantity_kMT,
			coeff.Coefficient,
			lg.Amount_Cur,

			t.Composition_Amount_Cur,
			t.Supersede_Cur
		FROM @PPFCLiquid											t

		LEFT OUTER JOIN	ante.PricingDiscountNonStdFeed				coeff
			ON	coeff.Refnum = t.Refnum
			AND coeff.CalDateKey = t.AnnDateKey
			AND	coeff.StreamId = t.AccountId

		LEFT OUTER JOIN ante.PricingAdjDutiesStreamCountryAgg		lg
			ON	lg.FactorSetId = t.FactorSetId
			AND	lg.CountryId = t.CountryId
			AND	lg.CalDateKey = t.AnnDateKey
			AND	lg.CurrencyRpt = t.CurrencyRpt
			AND	lg.StreamId = t.AccountId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PPFCFuelGas)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription 
			, Quantity_kMT, Adj_NonStdStream_Coeff, Adj_Logistics_Cur, Composition_Amount_Cur, Supersede_Amount_Cur)
		SELECT
			t.FactorSetId,
			t.ScenarioId,
			t.Refnum,
			t.CalDateKey,
			t.CurrencyRpt,
			t.AccountId,
			t.AccountId,

			t.Quantity_kMT,
			coeff.Coefficient,
			lg.Amount_Cur,

			t.Composition_Amount_Cur,
			t.Supersede_Cur
		FROM @PPFCFuelGas											t

		LEFT OUTER JOIN	ante.PricingDiscountNonStdFeed				coeff
			ON	coeff.Refnum = t.Refnum
			AND coeff.CalDateKey = t.AnnDateKey
			AND	coeff.StreamId = t.AccountId

		LEFT OUTER JOIN ante.PricingAdjDutiesStreamCountryAgg		lg
			ON	lg.FactorSetId = t.FactorSetId
			AND	lg.CountryId = t.CountryId
			AND	lg.CalDateKey = t.AnnDateKey
			AND	lg.CurrencyRpt = t.CurrencyRpt
			AND	lg.StreamId = t.AccountId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantStream (PPFCOther)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantStream](FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription,
			Quantity_kMT, Supersede_Amount_Cur)
		SELECT
			f.FactorSetId,
			f.ScenarioId,
			f.Refnum,
			f.Plant_QtrDateKey,
			f.CurrencyRpt,
			'PPFCOther',
			'PPFCOther',
			q.Quantity_kMT - COALESCE(g.Quantity_kMT, 0.0) - COALESCE(SUM(l.Quantity_kMT), 0.0),
			0.0
		FROM @Foundation				f
		INNER JOIN fact.Quantity		q
			ON	q.Refnum		= f.Refnum
			AND	q.CalDateKey	= f.Plant_QtrDateKey
			AND	q.StreamId		= f.StreamId
		LEFT OUTER JOIN	[inter].[PricingPPFCFuelGas]	g
			ON	g.FactorSetId	= f.FactorSetId
			AND	g.ScenarioId	= f.ScenarioId
			AND	g.Refnum		= f.Refnum
			AND	g.CountryId		= f.CountryId
			AND	g.CalDateKey	= f.Plant_QtrDateKey
			AND	g.CurrencyRpt	= f.CurrencyRpt
			AND	g.AccountId		= 'PPFCFuelGas'
		LEFT OUTER JOIN	@PPFCLiquid		l
			ON	l.FactorSetId	= f.FactorSetId
			AND	l.ScenarioId	= f.ScenarioId
			AND	l.Refnum		= f.Refnum
			AND	l.CountryId		= f.CountryId
			AND	l.CalDateKey	= f.Plant_QtrDateKey
			AND	l.CurrencyRpt	= f.CurrencyRpt	
		WHERE	f.StreamId		= 'PPFC'
		GROUP BY
			f.FactorSetId,
			f.ScenarioId,
			f.Refnum,
			f.Plant_QtrDateKey,
			f.CurrencyRpt,
			q.Quantity_kMT,
			g.Quantity_kMT;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantAccount (PPFCLiquid)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantAccount](FactorSetId, Refnum, CalDateKey, CurrencyRpt, AccountId, Amount_Cur)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.AnnDateKey,
			t.CurrencyRpt,
			t.AccountId,
			SUM(ISNULL(t._Supersede_Amount_Cur, t._MatlBal_Amount_Cur))	[Amount_Cur]
		FROM @PPFCLiquid t
		GROUP BY
			t.FactorSetId,
			t.Refnum,
			t.AnnDateKey,
			t.CurrencyRpt,
			t.AccountId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.PricingPlantAccount (FuelGas)';
		PRINT @ProcedureDesc;

		INSERT INTO calc.[PricingPlantAccount](FactorSetId, Refnum, CalDateKey, CurrencyRpt, AccountId, Amount_Cur)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.AnnDateKey,
			t.CurrencyRpt,
			t.AccountId,
			SUM(ISNULL(t._Supersede_Amount_Cur, t._MatlBal_Amount_Cur))	[Amount_Cur]
		FROM @PPFCFuelGas t
		GROUP BY
			t.FactorSetId,
			t.Refnum,
			t.AnnDateKey,
			t.CurrencyRpt,
			t.AccountId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;