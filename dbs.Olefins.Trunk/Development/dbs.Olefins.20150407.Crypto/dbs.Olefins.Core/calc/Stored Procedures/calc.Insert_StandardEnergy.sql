﻿CREATE PROCEDURE calc.Insert_StandardEnergy
(
	@Refnum		VARCHAR (18),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	--2013	
	BEGIN TRY

		SET NOCOUNT ON;

		-- Pyrolysis HVC

		--SELECT
		--	fpl.[FactorSetId],
		--	fpl.[Refnum],
		--	fpl.[Plant_AnnDateKey],
		--	SUM(c.[Component_kMT]),
		--	r.[Component_kMT],
		--	SUM(c.[Component_kMT]) - COALESCE(r.[Component_kMT], 0.0)	[PyrolysisProduct_Dur_kMT]
		--FROM [calc].[FoundationPlantList]					fpl
		--INNER JOIN [calc].[CompositionStream]				c
		--	ON	c.[FactorSetId]		= fpl.[FactorSetId]
		--	AND	c.[Refnum]			= fpl.[Refnum]
		--	AND	c.[CalDateKey]		= fpl.[Plant_QtrDateKey]
		--	AND	c.[SimModelId]		= 'Plant'
		--	AND	c.[OpCondId]		= 'OSOP'
		--INNER JOIN [dim].[Stream_Bridge]					s
		--	ON	s.[FactorSetId]		= c.[FactorSetId]
		--	AND	s.[DescendantId]	= c.[StreamId]
		--	AND	s.[StreamId]		IN ('ProdHVC', 'Methane', 'PPFC')
		--INNER JOIN [dim].[Component_Bridge]					k
		--	ON	k.[FactorSetId]		= c.[FactorSetId]
		--	AND	k.[DescendantId]	= c.[ComponentId]
		--	AND	k.[ComponentId]		= 'ProdHVC'
		--LEFT OUTER JOIN [inter].[YIRSupplementalAggregate]	r
		--	ON	r.[FactorSetId]		= fpl.[FactorSetId]
		--	AND	r.[Refnum]			= fpl.[Refnum]
		--	AND	r.[CalDateKey]		= fpl.[Plant_AnnDateKey]
		--	AND	r.ComponentId		= 'ProdHVC'
		--WHERE fpl.[FactorSet_AnnDateKey] > 20130000
		--GROUP BY
		--	fpl.[FactorSetId],
		--	fpl.[Refnum],
		--	fpl.[Plant_AnnDateKey],
		--	r.[Component_kMT];

		-- Supplemental

		--SELECT
		--	fpl.[FactorSetId],
		--	fpl.[Refnum],
		--	fpl.[Plant_AnnDateKey],
		--	'Plant',
		--	q.[StreamId],
		--	q.[StreamDescription],
		--	SUM(q.Quantity_kMT * e.[EnergySeparation_BtuLb] * 2.2046)	[StandardEnergy]
		--FROM [calc].[FoundationPlantList]		fpl
		--INNER JOIN [dim].[Stream_Bridge]		b
		--	ON	b.[FactorSetId]	= fpl.[FactorSetId]
		--	AND	b.[StreamId]	= 'SuppTot'
		--INNER JOIN [ante].[MapStreamComponent]	m
		--	ON	m.[FactorSetId]	= fpl.[FactorSetId]
		--	AND	m.[StreamId]	= b.[DescendantId]
		--INNER JOIN [fact].[Quantity]			q
		--	ON	q.[Refnum]		= fpl.[Refnum]
		--	AND	q.[CalDateKey]	= fpl.[Plant_QtrDateKey]
		--	AND	q.[StreamId]	= b.[DescendantId]
		--INNER JOIN [ante].[EnergySeparation]	e
		--	ON	e.[FactorSetId]	= fpl.[FactorSetId]
		--	AND	e.[ComponentId]	= m.[ComponentId]
		--WHERE fpl.[FactorSet_AnnDateKey] > 20130000
		--GROUP BY
		--	fpl.[FactorSetId],
		--	fpl.[Refnum],
		--	fpl.[Plant_AnnDateKey],
		--	b.[StreamId],
		--	q.[StreamId],
		--	q.[StreamDescription];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;