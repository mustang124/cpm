﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_FreshFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		DECLARE @sgHGO	REAL = 0.87;
		DECLARE @sgDie	REAL = 0.83;
		DECLARE	@sgHvN	REAL = 0.75;
		DECLARE	@sgFrN	REAL = 0.71;

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			s.SimModelId,
			q.StreamId,
			q.StreamDescription,
			s.YieldPlantSeverity * k.Value * q.Tot_kMT  / 100.0
		FROM @fpl													tsq
		INNER JOIN fact.QuantityPivot								q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN dim.Stream_Bridge								b
			ON	b.FactorSetId = tsq.FactorSetId
			AND	b.DescendantId = q.StreamId
			AND	 b.StreamId = 'FreshPyroFeed'
		INNER JOIN sim.[YieldCompositionPlantSeverity]				s
			ON	s.Refnum = tsq.Refnum
			AND s.FactorSetId = tsq.FactorSetId
		INNER JOIN fact.FeedStockCrackingParameters					a
			ON	a.Refnum = tsq.Refnum
			AND	a.CalDateKey = tsq.Plant_QtrDateKey
			AND	a.StreamId = q.StreamId
			AND	a.StreamDescription = q.StreamDescription
		INNER JOIN ante.StandardEnergyCoefficients					k
			ON	k.StandardEnergyId =
				CASE WHEN q.StreamId = 'FeedLiqOther' THEN
					CASE
						WHEN a.Density_SG >= @sgHGO THEN 'GasOilHv'
						WHEN a.Density_SG >= @sgDie AND a.Density_SG < @sgHGO THEN 'Diesel'
						WHEN a.Density_SG >= @sgHvN AND a.Density_SG < @sgDie THEN 'NaphthaHv'
						WHEN a.Density_SG >= @sgFrN AND a.Density_SG < @sgHvN THEN 'NaphthaFr'
						ELSE 'NaphthaLt'
					END
				ELSE
					q.StreamId
				END
			AND k.FactorSetId = tsq.FactorSetId
		WHERE	s.YieldPlantSeverity > 0.0
			AND	k.Value > 0.0
			AND	q.Tot_kMT > 0.0
			AND	tsq.FactorSet_AnnDateKey	<= 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;