﻿CREATE PROCEDURE [calc].[Insert_StandardEnergyRedEthylene]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		-------------------------------------------------------------------------------
		--	H346: Reduction: Ethylene

		INSERT INTO [calc].[StandardEnergyRedEthylene]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [Quantity_kMT], [StandardEnergy_MBtuDay])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'Red' + sqa.[StreamId],
			ABS(sqa.[Quantity_kMT]),
			ABS(sqa.[Quantity_kMT]) * 11.0 * 30.0 / 365.0		[StandardEnergy_MBtuDay]
		FROM @fpl											fpl
		INNER JOIN [fact].[StreamQuantityAggregate]			sqa	WITH(NOEXPAND)
			ON	sqa.[FactorSetId]	= fpl.[FactorSetId]
			AND	sqa.[Refnum]		= fpl.[Refnum]
			AND	sqa.[StreamId]		= 'EthyleneCG'
		WHERE	fpl.[CalQtr]			= 4
			AND	fpl.[FactorSet_AnnDateKey] > 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;