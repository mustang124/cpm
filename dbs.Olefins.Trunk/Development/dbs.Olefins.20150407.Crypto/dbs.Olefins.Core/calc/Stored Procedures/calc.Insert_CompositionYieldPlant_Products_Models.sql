﻿CREATE PROCEDURE [calc].[Insert_CompositionYieldPlant_Products_Models]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @CalcProduct TABLE (
			FactorSetId				VARCHAR(12)			NOT	NULL,
			Refnum					VARCHAR(25)			NOT NULL,
			CalDateKey				INT					NOT	NULL,
			SimModelId				VARCHAR (12)		NOT NULL,
			OpCondId				VARCHAR (12)		NOT NULL,
			RecycleId				INT					NOT NULL,
			ComponentId				VARCHAR(42)			NOT	NULL,

			Component_kMT			REAL				NOT	NULL, --	CHECK(Component_kMT >= 0.0),
			Component_WtPcnt		REAL					NULL,
			Component_Extrap_kMT	REAL					NULL,

			PRIMARY KEY CLUSTERED (FactorSetId ASC, Refnum ASC, CalDateKey ASC, SimModelId ASC, OpCondId ASC, RecycleId ASC, ComponentId ASC)
			);

		INSERT INTO @CalcProduct(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId, Component_kMT, Component_Extrap_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			y.SimModelId,
			l.OpCondId,
			y.RecycleId,
			p.ComponentId,
			CASE WHEN p.ComponentId IN ('Inerts', 'Loss') THEN 0.0 ELSE p.Component_kMT END,
			p.Component_kMT
		FROM @fpl												tsq
		INNER JOIN sim.[YieldCompositionPlantSeverity]			y
			ON	y.FactorSetId = tsq.FactorSetId
			AND	y.Refnum = tsq.Refnum
			AND	y.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN (
			SELECT
				f.FactorSetId,
				f.Refnum,
				f.CalDateKey,
				f.FeedInerts_kMT	[Inerts],
				f.Losses_kMT		[Loss]
			FROM calc.DivisorsFeed f
			WHERE f.StreamId = 'FreshPyroFeed'
			) u
			UNPIVOT(
			Component_kMT FOR ComponentId IN (
				[Inerts], [Loss])
			)													p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
		--	Task 141: Implement Tri-Tables in calculations
		--CROSS JOIN dim.SimOpCondLu			l
		CROSS JOIN dim.SimOpCond_LookUp			l
		WHERE l.OpCondId <> 'PlantFeed';

		INSERT INTO @CalcProduct(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId, Component_kMT, Component_WtPcnt, Component_Extrap_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey		[CalDateKey],
			c.SimModelId,
			c.OpCondId,
			c.RecycleId,
			c.ComponentId,

			CASE WHEN c.ComponentId IN ('Inerts', 'Loss') THEN 0.0 ELSE c.Component_kMT		END	[Component_kMT],
			CASE WHEN c.ComponentId IN ('Inerts', 'Loss') THEN 0.0 ELSE c.Component_WtPcnt	END	[Component_WtPcnt],

			(CASE c.ComponentId
				--WHEN 'Inerts'	THEN 0.0
				--WHEN 'Loss'		THEN 0.0
				WHEN 'C4H8'		THEN c.Component_WtPcnt / 2.0
				ELSE c.Component_WtPcnt
				END
				+ CASE WHEN c.ComponentId = 'C4H10' THEN ISNULL([C4H8].Component_WtPcnt / 2.0, 0.0) ELSE 0.0 END)
				
				* (f.FeedAnalysisWithLosses_kMT + ISNULL(qra.Recycled_kMT, 0.0)) / 100.0		[Component_Extrap_kMT]
			
		FROM	@fpl											tsq

		INNER JOIN sim.[YieldCompositionPlant]					c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_AnnDateKey

		LEFT OUTER JOIN sim.[YieldCompositionPlant]				[C4H8]
			ON	[C4H8].FactorSetId = c.FactorSetId
			AND	[C4H8].Refnum = c.Refnum
			AND	[C4H8].CalDateKey = c.CalDateKey
			AND [C4H8].SimModelId = c.SimModelId
			AND	[C4H8].OpCondId = c.OpCondId
			AND	[C4H8].RecycleId = c.RecycleId
			AND [C4H8].ComponentId = 'C4H8'
			AND c.ComponentId = 'C4H10'

		INNER JOIN calc.DivisorsFeed							f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_AnnDateKey
			AND f.StreamId = 'FreshPyroFeed'
		
		LEFT OUTER JOIN calc.QuantityRecycledAggregate			qra
			ON	qra.FactorSetId = tsq.FactorSetId
			AND	qra.Refnum = tsq.Refnum
			AND	qra.RecycleId = c.RecycleId
			AND	qra.CalDateKey = tsq.Plant_AnnDateKey

		WHERE	tsq.CalQtr = 4;

		UPDATE @CalcProduct
		SET Component_WtPcnt = t.[Calculated_WtPcnt]
		FROM  (
			SELECT
				c.FactorSetId,
				c.Refnum,
				c.CalDateKey,
				c.SimModelId,
				c.OpCondId,
				c.RecycleId,
				c.ComponentId,
				c.Component_kMT,
				c.Component_kMT / SUM(c.Component_kMT) OVER(PARTITION BY
					c.FactorSetId,
					c.Refnum,
					c.CalDateKey,
					c.SimModelId,
					c.OpCondId,
					c.RecycleId) * 100.0	[Calculated_WtPcnt]
			FROM @CalcProduct c
			) t
			INNER JOIN @CalcProduct c
				ON	c.FactorSetId = t.FactorSetId
				AND	c.Refnum = t.Refnum
				AND	c.CalDateKey = t.CalDateKey
				AND	c.SimModelId = t.SimModelId
				AND	c.OpCondId = t.OpCondId
				AND	c.RecycleId = t.RecycleId
				AND	c.ComponentId = t.ComponentId;

		INSERT INTO calc.[CompositionYieldPlant](FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId,
			Component_kMT, Component_WtPcnt,
			Component_Extrap_kMT, Component_Extrap_WtPcnt,
			Component_Supp_kMT)
		SELECT
			p.FactorSetId,
			p.Refnum,
			p.CalDateKey,
			p.SimModelId,
			p.OpCondId,
			p.RecycleId,
			p.ComponentId,

			p.Component_kMT,
			p.Component_WtPcnt,
			p.Component_Extrap_kMT,

			p.Component_Extrap_kMT /
			SUM(p.Component_Extrap_kMT) OVER(PARTITION BY
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey,
				p.SimModelId,
				p.OpCondId,
				p.RecycleId
				)	* 100.0						[Component_Extrap_WtPcnt],

			s.Component_kMT

		FROM @CalcProduct						p
		LEFT OUTER JOIN inter.YIRSupplemental	s
			ON	s.FactorSetId = p.FactorSetId
			AND	s.Refnum = p.Refnum
			AND	s.CalDateKey = p.CalDateKey
			AND	s.ComponentId = p.ComponentId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;