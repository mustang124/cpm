﻿CREATE VIEW [calc].[MeiAggregate]
WITH SCHEMABINDING
AS
--	Task 141: Implement Tri-Tables in calculations
--SELECT
--	y.FactorSetId,
--	t.Refnum,
--	t.CalDateKey,
--	d.MeiId,
--	t.CurrencyRpt,
--	SUM(CASE d.Operator
--		WHEN '+' THEN + o.Indicator_Index
--		WHEN '-' THEN - o.Indicator_Index
--		END)							[Indicator_Index],
--	SUM(CASE d.Operator
--		WHEN '+' THEN + o.Indicator_PcntRv
--		WHEN '-' THEN - o.Indicator_PcntRv
--		END)							[Indicator_PcntRv]
--FROM fact.TSort										t
--INNER JOIN ante.FactorSetsInStudyYear				y
--		ON	y._StudyYear = t.StudyYear
--CROSS JOIN dim.MeiLuDescendants(NULL, 1, ',')	d
--LEFT OUTER JOIN calc.Mei							o
--	ON	o.FactorSetId = y.FactorSetId
--	AND	o.Refnum = t.Refnum
--	AND	o.CurrencyRpt = t.CurrencyRpt
--	AND	o.CalDateKey = t.CalDateKey
--	AND	o.MeiId = d.DescendantId
--GROUP BY
--	y.FactorSetId,
--	t.Refnum,
--	t.CalDateKey,
--	d.MeiId,
--	t.CurrencyRpt;
SELECT
	m.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	b.MeiId,
	m.CurrencyRpt,
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + m.Indicator_Index
		WHEN '-' THEN - m.Indicator_Index
		END)							[Indicator_Index],
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + m.Indicator_PcntRv
		WHEN '-' THEN - m.Indicator_PcntRv
		END)							[Indicator_PcntRv]
FROM calc.Mei					m
INNER JOIN dim.Mei_Bridge		b
	ON	b.FactorSetId = m.FactorSetId
	AND	b.DescendantId = m.MeiId
GROUP BY
	m.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	b.MeiId,
	m.CurrencyRpt;