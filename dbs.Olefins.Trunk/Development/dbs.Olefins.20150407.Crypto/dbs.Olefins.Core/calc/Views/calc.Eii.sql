﻿CREATE VIEW calc.Eii
WITH SCHEMABINDING
AS
SELECT
	s.FactorSetId,
	s.Refnum,
	s.CalDateKey,
	s.SimModelId,
	e.LHValue_MBTU,
	s.StandardEnergy,
	e.LHValue_MBTU / s.StandardEnergy * 100.0	[Eii]
FROM fact.EnergyLHValueAggregate e
INNER JOIN calc.StandardEnergyAggregate s
	ON	s.FactorSetId = e.FactorSetId
	AND	s.Refnum = e.Refnum
	AND	s.CalDateKey = e.CalDateKey
WHERE	e.AccountId = 'TotRefExp'
	AND	s.StandardEnergyId = 'StandardEnergyTot'
	AND	s.StandardEnergy <> 0.0;