﻿CREATE VIEW [calc].[ReliabilityOpLossAvailabilityAggregate]
WITH SCHEMABINDING
AS
SELECT
	c.[FactorSetId],
	c.[Refnum],
	c.[CalDateKey],
	b.[OppLossId],

	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[DownTimeLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[DownTimeLoss_MT], 0.0)
		ELSE 0.0
		END)												[DownTimeLoss_MT],

	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[SlowDownLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[SlowDownLoss_MT], 0.0)
		ELSE 0.0
		END)												[SlowDownLoss_MT],

	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[TotLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[TotLoss_MT], 0.0)
		ELSE 0.0
		END)												[TotLoss_MT],

	(1.0 - (SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[DownTimeLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[DownTimeLoss_MT], 0.0)
		ELSE 0.0
		END) / 1000.0 / c.[_StreamAvg_kMT]))	* 100.0		[DtAvailability_Pcnt],

	(1.0 - (SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[SlowDownLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[SlowDownLoss_MT], 0.0)
		ELSE 0.0
		END) / 1000.0 / c.[_StreamAvg_kMT]))	* 100.0		[SdAvailability_Pcnt],

	(1.0 - (SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[TotLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[TotLoss_MT], 0.0)
		ELSE 0.0
		END) / 1000.0 / c.[_StreamAvg_kMT]))	* 100.0		[TotAvailability_Pcnt],

	(SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[DownTimeLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[DownTimeLoss_MT], 0.0)
		ELSE 0.0
		END) / 1000.0 / c.[_StreamAvg_kMT])		* 100.0		[DtReliability_Ind],

	(SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[SlowDownLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[SlowDownLoss_MT], 0.0)
		ELSE 0.0
		END) / 1000.0 / c.[_StreamAvg_kMT])		* 100.0		[SdReliability_Ind],

	(SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + COALESCE(a.[TotLoss_MT], 0.0)
		WHEN '-' THEN - COALESCE(a.[TotLoss_MT], 0.0)
		ELSE 0.0
		END) / 1000.0 / c.[_StreamAvg_kMT])		* 100.0		[TotReliability_Ind]

FROM  [dim].[ReliabilityOppLoss_Availability_Bridge]		b
INNER JOIN [calc].[CapacityPlant]							c
	ON	c.[FactorSetId]		= b.[FactorSetId]
	AND	c.[StreamId]		= 'Ethylene'
LEFT OUTER JOIN [calc].[ReliabilityOpLossAvailability]		a
	ON	a.[FactorSetId]		= b.[FactorSetId]
	AND	a.[OppLossId]		= b.[DescendantId]
	AND	a.[Refnum]			= c.[Refnum]
	AND a.[CalDateKey]		= c.[CalDateKey]
GROUP BY
	c.[FactorSetId],
	c.[Refnum],
	c.[CalDateKey],
	b.[OppLossId],
	c.[_StreamAvg_kMT];