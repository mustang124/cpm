﻿CREATE VIEW [calc].[EmissionsCeiProduct]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	p.CalDateKey,
	p.EmissionsCarbon_MTCO2e,
	d.Production_kMT,
	p.EmissionsCarbon_MTCO2e / d.Production_kMT / 1000.0	[Emissions_CO2eMtHVC]
FROM calc.EmissionsCarbonPlantAggregate			p
INNER JOIN calc.DivisorsProduction				d
	ON	d.FactorSetId = p.FactorSetId
	AND	d.Refnum = p.Refnum
	AND	d.CalDateKey = p.CalDateKey
WHERE	p.EmissionsId = 'NetEmissions'
	AND	d.ComponentId = 'ProdHVC';
