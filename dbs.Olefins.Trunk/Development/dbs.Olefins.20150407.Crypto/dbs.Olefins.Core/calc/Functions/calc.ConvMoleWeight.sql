﻿CREATE FUNCTION calc.ConvMoleWeight
(
	@ComponentId		VARCHAR(42),	/* ComponentId to convert percent		*/
	@Pcnt				REAL,		/* Mole or Weight Percent [0.0, 100.0]	*/
	@ConversionTo		CHAR(1)		/* M - Mole Percent; W - Weight Percent */
)
RETURNS REAL
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	-- Constant Values
	DECLARE	@H		REAL =  1.00794;	-- http://webbook.nist.gov/cgi/cbook.cgi?ID=C12385136&Units=SI
	DECLARE @C		REAL = 12.0107;		-- http://webbook.nist.gov/cgi/cbook.cgi?ID=C7440440&Units=SI
	
	DECLARE	@H2		REAL =            2.0 * @H;
	DECLARE	@CH4	REAL =       @C + 4.0 * @H;
	DECLARE	@C2H4	REAL = 2.0 * @C + 4.0 * @H;

	-- Verify and set @Pcnt to [0, 1.0]
	IF NOT (@Pcnt >= 0.0 AND @Pcnt <= 100.0) BEGIN
		RETURN NULL;
	END 
	
	SET @Pcnt = CASE WHEN @Pcnt > 1.0 THEN @Pcnt / 100.0 ELSE @Pcnt END;
	
	DECLARE @WtPcnt		REAL;
	DECLARE @MolPcnt	REAL;
	
	IF @ConversionTo = 'M' BEGIN
		
		SET @MolPcnt =
			CASE @ComponentId
			-- Hydrogen Rich Gas
			WHEN 'H2' THEN
				-- Components are Hydrogen (H2) and Methane (CH4)
				-- The impurity is 100% CH4. (Assumption made by Bob Broadfoot 2008.02.18)
				(@Pcnt / @H2) / (@Pcnt / @H2 + (1.0 - @Pcnt) / @CH4)

			WHEN 'CH4' THEN
				-- Components are Hydrogen (H2), Methane (CH4), and Ethylene (C2H4)
				-- The impurity is 98% H2 and 2% C2H4. (Assumption made by Bob Broadfoot 2008.02.18)
				(@Pcnt / @CH4) / ((@Pcnt / @CH4) + (1 - @Pcnt) / (0.98 * @H2 + 0.02 * @C2H4))

			END;
			
		SET @Pcnt = @MolPcnt * 100.0;
		
	END
	ELSE BEGIN
	
		SET @WtPcnt =
			CASE @ComponentId
			-- Hydrogen Rich Gas
			WHEN 'H2' THEN
				-- Components are Hydrogen (H2) and Methane (CH4)
				-- The impurity is 100% CH4. (Assumption made by Bob Broadfoot 2008.02.18)
				(@Pcnt * @H2) / (@Pcnt * @H2 + (1.0 - @Pcnt) * @CH4)
				
			-- Fuel Gas Sales (Methane Rich Gas)
			WHEN 'CH4' THEN
				-- The impurity is 98% H2 and 2% C2H4. (Assumption made by Bob Broadfoot 2008.02.18)
				(@Pcnt * @CH4) / (@Pcnt * @CH4 + (1.0 - @Pcnt) * (0.98 * @H2 + 0.02 * @C2H4))
				
			END;
		
		SET @Pcnt = @WtPcnt * 100.0;
		
	END;

	IF NOT (@Pcnt >= 0.0 AND @Pcnt <= 100.0) BEGIN
		RETURN NULL;
	END 
		
	RETURN @Pcnt;
	
END;