﻿






CREATE              PROC [dbo].[spFeedStocks](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DELETE FROM dbo.FeedStocks WHERE Refnum = @Refnum
Insert into dbo.FeedStocks (Refnum
, StreamId
, Quantity1Q_kMT
, Quantity2Q_kMT
, Quantity3Q_kMT
, Quantity4Q_kMT
, Quantity_kMT
, H2
, CH4
, C2H6
, C2H4
, C3H8
, C3H6
, NBUTA
, IBUTA
, IB
, B1
, C4H6
, NC5
, IC5
, NC6
, C6ISO
, C7H16
, C8H18
, CO_CO2
, P
, I
, O
, N
, A
, S
, Total
, SulfurContent_ppm
, Density_SG
, D000
, D005
, D010
, D030
, D050
, D070
, D090
, D095
, D100
, CoilOutletPressure_Psia
, CoilOutletTemp_C
, CoilInletPressure_Psia
, CoilInletTemp_C
, RadiantWallTemp_C
, SteamHydrocarbon_Ratio
, EthyleneYield_WtPcnt
, PropyleneEthylene_Ratio
, PropyleneMethane_Ratio
, FeedConv_WtPcnt
, DistMethodID
, FurnConstruction_Year
, ResidenceTime_s
, FlowRate_KgHr
, Recycle_Bit
, PlantReportingCount
)

SELECT Refnum		= @Refnum
, StreamId			= CASE c.StreamId
						WHEN 'Ethane'		THEN 'Ethane'
						WHEN 'EPMix'		THEN 'EPMix'
						WHEN 'Propane'		THEN 'Propane'
						WHEN 'LPG'			THEN 'LPG'
						WHEN 'BUTANE'		THEN 'BUTANE'
						WHEN 'NaphthaLt'	THEN 'Naphtha'
						WHEN 'NaphthaFr'	THEN 'Naphtha'
						WHEN 'NaphthaHv'	THEN 'Naphtha'
						WHEN 'HeavyNGL'		THEN 'HeavyNGL'
						WHEN 'Condensate'	THEN 'Condensate'
						WHEN 'Raffinate'	THEN 'Raffinate'
						WHEN 'Diesel'		THEN 'GasOil'
						WHEN 'GasOilHv'		THEN 'GasOil'
						WHEN 'GasOilHt'		THEN 'GasOil'
						WHEN 'FeedLiqOther'	THEN
							CASE WHEN c.StreamDescription like '%Other Feed 1)' THEN 'OthLiqFeed1' ELSE
							CASE WHEN c.StreamDescription like '%Other Feed 2)' THEN 'OthLiqFeed2' ELSE
							CASE WHEN c.StreamDescription like '%Other Feed 3)' THEN 'OthLiqFeed3' END END END
						END
, Quantity1Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 0331 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity2Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 0630 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity3Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 0930 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity4Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 1231 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity_kMT		= SUM(pps.Quantity_kMT)
, H2 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'H2' THEN pps.Quantity_kMT END) 
, CH4 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'CH4' THEN pps.Quantity_kMT  END) 
, C2H6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C2H6' THEN pps.Quantity_kMT  END) 
, C2H4 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C2H4' THEN pps.Quantity_kMT  END) 
, C3H8 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C3H8' THEN pps.Quantity_kMT  END) 
, C3H6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C3H6' THEN pps.Quantity_kMT  END) 
, NBUTA = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'NBUTA' THEN pps.Quantity_kMT  END) 
, IBUTA = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'IBUTA' THEN pps.Quantity_kMT  END) 
, IB = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'IB' THEN pps.Quantity_kMT  END) 
, B1 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'B1' THEN pps.Quantity_kMT  END) 
, C4H6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C4H6' THEN pps.Quantity_kMT  END) 
, NC5 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'NC5' THEN pps.Quantity_kMT  END) 
, IC5 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'IC5' THEN pps.Quantity_kMT  END) 
, NC6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'NC6' THEN pps.Quantity_kMT  END) 
, C6ISO = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C6ISO' THEN pps.Quantity_kMT  END) 
, C7H16 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C7H16' THEN pps.Quantity_kMT  END) 
, C8H18 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C8H18' THEN pps.Quantity_kMT  END) 
, CO_CO2 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'CO_CO2' THEN pps.Quantity_kMT  END) 
, P = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'P' THEN pps.Quantity_kMT  END) 
, I = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'I' THEN pps.Quantity_kMT  END) 
, O = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'O' THEN pps.Quantity_kMT  END) 
, N = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'N' THEN pps.Quantity_kMT  END) 
, A = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'A' THEN pps.Quantity_kMT  END) 
, S = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'S' THEN pps.Quantity_kMT  END) 
, Total = 100.0
, SulfurContent_ppm = [$(DbGlobal)].dbo.WtAvgNZ(SulfurContent_ppm, pps.Quantity_kMT ) 
, Density_SG = [$(DbGlobal)].dbo.WtAvgNZ(cp.Density_SG, pps.Quantity_kMT ) 
, D000 = [$(DbGlobal)].dbo.WtAvgNZ(D000, pps.Quantity_kMT ) 
, D005 = [$(DbGlobal)].dbo.WtAvgNZ(D005, pps.Quantity_kMT ) 
, D010 = [$(DbGlobal)].dbo.WtAvgNZ(D010, pps.Quantity_kMT ) 
, D030 = [$(DbGlobal)].dbo.WtAvgNZ(D030, pps.Quantity_kMT ) 
, D050 = [$(DbGlobal)].dbo.WtAvgNZ(D050, pps.Quantity_kMT ) 
, D070 = [$(DbGlobal)].dbo.WtAvgNZ(D070, pps.Quantity_kMT ) 
, D090 = [$(DbGlobal)].dbo.WtAvgNZ(D090, pps.Quantity_kMT ) 
, D095 = [$(DbGlobal)].dbo.WtAvgNZ(D095, pps.Quantity_kMT ) 
, D100 = [$(DbGlobal)].dbo.WtAvgNZ(D100, pps.Quantity_kMT) 
, CoilOutletPressure_Psia	= [$(DbGlobal)].dbo.WtAvgNZ(CoilOutletPressure_Psia, pps.Quantity_kMT ) 
, CoilOutletTemp_C			= [$(DbGlobal)].dbo.WtAvgNZ(CoilOutletTemp_C, pps.Quantity_kMT ) 
, CoilInletPressure_Psia	= [$(DbGlobal)].dbo.WtAvgNZ(CoilInletPressure_Psia, pps.Quantity_kMT) 
, CoilInletTemp_C			= [$(DbGlobal)].dbo.WtAvgNZ(CoilInletTemp_C, pps.Quantity_kMT) 
, RadiantWallTemp_C			= [$(DbGlobal)].dbo.WtAvgNZ(RadiantWallTemp_C, pps.Quantity_kMT) 
, SteamHydrocarbon_Ratio	= [$(DbGlobal)].dbo.WtAvgNZ(SteamHydrocarbon_Ratio, pps.Quantity_kMT) 
, EthyleneYield_WtPcnt		= [$(DbGlobal)].dbo.WtAvgNZ(EthyleneYield_WtPcnt, pps.Quantity_kMT) 
, PropyleneEthylene_Ratio	= [$(DbGlobal)].dbo.WtAvgNZ(PropyleneEthylene_Ratio, pps.Quantity_kMT) 
, PropyleneMethane_Ratio	= [$(DbGlobal)].dbo.WtAvgNZ(PropyleneMethane_Ratio, pps.Quantity_kMT) 
, FeedConv_WtPcnt			= [$(DbGlobal)].dbo.WtAvgNZ(FeedConv_WtPcnt, pps.Quantity_kMT) 
, DistMethodID				= [$(DbGlobal)].dbo.WhatsThere(DistMethodID) 
, FurnConstruction_Year		= [$(DbGlobal)].dbo.WtAvgNZ(FurnConstruction_Year, pps.Quantity_kMT) 
, ResidenceTime_s			= [$(DbGlobal)].dbo.WtAvgNZ(ResidenceTime_s, pps.Quantity_kMT) 
, FlowRate_KgHr				= [$(DbGlobal)].dbo.WtAvgNZ(FlowRate_KgHr, pps.Quantity_kMT) 
, Recycle_Bit				= [$(DbGlobal)].dbo.WtAvg(Recycle_Bit, pps.Quantity_kMT) 
, PlantReportingCount		= COUNT(Distinct t.Refnum)
from TSort t LEFT JOIN calc.CompositionStream c ON c.Refnum=t.Refnum and c.OpCondId = 'PlantFeed' and c.FactorSetId = @FactorSetId --and StreamId <> 'FeedLiqOther'
JOIN calc.PricingPlantStream pps on pps.Refnum=c.Refnum and pps.CalDateKey=c.CalDateKey and pps.StreamId=c.StreamId and pps.StreamDescription=c.StreamDescription and pps.ScenarioId = 'Basis' and pps.FactorSetId=c.FactorSetId 
LEFT JOIN fact.FeedStockCrackingParameters cp ON cp.Refnum=t.Refnum and cp.StreamId=c.StreamId
LEFT JOIN fact.FeedStockDistillationPivot d ON d.Refnum=c.Refnum AND d.StreamId=c.StreamId
LEFT JOIN fact.FeedStockLiquidAttributes l ON l.Refnum=c.Refnum AND l.StreamId=c.StreamId and l.FactorSetId=c.FactorSetId
where t.Refnum = @Refnum
AND c.StreamId in ('Ethane', 'EPMix', 'Propane', 'LPG', 'BUTANE', 'NaphthaLt', 'NaphthaFr', 'NaphthaHv', 'HeavyNGL', 'Condensate', 'Raffinate', 'Diesel', 'GasOilHv', 'GasOilHt','FeedLiqOther')
GROUP BY		CASE c.StreamId
						WHEN 'Ethane'		THEN 'Ethane'
						WHEN 'EPMix'		THEN 'EPMix'
						WHEN 'Propane'		THEN 'Propane'
						WHEN 'LPG'			THEN 'LPG'
						WHEN 'BUTANE'		THEN 'BUTANE'
						WHEN 'NaphthaLt'	THEN 'Naphtha'
						WHEN 'NaphthaFr'	THEN 'Naphtha'
						WHEN 'NaphthaHv'	THEN 'Naphtha'
						WHEN 'HeavyNGL'		THEN 'HeavyNGL'
						WHEN 'Condensate'	THEN 'Condensate'
						WHEN 'Raffinate'	THEN 'Raffinate'
						WHEN 'Diesel'		THEN 'GasOil'
						WHEN 'GasOilHv'		THEN 'GasOil'
						WHEN 'GasOilHt'		THEN 'GasOil'
						WHEN 'FeedLiqOther'	THEN
							CASE WHEN c.StreamDescription like '%Other Feed 1)' THEN 'OthLiqFeed1' ELSE
							CASE WHEN c.StreamDescription like '%Other Feed 2)' THEN 'OthLiqFeed2' ELSE
							CASE WHEN c.StreamDescription like '%Other Feed 3)' THEN 'OthLiqFeed3' END END END
						END
HAVING SUM(pps.Quantity_kMT) > 0


Insert into dbo.FeedStocks (Refnum
, StreamId
, Quantity1Q_kMT
, Quantity2Q_kMT
, Quantity3Q_kMT
, Quantity4Q_kMT
, Quantity_kMT
, H2
, CH4
, C2H6
, C2H4
, C3H8
, C3H6
, NBUTA
, IBUTA
, IB
, B1
, C4H6
, NC5
, IC5
, NC6
, C6ISO
, C7H16
, C8H18
, CO_CO2
, P
, I
, O
, N
, A
, S
, Total
, SulfurContent_ppm
, Density_SG
, D000
, D005
, D010
, D030
, D050
, D070
, D090
, D095
, D100
, CoilOutletPressure_Psia
, CoilOutletTemp_C
, CoilInletPressure_Psia
, CoilInletTemp_C
, RadiantWallTemp_C
, SteamHydrocarbon_Ratio
, EthyleneYield_WtPcnt
, PropyleneEthylene_Ratio
, PropyleneMethane_Ratio
, FeedConv_WtPcnt
, DistMethodID
, FurnConstruction_Year
, ResidenceTime_s
, FlowRate_KgHr
, Recycle_Bit
, PlantReportingCount
)

SELECT Refnum		= @Refnum
, StreamId			= 'Ethane+EPMix'
, Quantity1Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 0331 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity2Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 0630 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity3Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 0930 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity4Q_kMT	= SUM(CASE WHEN c.CalDateKey = StudyYear*10000 + 1231 THEN pps.Quantity_kMT ELSE 0.0 END)
, Quantity_kMT		= SUM(pps.Quantity_kMT)
, H2 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'H2' THEN pps.Quantity_kMT END) 
, CH4 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'CH4' THEN pps.Quantity_kMT  END) 
, C2H6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C2H6' THEN pps.Quantity_kMT  END) 
, C2H4 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C2H4' THEN pps.Quantity_kMT  END) 
, C3H8 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C3H8' THEN pps.Quantity_kMT  END) 
, C3H6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C3H6' THEN pps.Quantity_kMT  END) 
, NBUTA = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'NBUTA' THEN pps.Quantity_kMT  END) 
, IBUTA = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'IBUTA' THEN pps.Quantity_kMT  END) 
, IB = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'IB' THEN pps.Quantity_kMT  END) 
, B1 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'B1' THEN pps.Quantity_kMT  END) 
, C4H6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C4H6' THEN pps.Quantity_kMT  END) 
, NC5 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'NC5' THEN pps.Quantity_kMT  END) 
, IC5 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'IC5' THEN pps.Quantity_kMT  END) 
, NC6 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'NC6' THEN pps.Quantity_kMT  END) 
, C6ISO = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C6ISO' THEN pps.Quantity_kMT  END) 
, C7H16 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C7H16' THEN pps.Quantity_kMT  END) 
, C8H18 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'C8H18' THEN pps.Quantity_kMT  END) 
, CO_CO2 = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'CO_CO2' THEN pps.Quantity_kMT  END) 
, P = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'P' THEN pps.Quantity_kMT  END) 
, I = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'I' THEN pps.Quantity_kMT  END) 
, O = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'O' THEN pps.Quantity_kMT  END) 
, N = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'N' THEN pps.Quantity_kMT  END) 
, A = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'A' THEN pps.Quantity_kMT  END) 
, S = [$(DbGlobal)].dbo.WtAvg(c.Component_WtPcnt, CASE WHEN c.ComponentId = 'S' THEN pps.Quantity_kMT  END) 
, Total = 100.0
, SulfurContent_ppm = [$(DbGlobal)].dbo.WtAvgNZ(SulfurContent_ppm, pps.Quantity_kMT ) 
, Density_SG = [$(DbGlobal)].dbo.WtAvgNZ(cp.Density_SG, pps.Quantity_kMT ) 
, D000 = [$(DbGlobal)].dbo.WtAvgNZ(D000, pps.Quantity_kMT ) 
, D005 = [$(DbGlobal)].dbo.WtAvgNZ(D005, pps.Quantity_kMT ) 
, D010 = [$(DbGlobal)].dbo.WtAvgNZ(D010, pps.Quantity_kMT ) 
, D030 = [$(DbGlobal)].dbo.WtAvgNZ(D030, pps.Quantity_kMT ) 
, D050 = [$(DbGlobal)].dbo.WtAvgNZ(D050, pps.Quantity_kMT ) 
, D070 = [$(DbGlobal)].dbo.WtAvgNZ(D070, pps.Quantity_kMT ) 
, D090 = [$(DbGlobal)].dbo.WtAvgNZ(D090, pps.Quantity_kMT ) 
, D095 = [$(DbGlobal)].dbo.WtAvgNZ(D095, pps.Quantity_kMT ) 
, D100 = [$(DbGlobal)].dbo.WtAvgNZ(D100, pps.Quantity_kMT) 
, CoilOutletPressure_Psia	= [$(DbGlobal)].dbo.WtAvgNZ(CoilOutletPressure_Psia, pps.Quantity_kMT ) 
, CoilOutletTemp_C			= [$(DbGlobal)].dbo.WtAvgNZ(CoilOutletTemp_C, pps.Quantity_kMT ) 
, CoilInletPressure_Psia	= [$(DbGlobal)].dbo.WtAvgNZ(CoilInletPressure_Psia, pps.Quantity_kMT) 
, CoilInletTemp_C			= [$(DbGlobal)].dbo.WtAvgNZ(CoilInletTemp_C, pps.Quantity_kMT) 
, RadiantWallTemp_C			= [$(DbGlobal)].dbo.WtAvgNZ(RadiantWallTemp_C, pps.Quantity_kMT) 
, SteamHydrocarbon_Ratio	= [$(DbGlobal)].dbo.WtAvgNZ(SteamHydrocarbon_Ratio, pps.Quantity_kMT) 
, EthyleneYield_WtPcnt		= [$(DbGlobal)].dbo.WtAvgNZ(EthyleneYield_WtPcnt, pps.Quantity_kMT) 
, PropyleneEthylene_Ratio	= [$(DbGlobal)].dbo.WtAvgNZ(PropyleneEthylene_Ratio, pps.Quantity_kMT) 
, PropyleneMethane_Ratio	= [$(DbGlobal)].dbo.WtAvgNZ(PropyleneMethane_Ratio, pps.Quantity_kMT) 
, FeedConv_WtPcnt			= [$(DbGlobal)].dbo.WtAvgNZ(FeedConv_WtPcnt, pps.Quantity_kMT) 
, DistMethodID				= [$(DbGlobal)].dbo.WhatsThere(DistMethodID) 
, FurnConstruction_Year		= [$(DbGlobal)].dbo.WtAvgNZ(FurnConstruction_Year, pps.Quantity_kMT) 
, ResidenceTime_s			= [$(DbGlobal)].dbo.WtAvgNZ(ResidenceTime_s, pps.Quantity_kMT) 
, FlowRate_KgHr				= [$(DbGlobal)].dbo.WtAvgNZ(FlowRate_KgHr, pps.Quantity_kMT) 
, Recycle_Bit				= [$(DbGlobal)].dbo.WtAvg(Recycle_Bit, pps.Quantity_kMT) 
, PlantReportingCount		= COUNT(DISTINCT t.Refnum)
from TSort t LEFT JOIN calc.CompositionStream c ON c.Refnum=t.Refnum and c.OpCondId = 'PlantFeed' and c.FactorSetId = @FactorSetId and StreamId <> 'FeedLiqOther'
JOIN calc.PricingPlantStream pps on pps.Refnum=c.Refnum and pps.CalDateKey=c.CalDateKey and pps.StreamId=c.StreamId and pps.StreamDescription=c.StreamDescription and pps.ScenarioId = 'Basis' and pps.FactorSetId=c.FactorSetId 
LEFT JOIN fact.FeedStockCrackingParameters cp ON cp.Refnum=t.Refnum and cp.StreamId=c.StreamId
LEFT JOIN fact.FeedStockDistillationPivot d ON d.Refnum=c.Refnum AND d.StreamId=c.StreamId
LEFT JOIN fact.FeedStockLiquidAttributes l ON l.Refnum=c.Refnum AND l.StreamId=c.StreamId and l.FactorSetId=c.FactorSetId
where t.Refnum = @Refnum
AND c.StreamId in ('Ethane', 'EPMix')
HAVING SUM(pps.Quantity_kMT) > 0


SET NOCOUNT OFF

















