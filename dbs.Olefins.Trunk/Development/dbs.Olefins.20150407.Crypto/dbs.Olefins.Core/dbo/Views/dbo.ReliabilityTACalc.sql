﻿




CREATE View [dbo].[ReliabilityTACalc] AS
Select s.Refnum
	, Currency			= ISNULL(s.CurrencyRpt,'USD')
	, TAPlanInt_Month	= [$(DbGlobal)].dbo.WtAvg(Interval_Mnths, CASE WHEN s.SchedId='P' THEN Capacity_Pcnt END)
	, TAPlanDT_Hrs		= [$(DbGlobal)].dbo.WtAvg(DownTime_Hrs, CASE WHEN s.SchedId='P' THEN Capacity_Pcnt END)
	, TAPlanDT_Days		= [$(DbGlobal)].dbo.WtAvg(DownTime_Hrs, CASE WHEN s.SchedId='P' THEN Capacity_Pcnt END) / 24.0
	, TAPlanLoss_Pcnt	= SUM(CASE WHEN s.SchedId='P' THEN _Ann_TurnAroundLoss_Pcnt ELSE 0 END)					-- SUM(CASE WHEN s.SchedId='P' THEN Capacity_Pcnt*DownTime_Hrs/Interval_Mnths ELSE 0 END) * 12.0 / 24.0 / 365.25
	, TAInt_Month		= [$(DbGlobal)].dbo.WtAvg(Interval_Mnths, CASE WHEN s.SchedId='A' THEN Capacity_Pcnt END)
	, TADT_Hrs			= [$(DbGlobal)].dbo.WtAvg(DownTime_Hrs, CASE WHEN s.SchedId='A' THEN Capacity_Pcnt END)
	, TADT_Days			= [$(DbGlobal)].dbo.WtAvg(DownTime_Hrs, CASE WHEN s.SchedId='A' THEN Capacity_Pcnt END) / 24.0
	, TA_LossPcnt		= SUM(CASE WHEN s.SchedId='A' THEN _Ann_TurnAroundLoss_Pcnt ELSE 0 END)
	, TACost_MUS		= SUM(CASE WHEN s.SchedId='A' THEN TurnAroundCost_MCur ELSE 0 END)
	, TAWHrs			= SUM(CASE WHEN s.SchedId='A' THEN TurnAround_ManHrs ELSE 0 END)
	FROM fact.ReliabilityTA s 
	GROUP By s.Refnum, ISNULL(s.CurrencyRpt,'USD')
		





