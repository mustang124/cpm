﻿CREATE VIEW [super].[CompositionPivot]
WITH SCHEMABINDING
AS
SELECT
	p.[Refnum],
	p.[StreamId],
	p.[ComponentId],
	p.[Component_WtPcnt]
FROM (
	SELECT
		c.[Refnum],
		c.[StreamId],
		c.[H2],
		c.[CH4],
		c.[C2H2],
		c.[C2H6],
		c.[C2H4],
		c.[C3H6],
		c.[C3H8],
		c.[BUTAD]	[C4H6],
		c.[C4S]		[C4H8],
		c.[C4H10],
		c.[BZ]		[C6H6],
		c.[PYGAS]	[PyroGasoline],
		c.[PYOIL]	[PyroFuelOil],
		c.[INERTS]
	FROM [super].[Composition] c
	WHERE c.[StreamId] IN('OthSpl', 'SuppOther')
	) u
	UNPIVOT ([Component_WtPcnt] FOR [ComponentId] IN (
		[H2],
		[CH4],
		[C2H2],
		[C2H6],
		[C2H4],
		[C3H6],
		[C3H8],
		[C4H6],
		[C4H8],
		[C4H10],
		[C6H6],
		[PyroGasoline],
		[PyroFuelOil],
		[INERTS]
		)
	) p
WHERE p.[Component_WtPcnt] > 0.0;