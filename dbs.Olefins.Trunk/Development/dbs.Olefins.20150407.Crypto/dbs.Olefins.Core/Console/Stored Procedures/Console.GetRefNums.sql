﻿CREATE PROCEDURE [Console].[GetRefNums]
	@StudyYear smallint,
	@Study varchar(3)
AS
BEGIN
	SELECT substring(Refnum,3,len(refnum)-2) as refnum, CoLoc FROM dbo.TSort
	WHERE StudyYear = @StudyYear and CompanyID <> 'Solomon' 
	ORDER BY RefNum ASC
END
