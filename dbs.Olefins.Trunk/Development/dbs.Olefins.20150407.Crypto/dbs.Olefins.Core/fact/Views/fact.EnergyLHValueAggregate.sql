﻿CREATE VIEW fact.EnergyLHValueAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	t.Refnum,
	t.CalDateKey,
	b.AccountId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(v.LHValue_MBTU, e._LHValue_MBTU)
		WHEN '-' THEN - ISNULL(v.LHValue_MBTU, e._LHValue_MBTU)
		END)							[LHValue_MBTU],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(v._LHValue_GJ, e._LHValue_GJ)
		WHEN '-' THEN - ISNULL(v._LHValue_GJ, e._LHValue_GJ)
		END)							[LHValue_GJ]
FROM dim.Account_Bridge							b
CROSS JOIN fact.TSortClient						t
INNER JOIN ante.FactorSetsInStudyYear			y
	ON	y.FactorSetId = b.FactorSetId
	AND	y._StudyYear = t._DataYear
LEFT OUTER JOIN fact.EnergyLHValue				v
	ON	v.Refnum		= t.Refnum
	AND	v.CalDateKey	= t.CalDateKey
	AND v.AccountId		= b.DescendantId
LEFT OUTER JOIN fact.EnergyElec					e
	ON	e.Refnum		= t.Refnum
	AND	e.CalDateKey	= t.CalDateKey
	AND e.AccountId		= b.DescendantId
GROUP BY 
	b.FactorSetId,
	t.Refnum,
	t.CalDateKey,
	b.AccountId
HAVING
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(v.LHValue_MBTU, e._LHValue_MBTU)
		WHEN '-' THEN - ISNULL(v.LHValue_MBTU, e._LHValue_MBTU)
		END)							IS NOT NULL
	OR SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(v._LHValue_GJ, e._LHValue_GJ)
		WHEN '-' THEN - ISNULL(v._LHValue_GJ, e._LHValue_GJ)
		END)							IS NOT NULL;