﻿CREATE TABLE [fact].[ReliabilityPyroFurnPlantLimit] (
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [OppLossId]           VARCHAR (42)       NOT NULL,
    [PlantFurnLimit_Pcnt] REAL               NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityPyroFurnPlantLimit_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnPlantLimit_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnPlantLimit_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnPlantLimit_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityPyroFurnPlantLimit] PRIMARY KEY CLUSTERED ([Refnum] ASC, [OppLossId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ReliabilityPyroFurnPlantLimit_PlantFurnLimit_Pcnt] CHECK ([PlantFurnLimit_Pcnt]>=(0.0) AND [PlantFurnLimit_Pcnt]<=(100.0)),
    CONSTRAINT [FK_ReliabilityPyroFurnPlantLimit_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityPyroFurnPlantLimit_ReliabilityOppLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityPyroFurnPlantLimit_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityPyroFurnPlantLimit_u]
	ON [fact].[ReliabilityPyroFurnPlantLimit]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityPyroFurnPlantLimit]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityPyroFurnPlantLimit].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityPyroFurnPlantLimit].CalDateKey	= INSERTED.CalDateKey;

END;