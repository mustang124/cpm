﻿CREATE TABLE [fact].[PersAbsence] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PersId]         VARCHAR (34)       NOT NULL,
    [Absence_Pcnt]   REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PersAbsence_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PersAbsence_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PersAbsence_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PersAbsence_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PersAbsence] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PersId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PersAbsence_Absence_Pcnt] CHECK ([Absence_Pcnt]>=(0.0) AND [Absence_Pcnt]<=(100.0)),
    CONSTRAINT [FK_PersAbsence_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PersAbsence_Pers_LookUp] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_PersAbsence_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_PersAbsence_u]
	ON [fact].[PersAbsence]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[PersAbsence]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[PersAbsence].Refnum		= INSERTED.Refnum
		AND [fact].[PersAbsence].PersId		= INSERTED.PersId
		AND [fact].[PersAbsence].CalDateKey	= INSERTED.CalDateKey;

END;