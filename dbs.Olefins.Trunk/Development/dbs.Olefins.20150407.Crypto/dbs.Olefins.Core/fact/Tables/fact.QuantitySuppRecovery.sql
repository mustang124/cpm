﻿CREATE TABLE [fact].[QuantitySuppRecovery] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [Recovered_WtPcnt]  REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_QuantitySuppRecovery_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_QuantitySuppRecovery_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_QuantitySuppRecovery_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_QuantitySuppRecovery_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_QuantitySuppRecovery] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_QuantitySuppRecovery_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_QuantitySuppRecovery_Recovered_WtPcnt] CHECK ([Recovered_WtPcnt]>=(0.0) AND [Recovered_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_QuantitySuppRecovery_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_QuantitySuppRecovery_Quantity] FOREIGN KEY ([Refnum], [StreamId], [StreamDescription], [CalDateKey]) REFERENCES [fact].[Quantity] ([Refnum], [StreamId], [StreamDescription], [CalDateKey]),
    CONSTRAINT [FK_QuantitySuppRecovery_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_QuantitySuppRecovery_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_QuantitySuppRecovery_u]
	ON [fact].[QuantitySuppRecovery]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[QuantitySuppRecovery]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[QuantitySuppRecovery].Refnum				= INSERTED.Refnum
		AND [fact].[QuantitySuppRecovery].StreamId				= INSERTED.StreamId
		AND [fact].[QuantitySuppRecovery].StreamDescription		= INSERTED.StreamDescription
		AND [fact].[QuantitySuppRecovery].CalDateKey			= INSERTED.CalDateKey;

END;