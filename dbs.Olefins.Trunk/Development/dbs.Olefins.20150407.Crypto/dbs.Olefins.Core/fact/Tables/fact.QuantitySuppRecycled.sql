﻿CREATE TABLE [fact].[QuantitySuppRecycled] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [StreamId]        VARCHAR (42)       NOT NULL,
    [ComponentId]     VARCHAR (42)       NOT NULL,
    [Recycled_WtPcnt] REAL               NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_QuantitySuppRecycled_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_QuantitySuppRecycled_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_QuantitySuppRecycled_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_QuantitySuppRecycled_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_QuantitySuppRecycled] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_QuantitySuppRecovery_Recycled_WtPcnt] CHECK ([Recycled_WtPcnt]>=(0.0) AND [Recycled_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_QuantitySuppRecycled_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_QuantitySuppRecycled_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_QuantitySuppRecycled_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_QuantitySuppRecycled_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_QuantitySuppRecycled_u]
	ON [fact].[QuantitySuppRecycled]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[QuantitySuppRecycled]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[QuantitySuppRecycled].Refnum		= INSERTED.Refnum
		AND [fact].[QuantitySuppRecycled].StreamId		= INSERTED.StreamId
		AND [fact].[QuantitySuppRecycled].CalDateKey	= INSERTED.CalDateKey;

END;