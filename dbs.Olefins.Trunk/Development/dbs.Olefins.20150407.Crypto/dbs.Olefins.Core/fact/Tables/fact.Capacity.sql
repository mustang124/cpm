﻿CREATE TABLE [fact].[Capacity] (
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [StreamId]             VARCHAR (42)       NOT NULL,
    [Capacity_kMT]         REAL               NULL,
    [_Capacity_MTd]        AS                 (CONVERT([real],([Capacity_kMT]/((365.0)+isnull(case when datepart(year,CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0)))%(4)=(0) AND datepart(year,CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0)))%(100)<>(0) OR datepart(year,CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0)))%(400)=(0) then (1.0) else (0.0) end,(0.0))))*(1000.0),(1))) PERSISTED,
    [Stream_MTd]           REAL               NULL,
    [Record_MTd]           REAL               NULL,
    [_ServiceFactor_Ann]   AS                 (CONVERT([real],case when [Stream_MTd]<>(0.0) then (([Capacity_kMT]/[Stream_MTd])/((365.0)+isnull(case when datepart(year,CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0)))%(4)=(0) AND datepart(year,CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0)))%(100)<>(0) OR datepart(year,CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0)))%(400)=(0) then (1.0) else (0.0) end,(0.0))))*(1000.0)  end,(1))) PERSISTED,
    [_CapacityRecord_Pcnt] AS                 (CONVERT([real],case when [Stream_MTd]<>(0.0) then ([Record_MTd]/[Stream_MTd])*(100.0)  end,(0))) PERSISTED,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_Capacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_Capacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_Capacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_Capacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Capacity] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_Capacity_Capacity_kMT] CHECK ([Capacity_kMT]>=(0.0)),
    CONSTRAINT [CR_Capacity_Record_MTd] CHECK ([Record_MTd]>=(0.0)),
    CONSTRAINT [CR_Capacity_Stream_MTd] CHECK ([Stream_MTd]>=(0.0)),
    CONSTRAINT [FK_Capacity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Capacity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Capacity_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_Capacity_u]
	ON [fact].[Capacity]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Capacity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[Capacity].Refnum		= INSERTED.Refnum
		AND [fact].[Capacity].StreamId		= INSERTED.StreamId
		AND [fact].[Capacity].CalDateKey	= INSERTED.CalDateKey;

END;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Based on 365 days, not 1 year.', @level0type = N'SCHEMA', @level0name = N'fact', @level1type = N'TABLE', @level1name = N'Capacity', @level2type = N'COLUMN', @level2name = N'Stream_MTd';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Because Stream_MTd is based on 365 days (not 1 year) this computed column uses 365.0.  Leap years are accounted.', @level0type = N'SCHEMA', @level0name = N'fact', @level1type = N'TABLE', @level1name = N'Capacity', @level2type = N'COLUMN', @level2name = N'_ServiceFactor_Ann';

