﻿CREATE TABLE [fact].[TSortClient] (
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [_DataYear]           AS                 (CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0))) PERSISTED NOT NULL,
    [PlantAssetName]      NVARCHAR (256)     NOT NULL,
    [PlantCompanyName]    NVARCHAR (256)     NOT NULL,
    [UomId]               VARCHAR (12)       NOT NULL,
    [CurrencyFcn]         VARCHAR (4)        NOT NULL,
    [CurrencyRpt]         VARCHAR (4)        NOT NULL,
    [CoordName]           NVARCHAR (40)      NULL,
    [CoordTitle]          NVARCHAR (75)      NULL,
    [POBox]               NVARCHAR (35)      NULL,
    [Street]              NVARCHAR (60)      NULL,
    [City]                NVARCHAR (40)      NULL,
    [State]               NVARCHAR (25)      NULL,
    [Country]             NVARCHAR (30)      NULL,
    [Zip]                 NVARCHAR (20)      NULL,
    [Telephone]           NVARCHAR (30)      NULL,
    [Fax]                 NVARCHAR (30)      NULL,
    [WWW]                 NVARCHAR (254)     NULL,
    [PricingContact]      NVARCHAR (40)      NULL,
    [PricingContactEmail] NVARCHAR (254)     NULL,
    [DCContact]           NVARCHAR (40)      NULL,
    [DCContactEmail]      NVARCHAR (254)     NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_TSortClient_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_TSortClient_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_TSortClient_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_TSortClient_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_TSortClient] PRIMARY KEY CLUSTERED ([Refnum] ASC),
    CONSTRAINT [CL_TSortClient_PlantCompanyName] CHECK ([PlantCompanyName]<>''),
    CONSTRAINT [CL_TSortClient_PlantName] CHECK ([PlantAssetName]<>''),
    CONSTRAINT [FK_TSortClient_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_TSortClient_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_TSortClient_Currency_LookUp_Fcn] FOREIGN KEY ([CurrencyFcn]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_TSortClient_Uom_LookUp] FOREIGN KEY ([UomId]) REFERENCES [dim].[Uom_LookUp] ([UomId])
);


GO

CREATE TRIGGER [fact].[t_TSortClient_u]
	ON [fact].[TSortClient]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[TSortClient]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[TSortClient].Refnum		= INSERTED.Refnum;

END;