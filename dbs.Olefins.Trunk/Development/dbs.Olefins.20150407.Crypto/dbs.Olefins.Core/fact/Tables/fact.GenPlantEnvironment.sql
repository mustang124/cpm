﻿CREATE TABLE [fact].[GenPlantEnvironment] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [Emissions_MTd]   REAL               NULL,
    [WasteHazMat_MTd] REAL               NULL,
    [NOx_LbMBtu]      REAL               NULL,
    [WasteWater_MTd]  REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantEnvironment_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnvironment_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnvironment_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnvironment_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantEnvironment] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantEnvironment_Emissions_MTd] CHECK ([Emissions_MTd]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnvironment_NOx_LbMBtu] CHECK ([NOx_LbMBtu]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnvironment_WasteHazMat_MTd] CHECK ([WasteHazMat_MTd]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnvironment_WasteWater_MTd] CHECK ([WasteWater_MTd]>=(0.0)),
    CONSTRAINT [FK_GenPlantEnvironment_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantEnvironment_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantEnvironment_u]
	ON [fact].[GenPlantEnvironment]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantEnvironment]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantEnvironment].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlantEnvironment].CalDateKey	= INSERTED.CalDateKey;

END;