﻿CREATE PROCEDURE [fact].[Insert_CapacityGrowth]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.CapacityGrowth(Refnum, CalDateKey, StreamId, Prod_kMT, Capacity_kMT)
		SELECT
			ISNULL(Prod.Refnum, Cap.Refnum),
			ISNULL(Prod.CalDateKey, Cap.CalDateKey),
			ISNULL(Prod.StreamId, Cap.StreamId),
			Prod.Prod_kMT,
			Cap.Capacity_kMT
		FROM(
			SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)					[Refnum],
				etl.ConvDateKey(g.[Year])										[CalDateKey],
				CASE WHEN g.[Year] <= t.StudyYear	THEN g.EthylProdn	END		[Ethylene],
				CASE WHEN g.[Year] <= t.StudyYear	THEN g.PropylProdn	END		[Propylene]
			FROM stgFact.CapGrowth g
			INNER JOIN stgFact.TSort t ON t.Refnum = g.Refnum
			WHERE t.Refnum = @sRefnum
			) t0
			UNPIVOT (
				Prod_kMT FOR StreamId IN (
						Ethylene
					, Propylene
				)
			) Prod
		FULL OUTER JOIN (
			SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)					[Refnum],
				etl.ConvDateKey(g.[Year])										[CalDateKey],
				CASE WHEN g.[Year] >= t.StudyYear	THEN
					CASE WHEN  g.[Year] = t.StudyYear THEN c.EthylCapKMTA ELSE g.EthylProdn	END
					ELSE
						g.EthylCap
					END															[Ethylene],
				CASE WHEN g.[Year] >= t.StudyYear	THEN
					CASE WHEN  g.[Year] = t.StudyYear THEN c.PropylCapKMTA ELSE g.PropylProdn	END
					END															[Propylene]
					
			FROM stgFact.TSort					t
			LEFT OUTER JOIN stgFact.CapGrowth	g
				ON g.Refnum = t.Refnum
			LEFT OUTER JOIN stgFact.Capacity	c
				ON c.Refnum = t.Refnum
			WHERE t.Refnum = @sRefnum

			UNION
				
			SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)					[Refnum],
				etl.ConvDateKey(t.StudyYear)									[CalDateKey],
				c.EthylCapKMTA													[Ethylene],
				c.PropylCapKMTA													[Propylene]
					
			FROM stgFact.TSort					t
			LEFT OUTER JOIN stgFact.Capacity	c
				ON c.Refnum = t.Refnum
			WHERE t.Refnum = @sRefnum

			) t1
			UNPIVOT (
				Capacity_kMT FOR StreamId IN (
						Ethylene
					, Propylene
				)
			) Cap
			ON	Cap.Refnum = Prod.Refnum
			AND	Cap.CalDateKey = Prod.CalDateKey
			AND	Cap.StreamId = Prod.StreamId
		WHERE ISNULL(Prod.Refnum, Cap.Refnum) = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;