﻿CREATE PROCEDURE [fact].[Delete_OSIM]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.CapacityGrowth';

			DELETE FROM fact.CapacityGrowth
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.CapacityAttributes';

			DELETE FROM fact.CapacityAttributes
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.Capacity';

			DELETE FROM fact.Capacity
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.FacilitiesNameOther'

			DELETE FROM fact.FacilitiesNameOther
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FacilitiesMisc'
		
			DELETE FROM fact.FacilitiesMisc
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FacilitiesFeedFrac'

			DELETE FROM fact.FacilitiesFeedFrac
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FacilitiesHydroTreat'

			DELETE FROM fact.FacilitiesHydroTreat
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FacilitiesPressure'

			DELETE FROM fact.FacilitiesPressure
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FacilitiesCompressors'

			DELETE FROM fact.FacilitiesCompressors
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.Facilities'

			DELETE FROM fact.Facilities
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedStockCrackingParameters';

			DELETE FROM fact.FeedStockCrackingParameters
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ROGEntryPoint';

			DELETE FROM fact.ROGEntryPoint
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedStockDistillation';

			DELETE FROM fact.FeedStockDistillation
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.CompositionQuantity';

			DELETE FROM fact.CompositionQuantity
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.QuantityValue';

			DELETE FROM fact.QuantityValue
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.QuantityLHValue';

			DELETE FROM fact.QuantityLHValue
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.QuantitySuppRecycled';

			DELETE FROM fact.QuantitySuppRecycled
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.QuantitySuppRecovery';

			DELETE FROM fact.QuantitySuppRecovery
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.Quantity';

			DELETE FROM fact.Quantity
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.OpExNameOther';

			DELETE FROM fact.OpExNameOther
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.OpEx';

			DELETE FROM fact.OpEx
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.PersAbsence';

			DELETE FROM fact.PersAbsence
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.Pers';

			DELETE FROM fact.Pers
			WHERE Refnum = @Refnum;

		END;
		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityPyroFurnPlantLimit';

			DELETE FROM fact.ReliabilityPyroFurnPlantLimit
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityPyroFurnOpEx';

			DELETE FROM fact.ReliabilityPyroFurnOpEx
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityPyroFurnDownTime';

			DELETE FROM fact.ReliabilityPyroFurnDownTime
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityPyroFurn';

			DELETE FROM fact.ReliabilityPyroFurn
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityPyroFurnFeed';

			DELETE FROM fact.ReliabilityPyroFurnFeed
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityTA';

			DELETE FROM fact.ReliabilityTA
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.Reliability';

			DELETE FROM fact.Reliability
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityCritPathNameOther';

			DELETE FROM fact.ReliabilityCritPathNameOther
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityCritPath';

			DELETE FROM fact.ReliabilityCritPath
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityOppLossNameOther';

			DELETE FROM fact.ReliabilityOppLossNameOther
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityOppLoss';

			DELETE FROM fact.ReliabilityOppLoss
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityOppLoss_Availability';

			DELETE FROM fact.ReliabilityOppLoss_Availability
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityCompEventComments';

			DELETE FROM [fact].[ReliabilityCompEventComments]
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityCompEvents';

			DELETE FROM fact.ReliabilityCompEvents
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityCompOperation';

			DELETE FROM fact.ReliabilityCompOperation
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityDTOverhaulFrequency';

			DELETE FROM  [fact].[ReliabilityDTOverhaulFrequency]
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityDTPhilosophy';
			DELETE FROM  fact.ReliabilityDTPhilosophy
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ReliabilityDTPredMaint';
			DELETE FROM  fact.ReliabilityDTPredMaint
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyCogenPcnt';

			DELETE FROM fact.EnergyCogenPcnt
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyCogenFacilitiesNameOther';

			DELETE FROM fact.EnergyCogenFacilitiesNameOther
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyCogenFacilities';

			DELETE FROM fact.EnergyCogenFacilities
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyCogen';

			DELETE FROM fact.EnergyCogen
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyElec';

			DELETE FROM fact.EnergyElec
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergySteam';

			DELETE FROM fact.EnergySteam
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyComposition';

			DELETE FROM fact.EnergyComposition
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyPrice';

			DELETE FROM fact.EnergyPrice
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.EnergyLHValue';

			DELETE FROM fact.EnergyLHValue
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantEnvironment';

			DELETE FROM fact.GenPlantEnvironment
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantEnergy';

			DELETE FROM fact.GenPlantEnergy
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantEnergyEfficiency';

			DELETE FROM fact.GenPlantEnergyEfficiency
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlant';

			DELETE FROM fact.GenPlant
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantExpansion';

			DELETE FROM fact.GenPlantExpansion
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantCapEx';

			DELETE FROM fact.GenPlantCapEx
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantFeedFlex';

			DELETE FROM fact.GenPlantFeedFlex
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantCostsRevenue';

			DELETE FROM fact.GenPlantCostsRevenue
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantCommIntegration';

			DELETE FROM fact.GenPlantCommIntegration
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantLogistics';

			DELETE FROM fact.GenPlantLogistics
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantDuties';

			DELETE FROM fact.GenPlantDuties
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.GenPlantInventory';

			DELETE FROM fact.GenPlantInventory
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.ApcControllers';

			DELETE FROM fact.ApcControllers
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ApcExistance';

			DELETE FROM fact.ApcExistance
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.ApcOnLinePcnt';

			DELETE FROM fact.ApcOnLinePcnt
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.Apc';

			DELETE FROM fact.Apc
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.Maint';

			DELETE FROM fact.Maint
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.MaintMisc';

			DELETE FROM fact.[MaintAccuracy]
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.MaintExpense';

			DELETE FROM fact.MaintExpense
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSel';

			DELETE FROM fact.FeedSel
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSelModelPenalty';

			DELETE FROM fact.FeedSelModelPenalty
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSelModelPlan';

			DELETE FROM fact.FeedSelModelPlan
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSelModels';

			DELETE FROM fact.FeedSelModels
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSelPersonnel';

			DELETE FROM fact.FeedSelPersonnel
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSelLogistics';

			DELETE FROM fact.FeedSelLogistics
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSelPractices';

			DELETE FROM fact.FeedSelPractices
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.FeedSelNonDiscretionary';

			DELETE FROM fact.FeedSelNonDiscretionary
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.PolyQuantityNameOther';

			DELETE FROM fact.PolyQuantityNameOther
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.PolyQuantity';

			DELETE FROM fact.PolyQuantity
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.PolyOpEx';

			DELETE FROM fact.PolyOpEx
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.PolyFacilities';

			DELETE FROM fact.PolyFacilities
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM cons.AssetsParents';

			DELETE FROM cons.AssetsParents
			WHERE AssetId <> Parent
				AND Parent IN (SELECT AssetId FROM fact.TSort t WHERE t.Refnum = @Refnum);

			PRINT NCHAR(9) + 'DELETE FROM cons.Assets';

			DELETE FROM cons.Assets
			WHERE	AssetIDSec IS NOT NULL
				AND AssetId NOT IN (SELECT AssetId FROM fact.PolyFacilities)
				AND AssetIdPri IN (SELECT AssetId FROM fact.TSort t WHERE t.Refnum = @Refnum);

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.MetaMisc';

			DELETE FROM fact.MetaMisc
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.MetaCapEx';

			DELETE FROM fact.MetaCapEx
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.MetaEnergy';

			DELETE FROM fact.MetaEnergy
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.MetaOpEx';

			DELETE FROM fact.MetaOpEx
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.MetaQuantity';

			DELETE FROM fact.MetaQuantity
			WHERE Refnum = @Refnum;

		END;

		BEGIN

			PRINT NCHAR(9) + 'DELETE FROM fact.ForexRate';

			DELETE FROM fact.ForexRate
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.TSortContact';

			DELETE FROM fact.TSortContact
			WHERE Refnum = @Refnum;

			PRINT NCHAR(9) + 'DELETE FROM fact.TSortClient';

			DELETE FROM fact.TSortClient
			WHERE Refnum = @Refnum;

		END;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
