﻿CREATE PROCEDURE [fact].[Insert_TSortClient]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.TSortClient(Refnum, CalDateKey,
			PlantAssetName,
			PlantCompanyName,
			UomId,
			CurrencyFcn,
			CurrencyRpt,
		
			[CoordName],
			[CoordTitle],
			[POBox],
			[Street],
			[City],
			[State],
			[Country],
			[Zip],
			[Telephone],
			[Fax],
			[WWW],
			[PricingContact],
			[PricingContactEmail],
			[DCContact],
			[DCContactEmail]
			)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum],
			etl.ConvDateKey(t.StudyYear)								[CalDateKey],
			CASE WHEN LEN(t.PlantName) > 0 THEN ISNULL(t.PlantName, ISNULL(t.Loc, ISNULL(t.PlantLoc, N''))) ELSE N'————' END,
			CASE WHEN LEN(t.CoName) > 0 THEN ISNULL(t.CoName, ISNULL(etl.ConvCompanyID(t.CoName, t.CompanyId, t.Co), N'————')) ELSE N'————' END,
			CASE ISNULL(t.UOM, 1)
				WHEN 1 THEN 'US'
				WHEN 2 THEN 'Metric'
				WHEN '' THEN 'Metric'
				END														[UOM],
			
			CASE etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)
				WHEN '2005PCH185' THEN 'RON'
				WHEN '2009PCH213' THEN 'RUB'
				WHEN '2009PCH214' THEN 'RUB'
				WHEN '2009PCH217' THEN 'RUB'
				ELSE k.CurrencyId
				END														[CurrencyFcn],
			'USD'														[CurrencyRpt],
		
			t.[CoordName],
			t.[CoordTitle],
			t.[POBox],
			t.[Street],
			t.[City],
			t.[State],
			t.[Country],
			t.[Zip],
			t.[Telephone],
			t.[Fax],
			t.[WWW],
			t.[PricingContact],
			t.[PricingContactEmail],
			t.[DCContact],
			t.[DCContactEmail]

		FROM stgFact.TSort t
		INNER JOIN cons.SubscriptionsAssets s
			ON	s.Refnum		= @Refnum
		INNER JOIN cons.Assets a
			ON	a.AssetId		= s.AssetId
		INNER JOIN ante.CountriesCurrencies k
			ON	k.CountryId		= a.CountryId
			AND k.InflationUse	= 1
			AND k.CurrencyId	NOT IN ('BRCK', 'RUR', 'ROLK')
		LEFT OUTER JOIN fact.TSortClient	tc
			ON	tc.Refnum		= @Refnum
		WHERE	t.Refnum		= @sRefnum
			AND	tc.Refnum		IS NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;