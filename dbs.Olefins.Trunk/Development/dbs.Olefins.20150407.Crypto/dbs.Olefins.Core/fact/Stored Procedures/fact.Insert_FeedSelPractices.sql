﻿CREATE PROCEDURE [fact].[Insert_FeedSelPractices]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSelPractices(Refnum, CalDateKey, StreamId
			, Mix_Bit, Blend_Bit, Fractionation_Bit, HydroTreat_Bit, Purification_Bit
			, MinConsidDensity_SG, MaxConsidDensity_SG, MinActualDensity_SG, MaxActualDensity_SG)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvStreamID(CASE s.FeedProdID WHEN 'GasOil' THEN 'LiqHeavy' ELSE s.FeedProdID END)
		
			, etl.ConvBit(s.Mix)
			, etl.ConvBit(s.Blend)
			, etl.ConvBit(s.Fractionation)
			, etl.ConvBit(s.Hydrotreat)
			, etl.ConvBit(s.Purification)
		
			, MinSGEst
			, MaxSGEst
			, MinSGAct
			, MaxSGAct
		FROM stgFact.FeedSelections s
		INNER JOIN stgFact.TSort t ON t.Refnum = s.Refnum
		WHERE	(s.Mix IS NOT NULL
			OR	s.Blend IS NOT NULL
			OR	s.Fractionation IS NOT NULL
			OR	s.Hydrotreat IS NOT NULL
			OR	s.Purification IS NOT NULL
			OR	MinSGEst IS NOT NULL
			OR	MaxSGEst IS NOT NULL
			OR	MinSGAct IS NOT NULL
			OR	MaxSGAct IS NOT NULL)
			AND	t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;