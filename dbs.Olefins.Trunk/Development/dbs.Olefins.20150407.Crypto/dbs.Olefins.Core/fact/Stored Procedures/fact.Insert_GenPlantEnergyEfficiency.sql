﻿CREATE PROCEDURE [fact].[Insert_GenPlantEnergyEfficiency]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantEnergyEfficiency(
			  Refnum
			, CalDateKey
			, PyroFurnInletTemp_C
			, PyroFurnPreheat_Pcnt
			, PyroFurnPrimaryTLE_Pcnt
			, PyroFurnTLESteam_PSIa
			, PyroFurnSecondaryTLE_Pcnt
			, [DriverGT_Bit]
			, PyroFurnFlueGasTemp_C
			, PyroFurnFlueGasO2_Pcnt
			, TLEOutletTemp_C
			, EnergyDeterioration_Pcnt
			, IBSLSteamRequirement_Pcnt)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		
			, p.InletTemp
			, p.FurnPreheatPcnt	* 100.0
			, p.FurnOutputPcnt	* 100.0
			, p.StmPress
			, p.TLE2Pcnt		* 100.0
		
			--, CASE WHEN UPPER(LEFT(p.TurbDriver, 1)) IN ('Y', 'X') THEN 1 ELSE 0 END
			--										[GT] --	dim.FacilitiesDriverLu
			, etl.ConvBit(p.TurbDriver)			[GT]	--	dim.FacilitiesDriverLu
			, p.FurnTemp
			, p.OxyContent		* 100.0
			, p.OutletTemp
			, p.Deterioration	* 100.0
			, p.SteamPcnt		* 100.0
		
		FROM stgFact.Prac p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;