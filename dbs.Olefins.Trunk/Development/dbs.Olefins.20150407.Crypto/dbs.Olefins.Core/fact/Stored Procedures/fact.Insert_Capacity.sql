﻿CREATE PROCEDURE [fact].[Insert_Capacity]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Capacity(Refnum, CalDateKey, StreamId, Capacity_kMT, Stream_MTd, Record_MTd)
		SELECT
			[Refnum]		= etl.ConvRefnum(ISNULL(a.Refnum, ISNULL(s.Refnum, r.Refnum)), ISNULL(a.StudyYear, ISNULL(s.StudyYear, r.StudyYear)), @StudyId),
			[CalDateKey]	= etl.ConvDateKey(ISNULL(a.StudyYear, ISNULL(s.StudyYear, r.StudyYear))),
			[StreamId]		= ISNULL(a.StreamId, ISNULL(s.StreamId, r.StreamId)),
			a.Capacity_KMT,
			CASE WHEN s.Stream_MTd > 0.0
				THEN s.Stream_MTd
				ELSE CASE WHEN a.StreamId = 'Ethylene' THEN a.Capacity_KMT / a.SvcFactor / 356.0 * 1000.0 END
				END														[Stream_MTd],
			r.Record_MTd
		FROM (
			SELECT
				u.Refnum,
				u.StudyYear,
				u.StreamId,
				u.Capacity_KMT,
				u.SvcFactor
			FROM(
				SELECT
					t.Refnum,
					t.StudyYear,
					c.EthylCapKMTA		[Ethylene],
					c.PropylCapKMTA		[Propylene],
					c.FurnaceCapKMTA	[FreshPyroFeed],
					c.RecycleCapKMTA	[Recycle],
					c.SvcFactor
				FROM stgFact.Capacity c
				INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
				WHERE t.[Refnum] = @sRefnum
				) p
				UNPIVOT(
				Capacity_KMT FOR StreamId IN(
					[Ethylene],
					[Propylene],
					[FreshPyroFeed],
					[Recycle]
					)
				) u
			) a
		FULL OUTER JOIN (
			SELECT
				u.Refnum,
				u.StudyYear,
				u.StreamId,
				u.Stream_MTd
			FROM(
				SELECT
					t.Refnum,
					t.StudyYear,
					c.EthylCapMTD						[Ethylene],
					c.OlefinsCapMTD - c.EthylCapMTD	[Propylene]
				FROM stgFact.Capacity c
				INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
				WHERE t.[Refnum] = @sRefnum
				) p
				UNPIVOT(
				Stream_MTd FOR StreamId IN(
					[Ethylene],
					[Propylene]
					)
				) u
			) s
			ON	s.Refnum	= a.Refnum
			AND	s.StudyYear	= a.StudyYear
			AND s.StreamId	= a.StreamId
		FULL OUTER JOIN(
			SELECT
				u.Refnum,
				u.StudyYear,
				u.StreamId,
				u.Record_MTd
			FROM(
				SELECT
					t.Refnum,
					t.StudyYear,
					c.EthylMaxCapMTD							[Ethylene],
					CASE WHEN c.OlefinsMaxCapMTD > c.EthylMaxCapMTD THEN c.OlefinsMaxCapMTD - c.EthylMaxCapMTD END		[Propylene]
				FROM stgFact.Capacity c
				INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
				WHERE t.[Refnum] = @sRefnum
				) p
				UNPIVOT(
				Record_MTd FOR StreamId IN(
					[Ethylene],
					[Propylene]
					)
				) u
			) r
			ON	r.Refnum		= ISNULL(a.Refnum, s.Refnum)
			AND	r.StudyYear		= ISNULL(a.StudyYear, s.StudyYear)
			AND r.StreamId		= ISNULL(a.StreamId, s.StreamId)


	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;