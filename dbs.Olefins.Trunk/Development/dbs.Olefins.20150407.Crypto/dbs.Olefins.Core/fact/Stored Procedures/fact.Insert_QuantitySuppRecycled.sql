﻿CREATE PROCEDURE [fact].[Insert_QuantitySuppRecycled]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.QuantitySuppRecycled(Refnum, CalDateKey, StreamId, ComponentId, Recycled_WtPcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.StreamId
			, CASE u.StreamId
				WHEN 'SuppToRecEthane'	THEN 'C2H6'
				WHEN 'SuppToRecPropane'	THEN 'C3H8'
				WHEN 'SuppToRecButane'	THEN 'C4H10'
				END														[ComponentId]
			
			, CASE RecycledPcnt WHEN 100.0 THEN 1.0 ELSE RecycledPcnt END * 100.0
																		[RecycledPcnt]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, m.EthPcntTot	[SuppToRecEthane]
				, m.ProPcntTot	[SuppToRecPropane]
				, m.ButPcntTot	[SuppToRecButane]
			FROM stgFact.Misc m
			INNER JOIN stgFact.TSort t		ON	t.Refnum = m.Refnum
			WHERE t.Refnum = @sRefnum
			) p
			UNPIVOT(
			RecycledPcnt FOR StreamId IN (
				  [SuppToRecEthane]
				, [SuppToRecPropane]
				, [SuppToRecButane]
			)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;