﻿CREATE PROCEDURE [fact].[Insert_FeedStockDistillation]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedStockDistillation(Refnum, CalDateKey, StreamId, StreamDescription, DistillationID, Temp_C)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.FeedProdID)							[StreamId]
			, u.[StreamDescription]
			, u.DistillationID
			, u.Temp_C
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			
				, q.FeedProdID
				, etl.ConvStreamDescription(q.FeedProdID, q.OthLiqFeedDESC, NULL, NULL, NULL) [StreamDescription]
			
				, q.IBP			[D000]
				, q.D5pcnt		[D005]
				, q.D10Pcnt		[D010]
				, q.D30Pcnt		[D030]
				, q.D50Pcnt		[D050]
				, q.D70Pcnt		[D070]
				, q.D90Pcnt		[D090]
				, q.D95Pcnt		[D095]
				, q.EP			[D100]
			
			FROM stgFact.FeedQuality q
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = q.Refnum
			INNER JOIN stgFact.Quantity z
				ON	z.Refnum = q.Refnum
				AND	z.FeedProdID = q.FeedProdID
				AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
			WHERE	(q.IBP		> 0.0  
				OR	q.D5pcnt	> 0.0  
				OR	q.D10Pcnt	> 0.0  
				OR	q.D30Pcnt	> 0.0  
				OR	q.D50Pcnt	> 0.0  
				OR	q.D70Pcnt	> 0.0  
				OR	q.D90Pcnt	> 0.0  
				OR	q.D95Pcnt	> 0.0  
				OR	q.EP		> 0.0)
				AND t.Refnum = @sRefnum
			) p
			UNPIVOT(
			Temp_C FOR DistillationID IN(
				  [D000]
				, [D005]
				, [D010]
				, [D030]
				, [D050]
				, [D070]
				, [D090]
				, [D095]
				, [D100]
				)
			) u
		WHERE u.Temp_C > 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;