﻿CREATE PROCEDURE [fact].[Insert_MetaEnergy]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.MetaEnergy(Refnum, CalDateKey, AccountId, LHValue_MBTU)
		SELECT
			  u.Refnum
			, CONVERT(INT, CONVERT(VARCHAR(4), u.StudyYear) + CONVERT(VARCHAR(4), u.CalDateKey))
			, 'MetaEnergy'										[AccountId]
			, u.MBTU
		FROM(
			SELECT 
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)		[Refnum]
				, t.StudyYear
				, m.Qtr1											[0331]
				, m.Qtr2											[0630]
				, m.Qtr3											[0930]
				, m.Qtr4											[1231]
			FROM stgFact.MetaEnergy m
			INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
			WHERE	t.Refnum = @sRefnum

			) p
			UNPIVOT (
			MBTU FOR CalDateKey IN (
				[0331], [0630], [0930], [1231]
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;