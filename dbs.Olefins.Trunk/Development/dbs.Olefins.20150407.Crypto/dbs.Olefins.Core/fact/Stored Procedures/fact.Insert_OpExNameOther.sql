﻿CREATE PROCEDURE [fact].[Insert_OpExNameOther]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.OpExNameOther(Refnum, CalDateKey, AccountId, CurrencyRpt, AccountName)
		SELECT 
			  u.Refnum
			, u.CalDateKey
			, etl.ConvAccountID(u.OpExId)								[AccountId]
			, 'USD'
			, u.OpExDescription
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, d.OpExId
				, ISNULL(d.[Description], d.OpExId + ' - No Client Description Provided')
																	[OpExDescription]
			FROM stgFact.OpExMiscDetail d
			INNER JOIN stgFact.TSort t
				ON t.Refnum =  d.Refnum
			WHERE d.Amount <> 0.0
			AND t.Refnum = @sRefnum
			)u;	

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;