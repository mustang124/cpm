﻿CREATE PROCEDURE [fact].[Insert_ReliabilityOppLossNameOther]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		DECLARE @rn TABLE(
			[Refnum]      VARCHAR (25)   NOT NULL,
			[CalDateKey]  INT            NOT NULL,
			[OppLossId]   VARCHAR (42)   NOT NULL,
			[StreamId]    VARCHAR (42)   NOT NULL,
			[OppLossName] NVARCHAR (256) NOT NULL,
			PRIMARY KEY CLUSTERED ([Refnum] DESC, [OppLossId] ASC, [StreamId] ASC, [CalDateKey] DESC)
		);

		INSERT INTO @rn(Refnum, CalDateKey, OppLossId, StreamId, OppLossName)
		SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId]
			, 'Ethylene'												[StreamId]
			, CASE 
				WHEN p.[Description] = '0' THEN etl.ConvReliabilityOppLossId(p.CauseID)
				WHEN LEN(p.[Description]) = 0 THEN etl.ConvReliabilityOppLossId(p.CauseID)
				ELSE p.[Description]
				END														[OppLossName]
		FROM stgFact.ProdLoss p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	p.[Description] IS NOT NULL AND (p.DTLoss <> 0.0 OR p.SDLoss <> 0.0)
			AND t.Refnum = @sRefnum;

		INSERT INTO @rn(Refnum, CalDateKey, OppLossId, StreamId, OppLossName)
		SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear - 1)							[CalDateKey]
			, etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId]
			, 'Ethylene'												[StreamId]
			, CASE 
				WHEN p.[Description] = '0' THEN etl.ConvReliabilityOppLossId(p.CauseID)
				WHEN LEN(p.[Description]) = 0 THEN etl.ConvReliabilityOppLossId(p.CauseID)
				ELSE p.[Description]
				END														[OppLossName]
		FROM stgFact.ProdLoss p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	p.[Description] IS NOT NULL AND (p.PrevDTLoss <> 0.0 OR p.PrevSDLoss <> 0.0)
			AND t.Refnum = @sRefnum;

		INSERT INTO fact.ReliabilityOppLossNameOther(Refnum, CalDateKey, OppLossId, StreamId, OppLossName)
		SELECT Refnum, CalDateKey, OppLossId, StreamId, OppLossName
		FROM @rn;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;