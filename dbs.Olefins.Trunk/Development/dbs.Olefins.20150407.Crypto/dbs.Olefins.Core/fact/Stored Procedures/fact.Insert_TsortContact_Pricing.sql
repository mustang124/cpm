﻿CREATE PROCEDURE [fact].[Insert_TsortContact_Pricing]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.TSortContact([Refnum], [ContactTypeId], [NameFull],
			[eMail])
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)		[Refnum],
			'PricingCoord',
			t.PricingContact,
			CASE WHEN t.PricingContactEmail	<> '' THEN t.PricingContactEmail ELSE NULL END
		FROM stgFact.TSort				t
		INNER JOIN fact.TSortClient		c
			ON	c.Refnum		= @Refnum
		LEFT OUTER JOIN fact.TSortContact	x
			ON	x.Refnum		= c.Refnum
			AND	x.ContactTypeId	= 'PricingCoord'
		WHERE	t.PricingContact	IS NOT NULL
			AND	t.PricingContact	<> ''
			AND	x.Refnum			IS NULL
			AND t.Refnum			= @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;