﻿CREATE FUNCTION [etl].[ConvEventCauseID]
(
	@EventCauseID	TINYINT
)
RETURNS NVARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(42) =
	CASE @EventCauseID
		WHEN 1	THEN 'CompressorFouling'
		WHEN 2	THEN 'CompressorBearings'
		WHEN 3	THEN 'CompressorSeals'
		WHEN 4	THEN 'CompressorImpeller'
		WHEN 5	THEN 'PistonSeal'
		WHEN 6	THEN 'BearingThrust'
		WHEN 7	THEN 'CasingLeak'
		WHEN 8	THEN 'OverspeedTrip'
		WHEN 9	THEN 'TurbineBlade'
		WHEN 10	THEN 'TurbineBearing'
		WHEN 11	THEN 'ValveGoverner'
		WHEN 12	THEN 'ValveTT'
		WHEN 13	THEN 'SystemSpeed'
		WHEN 14	THEN 'SystemSurge'
		WHEN 15	THEN 'TurbinePower'
		WHEN 16	THEN 'GearBoxShaft'
		WHEN 17	THEN 'GearBoxBearing'
		WHEN 18	THEN 'GearBoxGears'
		WHEN 19	THEN 'GearBoxCoupling'
		WHEN 20	THEN 'GlandSeal'
		WHEN 21	THEN 'LubeSeal'
		WHEN 22	THEN 'SteamCondenser'
		WHEN 23	THEN 'Coolers'
		WHEN 24	THEN 'TurbineDisk'
		WHEN 25	THEN 'RV'
		WHEN 26	THEN 'ProcessPiping'
		WHEN 27	THEN 'FalseTripVibration'
		WHEN 28	THEN 'FalseTripOther'
		WHEN 29	THEN 'MotorElectricFault'
		WHEN 30	THEN 'MotorElectricStarter'
		WHEN 31	THEN 'MotorElectricBearing'
		WHEN 32	THEN 'ProcessUpset'
		WHEN 33	THEN 'OtherCause'
	END

	RETURN @ResultVar;
	
END