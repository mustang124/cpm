﻿
CREATE PROCEDURE [reports].[Insert_GapReliabilityOppLoss]
(
	@GroupId	VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @MyGroup TABLE 
		(
			[GroupId]	VARCHAR(25)	NOT NULL,
			[Refnum]	VARCHAR(25)	NOT NULL
			PRIMARY KEY CLUSTERED ([Refnum] ASC)
		);

		INSERT @MyGroup ([GroupId], [Refnum])
		SELECT
			gp.[GroupId],
			gp.[Refnum]
		FROM [reports].[GroupPlants]	gp
		WHERE gp.[GroupId] = @GroupId;

		INSERT INTO [reports].[GapReliabilityOppLoss]
		(
			[GroupId],
			[DataType],

			[LostProdOpp],
			[PlantRelated],
			[TurnAround],
			[UnPlanned],
			[PRO],
			[OperError],
			[IntFeedInterrupt],
			[FFP],
			[Fouling],
			[Freezing],
			[Plugging],
			[FFPCauses],
			[FFPCause1],
			[FFPCause2],
			[FFPCause3],
			[FFPCause4],
			[FFPCauseBalance],
			[Transition],
			[ProcessOther],
			[ProcessOther1],
			[ProcessOther2],
			[ProcessOther3],
			[ProcessOther4],
			[ProcessOther5],
			[ProcessOtherBalance],
			[FurnaceProcess],
			[AcetyleneConv],
			[EQF],
			[Compressor],
			[OtherRotating],
			[FurnaceMech],
			[Corrosion],
			[ElecDist],
			[ControlSys],
			[OtherFailure],
			[Furnace],
			[UTF],
			[ExtElecFailure],
			[ExtOthUtilFailure],
			[IntElecFailure],
			[IntSteamFailure],
			[IntOthUtilFailure],
			[PlantOutages],
			[PyroFurnDT],
			[StandByDT],
			[Maint],
			[MaintDecoke],
			[MaintTLE],
			[MaintMinor],
			[MaintMajor],
			[NOC],
			[Demand],
			[ExtFeedInterrupt],
			[CapProject],
			[Strikes],
			[FeedMix],
			[Severity],
			[OtherNonOp],
			[OtherNonOp1],
			[OtherNonOp2],
			[OtherNonOp3],
			[OtherNonOp4],
			[OtherNonOp5],
			[OtherNonOpBalance],
			[PeriodDays]
		)
		SELECT
			g.[GroupId],
			d.[DataType],

			[$(DbGlobal)].[dbo].[WtAvg](d.[LostProdOpp], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[PlantRelated], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[TurnAround], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[UnPlanned], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[PRO], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OperError], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[IntFeedInterrupt], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FFP], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Fouling], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Freezing], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Plugging], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FFPCauses], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FFPCause1], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FFPCause2], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FFPCause3], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FFPCause4], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FFPCauseBalance], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Transition], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ProcessOther], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ProcessOther1], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ProcessOther2], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ProcessOther3], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ProcessOther4], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ProcessOther5], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ProcessOtherBalance], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FurnaceProcess], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[AcetyleneConv], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[EQF], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Compressor], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherRotating], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FurnaceMech], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Corrosion], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ElecDist], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ControlSys], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherFailure], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Furnace], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[UTF], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ExtElecFailure], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ExtOthUtilFailure], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[IntElecFailure], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[IntSteamFailure], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[IntOthUtilFailure], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[PlantOutages], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[PyroFurnDT], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[StandByDT], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Maint], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[MaintDecoke], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[MaintTLE], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[MaintMinor], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[MaintMajor], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[NOC], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Demand], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[ExtFeedInterrupt], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[CapProject], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Strikes], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[FeedMix], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[Severity], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherNonOp], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherNonOp1], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherNonOp2], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherNonOp3], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherNonOp4], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherNonOp5], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[OtherNonOpBalance], 1.0),
			[$(DbGlobal)].[dbo].[WtAvg](d.[PeriodDays], 1.0)
		FROM @MyGroup								g
		INNER JOIN [dbo].[GapReliabilityOppLoss]	d
			ON	d.[Refnum]	= g.[Refnum]
		GROUP BY
			g.[GroupId],
			d.[DataType];

	END TRY
	BEGIN CATCH

		SET @GroupId = 'G:' + @GroupId;
		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
