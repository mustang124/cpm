﻿






CREATE              PROC [reports].[spFeedStocks](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint

DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
Select @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.FeedStocks WHERE GroupId = @GroupId
Insert into reports.FeedStocks (GroupId
, StreamId
, Quantity1Q_kMT
, Quantity2Q_kMT
, Quantity3Q_kMT
, Quantity4Q_kMT
, Quantity_kMT
, H2
, CH4
, C2H6
, C2H4
, C3H8
, C3H6
, NBUTA
, IBUTA
, IB
, B1
, C4H6
, NC5
, IC5
, NC6
, C6ISO
, C7H16
, C8H18
, CO_CO2
, P
, I
, O
, N
, A
, S
, Total
, SulfurContent_ppm
, Density_SG
, D000
, D005
, D010
, D030
, D050
, D070
, D090
, D095
, D100
, CoilOutletPressure_Psia
, CoilOutletTemp_C
, CoilInletPressure_Psia
, CoilInletTemp_C
, RadiantWallTemp_C
, SteamHydrocarbon_Ratio
, EthyleneYield_WtPcnt
, PropyleneEthylene_Ratio
, PropyleneMethane_Ratio
, FeedConv_WtPcnt
, DistMethodID
, FurnConstruction_Year
, ResidenceTime_s
, FlowRate_KgHr
, Recycle_Bit
, PlantReportingCount
)

SELECT GroupId		= @GroupId
, StreamId			= c.StreamId
, Quantity1Q_kMT	= SUM(Quantity1Q_kMT) / @RefCount
, Quantity2Q_kMT	= SUM(Quantity2Q_kMT) / @RefCount
, Quantity3Q_kMT	= SUM(Quantity3Q_kMT) / @RefCount
, Quantity4Q_kMT	= SUM(Quantity4Q_kMT) / @RefCount
, Quantity_kMT		= SUM(Quantity_kMT) / @RefCount
, H2 = [$(DbGlobal)].dbo.WtAvg(H2, c.Quantity_kMT) 
, CH4 = [$(DbGlobal)].dbo.WtAvg(CH4, c.Quantity_kMT) 
, C2H6 = [$(DbGlobal)].dbo.WtAvg(C2H6, c.Quantity_kMT) 
, C2H4 = [$(DbGlobal)].dbo.WtAvg(C2H4, c.Quantity_kMT) 
, C3H8 = [$(DbGlobal)].dbo.WtAvg(C3H8, c.Quantity_kMT) 
, C3H6 = [$(DbGlobal)].dbo.WtAvg(C3H6, c.Quantity_kMT) 
, NBUTA = [$(DbGlobal)].dbo.WtAvg(NBUTA, c.Quantity_kMT) 
, IBUTA = [$(DbGlobal)].dbo.WtAvg(IBUTA, c.Quantity_kMT) 
, IB = [$(DbGlobal)].dbo.WtAvg(IB, c.Quantity_kMT) 
, B1 = [$(DbGlobal)].dbo.WtAvg(B1, c.Quantity_kMT) 
, C4H6 = [$(DbGlobal)].dbo.WtAvg(C4H6, c.Quantity_kMT) 
, NC5 = [$(DbGlobal)].dbo.WtAvg(NC5, c.Quantity_kMT) 
, IC5 = [$(DbGlobal)].dbo.WtAvg(IC5, c.Quantity_kMT) 
, NC6 = [$(DbGlobal)].dbo.WtAvg(NC6, c.Quantity_kMT) 
, C6ISO = [$(DbGlobal)].dbo.WtAvg(C6ISO, c.Quantity_kMT) 
, C7H16 = [$(DbGlobal)].dbo.WtAvg(C7H16, c.Quantity_kMT) 
, C8H18 = [$(DbGlobal)].dbo.WtAvg(C8H18, c.Quantity_kMT) 
, CO_CO2 = [$(DbGlobal)].dbo.WtAvg(CO_CO2, c.Quantity_kMT) 
, P = [$(DbGlobal)].dbo.WtAvg(P, c.Quantity_kMT) 
, I = [$(DbGlobal)].dbo.WtAvg(I, c.Quantity_kMT) 
, O = [$(DbGlobal)].dbo.WtAvg(O, c.Quantity_kMT) 
, N = [$(DbGlobal)].dbo.WtAvg(N, c.Quantity_kMT) 
, A = [$(DbGlobal)].dbo.WtAvg(A, c.Quantity_kMT) 
, S = [$(DbGlobal)].dbo.WtAvg(S, c.Quantity_kMT) 
, Total = [$(DbGlobal)].dbo.WtAvg(Total, c.Quantity_kMT) 
, SulfurContent_ppm = [$(DbGlobal)].dbo.WtAvgNZ(SulfurContent_ppm, c.Quantity_kMT) 
, Density_SG = [$(DbGlobal)].dbo.WtAvgNZ(Density_SG, c.Quantity_kMT) 
, D000 = [$(DbGlobal)].dbo.WtAvgNZ(D000, c.Quantity_kMT) 
, D005 = [$(DbGlobal)].dbo.WtAvgNZ(D005, c.Quantity_kMT) 
, D010 = [$(DbGlobal)].dbo.WtAvgNZ(D010, c.Quantity_kMT) 
, D030 = [$(DbGlobal)].dbo.WtAvgNZ(D030, c.Quantity_kMT) 
, D050 = [$(DbGlobal)].dbo.WtAvgNZ(D050, c.Quantity_kMT) 
, D070 = [$(DbGlobal)].dbo.WtAvgNZ(D070, c.Quantity_kMT) 
, D090 = [$(DbGlobal)].dbo.WtAvgNZ(D090, c.Quantity_kMT) 
, D095 = [$(DbGlobal)].dbo.WtAvgNZ(D095, c.Quantity_kMT) 
, D100 = [$(DbGlobal)].dbo.WtAvgNZ(D100, c.Quantity_kMT) 
, CoilOutletPressure_Psia = [$(DbGlobal)].dbo.WtAvgNZ(CoilOutletPressure_Psia, c.Quantity_kMT) 
, CoilOutletTemp_C = [$(DbGlobal)].dbo.WtAvgNZ(CoilOutletTemp_C, c.Quantity_kMT) 
, CoilInletPressure_Psia = [$(DbGlobal)].dbo.WtAvgNZ(CoilInletPressure_Psia, c.Quantity_kMT) 
, CoilInletTemp_C = [$(DbGlobal)].dbo.WtAvgNZ(CoilInletTemp_C, c.Quantity_kMT) 
, RadiantWallTemp_C = [$(DbGlobal)].dbo.WtAvgNZ(RadiantWallTemp_C, c.Quantity_kMT) 
, SteamHydrocarbon_Ratio = [$(DbGlobal)].dbo.WtAvgNZ(SteamHydrocarbon_Ratio, c.Quantity_kMT) 
, EthyleneYield_WtPcnt = [$(DbGlobal)].dbo.WtAvgNZ(EthyleneYield_WtPcnt, c.Quantity_kMT) 
, PropyleneEthylene_Ratio = [$(DbGlobal)].dbo.WtAvgNZ(PropyleneEthylene_Ratio, c.Quantity_kMT) 
, PropyleneMethane_Ratio = [$(DbGlobal)].dbo.WtAvgNZ(PropyleneMethane_Ratio, c.Quantity_kMT) 
, FeedConv_WtPcnt = [$(DbGlobal)].dbo.WtAvgNZ(FeedConv_WtPcnt, c.Quantity_kMT) 
, DistMethodID = [$(DbGlobal)].dbo.WhatsThere(DistMethodID) 
, FurnConstruction_Year = [$(DbGlobal)].dbo.WtAvgNZ(FurnConstruction_Year, c.Quantity_kMT) 
, ResidenceTime_s = [$(DbGlobal)].dbo.WtAvgNZ(ResidenceTime_s, c.Quantity_kMT) 
, FlowRate_KgHr = [$(DbGlobal)].dbo.WtAvgNZ(FlowRate_KgHr, c.Quantity_kMT) 
, Recycle_Bit = [$(DbGlobal)].dbo.WtAvg(Recycle_Bit, c.Quantity_kMT) 
, PlantReportingCount		= COUNT(Distinct r.Refnum)
from @MyGroup r JOIN dbo.FeedStocks c on r.Refnum=c.Refnum 
--CROSS APPLY(Values('Ethane'),('EPMix'),('Ethane+EPMix'),('Propane'),('LPG'),('BUTANE'),('Naphtha'),('HeavyNGL'),('Condensate'),('Raffinate'),('GasOil')) s(StreamId)
--LEFT OUTER JOIN dbo.FeedStocks c on r.Refnum=c.Refnum and 
WHERE @RefCount > 0
GROUP BY	c.StreamId




SET NOCOUNT OFF

















