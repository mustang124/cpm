﻿CREATE TABLE [dim].[Edc_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [EdcId]          VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Edc_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Edc_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Edc_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Edc_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Edc_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Edc_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EdcId] ASC),
    CONSTRAINT [CR_Edc_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Edc_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Edc_Parent_LookUp_Edc] FOREIGN KEY ([EdcId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_Edc_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_Edc_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Edc_Parent] ([FactorSetId], [EdcId])
);


GO

CREATE TRIGGER [dim].[t_Edc_Parent_u]
ON [dim].[Edc_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Edc_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Edc_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Edc_Parent].[EdcId]			= INSERTED.[EdcId];

END;