﻿CREATE TABLE [dim].[AccountType_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [AccountTypeId]      VARCHAR (12)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (12)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_AccountType_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_AccountType_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_AccountType_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_AccountType_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_AccountType_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_AccountType_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [AccountTypeId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_AccountType_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_AccountType_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[AccountType_LookUp] ([AccountTypeId]),
    CONSTRAINT [FK_AccountType_Bridge_FactorSetLu] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_AccountType_Bridge_LookUp_AccountTypes] FOREIGN KEY ([AccountTypeId]) REFERENCES [dim].[AccountType_LookUp] ([AccountTypeId]),
    CONSTRAINT [FK_AccountType_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [AccountTypeId]) REFERENCES [dim].[AccountType_Parent] ([FactorSetId], [AccountTypeId]),
    CONSTRAINT [FK_AccountType_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[AccountType_Parent] ([FactorSetId], [AccountTypeId])
);


GO

CREATE TRIGGER [dim].[t_AccountType_Bridge_u]
ON [dim].[AccountType_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[AccountType_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[AccountType_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[AccountType_Bridge].[AccountTypeId]	= INSERTED.[AccountTypeId]
		AND	[dim].[AccountType_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;