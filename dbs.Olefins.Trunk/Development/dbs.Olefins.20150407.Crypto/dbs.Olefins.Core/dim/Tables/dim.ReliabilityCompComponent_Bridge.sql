﻿CREATE TABLE [dim].[ReliabilityCompComponent_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [CompComponentId]    VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ReliabilityCompComponent_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityCompComponent_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityCompComponent_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityCompComponent_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReliabilityCompComponent_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityCompComponent_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CompComponentId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ReliabilityCompComponent_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ReliabilityCompComponent_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ReliabilityCompComponent_LookUp] ([CompComponentId]),
    CONSTRAINT [FK_ReliabilityCompComponent_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityCompComponent_Bridge_LookUp_ReliabilityCompComponent] FOREIGN KEY ([CompComponentId]) REFERENCES [dim].[ReliabilityCompComponent_LookUp] ([CompComponentId]),
    CONSTRAINT [FK_ReliabilityCompComponent_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [CompComponentId]) REFERENCES [dim].[ReliabilityCompComponent_Parent] ([FactorSetId], [CompComponentId]),
    CONSTRAINT [FK_ReliabilityCompComponent_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ReliabilityCompComponent_Parent] ([FactorSetId], [CompComponentId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityCompComponent_Bridge_u]
ON [dim].[ReliabilityCompComponent_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityCompComponent_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityCompComponent_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityCompComponent_Bridge].[CompComponentId]	= INSERTED.[CompComponentId]
		AND	[dim].[ReliabilityCompComponent_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;