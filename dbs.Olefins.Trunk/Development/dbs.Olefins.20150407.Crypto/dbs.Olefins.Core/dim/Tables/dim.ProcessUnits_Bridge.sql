﻿CREATE TABLE [dim].[ProcessUnits_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [UnitId]             VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ProcessUnits_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ProcessUnits_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ProcessUnits_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ProcessUnits_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ProcessUnits_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ProcessUnits_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [UnitId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ProcessUnits_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ProcessUnits_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId]),
    CONSTRAINT [FK_ProcessUnits_Bridge_Factor_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ProcessUnits_Bridge_LookUp_ProcessUnits] FOREIGN KEY ([UnitId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId]),
    CONSTRAINT [FK_ProcessUnits_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [UnitId]) REFERENCES [dim].[ProcessUnits_Parent] ([FactorSetId], [UnitId]),
    CONSTRAINT [FK_ProcessUnits_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ProcessUnits_Parent] ([FactorSetId], [UnitId])
);


GO

CREATE TRIGGER [dim].[t_ProcessUnits_Bridge_u]
ON [dim].[ProcessUnits_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnits_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ProcessUnits_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ProcessUnits_Bridge].[UnitId]	= INSERTED.[UnitId]
		AND	[dim].[ProcessUnits_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;
