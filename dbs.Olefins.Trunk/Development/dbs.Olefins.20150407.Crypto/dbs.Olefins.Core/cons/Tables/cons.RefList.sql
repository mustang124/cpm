﻿CREATE TABLE [cons].[RefList] (
    [ListId]         VARCHAR (42)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [UserGroup]      VARCHAR (42)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_RefList_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_RefList_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_RefList_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_RefList_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_RefList] PRIMARY KEY CLUSTERED ([ListId] ASC, [Refnum] ASC, [UserGroup] ASC),
    CONSTRAINT [FK_RefList_ListID] FOREIGN KEY ([ListId]) REFERENCES [cons].[RefListLu] ([ListId])
);


GO
CREATE TRIGGER [cons].[t_RefList_u]
	ON cons.RefList
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[RefList]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[RefList].ListId		= INSERTED.ListId
		AND	[cons].[RefList].Refnum		= INSERTED.Refnum
		AND	[cons].[RefList].UserGroup	= INSERTED.UserGroup;

END;