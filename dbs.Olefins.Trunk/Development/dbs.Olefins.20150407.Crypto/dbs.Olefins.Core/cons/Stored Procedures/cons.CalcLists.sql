﻿CREATE PROC [cons].[CalcLists]
AS
    
	DECLARE @ListID varchar(20)	
	DECLARE cCalc CURSOR LOCAL FAST_FORWARD	
	FOR	SELECT ListID FROM cons.ListsToCalc WHERE DoCalcs = 1
	OPEN cCalc	
	FETCH NEXT FROM cCalc INTO @ListID	
	WHILE @@FETCH_STATUS = 0	
	BEGIN	
		EXEC spCalcs @ListID
		FETCH NEXT FROM cCalc INTO @ListID
	END	
	CLOSE cCalc	
	DEALLOCATE cCalc	    
    
/*
DECLARE @RefsToCalc TABLE(Refnum varchar(18))
INSERT @RefsToCalc (Refnum)
SELECT DISTINCT Refnum FROM cons.reflist where ListID in (SELECT ListID FROM cons.ListsToCalc WHERE DoCalcs = 1)
WHILE EXISTS(SELECT * FROM @RefsToCalc)
BEGIN
	SELECT TOP 1 @Refnum = Refnum FROM @RefsToCalc
	EXEC dbo.spCalcs @Refnum
	DELETE FROM @RefsToCalc WHERE Refnum = @Refnum
END        
*/                   
set Nocount OFF


