﻿using System;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace Sa.OsimExtensions
{
	[CLSCompliant(true)]
	[ComVisible(true)]
	public class Crypto
	{
		const string encryptedExtension = "encrypted";
		const string decryptedExtension = "decrypted";

		public void Encrypt(string pathPlaintext, string initVector, string key)
		{
			//	Read plain text to bytes
			byte[] PlainText = File.ReadAllBytes(pathPlaintext);

			//	Compress bytes
			byte[] GZipPacked;
			Compress(PlainText, out GZipPacked);

			//	Encrypt bytes
			byte[] CryptoText;
			AesEncryptBytes(GZipPacked, key, initVector, out CryptoText);

			//	Write encrypted bytes to file
			string PathEncrypted = Path.ChangeExtension(pathPlaintext, encryptedExtension + Path.GetExtension(pathPlaintext));
			File.WriteAllBytes(PathEncrypted, CryptoText);
		}

		public void Decrypt(string pathEncrypted, string initVector, string key)
		{
			//	Read Cypher Text File to Bytes
			byte[] CypherText = File.ReadAllBytes(pathEncrypted);

			//	Decrypt Bytes
			byte[] GZipPacked;
			AesDecryptBytes(CypherText, key, initVector, out GZipPacked);

			//	Expand Bytes
			byte[] PlainText;
			Expand(GZipPacked, out PlainText);

			//	Write
			string PathDecrypted = pathEncrypted.Replace(encryptedExtension, decryptedExtension);
			File.WriteAllBytes(PathDecrypted, PlainText);
		}

		private static void Compress(byte[] Raw, out byte[] Compressed)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				using (GZipStream gz = new GZipStream(ms, CompressionMode.Compress, false))
				{
					gz.Write(Raw, 0, Raw.Length);
					gz.Close();
				}
				Compressed = ms.ToArray();
				ms.Close();
			}
		}

		private static void Expand(byte[] Raw, out byte[] Expanded)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				using (MemoryStream cs = new MemoryStream(Raw, 0, Raw.Length, false, false))
				{
					using (GZipStream gz = new GZipStream(cs, CompressionMode.Decompress, false))
					{
						gz.CopyTo(ms);
						gz.Close();
					}
					cs.Close();
				}
				Expanded = ms.ToArray();
				ms.Close();
			}
		}

		private const int AesBlockSize = 128;		//	128
		private const int AesFeedbackSize = 64;		//	  8
		private const int AesKeySize = 256;			//	256

		private const CipherMode AesCipherMode = CipherMode.CBC;
		private const PaddingMode AesPaddingMode = PaddingMode.ANSIX923;

		private static void AesEncryptBytes(byte[] plainText, string sKey, string sIv, out byte[] cryptoText)
		{
			byte[] bKey = ASCIIEncoding.UTF8.GetBytes(sKey); ;
			byte[] bIv = ASCIIEncoding.UTF8.GetBytes(sIv);

			using (Rfc2898DeriveBytes rfcKey = new Rfc2898DeriveBytes(bKey, bIv, 1000))
			{
				using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
				{
					aes.BlockSize = AesBlockSize;
					aes.FeedbackSize = AesFeedbackSize;
					aes.KeySize = AesKeySize;
					aes.Mode = AesCipherMode;
					aes.Padding = AesPaddingMode;

					aes.Key = rfcKey.GetBytes(aes.KeySize / 8);
					aes.IV = rfcKey.GetBytes(aes.BlockSize / 8);

					using (MemoryStream ms = new MemoryStream())
					{
						using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
						{
							cs.Write(plainText, 0, plainText.Length);
							cs.Close();
						}
						cryptoText = ms.ToArray();
						ms.Close();
					}
				}
			}
		}

		private static void AesDecryptBytes(byte[] cryptoText, string sKey, string sIv, out byte[] plainText)
		{
			byte[] bKey = ASCIIEncoding.UTF8.GetBytes(sKey); ;
			byte[] bIv = ASCIIEncoding.UTF8.GetBytes(sIv);

			using (Rfc2898DeriveBytes rfcKey = new Rfc2898DeriveBytes(bKey, bIv, 1000))
			{
				using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
				{
					aes.BlockSize = AesBlockSize;
					aes.FeedbackSize = AesFeedbackSize;
					aes.KeySize = AesKeySize;
					aes.Mode = AesCipherMode;
					aes.Padding = AesPaddingMode;

					aes.Key = rfcKey.GetBytes(aes.KeySize / 8);
					aes.IV = rfcKey.GetBytes(aes.BlockSize / 8);

					using (MemoryStream ms = new MemoryStream())
					{
						using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
						{
							cs.Write(cryptoText, 0, cryptoText.Length);
							cs.Close();
						}
						plainText = ms.ToArray();
						ms.Close();
					}
				}
			}
		}
	}
}