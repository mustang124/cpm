﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Chem.UpLoad
{
	public partial class ControlForm : Form
	{
		public ControlForm()
		{
			InitializeComponent();
		}

		private void ControlForm_Load(object sender, EventArgs e)
		{

		}

		private void UpLoadOne_Click(object sender, EventArgs e)
		{
			string f = GetUploadFile();
			
			if (f != "")
			{
				uLoad(f);
				MessageBox.Show("File " + f + " Has been Uploaded");
			}
		}

		private void UpLoadFolder_Click(object sender, EventArgs e)
		{
			string p = GetUploadPath();

			if (System.IO.Directory.Exists(p))
			{
				int i = 0;
				foreach (string f in System.IO.Directory.GetFiles(p))
				{
					uLoad(f);
					i++;
				}
				MessageBox.Show(i.ToString() + " Files have been Uploaded");
			}
		}

		private static void uLoad(string FilePath)
		{
			ChemUpLoad.UpLoadFile(FilePath);
		}

		private string GetUploadFile()
		{
			string f = string.Empty;

			OpenFileDialog file = new OpenFileDialog();

			file.Filter = "Excel Files (*.xls)|*.xls";

			if (file.ShowDialog() == DialogResult.OK)
			{
				f = file.FileName;
			}

			return f;
		}

		private string GetUploadPath()
		{
			string p = string.Empty;

			FolderBrowserDialog path = new FolderBrowserDialog();

			if (path.ShowDialog() == DialogResult.OK)
			{
				p = path.SelectedPath;
			}

			return p;
		}

		//private void button1_Click(object sender, EventArgs e)
		//{
		//    string p = "\\\\Dallas2\\data\\Data\\STUDY\\Olefins\\2013\\Correspondence\\";

		//    System.Collections.Generic.List<string> Files = new System.Collections.Generic.List<string>();
		//    //Files.Add("C:\\Users\\RRH\\Desktop\\OSIM2013PCH011.xls");

		//    Files.Add(p + "13PCH003\\OSIM2013PCH003.xls");
		//    Files.Add(p + "13PCH003\\PYPS2013PCH003.xls");
		//    Files.Add(p + "13PCH003\\SPSL2013PCH003.xls");

		//    Files.Add(p + "13PCH058\\OSIM2013PCH058.xls");
		//    Files.Add(p + "13PCH058\\PYPS2013PCH058.xls");
		//    Files.Add(p + "13PCH058\\SPSL2013PCH058.xls");

		//    Files.Add(p + "13PCH122\\OSIM2013PCH122.xls");
		//    Files.Add(p + "13PCH122\\PYPS2013PCH122.xls");
		//    Files.Add(p + "13PCH122\\SPSL2013PCH122.xls");

		//    Files.Add(p + "13PCH148\\OSIM2013PCH148.xls");
		//    Files.Add(p + "13PCH148\\PYPS2013PCH148.xls");
		//    Files.Add(p + "13PCH148\\SPSL2013PCH148.xls");

		//    Files.Add(p + "13PCH011\\OSIM2013PCH011.xls");
		//    Files.Add(p + "13PCH011\\PYPS2013PCH011.xls");
		//    Files.Add(p + "13PCH011\\SPSL2013PCH011.xls");

		//    Files.Add(p + "13PCH175\\OSIM2013PCH175.xls");
		//    Files.Add(p + "13PCH175\\PYPS2013PCH175.xls");
		//    Files.Add(p + "13PCH175\\SPSL2013PCH175.xls");

		//    foreach (string f in Files)
		//    {
		//        if (System.IO.File.Exists(f))
		//        {
		//            textBox1.Text = System.IO.Path.GetFileName(f);
		//            ChemUpLoad.UpLoadFile(f);
		//            textBox1.Text = string.Empty;
		//        }
		//    }

		//    MessageBox.Show(Files.Count.ToString() + " have been Uploaded.");

		//}
	}
}
