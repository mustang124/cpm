﻿using System;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public class Upload_OSIM: Chem.UpLoad.InputForm
	{
		public void UpLoad(string Refnum, string PathFile)
		{
			string sRefnum = Common.GetRefnumSmall(Refnum);
			uint StudyYear = Common.GetStudyYear(Refnum);

			Delete(sRefnum);
			Insert(PathFile, sRefnum, StudyYear);
		}

		void Insert(string PathFile, string sRefnum, uint StudyYear)
		{
			Excel.Application xla = Common.NewExcelApplication();
			Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, PathFile);

			if (VerifyRefnum(wkb, sRefnum))
			{
				Parallel.Invoke
				(
					() => { UpLoadTSort(wkb, sRefnum, StudyYear); },
					() => { UpLoadCapacity(wkb, sRefnum, StudyYear); },
					() => { UpLoadCapGrowth(wkb, sRefnum); },
					() => { UpLoadFacilities(wkb, sRefnum, StudyYear); },
					() => { UpLoadFeedQuality(wkb, sRefnum); },
					() => { UpLoadQuantity(wkb, sRefnum); },
					() => { UpLoadOpEx(wkb, sRefnum); },
					() => { UpLoadOpExMiscDetail(wkb, sRefnum); },
					() => { UpLoadPers(wkb, sRefnum); },
					() => { UpLoadPracTA(wkb, sRefnum); },
					() => { UpLoadTADT(wkb, sRefnum); },
					() => { UpLoadPracFurnInfo(wkb, sRefnum); },
					() => { UpLoadFurnRely(wkb, sRefnum); },
					() => { UpLoadEnergyQnty(wkb, sRefnum, StudyYear); },
					() => { UpLoadPracAdv(wkb, sRefnum); },
					() => { UpLoadPracMPC(wkb, sRefnum); },
					() => { UpLoadPracControls(wkb, sRefnum); },
					() => { UpLoadPracOnlineFact(wkb, sRefnum); },
					() => { UpLoadMaint01(wkb, sRefnum); },
					() => { UpLoadMaintMisc(wkb, sRefnum); },
					() => { UpLoadFeedSelections(wkb, sRefnum, StudyYear); },
					() => { UpLoadFeedResources(wkb, sRefnum, StudyYear); },
					() => { UpLoadMisc(wkb, sRefnum, StudyYear); },
					() => { UpLoadPracOrg(wkb, sRefnum); },
					() => { UpLoadProdQuality(wkb, sRefnum); },
					() => { UpLoadComposition(wkb, sRefnum); },
					() => { UpLoadPrac(wkb, sRefnum, StudyYear); },
					() => { UpLoadInventory(wkb, sRefnum); },
					() => { UpLoadDuties(wkb, sRefnum); },
					() => { UpLoadFeedFlex(wkb, sRefnum); },
					() => { UpLoadCapitalExp(wkb, sRefnum); },
					() => { UpLoadEnviron(wkb, sRefnum); },
					() => { UpLoadFeedPlan(wkb, sRefnum, StudyYear); },
					() => { UpLoadProdLoss(wkb, sRefnum, StudyYear); },
					() => { if (StudyYear >= 2013) { UpLoadProdLoss_Availability(wkb, sRefnum, StudyYear); } }
				);

				Parallel.Invoke
				(
					() => { UpLoadPolyFacilities(wkb, sRefnum); },
					() => { UpLoadPolyOpEx(wkb, sRefnum); },
					() => { UpLoadPolyMatlBalance(wkb, sRefnum); },
					() => { UpLoadMetaFeedProd(wkb, sRefnum); },
					() => { UpLoadMetaOpEx(wkb, sRefnum); },
					() => { UpLoadMetaEnergy(wkb, sRefnum); },
					() => { UpLoadMetaMisc(wkb, sRefnum); }
				);
			}

			Common.CloseExcel(ref xla, ref wkb);
		}
	}
}
