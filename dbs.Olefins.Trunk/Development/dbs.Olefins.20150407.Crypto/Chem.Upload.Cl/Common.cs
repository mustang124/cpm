﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public class Common
	{
		public static string Osim = "OSIM";
		public static string Spsl = "SPSL";
		public static string Pyps = "PYPS";
		public static string Seec = "SEEC";

		public static Excel.Application NewExcelApplication()
		{
			Excel.Application xla = new Excel.Application();

			xla.AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityForceDisable;
			xla.AskToUpdateLinks = false;
			xla.DisplayAlerts = false;
			xla.FileValidation = Microsoft.Office.Core.MsoFileValidationMode.msoFileValidationSkip;
			xla.Interactive = false;
			xla.ScreenUpdating = false;
			xla.Visible = false;
			xla.EnableEvents = false;

			return xla;
		}

		public static Excel.Workbook OpenWorkbook_ReadOnly(Excel.Application xla, String PathFile)
		{
			PathFile = System.IO.Path.GetFullPath(PathFile);
			Excel.Workbook wkb = xla.Workbooks.Open(PathFile, Excel.XlUpdateLinks.xlUpdateLinksNever, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, false, Type.Missing, Excel.XlCorruptLoad.xlNormalLoad);
			wkb.EnableAutoRecover = false;

			return wkb;
		}

		public static string GetRefnum(string FileType, string PathFile)
		{
			string Refnum = string.Empty;
			string f = System.IO.Path.GetFileNameWithoutExtension(PathFile);

			if (FileType == Osim || FileType == Spsl || FileType == Pyps)
			{
				Refnum = f.Replace(FileType, "");
			}
			else if (FileType == Seec)
			{
				Refnum = f.Replace("OSIM", "");
			}

			return Refnum;
		}

		public static string GetRefnumSmall(string Refnum)
		{
			return Refnum.Substring(2, Refnum.Length - 2);
		}

		public static uint GetStudyYear(string Refnum)
		{
			string StudyYear = Refnum.Substring(0, 4);
			
			return Convert.ToUInt32(StudyYear);
		}

		/// <summary>
		/// http://www.regexr.com/
		/// File types to resolve:
		///		PYPS2013PCH012.XLS
		///		SPSL2013PCH033.xls
		///		OSIM2013PCH033.xls
		///		MEEP141OSIM09.xls
		/// </summary>
		/// <param name="PathFile"></param>
		/// <returns></returns>
		public static string GetFileType(string PathFile)
		{
			List<string[]> l = new List<string[]>();

			string xlsExtension = "(\\.xls)";

			l.Add(new string[2] { "(PYPS)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Pyps });
			l.Add(new string[2] { "(SPSL)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Spsl });
			l.Add(new string[2] { "(OSIM)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Osim });

			l.Add(new string[2] { "(PYPS)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Pyps });
			l.Add(new string[2] { "(SPSL)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Spsl });
			l.Add(new string[2] { "(OSIM)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Osim });

			l.Add(new string[2] { "(OSIM)(([0-9]{4})(SEEC)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Seec });
			l.Add(new string[2] { "(OSIM)(([0-9]{4})(SEEC)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Seec });

			System.Text.RegularExpressions.Regex r;

			foreach (string[] s in l)
			{
				r = new Regex(s[0], RegexOptions.IgnoreCase);
				if (r.IsMatch(PathFile))
				{
					return s[1];
				}
			}
			return string.Empty;
		}		

		public static string GetPathRoot(string PathFile)
		{
			string FileType = GetFileType(PathFile);
			string PathRoot = string.Empty;

			if (FileType == Osim || FileType == Spsl || FileType == Pyps)
			{
				PathRoot = "Olefins";
			}
			else if (FileType == Seec)
			{
				PathRoot = "SEEC";
			}

			PathRoot = "K:\\STUDY\\" + PathRoot + "\\";

			return PathRoot;
		}

		public static void CloseExcel(ref Excel.Application xla, ref Excel.Workbook wkb, ref Excel.Worksheet wks)
		{
			if (wks != null)
			{
				Marshal.FinalReleaseComObject(wks);
				wks = null;
			}

			CloseExcel(ref xla, ref wkb);
		}

		public static void CloseExcel(ref Excel.Application xla, ref Excel.Workbook wkb)
		{
			if (wkb != null)
			{
				wkb.Close(false);
				Marshal.FinalReleaseComObject(wkb);
				wkb = null;
			}

			if (xla != null)
			{
				xla.Quit();
				Marshal.FinalReleaseComObject(xla);
				xla = null;
			}

			System.GC.WaitForPendingFinalizers();
			System.GC.Collect();
			System.GC.WaitForFullGCComplete();
		}

		public static void CopyFile(string PathFile, string Refnum, uint StudyYear)
		{
			string PathRoot = GetPathRoot(PathFile);

			CopyFileToCorrespondence(PathRoot, PathFile, Refnum, StudyYear);
			CopyFileToPlants(PathRoot, PathFile, Refnum, StudyYear);
		}

		public static void CopyFileToCorrespondence(string PathRoot, string PathFile, string Refnum, uint StudyYear)
		{
			string PathTarget = PathRoot + StudyYear.ToString() + "\\Correspondence\\" + GetRefnumSmall(Refnum) + "\\Uploads\\";
			string FileTarget = System.IO.Path.GetFileNameWithoutExtension(PathFile) + " " + GetDateTimeStamp() + "." + System.IO.Path.GetExtension(PathFile);

			System.IO.Directory.CreateDirectory(PathTarget);
			System.IO.File.Copy(PathFile, PathTarget + FileTarget, false);
		}

		public static void CopyFileToPlants(string PathRoot, string PathFile, string Refnum, uint StudyYear)
		{
			string PathTarget = PathRoot + StudyYear.ToString() + "\\Plants\\" + GetRefnumSmall(Refnum) + "\\";
			string FileTarget = System.IO.Path.GetFileName(PathFile);

			System.IO.Directory.CreateDirectory(PathTarget);
			System.IO.File.Copy(PathFile, PathTarget + FileTarget, true);	
		}

		public static string GetDateTimeStamp()
		{
			return DateTime.Now.ToString("yyyyMMdd.HHmmss");
		}

		public static string cnString()
		{
			string db = "Olefins";

// HACK: Use development database
#if DEBUG
			db = db + "Dev";
#endif

			return Common.dbConnectionString("DBS5", db, "rrh", "rrh#4279");
		}

		public static string dbConnectionString(string dbInst, string dbName, string UserName, string UserPass)
		{
			string cn = "Data Source=" + dbInst + ";";
			cn = cn + "Initial Catalog=" + dbName + ";";
			cn = cn + "User Id=" + UserName + ";";
			cn = cn + "Password=" + UserPass + ";";

			cn = cn + "Application Name = " + ProdNameVer() + ";";

			return cn;
		}

		// HACK: set Version number  = SELECT DATEDIFF(d, '1/1/2000', SYSDATETIME())
		private const string ver = "5507";

		public static string ProdNameVer()
		{
			string rtn = string.Empty;

			try
			{
				Assembly ai = Assembly.GetExecutingAssembly();
				rtn = ai.GetName().Name + " (" + ai.GetName().Version + ")";
			}
			catch
			{
				rtn = "Chem.Upload.Cl 1.0." + ver + ".x";
			}

			return rtn;

		}

		public static string ProdVer()
		{
			string rtn = string.Empty;

			try
			{
				Assembly ai = Assembly.GetExecutingAssembly();
				rtn = Convert.ToString(ai.GetName().Version);
			}
			catch
			{
				rtn = "1.0." + ver + ".x";
			}

			return rtn;	
		}
	}

	class ErrorHandler
	{
		private static string CellAddress(UInt32 r, UInt32 c)
		{
			string l = String.Empty;
			UInt32 m;

			while (c > 0)
			{
				m = (c - 1) % 26;
				l = Convert.ToChar(65 + m).ToString() + l;
				c = Convert.ToUInt32((c - m) / 26);
			}

			return l + r.ToString();
		}

		public static List<string> EtlErrors = new List<string>();

		public static void Insert_UpLoadError(string FileType, string MethodName, string Refnum,
			Excel.Workbook wkb, Excel.Worksheet wks, Excel.Range rng, UInt32 row, UInt32 col, 
			string SqlProcedureName, Exception ex)
		{

			SqlConnection cn = new SqlConnection(Common.cnString());
			cn.Open();

			SqlCommand cmd = new SqlCommand("[stgFact].[Insert_UploadError]", cn);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 20).Value = FileType;
			cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
			cmd.Parameters.Add("@FilePath", SqlDbType.VarChar, 512).Value = wkb.Path + "\\" + wkb.Name;

			#region Sheet

			string Sheet = string.Empty;

			if (Sheet != null)
			{
				Sheet = wks.Name;
			}
			else
			{
				Sheet = "Worksheet not initialized";
			};

			cmd.Parameters.Add("@Sheet", SqlDbType.VarChar, 128).Value = Sheet;

			#endregion

			#region Range

			string Range = string.Empty;

			if (Range != null)
			{
				Range = rng.AddressLocal;
				if (!string.IsNullOrEmpty(rng.Text)) { cmd.Parameters.Add("@CellText", SqlDbType.NVarChar, 128).Value = rng.Text; }

			}
			else
			{
				Range = "Range not initialized (" + Sheet + "!" + CellAddress(row, col) + ")";
			};

			cmd.Parameters.Add("@Range", SqlDbType.VarChar, 128).Value = Range;

			#endregion

			cmd.Parameters.Add("@ProcedureName", SqlDbType.NVarChar, 128).Value = SqlProcedureName;

			cmd.Parameters.Add("@SystemErrorMessage", SqlDbType.NVarChar, 128).Value = ex.Message;

			cmd.ExecuteNonQuery();

			cn.Close();

			AddToEtlList(MethodName, wks, rng, row, col, ex);

		}

		public static void AddToEtlList(string MethodName, Excel.Worksheet wks, Excel.Range rng, UInt32 row, UInt32 col, Exception ex)
		{
			string MessageBody = "Module: " + MethodName + "\r\n";
			MessageBody = MessageBody + "Range: " + wks.Name + "!" + CellAddress(row, col) + "\r\n";

			if (rng == null)
			{
				MessageBody = MessageBody + "Address Range failed to initialize: " + wks.Name + "!" + CellAddress(row, col) + " (s= " + wks.Name + ", r= " + row.ToString() + ", c= " + col.ToString() + ")\r\n\r\n";
			}
			else
			{
				MessageBody = MessageBody + "Cell Text: " + Convert.ToString(rng.Text) + "\r\n\r\n";
			}

			EtlErrors.Add(MessageBody + ex.Message);
		}
		
		public static string AggregateMessage()
		{
			string msg = "";

			foreach (string s in EtlErrors)
			{
				if (msg != "") { msg = msg + "\r\n\r\n"; };
				msg = msg + s;
			}

			return msg;
		}

		public static void Delete_UploadErrors(string FileType, string Refnum)
		{
			string sRefnum = Common.GetRefnumSmall(Refnum);

			SqlConnection cn = new SqlConnection(Common.cnString());
			cn.Open();

			SqlCommand cmd = new SqlCommand("[stgFact].[Delete_UploadErrors]", cn);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 20).Value = FileType;
			cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = sRefnum;

			cmd.ExecuteNonQuery();

			cn.Close();
		}
	}
}
