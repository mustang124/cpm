﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPolyOpEx(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;
			Excel.Range rngB = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const UInt32 cBeg = 4;
			const UInt32 cEnd = 9;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table12-1"];

			for (c = cBeg; c <= cEnd; c++)
			{
				try
				{
					rng = wks.Range[wks.Cells[18, c], wks.Cells[20, c]];
					rngB = wks.Range[wks.Cells[23, c], wks.Cells[30, c]];

					if (RangeHasValue(rng) || RangeHasValue(rngB))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PolyOpEx]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum				CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@PlantID     TINYINT
						cmd.Parameters.Add("@PlantID", SqlDbType.TinyInt).Value = Convert.ToByte(c - cBeg + 1);

						//@NonTAMaint  REAL     = NULL
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@NonTAMaint", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TA          REAL     = NULL
						r = 19;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TA", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthNonVol   REAL     = NULL
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthNonVol", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@STNonVol    REAL     = NULL
						r = 21;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@STNonVol", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Catalysts   REAL     = NULL
						r = 23;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Catalysts", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Chemicals   REAL     = NULL
						r = 24;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Chemicals", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Additives   REAL     = NULL
						r = 25;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Additives", SqlDbType.Float).Value = ReturnFloat(rng); }
	
						//@Royalties   REAL     = NULL
						r = 26;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Royalties", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Energy      REAL     = NULL
						r = 27;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Energy", SqlDbType.Float).Value = ReturnFloat(rng); }

						//Done: Where is "Other Utilities - Nitrogen, Water, Etc.?

						//@Packaging   REAL     = NULL
						r = 29;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Packaging", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthVol      REAL     = NULL
						r = 30;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthVol", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@STVol       REAL     = NULL
						r = 31;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@STVol", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TotCashOpEx REAL     = NULL
						r = 32;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotCashOpEx", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPolyOpEx", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PolyOpEx]", ex);
				}
			}
			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rngB = null;
			rng = null;
			wks = null;
		}
	}
}
