﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPolyFacilities(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const UInt32 rBeg = 6;
			const UInt32 rEnd = 16;
			const UInt32 cBeg = 4;
			const UInt32 cEnd = 9;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table12-1"];

			for (c = cBeg; c <= cEnd; c++)
			{
				try
				{
					rng = wks.Range[wks.Cells[rBeg, c], wks.Cells[rEnd, c]];
					if (RangeHasValue(rng))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PolyFacilities]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum				CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@PlantID         TINYINT
						cmd.Parameters.Add("@PlantID", SqlDbType.TinyInt).Value = Convert.ToByte(c - cBeg + 1); ;

						//@Name            VARCHAR (50) = NULL
						r = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Name", SqlDbType.NVarChar, 50).Value = ConvertFeedProdId(rng, 50); }
	
						//@City            VARCHAR (40) = NULL
						r = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@City", SqlDbType.NVarChar, 40).Value = ConvertFeedProdId(rng, 40); }

						//@State           VARCHAR (25) = NULL
						r = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@State", SqlDbType.NVarChar, 25).Value = ConvertFeedProdId(rng, 25); }

						//@Country         VARCHAR (30) = NULL
						r = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Country", SqlDbType.NVarChar, 30).Value = ConvertFeedProdId(rng, 30); }

						//@CapMTD          REAL         = NULL
						r = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CapMTD", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CapKMTA         REAL         = NULL
						r = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CapKMTA", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ReactorTrainCnt INT          = NULL
						r = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ReactorTrainCnt", SqlDbType.Int).Value = ReturnUShort(rng); }

						//@ReactorStartup  SMALLINT     = NULL
						r = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ReactorStartup", SqlDbType.SmallInt).Value = ReturnShort(rng); }

						//@Product         VARCHAR (20) = NULL
						r = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Product", SqlDbType.VarChar, 20).Value = ConvertFeedProdId(rng, 20); }
	
						//@Technology      VARCHAR (20) = NULL
						r = 15;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Technology", SqlDbType.VarChar, 20).Value = ConvertFeedProdId(rng, 20); }
	
						//@ReactorResinPct REAL         = NULL
						r = 16;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ReactorResinPct", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPolyFacilities", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PolyFacilities]", ex);
				}
			}
		
			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
