﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracTA(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table6-2"];

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracTA]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum		CHAR (9),
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				#region Critical Path

				c = 5;
				r = 11;

				//@CGC           CHAR (1)     = NULL
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@CGC", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@RC            CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@RC", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@TLE           CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TLE", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@OHX           CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OHX", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Tower         CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tower", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Furnace       CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Furnace", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Util          CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Util", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@CapProj       CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@CapProj", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Other         CHAR (1)     = NULL
				r++;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Other", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				c = 3;

				//@OthCPMDesc    VARCHAR (75) = NULL
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthCPMDesc", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

				#endregion

				#region Additional Meta Data

				//@TimeFact      CHAR (1)     = NULL
				rng = wks.Range["E26"];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TimeFact", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@TAPrepTime    REAL         = NULL
				rng = wks.Range["F42"];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TAPrepTime", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@TAStartupTime REAL         = NULL
				rng = wks.Range["F46"];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TAStartupTime", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				//@OthTFDesc     VARCHAR (75) = NULL
				rng = wks.Range["C19"];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthTFDesc", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

				cmd.ExecuteNonQuery();

			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracTA", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracTA]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
