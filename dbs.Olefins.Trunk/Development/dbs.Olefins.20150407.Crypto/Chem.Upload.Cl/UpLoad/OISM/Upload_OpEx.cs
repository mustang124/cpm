﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadOpEx(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			const UInt32 c = 5;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table4"];

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_OpEx]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum				CHAR (9),
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				#region Personnel

				//@OCCSal            REAL     = NULL
				r = 7;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OCCSal", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@MPSSal            REAL     = NULL
				r = 8;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@MPSSal", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@OCCBen            REAL     = NULL
				r = 10;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OCCBen", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@MPSBen            REAL     = NULL
				r = 11;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@MPSBen", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Non-volume

				//@MaintMatl         REAL     = NULL
				r = 12;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@MaintMatl", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ContMaintLabor    REAL     = NULL
				r = 13;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ContMaintLabor", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ContMaintMatl     REAL     = NULL
				r = 14;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ContMaintMatl", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Equip             REAL     = NULL
				r = 15;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Equip", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ContTechSvc       REAL     = NULL
				r = 16;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ContTechSvc", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Envir             REAL     = NULL
				r = 17;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Envir", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@OthCont           REAL     = NULL
				r = 18;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthCont", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@NonMaintEquipRent REAL     = NULL
				r = 19;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@NonMaintEquipRent", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Tax               REAL     = NULL
				r = 20;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tax", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Insur             REAL     = NULL
				r = 21;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Insur", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@OthNonVol         REAL     = NULL
				r = 22;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthNonVol", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@STNonVol          REAL     = NULL
				r = 23;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@STNonVol", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Turnaround

				//@TAAccrual         REAL     = NULL
				r = 25;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TAAccrual", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@TACurrExp         REAL     = NULL
				r = 26;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TACurrExp", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@STTAExp           REAL     = NULL
				r = 27;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@STTAExp", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Chemicals

				//@Chemicals         REAL     = NULL
				r = 29;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Chemicals", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemBFWCW         REAL     = NULL
				r = 60;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemBFWCW", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemWW            REAL     = NULL
				r = 61;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemWW", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemCaustic       REAL     = NULL
				r = 62;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemCaustic", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemPretreatment  REAL     = NULL
				r = 63;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemPretreatment", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemAntiFoul      REAL     = NULL
				r = 64;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemAntiFoul", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemOthProc       REAL     = NULL
				r = 65;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemOthProc", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemAdd           REAL     = NULL
				r = 66;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemAdd", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ChemOth           REAL     = NULL
				r = 67;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ChemOth", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Catalysts and Royalties

				//@Catalysts         REAL     = NULL
				r = 30;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Catalysts", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Royalties         REAL     = NULL
				r = 31;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Royalties", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Purchased Utilities

				//@PurElec           REAL     = NULL
				r = 33;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PurElec", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PurSteam          REAL     = NULL
				r = 34;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PurSteam", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PurOth            REAL     = NULL
				r = 35;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PurOth", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Purchased Fuel

				//@PurFG             REAL     = NULL
				r = 37;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PurFG", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PurLiquid         REAL     = NULL
				r = 38;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PurLiquid", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PurSolid          REAL     = NULL
				r = 39;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PurSolid", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region PPFC

				//@PPCFuelGas        REAL     = NULL
				r = 41;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PPCFuelGas", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PPCFuelOth        REAL     = NULL
				r = 42;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PPCFuelOth", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Export

				//@SteamExports      REAL     = NULL
				r = 44;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@SteamExports", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PowerExports      REAL     = NULL
				r = 45;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PowerExports", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Sub-total Volume

				//@OthVol            REAL     = NULL
				r = 46;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthVol", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@STVol             REAL     = NULL
				r = 47;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@STVol", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Total Cash OpEx

				//@TotCashOpEx       REAL     = NULL
				r = 48;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotCashOpEx", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Plant Expenses

				//@InvenCarry        REAL     = NULL
				r = 50;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@InvenCarry", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@GANonPers         REAL     = NULL
				r = 51;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@GANonPers", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Depreciation      REAL     = NULL
				r = 52;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Depreciation", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Interest          REAL     = NULL
				r = 53;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Interest", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Total Expenses

				//@TotRefExp         REAL     = NULL
				r = 54;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotRefExp", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region ForeXRate

				//@ForeXRate         REAL     = NULL
				rng = wks.Range["D56"];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ForeXRate", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadOpEx (OpEx)", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_OpEx]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
