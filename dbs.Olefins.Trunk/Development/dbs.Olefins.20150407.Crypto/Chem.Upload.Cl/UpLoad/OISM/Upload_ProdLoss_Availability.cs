﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadProdLoss_Availability(Excel.Workbook wkb, string Refnum, uint StudyYear)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;

			float sdP;
			float dtP;
			float sdC;
			float dtC;
			float qty;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table6-1"];

			rBeg = 72;
			rEnd = 79;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					qty = 0f;

					rng = wks.Cells[r, 6];
					if (RangeHasValue(rng)) { qty = ReturnFloat(rng); }

					rng = wks.Cells[r, 7];
					if (RangeHasValue(rng)) { qty = qty + ReturnFloat(rng); }

					rng = wks.Cells[r, 8];
					if (RangeHasValue(rng)) { qty = qty + ReturnFloat(rng); }

					rng = wks.Cells[r, 9];
					if (RangeHasValue(rng)) { qty = qty + ReturnFloat(rng); }

					if (qty > 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdLoss_Availability]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum		CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@Category    CHAR (3),
						cmd.Parameters.Add("@Category", SqlDbType.VarChar, 3).Value = "NOC";

						if (r >= 83 && r <= 86)
						{
							c = 2;

							//@CauseID     VARCHAR (20),
							cmd.Parameters.Add("@CauseID", SqlDbType.VarChar, 20).Value = "NonOp" + (r - rBeg + 1).ToString();

							//@Description VARCHAR (250) = NULL,
							rng = wks.Cells[r, c];
							cmd.Parameters.Add("@Description", SqlDbType.VarChar, 250).Value = ReturnString(rng, 250);

						}
						else
						{
							//@CauseID     VARCHAR (20),
							c = 2;
							rng = wks.Cells[r, c];
							cmd.Parameters.Add("@CauseID", SqlDbType.VarChar, 20).Value = ConvertLossCause(r);
						}

						#region Previous

						//@PrevDTLoss  REAL          = NULL,
						c = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							dtP = ReturnFloat(rng);
							cmd.Parameters.Add("@PrevDTLoss", SqlDbType.Float).Value = ReturnFloat(rng);
						}

						//@PrevSDLoss  REAL          = NULL,
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							sdP = ReturnFloat(rng);
							cmd.Parameters.Add("@PrevSDLoss", SqlDbType.Float).Value = ReturnFloat(rng);
						}

						#endregion

						#region Current

						//@DTLoss      REAL          = NULL,
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							dtC = ReturnFloat(rng);
							cmd.Parameters.Add("@DTLoss", SqlDbType.Float).Value = ReturnFloat(rng);
						}

						//@SDLoss      REAL          = NULL,
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							sdC = ReturnFloat(rng);
							cmd.Parameters.Add("@SDLoss", SqlDbType.Float).Value = ReturnFloat(rng);
						}

						#endregion

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdLoss_Availability", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdLoss_Availability]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
