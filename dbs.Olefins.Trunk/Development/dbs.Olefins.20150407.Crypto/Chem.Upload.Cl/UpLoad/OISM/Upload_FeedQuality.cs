﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadFeedQuality(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 cBeg = 0;
			UInt32 cEnd = 0;

			string sht = string.Empty;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());
			cn.Open();

			object[] itm = new object[3]
			{
				new object[3] { "Table2A-1", 3, 8 },
				new object[3] { "Table2A-2", 3, 14 },
				new object[3] { "Table2B", 3, 5 }
			};

			foreach (object[] s in itm)
			{
				sht = (string)s[0];

				wks = wkb.Worksheets[sht];

				cBeg = Convert.ToUInt32(s[1]);
				cEnd = Convert.ToUInt32(s[2]);

				for (c = cBeg; c <= cEnd; c++)
				{
					try
					{
						r = 36;
						if (wks.Name == "Table2A-2") { r++; };

						if ((double?)wks.Cells[r, c].Value +
							(double?)wks.Cells[++r, c].Value +
							(double?)wks.Cells[++r, c].Value +
							(double?)wks.Cells[++r, c].Value +
							(double?)wks.Cells[++r, c].Value > 0.0f)
						{
							SqlCommand cmd = new SqlCommand("[stgFact].[Insert_FeedQuality]", cn);
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum				CHAR (9),
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@FeedProdID VARCHAR (20),
							r = 4;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = ConvertFeedProdId(rng, c); ; }

							if (wks.Name == "Table2A-2")
							{
								#region Liquid

								//@SG						REAL					= NULL,
								r = 5;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@SG", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@SulfurPPM				REAL					= NULL,
								r = 6;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@SulfurPPM", SqlDbType.Float).Value = ReturnFloat(rng); }

								#endregion

								#region Distillation

								//@ASTMethod				VARCHAR (10)			= NULL,
								r = 8;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@ASTMethod", SqlDbType.VarChar, 10).Value = ReturnString(rng, 10); }

								//@IBP						REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@IBP", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@D5pcnt					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@D5pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@D10Pcnt					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@D10Pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@D30Pcnt					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@D30Pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@D50Pcnt					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@D50Pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@D70Pcnt					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@D70Pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@D90Pcnt					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@D90Pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@D95Pcnt					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@D95Pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@EP						REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@EP", SqlDbType.Float).Value = ReturnFloat(rng); }

								#endregion

								#region Composition (Liquid)

								//@NParaffins				REAL					= NULL,
								r = 20;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@NParaffins", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@IsoParaffins				REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@IsoParaffins", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Naphthenes				REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Naphthenes", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Olefins					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Olefins", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Aromatics				REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Aromatics", SqlDbType.Float).Value = ReturnFloat(rng); }

								#endregion

								//@Hydrogen					REAL					= NULL,
								r = 26;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Hydrogen", SqlDbType.Float).Value = ReturnFloat(rng); }

								#region Other Liquids

								if (c >= 12 && c <= 14)
								{
									//@OthLiqFeedDESC			VARCHAR (50)			= NULL,
									r = 4;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedDESC", SqlDbType.VarChar, 50).Value = ReturnString(rng, 50); }

									//@OthLiqFeedPriceBasis		REAL					= NULL
									r = 47;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedPriceBasis", SqlDbType.Float).Value = ReturnFloat(rng); }
								}

								#endregion

							}
							else
							{
								#region composition (light)

								//@Methane					REAL					= NULL,
								r = 6;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Methane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Ethane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Ethane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Ethylene					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Ethylene", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Propane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Propane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Propylene				REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Propylene", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@nButane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@nButane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@iButane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@iButane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Isobutylene				REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Isobutylene", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Butene1					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Butene1", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Butadiene				REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Butadiene", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@nPentane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@nPentane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@iPentane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@iPentane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@nHexane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@nHexane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@iHexane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@iHexane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Septane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Septane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Octane					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Octane", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@CO2						REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@CO2", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Hydrogen					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Hydrogen", SqlDbType.Float).Value = ReturnFloat(rng); }

								//@Sulfur					REAL					= NULL,
								r++;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng)) { cmd.Parameters.Add("@Sulfur", SqlDbType.Float).Value = ReturnFloat(rng); }

								#endregion
							}

							#region Operational Indicators

							r = 27;
							if (wks.Name == "Table2A-2") { r++; };

							//@CoilOutletPressure		REAL					= NULL,
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@CoilOutletPressure", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@SteamHCRatio				REAL					= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@SteamHCRatio", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@CoilOutletTemp			REAL					= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@CoilOutletTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

							#endregion

							#region Severity Indicators

							r = 31;
							if (wks.Name == "Table2A-2") { r++; };

							//@EthyleneYield			REAL					= NULL,
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthyleneYield", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@PropylEthylRatio			REAL					= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropylEthylRatio", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@FeedConv					REAL					= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedConv", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@PropylMethaneRatio		REAL					= NULL,
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropylMethaneRatio", SqlDbType.Float).Value = ReturnFloat(rng); }

							#endregion

							#region Pyrolysis Technology

							r = 43;
							if (wks.Name == "Table2A-2") { r++; };

							//@ConstrYear				INT						= NULL,
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@ConstrYear", SqlDbType.Float).Value = ReturnUShort(rng); }

							//@ResTime					REAL					= NULL,
							r++;
							r++;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@ResTime", SqlDbType.Float).Value = ReturnFloat(rng); }

							#endregion

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFeedQuality", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_FeedQuality]", ex);
					}
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
			itm = null;
		}
	}
}
