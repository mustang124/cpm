﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracFurnInfo(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const UInt32 cBeg = 6;
			const UInt32 cEnd = 9;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table6-3"];

			for (c = cBeg; c <= cEnd; c++)
			{
				try
				{
					r = 25;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracFurnInfo]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@FeedProdID VARCHAR (20),
						r = 23;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = ConvertFeedProdId(rng, 20); }

						//@Run        REAL         = NULL,
						r = 25;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng) && ReturnFloat(rng) > 0.0f) { cmd.Parameters.Add("@Run", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Inj        CHAR (5)     = NULL,
						//@Pretreat   CHAR (1)     = NULL,
						//@AntiFoul   CHAR (1)     = NULL

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracFurnInfo", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracFurnInfo]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
