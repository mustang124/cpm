﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadFurnRely(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const UInt32 cBeg = 3;
			const UInt32 cEnd = 34;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());
			
			cn.Open();
			wks = wkb.Worksheets["Table6-4"];

			for (c = cBeg; c <= cEnd; c++)
			{
				try
				{
					r = 25;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_FurnRely]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@FurnNo					TINYINT,
						r = 5;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@FurnNo", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@FurnFeed					CHAR (25)
						//@FurnFeedBreak			CHAR (10)				= NULL
						r = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnFeed", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnFeedBreak", SqlDbType.VarChar, 10).Value = ReturnString(rng, 10); }

						//@AnnRetubeCostMatlMT		REAL					= NULL
						//@AnnRetubeCostLaborMT		REAL					= NULL
						//@AnnRetubeCostMT			REAL					= NULL
						//@OtherMajorCostMT			REAL					= NULL
						//@TotCostMT				REAL					= NULL

						#region Down Time

						//@DTDecoke					REAL					= NULL
						r = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DTDecoke", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@DTTLEClean				REAL					= NULL
						r = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DTTLEClean", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@DTMinor					REAL					= NULL
						r = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DTMinor", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@DTMajor					REAL					= NULL
						r = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DTMajor", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@STDT						REAL					= NULL
						r = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@STDT", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@DTStandby				REAL					= NULL
						r = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DTStandby", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TotTimeOff				REAL					= NULL
						r = 15;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotTimeOff", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Availability

						//@TADownDays				REAL					= NULL
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TADownDays", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthDownDays				REAL					= NULL
						r = 19;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthDownDays", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FurnAvail				REAL					= NULL
						r = 22;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnAvail", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@AdjFurnOnstream			REAL					= NULL
						r = 23;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AdjFurnOnstream", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FeedQtyKMTA				REAL					= NULL
						r = 24;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedQtyKMTA", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FeedCapMTD				REAL					= NULL
						r = 25;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedCapMTD", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OperDays					INT						= NULL
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OperDays", SqlDbType.Int).Value = ReturnUShort(rng); }

						//@ActCap					REAL					= NULL
						r = 27;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ActCap", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@AdjFurnUtil				REAL					= NULL
						r = 29;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AdjFurnUtil", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FurnSlowD				REAL					= NULL
						r = 30;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnSlowD", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						//@YearRetubed				SMALLINT				= NULL
						r = 39;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@YearRetubed", SqlDbType.SmallInt).Value = ReturnUShort(rng); }

						#region OpEx

						//@RetubeCost				REAL					= NULL
						//r++;
						//rng = wks.Cells[r, c];
						//if (RangeHasValue(rng))
						//{
						//	FurnRelyBuffer.RetubeCost = ReturnFloat(rng);
						//}
						//else
						//{
						//	FurnRelyBuffer.RetubeCost_IsNull = true;
						//}

						//@RetubeCostMatl			REAL					= NULL
						r = 43;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@RetubeCostMatl", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@RetubeCostLabor			REAL					= NULL
						r = 44;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@RetubeCostLabor", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OtherMajorCost			REAL					= NULL
						r = 45;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OtherMajorCost", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TotCost					REAL					= NULL
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotCost", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						//@RetubeInterval			REAL					= NULL
						r = 40;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@RetubeInterval", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OnStreamDays				INT						= NULL
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng))
						{
							float v = ReturnFloat(rng);
							Int32 s = Convert.ToInt32(Math.Round(v, 0, MidpointRounding.AwayFromZero));
							cmd.Parameters.Add("@OnStreamDays", SqlDbType.Float).Value = s;
						}

						#region Energy

						//@FuelCons					REAL					= NULL
						r = 32;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelCons", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FuelType					CHAR (2)				= NULL
						r = 33;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelType", SqlDbType.VarChar, 2).Value = ReturnString(rng, 2); }

						//@StackOxy					REAL					= NULL
						r = 34;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@StackOxy", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ArchDraft				REAL					= NULL
						r = 35;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ArchDraft", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@StackTemp				REAL					= NULL
						r = 36;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@StackTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CrossTemp				REAL					= NULL
						r = 37;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CrossTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Fuel Type

						r = 33;

						//@FuelTypeYN				INT						= NULL
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng) && (ReturnString(rng, 2) == "FO" || ReturnString(rng, 2) == "FG"))
						{
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelTypeYN", SqlDbType.Int).Value = 1; }
						}
						else
						{
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelTypeYN", SqlDbType.Int).Value = 0; }
						}

						//@FuelTypeFO				INT						= NULL
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng) && ReturnString(rng, 2) == "FO")
						{
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelTypeFO", SqlDbType.Int).Value = 1; }
						}
						else
						{
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelTypeFO", SqlDbType.Int).Value = 0; }
						}

						//@FuelTypeFG				INT						= NULL
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng) && ReturnString(rng, 2) == "FG")
						{
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelTypeFG", SqlDbType.Int).Value = 1; }
						}
						else
						{
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@FuelTypeFG", SqlDbType.Int).Value = 0; }
						}

						#endregion

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFurnRely", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_FurnRely]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
