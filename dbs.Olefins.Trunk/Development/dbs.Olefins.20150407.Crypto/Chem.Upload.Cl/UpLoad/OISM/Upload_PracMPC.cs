﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracMPC(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 9;

			string sht = string.Empty;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracMPC]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum			CHAR (9),
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				#region Table9-3

				sht = "Table9-3";

				wks = wkb.Worksheets[sht];

				//@Tbl9020    TINYINT
				r = 44;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9020", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9021    TINYINT
				r = 45;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9021", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9022    TINYINT
				r = 46;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9022", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9023    TINYINT
				r = 47;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9023", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9024    TINYINT
				r = 48;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9024", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9025    TINYINT
				r = 49;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9025", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9026    TINYINT
				r = 50;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9026", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9027    TINYINT
				r = 51;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9027", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9028    TINYINT
				r = 52;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9028", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tot9020_28 TINYINT 
				r = 53;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tot9020_28", SqlDbType.TinyInt).Value = ReturnByte(rng);

				#endregion

				#region Table9-4

				sht = "Table9-4";

				wks = wkb.Worksheets[sht];

				//@Tbl9029    TINYINT
				r = 10;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9029", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9030    REAL
				r = 12;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9030", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Tbl9031    REAL
				r = 13;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9031", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Tbl9032    REAL
				r = 15;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9032", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Tbl9033    TINYINT
				r = 19;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9033", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9034    TINYINT
				r = 23;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9034", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9035    SMALLINT
				r = 28;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9035", SqlDbType.SmallInt).Value = ReturnShort(rng);

				//@Tbl9036    TINYINT
				r = 29;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9036", SqlDbType.TinyInt).Value = ReturnByte(rng);

				//@Tbl9037    SMALLINT 
				r = 30;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) cmd.Parameters.Add("@Tbl9037", SqlDbType.SmallInt).Value = ReturnShort(rng);

				#endregion

				cmd.ExecuteNonQuery();

			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracMPC", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracMPC]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
