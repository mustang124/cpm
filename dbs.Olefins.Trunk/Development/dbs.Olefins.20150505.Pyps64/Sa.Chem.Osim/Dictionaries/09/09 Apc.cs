﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Apc
		{
			internal class General
			{
				private static RangeReference Loop_Count = new RangeReference(Tabs.T09_01, 33, 6, SqlDbType.Int);
				private static RangeReference ClosedLoopAPC_Pcnt = new RangeReference(Tabs.T09_01, 37, 6, SqlDbType.Float, 100.0);

				private static RangeReference ApcMonitorId = new RangeReference(Tabs.T09_02, 9, 10, SqlDbType.VarChar);
				private static RangeReference ApcAge_Years = new RangeReference(Tabs.T09_02, 21, 9, SqlDbType.Float);
				private static RangeReference OffLineLinearPrograms_X = new RangeReference(Tabs.T09_02, 28, 10, SqlDbType.VarChar);
				private static RangeReference OffLineNonLinearSimulators_X = new RangeReference(Tabs.T09_02, 32, 10, SqlDbType.VarChar);
				private static RangeReference OffLineNonLinearInput_X = new RangeReference(Tabs.T09_02, 36, 10, SqlDbType.VarChar);
				private static RangeReference OnLineClosedLoop_X = new RangeReference(Tabs.T09_02, 44, 10, SqlDbType.VarChar);
				private static RangeReference OffLineComparison_Days = new RangeReference(Tabs.T09_02, 48, 9, SqlDbType.Float);
				private static RangeReference OnLineComparison_Days = new RangeReference(Tabs.T09_02, 50, 9, SqlDbType.Float);

				private static RangeReference ClosedLoopModelCokingPred_YN = new RangeReference(Tabs.T09_03, 6, 9, SqlDbType.VarChar);
				private static RangeReference OptimizationOffLine_Mnths = new RangeReference(Tabs.T09_03, 10, 9, SqlDbType.Float);
				private static RangeReference OptimizationPlanFreq_Mnths = new RangeReference(Tabs.T09_03, 13, 9, SqlDbType.Float);
				private static RangeReference OptimizationOnLineOper_Mnths = new RangeReference(Tabs.T09_03, 15, 9, SqlDbType.Float);
				private static RangeReference OnLineInput_Count = new RangeReference(Tabs.T09_03, 18, 9, SqlDbType.Int);
				private static RangeReference OnLineVariable_Count = new RangeReference(Tabs.T09_03, 21, 9, SqlDbType.Int);
				private static RangeReference OnLineOperation_Pcnt = new RangeReference(Tabs.T09_03, 25, 9, SqlDbType.Float, 100.0);
				private static RangeReference OnLinePointCycles_Count = new RangeReference(Tabs.T09_03, 29, 9, SqlDbType.Int);

				private static RangeReference PyroFurnModeledIndependent_YN = new RangeReference(Tabs.T09_03, 32, 9, SqlDbType.VarChar);
				private static RangeReference OnLineAutoModeSwitch_YN = new RangeReference(Tabs.T09_03, 38, 9, SqlDbType.VarChar);

				private static RangeReference DynamicResponse_Count = new RangeReference(Tabs.T09_04, 10, 9, SqlDbType.Int);
				private static RangeReference StepTestSimple_Days = new RangeReference(Tabs.T09_04, 12, 9, SqlDbType.Float);
				private static RangeReference StepTestComplex_Days = new RangeReference(Tabs.T09_04, 13, 9, SqlDbType.Float);
				private static RangeReference StepTestUpdateFreq_Mnths = new RangeReference(Tabs.T09_04, 15, 9, SqlDbType.Float);
				private static RangeReference AdaptiveAlgorithm_Count = new RangeReference(Tabs.T09_04, 19, 9, SqlDbType.Int);
				private static RangeReference EmbeddedLP_Count = new RangeReference(Tabs.T09_04, 23, 9, SqlDbType.Int);

				private static RangeReference MaxVariablesManipulated_Count = new RangeReference(Tabs.T09_04, 28, 9, SqlDbType.Int);
				private static RangeReference MaxVariablesConstraint_Count = new RangeReference(Tabs.T09_04, 29, 9, SqlDbType.Int);
				private static RangeReference MaxVariablesControlled_Count = new RangeReference(Tabs.T09_04, 30, 9, SqlDbType.Int);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_Apc]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Loop_Count", General.Loop_Count);
							d.Add("ClosedLoopAPC_Pcnt", General.ClosedLoopAPC_Pcnt);

							d.Add("ApcMonitorId", General.ApcMonitorId);
							d.Add("ApcAge_Years", General.ApcAge_Years);
							d.Add("OnLineOperation_Pcnt", General.OnLineOperation_Pcnt);
							d.Add("OffLineLinearPrograms_X", General.OffLineLinearPrograms_X);
							d.Add("OffLineNonLinearSimulators_X", General.OffLineNonLinearSimulators_X);
							d.Add("OffLineNonLinearInput_X", General.OffLineNonLinearInput_X);
							d.Add("OnLineClosedLoop_X", General.OnLineClosedLoop_X);
							d.Add("OffLineComparison_Days", General.OffLineComparison_Days);
							d.Add("OnLineComparison_Days", General.OnLineComparison_Days);


							d.Add("ClosedLoopModelCokingPred_YN", General.ClosedLoopModelCokingPred_YN);
							d.Add("OptimizationOffLine_Mnths", General.OptimizationOffLine_Mnths);
							d.Add("OptimizationPlanFreq_Mnths", General.OptimizationPlanFreq_Mnths);
							d.Add("OptimizationOnLineOper_Mnths", General.OptimizationOnLineOper_Mnths);
							d.Add("OnLineInput_Count", General.OnLineInput_Count);
							d.Add("OnLineVariable_Count", General.OnLineVariable_Count);
							d.Add("OnLinePointCycles_Count", General.OnLinePointCycles_Count);

							d.Add("PyroFurnModeledIndependent_YN", General.PyroFurnModeledIndependent_YN);
							d.Add("OnLineAutoModeSwitch_YN", General.OnLineAutoModeSwitch_YN);

							d.Add("DynamicResponse_Count", General.DynamicResponse_Count);
							d.Add("StepTestSimple_Days", General.StepTestSimple_Days);
							d.Add("StepTestComplex_Days", General.StepTestComplex_Days);
							d.Add("StepTestUpdateFreq_Mnths", General.StepTestUpdateFreq_Mnths);
							d.Add("AdaptiveAlgorithm_Count", General.AdaptiveAlgorithm_Count);
							d.Add("EmbeddedLP_Count", General.EmbeddedLP_Count);

							d.Add("MaxVariablesManipulated_Count", General.MaxVariablesManipulated_Count);
							d.Add("MaxVariablesConstraint_Count", General.MaxVariablesConstraint_Count);
							d.Add("MaxVariablesControlled_Count", General.MaxVariablesControlled_Count);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}