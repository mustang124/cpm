﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal partial class Model
			{
				internal class Plan
				{
					private static RangeReference Daily_X = new RangeReference(Tabs.T11, 0, 6, SqlDbType.VarChar);
					private static RangeReference BiWeekly_X = new RangeReference(Tabs.T11, 0, 7, SqlDbType.VarChar);
					private static RangeReference Weekly_X = new RangeReference(Tabs.T11, 0, 8, SqlDbType.VarChar);
					private static RangeReference BiMonthly_X = new RangeReference(Tabs.T11, 0, 9, SqlDbType.VarChar);
					private static RangeReference Monthly_X = new RangeReference(Tabs.T11, 0, 10, SqlDbType.VarChar);
					private static RangeReference LessMonthly_X = new RangeReference(Tabs.T11, 0, 11, SqlDbType.VarChar);

					private static RangeReference FeedPrice = new RangeReference(Tabs.T11, 139, 0, SqlDbType.VarChar);
					private static RangeReference ProductPrice = new RangeReference(Tabs.T11, 140, 0, SqlDbType.VarChar);
					private static RangeReference YieldPattern = new RangeReference(Tabs.T11, 141, 0, SqlDbType.VarChar);
					private static RangeReference CFE = new RangeReference(Tabs.T11, 142, 0, SqlDbType.VarChar);
					private static RangeReference LPConstraints = new RangeReference(Tabs.T11, 143, 0, SqlDbType.VarChar);
					private static RangeReference AFS = new RangeReference(Tabs.T11, 144, 0, SqlDbType.VarChar);
					private static RangeReference LPActPlantOper = new RangeReference(Tabs.T11, 145, 0, SqlDbType.VarChar);
					private static RangeReference LPCaseAFS = new RangeReference(Tabs.T11, 146, 0, SqlDbType.VarChar);
					private static RangeReference ProcessData = new RangeReference(Tabs.T11, 147, 0, SqlDbType.VarChar);
					private static RangeReference AcctMatlBal = new RangeReference(Tabs.T11, 148, 0, SqlDbType.VarChar);
					private static RangeReference OperPlan = new RangeReference(Tabs.T11, 149, 0, SqlDbType.VarChar);
					private static RangeReference AuditFeed = new RangeReference(Tabs.T11, 151, 0, SqlDbType.VarChar);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_FeedSelModelPlan]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return "PlanID";
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Daily_X", Plan.Daily_X);
								d.Add("BiWeekly_X", Plan.BiWeekly_X);
								d.Add("Weekly_X", Plan.Weekly_X);
								d.Add("BiMonthly_X", Plan.BiMonthly_X);
								d.Add("Monthly_X", Plan.Monthly_X);
								d.Add("LessMonthly_X", Plan.LessMonthly_X);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("FeedPrice", Plan.FeedPrice);
								d.Add("ProductPrice", Plan.ProductPrice);
								d.Add("YieldPattern", Plan.YieldPattern);
								d.Add("CFE", Plan.CFE);
								d.Add("LPConstraints", Plan.LPConstraints);
								d.Add("AFS", Plan.AFS);
								d.Add("LPActPlantOper", Plan.LPActPlantOper);
								d.Add("LPCaseAFS", Plan.LPCaseAFS);
								d.Add("ProcessData", Plan.ProcessData);
								d.Add("AcctMatlBal", Plan.AcctMatlBal);
								d.Add("OperPlan", Plan.OperPlan);
								d.Add("AuditFeed", Plan.AuditFeed);

								return d;
							}
						}
					}
				}
			}
		}
	}
}