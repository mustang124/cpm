﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal class OpEx
		{
			private static RangeReference OccSal = new RangeReference(Tabs.T04, 10, 0);
			private static RangeReference MpsSal = new RangeReference(Tabs.T04, 11, 0);
			private static RangeReference OccBen = new RangeReference(Tabs.T04, 13, 0);
			private static RangeReference MpsBen = new RangeReference(Tabs.T04, 14, 0);

			private static RangeReference MaintMatl = new RangeReference(Tabs.T04, 15, 0);
			private static RangeReference MaintContractLabor = new RangeReference(Tabs.T04, 16, 0);
			private static RangeReference MaintContractMatl = new RangeReference(Tabs.T04, 17, 0);
			private static RangeReference MaintEquip = new RangeReference(Tabs.T04, 18, 0);
			private static RangeReference ContractTechSvc = new RangeReference(Tabs.T04, 19, 0);
			private static RangeReference Envir = new RangeReference(Tabs.T04, 20, 0);
			private static RangeReference ContractServOth = new RangeReference(Tabs.T04, 21, 0);
			private static RangeReference NonMaintEquipRent = new RangeReference(Tabs.T04, 22, 0);
			private static RangeReference Tax = new RangeReference(Tabs.T04, 23, 0);
			private static RangeReference Insur = new RangeReference(Tabs.T04, 24, 0);

			private static RangeReference OthNonVol = new RangeReference(Tabs.T04, 25, 0);
			private static RangeReference NVE1 = new RangeReference(Tabs.T04, 26, 0);
			private static RangeReference NVE2 = new RangeReference(Tabs.T04, 27, 0);
			private static RangeReference NVE3 = new RangeReference(Tabs.T04, 28, 0);
			private static RangeReference NVE4 = new RangeReference(Tabs.T04, 29, 0);
			private static RangeReference NVE5 = new RangeReference(Tabs.T04, 30, 0);
			private static RangeReference NVE6 = new RangeReference(Tabs.T04, 31, 0);
			private static RangeReference NVE7 = new RangeReference(Tabs.T04, 32, 0);
			private static RangeReference NVE8 = new RangeReference(Tabs.T04, 33, 0);
			private static RangeReference NVE9 = new RangeReference(Tabs.T04, 34, 0);

			private static RangeReference STNonVol = new RangeReference(Tabs.T04, 35, 0);

			private static RangeReference TaAccrual = new RangeReference(Tabs.T04, 37, 0);
			private static RangeReference TaCurrExp = new RangeReference(Tabs.T04, 38, 0);
			private static RangeReference STTaExp = new RangeReference(Tabs.T04, 39, 0);

			private static RangeReference Chemicals = new RangeReference(Tabs.T04, 41, 0);
			private static RangeReference ChemBFWCW = new RangeReference(Tabs.T04, 42, 0);
			private static RangeReference ChemWW = new RangeReference(Tabs.T04, 43, 0);
			private static RangeReference ChemCaustic = new RangeReference(Tabs.T04, 44, 0);
			private static RangeReference ChemPretreatment = new RangeReference(Tabs.T04, 45, 0);
			private static RangeReference ChemAntiFoul = new RangeReference(Tabs.T04, 46, 0);
			private static RangeReference ChemOth = new RangeReference(Tabs.T04, 47, 0);
			private static RangeReference ChemAdd = new RangeReference(Tabs.T04, 48, 0);
			private static RangeReference ChemOthProc = new RangeReference(Tabs.T04, 49, 0);

			private static RangeReference Catalysts = new RangeReference(Tabs.T04, 50, 0);
			private static RangeReference Royalties = new RangeReference(Tabs.T04, 51, 0);

			private static RangeReference PurElec = new RangeReference(Tabs.T04, 53, 0);
			private static RangeReference PurSteam = new RangeReference(Tabs.T04, 54, 0);
			private static RangeReference PurCoolingWater = new RangeReference(Tabs.T04, 55, 0);
			private static RangeReference PurOth = new RangeReference(Tabs.T04, 56, 0);

			private static RangeReference PurFuelGas = new RangeReference(Tabs.T04, 58, 0);
			private static RangeReference PurLiquid = new RangeReference(Tabs.T04, 59, 0);
			private static RangeReference PurSolid = new RangeReference(Tabs.T04, 60, 0);

			private static RangeReference PPFCFuelGas = new RangeReference(Tabs.T04, 62, 0);
			private static RangeReference PPFCOther = new RangeReference(Tabs.T04, 63, 0);

			private static RangeReference ExportSteam = new RangeReference(Tabs.T04, 65, 0);
			private static RangeReference ExportElectric = new RangeReference(Tabs.T04, 66, 0);

			private static RangeReference OthVol = new RangeReference(Tabs.T04, 67, 0);
			private static RangeReference VE1 = new RangeReference(Tabs.T04, 68, 0);
			private static RangeReference VE2 = new RangeReference(Tabs.T04, 69, 0);
			private static RangeReference VE3 = new RangeReference(Tabs.T04, 70, 0);
			private static RangeReference VE4 = new RangeReference(Tabs.T04, 71, 0);
			private static RangeReference VE5 = new RangeReference(Tabs.T04, 72, 0);
			private static RangeReference VE6 = new RangeReference(Tabs.T04, 73, 0);
			private static RangeReference VE7 = new RangeReference(Tabs.T04, 74, 0);
			private static RangeReference VE8 = new RangeReference(Tabs.T04, 75, 0);
			private static RangeReference VE9 = new RangeReference(Tabs.T04, 76, 0);

			private static RangeReference STVol = new RangeReference(Tabs.T04, 77, 0);
			private static RangeReference TotCashOpEx = new RangeReference(Tabs.T04, 78, 0);
			private static RangeReference GANonPers = new RangeReference(Tabs.T04, 80, 0);
			private static RangeReference TotRefExp = new RangeReference(Tabs.T04, 81, 0);

			private static RangeReference ForexRate = new RangeReference(Tabs.T04, 83, 0);

			private static RangeReference colDescription = new RangeReference(Tabs.T04, 0, 3, SqlDbType.VarChar);
			private static RangeReference colAmountReported = new RangeReference(Tabs.T04, 0, 6, SqlDbType.Float);
			private static RangeReference colAmountUsd = new RangeReference(Tabs.T04, 0, 5, SqlDbType.Float);

			internal class Download : TransferData.IDownload
			{
				public string StoredProcedure
				{
					get
					{
						return "[fact].[Select_OpEx]";
					}
				}

				public string LookUpColumn
				{
					get
					{
						return "AccountId";
					}
				}

				public Dictionary<string, RangeReference> Columns
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("AccountName", OpEx.colDescription);
						d.Add("Amount_Cur", OpEx.colAmountReported);

						return d;
					}
				}

				public Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("OccSal", OpEx.OccSal);
						d.Add("MpsSal", OpEx.MpsSal);
						d.Add("OccBen", OpEx.OccBen);
						d.Add("MpsBen", OpEx.MpsBen);

						d.Add("MaintMatl", OpEx.MaintMatl);
						d.Add("MaintContractLabor", OpEx.MaintContractLabor);
						d.Add("MaintContractMatl", OpEx.MaintContractMatl);
						d.Add("MaintEquip", OpEx.MaintEquip);
						d.Add("ContractTechSvc", OpEx.ContractTechSvc);
						d.Add("Envir", OpEx.Envir);
						d.Add("ContractServOth", OpEx.ContractServOth);
						d.Add("NonMaintEquipRent", OpEx.NonMaintEquipRent);
						d.Add("Tax", OpEx.Tax);
						d.Add("Insur", OpEx.Insur);

						d.Add("NVE1", OpEx.NVE1);
						d.Add("NVE2", OpEx.NVE2);
						d.Add("NVE3", OpEx.NVE3);
						d.Add("NVE4", OpEx.NVE4);
						d.Add("NVE5", OpEx.NVE5);
						d.Add("NVE6", OpEx.NVE6);
						d.Add("NVE7", OpEx.NVE7);
						d.Add("NVE8", OpEx.NVE8);
						d.Add("NVE9", OpEx.NVE9);

						d.Add("TaAccrual", OpEx.TaAccrual);
						d.Add("TaCurrExp", OpEx.TaCurrExp);

						d.Add("ChemBFWCW", OpEx.ChemBFWCW);
						d.Add("ChemWW", OpEx.ChemWW);
						d.Add("ChemCaustic", OpEx.ChemCaustic);
						d.Add("ChemPretreatment", OpEx.ChemPretreatment);
						d.Add("ChemAntiFoul", OpEx.ChemAntiFoul);
						d.Add("ChemOth", OpEx.ChemOth);
						d.Add("ChemAdd", OpEx.ChemAdd);
						d.Add("ChemOthProc", OpEx.ChemOthProc);

						d.Add("Catalysts", OpEx.Catalysts);
						d.Add("Royalties", OpEx.Royalties);

						d.Add("PurElec", OpEx.PurElec);
						d.Add("PurSteam", OpEx.PurSteam);
						d.Add("PurCoolingWater", OpEx.PurCoolingWater);
						d.Add("PurOth", OpEx.PurOth);

						d.Add("PurFuelGas", OpEx.PurFuelGas);
						d.Add("PurLiquid", OpEx.PurLiquid);
						d.Add("PurSolid", OpEx.PurSolid);

						d.Add("PPFCFuelGas", OpEx.PPFCFuelGas);
						d.Add("PPFCOther", OpEx.PPFCOther);

						d.Add("ExportSteam", OpEx.ExportSteam);
						d.Add("ExportElectric", OpEx.ExportElectric);

						d.Add("VE1", OpEx.VE1);
						d.Add("VE2", OpEx.VE2);
						d.Add("VE3", OpEx.VE3);
						d.Add("VE4", OpEx.VE4);
						d.Add("VE5", OpEx.VE5);
						d.Add("VE6", OpEx.VE6);
						d.Add("VE7", OpEx.VE7);
						d.Add("VE8", OpEx.VE8);
						d.Add("VE9", OpEx.VE9);

						d.Add("GANonPers", OpEx.GANonPers);

						return d;
					}
				}
			}

			internal class Upload : TransferData.IUploadMultiple
			{
				public string StoredProcedure
				{
					get
					{
						return "[stgFact].[Insert_OpEx]";
					}
				}

				public Dictionary<string, RangeReference> Parameters
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("@OCCSal", OpEx.OccSal);
						d.Add("@MPSSal", OpEx.MpsSal);
						d.Add("@OCCBen", OpEx.OccBen);
						d.Add("@MPSBen", OpEx.MpsBen);

						d.Add("@MaintMatl",OpEx.MaintMatl);
						d.Add("@ContMaintLabor",OpEx.MaintContractLabor);
						d.Add("@ContMaintMatl",OpEx.MaintContractMatl);
						d.Add("@ContTechSvc",OpEx.ContractTechSvc);
						d.Add("@Envir",OpEx.Envir);
						d.Add("@OthCont",OpEx.ContractServOth);
						d.Add("@Equip",OpEx.MaintEquip);
						d.Add("@Tax",OpEx.Tax);
						d.Add("@Insur",OpEx.Insur);

						d.Add("@OthNonVol",OpEx.OthNonVol);

						d.Add("@STNonVol",OpEx.STNonVol);

						d.Add("@TAAccrual",OpEx.TaAccrual);
						d.Add("@TACurrExp",OpEx.TaCurrExp);

						d.Add("@STTaExp",OpEx.STTaExp);

						d.Add("@Chemicals",OpEx.Chemicals);
						d.Add("@Catalysts",OpEx.Catalysts);
						d.Add("@Royalties",OpEx.Royalties);

						d.Add("@PurElec",OpEx.PurElec);
						d.Add("@PurSteam",OpEx.PurSteam);
						d.Add("@PurOth",OpEx.PurOth);
						d.Add("@PurCoolingWater",OpEx.PurCoolingWater);
						d.Add("@PurFG",OpEx.PurFuelGas);
						d.Add("@PurLiquid",OpEx.PurLiquid);
						d.Add("@PurSolid",OpEx.PurSolid);

						d.Add("@PPCFuelGas",OpEx.PPFCFuelGas);
						d.Add("@PPCFuelOth",OpEx.PPFCOther);

						d.Add("@SteamExports",OpEx.ExportSteam);
						d.Add("@PowerExports",OpEx.ExportElectric);

						d.Add("@OthVol",OpEx.OthVol);
						d.Add("@STVol",OpEx.STVol);

						d.Add("@TotCashOpEx",OpEx.TotCashOpEx);
						d.Add("@GANonPers",OpEx.GANonPers);
						d.Add("@TotRefExp",OpEx.TotRefExp);

						d.Add("@ForexRate", OpEx.ForexRate);

						d.Add("@NonMaintEquipRent",OpEx.NonMaintEquipRent);
						d.Add("@ChemBFWCW",OpEx.ChemBFWCW);
						d.Add("@ChemWW",OpEx.ChemWW);
						d.Add("@ChemCaustic",OpEx.ChemCaustic);
						d.Add("@ChemPretreatment",OpEx.ChemPretreatment);
						d.Add("@ChemAntiFoul",OpEx.ChemAntiFoul);
						d.Add("@ChemOthProc",OpEx.ChemOth);
						d.Add("@ChemAdd",OpEx.ChemAdd);
						d.Add("@ChemOth",OpEx.ChemOthProc);

						return d;
					}
				}

				public Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("amt", OpEx.colAmountUsd);

						return d;
					}
				}
			}
		}
	}
}