﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Misc
		{
			private static RangeReference EthPcntTot = new RangeReference(Tabs.T02_C, 44, 3, SqlDbType.Float);
			private static RangeReference ProPcntTot = new RangeReference(Tabs.T02_C, 45, 3, SqlDbType.Float);
			private static RangeReference ButPcntTot = new RangeReference(Tabs.T02_C, 46, 3, SqlDbType.Float);

			private static RangeReference OCCAbsence = new RangeReference(Tabs.T05_A, 42, 7, SqlDbType.Float);
			private static RangeReference MpsAbsence = new RangeReference(Tabs.T05_B, 44, 7, SqlDbType.Float);

			//private static RangeReference SplFeedEnergy = new RangeReference(string.Empty, 0, 0);

			private static RangeReference LtFeedPriceBasis = new RangeReference(Tabs.T02_A1, 47, 8, SqlDbType.Float);

			private static RangeReference OthLiqFeedName1 = new RangeReference(Tabs.T02_A2, 4, 12, SqlDbType.VarChar);
			private static RangeReference OthLiqFeedName2 = new RangeReference(Tabs.T02_A2, 4, 13, SqlDbType.VarChar);
			private static RangeReference OthLiqFeedName3 = new RangeReference(Tabs.T02_A2, 4, 14, SqlDbType.VarChar);

			private static RangeReference OthLiqFeedPriceBasis1 = new RangeReference(Tabs.T02_A2, 47, 12, SqlDbType.Float);
			private static RangeReference OthLiqFeedPriceBasis2 = new RangeReference(Tabs.T02_A2, 47, 13, SqlDbType.Float);
			private static RangeReference OthLiqFeedPriceBasis3 = new RangeReference(Tabs.T02_A2, 47, 14, SqlDbType.Float);

			//private static RangeReference TotCompOutage = new RangeReference(string.Empty, 0, 0);

			private static RangeReference PctFurnRelyLimit = new RangeReference(Tabs.T06_04, 48, 3, SqlDbType.Float);

			private static RangeReference ROG_CGCI = new RangeReference(Tabs.T02_C, 53, 3, SqlDbType.VarChar);
			private static RangeReference ROG_CGCD = new RangeReference(Tabs.T02_C, 54, 3, SqlDbType.VarChar);
			private static RangeReference ROG_C2RSF = new RangeReference(Tabs.T02_C, 55, 3, SqlDbType.VarChar);
			private static RangeReference ROG_C3RSF = new RangeReference(Tabs.T02_C, 56, 3, SqlDbType.VarChar);

			internal class Upload : TransferData.IUpload
			{
				public string StoredProcedure
				{
					get
					{
						return "[stgFact].[Insert_Misc]";
					}
				}

				public Dictionary<string, RangeReference> Parameters
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("@EthPcntTot", EthPcntTot);
						d.Add("@ProPcntTot", ProPcntTot);
						d.Add("@ButPcntTot", ButPcntTot);
						d.Add("@OCCAbsence", OCCAbsence);
						d.Add("@MpsAbsence", MpsAbsence);
						//d.Add("@SplFeedEnergy", SplFeedEnergy);
						d.Add("@LtFeedPriceBasis", LtFeedPriceBasis);
						d.Add("@OthLiqFeedName1", OthLiqFeedName1);
						d.Add("@OthLiqFeedPriceBasis1", OthLiqFeedPriceBasis1);
						d.Add("@OthLiqFeedName2", OthLiqFeedName2);
						d.Add("@OthLiqFeedPriceBasis2", OthLiqFeedPriceBasis2);
						d.Add("@OthLiqFeedName3", OthLiqFeedName3);
						d.Add("@OthLiqFeedPriceBasis3", OthLiqFeedPriceBasis3);
						//d.Add("@TotCompOutage", TotCompOutage);
						d.Add("@PctFurnRelyLimit", PctFurnRelyLimit);
						d.Add("@ROG_CGCI", ROG_CGCI);
						d.Add("@ROG_CGCD", ROG_CGCD);
						d.Add("@ROG_C2RSF", ROG_C2RSF);
						d.Add("@ROG_C3RSF", ROG_C3RSF);

						return d;
					}
				}
			}
		}
	}
}