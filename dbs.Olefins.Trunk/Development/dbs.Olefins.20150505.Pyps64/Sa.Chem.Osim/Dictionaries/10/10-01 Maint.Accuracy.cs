﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Maintenance
		{
			internal class Accuracy
			{
				private static RangeReference AccuracyID = new RangeReference(Tabs.T10_01, 63, 4, SqlDbType.VarChar);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MaintAccuracy]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("AccuracyID", Accuracy.AccuracyID);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}