﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Supp
			{
				internal class Composition
				{
					internal class Other
					{
						private static RangeReference SuppOtherFeedProdId = new RangeReference(Tabs.Upload, Tabs.T02_C, 38, 2, SqlDbType.VarChar);

						private static RangeReference SuppOtherComposition = new RangeReference(Tabs.T02_C, 0, 3, SqlDbType.Float, 100.0);

						private static RangeReference H2 = new RangeReference(Tabs.T02_C, 59, 0, SqlDbType.Float, 100.0);
						private static RangeReference CH4 = new RangeReference(Tabs.T02_C, 60, 0, SqlDbType.Float, 100.0);
						private static RangeReference C2H2 = new RangeReference(Tabs.T02_C, 61, 0, SqlDbType.Float, 100.0);
						private static RangeReference C2H4 = new RangeReference(Tabs.T02_C, 62, 0, SqlDbType.Float, 100.0);
						private static RangeReference C2H6 = new RangeReference(Tabs.T02_C, 63, 0, SqlDbType.Float, 100.0);
						private static RangeReference C3H6 = new RangeReference(Tabs.T02_C, 64, 0, SqlDbType.Float, 100.0);
						private static RangeReference C3H8 = new RangeReference(Tabs.T02_C, 65, 0, SqlDbType.Float, 100.0);
						private static RangeReference C4H6 = new RangeReference(Tabs.T02_C, 66, 0, SqlDbType.Float, 100.0);
						private static RangeReference C4H8 = new RangeReference(Tabs.T02_C, 67, 0, SqlDbType.Float, 100.0);
						private static RangeReference C4H10 = new RangeReference(Tabs.T02_C, 68, 0, SqlDbType.Float, 100.0);
						private static RangeReference C6H6 = new RangeReference(Tabs.T02_C, 69, 0, SqlDbType.Float, 100.0);
						private static RangeReference PyroGasoline = new RangeReference(Tabs.T02_C, 70, 0, SqlDbType.Float, 100.0);
						private static RangeReference PyroFuelOil = new RangeReference(Tabs.T02_C, 71, 0, SqlDbType.Float, 100.0);
						private static RangeReference Inerts = new RangeReference(Tabs.T02_C, 72, 0, SqlDbType.Float, 100.0);

						internal class Download : TransferData.IDownload
						{
							public string StoredProcedure
							{
								get
								{
									return "[fact].[Select_StreamsSuppOtherComposition]";
								}
							}

							public string LookUpColumn
							{
								get
								{
									return "StreamId";
								}
							}

							public Dictionary<string, RangeReference> Columns
							{
								get
								{
									Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

									d.Add("H2", Other.H2);
									d.Add("CH4", Other.CH4);
									d.Add("C2H2", Other.C2H2);
									d.Add("C2H4", Other.C2H4);
									d.Add("C2H6", Other.C2H6);
									d.Add("C3H6", Other.C3H6);
									d.Add("C3H8", Other.C3H8);
									d.Add("C4H6", Other.C4H6);
									d.Add("C4H8", Other.C4H8);
									d.Add("C4H10", Other.C4H10);
									d.Add("C6H6", Other.C6H6);
									d.Add("PyroGasoline", Other.PyroGasoline);
									d.Add("PyroFuelOil", Other.PyroFuelOil);
									d.Add("Inerts", Other.Inerts);

									return d;
								}
							}

							public Dictionary<string, RangeReference> Items
							{
								get
								{
									Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

									d.Add("SuppOther", Other.SuppOtherComposition);

									return d;
								}
							}
						}

						internal class Upload : TransferData.IUploadMultiple
						{
							public string StoredProcedure
							{
								get
								{
									return "[stgFact].[Insert_Composition]";
								}
							}

							public Dictionary<string, RangeReference> Items
							{
								get
								{
									Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

									d.Add("SuppOther", Other.SuppOtherComposition);

									return d;
								}
							}

							public Dictionary<string, RangeReference> Parameters
							{
								get
								{
									Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

									d.Add("@FeedProdID", Other.SuppOtherFeedProdId);

									d.Add("@H2", Other.H2);
									d.Add("@CH4", Other.CH4);
									d.Add("@C2H2", Other.C2H2);
									d.Add("@C2H4", Other.C2H4);
									d.Add("@C2H6", Other.C2H6);
									d.Add("@C3H6", Other.C3H6);
									d.Add("@C3H8", Other.C3H8);
									d.Add("@BUTAD", Other.C4H6);
									d.Add("@C4S", Other.C4H8);
									d.Add("@C4H10", Other.C4H10);
									d.Add("@BZ", Other.C6H6);
									d.Add("@PYGAS", Other.PyroGasoline);
									d.Add("@PYOIL", Other.PyroFuelOil);
									d.Add("@INERT", Other.Inerts);

									return d;
								}
							}
						}
					}
				}
			}
		}
	}
}