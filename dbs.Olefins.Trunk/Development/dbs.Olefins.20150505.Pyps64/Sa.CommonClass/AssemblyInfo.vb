﻿Imports System.Reflection

''' <summary>
''' Get information about the program to return to the user interface.
''' </summary>
''' <remarks>
''' <para>
''' Copyright © 2012 HSB Solomon Associates LLC
''' Solomon Associates
''' Two Galleria Tower, Suite 1500
''' 13455 Noel Road
''' Dallas, Texas 75240, United States of America
''' </para>
''' <para>The information retrieved by this class is set and changed in 'My Project'.</para>
''' <para>Most of the attributes are stored in Assembly Information.</para>
''' </remarks>
Public Module AssemblyInfo

    ''' <summary>
    ''' Specify the assembly that requests this information.
    ''' </summary>
    ''' <remarks>
    ''' <para>
    ''' Returns the initializing assembly.  The value for production is Assembly.GetEntryAssembly.  Also see, Assembly.GetExecutingAssembly.
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assembly.getentryassembly.aspx">
    ''' Assembly.GetEntryAssembly Method: http://msdn.microsoft.com/en-us/library/system.reflection.assembly.getentryassembly.aspx </a>
    ''' </para>
    ''' </remarks> 
#If DEBUG Then
    Private ai As Assembly = Assembly.GetExecutingAssembly
#Else
    Private ai As Assembly = Assembly.GetEntryAssembly
#End If

    ''' <summary>
    ''' Gets the application title.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblytitleattribute.aspx">
    ''' AssemblyTitleAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblytitleattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property Title() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(AssemblyTitleAttribute), False)(0), AssemblyTitleAttribute).Title.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the application description.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblydescriptionattribute.aspx">
    ''' AssemblyDescriptionAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblydescriptionattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property Description() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(AssemblyDescriptionAttribute), False)(0), AssemblyDescriptionAttribute).Description.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the company name.
    ''' </summary>
    ''' <value>The company name in Assembly Information.</value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>Typical result is "HSB Solomon Associates LLC".</para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblycompanyattribute.aspx">
    ''' AssemblyCompanyAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblycompanyattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property Company() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(AssemblyCompanyAttribute), False)(0), AssemblyCompanyAttribute).Company.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the product name.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblyproductattribute.aspx">
    ''' AssemblyProductAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblyproductattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property Product() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(AssemblyProductAttribute), False)(0), AssemblyProductAttribute).Product.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the copyright notice in Assembly Information.
    ''' </summary>
    ''' <value>The copyright notice in Assembly Information.</value>
    ''' <returns>String</returns>
    ''' <remarks><para>Typical result is "© 2011 HSB Solomon Associates LLC".</para>
    ''' <list type="table">
    ''' <item>
    ''' <term>Symbol</term>
    ''' <description>ASCII</description>
    ''' </item>
    ''' <item>
    ''' <term>©</term>
    ''' <description>169</description>
    ''' </item>
    ''' </list>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblycopyrightattribute.aspx">
    ''' AssemblyCopyrightAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblycopyrightattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property Copyright() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(AssemblyCopyrightAttribute), False)(0), AssemblyCopyrightAttribute).Copyright.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the trademark notice.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>Typical result is "® 2011 HSB Solomon Associates LLC".
    ''' <list type="table">
    ''' <item>
    ''' <term>Symbol</term>
    ''' <description>ASCII</description>
    ''' </item>
    ''' <item>
    ''' <term>®</term>
    ''' <description>174 (Registered Trademark)</description>
    ''' </item>
    ''' <item>
    ''' <term>™</term>
    ''' <description>153</description>
    ''' </item>
    ''' </list>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblytrademarkattribute.aspx">
    ''' AssemblyTrademarkAttribute Class http://msdn.microsoft.com/en-us/library/system.reflection.assemblytrademarkattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property Trademark() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(AssemblyTrademarkAttribute), False)(0), AssemblyTrademarkAttribute).Trademark.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the version number.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblyversionattribute.aspx">
    ''' AssemblyVersionAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblyversionattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property Version() As String
        Get
            Return ai.GetName.Version.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Returns the application  title and version.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblyproductattribute.aspx">
    ''' AssemblyProductAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblyproductattribute.aspx </a>
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblyversionattribute.aspx">
    ''' AssemblyVersionAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblyversionattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property ProductAndVersion() As String
        Get

            Dim t As String = CType(ai.GetCustomAttributes(GetType(AssemblyProductAttribute), False)(0), AssemblyProductAttribute).Product.ToString()
            Dim v As String = ai.GetName.Version.ToString()

            If v <> String.Empty Then v = " (" + v + ")"

            Return t + v

        End Get
    End Property

    ''' <summary>
    ''' Gets the file version number.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' <para>
    ''' Specifies a version number that instructs the compiler to use a specific version for the Win32 file version resource.
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.reflection.assemblyfileversionattribute.aspx">
    ''' AssemblyFileVersionAttribute Class: http://msdn.microsoft.com/en-us/library/system.reflection.assemblyfileversionattribute.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property FileVersion() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(AssemblyFileVersionAttribute), False)(0), AssemblyFileVersionAttribute).Version.ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the programs GUID; the GUID uniquely identifies the application.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property AssemblyGuid() As String
        Get
            Return CType(ai.GetCustomAttributes(GetType(Runtime.InteropServices.GuidAttribute), True)(0), Runtime.InteropServices.GuidAttribute).ToString()
        End Get
    End Property

    ''' <summary>
    ''' Gets the path to the programs executable.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' Specifies a version number that instructs the compiler to use a specific version for the Win32 file version resource.
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.fileio.filesystem.getparentpath.aspx">
    ''' FileSystem.GetParentPath Method: http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.fileio.filesystem.getparentpath.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property PathApplication() As String
        Get
            Return FileIO.FileSystem.GetParentPath(ai.Location.ToString()) + "\"
        End Get
    End Property

    ''' <summary>
    ''' Gets the host system root path.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.io.directory.getdirectoryroot.aspx">
    ''' Directory.GetDirectoryRoot Method: http://msdn.microsoft.com/en-us/library/system.io.directory.getdirectoryroot.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property PathRoot As String
        Get
            Return System.IO.Directory.GetDirectoryRoot(ai.Location.ToString()) + "\"
        End Get
    End Property

    ''' <summary>
    ''' Gets the host system root drive.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.io.directory.getdirectoryroot.aspx">
    ''' Directory.GetDirectoryRoot Method: http://msdn.microsoft.com/en-us/library/system.io.directory.getdirectoryroot.aspx </a>
    ''' </para>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.environment.systemdirectory.aspx">
    ''' Environment.SystemDirectory Property: http://msdn.microsoft.com/en-us/library/system.environment.systemdirectory.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property SystemDrive As String
        Get
            Return System.IO.Directory.GetDirectoryRoot(System.Environment.SystemDirectory.ToString())
        End Get
    End Property

    ''' <summary>
    ''' Gets the host system temporary directory.
    ''' </summary>
    ''' <value></value>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.io.path.gettemppath.aspx">
    ''' Path.GetTempPath Method: http://msdn.microsoft.com/en-us/library/system.io.path.gettemppath.aspx </a>
    ''' </para>
    ''' </remarks>
    Public ReadOnly Property TempDirectory As String
        Get
            Return System.IO.Path.GetTempPath
        End Get
    End Property

    Public ReadOnly Property Caller As String
        Get
            Return CType(ai.HostContext, String)
        End Get
    End Property

End Module
