﻿CREATE VIEW [gap].[AnalysisResultsAggregate]
WITH SCHEMABINDING
AS
SELECT
	t.[FactorSetId],
	t.[GroupId],
	t.[TargetId],
	t.[GroupProdCap_kMT],
	t.[BenchProdCap_kMT],
	b.[GapId],
	t.[CurrencyId],
	[GapAmount_Cur]	=
		SUM(CASE b.[DescendantOperator]
			WHEN '+' THEN + t.[GapAmount_Cur]
			WHEN '-' THEN - t.[GapAmount_Cur]
			END),
	[GapAmount_Div]	=
		SUM(CASE b.[DescendantOperator]
			WHEN '+' THEN + t.[GapAmount_Div]
			WHEN '-' THEN - t.[GapAmount_Div]
			END)
FROM [gap].[AnalysisResultsDiv]					t
INNER JOIN [dim].[Gap_Bridge]					b
	ON	b.[FactorSetId]		= t.[FactorSetId]
	AND	b.[DescendantId]	= t.[GapId]
GROUP BY
	t.[FactorSetId],
	t.[GroupId],
	t.[TargetId],
	t.[GroupProdCap_kMT],
	t.[BenchProdCap_kMT],
	b.[GapId],
	t.[CurrencyId];