﻿CREATE TABLE [dim].[FeedStockDistillation_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [DistillationId]     VARCHAR (4)         NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (4)         NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_FeedStockDistillation_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedStockDistillation_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistillation_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistillation_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistillation_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedStockDistillation_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [DistillationId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_FeedStockDistillation_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_FeedStockDistillation_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[FeedStockDistillation_LookUp] ([DistillationId]),
    CONSTRAINT [FK_FeedStockDistillation_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_FeedStockDistillation_Bridge_LookUp_FeedStockDistillation] FOREIGN KEY ([DistillationId]) REFERENCES [dim].[FeedStockDistillation_LookUp] ([DistillationId]),
    CONSTRAINT [FK_FeedStockDistillation_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [DistillationId]) REFERENCES [dim].[FeedStockDistillation_Parent] ([FactorSetId], [DistillationId]),
    CONSTRAINT [FK_FeedStockDistillation_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[FeedStockDistillation_Parent] ([FactorSetId], [DistillationId])
);


GO

CREATE TRIGGER [dim].[t_FeedStockDistillation_Bridge_u]
ON [dim].[FeedStockDistillation_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedStockDistillation_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedStockDistillation_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[FeedStockDistillation_Bridge].[DistillationId]	= INSERTED.[DistillationId]
		AND	[dim].[FeedStockDistillation_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;