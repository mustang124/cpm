﻿CREATE TABLE [dim].[Pers_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [PersId]         VARCHAR (34)        NOT NULL,
    [ParentId]       VARCHAR (34)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Pers_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Pers_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Pers_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Pers_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Pers_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Pers_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PersId] ASC),
    CONSTRAINT [CR_Pers_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Pers_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Pers_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_Pers_Parent_LookUp_Pers] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_Pers_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Pers_Parent] ([FactorSetId], [PersId])
);


GO

CREATE TRIGGER [dim].[t_Pers_Parent_u]
ON [dim].[Pers_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Pers_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Pers_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Pers_Parent].[PersId]	= INSERTED.[PersId];

END;