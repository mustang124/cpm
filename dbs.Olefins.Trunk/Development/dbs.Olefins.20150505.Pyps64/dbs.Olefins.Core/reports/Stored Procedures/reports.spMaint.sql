﻿




CREATE              PROC [reports].[spMaint](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint, @Currency as varchar(5)
SELECT @Currency = 'USD'
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup


DELETE FROM reports.Maint WHERE GroupId = @GroupId
Insert into reports.Maint (GroupId
, Currency
, RoutIndexEDC
, TAIndexEDC
, MaintIndexEDC
, RoutIndexRV
, TAIndexRV
, MaintIndexRV
, RoutMEI
, TAMEI
, MEI
, RelInd
, MechAvail
, OpAvail
, PlantAvail
, MechAvailSlow
, OpAvailSlow
, PlantAvailSlow
, OthMechUnAvail
, RMeiEDC
, RMeiRV
, MaintOCCSWBPerkEDC
, MaintMpsSWBPerkEDC
, MaintContractLaborPerkEDC
, MaintTotMatlPerkEDC
, MaintEquipPerkEDC
, MaintRoutMaintPerkEDC
, MaintContractMatlPerkEDC
, MaintMatlPerkEDC
, MaintOCCSWBPerHVChem
, MaintMpsSWBPerHVChem
, MaintContractLaborPerHVChem
, MaintTotMatlPerHVChem
, MaintEquipPerHVChem
, MaintRoutMaintPerHVChem
, MaintContractMatlPerHVChem
, MaintMatlPerHVChem
, MaintOCCSWBPerMEI
, MaintMpsSWBPerMEI
, MaintContractLaborPerMEI
, MaintTotMatlPerMEI
, MaintEquipPerMEI
, MaintRoutMaintPerMEI
, MaintContractMatlPerMEI
, MaintMatlPerMEI
, MaintCorrEmergencyPcnt
, MainCorrProcessPcnt
, MaintPrevCondPcnt
, MaintPrevCondAddPcnt
, MaintPrevSysPcnt
, MaintPrevSysAddPcnt
, FurnRunDaysEthane
, FurnRunDaysPropane
, FurnRunDaysNaphtha
, FurnRunDaysLiqHeavy
, FurnTubeLifeYrs
, FurnRetubeDays
)
Select @GroupId
, @Currency 
, RoutIndexEDC = [$(DbGlobal)].dbo.WtAvgNN(m.RoutIndexEDC, d.kEdc)
, TAIndexEDC = [$(DbGlobal)].dbo.WtAvgNN(m.TAIndexEDC, d.kEdc)
, MaintIndexEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintIndexEDC, d.kEdc)
, RoutIndexRV = [$(DbGlobal)].dbo.WtAvgNN(m.RoutIndexRV, d.RV_MUS)
, TAIndexRV = [$(DbGlobal)].dbo.WtAvgNN(m.TAIndexRV, d.RV_MUS)
, MaintIndexRV = [$(DbGlobal)].dbo.WtAvgNN(m.MaintIndexRV, d.RV_MUS)
, RoutMEI = [$(DbGlobal)].dbo.WtAvgNN(m.RoutMEI, d.MEIStd)
, TAMEI = [$(DbGlobal)].dbo.WtAvgNN(m.TAMEI, d.MEIStd)
, MEI = [$(DbGlobal)].dbo.WtAvgNN(m.MEI, d.MEIStd)
, RelInd = [$(DbGlobal)].dbo.WtAvgNN(m.RelInd, d.OlefinsProd_kMT)
, MechAvail = [$(DbGlobal)].dbo.WtAvgNN(m.MechAvail, d.kEdc)
, OpAvail = [$(DbGlobal)].dbo.WtAvgNN(m.OpAvail, d.kEdc)
, PlantAvail = [$(DbGlobal)].dbo.WtAvgNN(m.PlantAvail, d.kEdc)
, MechAvailSlow = [$(DbGlobal)].dbo.WtAvgNN(m.MechAvailSlow, d.kEdc)
, OpAvailSlow = [$(DbGlobal)].dbo.WtAvgNN(m.OpAvailSlow, d.kEdc)
, PlantAvailSlow = [$(DbGlobal)].dbo.WtAvgNN(m.PlantAvailSlow, d.kEdc)
, OthMechUnAvail = [$(DbGlobal)].dbo.WtAvgNN(m.OthMechUnAvail, d.kEdc)
, RMeiEDC = [$(DbGlobal)].dbo.WtAvgNN(m.RMeiEDC, d.kEdc)
, RMeiRV = [$(DbGlobal)].dbo.WtAvgNN(m.RMeiRV, d.RV_MUS)
, MaintOCCSWBPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintOCCSWBPerkEDC, d.kEdc)
, MaintMpsSWBPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintMpsSWBPerkEDC, d.kEdc)
, MaintContractLaborPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintContractLaborPerkEDC, d.kEdc)
, MaintTotMatlPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintTotMatlPerkEDC, d.kEdc)
, MaintEquipPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintEquipPerkEDC, d.kEdc)
, MaintRoutMaintPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintRoutMaintPerkEDC, d.kEdc)
, MaintContractMatlPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintContractMatlPerkEDC, d.kEdc)
, MaintMatlPerkEDC = [$(DbGlobal)].dbo.WtAvgNN(m.MaintMatlPerkEDC, d.kEdc)
, MaintOCCSWBPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintOCCSWBPerHVChem, d.HvcProd_kMT)
, MaintMpsSWBPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintMpsSWBPerHVChem, d.HvcProd_kMT)
, MaintContractLaborPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintContractLaborPerHVChem, d.HvcProd_kMT)
, MaintTotMatlPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintTotMatlPerHVChem, d.HvcProd_kMT)
, MaintEquipPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintEquipPerHVChem, d.HvcProd_kMT)
, MaintRoutMaintPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintRoutMaintPerHVChem, d.HvcProd_kMT)
, MaintContractMatlPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintContractMatlPerHVChem, d.HvcProd_kMT)
, MaintMatlPerHVChem = [$(DbGlobal)].dbo.WtAvgNN(m.MaintMatlPerHVChem, d.HvcProd_kMT)
, MaintOCCSWBPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintOCCSWBPerMEI, d.MEIStd)
, MaintMpsSWBPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintMpsSWBPerMEI, d.MEIStd)
, MaintContractLaborPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintContractLaborPerMEI, d.MEIStd)
, MaintTotMatlPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintTotMatlPerMEI, d.MEIStd)
, MaintEquipPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintEquipPerMEI, d.MEIStd)
, MaintRoutMaintPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintRoutMaintPerMEI, d.MEIStd)
, MaintContractMatlPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintContractMatlPerMEI, d.MEIStd)
, MaintMatlPerMEI = [$(DbGlobal)].dbo.WtAvgNN(m.MaintMatlPerMEI, d.MEIStd)
, MaintCorrEmergencyPcnt = [$(DbGlobal)].dbo.WtAvgNN(m.MaintCorrEmergencyPcnt, 1.0)
, MainCorrProcessPcnt = [$(DbGlobal)].dbo.WtAvgNN(m.MainCorrProcessPcnt, 1.0)
, MaintPrevCondPcnt = [$(DbGlobal)].dbo.WtAvgNN(m.MaintPrevCondPcnt, 1.0)
, MaintPrevCondAddPcnt = [$(DbGlobal)].dbo.WtAvgNN(m.MaintPrevCondAddPcnt, 1.0)
, MaintPrevSysPcnt = [$(DbGlobal)].dbo.WtAvgNN(m.MaintPrevSysPcnt, 1.0)
, MaintPrevSysAddPcnt = [$(DbGlobal)].dbo.WtAvgNN(m.MaintPrevSysAddPcnt, 1.0)
, FurnRunDaysEthane = [$(DbGlobal)].dbo.WtAvgNZ(m.FurnRunDaysEthane, 1.0)
, FurnRunDaysPropane = [$(DbGlobal)].dbo.WtAvgNZ(m.FurnRunDaysPropane, 1.0)
, FurnRunDaysNaphtha = [$(DbGlobal)].dbo.WtAvgNZ(m.FurnRunDaysNaphtha, 1.0)
, FurnRunDaysLiqHeavy = [$(DbGlobal)].dbo.WtAvgNZ(m.FurnRunDaysLiqHeavy, 1.0)
, FurnTubeLifeYrs = [$(DbGlobal)].dbo.WtAvgNZ(m.FurnTubeLifeYrs, 1.0)
, FurnRetubeDays = [$(DbGlobal)].dbo.WtAvgNZ(m.FurnRetubeDays, 1.0)

FROM @MyGroup r JOIN dbo.Maint m on m.Refnum=r.Refnum
JOIN dbo.Divisors d on d.Refnum=r.Refnum

SET NOCOUNT OFF



















