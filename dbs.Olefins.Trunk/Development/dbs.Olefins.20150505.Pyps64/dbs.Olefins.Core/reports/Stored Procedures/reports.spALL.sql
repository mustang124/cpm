﻿CREATE             PROC [reports].[spALL](@GroupId varchar(25))
AS
SET NOCOUNT ON
SELECT @GroupId = RTRIM(@GroupId)
Print 'REPORTS Processing ' + @GroupId

EXECUTE [reports].[Delete_GapEnergySeparation]		@GroupId;
EXECUTE [reports].[Delete_GapReliabilityOppLoss]	@GroupId;
EXECUTE [reports].[Delete_GapProductPurity]			@GroupId;


EXEC reports.CalcGroup @GroupId
--Print 'Cap ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spCapUtil	@GroupId
--Print 'Cpl ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spCapital				@GroupId
--Print 'Eng ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spEnergy				@GroupId
--Print 'Env ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spEnvironment			@GroupId
--Print 'Fcl ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spFacilities			@GroupId
--Print 'Fin ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spFactors			@GroupId
--Print 'Fct ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spFinancial			@GroupId
--Print 'Inv ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spInventory			@GroupId
--Print 'MTs ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spMaint				@GroupId
--Print 'MCt ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spMaintCostPcnt		@GroupId
--Print 'MTA ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spMaintTA				@GroupId
--Print 'Opx ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spOPEX					@GroupId
--Print 'PCs ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spPersCost				@GroupId
--Print 'Prs ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spPersonnel			@GroupId
--Print 'Rel ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spReliability			@GroupId
--Print 'Pyr ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spPyrolysis			@GroupId
--Print 'Yld ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spYield				@GroupId
--Print 'APC ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spAPC					@GroupId
--Print 'APCAppCount ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spAPCAppCount			@GroupId
--Print 'APCExistence ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spAPCExistence			@GroupId
EXEC reports.spFeedStocks			@GroupId

EXECUTE [reports].[Insert_GapProductPurity]			@GroupId;
EXECUTE [reports].[Insert_GapReliabilityOppLoss]	@GroupId;
EXECUTE [reports].[Insert_GapEnergySeparation]		@GroupId;

--Print 'GENSUM ' +  cast(SYSDATETIME() as varchar)
EXEC reports.spGENSUM				@GroupId
Print 'reports End ' +  cast(SYSDATETIME() as varchar)


SET NOCOUNT OFF