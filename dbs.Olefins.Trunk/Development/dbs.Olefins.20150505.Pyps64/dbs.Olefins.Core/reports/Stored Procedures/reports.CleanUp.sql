﻿CREATE PROC [reports].[CleanUp] as
DECLARE @name varchar(40), @qry varchar(200)

DECLARE cTables CURSOR
READ_ONLY
FOR SELECT name = s.name+'.'+t.name 
	FROM sys.tables t INNER JOIN sys.schemas s ON s.schema_id = t.schema_id
	WHERE EXISTS (SELECT * FROM sys.columns c WHERE c.object_id = t.object_id and c.name = 'tsModified')
	AND s.name = 'reports'
	
OPEN cTables

FETCH NEXT FROM cTables INTO @name
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
		SELECT @qry = 'Delete from ' + RTRIM(@Name) + ' Where GroupId not in (Select GroupId from reports.ReportGroups UNION Select Refnum from TSort)'
		print (@qry)
		exec(@qry)
	END
	FETCH NEXT FROM cTables INTO @name
END

CLOSE cTables
DEALLOCATE cTables




