﻿


CREATE VIEW [reports].[LTRanks] AS
Select
  GroupID = r.GroupID
, StudyYear = LT.StudyYear
, SourceDB = LT.SourceDB
, NumPlants = COUNT(*)
, HVCProd_kMT			= [$(DbGlobal)].dbo.WtAvg(lt.HVCProd_kMT, 1.0)
, MCI_Pcnt				= [$(DbGlobal)].dbo.WtAvg(lt.MCI_Pcnt, lt.HVCProd_kMT)
, RI_Pcnt				= [$(DbGlobal)].dbo.WtAvg(lt.RI_Pcnt, lt.HVCProd_kMT)
FROM reports.GroupPlants r JOIN dbo.LTRanks lt on lt.Refnum=r.Refnum
GROUP by r.GroupID, LT.StudyYear, LT.SourceDB

UNION

Select
  GroupID = lt.Refnum
 , StudyYear = LT.StudyYear
 , LT.SourceDB
 , NumPlants = 1
, HVCProd_kMT
, MCI_Pcnt
, RI_Pcnt
FROM dbo.LTRanks lt


