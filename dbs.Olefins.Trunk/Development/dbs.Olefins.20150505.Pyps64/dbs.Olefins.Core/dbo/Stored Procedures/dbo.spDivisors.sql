﻿






CREATE              PROC [dbo].[spDivisors](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @NumDays int
Select @NumDays = NumDays from dbo.TSort where Refnum = @Refnum

DECLARE @EthyleneCapYE_kMT real, @PropyleneCapYE_kMT real, @OlefinsCapYE_kMT real
SELECT
  @EthyleneCapYE_kMT			= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.Capacity_kMT ELSE 0 END)
, @PropyleneCapYE_kMT			= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.Capacity_kMT ELSE 0 END)
, @OlefinsCapYE_kMT				= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.Capacity_kMT ELSE 0 END)
From calc.CapacityPlant cu WHERE cu.Refnum = @Refnum and cu.FactorSetId = @FactorSetId

DECLARE @EIIStd real
SELECT @EIIStd = SUM(CASE WHEN e.StandardEnergyId = 'StandardEnergyTot' THEN StandardEnergy END) / t.NumDays
FROM calc.StandardEnergyAggregate e JOIN dbo.TSort t on t.Refnum=e.Refnum
WHERE e.FactorSetId = @FactorSetId and e.Refnum = @Refnum
GROUP BY t.NumDays


DECLARE @EthyleneCapAvg_kMT real, @PropyleneCapAvg_kMT real, @OlefinsCapAvg_kMT real, @PlantFeedCapAvg_kMT real, 
	@EthyleneCpbyAvg_kMT real, @PropyleneCpbyAvg_kMT real, @OlefinsCpbyAvg_kMT real,
	@EthyleneDiv_kMT real, @PropyleneDiv_kMT real, @OlefinsDiv_kMT real
SELECT
  @EthyleneCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.CapacityAvg_kMT ELSE 0 END)
, @PropyleneCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.CapacityAvg_kMT ELSE 0 END)
, @OlefinsCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.CapacityAvg_kMT ELSE 0 END)
, @PlantFeedCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='PlantFeed' THEN cu.CapacityAvg_kMT ELSE 0 END)
, @EthyleneCpbyAvg_kMT			= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.StreamAvg_kMT ELSE 0 END)
, @PropyleneCpbyAvg_kMT			= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.StreamAvg_kMT ELSE 0 END)
, @OlefinsCpbyAvg_kMT			= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.StreamAvg_kMT ELSE 0 END)
, @EthyleneDiv_kMT				= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.ProductionActual_kMT ELSE 0 END)
, @PropyleneDiv_kMT				= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.ProductionActual_kMT ELSE 0 END)
, @OlefinsDiv_kMT				= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.ProductionActual_kMT ELSE 0 END)
From calc.CapacityUtilization cu WHERE cu.Refnum=@Refnum and cu.SchedId='A' and cu.FactorSetId = @FactorSetId

DELETE FROM dbo.Divisors WHERE Refnum = @Refnum
Insert into dbo.Divisors (Refnum
, kEdc 
, kUEdc 
, EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT
, EthyleneCapYE_kMT
, PropyleneCapYE_kMT
, OlefinsCapYE_kMT
, PlantFeedCapAvg_kMT
, EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT
, EthyleneDiv_kMT 
, PropyleneDiv_kMT 
, OlefinsDiv_kMT 
, EthyleneProd_kMT 
, PropyleneProd_kMT 
, OlefinsProd_kMT 
, HvcProd_kMT 
, TotalPlantInput_kMT 
, PlantFeed_kMT 
, FreshPyroFeed_kMT 
, RV_MUS 
, RVUSGC_MUS 
, TaAdj_kUEDC
, EIIStd
, NEIStd
, MEIStd 
, PEIStd
, nmPEIStd
, mPEIStd
, NumDays )

Select d.Refnum
, kEdc = SUM(CASE WHEN d.DivisorId = 'kEdcTot' and d.DivisorField = 'kEdc' THEN d.DivisorValue END)
, kUEdc = SUM(CASE WHEN d.DivisorId = 'kEdcTot' and d.DivisorField = 'kUEdc' THEN d.DivisorValue END)
, EthyleneCapAvg_kMT = @EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT = @PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT = @OlefinsCapAvg_kMT
, EthyleneCapYE_kMT = @EthyleneCapYE_kMT
, PropyleneCapYE_kMT = @PropyleneCapYE_kMT
, OlefinsCapYE_kMT = @OlefinsCapYE_kMT
, PlantFeedCapAvg_kMT = @PlantFeedCapAvg_kMT
, EthyleneCpbyAvg_kMT = @EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT = @PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT = @OlefinsCpbyAvg_kMT
, EthyleneDiv_kMT = @EthyleneDiv_kMT
, PropyleneDiv_kMT = @PropyleneDiv_kMT
, OlefinsDiv_kMT = @OlefinsDiv_kMT
, EthyleneProd_kMT = SUM(CASE WHEN d.DivisorId = 'C2H4' and d.DivisorField = 'Production_kMT' THEN d.DivisorValue END)
, PropyleneProd_kMT = SUM(CASE WHEN d.DivisorId = 'C3H6' and d.DivisorField = 'Production_kMT' THEN d.DivisorValue END)
, OlefinsProd_kMT = SUM(CASE WHEN d.DivisorId = 'ProdOlefins' and d.DivisorField = 'Production_kMT' THEN d.DivisorValue END)
, HvcProd_kMT = SUM(CASE WHEN d.DivisorId = 'ProdHVC' and d.DivisorField = 'Production_kMT' THEN d.DivisorValue END)
, TotalPlantInput_kMT = SUM(CASE WHEN d.DivisorField = 'None' THEN -q.Tot_kMT END)
, PlantFeed_kMT = SUM(CASE WHEN d.DivisorId = 'PlantFeed' and d.DivisorField = 'FeedPyrolysis_kMT' THEN d.DivisorValue END)
, FreshPyroFeed_kMT = SUM(CASE WHEN d.DivisorId = 'FreshPyroFeed' and d.DivisorField = 'FeedPlant_kMT' THEN d.DivisorValue END)
, RV_MUS = SUM(CASE WHEN d.DivisorId = 'UscgReplacement' and d.DivisorField = 'ReplacementLocation' THEN d.DivisorValue END)
, RVUSGC_MUS = SUM(CASE WHEN d.DivisorId = 'UscgReplacement' and d.DivisorField = 'ReplacementValue' THEN d.DivisorValue END)
, TaAdj_kUEDC = SUM(CASE WHEN d.DivisorId = 'kEdcTot' and d.DivisorField = 'TaAdj_kUEDC' THEN d.DivisorValue END)
, EIIStd = @EIIStd
, NEIStd = SUM(CASE WHEN d.DivisorId = 'NonEnergy' and d.DivisorField = 'UnitTot' THEN d.DivisorValue END)
, MEIStd = SUM(CASE WHEN d.DivisorId = 'MES' and d.DivisorField = 'UnitTot' THEN d.DivisorValue END)
, PEIStd = SUM(CASE WHEN d.DivisorId = 'PersTot' and d.DivisorField = 'UnitTot' THEN d.DivisorValue END)
, nmPEIStd = SUM(CASE WHEN d.DivisorId = 'PersNonMaint' and d.DivisorField = 'UnitTot' THEN d.DivisorValue END)
, mPEIStd = SUM(CASE WHEN d.DivisorId = 'PersMaint' and d.DivisorField = 'UnitTot' THEN d.DivisorValue END)
, NumDays = @NumDays
From calc.Divisors d JOIN fact.QuantityAggregate q on q.Refnum=d.Refnum and q.StreamId = 'ProdLoss' and q.FactorSetId=d.FactorSetId
WHERE d.FactorSetId = @FactorSetId and d.Refnum = @Refnum
GROUP BY d.Refnum
SET NOCOUNT OFF
