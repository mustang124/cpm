﻿





CREATE              PROC [dbo].[spInventory](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @TotInven as real, @FreshPyroFeed as real

Select @TotInven = YearEndInventory_kMT
FROM fact.GenPlantInventoryAggregate g 
WHERE g.Refnum=@Refnum and g.FactorSetId = @FactorSetId
and g.FacilityId = 'Store'

select @FreshPyroFeed = Capacity_kMT
FROM  fact.Capacity f 
WHERE f.Refnum=@Refnum
AND StreamId='FreshPyroFeed'

Declare @StoreFeedAboveCnt real, @StoreFeedUnderCnt real, @StoreInterCnt real, @StoreProdAboveCnt real, @StoreProdUnderCnt real, @StoreCnt real

SELECT 
 @StoreFeedAboveCnt = SUM(CASE WHEN g.FacilityId='StoreFeedAbove' and StorageCapacity_kMT > 0 THEN 1 ELSE 0 END)
,@StoreFeedUnderCnt = SUM(CASE WHEN g.FacilityId='StoreFeedUnder' and StorageCapacity_kMT > 0 THEN 1 ELSE 0 END)
,@StoreInterCnt = SUM(CASE WHEN g.FacilityId='StoreInter' and StorageCapacity_kMT > 0 THEN 1 ELSE 0 END)
,@StoreProdAboveCnt = SUM(CASE WHEN g.FacilityId='StoreProdAbove' and StorageCapacity_kMT > 0 THEN 1 ELSE 0 END)
,@StoreProdUnderCnt = SUM(CASE WHEN g.FacilityId='StoreProdUnder' and StorageCapacity_kMT > 0 THEN 1 ELSE 0 END)
,@StoreCnt = SUM(CASE WHEN g.FacilityId='Store' and StorageCapacity_kMT > 0 THEN 1 ELSE 0 END)
FROM fact.GenPlantInventoryAggregate g 
WHERE g.Refnum=@Refnum and g.FactorSetId = @FactorsetID


DELETE FROM dbo.Inventory WHERE Refnum = @Refnum
Insert into dbo.Inventory (Refnum
,StoreFeedAbove_kMT
,StoreFeedUnder_kMT
,StoreInter_kMT
,StoreProdAbove_kMT
,StoreProdUnder_kMT
,Store_kMT
,InvenFeedAbove_kMT
,InvenFeedUnder_kMT
,InvenInter_kMT
,InvenProdAbove_kMT
,InvenProdUnder_kMT
,Inven_kMT
,InvenFeedAbove_Pcnt
,InvenFeedUnder_Pcnt
,InvenInter_Pcnt
,InvenProdAbove_Pcnt
,InvenProdUnder_Pcnt
,Inven_Pcnt
,InvenByTypeFeedAbove_Pcnt
,InvenByTypeFeedUnder_Pcnt
,InvenByTypeInter_Pcnt
,InvenByTypeProdAbove_Pcnt
,InvenByTypeProdUnder_Pcnt
,InvenByType_Pcnt
,InvenPerDaysCap
,TotInven
,FreshPyroFeed
,StoreFeedAboveCnt
,StoreFeedUnderCnt
,StoreInterCnt
,StoreProdAboveCnt
,StoreProdUnderCnt
,StoreCnt )

SELECT Refnum = @Refnum
,StoreFeedAbove_kMT = SUM(CASE WHEN g.FacilityId='StoreFeedAbove' THEN StorageCapacity_kMT ELSE 0 END)
,StoreFeedUnder_kMT = SUM(CASE WHEN g.FacilityId='StoreFeedUnder' THEN StorageCapacity_kMT ELSE 0 END)
,StoreInter_kMT = SUM(CASE WHEN g.FacilityId='StoreInter' THEN StorageCapacity_kMT ELSE 0 END) 
,StoreProdAbove_kMT = SUM(CASE WHEN g.FacilityId='StoreProdAbove' THEN StorageCapacity_kMT ELSE 0 END) 
,StoreProdUnder_kMT = SUM(CASE WHEN g.FacilityId='StoreProdUnder' THEN StorageCapacity_kMT ELSE 0 END) 
,Store_kMT = SUM(CASE WHEN g.FacilityId='Store' THEN StorageCapacity_kMT ELSE 0 END)
,InvenFeedAbove_kMT = SUM(CASE WHEN g.FacilityId='StoreFeedAbove' THEN YearEndInventory_kMT ELSE 0 END) 
,InvenFeedUnder_kMT = SUM(CASE WHEN g.FacilityId='StoreFeedUnder' THEN YearEndInventory_kMT ELSE 0 END) 
,InvenInter_kMT = SUM(CASE WHEN g.FacilityId='StoreInter' THEN YearEndInventory_kMT ELSE 0 END)
,InvenProdAbove_kMT = SUM(CASE WHEN g.FacilityId='StoreProdAbove' THEN YearEndInventory_kMT ELSE 0 END) 
,InvenProdUnder_kMT = SUM(CASE WHEN g.FacilityId='StoreProdUnder' THEN YearEndInventory_kMT ELSE 0 END)
,Inven_kMT = SUM(CASE WHEN g.FacilityId='Store' THEN YearEndInventory_kMT ELSE 0 END)
,InvenFeedAbove_Pcnt = CASE WHEN @StoreFeedAboveCnt > 0 THEN SUM(CASE WHEN g.FacilityId='StoreFeedAbove' THEN YearEndInventory_kMT ELSE 0 END) / SUM(CASE WHEN g.FacilityId='StoreFeedAbove' THEN StorageCapacity_kMT ELSE 0 END) * 100.0  ELSE 0 END
,InvenFeedUnder_Pcnt = CASE WHEN @StoreFeedUnderCnt > 0 THEN SUM(CASE WHEN g.FacilityId='StoreFeedUnder' THEN YearEndInventory_kMT ELSE 0 END) / SUM(CASE WHEN g.FacilityId='StoreFeedUnder' THEN StorageCapacity_kMT ELSE 0 END) * 100.0  ELSE 0 END
,InvenInter_Pcnt = CASE WHEN @StoreInterCnt > 0 THEN SUM(CASE WHEN g.FacilityId='StoreInter' THEN YearEndInventory_kMT ELSE 0 END) / SUM(CASE WHEN g.FacilityId='StoreInter' THEN StorageCapacity_kMT ELSE 0 END)  * 100.0 ELSE 0 END
,InvenProdAbove_Pcnt = CASE WHEN @StoreProdAboveCnt > 0 THEN SUM(CASE WHEN g.FacilityId='StoreProdAbove' THEN YearEndInventory_kMT ELSE 0 END) / SUM(CASE WHEN g.FacilityId='StoreProdAbove' THEN StorageCapacity_kMT ELSE 0 END)  * 100.0 ELSE 0 END
,InvenProdUnder_Pcnt = CASE WHEN @StoreProdUnderCnt > 0 THEN SUM(CASE WHEN g.FacilityId='StoreProdUnder' THEN YearEndInventory_kMT ELSE 0 END) / SUM(CASE WHEN g.FacilityId='StoreProdUnder' THEN StorageCapacity_kMT ELSE 0 END)  * 100.0 ELSE 0 END
,Inven_Pcnt = CASE WHEN @StoreCnt > 0 THEN SUM(CASE WHEN g.FacilityId='Store' THEN YearEndInventory_kMT ELSE 0 END) / SUM(CASE WHEN g.FacilityId='Store' THEN StorageCapacity_kMT ELSE 0 END)  * 100.0 ELSE 0 END
,InvenByTypeFeedAbove_Pcnt = CASE WHEN @TotInven > 0 THEN SUM(CASE WHEN g.FacilityId='StoreFeedAbove' THEN YearEndInventory_kMT ELSE 0 END) / @TotInven * 100.0 ELSE 0 END
,InvenByTypeFeedUnder_Pcnt = CASE WHEN @TotInven > 0 THEN SUM(CASE WHEN g.FacilityId='StoreFeedUnder' THEN YearEndInventory_kMT ELSE 0 END) / @TotInven * 100.0 ELSE 0 END
,InvenByTypeInter_Pcnt = CASE WHEN @TotInven > 0 THEN SUM(CASE WHEN g.FacilityId='StoreInter' THEN YearEndInventory_kMT ELSE 0 END) / @TotInven * 100.0 ELSE 0 END
,InvenByTypeProdAbove_Pcnt = CASE WHEN @TotInven > 0 THEN SUM(CASE WHEN g.FacilityId='StoreProdAbove' THEN YearEndInventory_kMT ELSE 0 END) / @TotInven * 100.0 ELSE 0 END
,InvenByTypeProdUnder_Pcnt = CASE WHEN @TotInven > 0 THEN SUM(CASE WHEN g.FacilityId='StoreProdUnder' THEN YearEndInventory_kMT ELSE 0 END) / @TotInven * 100.0 ELSE 0 END
,InvenByType_Pcnt = CASE WHEN @TotInven > 0 THEN SUM(CASE WHEN g.FacilityId='Store' THEN YearEndInventory_kMT ELSE 0 END) / @TotInven * 100.0 ELSE 0 END
,InvenPerDaysCap = CASE WHEN @FreshPyroFeed > 0 THEN SUM(CASE WHEN g.FacilityId='Store' THEN YearEndInventory_kMT ELSE 0 END) / @FreshPyroFeed ELSE 0 END * MIN(d.NumDays)
,@TotInven
,@FreshPyroFeed
,@StoreFeedAboveCnt
,@StoreFeedUnderCnt
,@StoreInterCnt
,@StoreProdAboveCnt
,@StoreProdUnderCnt
,@StoreCnt
FROM fact.GenPlantInventoryAggregate g JOIN dbo.Divisors d on d.Refnum=g.Refnum
WHERE g.Refnum=@Refnum and g.FactorSetId = @FactorsetID


SET NOCOUNT OFF
















