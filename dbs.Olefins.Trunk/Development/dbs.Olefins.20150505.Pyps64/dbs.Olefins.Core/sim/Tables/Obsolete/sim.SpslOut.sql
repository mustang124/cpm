﻿CREATE TABLE [sim].[SpslOut] (
    [QueueId]           BIGINT             NOT NULL,
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [SimModelId]        VARCHAR (12)       NOT NULL,
    [OpCondId]          VARCHAR (12)       NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [RecycleId]         INT                NOT NULL,
    [DataSetId]         VARCHAR (6)        CONSTRAINT [DF_SpslOut_DataSetId] DEFAULT ('Output') NOT NULL,
    [FieldId]           VARCHAR (12)       NOT NULL,
    [Value]             REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_SpslOut_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_SpslOut_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_SpslOut_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_SpslOut_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SpslOut] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [RecycleId] ASC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC, [StreamDescription] ASC, [FieldId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_SpslOut_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_SpslOut_DataSetId] CHECK ([DataSetId]='Output'),
    CONSTRAINT [CR_SpslOut_Value] CHECK ([Value]>=(0.0) OR [FieldId]=(140)),
    CONSTRAINT [FK_SpslOut_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_SpslOut_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_SpslOut_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_SpslOut_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_SpslOut_SimQueue] FOREIGN KEY ([QueueId]) REFERENCES [q].[Simulation] ([QueueId]),
    CONSTRAINT [FK_SpslOut_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [sim].[t_SpslOut_u]
	ON [sim].[SpslOut]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [sim].[SpslOut]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[sim].[SpslOut].FactorSetId			= INSERTED.FactorSetId
		AND	[sim].[SpslOut].Refnum				= INSERTED.Refnum
		AND	[sim].[SpslOut].CalDateKey			= INSERTED.CalDateKey
		AND	[sim].[SpslOut].SimModelId			= INSERTED.SimModelId
		AND	[sim].[SpslOut].OpCondId			= INSERTED.OpCondId
		AND	[sim].[SpslOut].StreamId			= INSERTED.StreamId
		AND	[sim].[SpslOut].StreamDescription	= INSERTED.StreamDescription
		AND	[sim].[SpslOut].RecycleId			= INSERTED.RecycleId
		AND	[sim].[SpslOut].FieldId				= INSERTED.FieldId;

END;