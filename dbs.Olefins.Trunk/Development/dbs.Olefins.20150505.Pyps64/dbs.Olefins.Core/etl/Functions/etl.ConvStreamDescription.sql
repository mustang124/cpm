﻿CREATE FUNCTION [etl].[ConvStreamDescription]
(
	@StreamId			VARCHAR(42), 
	@FeedDesc			NVARCHAR (256),
	@ProdDesc1			NVARCHAR (256),
	@ProdDesc2			NVARCHAR (256),
	@MiscDesc			NVARCHAR (256)
)
RETURNS NVARCHAR (256)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Desc NVARCHAR (256) = NULL;
	
	SET @Desc = 
		CASE @StreamId

		WHEN 'OthSpl'			THEN COALESCE(@MiscDesc, @StreamId)
		WHEN 'OthProd1'			THEN COALESCE(@ProdDesc1, @StreamId) + ' (Other Prod 1)'
		WHEN 'OthProd2'			THEN COALESCE(@ProdDesc2, @StreamId) + ' (Other Prod 2)'
			
		WHEN 'OthLiqFeed1'		THEN COALESCE(@FeedDesc, @StreamId) + ' (Other Feed 1)'
		WHEN 'OthLiqFeed2'		THEN COALESCE(@FeedDesc, @StreamId) + ' (Other Feed 2)'
		WHEN 'OthLiqFeed3'		THEN COALESCE(@FeedDesc, @StreamId) + ' (Other Feed 3)'
		WHEN 'OthLtFeed'		THEN COALESCE(@MiscDesc, @FeedDesc)

		ELSE @StreamId

		END;

	SET @Desc = ISNULL(@Desc, @StreamId)

	RETURN @Desc;

END