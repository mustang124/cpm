﻿CREATE FUNCTION [etl].[ConvRefnumStudy]
(
	  @Refnum		VARCHAR(25)
)
RETURNS VARCHAR(4)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @Study	VARCHAR(4) = '';

	IF (CHARINDEX('PCH', @Refnum) > 1)	SET @Study = 'PCH';
	IF (CHARINDEX('SEEC', @Refnum) > 1) SET @Study = 'SEEC';

	RETURN @Study;

END