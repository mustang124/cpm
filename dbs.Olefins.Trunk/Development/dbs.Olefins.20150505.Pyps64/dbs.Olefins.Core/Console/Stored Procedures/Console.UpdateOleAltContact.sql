﻿CREATE PROCEDURE [Console].[UpdateOleAltContact] 
@StudyYear nvarchar(4),
	@Refnum varchar(18),
	@AltFirstName nvarchar(80),
	@AltLastName nvarchar(80),
	@AltEmail nvarchar(80)
AS

BEGIN

DECLARE @ContactCode nvarchar(20)

	SELECT @ContactCode = ContactCode from TSort where Refnum=@Refnum

	IF EXISTS(SELECT ContactCode from CoContactInfo where ContactCode = @ContactCode)

		UPDATE C 
		SET
				c.AltFirstName=@AltFirstName ,
				c.AltLastName=@AltLastName ,
				c.AltEmail =@AltEmail
				
				
			 FROM CoContactInfo c join TSort t on t.ContactCode = c.ContactCode
				WHERE t.Refnum=@Refnum and c.StudyYear=@StudyYear
				
	ELSE
	
		INSERT INTO CoContactInfo (ContactCode, StudyYear, ContactType, AltFirstName,AltLastName,AltEmail, CCAlt)
		VALUES (@ContactCode, @StudyYear, 'Alt', @AltFirstName,@AltLastName, @AltEmail, 'Y')
END
