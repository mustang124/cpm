﻿CREATE PROCEDURE [Console].[UpdateNote]

	@User varchar(10),
	@Refnum varchar(18),
	@NoteType varchar(20),
	@Note nvarchar(max)

AS
BEGIN
    if not exists(select * from Val.Notes where Refnum=@Refnum and NoteType = @NoteType and Note = @Note)
	INSERT INTO Val.Notes (Refnum, NoteType, Note, UpdatedBy, Updated) Values(@Refnum,@NoteType, @Note, @User, GETDATE())

END
