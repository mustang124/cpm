﻿CREATE TABLE [Console].[Console] (
    [StudyType]        NVARCHAR (50)      NOT NULL,
    [DataConnection]   NVARCHAR (200)     NOT NULL,
    [CurrentStudyYear] NVARCHAR (4)       NOT NULL,
    [StudyDrive]       NVARCHAR (200)     NOT NULL,
    [TempDrive]        NVARCHAR (50)      NOT NULL,
    [StudyRegion]      NVARCHAR (50)      NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_Console_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_Console_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_Console_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_Console_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Console] PRIMARY KEY CLUSTERED ([StudyType] ASC),
    CONSTRAINT [CL_Console_CurrentStudyYear] CHECK ([CurrentStudyYear]<>''),
    CONSTRAINT [CL_Console_DataConnection] CHECK ([DataConnection]<>''),
    CONSTRAINT [CL_Console_StudyDrive] CHECK ([StudyDrive]<>''),
    CONSTRAINT [CL_Console_StudyType] CHECK ([StudyType]<>''),
    CONSTRAINT [CL_Console_TempDrive] CHECK ([TempDrive]<>'')
);


GO

CREATE TRIGGER [Console].[t_Console_u]
	ON  [Console].[Console]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [Console].[Console]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[Console].[Console].[StudyType]			= INSERTED.[StudyType];

END;