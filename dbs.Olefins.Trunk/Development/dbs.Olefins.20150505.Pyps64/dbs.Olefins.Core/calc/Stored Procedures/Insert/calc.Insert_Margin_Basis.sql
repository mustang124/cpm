﻿CREATE PROCEDURE [calc].[Insert_Margin_Basis]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @MarginAnalysis		VARCHAR(42)	= 'None';
		DECLARE @ComponentId		VARCHAR(42)	= 'Tot';

		INSERT INTO calc.Margin(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MarginAnalysis, [MarginId], Hierarchy,
			OpEx_Cur, Stream_Cur, Amount_Cur,
			TaAdj_OpEx_Cur, TaAdj_Stream_Cur,TaAdj_Amount_Cur
		)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey [CalDateKey],
			fpl.CurrencyRpt,
			@MarginAnalysis,
			bridge.MarginId,
			bridge.Hierarchy,

			- SUM (
				CASE bridge.DescendantOperator
				WHEN '+' THEN + o.Amount_Cur
				WHEN '-' THEN - o.Amount_Cur
				END
			) / 1000.0									[OpEx_Cur],

			- SUM (
				CASE bridge.DescendantOperator
				WHEN '+' THEN + ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur))
				WHEN '-' THEN - ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur))
				END
			) / 1000.0									[Stream_Cur],

			- SUM (
				CASE bridge.DescendantOperator
				WHEN '+' THEN + ISNULL(o.Amount_Cur, ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur)))
				WHEN '-' THEN - ISNULL(o.Amount_Cur, ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur)))
				END
			) / 1000.0									[Amount_Cur],

			- SUM (
				CASE bridge.DescendantOperator
				WHEN '+' THEN + o.Amount_Cur
				WHEN '-' THEN - o.Amount_Cur
				END * COALESCE(tar._TaAdj_Production_Ratio, 1.0)
			) / 1000.0									[OpEx_Cur],

			- SUM (
				CASE bridge.DescendantOperator
				WHEN '+' THEN + ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur))
				WHEN '-' THEN - ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur))
				END * COALESCE(tar._TaAdj_Production_Ratio, 1.0)
			) / 1000.0									[Stream_Cur],

			- SUM (
				CASE bridge.DescendantOperator
				WHEN '+' THEN + ISNULL(o.Amount_Cur, ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur)))
				WHEN '-' THEN - ISNULL(o.Amount_Cur, ISNULL(-b._Stream_Margin_Value_Cur, ISNULL(c._Stream_Margin_Value_Cur, s._Stream_Value_Cur)))
				END * COALESCE(tar._TaAdj_Production_Ratio, 1.0)
			) / 1000.0									[Amount_Cur]

		FROM @fpl											fpl
		INNER JOIN dim.Margin_Basis_Bridge					bridge
			ON	bridge.FactorSetId = fpl.FactorSetId

		LEFT OUTER JOIN calc.TaAdjRatio						tar
			ON	tar.FactorSetId	= fpl.FactorSetId
			AND	tar.Refnum		= fpl.Refnum
			AND	tar.CalDateKey	= fpl.Plant_AnnDateKey
			AND	tar.StreamId	= 'Ethylene'
			AND	tar.ComponentId	= 'C2H4'
			AND	bridge.DescendantId NOT IN (SELECT a.DescendantId FROM dim.Account_Bridge a WHERE a.FactorSetId = fpl.FactorSetId AND a.AccountId = 'STNonVolTA')

		LEFT OUTER JOIN calc.OpExAggregate					o
			ON	o.FactorSetId	= fpl.FactorSetId
			AND	o.Refnum		= fpl.Refnum
			AND	o.CalDateKey	= fpl.Plant_QtrDateKey
			AND	o.CurrencyRpt	= fpl.CurrencyRpt
			AND	o.AccountId		= bridge.DescendantId
			AND	o.AccountId		IN ('STVol', 'STNonVol', 'STTaExp', 'NonCash')

		LEFT OUTER JOIN calc.[PricingPlantStream]			s
			ON	s.FactorSetId	= fpl.FactorSetId
			AND	s.Refnum		= fpl.Refnum
			AND	s.CalDateKey	= fpl.Plant_QtrDateKey
			AND	s.CurrencyRpt	= fpl.CurrencyRpt
			AND	s.StreamId		= bridge.DescendantId

		LEFT OUTER JOIN ante.PricingStreamPurityLimits		pur
			ON	pur.FactorSetId	= fpl.FactorSetId
			AND	pur.SimModelId	= 'Plant'
			AND	pur.StreamId	= 'ProdOther'
			AND	pur.ComponentId IN (SELECT d.DescendantId FROM dim.Component_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.ComponentId = @ComponentId)
			-- BUG 99 - Gross Product Value for 11PCH058, 11PCH118
			--AND	(tsq.DescendantId = 'ProdOtherMargin'
			--OR	(tsq.DescendantId = 'ByProdAdjustmentProdOther' AND	@MarginAnalysis <> 'None'))
			AND((bridge.DescendantId	= 'ProdOtherMargin'				AND	@MarginAnalysis <> 'None')
			OR	(bridge.DescendantId	= 'ByProdAdjustmentProdOther'	AND	@MarginAnalysis <> 'None'))

		LEFT OUTER JOIN calc.[PricingPlantComposition]		c
			ON	c.FactorSetId	= fpl.FactorSetId
			AND	c.Refnum		= fpl.Refnum
			AND	c.CalDateKey	= fpl.Plant_QtrDateKey
			AND	c.CurrencyRpt	= fpl.CurrencyRpt
			AND	c.StreamId		= pur.StreamId
			AND	c.ComponentId	= pur.ComponentId
			AND	c.Component_WtPcnt >= pur.PurityProduct_WtPcnt
			AND	bridge.DescendantId	= 'ProdOtherMargin'

		LEFT OUTER JOIN calc.[PricingPlantComposition]		b
			ON	b.FactorSetId	= fpl.FactorSetId
			AND	b.Refnum		= fpl.Refnum
			AND	b.CalDateKey	= fpl.Plant_QtrDateKey
			AND	b.CurrencyRpt	= fpl.CurrencyRpt
			AND	b.StreamId		= pur.StreamId
			AND	b.ComponentId	= pur.ComponentId
			AND	b.Component_WtPcnt >= pur.PurityProduct_WtPcnt
			-- BUG 99 - Gross Product Value for 11PCH058, 11PCH118
			--AND	(tsq.DescendantId = 'ByProdAdjustmentProdOther' AND	@MarginAnalysis <> 'None')
			AND((bridge.DescendantId = 'ProdOtherMargin'			AND	@MarginAnalysis <> 'None')
			OR	(bridge.DescendantId = 'ByProdAdjustmentProdOther'	AND	@MarginAnalysis <> 'None'))

		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			fpl.CurrencyRpt,
			bridge.MarginId,
			bridge.Hierarchy

		HAVING SUM(COALESCE(o.Amount_Cur, -b._Stream_Margin_Value_Cur, c._Stream_Margin_Value_Cur, s._Stream_Value_Cur)) IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;