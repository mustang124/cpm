﻿CREATE PROCEDURE calc.Insert_DivisorsMetaProduction
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.DivisorsMetaProduction(FactorSetId, Refnum, CalDateKey, ComponentId,
			ProductionActual_kMT, ProductionActual_WtPcnt,
			Production_kMT, Production_WtPcnt, TaAdj_Production_Ratio)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			b.ComponentId,
			(SUM(c.Component_kMT)	- COALESCE(q.MetaOCEthylene, 0.0) + CASE WHEN b.ComponentId <> 'C2H4' THEN COALESCE(q.MetaProdPropylenePG, 0.0) ELSE 0.0 END)	[ProductionActual_kMT],
			(SUM(c.Component_kMT)	- COALESCE(q.MetaOCEthylene, 0.0) + CASE WHEN b.ComponentId <> 'C2H4' THEN COALESCE(q.MetaProdPropylenePG, 0.0) ELSE 0.0 END)
												/ f.FeedPyrolysis_kMT * 100.0														[ProductionActual_WtPcnt],
			(SUM(c.Purity_kMT)		- COALESCE(q.MetaOCEthylene, 0.0) + CASE WHEN b.ComponentId <> 'C2H4' THEN COALESCE(q.MetaProdPropylenePG, 0.0) ELSE 0.0 END)	[Production_kMT],
			(SUM(c.Purity_kMT)		- COALESCE(q.MetaOCEthylene, 0.0) + CASE WHEN b.ComponentId <> 'C2H4' THEN COALESCE(q.MetaProdPropylenePG, 0.0) ELSE 0.0 END)
												/ f.FeedPyrolysis_kMT * 100.0														[Production_WtPcnt],
			r._TaAdj_Production_Ratio									[TaAdj_Production_Ratio]

		FROM @fpl										tsq

		INNER JOIN calc.DivisorsFeed					f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_AnnDateKey
			AND	f.StreamId = 'PlantFeed'

		INNER JOIN calc.TaAdjRatio						r
			ON	r.FactorSetId = tsq.FactorSetId
			AND	r.Refnum = tsq.Refnum
			AND	r.CalDateKey = tsq.Plant_AnnDateKey
			AND	r.StreamId = 'Ethylene'
			AND	r.ComponentId = 'C2H4'

		INNER JOIN dim.Component_Bridge					b
			ON	b.FactorSetId = tsq.FactorSetId
			AND	b.ComponentId IN (SELECT d.DescendantId FROM dim.Component_Bridge d WHERE d.FactorSetId = tsq.FactorSetId AND d.ComponentId = 'ProdHVC')

		LEFT OUTER JOIN calc.[CompositionStream]		c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.ComponentId = b.DescendantId
			AND	c.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = tsq.FactorSetId AND d.StreamId = 'ProdHVC')
			AND (c.Component_kMT IS NOT NULL OR c.Purity_kMT IS NOT NULL)

		LEFT OUTER JOIN (
			SELECT
				q.Refnum,
				q.StreamId,
				q.Quantity_kMT
			FROM [fact].[Quantity]				q
			WHERE	q.StreamId	IN ('MetaOCEthylene', 'MetaProdPropylenePG')
			) u
			PIVOT(
				SUM(u.Quantity_kMT) FOR StreamId IN ([MetaOCEthylene], [MetaProdPropylenePG])
			) q
			ON	q.Refnum		= tsq.Refnum

		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			b.ComponentId,
			f.FeedPyrolysis_kMT,
			r._TaAdj_Production_Ratio,
			q.MetaOCEthylene,
			q.MetaProdPropylenePG

		HAVING SUM(c.Component_kMT) IS NOT NULL AND f.FeedPyrolysis_kMT IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;