﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonDirect_CO2Methane]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonDirect(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_MBtu, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			std.SimModelId,
			fpl.Plant_QtrDateKey,
			'CO2Methane',
			((dir.Quantity_MBtu / tot.Quantity_MBtu) * std.Quantity_MBtu - COALESCE(H2.Quantity_MBtu, 0.0) - COALESCE(utl.Quantity_MBtu, 0.0)),
			((dir.Quantity_MBtu / tot.Quantity_MBtu) * std.Quantity_MBtu - COALESCE(H2.Quantity_MBtu, 0.0) - COALESCE(utl.Quantity_MBtu, 0.0))
					* (44.01 / 16.04 / nhv.NHValue_MBtuLb / 2204.6 * 1000000.0)
		FROM @fpl												fpl

		INNER JOIN ante.PricingNetHeatingValueComposition		nhv
			ON	nhv.FactorSetId		= fpl.FactorSetId
			AND nhv.ComponentId		= 'CH4'

		INNER JOIN calc.EmissionsCarbonStandardAggregate		std
			ON	std.FactorSetId		= fpl.FactorSetId
			AND	std.Refnum			= fpl.Refnum
			AND	std.CalDateKey		= fpl.Plant_QtrDateKey
			AND	std.EmissionsId		= 'TotalEmissions'
		INNER JOIN calc.EmissionsCarbonPlantAggregate			dir
			ON	dir.FactorSetId		= fpl.FactorSetId
			AND	dir.Refnum			= fpl.Refnum
			AND	dir.CalDateKey		= fpl.Plant_QtrDateKey
			AND	dir.EmissionsId		= 'DirectCombustion'
		INNER JOIN calc.EmissionsCarbonPlantAggregate			tot
			ON	tot.FactorSetId		= fpl.FactorSetId
			AND	tot.Refnum			= fpl.Refnum
			AND	tot.CalDateKey		= fpl.Plant_QtrDateKey
			AND	tot.EmissionsId		= 'TotalEmissions'

		LEFT OUTER JOIN calc.EmissionsCarbonStandard			H2
			ON	H2.FactorSetId		= fpl.FactorSetId
			AND	H2.Refnum			= fpl.Refnum
			AND	H2.CalDateKey		= fpl.Plant_QtrDateKey
			AND	H2.EmissionsId		= 'CO2Hydrogen'
		LEFT OUTER JOIN calc.EmissionsCarbonStandardAggregate	utl
			ON	utl.FactorSetId		= fpl.FactorSetId
			AND	utl.Refnum			= fpl.Refnum
			AND	utl.CalDateKey		= fpl.Plant_QtrDateKey
			AND	utl.EmissionsId		= 'ExportUtilities'

		WHERE	fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END