﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_2013_FracFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'Plant',
			ff.[StandardEnergyId],
			ff.[StandardEnergyId],
			ff.[StandardEnergy_MBtuDay] * 365.0
		FROM @fpl										fpl
		INNER JOIN [calc].[StandardEnergyFracFeed]		ff
			ON	ff.[FactorSetId]		= fpl.[FactorSetId]
			AND	ff.[Refnum]				= fpl.[Refnum]
			AND	ff.[CalDateKey]			= fpl.[Plant_QtrDateKey]
		WHERE	fpl.[FactorSet_AnnDateKey]	> 20130000
			AND	fpl.[CalQtr]				= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;