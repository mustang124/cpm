﻿CREATE PROCEDURE [calc].[Insert_Edc_Hydrotreater]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			k.EdcId,
			k.EdcId,
			k.Value * f.FeedRate_kBsd / 1000.0			[kEdc]
		FROM @fpl										tsq
		INNER JOIN	fact.FacilitiesHydroTreat			f
			ON	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN ante.EdcCoefficients					k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.EdcId = f.FacilityId
		WHERE	f.FeedRate_kBsd IS NOT NULL
			AND tsq.[FactorSet_AnnDateKey] < 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	BEGIN TRY

		INSERT INTO [calc].[Edc]([FactorSetId], [Refnum], [CalDateKey], [EdcId], [EdcDetail], [kEdc])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			map.[EdcId],
			map.[EdcId],
			h.[FeedRate_kBsd] * k.[Value]
		FROM @fpl											fpl
		LEFT OUTER JOIN [fact].[FacilitiesHydroTreat]		h
			ON	h.[Refnum]		= fpl.[Refnum]
		INNER JOIN (VALUES
			('B', 'TowerPyroGasB'),
			('DS', 'TowerPyroGasDS'),
			('SS', 'TowerPyroGasSS')
			)												map ([HTTypeId], [EdcId])
			ON map.[HTTypeId]	= COALESCE(h.[HTTypeId], 'SS')
		INNER JOIN [ante].[EdcCoefficients]					k
			ON	k.[FactorSetId]	= fpl.[FactorSetId]
			AND	k.[EdcId]		= map.[EdcId]
		WHERE	fpl.[CalQtr]	= 4
			AND	fpl.[FactorSet_AnnDateKey] > 20130000
			AND	h.FeedRate_kBsd	IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;