﻿CREATE TABLE [calc].[PersMaintTa] (
    [FactorSetId]              VARCHAR (12)       NOT NULL,
    [Refnum]                   VARCHAR (25)       NOT NULL,
    [CalDateKey]               INT                NOT NULL,
    [PersId]                   VARCHAR (34)       NOT NULL,
    [InflAdjAnn_Company_Hrs]   REAL               NULL,
    [InflAdjAnn_Contract_Hrs]  REAL               NULL,
    [_InflAdjAnn_Tot_Hrs]      AS                 (CONVERT([real],isnull([InflAdjAnn_Company_Hrs],(0.0))+isnull([InflAdjAnn_Contract_Hrs],(0.0)),(2))) PERSISTED NOT NULL,
    [_InflAdjAnn_Company_Fte]  AS                 (CONVERT([real],[InflAdjAnn_Company_Hrs]/(2080.0),(2))) PERSISTED,
    [_InflAdjAnn_Contract_Fte] AS                 (CONVERT([real],[InflAdjAnn_Contract_Hrs]/(2080.0),(2))) PERSISTED,
    [_InflAdjAnn_Tot_Fte]      AS                 (CONVERT([real],(isnull([InflAdjAnn_Company_Hrs],(0.0))+isnull([InflAdjAnn_Contract_Hrs],(0.0)))/(2080.0),(2))) PERSISTED NOT NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_PersMaintTa_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_PersMaintTa_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_PersMaintTa_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_PersMaintTa_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PersMaintTa] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [PersId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PersMaintTa_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PersMaintTa_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PersMaintTa_Pers_LookUp] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_calc_PersMaintTa_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [calc].[t_PersMaintTa_u]
	ON [calc].[PersMaintTa]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.PersMaintTa
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.PersMaintTa.FactorSetId	= INSERTED.FactorSetId
		AND calc.PersMaintTa.Refnum			= INSERTED.Refnum
		AND calc.PersMaintTa.CalDateKey		= INSERTED.CalDateKey
		AND calc.PersMaintTa.PersId			= INSERTED.PersId;
	
END