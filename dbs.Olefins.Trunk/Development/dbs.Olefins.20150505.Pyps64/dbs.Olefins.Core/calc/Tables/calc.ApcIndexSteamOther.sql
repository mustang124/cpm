﻿CREATE TABLE [calc].[ApcIndexSteamOther] (
    [FactorSetId]             VARCHAR (12)       NOT NULL,
    [Refnum]                  VARCHAR (25)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [ApcId]                   VARCHAR (42)       NOT NULL,
    [AbsenceCorrectionFactor] REAL               NOT NULL,
    [OnLine_Pcnt]             REAL               NOT NULL,
    [Mpc_Int]                 INT                NOT NULL,
    [Mpc_Value]               REAL               NOT NULL,
    [_Abs_Mpc_Value]          AS                 (CONVERT([real],case when [AbsenceCorrectionFactor]<>(0.0) then [Mpc_Value]/[AbsenceCorrectionFactor]  end,(1))) PERSISTED NOT NULL,
    [Apc_Index]               REAL               NOT NULL,
    [_ApcOnLine_Index]        AS                 (CONVERT([real],([Apc_Index]*[OnLine_Pcnt])/(100.0),(1))) PERSISTED NOT NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_ApcIndexSteamOther_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_ApcIndexSteamOther_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_ApcIndexSteamOther_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_ApcIndexSteamOther_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ApcIndexSteamOther] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ApcId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ApcIndexSteamOther_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_calc_ApcIndexSteamOther_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ApcIndexSteamOther_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ApcIndexSteamOther_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_ApcIndexSteamOther_u
	ON  calc.ApcIndexSteamOther
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ApcIndexSteamOther
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ApcIndexSteamOther.FactorSetId	= INSERTED.FactorSetId
		AND calc.ApcIndexSteamOther.Refnum			= INSERTED.Refnum
		AND calc.ApcIndexSteamOther.CalDateKey		= INSERTED.CalDateKey
		AND calc.ApcIndexSteamOther.ApcId			= INSERTED.ApcId;
				
END;