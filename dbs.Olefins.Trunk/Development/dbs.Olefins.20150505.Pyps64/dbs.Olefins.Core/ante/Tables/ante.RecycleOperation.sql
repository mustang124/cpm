﻿CREATE TABLE [ante].[RecycleOperation] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [RecycleId]      INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_RecycleOperation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_RecycleOperation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_RecycleOperation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_RecycleOperation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_RecycleOperation] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CONSTRAINT [CR_RecycleOperation_RecycleId] CHECK ([RecycleId]>=(1)),
    CONSTRAINT [FK_RecycleOperation_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_RecycleOperation_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_RecycleOperation_u]
	ON [ante].[RecycleOperation]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[RecycleOperation]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[RecycleOperation].[FactorSetId]	= INSERTED.[FactorSetId]
		AND [ante].[RecycleOperation].StreamId		= INSERTED.StreamId;

END;