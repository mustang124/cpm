﻿CREATE TABLE [ante].[PricingNetHeatingValueStream] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [StreamId]         VARCHAR (42)       NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [NHValue_MBtuLb]   REAL               NOT NULL,
    [_NHValue_GJkMT]   AS                 (CONVERT([real],([NHValue_MBtuLb]*(1.05505585))/(2204.6),(1))) PERSISTED NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_PricingNetHeatingValueStream_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_PricingNetHeatingValueStream_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_PricingNetHeatingValueStream_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_PricingNetHeatingValueStream_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingNetHeatingValueStream] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingNetHeatingValueStream_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_PricingNetHeatingValueStream_LHValue_MBtuLb] CHECK ([NHValue_MBtuLb]>=(0.0)),
    CONSTRAINT [FK_PricingNetHeatingValueStream_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingNetHeatingValueStream_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingNetHeatingValueStream_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingNetHeatingValueStream_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_PricingNetHeatingValueStream_u]
	ON [ante].[PricingNetHeatingValueStream]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingNetHeatingValueStream]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingNetHeatingValueStream].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[PricingNetHeatingValueStream].CalDateKey		= INSERTED.CalDateKey
		AND	[ante].[PricingNetHeatingValueStream].StreamId			= INSERTED.StreamId;		

END;