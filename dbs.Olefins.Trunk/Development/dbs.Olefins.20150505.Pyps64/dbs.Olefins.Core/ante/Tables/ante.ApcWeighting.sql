﻿CREATE TABLE [ante].[ApcWeighting] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [ApcId]          VARCHAR (42)       NOT NULL,
    [Mpc_Value]      REAL               NOT NULL,
    [Sep_Value]      REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ApcWeighting_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ApcWeighting_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ApcWeighting_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ApcWeighting_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ApcWeighting] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ApcId] ASC),
    CONSTRAINT [FK_ApcWeighting_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_ApcWeighting_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_ApcWeighting_u]
	ON [ante].[ApcWeighting]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ApcWeighting]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[ApcWeighting].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[ApcWeighting].[ApcId]		= INSERTED.[ApcId];

END;