﻿CREATE TABLE [ante].[EmissionsFactorFlareLoss] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [FeedClassId]    TINYINT            NOT NULL,
    [CEF_MTCO2_MBtu] REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsFactorFlareLoss_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorFlareLoss_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorFlareLoss_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorFlareLoss_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EmissionsFactorFlareLoss] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [FeedClassId] ASC),
    CONSTRAINT [FK_EmissionsFactorFlareLoss_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_EmissionsFactorFlareLoss_u]
	ON [ante].[EmissionsFactorFlareLoss]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EmissionsFactorFlareLoss]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EmissionsFactorFlareLoss].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[EmissionsFactorFlareLoss].FeedClassId	= INSERTED.FeedClassId;

END;