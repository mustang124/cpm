﻿CREATE TABLE [ante].[PricingMultiplierROG] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [Coefficient]    REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PricingMultiplierROG_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PricingMultiplierROG_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PricingMultiplierROG_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PricingMultiplierROG_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingMultiplierROG] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingMultiplierROG_Coefficient] CHECK ([Coefficient]>=(0.0)),
    CONSTRAINT [FK_PricingMultiplierROG_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingMultiplierROG_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_PricingMultiplierROG_u]
	ON [ante].[PricingMultiplierROG]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingMultiplierROG]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingMultiplierROG].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[PricingMultiplierROG].CalDateKey		= INSERTED.CalDateKey;

END;