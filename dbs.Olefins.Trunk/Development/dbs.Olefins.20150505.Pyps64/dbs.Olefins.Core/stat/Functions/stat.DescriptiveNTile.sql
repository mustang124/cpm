﻿
CREATE FUNCTION [stat].[DescriptiveNTile]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics
	
	@Population			stat.PopulationRandomVar		READONLY,							--	Data points to examine
	@SortOrder			SMALLINT			= 1,											--	Sort order
	@Tiles				TINYINT				= NULL,											--	Number of nTiles
	@CapMin				FLOAT				= NULL,											--	Minimum reporting values to average
	@CapMax				FLOAT				= NULL											--	Maximum reporting values to average
)
RETURNS @ReturnTable TABLE 
(
	[qTile]				TINYINT				NOT	NULL	CHECK([qTile] >= 1),				--	Quartile value

	[xCount]			SMALLINT			NOT	NULL	CHECK([xCount]	>= 1),				--	Item Count
	[xMin]				FLOAT				NOT	NULL,										--	Minimum
	[xMax]				FLOAT				NOT	NULL,										--	Maximum
	[_xRange]			AS CAST([xMax] - [xMin]			AS FLOAT)							--	Range
						PERSISTED			NOT	NULL,
	[xMean]				FLOAT				NOT	NULL,										--	Mean								http://en.wikipedia.org/wiki/Mean
	[xStDevS]			FLOAT				NOT	NULL	CHECK([xStDevS]	>= 0.0),			--	Standard Deviation	s				http://en.wikipedia.org/wiki/Standard_deviation
	[xVarS]				FLOAT				NOT	NULL	CHECK([xVarS]	>= 0.0),			--	Variance			s²				http://en.wikipedia.org/wiki/Variance

	[_ErrorStandard]	AS CAST([xStDevS] / SQRT([xCount])	AS FLOAT)						--	Standard Error						http://en.wikipedia.org/wiki/Z-test
						PERSISTED			NOT	NULL,
	[_xCoeffVarS]		AS CAST(CASE WHEN [xMean] <> 0.0 THEN [xStDevS]	/ [xMean] END AS FLOAT)	--  Coefficient of Variation		http://en.wikipedia.org/wiki/Coefficient_of_variation
						PERSISTED					,
	[_xIdxDispS]		AS CAST(CASE WHEN [xMean] <> 0.0 THEN [xVarS]	/ [xMean] END AS FLOAT)	--  Index of Dispersion				http://en.wikipedia.org/wiki/Variance-to-mean_ratio
						PERSISTED					,

	[xSum]				FLOAT				NOT	NULL,										--	Sum	of x			Σ(x)
	[xSumXiX]			FLOAT				NOT	NULL,										--	Sum of Error		Σ(xi-AVG(x))

	[wMean]				FLOAT					NULL,										--	Weighted Mean						http://en.wikipedia.org/wiki/Weighted_arithmetic_mean
	[xwDiv]				FLOAT					NULL,										--	SUM(x) / SUM(w)

	[xLimitLower]		FLOAT					NULL,										--	Minimum (Smallest Two)
	[xLimitUpper]		FLOAT					NULL,										--	Minimum (Largest Two)
	[_xLimitRange]		AS CAST([xLimitUpper] - [xLimitLower]	AS FLOAT)					--	Range
						PERSISTED					,

	[TileBreakLower]	FLOAT					NULL,										--	Mean break value between nTiles
	[TileBreakUpper]	FLOAT					NULL,										--	Mean break value between nTiles
	
	CHECK([xMin] <= [xMax]),
	CHECK([xMean] BETWEEN [xMin] AND [xMax]),
	CHECK([xLimitLower] >= [xMin]),
	CHECK([xLimitUpper] <= [xMax]),

	PRIMARY KEY CLUSTERED ([qTile] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Order		FLOAT = CAST(ABS(@SortOrder) / @SortOrder AS FLOAT);
	DECLARE @nTiles		TINYINT = COALESCE(@Tiles, 4);

	DECLARE @nTile TABLE
	(
		[FactorSetId]		VARCHAR (12)		NOT	NULL	CHECK([FactorSetId]	<> ''),
		[Refnum]			VARCHAR (25)		NOT	NULL	CHECK([Refnum]		<> ''),
		[x]					FLOAT				NOT	NULL,
		[w]					FLOAT					NULL,
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile]	>= 1),			--	Quartile value
		[rnMin]				TINYINT					NULL	CHECK([rnMin]	>= 1),			--	Row Number
		[rnMax]				TINYINT					NULL	CHECK([rnMax]	>= 1),			--	Row Number
		
		PRIMARY KEY CLUSTERED ([x] ASC, [FactorSetId] ASC, [Refnum] ASC)
	);

	INSERT INTO @nTile(FactorSetId, Refnum, x, w, qTile, rnMin, rnMax)
	SELECT
		o.FactorSetId,
		o.Refnum,
		o.x,
		o.w,
		o.qTile,
		CASE WHEN o.x >= COALESCE(@CapMin, o.x) THEN
			ROW_NUMBER() OVER(PARTITION BY CASE WHEN o.x BETWEEN COALESCE(@CapMin, o.x) AND COALESCE(@CapMax, o.x) THEN o.qTile END ORDER BY o.x * @Order ASC, o.Refnum ASC)
			END	[MinRN],
		CASE WHEN o.x <= COALESCE(@CapMax, o.x) THEN
			ROW_NUMBER() OVER(PARTITION BY CASE WHEN o.x BETWEEN COALESCE(@CapMin, o.x) AND COALESCE(@CapMax, o.x) THEN o.qTile END ORDER BY o.x * @Order DESC, o.Refnum ASC)
			END	[MaxRN]
	FROM (
		SELECT
			o.FactorSetId,
			o.Refnum,
			o.x,
			o.w,
			NTILE(@nTiles) OVER(ORDER BY o.x * @Order ASC, o.Refnum ASC)	[qTile]
		FROM @Population o
		WHERE o.Basis = 1
		) o

	--------------------------------------------------------------------------------

	IF (@Tiles IS NULL)
	BEGIN

		DECLARE @MinCompanies	TINYINT = 2;
		DECLARE @MinItems		TINYINT = 4;

		DECLARE @Criteria TABLE
		(
			[Refnum]			VARCHAR (25)		NOT	NULL	CHECK([Refnum]		<> ''),
			[CompanyId]			VARCHAR (42)		NOT	NULL	CHECK([CompanyId]	<> ''),
			PRIMARY KEY CLUSTERED ([Refnum] ASC)
		)

		/*	Slow due to selecting from fact.TSort (view)
		*/
		--INSERT INTO @Criteria(Refnum, CompanyId)
		--SELECT o.Refnum, t.CompanyId
		--FROM @Population	o
		--INNER JOIN  fact.TSort	t
		--	ON	t.Refnum = o.Refnum
		--WHERE o.Basis = 1;

		WHILE @nTiles > 1 AND EXISTS
		(
			SELECT
				n.qTile
			FROM @nTile				n
			INNER JOIN @Criteria	c
				ON	c.Refnum = n.Refnum
			GROUP BY
				n.qTile
			HAVING	COUNT(1) < @MinItems
				OR	COUNT(DISTINCT c.CompanyId) < @MinCompanies
		)
		BEGIN
	
			SET @nTiles = @nTiles - 1;

			UPDATE	n
			SET	n.qTile = t.qTile
			FROM @nTile		n
			INNER JOIN (
				SELECT
					o.FactorSetId,
					o.Refnum,
					o.x,
					o.w,
					o.qTile,
					CASE WHEN o.x >= COALESCE(@CapMin, o.x) THEN
						ROW_NUMBER() OVER(PARTITION BY CASE WHEN o.x BETWEEN COALESCE(@CapMin, o.x) AND COALESCE(@CapMax, o.x) THEN o.qTile END ORDER BY o.x * @Order ASC, o.Refnum ASC)
						END	[MinRN],
					CASE WHEN o.x <= COALESCE(@CapMax, o.x) THEN
						ROW_NUMBER() OVER(PARTITION BY CASE WHEN o.x BETWEEN COALESCE(@CapMin, o.x) AND COALESCE(@CapMax, o.x) THEN o.qTile END ORDER BY o.x * @Order DESC, o.Refnum ASC)
						END	[MaxRN]
				FROM (
					SELECT
						o.FactorSetId,
						o.Refnum,
						o.x,
						o.w,
						NTILE(@nTiles) OVER(ORDER BY o.x * @Order ASC, o.Refnum ASC)	[qTile]
					FROM @nTile		o
					) o
				) t
				ON	t.FactorSetId = n.FactorSetId
				AND	t.Refnum = n.Refnum
				AND	t.x = n.x;

		END;
	
	END;

	--------------------------------------------------------------------------------

	DECLARE @MSE TABLE
	(
		[qTile]				TINYINT				NOT	NULL	CHECK([qTile] >= 1),				--	Quartile value
		[xMean]				FLOAT				NOT	NULL,										--	Mean								http://en.wikipedia.org/wiki/Mean
		PRIMARY KEY CLUSTERED ([qTile] ASC)
	)

	INSERT INTO @MSE(qTile, xMean)
	SELECT
		n.qTile,
		AVG(n.x)
	FROM @nTile	n
	GROUP BY
		n.qTile;

	INSERT INTO @ReturnTable(qTile, xCount, xMin, xMax, xMean, xStDevS, xVarS, xSum, xSumXiX, wMean, xwDiv)
	SELECT
		n.qTile,
		COUNT(n.x),
		MIN(n.x),
		MAX(n.x),
		AVG(n.x),
		STDEV(n.x),
		VAR(n.x),
		SUM(n.x),
		SUM(SQUARE(n.x - mse.xMean)),
		CASE WHEN SUM(n.w) <> 0.0 THEN SUM(n.x * n.w) / SUM(n.w) END,
		CASE WHEN SUM(n.w) <> 0.0 THEN SUM(n.x) / SUM(n.w) END
	FROM @nTile			n
	INNER JOIN @MSE		mse
		ON	mse.qTile = n.qTile
	GROUP BY
		n.qTile;

	UPDATE rt
	SET
		rt.TileBreakLower = n.TileBreakLower,
		rt.TileBreakUpper = n.TileBreakUpper,
		rt.xLimitLower = n.xLimitLower,
		rt.xLimitUpper = n.xLimitUpper
	FROM @ReturnTable	rt
	INNER JOIN (
		SELECT
			rt.qTile, 
			(rt.xMin + MAX(l.x)) / 2.0					[TileBreakLower],
			(rt.xMax + MIN(u.x)) / 2.0					[TileBreakUpper],
			AVG(CASE WHEN n.rnMin <= 2 THEN n.x END)	[xLimitLower],
			AVG(CASE WHEN n.rnMax <= 2 THEN n.x END)	[xLimitUpper]
		FROM @ReturnTable					rt
		LEFT OUTER JOIN @nTile				l
			ON	l.qTile = rt.qTile - 1
		LEFT OUTER JOIN @nTile				u
			ON	u.qTile = rt.qTile + 1
		LEFT OUTER JOIN @nTile				n
			ON	n.qTile = rt.qTile
			AND (n.rnMin <= 2 OR	n.rnMax <= 2)
		GROUP BY
			rt.qTile, 
			rt.xMin,
			rt.xMax
		) n
		ON n.qTile = rt.qTile;

	RETURN;

END;
