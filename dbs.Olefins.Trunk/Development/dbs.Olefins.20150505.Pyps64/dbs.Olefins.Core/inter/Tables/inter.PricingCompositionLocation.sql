﻿CREATE TABLE [inter].[PricingCompositionLocation] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [PricingMethodId]       VARCHAR (42)       NOT NULL,
    [LocationId]            VARCHAR (42)       NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [ComponentId]           VARCHAR (42)       NOT NULL,
    [MatlBal_Cur]           REAL               NOT NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],coalesce([MatlBal_Supersede_Cur],[MatlBal_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingCompositionLocation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionLocation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionLocation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingCompositionLocation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingCompositionLocation] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PricingMethodId] ASC, [CurrencyRpt] ASC, [LocationId] ASC, [StreamId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingCompositionLocation_MatlBal_Cur] CHECK ([MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingCompositionLocation_MatlBal_Supersede_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingCompositionLocation_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingCompositionLocation_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingCompositionLocation_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingCompositionLocation_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingCompositionLocation_PricingLocations] FOREIGN KEY ([LocationId]) REFERENCES [dim].[PricingLocationsLu] ([LocationId]),
    CONSTRAINT [FK_PricingCompositionLocation_PricingMethod] FOREIGN KEY ([PricingMethodId]) REFERENCES [dim].[PricingMethodLu] ([MethodId]),
    CONSTRAINT [FK_PricingCompositionLocation_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [inter].t_PricingCompositionLocation_u
	ON [inter].PricingCompositionLocation
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [inter].PricingCompositionLocation
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].PricingCompositionLocation.FactorSetId		= INSERTED.FactorSetId
		AND [inter].PricingCompositionLocation.LocationId		= INSERTED.LocationId
		AND [inter].PricingCompositionLocation.CalDateKey		= INSERTED.CalDateKey
		AND [inter].PricingCompositionLocation.ComponentId		= INSERTED.ComponentId
		AND [inter].PricingCompositionLocation.CurrencyRpt		= INSERTED.CurrencyRpt;

END;