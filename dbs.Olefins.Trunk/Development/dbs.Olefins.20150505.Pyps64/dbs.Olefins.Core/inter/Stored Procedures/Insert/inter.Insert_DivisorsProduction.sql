﻿CREATE PROCEDURE [inter].[Insert_DivisorsProduction]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO inter.DivisorsProduction(FactorSetId, Refnum, CalDateKey, ComponentId, Production_kMT, Loss_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			MAX(c.CalDateKey),
			c.ComponentId,
			SUM(c.Component_kMT),
			SUM(r._TotLoss_kMT)
		FROM	@fpl									tsq
		INNER JOIN calc.[CompositionStream]				c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
		LEFT OUTER JOIN fact.ReliabilityOppLoss			r
			ON	r.Refnum = tsq.Refnum
			AND	r.CalDateKey = tsq.Plant_QtrDateKey
			AND	r.OppLossId = 'TurnAround'
			AND	r.StreamId = 'Ethylene'
		WHERE	c.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = tsq.FactorSetId AND d.StreamId IN ('Ethylene', 'ProdOther'))
			AND	c.ComponentId = 'C2H4'
			AND c.Component_kMT IS NOT NULL
		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			c.ComponentId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;