﻿CREATE PROCEDURE [inter].[Insert_ReplValPyroCapacityEstimate]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @StreamMap	TABLE
		(
			[StreamId]		VARCHAR (42)	NOT	NULL,
			[ComponentId]	VARCHAR (42)	NOT	NULL,
			PRIMARY KEY CLUSTERED([StreamId] ASC, [ComponentId] ASC)
		);

		DECLARE @Heavier	TABLE
		(
			[StreamId]		VARCHAR (42)	NOT	NULL,
			[HeavierId]		VARCHAR (42)	NOT	NULL,
			PRIMARY KEY CLUSTERED([StreamId] ASC, [HeavierId] ASC)
		);

		DECLARE @FeedFlex	TABLE
		(
			[FactorSetId]				VARCHAR (12)		NOT	NULL,
			[Refnum]					VARCHAR (25)		NOT	NULL,
			[CurrencyRpt]				VARCHAR (4)			NOT	NULL,
			[CalDateKey]				INT					NOT	NULL,
			[StreamId]					VARCHAR (42)		NOT	NULL,
			[Capability_Pcnt]			REAL					NULL,
			[Demonstrate_Pcnt]			REAL					NULL,
			[Act_C2Yield_EstWtPcnt]		REAL				NOT	NULL,
			[Escalated_ENC_Cur]			REAL				NOT	NULL,
			PRIMARY KEY CLUSTERED([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [CalDateKey] ASC)
		);

		INSERT INTO @StreamMap(StreamId, ComponentId)
		VALUES
			('Ethane', 'C2H6'),
			('Propane', 'C3H8'),
			('Butane', 'C4H10'),
			('LiqLight', 'LiqLight'),
			('Diesel', 'LiqHeavy'),
			('GasOilHv', 'LiqHeavy');

		INSERT INTO @Heavier([StreamId], [HeavierId])
		VALUES
			('Ethane',		'Propane'),
			('Ethane',		'Butane'),
			('Ethane',		'LiqLight'),
			('Ethane',		'LiqHeavy'),
			('Propane',		'Butane'),
			('Propane',		'LiqLight'),
			('Propane',		'LiqHeavy'),
			('Butane',		'LiqLight'),
			('Butane',		'LiqHeavy'),
			('LiqLight',	'LiqHeavy'),
			('LiqHeavy',	'-');

		INSERT INTO @FeedFlex(FactorSetId, Refnum, CalDateKey, CurrencyRpt, StreamId, Capability_Pcnt, Demonstrate_Pcnt, Act_C2Yield_EstWtPcnt, Escalated_ENC_Cur)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			ff.StreamId,
			ff.Capability_Pcnt,
			ff.Demonstrate_Pcnt,
			ISNULL(enc.Act_C2Yield_EstWtPcnt, 0.0),
			ISNULL(enc.Escalated_ENC_Cur, 0.0)
		FROM @fpl											fpl
		INNER JOIN fact.GenPlantFeedFlex					ff
			ON	ff.Refnum = fpl.Refnum
			AND	ff.CalDateKey = fpl.Plant_QtrDateKey
			AND ff.StreamId NOT IN ('Diesel', 'GasOilHv')
		INNER JOIN @StreamMap								m
			ON	m.StreamId = ff.StreamId
		INNER JOIN inter.ReplValEncEstimate					enc
			ON	enc.FactorSetId = fpl.FactorSetId
			AND	enc.Refnum = fpl.Refnum
			AND	enc.CalDateKey = fpl.Plant_QtrDateKey
			AND	enc.ComponentId = m.ComponentId
			AND	enc.CurrencyRpt = fpl.CurrencyRpt
		WHERE fpl.CalQtr = 4;

		INSERT INTO @FeedFlex(FactorSetId, Refnum, CalDateKey, CurrencyRpt, StreamId, Capability_Pcnt, Demonstrate_Pcnt, Act_C2Yield_EstWtPcnt, Escalated_ENC_Cur)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			l.StreamId,
			calc.MaxValue(ISNULL(a.Capability_Pcnt, 0.0), ISNULL(v.Capability_Pcnt, 0.0))
												[Capability_Pcnt],
			CASE WHEN ISNULL(v.Capability_Pcnt, 0.0) > ISNULL(a.Demonstrate_Pcnt, 0.0) + ISNULL(v.Demonstrate_Pcnt, 0.0)
			THEN	calc.MinValue(100.0, ISNULL(a.Demonstrate_Pcnt, 0.0) + ISNULL(v.Demonstrate_Pcnt, 0.0))
			ELSE	calc.MaxValue(ISNULL(a.Demonstrate_Pcnt, 0.0), ISNULL(v.Demonstrate_Pcnt, 0.0))
			END									[Demonstrate_Pcnt],
			ISNULL(enc.Act_C2Yield_EstWtPcnt, 0.0),
			ISNULL(enc.Escalated_ENC_Cur, 0.0)
		FROM @fpl										fpl
		LEFT OUTER JOIN fact.GenPlantFeedFlex			a
			ON	a.Refnum = fpl.Refnum
			AND	a.CalDateKey = fpl.Plant_QtrDateKey
			AND	a.StreamId = 'Diesel'
		LEFT OUTER JOIN fact.GenPlantFeedFlex			v
			ON	v.Refnum = fpl.Refnum
			AND	v.CalDateKey = fpl.Plant_QtrDateKey
			AND v.StreamId = 'GasOilHv'
		INNER JOIN dim.Stream_LookUp					l
			ON	l.StreamId = 'LiqHeavy'
		INNER JOIN inter.ReplValEncEstimate				enc
			ON	enc.FactorSetId = fpl.FactorSetId
			AND	enc.Refnum = fpl.Refnum
			AND	enc.CalDateKey = fpl.Plant_QtrDateKey
			AND	enc.CurrencyRpt = fpl.CurrencyRpt
			AND	enc.ComponentId = l.StreamId
		WHERE fpl.CalQtr = 4;

		INSERT INTO	inter.ReplValPyroCapacityEstimate(FactorSetId, Refnum, CalDateKey, CurrencyRpt, StreamId, Capability_Pcnt, Demonstrate_Pcnt, Escalated_ENC_Cur, Act_C2Yield_EstWtPcnt, DesignCapability_Pcnt, DemonCapability_Pcnt)
		SELECT
			ff.FactorSetId,
			ff.Refnum,
			ff.CalDateKey,
			ff.CurrencyRpt,
			ff.StreamId,
			ff.Capability_Pcnt,
			ff.Demonstrate_Pcnt,
			ff.Escalated_ENC_Cur,
			ff.Act_C2Yield_EstWtPcnt,

			calc.MaxValue(0.0, calc.MinValue(100.0 - SUM(ISNULL(x.Capability_Pcnt, 0.0)), ff.Capability_Pcnt))	[DesignCapability_Pcnt],
			calc.MaxValue(0.0, calc.MinValue(100.0 - SUM(calc.MaxValue(ISNULL(x.Demonstrate_Pcnt, 0.0), ISNULL(CASE WHEN x.StreamId = 'LiqHeavy' THEN x.Capability_Pcnt * 0.1 ELSE x.Act_C2Yield_EstWtPcnt END, 0.0))), calc.MaxValue(ff.Demonstrate_Pcnt, CASE WHEN ff.StreamId = 'LiqHeavy' THEN ff.Capability_Pcnt * 0.1 ELSE ff.Act_C2Yield_EstWtPcnt END)))	[DemonCapability_Pcnt]

		FROM @FeedFlex					ff
		INNER JOIN @Heavier				h
			ON	h.StreamId = ff.StreamId
		LEFT OUTER JOIN @FeedFlex		x
			ON	x.FactorSetId = ff.FactorSetId
			AND	x.Refnum = ff.Refnum
			AND	x.CalDateKey = ff.CalDateKey
			AND	x.CurrencyRpt = ff.CurrencyRpt
			AND	x.StreamId = h.HeavierId

		GROUP BY
			ff.FactorSetId,
			ff.Refnum,
			ff.CalDateKey,
			ff.CurrencyRpt,
			ff.StreamId,
			ff.Capability_Pcnt,
			ff.Demonstrate_Pcnt,
			ff.Act_C2Yield_EstWtPcnt,
			ff.Escalated_ENC_Cur;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;