﻿CREATE TABLE [stgFact].[Misc] (
    [Refnum]                VARCHAR (25)       NOT NULL,
    [EthPcntTot]            REAL               NULL,
    [ProPcntTot]            REAL               NULL,
    [ButPcntTot]            REAL               NULL,
    [OCCAbsence]            REAL               NULL,
    [MpsAbsence]            REAL               NULL,
    [SplFeedEnergy]         REAL               NULL,
    [LtFeedPriceBasis]      REAL               NULL,
    [OthLiqFeedName1]       VARCHAR (25)       NULL,
    [OthLiqFeedPriceBasis1] REAL               NULL,
    [OthLiqFeedName2]       VARCHAR (25)       NULL,
    [OthLiqFeedPriceBasis2] REAL               NULL,
    [OthLiqFeedName3]       VARCHAR (25)       NULL,
    [OthLiqFeedPriceBasis3] REAL               NULL,
    [TotCompOutage]         REAL               NULL,
    [PctFurnRelyLimit]      REAL               NULL,
    [ROG_CGCI]              CHAR (1)           NULL,
    [ROG_CGCD]              CHAR (1)           NULL,
    [ROG_C2RSF]             CHAR (1)           NULL,
    [ROG_C3RSF]             CHAR (1)           NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Misc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_stgFact_Misc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_stgFact_Misc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_stgFact_Misc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Misc] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Misc_u]
	ON [stgFact].[Misc]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Misc]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Misc].Refnum		= INSERTED.Refnum;

END;