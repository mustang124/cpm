﻿CREATE TABLE [stgFact].[OsimMetaData]
(
	[Refnum]			VARCHAR(25)			NOT	NULL,

	[VersionOS]			VARCHAR(128)			NULL	CONSTRAINT [CL_OsimMetaData_VersionOS]			CHECK([VersionOS] <> ''),
	[VersionExcel]		VARCHAR(128)			NULL	CONSTRAINT [CL_OsimMetaData_VersionExcel]		CHECK([VersionExcel] <> ''),
	[VersionVbe]		VARCHAR(128)			NULL	CONSTRAINT [CL_OsimMetaData_VersionVbe]			CHECK([VersionVbe] <> ''),
	[LocaleId]			INT						NULL,

	[DateTimeOsim]		SMALLDATETIME			NULL,
	[DateTimeTemplate]	SMALLDATETIME			NULL,

	[RefnumCurrent]		VARCHAR(25)			NOT	NULL,
	[RefnumHistory]		VARCHAR(25)				NULL,

	[StudyYearCurrent]	SMALLINT			NOT	NULL,
	[StudyYearHistory]	SMALLINT				NULL,

	[tsModified]		DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_OsimMetaData_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OsimMetaData_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OsimMetaData_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OsimMetaData_tsModifiedApp]		DEFAULT(APP_NAME()),

	CONSTRAINT [PK_OsimMetaData]	PRIMARY KEY CLUSTERED([Refnum]	ASC)
);
GO

CREATE TRIGGER [stgFact].[t_OsimMetaData_u]
	ON [stgFact].[OsimMetaData]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stgFact].[OsimMetaData]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED
	WHERE
		[stgFact].[OsimMetaData].[Refnum]		= INSERTED.[Refnum];

END;