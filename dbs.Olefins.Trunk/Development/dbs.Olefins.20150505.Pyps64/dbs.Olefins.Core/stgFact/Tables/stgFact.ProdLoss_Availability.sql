﻿CREATE TABLE [stgFact].[ProdLoss_Availability] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [Category]       VARCHAR (3)        NOT NULL,
    [CauseId]        VARCHAR (20)       NOT NULL,
    [Description]    VARCHAR (250)      NULL,
    [PrevDTLoss]     REAL               NULL,
    [PrevSDLoss]     REAL               NULL,
    [DTLoss]         REAL               NULL,
    [SDLoss]         REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_ProdLoss_Availability_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdLoss_Availability_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdLoss_Availability_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdLoss_Availability_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_ProdLoss_Availability] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Category] ASC, [CauseId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_ProdLoss_Availability_u]
	ON [stgFact].[ProdLoss_Availability]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[ProdLoss_Availability]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[ProdLoss_Availability].Refnum		= INSERTED.Refnum
		AND	[stgFact].[ProdLoss_Availability].Category	= INSERTED.Category
		AND	[stgFact].[ProdLoss_Availability].CauseId		= INSERTED.CauseId;

END;