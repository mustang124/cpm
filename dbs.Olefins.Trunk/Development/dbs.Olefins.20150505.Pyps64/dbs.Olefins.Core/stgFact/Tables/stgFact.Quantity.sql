﻿CREATE TABLE [stgFact].[Quantity] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdId]     VARCHAR (20)       NOT NULL,
    [Q1Feed]         REAL               NULL,
    [Q2Feed]         REAL               NULL,
    [Q3Feed]         REAL               NULL,
    [Q4Feed]         REAL               NULL,
    [AnnFeedProd]    REAL               NULL,
    [RecPcnt]        REAL               NULL,
    [MiscFeed]       VARCHAR (35)       NULL,
    [OthProdDesc]    VARCHAR (35)       NULL,
    [MiscProd1]      VARCHAR (50)       NULL,
    [MiscProd2]      VARCHAR (50)       NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Quantity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Quantity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Quantity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Quantity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Quantity] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Quantity_u]
	ON [stgFact].[Quantity]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Quantity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Quantity].Refnum		= INSERTED.Refnum
		AND	[stgFact].[Quantity].FeedProdId	= INSERTED.FeedProdId;

END;