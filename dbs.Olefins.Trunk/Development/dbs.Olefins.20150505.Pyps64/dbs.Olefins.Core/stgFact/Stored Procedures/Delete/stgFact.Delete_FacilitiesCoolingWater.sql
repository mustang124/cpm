﻿CREATE PROCEDURE [stgFact].[Delete_FacilitiesCoolingWater]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[FacilitiesCoolingWater]
	WHERE [Refnum] = @Refnum;

END;