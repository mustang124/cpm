﻿CREATE PROCEDURE [stgFact].[Insert_TADTMini]
(
	@Refnum						VARCHAR(25),
	@TrainId					TINYINT,

	@MiniTaDowntime_Hrs			REAL	= NULL,
	@MiniTaCost_USD				REAL	= NULL,
	@MiniTaMaintWork_ManHrs		REAL	= NULL,
	@MiniTa_Date				DATE	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO [stgFact].[TADTMini]
	(
		[Refnum],
		[TrainId],
		[MiniTaDowntime_Hrs],
		[MiniTaCost_USD],
		[MiniTaMaintWork_ManHrs],
		[MiniTa_Date]
	)
	VALUES
	(
		@Refnum,
		@TrainId,
		@MiniTaDowntime_Hrs,
		@MiniTaCost_USD,
		@MiniTaMaintWork_ManHrs,
		@MiniTa_Date
	);

END