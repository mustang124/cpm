﻿CREATE PROCEDURE [fact].[Select_StreamsLight]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		--	Table 2A-1, Table 2A-2, Table 2B, Table 2C, Table 3
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[StreamIdInt]		= CASE WHEN [f].[StreamId] IN ('FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										[f].[StreamId] + SUBSTRING([f].[StreamDescription], LEN([f].[StreamDescription]) - 1, 1)
									ELSE
										[f].[StreamId]
									END,
		[f].[StreamDescription],
			[StreamDesc]		= CASE WHEN [f].[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										LEFT([f].[StreamDescription], LEN([f].[StreamDescription]) - CHARINDEX('(', REVERSE([f].[StreamDescription])))
									END,
		[f].[Q1_kMT],
		[f].[Q2_kMT],
		[f].[Q3_kMT],
		[f].[Q4_kMT],

		--	Table 2A-1, Table 2A-2, Table 2B, Table 2C
		[p].[CoilOutletPressure_Psia],
		[p].[CoilOutletTemp_C],
		[p].[CoilInletPressure_Psia],
		[p].[CoilInletTemp_C],
		[p].[RadiantWallTemp_C],
		[p].[SteamHydrocarbon_Ratio],
		[p].[EthyleneYield_WtPcnt],
		[p].[PropyleneEthylene_Ratio],
		[p].[PropyleneMethane_Ratio],
		[p].[FeedConv_WtPcnt],
		[p].[DistMethodId],
		[p].[SulfurContent_ppm],
		[p].[Density_SG],
		[p].[FurnConstruction_Year],
		[p].[ResidenceTime_s],
		[p].[FlowRate_KgHr],
		[p].[Recycle_Bit],

		--	Table 2A-1, Table 2B, Table 3
		[CH4]	= COALESCE([c].[CH4],	[s].[CH4]),
		[C2H2]	= COALESCE([c].[C2H2],	[s].[C2H2]),
		[C2H4]	= COALESCE([c].[C2H4],	[s].[C2H4]),
		[C2H6]	= COALESCE([c].[C2H6],	[s].[C2H6]),

		[C3H6]	= COALESCE([c].[C3H6],	[s].[C3H6]),
		[C3H8]	= COALESCE([c].[C3H8],	[s].[C3H8]),

		[C4H6]	= COALESCE([c].[C4H6],	[s].[BUTAD]),
		[C4H8]	= COALESCE([c].[C4H8],	[s].[C4S]),
		[C4H10]	= COALESCE([c].[C4H10],	[s].[C4H10]),

		[C6H6]	= COALESCE([c].[C6H6],	[s].[BZ]),
		[c].[C7H16],
		[c].[C8H18],

		[c].[NBUTA],
		[c].[IBUTA],
		[c].[IB],
		[c].[B1],
		[c].[NC5],
		[c].[IC5],
		[c].[NC6],
		[c].[C6ISO],
		[c].[CO_CO2],

		[H2]	= COALESCE([c].[H2], [s].[H2]),
		[c].[S],

		[Amount_Cur]	= COALESCE([v].[Amount_Cur], [q].[Amount_Cur])

	FROM
		[fact].[QuantityPivot]					[f]
	INNER JOIN
		[dim].[Stream_Bridge]					[b]
			ON	[b].[DescendantId]		= [f].[StreamId]
			AND	[b].[FactorSetId]		= '2013'
			AND	[b].[StreamId]			IN ('Light', 'Recycle')
	LEFT OUTER JOIN
		[fact].[FeedStockCrackingParameters]	[p]
			ON	[p].[Refnum]			= [f].[Refnum]
			AND	[p].[CalDateKey]		= [f].[CalDateKey]
			AND	[p].[StreamId]			= [f].[StreamId]
			AND	[p].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[fact].[CompositionQuantityPivot]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[StreamId]			= [f].[StreamId]
			AND	[c].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[fact].[QuantityValue]					[v]
			ON	[v].[Refnum]			= [f].[Refnum]
			AND	[v].[CalDateKey]		= [f].[CalDateKey]
			AND	[v].[StreamId]			= [f].[StreamId]
			AND	[v].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[super].[Composition]					[s]
			ON	[s].[Refnum]			= [f].[Refnum]
			AND	[s].[StreamId]			= [f].[StreamId]
	CROSS APPLY(
		SELECT
			[Amount_Cur] = AVG([q].[Amount_Cur])
		FROM
			[super].[QuantityValue]					[q]
		WHERE	[q].[Refnum]			= [f].[Refnum]
			AND	[q].[StreamId]			= [f].[StreamId]
			AND	[q].[StreamDescription]	= [f].[StreamDescription]
		) [q]
	WHERE
		[f].[Refnum]	= @Refnum;

END;