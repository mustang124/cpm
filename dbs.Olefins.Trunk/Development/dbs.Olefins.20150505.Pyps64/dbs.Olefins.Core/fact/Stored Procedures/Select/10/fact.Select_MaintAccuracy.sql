﻿CREATE PROCEDURE [fact].[Select_MaintAccuracy]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 10-1 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccuracyId]
	FROM
		[fact].[MaintAccuracy]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;
