﻿CREATE PROCEDURE [fact].[Select_MaintPers]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 10-2
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[PersId],
		[l].[PersIdPri],
		[l].[PersIdSec],
		[f].[Personnel_Count],
		[f].[StraightTime_Hrs],
		[f].[OverTime_Hrs],
			[Company_Hrs]		= [f].[_Company_Hrs]
	FROM
		[fact].[Pers]			[f]
	INNER JOIN
		[dim].[Pers_LookUp]		[l]
			ON	[l].[PersId]	= [f].[PersId]
	WHERE	[f].[PersId] IN ('OccMaint', 'MpsMaint')
		AND	[f].[Refnum]	= @Refnum;

END;