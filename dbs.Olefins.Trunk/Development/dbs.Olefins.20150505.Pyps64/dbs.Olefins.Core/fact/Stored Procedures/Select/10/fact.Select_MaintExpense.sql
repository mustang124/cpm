﻿CREATE PROCEDURE [fact].[Select_MaintExpense]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 10-2 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[CompMaintHours_Pcnt]
	FROM
		[fact].[MaintExpense]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;