﻿CREATE PROCEDURE [fact].[Select_GenPlant]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-2, Table 8-4 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[EthyleneCarrier_Bit],
			[EthyleneCarrier_YN]		= CASE WHEN [EthyleneCarrier_Bit]		= 1 THEN 'Y' ELSE 'N' END,
		[f].[ExtractButadiene_Bit],
			[ExtractButadiene_YN]		= CASE WHEN [ExtractButadiene_Bit]		= 1 THEN 'Y' ELSE 'N' END,
		[f].[ExtractAromatics_Bit],
			[ExtractAromatics_YN]		= CASE WHEN [ExtractAromatics_Bit]		= 1 THEN 'Y' ELSE 'N' END,
		[f].[MTBE_Bit],
			[MTBE_YN]					= CASE WHEN [MTBE_Bit]					= 1 THEN 'Y' ELSE 'N' END,
		[f].[Isobutylene_Bit],
			[Isobutylene_YN]			= CASE WHEN [Isobutylene_Bit]			= 1 THEN 'Y' ELSE 'N' END,
		[f].[IsobutyleneHighPurity_Bit],
			[IsobutyleneHighPurity_YN]	= CASE WHEN	[IsobutyleneHighPurity_Bit]	= 1 THEN 'Y' ELSE 'N' END,
		[f].[Butene1_Bit],
			[Butene1_YN]				= CASE WHEN	[Butene1_Bit]				= 1 THEN 'Y' ELSE 'N' END,
		[f].[Isoprene_Bit],
			[Isoprene_YN]				= CASE WHEN	[Isoprene_Bit]				= 1 THEN 'Y' ELSE 'N' END,
		[f].[CycloPentadiene_Bit],
			[CycloPentadiene_YN]		= CASE WHEN	[CycloPentadiene_Bit]		= 1 THEN 'Y' ELSE 'N' END,
		[f].[OtherC5_Bit],
			[OtherC5_YN]				= CASE WHEN	[OtherC5_Bit]				= 1 THEN 'Y' ELSE 'N' END,
		[f].[PolyPlantShare_Bit],
			[PolyPlantShare_YN]			= CASE WHEN	[PolyPlantShare_Bit]		= 1 THEN 'Y' ELSE 'N' END,
		[f].[OlefinsDerivatives_Bit],
			[OlefinsDerivatives_YN]		= CASE WHEN	[OlefinsDerivatives_Bit]	= 1 THEN 'Y' ELSE 'N' END,
		[f].[IntegrationRefinery_Bit],
			[IntegrationRefinery_YN]	= CASE WHEN	[IntegrationRefinery_Bit]	= 1 THEN 'Y' ELSE 'N' END,

		[f].[CapCreepAnn_Pcnt],

		[f].[LocationFactor],
		[f].[OSIMPrep_Hrs]
	FROM
		[fact].[GenPlant]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;