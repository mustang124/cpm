﻿CREATE PROCEDURE [fact].[Select_GenPlantDuties]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-1 (3)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[StreamId],
		[f].[CurrencyRpt],
		[f].[Amount_Cur]
	FROM
		[fact].[GenPlantDuties]	[f]
	WHERE	[f].[Refnum]		= @Refnum
		AND	[f].[Amount_Cur]	<> 0.0;

END;