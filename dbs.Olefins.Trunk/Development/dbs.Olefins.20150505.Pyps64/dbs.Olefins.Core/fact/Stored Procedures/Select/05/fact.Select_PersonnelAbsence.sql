﻿CREATE PROCEDURE [fact].[Select_PersonnelAbsence]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 5A, Table 5B
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[PersId],
		[l].[PersIdPri],
		[l].[PersIdSec],
		[f].[Absence_Pcnt]
	FROM
		[fact].[PersAbsence]	[f]
	INNER JOIN
		[dim].[Pers_LookUp]	[l]
			ON	[l].[PersId]	= [f].[PersId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;