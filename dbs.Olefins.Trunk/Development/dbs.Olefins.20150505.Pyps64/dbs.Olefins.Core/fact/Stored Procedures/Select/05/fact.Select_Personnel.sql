﻿CREATE PROCEDURE [fact].[Select_Personnel]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 5A, Table 5B
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[PersId],
		[l].[PersIdPri],
		[l].[PersIdSec],
			[Personnel_Count]	= CASE WHEN [f].[Personnel_Count]	<> 0.0 THEN [f].[Personnel_Count]	END,
			[StraightTime_Hrs]	= CASE WHEN [f].[StraightTime_Hrs]	<> 0.0 THEN [f].[StraightTime_Hrs]	END,
			[OverTime_Hrs]		= CASE WHEN [f].[OverTime_Hrs]		<> 0.0 THEN [f].[OverTime_Hrs]		END,
			[Contract_Hrs]		= CASE WHEN [f].[Contract_Hrs]		<> 0.0 THEN [f].[Contract_Hrs]		END
	FROM
		[fact].[Pers]	[f]
	INNER JOIN
		[dim].[Pers_LookUp]	[l]
			ON	[l].[PersId]	= [f].[PersId]
	WHERE  ([f].[_Tot_Hrs]		<> 0.0 
		OR	[f].[_Tot_Fte]		<> 0.0)
		AND	[f].[Refnum]		= @Refnum;

END;