﻿CREATE PROCEDURE [fact].[Select_EnergyElec]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 7
	SELECT
		[e].[Refnum],
		[e].[CalDateKey],
		[e].[AccountId],
		[e].[Amount_MWh],
		[e].[Rate_BTUkWh],

			[Rate_Imp]	= ROUND([e].[Rate_BTUkWh], 3) * 1000.0,
			[Rate_Met]	= ROUND([$(DbGlobal)].[dbo].[UnitsConv](ROUND([e].[Rate_BTUkWh], 3), 'MBTU', 'GJ'), 3),

		[e].[Price_Cur]
	FROM
		[fact].[EnergyElec]	[e]
	WHERE
		[e].[Refnum]	 = @Refnum;

END;