﻿CREATE PROCEDURE [fact].[Select_ApcExistance]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 9-4 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[ApcId],
		[f].[Mpc_Bit],
		[f].[Sep_Bit],
		[Mpc_X]	= CASE WHEN [f].[Mpc_Bit] = 1 THEN 'X' END,
		[Sep_X]	= CASE WHEN [f].[Sep_Bit] = 1 THEN 'X' END
	FROM
		[fact].[ApcExistance]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;