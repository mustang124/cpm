﻿CREATE PROCEDURE [fact].[Select_MetaOpEx]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 13 (1)
	SELECT
		[u].[Refnum],
		[u].[CurrencyRpt],
		[u].[AccountId],
		[u].[Q1],
		[u].[Q2],
		[u].[Q3],
		[u].[Q4]
	FROM (
		SELECT
				[Qtr]	 = 'Q' + CONVERT(CHAR(1), [c].[CalQtr]),
			[f].[Refnum],
			[f].[AccountId],
			[f].[CurrencyRpt],
			[f].[Amount_Cur]
		FROM
			[fact].[MetaOpEx]	[f]
		INNER JOIN
			[dim].[Calendar_LookUp]	[c]
				ON	[c].[CalDateKey]	= [f].[CalDateKey]
		WHERE	[f].[Refnum]		= @Refnum
			AND	[f].[Amount_Cur]	> 0.0
		) [p]
		PIVOT(
			MAX([p].[Amount_Cur]) FOR [p].[Qtr] IN ([Q1], [Q2], [Q3], [Q4])
		) [u];

END;