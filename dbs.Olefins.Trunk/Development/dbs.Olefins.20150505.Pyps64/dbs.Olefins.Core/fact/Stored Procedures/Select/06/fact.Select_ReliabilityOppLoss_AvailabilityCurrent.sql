﻿CREATE PROCEDURE [fact].[Select_ReliabilityOppLoss_AvailabilityCurrent]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-1
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[OppLossId],
		[f].[DownTimeLoss_Pcnt],
		[f].[SlowDownLoss_Pcnt],
		[f].[CurrentYear]
	FROM
		[fact].[ReliabilityOppLoss_Availability]	[f]
	WHERE	[f].[CurrentYear]	= 1
		AND	[f].[Refnum]		= @Refnum;

END;