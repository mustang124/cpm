﻿CREATE PROCEDURE [fact].[Insert_ReliabilityDTPredMaint]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[ReliabilityDTPredMaint]
		(
			[Refnum],
			[CalDateKey],
			[CauseId],
			[Frequency_Days]
		)
		SELECT
			[t].[Refnum],
			[t].[CalDateKey],
			[t].[CauseId],
			[t].[Frequency_Days]
		FROM (
			SELECT
				[Refnum]			= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
				[CalDateKey]		= [etl].[ConvDateKey]([t].[StudyYear]),
				[Compressor]		= [p].[FreqComp],
				[Stastical]			= [p].[FreqStat],
				[Turbine]			= [p].[FreqTurb],
				[Vibration]			= [p].[FreqVib]
			FROM
				[stgFact].[TSort]		[t]
			INNER JOIN
				[stgFact].[PracRely]	[p]
					ON	[p].[Refnum]	= [t].[Refnum]
			WHERE
				[t].[Refnum]			= @sRefnum
			) [p]
			UNPIVOT (
				[Frequency_Days] FOR [CauseId] IN
				(
					[Compressor],
					[Stastical],
					[Turbine],
					[Vibration]
				)
			) [t]
		WHERE
			[t].[Frequency_Days]	IS NOT NULL


	END TRY
	BEGIN CATCH

		EXECUTE [dbo].[usp_LogError] 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;