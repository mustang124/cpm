﻿CREATE PROCEDURE [fact].[Insert_FeedSelModelPenalty]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSelModelPenalty(Refnum, CalDateKey, PenaltyId, Penalty_Bit)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.PenaltyID
			, etl.ConvBit(u.Penalty)
		FROM (
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, r.FeedSlateChg	[FeedSlateChange]
				, r.Reliab			[Reliability]
				, r.Environ			[Envrionment]
				, r.Furnace			[FurnRunLength]
				, r.Maint			[Maintenance]
				, r.Interest		[Inventory]
			FROM stgFact.FeedResources r
			INNER JOIN stgFact.TSort t ON t.Refnum = r.Refnum
			WHERE	t.Refnum = @sRefnum
			) t
			UNPIVOT (
			Penalty FOR PenaltyID IN (
				  FeedSlateChange
				, Reliability
				, Envrionment
				, FurnRunLength
				, Maintenance
				, Inventory
				)
			) u
		WHERE u.Penalty IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;