﻿CREATE PROCEDURE [fact].[Insert_FeedSelPersonnel]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSelPersonnel(Refnum, CalDateKey, PersId, Plant_FTE, Corporate_FTE)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvPersId(ISNULL(p.PersId, c.PersId))
			, p.Plant
			, c.Corporate
		FROM (
			SELECT
				  r.Refnum
				, r.Plant_PPCnt			[BEAProdPlan]
				, r.Plant_3PlusCnt		[BEAProdPlanExperienced]
				, r.Plant_BuyerCnt		[BEATrader]
				, r.Plant_DataCnt		[BEADataAnalyst]
				, r.Plant_Logistic		[BEALogistics]
			FROM stgFact.FeedResources r
			WHERE r.Refnum = @sRefnum
			) r
			UNPIVOT (
			Plant FOR PersId IN(
				  BEAProdPlan
				, BEAProdPlanExperienced
				, BEATrader
				, BEADataAnalyst
				, BEALogistics
				)
			) p
		FULL OUTER JOIN (
			SELECT
				  r.Refnum
				, r.Corp_PPCnt			[BEAProdPlan]
				, r.Corp_3PlusCnt		[BEAProdPlanExperienced]
				, r.Corp_BuyerCnt		[BEATrader]
				, r.Corp_DataCnt		[BEADataAnalyst]
				, r.Corp_Logistic		[BEALogistics]
			FROM stgFact.FeedResources r
			WHERE r.Refnum = @sRefnum
			) r
			UNPIVOT (
			Corporate FOR PersId IN(
				  BEAProdPlan
				, BEAProdPlanExperienced
				, BEATrader
				, BEADataAnalyst
				, BEALogistics
				)
			) c
		ON c.Refnum = p.Refnum AND c.PersId = p.PersId
		INNER JOIN stgFact.TSort t ON t.Refnum = ISNULL(p.Refnum, c.Refnum)
		WHERE	t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;