﻿CREATE PROCEDURE [fact].[Insert_EnergyElec]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.EnergyElec(Refnum, CalDateKey, AccountId, Amount_MWh, Rate_BTUkWh, CurrencyRpt, Price_Cur)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)		[Refnum],
			etl.ConvDateKey(t.StudyYear)						[CalDateKey],
			etl.ConvEnergyID(e.EnergyType)						[AccountId],
			e.Amount											[Amount],
			COALESCE(e.MBTU / e.Amount, 9.09)					[Rate],
			'USD',
			e.Cost
		FROM stgFact.EnergyQnty e
		INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
		WHERE	e.EnergyType IN ('ElecPower', 'ExpElecPower')
			AND	e.Amount > 0.0
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;