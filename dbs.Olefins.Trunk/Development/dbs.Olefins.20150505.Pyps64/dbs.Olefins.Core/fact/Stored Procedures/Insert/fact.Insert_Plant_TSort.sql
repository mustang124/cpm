﻿CREATE PROCEDURE [fact].[Insert_Plant_TSort]
(
	@Refnum		VARCHAR(18)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	PRINT NCHAR(9) + 'INSERT INTO fact.TSortClient';

	INSERT INTO fact.TSortClient(Refnum, CalDateKey,
		PlantAssetName,
		PlantCompanyName,
		UomId,
		CurrencyFcn,
		CurrencyRpt,
		
		[CoordName],
		[CoordTitle],
		[POBox],
		[Street],
		[City],
		[State],
		[Country],
		[Zip],
		[Telephone],
		[Fax],
		[WWW],
		[PricingContact],
		[PricingContactEmail],
		[DCContact],
		[DCContactEmail]
		)
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
		etl.ConvDateKey(t.StudyYear)								[CalDateKey],
		CASE WHEN LEN(t.PlantName) > 0 THEN ISNULL(t.PlantName, ISNULL(t.Loc, ISNULL(t.PlantLoc, N''))) ELSE N'————' END,
		CASE WHEN LEN(t.CoName) > 0 THEN ISNULL(t.CoName, ISNULL(etl.ConvCompanyId(t.CoName, t.CompanyId, t.Co), N'————')) ELSE N'————' END,
		CASE ISNULL(t.UOM, 1)
			WHEN 1 THEN 'US'
			WHEN 2 THEN 'Metric'
			WHEN '' THEN 'Metric'
			END														[UOM],
			
		CASE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
			WHEN '2005PCH185' THEN 'RON'
			WHEN '2009PCH211' THEN 'TRLK'
			WHEN '2009PCH213' THEN 'RUB'
			WHEN '2009PCH214' THEN 'RUB'
			WHEN '2009PCH217' THEN 'RUB'
			ELSE k.CurrencyId
			END														[CurrencyFcn],
		'USD'														[CurrencyRpt],
		
		t.[CoordName],
		t.[CoordTitle],
		t.[POBox],
		t.[Street],
		t.[City],
		t.[State],
		t.[Country],
		t.[Zip],
		t.[Telephone],
		t.[Fax],
		t.[WWW],
		t.[PricingContact],
		t.[PricingContactEmail],
		t.[DCContact],
		t.[DCContactEmail]

	FROM stgFact.TSort t
	INNER JOIN cons.SubscriptionsAssets s
		ON	s.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
	INNER JOIN cons.Assets a
		ON	a.AssetId = s.AssetId
	INNER JOIN ante.CountriesCurrencies k
		ON	k.CountryId = a.CountryId
		AND k.InflationUse = 1
		AND k.CurrencyId NOT IN ('BRCK', 'TRLM', 'RUR', 'ROLK')
	LEFT OUTER JOIN fact.TSortClient	tc
		ON	tc.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
	WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		AND	tc.Refnum IS NULL;

	PRINT NCHAR(9) + 'INSERT INTO fact.TSortContact (DataCoordinator)';

	INSERT INTO fact.TSortContact([Refnum], [ContactTypeId], [NameFull], [NameTitle],
		[AddressPOBox], [AddressStreet], [AddressCity], [AddressState], [AddressCountry], [AddressCountryId], [AddressPostalCode],
		[NumberVoice], [NumberFax], [NumberMobile], [eMail])
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
		'Coord',
		t.CoordName,
		CASE WHEN t.CoordTitle	<> '' THEN t.CoordTitle		ELSE NULL END,

		CASE WHEN t.POBox		<> '' THEN t.POBox			ELSE NULL END,
		CASE WHEN t.Street		<> '' THEN t.Street			ELSE NULL END,
		CASE WHEN t.City		<> '' THEN t.City			ELSE NULL END,
		CASE WHEN t.[State]		<> '' THEN t.[State]		ELSE NULL END,
		CASE WHEN t.Country		<> '' THEN t.Country		ELSE NULL END,
		etl.ConvCountryId(t.Refnum, t.Country),
		CASE WHEN t.Zip			<> '' THEN t.Zip			ELSE NULL END,

		CASE WHEN t.Telephone	<> '' THEN t.Telephone		ELSE NULL END,
		CASE WHEN t.Fax			<> '' THEN t.Fax			ELSE NULL END,
		NULL,
		CASE WHEN t.WWW			<> '' THEN t.WWW			ELSE NULL END
	FROM stgFact.TSort				t
	INNER JOIN fact.TSortClient		c
		ON	c.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
	LEFT OUTER JOIN fact.TSortContact	x
		ON	x.Refnum = c.Refnum
		AND	x.ContactTypeId = 'Coord'
	WHERE	t.CoordName IS NOT NULL
		AND	t.CoordName <> ''
		AND	x.Refnum IS NULL
		AND c.Refnum = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.TSortContact (Pricing)';

	INSERT INTO fact.TSortContact([Refnum], [ContactTypeId], [NameFull],
		[eMail])
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
		'PricingCoord',
		t.PricingContact,
		CASE WHEN t.PricingContactEmail			<> '' THEN t.PricingContactEmail			ELSE NULL END
	FROM stgFact.TSort				t
	INNER JOIN fact.TSortClient		c
		ON	c.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
	LEFT OUTER JOIN fact.TSortContact	x
		ON	x.Refnum = c.Refnum
		AND	x.ContactTypeId = 'PricingCoord'
	WHERE	t.PricingContact IS NOT NULL
		AND	t.PricingContact <> ''
		AND	x.Refnum IS NULL
		AND c.Refnum = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.TSortContact (Secondary)';

	INSERT INTO fact.TSortContact([Refnum], [ContactTypeId], [NameFull],
		[eMail])
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
		'DataCoord',
		t.DCContact,
		CASE WHEN t.DCContactEmail	<> '' THEN t.DCContactEmail	ELSE NULL END
	FROM stgFact.TSort				t
	INNER JOIN fact.TSortClient		c
		ON	c.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
	LEFT OUTER JOIN fact.TSortContact	x
		ON	x.Refnum = c.Refnum
		AND	x.ContactTypeId = 'DataCoord'
	WHERE	t.DCContact IS NOT NULL
		AND	t.DCContact <> ''
		AND	x.Refnum IS NULL
		AND c.Refnum = @Refnum;

END;