﻿CREATE VIEW fact.QuantityAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	q.Refnum,
	q.CalDateKey,
	b.StreamId,
	p.StreamDescription,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + q.Q1_kMT
		WHEN '-' THEN - q.Q1_kMT
		END)							[Q1_kMT],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + q.Q2_kMT
		WHEN '-' THEN - q.Q2_kMT
		END)							[Q2_kMT],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + q.Q3_kMT
		WHEN '-' THEN - q.Q3_kMT
		END)							[Q3_kMT],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + q.Q4_kMT
		WHEN '-' THEN - q.Q4_kMT
		END)							[Q4_kMT],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + q.Tot_kMT
		WHEN '-' THEN - q.Tot_kMT
		END)							[Tot_kMT]

FROM dim.Stream_Bridge					b
INNER JOIN fact.QuantityPivot			q
	ON	q.StreamId = b.DescendantId
LEFT OUTER JOIN fact.QuantityPivot		p
	ON	p.Refnum = q.Refnum
	AND	p.StreamId = b.StreamId
	AND	p.StreamDescription = q.StreamDescription
GROUP BY
	b.FactorSetId,
	q.Refnum,
	q.CalDateKey,
	b.StreamId,
	p.StreamDescription;