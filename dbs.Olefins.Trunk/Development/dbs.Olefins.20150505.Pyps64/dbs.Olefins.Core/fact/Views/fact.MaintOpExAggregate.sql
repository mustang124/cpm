﻿CREATE VIEW [fact].[MaintOpExAggregate]
WITH SCHEMABINDING
AS
SELECT
	[t].[Refnum],
		[AccountId]			= [a].[DescendantId],
	[t].[CurrencyRpt],

		[CalDateKeyPrev]	= [p].[CalDateKey],
		[AmountPrev_Cur]	= [p].[Amount_Cur],

		[CalDateKey]		= [c].[CalDateKey],
		[Amount_Cur]		= [c].[Amount_Cur],

		[AmountAvg_Cur]		= (COALESCE([p].[Amount_Cur], 0.0) + COALESCE([c].[Amount_Cur], 0.0)) / 2.0

FROM
	[fact].[TSortClient]		[t]

INNER JOIN
	[dim].[Account_Bridge]		[a]
		ON	[a].[FactorSetId]	= [t].[_DataYear]
		AND	[a].[AccountId]		= 'MaintRoutine'

LEFT OUTER JOIN
	[fact].[OpExAggregate]		[p]	WITH(NOEXPAND)
		ON	[p].[FactorSetId]	= [a].[FactorSetId]
		AND	[p].[AccountId]		= [a].[DescendantId]
		AND	[p].[Refnum]		= [t].[Refnum]
		AND	[p].[CurrencyRpt]	= [t].[CurrencyRpt]
		AND	[p].[CalDateKey]	= ([t].[CalDateKey] - 10000)

LEFT OUTER JOIN
	[fact].[OpExAggregate]		[c]	WITH(NOEXPAND)
		ON	[c].[FactorSetId]	= [a].[FactorSetId]
		AND	[c].[AccountId]		= [a].[DescendantId]
		AND	[c].[Refnum]		= [t].[Refnum]
		AND	[c].[CurrencyRpt]	= [t].[CurrencyRpt]
		AND	[c].[CalDateKey]	= [t].[CalDateKey]

WHERE	[p].[Amount_Cur]	IS NOT NULL
	AND	[c].[Amount_Cur]	IS NOT NULL;