﻿CREATE FUNCTION [fact].[PlantRecycles]
(
	@FactorSetId	VARCHAR(12),
	@Refnum			VARCHAR(25)
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
(
SELECT
	[FactorSetId]	= @FactorSetId,
	[t].[Refnum],
	[t].[CalDateKey],
	[RecycleId] = COALESCE(SUM([r].[RecycleId]), 0)
FROM (
	SELECT
		[t].[Refnum],
		[t].[CalDateKey],
		[q].[StreamId]
	FROM
		[fact].[TSortClient]						[t]
	INNER JOIN
		[fact].[Quantity]							[q]
			ON	[q].[Refnum]			= @Refnum
			AND	[q].[Refnum]			= [t].[Refnum]
			AND	[q].[Quantity_kMT]		> 0.0
			AND	[q].[StreamId] IN ('ButRec', 'EthRec', 'ProRec')
	INNER JOIN
		[fact].[CompositionQuantity]				[c]
			ON	[c].[Refnum]			= @Refnum
			AND	[c].[Refnum]			= [q].[Refnum]
			AND	[c].[StreamId]			= [q].[StreamId]
			AND	[c].[StreamDescription]	= [q].[StreamDescription]
			AND	[c].[Component_WtPcnt]	> 0.0
	WHERE	[t].[Refnum]				= @Refnum
	GROUP BY
		[t].[Refnum],
		[t].[CalDateKey],
		[q].[StreamId]
	HAVING	SUM([q].[Quantity_kMT] * [c].[Component_WtPcnt]) > 0.0
		AND	ABS(100.0 * COUNT(DISTINCT q.[Quantity_kMT]) - SUM(c.[Component_WtPcnt])) / 100.0 / COUNT(DISTINCT q.[Quantity_kMT]) <= 10.0
	) [t]
LEFT OUTER JOIN [ante].[RecycleOperation]			[r]
	ON	[r].[FactorSetId]	= @FactorSetId
	AND	[r].[StreamId]		= [t].[StreamId]
GROUP BY
	[t].[Refnum],
	[t].[CalDateKey]
);