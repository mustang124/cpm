﻿CREATE TABLE [fact].[ReliabilityTAMini]
(
	[Refnum]							VARCHAR (25)		NOT	NULL	CONSTRAINT [FK_ReliabilityTAMini_TSort]					REFERENCES [fact].[TSortClient]([Refnum]),
	[CalDateKey]						INT					NOT	NULL	CONSTRAINT [FK_ReliabilityTAMini_Calendar_LookUp]		REFERENCES [dim].[Calendar_LookUp]([CalDateKey]),
	[TrainId]							INT					NOT	NULL	CONSTRAINT [CR_ReliabilityTAMini_TrainId]				CHECK([TrainId]>(0)),

	[CurrencyRpt]						VARCHAR (4)				NULL	CONSTRAINT [FK_ReliabilityTAMini_Currency_LookUp]		REFERENCES [dim].[Currency_LookUp]([CurrencyId]),

	[DownTime_Hrs]						REAL					NULL	CONSTRAINT [CR_ReliabilityTAMini_DownTime_Hrs]			CHECK ([DownTime_Hrs]>=(0.0)),
	[TurnAroundCost_MCur]				REAL					NULL	CONSTRAINT [CR_ReliabilityTAMini_TurnAroundCost_MCur]	CHECK ([TurnAroundCost_MCur]>=(0.0)),
	[TurnAround_ManHrs]					REAL					NULL	CONSTRAINT [CR_ReliabilityTAMini_TurnAround_ManHrs]		CHECK ([TurnAround_ManHrs]>=(0.0)),
	[TurnAround_Date]					DATE					NULL	CONSTRAINT [CR_ReliabilityTAMini_TurnAround_Date]		CHECK (DATEPART(YEAR,[TurnAround_Date])>=(1965)),

	[_DownTime_Days]					AS					(CONVERT(REAL, [DownTime_Hrs] / 24.0, 0))
										PERSISTED,
	[_CostPerManHour_Cur]				AS					(CONVERT(REAL, CASE WHEN [TurnAround_ManHrs] <> 0.0 THEN ([TurnAroundCost_MCur] / [TurnAround_ManHrs])* 1000000.0   END, 1))
										PERSISTED,

	[tsModified]						DATETIMEOFFSET (7)	NOT	NULL	CONSTRAINT [DF_ReliabilityTAMini_tsModified]			DEFAULT (SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR (168)		NOT	NULL	CONSTRAINT [DF_ReliabilityTAMini_tsModifiedHost]		DEFAULT (HOST_NAME()),
	[tsModifiedUser]					NVARCHAR (168)		NOT	NULL	CONSTRAINT [DF_ReliabilityTAMini_tsModifiedUser]		DEFAULT (SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR (168)		NOT	NULL	CONSTRAINT [DF_ReliabilityTAMini_tsModifiedApp]			DEFAULT (APP_NAME()),

	CONSTRAINT [PK_ReliabilityTAMini]	PRIMARY KEY CLUSTERED([Refnum] ASC, [TrainId] ASC, [CalDateKey] ASC),
);
GO

CREATE TRIGGER [fact].[t_ReliabilityTAMini_u]
	ON [fact].[ReliabilityTAMini]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityTAMini]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityTAMini].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityTAMini].TrainId		= INSERTED.TrainId
		AND [fact].[ReliabilityTAMini].CalDateKey	= INSERTED.CalDateKey;

END;