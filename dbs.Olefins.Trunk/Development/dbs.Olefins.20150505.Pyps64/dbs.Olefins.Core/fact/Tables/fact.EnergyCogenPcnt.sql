﻿CREATE TABLE [fact].[EnergyCogenPcnt] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [AccountId]        VARCHAR (42)       NOT NULL,
    [FacilityId]       VARCHAR (42)       NOT NULL,
    [CogenEnergy_Pcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyCogenPcnt_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenPcnt_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenPcnt_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenPcnt_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyCogenPcnt] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyCogenPcnt_CogenEnergy_Pcnt] CHECK ([CogenEnergy_Pcnt]>=(0.0) AND [CogenEnergy_Pcnt]<=(100.0)),
    CONSTRAINT [FK_EnergyCogenPcnt_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_EnergyCogenPcnt_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyCogenPcnt_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_EnergyCogenPcnt_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergyCogenPcnt_u]
	ON [fact].[EnergyCogenPcnt]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyCogenPcnt]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyCogenPcnt].Refnum		= INSERTED.Refnum
		AND [fact].[EnergyCogenPcnt].AccountId	= INSERTED.AccountId
		AND [fact].[EnergyCogenPcnt].CalDateKey	= INSERTED.CalDateKey;

END;