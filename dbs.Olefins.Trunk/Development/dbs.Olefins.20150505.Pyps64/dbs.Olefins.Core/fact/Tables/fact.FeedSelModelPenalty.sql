﻿CREATE TABLE [fact].[FeedSelModelPenalty] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PenaltyId]      VARCHAR (42)       NOT NULL,
    [Penalty_Bit]    BIT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelModelPenalty_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelPenalty_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelPenalty_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelPenalty_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSelModelPenalty] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PenaltyId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_FeedSelModelPenalty_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSelModelPenalty_FeedSelPenalty_LookUp] FOREIGN KEY ([PenaltyId]) REFERENCES [dim].[FeedSelPenalty_LookUp] ([PenaltyId]),
    CONSTRAINT [FK_FeedSelModelPenalty_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSelModelPenalty_u]
	ON [fact].[FeedSelModelPenalty]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSelModelPenalty]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSelModelPenalty].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSelModelPenalty].PenaltyId	= INSERTED.PenaltyId
		AND [fact].[FeedSelModelPenalty].CalDateKey	= INSERTED.CalDateKey;

END;