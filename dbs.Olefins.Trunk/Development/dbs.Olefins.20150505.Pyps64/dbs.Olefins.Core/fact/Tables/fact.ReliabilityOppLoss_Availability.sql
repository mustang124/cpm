﻿CREATE TABLE [fact].[ReliabilityOppLoss_Availability] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [OppLossId]         VARCHAR (42)       NOT NULL,
    [DownTimeLoss_Pcnt] REAL               NULL,
    [SlowDownLoss_Pcnt] REAL               NULL,
    [CurrentYear]       AS                 (case when left([Refnum],(4))=left([CalDateKey],(4)) then (1) else (0) end) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityOppLoss_Availability_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_Availability_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_Availability_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_Availability_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityOppLoss_Availability] PRIMARY KEY CLUSTERED ([Refnum] ASC, [OppLossId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ReliabilityOppLoss_Availability_DownTimeLoss_Pcnt] CHECK ([DownTimeLoss_Pcnt]>=(0.0) AND [DownTimeLoss_Pcnt]<=(100.0)),
    CONSTRAINT [CR_ReliabilityOppLoss_Availability_SlowDownLoss_Pcnt] CHECK ([SlowDownLoss_Pcnt]>=(0.0) AND [SlowDownLoss_Pcnt]<=(100.0)),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_OppLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Availability_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityOppLoss_Availability_u]
	ON [fact].[ReliabilityOppLoss_Availability]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityOppLoss_Availability]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityOppLoss_Availability].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityOppLoss_Availability].OppLossId	= INSERTED.OppLossId
		AND [fact].[ReliabilityOppLoss_Availability].CalDateKey	= INSERTED.CalDateKey;

END;