﻿CREATE TABLE [fact].[FacilitiesMisc] (
    [Refnum]             VARCHAR (25)       NOT NULL,
    [CalDateKey]         INT                NOT NULL,
    [PyroFurnSpare_Pcnt] REAL               NULL,
    [ElecGen_MW]         REAL               NULL,
    [StreamId]           VARCHAR (42)       NULL,
    [StreamShip_Pcnt]    REAL               NULL,
    [AcetylLocId]        CHAR (1)           NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesMisc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_FacilitiesMisc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_FacilitiesMisc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_FacilitiesMisc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FacilitiesMisc] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FacilitiesMisc_ElecGenMW] CHECK ([ElecGen_MW]>=(0.0)),
    CONSTRAINT [CR_FacilitiesMisc_PyroFurnSpare_Pcnt] CHECK ([PyroFurnSpare_Pcnt]>=(0.0) AND [PyroFurnSpare_Pcnt]<=(100.0)),
    CONSTRAINT [CR_FacilitiesMisc_StreamShip_Pcnt] CHECK ([StreamShip_Pcnt]>=(0.0) AND [StreamShip_Pcnt]<=(100.0)),
    CONSTRAINT [FK_FacilitiesMisc_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FacilitiesMisc_FacilitiesMisc_LookUp] FOREIGN KEY ([AcetylLocId]) REFERENCES [dim].[FacilityAcetylLoc_LookUp] ([AcetylLocId]),
    CONSTRAINT [FK_FacilitiesMisc_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FacilitiesMisc_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FacilitiesMisc_u]
	ON [fact].[FacilitiesMisc]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[FacilitiesMisc]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FacilitiesMisc].Refnum		= INSERTED.Refnum
		AND [fact].[FacilitiesMisc].CalDateKey	= INSERTED.CalDateKey;

END;