﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadFacilities(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			try
			{
				string cT01_03 = (includeSpace) ? "Table 1-3" : "Table1-3";
				wks = wkb.Worksheets[cT01_03];

				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Facilities]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum				CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						#region Group 1

						//@FracFeedCnt			TINYINT			= NULL,
						r = 6;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@FracFeedCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@FracFeedKBSD			REAL			= NULL,
						r = 6;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FracFeedKBSD", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FracFeed				VARCHAR (20)	= NULL,
						r = 6;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FracFeed", SqlDbType.VarChar, 20).Value = ReturnString(rng, 20); }

						//@PyrFurnCnt			TINYINT			= NULL,
						r = 7;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@PyrFurnCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@PyrFurnSparePcnt		REAL			= NULL,
						r = 7;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PyrFurnSparePcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@QTowerCnt			TINYINT			= NULL,
						r = 8;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@QTowerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@QWaterSysCnt			TINYINT			= NULL,
						r = 9;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@QWaterSysCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@GasFracCnt			TINYINT			= NULL,
						r = 10;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@GasFracCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@ProcDryerCnt			TINYINT			= NULL,
						r = 11;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@ProcDryerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						#endregion

						#region Gas Compressors

						//@GasCompCnt			TINYINT			= NULL,
						r = 13;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@GasCompCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@GasCompStageCnt		TINYINT			= NULL,
						r = 12;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@GasCompStageCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@GasCompAge			REAL			= NULL,
						r = 13;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@GasCompAge", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@GasCompBHP			REAL			= NULL,
						r = 12;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@GasCompBHP", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@GasCompDriver		CHAR (9)		= NULL,
						r = 13;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@GasCompDriver", SqlDbType.VarChar, 9).Value = ReturnString(rng, 9); }

						//@GasCompCoat			VARCHAR (75)	= NULL,
						r = 14;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@GasCompCoat", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

						#endregion

						#region Group 3

						//@CTowerCnt			TINYINT			= NULL,
						r = 14;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@CTowerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@MethSysCnt			TINYINT			= NULL,
						r = 15;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@MethSysCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@DemethTowerCnt		TINYINT			= NULL,
						r = 16;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@DemethTowerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@DemethTowerPSIG		REAL			= NULL,
						r = 16;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DemethTowerPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@DeethTowerCnt		TINYINT			= NULL,
						r = 17;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@DeethTowerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@DeethTowerPSIG		REAL			= NULL,
						r = 17;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DeethTowerPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@AcetylCnt			TINYINT			= NULL,
						r = 18;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@AcetylCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@AcetylLoc			CHAR (1)		= NULL,
						if (StudyYear >= 1995 && StudyYear <= 1999)
						{
							//Acetylene and MAPD Conversion
						}

						//@EthylFracCnt			TINYINT			= NULL,
						r = 19;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) cmd.Parameters.Add("@EthylFracCnt", SqlDbType.TinyInt).Value = ReturnByte(rng);

						//@EthylFracPSIG		REAL			= NULL,
						r = 19;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthylFracPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@DepropTowerCnt		TINYINT			= NULL,
						r = 20;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DepropTowerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@DepropTowerPSIG		REAL			= NULL,
						r = 20;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DepropTowerPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PropylFracCnt		TINYINT			= NULL,
						r = 21;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropylFracCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@PropylFracPSIG		REAL			= NULL,
						r = 21;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropylFracPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@DebutTowerCnt		TINYINT			= NULL,
						r = 22;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DebutTowerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						#endregion

						#region Propylene Compressor

						//@PropCompCnt			TINYINT			= NULL,
						r = 23;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropCompCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@PropCompAge			REAL			= NULL,
						r = 23;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropCompAge", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PropCompBHP			REAL			= NULL,
						r = 23;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropCompBHP", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PropCompDriver		CHAR (9)		= NULL,
						r = 24;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropCompDriver", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

						//@PropCompCoat			VARCHAR (50)	= NULL,
						r = 24;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropCompCoat", SqlDbType.VarChar, 50).Value = ReturnString(rng, 50); }

						#endregion

						#region Ethylene Compressor

						//@EthyCompCnt			TINYINT			= NULL,
						r = 25;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthyCompCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@EthyCompAge			REAL			= NULL,
						r = 25;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthyCompAge", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@EthyCompBHP			REAL			= NULL,
						r = 25;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthyCompBHP", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@EthyCompDriver		CHAR (9)		= NULL,
						r = 26;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthyCompDriver", SqlDbType.VarChar, 9).Value = ReturnString(rng, 9); }

						//@EthyCompCoat			VARCHAR (50)	= NULL,
						r = 26;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthyCompCoat", SqlDbType.VarChar, 50).Value = ReturnString(rng, 50); }

						#endregion

						#region Methane Compressor

						//@MethCompCnt			TINYINT			= NULL,
						r = 27;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MethCompCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@MethCompAge			REAL			= NULL,
						r = 27;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MethCompAge", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MethCompBHP			REAL			= NULL,
						r = 27;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MethCompBHP", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MethCompDriver		CHAR (9)		= NULL,
						r = 28;
						c = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MethCompDriver", SqlDbType.VarChar, 9).Value = ReturnString(rng, 9); }

						//@MethCompCoat			VARCHAR (50)	= NULL,
						r = 28;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MethCompCoat", SqlDbType.VarChar, 50).Value = ReturnString(rng, 50); }

						#endregion

						#region HP Boil

						//@HPBoilCnt			TINYINT			= NULL,
						r = 32;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HPBoilCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@HPBoilPSIG			REAL			= NULL,
						r = 32;
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HPBoilPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HPBoilRate			REAL			= NULL,
						r = 32;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HPBoilRate", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region LP Boil

						//@LPBoilCnt			TINYINT			= NULL,
						r = 33;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LPBoilCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@LPBoilPSIG			REAL			= NULL,
						r = 33;
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LPBoilPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@LPBoilRate			REAL			= NULL,
						r = 33;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LPBoilRate", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Steam Superheaters

						//@SteamHeatCnt			TINYINT			= NULL,
						r = 34;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@SteamHeatCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@SteamHeatPSIG		REAL			= NULL,
						r = 34;
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@SteamHeatPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@SteamHeatRate		REAL			= NULL,
						r = 34;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@SteamHeatRate", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Electricity

						//@ElecGenCnt			TINYINT			= NULL,
						r = 35;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ElecGenCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@ElecGenMW			REAL			= NULL,
						r = 35;
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ElecGenMW", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Cooling Tower

						//@CoolTowerCnt			TINYINT			= NULL,
						r = 36;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CoolTowerCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						#endregion

						#region Hydro

						//@HydroCryoCnt			TINYINT			= NULL,
						r = 38;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HydroCryoCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@HydroPSACnt			TINYINT			= NULL,
						r = 39;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HydroPSACnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@HydroMembCnt			TINYINT			= NULL,
						r = 40;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HydroMembCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						#endregion

						#region Storage

						//@FeedStorCnt		TINYINT			= NULL,
						r = 41;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedStorCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@AboveStorCnt		TINYINT			= NULL,
						r = 42;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AboveStorCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@UnderStorCnt		TINYINT			= NULL,
						r = 43;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@UnderStorCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						#endregion

						#region Ethylene Liquification

						//@EthyLiqCnt			TINYINT			= NULL,
						r = 44;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthyLiqCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@EthyLiqShipPcnt		REAL			= NULL,
						//if (StudyYear >= 1989 && StudyYear <= 1995)
						//{
						//	//EthyLiqShipPcnt
						//}

						#endregion

						#region Pyrolysis Gas Hydrotreator

						//@PGasHydroCnt			TINYINT			= NULL,
						r = 45;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PGasHydroCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@PGasHydroKBSD		REAL			= NULL,
						r = 45;
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PGasHydroKBSD", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PGasHydroPSIG		REAL			= NULL,
						r = 45;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PGasHydroPSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Transportation

						c = 7;

						//@PipeCnt				TINYINT			= NULL,
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PipeCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@RailCnt				TINYINT			= NULL,
						r = 47;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@RailCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@MarineCnt			TINYINT			= NULL,
						r = 48;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MarineCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@AirCompCnt			TINYINT			= NULL,
						r = 49;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AirCompCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@ElecTransfCnt		TINYINT			= NULL,
						r = 50;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ElecTransfCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@FlareCnt				TINYINT			= NULL,
						r = 51;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FlareCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@WaterTreatCnt		TINYINT			= NULL,
						r = 52;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@WaterTreatCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@BldgCnt				TINYINT			= NULL,
						r = 53;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@BldgCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@RGRCnt				TINYINT			= NULL,
						r = 54;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@RGRCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						#endregion

						#region Other

						//@OthName1				VARCHAR (75)	= NULL,
						r = 57;
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthName1", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

						//@OthCnt1				TINYINT			= NULL,
						r = 57;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthCnt1", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@OthName2				VARCHAR (75)	= NULL,
						r = 58;
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthName2", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

						//@OthCnt2				TINYINT			= NULL,
						r = 58;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthCnt2", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						//@OthName3				VARCHAR (75)	= NULL,
						r = 59;
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthName3", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

						//@OthCnt3				TINYINT			= NULL,
						r = 59;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthCnt3", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						#endregion

						#region Additional

						//@FracFeedProcMT		REAL			= NULL,
						r = 64;
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FracFeedProcMT", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@SG_FracFeedProc		REAL			= NULL,
						r = 64;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@SG_FracFeedProc", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PyrGasHydroType		VARCHAR (40)	= NULL,
						r = 66;
						c = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PyrGasHydroType", SqlDbType.VarChar, 40).Value = ReturnString(rng, 40); }

						//@PyrGasHydroMT		REAL			= NULL,
						r = 67;
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PyrGasHydroMT", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@SG_NonBnzPGH			REAL			= NULL
						r = 68;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@SG_NonBnzPGH", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						//@UnitCnt				TINYINT			= NULL,
						r = 60;
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@UnitCnt", SqlDbType.TinyInt).Value = ReturnByte(rng); }

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFacilities", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Facilities]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}