﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadInventory(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;

			Excel.Range rngCap = null;
			Excel.Range rngInv = null;

			UInt32 r = 0;

			const UInt32 cCap = 7;
			const UInt32 cInv = 8;

			string cT08_01 = (includeSpace) ? "Table 8-1" : "Table8-1";
			wks = wkb.Worksheets[cT08_01];

			object[] itm = new object[6]
			{
				new object[2] { "FA", 15 },
				new object[2] { "FU", 16 },
				new object[2] { "IS", 17 },
				new object[2] { "PA", 19 },
				new object[2] { "PU", 20 },
				new object[2] { "TT", 21 }
			};

			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				foreach (object[] s in itm)
				{
					try
					{
						r = Convert.ToUInt32(s[1]);
						rngCap = wks.Cells[r, cCap];
						rngInv = wks.Cells[r, cInv];

						if ((RangeHasValue(rngCap) && ReturnFloat(rngCap) > 0.0f) || (RangeHasValue(rngInv) && ReturnFloat(rngInv) > 0.0f))
						{
							using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Inventory]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@Refnum		CHAR (9)
								cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

								//@TankType	CHAR (2),
								cmd.Parameters.Add("@TankType", SqlDbType.VarChar, 2).Value = Convert.ToString(s[0]);

								//@KMT		REAL	= NULL
								if (RangeHasValue(rngCap))
								{
									cmd.Parameters.Add("@KMT", SqlDbType.Float).Value = ReturnFloat(rngCap);
								}
								else
								{
									cmd.Parameters.Add("@KMT", SqlDbType.Float).Value = 0.0f;
								}

								//@YEIL		REAL	= NULL
								if (RangeHasValue(rngInv))
								{
									cmd.Parameters.Add("@YEIL", SqlDbType.Float).Value = ReturnFloat(rngInv);
								}
								else
								{
									cmd.Parameters.Add("@YEIL", SqlDbType.Float).Value = 0.0f;
								}

								cmd.ExecuteNonQuery();
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadInventory", Refnum, wkb, wks, rngCap, r, cCap, "[stgFact].[Insert_Inventory]", ex);
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadInventory", Refnum, wkb, wks, rngInv, r, cInv, "[stgFact].[Insert_Inventory]", ex);
					}
				}
				cn.Close();
			}
			rngCap = null;
			rngInv = null;
			wks = null;
		}
	}
}