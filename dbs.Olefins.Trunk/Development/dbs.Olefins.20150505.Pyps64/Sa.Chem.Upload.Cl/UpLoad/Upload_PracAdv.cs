﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracAdv(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracAdv]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						string cT09_01 = (includeSpace) ? "Table 9-1" : "Table9-1";
						wks = wkb.Worksheets[cT09_01];

						//@Tbl9001  REAL     = NULL
						r = 33;
						c = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9001", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9002  REAL     = NULL (Percent)
						r = 37;
						c = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9002", SqlDbType.Float).Value = ReturnFloat(rng); }

						string cT09_02 = (includeSpace) ? "Table 9-2" : "Table9-2";
						wks = wkb.Worksheets[cT09_02];

						//@Tbl9003  CHAR (1) = NULL
						r = 9;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9003", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Tbl9004  REAL     = NULL
						r = 21;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9004", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9005  CHAR (1) = NULL
						r = 28;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9005", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Tbl9006  CHAR (1) = NULL
						r = 32;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9006", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Tbl9007  CHAR (1) = NULL
						r = 36;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9007", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Tbl9008  CHAR (1) = NULL
						r = 44;
						c = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9008", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Tbl9009  REAL     = NULL
						r = 48;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9009", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9009a REAL     = NULL
						r = 50;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9009a", SqlDbType.Float).Value = ReturnFloat(rng); }

						string cT09_03 = (includeSpace) ? "Table 9-3" : "Table9-3";
						wks = wkb.Worksheets[cT09_03];

						c = 9;

						//@Tbl9010  CHAR (1) = NULL
						r = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9010", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Tbl9011  REAL     = NULL
						r = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9011", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9012  REAL     = NULL
						r = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9012", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9013  REAL     = NULL
						r = 15;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9013", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9014  INT      = NULL
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9014", SqlDbType.Int).Value = ReturnUShort(rng); }

						//@Tbl9015  INT      = NULL
						r = 21;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9015", SqlDbType.Int).Value = ReturnUShort(rng); }

						//@Tbl9016  REAL     = NULL (Percent)
						r = 25;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9016", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9017  REAL     = NULL
						r = 29;
						c = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9017", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Tbl9018  CHAR (1) = NULL
						r = 32;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9018", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Tbl9019  CHAR (1) = NULL
						r = 38;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9019", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracAdv", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracAdv]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}