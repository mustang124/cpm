﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadMaint01(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			float m;
			float l;

			UInt32 r = 0;
			UInt32 c = 0;

			string cT09_05 = (includeSpace) ? "Table 10-1" : "Table10-1";
			wks = wkb.Worksheets[cT09_05];

			object[] itm = new object[20]
			{
				//new object[2] { "Tubes", 31 },
				new object[2] { "OthMajFurn", 33 },
				new object[2] { "MinFurn", 34 },
				new object[2] { "TLE_WHB", 35 },
				new object[2] { "Comp", 36 },
				new object[2] { "Pump", 37 },
				new object[2] { "Exchangers", 38 },
				new object[2] { "CoolTwr", 39 },
				new object[2] { "Boil", 40 },
				new object[2] { "OtherFixEq", 41 },
				new object[2] { "OnSitePipe", 42 },
				new object[2] { "InstrAnalyzer", 43 },
				new object[2] { "PCComputer", 44 },
				new object[2] { "PowerGen", 45 },
				new object[2] { "OtherOnSite", 47 },
				new object[2] { "Tank", 49 },
				new object[2] { "Flare", 50 },
				new object[2] { "Environ", 51 },
				new object[2] { "OthOffsite", 52 },
				new object[2] { "OthTASvc", 54 },
				new object[2] { "OverHead", 56 }
			};

			using (SqlConnection cn = new SqlConnection(Common.cnString()))
			{
				cn.Open();

				foreach (object[] s in itm)
				{
					try
					{
						using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Maint01]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum			CHAR (9),
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@ProjectID      VARCHAR (15)
							cmd.Parameters.Add("@ProjectID", SqlDbType.VarChar, 15).Value = (string)s[0];

							r = Convert.ToUInt32(s[1]);

							#region Data

							if (r != 54)
							{
								//@RoutMaintMatl  REAL         = NULL
								m = 0.0f;
								c = 5;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng) && !float.NaN.Equals(ReturnFloat(rng))) { m = ReturnFloat(rng); cmd.Parameters.Add("@RoutMaintMatl", SqlDbType.Float).Value = m; }

								//@RoutMaintLabor REAL         = NULL
								l = 0.0f;
								c = 6;
								rng = wks.Cells[r, c];
								if (RangeHasValue(rng) && !float.NaN.Equals(ReturnFloat(rng))) { l = ReturnFloat(rng); cmd.Parameters.Add("@RoutMaintLabor", SqlDbType.Float).Value = l; }

								//@RoutMaintTot   REAL         = NULL
								if (m + l > 0.0f)
								{
									cmd.Parameters.Add("@RoutMaintTot", SqlDbType.Float).Value = m + l;
								}
							}

							//@TAMatl         REAL         = NULL
							m = 0.0f;
							c = 7;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng) && !float.NaN.Equals(ReturnFloat(rng))) { m = ReturnFloat(rng); cmd.Parameters.Add("@TAMatl", SqlDbType.Float).Value = m; }

							//@TALabor        REAL         = NULL
							l = 0.0f;
							c = 8;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng) && !float.NaN.Equals(ReturnFloat(rng))) { l = ReturnFloat(rng); cmd.Parameters.Add("@TALabor", SqlDbType.Float).Value = l; }

							//@TATot          REAL         = NULL
							if (m + l > 0.0f)
							{
								cmd.Parameters.Add("@TATot", SqlDbType.Float).Value = m + l;
							}

							#endregion

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMaint01", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Maint01]", ex);
					}
				}
				cn.Close();
			}
			rng = null;
			wks = null;
		}
	}
}
