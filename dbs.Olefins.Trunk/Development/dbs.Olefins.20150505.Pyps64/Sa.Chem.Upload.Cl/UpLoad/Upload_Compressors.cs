﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadCompressors(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			const int eventNumber = 1;
			const int compressor = 2;
			const int eventType = 3;
			const int duration = 4;
			const int component = 5;
			const int cause = 6;
			const int eventYear = 7;
			const int comments = 8;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					string cT06_05 = (includeSpace) ? "Table 6-5" : "Table6-5";
					wks = wkb.Worksheets[cT06_05];

					for (int row = 29; row <= 99; row++)
					{
						r = (UInt32)row;

						rng = wks.Cells[row, compressor];
						if (RangeHasValue(rng))
						{
							using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Compressors]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@Refnum			CHAR (9),
								cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

								//@EventNo			TINYINT,
								rng = wks.Cells[row, eventNumber];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@EventNo", SqlDbType.TinyInt).Value = ReturnUShort(rng);

								//@Compressor		VARCHAR (5)		= NULL,
								rng = wks.Cells[row, compressor];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@Compressor", SqlDbType.VarChar, 5).Value = ReturnString(rng, 5);

								//@EventType		CHAR (8)		= NULL,
								rng = wks.Cells[row, eventType];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@EventType", SqlDbType.Char, 8).Value = ReturnString(rng, 8);

								//@Duration			REAL			= NULL,
								rng = wks.Cells[row, duration];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@Component", SqlDbType.Real).Value = ReturnFloat(rng);

								//@Component		CHAR (5)		= NULL,
								rng = wks.Cells[row, component];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@Component", SqlDbType.Char, 5).Value = ReturnString(rng, 5);

								//@Cause			TINYINT			= NULL,
								rng = wks.Cells[row, cause];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@Cause", SqlDbType.TinyInt).Value = ReturnUShort(rng);

								//@EventYear		INT				= NULL,
								rng = wks.Cells[row, eventYear];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@EventYear", SqlDbType.Int).Value = ReturnUShort(rng);

								//@Comments			VARCHAR (MAX)	= NULL,
								rng = wks.Cells[row, comments];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@Comments", SqlDbType.VarChar).Value = ReturnString(rng, 256);

								//@CompLife			TINYINT			= NULL
								r = 23;
								rng = wks.Cells[23, 8];
								if (RangeHasValue(rng)) cmd.Parameters.Add("@CompLife", SqlDbType.TinyInt).Value = ReturnUShort(rng);

								cmd.ExecuteNonQuery();
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCompressors", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Compressors]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}