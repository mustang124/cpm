﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadTSort(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					string wksName = "PlantName";
					wks = wkb.Worksheets[wksName];

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_TSort]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum					CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@StudyYear				SMALLINT		= NULL
						cmd.Parameters.Add("@StudyYear", SqlDbType.SmallInt).Value = StudyYear;

						c = 4;

						//@Co						CHAR (40)		= NULL
						//@Loc						CHAR (40)		= NULL
						//@CoLoc					VARCHAR (60)	= NULL

						//@CoName					VARCHAR (100)	= NULL
						r = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CoName", SqlDbType.VarChar, 100).Value = ReturnString(rng, 100); }

						//@PlantName				VARCHAR (60)	= NULL
						r = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PlantName", SqlDbType.VarChar, 60).Value = ReturnString(rng, 60); }

						//@PlantLoc					VARCHAR (40)	= NULL

						//@CoordName				VARCHAR (40)	= NULL
						r = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CoordName", SqlDbType.VarChar, 40).Value = ReturnString(rng, 40); }

						//@CoordTitle				VARCHAR (75)	= NULL
						r = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CoordTitle", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

						//@POBox					VARCHAR (35)	= NULL
						r = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@POBox", SqlDbType.VarChar, 35).Value = ReturnString(rng, 35); }

						//@Street					VARCHAR (60)	= NULL
						r = 15;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Street", SqlDbType.VarChar, 60).Value = ReturnString(rng, 60); }

						//@City						VARCHAR (40)	= NULL
						r = 16;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@City", SqlDbType.VarChar, 40).Value = ReturnString(rng, 40); }

						//@State					VARCHAR (25)	= NULL
						r = 17;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@State", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						//@Country					VARCHAR (30)	= NULL
						r = 18;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Country", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

						//@Zip						VARCHAR (20)	= NULL
						r = 19;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Zip", SqlDbType.VarChar, 20).Value = ReturnString(rng, 20); }

						//@Telephone				VARCHAR (30)	= NULL
						r = 20;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Telephone", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

						//@Fax						VARCHAR (30)	= NULL
						r = 21;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Fax", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

						//@WWW						VARCHAR (65)	= NULL
						r = 22;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@WWW", SqlDbType.VarChar, 65).Value = ReturnString(rng, 65); }

						if (StudyYear >= 2007)
						{
							//@PricingContact			VARCHAR (40)	= NULL
							r = 23;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@PricingContact", SqlDbType.VarChar, 40).Value = ReturnString(rng, 40); }

							//@PricingContactEmail		VARCHAR (65)	= NULL
							r = 24;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@PricingContactEmail", SqlDbType.VarChar, 65).Value = ReturnString(rng, 65); }

							//@DCContact				VARCHAR (40)	= NULL
							r = 25;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@DCContact", SqlDbType.VarChar, 40).Value = ReturnString(rng, 40); }

							//@DCContactEmail			VARCHAR (65)	= NULL
							r = 26;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@DCContactEmail", SqlDbType.VarChar, 65).Value = ReturnString(rng, 65); }
						};

						//@UOM						CHAR (1)		= NULL
						wks = wkb.Worksheets["Conv"];

						r = 2;
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@UOM", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						cmd.ExecuteNonQuery();
					}
					//@PlantCountry				CHAR (20)		= NULL
					//@Region					VARCHAR (10)	= NULL
					//@CapPeerGrp				TINYINT			= NULL
					//@TechPeerGrp				TINYINT			= NULL
					//@FeedClass				TINYINT			= NULL
					//@EDCPeerGrp				TINYINT			= NULL
					//@LocFactor				REAL			= NULL
					//@InflFactor				REAL			= NULL
					//@CompanyID				CHAR (25)		= NULL
					//@ContactCode				CHAR (25)		= NULL
					//@RefNumber				CHAR (4)		= NULL
					//@PresLabel				VARCHAR (3)		= NULL
					//@Energy					TINYINT			= NULL
					//@Polymer					TINYINT			= NULL
					//@CogenPeerGrp				VARCHAR (10)	= NULL
					//@SharedPeerGrp			VARCHAR (20)	= NULL
					//@Password					VARCHAR (20)	= NULL
					//@RefDirectory				VARCHAR (100)	= NULL
					//@Consultant				CHAR (5)		= NULL
					//@Comments					CHAR (255)		= NULL

					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadTSort", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_TSort]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}