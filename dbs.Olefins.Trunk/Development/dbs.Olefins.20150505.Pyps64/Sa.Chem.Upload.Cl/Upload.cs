﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void Delete(string sRefnum)
		{
			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				using (SqlCommand cmd = new SqlCommand("[stgFact].[Delete_OlefinsData]", cn))
				{
					cmd.CommandType = System.Data.CommandType.StoredProcedure;
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 9).SqlValue = sRefnum;
					cmd.ExecuteNonQuery();
				}
				cn.Close();
			}
		}

		#region Conversions

		public Boolean VerifyRefnum(Excel.Workbook wkb, string Refnum)
		{
			Boolean b = false;

			string OsimRefnum = (string)wkb.Worksheets["PlantName"].Range["REF_NUM"].Value2.Trim();

			if (OsimRefnum.Contains(Refnum) || Refnum.Contains(OsimRefnum))
			{
				b = true;
			};

			return b;
		}

		public Boolean RangeHasValue(Excel.Range rng)
		{
			string s = string.Empty;

			foreach (Excel.Range r in rng.Cells)
			{
				s = Convert.ToString(r.Text).Trim();

				if (s.Length > 0) s = Convert.ToString(r.Value).Trim();

				if (s.Length > 0) return true;
			}
			return false;
		}

		public Boolean BitConvert(Excel.Range rng)
		{
			Boolean b = false;

			string s = Convert.ToString(rng.Value).Substring(0, 1).ToUpper();

			if (s == "T" || s == "X" || s == "Y" || s == "1" || s == "-") b = true;

			return b;
		}

		public string ConvertFeedProdId(Excel.Range rng, UInt32 c)
		{
			string FeedProdID = Convert.ToString(rng.Value).Trim();

			switch (FeedProdID)
			{
				case "Ethane": return "Ethane";
				case "Ethane/ Propane Mix": return "EPMix";
				case "Propane": return "Propane";
				case "LPG": return "LPG";

				case "Butane and Other C4s": return "Butane";
				case " Butane and Other C4s": return "Butane";
				case "  Butane and Other C4s": return "Butane";
				case "' Butane and Other C4s": return "Butane";
				case "'  Butane and Other C4s": return "Butane";

				case "Light Naphtha (mostly C5 & C6)": return "LtNaphtha";
				case "Full-Range  Naphtha": return "FRNaphtha";
				case "Heavy Naphtha (mostly C6+)": return "HeavyNaphtha";
				case "Raffinates from Aromatics Extraction": return "Raffinate";
				case "Heavy Natural Gas Liquids": return "HeavyNGL";
				case "Field Condensate": return "Condensate";
				case "Diesel & Gas Oil with EP <390°C": return "Diesel";
				case "Diesel &\n Gas Oil with EP <390°C": return "Diesel";
				case "Heavy Feeds, Vacuum Gas Oil with EP >390°C": return "HeavyGasoil";
				case "Heavy Feeds, Vacuum Gas Oil with \nEP >390°C": return "HeavyGasoil";
				case "Severely Hydrotreated Gas Oil": return "HTGasoil";

				case "Ethane Recycle*": return "EthRec";
				case "Propane Recycle*": return "ProRec";
				case "C4 Recycle*": return "ButRec";

				case "Naphtha": return "FRNaphtha";
				case "Gas Oil": return "GasOil";

				default:
					switch (c)
					{
						case 8: return "OthLtFeed";
						case 12: return "OthLiqFeed1";
						case 13: return "OthLiqFeed2";
						case 14: return "OthLiqFeed3";
						default: return ReturnString(rng, 20);
					}
			}
		}

		public string ConvertEnergyId(Excel.Range rng, UInt32 r)
		{
			string EnergyId = Convert.ToString(rng.Value).Trim();

			if (r >= 7 && r <= 25)
			{
				switch (EnergyId)
				{
					case "Fuel Gas/Natural Gas": return "FuelGas";
					case "Ethane": return "Ethane";
					case "Propane": return "Propane";
					case "Butanes": return "Butanes";
					case "Naphtha/Gasoline": return "Naphtha";
					case "Distillate Fuels": return "Distillate";
					case "Residual Fuels": return "Residual";
					case "Coal or Coke": return "Coal";

					case "Super-High-Pressure (>62 bar g)": return "SHPS";
					case "Super-High-Pressure (>900 psig)": return "SHPS";
					case "High-Pressure (>31 bar g)": return "HPS";
					case "High-Pressure (>450 psig)": return "HPS";
					case "Intermediate-Pressure (>10 bar g)": return "IPS";
					case "Intermediate-Pressure (>150 psig)": return "IPS";
					case "Low-Pressure (<10 bar g)": return "LPS";
					case "Low-Pressure (<150 psig)": return "LPS";

					case "Super High Pressure (>62 bar g)": return "SHPS";
					case "Super High Pressure (>900 psig)": return "SHPS";
					case "High Pressure (>31 bar g)": return "HPS";
					case "High Pressure (>450 psig)": return "HPS";
					case "Intermediate Pressure (>10 bar g)": return "IPS";
					case "Intermediate Pressure (>150 psig)": return "IPS";
					case "Low Pressure (<10 bar g)": return "LPS";
					case "Low Pressure (<150 psig)": return "LPS";

					default: return null;
				}
			}
			else if (r >= 27 && r <= 34)
			{
				switch (EnergyId)
				{
					case "Fuel Gas": return "PPCFuelGas";
					case "Ethane": return "PPCEthane";
					case "Propane": return "PPCPropane";
					case "Butanes": return "PPCButanes";
					case "Pyrolysis Naphtha": return "PPCNaphtha";
					case "Pyrolysis Gas Oil": return "PPCGasOil";
					case "Pyrolysis Fuel Oil": return "PPCFuelOil";

					default: return null;
				}
			}
			else if (r >= 40 && r <= 45)
			{
				switch (EnergyId)
				{
					case "Super-High-Pressure (>62 bar g)": return "ExpSHPS";
					case "Super-High-Pressure (>900 psig)": return "ExpSHPS";
					case "High-Pressure (>31 bar g)": return "ExpHPS";
					case "High-Pressure (>450 psig)": return "ExpHPS";
					case "Intermediate-Pressure (>10 bar g)": return "ExpIPS";
					case "Intermediate-Pressure (>150 psig)": return "ExpIPS";
					case "Low-Pressure (<10 bar g)": return "ExpLPS";
					case "Low-Pressure (<150 psig)": return "ExpLPS";

					case "Super High Pressure (>62 bar g)": return "ExpSHPS";
					case "Super High Pressure (>900 psig)": return "ExpSHPS";
					case "High Pressure (>31 bar g)": return "ExpHPS";
					case "High Pressure (>450 psig)": return "ExpHPS";
					case "Intermediate Pressure (>10 bar g)": return "ExpIPS";
					case "Intermediate Pressure (>150 psig)": return "ExpIPS";
					case "Low Pressure (<10 bar g)": return "ExpLPS";
					case "Low Pressure (<150 psig)": return "ExpLPS";

					default: return null;
				}
			}
			else if (r >= 57 && r <= 60)
			{
				switch (EnergyId)
				{
					case "Gas Turbine Only": return "GasTurb";
					case "Steam Turbine Only": return "SteamTurb";
					case "Combined Cycle (gas turbine with downstream steam turbine)": return "Combine";

					default: return "Other";
				}
			}
			else
			{
				return null;
			}
		}
		
		public string ConvertSupplementalID(Excel.Range rng)
		{
			string RangeName = Convert.ToString(rng.Name.NameLocal);

			switch (RangeName)
			{
				case "ETH_CHEM_1Q": return "ConcEthylene";
				case "PRO_CHEM_1Q": return "ConcPropylene";
				case "BUT_CHEM_1Q": return "ConcButadiene";
				case "BENZ_CHEM_1Q": return "ConcBenzene";

				case "RefHYD_1Q": return "RefHydrogen";
				case "RefMETH_1Q": return "RefMethane";
				case "RefETH_1Q": return "RefEthane";
				case "RefETY_1Q": return "RefEthylene";
				case "RefPRO_1Q": return "RefPropane";
				case "RefPRY_1Q": return "RefPropylene";
				case "RefBUT_1Q": return "RefC4Plus";
				case "RefINERT_1Q": return "RefInerts";

				case "HYD_1Q": return "DiHydrogen";
				case "METH_1Q": return "DiMethane";
				case "ETH_1Q": return "DiEthane";
				case "ETY_1Q": return "DiEthylene";
				case "PRO_1Q": return "DiPropane";
				case "PRY_1Q": return "DiPropylene";
				case "BUT_1Q": return "DiButane";
				case "BUTYL_1Q": return "DiButylenes";
				case "BUTAD_1Q": return "DiButadiene";
				case "BENZ_1Q": return "DiBenzene";
				case "GASOLINE_1Q": return "DiMoGas";

				case "GAS_1Q": return "WashOil";
				case "FUEL_1Q": return "GasOil";
				case "OTH_1Q": return "OthSpl";

				case "HYD_PROD_1Q": return "Hydrogen";

				case "METH_PROD_1Q": return "FuelGasSales";
				case "ACET_1Q": return "Acetylene";

				case "POL_ETY_1Q": return "Ethylene";
				case "CHM_ETY_1Q": return "EthyleneCG";
				case "POL_PRY_1Q": return "Propylene";
				case "CHEM_PRY_1Q": return "PropyleneCG";
				case "REFY_PRY_1Q": return "PropyleneRG";

				case "PRO_PROD_1Q": return "PropaneC3";

				case "MX_BUT_1Q": return "Butadiene";
				case "MX_ISO_1Q": return "IsoButylene";
				case "MX_OTH_1Q": return "OthC4";

				case "PYR_BENZ_1Q": return "Benzene";
				case "PYR_OTH_1Q": return "OthPyGas";
				case "PYR_GAS_1Q": return "PyGasoil";
				case "PYR_FUEL_1Q": return "PyFuelOil";

				case "ACID_1Q": return "AcidGas";
				case "FUEL_PROD_1Q": return "PPCFuel";

				case "OTHERPROD1_1Q": return "OthProd1";
				case "OTHERPROD2_1Q": return "OthProd2";

				case "FLARE_1Q": return "FlareLoss";
				case "MISCLOSS_1Q": return "OthLoss";
				case "MSMTLSS_1Q": return "MeasureLoss";

				default: return null;
			}
		}

		[Obsolete("Converts to StreamID; Use conversion to ProdID", true)]
		public string Convert_Supplemental_ID(Excel.Range rng)
		{
			string RangeName = Convert.ToString(rng.Name.NameLocal);

			switch (RangeName)
			{
				case "ETH_CHEM_1Q": return "ConcEthylene";
				case "PRO_CHEM_1Q": return "ConcPropylene";
				case "BUT_CHEM_1Q": return "ConcButadiene";
				case "BENZ_CHEM_1Q": return "ConcBenzene";

				case "RefHYD_1Q": return "ROGHydrogen";
				case "RefMETH_1Q": return "ROGMethane";
				case "RefETH_1Q": return "ROGEthane";
				case "RefETY_1Q": return "ROGEthylene";
				case "RefPRO_1Q": return "ROGPropane";
				case "RefPRY_1Q": return "ROGPropylene";
				case "RefBUT_1Q": return "ROGC4Plus";
				case "RefINERT_1Q": return "ROGInerts";

				case "HYD_1Q": return "DilHydrogen";
				case "METH_1Q": return "DilMethane";
				case "ETH_1Q": return "DilEthane";
				case "ETY_1Q": return "DilEthylene";
				case "PRO_1Q": return "DilPropane";
				case "PRY_1Q": return "DilPropylene";
				case "BUT_1Q": return "DilButane";
				case "BUTYL_1Q": return "DilButylene";
				case "BUTAD_1Q": return "DilButadiene";
				case "BENZ_1Q": return "DilBenzene";
				case "GASOLINE_1Q": return "DilMoGas";

				case "GAS_1Q": return "SuppGasOil";
				case "FUEL_1Q": return "SuppWashOil";
				case "OTH_1Q": return "SuppOther";

				//case "ETH_PCT_TOT": return null;
				//case "PRO_PCT_TOT": return null;
				//case "BUT_PCT_TOT": return null;

				//case "SUPPFD_CompSuc": return null;
				//case "SUPPFD_CompDisch": return null;
				//case "SUPPFD_C2RecSect": return null;
				//case "SUPPFD_C3RecSect": return null;

				case "HYD_PROD_1Q": return "Hydrogen";

				case "METH_PROD_1Q": return "Methane";
				case "ACET_1Q": return "Acetylene";

				case "POL_ETY_1Q": return "EthylenePG";
				case "CHM_ETY_1Q": return "EthyleneCG";

				case "POL_PRY_1Q": return "PropylenePG";
				case "CHEM_PRY_1Q": return "PropyleneCG";
				case "REFY_PRY_1Q": return "PropyleneRG";

				case "PRO_PROD_1Q": return "PropaneC3Resid";

				case "MX_BUT_1Q": return "Butadiene";
				case "MX_ISO_1Q": return "IsoButylene";
				case "MX_OTH_1Q": return "C4Oth";

				case "PYR_BENZ_1Q": return "Benzene";
				case "PYR_OTH_1Q": return "PyroGasoline";
				case "PYR_GAS_1Q": return "PyroGasOil";
				case "PYR_FUEL_1Q": return "PyroFuelOil";

				case "ACID_1Q": return "AcidGas";
				case "FUEL_PROD_1Q": return "PPCFuel";

				case "OTHERPROD1_1Q": return "OthProd1";
				case "OTHERPROD2_1Q": return "OthProd2";

				case "FLARE_1Q": return "LossFlareVent";
				case "MISCLOSS_1Q": return "LossOther";
				case "MSMTLSS_1Q": return "LossMeasure";

				default: return null;
			}
		}

		public string ConvertPersID(Excel.Range rng, UInt32 c)
		{
			string PersID = Convert.ToString(rng.Value).Trim();
			string SheetName = Convert.ToString(rng.Parent.Name).Trim();

			switch (SheetName)
			{
				case "Table5A":
					{
						switch (PersID)
						{
							case "Assigned to Process Units (4)": return "OCCOpProc";
							case "Assigned to Utility Systems (5)": return "OCCOpUtil";
							case "Off-Sites, Hydrocarbon Movements (6)": return "OCCOpOffsite";
							case "Off-sites, Hydrocarbon Movements (6)": return "OCCOpOffsite";
							case "Truck & Railracks": return "OCCOpRacks";
							case "Marine Operations (7)": return "OCCOpMarine";
							case "Shift Relief (8)": return "OCCOpRelief";
							case "Training (9)": return "OCCOpTrng";
							case "Other (10)": return "OCCOpOth";

							case "Routine Field Maintenance": return "OCCMaintRtn";
							case "Turnaround Maintenance": return "OCCMaintTA";
							case "Inspectors": return "OCCMaintInsp";
							case "Central Shops": return "OCCMaintShop";
							case "Planning & Scheduling": return "OCCMaintPlan";
							case "Warehouse & Stores": return "OCCMaintWhs";
							case "Training, Relief, & Other": return "OCCMaintTrng";

							case "Laboratory (12)": return "OCCAdmLab";
							case "Accounting & Computing": return "OCCAdmAcct";
							case "Purchasing": return "OCCAdmPurch";
							case "Plant Security": return "OCCAdmSecurity";
							case "Clerical & Secretarial": return "OCCAdmClerical";
							case "Other Administrative Services (13)": return "OCCAdmOth";

							default: return ReturnString(rng, c);
						}
					}
				case "Table5B":
					{
						switch (PersID)
						{
							case "Process Supervisory Staff (4)": return "MPSProcess";

							case "Routine Field Maintenance Supervision": return "MPSMaintRtn";
							case "Turnaround Maintenance Supervision (6)": return "MPSMaintTA";
							case "Central Shops": return "MPSMaintShop";
							case "Planning & Scheduling (7)": return "MPSMaintPlan";
							case "Warehouse & Stores": return "MPSMaintWhs";

							case "Plant Laboratory Staff (8)": return "MPSLab";

							case "Process Support": return "MPSTechProc";
							case "Process Economics Including LP Studies": return "MPSTechEcon";
							case "Reliability & Maintenance": return "MPSTechRely";
							case "Equipment Inspection": return "MPSTechInsp";
							case "Advanced Process Control": return "MPSTechAPC";
							case "Environmental & Safety": return "MPSTechEnvir";
							case "Other Technical Staff": return "MPSTechOth";

							case "Accounting Staff (10)": return "MPSAdmAcct";
							case "Plant Computer & MIS (11)": return "MPSAdmMIS";
							case "Purchasing (12)": return "MPSAdmPurch";
							case "Plant Security": return "MPSAdmSecurity";
							case "Other Administrative Staff (13)": return "MPSAdmOth";

							case "Feed & Fuels Purchasing Staff (14)": return "MPSFeedStaff";
							case "Other Allocated G&A Staff (15)": return "MPSGA";

							default: return ReturnString(rng, c);
						}
					}
				default: return ReturnString(rng, c);
			}
		}

		public string ConvertLossCause(uint r)
		{
			switch (r)
			{
				case 10: return "PlantTA";

				case 12: return "OperError";
				case 13: return "IntFeedInterrupt";
				case 14: return "Fouling";
				case 15: return "Transition";
				case 16: return "AcetyleneConv";
				case 18: return "OtherProcess";
				case 19: return "FurnaceProcess";

				case 21: return "Compressor";
				case 22: return "OtherRotating";
				case 23: return "FurnaceMech";
				case 25: return "Corrosion";
				case 26: return "ElecDist";
				case 27: return "ControlSys";
				case 28: return "OtherFailure";

				case 30: return "ExtElecFailure";
				case 31: return "ExtOthUtilFailure";
				case 32: return "IntElecFailure";
				case 33: return "IntSteamFailure";
				case 34: return "IntOthUtilFailure";

				case 36: return "Demand";
				case 37: return "ExtFeedInterrupt";
				case 39: return "CapProject";
				case 40: return "Strikes";
				case 41: return "FeedMix";
				case 42: return "Severity";
				case 43: return "OtherNonOp";

				case 72: return "Demand";
				case 73: return "ExtFeedInterrupt";
				case 75: return "CapProject";
				case 76: return "Strikes";
				case 77: return "FeedMix";
				case 78: return "Severity";
				case 79: return "OtherNonOp";

				default: return null;
			}
		}

		#endregion

		#region Return Functions

		public string ReturnString(Excel.Range rng, UInt32 FieldLength)
		{
			string t = Convert.ToString(rng.Value).Trim();
			return t.Substring(0, Math.Min(t.Length, (int)FieldLength));
		}

		public float ReturnFloat(Excel.Range rng)
		{
			try
			{
				return (float)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value).Trim();
				return float.Parse(s);
			}
		}

		public DateTime ReturnDateTime(Excel.Range rng)
		{
			try
			{
				return (DateTime)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value).Trim();
				return DateTime.Parse(s);
			}
		}

		public byte ReturnByte(Excel.Range rng, Boolean ConvertTrueFalse = false)
		{
			if (ConvertTrueFalse)
			{
				byte b = 0;

				string s = Convert.ToString(rng.Value).Substring(0, 1).ToUpper();

				if (s == "T" || s == "X" || s == "Y" || s == "1" || s == "-") b = 1;

				return b;
			}
			else
			{
				try
				{
					return (byte)rng.Value;
				}
				catch
				{
					string s = Convert.ToString(rng.Value).Trim();
					return byte.Parse(s);
				}
			}
		}

		public ushort ReturnUShort(Excel.Range rng)
		{
			try
			{
				return (ushort)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value).Trim();
				return ushort.Parse(s);
			}
		}

		public short ReturnShort(Excel.Range rng)
		{
			try
			{
				return (short)rng.Value;
			}
			catch
			{
				string s = Convert.ToString(rng.Value).Trim();
				return short.Parse(s);
			}
		}

		#endregion
	}
}
