﻿CREATE TABLE [fact].[StreamRecycled] (
    [SubmissionId]    INT                NOT NULL,
    [ComponentId]     INT                NOT NULL,
    [Recycled_WtPcnt] FLOAT (53)         NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_StreamRecycled_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (128)     CONSTRAINT [DF_StreamRecycled_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (128)     CONSTRAINT [DF_StreamRecycled_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (128)     CONSTRAINT [DF_StreamRecycled_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamRecycled] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [ComponentId] ASC),
    CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MaxIncl_100.0] CHECK ([Recycled_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MinIncl_0.0] CHECK ([Recycled_WtPcnt]>=(0.0)),
    CONSTRAINT [FK_StreamRecycled_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StreamRecycled_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_StreamRecycled_u]
	ON [fact].[StreamRecycled]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamRecycled]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamRecycled].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[fact].[StreamRecycled].[ComponentId]		= INSERTED.[ComponentId];

END;