﻿CREATE TABLE [auth].[LoginsAttributes] (
    [LoginId]        INT                NOT NULL,
    [NameLast]       NVARCHAR (128)     NOT NULL,
    [NameFirst]      NVARCHAR (128)     NOT NULL,
    [_NameFull]      AS                 (([NameFirst]+' ')+[NameLast]) PERSISTED NOT NULL,
    [_NameComma]     AS                 (([NameLast]+', ')+[NameFirst]) PERSISTED NOT NULL,
    [eMail]          NVARCHAR (254)     NULL,
    [RoleId]         INT                DEFAULT ((0)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_LoginsAttributes_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_LoginsAttributes_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_LoginsAttributes_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_LoginsAttributes_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_LoginsAttributes] PRIMARY KEY CLUSTERED ([LoginId] ASC),
    CONSTRAINT [CL_LoginsAttributes_NameFirst] CHECK ([NameFirst]<>''),
    CONSTRAINT [CL_LoginsAttributes_NameLast] CHECK ([NameLast]<>''),
    CONSTRAINT [CL_LoginsAttributes_UserEMail] CHECK ([eMail]<>''),
    CONSTRAINT [FK_LoginsAttributes_Logins] FOREIGN KEY ([LoginId]) REFERENCES [auth].[Logins] ([LoginId]),
    CONSTRAINT [FK_LoginsAttributes_Roles] FOREIGN KEY ([RoleId]) REFERENCES [auth].[Roles] ([RoleId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_LoginsAttributes_EMail]
    ON [auth].[LoginsAttributes]([eMail] ASC) WHERE ([eMail] IS NOT NULL);


GO

CREATE TRIGGER [auth].[t_LoginsAttributes_u]
	ON [auth].[LoginsAttributes]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[LoginsAttributes]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[LoginsAttributes].[LoginId]		= INSERTED.[LoginId];

END;