﻿CREATE PROCEDURE [auth].[Merge_Roles]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [auth].[Roles] AS Target
	USING
	(
		VALUES
		('GreenLantern', 'Green Lantern', 'Green Lantern', 0),
		('Super', 'Super Admin', 'Super Administrator', 1),
		('Company', 'Company Coordinator', 'Company Coordinator', 5),
		('Site', 'Site Coordinator', 'Site Coordinator', 10)
	)
	AS Source([RoleTag], [RoleName], [RoleDetail], [RoleLevel])
	ON	Target.[RoleTag]	= Source.[RoleTag]
	WHEN MATCHED THEN UPDATE SET
		Target.[RoleName]	= Source.[RoleName],
		Target.[RoleDetail]	= Source.[RoleDetail],
		Target.[RoleLevel]	= Source.[RoleLevel]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RoleTag], [RoleName], [RoleDetail], [RoleLevel])
		VALUES([RoleTag], [RoleName], [RoleDetail], [RoleLevel])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;