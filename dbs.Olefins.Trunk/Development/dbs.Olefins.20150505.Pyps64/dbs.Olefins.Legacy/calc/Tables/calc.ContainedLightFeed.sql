﻿CREATE TABLE [calc].[ContainedLightFeed] (
    [MethodologyId]         INT                NOT NULL,
    [SubmissionId]          INT                NOT NULL,
    [StreamId]              INT                NOT NULL,
    [ComponentId]           INT                NOT NULL,
    [ContainedFeed_Dur_kMT] FLOAT (53)         NOT NULL,
    [ContainedFeed_Ann_kMT] FLOAT (53)         NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_ContainedLightFeed_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (128)     CONSTRAINT [DF_ContainedLightFeed_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (128)     CONSTRAINT [DF_ContainedLightFeed_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (128)     CONSTRAINT [DF_ContainedLightFeed_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]          ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ContainedLightFeed] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [StreamId] ASC),
    CONSTRAINT [CR_ContainedLightFeed_ContainedFeed_Ann_kMT_MinIncl_0.0] CHECK ([ContainedFeed_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_ContainedLightFeed_ContainedFeed_Dur_kMT_MinIncl_0.0] CHECK ([ContainedFeed_Dur_kMT]>=(0.0)),
    CONSTRAINT [FK_ContainedLightFeed_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_ContainedLightFeed_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_ContainedLightFeed_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ContainedLightFeed_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId]),
    CONSTRAINT [UK_ContainedLightFeed_ComponentId] UNIQUE NONCLUSTERED ([MethodologyId] ASC, [SubmissionId] ASC, [ComponentId] ASC)
);


GO

CREATE TRIGGER [calc].[t_ContainedLightFeed_u]
	ON [calc].[ContainedLightFeed]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[ContainedLightFeed]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[ContainedLightFeed].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[calc].[ContainedLightFeed].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[calc].[ContainedLightFeed].[StreamId]			= INSERTED.[StreamId];

END;