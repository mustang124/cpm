﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_HydroPur]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@Hydrogen		INT = [dim].[Return_StreamId]('Hydrogen');
	DECLARE	@H2				INT = [dim].[Return_ComponentId]('H2');
	DECLARE	@H2_WtPcnt		FLOAT = 40.0;
	DECLARE	@Coefficient	FLOAT = 20.0;

	INSERT INTO [calc].[StandardEnergy_HydroPur]([MethodologyId], [SubmissionId], [H2Pur_kScfYear], [H2Pur_kScfDay], [StandardEnergy_MBtuDay])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		p.[_H2Pur_kScfYear], /*	p.[_H2Pur_kScfDur], */
		p.[_H2Pur_kScfDay],
		p.[_H2Pur_MScfDay] * @Coefficient
	FROM [calc].[StreamComposition]			c
	INNER JOIN [calc].HydrogenPurification	p
		ON	p.[MethodologyId]		= c.[MethodologyId]
		AND	p.[SubmissionId]		= c.[SubmissionId]
		AND	p.[StreamNumber]		= c.[StreamNumber]
		AND	p.[StreamId]			= c.[StreamId]
		AND	p.[ComponentId]			= c.[ComponentId]
	WHERE	c.[MethodologyId]		= @MethodologyId
		AND	c.[SubmissionId]		= @SubmissionId
		AND	c.[StreamId]			= @Hydrogen
		AND	c.[ComponentId]			= @H2
		AND	c.[Component_WtPcnt]	> @H2_WtPcnt;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;