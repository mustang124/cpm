﻿CREATE PROCEDURE [ante].[Merge_MapFactorHydroPur]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapFactorHydroPur] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_FacilityId('HydroPurCryogenic'),	dim.Return_FactorId('HydroPurCryogenic')),
			(@MethodologyId, dim.Return_FacilityId('HydroPurPSA'),			dim.Return_FactorId('HydroPurPSA')),
			(@MethodologyId, dim.Return_FacilityId('HydroPurMembrane'),		dim.Return_FactorId('HydroPurMembrane'))
	)
	AS Source([MethodologyId], [FacilityId], [FactorId])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[FacilityId]		= Source.[FacilityId]
	WHEN MATCHED THEN UPDATE SET
		Target.[FactorId]	= Source.[FactorId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [FacilityId], [FactorId])
		VALUES([MethodologyId], [FacilityId], [FactorId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;