﻿CREATE TABLE [ante].[MapFactorFracType] (
    [MethodologyId]  INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [FactorId]       INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapFactorFracType_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_MapFactorFracType_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_MapFactorFracType_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_MapFactorFracType_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapFactorFracType] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC),
    CONSTRAINT [FK_MapFactorFracType_Factor_LookUp] FOREIGN KEY ([FactorId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_MapFactorFracType_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_MapFactorFracType_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_MapFactorFracType_u]
	ON [ante].[MapFactorFracType]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapFactorFracType]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapFactorFracType].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapFactorFracType].[StreamId]		= INSERTED.[StreamId];

END;