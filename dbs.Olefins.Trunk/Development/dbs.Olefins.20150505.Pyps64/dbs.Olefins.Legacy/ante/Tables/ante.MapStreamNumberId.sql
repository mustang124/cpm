﻿CREATE TABLE [ante].[MapStreamNumberId] (
    [MethodologyId]  INT                NOT NULL,
    [StreamNumber]   INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapStreamNumberId_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_MapStreamNumberId_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_MapStreamNumberId_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_MapStreamNumberId_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapStreamNumberId] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamNumber] ASC),
    CONSTRAINT [CR_MapStreamNumberId_StreamNumber] CHECK ([StreamNumber]>(0)),
    CONSTRAINT [FK_MapStreamNumberId_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_MapStreamNumberId_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_MapStreamNumberId]
    ON [ante].[MapStreamNumberId]([MethodologyId] DESC, [StreamNumber] ASC, [StreamId] ASC);


GO

CREATE TRIGGER [ante].[t_MapStreamNumberId_u]
	ON [ante].[MapStreamNumberId]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapStreamNumberId]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapStreamNumberId].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapStreamNumberId].[StreamNumber]	= INSERTED.[StreamNumber];

END;