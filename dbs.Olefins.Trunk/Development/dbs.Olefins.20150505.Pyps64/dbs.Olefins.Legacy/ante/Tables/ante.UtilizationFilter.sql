﻿CREATE TABLE [ante].[UtilizationFilter] (
    [MethodologyId]  INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [ComponentId]    INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_UtilizationFilter_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_UtilizationFilter_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_UtilizationFilter_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_UtilizationFilter_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_UtilizationFilter] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC, [ComponentId] ASC),
    CONSTRAINT [FK_UtilizationFilter_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_UtilizationFilter_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_UtilizationFilter_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_UtilizationFilter_u]
	ON [ante].[UtilizationFilter]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[UtilizationFilter]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[UtilizationFilter].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[UtilizationFilter].[StreamId]		= INSERTED.[StreamId];

END;