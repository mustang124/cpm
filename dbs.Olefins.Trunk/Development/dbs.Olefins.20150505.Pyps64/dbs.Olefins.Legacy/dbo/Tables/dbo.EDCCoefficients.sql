﻿CREATE TABLE [dbo].[EDCCoefficients] (
    [FactorSet]           SMALLINT   NOT NULL,
    [GasCompBHP]          FLOAT (53) NOT NULL,
    [MethCompBHP]         FLOAT (53) NOT NULL,
    [EthyCompBHP]         FLOAT (53) NOT NULL,
    [PropCompBHP]         FLOAT (53) NOT NULL,
    [FurnaceCapKMTA]      FLOAT (53) NOT NULL,
    [SpareFurnaceCapKMTA] FLOAT (53) NOT NULL,
    [Ethane]              FLOAT (53) NOT NULL,
    [EPMix]               FLOAT (53) NOT NULL,
    [Propane]             FLOAT (53) NOT NULL,
    [Butane]              FLOAT (53) NOT NULL,
    [LPG]                 FLOAT (53) NOT NULL,
    [OthLtFeed]           FLOAT (53) NOT NULL,
    [LtNaphtha]           FLOAT (53) NOT NULL,
    [Raffinate]           FLOAT (53) NOT NULL,
    [Condensate]          FLOAT (53) NOT NULL,
    [HeavyNGL]            FLOAT (53) NOT NULL,
    [FRNaphtha]           FLOAT (53) NOT NULL,
    [HeavyNaphtha]        FLOAT (53) NOT NULL,
    [Diesel]              FLOAT (53) NOT NULL,
    [HeavyGasoil]         FLOAT (53) NOT NULL,
    [HTGasoil]            FLOAT (53) NOT NULL,
    [OthLiqFeed1]         FLOAT (53) NOT NULL,
    [OthLiqFeed2]         FLOAT (53) NOT NULL,
    [OthLiqFeed3]         FLOAT (53) NOT NULL,
    [ConcEthylene]        FLOAT (53) NOT NULL,
    [DiEthylene]          FLOAT (53) NOT NULL,
    [RefEthylene]         FLOAT (53) NOT NULL,
    [ConcPropylene]       FLOAT (53) NOT NULL,
    [DiPropylene]         FLOAT (53) NOT NULL,
    [RefPropylene]        FLOAT (53) NOT NULL,
    CONSTRAINT [PK_EDCCoefficients] PRIMARY KEY CLUSTERED ([FactorSet] ASC)
);

