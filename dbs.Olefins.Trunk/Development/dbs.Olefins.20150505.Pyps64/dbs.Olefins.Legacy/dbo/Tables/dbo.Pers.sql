﻿CREATE TABLE [dbo].[Pers] (
    [Refnum]    [dbo].[Refnum] NOT NULL,
    [PersID]    CHAR (15)      NOT NULL,
    [SortKey]   INT            NOT NULL,
    [SectionID] CHAR (4)       NULL,
    [NumPers]   REAL           NULL,
    [STH]       REAL           NULL,
    [OvtHours]  REAL           NULL,
    [Contract]  REAL           NULL,
    CONSTRAINT [PK_Pers_1__10] PRIMARY KEY NONCLUSTERED ([Refnum] ASC, [PersID] ASC)
);


GO
CREATE CLUSTERED INDEX [PersBySortKey]
    ON [dbo].[Pers]([Refnum] ASC, [SortKey] ASC);

