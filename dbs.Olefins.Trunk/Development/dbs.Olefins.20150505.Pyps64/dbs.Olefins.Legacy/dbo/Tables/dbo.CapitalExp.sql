﻿CREATE TABLE [dbo].[CapitalExp] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [CptlCode]   CHAR (3)       NOT NULL,
    [Onsite]     REAL           NULL,
    [Offsite]    REAL           NULL,
    [Exist]      REAL           NULL,
    [EnergyCons] REAL           NULL,
    [Envir]      REAL           NULL,
    [Safety]     REAL           NULL,
    [Computer]   REAL           NULL,
    [Maint]      REAL           NULL,
    [Total]      REAL           NULL,
    CONSTRAINT [PK___8__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CptlCode] ASC)
);

