﻿CREATE TABLE [dbo].[ReportItemDef] (
    [ReportItemID]    INT                  IDENTITY (1, 1) NOT NULL,
    [ItemName]        VARCHAR (50)         NOT NULL,
    [FactorA]         INT                  NULL,
    [FactorB]         INT                  NULL,
    [Method]          TINYINT              NULL,
    [Multiplier]      REAL                 CONSTRAINT [DF_ReportItem_Multiplier_1__14] DEFAULT (1) NOT NULL,
    [Label]           VARCHAR (50)         CONSTRAINT [DF_ReportItem_Label] DEFAULT (' ') NOT NULL,
    [Format]          TINYINT              CONSTRAINT [DF_ReportItem_Format] DEFAULT (0) NOT NULL,
    [SectionID]       TINYINT              CONSTRAINT [DF_ReportItem_Section] DEFAULT (0) NOT NULL,
    [ItemDefScenario] VARCHAR (8)          NULL,
    [ItemDefSQL]      VARCHAR (255)        NULL,
    [JoinOverride]    VARCHAR (255)        NULL,
    [ItemDefCurrency] [dbo].[CurrencyCode] NULL,
    CONSTRAINT [PK_ReportItemDef_1__18] PRIMARY KEY CLUSTERED ([ReportItemID] ASC),
    CONSTRAINT [UniqueRptItemName] UNIQUE NONCLUSTERED ([ItemName] ASC)
);

