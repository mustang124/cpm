﻿CREATE TABLE [dbo].[PracRely] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [RBICurPcnt]  REAL           NULL,
    [RBIFutPcnt]  REAL           NULL,
    [RBIMethPcnt] REAL           NULL,
    [CompPhil]    CHAR (1)       NULL,
    [RTCGComp]    REAL           NULL,
    [RTCGTurb]    REAL           NULL,
    [RTC3Comp]    REAL           NULL,
    [RTC3Turb]    REAL           NULL,
    [RTC2Comp]    REAL           NULL,
    [RTC2Turb]    REAL           NULL,
    [FreqVib]     REAL           NULL,
    [FreqComp]    REAL           NULL,
    [FreqTurb]    REAL           NULL,
    [FreqStat]    REAL           NULL,
    [CompLife]    INT            NULL,
    CONSTRAINT [PK___8__19] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

