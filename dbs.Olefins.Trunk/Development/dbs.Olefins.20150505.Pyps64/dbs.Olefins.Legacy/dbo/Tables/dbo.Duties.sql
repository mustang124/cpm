﻿CREATE TABLE [dbo].[Duties] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [FeedProdID] VARCHAR (30)   NOT NULL,
    [ImpTx]      REAL           NULL,
    CONSTRAINT [PK___5__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

