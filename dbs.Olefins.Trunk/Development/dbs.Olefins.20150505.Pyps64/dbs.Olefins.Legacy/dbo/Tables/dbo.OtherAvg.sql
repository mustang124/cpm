﻿CREATE TABLE [dbo].[OtherAvg] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [FeedProdID] VARCHAR (20)   NOT NULL,
    [Q1Avg]      REAL           NULL,
    [Q2Avg]      REAL           NULL,
    [Q3Avg]      REAL           NULL,
    [Q4Avg]      REAL           NULL,
    CONSTRAINT [PK___OtherAvg] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

