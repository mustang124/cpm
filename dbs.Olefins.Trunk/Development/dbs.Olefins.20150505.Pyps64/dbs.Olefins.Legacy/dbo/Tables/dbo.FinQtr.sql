﻿CREATE TABLE [dbo].[FinQtr] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [FeedProdID]  VARCHAR (20)   NOT NULL,
    [Q1Avg]       REAL           NULL,
    [Q2Avg]       REAL           NULL,
    [Q3Avg]       REAL           NULL,
    [Q4Avg]       REAL           NULL,
    [AnnTotalBPC] REAL           NULL,
    CONSTRAINT [PK__FinQtr] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);

