﻿
/****** Object:  View dbo.vDOWReliability    Script Date: 3/16/2007 10:40:12 AM ******/

/****** Object:  View dbo.vDOWReliability    Script Date: 3/16/2007 10:09:57 AM ******/

/****** Object:  View dbo.vDOWReliability    Script Date: 3/16/2007 8:46:31 AM ******/
CREATE    VIEW dbo.vDOWReliability
AS
SELECT t.Refnum, 'Type'='CategoryA', 
'AnnDTLoss'=(AVG(CASE WHEN SortKey = 1 THEN AnnDTLoss END)+AVG(CASE WHEN SortKey = 23 THEN DTLoss end))
 /(SELECT EthylCapKMTA FROM Capacity WHERE Capacity.Refnum = t.Refnum)*.1
,'SDLoss'=SUM(SDLoss/c.EthylCapKMTA)*.1
, 'AnnTotLoss'=(AVG(CASE WHEN SortKey = 1 THEN AnnTotLoss END)+AVG(CASE WHEN SortKey = 23 THEN DTLoss end)+
AVG(CASE WHEN SortKey = 23 THEN SDLoss end))
 /(SELECT EthylCapKMTA FROM Capacity WHERE Capacity.Refnum = t.Refnum)*.1
FROM ProdLoss p
INNER JOIN ProdLoss_LU lu ON p.Category = lu.Category AND p.CauseID = lu.CauseID
INNER JOIN TSort t ON t.Refnum = p.Refnum
INNER JOIN Capacity c ON c.Refnum = t.Refnum
WHERE SortKey IN (1,23)
GROUP BY t.Refnum
UNION
SELECT t.Refnum, 'Type'='CategoryB', 'DTLoss'=SUM(DTLoss/c.EthylCapKMTA)*.1
,'SDLoss'=SUM(SDLoss/c.EthylCapKMTA)*.1, 'TotLoss'=SUM(TotLoss/c.EthylCapKMTA)*.1
FROM ProdLoss p
INNER JOIN ProdLoss_LU lu ON p.Category = lu.Category AND p.CauseID = lu.CauseID
INNER JOIN TSort t ON t.Refnum = p.Refnum
INNER JOIN Capacity c ON c.Refnum = t.Refnum
WHERE SortKey IN (9,10,11,12,13,14,15)
GROUP BY t.Refnum
UNION
SELECT t.Refnum, 'Type'='CategoryC', 'DTLoss'=SUM(DTLoss/c.EthylCapKMTA)*.1
,'SDLoss'=SUM(SDLoss/c.EthylCapKMTA)*.1, 'TotLoss'=SUM(TotLoss/c.EthylCapKMTA)*.1
FROM ProdLoss p
INNER JOIN ProdLoss_LU lu ON p.Category = lu.Category AND p.CauseID = lu.CauseID
INNER JOIN TSort t ON t.Refnum = p.Refnum
INNER JOIN Capacity c ON c.Refnum = t.Refnum
WHERE SortKey IN (4,8)
GROUP BY t.Refnum
UNION
SELECT t.Refnum, 'Type'='CategoryD', 'DTLoss'=SUM(DTLoss/c.EthylCapKMTA)*.1
,'SDLoss'=SUM(SDLoss/c.EthylCapKMTA)*.1, 'TotLoss'=SUM(TotLoss/c.EthylCapKMTA)*.1
FROM ProdLoss p
INNER JOIN ProdLoss_LU lu ON p.Category = lu.Category AND p.CauseID = lu.CauseID
INNER JOIN TSort t ON t.Refnum = p.Refnum
INNER JOIN Capacity c ON c.Refnum = t.Refnum
WHERE SortKey IN (2)
GROUP BY t.Refnum
UNION
SELECT t.Refnum, 'Type'='CategoryE', 'DTLoss'=SUM(DTLoss/c.EthylCapKMTA)*.1
,'SDLoss'=SUM(SDLoss/c.EthylCapKMTA)*.1, 'TotLoss'=SUM(TotLoss/c.EthylCapKMTA)*.1
FROM ProdLoss p
INNER JOIN ProdLoss_LU lu ON p.Category = lu.Category AND p.CauseID = lu.CauseID
INNER JOIN TSort t ON t.Refnum = p.Refnum
INNER JOIN Capacity c ON c.Refnum = t.Refnum
WHERE SortKey IN (16,17,18,19,20)
GROUP BY t.Refnum
UNION
SELECT t.Refnum, 'Type'='CategoryF', 'DTLoss'=SUM(DTLoss/c.EthylCapKMTA)*.1
,'SDLoss'=SUM(SDLoss/c.EthylCapKMTA)*.1, 'TotLoss'=SUM(TotLoss/c.EthylCapKMTA)*.1
FROM ProdLoss p
INNER JOIN ProdLoss_LU lu ON p.Category = lu.Category AND p.CauseID = lu.CauseID
INNER JOIN TSort t ON t.Refnum = p.Refnum
INNER JOIN Capacity c ON c.Refnum = t.Refnum
WHERE SortKey IN (3,22)
GROUP BY t.Refnum
UNION ALL
SELECT t.Refnum, 'Type'='Total'
,  (SUM(DTLoss) + (SELECT SUM(AnnDTLoss) FROM ProdLoss WHERE t.Refnum = ProdLoss.Refnum AND 
  ProdLoss.CauseID = 'PlantTA'))/(SELECT EthylCapKMTA FROM Capacity WHERE Capacity.Refnum = t.Refnum)*.1
, (SUM(SDLoss) + (SELECT SUM(SDLoss) FROM ProdLoss WHERE t.Refnum = ProdLoss.Refnum AND 
  ProdLoss.CauseID = 'PlantTA'))/(SELECT EthylCapKMTA FROM Capacity WHERE Capacity.Refnum = t.Refnum)*.1
, (SUM(TotLoss) + (SELECT SUM(AnnTotLoss) FROM ProdLoss WHERE t.Refnum = ProdLoss.Refnum AND 
  ProdLoss.CauseID = 'PlantTA'))/(SELECT EthylCapKMTA FROM Capacity WHERE Capacity.Refnum = t.Refnum)*.1
FROM ProdLoss p
INNER JOIN ProdLoss_LU lu ON p.Category = lu.Category AND p.CauseID = lu.CauseID
INNER JOIN TSort t ON t.Refnum = p.Refnum
INNER JOIN Capacity c ON c.Refnum = t.Refnum
WHERE SortKey IN (23,9,10,11,12,13,14,15,4,8,2,16,17,18,19,20,3,22) 
GROUP BY t.Refnum



