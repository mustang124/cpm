﻿

CREATE PROCEDURE spDDLookups;4
	@LookupID integer,
	@ObjName varchar(30) ,
	@ColName varchar(30) ,
	@DescField varchar(30) ,
	@Desc varchar(100) = NULL
AS
IF NOT EXISTS (SELECT * FROM DD_Lookups 
	WHERE LookupID = @LookupID)
	RETURN -101
ELSE
	UPDATE DD_Lookups
	SET ObjectName = @ObjName, ColumnName = @ColName, 
	DescField = @DescField, Description = @Desc
	WHERE LookupID = @LookupID


