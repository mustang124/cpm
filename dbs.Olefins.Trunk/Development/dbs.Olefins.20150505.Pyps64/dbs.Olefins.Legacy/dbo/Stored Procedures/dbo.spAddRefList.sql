﻿
/****** Object:  Stored Procedure dbo.spAddRefList    Script Date: 4/18/2003 4:32:52 PM ******/

CREATE PROCEDURE spAddRefList @ListName RefListName,
			 @Owner varchar(5) = NULL,
			 @JobNo varchar(10) = NULL,
			 @Description varchar(50) = NULL,
			 @ListNo RefListNo OUTPUT
AS
IF EXISTS (SELECT * FROM RefList_LU WHERE ListName = @ListName)
	SELECT @ListNo = -1
ELSE
BEGIN
	INSERT INTO RefList_LU (ListName, Owner, JobNo, Description)
	VALUES (@ListName, @Owner, @JobNo, @Description)
	SELECT @ListNo = RefListNo
	FROM RefList_LU
	WHERE ListName = @ListName
END


