﻿
/****** Object:  Stored Procedure dbo.spDDLookups    Script Date: 4/18/2003 4:32:53 PM ******/

/****** Object:  Stored Procedure dbo.spDDLookups    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.spDDLookups    Script Date: 04/13/2000 8:32:50 AM ******/
/* This procedure gets the LookupID for a given lookup table.	*/
CREATE PROCEDURE spDDLookups;1
	@ObjName varchar(30),
	@LookupID integer OUTPUT
AS
IF EXISTS (SELECT * FROM DD_Lookups WHERE ObjectName = @ObjName)
	SELECT @LookupID = LookupID
	FROM DD_Lookups
	WHERE ObjectName = @ObjName
ELSE
	SELECT @LookupID = -1


