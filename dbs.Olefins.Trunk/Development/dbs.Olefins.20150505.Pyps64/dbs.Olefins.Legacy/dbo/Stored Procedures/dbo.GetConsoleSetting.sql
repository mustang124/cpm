﻿
-- =============================================
-- Author:		GLC
-- Create date: 02-Feb-2011
-- Description:	Retrieve a value from ConsoleSettings table to replace use of registry
-- =============================================
CREATE PROCEDURE [dbo].[GetConsoleSetting] (@UserName varchar(10), @Application varchar(255), @Section varchar(255), @KeyName varchar(255), @KeyValue varchar(255) OUTPUT)
AS
BEGIN
	SET NOCOUNT ON
	SET @KeyValue = ''
	SELECT @KeyValue = KeyValue
	FROM dbo.ConsoleSettings 
	WHERE UserName = @UserName AND Application = @Application AND Section = @Section AND KeyName = @KeyName
	IF @KeyValue IS NULL
		SET @KeyValue = ''	
END

