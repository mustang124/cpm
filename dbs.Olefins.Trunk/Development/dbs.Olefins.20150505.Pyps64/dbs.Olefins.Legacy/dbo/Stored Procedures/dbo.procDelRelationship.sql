﻿
/****** Object:  Stored Procedure dbo.procDelRelationship    Script Date: 4/18/2003 4:32:52 PM ******/

/****** Object:  Stored Procedure dbo.procDelRelationship    Script Date: 12/28/2001 7:34:24 AM ******/
/****** Object:  Stored Procedure dbo.procDelRelationship    Script Date: 04/13/2000 8:32:49 AM ******/
CREATE PROC procDelRelationship @Object1 varchar(30), @Object2 varchar(30) AS
DECLARE @Obj1ID integer, @Obj2ID integer
EXEC procGetObjectID @Object1, @Obj1ID OUTPUT
EXEC procGetObjectID @Object2, @Obj2ID OUTPUT
DELETE FROM DD_Relationships
WHERE (ObjectA = @Obj1ID AND ObjectB = @Obj2ID)
 OR (ObjectA = @Obj2ID AND ObjectB = @Obj1ID)


