﻿CREATE TABLE [Console].[Consultants] (
    [Initials]       NVARCHAR (3)  NULL,
    [ConsultantName] NVARCHAR (80) NULL,
    [StudyYear]      NVARCHAR (4)  NULL,
    [StudyType]      NVARCHAR (10) NULL,
    [Active]         BIT           NULL
);

