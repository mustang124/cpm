﻿CREATE proc [Console].[GetTimeSummary]
@RefNum dbo.Refnum
as
Select Consultant, ValTime = Sum(ValTime), LastTime = MAX(StartTime) from Val.ValTime where refnum = @RefNum And LEN(Consultant)<4 Group by Consultant Order by LastTime Desc
