﻿CREATE PROCEDURE [Console].[GetContactInfo]
	@RefNum dbo.Refnum
AS
BEGIN
	SELECT	CoordName, 
			CoordTitle,
			PlantName, 
			Telephone, 
			CoordTitle,
			PlantLoc,
			Street,
			City,
			State,
			Country,
			Zip,
			Fax, 
			www,
			PricingContact,
			PricingContactEmail,
			DCContact,
			DCContactEmail,
			PlantCountry,
			Region
			 
	FROM [Tsort]  
	WHERE RefNum = @RefNum OR Refnum = dbo.FormatRefNum(@Refnum, 0)
END

