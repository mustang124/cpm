﻿CREATE PROC [Console].[CheckSANumber] 

@RefNum as dbo.Refnum

AS

SELECT * FROM SAMaster where StudyAndYear = dbo.FormatRefNum(@RefNum,2) + dbo.FormatRefNum(@RefNum,3)
