﻿CREATE TABLE [dim].[Operator] (
    [Operator] CHAR (1) CONSTRAINT [DF_Operator] DEFAULT ('+') NOT NULL,
    CONSTRAINT [PK_Operator] PRIMARY KEY CLUSTERED ([Operator] ASC),
    CONSTRAINT [CR_Operator] CHECK ([Operator]='*' OR [Operator]='/' OR [Operator]='+' OR [Operator]='-' OR [Operator]='~')
);

