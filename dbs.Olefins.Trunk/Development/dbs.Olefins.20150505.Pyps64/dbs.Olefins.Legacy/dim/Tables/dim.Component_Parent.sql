﻿CREATE TABLE [dim].[Component_Parent] (
    [MethodologyId]  INT                 NOT NULL,
    [ComponentId]    INT                 NOT NULL,
    [ParentId]       INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Component_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Component_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Component_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Component_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Component_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Component_Parent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [ComponentId] ASC),
    CONSTRAINT [FK_Component_Parent_LookUp_Components] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Component_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Component_Parent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Component_Parent_Operator] FOREIGN KEY ([Operator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Component_Parent_Parent] FOREIGN KEY ([MethodologyId], [ComponentId]) REFERENCES [dim].[Component_Parent] ([MethodologyId], [ComponentId])
);


GO

CREATE TRIGGER [dim].[t_Component_Parent_u]
	ON [dim].[Component_Parent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Component_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Component_Parent].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Component_Parent].[ComponentId]		= INSERTED.[ComponentId];

END;