﻿CREATE TABLE [dim].[Severity_LookUp] (
    [SeverityId]     INT                IDENTITY (1, 1) NOT NULL,
    [SeverityTag]    VARCHAR (42)       NOT NULL,
    [SeverityName]   NVARCHAR (84)      NOT NULL,
    [SeverityDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Severity_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Severity_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Severity_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Severity_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Severity_LookUp] PRIMARY KEY CLUSTERED ([SeverityId] ASC),
    CONSTRAINT [CL_Severity_LookUp_SeverityDetail] CHECK ([SeverityDetail]<>''),
    CONSTRAINT [CL_Severity_LookUp_SeverityName] CHECK ([SeverityName]<>''),
    CONSTRAINT [CL_Severity_LookUp_SeverityTag] CHECK ([SeverityTag]<>''),
    CONSTRAINT [UK_Severity_LookUp_LanguageTag] UNIQUE NONCLUSTERED ([SeverityTag] ASC),
    CONSTRAINT [UK_Severity_LookUp_SeverityDetail] UNIQUE NONCLUSTERED ([SeverityDetail] ASC),
    CONSTRAINT [UK_Severity_LookUp_SeverityName] UNIQUE NONCLUSTERED ([SeverityName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Severity_LookUp_u]
	ON [dim].[Severity_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Severity_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Severity_LookUp].[SeverityId]	= INSERTED.[SeverityId];

END;