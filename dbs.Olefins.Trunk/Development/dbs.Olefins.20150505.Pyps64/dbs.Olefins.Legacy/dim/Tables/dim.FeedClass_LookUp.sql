﻿CREATE TABLE [dim].[FeedClass_LookUp] (
    [FeedClassId]     INT                IDENTITY (1, 1) NOT NULL,
    [FeedClassTag]    VARCHAR (42)       NOT NULL,
    [FeedClassName]   VARCHAR (84)       NOT NULL,
    [FeedClassDetail] VARCHAR (256)      NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_FeedClass_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (128)     CONSTRAINT [DF_FeedClass_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (128)     CONSTRAINT [DF_FeedClass_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (128)     CONSTRAINT [DF_FeedClass_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedClass_LookUp] PRIMARY KEY CLUSTERED ([FeedClassId] ASC),
    CONSTRAINT [CL_FeedClass_LookUp_FeedClassDetail] CHECK ([FeedClassDetail]<>''),
    CONSTRAINT [CL_FeedClass_LookUp_FeedClassName] CHECK ([FeedClassName]<>''),
    CONSTRAINT [CL_FeedClass_LookUp_FeedClassTag] CHECK ([FeedClassTag]<>''),
    CONSTRAINT [UK_FeedClass_LookUp_FeedClassDetail] UNIQUE NONCLUSTERED ([FeedClassDetail] ASC),
    CONSTRAINT [UK_FeedClass_LookUp_FeedClassName] UNIQUE NONCLUSTERED ([FeedClassName] ASC),
    CONSTRAINT [UK_FeedClass_LookUp_FeedClassTag] UNIQUE NONCLUSTERED ([FeedClassTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_FeedClass_LookUp_u]
	ON [dim].[FeedClass_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedClass_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[FeedClass_LookUp].[FeedClassId]		= INSERTED.[FeedClassId];

END;