﻿CREATE PROCEDURE [web].[Update_StreamComposition]
(
	@SubmissionId			INT,
	@StreamNumber			INT,
	@ComponentId			INT,

	@Component_WtPcnt		FLOAT	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS (SELECT TOP 1 1 FROM [stage].[StreamComposition] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamNumber] = @StreamNumber AND t.[ComponentId] = @ComponentId)
	BEGIN
		SET @Component_WtPcnt = COALESCE(@Component_WtPcnt, 0.0);
		EXECUTE [stage].[Update_StreamComposition] @SubmissionId, @StreamNumber, @ComponentId, @Component_WtPcnt;
	END
	ELSE
	BEGIN
		IF(@Component_WtPcnt >= 0.0)
		EXECUTE [stage].[Insert_StreamComposition] @SubmissionId, @StreamNumber, @ComponentId, @Component_WtPcnt;
	END;

END;