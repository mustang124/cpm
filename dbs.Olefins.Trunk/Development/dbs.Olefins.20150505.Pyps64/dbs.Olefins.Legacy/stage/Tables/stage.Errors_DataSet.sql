﻿CREATE TABLE [stage].[Errors_DataSet] (
    [ErrorId]           INT                IDENTITY (1, 1) NOT NULL,
    [LanguageId]        INT                CONSTRAINT [DF_Message_Language_MessageLanguage] DEFAULT ((1033)) NOT NULL,
    [MessageId]         INT                NOT NULL,
    [SeverityId]        INT                NOT NULL,
    [SubmissionId]      INT                NOT NULL,
    [DisplayName]       NVARCHAR (256)     NULL,
    [DisplayDetail]     NVARCHAR (256)     NULL,
    [DisplaySection]    NVARCHAR (256)     NULL,
    [DisplayLocation]   NVARCHAR (256)     NULL,
    [DisplayError]      NVARCHAR (512)     NULL,
    [DisplayCorrection] NVARCHAR (512)     NULL,
    [Notes]             NVARCHAR (MAX)     NULL,
    [tsInserted]        DATETIMEOFFSET (7) CONSTRAINT [DF_Errors_DataSet_tsInserted] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Errors_DataSet_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_Errors_DataSet_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_Errors_DataSet_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_Errors_DataSet_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Errors_DataSet] PRIMARY KEY CLUSTERED ([ErrorId] DESC),
    CONSTRAINT [CL_Errors_DataSet_MessageCorrection] CHECK ([DisplayCorrection]<>''),
    CONSTRAINT [CL_Errors_DataSet_MessageDetail] CHECK ([DisplayDetail]<>''),
    CONSTRAINT [CL_Errors_DataSet_MessageError] CHECK ([DisplayError]<>''),
    CONSTRAINT [CL_Errors_DataSet_MessageLocation] CHECK ([DisplayLocation]<>''),
    CONSTRAINT [CL_Errors_DataSet_MessageName] CHECK ([DisplayName]<>''),
    CONSTRAINT [CL_Errors_DataSet_MessageSection] CHECK ([DisplaySection]<>''),
    CONSTRAINT [CL_Errors_DataSet_Notes] CHECK ([Notes]<>''),
    CONSTRAINT [FK_Message_Language_lcid] FOREIGN KEY ([LanguageId]) REFERENCES [dim].[SysLanguages_LookUp] ([lcid]),
    CONSTRAINT [FK_Message_Language_Message_LookUp] FOREIGN KEY ([MessageId]) REFERENCES [dim].[Message_LookUp] ([MessageId]),
    CONSTRAINT [FK_Message_Language_Severity_LookUp] FOREIGN KEY ([SeverityId]) REFERENCES [dim].[Severity_LookUp] ([SeverityId])
);


GO

CREATE TRIGGER [stage].[t_Errors_DataSet_u]
	ON [stage].[Errors_DataSet]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[Errors_DataSet]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[Errors_DataSet].[ErrorId]	= INSERTED.[ErrorId];

END;