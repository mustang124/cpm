﻿ALTER PROCEDURE [fact].[Select_Refnums]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		[t].[Refnum],
		[t].[PreviousRefnum]
	FROM
		[fact].[PlantNameHistoryLimit](2009)	[t]
	WHERE	[t].[StudyYear]	=	2015
		AND	[t].[Refnum] IN ('2015PCH210', '2015PCH123', '2015PCH029', '2015PCH217');

END;