﻿select
	'[f].[' + [c].[COLUMN_NAME] + '],'
FROM (VALUES
	('ROGEntryPoint')
	) [t] ([TABLE_NAME])

LEFT OUTER JOIN
	information_schema.columns	[c]
		ON	[c].[TABLE_SCHEMA]	= 'fact'
		AND	[c].[TABLE_NAME]	= [t].[TABLE_NAME]
		--AND	[s].[COLUMN_NAME]	= [c].[COLUMN_NAME]

ORDER BY 
	[c].[ORDINAL_POSITION] ASC