﻿USE [Olefins];
GO

SET NOCOUNT ON;

--------------------------------------'123456789012345';
DECLARE @ListId				VARCHAR(20)	= '13PCH'					--	Source List (limit plants to this list)
DECLARE @UserGroup			VARCHAR(20) = 'CPCHEMUS'				--	List Name
DECLARE @ReportGroupName	VARCHAR(40)	= 'CP CHEM US';				--	List Description
DECLARE @FactorSetId		VARCHAR(4)	= '2013';					--	

DECLARE @IsCompanyGroup		INT			= 0;						--	Appends 'c' to ListId
DECLARE @InOutPut			INT			= 1;						--	Report on OUTPUT.XLS

DECLARE @Title1				VARCHAR(20) = 'CP CHEM US';				--	'Solomon';
DECLARE @Title2				VARCHAR(20) = NULL;						--	'Feed Class 4'; --'Partners';
DECLARE @Title3				VARCHAR(20) = NULL;						--	'Naphtha/HEavy'; --'2013';

DECLARE @Database			VARCHAR(128) = 'Olefins13';

SET @ListId = @ListId + CASE WHEN (@IsCompanyGroup = 1 AND RIGHT(@ListId, 3) = 'PCH') THEN 'c' ELSE '' END;

DECLARE @TargetId			VARCHAR(25) = COALESCE(@ListId + ':', '') + @UserGroup;

DECLARE @RefnumsPeerGroup TABLE
(
	[Refnum]	VARCHAR(25)		CHECK([Refnum] <> '')
	PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

DECLARE @RefnumsOutput TABLE
(
	[Refnum]	VARCHAR(25)		CHECK([Refnum] <> '')
	PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

--	Refnums for the peer group
INSERT INTO @RefnumsPeerGroup([Refnum])
SELECT t.[Refnum]
FROM (VALUES
	('2013PCH004'),
	('2013PCH005'),
	('2013PCH23A'),
	('2013PCH23B'),
	('2013PCH23C')
	) t ([Refnum])
WHERE t.[Refnum] IN (SELECT sa.[Refnum] FROM [cons].[SubscriptionsAssets] sa);

--	Refnums where output will appear
INSERT INTO @RefnumsOutput([Refnum])
SELECT t.[Refnum]
FROM (VALUES
	('2013PCH004'),
	('2013PCH005'),
	('2013PCH23A'),
	('2013PCH23B'),
	('2013PCH23C')	) t ([Refnum])
WHERE t.[Refnum] IN (SELECT sa.[Refnum] FROM [cons].[SubscriptionsAssets] sa);

PRINT '';
PRINT '';
PRINT '1.	Olefins:   Insert the refnums into cons.reflist';			--		This associates a group of plants with a list

INSERT INTO [cons].[RefList]([ListId], [UserGroup], [Refnum])
SELECT
	@ListId,
	@UserGroup,
	cr.[Refnum]
FROM @RefnumsPeerGroup cr
LEFT OUTER JOIN [cons].[RefList] [rl]
	ON	[rl].[ListID]		= @ListId
	AND	[rl].[UserGroup]	= @UserGroup
	AND	[rl].[Refnum]		= [cr].[Refnum]
WHERE	[rl].[Refnum]	IS NULL;

PRINT '';
PRINT '';
PRINT '2.	Olefins:   Create a ReportGroup';							--		Determines how one wants to report a group of plants

INSERT INTO [reports].[ReportGroups]([GroupID], [ReportGroupName], [ListId], [UserGroup], [KeepUpdated])
SELECT [t].[GroupID], [t].[ReportGroupName], [t].[ListId], [t].[UserGroup], [t].[KeepUpdated]
FROM (VALUES
	(@TargetId,
	@ReportGroupName,
	@ListId,
	@UserGroup,
	'N')
	) [t]([GroupID], [ReportGroupName], [ListID], [UserGroup], [KeepUpdated])
LEFT OUTER JOIN [reports].[ReportGroups] [rg]
	ON	[rg].[GroupID]		= @TargetId
	AND	[rg].[ListId]		= @ListId
	AND	[rg].[UserGroup]	= @UserGroup
WHERE	[rg].[GroupID]		IS NULL;

PRINT '';
PRINT '';
PRINT '3.	Olefins/13 : Exec reports.spALL ReportGroupName';			--		Calculate values for the reports

EXECUTE [Olefins].[reports].[spALL] @TargetId;
EXECUTE [Olefins13].[reports].[spALL] @TargetId;

PRINT '';
PRINT '';
PRINT '4.	Olefins:   Insert a record(s) into Gap.peers';				--		Setup the gaps

INSERT INTO [Olefins].[gap].[Peers]([GroupId], [TargetId], [PeerNumber], [PeerDetail], [InOutPut], [DatabaseName], [Title1], [Title2], [Title3])
SELECT
	[ro].[Refnum],
	@TargetId,
	ROW_NUMBER() OVER(ORDER BY [ro].[Refnum]) * 10,
	@UserGroup,
	@InOutPut,
	@Database,
	@Title1,
	@Title2,
	@Title3
FROM @RefnumsOutput	[ro]
LEFT OUTER JOIN [gap].[Peers] [p]
	ON	[p].[TargetId]	= @TargetId
	AND	[p].[GroupId]	= [ro].[Refnum]
WHERE [p].[GroupId]	IS NULL;

PRINT '';
PRINT '';
PRINT '5.	Olefins13: Calculate the GAP values in the frozen database';

EXECUTE [Olefins13].[calc].[CalculateGroup] @FactorSetId, @TargetId;

PRINT '';
PRINT '';
PRINT '6.	Olefins:   Process the gaps';

EXECUTE [gap].[ProcessGaps] NULL, @TargetId, @Database;


---------------------------------------------------------------------
---------------------------------------------------------------------

INSERT INTO [gap].[Peers]
(
	[GroupId],
	[TargetId],
	[PeerNumber],
	[DatabaseName],
	[InOutPut]
)
SELECT
	[t].[Refnum],
		[TargetId]		= '13PCH:HPLPG',
		[PeerNumber]	= 0,
		[DatabaseName]	= 'Olefins13',
		[InOutPut]		= 0
FROM (VALUES
	('2013PCH029'),
	('2013PCH029P')
	) [t]([Refnum]);

EXECUTE [gap].[ProcessGaps] '2013PCH029', NULL, 'Olefins13';
EXECUTE [gap].[ProcessGaps] '2013PCH029P', NULL, 'Olefins13';


INSERT INTO [cons].[RefList]
(
	[ListID],
	[Refnum],
	[UserGroup]
)
SELECT
		[ListID]	= '13PCHc',
	[t].[Refnum],
		[UserGroup]	= 'FHR13'
FROM (VALUES
	('2013PCH029'),
	('2013PCH029P')
	) [t]([Refnum]);


INSERT INTO [cons].[RefList]
(
	[ListID],
	[Refnum],
	[UserGroup]
)
SELECT
		[ListID]	= '13PCH+late',
	[t].[Refnum],
		[UserGroup]	= 'NA'
FROM(VALUES
	('2013PCH029'),
	('2013PCH029P')
	) [t]([Refnum]);
