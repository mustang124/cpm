﻿SELECT
	[f].[Refnum],
	[f].[SmallRefnum],
	[f].[StudyYear],

	[f].[Co],
	[f].[Loc],
	[f].[CoLoc],

	[f].[PreviousRefnum],
	[f].[PreviousStudyYear],
	[f].[PreviousForexRate],
	[f].[StudyYearDifference],
		[C#]					= 'refnums.Add(new RefnumPair("' + [f].[Refnum] + '", "' + [f].[PreviousRefnum] + '"));'
FROM
	[fact].[PlantNameHistoryLimit](2009) [f]
WHERE	[f].[StudyYear] = 2015
	AND	[f].[AssetID]	IN
	(
		'196',
		'234',
		'222',
		'099',
		'148',
		'228',
		'053',
		'231',
		'214',
		'205',
		'152',
		'214',
		'012',
		'175'
	);

