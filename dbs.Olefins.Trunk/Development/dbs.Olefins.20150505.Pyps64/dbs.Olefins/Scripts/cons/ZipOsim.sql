﻿DECLARE @StudyYear		VARCHAR(4)	= '2015';

DECLARE @CorresPath		VARCHAR(256) = '\\Dallas2\DATA\Data\STUDY\Olefins\{STUDYYEAR}\Correspondence\{SMALLREFNUM}\';
DECLARE @PlantsPath		VARCHAR(256) = '\\Dallas2\DATA\Data\STUDY\Olefins\{STUDYYEAR}\Plants\{SMALLREFNUM}\'
DECLARE @OsimHelpPath	VARCHAR(256) = '\\Dallas2\DATA\Data\STUDY\Olefins\{STUDYYEAR}\InputForms\';

DECLARE @FileTypes TABLE
(
	[FileTypeId]	CHAR(4)			NOT	NULL CHECK([FileTypeId] <> ''),
	[SourcePath]	VARCHAR(256)	NOT	NULL CHECK([SourcePath] <> ''),
	[SourceFile]	VARCHAR(256)		NULL CHECK([SourceFile] <> ''),
	PRIMARY KEY CLUSTERED([FileTypeId] ASC)
);

INSERT INTO @FileTypes
(
	[FileTypeId],
	[SourcePath],
	[SourceFile]
)
VALUES
	('OSIM', @PlantsPath, NULL),
	('HELP', @OsimHelpPath, 'OSIM2015 Instructions.pdf'),
	('INST', @OsimHelpPath, 'OSIM2015 Installation Instructions.pdf');

SELECT
		[TargetPath]	= REPLACE(REPLACE(@CorresPath, '{STUDYYEAR}', @StudyYear), '{SMALLREFNUM}', [t].[SmallRefnum]),
		[TargetFile]	= 'OSIM' + @StudyYear + ' ' + [t].[CoLoc] + '.zip',
		[ZipPassword]	= COALESCE([t].[CompanyPassword], ''),
		[SourcePath]	= REPLACE(REPLACE([f].[SourcePath], '{STUDYYEAR}', @StudyYear), '{SMALLREFNUM}', [t].[SmallRefnum]),
		[SouceFile]		= COALESCE([f].[SourceFile], 'OSIM' + @StudyYear + ' ' + [t].[CoLoc] + '.xlsm'),
		[Rename]		= '',
		[Compression]	= 'PPZ'
FROM
	[cons].[TSort]	[t]
CROSS JOIN
	@FileTypes		[f]
WHERE	[t].[StudyYear] = CONVERT(INT, @StudyYear)
	--AND	[t].[AssetID]	IN
	--(
	--	--'012',
	--	--'053',
	--	--'099',
	--	--'148',
	--	--'152',
	--	--'175',
	--	--'196',
	--	--'205',
	--	--'214',
	--	--'222',
	--	--'228',
	--	--'231',
	--	--'234',

	--	--'126',
	--	--'147',
	--	--'181'

	--	'213',
	--	'214'
	--)
ORDER BY
	[t].[Refnum]		ASC,
	[f].[FileTypeId]	ASC;
