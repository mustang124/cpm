﻿Imports System.IO

Public Module Actions

    ''' <summary>
    ''' Copies a simulation folder from one direcotry to another, places target into a sub-directory.
    ''' </summary>
    ''' <param name="PathSource">String representing the path to the original folder to be copied.</param>
    ''' <param name="PathTarget">String representing the path to the target location.</param>
    ''' <param name="PathUnique">String representing a subdirectory of PathTarget.</param>
    ''' <returns>String</returns>
    ''' <remarks>
    ''' <list type= "table">
    ''' <listheader>
    '''    <term>Version</term>
    '''    <description>Changes</description>
    ''' </listheader>
    ''' <item>
    '''    <term>1.0 (20110830 RRH)</term>
    '''    <description>Base</description>
    ''' </item>
    ''' <item>
    '''    <term>1.1 (20110830 RRH)</term>
    '''    <description>Removed partially defined target path (root); moved entire target path to the parameters PathTarget and PathUnique.</description>
    ''' </item>
    ''' </list>
    ''' </remarks>
    Public Function CopyEngine(
        ByVal PathSource As String,
        ByVal PathTarget As String,
        Optional ByVal PathUnique As String = "") As String

        Dim sCopyEngine As String = Nothing
        If PathUnique = "" Then PathUnique = System.Guid.NewGuid.ToString()

        ' 20110620 CodeReview: Root drive does not have read/write access to PathEngineTarget (20110627 Possibly use TempFileLocation)
        ' 20110620 CodeReview: Verify error with CopyEngine (20110627 use sCopyEngine)
        Dim EnginePath As String = PathTarget + PathUnique + "\"

        ' 20110620 CodeReview: verify path is returned properly (CopyFolder verifies folder exists)
        If Sa.FileOps.CopyFolder(PathSource, EnginePath) Then sCopyEngine = EnginePath

        Return sCopyEngine

    End Function

End Module

Public MustInherit Class ErrorCode

    Public Const None As System.Int32 = 0

    Public Const Cancel As System.Int32 = -97
    Public Const Initilization As System.Int32 = -98
    Public Const Notice As System.Int32 = -99
    Public Const Finished As System.Int32 = -100

    Public Const SourceFiles As System.Int32 = -1
    Public Const Prepare As System.Int32 = -2
    Public Const Process As System.Int32 = -3
    Public Const Worker As System.Int32 = -4

    Public Const Energy As System.Int32 = -5
    Public Const Composition As System.Int32 = -6
    Public Const EnergyAndComposition As System.Int32 = -7

    Public Const FeedError As System.Int32 = -10
    Public Const PlantComposition As System.Int32 = -8

    Public Const Password As System.Int32 = -2001

End Class

Public MustInherit Class Threading

    Public Shared Function PopulateArgument(
        ByVal ConnectionString As String,
        ByVal rdr As System.Data.SqlClient.SqlDataReader,
        ByVal DeleteFolder As Boolean) As Sa.Simulation.Threading.Argument

        Dim a As New Sa.Simulation.Threading.Argument

        a.ConnectionString = ConnectionString

        a.QueueID = rdr.GetInt64(rdr.GetOrdinal("QueueID"))
        a.FactorSetID = rdr.GetString(rdr.GetOrdinal("FactorSetID"))
        a.Refnum = rdr.GetString(rdr.GetOrdinal("Refnum"))

        a.DeleteFolder = DeleteFolder

        Return a

    End Function

    ''' <summary>
    ''' Background Worker Argument containing the parameters used to filter the simulation feeds run.
    ''' </summary>
    ''' <remarks></remarks>
    Public NotInheritable Class Argument

        Private _ConnectionString As String
        Private _QueueID As System.Int64
        Private _FactorSetID As String
        Private _Refnum As String
        Private _DeleteFolder As Boolean

        Public Property QueueID As System.Int64

            Set(ByVal value As System.Int64)
                _QueueID = value
            End Set

            Get
                Return _QueueID
            End Get

        End Property

        Public Property FactorSetID As String

            Set(ByVal value As String)
                _FactorSetID = value
            End Set

            Get
                Return _FactorSetID
            End Get

        End Property

        Public Property Refnum As String

            Set(ByVal value As String)
                _Refnum = value
            End Set

            Get
                Return _Refnum
            End Get

        End Property

        Public Property ConnectionString As String

            Set(ByVal value As String)
                _ConnectionString = value
            End Set

            Get
                Return _ConnectionString
            End Get

        End Property

        Public Property DeleteFolder As Boolean

            Set(ByVal value As Boolean)
                _DeleteFolder = value
            End Set

            Get
                Return _DeleteFolder
            End Get

        End Property

    End Class

    Public MustInherit Class BackgroundWorker
        Inherits System.ComponentModel.BackgroundWorker

        Private SimWorker As Sa.Simulation.Threading.Engine

        Protected Friend Sub New(ByVal SimWkr As Sa.Simulation.Threading.Engine)
            Me.WorkerReportsProgress = True
            Me.WorkerSupportsCancellation = True
            Me.SimWorker = SimWkr
        End Sub

        Protected Overrides Sub OnDoWork(ByVal e As System.ComponentModel.DoWorkEventArgs)
            MyBase.OnDoWork(e)
            Me.SimWorker.OnDoWork(Me, e)
        End Sub

        Protected Overrides Sub OnProgressChanged(ByVal e As System.ComponentModel.ProgressChangedEventArgs)
            MyBase.OnProgressChanged(e)
            Me.SimWorker.OnProgressChanged(Me, e)
        End Sub

        Protected Overrides Sub OnRunWorkerCompleted(ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
            MyBase.OnRunWorkerCompleted(e)
            Me.SimWorker.OnRunWorkerCompleted(Me, e)
        End Sub

        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            MyBase.Dispose(disposing)
        End Sub

    End Class

    Public MustInherit Class Engine

        Private BGWs As New List(Of Sa.Simulation.Threading.BackgroundWorker)

        Public QueuedItems As New Queue(Of Sa.Simulation.Threading.Argument)
        Protected lv As System.Windows.Forms.ListView

        Private Event Done()

        Protected Friend MustOverride Sub OnDoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
        Public MustOverride Sub RunSimulation(ByVal lv As System.Windows.Forms.ListView, ByVal ThreadCount As System.Int32)

        Public MustOverride Sub StopSimulations()

        Protected Friend Sub OnRunWorkerCompleted(ByVal sender As System.Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

            Dim bgw As Sa.Simulation.Threading.BackgroundWorker = DirectCast(sender, Sa.Simulation.Threading.BackgroundWorker)

            If Me.QueuedItems.Count > 0 Then
                bgw.RunWorkerAsync(Me.QueuedItems.Dequeue)
            Else
                BGWs.Remove(bgw)
                If BGWs.Count = 0 Then RaiseEvent Done()
            End If

        End Sub

        Protected Friend Sub OnProgressChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs)

            Dim z As String() = e.UserState.ToString.Split(CChar("|"))
            Dim q As String = z(0)  ' QueueID
            Dim m As String = z(1)  ' SimModelId
            Dim r As String = z(2)  ' Refnum
            Dim f As String = z(3)  ' FactorSetId
            Dim s As String = z(4)  ' Status
            Dim d As String = z(5)  ' Detail

            Dim t As String = DateTimeOffset.Now.ToString()

            Dim lvi As New System.Windows.Forms.ListViewItem({q, m, r, f, s, d, t}, 0)

            Me.lv.Items.Insert(0, lvi)

            Select Case e.ProgressPercentage
                Case Sa.Simulation.ErrorCode.Finished

                    If Left(d, 23) = "Simulation Exit Code: 0" Then
                        Me.lv.Items(0).BackColor = System.Drawing.Color.Green
                        Me.lv.Items(0).ForeColor = System.Drawing.SystemColors.HighlightText

                    Else
                        Me.lv.Items(0).BackColor = System.Drawing.Color.Tomato

                    End If

                Case Sa.Simulation.ErrorCode.SourceFiles

                    Me.StopSimulations()

                    Me.lv.Items(0).BackColor = System.Drawing.Color.Tomato

                Case Sa.Simulation.ErrorCode.FeedError, Sa.Simulation.ErrorCode.Password

                    Me.lv.Items(0).BackColor = System.Drawing.Color.Tomato

            End Select

        End Sub

    End Class

End Class