﻿CREATE PROCEDURE [fact].[Insert_Quantity]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Quantity(Refnum, CalDateKey, StreamId, StreamDescription, Quantity_kMT)
		SELECT
			  u.Refnum
			, CONVERT(INT, CONVERT(VARCHAR(4), u.StudyYear) + CONVERT(VARCHAR(4), u.CalDateKey))
																	[CalDateKey]
			, etl.ConvStreamID(u.FeedProdID)						[StreamId]
			, ISNULL(u.StreamDescription, u.FeedProdID)				[StreamDescription]

			, ABS(u.kMT)											[kMT]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum]
				, t.StudyYear
				, q.FeedProdID	
				, CASE q.FeedProdID
					WHEN 'OthSpl'		THEN q.MiscFeed
					WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
					WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				
					WHEN 'OthLiqFeed1'	THEN f.OthLiqFeedDESC + ' (Other Feed 1)'
					WHEN 'OthLiqFeed2'	THEN f.OthLiqFeedDESC + ' (Other Feed 2)'
					WHEN 'OthLiqFeed3'	THEN f.OthLiqFeedDESC + ' (Other Feed 3)'
					WHEN 'OthLtFeed'	THEN q.MiscFeed
					ELSE q.FeedProdID
					END												[StreamDescription]
				
				, ISNULL(q.Q1Feed, 0.0)								[0331]
				, ISNULL(q.Q2Feed, 0.0)								[0630]
				, ISNULL(q.Q3Feed, 0.0)								[0930]
				, ISNULL(q.Q4Feed, 0.0)								[1231]
			
			FROM stgFact.Quantity q
			LEFT OUTER JOIN stgFact.FeedQuality f
				ON	f.Refnum = q.Refnum
				AND f.FeedProdID = q.FeedProdID
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = q.Refnum
			WHERE (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
				AND q.FeedProdID <> 'CO & CO2'
				AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT(
			kMT FOR CalDateKey IN (
				[0331], [0630], [0930], [1231]
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;