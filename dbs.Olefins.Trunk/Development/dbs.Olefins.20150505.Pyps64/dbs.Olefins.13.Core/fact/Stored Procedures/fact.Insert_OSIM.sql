﻿CREATE PROCEDURE [fact].[Insert_OSIM]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + 	@Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		EXECUTE [fact].[Insert_TSortClient]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_TsortContact_DC]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_TsortContact_Pricing]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_TsortContact_Secondary]					@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_Capacity]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_CapacityAttributes]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_CapacityGrowth]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_Facilities]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesComporessors_CompGas]			@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesComporessors_RefrigEthylene]	@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesComporessors_RefrigMethane]	@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesComporessors_RefrigPropylene]	@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesPressure]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesHydroTreat]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesFracFeed]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesMisc]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FacilitiesNameOther]						@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_Quantity]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_QuantitySuppRecovery]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_QuantitySuppRecycled]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_QuantityLHValue]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_QuantityValue]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_Meta_Quantity]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_CompositionQuantity]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_CompositionQuantity_OtherProd]			@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_CompositionQuantity_ProdQuality]			@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_CompositionQuantity_Sulfur]				@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_FeedStockDistillation]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedStockCrackingParameters]				@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ROGEntryPoint]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_OpEx]									@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_OpEx_Chemicals]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_OpEx_Maint]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_OpEx_Misc]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_OpExNameOther]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_Meta_OpEx]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_Meta_CapEx]								@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_Pers]									@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_Pers_Maint]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_PersAbsence]								@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_ReliabilityOppLoss]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityOppLossNameOther]				@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityOppLoss_Availability]			@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityCritPath]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityCritPathNameOther]			@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_Reliability]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityTA]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityPyroFurnFeed]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityPyroFurn]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityPyroFurnDownTime]				@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityPyroFurnOpEx]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ReliabilityPyroFurnPlantLimit]			@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_EnergyLHValue]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergyPrice]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergyComposition]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergySteam]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergyElec]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergyCogen]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergyCogenFacilities]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergyCogenFacilitiesNameOther]			@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_EnergyCogenPcnt]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_GenPlantInventory]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantDuties]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantLogistics]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantCommIntegration]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantCostRevenue]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantFeedFlex]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantCapEx]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantExpansion]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlant]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantEnergyEfficiency]				@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantEnergy]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_GenPlantEnvironment]						@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_Apc]										@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ApcControllers]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ApcExistance]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_ApcOnLinePcnt]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_MaintMisc]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_MaintExpense]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_Maint_Electricity]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_Maint_Overhead]							@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_FeedSelNonDiscretionary]					@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedSelPractices]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedSelLogistics]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedSelPersonnel]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedSelModels]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedSelModelPlan]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedSelModelPenalty]						@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_FeedSel]									@Refnum, @sRefnum, @StudyId;

		EXECUTE [cons].[Insert_Assets_Polymer]							@Refnum;
		EXECUTE [cons].[Insert_AssetsParents_Polymer]					@Refnum;

		EXECUTE [fact].[Insert_PolyFacilities]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_PolyOpEx]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_PolyQuantity]							@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_PolyQuantityNameOther]					@Refnum, @sRefnum, @StudyId;

		EXECUTE [fact].[Insert_MetaMisc]								@Refnum, @sRefnum, @StudyId;
		EXECUTE [fact].[Insert_MetaEnergy]								@Refnum, @sRefnum, @StudyId;
		--EXECUTE [fact].[Insert_MetaCapEx]								@Refnum, @sRefnum, @StudyId;
		--EXECUTE [fact].[Insert_MetaOpEx]								@Refnum, @sRefnum, @StudyId;
		--EXECUTE [fact].[Insert_MetaQuantity]							@Refnum, @sRefnum, @StudyId;

		EXECUTE	[fact].[Insert_ForexRate]								@Refnum, @sRefnum, @StudyId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, 	@Refnum, @sRefnum, @StudyId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
