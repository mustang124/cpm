﻿CREATE PROCEDURE [fact].[Insert_PolyQuantity]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.PolyQuantity(Refnum, CalDateKey, AssetId, StreamId, Quantity_kMT)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.AssetId
			, etl.ConvStreamIDPoly(u.StreamId)	[StreamId]
			, u.kMT
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, etl.ConvAssetPCH(p.Refnum)
					+ '-T' + CONVERT(VARCHAR(2), p.TrainId)				[AssetId]
				, p.Hydrogen
				, p.Ethylene_Plant
				, p.Ethylene_Other
				, p.Propylene_Plant
				, p.Propylene_Other
				, p.Butene
				, p.Hexene
				, p.Octene
				, p.OthMono1
				, p.OthMono2
				, p.OthMono3
				, p.Additives
				, p.Catalysts
				, p.OtherFeed
				--, p.TotalRawMatl
				--, p.OthMono1Desc
				--, p.OthMono2Desc
				--, p.OthMono3Desc
				, p.LDPE_Liner
				, p.LDPE_Clarity
				, p.LDPE_Blow
				, p.LDPE_GP
				, p.LDPE_LidResin
				, p.LDPE_EVA
				, p.LDPE_Other
				, p.LDPE_Offspec
				, p.LLDPE_ButeneLiner
				, p.LLDPE_ButeneMold
				, p.LLDPE_HAOLiner
				, p.LLDPE_HAOMeltIndex
				, p.LLDPE_HAOInject
				, p.LLDPE_HAOLidResin
				, p.LLDPE_HAORoto
				, p.LLDPE_Other
				, p.LLDPE_Offspec
				, p.HDPE_Homopoly
				, p.HDPE_Copoly
				, p.HDPE_Film
				, p.HDPE_GP
				, p.HDPE_Pipe
				, p.HDPE_Drum
				, p.HDPE_Roto
				, p.HDPE_Other
				, p.HDPE_Offspec
				, p.POLY_Homo
				, p.POLY_Copoly
				, p.POLY_CopolyBlow
				, p.POLY_CopolyInject
				, p.POLY_CopolyBlock
				, p.POLY_AtacticPP
				, p.POLY_Other
				, p.POLY_Offspec
				, p.ByProdOth1
				, p.ByProdOth2
				, p.ByProdOth3
				, p.ByProdOth4
				, p.ByProdOth5
				--, p.TotProducts
				--, p.ByProdOth1Desc
				--, p.ByProdOth2Desc
				--, p.ByProdOth3Desc
				--, p.ByProdOth4Desc
				--, p.ByProdOth5Desc
				, p.ReactorProd
				--, p.MonomerConsumed
				--, p.MonomerUtil
			FROM stgFact.PolyMatlBalance p
			INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
			INNER JOIN stgFact.PolyFacilities f ON f.Refnum = p.Refnum AND f.PlantId = p.TrainId
			WHERE	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT (
			kMT FOR StreamId IN (
				  p.Hydrogen
				, p.Ethylene_Plant
				, p.Ethylene_Other
				, p.Propylene_Plant
				, p.Propylene_Other
				, p.Butene
				, p.Hexene
				, p.Octene
				, p.OthMono1
				, p.OthMono2
				, p.OthMono3
				, p.Additives
				, p.Catalysts
				, p.OtherFeed
				--, p.TotalRawMatl
				--, p.OthMono1Desc
				--, p.OthMono2Desc
				--, p.OthMono3Desc
				, p.LDPE_Liner
				, p.LDPE_Clarity
				, p.LDPE_Blow
				, p.LDPE_GP
				, p.LDPE_LidResin
				, p.LDPE_EVA
				, p.LDPE_Other
				, p.LDPE_Offspec
				, p.LLDPE_ButeneLiner
				, p.LLDPE_ButeneMold
				, p.LLDPE_HAOLiner
				, p.LLDPE_HAOMeltIndex
				, p.LLDPE_HAOInject
				, p.LLDPE_HAOLidResin
				, p.LLDPE_HAORoto
				, p.LLDPE_Other
				, p.LLDPE_Offspec
				, p.HDPE_Homopoly
				, p.HDPE_Copoly
				, p.HDPE_Film
				, p.HDPE_GP
				, p.HDPE_Pipe
				, p.HDPE_Drum
				, p.HDPE_Roto
				, p.HDPE_Other
				, p.HDPE_Offspec
				, p.POLY_Homo
				, p.POLY_Copoly
				, p.POLY_CopolyBlow
				, p.POLY_CopolyInject
				, p.POLY_CopolyBlock
				, p.POLY_AtacticPP
				, p.POLY_Other
				, p.POLY_Offspec
				, p.ByProdOth1
				, p.ByProdOth2
				, p.ByProdOth3
				, p.ByProdOth4
				, p.ByProdOth5
				--, p.TotProducts
				--, p.ByProdOth1Desc
				--, p.ByProdOth2Desc
				--, p.ByProdOth3Desc
				--, p.ByProdOth4Desc
				--, p.ByProdOth5Desc
				, p.ReactorProd
				--, p.MonomerConsumed
				--, p.MonomerUtil
				)
			)u
		INNER JOIN cons.Assets a ON a.AssetId = u.AssetId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;