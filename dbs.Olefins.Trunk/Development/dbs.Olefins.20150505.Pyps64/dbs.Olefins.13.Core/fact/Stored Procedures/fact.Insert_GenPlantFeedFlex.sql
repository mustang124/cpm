﻿CREATE PROCEDURE [fact].[Insert_GenPlantFeedFlex]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantFeedFlex(Refnum, CalDateKey, StreamId, Capability_Pcnt, Demonstrate_Pcnt)
		SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvStreamID(f.FeedProdID)
			, CASE WHEN f.PlantCap > 1 THEN 1 ELSE f.PlantCap END * 100.0
			, CASE WHEN f.ActCap > 1 THEN 1 ELSE f.ActCap END	* 100.0 
		FROM stgFact.FeedFlex f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE (f.PlantCap IS NOT NULL OR f.ActCap IS NOT NULL)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;