﻿CREATE PROCEDURE [fact].[Insert_OpEx]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.OpEx(Refnum, CalDateKey, AccountId, CurrencyRpt, Amount_Cur)
		SELECT 
			  u.Refnum
			, u.CalDateKey
			, etl.ConvAccountID(u.AccountId)							[AccountId]
		
			, 'USD'
			-- OpEx Amount SHOULD be positive for all values.  The values are being
			-- massaged to account for negative data entry in these categories
			, CASE u.AccountId
				WHEN 'PowerExports'	THEN - u.Amount		-- 1989PCH015
				WHEN 'SteamExports'	THEN - u.Amount		-- 2003PCH171, 1989PCH020
				ELSE u.Amount
				END														[Amount]
		FROM (
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, t.StudyYear
				, o.OccSal
				, o.MpsSal
				, o.OccBen
				, o.MpsBen
				, o.MaintMatl
				, o.ContMaintLabor
				, o.ContMaintMatl
				, o.ContTechSvc
				, o.Envir
				, o.OthCont
				, o.Equip
				, o.Tax
				, o.Insur
			, o.OthNonVol
				--, o.STNonVol
				, COALESCE(o.TAAccrual, 0.0)	[TAAccrual]
				, COALESCE(o.TACurrExp, 0.0)	[TACurrExp]
				--, o.StTaExp
			, o.Chemicals
				, o.Catalysts
				, o.Royalties
				, o.PurElec
				, o.PurSteam
				, o.PurOth
				, o.PurFG
				, o.PurLiquid
				, o.PurSolid
				, o.PPCFuelGas
				, o.PPCFuelOth
				, o.SteamExports
				, o.PowerExports
			, o.OthVol
				--, o.STVol
				--, o.TotCashOpEx
				, o.InvenCarry
				, o.GANonPers
				, o.Depreciation
				, o.Interest
				--, o.TotRefExp
				, o.NonMaintEquipRent
			
				--, o.ChemBFWCW
				--, o.ChemWW
				--, o.ChemCaustic
				--, o.ChemPretreatment
				--, o.ChemAntiFoul
				--, o.ChemOth
				--, o.ChemOthProc
				--, o.ChemAdd
			
				--, ForeXRate
			FROM stgFact.OpEx o
			INNER JOIN stgFact.TSort t ON t.Refnum = o.Refnum
			WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT (
			Amount FOR AccountId IN (
				  OccSal
				, MpsSal
				, OccBen
				, MpsBen
				, MaintMatl
				, ContMaintLabor
				, ContMaintMatl
				, ContTechSvc
				, Envir
				, OthCont
				, Equip
				, Tax
				, Insur
			, OthNonVol
				--, STNonVol
				, TAAccrual
				, TACurrExp
				--, StTaExp
			, Chemicals
				, Catalysts
				, Royalties
				, PurElec
				, PurSteam
				, PurOth
				, PurFG
				, PurLiquid
				, PurSolid
				, PPCFuelGas
				, PPCFuelOth
				, SteamExports
				, PowerExports
			, OthVol
				--, STVol
				--, TotCashOpEx
				, InvenCarry
				, GANonPers
				, Depreciation
				, Interest
				--, TotRefExp
				, NonMaintEquipRent
			
				--, ChemBFWCW
				--, ChemWW
				--, ChemCaustic
				--, ChemPretreatment
				--, ChemAntiFoul
				--, ChemOthProc
				--, ChemAdd
				--, ChemOth
				--, ChemAggCalc
				--, ForeXRate
				)
			) u
		-- Chemicals were stored as separate values in studies 2001 and later.
		-- Remove the aggregate Chemicals for studies 2001 and later.
		WHERE NOT(u.StudyYear >= 2001 AND u.AccountId = 'Chemicals')

		-- Other Volume Related and Non-Volume related expenses are stored
		-- as detail in VE# and NVE#.
		-- Remove the aggregates for studies 2007 and later.
			AND NOT(u.StudyYear >= 2007 AND u.AccountId IN ('OthNonVol', 'OthVol'));

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;