﻿CREATE TABLE [fact].[Quantity] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [Quantity_kMT]      REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Quantity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_Quantity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_Quantity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_Quantity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Quantity] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_Quantity_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_Quantity_Quantity_kMT] CHECK ([Quantity_kMT]>=(0.0)),
    CONSTRAINT [FK_Quantity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Quantity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Quantity_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_Quantity_Aggregate]
    ON [fact].[Quantity]([Refnum] ASC, [Quantity_kMT] ASC)
    INCLUDE([CalDateKey], [StreamId], [StreamDescription]);


GO
CREATE NONCLUSTERED INDEX [IX_Quantity_Calc]
    ON [fact].[Quantity]([StreamId] ASC, [Quantity_kMT] ASC)
    INCLUDE([Refnum], [CalDateKey], [StreamDescription]);


GO

CREATE TRIGGER [fact].[t_Quantity_u]
	ON [fact].[Quantity]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Quantity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[Quantity].Refnum				= INSERTED.Refnum
		AND [fact].[Quantity].StreamId				= INSERTED.StreamId
		AND [fact].[Quantity].StreamDescription		= INSERTED.StreamDescription
		AND [fact].[Quantity].CalDateKey			= INSERTED.CalDateKey;

END;