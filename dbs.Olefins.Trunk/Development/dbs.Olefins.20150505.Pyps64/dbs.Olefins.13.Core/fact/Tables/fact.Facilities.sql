﻿CREATE TABLE [fact].[Facilities] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [Unit_Count]     INT                NOT NULL,
    [_Unit_Bit]      AS                 (CONVERT([Bit],case when [Unit_Count]>(0) then (1) else (0) end,(0))) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Facilities_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Facilities_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Facilities_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Facilities_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Facilities] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_Facilities_Unit_Count] CHECK ([Unit_Count]>=(0)),
    CONSTRAINT [FK_Facilities_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Facilities_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_Facilities_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_Facilities_Calc_Complexity]
    ON [fact].[Facilities]([Unit_Count] ASC)
    INCLUDE([Refnum], [CalDateKey], [FacilityId]);


GO

CREATE TRIGGER [fact].[t_Facilities_u]
	ON [fact].[Facilities]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Facilities]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[Facilities].Refnum		= INSERTED.Refnum
		AND [fact].[Facilities].FacilityId	= INSERTED.FacilityId
		AND [fact].[Facilities].CalDateKey	= INSERTED.CalDateKey;

END;