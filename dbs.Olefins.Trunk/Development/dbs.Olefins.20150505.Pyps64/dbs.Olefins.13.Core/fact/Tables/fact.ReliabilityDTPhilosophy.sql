﻿CREATE TABLE [fact].[ReliabilityDTPhilosophy] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [PhilosophyID]   CHAR (1)           NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityDTPhilosophy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTPhilosophy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTPhilosophy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTPhilosophy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityDTPhilosophy] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReliabilityDTPhilosophy_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityDTPhilosophy_ReliabilityPhilosophy_LookUp] FOREIGN KEY ([PhilosophyID]) REFERENCES [dim].[ReliabilityPhilosophy_LookUp] ([PhilosophyId]),
    CONSTRAINT [FK_ReliabilityDTPhilosophy_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityDTPhilosophy_u]
	ON [fact].[ReliabilityDTPhilosophy]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityDTPhilosophy]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityDTPhilosophy].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityDTPhilosophy].CalDateKey	= INSERTED.CalDateKey;

END;