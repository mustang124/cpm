﻿CREATE FUNCTION etl.ConvFacilityCoatingID
(
	@CoatingID	VARCHAR(64)
)
RETURNS VARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	VARCHAR(42) =
	CASE @CoatingID
		WHEN 'FLOUROPOLYMER'									THEN 'FlPoly'
		WHEN 'FLUOROPOLYMER'									THEN 'FlPoly'
		WHEN 'FLUOROPOLYMET'									THEN 'FlPoly'
		WHEN 'FLUORPOLYMER'										THEN 'FlPoly'
		WHEN 'POLYMER BASED'									THEN 'FlPoly'
		WHEN 'POS-E-COAT'										THEN 'FlPoly'
		WHEN 'POSECOAT'											THEN 'FlPoly'
		WHEN 'PTFE'												THEN 'FlPoly'
		WHEN '"SERMATEL"'										THEN 'FlPoly'
		WHEN 'SERMATEL'											THEN 'FlPoly'
		WHEN 'SERMATEL (STAGE 4 AND 5)'							THEN 'FlPoly'
		WHEN 'SERMALON'											THEN 'FlPoly'
		WHEN 'SERMALON®'										THEN 'FlPoly'
		WHEN 'SERMALOY'											THEN 'FlPoly'
		WHEN 'TEFLON'											THEN 'FlPoly'
		
		WHEN 'AL BASE WITH SEALER. AL + FLORO IN DIAPH.'		THEN 'Al'
		WHEN 'AL FILLED CR/PO4'									THEN 'Al'
		WHEN 'ALUMINUM CERAMIC'									THEN 'Al'
		WHEN 'ALUMINUM FILLED BASECOAT WITH CHROMATE PHOSPHATE SEALANT TOPCOAT'
																THEN 'Al'
		WHEN 'AL BASE'											THEN 'Al'
		
		WHEN 'CERAMIC'											THEN 'Al'
		WHEN 'CERMATEC 538 DP'									THEN 'Al'
		WHEN 'CR-TIN ,STELLITE'									THEN 'Al'
		WHEN 'CERMATECH 2F1'									THEN 'Al'
		
		WHEN 'ELECTROLESS NICKEL (4TH/5TH STAGES)'				THEN 'Alloy'
		WHEN 'ELECTROLESS NICKEL'								THEN 'Alloy'
		WHEN 'MCL'												THEN 'Alloy'
		WHEN 'MCL/BCL'											THEN 'Alloy'
		WHEN 'CR-TIN'											THEN 'Alloy'
		WHEN 'Y'												THEN 'Alloy'
		
		WHEN 'ANTI-COATING FOULING'								THEN 'Other'
		WHEN 'DRESSER RAND PROPRIETARY COATING'					THEN 'Other'
		WHEN 'GLASS FIBER'										THEN 'Other'
		WHEN 'ORGANIC SI'										THEN 'Other'

		WHEN 'EEC-A3 (FOULING RESISTANCE)'						THEN 'FlPoly' -- metallic-ceramic-polymeric'
		
		WHEN 'N'												THEN 'NONE'
		WHEN 'N/A'												THEN 'NONE'
		WHEN 'NIL'												THEN 'NONE'
		WHEN 'NONE'												THEN 'NONE'
		WHEN 'NON'												THEN 'NONE'
		WHEN '-'												THEN 'NONE'
		WHEN '0'												THEN 'NONE'
		WHEN 'NI'												THEN 'NONE'

		ELSE @CoatingID
		
	END;
	
	RETURN @ResultVar
		
END