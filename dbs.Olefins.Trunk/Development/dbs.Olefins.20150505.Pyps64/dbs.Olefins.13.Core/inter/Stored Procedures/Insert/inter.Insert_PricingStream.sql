﻿CREATE PROCEDURE inter.Insert_PricingStream
(
	@FactorSetId			VARCHAR(42) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	--	Insert Records - Basis Region	-------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (ASIA, EUR, NA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, r.RegionId
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.Raw_Amount_Cur
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId IN ('ASIA', 'EUR', 'NA')
			AND r.FactorSetId = @FactorSetId;

		------------------------------------------------------------------
		------------------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (LA) (< 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.Raw_Amount_Cur
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'EUR'
			AND r.StreamId NOT IN  ('Ethane', 'PPFCEthane')
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey < 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (LA: Ethane) (< 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.Raw_Amount_Cur
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'NA'
			AND r.StreamId	IN  ('Ethane', 'PPFCEthane')
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey < 20130000;

		--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--	--

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (LA: NA Basis) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.Raw_Amount_Cur * 1.5
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'NA'
			AND r.StreamId	IN (
				'DilEthane', 'Ethane', 'PPFCEthane',
				'DilPropane', 'PPFCPropane', 'Propane',
				'EPMix', 'LPG', 'Methane', 'PricingAdj'
				)
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey > 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (LA: EUR Basis) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.Raw_Amount_Cur * 1.5
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'EUR'
			AND r.StreamId	IN (
				'C4Oth',
				'Butane', 'DilButane', 'PPFCButane',
				'Condensate', 'HeavyNGL', 'LiqC5C6', 'Naphtha',
				'Raffinate',
				'GasOilHt',
				'GasOilHv'
				)
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey > 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (LA: 0.8 NA + 0.2 EUR) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  eu.FactorSetId
			, 'LA'				[RegionId]
			, eu.CalDateKey
			, eu.CurrencyRpt
			, eu.StreamId
			, 0.8 * na.Raw_Amount_Cur	+ 0.2 * eu.Raw_Amount_Cur
			, 0.8 * na.Adj_MatlBal_Cur	+ 0.2 * eu.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion		eu
		INNER JOIN inter.PricingStagingStreamRegion na
			ON	na.FactorSetId	= eu.FactorSetId
			AND	na.CalDateKey	= eu.CalDateKey
			AND	na.CurrencyRpt	= eu.CurrencyRpt
			AND	na.StreamId		= eu.StreamId
			AND	na.RegionId		= 'NA'
		WHERE	eu.RegionId		= 'EUR'
			AND eu.StreamId		IN (
				'Acetylene',
				'ConcEthylene', 'Ethylene', 'EthylenePG',
				'PropylenePG', 'PropyleneCG', 'PropyleneRG', 'ConcPropylene',
				'Butadiene', 'ConcButadiene', 'DilButadiene',
				'DilButylene', 'IsoButylene',
				'Benzene', 'ConcBenzene',
				'DilBenzene', 'PyroGasOilHt',
				'DilMoGas', 'PPFCPyroNaphtha', 'PyroGasOilUt',
				'PPFCPyroGasOil', 'PyroGasOil', 'SuppWashOil',
				'PPFCPyroFuelOil', 'PyroFuelOil', 'SuppGasOil'
				)
			AND eu.FactorSetId = @FactorSetId
			AND	eu.CalDateKey > 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (LA: Hydrogen) (> 20130000)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, 'LA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, 'Hydrogen'
			, r.Raw_Amount_Cur * 1.5 * 51.2 / 21.5
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'NA'
			AND r.StreamId	= 'Methane'
			AND r.FactorSetId = @FactorSetId
			AND	r.CalDateKey > 20130000;

		------------------------------------------------------------------
		------------------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (MEA: Direct)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, r.RegionId
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.Raw_Amount_Cur
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'MEA'
			AND r.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (MEA: Asian Conversion)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, 'MEA'				[RegionId]
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, CASE r.StreamId

			-- Fresh Pyro Feed (Reported)
				WHEN 'Ethane'				THEN NULL
				WHEN 'Propane'				THEN r.Raw_Amount_Cur - 45.0
				WHEN 'LPG'					THEN r.Raw_Amount_Cur - 45.0
				WHEN 'Butane'				THEN r.Raw_Amount_Cur - 45.0
				WHEN 'LiqC5C6'				THEN r.Raw_Amount_Cur - 25.0
				WHEN 'Raffinate'			THEN r.Raw_Amount_Cur - 25.0
				WHEN 'Naphtha'				THEN r.Raw_Amount_Cur - 25.0
				WHEN 'Diesel'				THEN r.Raw_Amount_Cur - 25.0
				WHEN 'GasOilHv'				THEN r.Raw_Amount_Cur - 25.0
				WHEN 'GasOilHt'				THEN r.Raw_Amount_Cur - 25.0
				WHEN 'Condensate'			THEN r.Raw_Amount_Cur - 25.0

			-- Products (Reported)
				WHEN 'Hydrogen'				THEN NULL
				WHEN 'Acetylene'			THEN NULL
				WHEN 'Ethylene'				THEN r.Raw_Amount_Cur - 75.0
				WHEN 'PropylenePG'			THEN r.Raw_Amount_Cur - 70.0
				WHEN 'PropyleneCG'			THEN r.Raw_Amount_Cur - 70.0
				WHEN 'PropyleneRG'			THEN r.Raw_Amount_Cur - 70.0
				WHEN 'Butadiene'			THEN r.Raw_Amount_Cur - 70.0
				WHEN 'Benzene'				THEN r.Raw_Amount_Cur - 25.0
				WHEN 'PyroGasoilHt'			THEN r.Raw_Amount_Cur - 25.0
				WHEN 'PyroGasoilUt'			THEN r.Raw_Amount_Cur - 25.0

			-- Stream Calcs (Not Reported)
				WHEN 'ConcBenzene'			THEN r.Raw_Amount_Cur - 25.0

				WHEN 'DilButane'			THEN r.Raw_Amount_Cur - 45.0
				WHEN 'DilButylene'			THEN r.Raw_Amount_Cur - 70.0
				WHEN 'DilBenzene'			THEN r.Raw_Amount_Cur - 25.0
				WHEN 'DilButadiene'			THEN r.Raw_Amount_Cur - 70.0

				WHEN 'DilMoGas'				THEN r.Raw_Amount_Cur - 25.0

			-- Plant Calcs (Not Reported)
				WHEN 'ConcEthylene'			THEN r.Raw_Amount_Cur - 75.0
				WHEN 'ConcPropylene'		THEN r.Raw_Amount_Cur - 70.0
				WHEN 'ConcButadiene'		THEN r.Raw_Amount_Cur - 70.0
				WHEN 'DilPropane'			THEN r.Raw_Amount_Cur - 45.0
			
				WHEN 'C4Oth'				THEN r.Raw_Amount_Cur - 75.0
				WHEN 'HeavyNGL'				THEN r.Raw_Amount_Cur - 25.0
				WHEN 'IsoButylene'			THEN r.Raw_Amount_Cur - 70.0
				WHEN 'PyroFuelOil'			THEN r.Raw_Amount_Cur - 25.0
				WHEN 'PyroGasOil'			THEN r.Raw_Amount_Cur - 25.0

				WHEN 'SuppGasOil'			THEN r.Raw_Amount_Cur - 25.0
				WHEN 'SuppWashOil'			THEN r.Raw_Amount_Cur - 25.0

				WHEN 'EPMix'				THEN r.Raw_Amount_Cur - 45.0

			-- Prod
				WHEN 'EthylenePG'			THEN r.Raw_Amount_Cur - 75.0
				WHEN 'EthyleneCG'			THEN r.Raw_Amount_Cur - 75.0 * 0.95
			
				WHEN 'PPFCEthane'			THEN NULL
				WHEN 'PPFCPropane'			THEN r.Raw_Amount_Cur - 45.0
				WHEN 'PPFCButane'			THEN r.Raw_Amount_Cur - 45.0
				WHEN 'PPFCPyroNaphtha'		THEN r.Raw_Amount_Cur - 25.0
				WHEN 'PPFCPyroGasOil'		THEN r.Raw_Amount_Cur - 25.0
				WHEN 'PPFCPyroFuelOil'		THEN r.Raw_Amount_Cur - 25.0

			END	[Amount_Cur]
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'ASIA'
			AND r.StreamId NOT IN ('Acetylene', 'Hydrogen', 'PricingAdj')
			AND r.StreamId NOT IN (SELECT r.StreamId FROM inter.PricingStagingStreamRegion r WHERE r.RegionId	= 'MEA')
			AND r.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (MEA: Acetylene)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, 'MEA'
			, r.CalDateKey
			, r.CurrencyRpt
			, 'Acetylene'
			, (r.Raw_Amount_Cur - 75.0) * 2.0
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'ASIA'
			AND r.StreamId	= 'Ethylene'
			AND r.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (MEA: Hydrogen)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, r.RegionId
			, r.CalDateKey
			, r.CurrencyRpt
			, 'Hydrogen'
			, r.Raw_Amount_Cur * 3.0 + 0.06 * 2205.0
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'MEA'
			AND r.StreamId	= 'Methane'
			AND r.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingBasisStreamRegion (MEA: PricingAdj)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingBasisStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  r.FactorSetId
			, r.RegionId
			, r.CalDateKey
			, r.CurrencyRpt
			, 'PricingAdj'
			, r.Raw_Amount_Cur / 2205.0 * 1000000.0 / 21500.0
			, r.Adj_MatlBal_Cur
		FROM inter.PricingStagingStreamRegion r
		WHERE	r.RegionId	= 'MEA'
			AND r.StreamId	= 'Methane'
			AND r.FactorSetId = @FactorSetId;

	--	Insert Records - Primary Region	-------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamRegion';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur, MatlBal_Supersede_Cur, Report_Supersede_Cur)
		SELECT
			  r.FactorSetId
			, r.RegionId
			, r.CalDateKey
			, r.CurrencyRpt
			, r.StreamId
			, r.Raw_Amount_Cur
			, r.Adj_MatlBal_Cur
			, NULL													[MatlBal_Supersede_Cur]
			, NULL													[Report_Supersede_Cur]
		FROM inter.PricingBasisStreamRegion r
		WHERE	r.StreamId IN (
			-- Fresh Pyro Feed
				  'Ethane'				-- Reporting Only
				, 'Propane'
				, 'LPG'					-- Reporting Only
				, 'Butane'				-- Reporting Only
				, 'LiqC5C6'
				, 'Raffinate'
				, 'Naphtha'
				, 'Diesel'
				, 'GasOilHv'
				, 'GasOilHt'
				, 'Condensate'
			
			-- Products
				, 'Hydrogen'
				, 'Acetylene'
				, 'Ethylene'			-- Reporting Only
				, 'PropylenePG'			-- Reporting Only
				, 'PropyleneCG'			-- Reporting Only
				, 'PropyleneRG'			-- Reporting Only
				, 'Butadiene'
				, 'Benzene'
				, 'PyroGasOilHt'
				, 'PyroGasOilUt'
			
			-- Stream Calcs
				, 'DilButane'
				, 'PricingAdj'
			
			-- Plant Calcs (Not Reported)
				, 'ConcEthylene'
				, 'ConcPropylene'
				, 'ConcButadiene'
				, 'DilPropane'
			
				, 'C4Oth'
				, 'HeavyNGL'
				, 'IsoButylene'
				, 'PyroFuelOil'
				, 'PyroGasOil'
			
				, 'SuppGasOil'
				, 'SuppWashOil'
			
				, 'EPMix'
			
				, 'EthylenePG'
				, 'EthyleneCG'
			
				, 'PPFCEthane'
				, 'PPFCPropane'
				, 'PPFCButane'
				, 'PPFCPyroNaphtha'
				, 'PPFCPyroGasOil'
				, 'PPFCPyroFuelOil'
				)
			AND	r.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamRegion (ConcBenzene)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur, MatlBal_Supersede_Cur, Report_Supersede_Cur)
		SELECT
			  b.FactorSetId
			, b.RegionId
			, b.CalDateKey
			, b.CurrencyRpt
			, b.StreamId
			, b.Raw_Amount_Cur
			, b.Adj_MatlBal_Cur
			, calc.MinValue(b.Raw_Amount_Cur, calc.MaxValue(p.Raw_Amount_Cur, b._MatlBal_Amount_Cur))
																	[MatlBal_Supersede_Cur]
			, NULL													[Report_Supersede_Cur]
		FROM inter.PricingBasisStreamRegion			b
		INNER JOIN inter.PricingBasisStreamRegion	p
			ON	p.FactorSetId	= b.FactorSetId
			AND	p.RegionId		= b.RegionId
			AND	p.CalDateKey	= b.CalDateKey
			AND p.CurrencyRpt	= b.CurrencyRpt
		WHERE	p.StreamId		= 'PyroGasoilHt'
			AND b.StreamId		= 'ConcBenzene'
			AND	b.FactorSetId	= @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamRegion (DilButylene)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur, MatlBal_Supersede_Cur, Report_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.RegionId
			, s.CalDateKey
			, s.CurrencyRpt
			, s.StreamId
			, s.Raw_Amount_Cur
			, s.Adj_MatlBal_Cur
			, calc.MaxValue(s._MatlBal_Amount_Cur, p._MatlBal_Amount_Cur)	[MatlBal_Supersede_Cur]
			, NULL															[Report_Supersede_Cur]
		FROM inter.PricingBasisStreamRegion			s
		INNER JOIN inter.PricingBasisStreamRegion	p
			ON	p.FactorSetId	= s.FactorSetId
			AND	p.RegionId		= s.RegionId
			AND	p.CalDateKey	= s.CalDateKey
			AND	p.CurrencyRpt	= s.CurrencyRpt
		WHERE	p.StreamId		= 'DilButane'
			AND	s.StreamId		= 'DilButylene'
			AND	p.FactorSetId	= @FactorSetId;

	--	Insert Records - Secondary Region	---------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingSecondaryStreamRegion (DilBenzene)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingSecondaryStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur, MatlBal_Supersede_Cur, Report_Supersede_Cur)
		SELECT
			  d.FactorSetId
			, d.RegionId
			, d.CalDateKey
			, d.CurrencyRpt
			, d.StreamId
			, d._MatlBal_Amount_Cur
			, NULL														[Adj_MatlBal_Cur]
			, calc.MinValue(p.Raw_Amount_Cur, c._MatlBal_Amount_Cur)	[MatlBal_Supersede_Cur]
			, NULL														[Report_Supersede_Cur]
		FROM inter.PricingBasisStreamRegion			d
		INNER JOIN inter.PricingBasisStreamRegion	p
			ON	p.FactorSetId	= d.FactorSetId
			AND	p.RegionId		= d.RegionId
			AND	p.CalDateKey	= d.CalDateKey
			AND	p.CurrencyRpt	= d.CurrencyRpt
		INNER JOIN inter.PricingPrimaryStreamRegion	c
			ON	c.FactorSetId	= d.FactorSetId
			AND	c.RegionId		= d.RegionId
			AND	c.CalDateKey	= d.CalDateKey
			AND	c.CurrencyRpt	= d.CurrencyRpt
		WHERE	d.StreamId		= 'DilBenzene'
			AND	p.StreamId		= 'PyroGasoilHt'
			AND	c.StreamId		= 'ConcBenzene'
			AND	d.FactorSetId	= @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingSecondaryStreamRegion (DilMoGas)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingSecondaryStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur, MatlBal_Supersede_Cur, Report_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.RegionId
			, s.CalDateKey
			, s.CurrencyRpt
			, s.StreamId
			, s.Raw_Amount_Cur
			, NULL															[Adj_MatlBal_Cur]
			, calc.MinValue(s._MatlBal_Amount_Cur, c._MatlBal_Amount_Cur)	[MatlBal_Supersede_Cur]
			, NULL															[Report_Supersede_Cur]
		FROM inter.PricingBasisStreamRegion				s
		INNER JOIN inter.PricingPrimaryStreamRegion		c
			ON	c.FactorSetId	= s.FactorSetId
			AND	c.RegionId		= s.RegionId
			AND	c.CalDateKey	= s.CalDateKey
			AND	c.CurrencyRpt	= s.CurrencyRpt
		WHERE	c.StreamId		= 'ConcBenzene'
			AND	s.StreamId		= 'DilMoGas'
			AND	s.FactorSetId	= @FactorSetId;

	--	Insert Records - Primary Country	---------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamCountry (Region: Primary)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryStreamCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, [Proximity_RegionId],
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.CountryId
			, c0.CalDateKey
			, s.CurrencyRpt
			, r.StreamId							[StreamId]
			, prx.[OverRide_RegionId]					[Proximity_RegionID]
		
			, r.Raw_Amount_Cur						[Raw_Amount_Cur]
			, rOvr.Raw_Amount_Cur					[Prx_Amount_Cur]
			, r.Adj_MatlBal_Cur						[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur					[Adj_Proximity_Cur]
			, ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.MatlBal_Supersede_Cur)
													[MatlBal_Supersede_Cur]
		FROM ante.PricingMultiplierStreamCountry					s
	
		INNER JOIN ante.CountryEconRegion							k
			ON	k.FactorSetId		= s.FactorSetId
			AND	k.CountryId			= s.CountryId
		INNER JOIN ante.FactorSetsInStudyYear						fs
			ON	fs.FactorSetId		= s.FactorSetId
			AND	fs.CalDateKey		= s.CalDateKey
		INNER JOIN dim.Calendar_LookUp								c0
			ON	c0.CalYear			= fs._StudyYear
		
		INNER JOIN inter.PricingPrimaryStreamRegion					r
			ON	r.FactorSetId		= s.FactorSetId
			AND	r.RegionId			= k.[EconRegionId]
			AND r.CalDateKey		= c0.CalDateKey
			AND	r.CurrencyRpt		= s.CurrencyRpt
		
		LEFT OUTER JOIN ante.PricingPeersStreamCountryAgg			prx
			ON	prx.FactorSetId		= fs.FactorSetId
			AND	prx.CountryId		= s.CountryId
			AND prx.CalDateKey		= s.CalDateKey
			AND	prx.CurrencyRpt		= s.CurrencyRpt
			AND prx.StreamId		= r.StreamId
		
		LEFT OUTER JOIN inter.PricingPrimaryStreamRegion			rOvr
			ON	rOvr.FactorSetId	= s.FactorSetId
			AND	rOvr.RegionId		= prx.[OverRide_RegionId]
			AND rOvr.CalDateKey		= c0.CalDateKey
			AND	rOvr.CurrencyRpt	= s.CurrencyRpt
			AND	rOvr.StreamId		= r.StreamId
		WHERE	s.FactorSetId		= @FactorSetId;
			
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamCountry (Region: Secondary)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryStreamCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, [Proximity_RegionId],
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  s.FactorSetId
			, s.CountryId
			, c0.CalDateKey
			, s.CurrencyRpt
			, r.StreamId							[StreamId]
			, prx.[OverRide_RegionId]					[Proximity_RegionID]
		
			, r.Raw_Amount_Cur						[Raw_Amount_Cur]
			, rOvr.Raw_Amount_Cur					[Prx_Amount_Cur]
			, r.Adj_MatlBal_Cur						[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur					[Adj_Proximity_Cur]
			, ISNULL(rOvr.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), r.MatlBal_Supersede_Cur)
													[MatlBal_Supersede_Cur]
		FROM ante.PricingMultiplierStreamCountry					s
		INNER JOIN ante.CountryEconRegion							k
			ON	k.FactorSetId		= s.FactorSetId
			AND	k.CountryId			= s.CountryId
		INNER JOIN ante.FactorSetsInStudyYear						fs
			ON	fs.FactorSetId		= s.FactorSetId
			AND	fs.CalDateKey		= s.CalDateKey
		INNER JOIN dim.Calendar_LookUp								c0
			ON	c0.CalYear			= fs._StudyYear
		
		INNER JOIN inter.PricingSecondaryStreamRegion				r
			ON	r.FactorSetId		= s.FactorSetId
			AND	r.RegionId			= k.[EconRegionId]
			AND r.CalDateKey		= c0.CalDateKey
			AND	r.CurrencyRpt		= s.CurrencyRpt
		
		LEFT OUTER JOIN ante.PricingPeersStreamCountryAgg			prx
			ON	prx.FactorSetId		= fs.FactorSetId
			AND	prx.CountryId		= s.CountryId
			AND prx.CalDateKey		= s.CalDateKey
			AND	prx.CurrencyRpt		= s.CurrencyRpt
			AND prx.StreamId		= r.StreamId
		
		LEFT OUTER JOIN inter.PricingSecondaryStreamRegion			rOvr
			ON	rOvr.FactorSetId	= s.FactorSetId
			AND	rOvr.RegionId		= prx.[OverRide_RegionId]
			AND rOvr.CalDateKey		= c0.CalDateKey
			AND	rOvr.CurrencyRpt	= s.CurrencyRpt
			AND	rOvr.StreamId		= r.StreamId
		
		WHERE	s.FactorSetId		= @FactorSetId;

	--	Insert Records - Methane	-----------------------------------------------------
		/* Possibly add DilMethane and DilHydrogen to Stream ETL */

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamCountry (Methane, DilMethane)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryStreamCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, [Proximity_RegionId],
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  p.FactorSetId
			, p.CountryId
			, p.CalDateKey
			, p.CurrencyRpt
			, p.StreamId
			, p.[OverRide_RegionId]
			, p.Raw_Amount_Cur
			, p.Prx_Amount_Cur
			, p.Adj_MatlBal_Cur
			, p.Adj_Proximity_Cur
			, p.MatlBal_Supersede_Cur
		FROM (	
			SELECT
				  s.FactorSetId
				, s.CountryId
				, c0.CalDateKey
				, s.CurrencyRpt
				, prx.[OverRide_RegionId]
				, mStd.Raw_Amount_Cur								[Raw_Amount_Cur]
				, mPrx.Raw_Amount_Cur								[Prx_Amount_Cur]
				, NULL												[Adj_MatlBal_Cur]
				, prx.Adj_Proximity_Cur								[Adj_Proximity_Cur]

				, ISNULL(rPrx.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), rStd.Raw_Amount_Cur) / AVG(rStd.Raw_Amount_Cur) OVER (PARTITION BY s.FactorSetId, ISNULL(prx.[OverRide_RegionId], k.[EconRegionId]), s.StreamId, s.CurrencyRpt)
					* 2204.6 / 1000000.0 * 21500.0 * s.Amount_Cur	[Methane]

				, ISNULL(rPrx.Raw_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), rStd.Raw_Amount_Cur) / AVG(rStd.Raw_Amount_Cur) OVER (PARTITION BY s.FactorSetId, ISNULL(prx.[OverRide_RegionId], k.[EconRegionId]), s.StreamId, s.CurrencyRpt)
					* 2204.6 / 1000000.0 * 21500.0 * s.Amount_Cur	[DilMethane]

			FROM ante.PricingMultiplierStreamCountry					s
			INNER JOIN ante.CountryEconRegion							k
				ON	k.FactorSetId		= s.FactorSetId
				AND	k.CountryId			= s.CountryId
			INNER JOIN ante.FactorSetsInStudyYear						fs
				ON	fs.FactorSetId	= s.FactorSetId
				AND	fs.CalDateKey	= s.CalDateKey
			INNER JOIN dim.Calendar_LookUp								c0
				ON	c0.CalYear		= fs._StudyYear

			INNER JOIN inter.PricingBasisStreamRegion					mStd
				ON	mStd.FactorSetId	= s.FactorSetId
				AND	mStd.CalDateKey		= c0.CalDateKey
				AND	mStd.RegionId		= k.[EconRegionId]
				AND	mStd.CurrencyRpt	= s.CurrencyRpt
				AND mStd.StreamId		= 'Methane'

			INNER JOIN inter.PricingBasisStreamRegion					rStd
				ON	rStd.FactorSetId	= s.FactorSetId
				AND	rStd.CalDateKey		= c0.CalDateKey
				AND	rStd.RegionId		= k.[EconRegionId]
				AND	rStd.CurrencyRpt	= s.CurrencyRpt
				AND rStd.StreamId		= s.StreamId

			LEFT OUTER JOIN ante.PricingPeersStreamCountryAgg	prx
				ON	prx.FactorSetId		= fs.FactorSetId
				AND	prx.CountryId		= s.CountryId
				AND prx.CalDateKey		= s.CalDateKey
				AND	prx.CurrencyRpt		= s.CurrencyRpt
				AND prx.StreamId		= 'Methane'

			LEFT OUTER JOIN inter.PricingBasisStreamRegion				mPrx
				ON	mPrx.FactorSetId	= s.FactorSetId
				AND	mPrx.CalDateKey		= c0.CalDateKey
				AND	mPrx.RegionId		= prx.[OverRide_RegionId]
				AND	mPrx.CurrencyRpt	= s.CurrencyRpt
				AND mPrx.StreamId		= 'Methane'

			LEFT OUTER JOIN inter.PricingBasisStreamRegion				rPrx
				ON	rPrx.FactorSetId	= s.FactorSetId
				AND	rPrx.CalDateKey		= c0.CalDateKey
				AND	rPrx.RegionId		= prx.[OverRide_RegionId]
				AND	rPrx.CurrencyRpt	= s.CurrencyRpt
				AND rPrx.StreamId		= s.StreamId	

			WHERE	s.StreamId			= 'PricingAdj'
			AND	s.FactorSetId			= @FactorSetId
			) u
		UNPIVOT (
			MatlBal_Supersede_Cur FOR StreamId IN ([Methane], [DilMethane])
			) p;		

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamCountry (DilHydrogen)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingPrimaryStreamCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, [Proximity_RegionId],
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  c.FactorSetId
			, c.CountryId
			, c.CalDateKey
			, c.CurrencyRpt
			, 'DilHydrogen'
			, c.[Proximity_RegionId]

			, bStd.Raw_Amount_Cur
			, bPrx.Raw_Amount_Cur
			, c.Adj_MatlBal_Cur								[Adj_MatlBal_Cur]
			, c.Adj_Proximity_Cur							[Adj_Proximity_Cur]
			, c._MatlBal_Amount_Cur * 51500.0 / 21500.0		[MatlBal_Supersede_Cur]

		FROM inter.PricingPrimaryStreamCountry						c
		INNER JOIN ante.CountryEconRegion							k
			ON	k.FactorSetId		= c.FactorSetId
			AND	k.CountryId			= c.CountryId

		INNER JOIN inter.PricingBasisStreamRegion					bStd
			ON	bStd.FactorSetId	= c.FactorSetId
			AND	bStd.RegionId		= k.[EconRegionId]
			AND	bStd.CalDateKey		= c.CalDateKey
			AND	bStd.CurrencyRpt	= c.CurrencyRpt
			AND bStd.StreamId		= 'Hydrogen'

		LEFT OUTER JOIN inter.PricingBasisStreamRegion				bPrx
			ON	bPrx.FactorSetId	= c.FactorSetId
			AND	bPrx.RegionId		= c.[Proximity_RegionId]
			AND	bPrx.CalDateKey		= c.CalDateKey
			AND	bPrx.CurrencyRpt	= c.CurrencyRpt
			AND bPrx.StreamId		= 'Hydrogen'

		WHERE	c.StreamId			= 'DilMethane'
			AND	c.FactorSetId		= @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamCountry (DilButadiene)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingSecondaryStreamCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, [Proximity_RegionId],
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  c.FactorSetId
			, c.CountryId
			, c.CalDateKey
			, c.CurrencyRpt
			, rStd.StreamId
			, prx.[OverRide_RegionId]

			, rStd.Raw_Amount_Cur
			, rPrx.Raw_Amount_Cur

			, rStd.Adj_MatlBal_Cur							[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur							[Adj_Proximity_Cur]

			, calc.MinValue(ISNULL(rPrx._MatlBal_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), rStd._MatlBal_Amount_Cur), calc.MaxValue(ISNULL(rPrx._MatlBal_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), rStd._MatlBal_Amount_Cur) * 0.8, c._MatlBal_Amount_Cur))
															[MatlBal_Supersede_Cur]
		FROM inter.PricingPrimaryStreamCountry						c
		INNER JOIN ante.CountryEconRegion							k
			ON	k.FactorSetId		= c.FactorSetId
			AND	k.CountryId			= c.CountryId

		INNER JOIN inter.PricingBasisStreamRegion					rStd
			ON	rStd.FactorSetId	= c.FactorSetId
			AND	rStd.RegionId		= k.[EconRegionId]
			AND	rStd.CalDateKey		= c.CalDateKey
			AND	rStd.CurrencyRpt	= c.CurrencyRpt
			AND rStd.StreamId		= 'DilButadiene'

		LEFT OUTER JOIN ante.PricingPeersStreamCountryAgg	prx
			ON	prx.FactorSetId		= c.FactorSetId
			AND	prx.CountryId		= c.CountryId
			AND prx.CalDateKey		= c.CalDateKey
			AND	prx.CurrencyRpt		= c.CurrencyRpt
			AND prx.StreamId		= 'DilButadiene'

		LEFT OUTER JOIN inter.PricingBasisStreamRegion				rPrx
			ON	rPrx.FactorSetId	= c.FactorSetId
			AND	rPrx.RegionId		= prx.[OverRide_RegionId]
			AND	rPrx.CalDateKey		= c.CalDateKey
			AND	rPrx.CurrencyRpt	= c.CurrencyRpt
			AND rPrx.StreamId		= 'DilButadiene'

		WHERE	c.StreamId			= 'DilMethane'
			AND	c.FactorSetId		= @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingPrimaryStreamCountry (DilEthane)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingSecondaryStreamCountry(FactorSetId, CountryId, CalDateKey, CurrencyRpt, StreamId, [Proximity_RegionId],
			Raw_Amount_Cur, Prx_Amount_Cur, Adj_MatlBal_Cur, Adj_Proximity_Cur, MatlBal_Supersede_Cur)
		SELECT
			  m.FactorSetId
			, m.CountryId
			, m.CalDateKey
			, m.CurrencyRpt
			, eStd.StreamId
			, prx.[OverRide_RegionId]

			, eStd.Raw_Amount_Cur
			, ePrx.Raw_Amount_Cur
			, eStd.Adj_MatlBal_Cur												[Adj_MatlBal_Cur]
			, prx.Adj_Proximity_Cur												[Adj_Proximity_Cur]
			, calc.MaxValue(ISNULL(ePrx._MatlBal_Amount_Cur + ISNULL(prx.Adj_Proximity_Cur, 0.0), eStd._MatlBal_Amount_Cur), m._MatlBal_Amount_Cur)	[MatlBal_Supersede_Cur]
		FROM inter.PricingPrimaryStreamCountry						m

		INNER JOIN ante.CountryEconRegion							k
			ON	k.FactorSetId		= m.FactorSetId
			AND	k.CountryId			= m.CountryId

		INNER JOIN inter.PricingBasisStreamRegion					eStd
			ON	eStd.FactorSetId	= m.FactorSetId
			AND	eStd.RegionId		= k.[EconRegionId]
			AND	eStd.CalDateKey		= m.CalDateKey
			AND	eStd.CurrencyRpt	= m.CurrencyRpt
			AND	eStd.StreamId		= 'DilEthane'

		LEFT OUTER JOIN ante.PricingPeersStreamCountryAgg			prx
			ON	prx.FactorSetId		= m.FactorSetId
			AND	prx.CountryId		= m.CountryId
			AND prx.CalDateKey		= m.CalDateKey
			AND	prx.CurrencyRpt		= m.CurrencyRpt
			AND prx.StreamId		= 'DilEthane'

		LEFT OUTER JOIN inter.PricingBasisStreamRegion				ePrx
			ON	ePrx.FactorSetId	= m.FactorSetId
			AND	ePrx.RegionId		= prx.[OverRide_RegionId]
			AND	ePrx.CalDateKey		= m.CalDateKey
			AND	ePrx.CurrencyRpt	= m.CurrencyRpt
			AND	ePrx.StreamId		= 'DilEthane'

		WHERE	m.StreamId			= 'DilMethane'
			AND	m.FactorSetId		= @FactorSetId;

	--	Insert Records	-----------------------------------------------------------------
	
		DECLARE @MethodID	VARCHAR(42);
		SELECT TOP 1 @MethodID = l.MethodID FROM dim.PricingMethodLu l;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStreamRegion (Secondary)';
		PRINT @ProcedureDesc;

		INSERT INTO [inter].PricingStreamRegion(FactorSetId, [PricingMethodId], RegionId, CalDateKey, CurrencyRpt, StreamId, Pricing_Cur, MatlBal_Cur, Report_Cur)
		SELECT l.FactorSetId, @MethodID, l.RegionId, l.CalDateKey, l.CurrencyRpt, l.StreamId, l.Raw_Amount_Cur, l._MatlBal_Amount_Cur, l._Report_Amount_Cur
		FROM inter.PricingSecondaryStreamRegion		l
		WHERE l.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStreamRegion (Primary)';
		PRINT @ProcedureDesc;

		INSERT INTO [inter].PricingStreamRegion(FactorSetId, [PricingMethodId], RegionId, CalDateKey, CurrencyRpt, StreamId, Pricing_Cur, MatlBal_Cur, Report_Cur)
		SELECT l.FactorSetId, @MethodID, l.RegionId, l.CalDateKey, l.CurrencyRpt, l.StreamId, l.Raw_Amount_Cur, l._MatlBal_Amount_Cur, l._Report_Amount_Cur
		FROM inter.PricingPrimaryStreamRegion		l
		WHERE l.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStreamCountry (Secondary)';
		PRINT @ProcedureDesc;

		INSERT INTO [inter].PricingStreamCountry(FactorSetId, [PricingMethodId], CountryId, CalDateKey, CurrencyRpt, StreamId, Pricing_Cur, MatlBal_Cur, Report_Cur)
		SELECT l.FactorSetId, @MethodID, l.CountryId, l.CalDateKey, l.CurrencyRpt, l.StreamId, l.Raw_Amount_Cur, l._MatlBal_Amount_Cur, l.Raw_Amount_Cur
		FROM inter.PricingSecondaryStreamCountry	l
		WHERE l.FactorSetId = @FactorSetId;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStreamCountry (Primary)';
		PRINT @ProcedureDesc;

		INSERT INTO [inter].PricingStreamCountry(FactorSetId, [PricingMethodId], CountryId, CalDateKey, CurrencyRpt, StreamId, Pricing_Cur, MatlBal_Cur, Report_Cur)
		SELECT l.FactorSetId, @MethodID, l.CountryId, l.CalDateKey, l.CurrencyRpt, l.StreamId, l.Raw_Amount_Cur, l._MatlBal_Amount_Cur, l.Raw_Amount_Cur
		FROM inter.PricingPrimaryStreamCountry		l
		WHERE l.FactorSetId = @FactorSetId;

END