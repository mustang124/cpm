﻿CREATE PROCEDURE [Console].[UpdateIssue] 

	@RefNum varchar(18),
	@IssueTitle nvarchar(20),
	@Status nvarchar(1),
	@UpdatedBy nvarchar(3)

AS
BEGIN
DECLARE @UpdateDate datetime
SET @UpdateDate = GETDATE()

IF @Status = 'N'
	BEGIN
		SET @UpdatedBy = null
		SET @UpdateDate = null
	END
UPDATE Val.Checklist
SET Completed = @Status, SetTime = @UpdateDate,SetBy = @UpdatedBy
Where Refnum = dbo.FormatRefNum('20' + @RefNum,0) AND IssueID = @IssueTitle
END
