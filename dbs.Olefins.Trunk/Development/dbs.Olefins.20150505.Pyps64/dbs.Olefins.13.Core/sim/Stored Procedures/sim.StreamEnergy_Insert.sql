﻿CREATE PROCEDURE sim.StreamEnergy_Insert(
	@QueueID				BIGINT,
	@FactorSetId			VARCHAR(12),
	  
	@Refnum					VARCHAR(25),
	@CalDateKey				INT,
	@SimModelId				VARCHAR(12),
	@OpCondId				VARCHAR(12),
	
	@StreamId				VARCHAR(42),
	@StreamDescription		NVARCHAR(256),
	@RecycleId				INT,
	
	@ErrorID				VARCHAR(12),
	@FurnaceTypeId			VARCHAR(12)	= NULL,
	@SimEnergy_kCalkg		REAL		= NULL
	)
AS
	INSERT INTO sim.[EnergyConsumptionStream](QueueID, FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, StreamDescription, RecycleId, ErrorID, FurnaceTypeId, SimEnergy_kCalkg)
	VALUES(@QueueID, @FactorSetId, @Refnum, @CalDateKey, @SimModelId, @OpCondId, @StreamId, @StreamDescription, @RecycleId, @ErrorID, @FurnaceTypeId, @SimEnergy_kCalkg)
	;