﻿CREATE TABLE [super].[PeerGroupFeedClass] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [PeerGroup]       TINYINT            NOT NULL,
    [StreamId]        VARCHAR (42)       NULL,
    [PlantDensity_SG] REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupFeedClass_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PeerGroupFeedClass] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_PeerGroupFeedClass_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PeerGroupFeedClass_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PeerGroupFeedClass_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [super].[t_PeerGroupFeedClass_u]
	ON  [super].[PeerGroupFeedClass]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [super].[PeerGroupFeedClass]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]	= APP_NAME()
	FROM INSERTED
	WHERE	[super].[PeerGroupFeedClass].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[super].[PeerGroupFeedClass].[Refnum]		= INSERTED.[Refnum]
		AND [super].[PeerGroupFeedClass].[CalDateKey]	= INSERTED.[CalDateKey];
			
END;
