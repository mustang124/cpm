﻿CREATE PROCEDURE [cons].[VerifyTSort_Group]
(
	@GroupId	VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		DECLARE @Assets	TABLE
		(
			[AssetId]		VARCHAR(30)		NOT	NULL,
			[AssetDetail]	NVARCHAR(256)	NOT	NULL,
			[CalDateKey]	INT				NOT	NULL,
			PRIMARY KEY CLUSTERED([AssetId] ASC)
		);

		INSERT INTO [cons].[Assets]([AssetIdPri], [AssetIdSec], [AssetName], [AssetDetail], [CountryId], [CalDateKey])
		OUTPUT inserted.[AssetId], inserted.[AssetDetail], inserted.[CalDateKey]
		INTO @Assets([AssetId], [AssetDetail], [CalDateKey])
		SELECT DISTINCT
			rg.[GroupId],
			':' + l.[CountryId],
			rg.[ReportGroupName] + ' (' + l.[CountryId] + ')',
			rg.[ReportGroupName] + ' (' + l.[CountryId] + ')',
			l.[CountryId],
			t.[CalDateKey]
		FROM [reports].[ReportGroups]		rg

		INNER JOIN [reports].[GroupPlants]	gp
			ON	gp.[GroupId]	= rg.[GroupId]
		INNER JOIN [fact].[TSortClient]		t
			ON	t.[Refnum]		= gp.[Refnum]

		INNER JOIN [dim].[Country_LookUp]	l
			ON	l.[Active] = 1
		LEFT OUTER JOIN [cons].[Assets]		a
			ON	a.[AssetIdPri]	= rg.[GroupId]
			AND	a.[AssetIdSec]	= ':' + l.[CountryId]
		WHERE	rg.[GroupId]	= @GroupId
			AND	a.[AssetId]		IS NULL;

		INSERT INTO [cons].[SubscriptionsAssets]([CompanyId], [CalDateKey], [StudyId], [AssetId], [ScenarioName], [SubscriberAssetName], [SubscriberAssetDetail])
		SELECT
			'Solomon',
			a.[CalDateKey],
			'PCH',
			a.[AssetId],
			'Basis',
			a.[AssetDetail],
			a.[AssetDetail]
		FROM @Assets									a
		LEFT OUTER JOIN [cons].[SubscriptionsAssets]	s
			ON	s.[AssetId] = a.[AssetId]
		WHERE	s.[Refnum]	IS NULL;

		INSERT INTO [cons].[TSortSolomon]([Refnum], [CalDateKey], [FactorSetId])
		SELECT
			a.[AssetId],
			a.[CalDateKey],
			LEFT(CONVERT(CHAR(8), a.[CalDateKey]), 4)
		FROM @Assets a
		LEFT OUTER JOIN [cons].[TSortSolomon]	t
			ON	t.[Refnum]	= a.AssetId
		WHERE	t.[Refnum]	IS NULL;

	END TRY
	BEGIN CATCH
		
		SET @GroupId = 'G:' + @GroupId;
		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;