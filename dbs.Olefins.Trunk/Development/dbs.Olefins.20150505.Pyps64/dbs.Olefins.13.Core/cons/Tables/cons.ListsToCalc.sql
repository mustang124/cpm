﻿CREATE TABLE [cons].[ListsToCalc] (
    [ListId]  VARCHAR (42) NOT NULL,
    [DoCalcs] BIT          DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ListId] ASC)
);

