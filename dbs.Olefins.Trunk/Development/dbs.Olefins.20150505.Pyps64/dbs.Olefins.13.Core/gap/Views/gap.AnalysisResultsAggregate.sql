﻿CREATE VIEW [gap].[AnalysisResultsAggregate]
WITH SCHEMABINDING
AS
SELECT
	t.[FactorSetId],
	t.[GroupId],
	t.[TargetId],
	t.[GroupProdCap_kMT],
	t.[BenchProdCap_kMT],
	b.[GapId],
	t.[CurrencyId],
	[GapAmount_Cur]	=
		SUM(CASE b.[DescendantOperator]
			WHEN '+' THEN + t.[GapAmount_Cur]
			WHEN '-' THEN - t.[GapAmount_Cur]
			END),

	[GapAmount_Div]	=
		SUM(CASE b.[DescendantOperator]
			WHEN '+' THEN + t.[GapAmount_Div]
			WHEN '-' THEN - t.[GapAmount_Div]
			END)

FROM (
	SELECT
		p.[FactorSetId],
		g.[GroupId],
		g.[TargetId],
		g.[GroupProdCap_kMT],
		g.[BenchProdCap_kMT],
		g.[GapId],
		g.[CurrencyId],
		g.[GapAmount_Cur],
		[GapAmount_Div] = COALESCE(g.[DivAmount_Cur], g.[GapAmount_Cur] / CASE WHEN d.[GapId] IS NOT NULL THEN g.[BenchProdCap_kMT] ELSE g.[GroupProdCap_kMT] END * 1000.0),
		g.[DivAmount_Cur]
	FROM [gap].[AnalysisResults]				g
	LEFT OUTER JOIN [dim].[Gap_Parent]			p
		ON	p.[GapId]			= g.[GapId]
	LEFT OUTER JOIN [dim].[Gap_Bridge]			d
		ON 	d.[FactorSetId]		= p.[FactorSetId]
		AND	d.[DescendantId]	= g.[GapId]
		AND	d.[GapId]			IN ('FeedProdPrices', 'NetCashMargin')
	) t
INNER JOIN [dim].[Gap_Bridge]					b
	ON	b.[FactorSetId]		= t.[FactorSetId]
	AND	b.[DescendantId]	= t.[GapId]
GROUP BY
	t.[FactorSetId],
	t.[GroupId],
	t.[TargetId],
	t.[GroupProdCap_kMT],
	t.[BenchProdCap_kMT],
	b.[GapId],
	t.[CurrencyId];