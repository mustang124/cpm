﻿CREATE TABLE [gap].[AnalysisResults] (
    [GroupId]          VARCHAR (25)       NOT NULL,
    [TargetId]         VARCHAR (25)       NOT NULL,
    [GroupProdCap_kMT] REAL               NOT NULL,
    [BenchProdCap_kMT] REAL               NOT NULL,
    [GapId]            VARCHAR (25)       NOT NULL,
    [CurrencyId]       VARCHAR (4)        NOT NULL,
    [DataType]         VARCHAR (24)       NOT NULL,
    [GapAmount_Cur]    REAL               NOT NULL,
    [DivAmount_Cur]    REAL               NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_AnalysisResults_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_AnalysisResults_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_AnalysisResults_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_AnalysisResults_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_AnalysisResults] PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC, [GapId] ASC, [DataType] ASC, [CurrencyId] ASC),
    CONSTRAINT [CK_AnalysisResults_CurrencyId_EmptyString] CHECK ([CurrencyId]<>''),
    CONSTRAINT [CK_AnalysisResults_DataType_EmptyString] CHECK ([DataType]<>''),
    CONSTRAINT [CK_AnalysisResults_GapId_EmptyString] CHECK ([GapId]<>''),
    CONSTRAINT [CK_AnalysisResults_GroupId_EmptyString] CHECK ([GroupId]<>''),
    CONSTRAINT [CK_AnalysisResults_TargetId_EmptyString] CHECK ([TargetId]<>'')
);


GO

CREATE TRIGGER [gap].[t_AnalysisResults_u]
    ON [gap].[AnalysisResults]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [gap].[AnalysisResults]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[gap].[AnalysisResults].[GroupId]		= INSERTED.[GroupId]
		AND [gap].[AnalysisResults].[TargetId]		= INSERTED.[TargetId]
		AND [gap].[AnalysisResults].[GapId]			= INSERTED.[GapId]
		AND [gap].[AnalysisResults].[DataType]		= INSERTED.[DataType]
		AND [gap].[AnalysisResults].[CurrencyId]	= INSERTED.[CurrencyId];

END;