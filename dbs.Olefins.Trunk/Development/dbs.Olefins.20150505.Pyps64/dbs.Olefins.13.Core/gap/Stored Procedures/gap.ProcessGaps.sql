﻿CREATE PROCEDURE [gap].[ProcessGaps]
(
	@FactorSetId			VARCHAR(12) = NULL,
	@GroupId				VARCHAR(25) = NULL,
	@TargetId				VARCHAR(25) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	DECLARE cPeer CURSOR FOR 		
	SELECT gp.[GroupId], gp.[TargetId]
	FROM [gap].[Peers]	gp
	WHERE	gp.[GroupId]	= COALESCE(@GroupId, gp.[GroupId])
		AND	gp.[TargetId]	= COALESCE(@TargetId, gp.[TargetId]);

	OPEN cPeer;

	FETCH NEXT FROM cPeer INTO @GroupId, @TargetId;

	WHILE (@@FETCH_STATUS = 0)		
	BEGIN

		PRINT 'Processing: ' + @GroupId + '::' + @TargetId;

		EXECUTE [gap].[Delete_AnalysisResults] @GroupId, @TargetId;
		EXECUTE [gap].[Insert_AnalysisResults] @FactorSetId, @GroupId, @TargetId;

		FETCH NEXT FROM cPeer INTO @GroupId, @TargetId;

	END;

	CLOSE cPeer;
	DEALLOCATE cPeer;

END;