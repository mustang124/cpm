﻿CREATE PROCEDURE [gap].[Delete_YieldMargin]
(
	@GroupId	VARCHAR(25),
	@DataType	VARCHAR(4)	
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	--BEGIN TRY

		DELETE FROM [gap].[YieldMargins]
		WHERE	[GroupId]	= @GroupId
			AND	[DataType]	= @DataType;

	--END TRY
	--BEGIN CATCH
		
	--	SET @GroupId = 'G:' + @GroupId + '-T:' + @TargetId;
	--	EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

	--	RETURN ERROR_NUMBER();

	--END CATCH;

END;