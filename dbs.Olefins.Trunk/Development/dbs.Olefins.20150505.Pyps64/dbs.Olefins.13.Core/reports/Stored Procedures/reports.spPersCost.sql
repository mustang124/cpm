﻿


CREATE              PROC [reports].[spPersCost](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @Currency as varchar(5)
SELECT @Currency = 'USD'
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DELETE FROM reports.PersCost WHERE GroupId = @GroupId
Insert into reports.PersCost (GroupId
, Currency
, PersCost_CurrPerEDC
, PersCost_CurrPerHVC_MT
)

SELECT GroupId = @GroupId
, Currency = @Currency
, PersCost_CurrPerEDC		= CASE WHEN SUM(d.kEdc)>0 THEN SUM(TaAdjAnn_Amount_Cur) / SUM(d.kEdc) END
, PersCost_CurrPerHVC_MT	= CASE WHEN SUM(d.HvcProd_kMT)>0 THEN SUM(TaAdjAnn_Amount_Cur) / SUM(d.HvcProd_kMT) END
FROM @MyGroup r JOIN calc.PersCostAggregate p on p.Refnum=r.Refnum and p.CurrencyRpt = @Currency 
JOIN dbo.TSort t on t.Refnum=r.Refnum and p.FactorSetId = t.FactorSetId
JOIN dbo.Divisors d on d.Refnum=r.Refnum

SET NOCOUNT OFF

















