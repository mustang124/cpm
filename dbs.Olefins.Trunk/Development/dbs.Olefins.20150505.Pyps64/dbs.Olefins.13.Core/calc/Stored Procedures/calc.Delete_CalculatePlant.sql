﻿CREATE PROCEDURE [calc].[Delete_CalculatePlant]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		EXECUTE [calc].[Delete_Efficiency]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_Efficiency]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_PeerGroupCapacity]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_PeerGroupCoGen]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_PeerGroupEdc]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_PeerGroupTech]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_Divisors]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_EmissionsCarbonDirect]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_EmissionsCarbonStandard]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_EmissionsCarbonPlant]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_PeerGroupFeedClass]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_ContainedPyroFeedLiquid]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_ContainedPyroFeedLight]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_MaintExpenseOccSwb]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_MaintExpenseMpsSwb]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_MaintExpenseLaborMatl]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_MaintExpenseCostCategory]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_MaintCostRoutine]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_MaintCostAverage]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_MaintPersAverage]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_EnergyNormal]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_Roi]										@Refnum, @fpl;
		EXECUTE [calc].[Delete_Mei]										@Refnum, @fpl;
		EXECUTE [calc].[Delete_MeiMaintenanceCostIndex]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_MeiLosses]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_MeiUnreliability]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_MarginSensitivity]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_Margin]									@Refnum, @fpl;
		EXECUTE [calc].[Delete_MarginAggregate]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_MarginMetaSensitivity]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_MarginMeta]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_MarginMetaAggregate]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_ReliabilityIndicator]					@Refnum, @fpl;

		EXECUTE [inter].[Delete_ReliabilityProductLost]					@Refnum, @fpl;

		EXECUTE [calc].[Delete_ReliabilityOpLossAnnualized]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_ReliabilityOpLossAvailability]			@Refnum, @fpl;
		EXECUTE [calc].[Delete_ReplacementValue]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_ReplValOtherOffSite]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_ReplValSuppFeedAdjustment]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_ReplValPyroFlexibility]					@Refnum, @fpl;

		EXECUTE [inter].[Delete_ReplValPyroCapacityEstimate]			@Refnum, @fpl;
		EXECUTE [inter].[Delete_ReplValPyroCapacity]					@Refnum, @fpl;
		EXECUTE [inter].[Delete_ReplValEncEstimate]						@Refnum, @fpl;
	
		EXECUTE [calc].[Delete_PersCost]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_PersSupervisoryRatioTaAdj]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_PersAggregateAdj]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_PersMaintTa]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_OpEx]									@Refnum, @fpl;

		EXECUTE [inter].[Delete_PricingPPFCFuelGas]						@Refnum, @fpl;

		EXECUTE [calc].[Delete_PricingPlantStreamQuarters]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_PricingPlantComposition]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_PricingPlantStream]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_PricingPlantAccount]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_PricingYieldComposition]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_Eei]										@Refnum, @fpl;
		EXECUTE [calc].[Delete_EnergySeparation]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_ApcIndex]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_ApcIndexApplications]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_ApcIndexOnLineModel]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_ApcIndexPyroFurnIndivid]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_ApcIndexSteamOther]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_ApcAbsenceCorrection]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_Furnaces]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_FacilitiesCountComplexity]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_NicenessFeed]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_NicenessEnergy]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergy]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergyFreshFeed]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergyFracFeed]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergyHydroPur]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergyHydroTreat]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergyRedCrackedGas]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergyRedEthylene]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergyRedPropylene]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_StandardEnergySupplemental]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_Edc]										@Refnum, @fpl;
		EXECUTE [calc].[Delete_SimulationCompositionYieldImprovement]	@Refnum, @fpl;
		EXECUTE [calc].[Delete_CompositionYieldRatio]					@Refnum, @fpl;

		EXECUTE [inter].[Delete_YIRProduct]								@Refnum, @fpl;
		EXECUTE [inter].[Delete_YIRSupplemental]						@Refnum, @fpl;

		EXECUTE [calc].[Delete_CompositionYieldPlant]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_CapacityLostProdOpportunity]				@Refnum, @fpl;
		EXECUTE [calc].[Delete_QuantityRecycled]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_CapacityUtilization]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_CapacityProductionRatio]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_CapacityPlantTaAdj]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_CapacityPlant]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_CapacityGrowthAggregate]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_DivisorsProduction]						@Refnum, @fpl;

		EXECUTE [calc].[Delete_DivisorsFeed]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_TaAdjRatio]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_TaAdjPers]								@Refnum, @fpl;
		EXECUTE [calc].[Delete_TaAdjReliability]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_TaAdjCapacityAggregate]					@Refnum, @fpl;
		EXECUTE [calc].[Delete_TaAdjCapacity]							@Refnum, @fpl;
		EXECUTE [calc].[Delete_TaAdjStudyYear]							@Refnum, @fpl;

		EXECUTE [inter].[Delete_DivisorsProduction]						@Refnum, @fpl;
		EXECUTE [calc].[Delete_CompositionStream]						@Refnum, @fpl;
		EXECUTE [inter].[Delete_CompositionCalc]						@Refnum, @fpl;

		EXECUTE [calc].[Delete_PlantRecycles]							@Refnum, @fpl;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;