﻿CREATE PROCEDURE [calc].[Insert_CalculatePlant]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_CapacityGrowthAggregate]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_ContainedPyroFeedLiquid]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_FacilitiesCountComplexity]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_PlantRecycles]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_TaAdjCapacity]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_TaAdjReliability]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_TaAdjStudyYear]								@Refnum, @fpl;
		
		EXECUTE [inter].[Insert_CompositionCalc]							@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_ApcAbsenceCorrection]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_CapacityPlant]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_CapacityQuantityRecycled]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_CompositionStream]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Furnaces]									@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReliabilityOpLossAnnualized]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_TaAdjCapacityAggregate]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_TaAdjPers]									@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE	[calc].[Insert_ReliabilityOpLossAvailability_Annualized]	@Refnum, @fpl;
		EXECUTE	[calc].[Insert_ReliabilityOpLossAvailability_TurnAround]	@Refnum, @fpl;
		EXECUTE	[calc].[Insert_ReliabilityOpLossAvailability_Other]			@Refnum, @fpl;

		EXECUTE [calc].[Insert_ApcIndexPyroFurnIndivid]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_CapacityPlantTaAdj]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_ContainedPyroFeedLight]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_PersCost]									@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_MaintCostRoutine]*								*/
		EXECUTE [calc].[Insert_MaintCostRoutine]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_MaintCostRoutine_Furnace]					@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_ApcIndexApplications]*							*/
		EXECUTE [calc].[Insert_ApcIndexApplications_General]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_ApcIndexApplications_Quench]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_ApcIndexApplications_Specific]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_ApcIndexApplications_SteamOther]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_ApcIndexApplications_TowerDepropDebut]		@Refnum, @fpl;

		/*EXECUTE [calc].[Insert_StandardEnergy] (2011 and prior)					*/
		EXECUTE [calc].[Insert_StandardEnergy_Compressors]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_FracFeed]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_FreshFeed]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_HydroPur]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_Hydrotreater]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_Purity]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_Supplemental]					@Refnum, @fpl;

		EXECUTE [inter].[Insert_DivisorsProduction]							@Refnum, @fpl;
		EXECUTE [inter].[Insert_ReliabilityProductLost]						@Refnum, @fpl;
		EXECUTE [inter].[Insert_ReplValEncEstimate]							@Refnum, @fpl;
		EXECUTE [inter].[Insert_YirProduct]									@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_ApcIndexOnLineModel]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_PeerGroup_FeedClass]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReliabilityIndicator]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_TaAdjRatio]									@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_Pers_MaintTa] *									*/
		EXECUTE [calc].[Insert_Pers_MaintTa_AnnRetube]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_Pers_MaintTa_Maint]							@Refnum, @fpl;

		EXECUTE [inter].[Insert_ReplValPyroCapacityEstimate]				@Refnum, @fpl;
		EXECUTE [inter].[Insert_YirSupplemental]							@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_ApcIndex]									@Refnum, @fpl;
		EXECUTE [calc].[Insert_DivisorsFeed]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_PersAggregateAdj]							@Refnum, @fpl;

		/*EXECUTE [calc].[Insert_NicenessFeed] (2013 and later)						*/
		EXECUTE [calc].[Insert_NicenessFeed_Butane]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_NicenessFeed_Liquid]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_NicenessFeed_LiquidOther]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_NicenessFeed_LiquidOther_NoComp]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_NicenessFeed_LiquidNoComp]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_NicenessFeed_LightEP]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_NicenessFeed_SupplementalEP]					@Refnum, @fpl;

		/*EXECUTE [calc].[Insert_NicenessEnergy] (2013 and later)					*/
		EXECUTE [calc].[Insert_NicenessEnergy_Feed]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_NicenessEnergy_Methane]						@Refnum, @fpl;

		EXECUTE [inter].[Insert_ReplValPyroCapacity]						@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_DivisorsProduction]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_MaintPersAverage]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplValPyroFlexibility]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_PersSupervisoryRatioTaAdj]					@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_CompositionYieldPlant_Products]					*/
		EXECUTE [calc].[Insert_CompositionYieldPlant_Products_Plant]		@Refnum, @fpl;
		EXECUTE [calc].[Insert_CompositionYieldPlant_Products_Models]		@Refnum, @fpl;

		----------------------------------------------------------------------------------

		/*EXECUTE [calc].[Insert_StandardEnergy] (2013 and later)					*/
		EXECUTE [calc].[Insert_StandardEnergyFracFeed]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergyFreshFeed]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergyHydroPur]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergyHydroTreat]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergyRedCrackedGas]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergyRedEthylene]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergyRedPropylene]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergySupplemental]					@Refnum, @fpl;

		EXECUTE [calc].[Insert_StandardEnergy_2013_FracFeed]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_2013_FreshFeed]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_2013_HydroPur]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_2013_HydroTreat]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_2013_RedCrackedGas]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_2013_RedEthylene]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_2013_RedPropylene]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_StandardEnergy_2013_Supplemental]			@Refnum, @fpl;
	
		EXECUTE [calc].[Insert_CapacityLostProdOpportunity]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_CapacityProductionRatio]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_CompositionYieldRatio]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_EnergyNormal]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplValSuppFeedAdjustment]					@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_MaintCostAverage]*								*/
		EXECUTE [calc].[Insert_MaintCostAverage_Maint]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_MaintCostAverage_MaintRetubeMatl]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_MaintCostAverage_MaintRetubeLabor]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_MaintCostAverage_Swb]						@Refnum, @fpl;

		EXECUTE [calc].[Insert_EnergySeparation_FracFeed]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_EnergySeparation_Hydrotreat]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_EnergySeparation_Supp]						@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_Eei]											@Refnum, @fpl;
		EXECUTE [calc].[Insert_PricingPlantStream]							@Refnum, @fpl;

		EXECUTE [calc].[Insert_PricingPlantStream_Metathesis]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_PricingYieldComposition]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplValOtherOffSite]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_SimulationCompositionYieldImprovement_Models]	@Refnum, @fpl;
		EXECUTE [calc].[Insert_SimulationCompositionYieldImprovement_C2H2]	@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_CapacityUtilization] *							*/
		EXECUTE [calc].[Insert_CapacityUtilization_PlantFeed]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_CapacityUtilization_FreshPyroFeed]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_CapacityUtilization_Products]				@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_OpEx]										@Refnum, @fpl;
		EXECUTE [calc].[Insert_PricingPlantQuarters]						@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_EmissionsCarbonPlant] *							*/
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_Direct]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_Electricity]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_Flare]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_LightFeed]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_Methane]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_N2O]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_PPFC]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_RogInerts]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonPlant_Steam]					@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_Edc] *											*/
		EXECUTE [calc].[Insert_Edc_FreshFeed]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_RecycledSupplemental]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_Compressors]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_FracFeed]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_Utilities]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_Electricity]								@Refnum, @fpl;

		EXECUTE [calc].[Insert_Edc_Boilers]									@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_Capacity]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_HydroPur]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_Hydrotreater]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_ElecGen]									@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_FeedPrep]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_RedPropylene]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_RedCrackedGas]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Edc_SuppTot]									@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_EmissionsCarbonStandard] *						*/
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_Direct]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_Electricity]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_Flare]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_LightFeed]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_Methane]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_N2O]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_RogInerts]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonStandard_Steam]				@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_ReplacementValue] *								*/
		EXECUTE [calc].[Insert_ReplacementValue_Base]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_Boilers]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_DualTrain]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_Electricity]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_FeedPrep]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_OffSite]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_Pyrolysis]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_Steam]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_ReplacementValue_Supplemental]				@Refnum, @fpl;
		EXECUTE [calc].[Update_ReplacementValue]							@Refnum, @fpl;

		----------------------------------------------------------------------------------
	
		EXECUTE [calc].[Insert_MeiLosses]									@Refnum, @fpl;
		EXECUTE [calc].[Insert_MeiMaintenanceCostIndex]						@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_EmissionsCarbonDirect]*							*/
		EXECUTE [calc].[Insert_EmissionsCarbonDirect_CO2Hydrogen]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonDirect_CO2Methane]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonDirect_N2OCombustion]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_EmissionsCarbonDirect_Standard]				@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_MaintExpenseLaborMatl]*							*/
		EXECUTE [calc].[Insert_MaintExpenseLaborMatl_MaintOpExAgg]			@Refnum, @fpl;
		EXECUTE [calc].[Insert_MaintExpenseLaborMatl_Furnaces]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_MaintExpenseLaborMatl_Retube]				@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_Margin] *										*/
		EXECUTE [calc].[Insert_Margin_Basis]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Margin_Ethylene]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Margin_ProdHvc]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Margin_ProdOlefins]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Margin_Propylene]							@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_MaintExpenseMpsSwb]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_MarginAggregate]								@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_MaintExpenseOccSwb]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_MarginSensitivity]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_MeiUnreliability]							@Refnum, @fpl;

		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_MaintExpenseCostCategory]					@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_Roi] *											*/
		EXECUTE [calc].[Insert_Roi_Margin]									@Refnum, @fpl;
		EXECUTE [calc].[Insert_Roi_MarginSensitivity]						@Refnum, @fpl;

		/*	EXECUTE [calc].[Insert_Mei] *											*/
		EXECUTE [calc].[Insert_Mei_Losses]									@Refnum, @fpl;
		EXECUTE [calc].[Insert_Mei_Maintenance]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Mei_Unreliability]							@Refnum, @fpl;

		----------------------------------------------------------------------------------
	
		/*	EXECUTE [calc].[Insert_Efficiency] *									*/
		EXECUTE [calc].[Insert_Efficiency_FreshPyroFeed]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_SuppTot]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_FeedPrep]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_Hydrotreater]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_HydroPur]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_RedPropyleneCG]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_RedPropyleneRG]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_RedCrackedGasTrans]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_BoilFiredSteam]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Efficiency_ElecGen]							@Refnum, @fpl;
		
		----------------------------------------------------------------------------------
		----------------------------------------------------------------------------------

		EXECUTE [calc].[Insert_Divisors_All]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_FreshFeed]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_FreshFeed_TaAdj]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_kEdc]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_kUEdc]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_kUEdc_TaAdj]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_Production]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_Production_TaAdj]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_Production_Cost]					@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_PyroFeed]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_PyroFeed_TaAdj]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_Replacement]						@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_ReplacementLocation]				@Refnum, @fpl;
		EXECUTE [calc].[Insert_Divisors_Efficiency]							@Refnum, @fpl;

		EXECUTE [calc].[Insert_PeerGroup_Capacity]							@Refnum, @fpl;
		EXECUTE [calc].[Insert_PeerGroup_CoGen]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_PeerGroup_Edc]								@Refnum, @fpl;
		EXECUTE [calc].[Insert_PeerGroup_Tech]								@Refnum, @fpl;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, 	@Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
