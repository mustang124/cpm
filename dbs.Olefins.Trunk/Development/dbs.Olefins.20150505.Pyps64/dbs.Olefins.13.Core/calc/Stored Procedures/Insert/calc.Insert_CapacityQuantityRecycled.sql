﻿CREATE PROCEDURE calc.Insert_CapacityQuantityRecycled
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	--	Insert Records	-----------------------------------------------------------------
	BEGIN TRY

		DECLARE @MapSuppToRec TABLE
		(
			StreamID_Supp		VARCHAR(42)		NOT	NULL	CHECK(StreamID_Supp <> ''),
			StreamID_SuppRec	VARCHAR(42)		NOT	NULL	CHECK(StreamID_SuppRec <> ''),
			StreamID_Rec		VARCHAR(42)		NOT	NULL	CHECK(StreamID_Rec <> ''),
			ComponentId			VARCHAR(42)		NOT	NULL	CHECK(ComponentId <> ''),
			RecycleId			TINYINT			NOT	NULL	CHECK(RecycleId >= 0 AND RecycleId <= 4),
			PRIMARY KEY CLUSTERED(StreamID_Supp ASC)
		);

		INSERT INTO @MapSuppToRec(StreamID_Supp, StreamID_SuppRec, ComponentId, StreamID_Rec, RecycleId)
		VALUES('DilEthane', 'SuppToRecEthane',	'C2H6',		'EthRec', 1),
			('ROGEthane',	'SuppToRecEthane',	'C2H6',		'EthRec', 1),
			('DilPropane',	'SuppToRecPropane',	'C3H8',		'ProRec', 2),
			('ROGPropane',	'SuppToRecPropane',	'C3H8',		'ProRec', 2),
			('DilButane',	'SuppToRecButane',	'C4H10',	'ButRec', 4),
			-- BUG 82 -- YIR's don't match -> include 'ROGC4Plus'
			('ROGC4Plus',	'SuppToRecButane',	'C4H10',	'ButRec', 4);
			----('ButRec',		'SuppToRecButane',	'C4H10',	'ButRec', 4);

		INSERT INTO calc.QuantityRecycled(FactorSetId, Refnum, CalDateKey, StreamId, RecycleId,
			ComponentId, Recycled_WtPcnt, Quantity_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			q.StreamId,
			calc.GeometricLimit(m.RecycleId, pr.RecycleId),
			m.ComponentId,
			r.Recycled_WtPcnt,
			q.Quantity_kMT
		FROM @fpl										tsq
		INNER JOIN fact.Quantity						q
			ON	q.Refnum = tsq.Refnum
			AND	q.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN @MapSuppToRec						m
			ON	m.StreamID_Supp = q.StreamId
		INNER JOIN fact.QuantitySuppRecycled			r
			ON	r.Refnum = tsq.Refnum
			AND	r.CalDateKey = tsq.Plant_AnnDateKey
			AND	r.StreamId = m.StreamID_SuppRec
			AND r.ComponentId = m.ComponentId
		INNER JOIN calc.PlantRecycles					pr
			ON	pr.FactorSetId = tsq.FactorSetId
			AND	pr.Refnum = tsq.Refnum
			AND pr.CalDateKey = tsq.Plant_AnnDateKey
		WHERE	q.Quantity_kMT > 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;