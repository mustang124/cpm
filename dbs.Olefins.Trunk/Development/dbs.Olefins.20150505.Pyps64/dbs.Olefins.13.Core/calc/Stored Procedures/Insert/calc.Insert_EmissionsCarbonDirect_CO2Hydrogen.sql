﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonDirect_CO2Hydrogen]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonDirect(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_MBtu, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			'PYPS',
			fpl.Plant_QtrDateKey,
			H2.EmissionsId,
			H2.Quantity_MBtu,
			0.0
		FROM @fpl										fpl
		INNER JOIN calc.EmissionsCarbonStandard			H2
			ON	H2.FactorSetId		= fpl.FactorSetId
			AND	H2.Refnum			= fpl.Refnum
			AND	H2.CalDateKey		= fpl.Plant_QtrDateKey
			AND	H2.EmissionsId		= 'CO2Hydrogen'
			AND	H2.Quantity_MBtu	> 0.0
		WHERE	fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END