﻿CREATE PROCEDURE [calc].[Insert_EnergyNormal]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EnergyNormal(FactorSetId, Refnum, CalDateKey, CoolingWaterDeltaTemp_C, CoolingWaterEnergy_BtuLb, FinFanEnergy_BtuLb, SteamExtraction_MtMtHVC, SteamCondensing_MtMtHVC, ProcessedHeatExchanged_MtMtHVC)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			gpe.CoolingWaterReturnTemp_C - gpe.CoolingWaterSupplyTemp_C		[CoolingWaterDeltaTemp_C],

			[$(DbGlobal)].dbo.UnitsConv(gpe.CoolingWater_MTd, 'MJ/MT', 'BTU/LB')
					* 365.0 * 4186.0 * (ISNULL(gpe.CoolingWaterReturnTemp_C, 0.0) - ISNULL(gpe.CoolingWaterSupplyTemp_C, 0.0))
					/ dp.[ProductionSold_kMT] / 1000000.0		[CoolingWaterEnergy_BtuLb],
			
			[$(DbGlobal)].dbo.UnitsConv(gpe.FinFanHeatAbsorb_kBTU, 'BTU/MT', 'BTU/LB') * 8760.0
					/ dp.[ProductionSold_kMT]		[FinFanEnergy_BtuLb],

			gpe.SteamExtratRate_kMTd	* 365.0
					/ dp.[ProductionSold_kMT]										[SteamExtraction_MtMtHVC],

			gpe.SteamCondenseRate_kMTd	* 365.0
					/ dp.[ProductionSold_kMT]										[SteamCondensing_MtMtHVC],

			[$(DbGlobal)].dbo.UnitsConv(gpe.ExchangerHeatDuty_BTU, 'BTU/MT', 'BTU/LB') * 8760.0
					/ dp.[ProductionSold_kMT]										[ProcessedHeatExchanged_MtMtHVC]

		FROM @fpl							fpl
		INNER JOIN fact.GenPlantEnergy		gpe
			ON	gpe.Refnum = fpl.Refnum
			AND	gpe.CalDateKey = fpl.Plant_QtrDateKey
		INNER JOIN calc.DivisorsProduction	dp
			ON	dp.FactorSetId = fpl.FactorSetId
			AND	dp.Refnum = fpl.Refnum
			AND	dp.CalDateKey = fpl.Plant_QtrDateKey
			AND	dp.ComponentId = 'ProdHVC'
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;