﻿CREATE PROCEDURE [calc].[Insert_MarginAggregate]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	BEGIN TRY

		INSERT INTO calc.MarginAggregate(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MarginAnalysis, MarginId, Hierarchy,
			Stream_Cur, OpEx_Cur, Amount_Cur,
			TaAdj_Stream_Cur, TaAdj_OpEx_Cur, TaAdj_Amount_Cur)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			fpl.CurrencyRpt,
			m.MarginAnalysis,
			m.MarginId,
			m.Hierarchy,
			SUM(m.Stream_Cur),
			SUM(m.OpEx_Cur),
			SUM(m.Amount_Cur),
			SUM(m.TaAdj_Stream_Cur),
			SUM(m.TaAdj_OpEx_Cur),
			SUM(m.TaAdj_Amount_Cur)
		FROM @fpl								fpl
		INNER JOIN calc.Margin					m
			ON	m.FactorSetId = fpl.FactorSetId
			AND	m.Refnum = fpl.Refnum
			AND	m.CalDateKey = fpl.Plant_QtrDateKey
			AND	m.CurrencyRpt = fpl.CurrencyRpt
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			fpl.CurrencyRpt,
			m.MarginAnalysis,
			m.MarginId,
			m.Hierarchy;
  
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END