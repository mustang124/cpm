﻿CREATE PROCEDURE [calc].[Insert_Edc_Capacity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			k.EdcId,
			k.EdcId,
			k.Value * Power(u.Value, e.Value) / 1000.0	[kEdc]
		FROM @fpl										tsq
		INNER JOIN(
			SELECT
				c.Refnum,
				c.CalDateKey,
				c.Capacity_kMT										[PyroFurnFresh],
				c.Capacity_kMT * m.PyroFurnSpare_Pcnt / 100.0		[PyroFurnSpare]
			FROM fact.Capacity c
			LEFT OUTER JOIN fact.FacilitiesMisc m
				ON	m.Refnum = c.Refnum
			WHERE 	c.StreamId = 'FreshPyroFeed'
				AND	c.Capacity_kMT			IS NOT NULL
			) p
			UNPIVOT (
			Value FOR EdcId IN (
				PyroFurnFresh,
				PyroFurnSpare
				)
			) u
			ON	u.Refnum = tsq.Refnum
			AND	u.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN ante.EdcCoefficients					k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.EdcId = u.EdcId
		INNER JOIN ante.EdcExponents e
			ON	e.FactorSetId = tsq.FactorSetId
			AND e.EdcId = u.EdcId
		WHERE	tsq.[FactorSet_AnnDateKey] < 20130000
			AND	u.Value		IS NOT NULL
			AND	e.Value		IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

	BEGIN TRY
		
		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.Edc (FreshPyroFeed)';
		PRINT @ProcedureDesc;

		INSERT INTO [calc].[Edc]([FactorSetId], [Refnum], [CalDateKey], [EdcId], [EdcDetail], [kEdc])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'FreshPyroFeed',
			'FreshPyroFeed',
			(cp.[Stream_MTd] - COALESCE(SUM(qSupp.[Quantity_kMT] * qRec.[Recovered_WtPcnt]) / cUtil.[_UtilizationStream_Pcnt] * 1000.0 / 365.0, 0.0)) * k.[Value] / 1000.0
		FROM @fpl											fpl
		INNER JOIN [calc].[CapacityPlant]					cp
			ON	cp.[FactorSetId]	= fpl.[FactorSetId]
			AND	cp.[Refnum]			= fpl.[Refnum]
			AND	cp.[CalDateKey]		= fpl.[Plant_AnnDateKey]
			AND	cp.[StreamId]		= 'ProdOlefins'

		LEFT OUTER JOIN [fact].[Quantity]					qSupp
			ON	qSupp.[Refnum]		= fpl.[Refnum]
			AND	qSupp.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	qSupp.StreamId		IN ('ConcEthylene', 'ConcPropylene', 'ROGEthylene', 'ROGPropylene', 'DilEthylene', 'DilPropylene')
		LEFT OUTER JOIN [fact].[QuantitySuppRecovery]		qRec
			ON	qRec.[Refnum]		= fpl.[Refnum]
			AND	qRec.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	qRec.[StreamId]		= qSupp.[StreamId]

		INNER JOIN [calc].[CapacityUtilization]				cUtil
			ON	cUtil.[FactorSetId]	= fpl.[FactorSetId]
			AND	cUtil.[Refnum]		= fpl.[Refnum]
			AND	cUtil.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	cUtil.[SchedId]		= 'P'
			AND	cUtil.[ComponentId]	= 'ProdOlefins'

		INNER JOIN [calc].[PeerGroupFeedClass]				f
			ON	f.[FactorSetId]		= fpl.[FactorSetId]
			AND	f.[Refnum]			= fpl.[Refnum]
			AND	f.[CalDateKey]		= fpl.[Plant_AnnDateKey]

		INNER JOIN (VALUES
			('1', 'Ethane'),
			('2', 'LPG'),
			('3', 'Liquid'),
			('4', 'Liquid')
			)												map ([PeerGroup], [EdcId])
			ON map.[PeerGroup]	= f.[PeerGroup]
		INNER JOIN [ante].[EdcCoefficients]					k
			ON	k.[FactorSetId]	= fpl.[FactorSetId]
			AND	k.[EdcId]		= map.[EdcId]

		WHERE	fpl.[FactorSet_AnnDateKey] > 20130000
			AND	cp.[Stream_MTd]				IS NOT NULL
			AND	cUtil.[_UtilizationStream_Pcnt]	<> 0.0
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			cp.[Stream_MTd],
			cUtil.[_UtilizationStream_Pcnt],
			k.[Value];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;