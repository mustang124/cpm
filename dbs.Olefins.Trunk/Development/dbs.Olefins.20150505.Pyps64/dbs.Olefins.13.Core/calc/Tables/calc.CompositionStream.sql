﻿CREATE TABLE [calc].[CompositionStream] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [SimModelId]        VARCHAR (12)       CONSTRAINT [DF_CompositionStream_SimModelID] DEFAULT ('Plant') NOT NULL,
    [OpCondId]          VARCHAR (12)       CONSTRAINT [DF_CompositionStream_OpCondID] DEFAULT ('OSOP') NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [RecycleId]         TINYINT            NULL,
    [ComponentId]       VARCHAR (42)       NOT NULL,
    [Component_kMT]     REAL               NOT NULL,
    [Component_WtPcnt]  REAL               NOT NULL,
    [Priced_kMT]        REAL               NULL,
    [Priced_WtPcnt]     REAL               NULL,
    [Purity_kMT]        REAL               NULL,
    [Purity_WtPcnt]     REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_CompositionStream_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_CompositionStream_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_CompositionStream_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_CompositionStream_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CompositionStream] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [StreamId] ASC, [StreamDescription] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_CompositionStream_Component_kMT] CHECK ([Component_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_CompositionStream_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_calc_CompositionStream_Priced_kMT] CHECK ([Priced_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_CompositionStream_Priced_WtPcnt] CHECK ([Priced_WtPcnt]>=(0.0) AND [Priced_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_calc_CompositionStream_Purity_kMT] CHECK ([Purity_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_CompositionStream_Purity_WtPcnt] CHECK ([Purity_WtPcnt]>=(0.0) AND [Purity_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_calc_CompositionStream_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CompositionStream_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_CompositionStream_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CompositionStream_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_CompositionStream_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_calc_CompositionStream_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_CompositionStream_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_CompositionStream_LossIndex_Ethylene]
    ON [calc].[CompositionStream]([StreamId] ASC, [ComponentId] ASC)
    INCLUDE([FactorSetId], [Refnum], [SimModelId], [OpCondId], [Component_kMT], [Component_WtPcnt]);


GO
CREATE NONCLUSTERED INDEX [IX_CompositionStream_PlantPricing_PPFC]
    ON [calc].[CompositionStream]([StreamId] ASC, [ComponentId] ASC, [Component_kMT] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey]);


GO
CREATE NONCLUSTERED INDEX [IX_CompositionStream_DivisorsProduction]
    ON [calc].[CompositionStream]([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [ComponentId] ASC)
    INCLUDE([StreamId], [Component_kMT], [Purity_kMT]);


GO
CREATE TRIGGER calc.t_CompositionStream_u
	ON  calc.[CompositionStream]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[CompositionStream]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[CompositionStream].FactorSetId		= INSERTED.FactorSetId
		AND calc.[CompositionStream].Refnum				= INSERTED.Refnum
		AND calc.[CompositionStream].CalDateKey			= INSERTED.CalDateKey
		AND calc.[CompositionStream].StreamId			= INSERTED.StreamId
		AND calc.[CompositionStream].StreamDescription	= INSERTED.StreamDescription
		AND calc.[CompositionStream].ComponentId		= INSERTED.ComponentId;
				
END