﻿CREATE TABLE [calc].[Mei] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [CurrencyRpt]      VARCHAR (4)        NOT NULL,
    [MeiId]            VARCHAR (42)       NOT NULL,
    [Indicator_Index]  REAL               NOT NULL,
    [Indicator_PcntRv] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_Mei_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_Mei_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_Mei_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_Mei_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_Mei] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MeiId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_Mei_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_Mei_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_Mei_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_Mei_Mei_LookUp] FOREIGN KEY ([MeiId]) REFERENCES [dim].[Mei_LookUp] ([MeiId]),
    CONSTRAINT [FK_calc_Mei_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_Mei_u
	ON  calc.Mei
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.Mei
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.Mei.FactorSetId	= INSERTED.FactorSetId
		AND calc.Mei.Refnum			= INSERTED.Refnum
		AND calc.Mei.CalDateKey		= INSERTED.CalDateKey
		AND calc.Mei.CurrencyRpt	= INSERTED.CurrencyRpt;
				
END