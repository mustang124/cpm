﻿CREATE TABLE [stgFact].[FurnRely] (
    [Refnum]               VARCHAR (25)       NOT NULL,
    [FurnNo]               TINYINT            NOT NULL,
    [FurnFeed]             VARCHAR (25)       NOT NULL,
    [DTDecoke]             REAL               NULL,
    [DTTLEClean]           REAL               NULL,
    [DTMinor]              REAL               NULL,
    [DTMajor]              REAL               NULL,
    [STDT]                 REAL               NULL,
    [DTStandby]            REAL               NULL,
    [TotTimeOff]           REAL               NULL,
    [TADownDays]           REAL               NULL,
    [OthDownDays]          REAL               NULL,
    [FurnAvail]            REAL               NULL,
    [AdjFurnOnstream]      REAL               NULL,
    [FeedQtyKMTA]          REAL               NULL,
    [FeedCapMTD]           REAL               NULL,
    [OperDays]             INT                NULL,
    [ActCap]               REAL               NULL,
    [AdjFurnUtil]          REAL               NULL,
    [FurnSlowD]            REAL               NULL,
    [YearRetubed]          SMALLINT           NULL,
    [RetubeCost]           REAL               NULL,
    [RetubeCostMatl]       REAL               NULL,
    [RetubeCostLabor]      REAL               NULL,
    [OtherMajorCost]       REAL               NULL,
    [TotCost]              REAL               NULL,
    [RetubeInterval]       REAL               NULL,
    [OnStreamDays]         INT                NULL,
    [FuelCons]             REAL               NULL,
    [FuelType]             VARCHAR (2)        NULL,
    [StackOxy]             REAL               NULL,
    [ArchDraft]            REAL               NULL,
    [StackTemp]            REAL               NULL,
    [CrossTemp]            REAL               NULL,
    [FurnFeedBreak]        VARCHAR (10)       NULL,
    [AnnRetubeCostMatlMT]  REAL               NULL,
    [AnnRetubeCostLaborMT] REAL               NULL,
    [AnnRetubeCostMT]      REAL               NULL,
    [OtherMajorCostMT]     REAL               NULL,
    [TotCostMT]            REAL               NULL,
    [FuelTypeYN]           INT                NULL,
    [FuelTypeFO]           INT                NULL,
    [FuelTypeFG]           INT                NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_FurnRely_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_stgFact_FurnRely_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_stgFact_FurnRely_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_stgFact_FurnRely_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_FurnRely] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FurnNo] ASC, [FurnFeed] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_FurnRely_u]
	ON [stgFact].[FurnRely]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[FurnRely]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[FurnRely].Refnum		= INSERTED.Refnum;

END;