﻿CREATE PROCEDURE [stgFact].[Delete_PolyMatlBalance]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PolyMatlBalance]
	WHERE [Refnum] = @Refnum;

END;