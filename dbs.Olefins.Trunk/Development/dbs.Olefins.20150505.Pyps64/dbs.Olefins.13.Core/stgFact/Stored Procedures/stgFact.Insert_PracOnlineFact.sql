﻿CREATE PROCEDURE [stgFact].[Insert_PracOnlineFact]
(
	@Refnum  VARCHAR (25),

	@Tbl9051 REAL     = NULL,
	@Tbl9061 REAL     = NULL,
	@Tbl9071 REAL     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PracOnlineFact]([Refnum], [Tbl9051], [Tbl9061], [Tbl9071])
	VALUES(@Refnum, @Tbl9051, @Tbl9061, @Tbl9071);

END;