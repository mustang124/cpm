﻿CREATE PROCEDURE [stgFact].[Delete_FeedQuality]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[FeedQuality]
	WHERE [Refnum] = @Refnum;

END;