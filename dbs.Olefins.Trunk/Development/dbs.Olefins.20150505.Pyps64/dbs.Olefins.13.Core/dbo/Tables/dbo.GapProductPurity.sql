﻿CREATE TABLE [dbo].[GapProductPurity] (
    [Refnum]                VARCHAR (25)  NOT NULL,
    [Hydrogen_WtPcnt]       REAL          NULL,
    [Methane_WtPcnt]        REAL          NULL,
    [EthyleneCG_WtPcnt]     REAL          NULL,
    [EthylenePG_WtPcnt]     REAL          NULL,
    [PropyleneCG_WtPcnt]    REAL          NULL,
    [PropylenePG_WtPcnt]    REAL          NULL,
    [PropyleneRG_WtPcnt]    REAL          NULL,
    [PropaneC3Resid_WtPcnt] REAL          NULL,
    [tsModified]            SMALLDATETIME CONSTRAINT [DF_GapProductPurity_tsModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_GapProductPurity] PRIMARY KEY CLUSTERED ([Refnum] ASC),
    CHECK ([Refnum]<>'')
);

