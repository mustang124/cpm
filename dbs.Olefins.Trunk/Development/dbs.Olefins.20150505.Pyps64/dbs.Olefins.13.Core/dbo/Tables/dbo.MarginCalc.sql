﻿CREATE TABLE [dbo].[MarginCalc] (
    [Refnum]                    VARCHAR (25) NOT NULL,
    [Currency]                  VARCHAR (8)  NOT NULL,
    [DataType]                  VARCHAR (12) NOT NULL,
    [DivisorValue]              REAL         NULL,
    [MarginAnalysis]            VARCHAR (16) NOT NULL,
    [RawMaterialCost]           REAL         NULL,
    [GrossProductValue]         REAL         NULL,
    [GrossMargin]               REAL         NULL,
    [TotCashOpEx]               REAL         NULL,
    [ProductionCost]            REAL         NULL,
    [NetCashMargin]             REAL         NULL,
    [NetCashMarginTAAdj]        REAL         NULL,
    [NetMargin]                 REAL         NULL,
    [NonCash]                   REAL         NULL,
    [STTaExp]                   REAL         NULL,
    [STNonVol]                  REAL         NULL,
    [STNonVolTA]                REAL         NULL,
    [MetaNonVol]                REAL         NULL,
    [STVol]                     REAL         NULL,
    [MetaSTVol]                 REAL         NULL,
    [TotalCashOpEx]             REAL         NULL,
    [FeedProdLossBal]           REAL         NULL,
    [EthyleneCG]                REAL         NULL,
    [EthylenePG]                REAL         NULL,
    [Ethylene]                  REAL         NULL,
    [ProdLoss]                  REAL         NULL,
    [PPFCOther]                 REAL         NULL,
    [PPFCPyroFuelOil]           REAL         NULL,
    [PPFCPyroGasOil]            REAL         NULL,
    [PPFCPyroNaphtha]           REAL         NULL,
    [PPFCButane]                REAL         NULL,
    [PPFCPropane]               REAL         NULL,
    [PPFCEthane]                REAL         NULL,
    [PPFCLiquid]                REAL         NULL,
    [PPFCFuelGas]               REAL         NULL,
    [PPFC]                      REAL         NULL,
    [ProLPG]                    REAL         NULL,
    [NRC]                       REAL         NULL,
    [PyroGasOil]                REAL         NULL,
    [PyroFuelOil]               REAL         NULL,
    [PropaneC3Resid]            REAL         NULL,
    [PyroGasoline]              REAL         NULL,
    [C4Oth]                     REAL         NULL,
    [IsoButylene]               REAL         NULL,
    [Methane]                   REAL         NULL,
    [AcidGas]                   REAL         NULL,
    [ProdMisc]                  REAL         NULL,
    [ProdOtherMargin]           REAL         NULL,
    [ProdOther]                 REAL         NULL,
    [Hydrogen]                  REAL         NULL,
    [Butadiene]                 REAL         NULL,
    [Benzene]                   REAL         NULL,
    [Acetylene]                 REAL         NULL,
    [PropyleneRG]               REAL         NULL,
    [PropyleneCG]               REAL         NULL,
    [PropylenePG]               REAL         NULL,
    [Propylene]                 REAL         NULL,
    [ProdOlefins]               REAL         NULL,
    [ProdHVC]                   REAL         NULL,
    [Prod]                      REAL         NULL,
    [ByProdAdjustmentProdOther] REAL         NULL,
    [ByProdCredit]              REAL         NULL,
    [SuppNitrogen]              REAL         NULL,
    [SuppOther]                 REAL         NULL,
    [SuppWashOil]               REAL         NULL,
    [SuppGasOil]                REAL         NULL,
    [SuppMisc]                  REAL         NULL,
    [DilMoGas]                  REAL         NULL,
    [DilPropane]                REAL         NULL,
    [DilMethane]                REAL         NULL,
    [DilHydrogen]               REAL         NULL,
    [DilEthylene]               REAL         NULL,
    [DilEthane]                 REAL         NULL,
    [DilButylene]               REAL         NULL,
    [DilButane]                 REAL         NULL,
    [DilButadiene]              REAL         NULL,
    [DilPropylene]              REAL         NULL,
    [DilBenzene]                REAL         NULL,
    [SuppDil]                   REAL         NULL,
    [ROGPropylene]              REAL         NULL,
    [ROGPropane]                REAL         NULL,
    [ROGMethane]                REAL         NULL,
    [ROGInerts]                 REAL         NULL,
    [ROGHydrogen]               REAL         NULL,
    [ROGEthylene]               REAL         NULL,
    [ROGEthane]                 REAL         NULL,
    [ROGC4Plus]                 REAL         NULL,
    [SuppROG]                   REAL         NULL,
    [ConcPropylene]             REAL         NULL,
    [ConcEthylene]              REAL         NULL,
    [ConcButadiene]             REAL         NULL,
    [ConcBenzene]               REAL         NULL,
    [SuppConc]                  REAL         NULL,
    [SuppToRec]                 REAL         NULL,
    [SuppToPyro]                REAL         NULL,
    [SuppTot]                   REAL         NULL,
    [FeedLiqOther]              REAL         NULL,
    [LiqHeavyOther]             REAL         NULL,
    [GasOilHt]                  REAL         NULL,
    [GasOilHv]                  REAL         NULL,
    [EpGT390]                   REAL         NULL,
    [Diesel]                    REAL         NULL,
    [EpLT390]                   REAL         NULL,
    [LiqHeavy]                  REAL         NULL,
    [LiqLightOther]             REAL         NULL,
    [Condensate]                REAL         NULL,
    [Raffinate]                 REAL         NULL,
    [NaphthaHv]                 REAL         NULL,
    [NaphthaFr]                 REAL         NULL,
    [NaphthaLt]                 REAL         NULL,
    [Naphtha]                   REAL         NULL,
    [HeavyNGL]                  REAL         NULL,
    [LiqC5C6]                   REAL         NULL,
    [LiqLight]                  REAL         NULL,
    [Liquid]                    REAL         NULL,
    [FeedLtOther]               REAL         NULL,
    [IsoButane]                 REAL         NULL,
    [Butane]                    REAL         NULL,
    [LPG]                       REAL         NULL,
    [Propane]                   REAL         NULL,
    [Ethane]                    REAL         NULL,
    [EPMix]                     REAL         NULL,
    [EthanePropane]             REAL         NULL,
    [Light]                     REAL         NULL,
    [FreshPyroFeed]             REAL         NULL,
    [PlantFeed]                 REAL         NULL,
    [FeedOther]                 REAL         NULL,
    [FeedProdLoss]              REAL         NULL,
    [MarginGross]               REAL         NULL,
    [MarginNetCash]             REAL         NULL,
    [TotRefExp]                 REAL         NULL,
    [PreTaxIncome]              REAL         NULL,
    CONSTRAINT [PK_MarginCalc] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Currency] ASC, [DataType] ASC, [MarginAnalysis] ASC) WITH (FILLFACTOR = 70)
);

