﻿CREATE TABLE [dbo].[EnergyCalc] (
    [Refnum]           VARCHAR (25) NOT NULL,
    [Currency]         VARCHAR (4)  NOT NULL,
    [DataType]         VARCHAR (20) NOT NULL,
    [AccountId]        VARCHAR (42) NOT NULL,
    [AccountName]      VARCHAR (84) NOT NULL,
    [EnergyParent]     VARCHAR (42) NOT NULL,
    [Stream_Value_Cur] REAL         NULL,
    [Amount_MBtu]      REAL         NULL,
    [Amount_GJ]        REAL         NULL,
    [CostPerMBtu]      REAL         NULL,
    [CostPerGJ]        REAL         NULL,
    [CostkWh]          REAL         NULL,
    [DivisorValue]     REAL         NULL,
    CONSTRAINT [PK_EnergyCalc] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Currency] ASC, [DataType] ASC, [AccountId] ASC) WITH (FILLFACTOR = 70)
);

