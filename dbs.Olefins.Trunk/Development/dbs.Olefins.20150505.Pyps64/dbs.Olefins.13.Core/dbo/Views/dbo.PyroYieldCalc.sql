﻿







CREATE View [dbo].[PyroYieldCalc] AS
SELECT pyca.Refnum, pyca.ScenarioId, pyca.OpCondId, pyca.SimModelId, pyca.CurrencyRpt, c.DataType
, PyroPV_USDperMT = pyca._Ann_Calc_Amount_Cur
, PyroPVPcntSOA = soa.PyroVal_Pcnt
, c.H2, c.CH4, c.C2H2,c.C2H4, c.C2H6 , c.C3H4, c.C3H6, c.C3H8, c.C4H6, c.C4H8, c.C4H10
, OthHVChem = ISNULL(c.H2,0)+ISNULL(c.C2H2,0)+ISNULL(c.C4H6,0)+ISNULL(c.C6H6,0)
, c.C5S, c.C6H6, c.C6C8NA, c.C7H8, c.C8H8, c.C8H10, c.PyroGasoline, c.PyroGasOil, c.PyroFuelOil, c.Inerts, c.Loss, c.Tot
, yir.C2H4YIR, yir.C3H6YIR, yir.ProdOlefinsYIR, yir.ProdHVCYIR
, FreshPyroFeedkMT = d.FreshPyroFeed_kMT
, EthyleneProd_kMT = d.EthyleneProd_kMT
, HvcProd_kMT = HvcProd_kMT
FROM calc.PricingYieldCompositionAggregate pyca 
INNER JOIN Divisors d on d.Refnum=pyca.Refnum
INNER JOIN dbo.TSort t on t.Refnum=pyca.Refnum and pyca.FactorSetId = t.FactorSetId
LEFT JOIN calc.PricingYieldComposition_SOA soa on soa.Refnum=pyca.Refnum and soa.FactorSetId=pyca.FactorSetId and soa.ScenarioId=pyca.ScenarioId and soa.CurrencyRpt=pyca.CurrencyRpt and soa.SimModelId=pyca.SimModelId and soa.OpCondId=pyca.OpCondId
LEFT JOIN dbo.PyroYieldComposition c       ON c.Refnum = pyca.Refnum AND c.OpCondId = pyca.OpCondId AND c.SimModelId = pyca.SimModelId
LEFT JOIN dbo.PyroYieldImprovementRatios yir ON yir.Refnum = pyca.Refnum AND yir.FactorSetId = pyca.FactorSetId AND yir.SimModelId = pyca.SimModelId AND yir.OpCondId = pyca.OpCondId







