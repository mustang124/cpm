﻿
CREATE VIEW [Ranking].[RankSummary]
AS
SELECT lu.ListId, 
lu.ListName, 
lu.RankBreak, 
lu.BreakValue, 
lu.RankVariable, 
s.LastTime, 
s.DataCount, 
s.NumTiles, 
s.Top2, 
s.Tile1Break, 
s.Tile2Break, 
s.Tile3Break, 
s.Btm2, 
lu.RankSpecsID
FROM Ranking.RankSpecsLu lu INNER JOIN Ranking.RankSpecsStats s ON s.RankSpecsID = lu.RankSpecsID

