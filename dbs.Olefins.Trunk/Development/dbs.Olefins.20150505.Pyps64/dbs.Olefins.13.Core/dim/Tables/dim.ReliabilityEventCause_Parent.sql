﻿CREATE TABLE [dim].[ReliabilityEventCause_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [EventCauseId]   VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_ReliabilityEventCause_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityEventCause_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityEventCause_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityEventCause_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_ReliabilityEventCause_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityEventCause_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EventCauseId] ASC),
    CONSTRAINT [CR_ReliabilityEventCause_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ReliabilityEventCause_Parent_LookUp_EventCause] FOREIGN KEY ([EventCauseId]) REFERENCES [dim].[ReliabilityEventCause_LookUp] ([EventCauseId]),
    CONSTRAINT [FK_ReliabilityEventCause_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ReliabilityEventCause_LookUp] ([EventCauseId]),
    CONSTRAINT [FK_ReliabilityEventCause_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ReliabilityEventCause_Parent] ([FactorSetId], [EventCauseId]),
    CONSTRAINT [FK_ReliabilityEventCause_ParentFactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityEventCause_Parent_u]
ON [dim].[ReliabilityEventCause_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityEventCause_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityEventCause_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityEventCause_Parent].[EventCauseId]		= INSERTED.[EventCauseId];

END;