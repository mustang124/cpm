﻿CREATE TABLE [dim].[ReliabilityPhilosophy_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [PhilosophyId]       CHAR (1)            NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       CHAR (1)            NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ReliabilityPhilosophy_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityPhilosophy_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPhilosophy_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPhilosophy_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPhilosophy_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityPhilosophy_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PhilosophyId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ReliabilityPhilosophy_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ReliabilityPhilosophy_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ReliabilityPhilosophy_LookUp] ([PhilosophyId]),
    CONSTRAINT [FK_ReliabilityPhilosophy_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityPhilosophy_Bridge_LookUp_Philosophy] FOREIGN KEY ([PhilosophyId]) REFERENCES [dim].[ReliabilityPhilosophy_LookUp] ([PhilosophyId]),
    CONSTRAINT [FK_ReliabilityPhilosophy_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [PhilosophyId]) REFERENCES [dim].[ReliabilityPhilosophy_Parent] ([FactorSetId], [PhilosophyId]),
    CONSTRAINT [FK_ReliabilityPhilosophy_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ReliabilityPhilosophy_Parent] ([FactorSetId], [PhilosophyId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityPhilosophy_Bridge_u]
ON [dim].[ReliabilityPhilosophy_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityPhilosophy_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityPhilosophy_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityPhilosophy_Bridge].[PhilosophyId]		= INSERTED.[PhilosophyId]
		AND	[dim].[ReliabilityPhilosophy_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;