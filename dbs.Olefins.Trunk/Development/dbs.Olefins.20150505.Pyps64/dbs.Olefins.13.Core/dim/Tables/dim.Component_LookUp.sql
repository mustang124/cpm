﻿CREATE TABLE [dim].[Component_LookUp] (
    [ComponentId]     VARCHAR (42)       NOT NULL,
    [ComponentName]   NVARCHAR (84)      NOT NULL,
    [ComponentDetail] NVARCHAR (256)     NOT NULL,
    [Carbon_Count]    TINYINT            NULL,
    [Hydrogen_Count]  TINYINT            NULL,
    [wtMolar]         REAL               NULL,
    [Moles]           REAL               NULL,
    [Density_SG]      REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_Component_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_Component_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_Component_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_Component_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Component_LookUp] PRIMARY KEY CLUSTERED ([ComponentId] ASC),
    CONSTRAINT [CL_Component_LookUp_ComponentDetail] CHECK ([ComponentDetail]<>''),
    CONSTRAINT [CL_Component_LookUp_ComponentId] CHECK ([ComponentId]<>''),
    CONSTRAINT [CL_Component_LookUp_ComponentName] CHECK ([ComponentName]<>''),
    CONSTRAINT [CR_Component_Carbon_Count] CHECK ([Carbon_Count]>=(0)),
    CONSTRAINT [CR_Component_Density_SG] CHECK ([Density_SG]>=(0)),
    CONSTRAINT [CR_Component_Hydrogen_Count] CHECK ([Hydrogen_Count]>=(0)),
    CONSTRAINT [CR_Component_Moles] CHECK ([Moles]>=(0)),
    CONSTRAINT [CR_Component_wtMolar] CHECK ([wtMolar]>=(0)),
    CONSTRAINT [UK_Component_LookUp_ComponentDetail] UNIQUE NONCLUSTERED ([ComponentDetail] ASC),
    CONSTRAINT [UK_Component_LookUp_ComponentName] UNIQUE NONCLUSTERED ([ComponentName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Component_LookUp_u]
	ON [dim].[Component_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Component_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Component_LookUp].[ComponentId]		= INSERTED.[ComponentId];
		
END;
