﻿CREATE TABLE [ante].[EmissionsFactorNitrousOxide] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [EmissionsId]    VARCHAR (42)       NOT NULL,
    [N2O_TBtu]       REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsFactorNitrousOxide_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorNitrousOxide_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorNitrousOxide_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorNitrousOxide_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EmissionsFactorNitrousOxide] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EmissionsId] ASC),
    CONSTRAINT [FK_EmissionsFactorNitrousOxide_Emissions_LookUp] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsFactorNitrousOxide_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_EmissionsFactorNitrousOxide_u]
	ON [ante].[EmissionsFactorNitrousOxide]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EmissionsFactorNitrousOxide]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EmissionsFactorNitrousOxide].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[EmissionsFactorNitrousOxide].EmissionsId	= INSERTED.EmissionsId;

END;