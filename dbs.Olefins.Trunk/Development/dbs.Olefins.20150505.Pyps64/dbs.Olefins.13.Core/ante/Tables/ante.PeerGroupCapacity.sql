﻿CREATE TABLE [ante].[PeerGroupCapacity] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [PeerGroup]      TINYINT            NOT NULL,
    [LimitLower_kMT] REAL               NOT NULL,
    [LimitUpper_kMT] REAL               NOT NULL,
    [_LimitRange]    AS                 (((('['+CONVERT([varchar](32),round([LimitLower_kMT],(2)),(0)))+', ')+CONVERT([varchar](32),round([LimitUpper_kMT],(2)),(0)))+')') PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupCapacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupCapacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupCapacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupCapacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PeerGroupCapacity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PeerGroup] ASC),
    CONSTRAINT [CV_PeerGroupCapacity_Limit] CHECK ([LimitLower_kMT]<[LimitUpper_kMT]),
    CONSTRAINT [FK_PeerGroupCapacity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_PeerGroupCapacity_u]
	ON [ante].[PeerGroupCapacity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PeerGroupCapacity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PeerGroupCapacity].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[PeerGroupCapacity].PeerGroup	= INSERTED.PeerGroup;

END;