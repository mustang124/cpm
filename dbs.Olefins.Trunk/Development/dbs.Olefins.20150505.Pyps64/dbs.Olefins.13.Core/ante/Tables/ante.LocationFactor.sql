﻿CREATE TABLE [ante].[LocationFactor] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CountryId]      CHAR (3)           NOT NULL,
    [StateName]      VARCHAR (42)       NULL,
    [LocationFactor] REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_LocationFactor_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_LocationFactor_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_LocationFactor_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_LocationFactor_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [FK_LocationFactor_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_LocationFactor_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [UK_LocationFactor] UNIQUE CLUSTERED ([FactorSetId] ASC, [CountryId] ASC, [StateName] ASC)
);


GO

CREATE TRIGGER ante.t_LocationFactor_u
	ON [ante].[LocationFactor]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[LocationFactor]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[LocationFactor].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[LocationFactor].CountryId	= INSERTED.CountryId
		AND	[ante].[LocationFactor].StateName	= INSERTED.StateName;

END;