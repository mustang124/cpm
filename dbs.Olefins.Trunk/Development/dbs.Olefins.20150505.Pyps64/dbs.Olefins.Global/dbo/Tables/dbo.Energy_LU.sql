﻿CREATE TABLE [dbo].[Energy_LU] (
    [EnergyType]  CHAR (12)    NOT NULL,
    [TransType]   CHAR (6)     NULL,
    [SortKey]     TINYINT      NULL,
    [Description] VARCHAR (70) NULL,
    CONSTRAINT [PK___1__14] PRIMARY KEY CLUSTERED ([EnergyType] ASC)
);

