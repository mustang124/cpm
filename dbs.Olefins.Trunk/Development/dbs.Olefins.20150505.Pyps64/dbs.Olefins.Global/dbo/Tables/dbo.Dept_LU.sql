﻿CREATE TABLE [dbo].[Dept_LU] (
    [DeptID]   CHAR (3)     NOT NULL,
    [DeptDesc] VARCHAR (35) NULL,
    CONSTRAINT [PK___10__14] PRIMARY KEY CLUSTERED ([DeptID] ASC)
);

