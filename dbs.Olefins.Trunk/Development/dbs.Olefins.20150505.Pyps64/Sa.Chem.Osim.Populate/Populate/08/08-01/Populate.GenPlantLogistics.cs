﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void GenPlantLogistics(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			const int c = 8;

			string streamId;

			Dictionary<string, int> genLogistics = GenPlantLogisticsRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "8-1"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_GenPlantLogistics]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));

								if (genLogistics.TryGetValue(streamId, out r))
								{
									AddRangeValues(wks, r, c, rdr, "Amount_Cur");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "GenPlantLogistics", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_GenPlantLogistics]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> GenPlantLogisticsRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("FreshPyroFeed", 29);
			d.Add("Propylene", 32);
			d.Add("Ethylene", 35);

			return d;
		}
	}
}