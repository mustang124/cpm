﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void ApcControllers(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string apcId;
			RangeReference rr;

			Dictionary<string, RangeReference> apcController = ApcControllerDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_ApcControllers]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								apcId = rdr.GetString(rdr.GetOrdinal("ApcId"));

								if (apcController.TryGetValue(apcId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;
									c = rr.col;
									AddRangeValues(wks, r, c, rdr, "Controller_Count");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "ApcControllers", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_ApcControllers]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> ApcControllerDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("ApcPyroFurnIndivid", new RangeReference(T09_03, 44, 9));
			d.Add("ApcFurnFeed", new RangeReference(T09_03, 45, 9));
			d.Add("ApcTowerQuench", new RangeReference(T09_03, 46, 9));
			d.Add("ApcTowerComp", new RangeReference(T09_03, 47, 9));
			d.Add("ApcOtherOnSite", new RangeReference(T09_03, 48, 9));
			d.Add("ApcTowerDepropDebut", new RangeReference(T09_03, 49, 9));
			d.Add("ApcTowerPropylene", new RangeReference(T09_03, 50, 9));
			d.Add("ApcSteamHeat", new RangeReference(T09_03, 51, 9));
			d.Add("ApcTowerOther", new RangeReference(T09_03, 52, 9));

			return d;
		}
	}
}