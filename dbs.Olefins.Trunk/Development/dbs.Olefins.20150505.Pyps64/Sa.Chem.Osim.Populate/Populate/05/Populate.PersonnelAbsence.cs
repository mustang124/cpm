﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void PersonnelAbsence(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string persIdPri;
			string wksName;

			Dictionary<string, string> persPri = PersPriDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_PersonnelAbsence]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								persIdPri = rdr.GetString(rdr.GetOrdinal("PersIdPri"));

								if (persPri.TryGetValue(persIdPri, out wksName))
								{
									wks = wkb.Worksheets[tabNamePrefix + wksName];

									if (persIdPri == "Occ")
									{
										AddRangeValues(wks, 42, 6, rdr, "Absence_Pcnt", 100.0);
									}
									else
									{
										AddRangeValues(wks, 44, 5, rdr, "Absence_Pcnt", 100.0);
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "Personnel", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_Personnel]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}