﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MaintPers(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string accountId;
			RangeReference rr;

			Dictionary<string, RangeReference> maintPers = MaintPersDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MaintPers]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("PersId"));

								if (maintPers.TryGetValue(accountId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;
									c = rr.col;
									AddRangeValues(wks, r, c, rdr, "Company_Hrs");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MaintOpExAggregate", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MaintOpExAggregate]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> MaintPersDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("OccMaint", new RangeReference(T10_02, 20, 7));
			d.Add("MpsMaint", new RangeReference(T10_02, 22, 7));

			return d;
		}
	}
}