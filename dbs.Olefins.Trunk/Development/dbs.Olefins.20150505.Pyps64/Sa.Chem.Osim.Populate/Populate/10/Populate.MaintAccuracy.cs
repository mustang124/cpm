﻿using System;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MaintAccuracy(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			const int r = 63;
			const int c = 4;

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + T10_01];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MaintAccuracy]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								AddRangeValues(wks, r, c, rdr, "AccuracyID");
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MaintAccuracy", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MaintAccuracy]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}