﻿CREATE TABLE [dbo].[ReportQueue] (
    [RptSetID]      INT         NOT NULL,
    [TimeSubmitted] DATETIME    CONSTRAINT [DF_ReportQueu_TimeSubmitt1__13] DEFAULT (getdate()) NOT NULL,
    [WhoSubmitted]  VARCHAR (5) NULL
);

