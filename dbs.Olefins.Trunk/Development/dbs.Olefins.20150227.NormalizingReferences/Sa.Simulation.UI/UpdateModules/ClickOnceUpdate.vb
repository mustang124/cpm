'---------------------------------------------------------------------------------------
' � 2011 HSB Solomon Associates LLC
' Solomon Associates
' Two Galleria Tower, Suite 1500
' 13455 Noel Road
' Dallas, Texas 75240, United States of America
'---------------------------------------------------------------------------------------

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports System.Deployment.Application
Imports System.Diagnostics
Imports System.Windows.Forms

Imports Microsoft.VisualBasic

<System.Runtime.Remoting.Contexts.Synchronization(True)> _
Friend Class ClickOnceUpdate

    <System.ThreadStatic()> Private WithEvents ADUpdateAsync As ApplicationDeployment
    <System.ThreadStatic()> Friend pnlControls As Panel
    <System.ThreadStatic()> Private strAppLocation As String = System.Reflection.Assembly.GetExecutingAssembly.Location.ToString()

    Const sClose As String = "Close"
    Const sCancel As String = "Cancel"
    Const sUpdate As String = "Update"
    Const sRestart As String = "Restart"

    Friend Sub CheckForUpdate()

        Me.btnUpdateRestart.Text = sUpdate
        Me.btnCancelClose.Text = sCancel

        Me.lblUpdate.Visible = False
        Me.pBarUpdate.Visible = False

        enableButtons(Me.btnUpdateRestart, False)
        enableButtons(Me.btnCancelClose, True)

        Me.BringToFront() : Me.Visible = True
        If Not Me.pnlControls Is Nothing Then Me.pnlControls.Visible = False

        If (ApplicationDeployment.IsNetworkDeployed) Then
            ADUpdateAsync = ApplicationDeployment.CurrentDeployment
            ADUpdateAsync.CheckForUpdateAsync()
        Else
            Me.MSIUpdate()
        End If

    End Sub

    Private Sub btnUpdateRestart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateRestart.Click

        Select Case Me.btnUpdateRestart.Text
            Case sUpdate

                If (ApplicationDeployment.IsNetworkDeployed) Then

                    Me.lblUpdate.Visible = True
                    Me.pBarUpdate.Visible = True

                    enableButtons(Me.btnUpdateRestart, False)

                    Me.btnCancelClose.Text = sCancel
                    Me.toolTip.SetToolTip(Me.btnCancelClose, "Cancel Update")

                    Me.lblUpdate.Text = "Downloading new version..."

                    ADUpdateAsync.UpdateAsync()

                Else
                    enableButtons(Me.btnUpdateRestart, True)
                End If

            Case sRestart

                System.Windows.Forms.Application.Restart()

        End Select

    End Sub

    Private Sub btnCancelClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelClose.Click

        Select Case Me.btnCancelClose.Text
            Case sCancel

                If (ApplicationDeployment.IsNetworkDeployed) Then
                    ADUpdateAsync.CheckForUpdateAsyncCancel()
                    ADUpdateAsync.UpdateAsyncCancel()
                    ApplicationDeployment.CurrentDeployment.CheckForUpdateAsyncCancel()
                    ApplicationDeployment.CurrentDeployment.UpdateAsyncCancel()
                Else
                End If

            Case sClose
                If Not Me.pnlControls Is Nothing Then Me.pnlControls.BringToFront() : Me.pnlControls.Visible = True
                Me.Visible = False

        End Select

    End Sub

    Private Sub UpdateCancel()

        MessageBox.Show("The update was cancelled by the user.", My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Me.txtStatus.Text = "The update was cancelled."

        Me.lblUpdate.Visible = False
        Me.pBarUpdate.Visible = False

        enableButtons(Me.btnUpdateRestart, True)

        Me.btnCancelClose.Text = sClose

    End Sub

#Region " MSI Update "

    Private Sub MSIUpdate()

        Me.txtVersionInstalled.Text = FileVersionInfo.GetVersionInfo(strAppLocation).ProductVersion.ToString()

        Me.txtVersionAvailable.Text = "N/A"
        Me.txtDownloadSize.Text = "N/A"
        Me.txtStatus.Text = "This Application Cannot be Updated with ClickOnce"
        Me.btnCancelClose.Text = sClose

    End Sub

#End Region

#Region " Async Update "

    Private Sub ADUpdateAsync_CheckForUpdateProgressChanged(ByVal sender As Object, ByVal e As DeploymentProgressChangedEventArgs) Handles ADUpdateAsync.CheckForUpdateProgressChanged
    End Sub

    Private Sub ADUpdateAsync_CheckForUpdateCompleted(ByVal sender As Object, ByVal e As CheckForUpdateCompletedEventArgs) Handles ADUpdateAsync.CheckForUpdateCompleted

        If (e.Error IsNot Nothing) Then MessageBox.Show("ERROR: Could not retrieve new version of the application. Reason: " + ControlChars.Lf + e.Error.Message + ControlChars.Lf + "Please report this error to the system administrator.", My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error) : Exit Sub
        If (e.Cancelled) Then UpdateCancel()

        Me.txtVersionInstalled.Text = My.Application.Deployment.CurrentVersion.ToString()

        ' Ask the user if (s)he would like to update the application now.
        If (e.UpdateAvailable) Then

            If (Not e.IsUpdateRequired) Then

                Me.txtVersionAvailable.Text = e.AvailableVersion.ToString()
                Me.txtDownloadSize.Text = sizeConv(e.UpdateSizeBytes)
                Me.txtStatus.Text = "A new version is available."
                enableButtons(Me.btnUpdateRestart, True)

            Else

                Me.txtStatus.Text = "A mandatory version is being installed."

                MessageBox.Show("A mandatory update is available for your application." & ControlChars.Lf & _
                                "We will install the update now, after which we will " & ControlChars.Lf & _
                                "save all of your in-progress data and restart your application.", My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                ADUpdateAsync.UpdateAsync()

            End If

        Else

            Me.txtVersionAvailable.Text = Me.txtVersionInstalled.Text
            Me.txtDownloadSize.Text = "0.0 KB"
            Me.txtStatus.Text = "You are using the current version."

        End If

        Me.btnCancelClose.Text = sClose
        Me.toolTip.SetToolTip(Me.btnCancelClose, "Close Update")

        enableButtons(Me.btnCancelClose, True)

    End Sub

    Private Sub ADUpdateAsync_UpdateProgressChanged(ByVal sender As Object, ByVal e As DeploymentProgressChangedEventArgs) Handles ADUpdateAsync.UpdateProgressChanged

        Me.lblUpdate.Text = sizeConv(e.BytesCompleted) & " of " & sizeConv(e.BytesTotal) & " downloaded - " & e.ProgressPercentage.ToString() & "% complete"
        Me.pBarUpdate.Value = e.ProgressPercentage

    End Sub

    Private Sub ADUpdateAsync_UpdateCompleted(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs) Handles ADUpdateAsync.UpdateCompleted

        If (e.Error IsNot Nothing) Then MessageBox.Show("ERROR: Could not install the latest version of the application. Reason: " + ControlChars.Lf + e.Error.Message + ControlChars.Lf + "Please report this error to the system administrator.", My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error) : Exit Sub

        If (e.Cancelled) Then
            UpdateCancel()
        Else
            Me.txtStatus.Text = "Finished downloading. Please restart or close the application."

            enableButtons(Me.btnUpdateRestart, True)

            Me.btnUpdateRestart.Text = sRestart
            Me.toolTip.SetToolTip(Me.btnUpdateRestart, "Close and Restart " & Application.ProductName)

            Me.btnCancelClose.Text = sClose
            Me.toolTip.SetToolTip(Me.btnCancelClose, "Close Update")

        End If

    End Sub

    Private Sub ADUpdateAsync_DownloadFileGroupProgressChanged(ByVal sender As Object, ByVal e As DeploymentProgressChangedEventArgs) Handles ADUpdateAsync.DownloadFileGroupProgressChanged
    End Sub

    Private Sub ADUpdateAsync_DownloadFileGroupCompleted(ByVal sender As Object, ByVal e As DownloadFileGroupCompletedEventArgs) Handles ADUpdateAsync.DownloadFileGroupCompleted
    End Sub

#End Region

    Private Function sizeConv(ByVal bytesFile As Double) As String

        sizeConv = "unknown"

        Dim bytesTest As Double = bytesFile * 1
        Dim bytesConv As Double = bytesFile
        Dim strUnits As String = " bytes"

        If ((bytesTest > 1024) And (bytesFile <= 1048576)) Then bytesConv = bytesFile / 1024 : strUnits = " KB"
        If ((bytesTest > 1048576) And (bytesFile <= 1073741824)) Then bytesConv = bytesFile / 1048576 : strUnits = " MB"
        If ((bytesTest > 1073741824) And (bytesFile <= 1099511627776)) Then bytesConv = bytesFile / 1073741824 : strUnits = " GB"
        If (bytesTest > 1099511627776) Then bytesConv = bytesFile / 1099511627776 : strUnits = " TB"

        sizeConv = System.Math.Round(bytesConv, 2).ToString() & strUnits

    End Function

    Private Sub enableButtons(ByVal btn As System.Windows.Forms.Button, ByVal btnEnabled As Boolean)

        If btnEnabled Then
            btn.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Else
            btn.FlatAppearance.BorderColor = System.Drawing.Color.White
        End If

        btn.Enabled = btnEnabled

    End Sub

    Private Sub ClickOnceUpdate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.toolTip.SetToolTip(Me.btnUpdateRestart, "Update " & Application.ProductName)
        Me.toolTip.SetToolTip(Me.btnCancelClose, "Close Update")

        Me.txtLocation.Text = strAppLocation
        Me.lblCopyright.Text = System.Diagnostics.FileVersionInfo.GetVersionInfo(strAppLocation).LegalCopyright.ToString()
        Me.lblTrademark.Text = System.Diagnostics.FileVersionInfo.GetVersionInfo(strAppLocation).LegalTrademarks.ToString()

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

    End Sub

End Class
