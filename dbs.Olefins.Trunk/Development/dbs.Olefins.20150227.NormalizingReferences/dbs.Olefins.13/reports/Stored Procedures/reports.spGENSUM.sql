﻿






CREATE              PROC [reports].[spGENSUM](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint

DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DECLARE @Description varchar(100)
SELECT @Description = g.ReportGroupName FROM reports.ReportGroups g where GroupId = @GroupId 

DECLARE @FullText varchar(100), @Line2 varchar(20), @Line3 varchar(20), @Line4 varchar(20), @StudyYear int
DECLARE @RegionText varchar(100), @CapGroupText varchar(100), @EDCGroupText varchar(100), @FeedClassText varchar(100), @TechClassText varchar(100)

select @StudyYear = MAX(g.StudyYear) from dbo.GENSUM g JOIN @MyGroup m on m.Refnum = g.Refnum

EXEC reports.spWhatsThereOverrides @GroupId=@GroupId, @Property='Region', @PropertySet='aaa', @StudyYear=@StudyYear, 
	@MinForAllExcept = 3, @CountForAllExcept = 1, @ValuesPerLine = 2, @Lines = 1, 
	@FullText = @FullText OUTPUT, @Line1 = @RegionText OUTPUT, @Line2 = @Line2 OUTPUT, @Line3 = @Line3 OUTPUT, @Line4 = @Line4 OUTPUT
	
EXEC reports.spWhatsThereOverrides @GroupId=@GroupId, @Property='CapGroup', @PropertySet='aaa', @StudyYear=@StudyYear, 
	@MinForAllExcept = 3, @CountForAllExcept = 1, @ValuesPerLine = 2, @Lines = 1, 
	@FullText = @FullText OUTPUT, @Line1 = @CapGroupText OUTPUT, @Line2 = @Line2 OUTPUT, @Line3 = @Line3 OUTPUT, @Line4 = @Line4 OUTPUT
	
EXEC reports.spWhatsThereOverrides @GroupId=@GroupId, @Property='EDCGroup', @PropertySet='aaa', @StudyYear=@StudyYear, 
	@MinForAllExcept = 3, @CountForAllExcept = 1, @ValuesPerLine = 2, @Lines = 1, 
	@FullText = @FullText OUTPUT, @Line1 = @EDCGroupText OUTPUT, @Line2 = @Line2 OUTPUT, @Line3 = @Line3 OUTPUT, @Line4 = @Line4 OUTPUT
	
EXEC reports.spWhatsThereOverrides @GroupId=@GroupId, @Property='FeedClass', @PropertySet='aaa', @StudyYear=@StudyYear, 
	@MinForAllExcept = 3, @CountForAllExcept = 1, @ValuesPerLine = 2, @Lines = 1, 
	@FullText = @FullText OUTPUT, @Line1 = @FeedClassText OUTPUT, @Line2 = @Line2 OUTPUT, @Line3 = @Line3 OUTPUT, @Line4 = @Line4 OUTPUT
	
EXEC reports.spWhatsThereOverrides @GroupId=@GroupId, @Property='TechClass', @PropertySet='aaa', @StudyYear=@StudyYear, 
	@MinForAllExcept = 3, @CountForAllExcept = 1, @ValuesPerLine = 2, @Lines = 1, 
	@FullText = @FullText OUTPUT, @Line1 = @TechClassText OUTPUT, @Line2 = @Line2 OUTPUT, @Line3 = @Line3 OUTPUT, @Line4 = @Line4 OUTPUT

DELETE FROM reports.GENSUM WHERE GroupId = @GroupId
Insert into reports.GENSUM (GroupId
, NumPlants
, Description
, StudyYear
, Region
, CapGroup
, EDCGroup
, FeedClass
, FeedSubClass
, TechClass
, CogenGroup
, kEdc
, kUEdc
, EthyleneDiv_kMT
, PropyleneDiv_kMT
, OlefinsDiv_kMT
, EthyleneProd_kMT
, PropyleneProd_kMT
, OlefinsProd_kMT
, HvcProd_kMT
, TotalPlantInput_kMT
, PlantFeed_kMT
, FreshPyroFeed_kMT
, EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT
, EthyleneCapYE_kMT
, PropyleneCapYE_kMT
, OlefinsCapYE_kMT
, EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT
, RV_MUS
, RVUSGC_MUS
, TAAdj_kUEDC
, EthyleneCapUtil_pcnt
, OlefinsCapUtil_pcnt
, EthyleneCapUtilTAAdj_pcnt
, OlefinsCapUtilTAAdj_pcnt
, EthyleneCpbyUtil_pcnt
, OlefinsCpbyUtil_pcnt
, EthyleneCpbyUtilTAAdj_pcnt
, OlefinsCpbyUtilTAAdj_pcnt
, NetEnergyCons_MBtuperUEDC
, NetEnergyCons_BTUperHVCLb
, NetEnergyCons_GJperHVCMt
, EII
, EEI_SPSL
, EEI_PYPS
, TotPersEDC
, TotPersHVCMt
, PEI
, nmPEI
, mPEI
, PersCost_CurrPerEDC
, PersCost_CurrPerHVC_MT
, MaintIndexEDC
, MaintIndexRV
, MEI
, RelInd
, MechAvail
, OpAvail
, PlantAvail
, MechAvailSlow
, OpAvailSlow
, PlantAvailSlow
, RMeiEDC
, RMeiRV
, C2H4_OSOP
, Chem_OSOP
, C2H4YIR_MS25_SPSL
, HVCProdYIR_MS25_SPSL
, C2H4YIR_OSOP_SPSL
, HVCProdYIR_OSOP_SPSL
, C2H4YIR_OS25_SPSL
, HVCProdYIR_OS25_SPSL
, SeverityIndex_SPSL
, C2H4YIR_MS25_PYPS
, HVCProdYIR_MS25_PYPS
, C2H4YIR_OSOP_PYPS
, HVCProdYIR_OSOP_PYPS
, C2H4YIR_OS25_PYPS
, HVCProdYIR_OS25_PYPS
, SeverityIndex_PYPS
, ActPPV_PcntOS25_SPSL
, ActPPV_PcntMS25_SPSL
, ActPPV_PcntOS25_PYPS
, ActPPV_PcntMS25_PYPS
, PhysLoss_Pcnt
, NEOpExEDC
, NEI
, TotCashOpExUEDC
, TotRefExpUEDC
, NEOpExHVCLb
, TotCashOpExHVCLb
, TotRefExpHVCLb
, NEOpExHVCMt
, TotCashOpExHVCMt
, TotRefExpHVCMt
, EthyleneProdCostPerUEDC
, OlefinsProdCostPerUEDC
, HVCProdCostPerUEDC
, EthyleneProdCostPerMT
, OlefinsProdCostPerMT
, HVCProdCostPerMT
, EthyleneProdCentsPerLB
, OlefinsProdCentsPerLB
, HVCProdCentsPerLB
, NCMPerEthyleneMT
, NCMPerOlefinsMT
, NCMPerHVCMT
, NCMPerEthyleneLB
, NCMPerOlefinsLB
, NCMPerHVCLB
, GMPerRV
, ROI
, ROITAAdj
, GMPerRVUSGC
, NCMPerRVUSGC
, NCMPerRVUSGCTAAdj
, ProformaOpExPerUEDC
, ProformaEthyleneProdCostPerUEDC
, ProformaHVCProdCostPerUEDC
, ProformaOpExPerHVCMT
, ProformaProdCostPerEthyleneMT
, ProformaProdCostPerHVCMT
, ProformaNCMPerEthyleneMT
, ProformaNCMPerHVCMT
, ProformaROI
, ProformaOpExPerHVCLb
, ProformaProdCostPerEthyleneLb
, ProformaProdCostPerHVCLb
, ProformaNCMPerEthyleneLb
, ProformaNCMPerHVCLb
, APCImplIndex
, APCOnLineIndex
, EIIStd
, PEIStd
, nmPEIStd
, mPEIStd
, MEIStd
, NEIStd
, RegionText
, CapGroupText
, EDCGroupText
, FeedClassText
, TechClassText
)


SELECT GroupId = @GroupId
, NumPlants		= COUNT(*)
, Description	= CASE WHEN @Description IS NULL THEN MIN(g.CoLoc) ELSE @Description END
, StudyYear		= [$(DbGlobal)].dbo.WhatsThere(StudyYear)
, Region		= [$(DbGlobal)].dbo.WhatsThere(Region)
, CapGroup		= [$(DbGlobal)].dbo.WhatsThere(CapGroup)
, EDCGroup		= [$(DbGlobal)].dbo.WhatsThere(EDCGroup)
, FeedClass		= [$(DbGlobal)].dbo.WhatsThere(FeedClass)
, FeedSubClass		= [$(DbGlobal)].dbo.WhatsThere(FeedSubClass)
, TechClass		= [$(DbGlobal)].dbo.WhatsThere(TechClass)
, CogenGroup	= [$(DbGlobal)].dbo.WhatsThere(CoGenGroup)
, kEdc = [$(DbGlobal)].dbo.WtAvg(kEdc,1.0)
, kUEdc = [$(DbGlobal)].dbo.WtAvg(kUEdc,1.0)
, EthyleneDiv_kMT = [$(DbGlobal)].dbo.WtAvg(EthyleneDiv_kMT,1.0)
, PropyleneDiv_kMT = [$(DbGlobal)].dbo.WtAvg(PropyleneDiv_kMT,1.0)
, OlefinsDiv_kMT = [$(DbGlobal)].dbo.WtAvg(OlefinsDiv_kMT,1.0)
, EthyleneProd_kMT = [$(DbGlobal)].dbo.WtAvg(EthyleneProd_kMT,1.0)
, PropyleneProd_kMT = [$(DbGlobal)].dbo.WtAvg(PropyleneProd_kMT,1.0)
, OlefinsProd_kMT = [$(DbGlobal)].dbo.WtAvg(OlefinsProd_kMT,1.0)
, HvcProd_kMT = [$(DbGlobal)].dbo.WtAvg(HvcProd_kMT,1.0)
, TotalPlantInput_kMT = [$(DbGlobal)].dbo.WtAvg(TotalPlantInput_kMT,1.0)
, PlantFeed_kMT = [$(DbGlobal)].dbo.WtAvg(PlantFeed_kMT,1.0)
, FreshPyroFeed_kMT = [$(DbGlobal)].dbo.WtAvg(FreshPyroFeed_kMT,1.0)
, EthyleneCapAvg_kMT = [$(DbGlobal)].dbo.WtAvg(EthyleneCapAvg_kMT,1.0)
, PropyleneCapAvg_kMT = [$(DbGlobal)].dbo.WtAvg(PropyleneCapAvg_kMT,1.0)
, OlefinsCapAvg_kMT = [$(DbGlobal)].dbo.WtAvg(OlefinsCapAvg_kMT,1.0)
, EthyleneCapYE_kMT = [$(DbGlobal)].dbo.WtAvg(EthyleneCapYE_kMT,1.0)
, PropyleneCapYE_kMT = [$(DbGlobal)].dbo.WtAvg(PropyleneCapYE_kMT,1.0)
, OlefinsCapYE_kMT = [$(DbGlobal)].dbo.WtAvg(OlefinsCapYE_kMT,1.0)
, EthyleneCpbyAvg_kMT = [$(DbGlobal)].dbo.WtAvg(EthyleneCpbyAvg_kMT,1.0)
, PropyleneCpbyAvg_kMT = [$(DbGlobal)].dbo.WtAvg(PropyleneCpbyAvg_kMT,1.0)
, OlefinsCpbyAvg_kMT = [$(DbGlobal)].dbo.WtAvg(OlefinsCpbyAvg_kMT,1.0)
, RV_MUS = [$(DbGlobal)].dbo.WtAvg(RV_MUS,1.0)
, RVUSGC_MUS = [$(DbGlobal)].dbo.WtAvg(RVUSGC_MUS,1.0)
, TAAdj_kUEDC = [$(DbGlobal)].dbo.WtAvg(TAAdj_kUEDC,1.0)
, EthyleneCapUtil_pcnt = [$(DbGlobal)].dbo.WtAvg(EthyleneCapUtil_pcnt, EthyleneCapAvg_kMT)
, OlefinsCapUtil_pcnt = [$(DbGlobal)].dbo.WtAvg(OlefinsCapUtil_pcnt, OlefinsCapAvg_kMT)
, EthyleneCapUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(EthyleneCapUtilTAAdj_pcnt, EthyleneCapAvg_kMT)
, OlefinsCapUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(OlefinsCapUtilTAAdj_pcnt, OlefinsCapAvg_kMT)
, EthyleneCpbyUtil_pcnt = [$(DbGlobal)].dbo.WtAvg(EthyleneCpbyUtil_pcnt, EthyleneCpbyAvg_kMT)
, OlefinsCpbyUtil_pcnt = [$(DbGlobal)].dbo.WtAvg(OlefinsCpbyUtil_pcnt, OlefinsCpbyAvg_kMT)
, EthyleneCpbyUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(EthyleneCpbyUtilTAAdj_pcnt, EthyleneCpbyAvg_kMT)
, OlefinsCpbyUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(OlefinsCpbyUtilTAAdj_pcnt, OlefinsCpbyAvg_kMT)
, NetEnergyCons_MBtuperUEDC = [$(DbGlobal)].dbo.WtAvg(NetEnergyCons_MBtuperUEDC, kUEdc)
, NetEnergyCons_BTUperHVCLb = [$(DbGlobal)].dbo.WtAvg(NetEnergyCons_BTUperHVCLb, HvcProd_kMT)
, NetEnergyCons_GJperHVCMt = [$(DbGlobal)].dbo.WtAvg(NetEnergyCons_GJperHVCMt, HvcProd_kMT)
, EII = [$(DbGlobal)].dbo.WtAvg(g.EII, EII_StdEnergy)
, EEI_SPSL = [$(DbGlobal)].dbo.WtAvg(g.EEI_SPSL, EEI_SPSL_StdEnergy)
, EEI_PYPS = [$(DbGlobal)].dbo.WtAvg(g.EEI_PYPS, EEI_PYPS_StdEnergy)
, TotPersEDC = [$(DbGlobal)].dbo.WtAvg(TotPersEDC, kEdc)
, TotPersHVCMt = [$(DbGlobal)].dbo.WtAvg(TotPersHVCMt, HvcProd_kMT)
, PEI = [$(DbGlobal)].dbo.WtAvg(PEI, PEIStd)
, nmPEI = [$(DbGlobal)].dbo.WtAvg(nmPEI, nmPEIStd)
, mPEI = [$(DbGlobal)].dbo.WtAvg(mPEI, mPEIStd)
, PersCost_CurrPerEDC = [$(DbGlobal)].dbo.WtAvg(PersCost_CurrPerEDC, kEdc)
, PersCost_CurrPerHVC_MT = [$(DbGlobal)].dbo.WtAvg(PersCost_CurrPerHVC_MT, HvcProd_kMT)
, MaintIndexEDC = [$(DbGlobal)].dbo.WtAvg(MaintIndexEDC, kEdc)
, MaintIndexRV = [$(DbGlobal)].dbo.WtAvg(MaintIndexRV, RV_MUS)
, MEI = [$(DbGlobal)].dbo.WtAvg(MEI, MEIStd)
, RelInd = [$(DbGlobal)].dbo.WtAvg(RelInd, RV_MUS)
, MechAvail = [$(DbGlobal)].dbo.WtAvg(MechAvail, kEdc)
, OpAvail = [$(DbGlobal)].dbo.WtAvg(OpAvail, kEdc)
, PlantAvail = [$(DbGlobal)].dbo.WtAvg(PlantAvail, kEdc)
, MechAvailSlow = [$(DbGlobal)].dbo.WtAvg(MechAvailSlow, kEdc)
, OpAvailSlow = [$(DbGlobal)].dbo.WtAvg(OpAvailSlow, kEdc)
, PlantAvailSlow = [$(DbGlobal)].dbo.WtAvg(PlantAvailSlow, kEdc)
, RMeiEDC = [$(DbGlobal)].dbo.WtAvg(RMeiEDC, kEdc)
, RMeiRV = [$(DbGlobal)].dbo.WtAvg(RMeiRV, RV_MUS)
, C2H4_OSOP = [$(DbGlobal)].dbo.WtAvg(C2H4_OSOP, FreshPyroFeed_kMT)
, Chem_OSOP = [$(DbGlobal)].dbo.WtAvg(Chem_OSOP, FreshPyroFeed_kMT)
, C2H4YIR_MS25_SPSL = [$(DbGlobal)].dbo.WtAvg(C2H4YIR_MS25_SPSL, FreshPyroFeed_kMT)
, HVCProdYIR_MS25_SPSL = [$(DbGlobal)].dbo.WtAvg(HVCProdYIR_MS25_SPSL, HvcProd_kMT)
, C2H4YIR_OSOP_SPSL = [$(DbGlobal)].dbo.WtAvg(C2H4YIR_OSOP_SPSL, FreshPyroFeed_kMT)
, HVCProdYIR_OSOP_SPSL = [$(DbGlobal)].dbo.WtAvg(HVCProdYIR_OSOP_SPSL, HvcProd_kMT)
, C2H4YIR_OS25_SPSL = [$(DbGlobal)].dbo.WtAvg(C2H4YIR_OS25_SPSL, FreshPyroFeed_kMT)
, HVCProdYIR_OS25_SPSL = [$(DbGlobal)].dbo.WtAvg(HVCProdYIR_OS25_SPSL, HvcProd_kMT)
, SeverityIndex_SPSL = [$(DbGlobal)].dbo.WtAvg(SeverityIndex_SPSL, FreshPyroFeed_kMT)
, C2H4YIR_MS25_PYPS = [$(DbGlobal)].dbo.WtAvg(C2H4YIR_MS25_PYPS, FreshPyroFeed_kMT)
, HVCProdYIR_MS25_PYPS = [$(DbGlobal)].dbo.WtAvg(HVCProdYIR_MS25_PYPS, HvcProd_kMT)
, C2H4YIR_OSOP_PYPS = [$(DbGlobal)].dbo.WtAvg(C2H4YIR_OSOP_PYPS, FreshPyroFeed_kMT)
, HVCProdYIR_OSOP_PYPS = [$(DbGlobal)].dbo.WtAvg(HVCProdYIR_OSOP_PYPS, HvcProd_kMT)
, C2H4YIR_OS25_PYPS = [$(DbGlobal)].dbo.WtAvg(C2H4YIR_OS25_PYPS, FreshPyroFeed_kMT)
, HVCProdYIR_OS25_PYPS = [$(DbGlobal)].dbo.WtAvg(HVCProdYIR_OS25_PYPS, HvcProd_kMT)
, SeverityIndex_PYPS = [$(DbGlobal)].dbo.WtAvg(SeverityIndex_PYPS, FreshPyroFeed_kMT)
, ActPPV_PcntOS25_SPSL = [$(DbGlobal)].dbo.WtAvg(ActPPV_PcntOS25_SPSL, FreshPyroFeed_kMT)
, ActPPV_PcntMS25_SPSL = [$(DbGlobal)].dbo.WtAvg(ActPPV_PcntMS25_SPSL, FreshPyroFeed_kMT)
, ActPPV_PcntOS25_PYPS = [$(DbGlobal)].dbo.WtAvg(ActPPV_PcntOS25_PYPS, FreshPyroFeed_kMT)
, ActPPV_PcntMS25_PYPS = [$(DbGlobal)].dbo.WtAvg(ActPPV_PcntMS25_PYPS, FreshPyroFeed_kMT)
, PhysLoss_Pcnt = [$(DbGlobal)].dbo.WtAvg(PhysLoss_Pcnt, PlantFeed_kMT)
, NEOpExEDC = [$(DbGlobal)].dbo.WtAvg(NEOpExEDC, kEdc)
, NEI = [$(DbGlobal)].dbo.WtAvg(NEI, NEIStd)
, TotCashOpExUEDC = [$(DbGlobal)].dbo.WtAvg(TotCashOpExUEDC, kUEdc)
, TotRefExpUEDC = [$(DbGlobal)].dbo.WtAvg(TotRefExpUEDC, kUEdc)
, NEOpExHVCLb = [$(DbGlobal)].dbo.WtAvg(NEOpExHVCLb, HvcProd_kMT)
, TotCashOpExHVCLb = [$(DbGlobal)].dbo.WtAvg(TotCashOpExHVCLb, HvcProd_kMT)
, TotRefExpHVCLb = [$(DbGlobal)].dbo.WtAvg(TotRefExpHVCLb, HvcProd_kMT)
, NEOpExHVCMt = [$(DbGlobal)].dbo.WtAvg(NEOpExHVCMt, HvcProd_kMT)
, TotCashOpExHVCMt = [$(DbGlobal)].dbo.WtAvg(TotCashOpExHVCMt, HvcProd_kMT)
, TotRefExpHVCMt = [$(DbGlobal)].dbo.WtAvg(TotRefExpHVCMt, HvcProd_kMT)
, EthyleneProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(EthyleneProdCostPerUEDC, kUEdc)
, OlefinsProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(OlefinsProdCostPerUEDC, kUEdc)
, HVCProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(HVCProdCostPerUEDC, kUEdc)
, EthyleneProdCostPerMT = [$(DbGlobal)].dbo.WtAvg(EthyleneProdCostPerMT, EthyleneProd_kMT)
, OlefinsProdCostPerMT = [$(DbGlobal)].dbo.WtAvg(OlefinsProdCostPerMT, OlefinsProd_kMT)
, HVCProdCostPerMT = [$(DbGlobal)].dbo.WtAvg(HVCProdCostPerMT, HvcProd_kMT)
, EthyleneProdCentsPerLB = [$(DbGlobal)].dbo.WtAvg(EthyleneProdCentsPerLB, EthyleneProd_kMT)
, OlefinsProdCentsPerLB = [$(DbGlobal)].dbo.WtAvg(OlefinsProdCentsPerLB, OlefinsProd_kMT)
, HVCProdCentsPerLB = [$(DbGlobal)].dbo.WtAvg(HVCProdCentsPerLB, HvcProd_kMT)
, NCMPerEthyleneMT = [$(DbGlobal)].dbo.WtAvg(NCMPerEthyleneMT, EthyleneProd_kMT)
, NCMPerOlefinsMT = [$(DbGlobal)].dbo.WtAvg(NCMPerOlefinsMT, OlefinsProd_kMT)
, NCMPerHVCMT = [$(DbGlobal)].dbo.WtAvg(NCMPerHVCMT, HvcProd_kMT)
, NCMPerEthyleneLB = [$(DbGlobal)].dbo.WtAvg(NCMPerEthyleneLB, EthyleneProd_kMT)
, NCMPerOlefinsLB = [$(DbGlobal)].dbo.WtAvg(NCMPerOlefinsLB, OlefinsProd_kMT)
, NCMPerHVCLB = [$(DbGlobal)].dbo.WtAvg(NCMPerHVCLB, HvcProd_kMT)
, GMPerRV = [$(DbGlobal)].dbo.WtAvg(GMPerRV, RV_MUS)
, ROI = [$(DbGlobal)].dbo.WtAvg(ROI, RV_MUS)
, ROITAAdj = [$(DbGlobal)].dbo.WtAvg(ROITAAdj, RV_MUS)
, GMPerRVUSGC = [$(DbGlobal)].dbo.WtAvg(GMPerRVUSGC, RVUSGC_MUS)
, NCMPerRVUSGC = [$(DbGlobal)].dbo.WtAvg(NCMPerRVUSGC, RVUSGC_MUS)
, NCMPerRVUSGCTAAdj = [$(DbGlobal)].dbo.WtAvg(NCMPerRVUSGCTAAdj, RVUSGC_MUS)
, ProformaOpExPerUEDC = [$(DbGlobal)].dbo.WtAvg(ProformaOpExPerUEDC, kUEdc)
, ProformaEthyleneProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(ProformaEthyleneProdCostPerUEDC, kUEdc)
, ProformaHVCProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(ProformaHVCProdCostPerUEDC, kUEdc)
, ProformaOpExPerHVCMT = [$(DbGlobal)].dbo.WtAvg(ProformaOpExPerHVCMT, HvcProd_kMT)
, ProformaProdCostPerEthyleneMT = [$(DbGlobal)].dbo.WtAvg(ProformaProdCostPerEthyleneMT, EthyleneProd_kMT)
, ProformaProdCostPerHVCMT = [$(DbGlobal)].dbo.WtAvg(ProformaProdCostPerHVCMT, HvcProd_kMT)
, ProformaNCMPerEthyleneMT = [$(DbGlobal)].dbo.WtAvg(ProformaNCMPerEthyleneMT, EthyleneProd_kMT)
, ProformaNCMPerHVCMT = [$(DbGlobal)].dbo.WtAvg(ProformaNCMPerHVCMT, HvcProd_kMT)
, ProformaROI = [$(DbGlobal)].dbo.WtAvg(ProformaROI, RV_MUS)
, ProformaOpExPerHVCLb = [$(DbGlobal)].dbo.WtAvg(ProformaOpExPerHVCLb, HvcProd_kMT)
, ProformaProdCostPerEthyleneLb = [$(DbGlobal)].dbo.WtAvg(ProformaProdCostPerEthyleneLb, EthyleneProd_kMT)
, ProformaProdCostPerHVCLb = [$(DbGlobal)].dbo.WtAvg(ProformaProdCostPerHVCLb, HvcProd_kMT)
, ProformaNCMPerEthyleneLb = [$(DbGlobal)].dbo.WtAvg(ProformaNCMPerEthyleneLb, EthyleneProd_kMT)
, ProformaNCMPerHVCLb = [$(DbGlobal)].dbo.WtAvg(ProformaNCMPerHVCLb, HvcProd_kMT)
, APCImplIndex = [$(DbGlobal)].dbo.WtAvg(APCImplIndex, 1.0)
, APCOnLineIndex = [$(DbGlobal)].dbo.WtAvg(APCOnLineIndex, 1.0)
, EIIStd = [$(DbGlobal)].dbo.WtAvg(EIIStd, 1.0)
, PEIStd = [$(DbGlobal)].dbo.WtAvg(PEIStd, 1.0)
, nmPEIStd = [$(DbGlobal)].dbo.WtAvg(nmPEIStd, 1.0)
, mPEIStd = [$(DbGlobal)].dbo.WtAvg(mPEIStd, 1.0)
, MEIStd = [$(DbGlobal)].dbo.WtAvg(MEIStd, 1.0)
, NEIStd = [$(DbGlobal)].dbo.WtAvg(NEIStd, 1.0)
, RegionText = @RegionText
, CapGroupText = @CapGroupText
, EDCGroupText = @EDCGroupText
, FeedClassText = @FeedClassText
, TechClassText = @TechClassText
from @MyGroup r join dbo.GENSUM g on r.Refnum=g.Refnum
JOIN dbo.Energy e on e.Refnum=r.Refnum and e.DataType = 'EDC'

SET NOCOUNT OFF

















