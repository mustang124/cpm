﻿
CREATE FUNCTION [Ranking].[GetBreakValues](@ListId varchar(42), @RankBreak sysname)
RETURNS @BreakValues TABLE (BreakValue varchar(12) NOT NULL)
AS
BEGIN
	IF EXISTS (SELECT * FROM Ranking.RankBreaks WHERE RankBreak = @RankBreak)
	BEGIN
		IF @RankBreak = 'TotalList'
			INSERT @BreakValues (BreakValue) VALUES ('ALL')
		ELSE BEGIN 
			IF @RankBreak = 'UserGroup'
				INSERT @BreakValues (BreakValue)
				SELECT DISTINCT UserGroup
				FROM cons.RefList WHERE ListId = @ListId
			ELSE 
				INSERT @BreakValues (BreakValue)
				SELECT DISTINCT BreakValue
				FROM Ranking.RefineryBreakValues
				WHERE RankBreak = @RankBreak AND BreakValue IS NOT NULL
				AND Refnum IN (SELECT Refnum FROM cons.RefList WHERE ListId = @ListId)
			
			INSERT @BreakValues(BreakValue)
			SELECT BreakValue FROM Ranking.RankSpecsLu rs
			WHERE ListId = @ListId AND RankBreak = @RankBreak
			AND NOT EXISTS (SELECT * FROM @BreakValues x WHERE x.BreakValue = rs.BreakValue)
		END
	END
	RETURN
END

