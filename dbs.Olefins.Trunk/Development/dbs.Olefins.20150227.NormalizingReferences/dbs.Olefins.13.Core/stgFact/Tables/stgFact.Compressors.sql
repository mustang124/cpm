﻿CREATE TABLE [stgFact].[Compressors] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [EventNo]        TINYINT            NOT NULL,
    [Compressor]     VARCHAR (5)        NULL,
    [EventType]      VARCHAR (8)        NULL,
    [Duration]       REAL               NULL,
    [Component]      VARCHAR (5)        NULL,
    [Cause]          TINYINT            NULL,
    [EventYear]      INT                NULL,
    [Comments]       VARCHAR (MAX)      NULL,
    [CompLife]       TINYINT            NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Compressors_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Compressors_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Compressors_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Compressors_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Compressors] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EventNo] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Compressors_u]
	ON [stgFact].[Compressors]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Compressors]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Compressors].Refnum		= INSERTED.Refnum
		AND	[stgFact].[Compressors].EventNo		= INSERTED.EventNo;

END;