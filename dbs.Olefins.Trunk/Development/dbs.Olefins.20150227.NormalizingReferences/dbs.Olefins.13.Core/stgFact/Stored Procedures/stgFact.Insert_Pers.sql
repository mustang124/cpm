﻿CREATE PROCEDURE [stgFact].[Insert_Pers]
(
	@Refnum    VARCHAR (25),
	@PersId    VARCHAR (15),

	@SortKey   INT,

	@SectionID VARCHAR (4)  = NULL,
	@NumPers   REAL      = NULL,
	@STH       REAL      = NULL,
	@OvtHours  REAL      = NULL,
	@Contract  REAL      = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Pers]([Refnum], [PersId], [SortKey], [SectionID], [NumPers], [STH], [OvtHours], [Contract])
	VALUES(@Refnum, @PersId, @SortKey, @SectionID, @NumPers, @STH, @OvtHours, @Contract);

END;
