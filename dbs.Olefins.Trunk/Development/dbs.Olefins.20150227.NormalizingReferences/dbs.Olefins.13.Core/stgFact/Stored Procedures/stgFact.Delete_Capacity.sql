﻿CREATE PROCEDURE [stgFact].[Delete_Capacity]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Capacity]
	WHERE [Refnum] = @Refnum;

END;