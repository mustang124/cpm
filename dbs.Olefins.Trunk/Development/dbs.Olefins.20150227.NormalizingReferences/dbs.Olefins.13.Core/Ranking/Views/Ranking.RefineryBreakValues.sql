﻿


CREATE VIEW [Ranking].[RefineryBreakValues]
AS

SELECT g.Refnum, breaks.RankBreak, breaks.GensumField AS BreakValue /*  
	 = CAST(FeedClass as varchar(12)),  = CAST(StudyYear as varchar(12)),  = CAST(EDCGroup as varchar(12))*/
FROM dbo.GENSUM g CROSS APPLY (VALUES ('Region', CAST(Region as varchar(12)))
									, ('CapGroup', CAST(CapGroup as varchar(12)))
									, ('TechClass', CAST(TechClass as varchar(12)))
									, ('FeedClass', CAST(FeedClass as varchar(12)))
									, ('FeedSubClass', CAST(FeedSubClass as varchar(12)))
									, ('StudyYear', CAST(StudyYear as varchar(12)))
									, ('EDCGroup', CAST(EDCGroup as varchar(12)))
								) breaks(RankBreak, GensumField)



