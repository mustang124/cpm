﻿CREATE PROCEDURE [calc].[Insert_StandardEnergyRedCrackedGas]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		-------------------------------------------------------------------------------
		--	H360: Cracked Gas

		INSERT INTO [calc].[StandardEnergyRedCrackedGas]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId],
			[StreamId], [StreamDescription], [Stream_kMT], [Energy_MBtu], [StandardEnergy_MBtuDay])
		SELECT
			t.[FactorSetId],
			t.[Refnum],
			t.[Plant_AnnDateKey],
			'RedCrackedGasTrans',
			t.[StreamId],
			t.[StreamDescription],
			SUM(t.[Component_kMT])		[Component_kMT],
			SUM(t.[Energy_MBtu])		[Energy_MBtu],
			SUM(t.[Energy_MBtuDay])		[StandardEnergy_MBtuDay]
		FROM (
			SELECT
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey],
				cs.[StreamId],
				cs.[StreamDescription],
				cs.[Component_kMT]											[Component_kMT],
				cs.[Component_kMT] * sec.[Energy_BTUlb] * 2.2046			[Energy_MBtu],
				cs.[Component_kMT] * sec.[Energy_BTUlb] * 2.2046 / 365.0	[Energy_MBtuDay],
				CASE WHEN cs.[ComponentId] IN ('C2H4', 'C3H6') THEN cs.[Component_WtPcnt] END [EP_Component_WtPcnt]
			FROM @fpl									fpl
			INNER JOIN [calc].[CompositionStream]		cs
				ON	cs.[FactorSetId]	= fpl.[FactorSetId]
				AND	cs.[Refnum]			= fpl.[Refnum]
				AND	cs.[CalDateKey]		= fpl.[Plant_QtrDateKey]
				AND	cs.[StreamId]		= 'ProdOther'
				AND	cs.[ComponentId]	IN ('H2', 'CH4', 'C2H4', 'C2H6', 'C3H6', 'C3H8', 'C4H6', 'C4H8', 'C4H10', 'C6H6')
			INNER JOIN [ante].[StandardEnergyComponents]	sec
				ON	sec.[FactorSetId]	= fpl.[FactorSetId]
				AND	sec.[ComponentId]	= cs.[ComponentId]
			WHERE	fpl.[FactorSet_AnnDateKey] > 20130000
			) t
		GROUP BY
			t.[FactorSetId],
			t.[Refnum],
			t.[Plant_AnnDateKey],
			t.[StreamId],
			t.[StreamDescription]
		HAVING SUM(t.EP_Component_WtPcnt) > 20.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;