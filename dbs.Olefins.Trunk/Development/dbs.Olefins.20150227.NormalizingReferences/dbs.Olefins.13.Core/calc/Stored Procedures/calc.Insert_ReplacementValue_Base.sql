﻿CREATE PROCEDURE [calc].[Insert_ReplacementValue_Base]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplacementValue(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReplacementValueId, ReplacementValue)
		SELECT
			u.FactorSetId,
			u.Refnum,
			u.Plant_QtrDateKey,
			u.CurrencyRpt,
			u.ReplacementValueId,
			u.ReplacementValue
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey, 
				fpl.CurrencyRpt,
				f.Base_ISBL			[Base],
				f.FlexAdjustment	[FeedFlex],
				f.StartUpCost		[OwnerCostAndStartUp]
			FROM @fpl									fpl
			INNER JOIN calc.ReplValPyroFlexibility		f
				ON	f.FactorSetId = fpl.FactorSetId
				AND	f.Refnum = fpl.Refnum
				AND	f.CalDateKey = fpl.Plant_QtrDateKey
			WHERE	fpl.CalQtr = 4
			) p
		UNPIVOT (
			ReplacementValue FOR ReplacementValueId IN (
				p.Base,
				p.FeedFlex,
				p.OwnerCostAndStartUp
				) 
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;