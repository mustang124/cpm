﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonPlant_Flare]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonPlant(FactorSetId, Refnum, CalDateKey, EmissionsId, Quantity_kMT, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			c.StreamId				[EmissionsId],
			SUM(c.Component_kMT),
			ef.CEF_MTCO2_MBtu * 44.01 / 12.01 * 1000.0,
			SUM(c.Component_kMT) * ef.CEF_MTCO2_MBtu * 44.01 / 12.01 * 1000.0
		FROM @fpl									fpl
		INNER JOIN calc.CompositionStream			c
			ON	c.FactorSetId = fpl.FactorSetId
			AND	c.Refnum = fpl.Refnum
			AND	c.CalDateKey = fpl.Plant_QtrDateKey
			AND	c.SimModelId = 'Plant'
			AND	c.OpCondId = 'OSOP'
			AND	c.StreamId ='LossFlareVent'
		INNER JOIN calc.[PeerGroupFeedClass]					fc
			ON	fc.FactorSetId = fpl.FactorSetId
			AND	fc.Refnum = fpl.Refnum
			AND	fc.CalDateKey = fpl.Plant_AnnDateKey
		INNER JOIN ante.EmissionsFactorFlareLoss	ef
			ON	ef.FactorSetId = fpl.FactorSetId
			AND	ef.FeedClassID = fc.PeerGroup
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			c.StreamId,
			c.SimModelId,
			c.OpCondId,
			ef.FeedClassID,
			ef.CEF_MTCO2_MBtu;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;