﻿CREATE PROCEDURE [calc].[Insert_MarginSensitivity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.MarginSensitivity(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MarginAnalysis, MarginId,
			Amount_Cur, Amount_SensEDC, Amount_SensProd_MT, Amount_SensProd_Cents,
			TaAdj_Amount_Cur, TAAdj_Amount_SensEDC, TaAdj_Amount_SensProd_MT, TAAdj_Amount_SensProd_Cents)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			m.MarginAnalysis,
			m.MarginId,
			m.Amount_Cur,

			m.Amount_Cur / edc.kEdc * 1000.0 /
				CASE WHEN m.MarginId = 'STVol'
					THEN util._UtilizationStream_Pcnt / 100.0
					ELSE 1.0
					END				[Amount_Edc],

			m.Amount_Cur / prod.Production_kMT * 1000.0 *
				CASE WHEN m.MarginId IN ('STNonVol', 'STTaExp', 'NonCash')
					THEN util._UtilizationStream_Pcnt / 100.0
					ELSE 1.0
					END				[Amount_Prod],

			m.Amount_Cur / prod.Production_kMT * 100.0 / 2.2046 *
				CASE WHEN m.MarginId IN ('STNonVol', 'STTaExp', 'NonCash')
					THEN util._UtilizationStream_Pcnt / 100.0
					ELSE 1.0
					END				[Amount_Cent],

			m.TaAdj_Amount_Cur,

			m.TaAdj_Amount_Cur / edc.kEdc * 1000.0 /
				CASE WHEN m.MarginId = 'STVol'
					THEN util._UtilizationStream_Pcnt / 100.0
					ELSE 1.0
					END				[TAAdj_Amount_Edc],

			m.TaAdj_Amount_Cur / prod.Production_kMT * 1000.0 *
				CASE WHEN m.MarginId IN ('STNonVol', 'STTaExp', 'NonCash')
					THEN util._UtilizationStream_Pcnt / 100.0
					ELSE 1.0
					END				[TAAdj_Amount_Prod],

			m.TaAdj_Amount_Cur / prod.Production_kMT * 100.0 / 2.2046 *
				CASE WHEN m.MarginId IN ('STNonVol', 'STTaExp', 'NonCash')
					THEN util._UtilizationStream_Pcnt / 100.0
					ELSE 1.0
					END				[TAAdj_Amount_Cent]

		FROM @fpl									fpl
		INNER JOIN calc.MarginAggregate				m
			ON	m.FactorSetId		= fpl.FactorSetId
			AND	m.Refnum			= fpl.Refnum
			AND	m.CalDateKey		= fpl.Plant_QtrDateKey
			AND	m.CurrencyRpt		= fpl.CurrencyRpt
			AND	m.MarginId			IN ('STVol', 'STNonVol', 'STTaExp', 'MarginGross', 'PlantFeed', 'NonCash')
			AND m.MarginAnalysis <> 'None'

		LEFT OUTER JOIN calc.CapacityUtilization	util
			ON	util.FactorSetId	= fpl.FactorSetId
			AND	util.Refnum			= fpl.Refnum
			AND	util.CalDateKey		= fpl.Plant_QtrDateKey
			AND	util.SchedId		= 'A'
			AND util.StreamId		= 'Ethylene'

		INNER JOIN calc.EdcAggregate				edc
			ON	edc.FactorSetId		= fpl.FactorSetId
			AND	edc.Refnum			= fpl.Refnum
			AND	edc.CalDateKey		= fpl.Plant_QtrDateKey
			AND	edc.EdcId			= 'kEdcTot'

		LEFT OUTER JOIN ante.MapStreamComponent		map
			ON	map.FactorSetId		= fpl.FactorSetId
			AND	map.StreamId		= m.MarginAnalysis
			AND	map.StreamId		IN ('Ethylene', 'ProdHVC', 'ProdOlefins', 'Propylene')
		INNER JOIN calc.DivisorsProduction			prod
			ON	prod.FactorSetId	= fpl.FactorSetId
			AND	prod.Refnum			= fpl.Refnum
			AND	prod.CalDateKey		= fpl.Plant_QtrDateKey
			AND	prod.ComponentId	= map.ComponentId

		WHERE	fpl.CalQtr		= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END