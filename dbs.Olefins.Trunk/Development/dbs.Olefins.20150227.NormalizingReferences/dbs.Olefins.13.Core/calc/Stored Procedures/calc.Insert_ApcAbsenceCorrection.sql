﻿CREATE PROCEDURE calc.Insert_ApcAbsenceCorrection
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ApcAbsenceCorrection(FactorSetId, Refnum, CalDateKey, TowerDebutanizer, TowerDepropanizer, TowerPropylene)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			p.TowerDebutanizer,
			p.TowerDepropanizer,
			p.TowerPropylene
		FROM @fpl						tsq
		INNER JOIN (
			SELECT
				f.FactorSetId,
				f.Refnum,
				f.CalDateKey,
				f.FacilityId,
				f.Unit_Count
			FROM fact.FacilitiesCount f	WITH (NOEXPAND)
			WHERE	f.FacilityId IN ('TowerDebutanizer', 'TowerDepropanizer', 'TowerPropylene')
			)u
			PIVOT(
			MAX(u.Unit_Count) FOR u.FacilityId IN(
				TowerDebutanizer,
				TowerDepropanizer,
				TowerPropylene
				)
			)p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_AnnDateKey
		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;