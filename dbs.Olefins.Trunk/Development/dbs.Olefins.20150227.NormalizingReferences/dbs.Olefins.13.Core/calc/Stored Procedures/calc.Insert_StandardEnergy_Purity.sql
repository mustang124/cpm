﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Purity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			m.ModelId,
			k.StandardEnergyId,
			b.StreamId,
			k.Value * POWER(SUM(ABS(a.Tot_kMT) * c.Component_WtPcnt) / 100.0, e.Value)
		FROM @fpl												tsq
		INNER JOIN fact.QuantityPivot							a
			ON	a.Refnum = tsq.Refnum
		INNER JOIN dim.Stream_Bridge							b
			ON	b.FactorSetId = tsq.FactorSetId
			AND	b.DescendantId = a.StreamId
			AND	 b.StreamId = 'Propylene'
		INNER JOIN fact.CompositionQuantity						c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.StreamId = a.StreamId
			AND	c.StreamDescription = a.StreamDescription
		INNER JOIN ante.StandardEnergyCoefficients				k
			ON	k.FactorSetId = tsq.FactorSetId
			AND	k.StandardEnergyId = 'Purity'
		INNER JOIN ante.StandardEnergyExponents					e
			ON	e.FactorSetId = tsq.FactorSetId
			AND	e.FactorSetId = k.FactorSetId
			AND	e.StandardEnergyId = 'Purity'
		INNER JOIN dim.SimModel_LookUp							m
			ON	m.ModelId	<>	'Plant'
		WHERE	a.Tot_kMT > 0.0
			AND	c.Component_WtPcnt > 0.0
			AND	tsq.FactorSet_AnnDateKey	<= 20130000
		GROUP BY
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			k.StandardEnergyId,
			b.StreamId,
			m.ModelId,
			k.Value,
			e.Value
		HAVING SUM(ABS(a.Tot_kMT) * c.Component_WtPcnt) > 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;