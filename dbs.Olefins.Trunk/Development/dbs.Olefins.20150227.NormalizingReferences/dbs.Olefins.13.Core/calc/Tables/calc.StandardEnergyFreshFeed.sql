﻿CREATE TABLE [calc].[StandardEnergyFreshFeed] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [StandardEnergyId]       VARCHAR (42)       NOT NULL,
    [Energy_kBTU]            REAL               NOT NULL,
    [Yield_HvcFeed]          REAL               NOT NULL,
    [Energy_kBtu_LbHvc]      REAL               NOT NULL,
    [ProdHvc_kMT]            REAL               NOT NULL,
    [StandardEnergy_MBtuDay] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyFreshFeed_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyFreshFeed_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyFreshFeed_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyFreshFeed_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyFreshFeed] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StandardEnergyId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_StandardEnergyFreshFeed_StandardEnergy_MBtuDay] CHECK ([StandardEnergy_MBtuDay]>=(0.0)),
    CONSTRAINT [FK_StandardEnergyFreshFeed_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_StandardEnergyFreshFeed_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergyFreshFeed_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergyFreshFeed_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_CalcStandardEnergyFreshFeed_u
	ON  calc.StandardEnergyFreshFeed
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE calc.StandardEnergyFreshFeed
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.StandardEnergyFreshFeed.[FactorSetId]		= INSERTED.[FactorSetId]
		AND	calc.StandardEnergyFreshFeed.[Refnum]			= INSERTED.[Refnum]
		AND calc.StandardEnergyFreshFeed.[StandardEnergyId]	= INSERTED.[StandardEnergyId]
		AND calc.StandardEnergyFreshFeed.[CalDateKey]		= INSERTED.[CalDateKey];

END;