﻿CREATE TABLE [calc].[DivisorsProduction] (
    [FactorSetId]                 VARCHAR (12)       NOT NULL,
    [Refnum]                      VARCHAR (25)       NOT NULL,
    [CalDateKey]                  INT                NOT NULL,
    [ComponentId]                 VARCHAR (42)       NOT NULL,
    [ProductionActual_kMT]        REAL               NOT NULL,
    [ProductionActual_WtPcnt]     REAL               NOT NULL,
    [_ProductionActual_klb]       AS                 (CONVERT([real],[ProductionActual_kMT]*(2.20461995668492),(1))) PERSISTED NOT NULL,
    [Production_kMT]              REAL               NULL,
    [Production_WtPcnt]           REAL               NULL,
    [_Production_klb]             AS                 (CONVERT([real],[Production_kMT]*(2.20461995668492),(1))) PERSISTED,
    [TaAdj_Production_Ratio]      REAL               NOT NULL,
    [_TaAdj_ProductionActual_kMT] AS                 (CONVERT([real],[TaAdj_Production_Ratio]*[ProductionActual_kMT],(1))) PERSISTED NOT NULL,
    [_TaAdj_ProductionActual_klb] AS                 (CONVERT([real],([TaAdj_Production_Ratio]*[ProductionActual_kMT])*(2.20461995668492),(1))) PERSISTED NOT NULL,
    [_TaAdj_Production_kMT]       AS                 (CONVERT([real],[TaAdj_Production_Ratio]*[Production_kMT],(1))) PERSISTED,
    [_TaAdj_Production_klb]       AS                 (CONVERT([real],([TaAdj_Production_Ratio]*[Production_kMT])*(2.20461995668492),(1))) PERSISTED,
    [tsModified]                  DATETIMEOFFSET (7) CONSTRAINT [DF_DivisorsProduction_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]              NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]              NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]               NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_DivisorsProduction] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_DivisorsProduction_Production_kMT] CHECK ([Production_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_Production_WtPcnt] CHECK ([Production_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_ProductionActual_kMT] CHECK ([ProductionActual_kMT]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_ProductionActual_WtPcnt] CHECK ([ProductionActual_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_TaAdj_Production_kMT] CHECK ([_TaAdj_Production_kMT]>=(0.0) AND [_TaAdj_Production_kMT]>=[Production_kMT]),
    CONSTRAINT [CR_calc_DivisorsProduction_TaAdj_Production_Ratio] CHECK ([TaAdj_Production_Ratio]>=(1.0)),
    CONSTRAINT [CR_calc_DivisorsProduction_TaAdj_ProductionActual_kMT] CHECK ([_TaAdj_ProductionActual_kMT]>=(0.0) AND [_TaAdj_ProductionActual_kMT]>=[ProductionActual_kMT]),
    CONSTRAINT [FK_calc_DivisorsProduction_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_DivisorsProduction_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_DivisorsProduction_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_DivisorsProduction_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_DivisorsProduction_u
	ON  calc.DivisorsProduction
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.DivisorsProduction
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.DivisorsProduction.FactorSetId		= INSERTED.FactorSetId
		AND calc.DivisorsProduction.Refnum			= INSERTED.Refnum
		AND calc.DivisorsProduction.CalDateKey		= INSERTED.CalDateKey
		AND calc.DivisorsProduction.ComponentId		= INSERTED.ComponentId;
				
END