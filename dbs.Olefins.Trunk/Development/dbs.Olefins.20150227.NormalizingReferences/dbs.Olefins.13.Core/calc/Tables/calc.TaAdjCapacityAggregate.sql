﻿CREATE TABLE [calc].[TaAdjCapacityAggregate] (
    [FactorSetId]                        VARCHAR (12)       NOT NULL,
    [Refnum]                             VARCHAR (25)       NOT NULL,
    [CalDateKey]                         INT                NOT NULL,
    [SchedId]                            CHAR (1)           NOT NULL,
    [StreamId]                           VARCHAR (42)       NULL,
    [CurrencyRpt]                        VARCHAR (4)        NULL,
    [Interval_Mnths]                     REAL               NULL,
    [Interval_Years]                     REAL               NULL,
    [DownTime_Hrs]                       REAL               NULL,
    [Ann_TurnAroundCost_kCur]            REAL               NULL,
    [Ann_TurnAround_ManHrs]              REAL               NULL,
    [Ann_CostPerManHour_kCur]            REAL               NULL,
    [Ann_TurnAroundLoss_Days]            REAL               NULL,
    [Ann_TurnAroundLoss_Pcnt]            REAL               NULL,
    [Ann_FullPlantEquivalentDT_HrsYr]    REAL               NULL,
    [TaAdjExRateAnn_TurnAroundCost_kCur] REAL               NULL,
    [tsModified]                         DATETIMEOFFSET (7) CONSTRAINT [DF_TaAdjCapacityAggregate_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                     NVARCHAR (168)     CONSTRAINT [DF_TaAdjCapacityAggregate_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                     NVARCHAR (168)     CONSTRAINT [DF_TaAdjCapacityAggregate_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                      NVARCHAR (168)     CONSTRAINT [DF_TaAdjCapacityAggregate_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_TaAdjCapacityAggregate] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SchedId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_TaAdjCapacityAggregate_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_TaAdjCapacityAggregate_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_TaAdjCapacityAggregate_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_TaAdjCapacityAggregate_Schedule_LookUp] FOREIGN KEY ([SchedId]) REFERENCES [dim].[Schedule_LookUp] ([ScheduleId]),
    CONSTRAINT [FK_calc_TaAdjCapacityAggregate_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_TaAdjCapacityAggregate_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_TaAdjCapacityAggregate_ReliabilityProductLost]
    ON [calc].[TaAdjCapacityAggregate]([SchedId] ASC, [StreamId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [Interval_Years]);


GO
CREATE TRIGGER calc.t_TaAdjCapacityAggregate_u
	ON  calc.TaAdjCapacityAggregate
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.TaAdjCapacityAggregate
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.TaAdjCapacityAggregate.FactorSetId		= INSERTED.FactorSetId
		AND calc.TaAdjCapacityAggregate.Refnum			= INSERTED.Refnum
		AND calc.TaAdjCapacityAggregate.CalDateKey		= INSERTED.CalDateKey
		AND calc.TaAdjCapacityAggregate.SchedId			= INSERTED.SchedId;
	
END