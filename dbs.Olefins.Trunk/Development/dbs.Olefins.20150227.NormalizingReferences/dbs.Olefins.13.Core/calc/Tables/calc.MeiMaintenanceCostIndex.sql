﻿CREATE TABLE [calc].[MeiMaintenanceCostIndex] (
    [FactorSetId]                      VARCHAR (12)       NOT NULL,
    [Refnum]                           VARCHAR (25)       NOT NULL,
    [CalDateKey]                       INT                NOT NULL,
    [CurrencyRpt]                      VARCHAR (4)        NOT NULL,
    [MaintAvg_Cur]                     REAL               NOT NULL,
    [InflAdjAnn_TurnAroundCost_kCur]   REAL               NOT NULL,
    [Ann_MaintCostIndex_Cur]           REAL               NOT NULL,
    [MaintAvg_PcntRv]                  REAL               NOT NULL,
    [InflAdjAnn_TurnAroundCost_PcntRv] REAL               NOT NULL,
    [Ann_MaintCostIndex_PcntRv]        REAL               NOT NULL,
    [tsModified]                       DATETIMEOFFSET (7) CONSTRAINT [DF_MeiMaintenanceCostIndex_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                   NVARCHAR (168)     CONSTRAINT [DF_MeiMaintenanceCostIndex_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                   NVARCHAR (168)     CONSTRAINT [DF_MeiMaintenanceCostIndex_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                    NVARCHAR (168)     CONSTRAINT [DF_MeiMaintenanceCostIndex_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_MeiMaintenanceCostIndex] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_MeiMaintenanceCostIndex_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_MeiMaintenanceCostIndex_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_MeiMaintenanceCostIndex_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_MeiMaintenanceCostIndex_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_MeiMaintenanceCostIndex_u
	ON  calc.MeiMaintenanceCostIndex
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.MeiMaintenanceCostIndex
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MeiMaintenanceCostIndex.FactorSetId	= INSERTED.FactorSetId
		AND calc.MeiMaintenanceCostIndex.Refnum			= INSERTED.Refnum
		AND calc.MeiMaintenanceCostIndex.CalDateKey		= INSERTED.CalDateKey
		AND calc.MeiMaintenanceCostIndex.CurrencyRpt	= INSERTED.CurrencyRpt;
				
END