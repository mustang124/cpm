﻿CREATE TABLE [inter].[PricingPrimaryStreamCountry] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [CountryId]             CHAR (3)           NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [Proximity_RegionID]    VARCHAR (5)        NULL,
    [Raw_Amount_Cur]        REAL               NOT NULL,
    [Prx_Amount_Cur]        REAL               NULL,
    [Adj_MatlBal_Cur]       REAL               NULL,
    [Adj_Proximity_Cur]     REAL               NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],coalesce([MatlBal_Supersede_Cur],(coalesce([Prx_Amount_Cur],[Raw_Amount_Cur])+coalesce([Adj_MatlBal_Cur],(0.0)))+coalesce([Adj_Proximity_Cur],(0.0))),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPrimaryStreamCountry_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryStreamCountry_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryStreamCountry_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryStreamCountry_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingPrimaryStreamCountry] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [CountryId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingPrimaryStreamCountry_MatlBal_Supersede_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPrimaryStreamCountry_Prox_Amount_Cur] CHECK ([Prx_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPrimaryStreamCountry_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingPrimaryStreamCountry_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPrimaryStreamCountry_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingPrimaryStreamCountry_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingPrimaryStreamCountry_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingPrimaryStreamCountry_Region_LookUp] FOREIGN KEY ([Proximity_RegionID]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingPrimaryStreamCountry_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingPrimaryStreamCountry_u
	ON inter.PricingPrimaryStreamCountry
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingPrimaryStreamCountry
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingPrimaryStreamCountry.FactorSetId	= INSERTED.FactorSetId
		AND inter.PricingPrimaryStreamCountry.CountryId		= INSERTED.CountryId
		AND inter.PricingPrimaryStreamCountry.CalDateKey	= INSERTED.CalDateKey
		AND inter.PricingPrimaryStreamCountry.StreamId		= INSERTED.StreamId
		AND inter.PricingPrimaryStreamCountry.CurrencyRpt	= INSERTED.CurrencyRpt;

END;