﻿CREATE PROCEDURE inter.Insert_PricingStaging
(
	@FactorSetId			VARCHAR(42) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingStreamRegion (ASIA, EUR, NA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur, Adj_MatlBal_Cur)
		SELECT
			  u.FactorSetId										[FactorSetId]
			, u.RegionId										[RegionId]
			, u.CalDateKey										[CalDateKey]
			, 'USD'												[CurrencyRpt]
			, u.StreamId
			, u.Amount_Cur										[Amount_Cur]
			, CASE u.StreamId
				WHEN 'ConcButadiene'	THEN - 2205.0 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.02224		ELSE 0.02497	END
				WHEN 'DilButadiene'		THEN - 2205.0 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.02224		ELSE 0.02497	END

				WHEN 'ConcBenzene'		THEN - 2205.0 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.05576		ELSE 0.06256	END
				WHEN 'ConcEthylene'		THEN - 2205.0 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.05576		ELSE 0.06256	END

				WHEN 'DilButylene'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.03320		ELSE 0.03725	END
				WHEN 'ConcPropylene'	THEN - 2205.0 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END

				WHEN 'DilButane'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'DilEthane'		THEN - 2205.0 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'DilPropane'		THEN - 2205.0 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'SuppOther'		THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				WHEN 'DilMoGas'			THEN - 2204.6 * CASE WHEN u.FactorSetId NOT IN ('2013') THEN 0.005566	ELSE 0.006245	END
				END												[Adj_MatlBal_Cur]

		FROM (
			SELECT
				  l.FactorSetId
				, CONVERT(INT, CONVERT(CHAR(4), t.[Year]) +
					CASE t.PeriodID
						WHEN 'Qtr1' THEN '0331'
						WHEN 'Qtr2' THEN '0630'
						WHEN 'Qtr3' THEN '0930'
						WHEN 'Qtr4' THEN '1231'
					END)										[CalDateKey]
				, CASE t.Region
					WHEN 'Asia'	THEN 'ASIA'
					WHEN 'NWE'	THEN 'EUR'
					WHEN 'USGC'	THEN 'NA'
					END											[RegionId]

				-- Fresh Pyro Feed
				, t.Ethane										[Ethane]
				, t.Propane										[Propane]
				, t.LPG											[LPG]
				, t.MIXEDBUTANES								[Butane]
				, t.LtNaphtha									[LiqC5C6]
				, t.Raffinate									[Raffinate]
				, t.REFORM_NAPHTHA								[Naphtha]
				, t.ATMO_GASOIL									[Diesel]
				, t.GasOil										[GasOilHv]
				, t.ATMO_GASOIL									[GasOilHt]
				, t.NRC											[Condensate]

				-- Products
				, t.Hydrogen									[Hydrogen]
				, t.Acetylene									[Acetylene]
				, t.Ethylene									[Ethylene]
				, t.Propylene									[PropylenePG]
				, t.PropyleneCG									[PropyleneCG]
				, t.PROPYLENE_REFY								[PropyleneRG]
				, t.Butadiene									[Butadiene]
				, t.Benzene										[Benzene]
				, t.PYROGASOLINE_HY								[PyroGasOilHt]
				, t.PYROGASOLINE_UT								[PyroGasOilUt]

				-- Errata
				, t.METHANE										[Methane]
				, t.NATURAL_GAS									[PricingAdj]

				-- Stream Calcs (Not Reported)
				, t.Benzene										[ConcBenzene]

				, t.PYROGASOLINE_HY								[DilBenzene]
				, t.Butadiene									[DilButadiene]
				, t.MIXEDBUTANES								[DilButane]
				, t.BUTYLENE_ALKY								[DilButylene]
				, t.Ethane										[DilEthane]

				, t.PYROGASOLINE_UT								[DilMoGas]
	
				-- Plant Calcs (Not Reported)
				, t.Ethylene									[ConcEthylene]
				, t.PROPYLENE_REFY								[ConcPropylene]
				, t.Butadiene									[ConcButadiene]
				, t.Propane										[DilPropane]

				, t.Butane										[C4Oth]
				, t.HeavyNGL									[HeavyNGL]
				, t.BUTYLENE_ALKY								[IsoButylene]
				, t.PyFuelOil									[PyroFuelOil]
				, t.PyGasoil									[PyroGasOil]

				, t.PyFuelOil									[SuppGasOil]
				, t.PyGasoil									[SuppWashOil]

				, t.EPMix										[EPMix]

				-- Product Streams
				, t.Ethylene									[EthylenePG]
				, t.Ethylene * 0.95								[EthyleneCG]

				, t.Ethane										[PPFCEthane]
				, t.Propane										[PPFCPropane]
				, t.MIXEDBUTANES								[PPFCButane]
				, t.PYROGASOLINE_UT								[PPFCPyroNaphtha]
				, t.PyGasoil									[PPFCPyroGasOil]
				, t.PyFuelOil									[PPFCPyroFuelOil]

			FROM stgFact.Pricing				t
			INNER JOIN dim.FactorSet_LookUp		l
				ON	l.[FactorSetYear] = t.[Year]
			WHERE l.FactorSetId = @FactorSetId
			) p
			UNPIVOT(
			Amount_Cur FOR StreamId IN (
				-- Fresh Pyro Feed
				  [Ethane]
				, [Propane]
				, [LPG]
				, [Butane]
				, [LiqC5C6]
				, [Raffinate]
				, [Naphtha]
				, [Diesel]
				, [GasOilHv]
				, [GasOilHt]
				, [Condensate]

				-- Products
				, [Hydrogen]
				, [Acetylene]
				, [Ethylene]
				, [PropylenePG]
				, [PropyleneCG]
				, [PropyleneRG]
				, [Butadiene]
				, [Benzene]
				, [PyroGasOilHt]
				, [PyroGasOilUt]

				-- Errata
				, [Methane]
				, [PricingAdj]

				-- Stream Calcs (Not Reported)
				, [ConcBenzene]

				, [DilBenzene]
				, [DilButadiene]
				, [DilButane]
				, [DilButylene]
				, [DilEthane]

				, [DilMoGas]

				-- Plant Calcs (Not Reported)
				, [ConcEthylene]
				, [ConcPropylene]
				, [ConcButadiene]
				, [DilPropane]

				, [C4Oth]
				, [HeavyNGL]
				, [IsoButylene]
				, [PyroFuelOil]
				, [PyroGasOil]
				, [SuppGasOil]
				, [SuppWashOil]

				, [EPMix]

				-- Product Streams
				, [EthylenePG]
				, [EthyleneCG]

				, [PPFCEthane]
				, [PPFCPropane]
				, [PPFCButane]
				, [PPFCPyroNaphtha]
				, [PPFCPyroGasOil]
				, [PPFCPyroFuelOil]
				)
			) u
		WHERE NOT (u.StreamId = 'Diesel' AND u.CalDateKey >= 20090000);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingStreamRegion (MEA: Methane, Ethane, EPMix)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingStreamRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, Raw_Amount_Cur)
		SELECT
			  l.FactorSetId
			, 'MEA'
			, c.CalDateKey
			, 'USD'
			, p.StreamId
			, p.Amount_Cur
		FROM dim.FactorSet_LookUp			l
		INNER JOIN dim.Calendar_LookUp		c
			ON	c.CalYear = l.[FactorSetYear]
		, (
		SELECT
			  55.0	[Methane]
			, 70.0	[Ethane]
			, 70.0	[EPMix]
			, 70.0	[DilEthane]
			, 70.0	[PPFCEthane]
			) u
			UNPIVOT (
			Amount_Cur FOR StreamId IN (
				  Methane
				, Ethane
				, EPMix
				, DilEthane
				, PPFCEthane
				)
			) p
		WHERE l.FactorSetId = @FactorSetId;

		---------------------------------------------------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingCompositionRegion (Light: ASIA, EUR, NA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  u.FactorSetId										[FactorSetId]
			, u.RegionId										[RegionId]						
			, u.CalDateKey										[CalDateKey]
			, 'USD'												[CurrencyRpt]
			, 'Light'											[StreamId]
			, u.ComponentId										[ComponentId]
			, u.Amount_Cur										[Amount_Cur]
		FROM (
			SELECT
				  t.[Year]										[FactorSetId]
				, CONVERT(INT, CONVERT(CHAR(4), t.[Year]) +
					CASE t.PeriodID
						WHEN 'Qtr1' THEN '0331'
						WHEN 'Qtr2' THEN '0630'
						WHEN 'Qtr3' THEN '0930'
						WHEN 'Qtr4' THEN '1231'
					END)										[CalDateKey]
				, CASE t.Region
					WHEN 'Asia'	THEN 'ASIA'
					WHEN 'NWE'	THEN 'EUR'
					WHEN 'USGC'	THEN 'NA'
					END											[RegionId]

				-- Direct Light Feed
				, t.Ethane										[C2H4]
				, t.Ethane										[C2H6]
				, t.PROPYLENE_ALKY								[C3H6]
				, t.Propane										[C3H8]
				, t.Butane										[NBUTA]
				, t.ISOBUTANE									[IBUTA]
				, t.BUTYLENE_ALKY								[IB]
				, t.BUTYLENE_ALKY								[B1]
				, t.LtNaphtha									[NC5]
				, t.LtNaphtha									[IC5]
				, t.LtNaphtha									[NC6]
				, t.LtNaphtha									[C6ISO]
				, t.LtNaphtha									[C7H16]	--	20121203	NC7
				, t.LtNaphtha									[C8H18]	--	20121203	NC8

				-- Calculations
				, t.MIXEDBUTANES								[C4H10]			-- Used to calculate C4H6
				, t.NATURAL_GAS									[PricingAdj]	-- Used to calculate CH4, H2

				-- Raw Price
				, t.Butadiene									[C4H6]
				, t.METHANE										[CH4]
				, t.METHANE										[H2]

			FROM stgFact.Pricing				t
			INNER JOIN dim.FactorSet_LookUp		l
				ON	l.[FactorSetYear] = t.[Year]
			WHERE l.FactorSetId = @FactorSetId
			) p
			UNPIVOT(
			Amount_Cur FOR ComponentId IN (
				-- Direct Light Feed
				  p.C2H4
				, p.C2H6
				, p.C3H6
				, p.C3H8
				, p.NBUTA
				, p.IBUTA
				, p.IB
				, p.B1
				, p.NC5
				, p.IC5
				, p.NC6
				, p.C6ISO
				, p.C7H16	--	20121203	NC7
				, p.C8H18	--	20121203	NC8

				-- Calculations
				, p.C4H10
				, p.PricingAdj

				-- Raw Price
				, p.C4H6
				, p.CH4
				, p.H2
				)
			) u;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingCompositionRegion (Light: MEA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  l.FactorSetId
			, 'MEA'
			, c.CalDateKey
			, 'USD'
			, 'Light'
			, p.ComponentId
			, p.Amount_Cur
		FROM dim.FactorSet_LookUp		l
		INNER JOIN dim.Calendar_LookUp	c
			ON	c.CalYear = l.[FactorSetYear]
		, (
		SELECT
			  CONVERT(REAL, 70.0								, 1)	[C2H4]
			, CONVERT(REAL, 70.0								, 1)	[C2H6]
			, CONVERT(REAL, 55.0 / 2205.0 * 1000000.0 / 21500.0	, 1)	[PricingAdj]
			, CONVERT(REAL, 55.0								, 1)	[CH4]
			, CONVERT(REAL, 55.0								, 1)	[H2]
			) u
			UNPIVOT (
			Amount_Cur FOR ComponentId IN (
				  C2H4
				, C2H6
				, PricingAdj
				, CH4
				, H2
				)
			) p
		WHERE l.FactorSetId = @FactorSetId;

		---------------------------------------------------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingCompositionRegion (Prod: ASIA, EUR, NA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  u.FactorSetId										[FactorSetId]
			, u.RegionId										[RegionId]						
			, u.CalDateKey										[CalDateKey]
			, 'USD'												[CurrencyRpt]
			, 'Prod'
			, etl.ConvComponentID(u.ComponentId)				[ComponentId]
			, u.Amount_Cur										[Amount_Cur]
		FROM (
			SELECT
				  t.[Year]										[FactorSetId]
				, CONVERT(INT, CONVERT(CHAR(4), t.[Year]) +
					CASE t.PeriodID
						WHEN 'Qtr1' THEN '0331'
						WHEN 'Qtr2' THEN '0630'
						WHEN 'Qtr3' THEN '0930'
						WHEN 'Qtr4' THEN '1231'
					END)										[CalDateKey]
				, CASE t.Region
					WHEN 'Asia'	THEN 'ASIA'
					WHEN 'NWE'	THEN 'EUR'
					WHEN 'USGC'	THEN 'NA'
					END											[RegionId]

				, t.Hydrogen									[H2]
				, t.METHANE										[CH4]
				, t.NATURAL_GAS									[PricingAdj]

				-- Supplemental and Other Products
				, t.Acetylene									[C2H2]
				, t.Ethylene * 0.90								[C2H4]
				, t.METHANE										[C2H6]
				, t.PROPYLENE_REFY								[C3H6]
				, t.LPG											[C3H8]
				, t.Butadiene									[C4H6]
				, t.LPG											[C4H8]
				, t.LPG											[C4H10]
				, t.Benzene										[C6H6]
				, t.PYROGASOLINE_UT								[PyroGasOil]
				, t.PyFuelOil									[PyroFuelOil]
				, t.PYROGASOLINE_UT								[PyroGasoline]

			FROM stgFact.Pricing				t
			INNER JOIN dim.FactorSet_LookUp		l
				ON	l.[FactorSetYear] = t.[Year]
			WHERE l.FactorSetId = @FactorSetId
			) p
			UNPIVOT(
			Amount_Cur FOR ComponentId IN (
				  H2
				, CH4
				, PricingAdj

				, C2H2
				, C2H4
				, C2H6
				, C3H6
				, C3H8
				, C4H6
				, C4H8
				, C4H10
				, C6H6
				, PyroGasOil
				, PyroFuelOil
				, PyroGasoline
				)
			) u;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingCompositionRegion (Prod: MEA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			  l.FactorSetId
			, 'MEA'
			, c.CalDateKey
			, 'USD'
			, 'Prod'
			, p.ComponentId
			, p.Amount_Cur
		FROM dim.FactorSet_LookUp			l
		INNER JOIN dim.Calendar_LookUp		c
			ON	c.CalYear = l.[FactorSetYear]
		, (
		SELECT
			  CONVERT(REAL, 55.0								, 1)	[CH4]
			, CONVERT(REAL, (55.0 * 3.0 + 0.06 * 2205.0) * 2.0	, 1)	[C2H2]
			, CONVERT(REAL, 55.0								, 1)	[C2H6]
			, CONVERT(REAL, 55.0 * 3.0 + 0.06 * 2205.0			, 1)	[H2]
			, CONVERT(REAL, 55.0 / 2205.0 * 1000000.0 / 21500.0	, 1)	[PricingAdj]
			) u
			UNPIVOT (
			Amount_Cur FOR ComponentId IN (
				  CH4
				, C2H2
				, C2H6
				, H2
				, PricingAdj
				)
			) p
		WHERE l.FactorSetId = @FactorSetId;

		---------------------------------------------------------------------------------------------------

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingCompositionRegion (Yield: ASIA, EUR, NA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			u.FactorSetId				[FactorSetId],
			u.RegionId					[RegionId],
			u.CalDateKey				[CalDateKey],
			'USD'						[CurrencyRpt],
			'Yield',
			u.ComponentId				[ComponentId],
			u.Amount_Cur				[Amount_Cur]
		FROM (
			SELECT
				t.[Year]											[FactorSetId],
				CONVERT(INT, CONVERT(CHAR(4), t.[Year]) +
					CASE t.PeriodID
						WHEN 'Qtr1' THEN '0331'
						WHEN 'Qtr2' THEN '0630'
						WHEN 'Qtr3' THEN '0930'
						WHEN 'Qtr4' THEN '1231'
					END)											[CalDateKey],
				CASE t.Region
					WHEN 'Asia'	THEN 'ASIA'
					WHEN 'NWE'	THEN 'EUR'
					WHEN 'USGC'	THEN 'NA'
					END												[RegionId],

				t.Hydrogen	 								[H2],
				t.Ethylene	 								[C2H2],
				t.Ethylene									[C2H4],
				t.Ethane									[C2H6],

				t.Ethylene	 								[C3H4],
				t.Propylene									[C3H6],
				t.Propane									[C3H8],

				t.Butadiene									[C4H6],
				t.BUTYLENE_ALKY + 0.01 / 5.0 * 2204.6		[C4H8],
				t.BUTYLENE_ALKY								[C4H10],

				t.Benzene									[C6H6],
				t.PYROGASOLINE_HY							[PyroGasoline],
				t.PyGasoil									[PyroGasOil],
				t.PyFuelOil									[PyroFuelOil],		

				t.PYROGASOLINE_HY							[C5S],
				t.PYROGASOLINE_HY							[C6C8NA],
				t.PYROGASOLINE_HY							[C7H8],
				t.PYROGASOLINE_HY							[C8H10],
				t.PYROGASOLINE_HY							[C8H8]

			FROM stgFact.Pricing				t
			INNER JOIN dim.FactorSet_LookUp		l
				ON	l.[FactorSetYear] = t.[Year]
			WHERE l.FactorSetId = @FactorSetId
			) p
			UNPIVOT(
			Amount_Cur FOR ComponentId IN (
				[H2],
				[C2H2],
				[C2H4],
				[C2H6],
				[C3H4],
				[C3H6],
				[C3H8],
				[C4H6],
				[C4H8],
				[C4H10],
				[C6H6],
				[PyroGasoline],
				[PyroGasOil],
				[PyroFuelOil],

				[C5S],
				[C6C8NA],
				[C7H8],
				[C8H10],
				[C8H8]
				)
			) u;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.PricingStagingCompositionRegion (Yield: MEA)';
		PRINT @ProcedureDesc;

		INSERT INTO inter.PricingStagingCompositionRegion(FactorSetId, RegionId, CalDateKey, CurrencyRpt, StreamId, ComponentId, Raw_Amount_Cur)
		SELECT
			l.FactorSetId,
			'MEA',
			c.CalDateKey,
			'USD',
			'Yield',
			p.ComponentId,
			p.Amount_Cur
		FROM dim.FactorSet_LookUp			l
		INNER JOIN dim.Calendar_LookUp		c
			ON	c.CalYear = l.[FactorSetYear]
		, (
		SELECT
			CONVERT(REAL, 70.0								, 1)	[C2H6],
			CONVERT(REAL, 55.0 * 3.0 + 0.06 * 2205.0		, 1)	[H2]
			) u
			UNPIVOT (
			Amount_Cur FOR ComponentId IN (
				C2H6,
				H2
				)
			) p
		WHERE l.FactorSetId = @FactorSetId;

END;