﻿CREATE TABLE [reports].[PersCost] (
    [GroupId]                VARCHAR (25)  NOT NULL,
    [Currency]               VARCHAR (8)   NOT NULL,
    [PersCost_CurrPerEDC]    REAL          NULL,
    [PersCost_CurrPerHVC_MT] REAL          NULL,
    [tsModified]             SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PersCost] PRIMARY KEY CLUSTERED ([GroupId] ASC, [Currency] ASC) WITH (FILLFACTOR = 70)
);

