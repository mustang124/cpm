﻿
CREATE PROCEDURE [reports].[spWhatsThereOverrides](@GroupId varchar(25), @Property varchar(10), @PropertySet varchar(10), @StudyYear int,
	@MinForAllExcept tinyint = 3, @CountForAllExcept tinyint = 1, @ValuesPerLine tinyint = 0, @Lines tinyint = 0,
	@FullText varchar(100) OUTPUT, @Line1 varchar(100) = NULL OUTPUT, @Line2 varchar(20) = NULL OUTPUT, @Line3 varchar(20) = NULL OUTPUT, @Line4 varchar(20) = NULL OUTPUT)
AS
SELECT	@FullText = '', @Line1 = '', @Line2 = '', @Line3 = '', @Line4 = ''
IF @Lines > 4
	RETURN -1

DECLARE @PropertyValues TABLE
(
	PropertyValue varchar(10) NOT NULL,
	OutputText varchar(20) NOT NULL,
	InGroup tinyint NOT NULL,
	OrderBy smallint NOT NULL
)

IF @Property IN ('EDCGroup', 'TechClass')
BEGIN

	INSERT INTO @PropertyValues
	SELECT DISTINCT PropertValue = ISNULL(CASE @Property 
			WHEN 'EDCGroup' THEN g.EDCGroup 
			WHEN 'CapGroup' THEN g.CapGroup 
			WHEN 'TechClass' THEN g.TechClass END, ''),
			OutputText = ISNULL(CASE @Property 
			WHEN 'EDCGroup' THEN g.EDCGroup 
			WHEN 'CapGroup' THEN g.CapGroup 
			WHEN 'TechClass' THEN g.TechClass END, ''), InGroup = 0, OrderBy = 0
	FROM dbo.GENSUM g 
	WHERE ((g.StudyYear = @StudyYear) OR g.Refnum IN (SELECT r.Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))

	DELETE FROM @PropertyValues WHERE PropertyValue = ''

	UPDATE @PropertyValues SET OrderBy = ISNULL(CAST(PropertyValue as smallint), 0)
	
	UPDATE @PropertyValues
	SET InGroup = 1
	WHERE OrderBy IN (SELECT DISTINCT CASE @Property 
			WHEN 'EDCGroup' THEN g.EDCGroup 
			WHEN 'CapGroup' THEN g.CapGroup 
			WHEN 'TechClass' THEN g.TechClass  END
		FROM dbo.GENSUM g
		WHERE g.Refnum IN (SELECT r. Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))
END

IF @Property IN ('Region')
BEGIN
	INSERT INTO @PropertyValues
	SELECT DISTINCT PropertValue = ISNULL(CASE WHEN @StudyYear>=2013 AND g.Region in ('NA','LA') THEN 'NSA' ELSE RTrim(g.Region) END, ''),
			OutputText = '', InGroup = 0, OrderBy = 0
	FROM dbo.GENSUM g 
	WHERE ((g.StudyYear = @StudyYear) OR g.Refnum IN (SELECT r.Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))

	DELETE FROM @PropertyValues WHERE PropertyValue = ''
	
	UPDATE @PropertyValues
	SET OrderBy = CASE PropertyValue 
				WHEN 'NA' THEN 1 
				WHEN 'EUR' THEN 2 
				WHEN 'MEA' THEN 3 
				WHEN 'ASIA' THEN 4 
				WHEN 'LA' THEN 5
				WHEN 'NSA' then 0
				ELSE 10 END
				
		,OutputText = CASE PropertyValue
				WHEN 'NA' THEN 'North America'
				WHEN 'EUR' THEN 'Europe'
				WHEN 'MEA' THEN 'Middle East/Africa'
				WHEN 'ASIA' THEN 'Asia'
				WHEN 'LA' THEN 'Latin America'
				WHEN 'NSA' then 'N&S America'
				ELSE '' END
				
	UPDATE @PropertyValues
	SET InGroup = 1
	WHERE PropertyValue IN (SELECT DISTINCT CASE WHEN @StudyYear>=2013 AND g.Region in ('NA','LA') THEN 'NSA' ELSE g.Region END
		FROM dbo.GENSUM g
		WHERE g.Refnum IN (SELECT r. Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))
		
		--select * from @PropertyValues
END


IF @Property IN ('FeedClass')
BEGIN
	INSERT INTO @PropertyValues
	SELECT DISTINCT PropertValue = ISNULL(g.FeedClass, ''), OutputText = '', InGroup = 0, OrderBy = ISNULL(g.FeedClass,0)
	FROM dbo.GENSUM g 
	WHERE ((g.StudyYear = @StudyYear) OR g.Refnum IN (SELECT r.Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))

	DELETE FROM @PropertyValues WHERE PropertyValue = ''
				
	UPDATE @PropertyValues
		SET OutputText = CASE PropertyValue
				WHEN 1 THEN 'Ethane'
				WHEN 2 THEN 'LPG'
				WHEN 3 THEN 'Mixed'
				WHEN 4 THEN 'Naphtha/Heavy'
				ELSE '' END
				
	UPDATE @PropertyValues
	SET InGroup = 1
	WHERE PropertyValue IN (SELECT DISTINCT g.FeedClass
		FROM dbo.GENSUM g
		WHERE g.Refnum IN (SELECT r. Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))
END

IF @Property IN ('CapGroup')
BEGIN
	INSERT INTO @PropertyValues
	SELECT DISTINCT PropertValue = ISNULL(g.CapGroup, ''), OutputText = '', InGroup = 0, OrderBy = ISNULL(g.CapGroup,0)
	FROM dbo.GENSUM g 
	WHERE ((g.StudyYear = @StudyYear) OR g.Refnum IN (SELECT r.Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))

	DELETE FROM @PropertyValues WHERE PropertyValue = ''
				
	UPDATE @PropertyValues
		SET OutputText = CASE PropertyValue
				WHEN 1 THEN '<650'
				WHEN 2 THEN '650-900'
				WHEN 3 THEN '900-1200'
				WHEN 4 THEN '>1200'
				ELSE '' END
				
	UPDATE @PropertyValues
	SET InGroup = 1
	WHERE PropertyValue IN (SELECT DISTINCT g.CapGroup
		FROM dbo.GENSUM g
		WHERE g.Refnum IN (SELECT r. Refnum FROM reports.GroupPlants r WHERE GroupId = @GroupId))
END

----------------------------------------------------------------------------------------------------------
DECLARE @NumGroups smallint, @NumGroupsInGroup smallint
SELECT @NumGroups = 0, @NumGroupsInGroup = 0
SELECT @NumGroups = COUNT(*), @NumGroupsInGroup = SUM(InGroup) FROM @PropertyValues

DECLARE @PropertyValue varchar(10), @OutputText varchar(20), @InGroup tinyint
DECLARE @LineNo tinyint, @LinePropCount tinyint, @LineValue varchar(100), @UseInGroup tinyint
SELECT @LineNo = 1, @UseInGroup = 1, @LinePropCount = 0
IF @NumGroups > @MinForAllExcept AND @NumGroupsInGroup >= @NumGroups - @CountForAllExcept
	SELECT @Line1 = 'All except', @LineNo = 1, @UseInGroup = 0
	
DECLARE cValues CURSOR LOCAL FAST_FORWARD FOR
	SELECT PropertyValue, OutputText, InGroup
	FROM @PropertyValues 
	ORDER BY OrderBy, OutputText
OPEN cValues
FETCH NEXT FROM cValues INTO @PropertyValue, @OutputText, @InGroup
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @InGroup = 1
		SELECT @FullText = CASE WHEN @FullText = '' THEN '' ELSE RTRIM(@FullText) + ', ' END + RTRIM(@OutputText)
	IF @InGroup = @UseInGroup AND @Lines > 0 AND @LineNo <= @Lines 
	BEGIN
		IF (@Property = 'RSC' AND @PropertyValue LIKE 'US[1-6]') OR (@Property = 'csRSC' AND @PropertyValue LIKE 'UC[1-6]')
			SELECT @LineNo = @LineNo
		ELSE IF @Property IN ('RPGGroup','csRPGGroup') AND @OutputText IN ('HS','HST')
			SELECT @LineNo = @LineNo
		ELSE IF @LinePropCount >= @ValuesPerLine
			SELECT @LinePropCount = 0, @LineNo = @LineNo + 1
		IF @LineNo <= @Lines 
		BEGIN
			IF @LineNo = 1
				SELECT @LineValue = @Line1
			ELSE IF @LineNo = 2
				SELECT @LineValue = @Line2
			ELSE IF @LineNo = 3
				SELECT @LineValue = @Line3
			ELSE IF @LineNo = 4
				SELECT @LineValue = @Line4

			SELECT @LineValue = RTRIM(@LineValue) + CASE @LineValue 
													WHEN 'All Except' THEN ' ' 
													WHEN '' THEN ''
													ELSE ', '
													END 
												+ @OutputText

			IF @LineNo = 1
				SELECT @Line1 = @LineValue
			ELSE IF @LineNo = 2
				SELECT @Line2 = @LineValue
			ELSE IF @LineNo = 3
				SELECT @Line3 = @LineValue
			ELSE IF @LineNo = 4
				SELECT @Line4 = @LineValue

			SELECT @LinePropCount = @LinePropCount + 1
		END
	END
	FETCH NEXT FROM cValues INTO @PropertyValue, @OutputText, @InGroup
END
IF @PropertySet = 'DNR'
	SELECT	@Line1 = '', @Line2 = '', @Line3 = '', @Line4 = ''
ELSE IF @NumGroupsInGroup = @NumGroups
	SELECT 	@Line1 = 'All', @Line2 = '', @Line3 = '', @Line4 = ''
ELSE IF @LineNo > @Lines
	SELECT @Line1 = @FullText, @Line2 = '', @Line3 = '', @Line4 = ''
ELSE BEGIN
	IF @Line1 = '' AND @FullText <> ''
		SELECT @Line1 = @FullText, @Line2 = '', @Line3 = '', @Line4 = ''
	ELSE IF @Line1 <> '' AND @Line1 <> 'All except' AND @Line2 <> ''
		SELECT @Line1 = @Line1 + ','
	IF @Line2 <> '' AND @Line3 <> ''
		SELECT @Line2 = @Line2 + ','
	IF @Line3 <> '' AND @Line4 <> ''
		SELECT @Line3 = @Line3 + ','
END
--Print 'Full ' + @FullText
--Print '1' + @Line1
--Print '2' + @Line2
--Print '3' + @Line3


/*
DECLARE @FullText varchar(100), @Line1 varchar(100), @Line2 varchar(20), @Line3 varchar(20), @Line4 varchar(20)

	
	EXEC [reports].[spWhatsThereOverrides] '13PCH:WEUR', 'FeedClass', NULL, 2013,
	3, 3, 1, 1,
	@FullText, @Line1, @Line2, @Line3, @Line4
	
	EXEC [reports].[spWhatsThereOverrides] '13PCH:FeedClass1', 'CapGroup', NULL, 2013,
	3, 1, 2, 1,
	@FullText, @Line1, @Line2, @Line3, @Line4

ALTER PROCEDURE [reports].[spWhatsThereOverrides](@GroupId varchar(25), @Property varchar(10), @PropertySet varchar(10) = '', @StudyYear int,
	@MinForAllExcept tinyint = 3, @CountForAllExcept tinyint = 1, @ValuesPerLine tinyint = 0, @Lines tinyint = 0,
	@FullText varchar(100) OUTPUT, @Line1 varchar(100) = NULL OUTPUT, @Line2 varchar(20) = NULL OUTPUT, @Line3 varchar(20) = NULL OUTPUT, @Line4 varchar(20) = NULL OUTPUT)
	*/

