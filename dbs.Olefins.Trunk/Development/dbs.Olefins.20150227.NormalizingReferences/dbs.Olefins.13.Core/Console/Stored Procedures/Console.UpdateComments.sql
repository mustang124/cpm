﻿CREATE PROCEDURE [Console].[UpdateComments]

	@RefNum varchar(18),
	@Comments varchar(255)

AS
BEGIN

UPDATE C  SET C.Comment = @Comments  FROM CoContactInfo c join tsort t on t.contactcode = c.contactcode and t.StudyYear = c.StudyYear
		WHERE t.SmallRefnum=@RefNum  

END
