﻿CREATE PROCEDURE [Console].[GetCompanyPassword](@RefNum varchar(18))
AS
BEGIN

	SELECT top 1 c.Password as CompanyPassword
	FROM dbo.CoContactInfo c join TSort t on c.ContactCode = t.ContactCode  WHERE t.SmallRefnum = @RefNum and Password <> '' order by c.StudyYear desc
END
