﻿CREATE PROCEDURE [fact].[Insert_OpEx_Misc]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.OpEx(Refnum, CalDateKey, AccountId, CurrencyRpt, Amount_Cur)
		SELECT 
			  u.Refnum
			, u.CalDateKey
			, etl.ConvAccountID(u.OpExId)								[AccountId]
			, 'USD'
			, u.Amount
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, d.OpExId
				, d.Amount
			FROM stgFact.OpExMiscDetail d
			INNER JOIN stgFact.TSort t
				ON t.Refnum =  d.Refnum
			WHERE d.Amount <> 0.0
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			)u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;