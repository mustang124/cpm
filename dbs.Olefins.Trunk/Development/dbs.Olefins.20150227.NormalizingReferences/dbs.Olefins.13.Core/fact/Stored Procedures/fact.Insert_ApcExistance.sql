﻿CREATE PROCEDURE [fact].[Insert_ApcExistance]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ApcExistance(Refnum, CalDateKey, ApcId, Mpc_Bit, Sep_Bit)
		SELECT
			p.Refnum,
			p.CalDateKey,
			etl.ConvFacilityID(p.FacilityId),
			etl.ConvBit(p.MPC)	[MPC],
			etl.ConvBit(p.SEP)	[SEP]
		FROM (
			SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
				etl.ConvDateKey(t.StudyYear)								[CalDateKey],
				ContType,
				Tbl9038,
				Tbl9039,
				Tbl9040,
				Tbl9041,
				Tbl9042,
				Tbl9043,
				Tbl9044,
				Tbl9045,
				Tbl9046,
				Tbl9047,
				Tbl9048,
				Tbl9049,
				Tbl9050,
				Tbl9052,
				Tbl9053,
				Tbl9054,
				Tbl9055,
				Tbl9056,
				Tbl9057,
				Tbl9058,
				Tbl9059,
				Tbl9060,
				Tbl9062,
				Tbl9063,
				Tbl9064,
				Tbl9065,
				Tbl9066,
				Tbl9067,
				Tbl9068,
				Tbl9069,
				Tbl9070,
				Tbl9071n,
				Tbl9072n,
				Tbl9073n,
				Tbl9074n,
				Tbl9075n
			FROM stgFact.PracControls c
			INNER JOIN stgFact.TSort t ON t.Refnum = c.Refnum
			WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) t
			UNPIVOT (
			Controller FOR FacilityId IN(
				Tbl9038,
				Tbl9039,
				Tbl9040,
				Tbl9041,
				Tbl9042,
				Tbl9043,
				Tbl9044,
				Tbl9045,
				Tbl9046,
				Tbl9047,
				Tbl9048,
				Tbl9049,
				Tbl9050,
				Tbl9052,
				Tbl9053,
				Tbl9054,
				Tbl9055,
				Tbl9056,
				Tbl9057,
				Tbl9058,
				Tbl9059,
				Tbl9060,
				Tbl9062,
				Tbl9063,
				Tbl9064,
				Tbl9065,
				Tbl9066,
				Tbl9067,
				Tbl9068,
				Tbl9069,
				Tbl9070,
				Tbl9071n,
				Tbl9072n,
				Tbl9073n,
				Tbl9074n,
				Tbl9075n
				)
			) u
			PIVOT (
			COUNT(Controller) FOR ContType IN(
				[MPC],
				[SEP]
				)
			) p
		WHERE p.MPC IS NOT NULL OR p.SEP IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;