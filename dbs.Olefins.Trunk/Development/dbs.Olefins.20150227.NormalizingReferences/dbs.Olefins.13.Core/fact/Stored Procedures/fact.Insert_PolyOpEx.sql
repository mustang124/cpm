﻿CREATE PROCEDURE [fact].[Insert_PolyOpEx]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.PolyOpEx(Refnum, CalDateKey, AssetId, AccountId, CurrencyRpt, Amount_Cur)
		SELECT
				u.Refnum
			, u.CalDateKey
			, u.AssetId
			, etl.ConvAccountID(u.AccountId)
			, 'USD'
			, u.AmountRPT
		FROM(
			SELECT
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, etl.ConvAssetPCH(p.Refnum)
					+ '-T' + CONVERT(VARCHAR(2), p.PlantId)				[AssetId]
				, p.NonTAMaint
				, p.TA
				, p.OthNonVol
				, p.Catalysts
				, p.Chemicals	[ChemOth]
				, p.Additives
				, p.Royalties
				, p.Energy
				, p.STVol
					- ISNULL(p.Catalysts, 0.0)
					- ISNULL(p.Chemicals, 0.0)
					- ISNULL(p.Additives, 0.0)
					- ISNULL(p.Royalties, 0.0)
					- ISNULL(p.Energy, 0.0)
					- ISNULL(p.Packaging, 0.0)
					- ISNULL(p.OthVol, 0.0)
								[PurOth]
				, p.Packaging
				, p.OthVol
				, p.STVol
			FROM stgFact.PolyOpEx p
			INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
			WHERE	p.TotCashOpEx <> 0.0
				AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) t
			UNPIVOT(
			AmountRPT FOR AccountId IN (
					NonTAMaint
				, TA
				, OthNonVol
		
				, Catalysts
				, ChemOth
				, Additives
				, Royalties
				, Energy
				, PurOth
				, Packaging
				, OthVol
				)
			) u
			WHERE u.AmountRPT > 0.0

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;