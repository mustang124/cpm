﻿CREATE PROCEDURE [fact].[Insert_Pers]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Pers(Refnum, CalDateKey, PersId, Personnel_Count, StraightTime_Hrs, OverTime_Hrs, Contract_Hrs)
		SELECT
			  etl.ConvRefnum(p.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvPersID(p.PersId)	[PersId]
			, p.NumPers					[Personnel_Count]
			, p.STH						[StraightTime_Hrs]
			, p.OvtHours				[OverTime_Hrs]
			, p.[Contract]				[Contract_Hrs]
		FROM stgFact.Pers p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE etl.ConvRefnum(p.Refnum, t.StudyYear, 'PCH') = @Refnum

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;