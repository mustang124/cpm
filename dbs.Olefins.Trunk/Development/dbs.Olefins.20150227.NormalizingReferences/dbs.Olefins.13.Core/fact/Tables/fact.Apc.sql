﻿CREATE TABLE [fact].[Apc] (
    [Refnum]                         VARCHAR (25)       NOT NULL,
    [CalDateKey]                     INT                NOT NULL,
    [Loop_Count]                     INT                NULL,
    [ClosedLoopAPC_Pcnt]             REAL               NULL,
    [ApcMonitorId]                   CHAR (1)           NULL,
    [ApcAge_Years]                   REAL               NULL,
    [OffLineLinearPrograms_Bit]      BIT                NULL,
    [OffLineNonLinearSimulators_Bit] BIT                NULL,
    [OffLineNonLinearInput_Bit]      BIT                NULL,
    [OnLineClosedLoop_Bit]           BIT                NULL,
    [OffLineComparison_Days]         REAL               NULL,
    [OnLineComparison_Days]          REAL               NULL,
    [PriceUpdateFreq_Days]           REAL               NULL,
    [ClosedLoopModelCokingPred_Bit]  BIT                NULL,
    [OptimizationOffLine_Mnths]      REAL               NULL,
    [OptimizationPlanFreq_Mnths]     REAL               NULL,
    [OptimizationOnLineOper_Mnths]   REAL               NULL,
    [OnLineInput_Count]              INT                NULL,
    [OnLineVariable_Count]           INT                NULL,
    [OnLineOperation_Pcnt]           REAL               NULL,
    [OnLinePointCycles_Count]        REAL               NULL,
    [PyroFurnModeledIndependent_Bit] BIT                NULL,
    [OnLineAutoModeSwitch_Bit]       BIT                NULL,
    [DynamicResponse_Count]          INT                NULL,
    [StepTestSimple_Days]            REAL               NULL,
    [StepTestComplex_Days]           REAL               NULL,
    [StepTestUpdateFreq_Mnths]       REAL               NULL,
    [AdaptiveAlgorithm_Count]        INT                NULL,
    [EmbeddedLP_Count]               INT                NULL,
    [MaxVariablesManipulated_Count]  INT                NULL,
    [MaxVariablesConstraint_Count]   INT                NULL,
    [MaxVariablesControlled_Count]   INT                NULL,
    [tsModified]                     DATETIMEOFFSET (7) CONSTRAINT [DF_Apc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                 NVARCHAR (168)     CONSTRAINT [DF_Apc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                 NVARCHAR (168)     CONSTRAINT [DF_Apc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                  NVARCHAR (168)     CONSTRAINT [DF_Apc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Apc] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_Apc_AdaptiveAlgorithm_Count] CHECK ([AdaptiveAlgorithm_Count]>=(0)),
    CONSTRAINT [CR_Apc_ApcAge_Years] CHECK ([ApcAge_Years]>=(0.0)),
    CONSTRAINT [CR_Apc_ClosedLoopAPC_Pcnt] CHECK ([ClosedLoopAPC_Pcnt]>=(0.0) AND [ClosedLoopAPC_Pcnt]<=(100.0)),
    CONSTRAINT [CR_Apc_DynamicResponse_Count] CHECK ([DynamicResponse_Count]>=(0)),
    CONSTRAINT [CR_Apc_EmbeddedLP_Count] CHECK ([EmbeddedLP_Count]>=(0)),
    CONSTRAINT [CR_Apc_Loop_Count] CHECK ([Loop_Count]>=(0)),
    CONSTRAINT [CR_Apc_MaxVariablesConstraint_Count] CHECK ([MaxVariablesConstraint_Count]>=(0)),
    CONSTRAINT [CR_Apc_MaxVariablesControlled_Count] CHECK ([MaxVariablesControlled_Count]>=(0)),
    CONSTRAINT [CR_Apc_MaxVariablesManipulated_Count] CHECK ([MaxVariablesManipulated_Count]>=(0)),
    CONSTRAINT [CR_Apc_OffLineComparison_Days] CHECK ([OffLineComparison_Days]>=(0.0)),
    CONSTRAINT [CR_Apc_OnLine_Pcnt] CHECK ([OnLineOperation_Pcnt]>=(0.0) AND [OnLineOperation_Pcnt]<=(100.0)),
    CONSTRAINT [CR_Apc_OnLineComparison_Days] CHECK ([OnLineComparison_Days]>=(0.0)),
    CONSTRAINT [CR_Apc_OnLineInput_Count] CHECK ([OnLineInput_Count]>=(0)),
    CONSTRAINT [CR_Apc_OnLinePointCycles_Count] CHECK ([OnLinePointCycles_Count]>=(0.0)),
    CONSTRAINT [CR_Apc_OnLineVariable_Count] CHECK ([OnLineVariable_Count]>=(0)),
    CONSTRAINT [CR_Apc_OptimizationOffLine_Mnths] CHECK ([OptimizationOffLine_Mnths]>=(0.0)),
    CONSTRAINT [CR_Apc_OptimizationOnLineOper_Mnths] CHECK ([OptimizationOnLineOper_Mnths]>=(0.0)),
    CONSTRAINT [CR_Apc_OptimizationPlanFreq_Mnths] CHECK ([OptimizationPlanFreq_Mnths]>=(0.0)),
    CONSTRAINT [CR_Apc_PriceUpdateFreq_Days] CHECK ([PriceUpdateFreq_Days]>=(0.0)),
    CONSTRAINT [CR_Apc_StepTestComplex_Days] CHECK ([StepTestComplex_Days]>=(0.0)),
    CONSTRAINT [CR_Apc_StepTestSimple_Days] CHECK ([StepTestSimple_Days]>=(0.0)),
    CONSTRAINT [CR_Apc_StepTestUpdateFreq_Mnths] CHECK ([StepTestUpdateFreq_Mnths]>=(0.0)),
    CONSTRAINT [FK_Apc_ApcLoopMonitor_LookUp] FOREIGN KEY ([ApcMonitorId]) REFERENCES [dim].[ApcLoopMonitor_LookUp] ([ApcLoopMonitorId]),
    CONSTRAINT [FK_Apc_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Apc_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_Apc_u]
	ON [fact].[Apc]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Apc]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[Apc].Refnum		= INSERTED.Refnum
		AND [fact].[Apc].CalDateKey	= INSERTED.CalDateKey;

END;