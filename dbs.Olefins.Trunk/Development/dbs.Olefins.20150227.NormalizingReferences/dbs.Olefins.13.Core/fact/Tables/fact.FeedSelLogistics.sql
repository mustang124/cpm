﻿CREATE TABLE [fact].[FeedSelLogistics] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [Delivery_Pcnt]  REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelLogistics_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FeedSelLogistics_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FeedSelLogistics_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelLogistics_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSelLogistics] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FeedSelLogistics_Delivery_Pcnt] CHECK ([Delivery_Pcnt]>=(0.0) AND [Delivery_Pcnt]<=(100.0)),
    CONSTRAINT [FK_FeedSelLogistics_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSelLogistics_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FeedSelLogistics_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FeedSelLogistics_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSelLogistics_u]
	ON [fact].[FeedSelLogistics]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSelLogistics]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSelLogistics].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSelLogistics].StreamId		= INSERTED.StreamId
		AND [fact].[FeedSelLogistics].FacilityId	= INSERTED.FacilityId
		AND [fact].[FeedSelLogistics].CalDateKey	= INSERTED.CalDateKey;

END;