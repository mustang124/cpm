﻿

CREATE procedure [dbo].[spCalcs] (@GroupId VARCHAR(25) = NULL) AS
/*

GroupsID could be a reflistname or a Refnum

*/

DECLARE @StartTime	DATETIMEOFFSET(7) = SYSDATETIMEOFFSET();

DECLARE @NumPlants SMALLINT, @FactorSetId VARCHAR(12)
DECLARE @qry varchar(1000), @Refnum varchar(25)	
DECLARE @ERROR_PROC varchar(255)

Select @NumPlants = COUNT(distinct Refnum) FROM cons.RefList r where r.ListId = @GroupId
Select @FactorSetId = NULL

IF (@NumPlants > 0) BEGIN

	-- 1.  Put all REFNUMS in FoundationPlantList
	-- 2.  Run Delete and Insert (uses the FoundationPlantList table)
	-- 3.  Run MakeTables to make the dbo. tables for each Refnum
	-- 4.  Run reports.spALL to make all of the reports for each Refnum
	DECLARE RefnumCursor CURSOR FOR 		
	SELECT Refnum = RTRIM(l.[Refnum]) FROM [cons].[RefList] l WHERE l.[ListId] = @GroupId;

	OPEN RefnumCursor;

	FETCH NEXT FROM RefnumCursor INTO @Refnum;
		
	WHILE (@@FETCH_STATUS <> -1)		
	BEGIN

		IF (@@FETCH_STATUS <> -2)	
		BEGIN
			--	calc.FoundationPlantList_Insert also deletes all records from the list.
			--SELECT @qry = 'EXECUTE calc.FoundationPlantList_Insert ''' + @Refnum + ''', ''' + @FactorSetId + ''''
			--EXEC(@qry)
			BEGIN TRY

				EXECUTE calc.CalculatePlant @FactorSetId, @Refnum;
				EXECUTE dbo.spInsertMessage		@Refnum, 4, 'Calcs';
				EXECUTE dbo.MakeTables @Refnum;
				EXECUTE reports.spALL @Refnum;
				EXECUTE dbo.spInsertMessage		@Refnum, 12, 'reports';

			END TRY
			BEGIN CATCH

				SET @ERROR_PROC = ERROR_PROCEDURE();
				EXECUTE dbo.spInsertMessage @Refnum, 2, @ERROR_PROC;

			END CATCH;
		END;
					
		FETCH NEXT FROM RefnumCursor INTO @Refnum;
					
	END;
	
	CLOSE RefnumCursor;
	DEALLOCATE RefnumCursor;

	SELECT
		le.ErrorNumber,
		le.XActState,
		le.Refnum,
		le.ProcedureSchema,
		le.ProcedureName,
		le.ProcedureLine,
		le.ProcedureDesc,
		le.ErrorMessage,
		le.ErrorSeverity,
		le.ErrorState,

		le.tsModified,
		le.tsModifiedHost,
		le.tsModifiedUser,
		le.tsModifiedApp,
		le.ts

	FROM dbo.LogError le
	WHERE	le.tsModified	>= @StartTime
		AND	le.Refnum		IN (SELECT l.[Refnum] FROM [cons].[RefList] l WHERE l.[ListId] = @GroupId);

	PRINT @GroupId + ': ' + CONVERT(VARCHAR, @NumPlants) + ' Plants Calculated';
		
END
ELSE BEGIN

	BEGIN TRY

		SET @Refnum = RTRIM(@GroupId);

		Print 'Calcs Start ' +  cast(SYSDATETIME() as varchar)
		EXECUTE calc.CalculatePlant @FactorSetId, @Refnum
		EXECUTE dbo.spInsertMessage		@Refnum, 4, 'Calcs'

		Print 'Calcs Completed ' +  cast(SYSDATETIME() as varchar) 
		EXECUTE dbo.MakeTables @Refnum

		Print 'MakeTables Completed ' +  cast(SYSDATETIME() as varchar)
		EXECUTE reports.spALL @Refnum

		EXECUTE dbo.spInsertMessage		@Refnum, 12, 'reports'	
		Print 'Report Completed ' +  cast(SYSDATETIME() as varchar)

		SELECT
			le.ErrorNumber,
			le.XActState,
			le.Refnum,
			le.ProcedureSchema,
			le.ProcedureName,
			le.ProcedureLine,
			le.ProcedureDesc,
			le.ErrorMessage,
			le.ErrorSeverity,
			le.ErrorState,

			le.tsModified,
			le.tsModifiedHost,
			le.tsModifiedUser,
			le.tsModifiedApp,
			le.ts

		FROM dbo.LogError le
		WHERE	le.tsModified	>= @StartTime
			AND	le.Refnum		= @Refnum;

	END TRY
	BEGIN CATCH

		SET @ERROR_PROC = ERROR_PROCEDURE();
		EXECUTE dbo.spInsertMessage @Refnum, 2, @ERROR_PROC;

	END CATCH;

END;
