﻿
CREATE              PROC [dbo].[spPyrolysis](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON
DECLARE @Currency as varchar(5), @Scenario as varchar(8)
SELECT @Currency = 'USD', @Scenario = 'Basis'

DECLARE @FurnConstructionYear real, @FurnResidenceTime real

SELECT 
  @FurnConstructionYear = [$(DbGlobal)].dbo.WtAvgNZ(q.FurnConstruction_Year, d.FreshPyroFeed_kMT)
, @FurnResidenceTime = [$(DbGlobal)].dbo.WtAvgNZ(q.ResidenceTime_s, d.FreshPyroFeed_kMT)
FROM fact.QuantityFeedAttributes q JOIN dbo.Divisors d on d.Refnum=q.Refnum 
WHERE d.Refnum = @Refnum and q.FactorSetId = @FactorSetId

DELETE FROM dbo.Pyrolysis WHERE Refnum = @Refnum
Insert into dbo.Pyrolysis(Refnum
, Scenario
, Currency
, SimModelId
, DataType
, C2H4_OSOP
, C3H6_OSOP
, OthHVChem_OSOP
, H2_OSOP
, C2H2_OSOP
, C4H6_OSOP
, C6H6_OSOP
, C2H4_MS25
, C3H6_MS25
, OthHVChem_MS25
, H2_MS25
, C4H6_MS25
, C6H6_MS25
, C2H4_OS25
, C3H6_OS25
, OthHVChem_OS25
, H2_OS25
, C4H6_OS25
, C6H6_OS25
, C2H4YIR_MS25
, C2H4YIR_OS25
, C2H4YIR_OSOP
, C3H6YIR_MS25
, C3H6YIR_OS25
, C3H6YIR_OSOP
, ProdOlefinsYIR_MS25
, ProdOlefinsYIR_OS25
, ProdOlefinsYIR_OSOP
, ProdHVCYIR_MS25
, ProdHVCYIR_OS25
, ProdHVCYIR_OSOP
, PyroPV_Act_CurrPerMT
, PyroPV_MS25_CurrPerMT
, PyroPV_OS25_CurrPerMT
, PyroPV_OSOP_CurrPerMT
, ActPPV_PcntMS25
, ActPPV_PcntOS25
, ActPPV_PcntOSOP
, SeverityIndex
, FurnConstructionYear
, FurnResidenceTime
)

SELECT Refnum = @Refnum
, Scenario = p.ScenarioId
, Currency = p.CurrencyRpt
, SimModelId = p.SimModelId
, DataType = p.DataType
, C2H4_OSOP				= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.C2H4 ELSE 0 END)
, C3H6_OSOP				= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.C3H6 ELSE 0 END)
, OthHVChem_OSOP		= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.OthHVChem ELSE 0 END)
, H2_OSOP				= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.H2 ELSE 0 END)
, C2H2_OSOP				= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.C2H2 ELSE 0 END)
, C4H6_OSOP				= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.C4H6 ELSE 0 END)
, C6H6_OSOP				= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.C6H6 ELSE 0 END)
, C2H4_MS25				= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.C2H4 ELSE 0 END)
, C3H6_MS25				= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.C3H6 ELSE 0 END)
, OthHVChem_MS25		= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.OthHVChem ELSE 0 END)
, H2_MS25				= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.H2 ELSE 0 END)
, C4H6_MS25				= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.C4H6 ELSE 0 END)
, C6H6_MS25				= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.C6H6 ELSE 0 END)
, C2H4_OS25				= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.C2H4 ELSE 0 END)
, C3H6_OS25				= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.C3H6 ELSE 0 END)
, OthHVChem_OS25		= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.OthHVChem ELSE 0 END)
, H2_OS25				= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.H2 ELSE 0 END)
, C4H6_OS25				= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.C4H6 ELSE 0 END)
, C6H6_OS25				= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.C6H6 ELSE 0 END)
, C2H4YIR_MS25			= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.C2H4YIR ELSE 0 END)
, C2H4YIR_OS25			= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.C2H4YIR ELSE 0 END)
, C2H4YIR_OSOP			= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.C2H4YIR ELSE 0 END)
, C3H6YIR_MS25			= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.C3H6YIR ELSE 0 END)
, C3H6YIR_OS25			= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.C3H6YIR ELSE 0 END)
, C3H6YIR_OSOP			= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.C3H6YIR ELSE 0 END)
, ProdOlefinsYIR_MS25			= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.ProdOlefinsYIR ELSE 0 END)
, ProdOlefinsYIR_OS25			= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.ProdOlefinsYIR ELSE 0 END)
, ProdOlefinsYIR_OSOP			= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.ProdOlefinsYIR ELSE 0 END)
, ProdHVCYIR_MS25		= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.ProdHVCYIR ELSE 0 END)
, ProdHVCYIR_OS25		= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.ProdHVCYIR ELSE 0 END)
, ProdHVCYIR_OSOP		= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.ProdHVCYIR ELSE 0 END)
, PyroPV_Act_CurrPerMT  = SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.PyroPV_USDperMT ELSE 0 END)
, PyroPV_MS25_CurrPerMT  = SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.PyroPV_USDperMT ELSE 0 END)
, PyroPV_OS25_CurrPerMT  = SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.PyroPV_USDperMT ELSE 0 END)
, PyroPV_OSOP_CurrPerMT  = SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.PyroPV_USDperMT ELSE 0 END)
, ActPPV_PcntMS25		= SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.PyroPVPcntSOA ELSE 0 END)
, ActPPV_PcntOS25		= SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.PyroPVPcntSOA ELSE 0 END)
, ActPPV_PcntOSOP		= SUM(CASE WHEN p.OpCondId = 'OSOP' THEN p.PyroPVPcntSOA ELSE 0 END)
, SeverityIndex			= CASE WHEN SUM(CASE WHEN p.OpCondId = 'MS25' THEN 
			p.C2H4 ELSE 0 END) > 0 THEN SUM(CASE WHEN p.OpCondId = 'OS25' THEN p.C2H4 ELSE 0 END) / SUM(CASE WHEN p.OpCondId = 'MS25' THEN p.C2H4 ELSE 0 END) * 100 END
, FurnConstructionYear = @FurnConstructionYear
, FurnResidenceTime = @FurnResidenceTime
FROM dbo.PyroYieldCalc p
Where p.Refnum=@Refnum
Group by p.CurrencyRpt, p.ScenarioId, p.SimModelId, p.DataType

SET NOCOUNT OFF

















