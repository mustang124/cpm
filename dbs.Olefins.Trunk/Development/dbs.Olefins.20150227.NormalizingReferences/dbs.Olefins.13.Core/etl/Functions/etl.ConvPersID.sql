﻿
CREATE FUNCTION etl.ConvPersID
(
	@PersId	NVARCHAR(34)
)
RETURNS NVARCHAR(34)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(34) =
	CASE RTRIM(LTRIM(@PersId))

		--WHEN 'Tot'						THEN 'PlantTotal'

		WHEN 'Occ'						THEN 'OccSubTotal'
		WHEN 'OP'						THEN 'OccProc'
		WHEN 'OccOpProc'				THEN 'OccProcProc'
		WHEN 'OccOpUtil'				THEN 'OccProcUtil'
		WHEN 'OccOpOffsite'				THEN 'OccProcOffSite'
		WHEN 'OccOpRacks'				THEN 'OccProcTruckRail'
		WHEN 'OccOpMarine'				THEN 'OccProcMarine'
		WHEN 'OccOpRelief'				THEN 'OccProcRelief'
		WHEN 'OccOpTrng'				THEN 'OccProcTraining'
		WHEN 'OccOpOth'					THEN 'OccProcOther'
		
		WHEN 'OM'						THEN 'OccMaint'
		--WHEN ''						THEN 'OccMaintPlant'
		WHEN 'OccMaintRtn'				THEN 'OccMaintRoutine'
		WHEN 'OccMaintShop'				THEN 'OccMaintShop'
		WHEN 'OccMaintPlan'				THEN 'OccMaintPlan'
		WHEN 'OccMaintWhs'				THEN 'OccMaintWarehouse'
		WHEN 'OccMaintInsp'				THEN 'OccMaintInsp'
		WHEN 'OccMaintTrng'				THEN 'OccMaintTraining'
		WHEN 'OccMaintTA'				THEN 'OccMaintTaMaint'
		WHEN 'OccAnnRetube'				THEN 'OccMaintTaAnnRetube'
		
		WHEN 'OA'						THEN 'OccAdmin'
		WHEN 'OccAdmLab'				THEN 'OccAdminLab'
		WHEN 'OccAdmAcct'				THEN 'OccAdminAcct'
		WHEN 'OccAdmPurch'				THEN 'OccAdminPurchase'
		WHEN 'OccAdmSecurity'			THEN 'OccAdminSecurity'
		WHEN 'OccAdmClerical'			THEN 'OccAdminClerical'
		WHEN 'OccAdmOth'				THEN 'OccAdminOther'

		WHEN 'Mps'						THEN 'MpsSubTotal'
		WHEN 'MP'						THEN 'MpsProc'
		WHEN 'MpsProcess'				THEN 'MpsProcProc'

		WHEN 'MM'						THEN 'MpsMaint'
		--WHEN ''						THEN 'MpsMaintPlant'
		WHEN 'MpsMaintRtn'				THEN 'MpsMaintRoutine'
		WHEN 'MpsMaintShop'				THEN 'MpsMaintShop'
		WHEN 'MpsMaintPlan'				THEN 'MpsMaintPlan'
		WHEN 'MpsMaintWhs'				THEN 'MpsMaintWarehouse'
		WHEN 'MpsMaintTA'				THEN 'MpsMaintTaMaint'
		WHEN 'MpsAnnRetube'				THEN 'MpsMaintTaAnnRetube'
		
		WHEN 'MT'						THEN 'MpsTech'
		WHEN 'MpsTechProc'				THEN 'MpsTechProc'
		WHEN 'MpsTechEcon'				THEN 'MpsTechEcon'
		WHEN 'MpsTechRely'				THEN 'MpsTechRely'
		WHEN 'MpsTechInsp'				THEN 'MpsTechInsp'
		WHEN 'MpsTechAPC'				THEN 'MpsTechAPC'
		WHEN 'MpsTechEnvir'				THEN 'MpsTechEnviron'
		WHEN 'MpsTechOth'				THEN 'MpsTechOther'
		
		WHEN 'MA'						THEN 'MpsAdmin'
		WHEN 'ML'						THEN 'MpsAdmin'
		WHEN 'MpsLab'					THEN 'MpsAdminLab'
		WHEN 'MpsAdmAcct'				THEN 'MpsAdminAcct'
		WHEN 'MpsAdmMIS'				THEN 'MpsAdminMIS'
		WHEN 'MpsAdmPurch'				THEN 'MpsAdminPurchase'
		WHEN 'MpsAdmSecurity'			THEN 'MpsAdminSecurity'
		WHEN 'MpsAdmOth'				THEN 'MpsAdminOther'
		
		WHEN 'MG'						THEN 'MpsGeneral'
		WHEN 'MpsFeedStaff'				THEN 'MpsGenPurchase'
		WHEN 'MpsGA'					THEN 'MpsGenOther'

		WHEN 'BEA'						THEN 'BeaSubTotal'
		WHEN 'BEAProdPlan'				THEN 'BeaProdPlan'
		WHEN 'BEAProdPlanExperienced'	THEN 'BeaProdPlanExperienced'
		WHEN 'BEATrader'				THEN 'BeaTrader'
		WHEN 'BEADataAnalyst'			THEN 'BeaAnalyst'
		WHEN 'BEALogistics'				THEN 'BeaLogistics'
	END;
	
	RETURN @ResultVar;
	
END;