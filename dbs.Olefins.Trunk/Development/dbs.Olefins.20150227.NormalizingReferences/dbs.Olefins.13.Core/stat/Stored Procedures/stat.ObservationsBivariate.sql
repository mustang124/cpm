﻿CREATE PROCEDURE [stat].[ObservationsBivariate]
(
	@Population				stat.PopulationBivariate		READONLY,	--	Validated data
	@Relationship			CHAR (1)	= 'L',				--	Suspect Point Estimation Function
															--		M: Mean								ŷ = ∑y/n
															--		L: Linear							ŷ = a + b*x
															--		P: Power							ŷ = a * x^b
															--		E: Exponential						ŷ = a * e^(b*x)
															--		S: Saturation Growth Rate			ŷ = a * x / (b + x)
	@Confidence_Pcnt		FLOAT		= 90.0,				--	Stastical Test Confidence			[0.0, 100.0]
	@xCeiling				BIT			= 0,				--	X-axis is absolute ceiling
	@PearsonThreshold		FLOAT		= 0.90,				--	Pearson threshold					[0.0, 1.0]
	@ResidualThreshold		FLOAT		= 2.00				--	Residual identification threshold
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Number of tiles for 'Quartiling' distance away from expected value
	DECLARE @zTile	TINYINT = 4;

	--------------------------------------------------------------------------------
	--	Normalizing by-unit X

	DECLARE @Cnt	FLOAT	 = 0;
	DECLARE @Min	FLOAT	 = 0;
	DECLARE @Max	FLOAT	 = 0;
	DECLARE @p		FLOAT	 = 0;

	DECLARE @Basis	stat.PopulationBivariate;
	DECLARE @Audit	stat.PopulationBivariate;

	INSERT INTO @Basis(FactorSetId, Refnum, x, y, Basis, Audit)
	SELECT
		o.FactorSetId,
		o.Refnum,
		o.x,
		o.y,
		o.Basis,
		o.Audit
	FROM @Population	o
	WHERE o.Basis = 1
	ORDER BY o.x;

	INSERT INTO @Audit(FactorSetId, Refnum, x, y, Basis, Audit)
	SELECT
		o.FactorSetId,
		o.Refnum,
		o.x,
		o.y,
		o.Basis,
		o.Audit
	FROM @Population	o
	ORDER BY o.x;

	SELECT
		@Cnt = COUNT(a.x),
		@Min = MIN(a.x),
		@Max = MIN(a.x),
		@p = 0
	FROM @Audit a
	WHERE a.Basis = 1;

	IF @Min = 1 AND @Max = 1
	BEGIN

		UPDATE @Basis
		SET @p = x = @p + x / @Cnt;

		UPDATE @Audit
		SET @p = x = @p + x / @Cnt
		WHERE Basis = 1;

		SET @xCeiling = 0;
		SET @Relationship = 'M';

	END;

	SELECT
		@Cnt = COUNT(a.x),
		@Min = MIN(a.x),
		@Max = MIN(a.x),
		@p = 0
	FROM @Audit a
	WHERE a.Audit = 1;

	IF @Min = 1 AND @Max = 1
	BEGIN

		UPDATE @Audit
		SET @p = x = @p + x / @Cnt
		WHERE Audit = 1;

	END;

	--------------------------------------------------------------------------------
	DECLARE @b0				FLOAT;			--	First Coefficient or Term
	DECLARE @b1				FLOAT;			--	Second Coefficient or Term

	DECLARE	@xMean			FLOAT;			--	Mean of linearlized x values
	DECLARE	@yMean			FLOAT;			--	Mean of Y for @b1e, Mean, Pearson
	DECLARE	@xStDev			FLOAT;			--	Standard Deviation, x
	DECLARE	@yStDev			FLOAT;			--	Standard Deviation, y

	DECLARE @SSE			FLOAT;			--	Sum of Squares due to Error (Error Sum of Squares)
	DECLARE @SSR			FLOAT;			--	Sum of Squares due to Regression (Regression Sum of Squares)
	DECLARE @SST			FLOAT;			--	Sum of Squares (Total: SST = SSE + SSR)
	DECLARE @MSE			FLOAT;			--	Mean Square Error (Estimates Population Variance (σ²) or Sample Variance (s²)) 
	DECLARE @MSR			FLOAT;			--	Mean Square Regression

	DECLARE	@COV			FLOAT;			--	Covariance
	DECLARE @COD			FLOAT;			--	Coefficient of Determination (r²)
	DECLARE @COR			FLOAT;			--	Correlation Coefficient (r)

	DECLARE @DFn			INT;			--	Degress of Freedom in the Numerator (Number of Independent Variables)
	DECLARE @DFd			INT;			--	Degress of Freedom in the Denominator (Number of Records) 
	DECLARE @tAlpha			FLOAT;			--	Confidence conversion for T-Test alpha paramater
	DECLARE @tStat			FLOAT;			--	Student T-Test Statistic for Intervals

	DECLARE	@StDev			FLOAT;			--	Standard Deviation of the dependent values about the function
	DECLARE	@SumXiX			FLOAT;			--	Sum of xi - xbar (Σ(xi-AVG(x))
	DECLARE @B1Var			FLOAT;			--	@b1 Variance
	DECLARE	@B1StDev		FLOAT;			--	T-Test Denominator

	DECLARE	@tTest			FLOAT;			--	Student T-Test
	DECLARE	@tConfidence	FLOAT;			--	Student T-Test Confidence
	DECLARE	@tStatistic		FLOAT;			--	Student T-Test Statistic
	DECLARE @tHypothesis	BIT;			--	Accept (1) or Reject (0) Student T-Test

	DECLARE	@fTest			FLOAT;			--	Fisher Test
	DECLARE	@fConfidence	FLOAT;			--	Fisher Test Confidence
	DECLARE	@fStatistic		FLOAT;			--	Fisher Test Statistic
	DECLARE @fHypothesis	BIT;			--	Accept (1) or Reject (0) Fisher Test

	DECLARE @cTest			FLOAT;			--	Chi-Squared Test
	DECLARE	@cConfidence	FLOAT;			--	Chi-Squared Confidence
	DECLARE @cStatistic		FLOAT;			--	Chi-Squared Statistic
	DECLARE @cHypothesis	BIT;			--	Accept (1) or Reject (0) Chi-Squared Test

	DECLARE @Tau			FLOAT;			--	Kendall rank correlation coefficient
	DECLARE @Rho			FLOAT;			--	Spearman's rank correlation coefficient
	DECLARE @PMCC			FLOAT;			--	Pearson product-moment correlation coefficient (PMCC)
	DECLARE	@StdAway		FLOAT;			--	Calculate the number of Standard Deviations to set the Min/Max validation range 

	DECLARE @Chart TABLE
	(
		[FactorSetId]			VARCHAR (12)	NOT	NULL		CHECK(LEN([FactorSetId]) >  0),
		[Refnum]				VARCHAR (18)	NOT	NULL		CHECK(LEN([Refnum])		 >  0),
		[x]						FLOAT			NOT	NULL,
		[y]						FLOAT			NOT	NULL,
		[Basis]					BIT				NOT	NULL,
		[Audit]					BIT				NOT	NULL,

		[Estimate]				FLOAT			NOT	NULL,
		[zTest]					FLOAT			NOT	NULL,
		[zNormal]				FLOAT				NULL,
		[zTile]					TINYINT				NULL		CHECK([zTile] BETWEEN 1 AND 4),

		[_ErrorAbsolute]		AS CAST([y] - [Estimate]		AS FLOAT	)
								PERSISTED		NOT	NULL,
		[_ErrorRelative]		AS CAST(1.0 - [Estimate] / [y]	AS FLOAT	)
								PERSISTED		NOT	NULL,

		[ResidualStandard]		FLOAT			NOT	NULL,
		[ResidualAbsolute]		FLOAT			NOT	NULL		CHECK([ResidualAbsolute] BETWEEN 0.0 AND 1.0),
		[ResidualRelative]		FLOAT			NOT	NULL		CHECK([ResidualRelative] BETWEEN 0.0 AND 1.0),

		[ResidualThreshold]		FLOAT			NOT	NULL		CHECK([ResidualThreshold]	>  0.0),
		[_BasisResidualStdIn]	AS CAST(CASE WHEN [Basis] = 1 AND ABS([ResidualStandard]) <  [ResidualThreshold] THEN [ResidualStandard] END	AS FLOAT	)
								PERSISTED				,
		[_BasisResidualStdOut]	AS CAST(CASE WHEN [Basis] = 1 AND ABS([ResidualStandard]) >= [ResidualThreshold] THEN [ResidualStandard] END	AS FLOAT	)
								PERSISTED				,
		[_AuditResidualStdIn]	AS CAST(CASE WHEN [Audit] = 1 AND ABS([ResidualStandard]) <  [ResidualThreshold] THEN [ResidualStandard] END	AS FLOAT	)
								PERSISTED				,
		[_AuditResidualStdOut]	AS CAST(CASE WHEN [Audit] = 1 AND ABS([ResidualStandard]) >= [ResidualThreshold] THEN [ResidualStandard] END	AS FLOAT	)
								PERSISTED				,

		[Confidence]			FLOAT			NOT	NULL			CHECK([Confidence]	>  0.0),
		[_ConfidenceMin]		AS CAST([Estimate] - [Confidence]	AS FLOAT	)
								PERSISTED		NOT	NULL,
		[_ConfidenceMax]		AS CAST([Estimate] + [Confidence]	AS FLOAT	)
								PERSISTED		NOT	NULL,

		[Prediction]			FLOAT			NOT	NULL			CHECK([Prediction]	>  0.0),
		[_PredictionMin]		AS CAST([Estimate] - [Prediction]	AS FLOAT	)
								PERSISTED		NOT	NULL,
		[_PredictionMax]		AS CAST([Estimate] + [Prediction]	AS FLOAT	)
								PERSISTED		NOT	NULL,

		[BasisRank]				INT					NULL		CHECK([BasisRank]	>  0.0),
		[_BasisQ1]				AS CAST(CASE WHEN [Basis] = 1 AND [zTile] = 1 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Green
		[_BasisQ2]				AS CAST(CASE WHEN [Basis] = 1 AND [zTile] = 2 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Blue
		[_BasisQ3]				AS CAST(CASE WHEN [Basis] = 1 AND [zTile] = 3 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Yellow
		[_BasisQ4]				AS CAST(CASE WHEN [Basis] = 1 AND [zTile] = 4 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Red

		[AuditRank]				INT					NULL		CHECK([AuditRank]	>  0),
		[_AuditQ1]				AS CAST(CASE WHEN [Audit] = 1 AND [zTile] = 1 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Green
		[_AuditQ2]				AS CAST(CASE WHEN [Audit] = 1 AND [zTile] = 2 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Blue
		[_AuditQ3]				AS CAST(CASE WHEN [Audit] = 1 AND [zTile] = 3 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Yellow
		[_AuditQ4]				AS CAST(CASE WHEN [Audit] = 1 AND [zTile] = 4 THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Red

		[PearsonThreshold]		FLOAT			NOT	NULL		CHECK([PearsonThreshold] BETWEEN 0.0 AND 1.0),
		[_SuspectThreshold]		AS CAST(CASE WHEN [Audit] = 1 AND ([ResidualAbsolute] >= [PearsonThreshold] OR [ResidualRelative] >= [PearsonThreshold]) THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Ring (Black)

		[xCeiling]				BIT				NOT	NULL,
		[_SuspectCeiling]		AS CAST(CASE WHEN [Audit] = 1 AND [xCeiling] = 1 AND [y] > [x] THEN [y] END	AS FLOAT	)
								PERSISTED				,	--	Ring (Red)

		PRIMARY KEY CLUSTERED ([x] ASC, [FactorSetId] DESC, [Refnum] DESC, [Basis] ASC, [Audit] ASC)
	);

	--------------------------------------------------------------------------------

	SELECT @DFd = COUNT(1) FROM @Basis;
	SET @DFn = 1;

	-- Normal form of 100.0 - (100.0 - @Confidence_Pcnt) / 2.0;
	SET @tAlpha = 50 + @Confidence_Pcnt / 2.0;	
	SET @tStat = stat.Student_Statistic(@DFd - 2, @tAlpha, 2);

	--------------------------------------------------------------------------------
	--	Mean: ŷ = avg(x)
	IF @Relationship = 'M'
	BEGIN

		SELECT
			@xMean = AVG(b.x),
			@yMean = AVG(b.y),
			@xStDev = STDEVP(b.x),
			@yStDev = STDEVP(b.y),
			@b0 = AVG(b.y),
			@b1 = 0.0
		FROM	@Basis b;
	
		SELECT	@SumXiX = SUM(SQUARE(b.y - @xMean))
		FROM @Basis b;

		SELECT
			@SST = SUM(b.SST),
			@SSR = SUM(b.SST) - SUM(b.SSE),
			@SSE = SUM(b.SSE),
			@COD = SUM(b.SSE) / SUM(b.SST),
			@MSE = SUM(b.SSE) / CAST(COUNT(1) - 2 AS FLOAT	)
		FROM (
			SELECT
				SQUARE(b.y - AVG(b.y) OVER())					[SSE],
				SQUARE(b.y					- AVG(b.y) OVER())	[SST]
			FROM @Basis b
			) b;

		SET @StDev = SQRT(@MSE);

		------------------------------------------------------------------------------
		-- Update calculatable fields
		--	• Expected values
		--	• Z-Test based on regression
		--	• Relative Error Percentiles: ((y - ŷ) / y) ^ 2
		--	• Absolute Error Percentiles: (y - ŷ) ^ 2
		--	• Confidence Interval
		--	• Prediction Interval
		--	• Standardized Residuals

		INSERT INTO @Chart(FactorSetId, Refnum, x, y, Basis, Audit,
			ResidualThreshold,
			PearsonThreshold,
			xCeiling,

			Estimate,
			zTest,
			ResidualStandard,
			ResidualAbsolute,
			ResidualRelative,

			Confidence,
			Prediction
		)
		SELECT
			a.FactorSetId,
			a.Refnum,
			a.x,
			a.y,
			a.Basis,
			a.Audit,

			@ResidualThreshold,
			@PearsonThreshold,
			@xCeiling,

					@yMean																			[Estimate],
			CASE WHEN @StDev <> 0.0 THEN
			(a.y -	@yMean) / @yStDev	END															[zTest],

			(a.y -	@yMean) /
			SQRT(SQUARE(@StDev) * (1.0 - 1.0/@DFd - SQUARE(a.x - @xMean) / @SumXiX))				[ResidualStandard],
			
			(SELECT COUNT(1) FROM @Basis b
				WHERE	SQUARE(b.y - @yMean)
					<=	SQUARE(a.y - @yMean)
				)	/	CAST(COUNT(1) OVER () AS FLOAT	)										[ResidualAbsolute],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	CASE WHEN b.y <> 0.0 THEN SQUARE((b.y - @yMean) / b.y) ELSE 1.0 END
					<=	CASE WHEN a.y <> 0.0 THEN SQUARE((a.y - @yMean) / a.y) ELSE 1.0 END
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)										[ResidualRelative],


			@tStat * SQRT(SQUARE(@StDev) * (	  1.0/@DFd + SQUARE(a.x - @xMean) / @SumXiX))		[Confidence],
			@tStat * SQRT(SQUARE(@StDev) * (1.0 + 1.0/@DFd + SQUARE(a.x - @xMean) / @SumXiX))		[Prediction]

		FROM @Audit a;

	END;

	--------------------------------------------------------------------------------
	--	Linear: ŷ = a + bx
	--	• Intercept (a): b0
	--	• Slope (b): b1
	IF @Relationship = 'L'
	BEGIN

		SELECT
			@b1 = CASE WHEN (SUM(SQUARE(b.x)) - SQUARE(SUM(b.x)) / CAST(COUNT(1) AS FLOAT	)) <> 0
				THEN
					(CAST(COUNT(1) AS FLOAT	) * SUM(b.x * b.y) - SUM(b.x) * SUM(b.y)) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(b.x)) - SQUARE(SUM(b.x)))
				ELSE
					0.0
				END,
			@xMean = AVG(b.x),
			@yMean = AVG(b.y),
			@xStdev = STDEVP(b.x),
			@yStdev = STDEVP(b.y)
		FROM @Basis b;

		SELECT
			@b0 = AVG(b.y) - @b1 * AVG(b.x)
		FROM @Basis b;

		SELECT
			@SST = SUM(B.SST),
			@SSR = SUM(B.SSR),
			@SSE = SUM(B.SSE),
			@MSE = SUM(B.SSE) / CAST(COUNT(1) - 2 AS FLOAT	),
			@SumXiX = SUM(B.SumXiX),
			@COV = SUM(B.COV)
		FROM (
			SELECT
				SQUARE(b.y - (@b0 + @b1 * b.x))						[SSE],
				SQUARE(b.y						- AVG(b.y) OVER())	[SST],
				SQUARE(		 (@b0 + @b1 * b.x)	- AVG(b.y) OVER())	[SSR],
				SQUARE(b.x						- AVG(b.x) OVER())	[SumXiX],
				(b.x - AVG(b.x) OVER()) * (b.y - AVG(b.y) OVER())	[COV]
			FROM @Basis b
			) B;
			
		SET @StDev = SQRT(@MSE);

		------------------------------------------------------------------------------
		-- Update calculatable fields

		INSERT INTO @Chart(FactorSetId, Refnum, x, y, Basis, Audit,
			ResidualThreshold,
			PearsonThreshold,
			xCeiling,

			Estimate,
			zTest,
			ResidualStandard,
			ResidualAbsolute,
			ResidualRelative,

			Confidence,
			Prediction
		)
		SELECT
			a.FactorSetId,
			a.Refnum,
			a.x,
			a.y,
			a.Basis,
			a.Audit,

			@ResidualThreshold,
			@PearsonThreshold,
			@xCeiling,

					(@b0 + @b1 * a.x)																		[Estimate],
			CASE WHEN @StDev <> 0.0 THEN
			(a.y -	(@b0 + @b1 * a.x)) / @StDev ELSE 0.0 END												[zTest],
			
			(a.y -	(@b0 + @b1 * a.x)) /
			SQRT(SQUARE(@StDev) * (1.0 - 1.0/@DFd - SQUARE(a.x - @xMean) / @SumXiX))						[ResidualStandard],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	SQUARE(b.y - (@b0 + @b1 * b.x))
					<=	SQUARE(a.y - (@b0 + @b1 * a.x))
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)												[ResidualAbsolute],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	CASE WHEN b.y <> 0.0 THEN SQUARE((b.y - (@b0 + @b1 * b.x)) / b.y) ELSE 1.0 END
					<=	CASE WHEN a.y <> 0.0 THEN SQUARE((a.y - (@b0 + @b1 * a.x)) / a.y) ELSE 1.0 END
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)												[ResidualRelative],
		
			@tStat * SQRT(SQUARE(@StDev) * (	  1.0/@DFd + SQUARE(a.x - @xMean) / @SumXiX))				[Confidence],
			@tStat * SQRT(SQUARE(@StDev) * (1.0 + 1.0/@DFd + SQUARE(a.x - @xMean) / @SumXiX))				[Prediction]

		FROM @Audit a;

	END;

	--------------------------------------------------------------------------------
	--	Exponential: ŷ = a * EXP(b*x)
	--	• Coefficient (a): b0
	--	• Coefficient (b): b1
	If @Relationship = 'E'
	BEGIN

		SELECT
			@b1 = CASE WHEN (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(b.x)) - SQUARE(SUM(b.x))) <> 0
				THEN
					(CAST(COUNT(1) AS FLOAT	) * SUM(b.x * LOG(b.y)) - (SUM(b.x) * SUM(LOG(b.y)))) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(b.x)) - SQUARE(SUM(b.x)))
				ELSE
					0.0
				END,
			@xMean = AVG(b.x),
			@yMean = AVG(LOG(b.y)),
			@xStdev = STDEVP(b.x),
			@yStdev = STDEVP(LOG(b.y))
		FROM @Basis b
		WHERE b.y <> 0.0;

		SELECT	@b0 = EXP(AVG(LOG(b.y)) - @b1 * AVG(b.x))
		FROM @Basis b
		WHERE b.y <> 0.0;

		SELECT
			@SST = SUM(B.SSE) + SUM(B.SSR),
			@SSR = SUM(B.SSR),
			@SSE = SUM(B.SSE),
			@MSE = SUM(B.SSE) / CAST(COUNT(1) - 2.0 AS FLOAT	),
			@SumXiX = SUM(B.SumXiX),
			@COV = SUM(B.COV)
		FROM (
			SELECT
				SQUARE(b.y - EXP(LOG(@b0) + @b1 * b.x))						[SSE],
				SQUARE(		 EXP(LOG(@b0) + @b1 * b.x)	- AVG(b.y) OVER())	[SSR],
				SQUARE(b.x - @xMean)										[SumXiX],
				(b.x - AVG(b.x) OVER()) * (b.y - AVG(b.y) OVER())			[COV]
			FROM @Basis b
			WHERE b.y <> 0.0
			) B;
			
		SET @StDev = SQRT(@MSE);

		------------------------------------------------------------------------------
		-- Update calculatable fields

		INSERT INTO @Chart(FactorSetId, Refnum, x, y, Basis, Audit,
			ResidualThreshold,
			PearsonThreshold,
			xCeiling,

			Estimate,
			zTest,
			ResidualStandard,
			ResidualAbsolute,
			ResidualRelative,

			Confidence,
			Prediction
		)
		SELECT
			a.FactorSetId,
			a.Refnum,
			a.x,
			a.y,
			a.Basis,
			a.Audit,

			@ResidualThreshold,
			@PearsonThreshold,
			@xCeiling,

					EXP(LOG(@b0) + @b1 * a.x)																[Estimate],
			CASE WHEN @StDev <> 0.0 THEN
			(a.y -	EXP(LOG(@b0) + @b1 * a.x)) / @StDev	END													[zTest],

			(a.y -	EXP(LOG(@b0) + @b1 * a.x)) /
			SQRT(SQUARE(@StDev) * (1.0 - 1.0/@DFd - SQUARE(a.x - @xMean) / @SumXiX))						[ResidualStandard],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	SQUARE(LOG(b.y) - EXP(LOG(@b0) + @b1 * b.x))
					<=	SQUARE(LOG(a.y) - EXP(LOG(@b0) + @b1 * a.x))
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)												[ResidualAbsolute],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	CASE WHEN b.y <> 0.0 THEN SQUARE((LOG(b.y) - EXP(LOG(@b0) + @b1 * b.x)) / LOG(b.y)) ELSE 1.0 END
					<=	CASE WHEN a.y <> 0.0 THEN SQUARE((LOG(a.y) - EXP(LOG(@b0) + @b1 * a.x)) / LOG(a.y)) ELSE 1.0 END
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)												[ResidualRelative],

			@tStat * SQRT(SQUARE(@StDev) * (	  1.0/@DFd + SQUARE(a.x - @xMean) / @SumXiX))				[Confidence],
			@tStat * SQRT(SQUARE(@StDev) * (1.0 + 1.0/@DFd + SQUARE(a.x - @xMean) / @SumXiX))				[Prediction]

		FROM @Audit a;

	END;

	--------------------------------------------------------------------------------
	--	Power: ŷ = a * x ^ b
	--	• Coefficient (a): b0
	--	• Exponent (b): b1
	IF @Relationship = 'P'
	BEGIN

		SELECT
			@b1 = CASE WHEN (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(LOG10(b.x))) - SQUARE(SUM(LOG10(b.x)))) <> 0
				THEN
					(CAST(COUNT(1) AS FLOAT	) * SUM(LOG10(b.x) * LOG10(b.y)) - (SUM(LOG10(b.x)) * SUM(LOG10(b.y)))) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(LOG10(b.x))) - SQUARE(SUM(LOG10(b.x))))
				ELSE
					0.0
				END,
			@xMean = AVG(LOG10(b.x)),
			@yMean = AVG(LOG10(b.y)),
			@xStdev = STDEVP(LOG10(b.x)),
			@yStdev = STDEVP(LOG10(b.y))
		FROM @Basis b
		WHERE b.x <> 0.0 AND b.y <> 0.0;

		SELECT	@b0 = POWER(10.0, (AVG(LOG10(b.y)) - @b1 * AVG(LOG10(b.x))))
		FROM @Basis b
		WHERE b.x <> 0.0 AND b.y <> 0.0;

		SELECT
			@SST = SUM(B.SSE) +  SUM(B.SSR),
			@SSR = SUM(B.SSR),
			@SSE = SUM(B.SSE),
			@MSE = SUM(B.SSE) / CAST(COUNT(1) - 2.0 AS FLOAT	),
			@SumXiX = SUM(B.SumXiX)
		FROM (
			SELECT
				SQUARE(b.y - POWER(10.0, LOG10(@b0) + @b1 * LOG10(b.x)))					[SSE],
				SQUARE(		 POWER(10.0, LOG10(@b0) + @b1 * LOG10(b.x))	- AVG(b.y) OVER())	[SSR],
				SQUARE(LOG10(b.x) - @xMean)													[SumXiX]
			FROM @Basis b
			WHERE b.y <> 0
			) B;

		SET @StDev = SQRT(@MSE);	

		------------------------------------------------------------------------------
		-- Update calculatable fields

		INSERT INTO @Chart(FactorSetId, Refnum, x, y, Basis, Audit,
			ResidualThreshold,
			PearsonThreshold,
			xCeiling,

			Estimate,
			zTest,
			ResidualStandard,
			ResidualAbsolute,
			ResidualRelative,

			Confidence,
			Prediction
		)
		SELECT
			a.FactorSetId,
			a.Refnum,
			a.x,
			a.y,
			a.Basis,
			a.Audit,

			@ResidualThreshold,
			@PearsonThreshold,
			@xCeiling,

					POWER(10.0, LOG10(@b0) + @b1 * LOG10(a.x))												[Estimate],
			CASE WHEN @StDev <> 0.0 THEN
			(a.y -	POWER(10.0, LOG10(@b0) + @b1 * LOG10(a.x))) / @StDev END								[zTest],

			(a.y -	POWER(10.0, LOG10(@b0) + @b1 * LOG10(a.x))) /
			SQRT(SQUARE(@StDev) * (1.0 - 1.0/@DFd - SQUARE(LOG10(a.x) - @xMean) / @SumXiX))					[ResidualStandard],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	CASE WHEN b.x <> 0.0 AND b.y <> 0.0 THEN SQUARE(LOG10(b.y) - (LOG10(@b0) + @b1 * LOG10(b.x))) ELSE 1.0 END
					<=	CASE WHEN a.x <> 0.0 AND a.y <> 0.0 THEN SQUARE(LOG10(a.y) - (LOG10(@b0) + @b1 * LOG10(a.x))) ELSE 1.0 END
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)												[ResidualAbsolute],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	CASE WHEN b.x <> 0.0 AND b.y <> 0.0 THEN SQUARE((LOG10(b.y) - (LOG10(@b0) + @b1 * LOG10(b.x))) / LOG10(b.y)) ELSE 1.0 END
					<=	CASE WHEN a.x <> 0.0 AND a.y <> 0.0 THEN SQUARE((LOG10(a.y) - (LOG10(@b0) + @b1 * LOG10(a.x))) / LOG10(a.y)) ELSE 1.0 END
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)												[ResidualRelative],

			@tStat * SQRT(SQUARE(@StDev) * (	  1.0/@DFd + SQUARE(LOG10(a.x) - @xMean) / @SumXiX))		[Confidence],
			@tStat * SQRT(SQUARE(@StDev) * (1.0 + 1.0/@DFd + SQUARE(LOG10(a.x) - @xMean) / @SumXiX))		[Prediction]

		FROM @Audit a;

	END;

	--------------------------------------------------------------------------------
	--	Saturation Growth Rate: ŷ = a * x / (b + x)
	--	• Coefficient (a): b0
	--	• Denominator (b): b1
	IF @Relationship = 'S'
	BEGIN

		SELECT
			@b1 = CASE WHEN (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(1.0/b.x)) - SQUARE(SUM(1.0/b.x))) <> 0
				THEN
					(CAST(COUNT(1) AS FLOAT	) * SUM(1.0/b.x * 1.0/b.y) - (SUM(1.0/b.x) * SUM(1.0/b.y))) / (CAST(COUNT(1) AS FLOAT	) * SUM(SQUARE(1.0/b.x)) - SQUARE(SUM(1.0/b.x)))
				ELSE
					0.0
				END,
			@xMean = AVG(1.0/b.x),
			@xMean = AVG(1.0/b.y),
			@xStDev = STDEVP(1.0/b.x),
			@yStDev = STDEVP(1.0/b.y)
		FROM @Basis b
		WHERE b.x <> 0 AND b.y <> 0.0;
		
		SELECT
			@b0 = 1.0/((AVG(1.0/b.y) - @b1 * AVG(1.0/b.x)))
		FROM @Basis b
		WHERE b.x <> 0.0 AND b.y <> 0.0
		HAVING
			((AVG(1.0/b.y) - @b1 * AVG(1.0/b.x))) <> 0.0;

		IF (@b0 IS NULL) 
		BEGIN
			RAISERROR('Data for Saturation Growth Rate produces a NULL function', 17, 0) WITH NOWAIT, SETERROR;
			RETURN
		END;

		SELECT
			@SST = SUM(B.SSE) +  SUM(B.SSR),
			@SSR = SUM(B.SSR),
			@SSE = SUM(B.SSE),
			@MSE = SUM(B.SSE) / CAST(COUNT(1) - 2.0 AS FLOAT	),
			@SumXiX = SUM(B.SumXiX)
		FROM (
			SELECT
				SQUARE(b.y - (1.0/(1.0/@b0 + @b1 * (1.0/b.x))))						[SSE],
				SQUARE(		 (1.0/(1.0/@b0 + @b1 * (1.0/b.x)))	- AVG(b.y) OVER())	[SSR],
				SQUARE(1.0 / b.x - @xMean)											[SumXiX]
			FROM @Basis b
			WHERE b.x <> 0.0
			) B;
		
		SET @StDev = SQRT(@MSE);

		------------------------------------------------------------------------------
		-- Update calculatable fields

		INSERT INTO @Chart(FactorSetId, Refnum, x, y, Basis, Audit,
			ResidualThreshold,
			PearsonThreshold,
			xCeiling,

			Estimate,
			zTest,
			ResidualStandard,
			ResidualAbsolute,
			ResidualRelative,

			Confidence,
			Prediction
		)
		SELECT
			a.FactorSetId,
			a.Refnum,
			a.x,
			a.y,
			a.Basis,
			a.Audit,

			@ResidualThreshold,
			@PearsonThreshold,
			@xCeiling,

					1.0/(1.0/@b0 + @b1 * (1.0/a.x))																	[Estimate],
			CASE WHEN @StDev <> 0.0 THEN
			(a.y -	1.0/(1.0/@b0 + @b1 * (1.0/a.x))) / @StDev	END													[zTest],

			(a.y - 1.0/(1.0/@b0 + @b1 * (1.0/a.x))) /
			SQRT(SQUARE(@StDev) * (1.0 - 1.0/@DFd - SQUARE((1.0/a.x) - @xMean) / @SumXiX))							[ResidualStandard],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	CASE WHEN b.x <> 0.0 AND b.y <> 0.0 THEN SQUARE(b.y - 1.0/(1.0/@b0 + @b1 * (1.0/b.x))) ELSE 1.0 END
					<=	CASE WHEN a.x <> 0.0 AND a.y <> 0.0 THEN SQUARE(a.y - 1.0/(1.0/@b0 + @b1 * (1.0/a.x))) ELSE 1.0 END
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)														[ResidualAbsolute],

			(SELECT COUNT(1) FROM @Basis b
				WHERE	CASE WHEN b.x <> 0.0 AND b.y <> 0.0 THEN SQUARE((b.y - 1.0/(1.0/@b0 + @b1 * (1.0/b.x))) / b.y) ELSE 1.0 END
					<=	CASE WHEN a.x <> 0.0 AND a.y <> 0.0 THEN SQUARE((a.y - 1.0/(1.0/@b0 + @b1 * (1.0/a.x))) / a.y) ELSE 1.0 END
				)	 /	CAST(COUNT(1) OVER () AS FLOAT	)														[ResidualRelative],

			@tStat * SQRT(SQUARE(@StDev) * (	  1.0/@DFd + SQUARE(1.0/a.x - @xMean) / @SumXiX))					[Confidence],
			@tStat * SQRT(SQUARE(@StDev) * (1.0 + 1.0/@DFd + SQUARE(1.0/a.x - @xMean) / @SumXiX))					[Prediction]
			
		FROM @Audit a;

	END;

	--------------------------------------------------------------------------------
	--	Update for all methods

	SET	@B1Var = @MSE / @SumXiX;
	SET	@B1StDev = SQRT(@B1Var);
	
	SET	@COR = CASE WHEN @b1 <> 0.0 THEN ABS(@b1) / @b1 * SQRT(@SSR / @SST) END;
	SET	@COD = CASE WHEN @SST <> 0.0 THEN @SSR / @SST END;
	SET	@MSR = @SSR / CAST(@DFd AS FLOAT	);
	SET	@StdAway = CASE WHEN @SSR <> 0.0 THEN @SST / @SSR END;

	--------------------------------------------------------------------------------
	--	Regression Analysis for Normal Probability chart, calculate Z-Test statistic
	--	to identify discrepant observations in the normal probability chart.

	DECLARE @rMSE			FLOAT	;

	SELECT	@rMSE = SUM(SQUARE(c.ResidualStandard - c.zTest)) / CAST(COUNT(1) - 2.0 AS FLOAT	)
	FROM	@Chart c
	WHERE	c.Basis = 1
		AND	ABS(c.ResidualStandard - c.zTest) < 0.35;

	UPDATE	c
	SET	zTile = t.zTile,
		zNormal = CASE WHEN @rMSE <> 0.0 THEN (c.ResidualStandard - c.zTest) / SQRT(@rMSE) END,
		BasisRank = CASE WHEN c.Basis = 1 THEN t.bRank END,
		AuditRank = CASE WHEN c.Audit = 1 THEN t.aRank END
	FROM @Chart	c
	INNER JOIN (
		SELECT
			o.FactorSetId,
			o.Refnum,
			o.x,
			NTILE(@zTile) OVER(ORDER BY ABS(o.zTest) ASC)			[zTile],
			RANK() OVER(PARTITION BY o.Basis ORDER BY o.x ASC)		[bRank],
			RANK() OVER(PARTITION BY o.Audit ORDER BY o.x ASC)		[aRank]
		FROM @Chart o
		) t
		ON	t.FactorSetId = c.FactorSetId
		AND	t.Refnum = c.Refnum
		AND	t.x = c.x;

	------------------------------------------------------------------------------
	--	Student T-Test

	SET	@tTest = CASE WHEN @B1StDev <> 0.0 THEN ABS(@b1 / @B1StDev) ELSE 0.0 END;
	SET	@tConfidence = stat.Student_Probability(@DFd, @tTest, 2);
	SET	@tStatistic = stat.Student_Statistic(@DFd, @Confidence_Pcnt, 2);
	SET	@tHypothesis = CASE WHEN @tTest > @tStatistic THEN 1 ELSE 0 END;

	------------------------------------------------------------------------------
	--	Fisher Test

	SET	@fTest = CASE WHEN @MSE <> 0.0 THEN @MSR / @MSE ELSE 0.0 END;
	SET	@fConfidence = stat.Fisher_Probability(@DFn, @DFd, @fTest);
	SET	@fStatistic = stat.Fisher_Statistic(@DFn, @DFd, @Confidence_Pcnt);
	SET	@fHypothesis = CASE WHEN @fTest > @fStatistic THEN 1 ELSE 0 END;

	--------------------------------------------------------------------------------
	--	Spearman's rank correlation coefficient (Rho)
    --	If X and Y are independent, then the coefficient is approximately zero.
	--	Pearson product-moment correlation coefficient (PMCC)
	--	Chi-Squared Test
 
	SELECT
		@Rho = SUM((r.x - @xMean) * (r.y - @yMean))
					/ SQRT(
						CASE WHEN SUM(SQUARE(r.x - @xMean)) * SUM(SQUARE(r.y - @yMean)) <> 0.0
						THEN SUM(SQUARE(r.x - @xMean)) * SUM(SQUARE(r.y - @yMean))
						END),
		@PMCC = SUM((r.x - @xMean) * (r.y - @yMean)) / @DFd / CASE WHEN (@xStdev * @yStdev) <> 0.0 THEN (@xStdev * @yStdev) END,
		@cTest = SUM(CASE WHEN r.Estimate <> 0.0 THEN SQUARE(r.y - r.Estimate) / r.Estimate END)
	FROM	@Chart r
	WHERE r.Basis = 1;

	SET	@cConfidence = stat.ChiSquared_Probability(@DFd, @cTest);
	SET	@cStatistic = stat.ChiSquared_Statistic(@DFd, @Confidence_Pcnt);
	SET	@cHypothesis = CASE WHEN @cTest > @cStatistic THEN 1 ELSE 0 END;

	--------------------------------------------------------------------------------
	--	Kendall rank correlation coefficient (tau)
    --	If the agreement between the two rankings is perfect (i.e., the two rankings are the same) the coefficient has value 1.
    --	If the disagreement between the two rankings is perfect (i.e., one ranking is the reverse of the other) the coefficient has value −1.
    --	If X and Y are independent, then the coefficient is approximately zero.
	
	SELECT @Tau = (SUM(CASE WHEN      (r1.x > r2.x AND r1.y > r2.y) OR     (r1.x < r2.x AND r1.y < r2.y) THEN 1.0 ELSE 0.0 END) -
		SUM(CASE WHEN NOT  ((r1.x > r2.x AND r1.y > r2.y) OR  (r1.x < r2.x AND r1.y < r2.y)) THEN 1.0 ELSE 0.0 END)) /
		CAST(COUNT(1) AS FLOAT	)
	FROM @Basis r1
	CROSS JOIN @Basis r2
	WHERE NOT (r1.FactorSetId = r2.FactorSetId
			AND	r1.Refnum = r2.Refnum);

	--------------------------------------------------------------------------------

	SELECT
		@Relationship				[Type],
		@b0							[b0],
		@b1							[b1],
		
		@StDev						[StDev],
		@B1StDev					[B1StDev],
		@B1Var						[B1Var],

		@COV						[COV],
		@COD						[COD],
		@COR						[COR],
		
		@DFn						[DFn],
		@DFd						[DFd],
		@Confidence_Pcnt			[Confidence],
		@tAlpha						[Alpha],
		@tStat						[tStatInterval],

		@tTest						[tTest],
		@tConfidence				[tConfidence],
		@tStatistic					[tStat],
		@tHypothesis				[tHypothesis],

		@fTest						[fTest],
		@fConfidence				[fConfidence],
		@fStatistic					[fStat],
		@fHypothesis				[fHypothesis],

		@cTest						[cTest],
		@cConfidence				[cConfidence],
		@cStatistic					[cStat],
		@cHypothesis				[cHypothesis],

		@Tau						[Tau],
		@Rho						[Rho],
		@PMCC						[PMCC],

		@SSE						[SSE],
		@SSR						[SSR],
		@SST						[SST],
		@MSE						[MSE],
		@MSR						[MSR],

		@StdAway					[StdAwy],

		@xMean						[xMeanLinear],
		@yMean						[yMeanLinear],
		@xStDev						[xStDevLinear],
		@yStDev						[yStDevLinear],
		@SumXiX						[SumXiX];

	SELECT
		c.[FactorSetId],
		c.[Refnum],
		c.[x],
		c.[y],
		c.[Basis],
		c.[Audit],

		c.[Estimate],
		c.[zTest],
		c.[zNormal],
		c.[zTile],
		c.[_ErrorAbsolute],
		c.[_ErrorRelative],

		c.[ResidualStandard],
		c.[ResidualAbsolute],
		c.[ResidualRelative],

		c.[ResidualThreshold],
		c.[_BasisResidualStdIn],
		c.[_BasisResidualStdOut],
		c.[_AuditResidualStdIn],
		c.[_AuditResidualStdOut],

		c.[Confidence],
		c.[_ConfidenceMin],
		c.[_ConfidenceMax],
		c.[Prediction],
		c.[_PredictionMin],
		c.[_PredictionMax],

		c.[BasisRank],
		c.[_BasisQ1],
		c.[_BasisQ2],
		c.[_BasisQ3],
		c.[_BasisQ4],

		c.[AuditRank],
		c.[_AuditQ1],
		c.[_AuditQ2],
		c.[_AuditQ3],
		c.[_AuditQ4],

		c.[PearsonThreshold],
		c.[_SuspectThreshold],
		c.[xCeiling],
		c.[_SuspectCeiling]

	FROM @Chart c;

	--------------------------------------------------------------------------------
	--	Determine Pearson Product Moment Correlation Coefficeints for Confidence
	--	intervals [0.2, 1.0]
	
	DECLARE @pTile			FLOAT	 = 0.20;
	DECLARE @pMax			FLOAT	 = 1.00;
	DECLARE @pStep			FLOAT	 = 0.01;		

	DECLARE @Pearson TABLE
	(
		[pTile]				FLOAT		NOT	NULL	CHECK([pTile] >= 0.0 AND ROUND([pTile], 3) <= 1.0),
		[Pearson]			FLOAT		NOT	NULL	CHECK([Pearson] >= 0.0 AND [Pearson] <= 1.0),
		[items]				INT			NOT NULL	CHECK([items] >  0),

		PRIMARY KEY CLUSTERED ([pTile] ASC)
	);

	WHILE @pTile <= @pMax + @pStep BEGIN
	
		SELECT
			@xMean = AVG(b.x),
			@yMean = AVG(b.y) 
		FROM	@Chart		b
		WHERE	b.Basis = 1
			AND	b.ResidualAbsolute < @pTile
			AND b.ResidualRelative < @pTile;
		
		INSERT INTO @Pearson (pTile, Pearson, items)
		SELECT
			@pTile,
			SUM((b.x - @xMean) * (b.y - @yMean)) / CAST(COUNT(1) AS FLOAT	) / STDEVP(b.x) / STDEVP(b.y),
			COUNT(1)
		FROM	@Chart		b
		WHERE	b.Basis = 1
			AND	b.ResidualAbsolute < @pTile
			AND b.ResidualRelative < @pTile
		HAVING	STDEVP(b.x) <> 0
			AND	STDEVP(b.y) <> 0;
		
		IF (@pTile >= @pMax) BREAK ELSE SET @pTile = @pTile + @pStep;
		
	END;

	SELECT
		p.pTile,
		p.Pearson,
		p.items
	FROM @Pearson p;

END
