﻿CREATE TYPE [stat].[PopulationRandomVar] AS TABLE (
    [FactorSetId] VARCHAR (12) NOT NULL,
    [Refnum]      VARCHAR (25) NOT NULL,
    [x]           FLOAT (53)   NOT NULL,
    [w]           FLOAT (53)   NULL,
    [Basis]       BIT          NOT NULL,
    [Audit]       BIT          NOT NULL,
    PRIMARY KEY CLUSTERED ([x] ASC, [FactorSetId] ASC, [Refnum] ASC),
    CHECK ([FactorSetId]<>''),
    CHECK ([Refnum]<>''),
    CHECK ([Basis]=(1) OR [Audit]=(1)));

