﻿CREATE TABLE [ante].[PricingStreamPurityLimits] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [SimModelId]           VARCHAR (12)       NOT NULL,
    [StreamId]             VARCHAR (42)       NOT NULL,
    [ComponentId]          VARCHAR (42)       NOT NULL,
    [Purity_WtPcnt]        REAL               NOT NULL,
    [PurityLower_WtPcnt]   REAL               NULL,
    [PurityUpper_WtPcnt]   REAL               NULL,
    [PurityProduct_WtPcnt] REAL               NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_PricingStreamPurityLimits_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_PricingStreamPurityLimits_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_PricingStreamPurityLimits_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_PricingStreamPurityLimits_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingStreamPurityLimits] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingStreamPurityLimits_Purity_WtPcnt] CHECK ([Purity_WtPcnt]>=(0.0) AND [Purity_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_PricingStreamPurityLimits_PurityLower_WtPcnt] CHECK ([PurityLower_WtPcnt]>=(0.0) AND [PurityLower_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_PricingStreamPurityLimits_PurityProduct_WtPcnt] CHECK ([PurityProduct_WtPcnt]>=(0.0) AND [PurityProduct_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_PricingStreamPurityLimits_PurityUpper_WtPcnt] CHECK ([PurityUpper_WtPcnt]>=(0.0) AND [PurityUpper_WtPcnt]<=(100.0)),
    CONSTRAINT [CV_PricingStreamPurityLimits_PurityLower_Upper] CHECK ([PurityLower_WtPcnt]<=[PurityUpper_WtPcnt]),
    CONSTRAINT [CV_PricingStreamPurityLimits_PurityUpper_Lower] CHECK ([PurityUpper_WtPcnt]>=[PurityLower_WtPcnt]),
    CONSTRAINT [FK_PricingStreamPurityLimits_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingStreamPurityLimits_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingStreamPurityLimits_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingStreamPurityLimits_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_PricingStreamPurityLimits_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_StreamPurityLimits_u]
	ON [ante].[PricingStreamPurityLimits]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingStreamPurityLimits]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingStreamPurityLimits].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[PricingStreamPurityLimits].StreamId		= INSERTED.StreamId
		AND	[ante].[PricingStreamPurityLimits].ComponentId	= INSERTED.ComponentId;

END;