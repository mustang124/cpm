﻿CREATE PROCEDURE sim.StreamYield_Delete
(
	@FactorSetId	VARCHAR(12)	= NULL,
	@Refnum			VARCHAR(25)	= NULL,
	@SimModelId		VARCHAR(12)	= NULL
)
AS
BEGIN

	DELETE FROM sim.YieldCompositionStream
	WHERE	FactorSetId		= @FactorSetId
		AND	Refnum			= @Refnum	
		AND	SimModelId		= @SimModelId;

END