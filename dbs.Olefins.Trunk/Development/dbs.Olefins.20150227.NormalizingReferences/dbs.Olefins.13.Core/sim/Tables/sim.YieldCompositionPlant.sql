﻿CREATE TABLE [sim].[YieldCompositionPlant] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CalDateKey]       INT                NOT NULL,
    [SimModelId]       VARCHAR (12)       NOT NULL,
    [OpCondId]         VARCHAR (12)       NOT NULL,
    [RecycleId]        INT                NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [Component_kMT]    REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_YieldCompositionPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_YieldCompositionPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_YieldCompositionPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_YieldCompositionPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_YieldCompositionPlant] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [RecycleId] ASC, [SimModelId] ASC, [OpCondId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_YieldCompositionPlant_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_YieldCompositionPlant_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_YieldCompositionPlant_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_YieldCompositionPlant_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_YieldCompositionPlant_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId])
);


GO
CREATE NONCLUSTERED INDEX [IX_simCompositionYieldPlant_Calc]
    ON [sim].[YieldCompositionPlant]([ComponentId] ASC, [OpCondId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [SimModelId], [RecycleId], [Component_WtPcnt], [tsModified], [tsModifiedHost], [tsModifiedUser], [tsModifiedApp]);


GO

CREATE TRIGGER [sim].[t_YieldCompositionPlant_u]
	ON [sim].[YieldCompositionPlant]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [sim].[YieldCompositionPlant]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
		WHERE	[sim].[YieldCompositionPlant].FactorSetId		= INSERTED.FactorSetId
			AND	[sim].[YieldCompositionPlant].SimModelId		= INSERTED.SimModelId
			AND	[sim].[YieldCompositionPlant].Refnum			= INSERTED.Refnum
			AND	[sim].[YieldCompositionPlant].CalDateKey		= INSERTED.CalDateKey
			AND	[sim].[YieldCompositionPlant].RecycleId			= INSERTED.RecycleId
			AND	[sim].[YieldCompositionPlant].OpCondId			= INSERTED.OpCondId
			AND	[sim].[YieldCompositionPlant].ComponentId		= INSERTED.ComponentId;

END;