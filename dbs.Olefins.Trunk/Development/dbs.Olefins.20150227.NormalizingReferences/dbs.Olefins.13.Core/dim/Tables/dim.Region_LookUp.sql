﻿CREATE TABLE [dim].[Region_LookUp] (
    [RegionId]       VARCHAR (5)        NOT NULL,
    [RegionName]     NVARCHAR (84)      NOT NULL,
    [RegionDetail]   NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Region_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Region_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Region_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Region_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Region_LookUp] PRIMARY KEY CLUSTERED ([RegionId] ASC),
    CONSTRAINT [CL_Region_LookUp_RegionDetail] CHECK ([RegionDetail]<>''),
    CONSTRAINT [CL_Region_LookUp_RegionID] CHECK ([RegionId]<>''),
    CONSTRAINT [CL_Region_LookUp_RegionName] CHECK ([RegionName]<>''),
    CONSTRAINT [UK_Region_LookUp_RegionDetail] UNIQUE NONCLUSTERED ([RegionDetail] ASC),
    CONSTRAINT [UK_Region_LookUp_RegionName] UNIQUE NONCLUSTERED ([RegionName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Region_LookUp_u]
	ON [dim].[Region_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Region_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Region_LookUp].[RegionId]		= INSERTED.[RegionId];
		
END;
