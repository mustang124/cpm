﻿CREATE TABLE [dim].[Margin_C3H6_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [MarginId]       VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Margin_C3H6_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Margin_C3H6_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Margin_C3H6_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Margin_C3H6_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Margin_C3H6_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Margin_C3H6_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [MarginId] ASC),
    CONSTRAINT [CR_Margin_C3H6_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Margin_C3H6_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Margin_C3H6_Parent_LookUp_Margin] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_Margin_C3H6_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_Margin_C3H6_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Margin_C3H6_Parent] ([FactorSetId], [MarginId])
);


GO

CREATE TRIGGER [dim].[t_Margin_C3H6_Parent_u]
ON [dim].[Margin_C3H6_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Margin_C3H6_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Margin_C3H6_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Margin_C3H6_Parent].[MarginId]		= INSERTED.[MarginId];

END;