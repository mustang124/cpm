﻿CREATE TABLE [dim].[ReliabilityCompComponent_Parent] (
    [FactorSetId]     VARCHAR (12)        NOT NULL,
    [CompComponentId] VARCHAR (42)        NOT NULL,
    [ParentId]        VARCHAR (42)        NOT NULL,
    [Operator]        CHAR (1)            CONSTRAINT [DF_ReliabilityCompComponent_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]         INT                 NOT NULL,
    [Hierarchy]       [sys].[hierarchyid] NOT NULL,
    [tsModified]      DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityCompComponent_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (128)      CONSTRAINT [DF_ReliabilityCompComponent_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (128)      CONSTRAINT [DF_ReliabilityCompComponent_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (128)      CONSTRAINT [DF_ReliabilityCompComponent_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityCompComponent_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CompComponentId] ASC),
    CONSTRAINT [CR_ReliabilityCompComponent_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ReliabilityCompComponent_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityCompComponent_Parent_LookUp_CompComponent] FOREIGN KEY ([CompComponentId]) REFERENCES [dim].[ReliabilityCompComponent_LookUp] ([CompComponentId]),
    CONSTRAINT [FK_ReliabilityCompComponent_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ReliabilityCompComponent_LookUp] ([CompComponentId]),
    CONSTRAINT [FK_ReliabilityCompComponent_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ReliabilityCompComponent_Parent] ([FactorSetId], [CompComponentId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityCompComponent_Parent_u]
ON [dim].[ReliabilityCompComponent_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityCompComponent_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityCompComponent_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityCompComponent_Parent].[CompComponentId]	= INSERTED.[CompComponentId];

END;