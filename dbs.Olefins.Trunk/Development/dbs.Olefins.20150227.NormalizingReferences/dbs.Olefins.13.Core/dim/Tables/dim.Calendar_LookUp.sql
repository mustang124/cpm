﻿CREATE TABLE [dim].[Calendar_LookUp] (
    [CalDate]        DATE               NOT NULL,
    [CalDateKey]     AS                 (CONVERT([int],CONVERT([char](10),[CalDate],(112)),(0))) PERSISTED NOT NULL,
    [YearSemester]   AS                 ((CONVERT([char](4),datepart(year,[CalDate]),(0))+'S')+case when datepart(month,[CalDate])<=(6) then '1' else '2' end) PERSISTED NOT NULL,
    [YearQuarter]    AS                 ((CONVERT([char](4),datepart(year,[CalDate]),(0))+'Q')+CONVERT([char](1),datepart(quarter,[CalDate]),(0))) PERSISTED NOT NULL,
    [YearMonth]      AS                 ((CONVERT([char](4),datepart(year,[CalDate]),(0))+'M')+right('0'+CONVERT([varchar](2),datepart(month,[CalDate]),(0)),(2))) PERSISTED NOT NULL,
    [YearWeek]       AS                 ((CONVERT([char](4),datepart(year,[CalDate]),(0))+'W')+right('0'+CONVERT([varchar](2),datepart(week,[CalDate]),(0)),(2))),
    [YearDay]        AS                 ((CONVERT([char](4),datepart(year,[CalDate]),(0))+'D')+right('00'+CONVERT([varchar](3),datepart(dayofyear,[CalDate]),(0)),(3))) PERSISTED NOT NULL,
    [MonthName]      AS                 (datename(month,[CalDate])),
    [MonthDay]       AS                 (datepart(day,[CalDate])) PERSISTED NOT NULL,
    [CalYear]        AS                 (datepart(year,[CalDate])) PERSISTED NOT NULL,
    [CalSem]         AS                 (case when datepart(month,[CalDate])<=(6) then (1) else (2) end) PERSISTED NOT NULL,
    [CalQtr]         AS                 (datepart(quarter,[CalDate])) PERSISTED NOT NULL,
    [CalMonth]       AS                 (datepart(month,[CalDate])) PERSISTED NOT NULL,
    [CalWeek]        AS                 (datepart(week,[CalDate])),
    [CalDay]         AS                 (datepart(dayofyear,[CalDate])) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Calendar_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Calendar_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Calendar_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Calendar_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Calendar_LookUp] PRIMARY KEY CLUSTERED ([CalDateKey] ASC)
);


GO

CREATE TRIGGER [dim].[t_Calendar_LookUp_u]
	ON [dim].[Calendar_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Calendar_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Calendar_LookUp].[CalDateKey]	= INSERTED.[CalDateKey];
		
END;