﻿CREATE TABLE [dim].[ReliabilityPredMaintCause_Parent] (
    [FactorSetId]      VARCHAR (12)        NOT NULL,
    [PredMaintCauseId] VARCHAR (42)        NOT NULL,
    [ParentId]         VARCHAR (42)        NOT NULL,
    [Operator]         CHAR (1)            CONSTRAINT [DF_ReliabilityPredMaintCause_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]          INT                 NOT NULL,
    [Hierarchy]        [sys].[hierarchyid] NOT NULL,
    [tsModified]       DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityPredMaintCause_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPredMaintCause_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPredMaintCause_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPredMaintCause_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityPredMaintCause_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PredMaintCauseId] ASC),
    CONSTRAINT [CR_ReliabilityPredMaintCause_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ReliabilityPredMaintCause_LookUp] ([PredMaintCauseId]),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Parent_LookUp_PredMaintCause] FOREIGN KEY ([PredMaintCauseId]) REFERENCES [dim].[ReliabilityPredMaintCause_LookUp] ([PredMaintCauseId]),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ReliabilityPredMaintCause_Parent] ([FactorSetId], [PredMaintCauseId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityPredMaintCause_Parent_u]
ON [dim].[ReliabilityPredMaintCause_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityPredMaintCause_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityPredMaintCause_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityPredMaintCause_Parent].[PredMaintCauseId]	= INSERTED.[PredMaintCauseId];

END;