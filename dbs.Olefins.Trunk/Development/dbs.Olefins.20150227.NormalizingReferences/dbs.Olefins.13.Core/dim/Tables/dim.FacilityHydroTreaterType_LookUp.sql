﻿CREATE TABLE [dim].[FacilityHydroTreaterType_LookUp] (
    [HTTypeId]       VARCHAR (2)        NOT NULL,
    [HTTypeName]     NVARCHAR (84)      NOT NULL,
    [HTTypeDetail]   NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilityHydroTreaterType_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FacilityHydroTreaterType_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FacilityHydroTreaterType_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FacilityHydroTreaterType_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilityHydroTreaterType_LookUp] PRIMARY KEY CLUSTERED ([HTTypeId] ASC),
    CONSTRAINT [CL_FacilityHydroTreaterType_LookUp_HTTypeDetail] CHECK ([HTTypeDetail]<>''),
    CONSTRAINT [CL_FacilityHydroTreaterType_LookUp_HTTypeID] CHECK ([HTTypeId]<>''),
    CONSTRAINT [CL_FacilityHydroTreaterType_LookUp_HTTypeName] CHECK ([HTTypeName]<>''),
    CONSTRAINT [UK_FacilityHydroTreaterType_LookUp_HTTypeDetail] UNIQUE NONCLUSTERED ([HTTypeDetail] ASC),
    CONSTRAINT [UK_FacilityHydroTreaterType_LookUp_HTTypeName] UNIQUE NONCLUSTERED ([HTTypeName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FacilityHydroTreaterType_LookUp_u]
	ON [dim].[FacilityHydroTreaterType_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FacilityHydroTreaterType_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FacilityHydroTreaterType_LookUp].[HTTypeId]	= INSERTED.[HTTypeId];
		
END;