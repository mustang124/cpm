﻿CREATE PROCEDURE [web].[Verify_DataSetStage]
(
	@SubmissionId			INT,
	@LanguageId				INT = 1033
)
AS
BEGIN

SET NOCOUNT ON;

	DECLARE @Error_Count INT;

	EXECUTE @Error_Count = [stage].[Verify_DataSet] @SubmissionId, @LanguageId;

	RETURN @Error_Count;
	
END;