﻿CREATE PROC [Console].[CheckForCombo]
	@RefNum dbo.Refnum
	
AS
	
	SELECT cb1.IndivRefnum FROM ComboRefs cb1 
	JOIN ComboRefs cb2 on cb1.ComboRefnum = cb2.ComboRefnum 
	WHERE cb2.IndivRefnum IN (SELECT REFNUM FROM TSort WHERE RefineryID = @RefNum) 
	and 
	cb1.IndivRefnum NOT IN (SELECT REFNUM FROM TSort WHERE RefineryID = @RefNum)
