﻿CREATE PROCEDURE [Console].[GetCompanyContactInfo] 
	@RefNum dbo.Refnum
AS

BEGIN

	SELECT FirstName ,
		LastName ,
		ContactType,
		cc.Email ,
		cc.Phone ,
		cc.Fax ,
		cc.MailAddr1 ,
		cc.MailAddr2 ,
		cc.MailAddr3 ,
		MailCity  ,
		MailState ,
		MailZip ,
		MailCountry ,
		StrAddr1,
		StrAddr2,
		StrAdd3,
		StrCity,
		StrState,
		StrZip,
		StrCountry,
		JobTitle,
		SendMethod,
		cc.Password as CompanyPassword,
		t.Password as Password,
		SendMethod as CompanySendMethod,
		cc.Comment,
		cc.AltFirstName,
		cc.AltLastName,
		cc.AltEmail,
		null as SANumber
	 FROM CoContactInfo cc join tsort t on t.ContactCode = cc.contactcode and t.StudyYear = cc.StudyYear
		WHERE t.Refnum=@RefNum
END
