﻿CREATE TABLE [xls].[StreamRecycled] (
    [Refnum]            VARCHAR (12)       NOT NULL,
    [StreamNumber]      INT                NOT NULL,
    [StreamId]          INT                NOT NULL,
    [Quantity_Dur_kMT]  FLOAT (53)         NOT NULL,
    [Quantity_Ann_kMT]  FLOAT (53)         NOT NULL,
    [ComponentId]       INT                NOT NULL,
    [Recycled_WtPcnt]   FLOAT (53)         NOT NULL,
    [_Recycled_Dur_kMT] AS                 (CONVERT([float],([Quantity_Dur_kMT]*[Recycled_WtPcnt])/(100.0),0)) PERSISTED NOT NULL,
    [_Recycled_Ann_kMT] AS                 (CONVERT([float],([Quantity_Ann_kMT]*[Recycled_WtPcnt])/(100.0),0)) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_StreamRecycled_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_StreamRecycled_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_StreamRecycled_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_StreamRecycled_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamRecycled] PRIMARY KEY CLUSTERED ([Refnum] DESC, [StreamNumber] ASC, [ComponentId] ASC),
    CONSTRAINT [CL_StreamRecycled_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_StreamRecycled_Quantity_Ann_kMT_MinIncl_0.0] CHECK ([Quantity_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamRecycled_Quantity_Dur_kMT_MinIncl_0.0] CHECK ([Quantity_Dur_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MaxIncl_100.0] CHECK ([Recycled_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamRecycled_Recycled_WtPcnt_MinIncl_0.0] CHECK ([Recycled_WtPcnt]>=(0.0)),
    CONSTRAINT [FK_StreamRecycled_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StreamRecycled_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [xls].[t_StreamRecycled_u]
	ON [xls].[StreamRecycled]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StreamRecycled]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StreamRecycled].[Refnum]			= INSERTED.[Refnum]
		AND	[xls].[StreamRecycled].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[xls].[StreamRecycled].[ComponentId]	= INSERTED.[ComponentId];

END;