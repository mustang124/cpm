﻿CREATE TABLE [fact].[Submissions] (
    [SubmissionId]         INT                NOT NULL,
    [SubmissionName]       NVARCHAR (128)     NOT NULL,
    [DateBeg]              DATE               NOT NULL,
    [DateEnd]              DATE               NOT NULL,
    [_Duration_Days]       AS                 (CONVERT([float],datediff(day,[DateBeg],[DateEnd])+(1.0),0)) PERSISTED NOT NULL,
    [_Duration_Multiplier] AS                 (CONVERT([float],(365.0)/(datediff(day,[DateBeg],[DateEnd])+(1.0)),0)) PERSISTED NOT NULL,
    [_SubmissionNameFull]  AS                 ((((([SubmissionName]+N' (')+CONVERT([nvarchar],[DateBeg],(106)))+N' - ')+CONVERT([nvarchar],[DateEnd],(106)))+')'),
    [Active]               BIT                CONSTRAINT [DF_Submissions_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_Submissions_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (128)     CONSTRAINT [DF_Submissions_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Submissions] PRIMARY KEY CLUSTERED ([SubmissionId] DESC),
    CONSTRAINT [CL_Sumbissions_SumbissionName] CHECK ([SubmissionName]<>''),
    CONSTRAINT [CV_Submissions_Dates] CHECK ([DateEnd]>=[DateBeg])
);


GO

CREATE TRIGGER [fact].[t_Submissions_u]
	ON [fact].[Submissions]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Submissions]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[Submissions].[SubmissionId]		= INSERTED.[SubmissionId]

END;