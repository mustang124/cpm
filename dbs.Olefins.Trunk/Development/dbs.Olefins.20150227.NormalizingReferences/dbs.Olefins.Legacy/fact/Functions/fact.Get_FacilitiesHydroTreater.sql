﻿CREATE FUNCTION [fact].[Get_FacilitiesHydroTreater]
(
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		h.[SubmissionId],
		h.[FacilityId],
		h.[Quantity_kBSD],
		h.[HydroTreaterTypeId],
		h.[Pressure_PSIg],
		h.[Processed_kMT],
		h.[Processed_Pcnt],
		h.[Density_SG]
	FROM [fact].[FacilitiesHydroTreater]		h
	WHERE	h.[SubmissionId]	= @SubmissionId
);