﻿CREATE RULE [dbo].[chkYorN]
    AS @Value IN ('Y', 'N') OR @Value IS NULL;


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[CoContactInfo].[CCAlt]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkYorN]', @objname = N'[dbo].[YorN]';

