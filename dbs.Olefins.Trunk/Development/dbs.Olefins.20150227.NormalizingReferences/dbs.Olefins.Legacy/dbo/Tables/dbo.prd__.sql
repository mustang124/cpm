﻿CREATE TABLE [dbo].[prd##] (
    [Refnum]     CHAR (9)  NOT NULL,
    [IssueID]    CHAR (8)  NOT NULL,
    [IssueTitle] CHAR (50) NOT NULL,
    [IssueText]  TEXT      NULL,
    [PostedBy]   CHAR (3)  NOT NULL,
    [PostedTime] DATETIME  NOT NULL,
    [Completed]  CHAR (1)  NOT NULL,
    [SetBy]      CHAR (3)  NULL,
    [SetTime]    DATETIME  NULL
);

