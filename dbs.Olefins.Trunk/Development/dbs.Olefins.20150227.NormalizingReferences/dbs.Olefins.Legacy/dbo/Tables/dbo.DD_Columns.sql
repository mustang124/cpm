﻿CREATE TABLE [dbo].[DD_Columns] (
    [ColumnID]     INT           IDENTITY (1, 1) NOT NULL,
    [ObjectID]     INT           NOT NULL,
    [ColumnName]   VARCHAR (256) NOT NULL,
    [StoredUnits]  VARCHAR (20)  NULL,
    [DisplayUnits] VARCHAR (20)  NULL,
    [HelpID]       VARCHAR (30)  NULL,
    [LookupID]     INT           NULL,
    CONSTRAINT [PK_DD_Columns_1__14] PRIMARY KEY CLUSTERED ([ColumnID] ASC),
    CONSTRAINT [ItemObjectRI] FOREIGN KEY ([ObjectID]) REFERENCES [dbo].[DD_Objects] ([ObjectID]),
    CONSTRAINT [UniqueColumn] UNIQUE NONCLUSTERED ([ObjectID] ASC, [ColumnName] ASC)
);

