﻿CREATE TABLE [dbo].[PracTA] (
    [Refnum]        [dbo].[Refnum] NOT NULL,
    [CGC]           CHAR (1)       NULL,
    [RC]            CHAR (1)       NULL,
    [TLE]           CHAR (1)       NULL,
    [OHX]           CHAR (1)       NULL,
    [Tower]         CHAR (1)       NULL,
    [Furnace]       CHAR (1)       NULL,
    [Util]          CHAR (1)       NULL,
    [CapProj]       CHAR (1)       NULL,
    [Other]         CHAR (1)       NULL,
    [OthCPMDesc]    VARCHAR (75)   NULL,
    [TimeFact]      CHAR (1)       NULL,
    [OthTFDesc]     VARCHAR (75)   NULL,
    [TAPrepTime]    REAL           NULL,
    [TAStartupTime] REAL           NULL,
    CONSTRAINT [PK___6__19] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

