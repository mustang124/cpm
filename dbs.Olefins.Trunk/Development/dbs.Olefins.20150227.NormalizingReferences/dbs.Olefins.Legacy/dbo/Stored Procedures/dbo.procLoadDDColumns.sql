﻿
/****** Object:  Stored Procedure dbo.procLoadDDColumns    Script Date: 4/18/2003 4:32:54 PM ******/

/****** Object:  Stored Procedure dbo.procLoadDDColumns    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.procLoadDDColumns    Script Date: 04/13/2000 8:32:49 AM ******/
CREATE PROCEDURE [dbo].[procLoadDDColumns] AS
INSERT INTO DD_Columns (ObjectID, ColumnName)
SELECT o.ObjectID, c.name
FROM syscolumns c, DD_Objects o, sysobjects s
WHERE s.name = o.ObjectName AND s.id = c.id
 AND NOT EXISTS (SELECT * FROM DD_Columns
	 WHERE DD_Columns.ObjectID = o.ObjectID AND DD_Columns.ColumnName = c.name)
DELETE FROM DD_Columns
WHERE NOT EXISTS 
	(SELECT *
	FROM syscolumns c, sysobjects o, DD_Objects do
	WHERE o.id = c.id AND o.name = do.ObjectName 
	AND do.ObjectID = DD_Columns.ObjectID
	AND c.name = DD_Columns.ColumnName)
