﻿CREATE PROCEDURE [dbo].[Update_StreamRecovered]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@Recovered_WtPcnt		FLOAT	= NULL
)
AS
BEGIN

	IF EXISTS (SELECT 1 FROM [stage].[StreamRecovered] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamNumber] = @StreamNumber)
	BEGIN
		SET @Recovered_WtPcnt = COALESCE(@Recovered_WtPcnt, 0.0);
		EXECUTE [stage].[Update_StreamRecovered] @SubmissionId, @StreamNumber, @Recovered_WtPcnt;
	END;
	ELSE
	BEGIN
		IF(@Recovered_WtPcnt >= 0.0)
		EXECUTE [stage].[Insert_StreamRecovered] @SubmissionId, @StreamNumber, @Recovered_WtPcnt;
	END;

END;
