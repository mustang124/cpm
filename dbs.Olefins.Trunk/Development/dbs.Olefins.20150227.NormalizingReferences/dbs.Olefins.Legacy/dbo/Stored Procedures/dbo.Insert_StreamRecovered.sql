﻿CREATE PROCEDURE [dbo].[Insert_StreamRecovered]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@Recovered_WtPcnt		FLOAT
)
AS
BEGIN

	IF(@Recovered_WtPcnt >= 0.0)
	EXECUTE [stage].[Insert_StreamRecovered] @SubmissionId, @StreamNumber, @Recovered_WtPcnt;

END;
