﻿CREATE PROC spOutputCheckData(@Study varchar(5), @StudyYear int = NULL, @OutputTab varchar(25))
AS
SELECT x.RowTag, x.ColTag, x.StudyYear, d2.CellValue, d2.CellText
FROM dbo.OutputCheckData d2 INNER JOIN dbo.OutputCheckTableIDs t2 ON t2.TableID = d2.TableID 
INNER JOIN (
	SELECT t.Study, t.OutputTab, d.RowTag, d.ColTag, StudyYear = MAX(t.StudyYear)
	FROM dbo.OutputCheckData d INNER JOIN dbo.OutputCheckTableIDs t ON t.TableID = d.TableID
	WHERE t.Study = @Study AND t.OutputTab = @OutputTab AND t.StudyYear <= ISNULL(@StudyYear, t.StudyYear)
	GROUP BY t.Study, t.OutputTab, d.RowTag, d.ColTag) x 
	ON t2.Study = x.Study AND t2.OutputTab = x.OutputTab
	AND x.RowTag = d2.RowTag AND x.ColTag = d2.ColTag AND x.StudyYear = t2.StudyYear


