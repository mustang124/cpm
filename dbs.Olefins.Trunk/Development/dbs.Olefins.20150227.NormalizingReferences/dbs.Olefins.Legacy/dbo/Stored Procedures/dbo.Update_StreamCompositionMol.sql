﻿CREATE PROCEDURE [dbo].[Update_StreamCompositionMol]
(
	@SubmissionId			INT,
	@StreamNumber			INT,
	@ComponentId			INT,

	@Component_MolPcnt		FLOAT	= NULL
)
AS
BEGIN

	IF EXISTS (SELECT 1 FROM [stage].[StreamCompositionMol] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamNumber] = @StreamNumber AND t.[ComponentId] = @ComponentId)
	BEGIN
		SET @Component_MolPcnt = COALESCE(@Component_MolPcnt, 0.0);
		EXECUTE [stage].[Update_StreamCompositionMol] @SubmissionId, @StreamNumber, @ComponentId, @Component_MolPcnt;
	END
	ELSE
	BEGIN
		IF(@Component_MolPcnt >= 0.0)
		EXECUTE [stage].[Insert_StreamCompositionMol] @SubmissionId, @StreamNumber, @ComponentId, @Component_MolPcnt;
	END;

END;
