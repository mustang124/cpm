﻿
/****** Object:  View dbo.vE_CompressorTotals    Script Date: 8/16/2006 7:43:33 AM ******/
CREATE  VIEW [dbo].vE_CompressorTotals
AS
SELECT Refnum,  'ISP'=SUM(ISP), 'EMS'=SUM(EMS), 'TargetEFF'=SUM(TargetEFF), 'WheelClean'=SUM(WheelClean)
, 'WheelCoating'=SUM(WheelCoating), 'PCE'=SUM(PCE), 'TDS_PSIG'=SUM(TDS_PSIG), 'TDSTemp'=SUM(TDSTemp)
, 'TDCTemp'=SUM(TDCTemp), 'TargetVSC'=SUM(TargetVSC), 'FreqVSC'=SUM(FreqVSC), 'MaintainVSC'=SUM(MaintainVSC)
, 'Kickbacks'=SUM(Kickbacks), 'AvgKickbacks_Pcnt'=SUM(AvgKickbacks_Pcnt) 
, 'LoopOptimize'=SUM(LoopOptimize), 'HEPerf'=SUM(HEPerf), 'HEService'=SUM(HEService) 
FROM E_Compressors
GROUP BY Refnum

/**Used to calc E_Rank_Score for Compressors - columns which were excluded: IHEB_Pcnt, Cnt,
   HEClean_Pcnt. **/



