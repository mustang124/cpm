﻿






CREATE  VIEW [dbo].[EfficiencyIndexes]
as
SELECT StandardsAndFactors.Refnum,

PersEffIndex = (select sum(PersCalc.TotWHr) from PersCalc where PersCalc.Refnum = StandardsAndFactors.Refnum group by PersCalc.Refnum)/ StandardsAndFactors.Pers/1000*100 ,
MaintPersEffIndex = (select PersSTCalc.TotWHr from PersSTCalc where PersSTCalc.SectionID = 'Maint'and PersSTCalc.Refnum = StandardsAndFactors.Refnum )  / StandardsAndFactors.PersMaint/1000*100, 
NonMaintPersEffIndex = (select PersSTCalc.TotWHr from PersSTCalc where PersSTCalc.SectionID = 'nonMaint'and PersSTCalc.Refnum = StandardsAndFactors.Refnum)/StandardsAndFactors.PersNonMaint/1000*100, 
MaintCostEffIndex = (select MaintInd.TotMaintCostAvg from MaintInd where MaintInd.Refnum = StandardsAndFactors.Refnum)/ StandardsAndFactors.Mes*100,
NEOpexCostEffIndex = (Select NEOpex From FinancialTot Where FinancialTot.Refnum = StandardsAndFactors.Refnum)/StandardsAndFactors.NonEnergy*100, 
Pers, PersMaint, PersNonMaint, Mes, NonEnergy
from  StandardsAndFactors

where StandardsAndFactors.ProcessUnitTag = 'Total'








