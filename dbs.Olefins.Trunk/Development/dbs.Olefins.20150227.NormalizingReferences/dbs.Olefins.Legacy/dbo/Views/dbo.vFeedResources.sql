﻿


/****** Object:  View dbo.vFeedResources    Script Date: 7/6/2006 11:09:24 AM ******/



CREATE    VIEW [dbo].[vFeedResources] AS
SELECT t.Refnum,
  Use_LP_PC= CASE WHEN (fr.Use_LP_PC='Y' OR fr.Lvl_LP_PC IS not null) THEN 100 ELSE 0 END,
  Lvl_LP_PC_P= CASE WHEN ((fr.Lvl_LP_PC='P' OR fr.Lvl_LP_PC IS NULL) AND fr.Use_LP_PC='Y')  THEN 100 ELSE 0 END,
  Lvl_LP_PC_C= CASE WHEN fr.Lvl_LP_PC='C' THEN 100 ELSE 0 END,
  Lvl_LP_PC_B= CASE WHEN fr.Lvl_LP_PC='B' THEN 100 ELSE 0 END,
  Src_LP_PC_W= CASE WHEN fr.Src_LP_PC='W' THEN 100 ELSE 0 END,
  Src_LP_PC_O= CASE WHEN fr.Src_LP_PC='O' THEN 100 ELSE 0 END,
  
  Use_LP_Main= CASE WHEN (fr.Use_LP_Main='Y' OR fr.Lvl_LP_Main IS not null)THEN 100 ELSE 0 END,
  Lvl_LP_Main_P= CASE WHEN ((fr.Lvl_LP_Main='P' OR fr.Lvl_LP_Main IS NULL) AND fr.Use_LP_Main='Y')THEN 100 ELSE 0 END,
  Lvl_LP_Main_C= CASE WHEN fr.Lvl_LP_Main='C' THEN 100 ELSE 0 END,
  Lvl_LP_Main_B= CASE WHEN fr.Lvl_LP_Main='B' THEN 100 ELSE 0 END,
  Src_LP_Main_W= CASE WHEN fr.Src_LP_Main='W' THEN 100 ELSE 0 END,
  Src_LP_Main_O= CASE WHEN fr.Src_LP_Main='O' THEN 100 ELSE 0 END,
  
  Use_LPMultiPlant= CASE WHEN (fr.Use_LPMultiPlant='Y' OR fr.Lvl_LPMultiPlant IS not null)  THEN 100 ELSE 0 END,
  Lvl_LPMultiPlant_P= CASE WHEN ((fr.Lvl_LPMultiPlant='P' OR fr.Lvl_LPMultiPlant IS NULL) AND fr.Use_LPMultiPlant='Y')THEN 100 ELSE 0 END,
  Lvl_LPMultiPlant_C= CASE WHEN fr.Lvl_LPMultiPlant='C' THEN 100 ELSE 0 END,
  Lvl_LPMultiPlant_B= CASE WHEN fr.Lvl_LPMultiPlant='B' THEN 100 ELSE 0 END,
  Src_MultiPlant_W= CASE WHEN fr.Src_LPMultiPlant='W' THEN 100 ELSE 0 END,
  Src_MultiPlant_O= CASE WHEN fr.Src_LPMultiPlant='O' THEN 100 ELSE 0 END,
  
  Use_LPMultiPeriod= CASE WHEN (fr.Use_LPMultiPeriod='Y' OR fr.Lvl_LPMultiPeriod IS not null) THEN 100 ELSE 0 END,
  Lvl_LPMultiPeriod_P= CASE WHEN ((fr.Lvl_LPMultiPeriod='P' OR fr.Lvl_LPMultiPeriod IS NULL) AND fr.Use_LPMultiPeriod='Y')THEN 100 ELSE 0 END,
  Lvl_LPMultiPeriod_C= CASE WHEN fr.Lvl_LPMultiPeriod='C' THEN 100 ELSE 0 END,
  Lvl_LPMultiPeriod_B= CASE WHEN fr.Lvl_LPMultiPeriod='B' THEN 100 ELSE 0 END,
  Src_MultiPeriod_W= CASE WHEN fr.Src_LPMultiPeriod='W' THEN 100 ELSE 0 END,
  Src_MultiPeriod_O= CASE WHEN fr.Src_LPMultiPeriod='O' THEN 100 ELSE 0 END,
  
  Use_TierData= CASE WHEN (fr.Use_TierData='Y' OR fr.Lvl_TierData IS not null) THEN 100 ELSE 0 END,
  Lvl_TierData_P= CASE WHEN ((fr.Lvl_TierData='P' OR fr.Lvl_TierData IS NULL) AND fr.Use_TierData='Y')THEN 100 ELSE 0 END,
  Lvl_TierData_C= CASE WHEN fr.Lvl_TierData='C' THEN 100 ELSE 0 END,
  Lvl_TierData_B= CASE WHEN fr.Lvl_TierData='B' THEN 100 ELSE 0 END,
  Src_TierData_W= CASE WHEN fr.Src_TierData='W' THEN 100 ELSE 0 END,
  Src_TierData_O= CASE WHEN fr.Src_TierData='O' THEN 100 ELSE 0 END,
  
  Use_TierProduct= CASE WHEN (fr.Use_TierProduct='Y' OR fr.Lvl_TierProduct IS not null) THEN 100 ELSE 0 END,
  Lvl_TierProduct_P= CASE WHEN ((fr.Lvl_TierProduct='P' OR fr.Lvl_TierProduct IS NULL) AND fr.Use_TierProduct='Y')THEN 100 ELSE 0 END,
  Lvl_TierProduct_C= CASE WHEN fr.Lvl_TierProduct='C' THEN 100 ELSE 0 END,
  Lvl_TierProduct_B= CASE WHEN fr.Lvl_TierProduct='B' THEN 100 ELSE 0 END,
  Src_TierProduct_W= CASE WHEN fr.Src_TierProduct='W' THEN 100 ELSE 0 END,
  Src_TierProduct_O= CASE WHEN fr.Src_TierProduct='O' THEN 100 ELSE 0 END,
  
  Use_NonLP= CASE WHEN (fr.Use_NonLP='Y' OR fr.Lvl_NonLP IS not null) THEN 100 ELSE 0 END,
  Lvl_NonLP_P= CASE WHEN ((fr.Lvl_NonLP='P' OR fr.Lvl_NonLP IS NULL) AND fr.Use_NonLP='Y')THEN 100 ELSE 0 END,
  Lvl_NonLP_C= CASE WHEN fr.Lvl_NonLP='C' THEN 100 ELSE 0 END,
  Lvl_NonLP_B= CASE WHEN fr.Lvl_NonLP='B' THEN 100 ELSE 0 END,
  Src_NonLP_W= CASE WHEN fr.Src_NonLP='W' THEN 100 ELSE 0 END,
  Src_NonLP_O= CASE WHEN fr.Src_NonLP='O' THEN 100 ELSE 0 END,
  
  Use_Spread= CASE WHEN (fr.Use_Spread='Y' OR fr.Lvl_Spread IS not null) THEN 100 ELSE 0 END,
  Lvl_Spread_P= CASE WHEN ((fr.Lvl_Spread='P' OR fr.Lvl_Spread IS NULL) AND fr.Use_Spread='Y')THEN 100 ELSE 0 END,
  Lvl_Spread_C= CASE WHEN fr.Lvl_Spread='C' THEN 100 ELSE 0 END,
  Lvl_Spread_B= CASE WHEN fr.Lvl_Spread='B' THEN 100 ELSE 0 END,
  Src_Spread_W= CASE WHEN fr.Src_Spread='W' THEN 100 ELSE 0 END,
  Src_Spread_O= CASE WHEN fr.Src_Spread='O' THEN 100 ELSE 0 END,
  
  Use_MarginData= CASE WHEN (fr.Use_MarginData='Y' OR fr.Lvl_MarginData IS not null) THEN 100 ELSE 0 END,
  Lvl_MarginData_P= CASE WHEN ((fr.Lvl_MarginData='P' OR fr.Lvl_MarginData IS NULL) AND fr.Use_MarginData='Y')THEN 100 ELSE 0 END,
  Lvl_MarginData_C= CASE WHEN fr.Lvl_MarginData='C' THEN 100 ELSE 0 END,
  Lvl_MarginData_B= CASE WHEN fr.Lvl_MarginData='B' THEN 100 ELSE 0 END,
  Src_MarginData_W= CASE WHEN fr.Src_MarginData='W' THEN 100 ELSE 0 END,
  Src_MarginData_O= CASE WHEN fr.Src_MarginData='O' THEN 100 ELSE 0 END,
  
  Use_AltFeed= CASE WHEN (fr.Use_AltFeed='Y' OR fr.Lvl_AltFeed IS not null) THEN 100 ELSE 0 END,
  Lvl_AltFeed_P= CASE WHEN ((fr.Lvl_AltFeed='P' OR fr.Lvl_AltFeed IS NULL) AND fr.Use_AltFeed='Y')THEN 100 ELSE 0 END,
  Lvl_AltFeed_C= CASE WHEN fr.Lvl_AltFeed='C' THEN 100 ELSE 0 END,
  Lvl_AltFeed_B= CASE WHEN fr.Lvl_AltFeed='B' THEN 100 ELSE 0 END,
  Src_AltFeed_W= CASE WHEN fr.Src_AltFeed='W' THEN 100 ELSE 0 END,
  Src_AltFeed_O= CASE WHEN fr.Src_AltFeed='O' THEN 100 ELSE 0 END,
  
  Use_BEAltFeed= CASE WHEN (fr.Use_BEAltFeed='Y' OR fr.Lvl_BEAltFeed IS not null) THEN 100 ELSE 0 END,
  Lvl_BEAltFeed_P= CASE WHEN ((fr.Lvl_BEAltFeed='P' OR fr.Lvl_BEAltFeed IS NULL) AND fr.Use_BEAltFeed='Y')THEN 100 ELSE 0 END,
  Lvl_BEAltFeed_C= CASE WHEN fr.Lvl_BEAltFeed='C' THEN 100 ELSE 0 END,
  Lvl_BEAltFeed_B= CASE WHEN fr.Lvl_BEAltFeed='B' THEN 100 ELSE 0 END,
  Src_BEAltFeed_W= CASE WHEN fr.Src_BEAltFeed='W' THEN 100 ELSE 0 END,
  Src_BEAltFeed_O= CASE WHEN fr.Src_BEAltFeed='O' THEN 100 ELSE 0 END,
  
  Use_YieldPredict= CASE WHEN (fr.Use_YieldPredict='Y' OR fr.Lvl_YieldPredict IS not null) THEN 100 ELSE 0 END,
  Lvl_YieldPredict_P= CASE WHEN ((fr.Lvl_YieldPredict='P' OR fr.Lvl_YieldPredict IS NULL) AND fr.Use_YieldPredict='Y')THEN 100 ELSE 0 END,
  Lvl_YieldPredict_C= CASE WHEN fr.Lvl_YieldPredict='C' THEN 100 ELSE 0 END,
  Lvl_YieldPredict_B= CASE WHEN fr.Lvl_YieldPredict='B' THEN 100 ELSE 0 END,
  Src_YieldPredict_W= CASE WHEN fr.Src_YieldPredict='W' THEN 100 ELSE 0 END,
  Src_YieldPredict_O= CASE WHEN fr.Src_YieldPredict='O' THEN 100 ELSE 0 END,
  Plant_Decision= CASE WHEN fr.Plant_Decision='X' THEN 100 ELSE 0 END,
  Corp_Decision= CASE WHEN fr.Corp_Decision='X' THEN 100 ELSE 0 END,
  DecisionMade= Case when (fr.Plant_Decision IS Null AND  fr.Corp_Decision IS Null) then 0 else 1 end,  
  FeedSlateChg= CASE WHEN fr.FeedSlateChg='Y' THEN 100 ELSE 0 END,
  Reliab= CASE WHEN fr.Reliab='Y' THEN 100 ELSE 0 END,
  Environ= CASE WHEN fr.Environ='Y' THEN 100 ELSE 0 END,
  Furnace= CASE WHEN fr.Furnace='Y' THEN 100 ELSE 0 END,
  Maint= CASE WHEN fr.Maint='Y' THEN 100 ELSE 0 END,
  Interest= CASE WHEN fr.Interest='Y' THEN 100 ELSE 0 END,
  
  Lvl_ScheduleModel_P = CASE WHEN ((fr.Lvl_ScheduleModel='P' OR fr.Lvl_ScheduleModel IS NULL) AND fr.Use_ScheduleModel='Y')THEN 100 ELSE 0 END,
  Lvl_ScheduleModel_C= CASE WHEN fr.Lvl_ScheduleModel='C' THEN 100 ELSE 0 END,
  Lvl_ScheduleModel_B= CASE WHEN fr.Lvl_ScheduleModel='B' THEN 100 ELSE 0 END,
  Use_ScheduleModel = CASE WHEN (fr.Use_ScheduleModel='Y' OR fr.Lvl_ScheduleModel IS not null) THEN 100 ELSE 0 END
FROM FeedResources fr
INNER JOIN TSort t ON fr.Refnum=t.Refnum






