﻿CREATE VIEW [auth].[PlantsByLogin_Active]
WITH SCHEMABINDING
AS
SELECT
	jpl.[JoinId],
	jpl.[PlantId],
	jpl.[LoginId],
	p.[PlantName],
	l.[LoginTag]
FROM [auth].[JoinPlantLogin]	jpl
INNER JOIN [auth].[Plants]		p
	ON	p.[PlantId]		= jpl.[PlantId]
INNER JOIN [auth].[Logins]		l
	ON	l.[LoginId]		= jpl.[LoginId]
WHERE	jpl.[Active]	= 1
	AND	p.[Active]		= 1
	AND	l.[Active]		= 1;
GO
CREATE UNIQUE CLUSTERED INDEX [UX_PlantsByLogin_Active]
    ON [auth].[PlantsByLogin_Active]([PlantId] ASC, [LoginId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PlantsByLogin_Active_Get]
    ON [auth].[PlantsByLogin_Active]([LoginId] ASC)
    INCLUDE([PlantId], [PlantName], [LoginTag]);

