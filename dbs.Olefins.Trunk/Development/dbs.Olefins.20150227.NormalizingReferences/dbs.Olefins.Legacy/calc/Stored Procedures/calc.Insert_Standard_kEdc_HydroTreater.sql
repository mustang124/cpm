﻿CREATE PROCEDURE [calc].[Insert_Standard_kEdc_HydroTreater]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@ProcessUnitId		INT	= dim.Return_ProcessUnitId('PyroGasHydroTreat');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		map.[MethodologyId],
		fht.[SubmissionId],
		f.[StandardId],
		@ProcessUnitId,
		f.[Coefficient] * fht.[Quantity_kBSD]
	FROM	[fact].[FacilitiesHydroTreater]				fht
	INNER JOIN	[ante].[MapFactorHydroTreater]			map
		ON	map.[HydroTreaterTypeId] = CASE [fht].[HydroTreaterTypeId]
										WHEN 5 THEN 4
										ELSE [fht].[HydroTreaterTypeId]
										END
	INNER JOIN	[ante].[Factors]						f
		ON	f.[MethodologyId]	= map.[MethodologyId]
		AND	f.[FactorId]		= map.[FactorId]
		AND	f.[StandardId]		= @StandardId
	WHERE	map.[MethodologyId]	= @MethodologyId
		AND	fht.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;