﻿CREATE TABLE [calc].[FeedClass] (
    [MethodologyId]  INT                NOT NULL,
    [SubmissionId]   INT                NOT NULL,
    [FeedClassId]    INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedClass_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FeedClass_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FeedClass_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FeedClass_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedClass] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [FK_FeedClass_FeedClassId] FOREIGN KEY ([FeedClassId]) REFERENCES [dim].[FeedClass_LookUp] ([FeedClassId]),
    CONSTRAINT [FK_FeedClass_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_FeedClass_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_FeedClass_u]
	ON [calc].[FeedClass]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[FeedClass]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[FeedClass].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[calc].[FeedClass].[SubmissionId]		= INSERTED.[SubmissionId];

END;