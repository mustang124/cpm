﻿CREATE FUNCTION [calc].[Get_Capacity]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		c.[_PlantCapacity_MTSD],
		c.[_SupplementalInfer_MTSD]
	FROM [calc].[Capacity] c
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
);