﻿CREATE TABLE [dim].[Factor_Parent] (
    [MethodologyId]  INT                 NOT NULL,
    [FactorId]       INT                 NOT NULL,
    [ParentId]       INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Factor_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Factor_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Factor_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Factor_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Factor_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Factor_Parent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FactorId] ASC),
    CONSTRAINT [FK_Factor_Parent_LookUp_Facilities] FOREIGN KEY ([FactorId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_Factor_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_Factor_Parent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Factor_Parent_Operator] FOREIGN KEY ([Operator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Factor_Parent_Parent] FOREIGN KEY ([MethodologyId], [FactorId]) REFERENCES [dim].[Factor_Parent] ([MethodologyId], [FactorId])
);


GO

CREATE TRIGGER [dim].[t_Factor_Parent_u]
	ON [dim].[Factor_Parent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Factor_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Factor_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Factor_Parent].[FactorId]			= INSERTED.[FactorId];

END;