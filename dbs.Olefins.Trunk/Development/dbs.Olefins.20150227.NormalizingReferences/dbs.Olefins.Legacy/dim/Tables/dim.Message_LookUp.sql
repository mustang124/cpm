﻿CREATE TABLE [dim].[Message_LookUp] (
    [MessageId]      INT                IDENTITY (1, 1) NOT NULL,
    [MessageTag]     VARCHAR (42)       NOT NULL,
    [MessageName]    NVARCHAR (256)     NOT NULL,
    [MessageDetail]  NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Message_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Message_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Message_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Message_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Message_LookUp] PRIMARY KEY CLUSTERED ([MessageId] ASC),
    CONSTRAINT [CL_Message_LookUp_MessageDetail] CHECK ([MessageDetail]<>''),
    CONSTRAINT [CL_Message_LookUp_MessageName] CHECK ([MessageName]<>''),
    CONSTRAINT [CL_Message_LookUp_MessageTag] CHECK ([MessageTag]<>''),
    CONSTRAINT [UK_Message_LookUp_LanguageTag] UNIQUE NONCLUSTERED ([MessageTag] ASC),
    CONSTRAINT [UK_Message_LookUp_MessageDetail] UNIQUE NONCLUSTERED ([MessageDetail] ASC),
    CONSTRAINT [UK_Message_LookUp_MessageName] UNIQUE NONCLUSTERED ([MessageName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Message_LookUp_u]
	ON [dim].[Message_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Message_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Message_LookUp].[MessageId]	= INSERTED.[MessageId];

END;