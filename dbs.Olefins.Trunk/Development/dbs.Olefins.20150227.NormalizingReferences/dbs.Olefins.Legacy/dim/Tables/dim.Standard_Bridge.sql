﻿CREATE TABLE [dim].[Standard_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [StandardId]         INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Standard_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Standard_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Standard_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Standard_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Standard_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Standard_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_Standard_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Standard_LookUp] ([StandardId]),
    CONSTRAINT [FK_Standard_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Standard_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Standard_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [StandardId]) REFERENCES [dim].[Standard_Parent] ([MethodologyId], [StandardId]),
    CONSTRAINT [FK_Standard_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[Standard_Parent] ([MethodologyId], [StandardId]),
    CONSTRAINT [FK_Standard_Bridge_StandardID] FOREIGN KEY ([StandardId]) REFERENCES [dim].[Standard_LookUp] ([StandardId])
);


GO

CREATE TRIGGER [dim].[t_Standard_Bridge_u]
	ON [dim].[Standard_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Standard_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Standard_Bridge].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[dim].[Standard_Bridge].[StandardId]	= INSERTED.[StandardId]
		AND	[dim].[Standard_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;