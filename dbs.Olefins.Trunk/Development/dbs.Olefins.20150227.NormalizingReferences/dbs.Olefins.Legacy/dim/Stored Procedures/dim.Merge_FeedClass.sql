﻿CREATE PROCEDURE [dim].[Merge_FeedClass]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [dim].[FeedClass_LookUp] AS Target
	USING
	(
		VALUES
			('0', 'All', 'All Feed Classes'),
			('1', 'Ethane', 'Ethane (>85%)'),
			('2', 'LPG Feeds', 'LPG Feeds (C2+C3+C4 > 50%; C2<85%)'),
			('3', 'Mixed Feeds', 'Mixed Feeds (>5% gas oil, >10% ethane, propane and naphtha flexibility)'),
			('4', 'Naphtha', 'Naphtha (LIQ. FEEDS > 85%; G.O. < 25% & S.G. OF NAPH < 0.70)')
	)
	AS Source([FeedClassTag], [FeedClassName], [FeedClassDetail])
	ON	Target.[FeedClassTag]		= Source.[FeedClassTag]
	WHEN MATCHED THEN UPDATE SET
		Target.[FeedClassName]		= Source.[FeedClassName],
		Target.[FeedClassDetail]	= Source.[FeedClassDetail]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([FeedClassTag], [FeedClassName], [FeedClassDetail])
		VALUES([FeedClassTag], [FeedClassName], [FeedClassDetail])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	MERGE INTO [dim].[FeedClass_Parent] AS Target
	USING
	(
		SELECT
			m.MethodologyId,
			l.FeedClassId,
			p.FeedClassId,
			t.Operator,
			t.SortKey,
			'/'
		FROM (VALUES
			('0', '0', '+', 0),
			('1', '0', '+', 1),
			('2', '0', '+', 2),
			('3', '0', '+', 3),
			('4', '0', '+', 4)
			)	t(FeedClassTag, ParentTag, Operator, SortKey)
		INNER JOIN [dim].[FeedClass_LookUp]			l
			ON	l.FeedClassTag = t.FeedClassTag
		INNER JOIN [dim].[FeedClass_LookUp]			p
			ON	p.FeedClassTag = t.ParentTag
		INNER JOIN [ante].[Methodology]				m
			ON	m.[MethodologyTag] = '2013'
	)
	AS Source([MethodologyId], [FeedClassId], [ParentId], [Operator], [SortKey], [Hierarchy])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[FeedClassId]	= Source.[FeedClassId]
	WHEN MATCHED THEN UPDATE SET
		Target.[ParentId]		= Source.[ParentId],
		Target.[Operator]		= Source.[Operator],
		Target.[SortKey]		= Source.[SortKey],
		Target.[Hierarchy]		= Source.[Hierarchy]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [FeedClassId], [ParentId], [Operator], [SortKey], [Hierarchy])
		VALUES([MethodologyId], [FeedClassId], [ParentId], [Operator], [SortKey], [Hierarchy])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

	EXECUTE dim.Update_Parent 'dim', 'FeedClass_Parent', 'MethodologyId', 'FeedClassId', 'ParentId', 'SortKey', 'Hierarchy';
	EXECUTE dim.Merge_Bridge 'dim', 'FeedClass_Parent', 'dim', 'FeedClass_Bridge', 'MethodologyId', 'FeedClassId', 'SortKey', 'Hierarchy', 'Operator';

END;