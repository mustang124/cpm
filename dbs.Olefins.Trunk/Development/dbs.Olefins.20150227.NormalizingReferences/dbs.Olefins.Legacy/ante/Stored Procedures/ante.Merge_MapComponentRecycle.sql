﻿CREATE PROCEDURE [ante].[Merge_MapComponentRecycle]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapComponentRecycle] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_StreamId('RogEthane'), dim.Return_ComponentId('C2H6')),
			(@MethodologyId, dim.Return_StreamId('DilEthane'), dim.Return_ComponentId('C2H6')),
			(@MethodologyId, dim.Return_StreamId('RogPropane'), dim.Return_ComponentId('C3H8')),
			(@MethodologyId, dim.Return_StreamId('DilPropane'), dim.Return_ComponentId('C3H8')),
			(@MethodologyId, dim.Return_StreamId('DilButane'), dim.Return_ComponentId('C4H10')),
			(@MethodologyId, dim.Return_StreamId('RogC4Plus'), dim.Return_ComponentId('C4H10'))
	)
	AS Source([MethodologyId], [StreamId], [ComponentId])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[ComponentId]	= Source.[ComponentId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [ComponentId])
		VALUES([MethodologyId], [StreamId], [ComponentId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;