﻿CREATE PROCEDURE [ante].[Merge_MapRecycledBalance]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapRecycledBalance] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_StreamId('ConcButadiene'),	dim.Return_ComponentId('C4H6'),	NULL,	dim.Return_ComponentId('C4H10')),
			(@MethodologyId, dim.Return_StreamId('ConcBenzene'),	dim.Return_ComponentId('C6H6'),	NULL,	dim.Return_ComponentId('Other')),
			(@MethodologyId, dim.Return_StreamId('ConcEthylene'),	dim.Return_ComponentId('C2H4'),	NULL,	dim.Return_ComponentId('C2H6')),

			(@MethodologyId, dim.Return_StreamId('RogEthylene'),	dim.Return_ComponentId('C2H4'),	dim.Return_StreamId('RogEthane'),	dim.Return_ComponentId('C2H6')),
			(@MethodologyId, dim.Return_StreamId('RogPropylene'),	dim.Return_ComponentId('C3H6'),	dim.Return_StreamId('RogPropane'),	dim.Return_ComponentId('C3H8')),

			(@MethodologyId, dim.Return_StreamId('DilButadiene'),	dim.Return_ComponentId('C4H6'),	dim.Return_StreamId('DilButylene'),	dim.Return_ComponentId('C4H8')),
			(@MethodologyId, dim.Return_StreamId('DilBenzene'),		dim.Return_ComponentId('C6H6'),	dim.Return_StreamId('DilMoGas'),	dim.Return_ComponentId('Other')),
			(@MethodologyId, dim.Return_StreamId('DilEthylene'),	dim.Return_ComponentId('C2H4'),	dim.Return_StreamId('DilEthane'),	dim.Return_ComponentId('C2H6')),
			(@MethodologyId, dim.Return_StreamId('DilPropylene'),	dim.Return_ComponentId('C3H6'),	dim.Return_StreamId('DilPropane'),	dim.Return_ComponentId('C3H8'))
	)
	AS Source([MethodologyId], [RecStreamId], [RecComponentId], [BalStreamId], [BalComponentId])
	ON	Target.[MethodologyId]		= Source.[MethodologyId]
	AND	Target.[RecStreamId]		= Source.[RecStreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[RecComponentId]		= Source.[RecComponentId],
		Target.[BalStreamId]		= Source.[BalStreamId],
		Target.[BalComponentId]		= Source.[BalComponentId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [RecStreamId], [RecComponentId], [BalStreamId], [BalComponentId])
		VALUES([MethodologyId], [RecStreamId], [RecComponentId], [BalStreamId], [BalComponentId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;