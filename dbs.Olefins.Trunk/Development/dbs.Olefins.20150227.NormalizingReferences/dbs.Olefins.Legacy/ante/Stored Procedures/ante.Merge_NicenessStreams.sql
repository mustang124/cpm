﻿CREATE PROCEDURE [ante].[Merge_NicenessStreams]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[NicenessStreams] AS Target
	USING
	(
		VALUES
		(@MethodologyId, [dim].[Return_StreamId]('Condensate'),	0.95, 0.983),
		(@MethodologyId, [dim].[Return_StreamId]('HeavyNGL'),	0.95, 0.983),
		(@MethodologyId, [dim].[Return_StreamId]('NaphthaLt'),	0.95, 1.001),
		(@MethodologyId, [dim].[Return_StreamId]('Raffinate'),	0.95, 1.001),
		(@MethodologyId, [dim].[Return_StreamId]('NaphthaFr'),	0.95, 0.986),
		(@MethodologyId, [dim].[Return_StreamId]('NaphthaHv'),	0.95, 0.966),

		(@MethodologyId, [dim].[Return_StreamId]('Diesel'),		NULL, 0.8443),
		(@MethodologyId, [dim].[Return_StreamId]('GasOilHv'),	NULL, 0.8742),
		(@MethodologyId, [dim].[Return_StreamId]('GasOilHt'),	NULL, 0.8371)
	)
	AS Source([MethodologyId], [StreamId], [EmptyComposition], [Divisor])
	ON	Target.[MethodologyId]		= Source.[MethodologyId]
	AND	Target.[StreamId]			= Source.[StreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[EmptyComposition]	= Source.[EmptyComposition],
		Target.[Divisor]			= Source.[Divisor]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [EmptyComposition], [Divisor])
		VALUES([MethodologyId], [StreamId], [EmptyComposition], [Divisor])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;