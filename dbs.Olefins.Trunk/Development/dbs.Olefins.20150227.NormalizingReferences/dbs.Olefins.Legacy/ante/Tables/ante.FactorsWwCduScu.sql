﻿CREATE TABLE [ante].[FactorsWwCduScu] (
    [MethodologyId]  INT                NOT NULL,
    [StandardId]     INT                NOT NULL,
    [WwCduScu]       FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FactorsWwCduScu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FactorsWwCduScu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FactorsWwCduScu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FactorsWwCduScu_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FactorsWwCduScu] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC),
    CONSTRAINT [CR_FactorsWwCduScu_WwCduScu_MinIncl_0.0] CHECK ([WwCduScu]>=(0.0)),
    CONSTRAINT [FK_FactorsWwCduScu_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_FactorsWwCduScu_Standards_LookUp] FOREIGN KEY ([StandardId]) REFERENCES [dim].[Standard_LookUp] ([StandardId])
);


GO

CREATE TRIGGER [ante].[t_FactorsWwCduScu_u]
	ON [ante].[FactorsWwCduScu]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[FactorsWwCduScu]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[FactorsWwCduScu].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[FactorsWwCduScu].[StandardId]			= INSERTED.[StandardId];

END;