﻿CREATE TABLE [ante].[Factors] (
    [MethodologyId]  INT                NOT NULL,
    [StandardId]     INT                NOT NULL,
    [FactorId]       INT                NOT NULL,
    [Coefficient]    FLOAT (53)         NOT NULL,
    [Exponent]       FLOAT (53)         NOT NULL,
    [ValueMin]       FLOAT (53)         NULL,
    [ValueMax]       FLOAT (53)         NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Factors_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Factors_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Factors_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Factors_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Factors] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC),
    CONSTRAINT [CV_Factors_MinMax] CHECK ([ValueMin]<=[ValueMax]),
    CONSTRAINT [FK_Factors_Factor_LookUp] FOREIGN KEY ([FactorId]) REFERENCES [dim].[Factor_LookUp] ([FactorId]),
    CONSTRAINT [FK_Factors_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Factors_Standards_LookUp] FOREIGN KEY ([StandardId]) REFERENCES [dim].[Standard_LookUp] ([StandardId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_kEdc]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(1));


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_PersMaint]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(4));


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_PersNonMaint]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(5));


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_Mes]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(6));


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Factors_NonEnergy]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC, [FactorId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(7));


GO
CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_PersMaint]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(4));


GO
CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_PersNonMaint]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(5));


GO
CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_Mes]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(6));


GO
CREATE NONCLUSTERED INDEX [IX_FactorsWwCduScu_NonEnergy]
    ON [ante].[Factors]([MethodologyId] DESC, [StandardId] ASC)
    INCLUDE([Coefficient], [Exponent], [ValueMin], [ValueMax]) WHERE ([StandardId]=(7));


GO

CREATE TRIGGER [ante].[t_Factors_u]
	ON [ante].[Factors]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Factors]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Factors].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[Factors].[StandardId]			= INSERTED.[StandardId]
		AND	[ante].[Factors].[FactorId]				= INSERTED.[FactorId];

END;