﻿CREATE TABLE [ante].[MapEiiModelComponents] (
    [MethodologyId]    INT                NOT NULL,
    [FreshComponentId] INT                NOT NULL,
    [ModelComponentId] INT                NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_MapEiiModelComponents_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)     CONSTRAINT [DF_MapEiiModelComponents_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)     CONSTRAINT [DF_MapEiiModelComponents_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)     CONSTRAINT [DF_MapEiiModelComponents_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapEiiModelComponents] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [FreshComponentId] ASC),
    CONSTRAINT [FK_MapEiiModelComponents_Fresh] FOREIGN KEY ([FreshComponentId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_MapEiiModelComponents_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_MapEiiModelComponents_Model] FOREIGN KEY ([ModelComponentId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_MapEiiModelComponents_u]
	ON [ante].[MapEiiModelComponents]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapEiiModelComponents]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapEiiModelComponents].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[ante].[MapEiiModelComponents].[ModelComponentId]	= INSERTED.[ModelComponentId];

END;