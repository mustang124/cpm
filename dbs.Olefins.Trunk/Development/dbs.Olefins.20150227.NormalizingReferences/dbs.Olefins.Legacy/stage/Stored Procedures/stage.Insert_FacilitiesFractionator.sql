﻿CREATE PROCEDURE [stage].[Insert_FacilitiesFractionator]
(
	@SubmissionId			INT,
	@FacilityId				INT,

	@Quantity_kBSD			FLOAT,
	@StreamId				INT,
	@Throughput_kMT			FLOAT,
	@Density_SG				FLOAT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [stage].[FacilitiesFractionator]([SubmissionId], [FacilityId], [Quantity_kBSD], [StreamId], [Throughput_kMT], [Density_SG])
	SELECT
		@SubmissionId,
		@FacilityId,
		@Quantity_kBSD,
		@StreamId,
		@Throughput_kMT,
		@Density_SG
	WHERE	@Quantity_kBSD		>= 0.0
		OR	@StreamId			>= 1
		OR	@Throughput_kMT		>= 0.0
		OR	@Density_SG			>= 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @FacilityId:'		+ CONVERT(VARCHAR, @FacilityId))
			+ COALESCE(', @Quantity_kBSD:'	+ CONVERT(VARCHAR, @Quantity_kBSD),		'')
			+ COALESCE(', @StreamId:'		+ CONVERT(VARCHAR, @StreamId),			'')
			+ COALESCE(', @Throughput_kMT:'	+ CONVERT(VARCHAR, @Throughput_kMT),	'')
			+ COALESCE(', @Density_SG:'		+ CONVERT(VARCHAR, @Density_SG),		'');

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;