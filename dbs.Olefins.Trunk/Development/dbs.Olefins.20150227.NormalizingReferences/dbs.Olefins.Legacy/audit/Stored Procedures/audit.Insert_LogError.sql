﻿CREATE PROCEDURE [audit].[Insert_LogError]
(
	@ProcId			INT,
	@Parameters		VARCHAR (4000) = NULL,
	@XActState		SMALLINT
)
AS
BEGIN

	SET NOCOUNT ON;

	IF (RTRIM(LTRIM(@Parameters)) = '') BEGIN SET @Parameters = NULL; END;

	INSERT INTO [audit].[LogError](
		[ErrorNumber],
		[XActState],
		[ProcedureSchema],	[ProcedureName],		[ProcedureLine],		[ProcedureParam],
		[ErrorMessage],		[ErrorSeverity],		[ErrorState],
		[tsModified],		[tsModifiedHost],		[tsModifiedUser],		[tsModifiedApp])
	SELECT
		ERROR_NUMBER(),
		@XActState,
		OBJECT_SCHEMA_NAME(@ProcId),		OBJECT_NAME(@ProcId),	ERROR_LINE(),		@Parameters,
		ERROR_MESSAGE(),	ERROR_SEVERITY(),	ERROR_STATE(),
		SYSDATETIMEOFFSET(),HOST_NAME(),		SUSER_SNAME(),		APP_NAME();

	DECLARE @ERROR_MESSAGE		VARCHAR (MAX)  = ERROR_MESSAGE();
	DECLARE @ERROR_SEVERITY		VARCHAR (4000) = ERROR_SEVERITY();
	DECLARE @ERROR_STATE		VARCHAR (4000) = ERROR_STATE();

	DECLARE @Index	INT = CHARINDEX(CHAR(10), @ERROR_MESSAGE);
	DECLARE @Len	INT = LEN(@ERROR_MESSAGE)

	SET @ERROR_MESSAGE = CHAR(13) + CHAR(10) +
		'Msg ' + CONVERT(VARCHAR(20), ERROR_NUMBER()) +
		', Level ' + CONVERT(VARCHAR(20), ERROR_SEVERITY()) +
		', State ' + CONVERT(VARCHAR(20), ERROR_STATE()) +
		', Procedure ' + SCHEMA_NAME() + '.' + ERROR_PROCEDURE() +
		', Line ' + CONVERT(VARCHAR(20), ERROR_LINE()) +
		CHAR(13) + CHAR(10) +
		RIGHT(@ERROR_MESSAGE, @Len - @Index)+
		CHAR(13) + CHAR(10) +
		@Parameters;

	RAISERROR(@ERROR_MESSAGE, @ERROR_SEVERITY, @ERROR_STATE) WITH NOWAIT, SETERROR;

END;