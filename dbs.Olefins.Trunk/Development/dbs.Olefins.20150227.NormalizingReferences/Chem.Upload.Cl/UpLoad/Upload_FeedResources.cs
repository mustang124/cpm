﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadFeedResources(Excel.Workbook wkb, string Refnum, uint StudyYear)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 y = 0;
			if (StudyYear >= 2009) { y = 2; }
			bool b = false;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table11"];

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_FeedResources]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum			CHAR (9),
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				#region Requirements

				c = 8;

				//@MinParaffin           REAL         = NULL
				r = 65 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@MinParaffin", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@MaxVaporPresFRS       REAL         = NULL
				r = 66 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@MaxVaporPresFRS", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Implement (Use)

				c = 7;

				//@Use_LP_PC             CHAR (1)     = NULL
				r = 89 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_LP_PC", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_LP_Main           CHAR (1)     = NULL
				//r = 92;
				//rng = wks.Cells[r, c];
				//if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_LP_Main", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_ScheduleModel     CHAR (1)     = NULL
				r = 90 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_ScheduleModel", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_LPMultiPlant      CHAR (1)     = NULL
				r = 91 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_LPMultiPlant", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_LPMultiPeriod     CHAR (1)     = NULL
				r = 92 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_LPMultiPeriod", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_TierData          CHAR (1)     = NULL
				r = 93 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_TierData", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_TierProduct       CHAR (1)     = NULL
				r = 94 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_TierProduct", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_NonLP             CHAR (1)     = NULL
				r = 95 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_NonLP", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_Spread            CHAR (1)     = NULL
				r = 96 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_Spread", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_MarginData        CHAR (1)     = NULL
				r = 97 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_MarginData", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_AltFeed           CHAR (1)     = NULL
				r = 98 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_AltFeed", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Use_BEAltFeed         CHAR (1)     = NULL
				r = 99 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_BEAltFeed", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }
		
				//@Use_YieldPredict      CHAR (1)     = NULL
				r = 100 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Use_YieldPredict", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				#endregion

				#region Location (LVL)

				c = 8;

				//@Lvl_LP_PC             CHAR (1)     = NULL
				r = 89 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_LP_PC", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_LP_Main           CHAR (1)     = NULL
				//r = 92;
				//rng = wks.Cells[r, c];
				//if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_LP_Main", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_ScheduleModel     CHAR (1)     = NULL
				r = 90 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_ScheduleModel", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_LPMultiPlant      CHAR (1)     = NULL
				r = 91 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_LPMultiPlant", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_LPMultiPeriod     CHAR (1)     = NULL
				r = 92 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_LPMultiPeriod", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_TierData          CHAR (1)     = NULL
				r = 93 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_TierData", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_TierProduct       CHAR (1)     = NULL
				r = 94 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_TierProduct", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_NonLP             CHAR (1)     = NULL
				r = 95 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_NonLP", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_Spread            CHAR (1)     = NULL
				r = 96 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_Spread", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_MarginData        CHAR (1)     = NULL
				r = 97 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_MarginData", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_AltFeed           CHAR (1)     = NULL
				r = 98 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_AltFeed", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_BEAltFeed         CHAR (1)     = NULL
				r = 99 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_BEAltFeed", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Lvl_YieldPredict      CHAR (1)     = NULL
				r = 100 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Lvl_YieldPredict", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				#endregion

				//Done: FeedResources - Verify Region Is Not Imported (Source)
				#region Source (Src)

				if (StudyYear <= 1989)
				{
					c = 9;

					//@Src_LP_PC             CHAR (1)     = NULL
					r = 89 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_LP_PC", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_LP_Main           CHAR (1)     = NULL
					r = 90 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_LP_Main", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_LPMultiPlant      CHAR (1)     = NULL
					r = 91 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_LPMultiPlant", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_LPMultiPeriod     CHAR (1)     = NULL
					r = 92 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_LPMultiPeriod", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_TierData          CHAR (1)     = NULL
					r = 93 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_TierData", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_TierProduct       CHAR (1)     = NULL
					r = 94 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_TierProduct", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_NonLP             CHAR (1)     = NULL
					r = 95 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_NonLP", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_Spread            CHAR (1)     = NULL
					r = 96 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_Spread", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_MarginData        CHAR (1)     = NULL
					r = 97 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_MarginData", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_AltFeed           CHAR (1)     = NULL
					r = 98 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_AltFeed", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_BEAltFeed         CHAR (1)     = NULL
					r = 99 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_BEAltFeed", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Src_YieldPredict      CHAR (1)     = NULL
					r = 100 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Src_YieldPredict", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }
				}

				#endregion

				#region Yield Patterns

				c = 7;

				//@YieldPatternCnt       REAL         = NULL
				r = 101 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@YieldPatternCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@YieldPatternLPCnt     REAL         = NULL
				r = 102 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@YieldPatternLPCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@YieldGroupCnt         REAL         = NULL
				r = 103 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@YieldGroupCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				#endregion

				#region Personnel (Plant)

				c = 7;

				//@Plant_PPCnt           REAL         = NULL
				r = 128 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Plant_PPCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Plant_3PlusCnt        REAL         = NULL
				r = 129 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Plant_3PlusCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Plant_BuyerCnt        REAL         = NULL
				r = 130 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Plant_BuyerCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Plant_DataCnt         REAL         = NULL
				r = 131 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Plant_DataCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Plant_Logistic        REAL         = NULL
				r = 132 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Plant_Logistic", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Plant_Decision        CHAR (1)     = NULL
				r = 133 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Plant_Decision", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				#endregion

				#region Personnel (Corp)

				c = 8;

				//@Corp_PPCnt            REAL         = NULL
				r = 128 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Corp_PPCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Corp_3PlusCnt         REAL         = NULL
				r = 129 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Corp_3PlusCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Corp_BuyerCnt         REAL         = NULL
				r = 130 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Corp_BuyerCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Corp_DataCnt          REAL         = NULL
				r = 131 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Corp_DataCnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Corp_Logistic         REAL         = NULL
				r = 132 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Corp_Logistic", SqlDbType.Float).Value = ReturnFloat(rng); }
	
				//@Corp_Decision         CHAR (1)     = NULL
				r = 133 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Corp_Decision", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				#endregion

				//@PlanTimeDays REAL = NULL
				c = 6;
				r = 151 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PlanTimeDays", SqlDbType.Float).Value = ReturnFloat(rng); }

				#region Penalty Costs

				c = 10;

				//@FeedSlateChg          CHAR (1)     = NULL
				r = 154 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedSlateChg", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Reliab                CHAR (1)     = NULL
				r = 155 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Reliab", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Environ               CHAR (1)     = NULL
				r = 156 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Environ", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Furnace               CHAR (1)     = NULL
				r = 157 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Furnace", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Maint                 CHAR (1)     = NULL
				r = 158 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Maint", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@Interest              CHAR (1)     = NULL
				r = 159 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Interest", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				#endregion

				#region Ability to Respond to Change

				c = 10;

				//@EffFeedChg            REAL         = NULL
				r = 163 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@EffFeedChg", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@FeedPurchase          REAL         = NULL
				r = 164 + y;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedPurchase", SqlDbType.Float).Value = ReturnFloat(rng); }

				//r = 0;
				//c = 0;
				//rng = wks.Cells[r, c];
				//if (RangeHasValue(rng))
				//{
				//	FeedResourcesBuffer.FeedPurchase = ReturnFloat(rng);
				//}
				//else
				//{
				//	FeedResourcesBuffer.FeedPurchase_IsNull = true;
				//}

				#endregion

				#region Yield Predision Software (YPS)

				b = false;
				r = 107 + y;

				//@YPS_Lummus            TINYINT      = NULL
				c = 21;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@YPS_Lummus", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@YPS_SPYRO             TINYINT      = NULL
				c = 22;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@YPS_SPYRO", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@YPS_SelfDev           TINYINT      = NULL
				c = 23;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@YPS_SelfDev", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@YPS_Other             TINYINT      = NULL
				c = 24;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@YPS_Other", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@YPS_RPT               TINYINT      = NULL
				if (b)
				{
					cmd.Parameters.Add("@YPS_RPT", SqlDbType.TinyInt).Value = 1;
				}
				else
				{
					cmd.Parameters.Add("@YPS_RPT", SqlDbType.TinyInt).Value = 0;
				}

				//@YPS_OtherDesc         VARCHAR (30) = NULL
				c = 6;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@YPS_OtherDesc", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

				#endregion

				#region Linear Program (LP)

				b = false;
				r = 114 + y;

				//@LP_Aspen              TINYINT      = NULL
				c = 21;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@LP_Aspen", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@LP_Honeywell          TINYINT      = NULL
				c = 22;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@LP_Honeywell", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@LP_Haverly            TINYINT      = NULL
				c = 23;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@LP_Haverly", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@LP_SelfDev            TINYINT      = NULL
				c = 24;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@LP_SelfDev", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@LP_Other              TINYINT      = NULL
				c = 25;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@LP_Other", SqlDbType.TinyInt).Value = ReturnByte(rng, true); 
				if (ReturnByte(rng, true) == 1) b = true;

				//@LP_OtherDesc          VARCHAR (30) = NULL
				c = 7;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_OtherDesc", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

				//@LP_RPT                TINYINT      = NULL
				if (b)
				{
					cmd.Parameters.Add("@LP_RPT", SqlDbType.TinyInt).Value = 1;
				}
				else
				{
					cmd.Parameters.Add("@LP_RPT", SqlDbType.TinyInt).Value = 0;
				}

				#endregion

				//@LP_RowCnt INT = NULL
				r = 116 + y;
				c = 7;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_RowCnt", SqlDbType.Int).Value = ReturnUShort(rng); }

				#region Beyond Cracker

				b = false;
				r = 124 + y;

				//@BeyondCrack_None      TINYINT      = NULL
				c = 21;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_None", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_C4        TINYINT      = NULL
				c = 22;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_C4", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_C5        TINYINT      = NULL
				c = 23;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_C5", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_Pygas     TINYINT      = NULL
				c = 24;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_Pygas", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_BTX       TINYINT      = NULL
				c = 25;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_BTX", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_OthChem   TINYINT      = NULL
				c = 26;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_OthChem", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_OthRefine TINYINT      = NULL
				c = 27;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_OthRefine", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_Oth       TINYINT      = NULL
				c = 28;
				rng = wks.Cells[r, c];
				cmd.Parameters.Add("@BeyondCrack_Oth", SqlDbType.TinyInt).Value = ReturnByte(rng, true);
				if (ReturnByte(rng, true) == 1) b = true;

				//@BeyondCrack_OthDesc   VARCHAR (75) = NULL
				c = 10;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@BeyondCrack_OthDesc", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

				//@BeyondCrack_RPT       TINYINT      = NULL
				if (b)
				{
					cmd.Parameters.Add("@BeyondCrack_RPT", SqlDbType.TinyInt).Value = 1;
				}
				else
				{
					cmd.Parameters.Add("@BeyondCrack_RPT", SqlDbType.TinyInt).Value = 0;
				}

				#endregion

				//Done: FeedResources - Verify Region Is Not Imported
				#region Invalid

				if (StudyYear <= 1989)
				{
					#region LP

					//@LP_Recur_Yes          TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Recur_Yes", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_Recur_No           TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Recur_No", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_Recur_NotSure      TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Recur_NotSure", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_Recur_RPT          TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Recur_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_DeltaBase_Yes      TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_DeltaBase_Yes", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_DeltaBase_No       TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_DeltaBase_No", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_DeltaBase_NotSure  TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_DeltaBase_NotSure", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_DeltaBase_RPT      TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_DeltaBase_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_MixInt_Yes         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_MixInt_Yes", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_MixInt_No          TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_MixInt_No", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_MixInt_NotSure     TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_MixInt_NotSure", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_MixInt_RPT         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_MixInt_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_Online_Yes         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Online_Yes", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_Online_No          TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Online_No", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_Online_NotSure     TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Online_NotSure", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@LP_Online_RPT         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@LP_Online_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region Data

					//@Data_LPSpecial        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_LPSpecial", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Data_Econ             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_Econ", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Data_FeedSupply       TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_FeedSupply", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Data_Market           TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_Market", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Data_TechSrvs         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_TechSrvs", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Data_Oper             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_Oper", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Data_Oth              TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_Oth", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Data_OthDesc          VARCHAR (30) = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_OthDesc", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }
	
					//@Data_RPT TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Data_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }
	
					#endregion

					#region Dev

					//@Dev_LPSpecial         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_LPSpecial", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Dev_Econ              TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_Econ", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Dev_FeedSupply        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_FeedSupply", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Dev_Market            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_Market", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Dev_TechSrvs          TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_TechSrvs", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Dev_Oper              TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_Oper", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Dev_Oth               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_Oth", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Dev_OthDesc           VARCHAR (30) = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_OthDesc", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

					//@Dev_RPT TINYINT = NULL,
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Dev_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region Valid

					//@Valid_LPSpecial       TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_LPSpecial", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Valid_Econ            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_Econ", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Valid_FeedSupply      TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_FeedSupply", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Valid_Market          TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_Market", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Valid_TechSrvs        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_TechSrvs", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Valid_Oper            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_Oper", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Valid_Oth             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_Oth", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Valid_OthDesc         VARCHAR (30) = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_OthDesc", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

					//@Valid_RPT TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region Constraint

					//@Constraint_LPSpecial  TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Constraint_LPSpecial", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Constraint_Econ       TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Constraint_Econ", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Constraint_FeedSupply TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Constraint_FeedSupply", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Constraint_Market     TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Constraint_Market", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Constraint_TechSrvs   TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Constraint_TechSrvs", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Constraint_Oper       TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Constraint_Oper", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Constraint_Oth        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Constraint_Oth", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@Constraint_OthDesc    VARCHAR (30) = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_LPSpecial", SqlDbType.VarChar, 30).Value = ReturnString(rng, 30); }

					//@Valid_RPT TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Valid_RPT", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion
					
					#region FeedPrice

					//@FeedPrice_LPSpecial   TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@FeedPrice_Econ        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@FeedPrice_FeedSupply  TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@FeedPrice_Market      TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@FeedPrice_TechSrvs    TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@FeedPrice_Oper        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@FeedPrice_Oth         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@FeedPrice_OthDesc     VARCHAR (75) = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

					//@FeedPrice_RPT	TINYINT = NULL,
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region ProdPrice

					//@ProdPrice_LPSpecial   TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@ProdPrice_Econ        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@ProdPrice_FeedSupply  TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@ProdPrice_Market      TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@ProdPrice_TechSrvs    TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@ProdPrice_Oper        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@ProdPrice_Oth         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@ProdPrice_OthDesc     VARCHAR (75) = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.VarChar, 75).Value = ReturnString(rng, 75); }

					//@ProdPrice_RPT		TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPCom

					//@PPCom_1               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_2               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_3               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_4               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_5               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_6               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_7               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_8               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_9               TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPCom_RPT			TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPFeedPrice

					//@PPFeedPrice_1         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_2         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_3         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_4         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_5         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_6         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_7         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_8         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPrice_9         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedPriceRPT_RPT				TINYINT = NULL,
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPFeedCrack

					//@PPFeedCrack_1         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_2         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_3         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_4         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_5         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_6         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_7         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_8         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_9         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedCrack_RPT	TINYINT = NULL,
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPFeedAvail

					//@PPFeedAvail_1         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_2         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_3         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_4         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_5         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_6         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_7         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_8         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_9         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPFeedAvail_RPT TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPProdPrice

					//@PPProdPrice_1         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_2         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_3         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_4         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_5         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_6         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_7         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_8         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_9         TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdPrice_RPT		TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPProdDemand

					//@PPProdDemand_1        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_2        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_3        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_4        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_5        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_6        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_7        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_8        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_9        TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdDemand_RPT		TINYINT = NULL,
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPStatus

					//@PPStatus_1            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_2            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_3            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_4            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_5            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_6            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_7            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_8            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPStatus_9            TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPProdStatus_RPT		TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					#region PPTools

					//@PPTools_1             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_2             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_3             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_4             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_5             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_6             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_7             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_8             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_9             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPTools_RPT		TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng))
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }
					#endregion

					#region PPExper

					//@PPExper_1             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_2             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_3             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_4             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_5             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_6             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_7             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_8             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_9             TINYINT      = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					//@PPExper_RPT	TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }

					#endregion

					//@PPStatus_RPT		TINYINT = NULL
					r = 0;
					c = 0;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@", SqlDbType.TinyInt).Value = ReturnByte(rng); }
				}

				#endregion

				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFeedResources", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_FeedResources]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
