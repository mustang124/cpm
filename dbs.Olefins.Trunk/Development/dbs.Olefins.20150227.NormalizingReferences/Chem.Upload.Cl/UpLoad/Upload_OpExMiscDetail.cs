﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadOpExMiscDetail(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table4"];

			object[] itm = new object[2]
			{
				new object[3] { "NVE", 73, 81 },
				new object[3] { "VE", 86, 94 }
			};


			foreach (object[] o in itm)
			{
				rBeg = Convert.ToUInt32(o[1]);
				rEnd = Convert.ToUInt32(o[2]);

				for (r = rBeg; r <= rEnd; r++)
				{
					try
					{
						c = 5;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
						{
							SqlCommand cmd = new SqlCommand("[stgFact].[Insert_OpExMiscDetail]", cn);
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum      CHAR (9),
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@OpexID      VARCHAR (6),
							cmd.Parameters.Add("@OpexID", SqlDbType.VarChar, 6).Value = (string)o[0] + Convert.ToString(r - rBeg + 1);

							//@Description VARCHAR (100) = NULL,
							c = 3;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Description", SqlDbType.VarChar, 100).Value = ReturnString(rng, 100); }

							//@Amount      REAL          = NULL
							c = 5;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Amount", SqlDbType.Float).Value = ReturnFloat(rng); }

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadOpExMiscDetail", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_OpExMiscDetail]", ex);
					}
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
			itm = null;
		}
	}
}
