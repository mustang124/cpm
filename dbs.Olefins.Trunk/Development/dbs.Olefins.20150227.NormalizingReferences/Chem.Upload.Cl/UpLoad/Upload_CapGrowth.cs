﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadCapGrowth(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table1-2"];

			rBeg = 11;
			rEnd = 23;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_CapGrowth]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum			CHAR (9),
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@Year			INT,
					c = 5;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Year", SqlDbType.Int).Value = ReturnUShort(rng); }

					//@EthylProdn	REAL	= NULL,
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthylProdn", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@PropylProdn	REAL	= NULL,
					c = 8;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropylProdn", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@EthylCap		REAL	= NULL
					c = 9;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthylCap", SqlDbType.Float).Value = ReturnFloat(rng); }

					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCapGrowth", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_CapGrowth]", ex);
				}
			}

			rBeg = 31;
			rEnd = 33;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_CapGrowth]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum			CHAR (9),
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@Year				INT,
					c = 5;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Year", SqlDbType.Int).Value = ReturnUShort(rng); }

					//@EthylProdn		REAL	= NULL,
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthylProdn", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@PropylProdn		REAL	= NULL,
					c = 8;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropylProdn", SqlDbType.Float).Value = ReturnFloat(rng); }

					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCapGrowth", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_CapGrowth]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
