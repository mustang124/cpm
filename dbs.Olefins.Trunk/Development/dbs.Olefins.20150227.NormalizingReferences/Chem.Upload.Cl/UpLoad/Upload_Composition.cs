﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadComposition(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table3"];

			object[] itm = new object[2]
			{
				new object[2] { "OthProd1", 6 },
				new object[2] { "OthProd2", 9 }
			};

			foreach (object[] s in itm)
			{
				try
				{
					c = Convert.ToUInt32(s[1]);

					r = 66;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng) && ReturnFloat(rng) > 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Composition]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum		CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@FeedProdID	VARCHAR (20)
						cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = Convert.ToString(s[0]);

						//@H2			REAL			= NULL
						r = 52;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@H2", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CH4			REAL			= NULL
						r = 53;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CH4", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@C2H2			REAL			= NULL
						r = 54;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@C2H2", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@C2H4			REAL			= NULL
						r = 55;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@C2H4", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@C2H6			REAL			= NULL
						r = 56;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@C2H6", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@C3H6			REAL			= NULL
						r = 57;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@C3H6", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@C3H8			REAL			= NULL
						r = 58;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@C3H8", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@BUTAD		REAL			= NULL
						r = 59;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@BUTAD", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@C4S			REAL			= NULL
						r = 60;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@C4S", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@C4H10		REAL			= NULL
						r = 61;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@C4H10", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@BZ			REAL			= NULL
						r = 62;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@BZ", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PYGAS		REAL			= NULL
						r = 63;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PYGAS", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PYOIL		REAL			= NULL
						r = 64;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PYOIL", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@INERT		REAL			= NULL
						r = 65;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@INERT", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadComposition", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Composition]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
