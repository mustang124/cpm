﻿CREATE VIEW [reports].FeedSelections AS
Select
  GroupId = r.GroupId
, FeedProdID			= CASE WHEN f.FeedProdID in ('LPGs','LPG') THEN 'LPG' ELSE f.FeedProdID END
, Mix = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Mix = 'Y' THEN 100.0 ELSE 0 END, 1.0)
, Blend = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Blend = 'Y' THEN 100.0 ELSE 0 END, 1.0)
, Fractionation = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Fractionation = 'Y' THEN 100.0 ELSE 0 END, 1.0)
, Hydrotreat = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Hydrotreat = 'Y' THEN 100.0 ELSE 0 END, 1.0)
, Purification = [$(DbGlobal)].dbo.WtAvg(CASE WHEN Purification = 'Y' THEN 100.0 ELSE 0 END, 1.0)
, MinSGEst = [$(DbGlobal)].dbo.WtAvgNZ(MinSGEst, 1.0)
, MaxSGEst = [$(DbGlobal)].dbo.WtAvgNZ(MaxSGEst, 1.0)
, MinSGAct = [$(DbGlobal)].dbo.WtAvgNZ(MinSGAct, 1.0)
, MaxSGAct = [$(DbGlobal)].dbo.WtAvgNZ(MaxSGAct, 1.0)
, PipelinePct = [$(DbGlobal)].dbo.WtAvg(PipelinePct, 1.0) * 100.0
, TruckPct = [$(DbGlobal)].dbo.WtAvg(TruckPct, 1.0) * 100.0
, BargePct = [$(DbGlobal)].dbo.WtAvg(BargePct, 1.0) * 100.0
, ShipPct = [$(DbGlobal)].dbo.WtAvg(ShipPct, 1.0) * 100.0
FROM reports.GroupPlants r JOIN dbo.FeedSelections f on f.Refnum=r.Refnum
GROUP by r.GroupId, CASE WHEN f.FeedProdID in ('LPGs','LPG') THEN 'LPG' ELSE f.FeedProdID END
