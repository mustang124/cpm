﻿
CREATE VIEW [reports].Prac AS
Select
  GroupId = r.GroupId
, AffGasPcntNonDisc				= [$(DbGlobal)].dbo.WtAvg(p.AffGasPcntNonDisc, 1.0) * 100.0
, NonAffGasPcnt				= [$(DbGlobal)].dbo.WtAvg(p.NonAffGasPcnt, 1.0) * 100.0

FROM reports.GroupPlants r JOIN dbo.Prac p on p.Refnum=r.Refnum
GROUP by r.GroupId
