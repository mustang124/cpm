﻿
CREATE VIEW [reports].[MarginCalc] AS
Select
  GroupId = r.GroupId
, DataType				= CASE m.DataType
							WHEN 'ETHDiv_MT' THEN 'USDPerEthMt'
							WHEN 'HVC_LB' THEN 'USCentPerHVCLb'
							WHEN 'HVC_MT' THEN 'USDPerHVCMt'
							ELSE m.DataType
							END
, m.MarginAnalysis
, Ethylene				= [$(DbGlobal)].dbo.WtAvg(m.Ethylene, m.DivisorValue)
, Propylene				= [$(DbGlobal)].dbo.WtAvg(m.Propylene, m.DivisorValue)
, Butadiene				= [$(DbGlobal)].dbo.WtAvg(m.Butadiene, m.DivisorValue)
, Hydrogen				= [$(DbGlobal)].dbo.WtAvg(m.Hydrogen, m.DivisorValue)
, Benzene				= [$(DbGlobal)].dbo.WtAvg(m.Benzene, m.DivisorValue)
, Acetylene				= [$(DbGlobal)].dbo.WtAvg(m.Acetylene, m.DivisorValue)
, ProdLoss				= [$(DbGlobal)].dbo.WtAvg(m.ProdLoss, m.DivisorValue)
, TotProdValue			= [$(DbGlobal)].dbo.WtAvg(m.ProdLoss, m.DivisorValue)
, FreshPyroFeed			= [$(DbGlobal)].dbo.WtAvg(m.FreshPyroFeed, m.DivisorValue)
, SuppTot				= [$(DbGlobal)].dbo.WtAvg(m.SuppTot, m.DivisorValue)
, GrossFeedStockCost	= [$(DbGlobal)].dbo.WtAvg(m.FreshPyroFeed + m.SuppTot, m.DivisorValue)
, ByProdCredit			= [$(DbGlobal)].dbo.WtAvg(m.ByProdCredit, m.DivisorValue)
, PlantFeed				= [$(DbGlobal)].dbo.WtAvg(m.PlantFeed, m.DivisorValue)
, NetFeedStockCost		= [$(DbGlobal)].dbo.WtAvg(m.PlantFeed, m.DivisorValue)
, GrossMargin			= [$(DbGlobal)].dbo.WtAvg(m.GrossMargin, m.DivisorValue)
, STVol					= [$(DbGlobal)].dbo.WtAvg(m.STVol, m.DivisorValue)
, STNonVol				= [$(DbGlobal)].dbo.WtAvg(m.STNonVol, m.DivisorValue)
, STTaExp				= [$(DbGlobal)].dbo.WtAvg(m.STTaExp, m.DivisorValue)
, TotCashOPEX			= [$(DbGlobal)].dbo.WtAvg(m.TotCashOPEX, m.DivisorValue)
, NetCashMargin			= [$(DbGlobal)].dbo.WtAvg(m.NetCashMargin, m.DivisorValue)
, NonCash				= [$(DbGlobal)].dbo.WtAvg(m.NonCash, m.DivisorValue)
, NetMargin				= [$(DbGlobal)].dbo.WtAvg(m.NetMargin, m.DivisorValue)
, ProductionCost		= [$(DbGlobal)].dbo.WtAvg(m.ProductionCost, m.DivisorValue)
FROM reports.GroupPlants r JOIN dbo.MarginCalc m on m.Refnum=r.Refnum
WHERE DataType in ('ADJ','ETHDiv_MT','HVC_LB','HVC_MT','UEDC', 'EDC')
GROUP by r.GroupId, m.MarginAnalysis, CASE m.DataType
							WHEN 'ETHDiv_MT' THEN 'USDPerEthMt'
							WHEN 'HVC_LB' THEN 'USCentPerHVCLb'
							WHEN 'HVC_MT' THEN 'USDPerHVCMt'
							ELSE m.DataType
							END
