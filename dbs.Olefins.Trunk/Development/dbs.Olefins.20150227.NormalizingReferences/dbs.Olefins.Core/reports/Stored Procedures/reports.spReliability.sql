﻿



CREATE              PROC [reports].[spReliability](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DECLARE @TotLostOpp real
SELECT @TotLostOpp = SUM(LostProdOpp) 
FROM @MyGroup m JOIN dbo.ReliabilityCalc r on r.Refnum=m.Refnum and r.DataType = 'TotLossMT'

DELETE FROM reports.Reliability WHERE GroupId = @GroupId
INSERT into reports.Reliability (GroupId
, DataType
, TurnAround_Pcnt
, OperError_Pcnt
, IntFeedInterrupt_Pcnt
, FurnaceProcess_Pcnt
, FFP_Pcnt
, Transition_Pcnt
, AcetyleneConv_Pcnt
, ProcessOther_Pcnt
, Compressor_Pcnt
, OtherRotating_Pcnt
, FurnaceMech_Pcnt
, Corrosion_Pcnt
, ElecDist_Pcnt
, ControlSys_Pcnt
, OtherFailure_Pcnt
, ExtElecFailure_Pcnt
, ExtOthUtilFailure_Pcnt
, IntElecFailure_Pcnt
, IntSteamFailure_Pcnt
, IntOthUtilFailure_Pcnt
, UnPlanned_Pcnt
, PlantRelated_Pcnt
, FeedMix_Pcnt
, Severity_Pcnt
, Demand_Pcnt
, ExtFeedInterrupt_Pcnt
, CapProject_Pcnt
, Strikes_Pcnt
, OtherNonOp_Pcnt
, OtherTot_Pcnt
, LostProdOpp_Pcnt
, PcntOfTot
)

SELECT GroupId = @GroupId
, DataType					= CASE r.DataType	WHEN 'DTLossMT' THEN 'DTLoss' 
												WHEN 'SDLossMT' THEN 'SDLoss' 
												ELSE 'TotLoss' END
, TurnAround_Pcnt			= SUM(TurnAround)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, OperError_Pcnt			= SUM(OperError)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, IntFeedInterrupt_Pcnt		= SUM(IntFeedInterrupt)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, FurnaceProcess_Pcnt		= SUM(FurnaceProcess)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, FFP_Pcnt= SUM(FFP)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, Transition_Pcnt= SUM(Transition)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, AcetyleneConv_Pcnt= SUM(AcetyleneConv)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, ProcessOther_Pcnt= SUM(ProcessOther)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, Compressor_Pcnt= SUM(Compressor)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, OtherRotating_Pcnt= SUM(OtherRotating)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, FurnaceMech_Pcnt= SUM(FurnaceMech)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, Corrosion_Pcnt= SUM(Corrosion)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, ElecDist_Pcnt= SUM(ElecDist)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, ControlSys_Pcnt= SUM(ControlSys)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, OtherFailure_Pcnt= SUM(OtherFailure)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, ExtElecFailure_Pcnt= SUM(ExtElecFailure)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, ExtOthUtilFailure_Pcnt= SUM(ExtOthUtilFailure)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, IntElecFailure_Pcnt= SUM(IntElecFailure)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, IntSteamFailure_Pcnt= SUM(IntSteamFailure)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, IntOthUtilFailure_Pcnt= SUM(IntOthUtilFailure)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, UnPlanned_Pcnt= SUM(UnPlanned)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, PlantRelated_Pcnt= SUM(PlantRelated)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, FeedMix_Pcnt= SUM(FeedMix)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, Severity_Pcnt= SUM(Severity)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, Demand_Pcnt= SUM(Demand)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, ExtFeedInterrupt_Pcnt= SUM(ExtFeedInterrupt)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, CapProject_Pcnt= SUM(CapProject)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, Strikes_Pcnt= SUM(Strikes)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, OtherNonOp_Pcnt= SUM(OtherNonOp)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, OtherTot_Pcnt= SUM(OtherTot)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, LostProdOpp_Pcnt= SUM(LostProdOpp)/SUM(r.EthyleneCpbyAvg_kMT) * 100 / 1000
, PcntOfTot = SUM(LostProdOpp) / @TotLostOpp * 100
FROM @MyGroup m JOIN dbo.ReliabilityCalc r on r.Refnum=m.Refnum
AND r.DataType in ('DTLossMT','SDLossMT','TotLossMT')
and r.EthyleneCpbyAvg_kMT > 0 AND @TotLostOpp > 0
GROUP BY r.DataType



--IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileRankVariable = 'EII')
--	EXEC spReportTileMinMax @ReportRefnum 

/*
Select * from reports.GroupPlants
insert into ReportS.GroupPlants SELECT '2011PCH148','2011PCH148'
insert into ReportS.GroupPlants SELECT '2011PCH50B','2011PCH50B'
exec reports.spReliability '11PCH'

exec reports.spReliability '2011PCH148'
exec reports.spReliability '2011PCH003'
exec reports.spReliability '2011PCH028'
exec reports.spReliability '2011PCH115'
exec reports.spReliability '2011PCH50B'
select * from ReportS.Reliability
where GroupId = '11PCH'
insert into reports.GroupPlants select Refnum, Refnum from cons.reflist where ListId = '11PCH' and Refnum not in (Select GroupId from reports.groupplants)
select * from cons.reflistlu where listname = '11pch'
select * from cons.reflist where ListId= '11pch'
*/
SET NOCOUNT OFF


















