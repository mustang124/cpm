﻿


CREATE              PROC [reports].[spOPEX](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint, @Currency as varchar(5)
SELECT @Currency = 'USD'
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.OpEx WHERE GroupId = @GroupId
Insert into reports.OpEx (GroupId
, DataType
, DivisorValue
, Currency
, OccSal
, MpsSal
, SalariesWages
, OccBen
, MpsBen
, EmployeeBenefits
, MaintRetube
, MaintMatl
, MaintContractLabor
, MaintContractMatl
, MaintEquip
, ContractTechSvc
, Envir
, ContractServOth
, NonMaintEquipRent
, Tax
, Insur
, OtherNonVol
, STNonVol
, TAAccrual
, Chemicals
, Catalysts
, Royalties
, PurElec
, PurSteam
, PurOth
, PurFuelGas
, PurLiquid
, PPFCFuelGas
, PPFCOther
, ExportSteam
, ExportElectric
, OtherVol
, STVol
, TotCashOpEx
, InvenCarry
, GANonPers
, Depreciation
, Interest
, NonCash
, TotRefExp
, EnergyOpex
, NEOpex
)

SELECT GroupId = @GroupId
, DataType					= o.DataType
, DivisorValue				= [$(DbGlobal)].dbo.WtAvg(o.DivisorValue,1)
, Curency					= o.Currency
, OCCSal					= [$(DbGlobal)].dbo.WtAvg(OCCSal, o.DivisorValue)
, MpsSal					= [$(DbGlobal)].dbo.WtAvg(MpsSal, o.DivisorValue)
, SalariesWages				= [$(DbGlobal)].dbo.WtAvg(SalariesWages, o.DivisorValue)
, OCCBen					= [$(DbGlobal)].dbo.WtAvg(OCCBen, o.DivisorValue)
, MpsBen					= [$(DbGlobal)].dbo.WtAvg(MpsBen, o.DivisorValue)
, EmployeeBenefits			= [$(DbGlobal)].dbo.WtAvg(EmployeeBenefits, o.DivisorValue)
, MaintRetube				= [$(DbGlobal)].dbo.WtAvg(MaintRetube, o.DivisorValue)
, MaintMatl					= [$(DbGlobal)].dbo.WtAvg(MaintMatl, o.DivisorValue)
, MaintContractLabor		= [$(DbGlobal)].dbo.WtAvg(MaintContractLabor, o.DivisorValue)
, MaintContractMatl			= [$(DbGlobal)].dbo.WtAvg(MaintContractMatl, o.DivisorValue)
, MaintEquip				= [$(DbGlobal)].dbo.WtAvg(MaintEquip, o.DivisorValue)
, ContractTechSvc			= [$(DbGlobal)].dbo.WtAvg(ContractTechSvc, o.DivisorValue)
, Envir						= [$(DbGlobal)].dbo.WtAvg(Envir, o.DivisorValue)
, ContractServOth			= [$(DbGlobal)].dbo.WtAvg(ContractServOth, o.DivisorValue)
, NonMaintEquipRent			= [$(DbGlobal)].dbo.WtAvg(NonMaintEquipRent, o.DivisorValue)
, Tax						= [$(DbGlobal)].dbo.WtAvg(Tax, o.DivisorValue)
, Insur						= [$(DbGlobal)].dbo.WtAvg(Insur, o.DivisorValue)
, OtherNonVol				= [$(DbGlobal)].dbo.WtAvg(OtherNonVol, o.DivisorValue)
, STNonVol					= [$(DbGlobal)].dbo.WtAvg(STNonVol, o.DivisorValue)
, TAAccrual					= [$(DbGlobal)].dbo.WtAvg(TAAccrual, o.DivisorValue)              
, Chemicals					= [$(DbGlobal)].dbo.WtAvg(Chemicals, o.DivisorValue)
, Catalysts					= [$(DbGlobal)].dbo.WtAvg(Catalysts, o.DivisorValue)
, Royalties					= [$(DbGlobal)].dbo.WtAvg(Royalties, o.DivisorValue)
, PurElec					= [$(DbGlobal)].dbo.WtAvg(PurElec, o.DivisorValue)
, PurSteam					= [$(DbGlobal)].dbo.WtAvg(PurSteam, o.DivisorValue)
, PurOth					= [$(DbGlobal)].dbo.WtAvg(PurOth, o.DivisorValue)
, PurFuelGas				= [$(DbGlobal)].dbo.WtAvg(PurFuelGas, o.DivisorValue)
, PurLiquid					= [$(DbGlobal)].dbo.WtAvg(PurLiquid, o.DivisorValue)
, PPFCFuelGas				= [$(DbGlobal)].dbo.WtAvg(PPFCFuelGas, o.DivisorValue)
, PPFCOther					= [$(DbGlobal)].dbo.WtAvg(PPFCOther, o.DivisorValue)
, ExportSteam				= [$(DbGlobal)].dbo.WtAvg(ExportSteam, o.DivisorValue)
, ExportElectric			= [$(DbGlobal)].dbo.WtAvg(ExportElectric, o.DivisorValue)
, OtherVol					= [$(DbGlobal)].dbo.WtAvg(OtherVol, o.DivisorValue)
, STVol						= [$(DbGlobal)].dbo.WtAvg(STVol, o.DivisorValue)
, TotCashOpEx				= [$(DbGlobal)].dbo.WtAvg(TotCashOpEx, o.DivisorValue)
, InvenCarry				= [$(DbGlobal)].dbo.WtAvg(InvenCarry, o.DivisorValue)
, GANonPers					= [$(DbGlobal)].dbo.WtAvg(GANonPers, o.DivisorValue)
, Depreciation				= [$(DbGlobal)].dbo.WtAvg(Depreciation, o.DivisorValue)
, Interest					= [$(DbGlobal)].dbo.WtAvg(Interest, o.DivisorValue)
, NonCash					= [$(DbGlobal)].dbo.WtAvg(NonCash, o.DivisorValue)
, TotRefExp					= [$(DbGlobal)].dbo.WtAvg(TotRefExp, o.DivisorValue)
, EnergyOpex				= [$(DbGlobal)].dbo.WtAvg(EnergyOpex, o.DivisorValue)
, NEOpex					= [$(DbGlobal)].dbo.WtAvg(NEOpex, o.DivisorValue)
FROM @MyGroup r JOIN dbo.OpEx o on o.Refnum=r.Refnum 
GROUP BY o.DataType, o.Currency


SET NOCOUNT OFF

















