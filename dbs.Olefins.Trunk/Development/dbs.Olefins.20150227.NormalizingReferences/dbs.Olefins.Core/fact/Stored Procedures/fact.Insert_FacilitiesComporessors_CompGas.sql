﻿CREATE PROCEDURE [fact].[Insert_FacilitiesComporessors_CompGas]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesCompressors(Refnum, CalDateKey, FacilityId
			, Age_Years, Power_BHP, CoatingID, DriverID, Stages_Count)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'CompGas'
			, f.GasCompAge
			, f.GasCompBHP
			, etl.ConvFacilityCoatingID(f.GasCompCoat)
			, etl.ConvFacilityDriverID(f.GasCompDriver)
			, f.GasCompStageCnt
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;