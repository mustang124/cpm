﻿CREATE PROCEDURE [fact].[Insert_Plant_Reliability]
(
	@Refnum		VARCHAR(18)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss';

	DECLARE @r TABLE(
		[Refnum]            VARCHAR (18) NOT NULL,
		[CalDateKey]        INT          NOT NULL,
		[OppLossId]         VARCHAR (42) NOT NULL,
		[DownTimeLoss_MT]   REAL         NULL,
		[SlowDownLoss_MT]   REAL         NULL,
		PRIMARY KEY CLUSTERED ([Refnum] DESC, [OppLossId] ASC, [CalDateKey] DESC)
	);
  
	INSERT INTO @r(Refnum, CalDateKey, OppLossId, DownTimeLoss_MT, SlowDownLoss_MT)
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
		etl.ConvDateKey(t.StudyYear)								[CalDateKey],
		'Fouling'													[OppLossId],	
		a.DTLoss - SUM(l.DTLoss),
		a.SDLoss - SUM(l.SDLoss)
	FROM stgFact.ProdLoss								l
	INNER JOIN stgFact.TSort							t
		ON	t.Refnum = l.Refnum
	LEFT OUTER JOIN stgFact.ProdLoss					a
		ON	a.Refnum = l.Refnum
		AND	a.CauseID = 'Fouling'
	WHERE	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'FFPCauses')
	GROUP BY
		t.Refnum,
		t.StudyYear,
		a.DTLoss,
		a.SDLoss
	HAVING ((a.DTLoss - SUM(l.DTLoss) <> 0.0) OR (a.SDLoss - SUM(l.SDLoss) <> 0.0));

	INSERT INTO @r(Refnum, CalDateKey, OppLossId, DownTimeLoss_MT, SlowDownLoss_MT)
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum],
		etl.ConvDateKey(t.StudyYear)							[CalDateKey],
		etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId],
		ABS(p.DTLoss)											[DownTimeLossMT],
		ABS(p.SDLoss)											[SlowDownLossMT]
	FROM stgFact.ProdLoss p
	INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	WHERE	p.CauseID <> 'Fouling'
		AND	(p.DTLoss <> 0.0 OR p.SDLoss <> 0.0)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	INSERT INTO @r(Refnum, CalDateKey, OppLossId, DownTimeLoss_MT, SlowDownLoss_MT)
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
		etl.ConvDateKey(t.StudyYear - 1)							[CalDateKey],
		'Fouling'													[OppLossId],	
		a.PrevDTLoss - SUM(l.PrevDTLoss),
		a.PrevSDLoss - SUM(l.PrevSDLoss)
	FROM stgFact.ProdLoss								l
	INNER JOIN stgFact.TSort								t
		ON	t.Refnum = l.Refnum
	LEFT OUTER JOIN stgFact.ProdLoss						a
		ON	a.Refnum = l.Refnum
		AND	a.CauseID = 'Fouling'
	WHERE	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'FFPCauses')
	GROUP BY
		t.Refnum,
		t.StudyYear,
		a.PrevDTLoss,
		a.PrevSDLoss
	HAVING ((a.PrevDTLoss - SUM(l.PrevDTLoss) <> 0.0) OR (a.PrevSDLoss - SUM(l.PrevSDLoss) <> 0.0));

	INSERT INTO @r(Refnum, CalDateKey, OppLossId, DownTimeLoss_MT, SlowDownLoss_MT)
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum],
		etl.ConvDateKey(t.StudyYear - 1)						[CalDateKey],
		etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId],
		ABS(p.PrevDTLoss)										[DownTimeLossMT],
		ABS(p.PrevSDLoss)										[SlowDownLossMT]
	FROM stgFact.ProdLoss p
	INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	WHERE	p.CauseID <> 'Fouling'
		AND	(p.PrevDTLoss <> 0.0 OR p.PrevSDLoss <> 0.0)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
	SELECT Refnum, CalDateKey, OppLossId, 'Ethylene', DownTimeLoss_MT, SlowDownLoss_MT
	FROM @r;

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLossNameOther';

	DECLARE @rn TABLE(
		[Refnum]      VARCHAR (18)   NOT NULL,
		[CalDateKey]  INT            NOT NULL,
		[OppLossId]   VARCHAR (42)   NOT NULL,
		[StreamId]    VARCHAR (42)   NOT NULL,
		[OppLossName] NVARCHAR (256) NOT NULL,
		PRIMARY KEY CLUSTERED ([Refnum] DESC, [OppLossId] ASC, [StreamId] ASC, [CalDateKey] DESC)
	);

	INSERT INTO @rn(Refnum, CalDateKey, OppLossId, StreamId, OppLossName)
	SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
		, etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId]
		, 'Ethylene'												[StreamId]
		, CASE 
			WHEN p.[Description] = '0' THEN etl.ConvReliabilityOppLossId(p.CauseID)
			WHEN LEN(p.[Description]) = 0 THEN etl.ConvReliabilityOppLossId(p.CauseID)
			ELSE p.[Description]
			END														[OppLossName]
	FROM stgFact.ProdLoss p
	INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	WHERE p.[Description] IS NOT NULL AND (p.DTLoss <> 0.0 OR p.SDLoss <> 0.0)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	INSERT INTO @rn(Refnum, CalDateKey, OppLossId, StreamId, OppLossName)
	SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear - 1)							[CalDateKey]
		, etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId]
		, 'Ethylene'												[StreamId]
		, CASE 
			WHEN p.[Description] = '0' THEN etl.ConvReliabilityOppLossId(p.CauseID)
			WHEN LEN(p.[Description]) = 0 THEN etl.ConvReliabilityOppLossId(p.CauseID)
			ELSE p.[Description]
			END														[OppLossName]
	FROM stgFact.ProdLoss p
	INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	WHERE p.[Description] IS NOT NULL AND (p.PrevDTLoss <> 0.0 OR p.PrevSDLoss <> 0.0)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	INSERT INTO fact.ReliabilityOppLossNameOther(Refnum, CalDateKey, OppLossId, StreamId, OppLossName)
	SELECT Refnum, CalDateKey, OppLossId, StreamId, OppLossName
	FROM @rn;

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityCritPath';

	INSERT INTO fact.ReliabilityCritPath(Refnum, CalDateKey, FacilityId, CritPath_Bit)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvFacilityId(u.FacilityId)							[FacilityId]
		, etl.ConvBit(u.CritPath)									[CritPath_Bit]
	FROM (
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, p.CGC
			, p.RC
			, p.TLE
			, p.OHX
			, p.Tower
			, p.Furnace
			, p.Util
			, p.CapProj
			, p.Other		[CritPathOther]
		FROM stgFact.PracTA p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT (
		CritPath FOR FacilityId IN (
			  p.CGC
			, p.RC
			, p.TLE
			, p.OHX
			, p.Tower
			, p.Furnace
			, p.Util
			, p.CapProj
			, p.CritPathOther
			)
		) u;

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityCritPathNameOther';

	INSERT INTO fact.ReliabilityCritPathNameOther(Refnum, CalDateKey, FacilityId, FacilityName)
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, 'CritPathOther'										[FacilityId]
		, p.OthCPMDesc											[FacilityName]
	FROM stgFact.PracTA p
	INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	WHERE p.OthCPMDesc IS NOT NULL AND p.Other IS NOT NULL
	AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.Reliability';

	INSERT INTO fact.Reliability(Refnum, CalDateKey
			, TimingFactorID, TaPrep_Hrs, TaStartUp_Hrs
			, MiniTurnAround_Bit
			, FurnRetubeWorkID, FurnTubeLife_Mnths, FurnRetube_Days)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')					[Refnum]
		, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
		, CASE p.TimeFact WHEN '?' THEN NULL ELSE p.TimeFact END		[TimingFactorID]
		, p.TaPrepTime
		, p.TaStartUpTime
		, etl.ConvBit(a.TAMini)											[MiniTurnAround]
		, etl.ConvFurnRetubeWork(m.FurnRetubeWho)						[FurnRetubeWorkID]
		, m.FurnTubeLife
		, m.FurnRetubeTime
	FROM stgFact.PracTA p
	LEFT OUTER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	LEFT OUTER JOIN stgFact.MaintMisc m ON m.Refnum = p.Refnum
	LEFT OUTER JOIN stgFact.TADT a ON a.Refnum = p.Refnum AND a.TrainId = 1
	WHERE	(p.TimeFact IS NOT NULL
		OR	p.TaPrepTime IS NOT NULL
		OR	p.TaStartUpTime IS NOT NULL
		OR	a.TAMini IS NOT NULL
		OR	m.FurnRetubeWho IS NOT NULL
		OR	m.FurnTubeLife IS NOT NULL
		OR	m.FurnRetubeTime IS NOT NULL)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityTA';
	
	INSERT INTO fact.ReliabilityTA(Refnum, CalDateKey, TrainId, SchedId
		, Interval_Mnths, DownTime_Hrs, CurrencyRpt, TurnAroundCost_MCur, TurnAround_ManHrs, TurnAround_Date
		, StreamId, Capacity_Pcnt
	)
	SELECT
		  u1.Refnum
		, u1.CalDateKey
		, u1.TrainId
		, LEFT(u1.SchedId, 1)	[SchedId]
		, u1.Interval_Mnths
		, u1.DownTime_Hrs
		, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN 'USD'				END
		, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN u1.TACost			END
		, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN u1.TAManHrs		END
		, CASE WHEN LEFT(u1.SchedId, 1) = 'A'		THEN u1.TaDate			END
		, CASE WHEN u1.Interval_Mnths IS NOT NULL	THEN 'Ethylene'			END
		, CASE WHEN u1.Interval_Mnths IS NOT NULL	THEN u1.PctTotEthylCap	END
	FROM (
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')					[Refnum]
			, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
			, a.TrainId
			, a.PlanInterval
			, a.ActInterval
			, a.PlanDT
			, a.ActDT
			, a.TACost
			, a.TAManHrs
			, CONVERT(DATE, CASE WHEN YEAR(a.TaDate) > 1900 THEN a.TaDate END)	[TaDate]
			, a.PctTotEthylCap
		FROM stgFact.TADT a
		INNER JOIN stgFact.TSort t ON t.Refnum = a.Refnum
		WHERE	(a.PlanInterval		> 0.0
			OR	a.PlanDT			> 0.0
			OR	a.ActInterval		> 0.0
			OR	a.ActDT				> 0.0
			OR	a.TACost			> 0.0
			OR	a.TAManHrs			> 0.0
			OR	a.PctTotEthylCap	> 0.0
			)
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT (
		Interval_Mnths FOR SchedId IN (
			  PlanInterval
			, ActInterval
			)
		) u0
		UNPIVOT (
		DownTime_Hrs FOR SchedIDd IN (
			  PlanDT
			, ActDT
			)
		) u1
	WHERE
		LEFT(u1.SchedId, 1) = LEFT(u1.SchedIDd, 1);

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityPyroFurnFeed';

	INSERT INTO fact.ReliabilityPyroFurnFeed(Refnum, CalDateKey, StreamId
			, RunLength_Days, Inj, PreTreat_Bit, AntiFoul_Bit)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')					[Refnum]
		, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
		, etl.ConvStreamID(
			CASE p.FeedProdID
			WHEN 'GasOil' THEN 'LiqHeavy'
			WHEN 'FRNaphtha' THEN 'Naphtha'
			ELSE p.FeedProdID END
			)
																		[StreamId]
		, p.Run
		, UPPER(p.Inj)													[Inj]
		, etl.ConvBit(p.Pretreat)										[PreTreat]
		, etl.ConvBit(p.AntiFoul)										[AntiFoul]
	FROM stgFact.PracFurnInfo p
	INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
	WHERE p.Run IS NOT NULL
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;
		
	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityPyroFurn';

	INSERT INTO fact.ReliabilityPyroFurn(Refnum, CalDateKey, FurnID, StreamId
		, FeedQty_kMT, FeedCap_MTd
		, FuelTypeID, FuelCons_kMT
		, StackOxygen_Pcnt, ArchDraft_H20, StackTemp_C, CrossTemp_C, Retubed_Year, RetubeInterval_Mnths)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
		, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
		, f.FurnNo
		, l.StreamId
		, f.FeedQtyKMTA
		, f.FeedCapMTD
		
		, etl.ConvStreamID(f.FuelType)
		, f.FuelCons
		
		, f.StackOxy
		, f.ArchDraft
		, f.StackTemp
		, f.CrossTemp
		
		, CASE WHEN LEN(f.YearRetubed) = 4 THEN f.YearRetubed END
		, f.RetubeInterval
	FROM stgFact.FurnRely f
	INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
	INNER JOIN dim.Stream_LookUp l ON l.StreamId  = etl.ConvStreamID(f.FurnFeed)
	WHERE ISNUMERIC(f.FurnFeed) = 0 AND f.FurnFeed <> '1+4'
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityPyroFurnDownTime';

	INSERT INTO fact.ReliabilityPyroFurnDownTime(Refnum, CalDateKey, FurnID, OppLossId, DownTime_Days)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, u.FurnID
		, etl.ConvDownTime(u.OppLossId)
		, u.DownTimeDays
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, f.FurnNo													[FurnID]
			, f.DTDecoke
			, f.DTTLEClean
			, f.DTMinor
			, f.DTMajor
			, f.DTStandby
			, CONVERT(REAL, 365)										[PeriodDays]
			, f.TADownDays
			, f.OthDownDays
		FROM  stgFact.FurnRely f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT(
		DownTimeDays FOR OppLossId IN(
			  DTDecoke
			, DTTLEClean
			, DTMinor
			, DTMajor
			, DTStandby
			, PeriodDays
			, TADownDays
			, OthDownDays
			)
		) u;

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityPyroFurnOpEx';

	INSERT INTO fact.ReliabilityPyroFurnOpEx(Refnum, CalDateKey, FurnID, AccountId, CurrencyRpt, Amount_Cur)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, u.FurnID
		, etl.ConvAccountID(u.AccountId)
		, 'USD'
		, u.Amount_RPT
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, f.FurnNo													[FurnID]
			, RetubeCost
			, RetubeCostMatl
			, RetubeCostLabor
			, OtherMajorCost
		FROM  stgFact.FurnRely f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT(
		Amount_RPT FOR AccountId IN(
			  RetubeCost
			, RetubeCostMatl
			, RetubeCostLabor
			, OtherMajorCost
			)
		) u;

	PRINT NCHAR(9) + 'INSERT INTO fact.ReliabilityPyroFurnPlantLimit';

	INSERT INTO fact.ReliabilityPyroFurnPlantLimit(Refnum, CalDateKey, OppLossId, PlantFurnLimit_Pcnt)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
		, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
		, 'FurnaceMech'												[OppLossId]
		, m.PctFurnRelyLimit * 100.0								[PlantFurnLimit_Pcnt]
		
	FROM stgFact.Misc m
	INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
	WHERE m.PctFurnRelyLimit IS NOT NULL
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

END;