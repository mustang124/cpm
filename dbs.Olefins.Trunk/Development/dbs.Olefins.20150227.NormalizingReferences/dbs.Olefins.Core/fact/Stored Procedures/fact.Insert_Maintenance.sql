﻿CREATE PROCEDURE [fact].[Insert_Maintenance]
(
	@Refnum		VARCHAR(18)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	PRINT NCHAR(9) + 'INSERT INTO fact.MaintMisc';

	INSERT INTO fact.[MaintAccuracy](Refnum, CalDateKey, AccuracyID)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, CASE WHEN m.Accuracy <> '0' THEN m.Accuracy END
	FROM stgFact.MaintMisc m
	INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
	WHERE m.Accuracy IS NOT NULL AND m.Accuracy <> '0'
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.MaintExpense';

	INSERT INTO fact.MaintExpense(Refnum, CalDateKey, AccountId, CompMaintHours_Pcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, etl.ConvAccountID(u.AccountId) [AccoutnID]
		, u.Pcnt * 100.0
	FROM (
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, m.CorrEmergMaint
			, m.CorrOthMaint
			, m.PrevCondAct
			, m.PrevAddMaint
			, m.PrevRoutine
			, m.PrefAddRoutine
		FROM stgFact.MaintMisc m
		INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) p
		UNPIVOT(
		Pcnt FOR AccountId IN(
			  p.CorrEmergMaint
			, p.CorrOthMaint
			, p.PrevCondAct
			, p.PrevAddMaint
			, p.PrevRoutine
			, p.PrefAddRoutine
			)
		)u;

	PRINT NCHAR(9) + 'INSERT INTO fact.Maint (1995 - 1999 (ElecGen/ElecDist))';

	INSERT INTO fact.Maint(Refnum, CalDateKey, FacilityId, CurrencyRpt, MaintMaterial_Cur, MaintLabor_Cur, TaMaterial_Cur, TaLabor_Cur)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
		, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
		, etl.ConvFacilityId(m.ProjectID)
		, 'USD'
		, m.RoutMaintMatl
		, m.RoutMaintLabor
		, m.TAMatl
		, m.TALabor
	FROM stgFact.Maint m
	INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
	WHERE	(m.RoutMaintMatl IS NOT NULL
		OR	m.RoutMaintLabor IS NOT NULL
		OR	m.TAMatl IS NOT NULL
		OR	m.TALabor IS NOT NULL)
		AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.Maint (2001 - 2009 (ElectGenDist))';

	INSERT INTO fact.Maint(Refnum, CalDateKey, FacilityId, CurrencyRpt, MaintMaterial_Cur, MaintLabor_Cur, TaMaterial_Cur, TaLabor_Cur)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
		, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
		, etl.ConvFacilityId(CASE m.ProjectID WHEN 'ElecGen' THEN 'ElectGenDist' ELSE m.ProjectID END)
		, 'USD'
		, m.RoutMaintMatl
		, m.RoutMaintLabor
		, m.TAMatl
		, m.TALabor
	FROM stgFact.Maint01 m
	INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
	WHERE	(m.RoutMaintMatl IS NOT NULL
		OR	m.RoutMaintLabor IS NOT NULL
		OR	m.TAMatl IS NOT NULL
		OR	m.TALabor IS NOT NULL)
		AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.Maint (Allocated Overhead Cost)';

	INSERT INTO fact.Maint(Refnum, CalDateKey, FacilityId, CurrencyRpt, MaintMaterial_Cur, MaintLabor_Cur, TaMaterial_Cur, TaLabor_Cur)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
		, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
		, 'OverHead'
		, 'USD'
		, m.AllocOHMaintMatl
		, m.AllocOHMaintLabor
		, m.AllocOHTAMatl
		, m.AllocOHTALabor
	FROM stgFact.TotMaintCost m
	INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
	WHERE	(m.AllocOHMaintMatl IS NOT NULL
		OR	m.AllocOHMaintLabor IS NOT NULL
		OR	m.AllocOHTAMatl IS NOT NULL
		OR	m.AllocOHTALabor IS NOT NULL)
		AND	etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

END;