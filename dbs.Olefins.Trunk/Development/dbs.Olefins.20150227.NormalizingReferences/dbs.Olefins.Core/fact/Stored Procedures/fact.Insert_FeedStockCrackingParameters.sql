﻿CREATE PROCEDURE [fact].[Insert_FeedStockCrackingParameters]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

	INSERT INTO fact.FeedStockCrackingParameters(Refnum, CalDateKey, StreamId, StreamDescription
		, CoilOutletPressure_Psia
		, CoilOutletTemp_C
		, SteamHydrocarbon_Ratio
		, EthyleneYield_WtPcnt
		, PropyleneEthylene_Ratio
		, PropyleneMethane_Ratio
		, FeedConv_WtPcnt
		, Density_SG
		, [SulfurContent_ppm]
		, DistMethodID
		, FurnConstruction_Year
		, ResidenceTime_s
		, Recycle_Bit)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		
		, etl.ConvStreamID(q.FeedProdID)						[StreamId]
		, etl.ConvStreamDescription(q.FeedProdID, q.OthLiqFeedDESC, NULL, NULL, NULL)
																[StreamDescription]
		, q.CoilOutletPressure
		, q.CoilOutletTemp
		, q.SteamHCRatio
		, q.EthyleneYield * CASE WHEN q.EthyleneYield < 1.0 THEN 100.0 ELSE 1.0 END		[EthyleneYield]
		, q.PropylEthylRatio
		, q.PropylMethaneRatio
		, q.FeedConv			* 100.0							[FeedConv]
		, q.SG
		, q.SulfurPPM
		, etl.ConvDistillationMethod(q.ASTMethod)				[DistMethodID]
		, q.ConstrYear
		, q.ResTime
		, CASE WHEN etl.ConvStreamID(q.FeedProdID) IN ('EthRec', 'ProRec', 'ButRec') THEN 1 ELSE 0 END	[Recycle_Bit]
		
	FROM stgFact.FeedQuality q
	INNER JOIN stgFact.TSort t
		ON t.Refnum = q.Refnum
	INNER JOIN stgFact.Quantity z
		ON	z.Refnum = q.Refnum
		AND	z.FeedProdID = q.FeedProdID
		AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
	WHERE t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;