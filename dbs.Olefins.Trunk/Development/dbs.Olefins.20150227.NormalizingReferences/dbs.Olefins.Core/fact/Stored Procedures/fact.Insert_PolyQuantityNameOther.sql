﻿CREATE PROCEDURE [fact].[Insert_PolyQuantityNameOther]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.PolyQuantityNameOther(Refnum, CalDateKey, AssetId, StreamId, StreamName)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.AssetId
			, etl.ConvStreamIDPoly(u.StreamId)	[StreamId]
			, CASE 
				WHEN LEN(u.StreamDetail) = 0 THEN etl.ConvStreamIDPoly(u.StreamId)
				WHEN u.StreamDetail = '0' THEN etl.ConvStreamIDPoly(u.StreamId)
				ELSE u.StreamDetail
				END
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum]
				, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
				, etl.ConvAssetPCH(p.Refnum)
					+ '-T' + CONVERT(VARCHAR(2), p.TrainId)					[AssetId]
				, p.OthMono1Desc											[OthMono1]
				, CASE WHEN p.OthMono2Desc <> '' THEN p.OthMono2Desc END	[OthMono2]
				, p.OthMono3Desc											[OthMono3]
				, p.ByProdOth1Desc		[ByProdOth1]
				, p.ByProdOth2Desc		[ByProdOth2]
				, p.ByProdOth3Desc		[ByProdOth3]
				, p.ByProdOth4Desc		[ByProdOth4]
				, p.ByProdOth5Desc		[ByProdOth5]
			FROM stgFact.PolyMatlBalance p
			INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
			INNER JOIN stgFact.PolyFacilities f ON f.Refnum = p.Refnum AND f.PlantID = p.TrainId
			WHERE f.Name <> '0'
				AND	t.Refnum = @sRefnum
			) p
			UNPIVOT (
			StreamDetail FOR StreamId IN (
				  p.OthMono1
				, p.OthMono2
				, p.OthMono3
				, p.ByProdOth1
				, p.ByProdOth2
				, p.ByProdOth3
				, p.ByProdOth4
				, p.ByProdOth5
				)
			)u
		INNER JOIN fact.PolyQuantity	pq
			ON	pq.Refnum = u.Refnum
			AND	pq.AssetId = u.AssetId
			AND	pq.StreamId = etl.ConvStreamIDPoly(u.StreamId)
		INNER JOIN cons.Assets a ON a.AssetId = u.AssetId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;