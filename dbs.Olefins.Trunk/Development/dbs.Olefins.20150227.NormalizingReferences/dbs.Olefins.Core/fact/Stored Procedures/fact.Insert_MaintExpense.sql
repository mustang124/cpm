﻿CREATE PROCEDURE [fact].[Insert_MaintExpense]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.MaintExpense(Refnum, CalDateKey, AccountId, CompMaintHours_Pcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvAccountID(u.AccountId) [AccountId]
			, u.Pcnt * 100.0
		FROM (
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, m.CorrEmergMaint
				, m.CorrOthMaint
				, m.PrevCondAct
				, m.PrevAddMaint
				, m.PrevRoutine
				, m.PrefAddRoutine
			FROM stgFact.MaintMisc m
			INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
			WHERE t.Refnum = @sRefnum
			) p
			UNPIVOT(
			Pcnt FOR AccountId IN(
				  p.CorrEmergMaint
				, p.CorrOthMaint
				, p.PrevCondAct
				, p.PrevAddMaint
				, p.PrevRoutine
				, p.PrefAddRoutine
				)
			)u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;