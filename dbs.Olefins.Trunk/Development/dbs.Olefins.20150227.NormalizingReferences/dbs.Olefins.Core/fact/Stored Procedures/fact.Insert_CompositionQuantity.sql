﻿CREATE PROCEDURE [fact].[Insert_CompositionQuantity]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		-- Table 2A-1 Light Feed, 2A-2 Liquid Feed, and 2B Recycle Feed Composition
		-- Includes Hydrogen for Liquids
		INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.FeedProdID)							[StreamId]
			, ISNULL(u.[StreamDescription], u.FeedProdID)					[StreamDescription]
			, etl.ConvComponentID(u.ComponentId)					[ComponentId]
			, u.WtPcnt * 100.0											[WtPcnt]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, q.FeedProdID
				--, q.OthLiqFeedDESC
				, etl.ConvStreamDescription(z.FeedProdID, q.OthLiqFeedDESC, z.MiscProd1, z.MiscProd2, z.MiscFeed) [StreamDescription]

				, q.Methane			[CH4]
				, q.Ethane			[C2H6]
				, q.Ethylene		[C2H4]
				, q.Propane			[C3H8]
				, q.Propylene		[C3H6]
				, q.nButane			[NBUTA]
				, q.iButane			[IBUTA]
				, q.Isobutylene		[IB]
				, q.Butene1			[B1]
				, q.Butadiene		[C4H6]
				, q.nPentane		[NC5]
				, q.iPentane		[IC5]
				, q.nHexane			[NC6]
				, q.iHexane			[C6ISO]
				, q.Septane			[C7H16]
				, q.Octane			[C8H18]
				, q.CO2				[CO2]
				, q.Hydrogen		[H2]
				, q.Sulfur			[S]
			
				, q.NParaffins		[P]
				, q.IsoParaffins	[I]
				, q.Aromatics		[A]
				, q.Naphthenes		[N]
				, q.Olefins			[O]
			
			FROM stgFact.FeedQuality q
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = q.Refnum
			INNER JOIN stgFact.Quantity z
				ON	z.Refnum = q.Refnum
				AND	z.FeedProdID = q.FeedProdID
				AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0

			WHERE	(q.Methane		> 0.0
				OR	q.Ethane		> 0.0
				OR	q.Ethylene		> 0.0
				OR	q.Propane		> 0.0
				OR	q.Propylene		> 0.0
				OR	q.nButane		> 0.0
				OR	q.iButane		> 0.0
				OR	q.Isobutylene	> 0.0
				OR	q.Butene1		> 0.0
				OR	q.Butadiene		> 0.0
				OR	q.nPentane		> 0.0
				OR	q.iPentane		> 0.0
				OR	q.nHexane		> 0.0
				OR	q.iHexane		> 0.0
				OR	q.Septane		> 0.0
				OR	q.Octane		> 0.0
				OR	q.CO2			> 0.0
				OR	q.Hydrogen		> 0.0
				OR	q.Sulfur		> 0.0
			
				OR	q.NParaffins	> 0.0
				OR	q.IsoParaffins	> 0.0
				OR	q.Aromatics		> 0.0
				OR	q.Naphthenes	> 0.0
				OR	q.Olefins		> 0.0)
				AND t.Refnum = @sRefnum
		) p
			UNPIVOT(
			WtPcnt FOR ComponentId IN(
				  [CH4]
				, [C2H6]
				, [C2H4]
				, [C3H8]
				, [C3H6]
				, [NBUTA]
				, [IBUTA]
				, [IB]
				, [B1]
				, [C4H6]
				, [NC5]
				, [IC5]
				, [NC6]
				, [C6ISO]
				, [C7H16]
				, [C8H18]
				, [CO2]
				, [H2]
				, [S]
			
				, [P]
				, [I]
				, [A]
				, [N]
				, [O]
				)
			) u
		WHERE u.WtPcnt > 0.0 AND u.WtPcnt <= 1.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;