﻿CREATE PROCEDURE [fact].[Insert_FeedSelModels]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSelModels(Refnum, CalDateKey, ModelId, Implemented_Bit, InCompany_Bit, LocationID)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)					[Refnum]
			, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
			, CASE WHEN t.StudyYear >= 2005
				THEN
					CASE ISNULL(i.ModelId, ISNULL(l.ModelId, c.ModelId))
					WHEN 'LPPC'		THEN 'LP'
					WHEN 'LPMain'	THEN 'LP'
					ELSE
						ISNULL(i.ModelId, ISNULL(l.ModelId, c.ModelId))
					END
				ELSE
					ISNULL(i.ModelId, ISNULL(l.ModelId, c.ModelId))	
				END															[ModelId]
			, etl.ConvBit(i.Implemented)									[Implemented]
			, etl.ConvBit(c.InCompany)										[InCompany]
			, CASE WHEN UPPER(LEFT(l.LevelPlantCorp, 1)) IN ('B', 'C', 'P')
				THEN l.LevelPlantCorp END									[LevelPlantCorp]
		FROM (
			SELECT
				  Refnum
				, r.Use_LP_PC				[LPPC]
				, r.Use_LP_Main				[LPMain]
				, r.Use_LPMultiPlant		[LPMultiPlant]
				, r.Use_LPMultiPeriod		[LPMultiPeriod]
				, r.Use_TierData			[PricingTrieredFeed]
				, r.Use_TierProduct			[PricingTieredProd]
				, r.Use_NonLP				[NonLP]
				, r.Use_Spread				[SpreadSheet]
				, r.Use_MarginData			[Margin]
				, r.Use_AltFeed				[CaseStudies]
				, r.Use_BEAltFeed			[BreakEven]
				, r.Use_YieldPredict		[YPS]
				, r.Use_ScheduleModel		[Scheduling]
			FROM stgFact.FeedResources r
			WHERE r.Refnum = @sRefnum
			) u
			UNPIVOT (
			Implemented FOR ModelId IN(
				  LPPC
				, LPMain
				, LPMultiPlant
				, LPMultiPeriod
				, PricingTrieredFeed
				, PricingTieredProd
				, NonLP
				, SpreadSheet
				, Margin
				, CaseStudies
				, BreakEven
				, YPS
				, Scheduling
				)
			) i
		FULL OUTER JOIN(
			SELECT
				  l.Refnum
				, l.ModelId
				, l.LevelPlantCorp
			FROM (
				SELECT
					  Refnum
					, r.Lvl_LP_PC				[LPPC]
					, r.Lvl_LP_Main				[LPMain]
					, r.Lvl_LPMultiPlant		[LPMultiPlant]
					, r.Lvl_LPMultiPeriod		[LPMultiPeriod]
					, r.Lvl_TierData			[PricingTrieredFeed]
					, r.Lvl_TierProduct			[PricingTieredProd]
					, r.Lvl_NonLP				[NonLP]
					, r.Lvl_Spread				[SpreadSheet]
					, r.Lvl_MarginData			[Margin]
					, r.Lvl_AltFeed				[CaseStudies]
					, r.Lvl_BEAltFeed			[BreakEven]
					, r.Lvl_YieldPredict		[YPS]
					, r.Lvl_ScheduleModel		[Scheduling]
				FROM stgFact.FeedResources r
				WHERE r.Refnum = @sRefnum
				) u
				UNPIVOT (
				LevelPlantCorp FOR ModelId IN(
					  LPPC
					, LPMain
					, LPMultiPlant
					, LPMultiPeriod
					, PricingTrieredFeed
					, PricingTieredProd
					, NonLP
					, SpreadSheet
					, Margin
					, CaseStudies
					, BreakEven
					, YPS
					, Scheduling
					)
				) l
			) l
		ON l.Refnum = i.Refnum AND l.ModelId = i.ModelId
		FULL OUTER JOIN(
			SELECT
				  c.Refnum
				, c.ModelId
				, c.InCompany
			FROM (
				SELECT
					  Refnum
					, r.Src_LP_PC				[LPPC]
					, r.Src_LP_Main				[LPMain]
					, r.Src_LPMultiPlant		[LPMultiPlant]
					, r.Src_LPMultiPeriod		[LPMultiPeriod]
					, r.Src_TierData			[PricingTrieredFeed]
					, r.Src_TierProduct			[PricingTieredProd]
					, r.Src_NonLP				[NonLP]
					, r.Src_Spread				[SpreadSheet]
					, r.Src_MarginData			[Margin]
					, r.Src_AltFeed				[CaseStudies]
					, r.Src_BEAltFeed			[BreakEven]
					, r.Src_YieldPredict		[YPS]  
				FROM stgFact.FeedResources r
				WHERE r.Refnum = @sRefnum
				) u
				UNPIVOT (
				InCompany FOR ModelId IN(
					  LPPC
					, LPMain
					, LPMultiPlant
					, LPMultiPeriod
					, PricingTrieredFeed
					, PricingTieredProd
					, NonLP
					, SpreadSheet
					, Margin
					, CaseStudies
					, BreakEven
					, YPS
					)
				) c
			) c
		ON c.Refnum = i.Refnum AND c.ModelId = i.ModelId
		INNER JOIN stgFact.TSort t ON t.Refnum = ISNULL(i.Refnum, ISNULL(l.Refnum, c.Refnum))
		WHERE	t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;