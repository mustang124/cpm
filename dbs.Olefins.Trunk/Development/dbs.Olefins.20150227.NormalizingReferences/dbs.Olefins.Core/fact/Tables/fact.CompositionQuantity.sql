﻿CREATE TABLE [fact].[CompositionQuantity] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [ComponentId]       VARCHAR (42)       NOT NULL,
    [Component_WtPcnt]  REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_CompositionQuantity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_CompositionQuantity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_CompositionQuantity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_CompositionQuantity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_CompositionQuantity] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [StreamDescription] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_CompositionQuantity_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_CompositionQuantity_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_CompositionQuantity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_CompositionQuantity_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_CompositionQuantity_Quantity] FOREIGN KEY ([Refnum], [StreamId], [StreamDescription], [CalDateKey]) REFERENCES [fact].[Quantity] ([Refnum], [StreamId], [StreamDescription], [CalDateKey]),
    CONSTRAINT [FK_CompositionQuantity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_CompositionQuantity_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_CompositionQuantity_PlantPricing]
    ON [fact].[CompositionQuantity]([StreamId] ASC)
    INCLUDE([Refnum], [StreamDescription], [ComponentId], [Component_WtPcnt]);


GO

CREATE TRIGGER [fact].[t_CompositionQuantity_u]
	ON [fact].[CompositionQuantity]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[CompositionQuantity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[CompositionQuantity].Refnum					= INSERTED.Refnum
		AND [fact].[CompositionQuantity].StreamId				= INSERTED.StreamId
		AND [fact].[CompositionQuantity].StreamDescription		= INSERTED.StreamDescription
		AND [fact].[CompositionQuantity].ComponentId			= INSERTED.ComponentId
		AND [fact].[CompositionQuantity].CalDateKey				= INSERTED.CalDateKey;

END;