﻿CREATE TABLE [fact].[Reliability] (
    [Refnum]             VARCHAR (25)       NOT NULL,
    [CalDateKey]         INT                NOT NULL,
    [TimingFactorID]     CHAR (1)           NULL,
    [TaPrep_Hrs]         REAL               NULL,
    [TaStartUp_Hrs]      REAL               NULL,
    [MiniTurnAround_Bit] BIT                NULL,
    [FurnRetubeWorkID]   VARCHAR (12)       NULL,
    [FurnTubeLife_Mnths] REAL               NULL,
    [FurnRetube_Days]    REAL               NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_Reliability_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_Reliability_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_Reliability_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_Reliability_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Reliability] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_Reliability_FurnRetube_Days] CHECK ([FurnRetube_Days]>=(0.0)),
    CONSTRAINT [CR_Reliability_FurnTubeLife_Mnths] CHECK ([FurnTubeLife_Mnths]>=(0.0)),
    CONSTRAINT [CR_Reliability_TaPrep_Hrs] CHECK ([TaPrep_Hrs]>=(0.0)),
    CONSTRAINT [CR_Reliability_TaStartUp_Hrs] CHECK ([TaStartUp_Hrs]>=(0.0)),
    CONSTRAINT [FK_Reliability_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Reliability_FurnRetubeWork_LookUp] FOREIGN KEY ([FurnRetubeWorkID]) REFERENCES [dim].[FurnRetubeWork_LookUp] ([WorkId]),
    CONSTRAINT [FK_Reliability_ReliabilityTaTiming_LookUp] FOREIGN KEY ([TimingFactorID]) REFERENCES [dim].[ReliabilityTaTiming_LookUp] ([TimingId]),
    CONSTRAINT [FK_Reliability_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_Reliability_u]
	ON [fact].[Reliability]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Reliability]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[Reliability].Refnum		= INSERTED.Refnum
		AND [fact].[Reliability].CalDateKey	= INSERTED.CalDateKey;

END;