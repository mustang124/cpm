﻿CREATE TABLE [fact].[PolyOpEx] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AssetId]        VARCHAR (30)       NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PolyOpEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PolyOpEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PolyOpEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PolyOpEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PolyOpEx] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [AssetId] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PolyOpEx_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_PolyOpEx_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_PolyOpEx_Assets] FOREIGN KEY ([AssetId]) REFERENCES [cons].[Assets] ([AssetId]),
    CONSTRAINT [FK_PolyOpEx_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PolyOpEx_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PolyOpEx_PolyFacilities] FOREIGN KEY ([Refnum], [AssetId], [CalDateKey]) REFERENCES [fact].[PolyFacilities] ([Refnum], [AssetId], [CalDateKey]),
    CONSTRAINT [FK_PolyOpEx_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_PolyOpEx_u]
	ON [fact].[PolyOpEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[PolyOpEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[PolyOpEx].Refnum		= INSERTED.Refnum
		AND [fact].[PolyOpEx].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[PolyOpEx].AssetId		= INSERTED.AssetId
		AND [fact].[PolyOpEx].AccountId		= INSERTED.AccountId
		AND [fact].[PolyOpEx].CalDateKey	= INSERTED.CalDateKey;

END;