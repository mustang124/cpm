﻿CREATE TABLE [Console].[RefnumStudyCodes] (
    [StudyCode]      VARCHAR (3)        NOT NULL,
    [Study]          VARCHAR (3)        NOT NULL,
    [StudyCodeSize]  AS                 (len(rtrim([StudyCode]))) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_RefnumStudyCodes_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_RefnumStudyCodes_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_RefnumStudyCodes_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_RefnumStudyCodes_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_RefnumStudyCodes] PRIMARY KEY CLUSTERED ([StudyCode] ASC),
    CONSTRAINT [CL_RefnumStudyCodes_Study] CHECK ([Study]<>''),
    CONSTRAINT [CL_RefnumStudyCodes_StudyCode] CHECK ([StudyCode]<>'')
);


GO

CREATE TRIGGER [Console].[t_RefnumStudyCodes_u]
	ON  [Console].[RefnumStudyCodes]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [Console].[RefnumStudyCodes]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[Console].[RefnumStudyCodes].[StudyCode]			= INSERTED.[StudyCode];

END;