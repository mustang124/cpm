﻿CREATE TABLE [stgFact].[EnergyConsumptionPlant] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [SimModelId]       VARCHAR (12)       NOT NULL,
    [OpCondId]         VARCHAR (12)       NOT NULL,
    [SimEnergy_kCalkg] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_EnergyCompositionPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyCompositionPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyCompositionPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_EnergyCompositionPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_EnergyCompositionPlant] PRIMARY KEY CLUSTERED ([Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC),
    CONSTRAINT [FK_stgFact_EnergyConsumptionPlant_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_stgFact_EnergyConsumptionPlant_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId])
);


GO

CREATE TRIGGER [stgFact].[t_EnergyConsumptionPlant_u]
	ON [stgFact].[EnergyConsumptionPlant]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[EnergyConsumptionPlant]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[EnergyConsumptionPlant].Refnum		= INSERTED.Refnum
		AND	[stgFact].[EnergyConsumptionPlant].SimModelId	= INSERTED.SimModelId
		AND	[stgFact].[EnergyConsumptionPlant].OpCondId	= INSERTED.OpCondId;

END;