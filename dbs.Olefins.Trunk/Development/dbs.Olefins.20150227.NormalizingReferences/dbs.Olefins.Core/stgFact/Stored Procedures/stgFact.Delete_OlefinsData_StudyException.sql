﻿CREATE PROCEDURE [stgFact].[Delete_OlefinsData_StudyException]
AS
BEGIN
	
	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Capacity]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[CapGrowth]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[CapitalExp]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Composition]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Duties]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[EnergyQnty]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Environ]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Facilities]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[FeedFlex]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[FeedPlan]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[FeedQuality]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[FeedResources]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[FeedSelections]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[FurnRely]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Inventory]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Maint]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Maint01]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[MaintMisc]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[MetaEnergy]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[MetaFeedProd]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[MetaMisc]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[MetaOpEx]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Misc]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[OpEx]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[OpExMiscDetail]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Pers]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PolyFacilities]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PolyMatlBalance]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PolyOpEx]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Prac]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracAdv]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracControls]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracFurnInfo]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracMPC]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracOnlineFact]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracOrg]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracTA]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[ProdLoss]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[ProdQuality]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Quantity]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[TADT]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[TotMaintCost]	WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[TSort]			WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[OtherAvg]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[PracRely]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );
	DELETE FROM [stgFact].[Compressors]		WHERE NOT (Refnum LIKE '07PCH%' OR Refnum LIKE '09PCH%' OR Refnum LIKE '11PCH%' );

END