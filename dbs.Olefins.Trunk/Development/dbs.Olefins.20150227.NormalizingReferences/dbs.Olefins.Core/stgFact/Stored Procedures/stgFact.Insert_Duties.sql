﻿CREATE PROCEDURE [stgFact].[Insert_Duties]
(
	@Refnum			VARCHAR (25),
	@FeedProdID		VARCHAR (30),

	@ImpTx			REAL		= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Duties]([Refnum], [FeedProdID], [ImpTx])
	VALUES(@Refnum, @FeedProdID, @ImpTx)

END;