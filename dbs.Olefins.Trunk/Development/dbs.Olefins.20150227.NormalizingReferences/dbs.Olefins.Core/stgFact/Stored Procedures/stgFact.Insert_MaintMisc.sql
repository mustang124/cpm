﻿CREATE PROCEDURE [stgFact].[Insert_MaintMisc]
(
    @Refnum           VARCHAR (25),

    @CorrEmergMaint   REAL         = NULL,
    @CorrOthMaint     REAL         = NULL,
    @PrevCondAct      REAL         = NULL,
    @PrevAddMaint     REAL         = NULL,
    @PrevRoutine      REAL         = NULL,
    @PrefAddRoutine   REAL         = NULL,
    @Accuracy         CHAR (1)     = NULL,
    @FurnTubeLife     REAL         = NULL,
    @RespUnplan       REAL         = NULL,
    @RespPlan         REAL         = NULL,
    @Prevent          REAL         = NULL,
    @Predict          REAL         = NULL,
    @Training         REAL         = NULL,
    @FurnRetubeTime   REAL         = NULL,
    @FurnRetubeWho    VARCHAR (25) = NULL,
    @MaintMatlPY      REAL         = NULL,
    @ContMaintLaborPY REAL         = NULL,
    @ContMaintMatlPY  REAL         = NULL,
    @MaintEquipRentPY REAL         = NULL,
    @OCCMaintST_PY    REAL         = NULL,
    @MpsMaintST_PY    REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[MaintMisc]([Refnum], [CorrEmergMaint], [CorrOthMaint], [PrevCondAct], [PrevAddMaint], [PrevRoutine], [PrefAddRoutine], [Accuracy], [FurnTubeLife], [RespUnplan], [RespPlan], [Prevent], [Predict], [Training], [FurnRetubeTime], [FurnRetubeWho], [MaintMatlPY], [ContMaintLaborPY], [ContMaintMatlPY], [MaintEquipRentPY], [OCCMaintST_PY], [MpsMaintST_PY])
	VALUES(@Refnum, @CorrEmergMaint, @CorrOthMaint, @PrevCondAct, @PrevAddMaint, @PrevRoutine, @PrefAddRoutine, @Accuracy, @FurnTubeLife, @RespUnplan, @RespPlan, @Prevent, @Predict, @Training, @FurnRetubeTime, @FurnRetubeWho, @MaintMatlPY, @ContMaintLaborPY, @ContMaintMatlPY, @MaintEquipRentPY, @OCCMaintST_PY, @MpsMaintST_PY);

END;
