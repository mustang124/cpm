﻿CREATE PROCEDURE [stgFact].[Insert_FeedPlan]
(
	@Refnum      VARCHAR (25),
	@PlanID      VARCHAR (25),

	@Daily       CHAR (1)     = NULL,
	@BiWeekly    CHAR (1)     = NULL,
	@Weekly      CHAR (1)     = NULL,
	@BiMonthly   CHAR (1)     = NULL,
	@Monthly     CHAR (1)     = NULL,
	@LessMonthly CHAR (1)     = NULL,
	@Rpt         TINYINT      = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FeedPlan]([Refnum], [PlanID], [Daily], [BiWeekly], [Weekly], [BiMonthly], [Monthly], [LessMonthly], [Rpt])
	VALUES(@Refnum, @PlanID, @Daily, @BiWeekly, @Weekly, @BiMonthly, @Monthly, @LessMonthly, @Rpt);

END;