﻿CREATE PROCEDURE [stgFact].[Insert_FeedResources]
(
    @Refnum                VARCHAR (25),

    @MinParaffin           REAL         = NULL,
    @MaxVaporPresFRS       REAL         = NULL,
    @Use_LP_PC             CHAR (1)     = NULL,
    @Lvl_LP_PC             CHAR (1)     = NULL,
    @Src_LP_PC             CHAR (1)     = NULL,
    @Use_LP_Main           CHAR (1)     = NULL,
    @Lvl_LP_Main           CHAR (1)     = NULL,
    @Src_LP_Main           CHAR (1)     = NULL,
    @Use_LPMultiPlant      CHAR (1)     = NULL,
    @Lvl_LPMultiPlant      CHAR (1)     = NULL,
    @Src_LPMultiPlant      CHAR (1)     = NULL,
    @Use_LPMultiPeriod     CHAR (1)     = NULL,
    @Lvl_LPMultiPeriod     CHAR (1)     = NULL,
    @Src_LPMultiPeriod     CHAR (1)     = NULL,
    @Use_TierData          CHAR (1)     = NULL,
    @Lvl_TierData          CHAR (1)     = NULL,
    @Src_TierData          CHAR (1)     = NULL,
    @Use_TierProduct       CHAR (1)     = NULL,
    @Lvl_TierProduct       CHAR (1)     = NULL,
    @Src_TierProduct       CHAR (1)     = NULL,
    @Use_NonLP             CHAR (1)     = NULL,
    @Lvl_NonLP             CHAR (1)     = NULL,
    @Src_NonLP             CHAR (1)     = NULL,
    @Use_Spread            CHAR (1)     = NULL,
    @Lvl_Spread            CHAR (1)     = NULL,
    @Src_Spread            CHAR (1)     = NULL,
    @Use_MarginData        CHAR (1)     = NULL,
    @Lvl_MarginData        CHAR (1)     = NULL,
    @Src_MarginData        CHAR (1)     = NULL,
    @Use_AltFeed           CHAR (1)     = NULL,
    @Lvl_AltFeed           CHAR (1)     = NULL,
    @Src_AltFeed           CHAR (1)     = NULL,
    @Use_BEAltFeed         CHAR (1)     = NULL,
    @Lvl_BEAltFeed         CHAR (1)     = NULL,
    @Src_BEAltFeed         CHAR (1)     = NULL,
    @Use_YieldPredict      CHAR (1)     = NULL,
    @Lvl_YieldPredict      CHAR (1)     = NULL,
    @Src_YieldPredict      CHAR (1)     = NULL,
    @YieldPatternCnt       REAL         = NULL,
    @YieldPatternLPCnt     REAL         = NULL,
    @YieldGroupCnt         REAL         = NULL,
    @Plant_PPCnt           REAL         = NULL,
    @Corp_PPCnt            REAL         = NULL,
    @Plant_3PlusCnt        REAL         = NULL,
    @Corp_3PlusCnt         REAL         = NULL,
    @Plant_BuyerCnt        REAL         = NULL,
    @Corp_BuyerCnt         REAL         = NULL,
    @Plant_DataCnt         REAL         = NULL,
    @Corp_DataCnt          REAL         = NULL,
    @Plant_Logistic        REAL         = NULL,
    @Corp_Logistic         REAL         = NULL,
    @Plant_Decision        CHAR (1)     = NULL,
    @Corp_Decision         CHAR (1)     = NULL,
    @PlanTimeDays          REAL         = NULL,
    @FeedSlateChg          CHAR (1)     = NULL,
    @Reliab                CHAR (1)     = NULL,
    @Environ               CHAR (1)     = NULL,
    @Furnace               CHAR (1)     = NULL,
    @Maint                 CHAR (1)     = NULL,
    @Interest              CHAR (1)     = NULL,
    @EffFeedChg            REAL         = NULL,
    @FeedPurchase          REAL         = NULL,
    @YPS_Lummus            TINYINT      = NULL,
    @YPS_SPYRO             TINYINT      = NULL,
    @YPS_SelfDev           TINYINT      = NULL,
    @YPS_Other             TINYINT      = NULL,
    @YPS_OtherDesc         VARCHAR (30) = NULL,
    @LP_Aspen              TINYINT      = NULL,
    @LP_Honeywell          TINYINT      = NULL,
    @LP_Haverly            TINYINT      = NULL,
    @LP_SelfDev            TINYINT      = NULL,
    @LP_Other              TINYINT      = NULL,
    @LP_OtherDesc          VARCHAR (30) = NULL,
    @LP_RowCnt             INT          = NULL,
    @LP_Recur_Yes          TINYINT      = NULL,
    @LP_Recur_No           TINYINT      = NULL,
    @LP_Recur_NotSure      TINYINT      = NULL,
    @LP_Recur_RPT          TINYINT      = NULL,
    @LP_DeltaBase_Yes      TINYINT      = NULL,
    @LP_DeltaBase_No       TINYINT      = NULL,
    @LP_DeltaBase_NotSure  TINYINT      = NULL,
    @LP_DeltaBase_RPT      TINYINT      = NULL,
    @LP_MixInt_Yes         TINYINT      = NULL,
    @LP_MixInt_No          TINYINT      = NULL,
    @LP_MixInt_NotSure     TINYINT      = NULL,
    @LP_MixInt_RPT         TINYINT      = NULL,
    @LP_Online_Yes         TINYINT      = NULL,
    @LP_Online_No          TINYINT      = NULL,
    @LP_Online_NotSure     TINYINT      = NULL,
    @LP_Online_RPT         TINYINT      = NULL,
    @BeyondCrack_None      TINYINT      = NULL,
    @BeyondCrack_C4        TINYINT      = NULL,
    @BeyondCrack_C5        TINYINT      = NULL,
    @BeyondCrack_Pygas     TINYINT      = NULL,
    @BeyondCrack_BTX       TINYINT      = NULL,
    @BeyondCrack_OthChem   TINYINT      = NULL,
    @BeyondCrack_OthRefine TINYINT      = NULL,
    @BeyondCrack_Oth       TINYINT      = NULL,
    @BeyondCrack_OthDesc   VARCHAR (75) = NULL,
    @Data_LPSpecial        TINYINT      = NULL,
    @Data_Econ             TINYINT      = NULL,
    @Data_FeedSupply       TINYINT      = NULL,
    @Data_Market           TINYINT      = NULL,
    @Data_TechSrvs         TINYINT      = NULL,
    @Data_Oper             TINYINT      = NULL,
    @Data_Oth              TINYINT      = NULL,
    @Data_OthDesc          VARCHAR (30) = NULL,
    @Dev_LPSpecial         TINYINT      = NULL,
    @Dev_Econ              TINYINT      = NULL,
    @Dev_FeedSupply        TINYINT      = NULL,
    @Dev_Market            TINYINT      = NULL,
    @Dev_TechSrvs          TINYINT      = NULL,
    @Dev_Oper              TINYINT      = NULL,
    @Dev_Oth               TINYINT      = NULL,
    @Dev_OthDesc           VARCHAR (30) = NULL,
    @Valid_LPSpecial       TINYINT      = NULL,
    @Valid_Econ            TINYINT      = NULL,
    @Valid_FeedSupply      TINYINT      = NULL,
    @Valid_Market          TINYINT      = NULL,
    @Valid_TechSrvs        TINYINT      = NULL,
    @Valid_Oper            TINYINT      = NULL,
    @Valid_Oth             TINYINT      = NULL,
    @Valid_OthDesc         VARCHAR (30) = NULL,
    @Constraint_LPSpecial  TINYINT      = NULL,
    @Constraint_Econ       TINYINT      = NULL,
    @Constraint_FeedSupply TINYINT      = NULL,
    @Constraint_Market     TINYINT      = NULL,
    @Constraint_TechSrvs   TINYINT      = NULL,
    @Constraint_Oper       TINYINT      = NULL,
    @Constraint_Oth        TINYINT      = NULL,
    @Constraint_OthDesc    VARCHAR (30) = NULL,
    @FeedPrice_LPSpecial   TINYINT      = NULL,
    @FeedPrice_Econ        TINYINT      = NULL,
    @FeedPrice_FeedSupply  TINYINT      = NULL,
    @FeedPrice_Market      TINYINT      = NULL,
    @FeedPrice_TechSrvs    TINYINT      = NULL,
    @FeedPrice_Oper        TINYINT      = NULL,
    @FeedPrice_Oth         TINYINT      = NULL,
    @FeedPrice_OthDesc     VARCHAR (75) = NULL,
    @ProdPrice_LPSpecial   TINYINT      = NULL,
    @ProdPrice_Econ        TINYINT      = NULL,
    @ProdPrice_FeedSupply  TINYINT      = NULL,
    @ProdPrice_Market      TINYINT      = NULL,
    @ProdPrice_TechSrvs    TINYINT      = NULL,
    @ProdPrice_Oper        TINYINT      = NULL,
    @ProdPrice_Oth         TINYINT      = NULL,
    @ProdPrice_OthDesc     VARCHAR (75) = NULL,
    @PPCom_1               TINYINT      = NULL,
    @PPCom_2               TINYINT      = NULL,
    @PPCom_3               TINYINT      = NULL,
    @PPCom_4               TINYINT      = NULL,
    @PPCom_5               TINYINT      = NULL,
    @PPCom_6               TINYINT      = NULL,
    @PPCom_7               TINYINT      = NULL,
    @PPCom_8               TINYINT      = NULL,
    @PPCom_9               TINYINT      = NULL,
    @PPFeedPrice_1         TINYINT      = NULL,
    @PPFeedPrice_2         TINYINT      = NULL,
    @PPFeedPrice_3         TINYINT      = NULL,
    @PPFeedPrice_4         TINYINT      = NULL,
    @PPFeedPrice_5         TINYINT      = NULL,
    @PPFeedPrice_6         TINYINT      = NULL,
    @PPFeedPrice_7         TINYINT      = NULL,
    @PPFeedPrice_8         TINYINT      = NULL,
    @PPFeedPrice_9         TINYINT      = NULL,
    @PPFeedCrack_1         TINYINT      = NULL,
    @PPFeedCrack_2         TINYINT      = NULL,
    @PPFeedCrack_3         TINYINT      = NULL,
    @PPFeedCrack_4         TINYINT      = NULL,
    @PPFeedCrack_5         TINYINT      = NULL,
    @PPFeedCrack_6         TINYINT      = NULL,
    @PPFeedCrack_7         TINYINT      = NULL,
    @PPFeedCrack_8         TINYINT      = NULL,
    @PPFeedCrack_9         TINYINT      = NULL,
    @PPFeedAvail_1         TINYINT      = NULL,
    @PPFeedAvail_2         TINYINT      = NULL,
    @PPFeedAvail_3         TINYINT      = NULL,
    @PPFeedAvail_4         TINYINT      = NULL,
    @PPFeedAvail_5         TINYINT      = NULL,
    @PPFeedAvail_6         TINYINT      = NULL,
    @PPFeedAvail_7         TINYINT      = NULL,
    @PPFeedAvail_8         TINYINT      = NULL,
    @PPFeedAvail_9         TINYINT      = NULL,
    @PPProdPrice_1         TINYINT      = NULL,
    @PPProdPrice_2         TINYINT      = NULL,
    @PPProdPrice_3         TINYINT      = NULL,
    @PPProdPrice_4         TINYINT      = NULL,
    @PPProdPrice_5         TINYINT      = NULL,
    @PPProdPrice_6         TINYINT      = NULL,
    @PPProdPrice_7         TINYINT      = NULL,
    @PPProdPrice_8         TINYINT      = NULL,
    @PPProdPrice_9         TINYINT      = NULL,
    @PPProdDemand_1        TINYINT      = NULL,
    @PPProdDemand_2        TINYINT      = NULL,
    @PPProdDemand_3        TINYINT      = NULL,
    @PPProdDemand_4        TINYINT      = NULL,
    @PPProdDemand_5        TINYINT      = NULL,
    @PPProdDemand_6        TINYINT      = NULL,
    @PPProdDemand_7        TINYINT      = NULL,
    @PPProdDemand_8        TINYINT      = NULL,
    @PPProdDemand_9        TINYINT      = NULL,
    @PPStatus_1            TINYINT      = NULL,
    @PPStatus_2            TINYINT      = NULL,
    @PPStatus_3            TINYINT      = NULL,
    @PPStatus_4            TINYINT      = NULL,
    @PPStatus_5            TINYINT      = NULL,
    @PPStatus_6            TINYINT      = NULL,
    @PPStatus_7            TINYINT      = NULL,
    @PPStatus_8            TINYINT      = NULL,
    @PPStatus_9            TINYINT      = NULL,
    @PPTools_1             TINYINT      = NULL,
    @PPTools_2             TINYINT      = NULL,
    @PPTools_3             TINYINT      = NULL,
    @PPTools_4             TINYINT      = NULL,
    @PPTools_5             TINYINT      = NULL,
    @PPTools_6             TINYINT      = NULL,
    @PPTools_7             TINYINT      = NULL,
    @PPTools_8             TINYINT      = NULL,
    @PPTools_9             TINYINT      = NULL,
    @PPExper_1             TINYINT      = NULL,
    @PPExper_2             TINYINT      = NULL,
    @PPExper_3             TINYINT      = NULL,
    @PPExper_4             TINYINT      = NULL,
    @PPExper_5             TINYINT      = NULL,
    @PPExper_6             TINYINT      = NULL,
    @PPExper_7             TINYINT      = NULL,
    @PPExper_8             TINYINT      = NULL,
    @PPExper_9             TINYINT      = NULL,
    @ExeFeedPurchase       REAL         = NULL,
    @YPS_RPT               TINYINT      = NULL,
    @LP_RPT                TINYINT      = NULL,
    @BeyondCrack_RPT       TINYINT      = NULL,
    @Data_RPT              TINYINT      = NULL,
    @Dev_RPT               TINYINT      = NULL,
    @Valid_RPT             TINYINT      = NULL,
    @Constraint_RPT        TINYINT      = NULL,
    @FeedPrice_RPT         TINYINT      = NULL,
    @ProdPrice_RPT         TINYINT      = NULL,
    @PPCom_RPT             TINYINT      = NULL,
    @PPFeedPrice_RPT       TINYINT      = NULL,
    @PPFeedCrack_RPT       TINYINT      = NULL,
    @PPFeedAvail_RPT       TINYINT      = NULL,
    @PPProdPrice_RPT       TINYINT      = NULL,
    @PPProdDemand_RPT      TINYINT      = NULL,
    @PPProdStatus_RPT      TINYINT      = NULL,
    @PPExper_RPT           TINYINT      = NULL,
    @PPTools_RPT           TINYINT      = NULL,
    @PPStatus_RPT          TINYINT      = NULL,
    @Use_ScheduleModel     CHAR (1)     = NULL,
    @Lvl_ScheduleModel     CHAR (1)     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FeedResources]([Refnum], [MinParaffin], [MaxVaporPresFRS], [Use_LP_PC], [Lvl_LP_PC], [Src_LP_PC], [Use_LP_Main], [Lvl_LP_Main], [Src_LP_Main], [Use_LPMultiPlant], [Lvl_LPMultiPlant], [Src_LPMultiPlant], [Use_LPMultiPeriod], [Lvl_LPMultiPeriod], [Src_LPMultiPeriod], [Use_TierData], [Lvl_TierData], [Src_TierData], [Use_TierProduct], [Lvl_TierProduct], [Src_TierProduct], [Use_NonLP], [Lvl_NonLP], [Src_NonLP], [Use_Spread], [Lvl_Spread], [Src_Spread], [Use_MarginData], [Lvl_MarginData], [Src_MarginData], [Use_AltFeed], [Lvl_AltFeed], [Src_AltFeed], [Use_BEAltFeed], [Lvl_BEAltFeed], [Src_BEAltFeed], [Use_YieldPredict], [Lvl_YieldPredict], [Src_YieldPredict], [YieldPatternCnt], [YieldPatternLPCnt], [YieldGroupCnt], [Plant_PPCnt], [Corp_PPCnt], [Plant_3PlusCnt], [Corp_3PlusCnt], [Plant_BuyerCnt], [Corp_BuyerCnt], [Plant_DataCnt], [Corp_DataCnt], [Plant_Logistic], [Corp_Logistic], [Plant_Decision], [Corp_Decision], [PlanTimeDays], [FeedSlateChg], [Reliab], [Environ], [Furnace], [Maint], [Interest], [EffFeedChg], [FeedPurchase], [YPS_Lummus], [YPS_SPYRO], [YPS_SelfDev], [YPS_Other], [YPS_OtherDesc], [LP_Aspen], [LP_Honeywell], [LP_Haverly], [LP_SelfDev], [LP_Other], [LP_OtherDesc], [LP_RowCnt], [LP_Recur_Yes], [LP_Recur_No], [LP_Recur_NotSure], [LP_Recur_RPT], [LP_DeltaBase_Yes], [LP_DeltaBase_No], [LP_DeltaBase_NotSure], [LP_DeltaBase_RPT], [LP_MixInt_Yes], [LP_MixInt_No], [LP_MixInt_NotSure], [LP_MixInt_RPT], [LP_Online_Yes], [LP_Online_No], [LP_Online_NotSure], [LP_Online_RPT], [BeyondCrack_None], [BeyondCrack_C4], [BeyondCrack_C5], [BeyondCrack_Pygas], [BeyondCrack_BTX], [BeyondCrack_OthChem], [BeyondCrack_OthRefine], [BeyondCrack_Oth], [BeyondCrack_OthDesc], [Data_LPSpecial], [Data_Econ], [Data_FeedSupply], [Data_Market], [Data_TechSrvs], [Data_Oper], [Data_Oth], [Data_OthDesc], [Dev_LPSpecial], [Dev_Econ], [Dev_FeedSupply], [Dev_Market], [Dev_TechSrvs], [Dev_Oper], [Dev_Oth], [Dev_OthDesc], [Valid_LPSpecial], [Valid_Econ], [Valid_FeedSupply], [Valid_Market], [Valid_TechSrvs], [Valid_Oper], [Valid_Oth], [Valid_OthDesc], [Constraint_LPSpecial], [Constraint_Econ], [Constraint_FeedSupply], [Constraint_Market], [Constraint_TechSrvs], [Constraint_Oper], [Constraint_Oth], [Constraint_OthDesc], [FeedPrice_LPSpecial], [FeedPrice_Econ], [FeedPrice_FeedSupply], [FeedPrice_Market], [FeedPrice_TechSrvs], [FeedPrice_Oper], [FeedPrice_Oth], [FeedPrice_OthDesc], [ProdPrice_LPSpecial], [ProdPrice_Econ], [ProdPrice_FeedSupply], [ProdPrice_Market], [ProdPrice_TechSrvs], [ProdPrice_Oper], [ProdPrice_Oth], [ProdPrice_OthDesc], [PPCom_1], [PPCom_2], [PPCom_3], [PPCom_4], [PPCom_5], [PPCom_6], [PPCom_7], [PPCom_8], [PPCom_9], [PPFeedPrice_1], [PPFeedPrice_2], [PPFeedPrice_3], [PPFeedPrice_4], [PPFeedPrice_5], [PPFeedPrice_6], [PPFeedPrice_7], [PPFeedPrice_8], [PPFeedPrice_9], [PPFeedCrack_1], [PPFeedCrack_2], [PPFeedCrack_3], [PPFeedCrack_4], [PPFeedCrack_5], [PPFeedCrack_6], [PPFeedCrack_7], [PPFeedCrack_8], [PPFeedCrack_9], [PPFeedAvail_1], [PPFeedAvail_2], [PPFeedAvail_3], [PPFeedAvail_4], [PPFeedAvail_5], [PPFeedAvail_6], [PPFeedAvail_7], [PPFeedAvail_8], [PPFeedAvail_9], [PPProdPrice_1], [PPProdPrice_2], [PPProdPrice_3], [PPProdPrice_4], [PPProdPrice_5], [PPProdPrice_6], [PPProdPrice_7], [PPProdPrice_8], [PPProdPrice_9], [PPProdDemand_1], [PPProdDemand_2], [PPProdDemand_3], [PPProdDemand_4], [PPProdDemand_5], [PPProdDemand_6], [PPProdDemand_7], [PPProdDemand_8], [PPProdDemand_9], [PPStatus_1], [PPStatus_2], [PPStatus_3], [PPStatus_4], [PPStatus_5], [PPStatus_6], [PPStatus_7], [PPStatus_8], [PPStatus_9], [PPTools_1], [PPTools_2], [PPTools_3], [PPTools_4], [PPTools_5], [PPTools_6], [PPTools_7], [PPTools_8], [PPTools_9], [PPExper_1], [PPExper_2], [PPExper_3], [PPExper_4], [PPExper_5], [PPExper_6], [PPExper_7], [PPExper_8], [PPExper_9], [ExeFeedPurchase], [YPS_RPT], [LP_RPT], [BeyondCrack_RPT], [Data_RPT], [Dev_RPT], [Valid_RPT], [Constraint_RPT], [FeedPrice_RPT], [ProdPrice_RPT], [PPCom_RPT], [PPFeedPrice_RPT], [PPFeedCrack_RPT], [PPFeedAvail_RPT], [PPProdPrice_RPT], [PPProdDemand_RPT], [PPProdStatus_RPT], [PPExper_RPT], [PPTools_RPT], [PPStatus_RPT], [Use_ScheduleModel], [Lvl_ScheduleModel])
	VALUES(@Refnum, @MinParaffin, @MaxVaporPresFRS, @Use_LP_PC, @Lvl_LP_PC, @Src_LP_PC, @Use_LP_Main, @Lvl_LP_Main, @Src_LP_Main, @Use_LPMultiPlant, @Lvl_LPMultiPlant, @Src_LPMultiPlant, @Use_LPMultiPeriod, @Lvl_LPMultiPeriod, @Src_LPMultiPeriod, @Use_TierData, @Lvl_TierData, @Src_TierData, @Use_TierProduct, @Lvl_TierProduct, @Src_TierProduct, @Use_NonLP, @Lvl_NonLP, @Src_NonLP, @Use_Spread, @Lvl_Spread, @Src_Spread, @Use_MarginData, @Lvl_MarginData, @Src_MarginData, @Use_AltFeed, @Lvl_AltFeed, @Src_AltFeed, @Use_BEAltFeed, @Lvl_BEAltFeed, @Src_BEAltFeed, @Use_YieldPredict, @Lvl_YieldPredict, @Src_YieldPredict, @YieldPatternCnt, @YieldPatternLPCnt, @YieldGroupCnt, @Plant_PPCnt, @Corp_PPCnt, @Plant_3PlusCnt, @Corp_3PlusCnt, @Plant_BuyerCnt, @Corp_BuyerCnt, @Plant_DataCnt, @Corp_DataCnt, @Plant_Logistic, @Corp_Logistic, @Plant_Decision, @Corp_Decision, @PlanTimeDays, @FeedSlateChg, @Reliab, @Environ, @Furnace, @Maint, @Interest, @EffFeedChg, @FeedPurchase, @YPS_Lummus, @YPS_SPYRO, @YPS_SelfDev, @YPS_Other, @YPS_OtherDesc, @LP_Aspen, @LP_Honeywell, @LP_Haverly, @LP_SelfDev, @LP_Other, @LP_OtherDesc, @LP_RowCnt, @LP_Recur_Yes, @LP_Recur_No, @LP_Recur_NotSure, @LP_Recur_RPT, @LP_DeltaBase_Yes, @LP_DeltaBase_No, @LP_DeltaBase_NotSure, @LP_DeltaBase_RPT, @LP_MixInt_Yes, @LP_MixInt_No, @LP_MixInt_NotSure, @LP_MixInt_RPT, @LP_Online_Yes, @LP_Online_No, @LP_Online_NotSure, @LP_Online_RPT, @BeyondCrack_None, @BeyondCrack_C4, @BeyondCrack_C5, @BeyondCrack_Pygas, @BeyondCrack_BTX, @BeyondCrack_OthChem, @BeyondCrack_OthRefine, @BeyondCrack_Oth, @BeyondCrack_OthDesc, @Data_LPSpecial, @Data_Econ, @Data_FeedSupply, @Data_Market, @Data_TechSrvs, @Data_Oper, @Data_Oth, @Data_OthDesc, @Dev_LPSpecial, @Dev_Econ, @Dev_FeedSupply, @Dev_Market, @Dev_TechSrvs, @Dev_Oper, @Dev_Oth, @Dev_OthDesc, @Valid_LPSpecial, @Valid_Econ, @Valid_FeedSupply, @Valid_Market, @Valid_TechSrvs, @Valid_Oper, @Valid_Oth, @Valid_OthDesc, @Constraint_LPSpecial, @Constraint_Econ, @Constraint_FeedSupply, @Constraint_Market, @Constraint_TechSrvs, @Constraint_Oper, @Constraint_Oth, @Constraint_OthDesc, @FeedPrice_LPSpecial, @FeedPrice_Econ, @FeedPrice_FeedSupply, @FeedPrice_Market, @FeedPrice_TechSrvs, @FeedPrice_Oper, @FeedPrice_Oth, @FeedPrice_OthDesc, @ProdPrice_LPSpecial, @ProdPrice_Econ, @ProdPrice_FeedSupply, @ProdPrice_Market, @ProdPrice_TechSrvs, @ProdPrice_Oper, @ProdPrice_Oth, @ProdPrice_OthDesc, @PPCom_1, @PPCom_2, @PPCom_3, @PPCom_4, @PPCom_5, @PPCom_6, @PPCom_7, @PPCom_8, @PPCom_9, @PPFeedPrice_1, @PPFeedPrice_2, @PPFeedPrice_3, @PPFeedPrice_4, @PPFeedPrice_5, @PPFeedPrice_6, @PPFeedPrice_7, @PPFeedPrice_8, @PPFeedPrice_9, @PPFeedCrack_1, @PPFeedCrack_2, @PPFeedCrack_3, @PPFeedCrack_4, @PPFeedCrack_5, @PPFeedCrack_6, @PPFeedCrack_7, @PPFeedCrack_8, @PPFeedCrack_9, @PPFeedAvail_1, @PPFeedAvail_2, @PPFeedAvail_3, @PPFeedAvail_4, @PPFeedAvail_5, @PPFeedAvail_6, @PPFeedAvail_7, @PPFeedAvail_8, @PPFeedAvail_9, @PPProdPrice_1, @PPProdPrice_2, @PPProdPrice_3, @PPProdPrice_4, @PPProdPrice_5, @PPProdPrice_6, @PPProdPrice_7, @PPProdPrice_8, @PPProdPrice_9, @PPProdDemand_1, @PPProdDemand_2, @PPProdDemand_3, @PPProdDemand_4, @PPProdDemand_5, @PPProdDemand_6, @PPProdDemand_7, @PPProdDemand_8, @PPProdDemand_9, @PPStatus_1, @PPStatus_2, @PPStatus_3, @PPStatus_4, @PPStatus_5, @PPStatus_6, @PPStatus_7, @PPStatus_8, @PPStatus_9, @PPTools_1, @PPTools_2, @PPTools_3, @PPTools_4, @PPTools_5, @PPTools_6, @PPTools_7, @PPTools_8, @PPTools_9, @PPExper_1, @PPExper_2, @PPExper_3, @PPExper_4, @PPExper_5, @PPExper_6, @PPExper_7, @PPExper_8, @PPExper_9, @ExeFeedPurchase, @YPS_RPT, @LP_RPT, @BeyondCrack_RPT, @Data_RPT, @Dev_RPT, @Valid_RPT, @Constraint_RPT, @FeedPrice_RPT, @ProdPrice_RPT, @PPCom_RPT, @PPFeedPrice_RPT, @PPFeedCrack_RPT, @PPFeedAvail_RPT, @PPProdPrice_RPT, @PPProdDemand_RPT, @PPProdStatus_RPT, @PPExper_RPT, @PPTools_RPT, @PPStatus_RPT, @Use_ScheduleModel, @Lvl_ScheduleModel)

END;