﻿CREATE PROCEDURE [stgFact].[Insert_FurnRely]
(
	@Refnum					VARCHAR (25),
	@FurnNo					TINYINT,
	@FurnFeed				VARCHAR (25),

	@DTDecoke				REAL					= NULL,
	@DTTLEClean				REAL					= NULL,
	@DTMinor				REAL					= NULL,
	@DTMajor				REAL					= NULL,
	@STDT					REAL					= NULL,
	@DTStandby				REAL					= NULL,
	@TotTimeOff				REAL					= NULL,
	@TADownDays				REAL					= NULL,
	@OthDownDays			REAL					= NULL,
	@FurnAvail				REAL					= NULL,
	@AdjFurnOnstream		REAL					= NULL,
	@FeedQtyKMTA			REAL					= NULL,
	@FeedCapMTD				REAL					= NULL,
	@OperDays				INT						= NULL,
	@ActCap					REAL					= NULL,
	@AdjFurnUtil			REAL					= NULL,
	@FurnSlowD				REAL					= NULL,
	@YearRetubed			SMALLINT				= NULL,
	@RetubeCost				REAL					= NULL,
	@RetubeCostMatl			REAL					= NULL,
	@RetubeCostLabor		REAL					= NULL,
	@OtherMajorCost			REAL					= NULL,
	@TotCost				REAL					= NULL,
	@RetubeInterval			REAL					= NULL,
	@OnStreamDays			INT						= NULL,
	@FuelCons				REAL					= NULL,
	@FuelType				VARCHAR (2)				= NULL,
	@StackOxy				REAL					= NULL,
	@ArchDraft				REAL					= NULL,
	@StackTemp				REAL					= NULL,
	@CrossTemp				REAL					= NULL,
	@FurnFeedBreak			VARCHAR (10)			= NULL,
	@AnnRetubeCostMatlMT	REAL					= NULL,
	@AnnRetubeCostLaborMT	REAL					= NULL,
	@AnnRetubeCostMT		REAL					= NULL,
	@OtherMajorCostMT		REAL					= NULL,
	@TotCostMT				REAL					= NULL,
	@FuelTypeYN				INT						= NULL,
	@FuelTypeFO				INT						= NULL,
	@FuelTypeFG				INT						= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FurnRely]([Refnum], [FurnNo], [FurnFeed], [DTDecoke], [DTTLEClean], [DTMinor], [DTMajor], [STDT], [DTStandby], [TotTimeOff], [TADownDays], [OthDownDays], [FurnAvail], [AdjFurnOnstream], [FeedQtyKMTA], [FeedCapMTD], [OperDays], [ActCap], [AdjFurnUtil], [FurnSlowD], [YearRetubed], [RetubeCost], [RetubeCostMatl], [RetubeCostLabor], [OtherMajorCost], [TotCost], [RetubeInterval], [OnStreamDays], [FuelCons], [FuelType], [StackOxy], [ArchDraft], [StackTemp], [CrossTemp], [FurnFeedBreak], [AnnRetubeCostMatlMT], [AnnRetubeCostLaborMT], [AnnRetubeCostMT], [OtherMajorCostMT], [TotCostMT], [FuelTypeYN], [FuelTypeFO], [FuelTypeFG])
	VALUES(@Refnum, @FurnNo, @FurnFeed, @DTDecoke, @DTTLEClean, @DTMinor, @DTMajor, @STDT, @DTStandby, @TotTimeOff, @TADownDays, @OthDownDays, @FurnAvail, @AdjFurnOnstream, @FeedQtyKMTA, @FeedCapMTD, @OperDays, @ActCap, @AdjFurnUtil, @FurnSlowD, @YearRetubed, @RetubeCost, @RetubeCostMatl, @RetubeCostLabor, @OtherMajorCost, @TotCost, @RetubeInterval, @OnStreamDays, @FuelCons, @FuelType, @StackOxy, @ArchDraft, @StackTemp, @CrossTemp, @FurnFeedBreak, @AnnRetubeCostMatlMT, @AnnRetubeCostLaborMT, @AnnRetubeCostMT, @OtherMajorCostMT, @TotCostMT, @FuelTypeYN, @FuelTypeFO, @FuelTypeFG);

END;