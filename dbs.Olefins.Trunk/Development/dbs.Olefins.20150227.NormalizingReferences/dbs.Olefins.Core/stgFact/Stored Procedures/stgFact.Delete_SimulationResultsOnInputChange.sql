﻿CREATE PROCEDURE [stgFact].[Delete_SimulationResultsOnInputChange]
(
	@Refnum					VARCHAR (25),
	@Tolerance				FLOAT = 0.0001
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @UploadSim	BIT = 0;

		DECLARE @Delete		BIT = 0;

		/*	stgFact.FeedQuality		-> fact.FeedStockCrackingParameters		*/
		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum],
					etl.ConvDateKey(t.StudyYear)							[CalDateKey],
		
					etl.ConvStreamID(q.FeedProdID)							[StreamId],
					etl.ConvStreamDescription(q.FeedProdID, q.OthLiqFeedDESC, NULL, NULL, NULL)
																			[StreamDescription],
					q.CoilOutletPressure									[CoilOutletPressure_Psia],
					q.CoilOutletTemp										[CoilOutletTemp_C],
					q.SteamHCRatio											[SteamHydrocarbon_Ratio],
					q.EthyleneYield * CASE WHEN q.EthyleneYield < 1.0 THEN 100.0 ELSE 1.0 END		[EthyleneYield_WtPcnt],
					q.PropylEthylRatio										[PropyleneEthylene_Ratio],
					q.PropylMethaneRatio									[PropyleneMethane_Ratio],
					q.FeedConv			* 100.0								[FeedConv_WtPcnt],
					q.SG													[Density_SG],
					q.SulfurPPM												[SulfurContent_ppm],
					etl.ConvDistillationMethod(q.ASTMethod)					[DistMethodID],
					q.ConstrYear											[FurnConstruction_Year],
					q.ResTime												[ResidenceTime_s],
					CASE WHEN etl.ConvStreamID(q.FeedProdID) IN ('EthRec', 'ProRec', 'ButRec') THEN 1 ELSE 0 END	[Recycle_Bit]
		
				FROM stgFact.FeedQuality q
				INNER JOIN stgFact.TSort t
					ON t.Refnum = q.Refnum
				INNER JOIN stgFact.Quantity z
					ON	z.Refnum = q.Refnum
					AND	z.FeedProdID = q.FeedProdID
					AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
				WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
				) stage
			FULL OUTER JOIN (
				SELECT
					fcp.Refnum,
					fcp.CalDateKey,
					fcp.StreamId,
					fcp.StreamDescription,
					fcp.CoilOutletPressure_Psia,
					fcp.CoilOutletTemp_C,
					fcp.SteamHydrocarbon_Ratio,
					fcp.EthyleneYield_WtPcnt,
					fcp.PropyleneEthylene_Ratio,
					fcp.PropyleneMethane_Ratio,
					fcp.FeedConv_WtPcnt,
					fcp.Density_SG,
					fcp.RadiantWallTemp_C,
					fcp.CoilInletTemp_C,
					fcp.FlowRate_KgHr,
					fcp.FurnConstruction_Year,
					fcp.ResidenceTime_s,
					fcp.Recycle_Bit,
					fcp.SulfurContent_ppm
				FROM fact.FeedStockCrackingParameters fcp
				WHERE fcp.Refnum = @Refnum
				) fact
				ON	fact.Refnum = stage.Refnum
				AND	fact.CalDateKey = stage.CalDateKey
				AND	fact.StreamId = stage.StreamId
				AND	fact.StreamDescription = stage.StreamDescription
			WHERE	[stgFact].[RelativeError](stage.CoilOutletPressure_Psia, fact.CoilOutletPressure_Psia) > @Tolerance
				OR	[stgFact].[RelativeError](stage.CoilOutletTemp_C, fact.CoilOutletTemp_C) > @Tolerance
				OR	[stgFact].[RelativeError](stage.SteamHydrocarbon_Ratio, fact.SteamHydrocarbon_Ratio) > @Tolerance
				OR	[stgFact].[RelativeError](stage.EthyleneYield_WtPcnt, fact.EthyleneYield_WtPcnt) > @Tolerance
				OR	[stgFact].[RelativeError](stage.PropyleneEthylene_Ratio, fact.PropyleneEthylene_Ratio) > @Tolerance
				OR	[stgFact].[RelativeError](stage.PropyleneMethane_Ratio, fact.PropyleneMethane_Ratio) > @Tolerance
				OR	[stgFact].[RelativeError](stage.FeedConv_WtPcnt, fact.FeedConv_WtPcnt) > @Tolerance
				--OR	ABS(fact.Density_SG					- stage.Density_SG) / fact.Density_SG								> @Tolerance
				--OR	ABS(fact.ResidenceTime_s			- stage.ResidenceTime_s) / fact.ResidenceTime_s						> @Tolerance
				--OR	ABS(fact.SulfurContent_ppm			- stage.SulfurContent_ppm) / fact.SulfurContent_ppm					> @Tolerance
				--OR	fact.Recycle_Bit				<> stage.Recycle_Bit
			)
		BEGIN
			SET @Delete = 1;
		END;

		/*	stgFact.FeedQuality		-> fact.CompositionQuantity				*/
		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum],
					etl.ConvDateKey(t.StudyYear)							[CalDateKey],
					etl.ConvStreamID(q.FeedProdID)							[StreamId],
					etl.ConvStreamDescription(z.FeedProdID, q.OthLiqFeedDESC, z.MiscProd1, z.MiscProd2, z.MiscFeed) [StreamDescription],

					q.Methane			* 100.0 [CH4],
					q.Ethane			* 100.0 [C2H6],
					q.Ethylene			* 100.0 [C2H4],
					q.Propane			* 100.0 [C3H8],
					q.Propylene			* 100.0 [C3H6],
					q.nButane			* 100.0 [NBUTA],
					q.iButane			* 100.0 [IBUTA],
					q.Isobutylene		* 100.0 [IB],
					q.Butene1			* 100.0 [B1],
					q.Butadiene			* 100.0 [C4H6],
					q.nPentane			* 100.0 [NC5],
					q.iPentane			* 100.0 [IC5],
					q.nHexane			* 100.0 [NC6],
					q.iHexane			* 100.0 [C6ISO],	--	IC6
					q.Septane			* 100.0 [C7H16],	--	NC7
					q.Octane			* 100.0 [C8H18],	--	NC8
					q.CO2				* 100.0 [CO2],
					q.Hydrogen			* 100.0 [H2],
					q.Sulfur			* 100.0 [S],
			
					q.NParaffins		* 100.0 [P],
					q.IsoParaffins		* 100.0 [I],
					q.Aromatics			* 100.0 [A],
					q.Naphthenes		* 100.0 [N],
					q.Olefins			* 100.0 [O]
			
				FROM stgFact.FeedQuality q
				INNER JOIN stgFact.TSort t
					ON	t.Refnum = q.Refnum
				INNER JOIN stgFact.Quantity z
					ON	z.Refnum = q.Refnum
					AND	z.FeedProdID = q.FeedProdID
					AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
				WHERE	(q.Methane		> 0.0
					OR	q.Ethane		> 0.0
					OR	q.Ethylene		> 0.0
					OR	q.Propane		> 0.0
					OR	q.Propylene		> 0.0
					OR	q.nButane		> 0.0
					OR	q.iButane		> 0.0
					OR	q.Isobutylene	> 0.0
					OR	q.Butene1		> 0.0
					OR	q.Butadiene		> 0.0
					OR	q.nPentane		> 0.0
					OR	q.iPentane		> 0.0
					OR	q.nHexane		> 0.0
					OR	q.iHexane		> 0.0
					OR	q.Septane		> 0.0
					OR	q.Octane		> 0.0
					OR	q.CO2			> 0.0
					OR	q.Hydrogen		> 0.0
					OR	q.Sulfur		> 0.0
			
					OR	q.NParaffins	> 0.0
					OR	q.IsoParaffins	> 0.0
					OR	q.Aromatics		> 0.0
					OR	q.Naphthenes	> 0.0
					OR	q.Olefins		> 0.0)
					AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
				) p
				UNPIVOT(Component_WtPcnt FOR ComponentId IN(
					[CH4],
					[C2H6],
					[C2H4],
					[C3H8],
					[C3H6],
					[NBUTA],
					[IBUTA],
					[IB],
					[B1],
					[C4H6],
					[NC5],
					[IC5],
					[NC6],
					[C6ISO],
					[C7H16],
					[C8H18],
					[CO2],
					[H2],
					[S],
		
					[P],
					[I],
					[A],
					[N],
					[O]
					)
				) stage
			FULL OUTER JOIN (
				SELECT
					c.Refnum,
					c.CalDateKey,
					c.StreamId,
					c.StreamDescription,
					c.ComponentId,
					c.Component_WtPcnt [Component_WtPcnt]
				FROM fact.CompositionQuantity		c
				WHERE	c.ComponentId IN (SELECT b.DescendantId FROM dim.Stream_Bridge b WHERE b.StreamId IN ('Light', 'Recycle', 'Liquid'))
					AND	c.Component_WtPcnt > 0.0
					AND	c.Refnum = @Refnum
				) fact
				ON	fact.Refnum				= stage.Refnum
				AND	fact.CalDateKey			= stage.CalDateKey
				AND	fact.StreamId			= stage.StreamId
				AND	fact.StreamDescription	= stage.StreamDescription
				AND	fact.ComponentId		= stage.ComponentId
			WHERE	ABS(fact.Component_WtPcnt - stage.Component_WtPcnt) / fact.Component_WtPcnt	> @Tolerance
			)
		BEGIN
			SET @Delete = 1;
		END;

		/*	stgFact.FeedQuality		-> fact.FeedStockDistillation			*/
		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum],
					etl.ConvDateKey(t.StudyYear)							[CalDateKey],
			
					etl.ConvStreamID(q.FeedProdID)							[StreamId],
					etl.ConvStreamDescription(q.FeedProdID, q.OthLiqFeedDESC, NULL, NULL, NULL) [StreamDescription],
			
					q.IBP			[D000],
					q.D5pcnt		[D005],
					q.D10Pcnt		[D010],
					q.D30Pcnt		[D030],
					q.D50Pcnt		[D050],
					q.D70Pcnt		[D070],
					q.D90Pcnt		[D090],
					q.D95Pcnt		[D095],
					q.EP			[D100]
			
				FROM stgFact.FeedQuality q
				INNER JOIN stgFact.TSort t
					ON	t.Refnum = q.Refnum
				INNER JOIN stgFact.Quantity z
					ON	z.Refnum = q.Refnum
					AND	z.FeedProdID = q.FeedProdID
					AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
				WHERE	(q.IBP		> 0.0  
					OR	q.D5pcnt	> 0.0  
					OR	q.D10Pcnt	> 0.0  
					OR	q.D30Pcnt	> 0.0  
					OR	q.D50Pcnt	> 0.0  
					OR	q.D70Pcnt	> 0.0  
					OR	q.D90Pcnt	> 0.0  
					OR	q.D95Pcnt	> 0.0  
					OR	q.EP		> 0.0)
					AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
				) p
				UNPIVOT(
				Temp_C FOR DistillationID IN(
					[D000],
					[D005],
					[D010],
					[D030],
					[D050],
					[D070],
					[D090],
					[D095],
					[D100]
					)
				) stage
			FULL OUTER JOIN (
				SELECT
					fd.Refnum,
					fd.CalDateKey,
					fd.StreamId,
					fd.StreamDescription,
					fd.DistillationID,
					fd.Temp_C
				FROM fact.FeedStockDistillation fd
				WHERE	fd.Temp_C > 0.0
					AND	fd.Refnum = @Refnum
				) fact
				ON	fact.Refnum = stage.Refnum
				AND	fact.CalDateKey = stage.CalDateKey
				AND	fact.StreamId = stage.StreamId
				AND	fact.StreamDescription = stage.StreamDescription
				AND	fact.DistillationID = stage.DistillationID
			WHERE	ABS(fact.Temp_C - stage.Temp_C) / fact.Temp_C > @Tolerance
			)
		BEGIN
			SET @Delete = 1;
		END;

		/*	stgFact.Quantity		-> fact.Quantity						*/
		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT
					etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum],
					etl.ConvDateKey(t.StudyYear)							[CalDateKey],
					etl.ConvStreamID(q.FeedProdID)							[StreamId],
					etl.ConvStreamDescription(q.FeedProdID, q.OthLiqFeedDESC, NULL, NULL, NULL)
																			[StreamDescription],
					z.AnnFeedProd											[Quantity_kMT]
				FROM stgFact.FeedQuality q
				INNER JOIN stgFact.TSort t
					ON t.Refnum = q.Refnum
				INNER JOIN stgFact.Quantity z
					ON	z.Refnum = q.Refnum
					AND	z.FeedProdID = q.FeedProdID
					AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
				WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
				) stage
			FULL OUTER JOIN (
				SELECT
					q.Refnum,
					MAX(q.CalDateKey)		[CalDateKey],
					q.StreamId,
					q.StreamDescription,
					SUM(q.Quantity_kMT)		[Quantity_kMT]
				FROM fact.Quantity		q
				WHERE	q.StreamId IN (SELECT b.DescendantId FROM dim.Stream_Bridge b WHERE b.StreamId IN ('Light', 'Recycle', 'Liquid'))
					AND	q.Refnum = @Refnum
				GROUP BY
					q.Refnum,
					q.StreamId,
					q.StreamDescription
				) fact
				ON	fact.Refnum				= stage.Refnum
				AND	fact.CalDateKey			= stage.CalDateKey
				AND	fact.StreamId			= stage.StreamId
				AND	fact.StreamDescription	= stage.StreamDescription
			WHERE	ABS(fact.Quantity_kMT - stage.Quantity_kMT) / fact.Quantity_kMT	> @Tolerance
			)
		BEGIN
			SET @Delete = 1;
		END;

		/*	stgFact.Misc (Recycle)	-> fact.QuantitySuppRecycled			*/
		IF EXISTS (
			SELECT TOP 1 1 [DeleteSimData]
			FROM (
				SELECT *
				FROM(
					SELECT
						etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')	[Refnum],
						etl.ConvDateKey(t.StudyYear)					[CalDateKey],
						m.EthPcntTot * 100.0	[C2H6],
						m.ProPcntTot * 100.0	[C3H8],
						m.ButPcntTot * 100.0	[C4H10]
					FROM stgFact.Misc m
					INNER JOIN stgFact.TSort t
						ON t.Refnum = m.Refnum
					WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
					) p
					UNPIVOT(Recycled_WtPcnt FOR ComponentId IN(
						[C2H6],
						[C3H8],
						[C4H10]
						)
					) u
				) stage
			FULL OUTER JOIN (
				SELECT
					qsr.Refnum,
					qsr.CalDateKey,
					qsr.ComponentId,
					qsr.Recycled_WtPcnt
				FROM fact.QuantitySuppRecycled	qsr
				WHERE	qsr.Refnum = @Refnum
					AND	qsr.Recycled_WtPcnt <> 0.0
				) fact
				ON	fact.Refnum				= stage.Refnum
				AND	fact.CalDateKey			= stage.CalDateKey
				AND	fact.ComponentId		= stage.ComponentId
			WHERE	ABS(fact.Recycled_WtPcnt - stage.Recycled_WtPcnt) / fact.Recycled_WtPcnt	> @Tolerance
			)
		BEGIN
			SET @Delete = 1;
		END;

		IF (@Delete = 1)
		BEGIN

			PRINT NCHAR(9) + 'DELETE SIMULATION DATA ' + @Refnum;

			EXECUTE [dbo].[spInsertMessage]				@Refnum, 15, 'Calcs';
			EXECUTE [dbo].[spInsertMessage]				@Refnum, 16, 'Calcs';

			EXECUTE [stgFact].[Delete_SimulationData]	@Refnum, 'PYPS';
			EXECUTE [stgFact].[Delete_SimulationData]	@Refnum, 'SPSL';

			EXECUTE [sim].[Delete_SimulationData]		@Refnum;

		END;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;