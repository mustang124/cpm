﻿CREATE PROCEDURE [stgFact].[Insert_Composition]
(
	@Refnum     VARCHAR (25),
	@FeedProdID VARCHAR (20),

	@H2         REAL         = NULL,
	@CH4        REAL         = NULL,
	@C2H2       REAL         = NULL,
	@C2H6       REAL         = NULL,
	@C2H4       REAL         = NULL,
	@C3H6       REAL         = NULL,
	@C3H8       REAL         = NULL,
	@BUTAD      REAL         = NULL,
	@C4S        REAL         = NULL,
	@C4H10      REAL         = NULL,
	@BZ         REAL         = NULL,
	@PYGAS      REAL         = NULL,
	@PYOIL      REAL         = NULL,
	@INERT      REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Composition]([Refnum], [FeedProdID], [H2], [CH4], [C2H2], [C2H6], [C2H4], [C3H6], [C3H8], [BUTAD], [C4S], [C4H10], [BZ], [PYGAS], [PYOIL], [INERT])
	VALUES(@Refnum, @FeedProdID, @H2, @CH4, @C2H2, @C2H6, @C2H4, @C3H6, @C3H8, @BUTAD, @C4S, @C4H10, @BZ, @PYGAS, @PYOIL, @INERT);

END;