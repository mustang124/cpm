﻿CREATE TABLE [ante].[EdcExponents] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [EdcId]          VARCHAR (42)       NOT NULL,
    [Value]          REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EdcExponents_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EdcExponents_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EdcExponents_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EdcExponents_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EdcExponents] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EdcId] ASC),
    CONSTRAINT [FK_EdcExponents_Edc_LookUp] FOREIGN KEY ([EdcId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_EdcExponents_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_AnteEdcExponents_u]
	ON [ante].[EdcExponents]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EdcExponents]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EdcExponents].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[EdcExponents].EdcId			= INSERTED.EdcId;

END;