﻿CREATE TABLE [ante].[SimNormalizationDistillation] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [DistillationID] VARCHAR (4)        NOT NULL,
    [Density_Min_SG] REAL               NOT NULL,
    [Density_Max_SG] REAL               NOT NULL,
    [iFact]          REAL               NULL,
    [Slope]          REAL               NOT NULL,
    [Inter]          REAL               NOT NULL,
    [Temp_Min_C]     REAL               NULL,
    [Temp_Max_C]     REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_SimNormalizationDistillation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_SimNormalizationDistillation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_SimNormalizationDistillation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_SimNormalizationDistillation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SimNormalizationDistillation] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC, [DistillationID] ASC, [Density_Min_SG] ASC),
    CONSTRAINT [CR_SimNormalizationDistillation_Density_Max_SG] CHECK ([Density_Max_SG]>=(0.0)),
    CONSTRAINT [CR_SimNormalizationDistillation_Density_Min_SG] CHECK ([Density_Min_SG]>=(0.0)),
    CONSTRAINT [CR_SimNormalizationDistillation_Temp_Max_SG] CHECK ([Temp_Max_C]>=(0.0)),
    CONSTRAINT [CR_SimNormalizationDistillation_Temp_Min_SG] CHECK ([Temp_Min_C]>=(0.0)),
    CONSTRAINT [CV_SimNormalizationDistillation_Density] CHECK ([Density_Max_SG]>=[Density_Min_SG]),
    CONSTRAINT [CV_SimNormalizationDistillation_Temp] CHECK ([Temp_Max_C]>=[Temp_Min_C]),
    CONSTRAINT [FK_SimNormalizationDistillation_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_SimNormalizationDistillation_FeedStockDistillation_LookUp] FOREIGN KEY ([DistillationID]) REFERENCES [dim].[FeedStockDistillation_LookUp] ([DistillationId]),
    CONSTRAINT [FK_SimNormalizationDistillation_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_SimNormalizationDistillation_u]
	ON [ante].[SimNormalizationDistillation]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[SimNormalizationDistillation]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[SimNormalizationDistillation].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[SimNormalizationDistillation].StreamId			= INSERTED.StreamId
		AND	[ante].[SimNormalizationDistillation].DistillationID	= INSERTED.DistillationID
		AND	[ante].[SimNormalizationDistillation].Density_Min_SG	= INSERTED.Density_Min_SG;

END;