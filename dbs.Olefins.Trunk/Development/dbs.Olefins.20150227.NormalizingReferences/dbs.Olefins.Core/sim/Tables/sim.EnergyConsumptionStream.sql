﻿CREATE TABLE [sim].[EnergyConsumptionStream] (
    [QueueId]           BIGINT             NOT NULL,
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [SimModelId]        VARCHAR (12)       NOT NULL,
    [OpCondId]          VARCHAR (12)       NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [RecycleId]         INT                NOT NULL,
    [ErrorID]           VARCHAR (12)       NOT NULL,
    [FurnaceTypeId]     VARCHAR (12)       NULL,
    [SimEnergy_kCalkg]  REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyConsumptionStream_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_EnergyConsumptionStream_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_EnergyConsumptionStream_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_EnergyConsumptionStream_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyConsumptionStream] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [RecycleId] ASC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_EnergyConsumptionStream_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_EnergyConsumptionStream_SimEnergy_kCalC2H4] CHECK ([SimEnergy_kCalkg]>=(0.0)),
    CONSTRAINT [FK_EnergyConsumptionStream_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyConsumptionStream_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_EnergyConsumptionStream_FurnaceType_LookUp] FOREIGN KEY ([SimModelId], [FurnaceTypeId]) REFERENCES [dim].[SimFurnaceType_LookUp] ([ModelId], [FurnaceTypeId]),
    CONSTRAINT [FK_EnergyConsumptionStream_SimErrorMessages_LookUp] FOREIGN KEY ([SimModelId], [ErrorID]) REFERENCES [dim].[SimErrorMessages_LookUp] ([ModelId], [ErrorId]),
    CONSTRAINT [FK_EnergyConsumptionStream_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_EnergyConsumptionStream_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_EnergyConsumptionStream_SimQueue] FOREIGN KEY ([QueueId]) REFERENCES [q].[Simulation] ([QueueId]),
    CONSTRAINT [FK_EnergyConsumptionStream_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [sim].[t_EnergyConsumptionStream_u]
	ON [sim].[EnergyConsumptionStream]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [sim].[EnergyConsumptionStream]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[sim].[EnergyConsumptionStream].FactorSetId			= INSERTED.FactorSetId
		AND	[sim].[EnergyConsumptionStream].Refnum				= INSERTED.Refnum
		AND	[sim].[EnergyConsumptionStream].CalDateKey			= INSERTED.CalDateKey
		AND	[sim].[EnergyConsumptionStream].SimModelId			= INSERTED.SimModelId
		AND	[sim].[EnergyConsumptionStream].OpCondId			= INSERTED.OpCondId
		AND	[sim].[EnergyConsumptionStream].StreamId			= INSERTED.StreamId
		AND	[sim].[EnergyConsumptionStream].StreamDescription	= INSERTED.StreamDescription
		AND	[sim].[EnergyConsumptionStream].RecycleId			= INSERTED.RecycleId;

END;