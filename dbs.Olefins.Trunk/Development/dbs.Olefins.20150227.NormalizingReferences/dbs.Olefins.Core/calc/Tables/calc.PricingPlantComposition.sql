﻿CREATE TABLE [calc].[PricingPlantComposition] (
    [FactorSetId]              VARCHAR (12)       NOT NULL,
    [ScenarioId]               VARCHAR (42)       NOT NULL,
    [Refnum]                   VARCHAR (25)       NOT NULL,
    [CalDateKey]               INT                NOT NULL,
    [CurrencyRpt]              VARCHAR (4)        NOT NULL,
    [AccountId]                VARCHAR (42)       NULL,
    [StreamId]                 VARCHAR (42)       NOT NULL,
    [StreamDescription]        NVARCHAR (256)     NOT NULL,
    [ComponentId]              VARCHAR (42)       NOT NULL,
    [Component_kMT]            REAL               NOT NULL,
    [Component_WtPcnt]         REAL               NOT NULL,
    [Client_Amount_Cur]        REAL               NULL,
    [Region_Amount_Cur]        REAL               NULL,
    [Country_Amount_Cur]       REAL               NULL,
    [Location_Amount_Cur]      REAL               NULL,
    [Composition_Amount_Cur]   REAL               NULL,
    [Energy_Amount_Cur]        REAL               NULL,
    [Adj_CompPurity_Cur]       REAL               NULL,
    [Adj_NonStdStream_Coeff]   REAL               NULL,
    [Adj_Logistics_Cur]        REAL               NULL,
    [Supersede_Amount_Cur]     REAL               NULL,
    [_Calc_Amount_Cur]         AS                 (CONVERT([real],isnull([Supersede_Amount_Cur],isnull([Adj_NonStdStream_Coeff],(1.0))*(isnull([Adj_Logistics_Cur],(0.0))+isnull([Energy_Amount_Cur],isnull([Composition_Amount_Cur]+isnull([Adj_CompPurity_Cur],(0.0)),isnull([Location_Amount_Cur],isnull([Country_Amount_Cur],isnull([Region_Amount_Cur],[Client_Amount_Cur]))))))),(1))) PERSISTED NOT NULL,
    [_Stream_Value_Cur]        AS                 (CONVERT([real],[Component_kMT]*isnull([Supersede_Amount_Cur],isnull([Adj_NonStdStream_Coeff],(1.0))*(isnull([Adj_Logistics_Cur],(0.0))+isnull([Energy_Amount_Cur],isnull([Composition_Amount_Cur]+isnull([Adj_CompPurity_Cur],(0.0)),isnull([Location_Amount_Cur],isnull([Country_Amount_Cur],isnull([Region_Amount_Cur],[Client_Amount_Cur]))))))),(1))) PERSISTED NOT NULL,
    [_Calc_Margin_Amount_Cur]  AS                 (CONVERT([real],isnull([Adj_NonStdStream_Coeff],(1.0))*(isnull([Adj_Logistics_Cur],(0.0))+isnull([Energy_Amount_Cur],isnull([Composition_Amount_Cur]+isnull([Adj_CompPurity_Cur],(0.0)),isnull([Location_Amount_Cur],isnull([Country_Amount_Cur],isnull([Region_Amount_Cur],[Client_Amount_Cur])))))),(1))) PERSISTED,
    [_Stream_Margin_Value_Cur] AS                 (CONVERT([real],([Component_kMT]*isnull([Adj_NonStdStream_Coeff],(1.0)))*(isnull([Adj_Logistics_Cur],(0.0))+isnull([Energy_Amount_Cur],isnull([Composition_Amount_Cur]+isnull([Adj_CompPurity_Cur],(0.0)),isnull([Location_Amount_Cur],isnull([Country_Amount_Cur],isnull([Region_Amount_Cur],[Client_Amount_Cur])))))),(1))) PERSISTED,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPlantComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_PricingPlantComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_PricingPlantComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_PricingPlantComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PricingPlantComposition] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ScenarioId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [StreamDescription] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingPlantComposition_Client_Amount_Cur] CHECK ([Client_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantComposition_Component_kMT] CHECK ([Component_kMT]>=(0.0)),
    CONSTRAINT [CR_PricingPlantComposition_Composition_Amount_Cur] CHECK ([Composition_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantComposition_Country_Amount_Cur] CHECK ([Country_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantComposition_Location_Amount_Cur] CHECK ([Location_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantComposition_Region_Amount_Cur] CHECK ([Region_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantComposition_Supersede_Amount_Cur] CHECK ([Supersede_Amount_Cur]>=(0.0)),
    CONSTRAINT [CV_PricingPlantComposition_Amount_Cur] CHECK ([Composition_Amount_Cur] IS NOT NULL OR [Location_Amount_Cur] IS NOT NULL OR [Region_Amount_Cur] IS NOT NULL OR [Country_Amount_Cur] IS NOT NULL OR [Client_Amount_Cur] IS NOT NULL OR [Energy_Amount_Cur] IS NOT NULL OR [Supersede_Amount_Cur] IS NOT NULL),
    CONSTRAINT [FK_calc_PricingPlantComposition_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_calc_PricingPlantComposition_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PricingPlantComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_PricingPlantComposition_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_PricingPlantComposition_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PricingPlantComposition_Scenarios] FOREIGN KEY ([FactorSetId], [ScenarioId]) REFERENCES [ante].[PricingScenarioConfig] ([FactorSetId], [ScenarioId]),
    CONSTRAINT [FK_calc_PricingPlantComposition_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_PricingPlantComposition_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_PricingPlantComposition_Margin]
    ON [calc].[PricingPlantComposition]([Refnum] ASC)
    INCLUDE([FactorSetId], [CalDateKey], [CurrencyRpt], [StreamId], [ComponentId], [Component_WtPcnt], [_Stream_Margin_Value_Cur]);


GO
CREATE TRIGGER [calc].[t_PricingPlantComnposition_u]
	ON [calc].[PricingPlantComposition]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[PricingPlantComposition]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[PricingPlantComposition].FactorSetId			= INSERTED.FactorSetId
		AND calc.[PricingPlantComposition].ScenarioId			= INSERTED.ScenarioId
		AND calc.[PricingPlantComposition].Refnum				= INSERTED.Refnum
		AND calc.[PricingPlantComposition].CalDateKey			= INSERTED.CalDateKey
		AND calc.[PricingPlantComposition].CurrencyRpt			= INSERTED.CurrencyRpt
		AND calc.[PricingPlantComposition].StreamId				= INSERTED.StreamId
		AND calc.[PricingPlantComposition].StreamDescription	= INSERTED.StreamDescription
		AND calc.[PricingPlantComposition].ComponentId			= INSERTED.ComponentId;

END;