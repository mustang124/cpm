﻿CREATE PROCEDURE [calc].[Insert_Edc_FeedPrep]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[Edc]([FactorSetId], [Refnum], [CalDateKey], [EdcId], [EdcDetail], [kEdc])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			k.[EdcId],
			k.[EdcId],
			fff.[FeedRate_kBsd] * k.[Value]
		FROM @fpl										fpl
		INNER JOIN [fact].[FacilitiesFeedFrac]			fff
			ON	fff.[Refnum]		= fpl.[Refnum]
			AND	fff.[FacilityId]	= 'FracFeed'
		INNER JOIN (VALUES
			('Condensate',	'TowerNAPS'),
			('EPMix',		'TowerDeethanizer'),
			('LPG',			'TowerDepropanizer'),
			('Naphtha',		'TowerNAPS')
			)												map ([StreamId], [EdcId])
			ON	map.[StreamId] = fff.[StreamId]
		INNER JOIN [ante].[EdcCoefficients]				k
			ON	k.[FactorSetId]		= fpl.[FactorSetId]
			AND	k.[EdcId]			= map.[EdcId]
		WHERE	fpl.[CalQtr] = 4
			AND	fpl.[FactorSet_QtrDateKey]	> 20130000
			AND	fff.[FeedRate_kBsd]	IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;