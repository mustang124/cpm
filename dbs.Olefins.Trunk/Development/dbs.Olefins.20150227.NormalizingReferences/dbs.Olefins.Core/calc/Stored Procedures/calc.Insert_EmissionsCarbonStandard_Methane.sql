﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonStandard_Methane]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.EmissionsCarbonStandard(FactorSetId, Refnum, SimModelId, CalDateKey, EmissionsId, Quantity_kMT, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			'PYPS',
			fpl.Plant_AnnDateKey,
			'Methane'					[EmissionsId],
			ISNULL(dp.Component_Extrap_kMT, 0.0) + ISNULL(dp.Component_Supp_kMT, 0.0),
			21.0,
			(ISNULL(dp.Component_Extrap_kMT, 0.0) + ISNULL(dp.Component_Supp_kMT, 0.0)) * 21.0
		FROM @fpl												fpl
		INNER JOIN calc.CompositionYieldPlant					dp
			ON	dp.FactorSetId		= fpl.FactorSetId
			AND dp.Refnum			= fpl.Refnum
			AND dp.CalDateKey		= fpl.Plant_QtrDateKey
			AND	dp.SimModelId		= 'PYPS'
			AND dp.OpCondId			= 'OS25'
			AND dp.ComponentId		= 'C2H4'
		INNER JOIN calc.PeerGroupFeedClass						fc
			ON	fc.FactorSetId		= fpl.FactorSetId
			AND	fc.Refnum			= fpl.Refnum
			AND	fc.CalDateKey		= fpl.Plant_AnnDateKey
		LEFT OUTER JOIN [super].[PeerGroupFeedClass]			sf
			ON	sf.[FactorSetId]	= fpl.[FactorSetId]
			AND	sf.[Refnum]			= fpl.[Refnum]
			AND	sf.[CalDateKey]		= fpl.[Plant_AnnDateKey]
		INNER JOIN ante.EmissionsFactorFlareLoss				ef
			ON	ef.FactorSetId		= fpl.FactorSetId
			AND	ef.FeedClassID		= COALESCE(sf.PeerGroup, fc.PeerGroup);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;