﻿CREATE PROCEDURE calc.Insert_Furnaces
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;
	
	BEGIN TRY

		DECLARE @InflationYearLimit		FLOAT = 12.0;

		INSERT INTO calc.Furnaces(FactorSetId, Refnum, CalDateKey, StreamId, FurnID,
			CurrencyRpt,
			Retubed_Year, RunLength_Days, AverageDecokesPerYear, DownTime_Days,
			Contract_Hrs, ContractOcc_Hrs, ContractMps_Hrs,
			ContractLabor_Cur, MaintMajor_Cur,
			[CompanyLabor_Hrs], [CompanyLabor_Cur],
			RetubeInterval_Mnths, RetubeInterval_Years, MaintRetubeMatl_Cur, MaintRetubeLabor_Cur,
			InflAdjAnn,
			FuelTypeID, FuelCons_kMT, StackOxygen_Pcnt, ArchDraft_H20, StackTemp_C, CrossTemp_C,

			[FeedCap_MTd],
			[OnStream_Days],
			[Availability_Pcnt],
			[AdjOnStream_Pcnt],
			[ActCapacity_kMT],
			[AdjUtilization_Pcnt],
			[SlowDown_Pcnt]
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			p.StreamId,
			p.FurnID,
		
			tsq.CurrencyRpt,
		
			p.Retubed_Year,
			f.RunLength_Days,
			f._AverageDecokesPerYear,
			d.DownTime_Days,
		
			h.Contract_Hrs,
			hOcc.Contract_Hrs,
			hMps.Contract_Hrs,
		
			c.Amount_Cur				[ContractLabor_Cur],
			o.MaintMajor				[MaintMajor_Cur],
		
			compOccTot.Company_Hrs - COALESCE(compOccMaintTa.Company_Hrs, 0.0),
			compCur.Amount_Cur,

			p.RetubeInterval_Mnths,
			p._RetubeInterval_Years		[RetubeInterval_Years],
			o.MaintRetubeMatl			[MaintRetubeMatl_Cur],
			o.MaintRetubeLabor			[MaintRetubeLabor_Cur],
		
			[$(DbGlobal)].[dbo].[ExchangeRate](tsq.CurrencyRpt, 'USD',
				CASE WHEN p.Retubed_Year < tsq.StudyYear - 12 THEN tsq.StudyYear - 12 ELSE p.Retubed_Year END,
				tsq.StudyYear, tsq.CurrencyFcn),

			p.FuelTypeID,
			p.FuelCons_kMT,
			p.StackOxygen_Pcnt,
			p.ArchDraft_H20,
			p.StackTemp_C,
			p.CrossTemp_C,
		
			p.FeedCap_MTd,
			dtStream.OnStream_Days,
			[Availability_Pcnt] = (dtStream.OnStream_Days - dtMaint.DownTime_Days) / dtStream.OnStream_Days * 100.0,
			[AdjOnStream_Pcnt] = (dtStream.OnStream_Days - dtPyro.DownTime_Days) / dtStream.OnStream_Days * 100.0,
			[ActCapacity_kMT] = p.FeedCap_MTd * dtStream.OnStream_Days,
			[AdjUtilization_Pcnt] = p.FeedQty_kMT / p.FeedCap_MTd / dtStream.OnStream_Days * 100.0,
			[SlowDown_Pcnt] = ((dtStream.OnStream_Days - dtPyro.DownTime_Days) / dtStream.OnStream_Days  -
					p.FeedQty_kMT / p.FeedCap_MTd / dtStream.OnStream_Days) * 100.0

		FROM @fpl													tsq
		INNER JOIN fact.ReliabilityPyroFurn							p
			ON	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
		LEFT OUTER JOIN fact.ReliabilityPyroFurnFeed				f
			ON	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
			AND	f.StreamId = etl.PyroFurnFeed(p.StreamId)
		LEFT OUTER JOIN fact.ReliabilityPyroFurnDownTime			d
			ON	d.Refnum = tsq.Refnum
			AND	d.CalDateKey = tsq.Plant_QtrDateKey
			AND	d.FurnID = p.FurnID
			AND d.OppLossId = 'MaintDecoke'

		LEFT OUTER JOIN (
			SELECT
				dt.[Refnum],
				dt.[CalDateKey],
				dt.[FurnID],
				[OnStream_Days] = 365.0 - SUM(dt.DownTime_Days)
			FROM fact.ReliabilityPyroFurnDownTime dt
			WHERE	dt.OppLossId IN ('PlantOutages', 'TurnAround')
			GROUP BY
				dt.[Refnum],
				dt.[CalDateKey],
				dt.[FurnID]
			)	dtStream
			ON	dtStream.Refnum			= tsq.Refnum
			AND	dtStream.CalDateKey		= tsq.Plant_QtrDateKey
			AND	dtStream.FurnID			= p.FurnID

		LEFT OUTER JOIN fact.ReliabilityPyroFurnDownTimeAggregate			dtMaint
			ON	dtMaint.FactorSetId		= tsq.FactorSetId
			AND	dtMaint.Refnum			= tsq.Refnum
			AND	dtMaint.CalDateKey		= tsq.Plant_QtrDateKey
			AND	dtMaint.FurnID			= p.FurnID
			AND dtMaint.OppLossId		= 'Maint'
		LEFT OUTER JOIN fact.ReliabilityPyroFurnDownTimeAggregate			dtPyro
			ON	dtPyro.FactorSetId		= tsq.FactorSetId
			AND	dtPyro.Refnum			= tsq.Refnum
			AND	dtPyro.CalDateKey		= tsq.Plant_QtrDateKey
			AND	dtPyro.FurnID			= p.FurnID
			AND dtPyro.OppLossId		= 'PyroFurnDT'
			
		INNER JOIN (
			SELECT
				o.Refnum,
				o.CalDateKey,
				o.CurrencyRpt,
				SUM(o.Amount_Cur)	[Amount_Cur]
			FROM fact.OpEx o
			WHERE o.AccountId IN ('OCCSal', 'OCCBen')
			GROUP BY
				o.Refnum,
				o.CalDateKey,
				o.CurrencyRpt
			)														compCur
			ON	compCur.Refnum = tsq.Refnum
			AND	compCur.CalDateKey = tsq.Plant_QtrDateKey
			AND	compCur.CurrencyRpt = tsq.CurrencyRpt

		

		LEFT OUTER JOIN fact.PersAggregate							compOccTot
			ON	compOccTot.FactorSetId = tsq.FactorSetId
			AND	compOccTot.Refnum = tsq.Refnum
			AND	compOccTot.CalDateKey = tsq.Plant_QtrDateKey
			AND	compOccTot.PersId = 'OccSubTotal'

		LEFT OUTER JOIN fact.PersAggregate							compOccMaintTa
			ON	compOccMaintTa.FactorSetId = tsq.FactorSetId
			AND	compOccMaintTa.Refnum = tsq.Refnum
			AND	compOccMaintTa.CalDateKey = tsq.Plant_QtrDateKey
			AND	compOccMaintTa.PersId = 'OccMaintTa'

		INNER JOIN (
			SELECT
				o.Refnum,
				o.CalDateKey,
				o.FurnID,
				o.AccountId,
				o.CurrencyRpt,
				o.Amount_Cur
			FROM fact.ReliabilityPyroFurnOpEx o
			WHERE o.AccountId IN ('MaintMajor', 'MaintRetube', 'MaintRetubeLabor', 'MaintRetubeMatl')
			)														u
			PIVOT(
			MAX(u.Amount_Cur) FOR u.AccountId IN(
				MaintMajor,
				MaintRetube,
				MaintRetubeLabor,
				MaintRetubeMatl
				)
			)														o
			ON	o.Refnum = tsq.Refnum
			AND	o.CalDateKey = tsq.Plant_QtrDateKey
			AND	o.FurnID = p.FurnID
		INNER JOIN (
			SELECT
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey,
				SUM(CASE WHEN p.PersId IN ('OccMaint', 'MpsMaint')
					THEN + p.Contract_Hrs
					ELSE - p.Contract_Hrs
					END)									[Contract_Hrs]
			FROM fact.PersAggregate p
			WHERE	p.PersId IN ('OccMaint', 'MpsMaint', 'OccMaintTa', 'MpsMaintTa')
			GROUP BY
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey
			)														h
			ON	h.FactorSetId = tsq.FactorSetId
			AND	h.Refnum = tsq.Refnum
			AND	h.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN (
			SELECT
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey,
				SUM(CASE WHEN p.PersId IN ('OccMaint')
					THEN + p.Contract_Hrs
					ELSE - p.Contract_Hrs
					END)									[Contract_Hrs]
			FROM fact.PersAggregate p
			WHERE	p.PersId IN ('OccMaint', 'OccMaintTa')
			GROUP BY
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey
			)														hOcc
			ON	hOcc.FactorSetId = tsq.FactorSetId
			AND	hOcc.Refnum = tsq.Refnum
			AND	hOcc.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN (
			SELECT
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey,
				SUM(CASE WHEN p.PersId IN ('MpsMaint')
					THEN + p.Contract_Hrs
					ELSE - p.Contract_Hrs
					END)									[Contract_Hrs]
			FROM fact.PersAggregate p
			WHERE	p.PersId IN ('MpsMaint', 'MpsMaintTa')
			GROUP BY
				p.FactorSetId,
				p.Refnum,
				p.CalDateKey
			)														hMps
			ON	hMps.FactorSetId = tsq.FactorSetId
			AND	hMps.Refnum = tsq.Refnum
			AND	hMps.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN (
			SELECT
				o.Refnum,
				o.CalDateKey,
				o.AccountId,
				o.CurrencyRpt,
				o.Amount_Cur
			FROM fact.OpEx o
			WHERE	o.AccountId = 'MaintContractLabor'
			)														c
			ON	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.CurrencyRpt = tsq.CurrencyRpt;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;