﻿CREATE PROCEDURE calc.Insert_PersCost
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY
		
		INSERT INTO calc.PersCost(
			FactorSetId, Refnum, CalDateKey, AccountId,
			CurrencyRpt,
			TaLabor_Cur,
			Amount_Cur
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			o.AccountId,
			tsq.CurrencyRpt,
			CASE WHEN m.TA_Cur <> 0 AND o.AccountId = 'STTaExp' THEN c.TaAdjExRateAnn_TurnAroundCost_kCur * m.TaLabor_Cur / m.TA_Cur END,
			o.Amount_Cur
		FROM @fpl											tsq

		INNER JOIN fact.OpExAggregate						o	WITH (NOEXPAND)
			ON	o.FactorSetId = tsq.FactorSetId
			AND	o.Refnum = tsq.Refnum
			AND	o.CalDateKey = tsq.Plant_QtrDateKey
			AND	o.AccountId IN ('EmployeeBenefits', 'SalariesWages', 'MaintContractLabor', 'STTaExp', 'ContractTechSvc', 'ContractServOth')
	
		LEFT OUTER JOIN fact.MaintAggregate					m
			ON	m.FactorSetId = tsq.FactorSetId
			AND	m.Refnum = tsq.Refnum
			AND	m.CalDateKey = tsq.Plant_QtrDateKey
			AND	m.CurrencyRpt = tsq.CurrencyRpt
			AND	m.FacilityId  = 'TotUnitCnt'
		
		LEFT OUTER JOIN calc.TaAdjCapacityAggregate			c
			ON	c.FactorSetId = tsq.FactorSetId
			AND	c.Refnum = tsq.Refnum
			AND	c.CalDateKey = tsq.Plant_QtrDateKey
			AND	c.CurrencyRpt = tsq.CurrencyRpt

		WHERE	tsq.CalQtr = 4;
			
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;