﻿CREATE PROCEDURE calc.Insert_TaAdjReliability
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.TaAdjReliability(FactorSetId, Refnum, CalDateKey, SchedId, TrainId,
			TurnAround_Date, DataYear, CurrencyRpt, CurrencyFcn,
			TurnAroundCost_MCur, Ann_TurnAroundCost_kCur, Ann_TurnAround_ManHrs, CostPerManHour,
			InflAdj
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			r.SchedId,
			r.TrainId,

			r.TurnAround_Date,
			tsq.DataYear,
			tsq.CurrencyRpt,
			tsq.CurrencyFcn,

			r.TurnAroundCost_MCur,
			r._Ann_TurnAroundCost_kCur,
			r._Ann_TurnAround_ManHrs,
			r._CostPerManHour_kCur,

			[$(DbGlobal)].[dbo].[ExchangeRate](tsq.CurrencyRpt, 'USD',
				CASE WHEN YEAR(r.TurnAround_Date) < tsq.StudyYear - 12 THEN tsq.StudyYear - 12 ELSE YEAR(r.TurnAround_Date) END,
				tsq.StudyYear, tsq.CurrencyFcn)				[TaAdjExRate]


		FROM @fpl							tsq
		INNER JOIN fact.ReliabilityTA		r
			ON	r.Refnum = tsq.Refnum
			AND	r.CalDateKey = tsq.Plant_QtrDateKey
			AND	r.CurrencyRpt = tsq.CurrencyRpt
			AND r.SchedId = 'A'
		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;