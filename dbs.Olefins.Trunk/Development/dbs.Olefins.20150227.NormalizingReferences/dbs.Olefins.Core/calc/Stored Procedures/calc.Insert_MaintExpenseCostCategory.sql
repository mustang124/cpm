﻿CREATE PROCEDURE [calc].[Insert_MaintExpenseCostCategory]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.MaintExpenseCostCategory (MaintExpenseLaborMatl)';
		PRINT @ProcedureDesc;

		INSERT INTO [calc].[MaintExpenseCostCategory]([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [AccountId], [Amount_Cur])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[CurrencyRpt],
			e.[AccountId],
			e.[Amount_Cur]
		FROM @fpl									fpl
		INNER JOIN [calc].[MaintExpenseLaborMatl]	e
			ON	e.[FactorSetId] = fpl.[FactorSetId]
			AND	e.[Refnum]		= fpl.[Refnum]
			AND	e.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	e.[CurrencyRpt] = fpl.[CurrencyRpt]
		WHERE fpl.[CalQtr] = 4;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.MaintExpenseCostCategory (MaintExpenseMpsSwb)';
		PRINT @ProcedureDesc;

		INSERT INTO [calc].[MaintExpenseCostCategory]([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [AccountId], [Amount_Cur])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[CurrencyRpt],
			e.[AccountId],
			e.[Amount_Cur]
		FROM @fpl									fpl
		INNER JOIN [calc].[MaintExpenseMpsSwb]		e
			ON	e.[FactorSetId] = fpl.[FactorSetId]
			AND	e.[Refnum]		= fpl.[Refnum]
			AND	e.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	e.[CurrencyRpt] = fpl.[CurrencyRpt]
		WHERE fpl.[CalQtr] = 4;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.MaintExpenseCostCategory (MaintExpenseOccSwb)';
		PRINT @ProcedureDesc;

		INSERT INTO [calc].[MaintExpenseCostCategory]([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [AccountId], [Amount_Cur])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[CurrencyRpt],
			e.[AccountId],
			e.[Amount_Cur]
		FROM @fpl								fpl
		INNER JOIN [calc].[MaintExpenseOccSwb]		e
			ON	e.[FactorSetId] = fpl.[FactorSetId]
			AND	e.[Refnum]		= fpl.[Refnum]
			AND	e.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	e.[CurrencyRpt] = fpl.[CurrencyRpt]
		WHERE fpl.[CalQtr] = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END