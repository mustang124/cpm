﻿CREATE PROCEDURE [calc].[Insert_Edc_ElecGen]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey]	[CalDateKey],
			k.[EdcId],
			k.[EdcId],
			f.[ElecGen_MW] * k.[Value]
		FROM @fpl										fpl
		INNER JOIN [fact].[FacilitiesMisc]				f
			ON	f.[Refnum]			= fpl.[Refnum]
			AND	f.[CalDateKey]		= fpl.[Plant_QtrDateKey]
		INNER JOIN [ante].[EdcCoefficients]				k
			ON	k.[FactorSetId]		= fpl.[FactorSetId]
			AND	k.[EdcId]			= 'ElecGen'
		WHERE	fpl.[CalQtr]		= 4
			AND	fpl.[FactorSet_QtrDateKey]	> 20130000
			AND	f.[ElecGen_MW]		IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;