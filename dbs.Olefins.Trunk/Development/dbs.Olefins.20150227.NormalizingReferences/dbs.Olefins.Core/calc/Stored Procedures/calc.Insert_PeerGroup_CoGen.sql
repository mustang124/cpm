﻿CREATE PROCEDURE [calc].[Insert_PeerGroup_CoGen]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PeerGroupCoGen(FactorSetId, Refnum, CalDateKey, PeerGroup, [ElecGen_MW], [LowerLimit_MW])
		SELECT
			l.FactorSetId,
			fm.Refnum,
			fm.CalDateKey,
			CASE WHEN fm.ElecGen_MW >= l.LowerLimit_MW THEN 'Cogen' ELSE 'NonCogen' END	[PeerGroup],
			fm.ElecGen_MW,
			l.LowerLimit_MW
		FROM @fpl							fpl
		INNER JOIN ante.[PeerGroupCogen]	l
			ON	l.FactorSetId = fpl.FactorSetId
		INNER JOIN fact.FacilitiesMisc		fm
			ON	fm.Refnum = fpl.Refnum
			AND	fm.CalDateKey = fpl.Plant_QtrDateKey
		WHERE	fpl.CalQtr = 4

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;