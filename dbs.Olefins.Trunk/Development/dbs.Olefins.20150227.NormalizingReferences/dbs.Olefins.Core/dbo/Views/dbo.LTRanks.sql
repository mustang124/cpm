﻿

CREATE View [dbo].[LTRanks] as
SELECT  Refnum = LEFT(k.StudyYear, 2) + Rtrim(k.Refnum)
, SourceDB = 'OlefinsLegacy'
, t.StudyYear
, HVCProd_Kmt = p.HVChemDiv
, MCI_Pcnt = 100.0 - mci.Percentile
, RI_Pcnt = ri.Percentile
FROM [$(DbOlefinsLegacy)].dbo._RankView ri 
INNER JOIN [$(DbOlefinsLegacy)].dbo._RankView mci ON mci.Refnum = ri.Refnum AND mci.Variable = 'Maintenance Cost Index' AND mci.ListName = ri.ListName AND mci.BreakCondition = ri.BreakCondition AND mci.BreakValue = ri.BreakValue
INNER JOIN [$(DbOlefinsLegacy)].dbo.TSort t ON t.Refnum = ri.Refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.TSort k ON k.Refnumber = t.Refnumber
INNER JOIN [$(DbOlefinsLegacy)].dbo.Production p on p.Refnum=ri.refnum
WHERE ri.Variable = 'Reliability Indicator'
AND ri.BreakCondition = 'Total List' 
AND ri.BreakValue = 'ALL'
AND ri.ListName IN ('97pch', '99pch+Late', '01PCH+LATE', '03PCH+Late', '05PCH+LATE')

UNION

SELECT k.Refnum
, SourceDB = 'Olefins'
, t.StudyYear
, HVCProd_kMT = g.HVCProd_kMT
, MCI_Pcnt = 100.0 - mci.Percentile
, RI_Pcnt = ri.Percentile
FROM Olefins.Ranking.RankView ri 
INNER JOIN Olefins.Ranking.RankView mci ON mci.Refnum = ri.Refnum AND mci.RankVariable = 'MaintIndexRV' AND mci.ListName = ri.ListName AND mci.RankBreak = ri.RankBreak AND mci.BreakValue = ri.BreakValue
INNER JOIN Olefins.dbo.TSort t ON t.Refnum = ri.Refnum
INNER JOIN Olefins.dbo.TSort k ON k.Refnumber = t.Refnumber
INNER JOIN GENSUM g on g.Refnum=ri.Refnum
WHERE ri.RankVariable = 'RelInd'
AND ri.RankBreak = 'TotalList' 
AND ri.BreakValue = 'ALL'
AND ri.ListName IN ('07PCH+LATE', '09PCH+Late', '11PCH+late','13PCH+late')

--select * from cons.RefListLu where ListName = '11PCH+late'
--select * from _rl where ListName like '__PCH+late'
--select * into #dbb from cons.RefListLu where listid = '13PCH'
--update #DBB set ListID = '11PCH+late', Listname = '11PCH+late', listdetail = 'New', parent = '11PCH'
--select * from #dbb
--insert into cons.RefListLu select * from #DBB
--select * from cons.reflist
--insert into cons.RefList (ListID, Refnum, usergroup) select '11PCH+late', Refnum, usergroup from [$(DbOlefinsLegacy)].dbo._rl where ListName = '11pch+late'

--update cons.RefList set Refnum = '20'+rtrim(refnum) 
--where ListID = '11PCH+late'

--select * from Olefins.Ranking.RankView ri 
--where ri.Variable = 'RelInd'
--AND ri.RankBreak = 'TotalList' 
--AND ri.BreakValue = 'ALL'


 


