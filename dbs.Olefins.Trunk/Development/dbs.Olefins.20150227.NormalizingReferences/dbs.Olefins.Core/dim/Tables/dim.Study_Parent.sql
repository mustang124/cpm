﻿CREATE TABLE [dim].[Study_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [StudyId]        VARCHAR (4)         NOT NULL,
    [ParentId]       VARCHAR (4)         NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Study_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Study_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Study_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Study_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Study_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Study_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StudyId] ASC),
    CONSTRAINT [CR_Study_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_Study_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Study_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Study_LookUp] ([StudyId]),
    CONSTRAINT [FK_Study_Parent_LookUp_Study] FOREIGN KEY ([StudyId]) REFERENCES [dim].[Study_LookUp] ([StudyId]),
    CONSTRAINT [FK_Study_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[Study_Parent] ([FactorSetId], [StudyId])
);


GO

CREATE TRIGGER [dim].[t_Study_Parent_u]
ON [dim].[Study_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Study_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Study_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Study_Parent].[StudyId]	= INSERTED.[StudyId];

END;
