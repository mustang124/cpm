﻿CREATE TABLE [dim].[FeedStockDistillationLu] (
    [DistillationID]     VARCHAR (4)         NOT NULL,
    [DistillationName]   NVARCHAR (84)       NOT NULL,
    [DistillationDetail] NVARCHAR (256)      NOT NULL,
    [OSIM]               INT                 NULL,
    [SPSL]               INT                 NULL,
    [PYPS]               INT                 NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedStockDistillationLu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)      CONSTRAINT [DF_FeedStockDistillationLu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)      CONSTRAINT [DF_FeedStockDistillationLu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)      CONSTRAINT [DF_FeedStockDistillationLu_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedStockDistillationLu] PRIMARY KEY CLUSTERED ([DistillationID] ASC),
    CONSTRAINT [CL_FeedStockDistillationLu_DistillationDetail] CHECK (len([DistillationDetail])>(0)),
    CONSTRAINT [CL_FeedStockDistillationLu_DistillationID] CHECK (len([DistillationID])>(0)),
    CONSTRAINT [CL_FeedStockDistillationLu_DistillationName] CHECK (len([DistillationName])>(0)),
    CONSTRAINT [CR_FeedStockDistillationLu_OSIM] CHECK ([OSIM]>(0)),
    CONSTRAINT [CR_FeedStockDistillationLu_PYPS] CHECK ([PYPS]>(0)),
    CONSTRAINT [CR_FeedStockDistillationLu_SPSL] CHECK ([SPSL]>(0)),
    CONSTRAINT [UK_FeedStockDistillationLu_DistillationDetail] UNIQUE NONCLUSTERED ([DistillationDetail] ASC),
    CONSTRAINT [UK_FeedStockDistillationLu_DistillationName] UNIQUE NONCLUSTERED ([DistillationName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FeedStockDistillationLu_u]
	ON [dim].[FeedStockDistillationLu]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FeedStockDistillationLu]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedStockDistillationLu].[DistillationID]		= INSERTED.[DistillationID];
		
END;