﻿CREATE TABLE [dim].[Pers_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [PersId]             VARCHAR (34)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (34)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Pers_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Pers_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Pers_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Pers_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Pers_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Pers_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PersId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Pers_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Pers_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_Pers_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Pers_Bridge_LookUp_Pers] FOREIGN KEY ([PersId]) REFERENCES [dim].[Pers_LookUp] ([PersId]),
    CONSTRAINT [FK_Pers_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [PersId]) REFERENCES [dim].[Pers_Parent] ([FactorSetId], [PersId]),
    CONSTRAINT [FK_Pers_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Pers_Parent] ([FactorSetId], [PersId])
);


GO

CREATE TRIGGER [dim].[t_Pers_Bridge_u]
ON [dim].[Pers_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Pers_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Pers_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Pers_Bridge].[PersId]	= INSERTED.[PersId]
		AND	[dim].[Pers_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;