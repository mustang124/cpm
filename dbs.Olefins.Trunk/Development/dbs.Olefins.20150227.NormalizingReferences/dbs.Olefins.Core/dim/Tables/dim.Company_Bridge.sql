﻿CREATE TABLE [dim].[Company_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [CompanyId]          VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Company_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Company_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Company_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Company_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Company_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Company_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CompanyId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Company_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Company_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_Company_Bridge_FactorSetLu] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Company_Bridge_LookUp_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_Company_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [CompanyId]) REFERENCES [dim].[Company_Parent] ([FactorSetId], [CompanyId]),
    CONSTRAINT [FK_Company_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Company_Parent] ([FactorSetId], [CompanyId])
);


GO

CREATE TRIGGER [dim].[t_Company_Bridge_u]
ON [dim].[Company_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Company_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Company_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Company_Bridge].[CompanyId]		= INSERTED.[CompanyId]
		AND	[dim].[Company_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;