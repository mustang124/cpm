﻿






/*

This view is for long term trends for the XOM custon Followup work

DBB 14Aug14
*/

CREATE view [Cust].[XOMMaintCosts] AS
Select Refnum = LEFT(t.StudyYear, 2) + Rtrim(t.Refnum), 
t.StudyYear,
ByProdBasis,
DivFactor, 
(r.TotReplVal *1000 * MaintCostIndex * (CASE WHEN t.StudyYear <=1993 THEN 100 ELSE 1 END) / 100 / DivFactor) as TotMaintCost,
(r.TotReplVal *1000 * MaintCostIndex  * (CASE WHEN t.StudyYear<=1993 THEN 100 ELSE 1 END) / 100 - f.AnnTAExp) / Divfactor as RoutMaintCost,
 f.AnnTAExp / DivFactor as AnnTAMaintCost
FROM  [$(DbOlefinsLegacy)].dbo.ByProd b INNER JOIN  [$(DbOlefinsLegacy)].dbo.FinancialTot f on f.refnum=b.refnum
INNER JOIN [$(DbOlefinsLegacy)].dbo.MaintInd m on m.refnum=f.refnum
INNER JOIN  [$(DbOlefinsLegacy)].dbo.Replacement r on r.refnum=f.refnum
INNER JOIN  [$(DbOlefinsLegacy)].dbo.TSort t on t.refnum=f.refnum  
where t.StudyYear < 2007

UNION

Select o.Refnum, 
g.StudyYear, 
DataType = CASE d.DataType WHEN 'ETHDiv_MT' THEN 'C2' WHEN 'OLEDiv_MT' THEN 'OLE' WHEN 'HVC_MT' THEN 'HVC' ELSE d.DataType END, 
d.DivisorValue, 
TotMaintCost = m.MaintIndexEDC * g.kEDC / d.DivisorValue,
RoutMaintCost = m.RoutIndexEDC * g.kEDC / d.DivisorValue,
AnnTAMaintCost = m.TAIndexEDC * g.kEDC / d.DivisorValue
from Olefins.dbo.OPEX o JOIN Olefins.dbo.GENSUM g on g.Refnum=o.refnum and o.DataType = 'ADJ'
JOIN Olefins.dbo.Maint m on m.Refnum=g.refnum
Join Olefins.dbo.DivisorsUnPivot d on d.Refnum=o.refnum and d.DataType in ('ETHDiv_MT','OLEDiv_MT','HVC_MT', 'EDC', 'UEDC', 'OLECpby_MT')
where g.Studyyear >= 2007

--Select * from Olefins.dbo.Maint where Refnum = '2011PCH115'
--Select * from cust.XOMMaintCosts where Refnum = '2011PCH115'









