﻿CREATE FUNCTION [etl].[ConvStreamDescription]
(
	@StreamId			VARCHAR(42), 
	@FeedDesc			NVARCHAR (256),
	@ProdDesc1			NVARCHAR (256),
	@ProdDesc2			NVARCHAR (256),
	@MiscDesc			NVARCHAR (256)
)
RETURNS NVARCHAR (256)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Desc NVARCHAR (256) = NULL;
	
	SET @Desc = 
		CASE @StreamId

		WHEN 'OthSpl'			THEN @MiscDesc
		WHEN 'OthProd1'			THEN @ProdDesc1 + ' (Other Prod 1)'
		WHEN 'OthProd2'			THEN @ProdDesc2 + ' (Other Prod 2)'
			
		WHEN 'OthLiqFeed1'		THEN @FeedDesc + ' (Other Feed 1)'
		WHEN 'OthLiqFeed2'		THEN @FeedDesc + ' (Other Feed 2)'
		WHEN 'OthLiqFeed3'		THEN @FeedDesc + ' (Other Feed 3)'
		WHEN 'OthLtFeed'		THEN COALESCE(@MiscDesc, @FeedDesc)

		ELSE @StreamId

		END;

	SET @Desc = ISNULL(@Desc, @StreamId)

	RETURN @Desc;

END