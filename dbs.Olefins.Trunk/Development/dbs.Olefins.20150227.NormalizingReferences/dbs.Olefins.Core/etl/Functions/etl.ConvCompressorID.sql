﻿CREATE FUNCTION [etl].[ConvCompressorID]
(
	@FacilityId	NVARCHAR(5)
)
RETURNS NVARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(42) =
	CASE UPPER(LTRIM(RTRIM(@FacilityId)))
		WHEN 'CG'		THEN 'CompGas'
		WHEN 'C1'		THEN 'CompRefrigMethane'
		WHEN 'C2/C3'	THEN 'CompRefrigOlefins'
		WHEN 'C2'		THEN 'CompRefrigEthylene'
		WHEN 'C3'		THEN 'CompRefrigPropylene'
		WHEN 'GB120'	THEN 'CompOther'
		WHEN 'OTHER'	THEN 'CompOther'
		WHEN 'GB201'	THEN 'CompOther'
	END
		
	RETURN @ResultVar;
	
END