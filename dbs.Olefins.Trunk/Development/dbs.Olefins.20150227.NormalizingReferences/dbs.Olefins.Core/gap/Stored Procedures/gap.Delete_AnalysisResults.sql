﻿CREATE PROCEDURE [gap].[Delete_AnalysisResults]
(
	@GroupId	VARCHAR(25) = NULL,
	@TargetId	VARCHAR(25) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	--BEGIN TRY

		DELETE FROM [gap].[AnalysisResults]
		WHERE	[GroupId]	= @GroupId
			AND	[TargetId]	= @TargetId;

	--END TRY
	--BEGIN CATCH
		
	--	SET @GroupId = 'G:' + @GroupId + '-T:' + @TargetId;
	--	EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

	--	RETURN ERROR_NUMBER();

	--END CATCH;

END;