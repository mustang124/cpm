﻿CREATE TABLE [fact].[FacilitiesHydroTreat] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [FacilityId]        VARCHAR (42)       NOT NULL,
    [HTTypeID]          VARCHAR (2)        NULL,
    [FeedRate_kBsd]     REAL               NULL,
    [FeedPressure_PSIg] REAL               NULL,
    [FeedProcessed_kMT] REAL               NULL,
    [FeedDensity_SG]    REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesHydroTreat_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_FacilitiesHydroTreat_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_FacilitiesHydroTreat_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_FacilitiesHydroTreat_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FacilitiesHydroTreat] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FacilitiesHydroTreat_FeedDensity_SG] CHECK ([FeedDensity_SG]>=(0.0)),
    CONSTRAINT [CR_FacilitiesHydroTreat_FeedPressure_PSIg] CHECK ([FeedPressure_PSIg]>=(0.0)),
    CONSTRAINT [CR_FacilitiesHydroTreat_FeedProcessed_kMT] CHECK ([FeedProcessed_kMT]>=(0.0)),
    CONSTRAINT [CR_FacilitiesHydroTreat_kFeedRate_kBsd] CHECK ([FeedRate_kBsd]>=(0.0)),
    CONSTRAINT [FK_FacilitiesHydroTreat_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FacilitiesHydroTreat_Facilities] FOREIGN KEY ([Refnum], [FacilityId], [CalDateKey]) REFERENCES [fact].[Facilities] ([Refnum], [FacilityId], [CalDateKey]),
    CONSTRAINT [FK_FacilitiesHydroTreat_FacilitiesHydroTreatHydroTreaterType_LookUp] FOREIGN KEY ([HTTypeID]) REFERENCES [dim].[FacilityHydroTreaterType_LookUp] ([HTTypeId]),
    CONSTRAINT [FK_FacilitiesHydroTreat_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesHydroTreat_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FacilitiesHydroTreat_u]
	ON [fact].[FacilitiesHydroTreat]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesHydroTreat]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FacilitiesHydroTreat].Refnum		= INSERTED.Refnum
		AND [fact].[FacilitiesHydroTreat].FacilityId	= INSERTED.FacilityId
		AND [fact].[FacilitiesHydroTreat].CalDateKey	= INSERTED.CalDateKey;

END;