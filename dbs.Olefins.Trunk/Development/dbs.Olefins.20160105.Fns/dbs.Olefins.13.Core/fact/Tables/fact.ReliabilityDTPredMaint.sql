﻿CREATE TABLE [fact].[ReliabilityDTPredMaint] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CauseID]        VARCHAR (42)       NOT NULL,
    [Frequency_Days] REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityDTPredMaint_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTPredMaint_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTPredMaint_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTPredMaint_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityDTPredMaint] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CauseID] ASC),
    CONSTRAINT [FK_ReliabilityDTPredMaint_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityDTPredMaint_ReliabilityPredMaintCause_LookUp] FOREIGN KEY ([CauseID]) REFERENCES [dim].[ReliabilityPredMaintCause_LookUp] ([PredMaintCauseId]),
    CONSTRAINT [FK_ReliabilityDTPredMaint_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityDTPredMaint_u]
	ON [fact].[ReliabilityDTPredMaint]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityDTPredMaint]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityDTPredMaint].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityDTPredMaint].CauseID	= INSERTED.CauseID
		AND [fact].[ReliabilityDTPredMaint].CalDateKey	= INSERTED.CalDateKey;

END;