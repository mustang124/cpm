﻿CREATE TABLE [fact].[ReliabilityCompEventComments] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [EventNo]        TINYINT            NOT NULL,
    [Comments]       VARCHAR (MAX)      NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityCompEventComments_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompEventComments_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompEventComments_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityCompEventComments_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityCompEventComments] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EventNo] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_ReliabilityOppLossNameOther_Comments] CHECK ([Comments]<>''),
    CONSTRAINT [FK_ReliabilityCompEventComments_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityCompEventComments_ReliabilityCompEvent] FOREIGN KEY ([Refnum], [EventNo], [CalDateKey]) REFERENCES [fact].[ReliabilityCompEvents] ([Refnum], [EventNo], [CalDateKey]),
    CONSTRAINT [FK_ReliabilityCompEventComments_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityCompEventComments_u]
	ON [fact].[ReliabilityCompEventComments]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityCompEventComments]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityCompEventComments].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityCompEventComments].EventNo		= INSERTED.EventNo
		AND [fact].[ReliabilityCompEventComments].CalDateKey	= INSERTED.CalDateKey;

END;