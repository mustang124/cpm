﻿CREATE TABLE [fact].[FeedSelModels] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [ModelId]         VARCHAR (42)       NOT NULL,
    [Implemented_Bit] BIT                NULL,
    [InCompany_Bit]   BIT                NULL,
    [LocationID]      CHAR (1)           NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelModels_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelModels_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelModels_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_FeedSelModels_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSelModels] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ModelId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_FeedSelModels_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSelModels_FeedSelModel_LookUp] FOREIGN KEY ([ModelId]) REFERENCES [dim].[FeedSelModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_FeedSelModels_FeedSelModelLocation_LookUp] FOREIGN KEY ([LocationID]) REFERENCES [dim].[FeedSelModelLocation_LookUp] ([LocationId]),
    CONSTRAINT [FK_FeedSelModels_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSelModels_u]
	ON [fact].[FeedSelModels]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSelModels]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSelModels].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSelModels].ModelId		= INSERTED.ModelId
		AND [fact].[FeedSelModels].CalDateKey	= INSERTED.CalDateKey;

END;