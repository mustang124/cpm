﻿CREATE TABLE [fact].[ReliabilityPyroFurnFeed] (
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [StreamId]               VARCHAR (42)       NOT NULL,
    [RunLength_Days]         REAL               NOT NULL,
    [Inj]                    VARCHAR (5)        NULL,
    [PreTreat_Bit]           BIT                NULL,
    [AntiFoul_Bit]           BIT                NULL,
    [_AverageDecokesPerYear] AS                 (CONVERT([real],case when [RunLength_Days]<>(0.0) then (365.0)/[RunLength_Days]  end,(1))) PERSISTED,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityPyroFurnFeed_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnFeed_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnFeed_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnFeed_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityPyroFurnFeed] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_ReliabilityPyroFurnFeed_Inj] CHECK ([Inj]<>''),
    CONSTRAINT [CR_ReliabilityPyroFurnFeed_RunLength_Days] CHECK ([RunLength_Days]>=(0.0)),
    CONSTRAINT [FK_ReliabilityPyroFurnFeed_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityPyroFurnFeed_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReliabilityPyroFurnFeed_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityPyroFurnFeed_u]
	ON [fact].[ReliabilityPyroFurnFeed]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityPyroFurnFeed]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityPyroFurnFeed].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityPyroFurnFeed].StreamId	= INSERTED.StreamId
		AND [fact].[ReliabilityPyroFurnFeed].CalDateKey	= INSERTED.CalDateKey;

END;