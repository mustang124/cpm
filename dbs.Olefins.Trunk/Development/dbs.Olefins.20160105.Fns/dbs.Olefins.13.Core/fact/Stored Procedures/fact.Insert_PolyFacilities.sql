﻿CREATE PROCEDURE [fact].[Insert_PolyFacilities]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.PolyFacilities(
			  Refnum
			, CalDateKey
			, AssetId
		
			, PolyName
			, PolyCity
			, PolyState
		
			, Capacity_kMT
		
			, Stream_MTd
			, ReactorTrains_Count
			, StartUp_Year
			
			, PolymerFamilyID
			, PolymerTechID
			, PrimeResin_Pcnt
			)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvAssetPCH(p.Refnum)
				+ '-T' + CONVERT(VARCHAR(2), p.PlantId)	[AssetId]
			, p.Name
			, p.City
			, p.[State]
			, p.CapKMTA
			, p.CapMTD
			, p.ReactorTrainCnt
			, CASE WHEN p.ReactorStartup > 1900 THEN p.ReactorStartup END
			, etl.ConvPolymerFamily(p.Product)
			, etl.ConvPolymerTech(p.Technology)
			, CASE WHEN p.ReactorResinPct > 100.0 THEN 100.0 ELSE p.ReactorResinPct END
		FROM stgFact.PolyFacilities p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE p.Name <> '0'
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;