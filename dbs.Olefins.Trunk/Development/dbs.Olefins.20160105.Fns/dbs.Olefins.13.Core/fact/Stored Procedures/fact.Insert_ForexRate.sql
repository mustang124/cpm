﻿CREATE PROCEDURE [fact].[Insert_ForexRate]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ForexRate(Refnum, CalDateKey, CurrencyFrom, CurrencyTo, ForexRate)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
			etl.ConvDateKey(t.StudyYear)								[CalDateKey],
		
			CASE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
				WHEN '2005PCH185' THEN 'RON'
				WHEN '2009PCH211' THEN 'TRLK'
				WHEN '2009PCH213' THEN 'RUB'
				WHEN '2009PCH214' THEN 'RUB'
				WHEN '2009PCH217' THEN 'RUB'
				ELSE k.CurrencyId
				END														[CurrencyFcn],
			'USD'														[CurrencyRpt],
		
			o.ForeXRate

		FROM stgFact.TSort						t
		INNER JOIN stgFact.OpEx					o
			ON	o.Refnum = t.Refnum
		INNER JOIN cons.SubscriptionsAssets		s
			ON	s.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
		INNER JOIN cons.Assets					a
			ON	a.AssetId = s.AssetId
		INNER JOIN ante.CountriesCurrencies		k
			ON	k.CountryId = a.CountryId
			AND k.InflationUse = 1
			AND k.CurrencyId NOT IN ('BRCK', 'TRLM', 'RUR', 'ROLK')

		WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			AND	o.ForeXRate IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;