﻿
CREATE PROCEDURE [dbo].[Insert_GapReliabilityOppLoss]
(
	@Refnum			VARCHAR(25),
	@FactorSetId	VARCHAR(12)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [dbo].[GapReliabilityOppLoss]
		(
			[Refnum],
			[DataType],

			[LostProdOpp],
			[PlantRelated],
			[TurnAround],
			[UnPlanned],
			[PRO],
			[OperError],
			[IntFeedInterrupt],
			[FFP],
			[Fouling],
			[Freezing],
			[Plugging],
			[FFPCauses],
			[FFPCause1],
			[FFPCause2],
			[FFPCause3],
			[FFPCause4],
			[FFPCauseBalance],
			[Transition],
			[ProcessOther],
			[ProcessOther1],
			[ProcessOther2],
			[ProcessOther3],
			[ProcessOther4],
			[ProcessOther5],
			[ProcessOtherBalance],
			[FurnaceProcess],
			[AcetyleneConv],
			[EQF],
			[Compressor],
			[OtherRotating],
			[FurnaceMech],
			[Corrosion],
			[ElecDist],
			[ControlSys],
			[OtherFailure],
			[Furnace],
			[UTF],
			[ExtElecFailure],
			[ExtOthUtilFailure],
			[IntElecFailure],
			[IntSteamFailure],
			[IntOthUtilFailure],
			[PlantOutages],
			[PyroFurnDT],
			[StandByDT],
			[Maint],
			[MaintDecoke],
			[MaintTLE],
			[MaintMinor],
			[MaintMajor],
			[NOC],
			[Demand],
			[ExtFeedInterrupt],
			[CapProject],
			[Strikes],
			[FeedMix],
			[Severity],
			[OtherNonOp],
			[OtherNonOp1],
			[OtherNonOp2],
			[OtherNonOp3],
			[OtherNonOp4],
			[OtherNonOp5],
			[OtherNonOpBalance],
			[PeriodDays]
		)
		SELECT
			p.[Refnum],
			p.[DataType],

			p.[LostProdOpp],
			p.[PlantRelated],
			p.[TurnAround],
			p.[UnPlanned],
			p.[PRO],
			p.[OperError],
			p.[IntFeedInterrupt],
			p.[FFP],
			p.[Fouling],
			p.[Freezing],
			p.[Plugging],
			p.[FFPCauses],
			p.[FFPCause1],
			p.[FFPCause2],
			p.[FFPCause3],
			p.[FFPCause4],
			p.[FFPCauseBalance],
			p.[Transition],
			p.[ProcessOther],
			p.[ProcessOther1],
			p.[ProcessOther2],
			p.[ProcessOther3],
			p.[ProcessOther4],
			p.[ProcessOther5],
			p.[ProcessOtherBalance],
			p.[FurnaceProcess],
			p.[AcetyleneConv],
			p.[EQF],
			p.[Compressor],
			p.[OtherRotating],
			p.[FurnaceMech],
			p.[Corrosion],
			p.[ElecDist],
			p.[ControlSys],
			p.[OtherFailure],
			p.[Furnace],
			p.[UTF],
			p.[ExtElecFailure],
			p.[ExtOthUtilFailure],
			p.[IntElecFailure],
			p.[IntSteamFailure],
			p.[IntOthUtilFailure],
			p.[PlantOutages],
			p.[PyroFurnDT],
			p.[StandByDT],
			p.[Maint],
			p.[MaintDecoke],
			p.[MaintTLE],
			p.[MaintMinor],
			p.[MaintMajor],
			p.[NOC],
			p.[Demand],
			p.[ExtFeedInterrupt],
			p.[CapProject],
			p.[Strikes],
			p.[FeedMix],
			p.[Severity],
			p.[OtherNonOp],
			p.[OtherNonOp1],
			p.[OtherNonOp2],
			p.[OtherNonOp3],
			p.[OtherNonOp4],
			p.[OtherNonOp5],
			p.[OtherNonOpBalance],
			p.[PeriodDays]
		FROM (
			SELECT
				t.[Refnum],
				a.[OppLossId],
				c.[DataType],
				c.[Loss]
			FROM [fact].[TSortClient]								t
			LEFT OUTER JOIN [fact].[ReliabilityOppLossAggregate]	a WITH (NOEXPAND)
				ON	a.[Refnum]		= t.[Refnum]
				AND	a.[FactorSetId]	= @FactorSetId
				AND	a.[CurrentYear]	= 1
			CROSS APPLY (VALUES
				('DTLoss_kMT', a.[DownTimeLoss_kMT]),
				('SDLoss_kMT', a.[SlowDownLoss_kMT]),
				('TotLoss_kMT', a.[TotLoss_kMT]),
				('DTLoss_MT', a.[DownTimeLoss_MT]),
				('SDLoss_MT', a.[SlowDownLoss_MT]),
				('TotLoss_MT', a.[TotLoss_MT])
				) c([DataType], [Loss])
			WHERE	t.[Refnum]		= @Refnum
			) u
		PIVOT (MAX(u.[Loss]) FOR u.[OppLossId] IN
			(
				[LostProdOpp],
				[PlantRelated],
				[TurnAround],
				[UnPlanned],
				[PRO],
				[OperError],
				[IntFeedInterrupt],
				[FFP],
				[Fouling],
				[Freezing],
				[Plugging],
				[FFPCauses],
				[FFPCause1],
				[FFPCause2],
				[FFPCause3],
				[FFPCause4],
				[FFPCauseBalance],
				[Transition],
				[ProcessOther],
				[ProcessOther1],
				[ProcessOther2],
				[ProcessOther3],
				[ProcessOther4],
				[ProcessOther5],
				[ProcessOtherBalance],
				[FurnaceProcess],
				[AcetyleneConv],
				[EQF],
				[Compressor],
				[OtherRotating],
				[FurnaceMech],
				[Corrosion],
				[ElecDist],
				[ControlSys],
				[OtherFailure],
				[Furnace],
				[UTF],
				[ExtElecFailure],
				[ExtOthUtilFailure],
				[IntElecFailure],
				[IntSteamFailure],
				[IntOthUtilFailure],
				[PlantOutages],
				[PyroFurnDT],
				[StandByDT],
				[Maint],
				[MaintDecoke],
				[MaintTLE],
				[MaintMinor],
				[MaintMajor],
				[NOC],
				[Demand],
				[ExtFeedInterrupt],
				[CapProject],
				[Strikes],
				[FeedMix],
				[Severity],
				[OtherNonOp],
				[OtherNonOp1],
				[OtherNonOp2],
				[OtherNonOp3],
				[OtherNonOp4],
				[OtherNonOp5],
				[OtherNonOpBalance],
				[PeriodDays]
			)
		) p;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
