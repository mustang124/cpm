﻿







CREATE              PROC [dbo].[spPyroYieldComposition](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON


DELETE FROM dbo.PyroYieldComposition WHERE Refnum = @Refnum
Insert into dbo.PyroYieldComposition (Refnum
, OpCondId
, SimModelId
, DataType
, H2
, CH4
, C2H2
, C2H4
, C2H6
, C3H4
, C3H6
, C3H8
, C4H6
, C4H8
, C4H10
, C5S
, C6H6
, C6C8NA
, C7H8
, C8H8
, C8H10
, PyroGasoline
, PyroGasOil
, PyroFuelOil
, Inerts
, Loss
, Tot)

SELECT Refnum, OpCondId, SimModelId, DataType, [H2], [CH4], [C2H2],[C2H4],[C2H6],[C3H4],[C3H6],[C3H8],[C4H6],[C4H8],[C4H10],[C5S],[C6H6],[C6C8NA],[C7H8],[C8H8],[C8H10],[PyroGasoline],[PyroGasOil],[PyroFuelOil],[Inerts],[Loss],[Tot]
      FROM (SELECT Refnum, FactorSetId, OpCondId, SimModelId, ComponentId, DataType, Value
            FROM (SELECT Refnum, FactorSetId, OpCondId, SimModelId, ComponentId, WtPcnt = Component_WtPcnt, kMT = cypa.Component_kMT, ExtWtPcnt = cypa.Component_Extrap_WtPcnt, ExtkMT = cypa.Component_Extrap_kMT, SuppkMT = cypa.Component_Supp_kMT
                  FROM calc.CompositionYieldPlantAggregate cypa) src
            UNPIVOT (Value for DataType IN (WtPcnt, kMT, ExtWtPcnt, ExtkMT, SuppkMT)) as unp) dt
      PIVOT (SUM(dt.Value) FOR dt.ComponentId IN ([H2],[CH4],[C2H2],[C2H4],[C2H6],[C3H4],[C3H6],[C3H8],[C4H6],[C4H8],[C4H10],[C5S],[C6H6],[C6C8NA],[C7H8],[C8H8],[C8H10],[PyroGasoline],[PyroGasOil],[PyroFuelOil],[Inerts],[Loss],[Tot])) pvt
WHERE Refnum = @Refnum AND FactorSetId = @FactorSetId
SET NOCOUNT OFF


















