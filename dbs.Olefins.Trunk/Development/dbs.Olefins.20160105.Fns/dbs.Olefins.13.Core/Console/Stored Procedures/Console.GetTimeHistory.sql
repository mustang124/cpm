﻿CREATE proc [Console].[GetTimeHistory]
@RefNum varchar(18)
as
Select Cast(RevNo As Integer) As IntRev, ValTime = Sum(ValTime), LastTime = MAX(StartTime) from Val.ValTime where refnum = @RefNum And LEN(Consultant)<4 AND RevNo IS NOT NULL Group by RevNo Order by IntRev
