﻿CREATE TABLE [stgFact].[PolyMatlBalance] (
    [Refnum]             VARCHAR (25)       NOT NULL,
    [TrainId]            TINYINT            NOT NULL,
    [Hydrogen]           REAL               NULL,
    [Ethylene_Plant]     REAL               NULL,
    [Ethylene_Other]     REAL               NULL,
    [Propylene_Plant]    REAL               NULL,
    [Propylene_Other]    REAL               NULL,
    [Butene]             REAL               NULL,
    [Hexene]             REAL               NULL,
    [Octene]             REAL               NULL,
    [OthMono1]           REAL               NULL,
    [OthMono2]           REAL               NULL,
    [OthMono3]           REAL               NULL,
    [Additives]          REAL               NULL,
    [Catalysts]          REAL               NULL,
    [OtherFeed]          REAL               NULL,
    [TotalRawMatl]       REAL               NULL,
    [OthMono1Desc]       NVARCHAR (50)      NULL,
    [OthMono2Desc]       NVARCHAR (50)      NULL,
    [OthMono3Desc]       NVARCHAR (50)      NULL,
    [LDPE_Liner]         REAL               NULL,
    [LDPE_Clarity]       REAL               NULL,
    [LDPE_Blow]          REAL               NULL,
    [LDPE_GP]            REAL               NULL,
    [LDPE_LidResin]      REAL               NULL,
    [LDPE_EVA]           REAL               NULL,
    [LDPE_Other]         REAL               NULL,
    [LDPE_Offspec]       REAL               NULL,
    [LLDPE_ButeneLiner]  REAL               NULL,
    [LLDPE_ButeneMold]   REAL               NULL,
    [LLDPE_HAOLiner]     REAL               NULL,
    [LLDPE_HAOMeltIndex] REAL               NULL,
    [LLDPE_HAOInject]    REAL               NULL,
    [LLDPE_HAOLidResin]  REAL               NULL,
    [LLDPE_HAORoto]      REAL               NULL,
    [LLDPE_Other]        REAL               NULL,
    [LLDPE_Offspec]      REAL               NULL,
    [HDPE_Homopoly]      REAL               NULL,
    [HDPE_Copoly]        REAL               NULL,
    [HDPE_Film]          REAL               NULL,
    [HDPE_GP]            REAL               NULL,
    [HDPE_Pipe]          REAL               NULL,
    [HDPE_Drum]          REAL               NULL,
    [HDPE_Roto]          REAL               NULL,
    [HDPE_Other]         REAL               NULL,
    [HDPE_Offspec]       REAL               NULL,
    [POLY_Homo]          REAL               NULL,
    [POLY_Copoly]        REAL               NULL,
    [POLY_CopolyBlow]    REAL               NULL,
    [POLY_CopolyInject]  REAL               NULL,
    [POLY_CopolyBlock]   REAL               NULL,
    [POLY_AtacticPP]     REAL               NULL,
    [POLY_Other]         REAL               NULL,
    [POLY_Offspec]       REAL               NULL,
    [ByProdOth1]         REAL               NULL,
    [ByProdOth2]         REAL               NULL,
    [ByProdOth3]         REAL               NULL,
    [ByProdOth4]         REAL               NULL,
    [ByProdOth5]         REAL               NULL,
    [TotProducts]        REAL               NULL,
    [ByProdOth1Desc]     NVARCHAR (50)      NULL,
    [ByProdOth2Desc]     NVARCHAR (50)      NULL,
    [ByProdOth3Desc]     NVARCHAR (50)      NULL,
    [ByProdOth4Desc]     NVARCHAR (50)      NULL,
    [ByProdOth5Desc]     NVARCHAR (50)      NULL,
    [ReactorProd]        REAL               NULL,
    [MonomerConsumed]    REAL               NULL,
    [MonomerUtil]        REAL               NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PolyMatlBalance_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyMatlBalance_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyMatlBalance_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyMatlBalance_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PolyMatlBalance] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TrainId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PolyMatlBalance_u]
	ON [stgFact].[PolyMatlBalance]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PolyMatlBalance]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PolyMatlBalance].Refnum		= INSERTED.Refnum
		AND	[stgFact].[PolyMatlBalance].TrainId		= INSERTED.TrainId;

END;