﻿CREATE TABLE [stgFact].[PracControls] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [ContType]       VARCHAR (3)        NOT NULL,
    [Tbl9038]        CHAR (1)           NULL,
    [Tbl9039]        CHAR (1)           NULL,
    [Tbl9040]        CHAR (1)           NULL,
    [Tbl9041]        CHAR (1)           NULL,
    [Tbl9042]        CHAR (1)           NULL,
    [Tbl9043]        CHAR (1)           NULL,
    [Tbl9044]        CHAR (1)           NULL,
    [Tbl9045]        CHAR (1)           NULL,
    [Tbl9046]        CHAR (1)           NULL,
    [Tbl9047]        CHAR (1)           NULL,
    [Tbl9048]        CHAR (1)           NULL,
    [Tbl9049]        CHAR (1)           NULL,
    [Tbl9050]        CHAR (1)           NULL,
    [Tbl9052]        CHAR (1)           NULL,
    [Tbl9053]        CHAR (1)           NULL,
    [Tbl9054]        CHAR (1)           NULL,
    [Tbl9055]        CHAR (1)           NULL,
    [Tbl9056]        CHAR (1)           NULL,
    [Tbl9057]        CHAR (1)           NULL,
    [Tbl9058]        CHAR (1)           NULL,
    [Tbl9059]        CHAR (1)           NULL,
    [Tbl9060]        CHAR (1)           NULL,
    [Tbl9062]        CHAR (1)           NULL,
    [Tbl9063]        CHAR (1)           NULL,
    [Tbl9064]        CHAR (1)           NULL,
    [Tbl9065]        CHAR (1)           NULL,
    [Tbl9066]        CHAR (1)           NULL,
    [Tbl9067]        CHAR (1)           NULL,
    [Tbl9068]        CHAR (1)           NULL,
    [Tbl9069]        CHAR (1)           NULL,
    [Tbl9070]        CHAR (1)           NULL,
    [Tbl9071n]       CHAR (1)           NULL,
    [Tbl9072n]       CHAR (1)           NULL,
    [Tbl9073n]       CHAR (1)           NULL,
    [Tbl9074n]       CHAR (1)           NULL,
    [Tbl9075n]       CHAR (1)           NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PracControls_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracControls_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracControls_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PracControls_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PracControls] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ContType] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PracControls_u]
	ON [stgFact].[PracControls]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PracControls]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PracControls].Refnum		= INSERTED.Refnum
		AND	[stgFact].[PracControls].ContType	= INSERTED.ContType;

END;