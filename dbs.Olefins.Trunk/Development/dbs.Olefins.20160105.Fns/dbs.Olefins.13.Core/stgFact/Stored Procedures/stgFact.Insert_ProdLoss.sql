﻿CREATE PROCEDURE [stgFact].[Insert_ProdLoss]
(
	@Refnum      VARCHAR (25),
	@Category    VARCHAR (3),
	@CauseID     VARCHAR (20),

	@Description VARCHAR (250) = NULL,
	@PrevDTLoss  REAL          = NULL,
	@PrevSDLoss  REAL          = NULL,
	@PrevTotLoss REAL          = NULL,
	@DTLoss      REAL          = NULL,
	@SDLoss      REAL          = NULL,
	@TotLoss     REAL          = NULL,
	@AnnDTLoss   REAL          = NULL,
	@AnnSDLoss   REAL          = NULL,
	@AnnTotLoss  REAL          = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[ProdLoss]([Refnum], [Category], [CauseID], [Description], [PrevDTLoss], [PrevSDLoss], [PrevTotLoss], [DTLoss], [SDLoss], [TotLoss], [AnnDTLoss], [AnnSDLoss], [AnnTotLoss])
	VALUES(@Refnum, @Category, @CauseID, @Description, @PrevDTLoss, @PrevSDLoss, @PrevTotLoss, @DTLoss, @SDLoss, @TotLoss, @AnnDTLoss, @AnnSDLoss, @AnnTotLoss);

END;