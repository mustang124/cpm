﻿CREATE PROCEDURE [stgFact].[Delete_FeedFlex]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[FeedFlex]
	WHERE [Refnum] = @Refnum;

END;