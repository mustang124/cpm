﻿CREATE PROCEDURE [stgFact].[Insert_PracTA]
(
	@Refnum        VARCHAR (25),

	@CGC           CHAR (1)     = NULL,
	@RC            CHAR (1)     = NULL,
	@TLE           CHAR (1)     = NULL,
	@OHX           CHAR (1)     = NULL,
	@Tower         CHAR (1)     = NULL,
	@Furnace       CHAR (1)     = NULL,
	@Util          CHAR (1)     = NULL,
	@CapProj       CHAR (1)     = NULL,
	@Other         CHAR (1)     = NULL,
	@OthCPMDesc    VARCHAR (75) = NULL,
	@TimeFact      CHAR (1)     = NULL,
	@OthTFDesc     VARCHAR (75) = NULL,
	@TAPrepTime    REAL         = NULL,
	@TaStartUpTime REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PracTA]([Refnum], [CGC], [RC], [TLE], [OHX], [Tower], [Furnace], [Util], [CapProj], [Other], [OthCPMDesc], [TimeFact], [OthTFDesc], [TAPrepTime], [TaStartUpTime])
	VALUES(@Refnum, @CGC, @RC, @TLE, @OHX, @Tower, @Furnace, @Util, @CapProj, @Other, @OthCPMDesc, @TimeFact, @OthTFDesc, @TAPrepTime, @TaStartUpTime);

END;