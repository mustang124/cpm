﻿CREATE PROCEDURE [stgFact].[Insert_SimulationParameters_Pyps]
(
	@Refnum					VARCHAR (25),

	@AdjButane				VARCHAR (50),
	@ErrorStream			VARCHAR (14),

	@FeedId_Raffinate		INT				= NULL,
	@FeedId_Heavy			INT				= NULL,
	@FeedId_Condensate		INT				= NULL,
	@FeedId_Atmospheric		INT				= NULL,
	@FeedId_Vacuum			INT				= NULL,
	@FeedId_Hydrotreated	INT				= NULL,
	@FeedId_OthLiq1			INT				= NULL,
	@FeedId_OthLiq2			INT				= NULL,
	@FeedId_OthLiq3			INT				= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[SimulationParameters_Pyps]([Refnum], [AdjButane], [ErrorStream], [FeedId_Raffinate], [FeedId_Heavy], [FeedId_Condensate], [FeedId_Atmospheric], [FeedId_Vacuum], [FeedId_Hydrotreated], [FeedId_OthLiq1], [FeedId_OthLiq2], [FeedId_OthLiq3])
	VALUES(@Refnum, @AdjButane, @ErrorStream, @FeedId_Raffinate, @FeedId_Heavy, @FeedId_Condensate, @FeedId_Atmospheric, @FeedId_Vacuum, @FeedId_Hydrotreated, @FeedId_OthLiq1, @FeedId_OthLiq2, @FeedId_OthLiq3);

END;