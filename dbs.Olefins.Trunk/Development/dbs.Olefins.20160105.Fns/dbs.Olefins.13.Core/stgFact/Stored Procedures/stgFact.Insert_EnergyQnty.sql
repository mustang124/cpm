﻿CREATE PROCEDURE [stgFact].[Insert_EnergyQnty]
(
	@Refnum           VARCHAR (25),
	@EnergyType       VARCHAR (12),

	@Amount           REAL          = NULL,
	@PSIG             REAL          = NULL,
	@MBtu             REAL          = NULL,
	@Cost             REAL          = NULL,
	@YN               VARCHAR (3)   = NULL,
	@UnitCnt          INT           = NULL,
	@Efficiency       REAL          = NULL,
	@PurchaseSteamPct REAL          = NULL,
	@ElecPowerPct     REAL          = NULL,
	@ThermalPct       REAL          = NULL,
	@OthDesc          VARCHAR (100) = NULL,
	@Temperature      REAL          = NULL,
	@Methane_WtPcnt   REAL          = NULL

)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[EnergyQnty]([Refnum], [EnergyType], [Amount], [PSIG], [MBtu], [Cost], [YN], [UnitCnt], [Efficiency], [PurchaseSteamPct], [ElecPowerPct], [ThermalPct], [OthDesc], [Temperature], [Methane_WtPcnt])
	VALUES(@Refnum, @EnergyType, @Amount, @PSIG, @MBtu, @Cost, @YN, @UnitCnt, @Efficiency, @PurchaseSteamPct, @ElecPowerPct, @ThermalPct, @OthDesc, @Temperature, @Methane_WtPcnt);

END;