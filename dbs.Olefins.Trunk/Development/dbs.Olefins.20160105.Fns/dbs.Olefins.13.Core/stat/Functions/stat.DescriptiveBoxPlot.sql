﻿
CREATE FUNCTION [stat].[DescriptiveBoxPlot]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics
	--	http://en.wikipedia.org/wiki/Box_plot
	
	@Population			stat.PopulationRandomVar		READONLY
)
RETURNS @ReturnTable TABLE 
(
	[xCount]			SMALLINT			NOT	NULL	CHECK([xCount]	>= 1),						--	Item Count

	[_BoxLowerE]		AS CAST([BoxLower] - ([BoxUpper] - [BoxLower]) * 3.0 AS FLOAT)
						PERSISTED			NOT	NULL,
	[_BoxLowerM]		AS CAST([BoxLower] - ([BoxUpper] - [BoxLower]) * 1.5 AS FLOAT)
						PERSISTED			NOT	NULL,
	[BoxLower]			FLOAT				NOT	NULL,
	[BoxMedian]			FLOAT				NOT	NULL,
	[BoxUpper]			FLOAT				NOT	NULL,
	[_BoxUpperM]		AS CAST([BoxUpper] + ([BoxUpper] - [BoxLower]) * 1.5 AS FLOAT)
						PERSISTED			NOT	NULL,
	[_BoxUpperE]		AS CAST([BoxUpper] + ([BoxUpper] - [BoxLower]) * 3.0 AS FLOAT)
						PERSISTED			NOT	NULL,

	[_BoxIMR]			AS CAST([BoxUpper] - [BoxLower]	AS FLOAT)
						PERSISTED			NOT	NULL
						CHECK([_BoxIMR] >= 0.0),
	[_BoxWidth]			AS CAST(SQRT([xCount]) AS FLOAT)
						PERSISTED			NOT	NULL,
	[_BoxNotch]			AS CAST(([BoxUpper] - [BoxLower]) / SQRT([xCount]) AS FLOAT)
						PERSISTED			NOT	NULL,

	CHECK([BoxLower] <= [BoxUpper]),
	CHECK([BoxMedian] BETWEEN [BoxLower] AND [BoxUpper])
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @PopulationRN TABLE
	(
		[FactorSetId]		VARCHAR (12)		NOT	NULL	CHECK([FactorSetId]	<> ''),
		[Refnum]			VARCHAR (25)		NOT	NULL	CHECK([Refnum]		<> ''),
		[x]					FLOAT				NOT	NULL,
		[nRow]				SMALLINT			NOT	NULL	CHECK([nRow]			 >= 1),

		PRIMARY KEY CLUSTERED ([x] ASC, [FactorSetId] ASC, [Refnum] ASC)
	)

	INSERT INTO @PopulationRN(FactorSetId, Refnum, x, nRow)
	SELECT
		o.FactorSetId,
		o.Refnum,
		o.x,
		ROW_NUMBER() OVER(ORDER BY o.x ASC)
	FROM @Population o
	WHERE o.Basis = 1;

	DECLARE @xCount		FLOAT;
	DECLARE @BoxMedian	FLOAT;
	DECLARE @BoxLB		FLOAT;	-- Lower fourth
	DECLARE	@BoxUB		FLOAT;	-- Upper fourth
	
	SELECT
		@xCount = COUNT(o.x)
	FROM @PopulationRN o;

	IF (@xCount = 0) RETURN;

	IF CAST(@xCount AS INT) % 2 = 0.0 BEGIN --EVEN
	
		SELECT	@BoxMedian = AVG(o.x)
		FROM	@PopulationRN o
		WHERE	o.nRow = ROUND(@xCount / 2.0, 0)
			OR	o.nRow = ROUND(@xCount / 2.0, 0) + 1;
			
		SELECT	@BoxLB = o.x
		FROM	@PopulationRN o
		WHERE	o.nRow = ROUND(@xCount / 4.0, 0);
		
		SELECT	@BoxUB = o.x
		FROM	@PopulationRN o
		WHERE	o.nRow = ROUND(3.0 * @xCount / 4.0, 0);
		
	END;
	ELSE BEGIN
		
		SELECT	@BoxMedian = o.x
		FROM	@PopulationRN o
		WHERE	o.nRow = ROUND(@xCount / 2.0, 0);
		
		SELECT	@BoxLB = AVG(o.x)
		FROM	@PopulationRN o
		WHERE	o.nRow = ROUND(@xCount / 4.0, 0)
			OR	o.nRow = ROUND(@xCount / 4.0, 0) + 1;
			
		SELECT	@BoxUB = AVG(o.x)
		FROM	@PopulationRN o
		WHERE	o.nRow = ROUND(3.0 * @xCount / 4.0, 0)
			OR	o.nRow = ROUND(3.0 * @xCount / 4.0, 0) + 1;
			
	END;

	INSERT INTO @ReturnTable(BoxLower, BoxMedian, BoxUpper, xCount)
	SELECT
		@BoxLB,
		@BoxMedian,
		@BoxUB,
		@xCount
		
	RETURN;

END;
