﻿
CREATE FUNCTION [stat].[DescriptiveGini]
(
	--	http://en.wikipedia.org/wiki/Gini_coefficient

	@Population			stat.PopulationRandomVar		READONLY										--	Data points to examine
)
RETURNS @ReturnTable TABLE 
(
	[FactorSetId]			VARCHAR (12)	NOT	NULL	CHECK([FactorSetId]	<> ''),
	[Refnum]				VARCHAR (25)	NOT	NULL	CHECK([Refnum]		<> ''),
	[x]						FLOAT 			NOT	NULL,
	[w]						FLOAT			NOT	NULL,

	[xLorenz]				FLOAT			NOT	NULL	CHECK(ROUND([xLorenz], 5) BETWEEN 0.0 AND 100.0)
														DEFAULT(0),
	[yLorenz]				FLOAT			NOT	NULL	CHECK(ROUND([yLorenz], 5) BETWEEN 0.0 AND 100.0)
														DEFAULT(0),
	[_dLorenz]				AS CAST([xLorenz] - [yLorenz] AS FLOAT)
							PERSISTED		NOT	NULL,

	[Gini]					FLOAT				NULL 	CHECK([Gini] BETWEEN 0.0 AND 100.0),
				
	PRIMARY KEY CLUSTERED ([x] ASC, [FactorSetId] ASC, [Refnum] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@xCount			SMALLINT;
	DECLARE	@i				FLOAT	= 0.0;		--	Cumulative w
	DECLARE	@j				FLOAT	= 0.0;		--	Cumulative x
	DECLARE	@s				FLOAT	= 0.0;		--	Previous value of @i
	DECLARE	@t				FLOAT	= 0.0;		--	Previous value of @j
	DECLARE	@xSum			FLOAT;				--	Sum of X values; Value of interest on the Y-Axis
	DECLARE	@wSum			FLOAT;				--	Sum of W values

	DECLARE @Area			FLOAT	= 0.0;	--	Gini Coefficient

	SELECT
		@xCount	= COUNT(o.x),
		@xSum	= SUM(ABS(o.x)),
		@wSum	= SUM(COALESCE(ABS(o.w), 1))
	FROM @Population o
	WHERE o.Basis = 1;

	IF (@xCount = 0 OR @xSum = 0.0) RETURN;

	INSERT INTO @ReturnTable(FactorSetId, Refnum, x, w)
	SELECT
		o.FactorSetId,
		o.Refnum,
		ABS(o.x),
		COALESCE(ABS(o.w), 1)
	FROM @Population o
	WHERE o.Basis = 1;

	UPDATE @ReturnTable
	SET
		@i = xLorenz = @i + w / @wSum * 100.0,
		@j = yLorenz = @j + x / @xSum * 100.0,
		@Area = @Area + (@i - @s) * (@j + @t) / 100.0,
		@s = @i,
		@t = @j;

	IF (@Area >= 100.0)
	BEGIN
		DELETE FROM @ReturnTable;
		RETURN;
	END;

	UPDATE @ReturnTable
	SET Gini = 100.0 - @Area;

	RETURN;

END
