﻿CREATE TABLE [inter].[PricingSecondaryStreamRegion] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [RegionId]              VARCHAR (5)        NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [Raw_Amount_Cur]        REAL               NOT NULL,
    [Adj_MatlBal_Cur]       REAL               NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [Report_Supersede_Cur]  REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],coalesce([MatlBal_Supersede_Cur],[Raw_Amount_Cur]+coalesce([Adj_MatlBal_Cur],(0.0))),(1))) PERSISTED NOT NULL,
    [_Report_Amount_Cur]    AS                 (CONVERT([real],coalesce([Report_Supersede_Cur],[Raw_Amount_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingSecondaryStreamRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingSecondaryStreamRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingSecondaryStreamRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingSecondaryStreamRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingSecondaryStreamRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingSecondaryStreamRegion_Adj_MatlBal_Cur] CHECK ([Adj_MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingSecondaryStreamRegion_MatlBal_Supersede_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingSecondaryStreamRegion_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingSecondaryStreamRegion_Report_Supersede_Cur] CHECK ([Report_Supersede_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingSecondaryStreamRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingSecondaryStreamRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingSecondaryStreamRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingSecondaryStreamRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingSecondaryStreamRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingSecondaryStreamRegion_u
	ON inter.PricingSecondaryStreamRegion
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingSecondaryStreamRegion
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingSecondaryStreamRegion.FactorSetId		= INSERTED.FactorSetId
		AND inter.PricingSecondaryStreamRegion.RegionId			= INSERTED.RegionId
		AND inter.PricingSecondaryStreamRegion.CalDateKey		= INSERTED.CalDateKey
		AND inter.PricingSecondaryStreamRegion.StreamId			= INSERTED.StreamId
		AND inter.PricingSecondaryStreamRegion.CurrencyRpt		= INSERTED.CurrencyRpt;

END;