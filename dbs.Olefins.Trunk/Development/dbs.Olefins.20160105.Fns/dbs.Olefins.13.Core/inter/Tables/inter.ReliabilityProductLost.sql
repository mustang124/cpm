﻿CREATE TABLE [inter].[ReliabilityProductLost] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [SchedId]        CHAR (1)           NOT NULL,
    [Loss_MTYr]      REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityProductLost_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityProductLost_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityProductLost_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityProductLost_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityProductLost] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [SchedId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReliabilityProductLost_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityProductLost_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityProductLost_Schedule_LookUp] FOREIGN KEY ([SchedId]) REFERENCES [dim].[Schedule_LookUp] ([ScheduleId]),
    CONSTRAINT [FK_ReliabilityProductLost_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReliabilityProductLost_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER inter.t_ReliabilityProductLost_u
	ON inter.ReliabilityProductLost
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.ReliabilityProductLost
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.ReliabilityProductLost.FactorSetId	= INSERTED.FactorSetId
		AND inter.ReliabilityProductLost.Refnum			= INSERTED.Refnum
		AND inter.ReliabilityProductLost.CalDateKey		= INSERTED.CalDateKey
		AND inter.ReliabilityProductLost.StreamId		= INSERTED.StreamId
		AND inter.ReliabilityProductLost.SchedId		= INSERTED.SchedId;

END;