﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonPlant_PPFC]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY
	
		INSERT INTO calc.EmissionsCarbonPlant(FactorSetId, Refnum, CalDateKey, EmissionsId, Quantity_MBtu, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.Plant_QtrDateKey,
			'CO2PPFCFuelGas'									[AccountId],
			t.LHValue_MBtu,
			SUM(t.MBtu * se.StdEmissions / CASE WHEN t.ComponentId = 'CH4' THEN nhv.NHValue_MBtulb ELSE 1.0 END) /	t.LHValue_MBtu	[CEF],
			SUM(t.MBtu * se.StdEmissions / CASE WHEN t.ComponentId = 'CH4' THEN nhv.NHValue_MBtulb ELSE 1.0 END)					[CO2e_MT]
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey,
				e.LHValue_MBtu,
				'—'[—],
				ISNULL(c.ComponentId, 'CH4')		[ComponentId],
				CASE WHEN c.ComponentId IS NULL
					THEN e.LHValue_MBtu - SUM(e.LHValue_MBtu / ie.LHValue_MBtuLb * nhv.NHValue_MBtulb * c.Component_WtPcnt / 100.0)
					ELSE				  SUM(e.LHValue_MBtu / ie.LHValue_MBtuLb * nhv.NHValue_MBtulb * c.Component_WtPcnt / 100.0)
					END	[MBtu]
			FROM @fpl											fpl
			INNER JOIN fact.EnergyLHValue						e
				ON	e.Refnum		= fpl.Refnum
				AND	e.CalDateKey	= fpl.Plant_QtrDateKey
				AND	e.AccountId		= 'PPFCFuelGas'
			INNER JOIN inter.[PricingPPFCFuelGasEnergy]			ie
				ON	ie.FactorSetId	= fpl.FactorSetId
				AND	ie.Refnum		= fpl.Refnum
				AND	ie.CalDateKey	= fpl.Plant_QtrDateKey
				AND	ie.AccountId	= e.AccountId
				AND	ie.StreamId		= 'PPFCFuelGas'
				AND	ie.AccountId	= 'PPFCFuelGas'
			INNER JOIN calc.CompositionStream					c
				ON	c.FactorSetId	= fpl.FactorSetId
				AND	c.Refnum		= fpl.Refnum
				AND	c.CalDateKey	= fpl.Plant_QtrDateKey
				AND	c.StreamId		= 'PPFC'
				AND	c.ComponentId	= c.ComponentId
				AND c.ComponentId IN ('H2', 'C2H4')
				AND c.Component_kMT	<> 0.0
			INNER JOIN ante.PricingNetHeatingValueComposition	nhv
				ON	nhv.FactorSetId	= fpl.FactorSetId
				AND	nhv.ComponentId	= c.ComponentId
			WHERE fpl.CalQtr = 4
			GROUP BY
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey,
				e.LHValue_MBtu,
				ie.[LHValue_MBtuLb],
				ROLLUP (c.ComponentId)
			) t
		LEFT OUTER JOIN ante.PricingNetHeatingValueComposition	nhv
			ON	nhv.FactorSetId = t.FactorSetId
			AND	nhv.ComponentId = t.ComponentId
			AND	nhv.ComponentId = 'CH4'
		LEFT OUTER JOIN (VALUES
			('CH4', 44.01 / 16.04 / 2204.6 * 1000000.0),
			('C2H4', 0.0697))	se(ComponentId, StdEmissions)
			ON	se.ComponentId	= t.ComponentId
		GROUP BY
			t.FactorSetId,
			t.Refnum,
			t.Plant_QtrDateKey,
			t.LHValue_MBtu;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;