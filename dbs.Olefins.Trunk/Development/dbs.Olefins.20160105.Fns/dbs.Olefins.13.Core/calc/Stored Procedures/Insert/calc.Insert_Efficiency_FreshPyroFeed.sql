﻿CREATE PROCEDURE [calc].[Insert_Efficiency_FreshPyroFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Efficiency([FactorSetId], [Refnum], [CalDateKey], [EfficiencyId], [UnitId], [EfficiencyStandard])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			wCdu.[EfficiencyId],
			uef.[UnitId],
			uef.[Coefficient]
			* POWER(
				[calc].[MaxValue](
					[calc].[MinValue](
						uef.[ValueMax],
						(cp.[Stream_MTd] - COALESCE(SUM(qSupp.[Quantity_kMT] * qRec.[Recovered_WtPcnt]) / cUtil.[_UtilizationStream_Pcnt] * 1000.0 / 365.0, 0.0)) / tra.[Train_Count]
						),
					uef.[ValueMin]
					),
				uef.[Exponent])
			* (cp.[Stream_MTd] - COALESCE(SUM(qSupp.[Quantity_kMT] * qRec.[Recovered_WtPcnt]) / cUtil.[_UtilizationStream_Pcnt] * 1000.0 / 365.0, 0.0))
			* wCdu.[Coefficient]
			/ 1000.0
		FROM @fpl										fpl
		INNER JOIN ante.UnitEfficiencyWWCdu				wCdu
			ON	wCdu.[FactorSetId]	= fpl.[FactorSetId]
		INNER JOIN [calc].[PeerGroupFeedClass]			fc
			ON	fc.[FactorSetId]	= fpl.[FactorSetId]
			AND	fc.[Refnum]			= fpl.[Refnum]
			AND	fc.[CalDateKey]		= fpl.[Plant_AnnDateKey]
		INNER JOIN (
			VALUES
				(1, 'Ethane'),
				(2, 'LPG'),
				(3, 'Liquid'),
				(4, 'Liquid'),
				(5, 'Liquid')
			)											map ([PeerGroup], [UnitId])
			ON	map.[PeerGroup]	= fc.[PeerGroup]
		INNER JOIN ante.UnitEfficiencyFactors			uef
			ON	uef.[FactorSetId]	= fpl.[FactorSetId]
			AND	uef.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	uef.[UnitId]		= map.[UnitId]

		INNER JOIN [calc].[CapacityPlant]				cp
			ON	cp.[FactorSetId]	= fpl.[FactorSetId]
			AND	cp.[Refnum]			= fpl.[Refnum]
			AND	cp.[CalDateKey]		= fpl.[Plant_AnnDateKey]
			AND	cp.[StreamId]		= 'ProdOlefins'
		INNER JOIN [calc].[CapacityUtilization]			cUtil
			ON	cUtil.[FactorSetId]	= fpl.[FactorSetId]
			AND	cUtil.[Refnum]		= fpl.[Refnum]
			AND	cUtil.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	cUtil.[SchedId]		= 'P'
			AND	cUtil.[StreamId]	= 'ProdOlefins'
		LEFT OUTER JOIN [fact].[Quantity]				qSupp
			ON	qSupp.[Refnum]		= fpl.[Refnum]
			AND	qSupp.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	qSupp.StreamId		IN ('ConcEthylene', 'ConcPropylene', 'ROGEthylene', 'ROGPropylene', 'DilEthylene', 'DilPropylene')
		LEFT OUTER JOIN [fact].[QuantitySuppRecovery]	qRec
			ON	qRec.[Refnum]		= fpl.[Refnum]
			AND	qRec.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	qRec.[StreamId]		= qSupp.[StreamId]
		INNER JOIN [fact].[ReliabilityTAAggregate]		tra
			ON	tra.[Refnum]		= fpl.[Refnum]
			AND	tra.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	tra.[SchedId]		= 'P'
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			uef.[UnitId],
			wCdu.[EfficiencyId],
			wCdu.[Coefficient],
			uef.[Coefficient],
			uef.[Exponent],
			uef.[ValueMax],
			uef.[ValueMin],
			cp.[Stream_MTd],
			cUtil.[_UtilizationStream_Pcnt],
			tra.[Train_Count];
		
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;