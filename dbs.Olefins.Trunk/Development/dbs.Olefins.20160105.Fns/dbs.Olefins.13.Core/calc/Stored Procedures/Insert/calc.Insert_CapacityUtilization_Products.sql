﻿CREATE PROCEDURE calc.Insert_CapacityUtilization_Products
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @m TABLE (
			[StreamId]		VARCHAR(42)	NOT NULL CHECK([StreamId] <> ''),
			[ComponentId]	VARCHAR(42)	NOT NULL CHECK([ComponentId] <> ''),
			PRIMARY KEY CLUSTERED ([StreamId] ASC)
		);

		INSERT INTO @m([StreamId], [ComponentId])
		VALUES
			('Ethylene', 'C2H4'),
			('Propylene', 'C3H6'),
			('ProdOlefins', 'ProdOlefins');

		INSERT INTO calc.CapacityUtilization(FactorSetId, Refnum, CalDateKey, SchedId, StreamId,
			StreamAvg_kMT, CapacityAvg_kMT,
			ComponentId, ProductionActual_kMT, Loss_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			u.SchedId,
			u.StreamId,
			u.StreamAvg_kMT,
			u.CapacityAvg_kMT,
			p.ComponentId,
			/*	Bug  62 - ProductionActual_kMT (contained) Incorrect value; needs to use Production_kMT (sold)*/
			/*	Bug 201 - needs to use ProductionActual_kMT (contained)	2011PCH059  */
			p.[ProductionSold_kMT],
			l.Loss_kMT
		FROM @fpl									tsq
		INNER JOIN	calc.CapacityPlantTaAdj			u
			ON	u.FactorSetId = tsq.FactorSetId
			AND	u.Refnum = tsq.Refnum
			AND	u.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN @m								m
			ON	m.StreamId = u.StreamId
		INNER JOIN calc.DivisorsProduction			p
			ON	p.FactorSetId = tsq.FactorSetId
			AND p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
			AND	p.ComponentId = m.ComponentId
			AND p.FactorSetId = tsq.FactorSetId
		LEFT OUTER JOIN	(
			SELECT
				r.FactorSetId,
				r.Refnum,
				r.CalDateKey,
				r.EthyleneLoss_kMT		[Ethylene],
				r._PropyleneLoss_kMT	[Propylene],
				r._OLEProdLoss_kMT		[ProdOlefins]
			FROM calc.CapacityProductionRatio		r
			) u
			UNPIVOT (
			Loss_kMT FOR StreamId IN(
				Ethylene, Propylene, ProdOlefins
				)
			) l
			ON	l.FactorSetId = tsq.FactorSetId
			AND	l.Refnum = tsq.Refnum
			AND	l.CalDateKey = tsq.Plant_QtrDateKey
			AND	l.FactorSetId = tsq.FactorSetId
			AND	l.StreamId = u.StreamId;
		
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;