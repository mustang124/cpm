﻿CREATE PROCEDURE calc.Insert_OpEx
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @AccountId VARCHAR(42) = NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO calc.OpEx';
		PRINT @ProcedureDesc;
	
		INSERT INTO calc.OpEx(
			FactorSetId,
			Refnum,
			CalDateKey,
			AccountId,
			CurrencyRpt,
			Reported_Cur
			)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			o.AccountId,
			tsq.CurrencyRpt,
			o.Amount_Cur
		FROM @fpl										tsq
		INNER JOIN fact.OpEx							o
			ON	o.Refnum = tsq.Refnum
			AND	o.CalDateKey = tsq.Plant_QtrDateKey
			AND	o.CurrencyRpt = tsq.CurrencyRpt
		WHERE	tsq.CalQtr = 4;

		SET @AccountId = 'MaintRetube';
		SET @ProcedureDesc = NCHAR(9) + N'UPDATE calc.OpEx (' + @AccountId + ')';
		PRINT @ProcedureDesc;

		UPDATE calc.OpEx
		SET Adjusted_Cur = 0.0
		FROM @fpl												tsq
		INNER JOIN calc.FurnacesAggregate						f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
			AND	f.CurrencyRpt = tsq.CurrencyRpt
		WHERE	tsq.FactorSetId			= calc.OpEx.FactorSetId
			AND tsq.Refnum				= calc.OpEx.Refnum
			AND	tsq.Plant_QtrDateKey	= calc.OpEx.CalDateKey
			AND tsq.CurrencyRpt			= calc.OpEx.CurrencyRpt
			AND calc.OpEx.AccountId		= @AccountId;

		SET @AccountId = 'MaintRetubeMatl';
		SET @ProcedureDesc = NCHAR(9) + N'INSERT/UPDATE calc.OpEx (' + @AccountId + ')';
		PRINT @ProcedureDesc;

		UPDATE calc.OpEx
		SET Adjusted_Cur = f.InflAdjAnn_MaintRetubeMatl_Cur
		FROM @fpl												tsq
		INNER JOIN calc.FurnacesAggregate						f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
			AND	f.CurrencyRpt = tsq.CurrencyRpt
		WHERE	tsq.FactorSetId			= calc.OpEx.FactorSetId
			AND tsq.Refnum				= calc.OpEx.Refnum
			AND	tsq.Plant_QtrDateKey	= calc.OpEx.CalDateKey
			AND tsq.CurrencyRpt			= calc.OpEx.CurrencyRpt
			AND calc.OpEx.AccountId		= @AccountId;
		
		INSERT INTO calc.OpEx(
			FactorSetId,
			Refnum,
			CalDateKey,
			AccountId,
			CurrencyRpt,
			Adjusted_Cur
			)
		SELECT
			f.FactorSetId,
			f.Refnum,
			f.CalDateKey,
			@AccountId,
			f.CurrencyRpt,
			f.InflAdjAnn_MaintRetubeMatl_Cur
		FROM @fpl									tsq
		INNER JOIN calc.FurnacesAggregate			f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
			ANd	f.CurrencyRpt = tsq.CurrencyRpt
		LEFT OUTER JOIN calc.OpEx					o
			ON	o.FactorSetId = tsq.FactorSetId
			AND	o.Refnum = tsq.Refnum
			AND	o.CalDateKey = tsq.Plant_QtrDateKey
			AND	o.CurrencyRpt = tsq.CurrencyRpt
			AND	o.AccountId = @AccountId		
		WHERE	o.AccountId IS NULL
			AND	tsq.CalQtr = 4;

		SET @AccountId = 'MaintRetubeLabor';
		SET @ProcedureDesc = NCHAR(9) + N'INSERT/UPDATE calc.OpEx (' + @AccountId + ')';
		PRINT @ProcedureDesc;

		UPDATE calc.OpEx
		SET Adjusted_Cur = f.InflAdjAnn_MaintRetubeLabor_Cur
		FROM @fpl												tsq
		INNER JOIN calc.FurnacesAggregate						f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
			AND	f.CurrencyRpt = tsq.CurrencyRpt
		WHERE	tsq.FactorSetId			= calc.OpEx.FactorSetId
			AND tsq.Refnum				= calc.OpEx.Refnum
			AND	tsq.Plant_QtrDateKey	= calc.OpEx.CalDateKey
			AND tsq.CurrencyRpt			= calc.OpEx.CurrencyRpt
			AND calc.OpEx.AccountId		= @AccountId;
		
		INSERT INTO calc.OpEx(
			FactorSetId,
			Refnum,
			CalDateKey,
			AccountId,
			CurrencyRpt,
			Adjusted_Cur
			)
		SELECT
			f.FactorSetId,
			f.Refnum,
			f.CalDateKey,
			@AccountId,
			f.CurrencyRpt,
			f.InflAdjAnn_MaintRetubeLabor_Cur
		FROM @fpl									tsq
		INNER JOIN calc.FurnacesAggregate			f
			ON	f.FactorSetId = tsq.FactorSetId
			AND	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
			ANd	f.CurrencyRpt = tsq.CurrencyRpt
		LEFT OUTER JOIN calc.OpEx					o
			ON	o.FactorSetId = tsq.FactorSetId
			AND	o.Refnum = tsq.Refnum
			AND	o.CalDateKey = tsq.Plant_QtrDateKey
			AND	o.CurrencyRpt = tsq.CurrencyRpt
			AND	o.AccountId = @AccountId		
		WHERE	o.AccountId IS NULL
			AND	tsq.CalQtr = 4;

		SET @AccountId = 'STTaExp';
		SET @ProcedureDesc = NCHAR(9) + N'INSERT/UPDATE calc.OpEx (' + @AccountId + ')';
		PRINT @ProcedureDesc;

		UPDATE o
		SET Adjusted_Cur = 0.0
		FROM @fpl									tsq
		INNER JOIN calc.OpEx						o
			ON	o.FactorSetId	= tsq.FactorSetId
			AND	o.Refnum		= tsq.Refnum
			AND	o.CalDateKey	= tsq.Plant_QtrDateKey
			AND	o.CurrencyRpt	= tsq.CurrencyRpt
			AND o.AccountId		= 'TaCurrExp';

		UPDATE o
		SET Adjusted_Cur = t.TaAdjExRateAnn_TurnAroundCost_kCur
		FROM @fpl									tsq
		INNER JOIN calc.TaAdjCapacityAggregate		t
			ON	t.FactorSetId	= tsq.FactorSetId
			AND	t.Refnum		= tsq.Refnum
			AND	t.CalDateKey	= tsq.Plant_QtrDateKey
			AND	t.CurrencyRpt	= tsq.CurrencyRpt
			AND	t.SchedId		= 'A'
		INNER JOIN calc.OpEx						o
			ON	o.FactorSetId	= tsq.FactorSetId
			AND	o.Refnum		= tsq.Refnum
			AND	o.CalDateKey	= tsq.Plant_QtrDateKey
			AND	o.CurrencyRpt	= tsq.CurrencyRpt
			AND o.AccountId		= 'TAAccrual';

		INSERT INTO calc.OpEx(
			FactorSetId,
			Refnum,
			CalDateKey,
			AccountId,
			CurrencyRpt,
			Adjusted_Cur
			)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			'TAAccrual',
			fpl.CurrencyRpt,
			a.TaAdjExRateAnn_TurnAroundCost_kCur
		FROM @fpl											fpl
		INNER JOIN calc.TaAdjCapacityAggregate				a
			ON	a.FactorSetId	= fpl.FactorSetId
			AND	a.Refnum		= fpl.Refnum
			AND	a.CalDateKey	= fpl.Plant_QtrDateKey
			AND	a.CurrencyRpt	= fpl.CurrencyRpt
			AND	a.SchedId		= 'A'
		LEFT OUTER JOIN calc.OpEx							o
			ON	o.FactorSetId	= fpl.FactorSetId
			AND	o.Refnum		= fpl.Refnum
			AND	o.CalDateKey	= fpl.Plant_QtrDateKey
			AND	o.CurrencyRpt	= fpl.CurrencyRpt
			AND o.AccountId		= 'TAAccrual'
		WHERE	fpl.CalQtr	= 4
			AND	o.AccountId IS NULL;
		
		SET @AccountId = 'PPFC';
		SET @ProcedureDesc = NCHAR(9) + N'INSERT/UPDATE calc.OpEx (' + @AccountId + ')';
		PRINT @ProcedureDesc;

		UPDATE calc.OpEx
		SET Adjusted_Cur = 0.0
		FROM @fpl							tsq
		--	Task 141: Implement Tri-Tables in calculations
		--WHERE	calc.OpEx.AccountId IN (SELECT d.DescendantId FROM dim.AccountLuDescendants(@AccountId, 1, '') d)
		WHERE	calc.OpEx.AccountId IN (SELECT d.DescendantId FROM dim.Account_Bridge d WHERE d.FactorSetId = tsq.FactorSetId AND d.AccountId = @AccountId)
			AND	tsq.Refnum			= calc.OpEx.Refnum
			AND	tsq.FactorSetId		= calc.OpEx.FactorSetId;

		UPDATE calc.OpEx
		SET Adjusted_Cur = p.Amount_Cur
		FROM @fpl												tsq
		INNER JOIN calc.[PricingPlantAccount]					p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
		WHERE	p.FactorSetId		= calc.OpEx.FactorSetId
			AND	p.Refnum			= calc.OpEx.Refnum
			AND	p.CalDateKey		= calc.OpEx.CalDateKey
			AND p.AccountId			= calc.OpEx.AccountId
			AND p.CurrencyRpt		= calc.OpEx.CurrencyRpt;

		INSERT INTO calc.OpEx(
			FactorSetId,
			Refnum,
			CalDateKey,
			AccountId,
			CurrencyRpt,
			Adjusted_Cur
			)
		SELECT
			p.FactorSetId,
			p.Refnum,
			p.CalDateKey,
			p.AccountId,
			p.CurrencyRpt,
			p.Amount_Cur
		FROM @fpl									tsq
		INNER JOIN calc.[PricingPlantAccount]			p
			ON	p.FactorSetId = tsq.FactorSetId
			AND	p.Refnum = tsq.Refnum
			AND	p.CalDateKey = tsq.Plant_QtrDateKey
		LEFT OUTER JOIN calc.OpEx					o
			ON	o.FactorSetId = p.FactorSetId
			AND	o.Refnum = p.Refnum
			AND	o.CalDateKey = p.CalDateKey
			AND	o.CurrencyRpt = p.CurrencyRpt
			AND	o.AccountId = p.AccountId
		WHERE	o.AccountId IS NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;