﻿CREATE PROCEDURE [calc].[Insert_StandardEnergyFracFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY
	
		-------------------------------------------------------------------------------
		--	F317: Feed Prep

		INSERT INTO [calc].[StandardEnergyFracFeed]([FactorSetId], [Refnum], [CalDateKey], [StandardEnergyId], [FeedProcessed_kMT], [FeedDensity_SG], [Energy_kBtuBBL], [StandardEnergy_MBtuDay])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			'FracFeed',
			fff.[FeedProcessed_kMT],
			fff.[FeedDensity_SG],
			map.[Energy_kBtuBBL],
			fff.[FeedProcessed_kMT]
			* [calc].[MinValue](
				11.0,
				[calc].[MaxValue](
					6.0,
					0.159 / fff.[FeedDensity_SG]
					)
				)
			/ 365.0
			* map.[Energy_kBtuBBL] [StandardEnergy_MBtuDay]
		FROM @fpl									fpl
		INNER JOIN [fact].[FacilitiesFeedFrac]		fff
			ON	fff.[Refnum]			= fpl.[Refnum]
			AND	fff.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	fff.[FeedProcessed_kMT]	> 0.0
			AND	fff.[FeedDensity_SG]	> 0.0
		INNER JOIN [calc].[PeerGroupFeedClass]		fc
			ON	fc.[FactorSetId]		= fpl.[FactorSetId]
			AND	fc.[Refnum]				= fpl.[Refnum]
			AND	fc.[CalDateKey]			= fpl.[Plant_QtrDateKey]
		INNER JOIN (VALUES
			('1', 60),
			('2', 50),
			('3', 55),
			('4', 55)
			)									map ([PeerGroup], [Energy_kBtuBBL])
			ON map.[PeerGroup]	= fc.[PeerGroup]
		WHERE	fpl.[CalQtr]	= 4
			AND	fpl.[FactorSet_AnnDateKey] > 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;