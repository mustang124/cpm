﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonPlant_Electricity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY
	
		INSERT INTO calc.EmissionsCarbonPlant(FactorSetId, Refnum, CalDateKey, EmissionsId, Quantity_MBtu, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			ef.EmissionsId,
			e._LHValue_MBtu,
			ef.CEF_MTCO2_MBtu * 44.01 / 12.01 / 9.09,
			e._LHValue_MBtu * ef.CEF_MTCO2_MBtu * 44.01 / 12.01 / 9.09
		FROM @fpl												fpl
		INNER JOIN ante.EmissionsFactorElectricity				ef
			ON	ef.FactorSetId = fpl.FactorSetId
			AND	ef.CountryId = fpl.CountryId
			AND	ef.CEF_MTCO2_MBtu IS NOT NULL
		INNER JOIN fact.EnergyElec								e
			ON	e.Refnum = fpl.Refnum
			AND	e.CalDateKey = fpl.Plant_QtrDateKey
			AND	e.CurrencyRpt = fpl.CurrencyRpt
			AND	e.AccountId = ef.EmissionsId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;