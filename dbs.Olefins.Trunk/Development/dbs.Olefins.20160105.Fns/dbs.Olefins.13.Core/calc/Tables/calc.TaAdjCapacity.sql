﻿CREATE TABLE [calc].[TaAdjCapacity] (
    [FactorSetId]                         VARCHAR (12)       NOT NULL,
    [Refnum]                              VARCHAR (25)       NOT NULL,
    [CalDateKey]                          INT                NOT NULL,
    [TrainId]                             INT                NOT NULL,
    [SchedId]                             CHAR (1)           NOT NULL,
    [StreamId]                            VARCHAR (42)       NULL,
    [Capacity_Pcnt]                       REAL               NULL,
    [CapacityCalc_Pcnt]                   REAL               NOT NULL,
    [Interval_Mnths]                      REAL               NOT NULL,
    [Interval_Years]                      REAL               NOT NULL,
    [DownTime_Hrs]                        REAL               NOT NULL,
    [TurnAround_Date]                     DATE               NULL,
    [TurnAroundInStudyYear_Bit]           BIT                NULL,
    [CurrencyRpt]                         VARCHAR (4)        NULL,
    [TurnAroundCost_MCur]                 REAL               NULL,
    [TurnAround_ManHrs]                   REAL               NULL,
    [Ann_DownTime_Hrs]                    REAL               NOT NULL,
    [Ann_TurnAroundCost_kCur]             REAL               NULL,
    [Ann_TurnAround_ManHrs]               REAL               NULL,
    [CostPerManHour_Cur]                 REAL               NULL,
    [_Ann_TurnAroundLoss_Days]            AS                 (CONVERT([real],case when [Interval_Mnths]<>(0.0) then (([CapacityCalc_Pcnt]/(100.0))*([DownTime_Hrs]/(24.0)))/([Interval_Mnths]/(12.0))  end,(1))) PERSISTED,
    [_Ann_TurnAroundLoss_Pcnt]            AS                 (CONVERT([real],(case when [Interval_Mnths]<>(0.0) then (([CapacityCalc_Pcnt]/(100.0))*([DownTime_Hrs]/(24.0)))/([Interval_Mnths]/(12.0))  end/(365.25))*(100.0),(1))) PERSISTED,
    [_Ann_FullPlantEquivalentDT_HrsYr]    AS                 (CONVERT([real],(([DownTime_Hrs]/[Interval_Years])*[CapacityCalc_Pcnt])/(100.0),(1))) PERSISTED,
    [TaAdjExRate]                         REAL               NOT NULL,
    [_TaAdjExRateAnn_TurnAroundCost_kCur] AS                 (CONVERT([real],((1.0)+([TaAdjExRate]-(1.0))/(1.2))*[Ann_TurnAroundCost_kCur],(1))) PERSISTED,
    [tsModified]                          DATETIMEOFFSET (7) CONSTRAINT [DF_TaAdjCapacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                      NVARCHAR (168)     CONSTRAINT [DF_TaAdjCapacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                      NVARCHAR (168)     CONSTRAINT [DF_TaAdjCapacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                       NVARCHAR (168)     CONSTRAINT [DF_TaAdjCapacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_TaAdjCapacity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [TrainId] ASC, [SchedId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_calc_ReliabilityTrainCap_Capacity_Pcnt] CHECK ([Capacity_Pcnt]>=(0.0) AND [Capacity_Pcnt]<=(100.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_Ann_DownTime_Hrs] CHECK ([Ann_DownTime_Hrs]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_Ann_TurnAround_ManHrs] CHECK ([Ann_TurnAround_ManHrs]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_Ann_TurnAroundCost_kCur] CHECK ([Ann_TurnAroundCost_kCur]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_CapacityCalc_Pcnt] CHECK ([CapacityCalc_Pcnt]>=(0.0) AND [CapacityCalc_Pcnt]<=(100.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_CostPerManHour_Cur] CHECK ([CostPerManHour_Cur]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_DownTime_Hrs] CHECK ([DownTime_Hrs]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_Interval_Mnths] CHECK ([Interval_Mnths]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_Interval_Years] CHECK ([Interval_Years]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_TurnAround_Date] CHECK (datepart(year,[TurnAround_Date])>=(1965)),
    CONSTRAINT [CR_calc_TaAdjCapacity_TurnAround_ManHrs] CHECK ([TurnAround_ManHrs]>=(0.0)),
    CONSTRAINT [CR_calc_TaAdjCapacity_TurnAroundCost_MCur] CHECK ([TurnAroundCost_MCur]>=(0.0)),
    CONSTRAINT [FK_calc_TaAdjCapacity_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_TaAdjCapacity_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_TaAdjCapacity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_TaAdjCapacity_Schedule_LookUp] FOREIGN KEY ([SchedId]) REFERENCES [dim].[Schedule_LookUp] ([ScheduleId]),
    CONSTRAINT [FK_calc_TaAdjCapacity_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_TaAdjCapacity_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_TaAdjCapacity_ReliabilityProductLost]
    ON [calc].[TaAdjCapacity]([SchedId] ASC, [StreamId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [_Ann_FullPlantEquivalentDT_HrsYr]);


GO
CREATE TRIGGER calc.t_TaAdjCapacity_u
	ON  calc.TaAdjCapacity
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.TaAdjCapacity
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.TaAdjCapacity.FactorSetId		= INSERTED.FactorSetId
		AND calc.TaAdjCapacity.Refnum			= INSERTED.Refnum
		AND calc.TaAdjCapacity.CalDateKey		= INSERTED.CalDateKey
		AND calc.TaAdjCapacity.TrainId			= INSERTED.TrainId
		AND calc.TaAdjCapacity.SchedId			= INSERTED.SchedId;
	
END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Replaces Ann_TurnAroundLoss_Days in fact.ReliabilityTA; utilizes the calculated capacity percent.', @level0type = N'SCHEMA', @level0name = N'calc', @level1type = N'TABLE', @level1name = N'TaAdjCapacity', @level2type = N'COLUMN', @level2name = N'_Ann_TurnAroundLoss_Days';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Replaces Ann_TurnAroundLoss_Pcnt in fact.ReliabilityTA; utilizes the calculated capacity percent.', @level0type = N'SCHEMA', @level0name = N'calc', @level1type = N'TABLE', @level1name = N'TaAdjCapacity', @level2type = N'COLUMN', @level2name = N'_Ann_TurnAroundLoss_Pcnt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Formula is a reduction from the maintenance cost calculation MaintCost!E58:G58; and the inflation/exchange rates on PeerGroups!R184:AH225 (References may not be exact)', @level0type = N'SCHEMA', @level0name = N'calc', @level1type = N'TABLE', @level1name = N'TaAdjCapacity', @level2type = N'COLUMN', @level2name = N'_TaAdjExRateAnn_TurnAroundCost_kCur';

