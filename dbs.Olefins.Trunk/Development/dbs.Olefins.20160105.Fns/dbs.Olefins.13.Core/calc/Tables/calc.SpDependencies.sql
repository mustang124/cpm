﻿CREATE TABLE [calc].[SpDependencies] (
    [FactorSetId] CHAR (4)            NOT NULL,
    [SpName]      VARCHAR (128)       NOT NULL,
    [InsertInto]  VARCHAR (128)       NOT NULL,
    [SelectFrom]  VARCHAR (128)       NOT NULL,
    [SortKey]     INT                 NOT NULL,
    [Hierarchy]   [sys].[hierarchyid] NOT NULL,
    CONSTRAINT [PK_SpDependencies] PRIMARY KEY CLUSTERED ([SpName] ASC, [InsertInto] ASC, [SelectFrom] ASC),
    CONSTRAINT [CL_SpDependencies_From] CHECK ([SelectFrom]<>''),
    CONSTRAINT [CL_SpDependencies_FsId] CHECK ([FactorSetId]<>''),
    CONSTRAINT [CL_SpDependencies_Into] CHECK ([InsertInto]<>''),
    CONSTRAINT [CL_SpDependencies_Name] CHECK ([SpName]<>'')
);

