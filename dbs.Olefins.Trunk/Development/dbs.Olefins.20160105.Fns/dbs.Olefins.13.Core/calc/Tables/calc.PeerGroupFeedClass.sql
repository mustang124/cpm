﻿CREATE TABLE [calc].[PeerGroupFeedClass] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [PeerGroup]       TINYINT            NOT NULL,
    [StreamId]        VARCHAR (42)       NULL,
    [PlantDensity_SG] REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupFeedClass_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_PeerGroupFeedClass_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PeerGroupFeedClass] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_PeerGroupFeedClass_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PeerGroupFeedClass_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PeerGroupFeedClass_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_PeerGroupFeedClass_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_PeerGroupFeedClass_u
	ON  calc.[PeerGroupFeedClass]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[PeerGroupFeedClass]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[PeerGroupFeedClass].FactorSetId	= INSERTED.FactorSetId
		AND	calc.[PeerGroupFeedClass].Refnum			= INSERTED.Refnum
		AND calc.[PeerGroupFeedClass].CalDateKey		= INSERTED.CalDateKey;
			
END