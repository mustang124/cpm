﻿CREATE VIEW [calc].[OpExAggregateEnergy]
WITH SCHEMABINDING
AS
SELECT
	o.FactorSetId,
	o.Refnum,
	o.CalDateKey,
	b.AccountId,
	o.CurrencyRpt,
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + o.Reported_Cur
		WHEN '-' THEN - o.Reported_Cur
		END)							[Reported_Cur],
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + o.Adjusted_Cur
		WHEN '-' THEN - o.Adjusted_Cur
		END)							[Adjusted_Cur],
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + o._Amount_Cur
		WHEN '-' THEN - o._Amount_Cur
		END)							[Amount_Cur]
FROM calc.OpEx							o
INNER JOIN dim.AccountEnergy_Bridge			b
	ON	b.FactorSetId = o.FactorSetId
	AND	b.DescendantId = o.AccountId
GROUP BY
	o.FactorSetId,
	o.Refnum,
	o.CalDateKey,
	b.AccountId,
	o.CurrencyRpt;