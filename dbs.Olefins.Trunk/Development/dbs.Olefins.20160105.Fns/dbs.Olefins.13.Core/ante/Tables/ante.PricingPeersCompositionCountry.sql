﻿CREATE TABLE [ante].[PricingPeersCompositionCountry] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [CountryId]         CHAR (3)           NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [OverRide_RegionId] VARCHAR (5)        NOT NULL,
    [AccountId]         VARCHAR (42)       NOT NULL,
    [StreamId]          VARCHAR (42)       NULL,
    [ComponentId]       VARCHAR (42)       NULL,
    [Adj_Proximity_Cur] REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPeersCompositionCountry_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_PricingPeersCompositionCountry_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_PricingPeersCompositionCountry_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_PricingPeersCompositionCountry_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [FK_PricingPeersCompositionCountry_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_PricingPeersCompositionCountry_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPeersCompositionCountry_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingPeersCompositionCountry_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingPeersCompositionCountry_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingPeersCompositionCountry_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingPeersCompositionCountry_Region_LookUp] FOREIGN KEY ([OverRide_RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingPeersCompositionCountry_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [UK_PricingPeersCompositionCountry] UNIQUE CLUSTERED ([FactorSetId] ASC, [CountryId] ASC, [CurrencyRpt] ASC, [OverRide_RegionId] ASC, [AccountId] ASC, [ComponentId] ASC, [CalDateKey] ASC)
);


GO

CREATE TRIGGER [ante].[t_PricingPeersCompositionCountry_u]
	ON [ante].[PricingPeersCompositionCountry]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingPeersCompositionCountry]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingPeersCompositionCountry].FactorSetId		= INSERTED.FactorSetId
		AND	[ante].[PricingPeersCompositionCountry].CountryId		= INSERTED.CountryId
		AND	[ante].[PricingPeersCompositionCountry].CalDateKey		= INSERTED.CalDateKey
		AND	[ante].[PricingPeersCompositionCountry].ComponentId		= INSERTED.ComponentId
		AND	[ante].[PricingPeersCompositionCountry].CurrencyRpt		= INSERTED.CurrencyRpt
		AND	[ante].[PricingPeersCompositionCountry].AccountId		= INSERTED.AccountId;

END;