﻿CREATE TABLE [ante].[EngrConstEstimateBase] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [BaseYear]       SMALLINT           NOT NULL,
    [ENC_Cur]        REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EngrConstEstimateBase_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EngrConstEstimateBase_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EngrConstEstimateBase_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EngrConstEstimateBase_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EngrConstEstimateBase] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [ComponentId] ASC, [BaseYear] ASC),
    CONSTRAINT [FK_EngrConstEstimateBase_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_EngrConstEstimateBase_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_EngrConstEstimateBase_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_EngrConstEstimateBase_u]
	ON [ante].[EngrConstEstimateBase]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EngrConstEstimateBase]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EngrConstEstimateBase].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[EngrConstEstimateBase].CurrencyRpt	= INSERTED.CurrencyRpt
		AND	[ante].[EngrConstEstimateBase].ComponentId	= INSERTED.ComponentId
		AND	[ante].[EngrConstEstimateBase].BaseYear		= INSERTED.BaseYear;

END;