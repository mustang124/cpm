﻿CREATE TABLE [ante].[StandardEnergyComponents] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [Energy_BTUlb]   REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyComponents_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyComponents_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyComponents_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyComponents_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyComponents] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_StandardEnergyComponents_EnergySeparation_BTUlb] CHECK ([Energy_BTUlb]>=(0.0)),
    CONSTRAINT [FK_StandardEnergyComponents_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StandardEnergyComponents_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_StandardEnergyComponents_u]
	ON [ante].[StandardEnergyComponents]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[StandardEnergyComponents]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[StandardEnergyComponents].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[StandardEnergyComponents].ComponentId	= INSERTED.ComponentId;

END;
