﻿CREATE VIEW ante.PricingPeersCompositionCountryAgg
WITH SCHEMABINDING
AS
SELECT
	c.FactorSetId,
	c.CountryId,
	c.CalDateKey,
	'LogisticsTransportAdj'			[AccountId],
	c.CurrencyRpt,
	c.[OverRide_RegionId],
	c.StreamId,
	c.ComponentId,
	SUM(c.Adj_Proximity_Cur)			[Adj_Proximity_Cur]
FROM ante.PricingPeersCompositionCountry c
GROUP BY
	c.FactorSetId,
	c.CountryId,
	c.CalDateKey,
	c.CurrencyRpt,
	c.[OverRide_RegionId],
	c.StreamId,
	c.ComponentId;
