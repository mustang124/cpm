﻿CREATE FUNCTION [Ranking].[RankAndTile](@RankData Ranking.RankData READONLY, @HigherIsBetter Bit, @ForceTiles tinyint = NULL)
RETURNS @RankWork TABLE (Refnum varchar(18) NOT NULL, RankValue real NOT NULL, PcntFactor real NOT NULL, CompanyId varchar(42) NULL, Rank smallint NULL, Percentile real NULL, Tile tinyint NULL)
AS
BEGIN

INSERT @RankWork (Refnum, RankValue, PcntFactor, Rank)
SELECT Refnum, RankValue, PcntFactor, RANK() OVER (ORDER BY RankValue * CASE WHEN @HigherIsBetter = 1 THEN -1 ELSE 1 END)
FROM @RankData ORDER BY RankValue*CASE WHEN @HigherIsBetter = 1 THEN -1 ELSE 1 END

UPDATE rw
SET CompanyId = t.CompanyId
FROM @RankWork rw INNER JOIN fact.TSort t ON t.Refnum = rw.Refnum

DECLARE @SumPcntFactor float; SELECT @SumPcntFactor = SUM(PcntFactor) FROM @RankWork;

UPDATE rw
SET Percentile = 100-(ISNULL((SELECT SUM(PcntFactor) FROM @RankWork b WHERE b.Rank < rw.Rank),0) + PcntFactor/2.0)/@SumPcntFactor*100
FROM @RankWork rw 


DECLARE @Tiles tinyint, @MaxTiles tinyint, @MinTiles tinyint
IF @ForceTiles > 0
	SELECT @Tiles = @ForceTiles, @MinTiles = @ForceTiles
ELSE
	SELECT @Tiles = 4, @MinTiles = 1

DECLARE @TilesCheck TABLE (Tile tinyint NOT NULL, CoCount int NULL, RefCount int NULL)
INSERT @TilesCheck(Tile) SELECT Tile FROM (VALUES (1),(2),(3),(4)) AS v(Tile) WHERE Tile <= @Tiles
WHILE @Tiles >= @MinTiles
BEGIN
	IF @Tiles = 1
		UPDATE @RankWork SET Tile = 1
	ELSE
		UPDATE @RankWork
		SET Tile = CASE WHEN Percentile >= 100 - 100.0/@Tiles THEN 1 
						WHEN Percentile >= 100 - 200.0/@Tiles THEN 2
						WHEN Percentile >= 100 - 300.0/@Tiles THEN 3
						ELSE @Tiles END

	IF @Tiles > @MinTiles
	BEGIN
		DELETE FROM @TilesCheck WHERE Tile > @Tiles
		UPDATE tc
		SET CoCount = (SELECT COUNT(DISTINCT CompanyId) FROM @RankWork rw WHERE rw.Tile = tc.Tile)
			, RefCount = (SELECT COUNT(DISTINCT Refnum) FROM @RankWork rw WHERE rw.Tile = tc.Tile)
		FROM @TilesCheck tc
		IF EXISTS (SELECT * FROM @TilesCheck WHERE CoCount < 2 OR RefCount < 4)
			SET @Tiles = @Tiles - 1
		ELSE
			BREAK
	END
	ELSE
		BREAK
END

RETURN

END