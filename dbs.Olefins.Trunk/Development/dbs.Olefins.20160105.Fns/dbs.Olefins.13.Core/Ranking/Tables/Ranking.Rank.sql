﻿CREATE TABLE [Ranking].[Rank] (
    [Refnum]      VARCHAR (25) NOT NULL,
    [RankSpecsId] INT          NOT NULL,
    [Rank]        SMALLINT     NULL,
    [Percentile]  REAL         NULL,
    [Tile]        TINYINT      NULL,
    [Value]       FLOAT (53)   NULL,
    CONSTRAINT [PK_Rank_1] PRIMARY KEY CLUSTERED ([Refnum] ASC, [RankSpecsId] ASC) WITH (FILLFACTOR = 70)
);

