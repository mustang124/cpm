﻿CREATE FUNCTION etl.ConvApcID
(
	@ReliabilityOppLossID	NVARCHAR(24)
)
RETURNS NVARCHAR(24)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar NVARCHAR(24) =
	CASE @ReliabilityOppLossID
		WHEN 'Tbl9020'		THEN 'ApcPyroFurnIndivid'
		WHEN 'Tbl9021'		THEN 'ApcFurnFeed'
		WHEN 'Tbl9022'		THEN 'ApcTowerQuench'
		WHEN 'Tbl9023'		THEN 'ApcTowerComp'
		WHEN 'Tbl9024'		THEN 'ApcTowerOther'
		WHEN 'Tbl9025'		THEN 'ApcTowerDepropDebut'
		WHEN 'Tbl9026'		THEN 'ApcTowerPropylene'
		WHEN 'Tbl9027'		THEN 'ApcSteamHeat'
		WHEN 'Tbl9028'		THEN 'ApcOtherOnSite'
		ELSE NULL
	END;
	
	RETURN @ResultVar;
	
END;