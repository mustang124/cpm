﻿CREATE TABLE [reports].[APCAppCount] (
    [GroupId]          VARCHAR (25)  NOT NULL,
    [PyroFurnIndivid]  REAL          NULL,
    [PyroFurn]         REAL          NULL,
    [TowerQuench]      REAL          NULL,
    [CompGas]          REAL          NULL,
    [TowerOther]       REAL          NULL,
    [TowerDepropDebut] REAL          NULL,
    [TowerPropylene]   REAL          NULL,
    [SteamHeat]        REAL          NULL,
    [OtherOnSite]      REAL          NULL,
    [Total]            REAL          NULL,
    [tsModified]       SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_APCAppCount] PRIMARY KEY CLUSTERED ([GroupId] ASC) WITH (FILLFACTOR = 70)
);

