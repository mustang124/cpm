﻿


CREATE              PROC [reports].[spCapUtil](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint

DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId
SELECT @RefCount = COUNT(*) from @MyGroup

DELETE FROM reports.CapUtil WHERE GroupId = @GroupId
Insert into reports.CapUtil (GroupId
, EDCAvg_k
, UEDCAvg_k
, UEDCAvgTAAdj_k
, EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT
, EthyleneCapYE_kMT
, PropyleneCapYE_kMT
, OlefinsCapYE_kMT
, FreshPyroFeedCapYE_kMT
, PlantFeedCapAvg_kMT
, RecycleCapAvg_kMT
, EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT
, EthyleneProd_kMT
, PropyleneProd_kMT
, OlefinsProd_kMT
, HvcProd_kMT
, EthyleneMaxProd_MTd
, PropyleneMaxProd_MTd
, OlefinsMaxProd_MTd
, ServiceFactor
, EthyleneCapUtil_pcnt
, OlefinsCapUtil_pcnt
, EthyleneCapUtilTAAdj_pcnt
, OlefinsCapUtilTAAdj_pcnt
, EthyleneCapUtilYE_pcnt
, OlefinsCapUtilYE_pcnt
, EthyleneCpbyUtil_pcnt
, OlefinsCpbyUtil_pcnt
, EthyleneCpbyUtilTAAdj_pcnt
, OlefinsCpbyUtilTAAdj_pcnt
, CapLoss_Pcnt
, HVCPerEDC
, RVPerEDC
, RVPerHVC
, FurnFeedToEthyleneRatio
, FurnFeedToProdOlefinsRatio)

SELECT GroupId = @GroupId
, EDCAvg_k				= [$(DbGlobal)].dbo.WtAvg(EDCAvg_k, 1.0)
, UEDCAvg_k				= [$(DbGlobal)].dbo.WtAvg(UEDCAvg_k, 1.0)
, UEDCAvgTAAdj_k		= [$(DbGlobal)].dbo.WtAvg(UEDCAvgTAAdj_k, 1.0)
, EthyleneCapAvg_kMT	= [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCapAvg_kMT, 1.0)
, PropyleneCapAvg_kMT	= [$(DbGlobal)].dbo.WtAvg(cu.PropyleneCapAvg_kMT, 1.0)
, OlefinsCapAvg_kMT		= [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCapAvg_kMT, 1.0)
, EthyleneCapYE_kMT		= [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCapYE_kMT, 1.0)
, PropyleneCapYE_kMT	= [$(DbGlobal)].dbo.WtAvg(cu.PropyleneCapYE_kMT, 1.0)
, OlefinsCapYE_kMT		= [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCapYE_kMT, 1.0)
, FreshPyroFeedCapYE_kMT= [$(DbGlobal)].dbo.WtAvg(cu.FreshPyroFeedCapYE_kMT, 1.0)
, PlantFeedCapAvg_kMT	= [$(DbGlobal)].dbo.WtAvg(cu.PlantFeedCapAvg_kMT, 1.0)
, RecycleCapAvg_kMT		= [$(DbGlobal)].dbo.WtAvg(cu.RecycleCapAvg_kMT, 1.0)
, EthyleneCpbyAvg_kMT	= [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCpbyAvg_kMT, 1.0)
, PropyleneCpbyAvg_kMT	= [$(DbGlobal)].dbo.WtAvg(cu.PropyleneCpbyAvg_kMT, 1.0)
, OlefinsCpbyAvg_kMT	= [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCpbyAvg_kMT, 1.0)
, EthyleneProd_kMT		= [$(DbGlobal)].dbo.WtAvg(cu.EthyleneProd_kMT, 1.0)
, PropyleneProd_kMT		= [$(DbGlobal)].dbo.WtAvg(cu.PropyleneProd_kMT, 1.0)
, OlefinsProd_kMT		= [$(DbGlobal)].dbo.WtAvg(cu.OlefinsProd_kMT, 1.0)
, HvcProd_kMT			= [$(DbGlobal)].dbo.WtAvg(cu.HvcProd_kMT, 1.0)
, EthyleneMaxProd_MTd	= [$(DbGlobal)].dbo.WtAvg(cu.EthyleneMaxProd_MTd, 1.0)
, PropyleneMaxProd_MTd	= [$(DbGlobal)].dbo.WtAvg(cu.PropyleneMaxProd_MTd, 1.0)
, OlefinsMaxProd_MTd	= [$(DbGlobal)].dbo.WtAvg(cu.OlefinsMaxProd_MTd, 1.0)
, ServiceFactor			= [$(DbGlobal)].dbo.WtAvg(cu.ServiceFactor, cu.EthyleneCapAvg_kMT)
, EthyleneCapUtil_pcnt	= [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCapUtil_pcnt, cu.EthyleneCapAvg_kMT)
, OlefinsCapUtil_pcnt	= [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCapUtil_pcnt, cu.OlefinsCapAvg_kMT)
, EthyleneCapUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCapUtilTAAdj_pcnt, cu.EthyleneCapAvg_kMT)
, OlefinsCapUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCapUtilTAAdj_pcnt, cu.OlefinsCapAvg_kMT)
, EthyleneCapUtilYE_pcnt = [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCapUtilYE_pcnt, cu.EthyleneCapYE_kMT)
, OlefinsCapUtilYE_pcnt = [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCapUtilYE_pcnt, cu.OlefinsCapYE_kMT)
, EthyleneCpbyUtil_pcnt = [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCpbyUtil_pcnt, cu.EthyleneCpbyAvg_kMT)
, OlefinsCpbyUtil_pcnt	= [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCpbyUtil_pcnt, cu.OlefinsCpbyAvg_kMT)
, EthyleneCpbyUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(cu.EthyleneCpbyUtilTAAdj_pcnt, cu.EthyleneCpbyAvg_kMT)
, OlefinsCpbyUtilTAAdj_pcnt = [$(DbGlobal)].dbo.WtAvg(cu.OlefinsCpbyUtilTAAdj_pcnt, cu.OlefinsCpbyAvg_kMT)
, CapLoss_Pcnt			= [$(DbGlobal)].dbo.WtAvg(cu.CapLoss_Pcnt, cu.EthyleneCapAvg_kMT)
, HVCPerEDC				= [$(DbGlobal)].dbo.WtAvg(cu.HVCPerEDC, cu.EDCAvg_k)
, RVPerEDC				= [$(DbGlobal)].dbo.WtAvg(cu.RVPerEDC, cu.EDCAvg_k)
, RVPerHVC				= [$(DbGlobal)].dbo.WtAvg(cu.RVPerHVC, d.HvcProd_kMT)
, FurnFeedToEthyleneRatio = [$(DbGlobal)].dbo.WtAvg(cu.FurnFeedToEthyleneRatio, cu.EthyleneCapAvg_kMT)
, FurnFeedToProdOlefinsRatio = [$(DbGlobal)].dbo.WtAvg(cu.FurnFeedToProdOlefinsRatio, cu.OlefinsCapAvg_kMT)
FROM @MyGroup r JOIN dbo.CapUtil cu on cu.Refnum=r.Refnum
JOIN dbo.Divisors d on d.Refnum=r.Refnum

SET NOCOUNT OFF
