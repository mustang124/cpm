﻿





CREATE              PROC [reports].[spEnergy](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId


DELETE FROM reports.EnergyRaw WHERE GroupId = @GroupId

INSERT INTO reports.EnergyRaw (GroupId, Currency, Scenario, DataType
, EII, EII_StdEnergy, EEI_SPSL, EEI_SPSL_StdEnergy, EEI_PYPS, EEI_PYPS_StdEnergy
,PurFuelNatural,PurSteamSHP, PurSteamHP,PurSteamIP,PurSteamLP,PurElec,PurOther,PurchasedEnergy
,PPFCFuelGas,PPFCEthane,PPFCPropane,PPFCOther,PPFC
,TotConsumption
,ExportSteamSHP,ExportSteamHP,ExportSteamIP,ExportSteamLP,ExportElectric,ExportCredits
,NetEnergyCons
,SeparationEnergy_Supp_MBtu
,EIIStdPerUEDC
,PurFuelNatural_CostPer,PurSteamSHP_CostPer,PurSteamHP_CostPer,PurSteamIP_CostPer,PurSteamLP_CostPer,PurElec_CostPerkWh,PurOther_CostPer, PurTot_CostPer
,PPFCFuelGas_CostPer,PPFCOther_CostPer,PPFC_CostPer
,TotConsumption_CostPer
,ExportSteamSHP_CostPer,ExportSteamHP_CostPer,ExportSteamIP_CostPer,ExportSteamLP_CostPer,ExportElectric_CostPerkWh,ExportTot_CostPer
,NetEnergyCons_CostPer
,PyroFurnInletTemp_C
,PyroFurnInletTemp_F
,PyroFurnPreheat_Pcnt
,PyroFurnFlueGasTemp_C
,PyroFurnFlueGasTemp_F
,PyroFurnFlueGasO2_Pcnt
,PyroFurnPrimaryTLE_Pcnt
,PyroFurnSecondaryTLE_Pcnt
,PyroFurnTLESteam_PSIa
,TLEOutletTemp_C
,TLEOutletTemp_F
,CoolingWater_MTPerHVCMT
,CoolingWaterDelta_C
,CoolingWaterDelta_F
,CoolingWaterEnergy_BtuLbHVC
,CoolingWaterEnergy_GJMTHVC
,FinFanEnergy_BtuLb
,FinFanEnergy_GJMT
,CompGasEfficiency_Pcnt
,EffCalcCompany_Pcnt
,EffCalcAPI_Pcnt
,SteamExtraction_MtMtHVC
,SteamCondensing_MtMtHVC
,ProcessedHeatExchanged_BTULbHVC
,ProcessedHeatExchanged_GJMtHVC
,QuenchHeatMatl_Pcnt
,QuenchDilution_Pcnt
,PurFuelNatural_cnt
,PurEthane_cnt
,PurPropane_cnt
,PurResidual_cnt
,PurOther_cnt
,PurSteamSHP_cnt
,PurSteamHP_cnt
,PurSteamIP_cnt
,PurSteamLP_cnt
,PurElec_cnt
,PPFCFuelGas_cnt
,PPFCOther_cnt
,ExportSteamSHP_cnt
,ExportSteamHP_cnt
,ExportSteamIP_cnt
,ExportSteamLP_cnt
,ExportElectric_cnt
,EnergyDeteriation_Pcnt
,IBSLSteamRequirement_Pcnt
,DivisorValue
,DivisorAvgValue
)

SELECT @GroupId, e.Currency, 'BASE', e.DataType 
, EII = [$(DbGlobal)].dbo.WtAvg(EII, EII_StdEnergy)
, EII_StdEnergy = [$(DbGlobal)].dbo.WtAvg(EII_StdEnergy, 1.0)
, EEI_SPSL = [$(DbGlobal)].dbo.WtAvg(EEI_SPSL, EEI_SPSL_StdEnergy)           
, EEI_SPSL_StdEnergy = [$(DbGlobal)].dbo.WtAvg(EEI_SPSL_StdEnergy, 1.0)           
, EEI_PYPS = [$(DbGlobal)].dbo.WtAvg(EEI_PYPS, EEI_PYPS_StdEnergy)				
, EEI_PYPS_StdEnergy = [$(DbGlobal)].dbo.WtAvg(EEI_PYPS_StdEnergy, 1.0)				
, PurFuelNatural = [$(DbGlobal)].dbo.WtAvg(PurFuelNatural, e.DivisorValue)
, PurSteamSHP = [$(DbGlobal)].dbo.WtAvg(PurSteamSHP, e.DivisorValue)
, PurSteamHP = [$(DbGlobal)].dbo.WtAvg(PurSteamHP, e.DivisorValue)
, PurSteamIP = [$(DbGlobal)].dbo.WtAvg(PurSteamIP, e.DivisorValue)
, PurSteamLP = [$(DbGlobal)].dbo.WtAvg(PurSteamLP, e.DivisorValue)
, PurElec = [$(DbGlobal)].dbo.WtAvg(PurElec, e.DivisorValue)
, PurOther = [$(DbGlobal)].dbo.WtAvg(PurOther, e.DivisorValue)
, PurchasedEnergy = [$(DbGlobal)].dbo.WtAvg(PurchasedEnergy, e.DivisorValue)
, PPFCFuelGas = [$(DbGlobal)].dbo.WtAvg(PPFCFuelGas, e.DivisorValue)
, PPFCEthane = [$(DbGlobal)].dbo.WtAvg(PPFCEthane, e.DivisorValue)
, PPFCPropane = [$(DbGlobal)].dbo.WtAvg(PPFCPropane, e.DivisorValue)
, PPFCOther = [$(DbGlobal)].dbo.WtAvg(PPFCOther, e.DivisorValue)
, PPFC = [$(DbGlobal)].dbo.WtAvg(PPFC, e.DivisorValue)
, TotConsumption = [$(DbGlobal)].dbo.WtAvg(TotConsumption, e.DivisorValue)
, ExportSteamSHP = [$(DbGlobal)].dbo.WtAvg(ExportSteamSHP, e.DivisorValue)
, ExportSteamHP = [$(DbGlobal)].dbo.WtAvg(ExportSteamHP, e.DivisorValue)
, ExportSteamIP = [$(DbGlobal)].dbo.WtAvg(ExportSteamIP, e.DivisorValue)
, ExportSteamLP = [$(DbGlobal)].dbo.WtAvg(ExportSteamLP, e.DivisorValue)
, ExportElectric = [$(DbGlobal)].dbo.WtAvg(ExportElectric, e.DivisorValue)
, ExportCredits = [$(DbGlobal)].dbo.WtAvg(ExportCredits, e.DivisorValue)
, NetEnergyCons = [$(DbGlobal)].dbo.WtAvg(NetEnergyCons, e.DivisorValue)
, SeparationEnergy_Supp_MBtu = [$(DbGlobal)].dbo.WtAvg(SeparationEnergy_Supp_MBtu, e.DivisorValue)
, EIIStdPerUEDC = [$(DbGlobal)].dbo.WtAvg(EIIStdPerUEDC, d.kUEdc)
, PurFuelNatural_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PurFuelNatural_CostPer, PurFuelNatural*e.DivisorValue)
, PurSteamSHP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PurSteamSHP_CostPer, PurSteamSHP*e.DivisorValue)
, PurSteamHP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PurSteamHP_CostPer, PurSteamHP*e.DivisorValue)
, PurSteamIP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PurSteamIP_CostPer, PurSteamIP*e.DivisorValue)
, PurSteamLP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PurSteamLP_CostPer, PurSteamLP*e.DivisorValue)
, PurElec_CostPerkWh = [$(DbGlobal)].dbo.WtAvgNZ(PurElec_CostPerkWh, PurElec*e.DivisorValue)
, PurOther_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PurOther_CostPer, PurOther*e.DivisorValue)
, PurTot_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PurTot_CostPer, PurchasedEnergy*e.DivisorValue)
, PPFCFuelGas_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PPFCFuelGas_CostPer, PPFCFuelGas*e.DivisorValue)
, PPFCOther_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PPFCOther_CostPer, (ISNULL(e.PPFCEthane,0) + ISNULL(e.PPFCPropane,0) + ISNULL(e.PPFCOther,0))*e.DivisorValue)
, PPFC_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(PPFC_CostPer, PPFC*e.DivisorValue)
, TotConsumption_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(TotConsumption_CostPer, TotConsumption*e.DivisorValue)
, ExportSteamSHP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(ExportSteamSHP_CostPer, ExportSteamSHP*e.DivisorValue)
, ExportSteamHP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(ExportSteamHP_CostPer, ExportSteamHP*e.DivisorValue)
, ExportSteamIP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(ExportSteamIP_CostPer, ExportSteamIP*e.DivisorValue)
, ExportSteamLP_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(ExportSteamLP_CostPer, ExportSteamLP*e.DivisorValue)
, ExportElectric_CostPerkWh = [$(DbGlobal)].dbo.WtAvgNZ(ExportElectric_CostPerkWh, ExportElectric*e.DivisorValue)
, ExportTot_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(ExportTot_CostPer, ExportCredits*e.DivisorValue)
, NetEnergyCons_CostPer = [$(DbGlobal)].dbo.WtAvgNZ(NetEnergyCons_CostPer, NetEnergyCons*e.DivisorValue)
, PyroFurnInletTemp_C = [$(DbGlobal)].dbo.WtAvgNZ(PyroFurnInletTemp_C, d.HvcProd_kMT)
, PyroFurnInletTemp_F = [$(DbGlobal)].dbo.WtAvgNZ(PyroFurnInletTemp_F, d.HvcProd_kMT)
, PyroFurnPreheat_Pcnt = [$(DbGlobal)].dbo.WtAvg(PyroFurnPreheat_Pcnt, d.HvcProd_kMT)
, PyroFurnFlueGasTemp_C = [$(DbGlobal)].dbo.WtAvgNZ(PyroFurnFlueGasTemp_C, d.HvcProd_kMT)
, PyroFurnFlueGasTemp_F = [$(DbGlobal)].dbo.WtAvgNZ(PyroFurnFlueGasTemp_F, d.HvcProd_kMT)
, PyroFurnFlueGasO2_Pcnt = [$(DbGlobal)].dbo.WtAvgNZ(PyroFurnFlueGasO2_Pcnt, d.HvcProd_kMT)
, PyroFurnPrimaryTLE_Pcnt = [$(DbGlobal)].dbo.WtAvg(PyroFurnPrimaryTLE_Pcnt, d.HvcProd_kMT)
, PyroFurnSecondaryTLE_Pcnt = [$(DbGlobal)].dbo.WtAvg(PyroFurnSecondaryTLE_Pcnt, d.HvcProd_kMT)
, PyroFurnTLESteam_PSIa = [$(DbGlobal)].dbo.WtAvgNZ(PyroFurnTLESteam_PSIa, d.HvcProd_kMT)
, TLEOutletTemp_C = [$(DbGlobal)].dbo.WtAvgNZ(TLEOutletTemp_C, d.HvcProd_kMT)
, TLEOutletTemp_F = [$(DbGlobal)].dbo.WtAvgNZ(TLEOutletTemp_F, d.HvcProd_kMT)
, CoolingWater_MTPerHVCMT = [$(DbGlobal)].dbo.WtAvg(CoolingWater_MTPerHVCMT, d.HvcProd_kMT)
, CoolingWaterDelta_C = [$(DbGlobal)].dbo.WtAvgNZ(CoolingWaterDelta_C, d.HvcProd_kMT)
, CoolingWaterDelta_F = [$(DbGlobal)].dbo.WtAvgNZ(CoolingWaterDelta_F, d.HvcProd_kMT)
, CoolingWaterEnergy_BtuLbHVC = [$(DbGlobal)].dbo.WtAvgNZ(CoolingWaterEnergy_BtuLbHVC, d.HvcProd_kMT)
, CoolingWaterEnergy_GJMTHVC = [$(DbGlobal)].dbo.WtAvgNZ(CoolingWaterEnergy_GJMTHVC, d.HvcProd_kMT)
, FinFanEnergy_BtuLb = [$(DbGlobal)].dbo.WtAvgNZ(FinFanEnergy_BtuLb, d.HvcProd_kMT)
, FinFanEnergy_GJMT = [$(DbGlobal)].dbo.WtAvgNZ(FinFanEnergy_GJMT, d.HvcProd_kMT)
, CompGasEfficiency_Pcnt = [$(DbGlobal)].dbo.WtAvgNZ(CompGasEfficiency_Pcnt, d.HvcProd_kMT)
, EffCalcCompany_Pcnt = [$(DbGlobal)].dbo.WtAvg(EffCalcCompany_Pcnt, 1.0)
, EffCalcAPI_Pcnt = [$(DbGlobal)].dbo.WtAvg(EffCalcAPI_Pcnt, 1.0)
, SteamExtraction_MtMtHVC = [$(DbGlobal)].dbo.WtAvgNZ(SteamExtraction_MtMtHVC, d.HvcProd_kMT)
, SteamCondensing_MtMtHVC = [$(DbGlobal)].dbo.WtAvgNZ(SteamCondensing_MtMtHVC, d.HvcProd_kMT)
, ProcessedHeatExchanged_BTULbHVC = [$(DbGlobal)].dbo.WtAvgNZ(ProcessedHeatExchanged_BTULbHVC, d.HvcProd_kMT)
, ProcessedHeatExchanged_GJMtHVC = [$(DbGlobal)].dbo.WtAvgNZ(ProcessedHeatExchanged_GJMtHVC, d.HvcProd_kMT)
, QuenchHeatMatl_Pcnt = [$(DbGlobal)].dbo.WtAvgNN(QuenchHeatMatl_Pcnt, d.HvcProd_kMT)
, QuenchDilution_Pcnt = [$(DbGlobal)].dbo.WtAvgNN(QuenchDilution_Pcnt, d.HvcProd_kMT)
, PurFuelNatural_cnt = SUM(PurFuelNatural_cnt)
, PurEthane_cnt = SUM(PurEthane_cnt)
, PurPropane_cnt = SUM(PurPropane_cnt)
, PurResidual_cnt = SUM(PurResidual_cnt)
, PurOther_cnt = SUM(PurOther_cnt)
, PurSteamSHP_cnt = SUM(PurSteamSHP_cnt)
, PurSteamHP_cnt = SUM(PurSteamHP_cnt)
, PurSteamIP_cnt = SUM(PurSteamIP_cnt)
, PurSteamLP_cnt = SUM(PurSteamLP_cnt)
, PurElec_cnt = SUM(PurElec_cnt)
, PPFCFuelGas_cnt = SUM(PPFCFuelGas_cnt)
, PPFCOther_cnt = SUM(PPFCOther_cnt)
, ExportSteamSHP_cnt = SUM(ExportSteamSHP_cnt)
, ExportSteamHP_cnt = SUM(ExportSteamHP_cnt)
, ExportSteamIP_cnt = SUM(ExportSteamIP_cnt)
, ExportSteamLP_cnt = SUM(ExportSteamLP_cnt)
, ExportElectric_cnt = SUM(ExportElectric_cnt)
, EnergyDeteriation_Pcnt = [$(DbGlobal)].dbo.WtAvg(EnergyDeteriation_Pcnt, d.HvcProd_kMT)
, IBSLSteamRequirement_Pcnt = [$(DbGlobal)].dbo.WtAvg(IBSLSteamRequirement_Pcnt, d.HvcProd_kMT)
, DivisorValue = SUM(DivisorValue)
, DivisorAvgValue = [$(DbGlobal)].dbo.WtAvg(DivisorAvgValue, 1.0)
FROM @MyGroup r JOIN dbo.Energy e on e.Refnum=r.Refnum
INNER JOIN dbo.Divisors d on d.Refnum=r.Refnum
GROUP BY e.Currency, e.DataType

SET NOCOUNT OFF
