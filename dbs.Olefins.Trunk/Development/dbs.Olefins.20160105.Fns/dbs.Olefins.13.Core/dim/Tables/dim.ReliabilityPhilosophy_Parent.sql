﻿CREATE TABLE [dim].[ReliabilityPhilosophy_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [PhilosophyId]   CHAR (1)            NOT NULL,
    [ParentId]       CHAR (1)            NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_ReliabilityPhilosophy_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityPhilosophy_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPhilosophy_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPhilosophy_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPhilosophy_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityPhilosophy_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PhilosophyId] ASC),
    CONSTRAINT [CR_ReliabilityPhilosophy_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ReliabilityPhilosophy_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityPhilosophy_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ReliabilityPhilosophy_LookUp] ([PhilosophyId]),
    CONSTRAINT [FK_ReliabilityPhilosophy_Parent_LookUp_Philosophy] FOREIGN KEY ([PhilosophyId]) REFERENCES [dim].[ReliabilityPhilosophy_LookUp] ([PhilosophyId]),
    CONSTRAINT [FK_ReliabilityPhilosophy_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ReliabilityPhilosophy_Parent] ([FactorSetId], [PhilosophyId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityPhilosophy_Parent_u]
ON [dim].[ReliabilityPhilosophy_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityPhilosophy_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityPhilosophy_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityPhilosophy_Parent].[PhilosophyId]		= INSERTED.[PhilosophyId];

END;