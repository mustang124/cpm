﻿CREATE TABLE [dim].[ReliabilityPredMaintCause_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [PredMaintCauseId]   VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_ReliabilityPredMaintCause_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_ReliabilityPredMaintCause_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPredMaintCause_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPredMaintCause_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_ReliabilityPredMaintCause_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ReliabilityPredMaintCause_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PredMaintCauseId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_ReliabilityPredMaintCause_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[ReliabilityPredMaintCause_LookUp] ([PredMaintCauseId]),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Bridge_LookUp_PredMaintCause] FOREIGN KEY ([PredMaintCauseId]) REFERENCES [dim].[ReliabilityPredMaintCause_LookUp] ([PredMaintCauseId]),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [PredMaintCauseId]) REFERENCES [dim].[ReliabilityPredMaintCause_Parent] ([FactorSetId], [PredMaintCauseId]),
    CONSTRAINT [FK_ReliabilityPredMaintCause_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[ReliabilityPredMaintCause_Parent] ([FactorSetId], [PredMaintCauseId])
);


GO

CREATE TRIGGER [dim].[t_ReliabilityPredMaintCause_Bridge_u]
ON [dim].[ReliabilityPredMaintCause_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ReliabilityPredMaintCause_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityPredMaintCause_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[ReliabilityPredMaintCause_Bridge].[PredMaintCauseId]	= INSERTED.[PredMaintCauseId]
		AND	[dim].[ReliabilityPredMaintCause_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;