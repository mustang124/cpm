﻿CREATE TABLE [dim].[Margin_C2H4_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [MarginId]           VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Margin_C2H4_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Margin_C2H4_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Margin_C2H4_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Margin_C2H4_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Margin_C2H4_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Margin_C2H4_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [MarginId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Margin_C2H4_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Margin_C2H4_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_Margin_C2H4_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Margin_C2H4_Bridge_LookUp_Margin] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_Margin_C2H4_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [MarginId]) REFERENCES [dim].[Margin_C2H4_Parent] ([FactorSetId], [MarginId]),
    CONSTRAINT [FK_Margin_C2H4_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Margin_C2H4_Parent] ([FactorSetId], [MarginId])
);


GO

CREATE TRIGGER [dim].[t_Margin_C2H4_Bridge_u]
ON [dim].[Margin_C2H4_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Margin_C2H4_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Margin_C2H4_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Margin_C2H4_Bridge].[MarginId]		= INSERTED.[MarginId]
		AND	[dim].[Margin_C2H4_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;