﻿CREATE TABLE [dim].[FeedStockDistMethod_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [DistMethodId]       VARCHAR (8)         NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (8)         NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_FeedStockDistMethod_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_FeedStockDistMethod_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistMethod_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistMethod_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_FeedStockDistMethod_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_FeedStockDistMethod_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [DistMethodId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_FeedStockDistMethod_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_FeedStockDistMethod_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[FeedStockDistMethod_LookUp] ([DistMethodId]),
    CONSTRAINT [FK_FeedStockDistMethod_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_FeedStockDistMethod_Bridge_LookUp_FeedStockDistMethod] FOREIGN KEY ([DistMethodId]) REFERENCES [dim].[FeedStockDistMethod_LookUp] ([DistMethodId]),
    CONSTRAINT [FK_FeedStockDistMethod_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [DistMethodId]) REFERENCES [dim].[FeedStockDistMethod_Parent] ([FactorSetId], [DistMethodId]),
    CONSTRAINT [FK_FeedStockDistMethod_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[FeedStockDistMethod_Parent] ([FactorSetId], [DistMethodId])
);


GO

CREATE TRIGGER [dim].[t_FeedStockDistMethod_Bridge_u]
ON [dim].[FeedStockDistMethod_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[FeedStockDistMethod_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedStockDistMethod_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[FeedStockDistMethod_Bridge].[DistMethodId]	= INSERTED.[DistMethodId]
		AND	[dim].[FeedStockDistMethod_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;