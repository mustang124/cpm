﻿CREATE TABLE [dim].[FeedSelModelLocation_LookUp] (
    [LocationId]     CHAR (1)           NOT NULL,
    [LocationName]   NVARCHAR (84)      NOT NULL,
    [LocationDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelModelLocation_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelLocation_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelLocation_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelLocation_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FeedSelModelLocation_LookUp] PRIMARY KEY CLUSTERED ([LocationId] ASC),
    CONSTRAINT [CL_FeedSelModelLocation_LookUp_LocationDetail] CHECK ([LocationDetail]<>''),
    CONSTRAINT [CL_FeedSelModelLocation_LookUp_LocationId] CHECK ([LocationId]<>''),
    CONSTRAINT [CL_FeedSelModelLocation_LookUp_LocationName] CHECK ([LocationName]<>''),
    CONSTRAINT [UK_FeedSelModelLocation_LookUp_LocationDetail] UNIQUE NONCLUSTERED ([LocationDetail] ASC),
    CONSTRAINT [UK_FeedSelModelLocation_LookUp_LocationName] UNIQUE NONCLUSTERED ([LocationName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FeedSelModelLocation_LookUp_u]
	ON [dim].[FeedSelModelLocation_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FeedSelModelLocation_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FeedSelModelLocation_LookUp].[LocationId]	= INSERTED.[LocationId];
		
END;
