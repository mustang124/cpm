﻿CREATE TABLE [dim].[Study_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [StudyId]            VARCHAR (4)         NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (4)         NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Study_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Study_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Study_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Study_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Study_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Study_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StudyId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Study_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Study_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Study_LookUp] ([StudyId]),
    CONSTRAINT [FK_Study_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Study_Bridge_LookUp_Study] FOREIGN KEY ([StudyId]) REFERENCES [dim].[Study_LookUp] ([StudyId]),
    CONSTRAINT [FK_Study_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [StudyId]) REFERENCES [dim].[Study_Parent] ([FactorSetId], [StudyId]),
    CONSTRAINT [FK_Study_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Study_Parent] ([FactorSetId], [StudyId])
);


GO

CREATE TRIGGER [dim].[t_Study_Bridge_u]
ON [dim].[Study_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Study_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Study_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Study_Bridge].[StudyId]	= INSERTED.[StudyId]
		AND	[dim].[Study_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;
