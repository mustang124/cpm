﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class General
			{
				private static RangeReference EthyleneCarrier_YN = new RangeReference(Tabs.T08_02, 7, 9, SqlDbType.VarChar);
				private static RangeReference ExtractButadiene_YN = new RangeReference(Tabs.T08_02, 8, 9, SqlDbType.VarChar);
				private static RangeReference ExtractAromatics_YN = new RangeReference(Tabs.T08_02, 9, 9, SqlDbType.VarChar);
				private static RangeReference MTBE_YN = new RangeReference(Tabs.T08_02, 10, 9, SqlDbType.VarChar);
				private static RangeReference Isobutylene_YN = new RangeReference(Tabs.T08_02, 11, 9, SqlDbType.VarChar);
				private static RangeReference IsobutyleneHighPurity_YN = new RangeReference(Tabs.T08_02, 12, 9, SqlDbType.VarChar);
				private static RangeReference Butene1_YN = new RangeReference(Tabs.T08_02, 13, 9, SqlDbType.VarChar);
				private static RangeReference Isoprene_YN = new RangeReference(Tabs.T08_02, 14, 9, SqlDbType.VarChar);
				private static RangeReference CycloPentadiene_YN = new RangeReference(Tabs.T08_02, 15, 9, SqlDbType.VarChar);
				private static RangeReference PolyPlantShare_YN = new RangeReference(Tabs.T08_02, 17, 9, SqlDbType.VarChar);
				private static RangeReference OlefinsDerivatives_YN = new RangeReference(Tabs.T08_02, 19, 9, SqlDbType.VarChar);
				private static RangeReference IntegrationRefinery_YN = new RangeReference(Tabs.T08_02, 20, 9, SqlDbType.VarChar);

				private static RangeReference CapCreepAnn_Pcnt = new RangeReference(Tabs.T08_02, 58, 8, SqlDbType.Float, 100.0);

				private static RangeReference OSIMPrep_Hrs = new RangeReference(Tabs.T08_04, 10, 5, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlant]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("EthyleneCarrier_YN", General.EthyleneCarrier_YN);
							d.Add("ExtractButadiene_YN", General.ExtractButadiene_YN);
							d.Add("ExtractAromatics_YN", General.ExtractAromatics_YN);
							d.Add("MTBE_YN", General.MTBE_YN);
							d.Add("Isobutylene_YN", General.Isobutylene_YN);
							d.Add("IsobutyleneHighPurity_YN", General.IsobutyleneHighPurity_YN);
							d.Add("Butene1_YN", General.Butene1_YN);
							d.Add("Isoprene_YN", General.Isoprene_YN);
							d.Add("CycloPentadiene_YN", General.CycloPentadiene_YN);
							d.Add("PolyPlantShare_YN", General.PolyPlantShare_YN);
							d.Add("OlefinsDerivatives_YN", General.OlefinsDerivatives_YN);
							d.Add("IntegrationRefinery_YN", General.IntegrationRefinery_YN);

							d.Add("CapCreepAnn_Pcnt", General.CapCreepAnn_Pcnt);

							d.Add("OSIMPrep_Hrs", General.OSIMPrep_Hrs);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}