﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class CapEx
			{
				private static RangeReference Onsite = new RangeReference(Tabs.T08_03, 39, 7, SqlDbType.Float);
				private static RangeReference Offsite = new RangeReference(Tabs.T08_03, 40, 7, SqlDbType.Float);
				private static RangeReference Exist = new RangeReference(Tabs.T08_03, 41, 7, SqlDbType.Float);
				private static RangeReference EnergyCons = new RangeReference(Tabs.T08_03, 42, 7, SqlDbType.Float);
				private static RangeReference RegEnvir = new RangeReference(Tabs.T08_03, 43, 7, SqlDbType.Float);
				private static RangeReference Safety = new RangeReference(Tabs.T08_03, 44, 7, SqlDbType.Float);
				private static RangeReference Computer = new RangeReference(Tabs.T08_03, 45, 7, SqlDbType.Float);
				private static RangeReference MaintCap = new RangeReference(Tabs.T08_03, 46, 7, SqlDbType.Float);
				private static RangeReference Other = new RangeReference(Tabs.T08_03, 47, 7, SqlDbType.Float);

				private static RangeReference Amount_Cur = new RangeReference(Tabs.T08_03, 0, 7, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantCapEx]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "AccountID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Amount_Cur", CapEx.Amount_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Onsite", CapEx.Onsite);
							d.Add("Offsite", CapEx.Offsite);
							d.Add("Exist", CapEx.Exist);
							d.Add("EnergyCons", CapEx.EnergyCons);
							d.Add("RegEnvir", CapEx.RegEnvir);
							d.Add("Safety", CapEx.Safety);
							d.Add("Computer", CapEx.Computer);
							d.Add("MaintCap", CapEx.MaintCap);
							d.Add("OthCapEx", CapEx.Other);

							return d;
						}
					}
				}
			}
		}
	}
}