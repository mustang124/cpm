﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class Inventory
			{
				private static RangeReference StoreFeedAbove = new RangeReference(Tabs.T08_01, 15, 0);
				private static RangeReference StoreFeedUnder = new RangeReference(Tabs.T08_01, 16, 0);
				private static RangeReference StoreInter = new RangeReference(Tabs.T08_01, 17, 0);
				private static RangeReference StoreProdAbove = new RangeReference(Tabs.T08_01, 19, 0);
				private static RangeReference StoreProdUnder = new RangeReference(Tabs.T08_01, 20, 0);

				private static RangeReference StorageCapacity_kMT = new RangeReference(Tabs.T08_01, 0, 7, SqlDbType.Float);
				private static RangeReference StorageLevel_Pcnt = new RangeReference(Tabs.T08_01, 0, 8, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantInventory]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "FacilityID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("StorageCapacity_kMT", Inventory.StorageCapacity_kMT);
							d.Add("StorageLevel_Pcnt", Inventory.StorageLevel_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("StoreFeedAbove", Inventory.StoreFeedAbove);
							d.Add("StoreFeedUnder", Inventory.StoreFeedUnder);
							d.Add("StoreInter", Inventory.StoreInter);
							d.Add("StoreProdAbove", Inventory.StoreProdAbove);
							d.Add("StoreProdUnder", Inventory.StoreProdUnder);

							return d;
						}
					}
				}
			}
		}
	}
}