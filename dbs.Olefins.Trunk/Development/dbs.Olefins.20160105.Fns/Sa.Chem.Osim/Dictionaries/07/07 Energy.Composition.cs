﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Energy
		{
			internal class Composition
			{
				private static RangeReference PurFuelNatural = new RangeReference(Tabs.T07, 7, 0);
				private static RangeReference PurEthane = new RangeReference(Tabs.T07, 8, 0);
				private static RangeReference PurPropane = new RangeReference(Tabs.T07, 9, 0);
				private static RangeReference PurButane = new RangeReference(Tabs.T07, 10, 0);
				private static RangeReference PurNaphtha = new RangeReference(Tabs.T07, 11, 0);
				private static RangeReference PurSteam = new RangeReference(Tabs.T07, 21, 0);

				private static RangeReference Component_WtPcnt = new RangeReference(Tabs.T07, 0, 6, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_EnergyComposition]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "AccountId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Component_WtPcnt", Composition.Component_WtPcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("PurFuelNatural", Composition.PurFuelNatural);
							d.Add("PurEthane", Composition.PurEthane);
							d.Add("PurPropane", Composition.PurPropane);
							d.Add("PurButane", Composition.PurButane);
							d.Add("PurNaphtha", Composition.PurNaphtha);
							d.Add("PurSteam", Composition.PurSteam);

							return d;
						}
					}
				}
			}
		}
	}
}