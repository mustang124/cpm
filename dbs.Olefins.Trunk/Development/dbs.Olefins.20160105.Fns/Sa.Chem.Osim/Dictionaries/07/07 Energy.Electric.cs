﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Energy
		{
			internal class Electric
			{
				private RangeReference Amount_MWh = new RangeReference(Tabs.T07, 0, 4, SqlDbType.Float);
				//private RangeReference Rate = new RangeReference(Tabs.T07, 0, 6, SqlDbType.Float);
				private RangeReference Price_Cur = new RangeReference(Tabs.T07, 0, 8, SqlDbType.Float);

				private Dictionary<string, RangeReference> Values
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						//d.Add("Rate_", this.Rate);
						d.Add("Amount_MWh", this.Amount_MWh);
						d.Add("Price_Cur", this.Price_Cur);

						return d;
					}
				}

				private RangeReference PurElec = new RangeReference(Tabs.T07, 24, 0);
				private RangeReference ExportElectric = new RangeReference(Tabs.T07, 45, 0);

				private Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("PurElec", this.PurElec);
						d.Add("ExportElectric", this.ExportElectric);
						
						return d;
					}
				}

				internal void CustomLoad(SqlConnection cn, Excel.Workbook wkb, string tabNamePrefix, string refnum, bool forceIn)
				{
					string sheetName = tabNamePrefix + Tabs.T07;

					if (wkb.SheetExists(sheetName))
					{
						string uomSuffix = (wkb.Worksheets["Conv"].Cells[2, 2].Value == 1) ? "Imp" : "Met";
						string storedProc = "[fact].[Select_EnergyElec]";
						string columnName = "AccountId";

						string sht;
						int row;
						int col;
						SqlDbType typ;
						double div;
						string colName;

						using (SqlCommand cmd = new SqlCommand(storedProc, cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

							using (SqlDataReader rdr = cmd.ExecuteReader())
							{
								RangeReference r;
								RangeReference rr;

								while (rdr.Read())
								{
									if (Items.TryGetValue(rdr.GetString(rdr.GetOrdinal(columnName)), out r))
									{
										foreach (KeyValuePair<string, RangeReference> entry in Values)
										{
											sht = (r.sheetSuffix != string.Empty) ? r.sheetSuffix : entry.Value.sheetSuffix;
											row = (r.row != 0) ? r.row : entry.Value.row;
											col = (r.col != 0) ? r.col : entry.Value.col;
											typ = (r.sqlType != SqlDbType.Variant) ? r.sqlType : entry.Value.sqlType;
											div = (!double.IsNaN(r.divisor)) ? r.divisor : entry.Value.divisor;

											colName = entry.Key + (entry.Key == "Rate_" ? uomSuffix : string.Empty);

											rr = new RangeReference(sht, row, col, typ, div, forceIn);

											XL.SetCellValue(rdr, colName, wkb, tabNamePrefix, rr);
										}
									}
								}
							}
						}

					}
				}

				//internal class Download : TransferData.IDownload
				//{
				//	public string StoredProcedure
				//	{
				//		get
				//		{
				//			return "[fact].[Select_EnergyElec]";
				//		}
				//	}

				//	public string LookUpColumn
				//	{
				//		get
				//		{
				//			return "AccountID";
				//		}
				//	}

				//	public Dictionary<string, RangeReference> Columns
				//	{
				//		get
				//		{
				//			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

				//			string uomSuffix = (wkb.Worksheets["Conv"].Cells[2, 2].Value == 1) ? "Imp" : "Met";

				//			d.Add("Amount_MWh", Electric.Amount_MWh);
				//			d.Add("Rate_BTUkWh", Electric.Rate_BTUkWh);
				//			d.Add("Price_Cur", Electric.Price_Cur);

				//			return d;
				//		}
				//	}

				//	public Dictionary<string, RangeReference> Items
				//	{
				//		get
				//		{
				//			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

				//			d.Add("PurElec", Electric.PurElec);
				//			d.Add("ExportElectric", Electric.ExportElectric);

				//			return d;
				//		}
				//	}
				//}
			}
		}
	}
}