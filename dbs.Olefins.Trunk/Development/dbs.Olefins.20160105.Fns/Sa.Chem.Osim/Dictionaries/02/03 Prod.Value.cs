﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Prod
			{
				internal class Value
				{
					private static RangeReference Hydrogen = new RangeReference(Tabs.T03, 14, 8, SqlDbType.Float);
					private static RangeReference ProdOther1 = new RangeReference(Tabs.T03, 49, 8, SqlDbType.Float);
					private static RangeReference ProdOther2 = new RangeReference(Tabs.T03, 50, 8, SqlDbType.Float);

					private static RangeReference Amount_Cur = new RangeReference(string.Empty, 0, 0, SqlDbType.Float);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_StreamsProdValue]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return new Streams().LookUpColumn;
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Amount_Cur", Value.Amount_Cur);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								//	Production
								d.Add("Hydrogen", Value.Hydrogen);
								d.Add("ProdOther1", Value.ProdOther1);
								d.Add("ProdOther2", Value.ProdOther2);

								return d;
							}
						}
					}
				}
			}
		}
	}
}