﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Supp
			{
				internal static RangeReference FeedProdId = new RangeReference(Tabs.Upload, string.Empty, 0, 2, SqlDbType.VarChar);

				internal class Quantity
				{
					private static RangeReference StreamDesc = new RangeReference(string.Empty, 0, 2, SqlDbType.VarChar);

					private static RangeReference Q1_kMT = new RangeReference(string.Empty, 0, 3, SqlDbType.Float);
					private static RangeReference Q2_kMT = new RangeReference(string.Empty, 0, 4, SqlDbType.Float);
					private static RangeReference Q3_kMT = new RangeReference(string.Empty, 0, 5, SqlDbType.Float);
					private static RangeReference Q4_kMT = new RangeReference(string.Empty, 0, 6, SqlDbType.Float);
					private static RangeReference QT_kMT = new RangeReference(string.Empty, 0, 7, SqlDbType.Float);

					private static RangeReference Recovered_WtPcnt = new RangeReference(string.Empty, 0, 8, SqlDbType.Float, 100.0);

					private static Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("ConcEthylene", Streams.ConcEthylene);
							d.Add("ConcPropylene", Streams.ConcPropylene);
							d.Add("ConcButadiene", Streams.ConcButadiene);
							d.Add("ConcBenzene", Streams.ConcBenzene);

							d.Add("ROGHydrogen", Streams.ROGHydrogen);
							d.Add("ROGMethane", Streams.ROGMethane);
							d.Add("ROGEthane", Streams.ROGEthane);
							d.Add("ROGEthylene", Streams.ROGEthylene);
							d.Add("ROGPropane", Streams.ROGPropane);
							d.Add("ROGPropylene", Streams.ROGPropylene);
							d.Add("ROGC4Plus", Streams.ROGC4Plus);
							d.Add("ROGInerts", Streams.ROGInerts);

							d.Add("DilHydrogen", Streams.DilHydrogen);
							d.Add("DilMethane", Streams.DilMethane);
							d.Add("DilEthane", Streams.DilEthane);
							d.Add("DilEthylene", Streams.DilEthylene);
							d.Add("DilPropane", Streams.DilPropane);
							d.Add("DilPropylene", Streams.DilPropylene);
							d.Add("DilButane", Streams.DilButane);
							d.Add("DilButylene", Streams.DilButylene);
							d.Add("DilButadiene", Streams.DilButadiene);
							d.Add("DilBenzene", Streams.DilBenzene);
							d.Add("DilMogas", Streams.DilMoGas);

							d.Add("SuppWashOil", Streams.SuppWashOil);
							d.Add("SuppGasOil", Streams.SuppGasOil);
							d.Add("SuppOther", Streams.SuppOther);

							return d;
						}
					}

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_StreamsSuppQuantity]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return new Streams().LookUpColumn;
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Q1_kMT", Quantity.Q1_kMT);
								d.Add("Q2_kMT", Quantity.Q2_kMT);
								d.Add("Q3_kMT", Quantity.Q3_kMT);
								d.Add("Q4_kMT", Quantity.Q4_kMT);
								d.Add("StreamDesc", Quantity.StreamDesc);
								d.Add("Recovered_WtPcnt", Quantity.Recovered_WtPcnt);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								return Quantity.Items;
							}
						}
					}

					internal class Upload : TransferData.IUploadMultiple
					{
						public string StoredProcedure
						{
							get
							{
								return "[stgFact].[Insert_Quantity]";
							}
						}

						public Dictionary<string, RangeReference> Parameters
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("@FeedProdID", Supp.FeedProdId);

								d.Add("@Q1Feed", Quantity.Q1_kMT);
								d.Add("@Q2Feed", Quantity.Q2_kMT);
								d.Add("@Q3Feed", Quantity.Q3_kMT);
								d.Add("@Q4Feed", Quantity.Q4_kMT);
								d.Add("@AnnFeedProd", Quantity.QT_kMT);

								d.Add("@RecPcnt", Quantity.Recovered_WtPcnt);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								return Quantity.Items;
							}
						}
					}
				}
			}
		}
	}
}