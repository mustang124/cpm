﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Supp
			{
				internal class Recycled
				{
					private static RangeReference SuppToRecEthane = new RangeReference(Tabs.T02_C, 44, 0, SqlDbType.Float, 100.0);
					private static RangeReference SuppToRecPropane = new RangeReference(Tabs.T02_C, 45, 0, SqlDbType.Float, 100.0);
					private static RangeReference SuppToRecButane = new RangeReference(Tabs.T02_C, 46, 0, SqlDbType.Float, 100.0);

					private static RangeReference Recycled_WtPcnt = new RangeReference(Tabs.T03, 0, 3, SqlDbType.Float);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_QuantitySuppRecycled]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return new Streams().LookUpColumn;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("SuppToRecButane", Recycled.SuppToRecButane);
								d.Add("SuppToRecEthane", Recycled.SuppToRecEthane);
								d.Add("SuppToRecPropane", Recycled.SuppToRecPropane);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Recycled_WtPcnt", Recycled.Recycled_WtPcnt);

								return d;
							}
						}
					}
				}
			}
		}
	}
}