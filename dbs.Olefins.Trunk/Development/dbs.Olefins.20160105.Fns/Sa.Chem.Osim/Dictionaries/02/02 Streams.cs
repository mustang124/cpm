﻿using System.Collections.Generic;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			public string LookUpColumn
			{
				get
				{
					return "StreamIdInt";
				}
			}

			//	Light
			static RangeReference Ethane = new RangeReference(Tabs.T02_A1, 0, 3);
			static RangeReference EPMix = new RangeReference(Tabs.T02_A1, 0, 4);
			static RangeReference Propane = new RangeReference(Tabs.T02_A1, 0, 5);
			static RangeReference LPG = new RangeReference(Tabs.T02_A1, 0, 6);
			static RangeReference Butane = new RangeReference(Tabs.T02_A1, 0, 7);
			static RangeReference FeedLtOther = new RangeReference(Tabs.T02_A1, 0, 8);

			//	Liqud
			static RangeReference NaphthaLt = new RangeReference(Tabs.T02_A2, 0, 3);
			static RangeReference NaphthaFr = new RangeReference(Tabs.T02_A2, 0, 4);
			static RangeReference NaphthaHv = new RangeReference(Tabs.T02_A2, 0, 5);
			static RangeReference Raffinate = new RangeReference(Tabs.T02_A2, 0, 6);
			static RangeReference HeavyNGL = new RangeReference(Tabs.T02_A2, 0, 7);
			static RangeReference Condensate = new RangeReference(Tabs.T02_A2, 0, 8);
			static RangeReference Diesel = new RangeReference(Tabs.T02_A2, 0, 9);
			static RangeReference GasOilHv = new RangeReference(Tabs.T02_A2, 0, 10);
			static RangeReference GasOilHt = new RangeReference(Tabs.T02_A2, 0, 11);
			//static RangeReference FeedLiqOther = new RangeReference(Tabs.T02_A2, 0, 12);
			static RangeReference FeedLiqOther1 = new RangeReference(Tabs.T02_A2, 0, 12);
			static RangeReference FeedLiqOther2 = new RangeReference(Tabs.T02_A2, 0, 13);
			static RangeReference FeedLiqOther3 = new RangeReference(Tabs.T02_A2, 0, 14);

			//	Recycle
			static RangeReference EthRec = new RangeReference(Tabs.T02_B, 0, 3);
			static RangeReference ProRec = new RangeReference(Tabs.T02_B, 0, 4);
			static RangeReference ButRec = new RangeReference(Tabs.T02_B, 0, 5);

			//	Supplemental
			static RangeReference ConcEthylene = new RangeReference(Tabs.T02_C, 10, 0);
			static RangeReference ConcPropylene = new RangeReference(Tabs.T02_C, 11, 0);
			static RangeReference ConcButadiene = new RangeReference(Tabs.T02_C, 12, 0);
			static RangeReference ConcBenzene = new RangeReference(Tabs.T02_C, 13, 0);

			static RangeReference ROGHydrogen = new RangeReference(Tabs.T02_C, 15, 0);
			static RangeReference ROGMethane = new RangeReference(Tabs.T02_C, 16, 0);
			static RangeReference ROGEthane = new RangeReference(Tabs.T02_C, 17, 0);
			static RangeReference ROGEthylene = new RangeReference(Tabs.T02_C, 18, 0);
			static RangeReference ROGPropane = new RangeReference(Tabs.T02_C, 19, 0);
			static RangeReference ROGPropylene = new RangeReference(Tabs.T02_C, 20, 0);
			static RangeReference ROGC4Plus = new RangeReference(Tabs.T02_C, 21, 0);
			static RangeReference ROGInerts = new RangeReference(Tabs.T02_C, 22, 0);

			static RangeReference DilHydrogen = new RangeReference(Tabs.T02_C, 25, 0);
			static RangeReference DilMethane = new RangeReference(Tabs.T02_C, 26, 0);
			static RangeReference DilEthane = new RangeReference(Tabs.T02_C, 27, 0);
			static RangeReference DilEthylene = new RangeReference(Tabs.T02_C, 28, 0);
			static RangeReference DilPropane = new RangeReference(Tabs.T02_C, 29, 0);
			static RangeReference DilPropylene = new RangeReference(Tabs.T02_C, 30, 0);
			static RangeReference DilButane = new RangeReference(Tabs.T02_C, 31, 0);
			static RangeReference DilButylene = new RangeReference(Tabs.T02_C, 32, 0);
			static RangeReference DilButadiene = new RangeReference(Tabs.T02_C, 33, 0);
			static RangeReference DilBenzene = new RangeReference(Tabs.T02_C, 34, 0);
			static RangeReference DilMoGas = new RangeReference(Tabs.T02_C, 35, 0);

			static RangeReference SuppWashOil = new RangeReference(Tabs.T02_C, 36, 0);
			static RangeReference SuppGasOil = new RangeReference(Tabs.T02_C, 37, 0);
			static RangeReference SuppOther = new RangeReference(Tabs.T02_C, 38, 0);

			//	Production
			static RangeReference Hydrogen = new RangeReference(Tabs.T03, 9, 0);
			static RangeReference Methane = new RangeReference(Tabs.T03, 16, 0);

			static RangeReference Acetylene = new RangeReference(Tabs.T03, 20, 0);
			static RangeReference EthylenePG = new RangeReference(Tabs.T03, 21, 0);
			static RangeReference EthyleneCG = new RangeReference(Tabs.T03, 22, 0);
			static RangeReference PropylenePG = new RangeReference(Tabs.T03, 23, 0);
			static RangeReference PropyleneCG = new RangeReference(Tabs.T03, 24, 0);
			static RangeReference PropyleneRG = new RangeReference(Tabs.T03, 25, 0);
			static RangeReference PropaneC3Resid = new RangeReference(Tabs.T03, 26, 0);

			static RangeReference Butadiene = new RangeReference(Tabs.T03, 29, 0);
			static RangeReference IsoButylene = new RangeReference(Tabs.T03, 28, 0);
			static RangeReference Isobutylene = new RangeReference(Tabs.T03, 30, 0);
			static RangeReference C4Oth = new RangeReference(Tabs.T03, 31, 0);

			static RangeReference Benzene = new RangeReference(Tabs.T03, 34, 0);
			static RangeReference PyroGasoline = new RangeReference(Tabs.T03, 35, 0);

			static RangeReference PyroGasOil = new RangeReference(Tabs.T03, 37, 0);
			static RangeReference PyroFuelOil = new RangeReference(Tabs.T03, 38, 0);
			static RangeReference AcidGas = new RangeReference(Tabs.T03, 39, 0);

			static RangeReference PPFC = new RangeReference(Tabs.T03, 41, 0);

			//static RangeReference ProdOther = new RangeReference(Tabs.T03, 41, 0);
			static RangeReference ProdOther1 = new RangeReference(Tabs.T03, 49, 0);
			static RangeReference ProdOther2 = new RangeReference(Tabs.T03, 50, 0);

			static RangeReference LossFlareVent = new RangeReference(Tabs.T03, 52, 0);
			static RangeReference LossOther = new RangeReference(Tabs.T03, 53, 0);
			static RangeReference LossMeasure = new RangeReference(Tabs.T03, 54, 0);
		}
	}
}