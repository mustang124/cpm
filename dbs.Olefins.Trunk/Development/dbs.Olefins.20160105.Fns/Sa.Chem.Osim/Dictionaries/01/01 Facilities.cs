﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal class Facilities
		{
			private const int colDescription = 2;
			private const int colYN = 6;
			private const int colCount = 7;
			private const int colA = 9;
			private const int colB = 12;

			private static RangeReference FracFeed_Count = new RangeReference(Tabs.T01_03, 6, colCount, SqlDbType.TinyInt);
			private static RangeReference FracFeed_Capacity = new RangeReference(Tabs.T01_03, 6, colA, SqlDbType.Float);
			private static RangeReference FracFeed_FeedType = new RangeReference(Tabs.T01_03, 6, colB, SqlDbType.VarChar);
			private static RangeReference FracFeed_ProcMT = new RangeReference(Tabs.T01_03, 7, colA, SqlDbType.Float);
			private static RangeReference FracFeed_SG = new RangeReference(Tabs.T01_03, 7, colB, SqlDbType.Float);

			private static RangeReference PyrFurn_Count = new RangeReference(Tabs.T01_03, 9, colCount, SqlDbType.TinyInt);
			private static RangeReference PyrFurn_SparePcnt = new RangeReference(Tabs.T01_03, 9, colA, SqlDbType.Float, 100.0);

			private static RangeReference QTower_Count = new RangeReference(Tabs.T01_03, 10, colCount, SqlDbType.TinyInt);
			private static RangeReference QWaterSys_Count = new RangeReference(Tabs.T01_03, 11, colCount, SqlDbType.TinyInt);
			private static RangeReference GasFrac_Count = new RangeReference(Tabs.T01_03, 12, colCount, SqlDbType.TinyInt);
			private static RangeReference ProcDryer_Count = new RangeReference(Tabs.T01_03, 13, colCount, SqlDbType.TinyInt);

			private static RangeReference CompGas_Count = new RangeReference(Tabs.T01_03, 15, colCount, SqlDbType.TinyInt);
			private static RangeReference CompGas_Stages = new RangeReference(Tabs.T01_03, 15, colA, SqlDbType.TinyInt);
			private static RangeReference CompGas_Age = new RangeReference(Tabs.T01_03, 16, colA, SqlDbType.Float);
			private static RangeReference CompGas_Power = new RangeReference(Tabs.T01_03, 16, colB, SqlDbType.Float);
			private static RangeReference CompGas_Coating = new RangeReference(Tabs.T01_03, 17, colA, SqlDbType.VarChar);
			private static RangeReference CompGas_Driver = new RangeReference(Tabs.T01_03, 17, colB, SqlDbType.VarChar);

			private static RangeReference TowerCaustic_Count = new RangeReference(Tabs.T01_03, 19, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerMethSys_Count = new RangeReference(Tabs.T01_03, 20, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerDemethanizer_Count = new RangeReference(Tabs.T01_03, 21, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerDemethanizer_Pressure = new RangeReference(Tabs.T01_03, 21, colA, SqlDbType.Float);
			private static RangeReference TowerDeethanizer_Count = new RangeReference(Tabs.T01_03, 22, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerDeethanize_Pressure = new RangeReference(Tabs.T01_03, 22, colA, SqlDbType.Float);


			//private static RangeReference ConvertersAce_YN = new RangeReference(Tabs.T01_03, 23, colYN, SqlDbType.VarChar);
			//private static RangeReference ConvertersAce_Count = new RangeReference(Tabs.T01_03, 23, colCount, SqlDbType.TinyInt);
			private static RangeReference ConvertersAce_Loc = new RangeReference(Tabs.T01_03, 24, colA, SqlDbType.Float);
			private static RangeReference ConvertersAce_Feed = new RangeReference(Tabs.T01_03, 25, colA, SqlDbType.Float);
			private static RangeReference ConvertersAce_Selectivity = new RangeReference(Tabs.T01_03, 26, colA, SqlDbType.Float);

			private static RangeReference ConvertersMAPD_YN = new RangeReference(Tabs.T01_03, 27, colYN, SqlDbType.VarChar);
			private static RangeReference ConvertersMAPD_Count = new RangeReference(Tabs.T01_03, 27, colCount, SqlDbType.TinyInt);
			private static RangeReference ConvertersMAPD_Feed = new RangeReference(Tabs.T01_03, 28, colA, SqlDbType.Float);
			private static RangeReference ConvertersMAPD_Selectivity = new RangeReference(Tabs.T01_03, 29, colA, SqlDbType.Float);

			private static RangeReference TowerEthylene_Count = new RangeReference(Tabs.T01_03, 30, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerEthylenee_Pressure = new RangeReference(Tabs.T01_03, 30, colA, SqlDbType.Float);
			private static RangeReference TowerDepropanizer_Count = new RangeReference(Tabs.T01_03, 31, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerDepropanizer_Pressure = new RangeReference(Tabs.T01_03, 31, colA, SqlDbType.Float);
			private static RangeReference TowerPropylene_YN = new RangeReference(Tabs.T01_03, 32, colYN, SqlDbType.VarChar);
			private static RangeReference TowerPropylene_Count = new RangeReference(Tabs.T01_03, 32, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerPropylene_Pressure = new RangeReference(Tabs.T01_03, 32, colA, SqlDbType.Float);

			private static RangeReference TowerDebutanizer_YN = new RangeReference(Tabs.T01_03, 33, colYN, SqlDbType.VarChar);
			private static RangeReference TowerDebutanizer_Count = new RangeReference(Tabs.T01_03, 33, colCount, SqlDbType.TinyInt);

			private static RangeReference CompRefrigPropylene_YN = new RangeReference(Tabs.T01_03, 35, colYN, SqlDbType.VarChar);
			private static RangeReference CompRefrigPropylene_Count = new RangeReference(Tabs.T01_03, 35, colCount, SqlDbType.TinyInt);
			private static RangeReference CompRefrigPropylene_Age = new RangeReference(Tabs.T01_03, 35, colA, SqlDbType.Float);
			private static RangeReference CompRefrigPropylene_Power = new RangeReference(Tabs.T01_03, 35, colB, SqlDbType.Float);
			private static RangeReference CompRefrigPropylene_Coating = new RangeReference(Tabs.T01_03, 36, colA, SqlDbType.VarChar);
			private static RangeReference CompRefrigPropylene_Driver = new RangeReference(Tabs.T01_03, 36, colB, SqlDbType.VarChar);

			private static RangeReference CompRefrigEthylene_YN = new RangeReference(Tabs.T01_03, 38, colYN, SqlDbType.VarChar);
			private static RangeReference CompRefrigEthylene_Count = new RangeReference(Tabs.T01_03, 38, colCount, SqlDbType.TinyInt);
			private static RangeReference CompRefrigEthylene_Age = new RangeReference(Tabs.T01_03, 38, colA, SqlDbType.Float);
			private static RangeReference CompRefrigEthylene_Power = new RangeReference(Tabs.T01_03, 38, colB, SqlDbType.Float);
			private static RangeReference CompRefrigEthylene_Coating = new RangeReference(Tabs.T01_03, 39, colA, SqlDbType.VarChar);
			private static RangeReference CompRefrigEthylene_Driver = new RangeReference(Tabs.T01_03, 39, colB, SqlDbType.VarChar);

			private static RangeReference CompRefrigMethane_YN = new RangeReference(Tabs.T01_03, 41, colYN, SqlDbType.VarChar);
			private static RangeReference CompRefrigMethane_Count = new RangeReference(Tabs.T01_03, 41, colCount, SqlDbType.TinyInt);
			private static RangeReference CompRefrigMethane_Age = new RangeReference(Tabs.T01_03, 41, colA, SqlDbType.Float);
			private static RangeReference CompRefrigMethane_Power = new RangeReference(Tabs.T01_03, 41, colB, SqlDbType.Float);
			private static RangeReference CompRefrigMethane_Coating = new RangeReference(Tabs.T01_03, 42, colA, SqlDbType.VarChar);
			private static RangeReference CompRefrigMethane_Driver = new RangeReference(Tabs.T01_03, 42, colB, SqlDbType.VarChar);




			private static RangeReference CompRefrigBinary_YN = new RangeReference(Tabs.T01_03, 44, colYN, SqlDbType.VarChar);
			private static RangeReference CompRefrigBinary_Count = new RangeReference(Tabs.T01_03, 44, colCount, SqlDbType.TinyInt);
			private static RangeReference CompRefrigBinary_Type = new RangeReference(Tabs.T01_03, 44, colA, SqlDbType.VarChar);
			private static RangeReference CompRefrigBinary_Refrg = new RangeReference(Tabs.T01_03, 44, colB, SqlDbType.VarChar);
			private static RangeReference CompRefrigBinary_Age = new RangeReference(Tabs.T01_03, 45, colA, SqlDbType.Float);
			private static RangeReference CompRefrigBinary_Power = new RangeReference(Tabs.T01_03, 45, colB, SqlDbType.Float);
			private static RangeReference CompRefrigBinary_Coating = new RangeReference(Tabs.T01_03, 46, colA, SqlDbType.VarChar);
			private static RangeReference CompRefrigBinary_Driver = new RangeReference(Tabs.T01_03, 46, colB, SqlDbType.VarChar);

			private static RangeReference CompRefrigHMTurb_YN = new RangeReference(Tabs.T01_03, 48, colYN, SqlDbType.VarChar);
			private static RangeReference CompRefrigHMTurb_Count = new RangeReference(Tabs.T01_03, 48, colCount, SqlDbType.TinyInt);
			private static RangeReference CompRefrigHMTurb_Age = new RangeReference(Tabs.T01_03, 48, colA, SqlDbType.Float);
			private static RangeReference CompRefrigHMTurb_Power = new RangeReference(Tabs.T01_03, 48, colB, SqlDbType.Float);
			private static RangeReference CompRefrigHMTurb_Turbo = new RangeReference(Tabs.T01_03, 49, colB, SqlDbType.VarChar);


			private static RangeReference BoilHPb_YN = new RangeReference(Tabs.T01_03, 54, colYN, SqlDbType.VarChar);
			private static RangeReference BoilHP_Count = new RangeReference(Tabs.T01_03, 54, colCount, SqlDbType.TinyInt);
			private static RangeReference BoilHP_Pressure = new RangeReference(Tabs.T01_03, 54, colA, SqlDbType.Float);
			private static RangeReference BoilHP_Capacity = new RangeReference(Tabs.T01_03, 54, colB, SqlDbType.Float);

			private static RangeReference BoilLPb_YN = new RangeReference(Tabs.T01_03, 55, colYN, SqlDbType.VarChar);
			private static RangeReference BoilLP_Count = new RangeReference(Tabs.T01_03, 55, colCount, SqlDbType.TinyInt);
			private static RangeReference BoilLP_Pressure = new RangeReference(Tabs.T01_03, 55, colA, SqlDbType.Float);
			private static RangeReference BoilLP_Capacity = new RangeReference(Tabs.T01_03, 55, colB, SqlDbType.Float);
			
			private static RangeReference SteamSuperHeat_YN = new RangeReference(Tabs.T01_03, 57, colYN, SqlDbType.VarChar);
			private static RangeReference SteamSuperHeat_Pressure = new RangeReference(Tabs.T01_03, 57, colA, SqlDbType.Float);
			private static RangeReference SteamSuperHeat_Capacity = new RangeReference(Tabs.T01_03, 57, colB, SqlDbType.Float);

			private static RangeReference ElecGenDist_YN = new RangeReference(Tabs.T01_03, 58, colYN, SqlDbType.VarChar);
			private static RangeReference ElecGenDist_Count = new RangeReference(Tabs.T01_03, 58, colCount, SqlDbType.TinyInt);
			private static RangeReference ElecGenDist_Capacity = new RangeReference(Tabs.T01_03, 58, colA, SqlDbType.Float);

			private static RangeReference TowerCool_Count = new RangeReference(Tabs.T01_03, 60, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerCool_Fans_Power = new RangeReference(Tabs.T01_03, 61, colA, SqlDbType.Float);
			private static RangeReference TowerCool_Pumps_Power = new RangeReference(Tabs.T01_03, 62, colA, SqlDbType.Float);
			private static RangeReference TowerCool_FlowRate = new RangeReference(Tabs.T01_03, 63, colA, SqlDbType.Float);

		
			private static RangeReference HydroPurCryogenic_YN = new RangeReference(Tabs.T01_03, 66, colYN, SqlDbType.VarChar);
			private static RangeReference HydroPurCryogenic_Count = new RangeReference(Tabs.T01_03, 66, colCount, SqlDbType.TinyInt);

			private static RangeReference HydroPurPSA_YN = new RangeReference(Tabs.T01_03, 67, colYN, SqlDbType.VarChar);
			private static RangeReference HydroPurPSA_Count = new RangeReference(Tabs.T01_03, 67, colCount, SqlDbType.TinyInt);

			private static RangeReference HydroPurMembrane_YN = new RangeReference(Tabs.T01_03, 68, colYN, SqlDbType.VarChar);
			private static RangeReference HydroPurMembrane_Count = new RangeReference(Tabs.T01_03, 68, colCount, SqlDbType.TinyInt);

			private static RangeReference StoreFeed_YN = new RangeReference(Tabs.T01_03, 70, colYN, SqlDbType.VarChar);
			private static RangeReference StoreProdAbove_YN = new RangeReference(Tabs.T01_03, 71, colYN, SqlDbType.VarChar);
			private static RangeReference StoreProdUnder_YN = new RangeReference(Tabs.T01_03, 72, colYN, SqlDbType.VarChar);

			private static RangeReference EthyLiq_YN = new RangeReference(Tabs.T01_03, 73, colYN, SqlDbType.VarChar);
			private static RangeReference EthyLiq_Count = new RangeReference(Tabs.T01_03, 73, colCount, SqlDbType.TinyInt);

			private static RangeReference TowerPyroGasHT_YN = new RangeReference(Tabs.T01_03, 75, colYN, SqlDbType.VarChar);
			private static RangeReference TowerPyroGasHT_Count = new RangeReference(Tabs.T01_03, 75, colCount, SqlDbType.TinyInt);
			private static RangeReference TowerPyroGasHT_Pressure = new RangeReference(Tabs.T01_03, 75, colA, SqlDbType.Float);
			private static RangeReference TowerPyroGasHT_Capacity = new RangeReference(Tabs.T01_03, 75, colB, SqlDbType.Float);
			private static RangeReference TowerPyroGasHT_Type = new RangeReference(Tabs.T01_03, 76, colA, SqlDbType.VarChar);
			private static RangeReference TowerPyroGasHT_Processed = new RangeReference(Tabs.T01_03, 77, colA, SqlDbType.Float);
			private static RangeReference TowerPyroGasHT_SG = new RangeReference(Tabs.T01_03, 78, colA, SqlDbType.Float);

			private static RangeReference PipeStation_YN = new RangeReference(Tabs.T01_03, 80, colYN, SqlDbType.VarChar);
			private static RangeReference RailTruck_YN = new RangeReference(Tabs.T01_03, 81, colYN, SqlDbType.VarChar);
			private static RangeReference Marine_YN = new RangeReference(Tabs.T01_03, 82, colYN, SqlDbType.VarChar);
			private static RangeReference CompAir_YN = new RangeReference(Tabs.T01_03, 83, colYN, SqlDbType.VarChar);
			private static RangeReference ElecTransf_YN = new RangeReference(Tabs.T01_03, 84, colYN, SqlDbType.VarChar);
			private static RangeReference Flare_YN = new RangeReference(Tabs.T01_03, 85, colYN, SqlDbType.VarChar);
			private static RangeReference WaterTreat_YN = new RangeReference(Tabs.T01_03, 86, colYN, SqlDbType.VarChar);
			private static RangeReference Bldg_YN = new RangeReference(Tabs.T01_03, 87, colYN, SqlDbType.VarChar);
			private static RangeReference RGR_YN = new RangeReference(Tabs.T01_03, 88, colYN, SqlDbType.VarChar);
		

			private static RangeReference VesselOth1_Count = new RangeReference(Tabs.T01_03, 91, colCount, SqlDbType.TinyInt);
			private static RangeReference VesselOth1_Description = new RangeReference(Tabs.T01_03, 91, colDescription, SqlDbType.VarChar);
			private static RangeReference VesselOth2_Count = new RangeReference(Tabs.T01_03, 92, colCount, SqlDbType.TinyInt);
			private static RangeReference VesselOth2_Description = new RangeReference(Tabs.T01_03, 92, colDescription, SqlDbType.VarChar);
			private static RangeReference VesselOth3_Count = new RangeReference(Tabs.T01_03, 93, colCount, SqlDbType.TinyInt);
			private static RangeReference VesselOth3_Description = new RangeReference(Tabs.T01_03, 93, colDescription, SqlDbType.VarChar);

			private static RangeReference TotalUnit_Count = new RangeReference(Tabs.T01_03, 94, colCount, SqlDbType.TinyInt);


			//provided for unknow upload issues
			private static RangeReference Bldg_Count = new RangeReference(Tabs.T01_03, 94, colCount, SqlDbType.TinyInt);
			private static RangeReference CompAir_Count = new RangeReference(Tabs.T01_03, 94, colCount, SqlDbType.TinyInt);
		







			private static Dictionary<string, RangeReference> DownloadCount()
			{
				Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

				d.Add("FracFeed", Facilities.FracFeed_Count);

				d.Add("PyroFurn", Facilities.PyrFurn_Count);

				d.Add("TowerQuench", Facilities.QTower_Count);
				d.Add("QWaterSys", Facilities.QWaterSys_Count);
				d.Add("TowerGas", Facilities.GasFrac_Count);
				d.Add("DryerProcess", Facilities.ProcDryer_Count);

				d.Add("CompGas", Facilities.CompGas_Count);

				d.Add("TowerCaustic", Facilities.TowerCaustic_Count);
				d.Add("MethSys", Facilities.TowerMethSys_Count);
				d.Add("TowerDemethanizer", Facilities.TowerDemethanizer_Count);
				d.Add("TowerDeethanizer", Facilities.TowerDeethanizer_Count);
				//d.Add("Acetyl", Facilities.ConvertersAce_Count);
				d.Add("AcetylMAPD", Facilities.ConvertersMAPD_Count);

			
				d.Add("TowerEthylene", Facilities.TowerEthylene_Count);
				d.Add("TowerDepropanizer", Facilities.TowerDepropanizer_Count);
				d.Add("TowerPropylene", Facilities.TowerPropylene_Count);
				d.Add("TowerDebutanizer", Facilities.TowerDebutanizer_Count);

				d.Add("CompRefrigPropylene", Facilities.CompRefrigPropylene_Count);
				d.Add("CompRefrigEthylene", Facilities.CompRefrigEthylene_Count);
				d.Add("CompRefrigMethane", Facilities.CompRefrigMethane_Count);
				d.Add("BinTerComp", Facilities.CompRefrigBinary_Count);
				d.Add("HydMetTurbo", Facilities.CompRefrigHMTurb_Count);


				d.Add("BoilHP", Facilities.BoilHP_Count);
				d.Add("BoilLP", Facilities.BoilLP_Count);
				d.Add("ElecGenDist", Facilities.ElecGenDist_Count);

				d.Add("TowerCool", Facilities.TowerCool_Count);

				d.Add("HydroPurCryogenic", Facilities.HydroPurCryogenic_Count);
				d.Add("HydroPurPSA", Facilities.HydroPurPSA_Count);
				d.Add("HydroPurMembrane", Facilities.HydroPurMembrane_Count);

				d.Add("EthyLiq", Facilities.EthyLiq_Count);

				d.Add("TowerPyroGasHT", Facilities.TowerPyroGasHT_Count);

				d.Add("VesselOth1", Facilities.VesselOth1_Count);
				d.Add("VesselOth2", Facilities.VesselOth2_Count);
				d.Add("VesselOth3", Facilities.VesselOth3_Count);
			

				return d;
			}

			private static Dictionary<string, RangeReference> DownloadYN()
			{
				Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

				d.Add("SteamSuperHeat", Facilities.SteamSuperHeat_YN);
				d.Add("StoreFeed", Facilities.StoreFeed_YN);
				d.Add("StoreProdAbove", Facilities.StoreProdAbove_YN);
				d.Add("StoreProdUnder", Facilities.StoreProdUnder_YN);
				d.Add("PipeStation", Facilities.PipeStation_YN);
				d.Add("RailTruck", Facilities.RailTruck_YN);
				d.Add("Marine", Facilities.Marine_YN);
				d.Add("CompAir", Facilities.CompAir_YN);
				d.Add("ElecTransf", Facilities.ElecTransf_YN);
				d.Add("Flare", Facilities.Flare_YN);
				d.Add("WaterTreat", Facilities.WaterTreat_YN);
				d.Add("Bldg", Facilities.Bldg_YN);
				d.Add("RGR", Facilities.RGR_YN);
				//d.Add("StoreFeed", Facilities.StoreFeed_YN);
				//d.Add("Acetyl", Facilities.ConvertersAce_YN);
				d.Add("AcetylMAPD", Facilities.ConvertersMAPD_YN);
				d.Add("TowerPropylene", Facilities.TowerPropylene_YN);
				d.Add("TowerDebutanizer", Facilities.TowerDebutanizer_YN);
				d.Add("CompRefrigPropylene", Facilities.CompRefrigPropylene_YN);
				d.Add("CompRefrigEthylene", Facilities.CompRefrigEthylene_YN);
				d.Add("CompRefrigMethane", Facilities.CompRefrigMethane_YN);
				d.Add("BinTerComp", Facilities.CompRefrigBinary_YN);
				d.Add("HydMetTurbo", Facilities.CompRefrigHMTurb_YN);
				d.Add("BoilHP", Facilities.BoilHPb_YN);
				d.Add("BoilLP", Facilities.BoilLPb_YN);
				d.Add("ElecGenDist", Facilities.ElecGenDist_YN);
				d.Add("HydroPurCryogenic", Facilities.HydroPurCryogenic_YN);
				d.Add("HydroPurPSA", Facilities.HydroPurPSA_YN);
				d.Add("HydroPurMembrane", Facilities.HydroPurMembrane_YN);
				d.Add("EthyLiq", Facilities.EthyLiq_YN);
				d.Add("TowerPyroGasHT", Facilities.TowerPyroGasHT_YN);


				return d;
			}

			internal void CustomLoad(SqlConnection cn, Excel.Workbook wkb, string tabNamePrefix, string refnum, bool forceIn)
			{
				string sheetName = tabNamePrefix + Tabs.T01_03;

				if (wkb.SheetExists(sheetName))
				{
					Excel.Worksheet wks = wkb.Worksheets[sheetName];

					string facilityId;

					RangeReference rr;

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_Facilities]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								facilityId = rdr.GetString(rdr.GetOrdinal("FacilityId"));

								if (DownloadCount().TryGetValue(facilityId, out rr))
								{
									XL.SetCellValue(rdr, "Unit_Count", wkb, tabNamePrefix, rr, forceIn);
								}

								if (DownloadYN().TryGetValue(facilityId, out rr))
								{
									XL.SetCellValue(rdr, "HasUnit_YN", wkb, tabNamePrefix, rr, forceIn);
								}

								LoadAuxiliaryData(rdr, facilityId, wkb, tabNamePrefix, forceIn);
							}
						}
					}
				}
			}

			private void LoadAuxiliaryData(SqlDataReader rdr, string facilityId, Excel.Workbook wkb, string tabNamePrefix, bool forceIn)
			{
				//int YearsSLS = 0;
				//if (Int32.TryParse(wkb.Sheet("CONV").cells[6, 2].value, YearsSLS))
				//int YearsSLS = 0;
				//Excel.Worksheet excelSheet = wkb.Sheets["Conv"];
				//YearsSLS = excelSheet.Cells[6, 2].Value;
				//System.Int32.TryParse(excelSheet.Cells[6, 2].Value, out YearsSLS);
				
				
				switch (facilityId)
				{

					case "FracFeed":
						XL.SetCellValue(rdr, "FFFeedRate_kBsd", wkb, tabNamePrefix, FracFeed_Capacity, forceIn);
						XL.SetCellValue(rdr, "FFStreamId", wkb, tabNamePrefix, FracFeed_FeedType, forceIn);
						XL.SetCellValue(rdr, "FFFeedProcessed_kMT", wkb, tabNamePrefix, FracFeed_ProcMT, forceIn);
						XL.SetCellValue(rdr, "FFFeedDensity_SG", wkb, tabNamePrefix, FracFeed_SG, forceIn);
						break;

					case "PyroFurn":
						XL.SetCellValue(rdr, "PyroFurnSpare_Pcnt", wkb, tabNamePrefix, PyrFurn_SparePcnt, forceIn);
						break;

					case "CompGas":
					
						XL.SetCellValue(rdr, "Stages_Count", wkb, tabNamePrefix, CompGas_Stages, forceIn);

						if (tabNamePrefix == "Table")
							{
							XL.SetCellValue(rdr, "Total_Age", wkb, tabNamePrefix, CompGas_Age, forceIn);
							}
						else
							{
							XL.SetCellValue(rdr, "Age_Years", wkb, tabNamePrefix, CompGas_Age, forceIn);
							}

						XL.SetCellValue(rdr, "Power_BHP", wkb, tabNamePrefix, CompGas_Power, forceIn);
						XL.SetCellValue(rdr, "CoatingID", wkb, tabNamePrefix, CompGas_Coating, forceIn);
						XL.SetCellValue(rdr, "DriverID", wkb, tabNamePrefix, CompGas_Driver, forceIn);
						break;

					case "TowerDemethanizer":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, TowerDemethanizer_Pressure, forceIn);
						break;

					case "TowerDeethanizer":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, TowerDeethanize_Pressure, forceIn);
						break;
					case "Acetyl":
						XL.SetCellValue(rdr, "AcetylLocID", wkb, tabNamePrefix, ConvertersAce_Loc, forceIn);
						XL.SetCellValue(rdr, "FeedType", wkb, tabNamePrefix, ConvertersAce_Feed, forceIn);
						XL.SetCellValue(rdr, "FeedSelectPcnt", wkb, tabNamePrefix, ConvertersAce_Selectivity, forceIn);
						break;
					case "AcetylMAPD":
						XL.SetCellValue(rdr, "FeedType", wkb, tabNamePrefix, ConvertersMAPD_Feed, forceIn);
						XL.SetCellValue(rdr, "FeedSelectPcnt", wkb, tabNamePrefix, ConvertersMAPD_Selectivity, forceIn);
						break;


					case "TowerEthylene":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, TowerEthylenee_Pressure, forceIn);
						break;

					case "TowerDepropanizer":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, TowerDepropanizer_Pressure, forceIn);
						break;

					case "TowerPropylene":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, TowerPropylene_Pressure, forceIn);
						break;

					case "CompRefrigPropylene":
						if (tabNamePrefix == "Table")
							{
							XL.SetCellValue(rdr, "Total_Age", wkb, tabNamePrefix, CompRefrigPropylene_Age, forceIn);
							}
						else
							{
							XL.SetCellValue(rdr, "Age_Years", wkb, tabNamePrefix, CompRefrigPropylene_Age, forceIn);
							}

						XL.SetCellValue(rdr, "Power_BHP", wkb, tabNamePrefix, CompRefrigPropylene_Power, forceIn);
						XL.SetCellValue(rdr, "CoatingID", wkb, tabNamePrefix, CompRefrigPropylene_Coating, forceIn);
						XL.SetCellValue(rdr, "DriverID", wkb, tabNamePrefix, CompRefrigPropylene_Driver, forceIn);
						break;

					case "CompRefrigEthylene":
						if (tabNamePrefix == "Table")
						{
							XL.SetCellValue(rdr, "Total_Age", wkb, tabNamePrefix, CompRefrigEthylene_Age, forceIn);
						}
						else
						{
							XL.SetCellValue(rdr, "Age_Years", wkb, tabNamePrefix, CompRefrigEthylene_Age, forceIn);
						}
						XL.SetCellValue(rdr, "Power_BHP", wkb, tabNamePrefix, CompRefrigEthylene_Power, forceIn);
						XL.SetCellValue(rdr, "CoatingID", wkb, tabNamePrefix, CompRefrigEthylene_Coating, forceIn);
						XL.SetCellValue(rdr, "DriverID", wkb, tabNamePrefix, CompRefrigEthylene_Driver, forceIn);
						break;

					case "CompRefrigMethane":
						if (tabNamePrefix == "Table")
						{
							XL.SetCellValue(rdr, "Total_Age", wkb, tabNamePrefix, CompRefrigMethane_Age, forceIn);
						}
						else
						{
							XL.SetCellValue(rdr, "Age_Years", wkb, tabNamePrefix, CompRefrigMethane_Age, forceIn);
						}
						XL.SetCellValue(rdr, "Power_BHP", wkb, tabNamePrefix, CompRefrigMethane_Power, forceIn);
						XL.SetCellValue(rdr, "CoatingID", wkb, tabNamePrefix, CompRefrigMethane_Coating, forceIn);
						XL.SetCellValue(rdr, "DriverID", wkb, tabNamePrefix, CompRefrigMethane_Driver, forceIn);
						break;
					case "BinTerComp":
						if (tabNamePrefix == "Table")
						{
							XL.SetCellValue(rdr, "Total_Age", wkb, tabNamePrefix, CompRefrigBinary_Age, forceIn);
						}
						else
						{
						XL.SetCellValue(rdr, "Age_Years", wkb, tabNamePrefix, CompRefrigBinary_Age, forceIn);
						}
						XL.SetCellValue(rdr, "FeedType", wkb, tabNamePrefix, CompRefrigBinary_Type, forceIn);
						XL.SetCellValue(rdr, "Refrigerant", wkb, tabNamePrefix, CompRefrigBinary_Refrg, forceIn);
						XL.SetCellValue(rdr, "Power_BHP", wkb, tabNamePrefix, CompRefrigBinary_Power, forceIn);
						XL.SetCellValue(rdr, "CoatingID", wkb, tabNamePrefix, CompRefrigBinary_Coating, forceIn);
						XL.SetCellValue(rdr, "DriverID", wkb, tabNamePrefix, CompRefrigBinary_Driver, forceIn);
						break;
					case "HydMetTurbo":
						if (tabNamePrefix == "Table")
						{
							XL.SetCellValue(rdr, "Total_Age", wkb, tabNamePrefix, CompRefrigHMTurb_Age, forceIn);
						}
						else
						{
							XL.SetCellValue(rdr, "Age_Years", wkb, tabNamePrefix, CompRefrigHMTurb_Age, forceIn);
						}
						XL.SetCellValue(rdr, "Power_BHP", wkb, tabNamePrefix, CompRefrigHMTurb_Power, forceIn);
						XL.SetCellValue(rdr, "Turboexpander", wkb, tabNamePrefix, CompRefrigHMTurb_Turbo, forceIn);
						
						
						break;
					case "BoilHP":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, BoilHP_Pressure, forceIn);
						XL.SetCellValue(rdr, "Rate_KLbHr", wkb, tabNamePrefix, BoilHP_Capacity, forceIn);
						break;

					case "BoilLP":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, BoilLP_Pressure, forceIn);
						XL.SetCellValue(rdr, "Rate_KLbHr", wkb, tabNamePrefix, BoilLP_Capacity, forceIn);
						break;

					case "SteamSuperHeat":
						XL.SetCellValue(rdr, "Pressure_PSIg", wkb, tabNamePrefix, SteamSuperHeat_Pressure, forceIn);
						XL.SetCellValue(rdr, "Rate_KLbHr", wkb, tabNamePrefix, SteamSuperHeat_Capacity, forceIn);
						break;

					case "ElecGenDist":
						XL.SetCellValue(rdr, "ElecGen_MW", wkb, tabNamePrefix, ElecGenDist_Capacity, forceIn);
						break;

					case "TowerCool":
						XL.SetCellValue(rdr, "FanHorsepower_bhp", wkb, tabNamePrefix, TowerCool_Fans_Power, forceIn);
						XL.SetCellValue(rdr, "PumpHorsepower_bhp", wkb, tabNamePrefix, TowerCool_Pumps_Power, forceIn);
						XL.SetCellValue(rdr, "FlowRate_TonneDay", wkb, tabNamePrefix, TowerCool_FlowRate, forceIn);
						break;

					case "TowerPyroGasHT":
						XL.SetCellValue(rdr, "HTFeedPressure_PSIg", wkb, tabNamePrefix, TowerPyroGasHT_Pressure, forceIn);
						XL.SetCellValue(rdr, "HTFeedRate_kBsd", wkb, tabNamePrefix, TowerPyroGasHT_Capacity, forceIn);
						XL.SetCellValue(rdr, "HTTypeID", wkb, tabNamePrefix, TowerPyroGasHT_Type, forceIn);
						XL.SetCellValue(rdr, "HTFeedProcessed_kMT", wkb, tabNamePrefix, TowerPyroGasHT_Processed, forceIn);
						XL.SetCellValue(rdr, "HTFeedDensity_SG", wkb, tabNamePrefix, TowerPyroGasHT_SG, forceIn);
						break;

					case "VesselOth1":
						XL.SetCellValue(rdr, "FacilityName", wkb, tabNamePrefix, VesselOth1_Description, forceIn);
						break;

					case "VesselOth2":
						XL.SetCellValue(rdr, "FacilityName", wkb, tabNamePrefix, VesselOth2_Description, forceIn);
						break;

					case "VesselOth3":
						XL.SetCellValue(rdr, "FacilityName", wkb, tabNamePrefix, VesselOth3_Description, forceIn);
						break;
				}
			}

			internal class Upload : TransferData.IUpload
			{
				public string StoredProcedure
				{
					get
					{
						return "[stgFact].[Insert_Facilities]";
					}
				}

				public Dictionary<string, RangeReference> Parameters
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("@FracFeedCnt", Facilities.FracFeed_Count);
						d.Add("@FracFeedKBSD", Facilities.FracFeed_Capacity);
						d.Add("@FracFeed", Facilities.FracFeed_FeedType);
						d.Add("@FracFeedProcMT", Facilities.FracFeed_ProcMT);
						d.Add("@SG_FracFeedProc", Facilities.FracFeed_SG);

						d.Add("@PyrFurnCnt", Facilities.PyrFurn_Count);
						d.Add("@PyrFurnSparePcnt", Facilities.PyrFurn_SparePcnt);

						d.Add("@QTowerCnt", Facilities.QTower_Count);
						d.Add("@QWaterSysCnt", Facilities.QWaterSys_Count);
						d.Add("@GasFracCnt", Facilities.GasFrac_Count);
						d.Add("@ProcDryerCnt", Facilities.ProcDryer_Count);

						d.Add("@GasCompCnt", Facilities.CompGas_Count);
						d.Add("@GasCompStageCnt", Facilities.CompGas_Stages);
						d.Add("@GasCompAge", Facilities.CompGas_Age);
						d.Add("@GasCompBHP", Facilities.CompGas_Power);
						d.Add("@GasCompCoat", Facilities.CompGas_Coating);
						d.Add("@GasCompDriver", Facilities.CompGas_Driver);

						d.Add("@CTowerCnt", Facilities.TowerCaustic_Count);
						d.Add("@MethSysCnt", Facilities.TowerMethSys_Count);
						d.Add("@DemethTowerCnt", Facilities.TowerDemethanizer_Count);
						d.Add("@DemethTowerPSIG", Facilities.TowerDemethanizer_Pressure);
						d.Add("@DeethTowerCnt", Facilities.TowerDeethanizer_Count);
						d.Add("@DeethTowerPSIG", Facilities.TowerDeethanize_Pressure);
						//d.Add("@AcetylCnt", Facilities.ConvertersAce_Count);
						d.Add("@EthylFracCnt", Facilities.TowerEthylene_Count);
						d.Add("@EthylFracPSIG", Facilities.TowerEthylenee_Pressure);
						d.Add("@DepropTowerCnt", Facilities.TowerDepropanizer_Count);
						d.Add("@DepropTowerPSIG", Facilities.TowerDepropanizer_Pressure);
						d.Add("@PropylFracCnt", Facilities.TowerPropylene_Count);
						d.Add("@PropylFracPSIG", Facilities.TowerPropylene_Pressure);
						d.Add("@DebutTowerCnt", Facilities.TowerDebutanizer_Count);

						d.Add("@PropCompCnt", Facilities.CompRefrigPropylene_Count);
						d.Add("@PropCompAge", Facilities.CompRefrigPropylene_Age);
						d.Add("@PropCompBHP", Facilities.CompRefrigPropylene_Power);
						d.Add("@PropCompCoat", Facilities.CompRefrigPropylene_Coating);
						d.Add("@PropCompDriver", Facilities.CompRefrigPropylene_Driver);

						d.Add("@EthyCompCnt", Facilities.CompRefrigEthylene_Count);
						d.Add("@EthyCompAge", Facilities.CompRefrigEthylene_Age);
						d.Add("@EthyCompBHP", Facilities.CompRefrigEthylene_Power);
						d.Add("@EthyCompCoat", Facilities.CompRefrigEthylene_Coating);
						d.Add("@EthyCompDriver", Facilities.CompRefrigEthylene_Driver);

						d.Add("@MethCompCnt", Facilities.CompRefrigMethane_Count);
						d.Add("@MethCompAge", Facilities.CompRefrigMethane_Age);
						d.Add("@MethCompBHP", Facilities.CompRefrigMethane_Power);
						d.Add("@MethCompCoat", Facilities.CompRefrigMethane_Coating);
						d.Add("@MethCompDriver", Facilities.CompRefrigMethane_Driver);

						d.Add("@HPBoilCnt", Facilities.BoilHP_Count);
						d.Add("@HPBoilPSIG", Facilities.BoilHP_Pressure);
						d.Add("@HPBoilRate", Facilities.BoilHP_Capacity);

						d.Add("@LPBoilCnt", Facilities.BoilLP_Count);
						d.Add("@LPBoilPSIG", Facilities.BoilLP_Pressure);
						d.Add("@LPBoilRate", Facilities.BoilLP_Capacity);

						//d.Add("@SteamHeatCnt", Facilities.SteamSuperHeat_Count);
						d.Add("@SteamHeatPSIG", Facilities.SteamSuperHeat_Pressure);
						d.Add("@SteamHeatRate", Facilities.SteamSuperHeat_Capacity);

						d.Add("@ElecGenCnt", Facilities.ElecGenDist_Count);
						d.Add("@ElecGenMW", Facilities.ElecGenDist_Capacity);

						d.Add("@CoolTowerCnt", Facilities.TowerCool_Count);
						//d.Add("@CoolTowerFansCnt", Facilities.TowerCool_Fans_Count);
						//d.Add("@CoolTowerPumpsCnt", Facilities.TowerCool_Pumps_Count);
						//d.Add("@CoolTowerFlowRate", Facilities.TowerCool_FlowRate);

						d.Add("@HydroCryoCnt", Facilities.HydroPurCryogenic_Count);
						d.Add("@HydroPSACnt", Facilities.HydroPurPSA_Count);
						d.Add("@HydroMembCnt", Facilities.HydroPurMembrane_Count);

						//d.Add("@FeedStorCnt", Facilities.StoreFeed_Count);
						//d.Add("@AboveStorCnt", Facilities.StoreProdAbove_Count);
						//d.Add("@UnderStorCnt", Facilities.StoreProdUnder_Count);

						d.Add("@EthyLiqCnt", Facilities.EthyLiq_Count);

						d.Add("@PGasHydroCnt", Facilities.TowerPyroGasHT_Count);
						d.Add("@PGasHydroPSIG", Facilities.TowerPyroGasHT_Pressure);
						d.Add("@PGasHydroKBSD", Facilities.TowerPyroGasHT_Capacity);
						d.Add("@PyrGasHydroType", Facilities.TowerPyroGasHT_Type);
						d.Add("@PyrGasHydroMT", Facilities.TowerPyroGasHT_Processed);
						d.Add("@SG_NonBnzPGH", Facilities.TowerPyroGasHT_SG);

						//d.Add("@PipeCnt", Facilities.PipeStation_Count);
						//d.Add("@RailCnt", Facilities.RailTruck_Count);
						//d.Add("@MarineCnt", Facilities.Marine_Count);
						//d.Add("@AirCompCnt", Facilities.CompAir_Count);
						//d.Add("@ElecTransfCnt", Facilities.ElecTransf_Count);
						//d.Add("@FlareCnt", Facilities.Flare_Count);
						//d.Add("@WaterTreatCnt", Facilities.WaterTreat_Count);
						//d.Add("@BldgCnt", Facilities.Bldg_Count);
						//d.Add("@RGRCnt", Facilities.RGR_Count);

						d.Add("@OthCnt1", Facilities.VesselOth1_Count);
						d.Add("@OthName1", Facilities.VesselOth1_Description);
						d.Add("@OthCnt2", Facilities.VesselOth2_Count);
						d.Add("@OthName2", Facilities.VesselOth2_Description);
						d.Add("@OthCnt3", Facilities.VesselOth3_Count);
						d.Add("@OthName3", Facilities.VesselOth3_Description);

						d.Add("@UnitCnt", Facilities.TotalUnit_Count);

						return d;
					}
				}
			}

			internal class CoolingWater
			{
				internal class Upload : TransferData.IUpload
				{
					public string StoredProcedure
					{
						get
						{
							return "[stgFact].[Insert_FacilitiesCoolingWater]";
						}
					}

					public Dictionary<string, RangeReference> Parameters
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("@FanHorsepower_bhp", Facilities.TowerCool_Fans_Power);
							d.Add("@PumpHorsepower_bhp", Facilities.TowerCool_Pumps_Power);
							d.Add("@FlowRate_TonneDay", Facilities.TowerCool_FlowRate);

							return d;
						}
					}
				}
			}
		}
	}
}