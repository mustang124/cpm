﻿using System.Collections.Generic;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Capacity
		{
			internal class Attributes
			{
				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_CapacityAttributes]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Expansion_Pcnt", Capacity.Expansion_Pcnt);
							d.Add("Expansion_Date", Capacity.Expansion_Date);
							d.Add("CapAllow_YN", Capacity.CapacityLoss_YN);
							d.Add("CapLoss_Pcnt", Capacity.CapacityLoss_Pcnt);
							d.Add("StartUp_Year", Capacity.PlantStart_Date);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}