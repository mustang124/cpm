﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Polymer
		{
			internal partial class Quantity
			{
				private static RangeReference PolyHydrogen = new RangeReference(Tabs.T12_02, 7, 0, SqlDbType.Float);
				private static RangeReference PolyAPEthylene = new RangeReference(Tabs.T12_02, 8, 0, SqlDbType.Float);
				private static RangeReference PolyPurchEthylene = new RangeReference(Tabs.T12_02, 9, 0, SqlDbType.Float);
				private static RangeReference PolyAPPropylene = new RangeReference(Tabs.T12_02, 10, 0, SqlDbType.Float);
				private static RangeReference PolyPurchPropylene = new RangeReference(Tabs.T12_02, 11, 0, SqlDbType.Float);
				private static RangeReference PolyButene = new RangeReference(Tabs.T12_02, 12, 0, SqlDbType.Float);
				private static RangeReference PolyHexene = new RangeReference(Tabs.T12_02, 13, 0, SqlDbType.Float);
				private static RangeReference PolyOctene = new RangeReference(Tabs.T12_02, 14, 0, SqlDbType.Float);
				private static RangeReference PolyMonomer1 = new RangeReference(Tabs.T12_02, 16, 0, SqlDbType.Float);
				private static RangeReference PolyMonomer2 = new RangeReference(Tabs.T12_02, 17, 0, SqlDbType.Float);
				private static RangeReference PolyMonomer3 = new RangeReference(Tabs.T12_02, 18, 0, SqlDbType.Float);
				private static RangeReference PolyAdditives = new RangeReference(Tabs.T12_02, 19, 0, SqlDbType.Float);
				private static RangeReference PolyCatalysts = new RangeReference(Tabs.T12_02, 20, 0, SqlDbType.Float);
				private static RangeReference PolyOther = new RangeReference(Tabs.T12_02, 21, 0, SqlDbType.Float);

				private static RangeReference LDPELiner = new RangeReference(Tabs.T12_02, 28, 0, SqlDbType.Float);
				private static RangeReference LDPEClarity = new RangeReference(Tabs.T12_02, 29, 0, SqlDbType.Float);
				private static RangeReference LDPEBlowMold = new RangeReference(Tabs.T12_02, 30, 0, SqlDbType.Float);
				private static RangeReference LDPEGPInjMold = new RangeReference(Tabs.T12_02, 31, 0, SqlDbType.Float);
				private static RangeReference LDPEInjLidResin = new RangeReference(Tabs.T12_02, 32, 0, SqlDbType.Float);
				private static RangeReference LDPEEVA = new RangeReference(Tabs.T12_02, 33, 0, SqlDbType.Float);
				private static RangeReference LDPEOther = new RangeReference(Tabs.T12_02, 34, 0, SqlDbType.Float);
				private static RangeReference LDPEOff = new RangeReference(Tabs.T12_02, 35, 0, SqlDbType.Float);

				private static RangeReference LLDPEBCLinerfilm = new RangeReference(Tabs.T12_02, 37, 0, SqlDbType.Float);
				private static RangeReference LLDPEBCMold = new RangeReference(Tabs.T12_02, 38, 0, SqlDbType.Float);
				private static RangeReference LLDPEHAOLinerFilm = new RangeReference(Tabs.T12_02, 39, 0, SqlDbType.Float);
				private static RangeReference LLDPEHAOMIFilm = new RangeReference(Tabs.T12_02, 40, 0, SqlDbType.Float);
				private static RangeReference LLDPEHAOInjMold = new RangeReference(Tabs.T12_02, 41, 0, SqlDbType.Float);
				private static RangeReference LLDPEHAOLidResin = new RangeReference(Tabs.T12_02, 42, 0, SqlDbType.Float);
				private static RangeReference LLDPEHAORotoMold = new RangeReference(Tabs.T12_02, 43, 0, SqlDbType.Float);
				private static RangeReference LLDPEOther = new RangeReference(Tabs.T12_02, 44, 0, SqlDbType.Float);
				private static RangeReference LLDPEOff = new RangeReference(Tabs.T12_02, 45, 0, SqlDbType.Float);

				private static RangeReference HDPEBlowMoldHomoPoly = new RangeReference(Tabs.T12_02, 47, 0, SqlDbType.Float);
				private static RangeReference HDPEBlowMoldCoPoly = new RangeReference(Tabs.T12_02, 48, 0, SqlDbType.Float);
				private static RangeReference HDPEFilm = new RangeReference(Tabs.T12_02, 49, 0, SqlDbType.Float);
				private static RangeReference HDPEGPInjMold = new RangeReference(Tabs.T12_02, 50, 0, SqlDbType.Float);
				private static RangeReference HDPEPipe = new RangeReference(Tabs.T12_02, 51, 0, SqlDbType.Float);
				private static RangeReference HDPEDrum = new RangeReference(Tabs.T12_02, 52, 0, SqlDbType.Float);
				private static RangeReference HDPERotoMold = new RangeReference(Tabs.T12_02, 53, 0, SqlDbType.Float);
				private static RangeReference HDPEOther = new RangeReference(Tabs.T12_02, 54, 0, SqlDbType.Float);
				private static RangeReference HDPEOff = new RangeReference(Tabs.T12_02, 55, 0, SqlDbType.Float);

				private static RangeReference PolyProHomoPoly = new RangeReference(Tabs.T12_02, 57, 0, SqlDbType.Float);
				private static RangeReference PolyProFilm = new RangeReference(Tabs.T12_02, 58, 0, SqlDbType.Float);
				private static RangeReference PolyProBlowMold = new RangeReference(Tabs.T12_02, 59, 0, SqlDbType.Float);
				private static RangeReference PolyProInj = new RangeReference(Tabs.T12_02, 60, 0, SqlDbType.Float);
				private static RangeReference PolyProBlockCoPoly = new RangeReference(Tabs.T12_02, 61, 0, SqlDbType.Float);
				private static RangeReference PolyProAtacticPolyPro = new RangeReference(Tabs.T12_02, 62, 0, SqlDbType.Float);
				private static RangeReference PolyProOther = new RangeReference(Tabs.T12_02, 63, 0, SqlDbType.Float);
				private static RangeReference PolyProOff = new RangeReference(Tabs.T12_02, 64, 0, SqlDbType.Float);

				private static RangeReference PolyByClient1 = new RangeReference(Tabs.T12_02, 66, 0, SqlDbType.Float);
				private static RangeReference PolyByClient2 = new RangeReference(Tabs.T12_02, 67, 0, SqlDbType.Float);
				private static RangeReference PolyByClient3 = new RangeReference(Tabs.T12_02, 68, 0, SqlDbType.Float);
				private static RangeReference PolyByClient4 = new RangeReference(Tabs.T12_02, 69, 0, SqlDbType.Float);
				private static RangeReference PolyByClient5 = new RangeReference(Tabs.T12_02, 70, 0, SqlDbType.Float);

				private static RangeReference PolyProdReactor = new RangeReference(Tabs.T12_02, 76, 0, SqlDbType.Float);

				private static RangeReference T1 = new RangeReference(Tabs.T12_01, 0, 6);
				private static RangeReference T2 = new RangeReference(Tabs.T12_01, 0, 7);
				private static RangeReference T3 = new RangeReference(Tabs.T12_01, 0, 8);
				private static RangeReference T4 = new RangeReference(Tabs.T12_01, 0, 9);
				private static RangeReference T5 = new RangeReference(Tabs.T12_01, 0, 10);
				private static RangeReference T6 = new RangeReference(Tabs.T12_01, 0, 11);

				internal class Download : TransferData.IDownloadIntersection
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_PolyQuantity]";
						}
					}

					public string ValueColumn
					{
						get
						{
							return "Quantity_kMT";
						}
					}

					public string RowLookUpColumn
					{
						get
						{
							return "StreamId";
						}
					}

					public string ColLookUpColumn
					{
						get
						{
							return "TrainId";
						}
					}

					public Dictionary<string, RangeReference> Rows
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("PolyHydrogen", Quantity.PolyHydrogen);
							d.Add("PolyAPEthylene", Quantity.PolyAPEthylene);
							d.Add("PolyPurchEthylene", Quantity.PolyPurchEthylene);
							d.Add("PolyAPPropylene", Quantity.PolyAPPropylene);
							d.Add("PolyPurchPropylene", Quantity.PolyPurchPropylene);
							d.Add("PolyButene", Quantity.PolyButene);
							d.Add("PolyHexene", Quantity.PolyHexene);
							d.Add("PolyOctene", Quantity.PolyOctene);
							d.Add("PolyMonomer1", Quantity.PolyMonomer1);
							d.Add("PolyMonomer2", Quantity.PolyMonomer2);
							d.Add("PolyMonomer3", Quantity.PolyMonomer3);
							d.Add("PolyAdditives", Quantity.PolyAdditives);
							d.Add("PolyCatalysts", Quantity.PolyCatalysts);
							d.Add("PolyOther", Quantity.PolyOther);

							d.Add("LDPELiner", Quantity.LDPELiner);
							d.Add("LDPEClarity", Quantity.LDPEClarity);
							d.Add("LDPEBlowMold", Quantity.LDPEBlowMold);
							d.Add("LDPEGPInjMold", Quantity.LDPEGPInjMold);
							d.Add("LDPEInjLidResin", Quantity.LDPEInjLidResin);
							d.Add("LDPEEVA", Quantity.LDPEEVA);
							d.Add("LDPEOther", Quantity.LDPEOther);
							d.Add("LDPEOff", Quantity.LDPEOff);

							d.Add("LLDPEBCLinerfilm", Quantity.LLDPEBCLinerfilm);
							d.Add("LLDPEBCMold", Quantity.LLDPEBCMold);
							d.Add("LLDPEHAOLinerFilm", Quantity.LLDPEHAOLinerFilm);
							d.Add("LLDPEHAOMIFilm", Quantity.LLDPEHAOMIFilm);
							d.Add("LLDPEHAOInjMold", Quantity.LLDPEHAOInjMold);
							d.Add("LLDPEHAOLidResin", Quantity.LLDPEHAOLidResin);
							d.Add("LLDPEHAORotoMold", Quantity.LLDPEHAORotoMold);
							d.Add("LLDPEOther", Quantity.LLDPEOther);
							d.Add("LLDPEOff", Quantity.LLDPEOff);

							d.Add("HDPEBlowMoldHomoPoly", Quantity.HDPEBlowMoldHomoPoly);
							d.Add("HDPEBlowMoldCoPoly", Quantity.HDPEBlowMoldCoPoly);
							d.Add("HDPEFilm", Quantity.HDPEFilm);
							d.Add("HDPEGPInjMold", Quantity.HDPEGPInjMold);
							d.Add("HDPEPipe", Quantity.HDPEPipe);
							d.Add("HDPEDrum", Quantity.HDPEDrum);
							d.Add("HDPERotoMold", Quantity.HDPERotoMold);
							d.Add("HDPEOther", Quantity.HDPEOther);
							d.Add("HDPEOff", Quantity.HDPEOff);

							d.Add("PolyProHomoPoly", Quantity.PolyProHomoPoly);
							d.Add("PolyProFilm", Quantity.PolyProFilm);
							d.Add("PolyProBlowMold", Quantity.PolyProBlowMold);
							d.Add("PolyProInj", Quantity.PolyProInj);
							d.Add("PolyProBlockCoPoly", Quantity.PolyProBlockCoPoly);
							d.Add("PolyProAtacticPolyPro", Quantity.PolyProAtacticPolyPro);
							d.Add("PolyProOther", Quantity.PolyProOther);
							d.Add("PolyProOff", Quantity.PolyProOff);

							d.Add("PolyByClient1", Quantity.PolyByClient1);
							d.Add("PolyByClient2", Quantity.PolyByClient2);
							d.Add("PolyByClient3", Quantity.PolyByClient3);
							d.Add("PolyByClient4", Quantity.PolyByClient4);
							d.Add("PolyByClient5", Quantity.PolyByClient5);

							d.Add("PolyProdReactor", Quantity.PolyProdReactor);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Cols
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("T1", Quantity.T1);
							d.Add("T2", Quantity.T2);
							d.Add("T3", Quantity.T3);
							d.Add("T4", Quantity.T4);
							d.Add("T5", Quantity.T5);
							d.Add("T6", Quantity.T6);

							return d;
						}
					}
				}
			}
		}
	}
}