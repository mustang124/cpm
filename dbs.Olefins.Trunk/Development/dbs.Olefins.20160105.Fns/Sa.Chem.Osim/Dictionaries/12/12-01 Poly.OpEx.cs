﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Polymer
		{
			internal partial class OpEx
			{
				private static RangeReference Maint = new RangeReference(Tabs.T12_01, 18, 0, SqlDbType.Float);
				private static RangeReference TACurrExp = new RangeReference(Tabs.T12_01, 19, 0, SqlDbType.Float);
				private static RangeReference OtherNonVol = new RangeReference(Tabs.T12_01, 20, 0, SqlDbType.Float);

				private static RangeReference Catalysts = new RangeReference(Tabs.T12_01, 23, 0, SqlDbType.Float);
				private static RangeReference ChemAdd = new RangeReference(Tabs.T12_01, 24, 0, SqlDbType.Float);
				private static RangeReference ChemOth = new RangeReference(Tabs.T12_01, 25, 0, SqlDbType.Float);
				private static RangeReference Royalties = new RangeReference(Tabs.T12_01, 26, 0, SqlDbType.Float);
				private static RangeReference PurchasedEnergy = new RangeReference(Tabs.T12_01, 27, 0, SqlDbType.Float);
				private static RangeReference PurOth = new RangeReference(Tabs.T12_01, 28, 0, SqlDbType.Float);
				private static RangeReference PackagingMatlSvcs = new RangeReference(Tabs.T12_01, 29, 0, SqlDbType.Float);
				private static RangeReference OtherVol = new RangeReference(Tabs.T12_01, 30, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownloadIntersection
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_PolyOpEx]";
						}
					}

					public string ValueColumn
					{
						get
						{
							return "Amount_Cur";
						}
					}

					public string RowLookUpColumn
					{
						get
						{
							return "AccountId";
						}
					}

					public string ColLookUpColumn
					{
						get
						{
							return "TrainId";
						}
					}

					public Dictionary<string, RangeReference> Rows
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Maint", OpEx.Maint);
							d.Add("TACurrExp", OpEx.TACurrExp);
							d.Add("OtherNonVol", OpEx.OtherNonVol);

							d.Add("Catalysts", OpEx.Catalysts);
							d.Add("ChemAdd", OpEx.ChemAdd);
							d.Add("ChemOth", OpEx.ChemOth);
							d.Add("Royalties", OpEx.Royalties);
							d.Add("PurchasedEnergy", OpEx.PurchasedEnergy);
							d.Add("PurOth", OpEx.PurOth);
							d.Add("PackagingMatlSvcs", OpEx.PackagingMatlSvcs);
							d.Add("OtherVol", OpEx.OtherVol);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Cols
					{
						get
						{
							return new Polymer.Facilities.Download().Items;
						}
					}
				}
			}
		}
	}
}