﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Apc
		{
			internal class Existence
			{
				private static RangeReference Mpc_X = new RangeReference(string.Empty, 0, 8, SqlDbType.VarChar);
				private static RangeReference Sep_X = new RangeReference(string.Empty, 0, 10, SqlDbType.VarChar);

				private static RangeReference ApcStartStop = new RangeReference(Tabs.T09_04, 42, 0, SqlDbType.VarChar);
				private static RangeReference ApcDecokeAutomatic = new RangeReference(Tabs.T09_04, 44, 0, SqlDbType.VarChar);
				private static RangeReference ApcDecokeOperator = new RangeReference(Tabs.T09_04, 47, 0, SqlDbType.VarChar);
				private static RangeReference ApcThroughputIndivid = new RangeReference(Tabs.T09_04, 49, 0, SqlDbType.VarChar);
				private static RangeReference ApcThroughputTotal = new RangeReference(Tabs.T09_04, 53, 0, SqlDbType.VarChar);

				private static RangeReference ApcClosedLoop = new RangeReference(Tabs.T09_05, 11, 0, SqlDbType.VarChar);
				private static RangeReference ApcCoilOutlet = new RangeReference(Tabs.T09_05, 14, 0, SqlDbType.VarChar);
				private static RangeReference ApcFiringConstraint = new RangeReference(Tabs.T09_05, 16, 0, SqlDbType.VarChar);
				private static RangeReference ApcExcessAir = new RangeReference(Tabs.T09_05, 19, 0, SqlDbType.VarChar);
				private static RangeReference ApcFiringDuty = new RangeReference(Tabs.T09_05, 22, 0, SqlDbType.VarChar);
				private static RangeReference ApcBalancing = new RangeReference(Tabs.T09_05, 24, 0, SqlDbType.VarChar);
				private static RangeReference ApcSteamHydrocarbon = new RangeReference(Tabs.T09_05, 26, 0, SqlDbType.VarChar);
				private static RangeReference ApcRecycleFlowRate = new RangeReference(Tabs.T09_05, 28, 0, SqlDbType.VarChar);

				private static RangeReference ApcSuction = new RangeReference(Tabs.T09_05, 38, 0, SqlDbType.VarChar);
				private static RangeReference ApcSurgeMargin = new RangeReference(Tabs.T09_05, 40, 0, SqlDbType.VarChar);
				private static RangeReference ApcCompLoad = new RangeReference(Tabs.T09_05, 44, 0, SqlDbType.VarChar);
				private static RangeReference ApcSurge = new RangeReference(Tabs.T09_05, 46, 0, SqlDbType.VarChar);
				private static RangeReference ApcCooling = new RangeReference(Tabs.T09_05, 49, 0, SqlDbType.VarChar);
				private static RangeReference ApcCompConstraint = new RangeReference(Tabs.T09_05, 53, 0, SqlDbType.VarChar);

				private static RangeReference ApcHotWater = new RangeReference(Tabs.T09_06, 10, 0, SqlDbType.VarChar);
				private static RangeReference ApcPyroGasBP = new RangeReference(Tabs.T09_06, 13, 0, SqlDbType.VarChar);
				private static RangeReference ApcStrippingSteamRatio = new RangeReference(Tabs.T09_06, 15, 0, SqlDbType.VarChar);

				private static RangeReference ApcDemethComposition = new RangeReference(Tabs.T09_06, 27, 0, SqlDbType.VarChar);
				private static RangeReference ApcDemethThroughput = new RangeReference(Tabs.T09_06, 29, 0, SqlDbType.VarChar);
				private static RangeReference ApcDeethComposition = new RangeReference(Tabs.T09_06, 32, 0, SqlDbType.VarChar);
				private static RangeReference ApcDeethThroughput = new RangeReference(Tabs.T09_06, 34, 0, SqlDbType.VarChar);
				private static RangeReference ApcFracEthyleneComposition = new RangeReference(Tabs.T09_06, 36, 0, SqlDbType.VarChar);
				private static RangeReference ApcFracEthyleneThroughput = new RangeReference(Tabs.T09_06, 38, 0, SqlDbType.VarChar);
				private static RangeReference ApcRefrig = new RangeReference(Tabs.T09_06, 41, 0, SqlDbType.VarChar);
				private static RangeReference ApcRefrigCompSurgeMargin = new RangeReference(Tabs.T09_06, 44, 0, SqlDbType.VarChar);
				private static RangeReference ApcRefrigCompSurge = new RangeReference(Tabs.T09_06, 47, 0, SqlDbType.VarChar);
				private static RangeReference ApcC3Recovery = new RangeReference(Tabs.T09_06, 49, 0, SqlDbType.VarChar);
				private static RangeReference ApcC3RecoveryThroughput = new RangeReference(Tabs.T09_06, 51, 0, SqlDbType.VarChar);
				private static RangeReference ApcSteamNaphthaComposition = new RangeReference(Tabs.T09_06, 53, 0, SqlDbType.VarChar);
				private static RangeReference ApcSteamNaphthaThroughput = new RangeReference(Tabs.T09_06, 56, 0, SqlDbType.VarChar);
				private static RangeReference ApcPressureMin = new RangeReference(Tabs.T09_06, 58, 0, SqlDbType.VarChar);
				private static Dictionary<string, RangeReference> Items;

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ApcExistance]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "ApcId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Mpc_X", Existence.Mpc_X);
							d.Add("Sep_X", Existence.Sep_X);

							return d;
						}
					}


					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("ApcStartStop", Existence.ApcStartStop);
							d.Add("ApcDecokeAutomatic", Existence.ApcDecokeAutomatic);
							d.Add("ApcDecokeOperator", Existence.ApcDecokeOperator);
							d.Add("ApcThroughputIndivid", Existence.ApcThroughputIndivid);
							d.Add("ApcThroughputTotal", Existence.ApcThroughputTotal);

							d.Add("ApcClosedLoop", Existence.ApcClosedLoop);
							d.Add("ApcCoilOutlet", Existence.ApcCoilOutlet);
							d.Add("ApcFiringConstraint", Existence.ApcFiringConstraint);
							d.Add("ApcExcessAir", Existence.ApcExcessAir);
							d.Add("ApcFiringDuty", Existence.ApcFiringDuty);
							d.Add("ApcBalancing", Existence.ApcBalancing);
							d.Add("ApcSteamHydrocarbon", Existence.ApcSteamHydrocarbon);
							d.Add("ApcRecycleFlowRate", Existence.ApcRecycleFlowRate);

							d.Add("ApcSuction", Existence.ApcSuction);
							d.Add("ApcSurgeMargin", Existence.ApcSurgeMargin);
							d.Add("ApcCompLoad", Existence.ApcCompLoad);
							d.Add("ApcSurge", Existence.ApcSurge);
							d.Add("ApcCooling", Existence.ApcCooling);
							d.Add("ApcCompConstraint", Existence.ApcCompConstraint);

							d.Add("ApcHotWater", Existence.ApcHotWater);
							d.Add("ApcPyroGasBP", Existence.ApcPyroGasBP);
							d.Add("ApcStrippingSteamRatio", Existence.ApcStrippingSteamRatio);

							d.Add("ApcDemethComposition", Existence.ApcDemethComposition);
							d.Add("ApcDemethThroughput", Existence.ApcDemethThroughput);
							d.Add("ApcDeethComposition", Existence.ApcDeethComposition);
							d.Add("ApcDeethThroughput", Existence.ApcDeethThroughput);
							d.Add("ApcFracEthyleneComposition", Existence.ApcFracEthyleneComposition);
							d.Add("ApcFracEthyleneThroughput", Existence.ApcFracEthyleneThroughput);
							d.Add("ApcRefrig", Existence.ApcRefrig);
							d.Add("ApcRefrigCompSurgeMargin", Existence.ApcRefrigCompSurgeMargin);
							d.Add("ApcRefrigCompSurge", Existence.ApcRefrigCompSurge);
							d.Add("ApcC3Recovery", Existence.ApcC3Recovery);
							d.Add("ApcC3RecoveryThroughput", Existence.ApcC3RecoveryThroughput);
							d.Add("ApcSteamNaphthaComposition", Existence.ApcSteamNaphthaComposition);
							d.Add("ApcSteamNaphthaThroughput", Existence.ApcSteamNaphthaThroughput);
							d.Add("ApcPressureMin", Existence.ApcPressureMin);

							return d;
						}
					}
				}
	internal class Upload : TransferData.IUploadMultiple
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ApcExistance]";
						}
					}
					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Existence.Items;
						}
					}
					public string LookUpColumn
					{
						get
						{
							return "ApcId";
						}
					}
					public Dictionary<string, RangeReference> Parameters
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("@ApcStartStop", Existence.ApcStartStop);
							d.Add("@ApcDecokeAutomatic", Existence.ApcDecokeAutomatic);
							d.Add("@ApcDecokeOperator", Existence.ApcDecokeOperator);
							d.Add("@ApcThroughputIndivid", Existence.ApcThroughputIndivid);
							d.Add("@ApcThroughputTotal", Existence.ApcThroughputTotal);

							d.Add("@ApcClosedLoop", Existence.ApcClosedLoop);
							d.Add("@ApcCoilOutlet", Existence.ApcCoilOutlet);
							d.Add("@ApcFiringConstraint", Existence.ApcFiringConstraint);
							d.Add("@ApcExcessAir", Existence.ApcExcessAir);
							d.Add("@ApcFiringDuty", Existence.ApcFiringDuty);
							d.Add("@ApcBalancing", Existence.ApcBalancing);
							d.Add("@ApcSteamHydrocarbon", Existence.ApcSteamHydrocarbon);
							d.Add("@ApcRecycleFlowRate", Existence.ApcRecycleFlowRate);

							d.Add("@ApcSuction", Existence.ApcSuction);
							d.Add("@ApcSurgeMargin", Existence.ApcSurgeMargin);
							d.Add("@ApcCompLoad", Existence.ApcCompLoad);
							d.Add("@ApcSurge", Existence.ApcSurge);
							d.Add("@ApcCooling", Existence.ApcCooling);
							d.Add("@ApcCompConstraint", Existence.ApcCompConstraint);

							d.Add("@ApcHotWater", Existence.ApcHotWater);
							d.Add("@ApcPyroGasBP", Existence.ApcPyroGasBP);
							d.Add("@ApcStrippingSteamRatio", Existence.ApcStrippingSteamRatio);

							d.Add("@ApcDemethComposition", Existence.ApcDemethComposition);
							d.Add("@ApcDemethThroughput", Existence.ApcDemethThroughput);
							d.Add("@ApcDeethComposition", Existence.ApcDeethComposition);
							d.Add("@ApcDeethThroughput", Existence.ApcDeethThroughput);
							d.Add("@ApcFracEthyleneComposition", Existence.ApcFracEthyleneComposition);
							d.Add("@ApcFracEthyleneThroughput", Existence.ApcFracEthyleneThroughput);
							d.Add("@ApcRefrig", Existence.ApcRefrig);
							d.Add("@ApcRefrigCompSurgeMargin", Existence.ApcRefrigCompSurgeMargin);
							d.Add("@ApcRefrigCompSurge", Existence.ApcRefrigCompSurge);
							d.Add("@ApcC3Recovery", Existence.ApcC3Recovery);
							d.Add("@ApcC3RecoveryThroughput", Existence.ApcC3RecoveryThroughput);
							d.Add("@ApcSteamNaphthaComposition", Existence.ApcSteamNaphthaComposition);
							d.Add("@ApcSteamNaphthaThroughput", Existence.ApcSteamNaphthaThroughput);
							d.Add("@ApcPressureMin", Existence.ApcPressureMin);


							return d;
						}
					}
				}
			}
		}
	}
}