﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class CompressorEvents
			{
				private static RangeReference WksCompressorID = new RangeReference(Tabs.T06_05, 0, 2, SqlDbType.VarChar);
				private static RangeReference ServiceLevelID = new RangeReference(Tabs.T06_05, 0, 3, SqlDbType.VarChar);
				private static RangeReference Duration_Hours = new RangeReference(Tabs.T06_05, 0, 4, SqlDbType.Float);
				private static RangeReference WksCompComponentID = new RangeReference(Tabs.T06_05, 0, 5, SqlDbType.VarChar);
				private static RangeReference WksEventCauseID = new RangeReference(Tabs.T06_05, 0, 6, SqlDbType.VarChar);
				private static RangeReference EventOccur_Year = new RangeReference(Tabs.T06_05, 0, 7, SqlDbType.Int);
				private static RangeReference Comments = new RangeReference(Tabs.T06_05, 0, 8, SqlDbType.VarChar);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityCompEvents]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "EEventNo";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("WksCompressorID", CompressorEvents.WksCompressorID);
							d.Add("ServiceLevelID", CompressorEvents.ServiceLevelID);
							d.Add("Duration_Hours", CompressorEvents.Duration_Hours);
							d.Add("WksCompComponentID", CompressorEvents.WksCompComponentID);
							d.Add("WksEventCauseID", CompressorEvents.WksEventCauseID);
							d.Add("EventOccur_Year", CompressorEvents.EventOccur_Year);
							d.Add("Comments", CompressorEvents.Comments);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							const int offset = 28;

							for (int i = 1; i <= 110; i++)
							{
								d.Add("E" + i.ToString(), new RangeReference(Tabs.T06_05, i + offset, 0));
							}

							return d;
						}
					}
				}
			}
		}
	}
}