﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class Attributes
			{
				private static RangeReference TimingFactorID = new RangeReference(Tabs.T06_02, 26, 5, SqlDbType.VarChar);
				private static RangeReference TaPrep_Hrs = new RangeReference(Tabs.T06_02, 42, 6, SqlDbType.Float);
				private static RangeReference TaStartUp_Hrs = new RangeReference(Tabs.T06_02, 46, 6, SqlDbType.Float);

				private static RangeReference FurnTubeLife_Mnths = new RangeReference(Tabs.T06_03, 48, 8, SqlDbType.Float);
				private static RangeReference FurnRetube_Days = new RangeReference(Tabs.T06_03, 52, 8, SqlDbType.Float);
				private static RangeReference XlsFurnRetubeWork = new RangeReference(Tabs.T06_03, 55, 8, SqlDbType.VarChar);

				private static RangeReference Operation_Years = new RangeReference(Tabs.T06_05, 23, 7, SqlDbType.Float);

				private static RangeReference PhilosophyID = new RangeReference(Tabs.T06_06, 8, 6, SqlDbType.VarChar);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityAttributes]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("TimingFactorID", Attributes.TimingFactorID);
							d.Add("TaPrep_Hrs", Attributes.TaPrep_Hrs);
							d.Add("TaStartUp_Hrs", Attributes.TaStartUp_Hrs);

							d.Add("FurnTubeLife_Mnths", Attributes.FurnTubeLife_Mnths);
							d.Add("FurnRetube_Days", Attributes.FurnRetube_Days);
							d.Add("XlsFurnRetubeWork", Attributes.XlsFurnRetubeWork);

							d.Add("Operation_Years", Attributes.Operation_Years);

							d.Add("PhilosophyID", Attributes.PhilosophyID);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}