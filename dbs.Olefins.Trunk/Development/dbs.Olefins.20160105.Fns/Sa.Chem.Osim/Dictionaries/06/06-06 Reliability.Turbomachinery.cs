﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class Turbomachinery
			{
				private static RangeReference Frequency_Years = new RangeReference(Tabs.T06_06, 0, 6, SqlDbType.Float);

				private static RangeReference CompGas = new RangeReference(Tabs.T06_06, 18, 6, SqlDbType.Int);
				private static RangeReference CompTurbGas = new RangeReference(Tabs.T06_06, 19, 6, SqlDbType.Int);
				private static RangeReference CompRefrigPropylene = new RangeReference(Tabs.T06_06, 20, 6, SqlDbType.Int);
				private static RangeReference CompTurbPropylene = new RangeReference(Tabs.T06_06, 21, 6, SqlDbType.Int);
				private static RangeReference CompRefrigEthylene = new RangeReference(Tabs.T06_06, 22, 6, SqlDbType.Int);
				private static RangeReference CompTurbEthylene = new RangeReference(Tabs.T06_06, 23, 6, SqlDbType.Int);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityDTOverhaulFrequency]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "FacilityID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Frequency_Years", Turbomachinery.Frequency_Years);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("CompGas", Turbomachinery.CompGas);
							d.Add("CompTurbGas", Turbomachinery.CompTurbGas);
							d.Add("CompRefrigPropylene", Turbomachinery.CompRefrigPropylene);
							d.Add("CompTurbPropylene", Turbomachinery.CompTurbPropylene);
							d.Add("CompRefrigEthylene", Turbomachinery.CompRefrigEthylene);
							d.Add("CompTurbEthylene", Turbomachinery.CompTurbEthylene);

							return d;
						}
					}
				}
			}
		}
	}
}