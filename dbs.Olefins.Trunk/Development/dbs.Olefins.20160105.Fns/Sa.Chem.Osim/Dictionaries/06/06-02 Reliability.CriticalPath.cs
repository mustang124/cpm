﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class CriticalPath
			{
				private static RangeReference CritPath_X = new RangeReference(Tabs.T06_02, 0, 5, SqlDbType.VarChar);
				private static RangeReference FacilityName = new RangeReference(Tabs.T06_02, 0, 3, SqlDbType.VarChar);

				const int col = 0;

				private static RangeReference CompGas = new RangeReference(Tabs.T06_02, 11, col, SqlDbType.VarChar);
				private static RangeReference CompRefrig = new RangeReference(Tabs.T06_02, 12, col, SqlDbType.VarChar);
				private static RangeReference PyroFurnTLE = new RangeReference(Tabs.T06_02, 13, col, SqlDbType.VarChar);
				private static RangeReference Exchanger = new RangeReference(Tabs.T06_02, 14, col, SqlDbType.VarChar);
				private static RangeReference Vessel = new RangeReference(Tabs.T06_02, 15, col, SqlDbType.VarChar);
				private static RangeReference PyroFurn = new RangeReference(Tabs.T06_02, 16, col, SqlDbType.VarChar);
				private static RangeReference Utilities = new RangeReference(Tabs.T06_02, 17, col, SqlDbType.VarChar);
				private static RangeReference CritPathCapProj = new RangeReference(Tabs.T06_02, 18, col, SqlDbType.VarChar);
				private static RangeReference CritPathOther = new RangeReference(Tabs.T06_02, 19, col, SqlDbType.VarChar);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityCriticalPath]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "FacilityID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("CritPath_X", CriticalPath.CritPath_X);
							d.Add("FacilityName", CriticalPath.FacilityName);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("CompGas", CriticalPath.CompGas);
							d.Add("CompRefrig", CriticalPath.CompRefrig);
							d.Add("PyroFurnTLE", CriticalPath.PyroFurnTLE);
							d.Add("Exchanger", CriticalPath.Exchanger);
							d.Add("Vessel", CriticalPath.Vessel);
							d.Add("PyroFurn", CriticalPath.PyroFurn);
							d.Add("Utilities", CriticalPath.Utilities);
							d.Add("CritPathCapProj", CriticalPath.CritPathCapProj);
							d.Add("CritPathOther", CriticalPath.CritPathOther);

							return d;
						}
					}
				}
			}
		}
	}
}