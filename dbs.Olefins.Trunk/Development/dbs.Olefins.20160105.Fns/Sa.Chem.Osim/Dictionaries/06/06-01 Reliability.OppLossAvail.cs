﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class Availability
			{
				private static RangeReference Demand = new RangeReference(Tabs.T06_01, 71, 0, SqlDbType.Float);
				private static RangeReference ExtFeedInterrupt = new RangeReference(Tabs.T06_01, 72, 0, SqlDbType.Float);
				private static RangeReference CapProject = new RangeReference(Tabs.T06_01, 74, 0, SqlDbType.Float);
				private static RangeReference Strikes = new RangeReference(Tabs.T06_01, 75, 0, SqlDbType.Float);
				private static RangeReference FeedMix = new RangeReference(Tabs.T06_01, 76, 0, SqlDbType.Float);
				private static RangeReference Severity = new RangeReference(Tabs.T06_01, 77, 0, SqlDbType.Float);
				private static RangeReference OtherNonOp = new RangeReference(Tabs.T06_01, 78, 0, SqlDbType.Float);

				private static Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("Demand", Availability.Demand);
						d.Add("ExtFeedInterrupt", Availability.ExtFeedInterrupt);
						d.Add("CapProject", Availability.CapProject);
						d.Add("Strikes", Availability.Strikes);
						d.Add("FeedMix", Availability.FeedMix);
						d.Add("Severity", Availability.Severity);
						d.Add("OtherNonOp", Availability.OtherNonOp);

						return d;
					}
				}

				internal class History : TransferData.IDownload
				{
					private RangeReference downTimePcnt = new RangeReference(Tabs.T06_01, 0, 6, SqlDbType.Float, 100.0);
					private RangeReference slowDownPcnt = new RangeReference(Tabs.T06_01, 0, 7, SqlDbType.Float, 100.0);

					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityOppLoss_AvailabilityHistory]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "OppLossId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("DownTimeLoss_Pcnt", this.downTimePcnt);
							d.Add("SlowDownLoss_Pcnt", this.slowDownPcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Availability.Items;
						}
					}
				}

				internal class Current : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityOppLoss_AvailabilityCurrent]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "OppLossId";
						}
					}

					private RangeReference downTimePcnt = new RangeReference(Tabs.T06_01, 0, 8, SqlDbType.Float, 100.0);
					private RangeReference slowDownPcnt = new RangeReference(Tabs.T06_01, 0, 9, SqlDbType.Float, 100.0);

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("DownTimeLoss_Pcnt", this.downTimePcnt);
							d.Add("SlowDownLoss_Pcnt", this.slowDownPcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Availability.Items;
						}
					}
				}
			}
		}
	}
}