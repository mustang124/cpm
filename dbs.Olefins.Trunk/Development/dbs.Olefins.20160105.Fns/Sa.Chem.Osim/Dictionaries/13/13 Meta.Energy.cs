﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Metathesis
		{
			internal partial class Energy
			{
				private static RangeReference MetaEnergy = new RangeReference(Tabs.T13, 53, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MetaEnergy]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "AccountId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							return Metathesis.Values;
						}
					}


					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("MetaEnergy", Energy.MetaEnergy);

							return d;
						}
					}
				}
			}
		}
	}
}