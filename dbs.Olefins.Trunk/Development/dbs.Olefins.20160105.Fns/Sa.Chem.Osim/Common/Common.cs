﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	public class Common
	{
		public static string cnString()
		{
			return dbConnectionString("DBS1", "Olefins", "rrh", "rrh#4279");
			//return dbConnectionString(@"(localdb)\ProjectsV12", "dbs.Olefins");
		}

		public static string dbConnectionString(string dbInst, string dbName)
		{
			string cn = "Data Source=" + dbInst + ";";
			cn = cn + "Initial Catalog=" + dbName + ";";
			cn = cn + "Trust_Connection=True;";
			cn = cn + "MultipleActiveResultSets=True;";
			cn = cn + "Application Name = " + ProdNameVer() + ";";

			return cn;
		}

		public static string dbConnectionString(string dbInst, string dbName, string UserName, string UserPass)
		{
			string cn = "Data Source=" + dbInst + ";";
			cn = cn + "Initial Catalog=" + dbName + ";";
            cn = cn + "Trusted_Connection=True;";
            //cn = cn + "User Id=" + UserName + ";";
            //cn = cn + "Password=" + UserPass + ";";
			cn = cn + "MultipleActiveResultSets=True;";
			cn = cn + "Application Name = " + ProdNameVer() + ";";

			return cn;
		}

		// HACK: set Version number  = SELECT DATEDIFF(d, '1/1/2000', SYSDATETIME())
		private const string ver = "5507";

		public static string ProdNameVer()
		{
			string rtn = string.Empty;

			try
			{
				Assembly ai = Assembly.GetExecutingAssembly();
				rtn = ai.GetName().Name + " (" + ai.GetName().Version + ")";
			}
			catch
			{
				rtn = "Chem.Upload.Cl 1.0." + ver + ".x";
			}

			return rtn;

		}

		public static string ProdVer()
		{
			string rtn = string.Empty;

			try
			{
				Assembly ai = Assembly.GetExecutingAssembly();
				rtn = Convert.ToString(ai.GetName().Version);
			}
			catch
			{
				rtn = "1.0." + ver + ".x";
			}

			return rtn;
		}

		public static string GetDateTimeStamp()
		{
			return DateTime.Now.ToString("yyyyMMdd.HHmmss");
		}
	}

	public struct RefnumPair
	{
		private readonly string current;
		private readonly string history;

		public string refnumCurrent
		{
			get
			{
				return this.current;
			}
		}

		public string refnumHistory
		{
			get
			{
				return this.history;
			}
		}

		public RefnumPair(string refnumCurrent)
		{
			this.current = refnumCurrent;
			this.history = string.Empty;
		}

		public RefnumPair(string refnumCurrent, string refnumHistory)
		{
			this.current = refnumCurrent;
			this.history = refnumHistory;
		}
	}

	public struct SiteInfo
	{
		private readonly string _refnum;
		private readonly string _smallRefnum;
		private readonly string _coLoc;
		private readonly int _studyYear;
		private readonly int _uomNumber;

		public string refnum
		{
			get
			{
				return this._refnum;
			}
		}

		public string smallRefnum
		{
			get
			{
				return this._smallRefnum;
			}
		}

		public string coLoc
		{
			get
			{
				return this._coLoc;
			}
		}
		
		public int studyYear
		{
			get
			{
				return this._studyYear;
			}
		}
		public int uomNumber
		{
			get
			{
				return this._uomNumber;
			}
		}

		public SiteInfo(string refnum, string smallRefnum, string coLoc, int studyYear, int uomNumber)
		{
			this._refnum = refnum;
			this._smallRefnum = smallRefnum;
			this._coLoc = coLoc;
			this._studyYear = studyYear;
			this._uomNumber = uomNumber;
		}
	}

	class ErrorHandler
	{
		private static string CellAddress(int r, int c)
		{
			string l = String.Empty;
			int m;

			while (c > 0)
			{
				m = (c - 1) % 26;
				l = Convert.ToChar(65 + m).ToString() + l;
				c = Convert.ToInt32((c - m) / 26);
			}

			return l + r.ToString();
		}

		public static List<string> EtlErrors = new List<string>();

		public static void Insert_UpLoadError(string FileType, string MethodName, string Refnum,
			Excel.Workbook wkb, Excel.Worksheet wks, Excel.Range rng, int row, int col, 
			string SqlProcedureName, Exception ex)
		{

			SqlConnection cn = new SqlConnection(Common.cnString());
			cn.Open();

			SqlCommand cmd = new SqlCommand("[stgFact].[Insert_UploadError]", cn);
			cmd.CommandType = CommandType.StoredProcedure;

			cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 20).Value = FileType;
			cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
			cmd.Parameters.Add("@FilePath", SqlDbType.VarChar, 512).Value = wkb.Path + "\\" + wkb.Name;

			#region Sheet

			string Sheet = string.Empty;

			if (Sheet != null)
			{
				Sheet = wks.Name;
			}
			else
			{
				Sheet = "Worksheet not initialized";
			};

			cmd.Parameters.Add("@Sheet", SqlDbType.VarChar, 128).Value = Sheet;

			#endregion

			#region Range

			string Range = string.Empty;

			if (Range != null)
			{
				Range = rng.AddressLocal;
				if (!string.IsNullOrEmpty(rng.Text)) { cmd.Parameters.Add("@CellText", SqlDbType.NVarChar, 128).Value = rng.Text; }

			}
			else
			{
				Range = "Range not initialized (" + Sheet + "!" + CellAddress(row, col) + ")";
			};

			cmd.Parameters.Add("@Range", SqlDbType.VarChar, 128).Value = Range;

			#endregion

			cmd.Parameters.Add("@ProcedureName", SqlDbType.NVarChar, 128).Value = SqlProcedureName;

			cmd.Parameters.Add("@SystemErrorMessage", SqlDbType.NVarChar, 128).Value = ex.Message;

			cmd.ExecuteNonQuery();

			cn.Close();

			AddToEtlList(MethodName, wks, rng, row, col, ex);

		}

		public static void AddToEtlList(string MethodName, Excel.Worksheet wks, Excel.Range rng, int row, int col, Exception ex)
		{
			string MessageBody = "Module: " + MethodName + "\r\n";
			MessageBody = MessageBody + "Range: " + wks.Name + "!" + CellAddress(row, col) + "\r\n";

			if (rng == null)
			{
				MessageBody = MessageBody + "Address Range failed to initialize: " + wks.Name + "!" + CellAddress(row, col) + " (s= " + wks.Name + ", r= " + row.ToString() + ", c= " + col.ToString() + ")\r\n\r\n";
			}
			else
			{
				MessageBody = MessageBody + "Cell Text: " + Convert.ToString(rng.Text) + "\r\n\r\n";
			}

			EtlErrors.Add(MessageBody + ex.Message);
		}
		
		public static string AggregateMessage()
		{
			string msg = "";

			foreach (string s in EtlErrors)
			{
				if (msg != "") { msg = msg + "\r\n\r\n"; };
				msg = msg + s;
			}

			return msg;
		}

		//public static void Delete_UploadErrors(string FileType, string Refnum)
		//{
		//	string sRefnum = Common.GetRefnumSmall(Refnum);

		//	SqlConnection cn = new SqlConnection(Common.cnString());
		//	cn.Open();

		//	SqlCommand cmd = new SqlCommand("[stgFact].[Delete_UploadErrors]", cn);
		//	cmd.CommandType = CommandType.StoredProcedure;

		//	cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 20).Value = FileType;
		//	cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 18).Value = sRefnum;

		//	cmd.ExecuteNonQuery();

		//	cn.Close();
		//}
	}
}
