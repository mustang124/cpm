﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	public class XL
	{
		public static Excel.Application NewExcelApplication(bool EnableUi)
		{
			Excel.Application xla = new Excel.Application();

			xla.AutomationSecurity = Microsoft.Office.Core.MsoAutomationSecurity.msoAutomationSecurityLow;
			xla.FileValidation = Microsoft.Office.Core.MsoFileValidationMode.msoFileValidationSkip;

			xla.Visible = EnableUi;
			xla.ScreenUpdating = EnableUi;
			xla.Interactive = EnableUi;
			xla.EnableEvents = EnableUi;

			xla.AskToUpdateLinks = EnableUi;
			xla.DisplayAlerts = EnableUi;

			return xla;
		}

		public static Excel.Workbook OpenWorkbook_ReadOnly(Excel.Application xla, String PathFile)
		{
			PathFile = System.IO.Path.GetFullPath(PathFile);
			Excel.Workbook wkb = xla.Workbooks.Open(PathFile, Excel.XlUpdateLinks.xlUpdateLinksNever, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, true, Type.Missing, false, Type.Missing, Excel.XlCorruptLoad.xlNormalLoad);

			wkb.EnableAutoRecover = false;

			xla.Calculation = Excel.XlCalculation.xlCalculationManual;

			return wkb;
		}

		public static void CloseExcel(ref Excel.Application xla, ref Excel.Workbook wkb, ref Excel.Worksheet wks)
		{
			if (wks != null)
			{
				Marshal.FinalReleaseComObject(wks);
				wks = null;
			}

			CloseExcel(ref xla, ref wkb);
		}

		public static void CloseExcel(ref Excel.Application xla, ref Excel.Workbook wkb)
		{
			if (wkb != null)
			{
				wkb.Close(false);
				Marshal.ReleaseComObject(wkb);
				wkb = null;
			}

			if (xla != null)
			{
				xla.Quit();
				Marshal.ReleaseComObject(xla);
				xla = null;
			}

			System.GC.Collect();
			System.GC.WaitForPendingFinalizers();
			System.GC.WaitForFullGCComplete();
		}

		internal static double GetForex(Excel.Range rng)
		{
			//	The exchange rate is for foreign to dollar; therefore, the exchange rate needs
			//	to be inverted to calculate the	foreign rate from the dollar.
			//	if the rate is zero (0.0) set it to 1.0; otherwise invert the exchange rate.
			if (rng.Text != string.Empty)
			{
				return (rng.Value == 0.0) ? 1.0 : 1.0 / rng.Value;
			}
			else
			{
				return 1.0;
			}
		}

		internal static dynamic GetCellValue(Excel.Workbook wkb, string tabNamePrefix, RangeReference rangeReference)
		{
			string sheetPrefix = (string.IsNullOrEmpty(rangeReference.sheetPrefix)) ? tabNamePrefix : rangeReference.sheetPrefix;
			string sheetName = sheetPrefix + rangeReference.sheetSuffix;

			Excel.Worksheet wks = wkb.Worksheets[sheetName];
			Excel.Range rng = wks.Cells[rangeReference.row, rangeReference.col];

			if (rng.Value != null && rng.Value.ToString() != string.Empty)
			{
				return rng.Value;
			}
			else
			{
				return null;
			}
		}

		internal static void SetCellValue(SqlDataReader rdr, string columnName, Excel.Worksheet wks, int row, int col, bool forceIn)
		{
			int ord = rdr.GetOrdinal(columnName);
			Excel.Range rng = wks.Cells[row, col];

			if (!rdr.IsDBNull(ord) && (!rng.Locked || forceIn))
			{
				rng.Value = rdr.GetValue(ord);
			}
		}

		internal static void SetCellValue(SqlDataReader rdr, string columnName, Excel.Workbook wkb, string tabNamePrefix, RangeReference rangeReference)
		{
			string sheetPrefix = (string.IsNullOrEmpty(rangeReference.sheetPrefix)) ? tabNamePrefix : rangeReference.sheetPrefix;
			string sheetName = sheetPrefix + rangeReference.sheetSuffix;

			if (wkb.SheetExists(sheetName))
			{
				try
				{

					Excel.Worksheet wks = wkb.Worksheets[sheetName];
					Excel.Range rng = wks.Cells[rangeReference.row, rangeReference.col];

					SetCellValue(rdr, columnName, rng, rangeReference);
				}
				catch (Exception e)
				{
					string msg = e.Message;
				}
			}
		}

		internal static void SetCellValue(SqlDataReader rdr, string columnName, Excel.Workbook wkb, string tabNamePrefix, RangeReference rangeReference, bool forceIn)
		{
			string sheetPrefix = (string.IsNullOrEmpty(rangeReference.sheetPrefix)) ? tabNamePrefix : rangeReference.sheetPrefix;
			string sheetName = sheetPrefix + rangeReference.sheetSuffix;

			if (wkb.SheetExists(sheetName))
			{
				Excel.Worksheet wks = wkb.Worksheets[sheetName];
				Excel.Range rng = wks.Cells[rangeReference.row, rangeReference.col];

				rangeReference.forceIn = rangeReference.forceIn || forceIn;

				SetCellValue(rdr, columnName, rng, rangeReference);
			}
		}

		private static void SetCellValue(SqlDataReader rdr, string columnName, Excel.Range rng, RangeReference rangeReference)
		{
			int ord;

			try
			{
				if (rdr.HasColumn(columnName, out ord))
				{
					if (!rdr.IsDBNull(ord))// && (!rng.Locked || rangeReference.forceIn))
					{
						if (rangeReference.sqlType == SqlDbType.VarChar || double.IsNaN(rangeReference.divisor) || rangeReference.divisor == 0.0)
						{
							rng.Value = rdr.GetValue(ord);
						}
						else
						{
							if (rangeReference.sheetSuffix == "4")
							{
								rng.Value = double.Parse(rdr.GetValue(ord).ToString());
							}
							else
							{
								rng.Value = double.Parse(rdr.GetValue(ord).ToString()) / rangeReference.divisor;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				string excp = ex.InnerException.ToString();
			}
		}
	}

	public class RangeReference
	{
		readonly public string sheetPrefix;
		readonly public string sheetSuffix;

		readonly public int row;
		readonly public int col;

		readonly public double divisor;
		readonly public SqlDbType sqlType;

		public bool forceIn;

		public RangeReference()
		{
			this.sheetPrefix = string.Empty;
			this.sheetSuffix = string.Empty;
			this.row = 0;
			this.col = 0;

			this.divisor = double.NaN;
			this.sqlType = SqlDbType.Variant;
			this.forceIn = false;
		}


		public RangeReference(string sheetSuffix, int rowNumber, int colNumber)
		{
			this.sheetPrefix = string.Empty;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;

			this.divisor = double.NaN;
			this.sqlType = SqlDbType.Variant;
			this.forceIn = false;
		}

		public RangeReference(string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType)
		{
			this.sheetPrefix = string.Empty;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = double.NaN;
			this.sqlType = sqlType;
			this.forceIn = false;
		}

		public RangeReference(string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType, double divisor)
		{
			this.sheetPrefix = string.Empty;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = divisor;
			this.sqlType = sqlType;
			this.forceIn = false;
		}

		public RangeReference(string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType, bool forceIn)
		{
			this.sheetPrefix = string.Empty;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = double.NaN;
			this.sqlType = sqlType;
			this.forceIn = forceIn;
		}

		public RangeReference(string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType, double divisor, bool forceIn)
		{
			this.sheetPrefix = string.Empty;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = divisor;
			this.sqlType = sqlType;
			this.forceIn = forceIn;
		}


		public RangeReference(string sheetPrefix, string sheetSuffix, int rowNumber, int colNumber)
		{
			this.sheetPrefix = sheetPrefix ;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;

			this.divisor = double.NaN;
			this.sqlType = SqlDbType.Variant;
			this.forceIn = false;
		}

		public RangeReference(string sheetPrefix, string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType)
		{
			this.sheetPrefix = sheetPrefix ;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = double.NaN;
			this.sqlType = sqlType;
			this.forceIn = false;
		}

		public RangeReference(string sheetPrefix, string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType, double divisor)
		{
			this.sheetPrefix = sheetPrefix ;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = divisor;
			this.sqlType = sqlType;
			this.forceIn = false;
		}

		public RangeReference(string sheetPrefix, string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType, bool forceIn)
		{
			this.sheetPrefix = sheetPrefix ;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = double.NaN;
			this.sqlType = sqlType;
			this.forceIn = forceIn;
		}

		public RangeReference(string sheetPrefix, string sheetSuffix, int rowNumber, int colNumber, SqlDbType sqlType, double divisor, bool forceIn)
		{
			this.sheetPrefix = sheetPrefix ;
			this.sheetSuffix = sheetSuffix;
			this.row = rowNumber;
			this.col = colNumber;
			this.divisor = divisor;
			this.sqlType = sqlType;
			this.forceIn = forceIn;
		}
	}

	public static class Extensions
	{
		public static bool SheetExists(this Excel.Workbook wkb, string sheetName)
		{
			foreach (Excel.Worksheet wks in wkb.Sheets)
			{
				if (wks.Name == sheetName)
				{
					return true;
				}
			}
			return false;
		}

		public static bool HasColumn(this IDataRecord dr, string columnName, out int ordinal)
		{
			for (int i = 0; i < dr.FieldCount; i++)
			{
				if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
				{
					ordinal = dr.GetOrdinal(columnName);
					return true;
				}
			}
			ordinal = -1;
			return false;
		}
	}
}