﻿DROP PROCEDURE [fact].[Select_PlantName];
DROP VIEW [dbo].[TSort];
DROP FUNCTION [fact].[PlantNameHistoryLimit];
DROP VIEW [fact].[PlantName];
DROP VIEW [fact].[InflationTagging];
DROP VIEW [fact].[TSort];
DROP VIEW [cons].[TSort];
GO

:r .\..\..\..\dbs.Olefins.Core\cons\Views\cons.TSort.sql
:r .\..\..\..\dbs.Olefins.Core\fact\Views\fact.TSort.sql
:r .\..\..\..\dbs.Olefins.Core\dbo\Views\dbo.TSort.sql
:r .\..\..\..\dbs.Olefins.Core\fact\Views\fact.PlantName.sql
:r .\..\..\..\dbs.Olefins.Core\fact\Views\fact.InflationTagging.sql
:r .\..\..\..\dbs.Olefins.Core\fact\Functions\fact.PlantNameHistoryLimit.sql
:r .\..\..\..\dbs.Olefins.Core\fact\"Stored Procedures"\Select\fact.Select_PlantName.sql

