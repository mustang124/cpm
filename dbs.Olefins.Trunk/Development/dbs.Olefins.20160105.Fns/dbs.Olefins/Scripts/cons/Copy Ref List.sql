﻿SELECT
	*
FROM
	[reports].[GroupPlants] [r]
WHERE
	[r].[GroupID] = '09PCH';

INSERT INTO [reports].[GroupPlants]
(
	[GroupID],
	[Refnum]
)
SELECT
	[l].[ListName],
	'20' + [l].[Refnum]
FROM
	[OlefinsLegacy].[dbo].[_RL] [l]
WHERE
	[l].[ListName] = '09PCH';