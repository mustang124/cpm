﻿--SELECT DATEDIFF(d, '1/1/2000', SYSDATETIME())

DECLARE @Beg			DATETIMEOFFSET(7)	= SYSDATETIMEOFFSET();

DECLARE @Refnum			VARCHAR(18) = '2013PCH010';
DECLARE @sRefnum		VARCHAR(18) = RIGHT(@Refnum, LEN(@Refnum) - 2);
DECLARE @FactorSetID	VARCHAR(4)	= '2013'; -- LEFT(@Refnum, 4);
DECLARE @StudyId		VARCHAR(4)	= 'PCH';

SET NOCOUNT ON;

DECLARE @ProcedureDesc	NVARCHAR(4000);

--EXECUTE [dbo].[spCalcs] @Refnum;

--SELECT * FROM [dbo].[LogError]	[le] WHERE [le].[Refnum] = @Refnum ORDER BY [le].[tsModified] DESC;

DECLARE @fpl	[calc].[FoundationPlantList];

		INSERT INTO @fpl
		(
			[FactorSetId],
			[FactorSetName],
			[FactorSet_AnnDateKey],
			[FactorSet_QtrDateKey],
			[FactorSet_QtrDate],
			[Refnum],
			[Plant_AnnDateKey],
			[Plant_QtrDateKey],
			[Plant_QtrDate],
			[CompanyId],
			[AssetId],
			[AssetName],
			[SubscriberCompanyName],
			[PlantCompanyName],
			[SubscriberAssetName],
			[PlantAssetName],
			[CountryId],
			[StateName],
			[EconRegionId],
			[UomId],
			[CurrencyFcn],
			[CurrencyRpt],
			[AssetPassWord_PlainText],
			[StudyYear],
			[DataYear],
			[Consultant],
			[StudyYearDifference],
			[CalQtr]
		)
		SELECT
			tsq.[FactorSetId],
			tsq.[FactorSetName],
			tsq.[FactorSet_AnnDateKey],
			tsq.[FactorSet_QtrDateKey],
			tsq.[FactorSet_QtrDate],
			tsq.[Refnum],
			tsq.[Plant_AnnDateKey],
			tsq.[Plant_QtrDateKey],
			tsq.[Plant_QtrDate],
			tsq.[CompanyId],
			tsq.[AssetId],
			tsq.[AssetName],
			tsq.[SubscriberCompanyName],
			tsq.[PlantCompanyName],
			tsq.[SubscriberAssetName],
			tsq.[PlantAssetName],
			tsq.[CountryId],
			tsq.[StateName],
			tsq.[EconRegionId],
			tsq.[UomId],
			tsq.[CurrencyFcn],
			tsq.[CurrencyRpt],
			tsq.[AssetPassWord_PlainText],
			tsq.[StudyYear],
			tsq.[DataYear],
			tsq.[Consultant],
			tsq.[StudyYearDifference],
			tsq.[CalQtr]
		FROM [calc].[FoundationPlantListSource]						tsq
		WHERE	tsq.Refnum = @Refnum
			AND	tsq.FactorSetId = ISNULL(@FactorSetId, tsq.FactorSetId);


ALTER PROCEDURE [calc].[Insert_MaintCostAverage_Swb]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY
	
		INSERT INTO calc.MaintCostAverage(FactorSetId, Refnum, CalDateKey, CurrencyRpt, AccountId, MaintPrev_Cur, MaintCurr_Cur)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			'Swb',
			(r.Maint_Cur - COALESCE(m.Amount_Cur, 0.0) - COALESCE(f.InflAdjAnn_MaintRetube_Cur, 0.0)) * aa.MaintPrev_Pcnt / 100.0,
			 r.Maint_Cur - COALESCE(m.Amount_Cur, 0.0) - COALESCE(f.InflAdjAnn_MaintRetube_Cur, 0.0)
		FROM @fpl						fpl
		INNER JOIN calc.MaintCostRoutineAggregate	r
			ON	r.FactorSetId		= fpl.FactorSetId
			AND	r.Refnum			= fpl.Refnum
			AND	r.CalDateKey		= fpl.Plant_QtrDateKey
			AND	r.CurrencyRpt		= fpl.CurrencyRpt
			AND	r.FacilityId		= 'TotUnitCnt'

		LEFT OUTER JOIN fact.MaintOpExAggregate		m
			ON	m.Refnum			= fpl.Refnum
			AND	m.CalDateKey		= fpl.Plant_QtrDateKey
			AND	m.CurrencyRpt		= fpl.CurrencyRpt
			AND	m.AccountId			= 'MaintRoutine'
			AND	m.Amount_Cur		<> 0.0

		LEFT OUTER JOIN calc.MaintPersAvgAggregate	aa
			ON	aa.FactorSetId		= fpl.FactorSetId
			AND	aa.Refnum			= fpl.Refnum
			AND	aa.CalDateKey		= fpl.Plant_QtrDateKey

		LEFT OUTER JOIN calc.FurnacesAggregate		f
			ON	f.FactorSetId		= fpl.FactorSetId
			AND	f.Refnum			= fpl.Refnum
			AND	f.CalDateKey		= fpl.Plant_QtrDateKey
			AND	f.CurrencyRpt		= fpl.CurrencyRpt

		WHERE	fpl.CalQtr		= 4
			AND	(r.Maint_Cur - COALESCE(m.Amount_Cur, 0.0) - COALESCE(f.InflAdjAnn_MaintRetube_Cur, 0.0)) > 0.0
			AND	aa.MaintCurr_Hrs > 2.0

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;



			--select * from fact.MaintOpExAggregate where refnum = '2013pch010'