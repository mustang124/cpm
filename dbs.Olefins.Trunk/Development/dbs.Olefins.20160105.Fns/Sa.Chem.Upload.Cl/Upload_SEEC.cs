﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Threading.Tasks;

namespace Chem.UpLoad
{
	public class Upload_SEEC: Chem.UpLoad.InputForm
	{
		public void UpLoad(string Refnum, string PathFile)
		{
			string sRefnum = Common.GetRefnumSmall(Refnum);
			uint StudyYear = Common.GetStudyYear(Refnum);

			Delete(sRefnum);
			Insert(PathFile, sRefnum, StudyYear);
		}

		void Insert(string PathFile, string sRefnum, uint StudyYear)
		{
			Excel.Application xla = Common.NewExcelApplication(false);
			Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, PathFile);

			bool includeSpace = ((StudyYear >= 2015) && (Common.GetFileType(PathFile) == Common.Osim));

			Parallel.Invoke
			(
				() => { UpLoadTSort(wkb, sRefnum, StudyYear, includeSpace); },
				() => { UpLoadCapacity(wkb, sRefnum, StudyYear, includeSpace); },
				() => { UpLoadFacilities(wkb, sRefnum, StudyYear, includeSpace); },
				() => { UpLoadFeedQuality(wkb, sRefnum, includeSpace); },
				() => { UpLoadQuantity(wkb, sRefnum, includeSpace); },
				() => { UpLoadEnergyQnty(wkb, sRefnum, StudyYear, includeSpace); },

				() => { UpLoadMisc_SEEC(wkb, sRefnum, StudyYear); },

				() => { UpLoadProdQuality(wkb, sRefnum, includeSpace); },
				() => { UpLoadComposition(wkb, sRefnum, includeSpace); }
			);

			Common.CloseExcel(ref xla, ref wkb);
		}
	}
}
