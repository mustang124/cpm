﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadProdLoss2015(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;

			string Category = string.Empty;

			float sdP;
			float dtP;
			float sdC;
			float dtC;
			float qty;

			string cT06_01 = (includeSpace) ? "Table 6-1" : "Table6-1";
			wks = wkb.Worksheets[cT06_01];

			object[] itm = new object[8] 
			{
				new object[3] { "TA", 10, 10 },
				new object[3] { "PRO", 12, 19 },
				new object[3] { "EQF", 21, 28 },
				new object[3] { "UTF", 30, 34 },
				new object[3] { "NOC", 36, 43 },

				new object[3] { "FFP", 47, 50 },
				new object[3] { "OTH", 54, 57 },
				new object[3] { "NOP", 83, 86 }
			};

			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				foreach (object[] s in itm)
				{
					try
					{
						Category = Convert.ToString(s[0]);
						rBeg = Convert.ToUInt32(s[1]);
						rEnd = Convert.ToUInt32(s[2]);

						for (r = rBeg; r <= rEnd; r++)
						{
							qty = 0f;

							rng = wks.Cells[r, 6];
							if (RangeHasValue(rng)) { qty = ReturnFloat(rng); }

							rng = wks.Cells[r, 7];
							if (RangeHasValue(rng)) { qty = qty + ReturnFloat(rng); }

							rng = wks.Cells[r, 8];
							if (RangeHasValue(rng)) { qty = qty + ReturnFloat(rng); }

							rng = wks.Cells[r, 9];
							if (RangeHasValue(rng)) { qty = qty + ReturnFloat(rng); }

							if (qty > 0.0f)
							{
								using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_ProdLoss]", cn))
								{
									cmd.CommandType = CommandType.StoredProcedure;

									//@Refnum		CHAR (9),
									cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

									//@Category    CHAR (3),
									cmd.Parameters.Add("@Category", SqlDbType.VarChar, 3).Value = Category;

									if ((r >= 47 && r <= 50) || (r >= 54 && r <= 57) || (r >= 83 && r <= 86))
									{
										c = 2;

										//@CauseID     VARCHAR (20),
										if (r >= 47 && r <= 50)
										{
											cmd.Parameters.Add("@CauseID", SqlDbType.VarChar, 20).Value = "Cause" + (r - rBeg + 1).ToString();
										}

										if (r >= 54 && r <= 57)
										{
											cmd.Parameters.Add("@CauseID", SqlDbType.VarChar, 20).Value = "OtherProcess" + (r - rBeg + 1).ToString();
										}

										if (r >= 83 && r <= 86)
										{
											cmd.Parameters.Add("@CauseID", SqlDbType.VarChar, 20).Value = "OtherNonOp" + (r - rBeg + 1).ToString();
										}

										//@Description VARCHAR (250) = NULL,
										rng = wks.Cells[r, c];
										cmd.Parameters.Add("@Description", SqlDbType.VarChar, 250).Value = ReturnString(rng, 250);
									}
									else
									{
										//@CauseID     VARCHAR (20),
										c = 2;
										rng = wks.Cells[r, c];
										cmd.Parameters.Add("@CauseID", SqlDbType.VarChar, 20).Value = ConvertLossCause(r);
									}

									#region Previous

									dtP = 0.0f;
									sdP = 0.0f;

									//@PrevDTLoss  REAL          = NULL,
									c = 6;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng))
									{
										dtP = ReturnFloat(rng);
										cmd.Parameters.Add("@PrevDTLoss", SqlDbType.Float).Value = ReturnFloat(rng);
									}
									else
									{
										dtP = 0.0f;
									}

									//@PrevSDLoss  REAL          = NULL,
									c = 7;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng))
									{
										sdP = ReturnFloat(rng);
										cmd.Parameters.Add("@PrevSDLoss", SqlDbType.Float).Value = ReturnFloat(rng);
									}
									else
									{
										sdP = 0.0f;
									}

									//@PrevTotLoss REAL          = NULL,
									if (dtP + sdP > 0.0f)
									{
										cmd.Parameters.Add("@PrevTotLoss", SqlDbType.Float).Value = dtP + sdP;
									}

									#endregion

									#region Current

									dtC = 0.0f;
									sdC = 0.0f;

									//@DTLoss      REAL          = NULL,
									c = 8;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng))
									{
										dtC = ReturnFloat(rng);
										cmd.Parameters.Add("@DTLoss", SqlDbType.Float).Value = ReturnFloat(rng);
									}
									else
									{
										dtC = 0.0f;
									}

									//@SDLoss      REAL          = NULL,
									c = 9;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng))
									{
										sdC = ReturnFloat(rng);
										cmd.Parameters.Add("@SDLoss", SqlDbType.Float).Value = ReturnFloat(rng);
									}
									else
									{
										sdC = 0.0f;
									}

									//@TotLoss     REAL          = NULL,
									if (dtC + sdC > 0.0f)
									{
										cmd.Parameters.Add("@TotLoss", SqlDbType.Float).Value = dtC + sdC;
									}

									#endregion

									#region Annual

									//@AnnDTLoss   REAL          = NULL,
									if (dtP + dtC > 0.0f)
									{
										cmd.Parameters.Add("@AnnDTLoss", SqlDbType.Float).Value = (dtP + dtC) / 2.0f;
									}
									else
									{
										cmd.Parameters.Add("@AnnDTLoss", SqlDbType.Float).Value = 0.0f;
									}

									//@AnnSDLoss   REAL          = NULL,
									if (sdP + sdC > 0.0f)
									{
										cmd.Parameters.Add("@AnnSDLoss", SqlDbType.Float).Value = (sdP + sdC) / 2.0f;
									}
									else
									{
										cmd.Parameters.Add("@AnnSDLoss", SqlDbType.Float).Value = 0.0f;
									}

									//@AnnTotLoss  REAL          = NULL
									if (dtP + dtC + sdP + sdC > 0.0f)
									{
										cmd.Parameters.Add("@AnnTotLoss", SqlDbType.Float).Value = (dtP + dtC + sdP + sdC) / 2.0f;
									}
									else
									{
										cmd.Parameters.Add("@AnnTotLoss", SqlDbType.Float).Value = 0.0f;
									}

									#endregion

									cmd.ExecuteNonQuery();
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadProdLoss", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_ProdLoss]", ex);
					}
				}
				cn.Close();
			}
			rng = null;
			wks = null;
			itm = null;
		}
	}
}