﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadFeedPlan(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			string cT11_01 = (includeSpace) ? "Table 11" : "Table11";
			wks = wkb.Worksheets[cT11_01];

			object[] itm = new object[12]
			{
				new object[2] { "FeedPrice", 137 },
				new object[2] { "ProductPrice", 138 },
				new object[2] { "YieldPattern", 139 },
				new object[2] { "CFE", 140 },
				new object[2] { "LPConstraints", 141 },
				new object[2] { "AFS", 142 },
				new object[2] { "LPActPlantOper", 143 },
				new object[2] { "LPCaseAFS", 144 },
				new object[2] { "ProcessData", 145 },
				new object[2] { "AcctMatlBal", 146 },
				new object[2] { "OperPlan", 147 },
				new object[2] { "AuditFeed", 149 }
			};

			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				foreach (object[] s in itm)
				{
					try
					{
						r = Convert.ToUInt32(s[1]);

						if (StudyYear >= 2009) r = r + 2;

						using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_FeedPlan]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							//@Refnum		CHAR (9)
							cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

							//@PlanID      VARCHAR (25),
							cmd.Parameters.Add("@PlanID", SqlDbType.VarChar, 25).Value = Convert.ToString(s[0]);

							//@Daily       CHAR (1)     = NULL
							c = 6;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Daily", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

							//@BiWeekly    CHAR (1)     = NULL
							c = 7;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@BiWeekly", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

							//@Weekly      CHAR (1)     = NULL
							c = 8;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Weekly", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

							//@BiMonthly   CHAR (1)     = NULL
							c = 9;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@BiMonthly", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

							//@Monthly     CHAR (1)     = NULL
							c = 10;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@Monthly", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

							//@LessMonthly CHAR (1)     = NULL
							c = 11;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@LessMonthly", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

							//@Rpt         TINYINT      = NULL
							rng = wks.Range["F" + r.ToString() + ":K" + r.ToString()];
							if (RangeHasValue(rng))
							{
								cmd.Parameters.Add("@Rpt", SqlDbType.TinyInt, 1).Value = 1;
							}
							else
							{
								cmd.Parameters.Add("@Rpt", SqlDbType.TinyInt, 1).Value = 0;
							}

							cmd.ExecuteNonQuery();
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFeedPlan", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_FeedPlan]", ex);
					}
				}
				cn.Close();
			}
			rng = null;
			wks = null;
		}
	}
}