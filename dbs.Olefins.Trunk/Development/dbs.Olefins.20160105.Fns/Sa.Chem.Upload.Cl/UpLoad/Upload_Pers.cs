﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPers(Excel.Workbook wkb, string Refnum, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;

			string sht = string.Empty;
			string SectionID = string.Empty;

			string cT05_A = (includeSpace) ? "Table 5A" : "Table5A";
			string cT05_B = (includeSpace) ? "Table 5B" : "Table5B";

			object[] itm = new object[9]
			{
				new object[4] { cT05_A, 10, 17, "OP" },
				new object[4] { cT05_A, 21, 27, "OM"  },
				new object[4] { cT05_A, 31, 36, "OA"  },

				new object[4] { cT05_B, 9, 9, "MP"  },
				new object[4] { cT05_B, 12, 16, "MM"  },
				new object[4] { cT05_B, 19, 19, "ML"  },
				new object[4] { cT05_B, 22, 28, "MT"  },
				new object[4] { cT05_B, 31, 35, "MA"  },
				new object[4] { cT05_B, 38, 39, "MG"  }
			};

			using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
			{
				cn.Open();

				foreach (object[] s in itm)
				{
					sht = (string)s[0];

					wks = wkb.Worksheets[sht];

					rBeg = Convert.ToUInt32(s[1]);
					rEnd = Convert.ToUInt32(s[2]);
					SectionID = (string)s[3];

					for (r = rBeg; r <= rEnd; r++)
					{
						try
						{
							rng = wks.Range["D" + r.ToString() + ":G" + r.ToString()];
							if (RangeHasValue(rng))
							{
								using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Pers]", cn))
								{
									cmd.CommandType = CommandType.StoredProcedure;

									//@Refnum		CHAR (9),
									cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

									//@SortKey		VARCHAR (6),
									cmd.Parameters.Add("@SortKey", SqlDbType.Int).Value = (ushort)r;

									//@SectionID	CHAR (4)  = NULL,
									cmd.Parameters.Add("@SectionID", SqlDbType.VarChar, 4).Value = SectionID;

									//@PersID    CHAR (15),
									c = 2;
									rng = wks.Cells[r, c];
									cmd.Parameters.Add("@PersID", SqlDbType.VarChar, 15).Value = ConvertPersID(rng, c);

									#region Personnel Data

									//@NumPers   REAL      = NULL,
									c = 4;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@NumPers", SqlDbType.Float).Value = ReturnFloat(rng); }

									//@STH       REAL      = NULL,
									c = 5;
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@STH", SqlDbType.Float).Value = ReturnFloat(rng); }

									if (wks.Name == cT05_A)
									{
										//@OvtHours  REAL      = NULL,
										c = 6;
										rng = wks.Cells[r, c];
										if (RangeHasValue(rng)) { cmd.Parameters.Add("@OvtHours", SqlDbType.Float).Value = ReturnFloat(rng); }
									}

									if (wks.Name == cT05_A) { c = 7; } else { c = 6; };

									//@Contract  REAL      = NULL	
									rng = wks.Cells[r, c];
									if (RangeHasValue(rng)) { cmd.Parameters.Add("@Contract", SqlDbType.Float).Value = ReturnFloat(rng); }

									#endregion

									cmd.ExecuteNonQuery();
								}
							}
						}
						catch (Exception ex)
						{
							ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPers", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Pers]", ex);
						}
					}
				}
				cn.Close();
			}
			rng = null;
			wks = null;
			itm = null;
		}
	}
}