﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		//Refnum		CalDateKey		StreamID	ComponentID		Recycled_WtPcnt

		public void SupplementalRecycled(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string streamId;

			Dictionary<string, int> rowRecycle = RecycleRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "2C"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_QuantitySuppRecycled]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamID"));

								if (rowRecycle.TryGetValue(streamId, out r))
								{
									AddRangeValues(wks, r, 3, rdr, "Recycled_WtPcnt", 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "QuantitySuppRecycled", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_QuantitySuppRecycled]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> RecycleRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("SuppToRecEthane", 44);
			d.Add("SuppToRecPropane", 45);
			d.Add("SuppToRecButane", 46);

			return d;
		}
	}
}

