﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void PolyFacilities(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string traindId;
			string countryIso;
			string countryName;

			Dictionary<string, int> polyCol = PolyTrainColDictionary();
			Dictionary<string, RangeReference> polyFacilities = PolyFacilitiesDictionary();
			Dictionary<string, RangeReference> polyFacilitiesPcnt = PolyFacilitiesPcntDictionary();
			Dictionary<string, string> countries = CountryDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_PolyFacilities]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								traindId = rdr.GetString(rdr.GetOrdinal("AssetId"));
								traindId = traindId.Substring(traindId.Length - 2);

								if (polyCol.TryGetValue(traindId, out c))
								{
									foreach (KeyValuePair<string, RangeReference> entry in polyFacilities)
									{
										wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
										r = entry.Value.row;

										if (entry.Key == "PolyCountryID")
										{
											countryIso = (string)rdr.GetValue(rdr.GetOrdinal(entry.Key));

											if (countries.TryGetValue(countryIso, out countryName))
											{
												wks.Cells[r, c].value2 = countryName;
											}
										}
										else
										{
											AddRangeValues(wks, r, c, rdr, entry.Key);
										}
									}

									foreach (KeyValuePair<string, RangeReference> entry in polyFacilitiesPcnt)
									{
										wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
										r = entry.Value.row;
										AddRangeValues(wks, r, c, rdr, entry.Key, 100.0);
									}
								}
							}
						}
					}
					cn.Close();
				};
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "PolyFacilities", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_PolyFacilities]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, string> CountryDictionary()
		{
			Dictionary<string, string> d = new Dictionary<string, string>();

			d.Add("ARG", "ARGENTINA");
			d.Add("AUS", "AUSTRALIA");
			d.Add("AUT", "AUSTRIA");
			d.Add("BEL", "BELGIUM");
			d.Add("BRA", "BRAZIL");
			d.Add("CAN", "CANADA");
			d.Add("CHL", "CHILE");
			d.Add("COL", "COLOMBIA");
			d.Add("CZE", "CZECH REP");
			d.Add("FIN", "FINLAND");
			d.Add("FRA", "FRANCE");
			d.Add("DEU", "GERMANY");
			d.Add("HUN", "HUNGARY");
			d.Add("IND", "INDIA");
			d.Add("IDN", "INDONESIA");
			d.Add("ISR", "ISRAEL");
			d.Add("ITA", "ITALY");
			d.Add("JPN", "JAPAN");
			d.Add("KOR", "SOUTH KOREA");
			d.Add("KWT", "KUWAIT");
			d.Add("MYS", "MALAYSIA");
			d.Add("MEX", "MEXICO");
			d.Add("NLD", "NETHERLANDS");
			d.Add("NOR", "NORWAY");
			d.Add("POL", "POLAND");
			d.Add("PRT", "PORTUGAL");
			d.Add("CHN", "PRC CHINA");
			d.Add("QAT", "QATAR");
			d.Add("ROU", "ROMANIA");
			d.Add("RUS", "RUSSIA");
			d.Add("SAU", "SAUDI ARABIA");
			d.Add("SGP", "SINGAPORE");
			d.Add("SVK", "SLOVAKIA");
			d.Add("ZAF", "SOUTH AFRICA");
			d.Add("ESP", "SPAIN");
			d.Add("SWE", "SWEDEN");
			d.Add("TWN", "TAIWAN");
			d.Add("THA", "THAILAND");
			d.Add("TUR", "TURKEY");
			d.Add("ARE", "UAE");
			d.Add("GBR", "UNITED KINGDOM");
			d.Add("USA", "USA");

			return d;
		}

		private Dictionary<string, RangeReference> PolyFacilitiesDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("PolyName", new RangeReference(T12_01, 6, 0));
			d.Add("PolyCity", new RangeReference(T12_01, 7, 0));
			d.Add("PolyState", new RangeReference(T12_01, 8, 0));
			d.Add("PolyCountryID", new RangeReference(T12_01, 9, 0));
			d.Add("Stream_MTd", new RangeReference(T12_01, 10, 0));
			d.Add("Capacity_kMT", new RangeReference(T12_01, 11, 0));
			d.Add("ReactorTrains_Count", new RangeReference(T12_01, 12, 0));
			d.Add("StartUp_Year", new RangeReference(T12_01, 13, 0));
			d.Add("PolymerFamilyID", new RangeReference(T12_01, 14, 0));
			d.Add("PolymerTechID", new RangeReference(T12_01, 15, 0));

			return d;
		}

		private Dictionary<string, RangeReference> PolyFacilitiesPcntDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("PrimeResin_Pcnt", new RangeReference(T12_01, 16, 0));

			return d;
		}

		private Dictionary<string, int> PolyTrainColDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("T1", 4);
			d.Add("T2", 5);
			d.Add("T3", 6);
			d.Add("T4", 7);
			d.Add("T5", 8);
			d.Add("T6", 9);

			return d;
		}
	}
}