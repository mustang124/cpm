﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void MaintOpExAggregate(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string accountId;
			RangeReference rr;

			Dictionary<string, RangeReference> maintOpEx = MaintOpExDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_MaintOpExAggregate]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								accountId = rdr.GetString(rdr.GetOrdinal("AccountId"));

								if (maintOpEx.TryGetValue(accountId, out rr))
								{
									wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
									r = rr.row;
									c = rr.col;
									AddRangeValues(wks, r, c, rdr, "AmountPrev_Cur");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "MaintOpExAggregate", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_MaintOpExAggregate]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> MaintOpExDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("MaintMatl", new RangeReference(T10_02, 15, 7));
			d.Add("MaintContractLabor", new RangeReference(T10_02, 16, 7));
			d.Add("MaintContractMatl", new RangeReference(T10_02, 17, 7));
			d.Add("MaintEquip", new RangeReference(T10_02, 18, 7));

			return d;
		}
	}
}