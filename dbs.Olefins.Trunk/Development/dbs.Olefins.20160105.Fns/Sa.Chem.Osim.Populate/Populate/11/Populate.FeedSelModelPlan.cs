﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void FeedSelModelPlan(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string planId;

			Dictionary<string, int> feedSelPlan = FeedSelPlanRowDictionary();
			Dictionary<string, int> feedSelFreq = FeedSelFrequencyRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + T11_01];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_FeedSelModelPlan]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								planId = rdr.GetString(rdr.GetOrdinal("PlanID"));

								if (feedSelPlan.TryGetValue(planId, out r))
								{
									foreach (KeyValuePair<string, int> entry in feedSelFreq)
									{
										c = entry.Value;
										AddRangeValues(wks, r, c, rdr, entry.Key);
									}
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "FeedSelModelPlan", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_FeedSelModelPlan]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> FeedSelPlanRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("FeedPrice", 139);
			d.Add("ProductPrice", 140);
			d.Add("YieldPattern", 141);
			d.Add("CFE", 142);
			d.Add("LPConstraints", 143);
			d.Add("AFS", 144);
			d.Add("LPActPlantOper", 145);
			d.Add("LPCaseAFS", 146);
			d.Add("ProcessData", 147);
			d.Add("AcctMatlBal", 148);
			d.Add("OperPlan", 149);
			d.Add("AuditFeed", 151);

			return d;
		}

		private Dictionary<string, int> FeedSelFrequencyRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Daily_X", 6);
			d.Add("BiWeekly_X", 7);
			d.Add("Weekly_X", 8);
			d.Add("BiMonthly_X", 9);
			d.Add("Monthly_X", 10);
			d.Add("LessMonthly_X", 11);

			return d;
		}
	}
}