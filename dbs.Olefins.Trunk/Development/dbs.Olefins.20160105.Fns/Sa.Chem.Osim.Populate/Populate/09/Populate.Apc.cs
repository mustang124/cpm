﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void Apc(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			Dictionary<string, RangeReference> apcReference = ApcDictionary();
			Dictionary<string, RangeReference> apcPcntReference = ApcPcntDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_Apc]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								foreach (KeyValuePair<string, RangeReference> entry in apcReference)
								{
									wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
									r = entry.Value.row;
									c = entry.Value.col;

									AddRangeValues(wks, r, c, rdr, entry.Key);
								}

								foreach (KeyValuePair<string, RangeReference> entry in apcPcntReference)
								{
									wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
									r = entry.Value.row;
									c = entry.Value.col;

									AddRangeValues(wks, r, c, rdr, entry.Key, 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "Apc", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_Apc]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> ApcDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("Loop_Count", new RangeReference(T09_01, 33, 6));

			d.Add("ApcMonitorId", new RangeReference(T09_02, 9, 10));
			d.Add("ApcAge_Years", new RangeReference(T09_02, 21, 9));
			d.Add("OffLineLinearPrograms_X", new RangeReference(T09_02, 28, 10));
			d.Add("OffLineNonLinearSimulators_X", new RangeReference(T09_02, 32, 10));
			d.Add("OffLineNonLinearInput_X", new RangeReference(T09_02, 36, 10));
			d.Add("OnLineClosedLoop_X", new RangeReference(T09_02, 44, 10));
			d.Add("OffLineComparison_Days", new RangeReference(T09_02, 48, 9));
			d.Add("OnLineComparison_Days", new RangeReference(T09_02, 50, 9));

			d.Add("ClosedLoopModelCokingPred_YN", new RangeReference(T09_03, 6, 9));
			d.Add("OptimizationOffLine_Mnths", new RangeReference(T09_03, 10, 9));
			d.Add("OptimizationPlanFreq_Mnths", new RangeReference(T09_03, 13, 9));
			d.Add("OptimizationOnLineOper_Mnths", new RangeReference(T09_03, 15, 9));
			d.Add("OnLineInput_Count", new RangeReference(T09_03, 18, 9));
			d.Add("OnLineVariable_Count", new RangeReference(T09_03, 21, 9));
			d.Add("OnLinePointCycles_Count", new RangeReference(T09_03, 29, 9));

			d.Add("PyroFurnModeledIndependent_YN", new RangeReference(T09_03, 32, 9));
			d.Add("OnLineAutoModeSwitch_YN", new RangeReference(T09_03, 38, 9));

			d.Add("DynamicResponse_Count", new RangeReference(T09_04, 10, 9));
			d.Add("StepTestSimple_Days", new RangeReference(T09_04, 12, 9));
			d.Add("StepTestComplex_Days", new RangeReference(T09_04, 13, 9));
			d.Add("StepTestUpdateFreq_Mnths", new RangeReference(T09_04, 15, 9));
			d.Add("AdaptiveAlgorithm_Count", new RangeReference(T09_04, 19, 9));
			d.Add("EmbeddedLP_Count", new RangeReference(T09_04, 23, 9));

			d.Add("MaxVariablesManipulated_Count", new RangeReference(T09_04, 28, 9));
			d.Add("MaxVariablesConstraint_Count", new RangeReference(T09_04, 29, 9));
			d.Add("MaxVariablesControlled_Count", new RangeReference(T09_04, 30, 9));

			return d;
		}

		private Dictionary<string, RangeReference> ApcPcntDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("ClosedLoopAPC_Pcnt", new RangeReference(T09_01, 37, 6));

			d.Add("OnLineOperation_Pcnt", new RangeReference(T09_03, 25, 9));

			return d;
		}
	}
}