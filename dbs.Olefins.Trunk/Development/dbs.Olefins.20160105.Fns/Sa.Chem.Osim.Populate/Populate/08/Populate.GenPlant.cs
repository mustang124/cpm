﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void GenPlant(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			Dictionary<string, RangeReference> genPlant = GenPlantRowDictionary();
			Dictionary<string, RangeReference> genPlantPcnt = GenPlantPcntRowDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_GenPlant]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								foreach (KeyValuePair<string, RangeReference> entry in genPlant)
								{
									wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
									r = entry.Value.row;
									c = entry.Value.col;
									AddRangeValues(wks, r, c, rdr, entry.Key);
								}

								foreach (KeyValuePair<string, RangeReference> entry in genPlantPcnt)
								{
									wks = wkb.Worksheets[tabNamePrefix + entry.Value.sheet];
									r = entry.Value.row;
									c = entry.Value.col;
									AddRangeValues(wks, r, c, rdr, entry.Key, 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "GenPlant", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_GenPlant]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> GenPlantRowDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("EthyleneCarrier_YN", new RangeReference("8-2", 7, 9));
			d.Add("ExtractButadiene_YN", new RangeReference("8-2", 8, 9));
			d.Add("ExtractAromatics_YN", new RangeReference("8-2", 9, 9));
			d.Add("MTBE_YN", new RangeReference("8-2", 10, 9));
			d.Add("Isobutylene_YN", new RangeReference("8-2", 11, 9));
			d.Add("IsobutyleneHighPurity_YN", new RangeReference("8-2", 12, 9));
			d.Add("Butene1_YN", new RangeReference("8-2", 13, 9));
			d.Add("Isoprene_YN",new RangeReference("8-2", 14, 9));
			d.Add("CycloPentadiene_YN", new RangeReference("8-2", 15, 9));
			d.Add("PolyPlantShare_YN", new RangeReference("8-2", 17, 9));
			d.Add("OlefinsDerivatives_YN", new RangeReference("8-2", 19, 9));
			d.Add("IntegrationRefinery_YN", new RangeReference("8-2", 20, 9));

			d.Add("OSIMPrep_Hrs", new RangeReference("8-4", 10, 5));

			return d;
		}

		private Dictionary<string, RangeReference> GenPlantPcntRowDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("CapCreepAnn_Pcnt", new RangeReference("8-2", 58, 8));

			return d;
		}
	}
}