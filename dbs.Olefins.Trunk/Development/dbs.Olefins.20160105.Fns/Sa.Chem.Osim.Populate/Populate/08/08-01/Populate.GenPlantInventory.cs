﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void GenPlantInventory(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string facilityId;

			Dictionary<string, int> genInventory = GenPlantInventoryRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "8-1"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_GenPlantInventory]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								facilityId = rdr.GetString(rdr.GetOrdinal("FacilityID"));

								if (genInventory.TryGetValue(facilityId, out r))
								{
									AddRangeValues(wks, r, 7, rdr, "StorageCapacity_kMT");
									AddRangeValues(wks, r, 8, rdr, "StorageLevel_Pcnt", 100.0);
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "GenPlantInventory", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_GenPlantInventory]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> GenPlantInventoryRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("StoreFeedAbove", 15);
			d.Add("StoreFeedUnder", 16);
			d.Add("StoreInter", 17);
			d.Add("StoreProdAbove", 19);
			d.Add("StoreProdUnder", 20);

			return d;
		}
	}
}