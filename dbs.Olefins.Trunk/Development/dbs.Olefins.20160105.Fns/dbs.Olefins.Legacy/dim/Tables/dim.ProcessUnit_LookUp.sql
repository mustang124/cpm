﻿CREATE TABLE [dim].[ProcessUnit_LookUp] (
    [ProcessUnitId]     INT                IDENTITY (1, 1) NOT NULL,
    [ProcessUnitTag]    VARCHAR (42)       NOT NULL,
    [ProcessUnitName]   VARCHAR (84)       NOT NULL,
    [ProcessUnitDetail] VARCHAR (256)      NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_ProcessUnit_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ProcessUnit_LookUp] PRIMARY KEY CLUSTERED ([ProcessUnitId] ASC),
    CONSTRAINT [CL_ProcessUnit_LookUp_ProcessUnitDetail] CHECK ([ProcessUnitDetail]<>''),
    CONSTRAINT [CL_ProcessUnit_LookUp_ProcessUnitName] CHECK ([ProcessUnitName]<>''),
    CONSTRAINT [CL_ProcessUnit_LookUp_ProcessUnitTag] CHECK ([ProcessUnitTag]<>''),
    CONSTRAINT [UK_ProcessUnit_LookUp_ProcessUnitDetail] UNIQUE NONCLUSTERED ([ProcessUnitDetail] ASC),
    CONSTRAINT [UK_ProcessUnit_LookUp_ProcessUnitName] UNIQUE NONCLUSTERED ([ProcessUnitName] ASC),
    CONSTRAINT [UK_ProcessUnit_LookUp_ProcessUnitTag] UNIQUE NONCLUSTERED ([ProcessUnitTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_ProcessUnit_LookUp_u]
	ON [dim].[ProcessUnit_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnit_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[ProcessUnit_LookUp].[ProcessUnitId]		= INSERTED.[ProcessUnitId];

END;