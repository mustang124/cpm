﻿CREATE TABLE [dim].[HydroTreaterType_Bridge] (
    [MethodologyId]      INT                 NOT NULL,
    [HydroTreaterTypeId] INT                 NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       INT                 NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_HydroTreaterType_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_HydroTreaterType_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_HydroTreaterType_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_HydroTreaterType_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_HydroTreaterType_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_HydroTreaterType_Bridge] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [HydroTreaterTypeId] ASC, [DescendantId] ASC),
    CONSTRAINT [FK_HydroTreaterType_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId]),
    CONSTRAINT [FK_HydroTreaterType_Bridge_DescendantOperator] FOREIGN KEY ([DescendantOperator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_HydroTreaterType_Bridge_HydroTreaterTypeId] FOREIGN KEY ([HydroTreaterTypeId]) REFERENCES [dim].[HydroTreaterType_LookUp] ([HydroTreaterTypeId]),
    CONSTRAINT [FK_HydroTreaterType_Bridge_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_HydroTreaterType_Bridge_Parent_Ancestor] FOREIGN KEY ([MethodologyId], [HydroTreaterTypeId]) REFERENCES [dim].[HydroTreaterType_Parent] ([MethodologyId], [HydroTreaterTypeId]),
    CONSTRAINT [FK_HydroTreaterType_Bridge_Parent_Descendant] FOREIGN KEY ([MethodologyId], [DescendantId]) REFERENCES [dim].[HydroTreaterType_Parent] ([MethodologyId], [HydroTreaterTypeId])
);


GO

CREATE TRIGGER [dim].[t_HydroTreaterType_Bridge_u]
	ON [dim].[HydroTreaterType_Bridge]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[HydroTreaterType_Bridge]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[HydroTreaterType_Bridge].[MethodologyId]			= INSERTED.[MethodologyId]
		AND	[dim].[HydroTreaterType_Bridge].[HydroTreaterTypeId]	= INSERTED.[HydroTreaterTypeId]
		AND	[dim].[HydroTreaterType_Bridge].[DescendantId]			= INSERTED.[DescendantId];

END;