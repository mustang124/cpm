﻿CREATE PROCEDURE [dim].[Merge_SysLanguages]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	MERGE INTO [dim].[SysLanguages_LookUp] AS Target
	USING
	(
		SELECT
			l.[langid],
			l.[dateformat],
			l.[datefirst],
			l.[upgrade],
			l.[name],
			l.[alias],
			l.[months],
			l.[shortmonths],
			l.[days],
			l.[lcid],
			l.[msglangid]
		FROM master.sys.syslanguages l
	)
	AS Source([langid], [dateformat], [datefirst], [upgrade], [name], [alias], [months], [shortmonths], [days], [lcid], [msglangid])
	ON	Target.[langid]			= Source.[langid]
	WHEN MATCHED THEN UPDATE SET
		Target.[dateformat]		= Source.[dateformat],
		Target.[datefirst]		= Source.[datefirst],
		Target.[upgrade]		= Source.[upgrade],
		Target.[name]			= Source.[name],
		Target.[alias]			= Source.[alias],
		Target.[months]			= Source.[months],
		Target.[shortmonths]	= Source.[shortmonths],
		Target.[days]			= Source.[days],
		Target.[lcid]			= Source.[lcid],
		Target.[msglangid]		= Source.[msglangid]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([langid], [dateformat], [datefirst], [upgrade], [name], [alias], [months], [shortmonths], [days], [lcid], [msglangid])
		VALUES([langid], [dateformat], [datefirst], [upgrade], [name], [alias], [months], [shortmonths], [days], [lcid], [msglangid])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;