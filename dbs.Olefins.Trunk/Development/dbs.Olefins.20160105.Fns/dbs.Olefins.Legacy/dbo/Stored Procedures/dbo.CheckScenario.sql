﻿
/****** Object:  Stored Procedure dbo.CheckScenario    Script Date: 4/18/2003 4:32:52 PM ******/
/****** Object:  Stored Procedure dbo.CheckScenario    Script Date: 12/28/2001 7:34:24 AM ******/
/****** Object:  Stored Procedure dbo.CheckScenario    Script Date: 7/15/99 12:09:10 PM ******/
/****** Object:  Stored Procedure dbo.CheckScenario    Script Date: 8/12/97 1:40:44 PM ******/
CREATE PROCEDURE [dbo].[CheckScenario] @TableName char (30), @RecCount tinyint OUTPUT
AS
SELECT @RecCount = (SELECT count(*) FROM sys.columns
	WHERE name = 'Scenario'
	AND object_id = (SELECT object_id FROM sys.objects WHERE name = @TableName AND SCHEMA_NAME(schema_id) = 'dbo'))

