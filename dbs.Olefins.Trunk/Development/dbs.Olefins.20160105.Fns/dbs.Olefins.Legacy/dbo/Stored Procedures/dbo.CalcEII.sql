﻿


-- =============================================
-- Author:		Rogge Heflin
-- Create date: 19 June 2009
-- Description:	EII Calculator
-- Version:		2009.08.20
-- =============================================
-- EXECUTE dbo.CalcEII '07PCH011', 2007
CREATE PROCEDURE [dbo].[CalcEII](
	@Refnum Refnum,
	@FactorSet varchar(8)
	)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@ratioEP		REAL		= 0.8;			-- Percent of Ethane/Propane Mix distributed to Ethane
	DECLARE	@pqFeedProdID	VarChar(8)	= 'Hydrogen';	--
	DECLARE @ModelVersion	varChar(10) = '2007';
	
	DECLARE @sgHGO			REAL		= 0.87;
	DECLARE @sgDie			REAL		= 0.83;
	DECLARE	@sgHvN			REAL		= 0.75;
	DECLARE	@sgFrN			REAL		= 0.71;

	DECLARE @SimModel		VarChar(4)	= 'PYPS';

	IF OBJECT_ID('tempdb..#RawData')			IS NOT NULL BEGIN DROP TABLE #RawData;			END;
	IF OBJECT_ID('tempdb..#EIICalculations')	IS NOT NULL BEGIN DROP TABLE #EIICalculations;	END;
	IF OBJECT_ID('tempdb..#EIIAggregate')		IS NOT NULL BEGIN DROP TABLE #EIIAggregate;	END;

	-------------------------------------------------------------------------------------------------------------------
	-- Retrieve Raw data values used as basis for EII calculation

	SELECT	FQnt.*
		, IsNull(FQual.SG1, 0) AS 'SG1', IsNull(FQual.SG2, 0) AS 'SG2', IsNull(FQual.SG3, 0) AS 'SG3'
	
		-- Aggregate the feed components to create Fresh Feed
		, IsNull(p.FreshFeedDiv, 
			IsNull(FQnt.Ethane, 0) + IsNull(FQnt.EPMix, 0) + IsNull(FQnt.Propane, 0) + IsNull(FQnt.Butane, 0) + IsNull(FQnt.LPG, 0) + IsNull(FQnt.OthLtFeed, 0)
			+ IsNull(FQnt.HeavyNGL, 0) + IsNull(FQnt.Condensate, 0) + IsNull(FQnt.FRNaphtha, 0) + IsNull(FQnt.LtNaphtha, 0) + IsNull(FQnt.Raffinate, 0) + IsNull(FQnt.HeavyNaphtha, 0)
			+ IsNull(FQnt.Diesel, 0) + IsNull(FQnt.HeavyGasoil, 0) + IsNull(FQnt.HTGasoil, 0)
			+ IsNull(FQnt.OthLiqFeed1, 0) + IsNull(FQnt.OthLiqFeed2, 0) + IsNull(FQnt.OthLiqFeed3, 0)) AS 'FreshFeed'
		
		-- Aggregate the Supplemental Feeds
		, IsNull(FQnt.ConcEthylene, 0) + IsNull(FQnt.DiEthylene, 0) + IsNull(FQnt.RefEthylene, 0) + IsNull(FQnt.ConcPropylene, 0) + IsNull(FQnt.DiPropylene, 0) + IsNull(FQnt.RefPropylene, 0) AS 'SupplEP'
		
		, IsNull(2204.6 * 379 / ( 8 - 7 * PQ.MolePcnt) / 2, 0)	AS 'StoichiometryH'

	INTO	#RawData
	
	FROM (
		
		SELECT Refnum, FeedProdID, AnnFeedProd
		FROM dbo.Quantity
		WHERE Refnum = @Refnum
		) AS ST
		PIVOT
			(
			SUM(AnnFeedProd)
			FOR FeedProdID IN
				(
				[Ethane],[EPMix],[Propane],[Butane],[LPG],[OthLtFeed],[HeavyNGL],[Condensate],[FRNaphtha],[LtNaphtha],[Raffinate],[HeavyNaphtha],[Diesel],[HeavyGasoil],[HTGasoil],[OthLiqFeed1],[OthLiqFeed2],[OthLiqFeed3]
				
				,[DiHydrogen],[DiMethane],[RefHydrogen],[RefMethane]					-- C1 & H2
				
				,[ConcEthylene],[DiEthane],[RefEthane],[DiEthylene],[RefEthylene]		-- C2
				,[ConcPropylene],[RefPropylene],[DiPropylene],[RefPropane],[DiPropane]	-- C3
				,[DiButane],[DiButylenes],[RefC4Plus],[ConcBenzene]						-- C4
				,[ConcButadiene],[DiBenzene],[DiButadiene]								-- C4
				,[GasOil],[WashOil],[OthSpl],[DiMoGas],[RefInerts]						-- C5+
				
				,[Hydrogen]
				,[Propylene],[PropyleneCG],[PropyleneRG]
				)
			)
			AS FQnt
			
		INNER JOIN dbo.ProdQuality	PQ	ON PQ.Refnum	= FQnt.Refnum
		INNER JOIN dbo.Production	P	ON P.Refnum		= FQnt.Refnum

		LEFT OUTER  JOIN
		
			(SELECT PT.Refnum
				, PT.OthLiqFeed1 'SG1'
				, PT.OthLiqFeed2 'SG2'
				, PT.OthLiqFeed3 'SG3'
			FROM
				(SELECT	Refnum, FeedProdID, SG
				FROM	dbo.FeedQuality
				WHERE	Refnum =  @Refnum
						AND SG IS NOT NULL AND FeedProdID LIKE 'OthLiqFeed%'
				) AS ST
				PIVOT
					(
					MAX(SG)
					FOR FeedProdID IN
						(
						[OthLiqFeed1],[OthLiqFeed2],[OthLiqFeed3]
						)
					) AS PT
				) AS FQual			ON FQual.Refnum	= FQnt.Refnum		

	WHERE
			PQ.Refnum		= @Refnum
		AND PQ.FeedProdID	= @pqFeedProdID

	-------------------------------------------------------------------------------------------------------------------
	-- Calculate individual EII values
	
	SELECT RD.Refnum 

		, (IsNull(FC.FracFeedKBSD, 0)	* eiiF.FracFeedKBSD)											AS 'FracFeedKBSD'
		, (IsNull(FC.HydroCryoCnt, 0)	* eiiF.HydroCryoCnt	* RD.Hydrogen * RD.StoichiometryH)	/ 1000	AS 'HydroCryoCnt'
		, (IsNull(FC.HydroPSACnt, 0)	* eiiF.HydroPSACnt	* RD.Hydrogen * RD.StoichiometryH)	/ 1000	AS 'HydroPSACnt'
		, (IsNull(FC.HydroMembCnt, 0)	* eiiF.HydroMembCnt	* RD.Hydrogen * RD.StoichiometryH)	/ 1000	AS 'HydroMembCnt'
		, (IsNull(FC.PGasHydroKBSD, 0)	* eiiF.PGasHydroKBSD)											AS 'PGasHydroKBSD'
		
		, eiiC.GasCompUHP	* Power(IsNull(FC.GasCompBHP  * RD.FreshFeed / CAP.FurnaceCapKMTA,	0)	, eiiE.GasCompUHP)
																										AS 'GasCompBHP'
		, eiiC.MethCompUHP	* Power(IsNull(FC.MethCompBHP * RD.FreshFeed / CAP.FurnaceCapKMTA, 0)	, eiiE.MethCompUHP)
																										AS 'MethCompBHP'
		, eiiC.EthyCompUHP	* Power(IsNull(FC.EthyCompBHP * RD.FreshFeed / CAP.FurnaceCapKMTA, 0)	, eiiE.EthyCompUHP)
																										AS 'EthyCompBHP'
		, eiiC.PropCompUHP	* Power(IsNull(FC.PropCompBHP * RD.FreshFeed / CAP.FurnaceCapKMTA, 0)	, eiiE.PropCompUHP)
																										AS 'PropCompBHP'

		, CASE WHEN  (RD.Propylene + RD.PropyleneCG + RD.PropyleneRG) <> 0 THEN 
		  eiiC.Purity		* Power(IsNull((RD.Propylene + RD.PropyleneCG + RD.PropyleneRG) *
							  (RD.Propylene * PQ.Propylene + RD.PropyleneCG * PQ.PropyleneCG + RD.PropyleneRG * PQ.PropyleneRG)
							/ (RD.Propylene + RD.PropyleneCG + RD.PropyleneRG), 0), eiiE.Purity)
																	ELSE 0 END	 AS 'Purity'																	

		, eiiC.Ethane		* IsNull(RD.Ethane + RD.EPMix * @ratioEP, 0)		 * Y.SIC2 / 100 AS 'Ethane'
		, eiiC.Propane		* IsNull(RD.Propane + RD.EPMix * (1 - @ratioEP), 0)	 * Y.SIC2 / 100 AS 'Propane'
		, eiiC.Butane		* IsNull(RD.Butane, 0)								 * Y.SIC2 / 100 AS 'Butane'
		, eiiC.LPG			* IsNull(RD.LPG, 0)									 * Y.SIC2 / 100 AS 'LPG'
		, eiiC.OthLtFeed	* IsNull(RD.OthLtFeed, 0)							 * Y.SIC2 / 100 AS 'OthLtFeed'

		, eiiC.LtNaphtha	* IsNull(RD.LtNaphtha, 0)							 * Y.SIC2 / 100 AS 'LtNaphtha'
		, eiiC.Raffinate	* IsNull(RD.Raffinate, 0)							 * Y.SIC2 / 100 AS 'Raffinate'
		, eiiC.HeavyNGL		* IsNull(RD.HeavyNGL, 0)							 * Y.SIC2 / 100 AS 'HeavyNGL'
		, eiiC.Condensate	* IsNull(RD.Condensate, 0)							 * Y.SIC2 / 100 AS 'Condensate'
		, eiiC.FRNaphtha	* IsNull(RD.FRNaphtha, 0)							 * Y.SIC2 / 100 AS 'FRNaphtha'
		, eiiC.HeavyNaphtha	* IsNull(RD.HeavyNaphtha, 0)						 * Y.SIC2 / 100 AS 'HeavyNaphtha'
		, eiiC.Diesel		* IsNull(RD.Diesel, 0)								 * Y.SIC2 / 100 AS 'Diesel'
		, eiiC.HeavyGasoil	* IsNull(RD.HeavyGasoil, 0)							 * Y.SIC2 / 100 AS 'HeavyGasoil'
		, eiiC.HTGasoil		* IsNull(RD.HTGasoil, 0)							 * Y.SIC2 / 100 AS 'HTGasoil'
/*
	DECLARE @sgHGO			REAL		= 0.87;
	DECLARE @sgDie			REAL		= 0.83;
	DECLARE	@sgHvN			REAL		= 0.75;
	DECLARE	@sgFrN			REAL		= 0.71;
*/
	
		, CASE
			WHEN RD.SG1 >= @sgHGO THEN eiiC.HeavyGasoil
			WHEN RD.SG1 >= @sgDie THEN eiiC.Diesel
			WHEN RD.SG1 >= @sgHvN THEN eiiC.HeavyNaphtha
			WHEN RD.SG1 >= @sgFrN THEN eiiC.FRNaphtha
			ELSE eiiC.LtNaphtha
		  END				* IsNull(RD.OthLiqFeed1, 0)							 * Y.SIC2 / 100 AS 'OthLiqFeed1'

		, CASE
			WHEN RD.SG2 >= @sgHGO THEN eiiC.HeavyGasoil
			WHEN RD.SG2 >= @sgDie THEN eiiC.Diesel
			WHEN RD.SG2 >= @sgHvN THEN eiiC.HeavyNaphtha
			WHEN RD.SG2 >= @sgFrN THEN eiiC.FRNaphtha
			ELSE eiiC.LtNaphtha
		  END				* IsNull(RD.OthLiqFeed2, 0)							 * Y.SIC2 / 100 AS 'OthLiqFeed2'

		, CASE
			WHEN RD.SG3 >= @sgHGO THEN eiiC.HeavyGasoil
			WHEN RD.SG3 >= @sgDie THEN eiiC.Diesel
			WHEN RD.SG3 >= @sgHvN THEN eiiC.HeavyNaphtha
			WHEN RD.SG3 >= @sgFrN THEN eiiC.FRNaphtha
			ELSE eiiC.LtNaphtha
		  END				* IsNull(RD.OthLiqFeed3, 0)							 * Y.SIC2 / 100 AS 'OthLiqFeed3'


		, eiiC.C1 * IsNull(RD.DiHydrogen, 0)						AS 'DiHydrogen'
		, eiiC.C1 * IsNull(RD.DiMethane, 0)							AS 'DiMethane'
		, eiiC.C1 * IsNull(RD.RefHydrogen, 0)						AS 'RefHydrogen'
		, eiiC.C1 * IsNull(RD.RefMethane, 0)						AS 'RefMethane'

		, eiiC.C2 * IsNull(RD.DiEthane, 0)							AS 'DiEthane'
		, eiiC.C2 * IsNull(RD.RefEthane, 0)							AS 'RefEthane'
		, eiiC.C2 * IsNull(RD.ConcEthylene, 0)						AS 'ConcEthylene'
		, eiiC.C2 * IsNull(RD.DiEthylene, 0)						AS 'DiEthylene'
		, eiiC.C2 * IsNull(RD.RefEthylene, 0)						AS 'RefEthylene'
		
		, eiiC.C3b * IsNull(RD.ConcPropylene, 0)					AS 'ConcPropylene'
		, eiiC.C3b * IsNull(RD.DiPropylene, 0)						AS 'DiPropylene'
		, eiiC.C3b * IsNull(RD.RefPropylene, 0)						AS 'RefPropylene'
		, eiiC.C3b * IsNull(RD.DiPropane, 0)						AS 'DiPropane'
		, eiiC.C3b * IsNull(RD.RefPropane, 0)						AS 'RefPropane'

		, eiiC.C4 * IsNull(RD.DiButane, 0)							AS 'DiButane'
		, eiiC.C4 * IsNull(RD.DiButylenes, 0)						AS 'DiButylenes'
		, eiiC.C4 * IsNull(RD.ConcBenzene, 0)						AS 'ConcBenzene'
		, eiiC.C4 * IsNull(RD.DiBenzene, 0)							AS 'DiBenzene'
		, eiiC.C4 * IsNull(RD.ConcButadiene, 0)						AS 'ConcButadiene'
		, eiiC.C4 * IsNull(RD.DiButadiene, 0)						AS 'DiButadiene'
		, eiiC.C4 *	IsNull(RD.RefC4Plus, 0)							AS 'RefC4Plus'

		, eiiC.C5p * IsNull(RD.GasOil, 0)							AS 'GasOil'
		, eiiC.C5p * IsNull(RD.WashOil, 0)							AS 'WashOil'
		, eiiC.C5p * IsNull(RD.OthSpl, 0)							AS 'OthSpl'
		, eiiC.C5p * IsNull(RD.DiMoGas, 0)							AS 'DiMoGas'
		, eiiC.C5p * IsNull(RD.RefInerts, 0)							AS 'RefInerts'

		, RD.SG1 AS 'SG1'
		, RD.SG2 AS 'SG2'
		, RD.SG3 AS 'SG3'
	
	INTO #EIICalculations

	FROM
		
		#RawData						RD
		INNER JOIN dbo.Capacity			CAP		ON CAP.Refnum		= RD.Refnum
		INNER JOIN dbo.Facilities		FC		ON FC.Refnum		= RD.Refnum
		
		INNER JOIN
			(
			SELECT Refnum
				, FeedProdID
				, WtPcnt
			FROM ProdQuality
			WHERE Refnum = @Refnum
			) AS SourceTable
			PIVOT
				(
				MAX(WtPcnt)
				FOR FeedProdID
				IN (Propylene, PropyleneCG, PropyleneRG))
			AS							PQ		ON PQ.Refnum		= RD.Refnum
	
		INNER JOIN dbo.Yield			Y		ON Y.Refnum			= RD.Refnum
				
				 , dbo.EIIFactors		eiiF
		INNER JOIN dbo.EIICoefficients	eiiC	ON eiiC.FactorSet	= eiiF.FactorSet
		INNER JOIN dbo.EIIExponents		eiiE	ON eiiE.FactorSet	= eiiF.FactorSet
		
	WHERE
			CAP.Refnum	= @Refnum
		AND FC.Refnum	= @Refnum
		AND PQ.Refnum	= @Refnum
		AND Y.Refnum	= @Refnum	AND Y.Model = @SimModel
		AND eiiF.FactorSet = @FactorSet
		AND eiiC.FactorSet = @FactorSet
		AND eiiE.FactorSet = @FactorSet;


	-------------------------------------------------------------------------------------------------------------------
	-- Aggregate EII and insert values into table
	SELECT eiiC.*
		 , eei.NetEnergyConsumed / 10000 /
			(FracFeedKBSD
			+ Ethane + Propane + Butane + LPG + OthLtFeed
				+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
				+ Diesel + HeavyGasoil + HTGasoil
				+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3
			+ GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP
			+ DiHydrogen + DiMethane + DiEthane + DiEthylene + DiPropylene + DiPropane + DiButane + DiButylenes + DiBenzene + DiButadiene + DiMoGas
				+ RefHydrogen + RefMethane + RefEthane + RefEthylene + RefPropylene + RefPropane + RefC4Plus + RefInerts
				+ ConcEthylene + ConcPropylene + ConcBenzene + ConcButadiene
				+ GasOil + WashOil + OthSpl				
			+ HydroCryoCnt + HydroPSACnt + HydroMembCnt
			+ PGasHydroKBSD
			+ Purity)	* 1000000												AS 'EII'

		, Ethane + Propane + Butane + LPG + OthLtFeed
			+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
			+ Diesel + HeavyGasoil + HTGasoil
			+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3							AS 'PyrolysisFeed'
			
		, GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP + Purity			AS 'Compression'

		, DiHydrogen + DiMethane + DiEthane + DiEthylene + DiPropylene + DiPropane + DiButane + DiButylenes + DiBenzene + DiButadiene + DiMoGas
			+ RefHydrogen + RefMethane + RefEthane + RefEthylene + RefPropylene + RefPropane + RefC4Plus + RefInerts
			+ ConcEthylene + ConcPropylene + ConcBenzene + ConcButadiene
			+ GasOil + WashOil + OthSpl											AS 'Supplemental'

		, HydroCryoCnt + HydroPSACnt + HydroMembCnt + FracFeedKBSD + PGasHydroKBSD
																				AS 'Auxiliary'
																						
		, FracFeedKBSD
			+ Ethane + Propane + Butane + LPG + OthLtFeed
				+ LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
				+ Diesel + HeavyGasoil + HTGasoil
				+ OthLiqFeed1 + OthLiqFeed2 + OthLiqFeed3
			+ GasCompBHP + MethCompBHP + EthyCompBHP + PropCompBHP
			+ DiHydrogen + DiMethane + DiEthane + DiEthylene + DiPropylene + DiPropane + DiButane + DiButylenes + DiBenzene + DiButadiene + DiMoGas
				+ RefHydrogen + RefMethane + RefEthane + RefEthylene + RefPropylene + RefPropane + RefC4Plus + RefInerts
				+ ConcEthylene + ConcPropylene + ConcBenzene + ConcButadiene
				+ GasOil + WashOil + OthSpl				
			+ HydroCryoCnt + HydroPSACnt + HydroMembCnt
			+ PGasHydroKBSD
			+ Purity															AS 'StdEnergy'
			
		, Ethane + Propane + Butane + LPG + OthLtFeed							AS 'LightFeed'

		, LtNaphtha + Raffinate + HeavyNGL + Condensate + FRNaphtha + HeavyNaphtha
			+ CASE WHEN SG1 < @sgDie THEN IsNull(OthLiqFeed1, 0) ELSE 0 END
			+ CASE WHEN SG2 < @sgDie THEN IsNull(OthLiqFeed2, 0) ELSE 0 END
			+ CASE WHEN SG3 < @sgDie THEN IsNull(OthLiqFeed3, 0) ELSE 0 END
																				AS 'NaphthaFeed'
		, Diesel + HeavyGasoil + HTGasoil
			+ CASE WHEN SG1 >= @sgDie THEN IsNull(OthLiqFeed1, 0) ELSE 0 END
			+ CASE WHEN SG2 >= @sgDie THEN IsNull(OthLiqFeed2, 0) ELSE 0 END
			+ CASE WHEN SG3 >= @sgDie THEN IsNull(OthLiqFeed3, 0) ELSE 0 END
																				AS 'HeavyFeed'

		, DiHydrogen + DiMethane + DiEthane + DiEthylene + DiPropylene + DiPropane + DiButane + DiButylenes + DiBenzene + DiButadiene + DiMoGas
																				AS 'DiSuppl'
		, RefHydrogen + RefMethane + RefEthane + RefEthylene + RefPropylene + RefPropane + RefC4Plus + RefInerts
																				AS 'RefSuppl'
		, ConcEthylene + ConcPropylene + ConcBenzene + ConcButadiene			AS 'ConcSuppl'
		, GasOil + WashOil + OthSpl												AS 'OtherSuppl'

	INTO	#EIIAggregate
	
	FROM	#EIICalculations eiiC
		INNER JOIN EEI ON EEI.Refnum = eiiC.Refnum
	WHERE EEI.Refnum = @Refnum AND EEI.Model = @SimModel

	DELETE FROM dbo.EIIVersionCalcs WHERE Refnum = @Refnum AND FactorSet = @FactorSet AND ModelVersion = @ModelVersion

	INSERT INTO dbo.EIIVersionCalcs
	SELECT
		eiiC.Refnum			AS 'Refnum'
		, @FactorSet		AS 'FactorSet'
		, @ModelVersion		AS 'ModelVersion'
		, EII				AS 'EII'
		, StdEnergy			AS 'StdEnergy'

		, PyrolysisFeed		AS 'PyrolysisFeed'
		, [Compression]		AS 'Compression'
		, Supplemental		AS 'Supplemental'
		, Auxiliary			AS 'Auxiliary'

		, LightFeed			AS 'LightFeed'
		, NaphthaFeed		AS 'NaphthatFeed'
		, HeavyFeed			AS 'HeavyFeed'
		, DiSuppl			AS 'DiSuppl'
		, RefSuppl			AS 'RefSuppl'
		, ConcSuppl			AS 'ConcSuppl'
		, OtherSuppl		AS 'OtherSuppl'

		, FracFeedKBSD		AS 'FracFeedKBSD'
		, HydroCryoCnt		AS 'HydroCryoCnt'
		, HydroPSACnt		AS 'HydroPSACnt'
		, HydroMembCnt		AS 'HydroMembCnt'
		, PGasHydroKBSD		AS 'PGasHydroKBSD'

		, GasCompBHP		AS 'GasCompBHP'
		, MethCompBHP		AS 'MethCompBHP'
		, EthyCompBHP		AS 'EthyCompBHP'
		, PropCompBHP		AS 'PropCompBHP'
		
		, Purity			AS 'Purity'

		, Ethane			AS 'Ethane'
		, Propane			AS 'Propane'
		, Butane			AS 'Butane'
		, LPG				AS 'LPG'
		, OthLtFeed			AS 'OthLtFeed'
		, LtNaphtha			AS 'LtNaphtha'
		, Raffinate			AS 'Raffinate'
		, HeavyNGL			AS 'HeavyNGL'
		, Condensate		AS 'Condensate'
		, FRNaphtha			AS 'FRNaphtha'
		, HeavyNaphtha		AS 'HeavyNaphtha'
		, Diesel			AS 'Diesel'
		, HeavyGasoil		AS 'HeavyGasoil'
		, HTGasoil			AS 'HTGasoil'
		, OthLiqFeed1		AS 'OthLiqFeed1'
		, OthLiqFeed2		AS 'OthLiqFeed2'
		, OthLiqFeed3		AS 'OthLiqFeed3'

		, DiHydrogen		AS 'DiHydrogen'
		, DiMethane			AS 'DiMethane'
		, DiEthane			AS 'DiEthane'
		, DiEthylene		AS 'DiEthylene'
		, DiPropylene		AS 'DiPropylene'
		, DiPropane			AS 'DiPropane'
		, DiButane			AS 'DiButane'
		, DiButylenes		AS 'DiButylenes'
		, DiBenzene			AS 'DiBenzene'
		, DiButadiene		AS 'DiButadiene'
		, DiMoGas			AS 'DiMoGas'
		
		, RefHydrogen		AS 'RefHydrogen'
		, RefMethane		AS 'RefMethane'
		, RefEthane			AS 'RefEthane'
		, RefEthylene		AS 'RefEthylene'
		, RefPropylene		AS 'RefPropylene'
		, RefPropane		AS 'RefPropane'
		, RefC4Plus			AS 'RefC4Plus'
		, RefInerts			AS 'RefInerts'
		
		, ConcEthylene		AS 'ConcEthylene'
		, ConcPropylene		AS 'ConcPropylene'
		, ConcBenzene		AS 'ConcBenzene'
		, ConcButadiene		AS 'ConcButadiene'

		, GasOil			AS 'GasOil'
		, WashOil			AS 'WashOil'
		, OthSpl			AS 'OthSpl'
		
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL

		, GETDATE()			AS 'tsCalculated'

	FROM #EIIAggregate eiiC

END



