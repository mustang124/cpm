﻿




-- =============================================
-- Author:		Rogge Heflin
-- Create date: 2010.01.25
-- Version:		20100125D01
-- Description:	Identify Discrepant Observations
-- =============================================
/*

Examples:
--	Year-over-year
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', 'StudyYear', '_vOutlierTest_Ethylene', 'AnnFeedProd', '%006', '07PCH006', 'L', 0.90;

--	Verification Independent Value(s)
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', '1', '_vOutlierTest_Ethylene', 'EthyleneEthPG', '05PCH+LATE', '07PCH+LATE', 'L', 0.90;
	
--	Verification Dependent Value(s)
--	Two sets of ListName for @RefListBasis and two Refnums for @RefListCheck (20100119D00 Depreciated for performance; need)
--	Requires "rl.ListName IN (''' + @RefListBasis + ''')" also include "rl.Refnum IN (''' + @RefListBasis + ''')"
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', 'EthylCapKMTA', '_vOutlierTest_Ethylene', 'AnnFeedProd', '05PCH+LATE'',''07PCH+LATE', '07PCH100'',''07PCH006', 'L', 0.90;
	
--	Typical calls
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', 'EthylCapKMTA', '_vOutlierTest_Ethylene', 'AnnFeedProd', '05PCH+LATE', '07PCH: FAO', 'M', 0.90;
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', 'EthylCapKMTA', '_vOutlierTest_Ethylene', 'AnnFeedProd', '05PCH+LATE', '07PCH: FAO', 'L', 0.90;
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', 'EthylCapKMTA', '_vOutlierTest_Ethylene', 'AnnFeedProd', '05PCH+LATE', '07PCH: FAO', 'E', 0.90;
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', 'EthylCapKMTA', '_vOutlierTest_Ethylene', 'AnnFeedProd', '05PCH+LATE', '07PCH: FAO', 'P', 0.90;
	EXECUTE [dbo].[spDiscrepantObservations] '_vOutlierTest_Ethylene', 'EthylCapKMTA', '_vOutlierTest_Ethylene', 'AnnFeedProd', '05PCH+LATE', '07PCH: FAO', 'S', 0.90;

DECLARE @sql NVARCHAR(MAX) = '(SELECT ta.Refnum, ta.TrainID, ta.TACost , ta.ActDT, ta.TAManHrs, ta.PctTotEthylCap FROM TADT ta WHERE PctTotEthylCap > 0)'
EXECUTE [dbo].[spDiscrepantObservations] @sql, 'TACost', @sql, 'ActDT', '07PCH+LATE', '07PCH+LATE', 'L', 0.90;
*/

CREATE PROCEDURE [dbo].[spDiscrepantObservations]
(
	  @xTable			NVARCHAR(MAX)			-- Independent Table
	, @xField			NVARCHAR(56)			-- Independent Variable	(Correlation against this column, Value on X-Axis)
	, @yTable			NVARCHAR(MAX)			-- Dependent Table
	, @yField			NVARCHAR(56)			-- Dependent Variable	(Variable in Question, Value on Y-Axis)
	, @RefListBasis		NVARCHAR(56)			-- Reflist
	, @RefListCheck		NVARCHAR(56)			-- Refnum
	---------------------------------------------------------------------------------
	, @fType			NVARCHAR(12)	= 'L'	-- Suspect Data Point Estimation Function
													-- 'M' Mean						ŷ = ∑y/n
													-- 'L' Linear					ŷ = a + b*x
													-- 'P' Power					ŷ = a * x^b
													-- 'E' Exponential				ŷ = a * e^(b*x)
													-- 'S' Saturation Growth Rate	ŷ = a * x / (b + x)
	, @Threshold		FLOAT			= 0.90	-- Pearson Threshold
)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Validate the text as uppercase.
	IF @fType = '' BEGIN SET @fType = 'L'; END; SET @fType = UPPER(ISNULL(@fType, 'L'));
	
	-- Set @Use1 to 1 (No Independent Variable, set X = 1) or 0 (Independent Exists)
	DECLARE @Use1 BIT = CASE WHEN @xTable = '' OR @xTable IS NULL OR @xField = '1' OR @xField IS NULL THEN 1 ELSE 0 END;
	IF (ISNUMERIC(@xField) = 1) BEGIN SET @xField = '1'; END;
	
	-- Standardized Residual Threshold
	DECLARE @ResidThres FLOAT = 2.0;
	
	-- Identify discreptant points on Normal Probability Chart (Z-Test statistic)
	DECLARE @NormThres FLOAT = 1.0;
	
	-- Number of tiles for 'Quartiling' distance away from expected value
	DECLARE @zTile	TINYINT = 4;
	DECLARE @sql	NVARCHAR(MAX);
	
	--------------------------------------------------------------------------------
	--	Insert data into Basis table. This table is used to calculate equations
	IF OBJECT_ID('tempdb..#Basis') IS NOT NULL BEGIN DROP TABLE #Basis; END;
	
	CREATE TABLE #Basis(
		  Refnum	NVARCHAR(56)	NOT NULL	-- Refnum
		, x			FLOAT			NOT NULL	-- Independent variable, X-axis
		, y			FLOAT			NOT NULL	-- Dependent variable, Y-axis
		, CONSTRAINT [PK_Basis] PRIMARY KEY CLUSTERED(
			  x ASC
			, y ASC
			, Refnum ASC
			)
		);

	IF @Use1 = 1 BEGIN
	
		SET @sql = N'SELECT DISTINCT yT.Refnum [Refnum], 1 [x], yT.[' + @yField + '] [y]
			FROM ' + @yTable + ' [yT]
			INNER JOIN (
				SELECT DISTINCT rl.Refnum FROM dbo._RL rl
				WHERE rl.ListName IN (''' + @RefListBasis + ''')
				   OR rl.Refnum IN (''' + @RefListBasis + ''')
				   
				   OR rl.Refnum LIKE ''' + @RefListBasis + '''
				   OR rl.Refnum LIKE ''' + @RefListCheck + '''
				   
				   ) rl ON rl.Refnum = yT.Refnum
			WHERE yT.[' + @yField + '] IS NOT NULL
			ORDER BY yT.[' + @yField + '] ASC';

	END;
	ELSE BEGIN
		
		SET @sql = N'SELECT DISTINCT xT.Refnum [Refnum], xT.[' + @xField + '] [x], yT.[' + @yField + '] [y]
			FROM ' + @xTable + ' [xT]
			INNER JOIN ' + @yTable + ' [yT] ON yT.Refnum = xT.Refnum
			INNER JOIN (
				SELECT DISTINCT rl.Refnum FROM dbo._RL rl
				WHERE rl.ListName IN (''' + @RefListBasis + ''')
				   OR rl.Refnum IN (''' + @RefListBasis + ''')
				   OR rl.Refnum LIKE ''' + @RefListBasis + '''

				   OR rl.Refnum LIKE ''' + @RefListBasis + '''
				   OR rl.Refnum LIKE ''' + @RefListCheck + '''

				   ) rl ON rl.Refnum = xT.Refnum AND rl.Refnum = yT.Refnum 
			WHERE xT.[' + @xField + '] IS NOT NULL AND yT.[' + @yField + '] IS NOT NULL
			ORDER BY xT.[' + @xField + '] ASC';
			
	END;
	
	SET @sql = N'INSERT INTO #Basis ([Refnum], [x], [y]) ' + @sql + ';';
	
	EXECUTE sp_executesql @sql;

	--------------------------------------------------------------------------------
	--	Insert data into Raw table. This table is used for censoring calculations
	IF OBJECT_ID('tempdb..#Raw') IS NOT NULL BEGIN DROP TABLE #Raw; END;
	
	CREATE TABLE #Raw(
		  Refnum	NVARCHAR(56)	NOT NULL	-- Refnum
		, x			FLOAT			NOT NULL	-- Independent variable, X-axis
		, y			FLOAT			NOT NULL	-- Dependent variable, Y-Axis
		
		, yFcn		FLOAT				NULL	-- Calculated dependent variable, Y-Axis
		, zFcn		FLOAT				NULL	-- Z-Test statistic value
		, zTile		TINYINT				NULL	-- N-Tile of zFcn (Z-Test) values
		, eRel		FLOAT				NULL	-- Relative error
		, eAbs		FLOAT				NULL	-- Absolute error
		, yCheck	FLOAT				NULL	-- y values requiring examination (@RefListCheck)
		
		, cInterval	FLOAT				NULL	-- Confidence (Estimate) interval, Excludes T-test coefficient
		, pInterval	FLOAT				NULL	-- Prediction interval, Excludes T-test coefficient
		, ciMin		FLOAT				NULL	-- Confidence interval, Min Value
		, ciMax		FLOAT				NULL	-- Confidence interval, Max Value
		, piMin		FLOAT				NULL	-- Prediction interval, Min Value
		, piMax		FLOAT				NULL	-- Prediction interval, Max Value

		, eResidual	FLOAT				NULL	-- Residual
		, sResidual	FLOAT				NULL	-- Standarized residual, expected between ±2.0
		, zNormal	FLOAT				NULL	-- Z-Test statistic for Normal Probability
		
		, CONSTRAINT [PK_Raw] PRIMARY KEY CLUSTERED(
			  x ASC
			, y ASC
			, Refnum ASC
			)
		);
	
	IF @Use1 = 1 BEGIN
	
		SET @sql = N'SELECT DISTINCT yT.Refnum [Refnum], 1 [x], yT.[' + @yField + '] [y]
			FROM ' + @yTable + ' [yT]
			INNER JOIN (
				SELECT DISTINCT rl.Refnum FROM dbo._RL rl
				WHERE rl.ListName IN (''' + @RefListBasis + ''')
				   OR rl.ListName IN (''' + @RefListCheck + ''')
				   OR rl.Refnum IN (''' + @RefListBasis + ''')
				   OR rl.Refnum IN (''' + @RefListCheck + ''')
				   
				   OR rl.Refnum LIKE ''' + @RefListBasis + '''
				   OR rl.Refnum LIKE ''' + @RefListCheck + '''
				   
				) rl ON rl.Refnum = yT.Refnum
			WHERE yT.[' + @yField + '] IS NOT NULL
			ORDER BY yT.[' + @yField + '] ASC';
	
	END;
	ELSE BEGIN

		SET @sql = N'SELECT DISTINCT xT.Refnum [Refnum], xT.[' + @xField + '] [x], yT.[' + @yField + '] [y]
			FROM ' + @xTable + ' [xT]
			INNER JOIN ' + @yTable + ' [yT] ON yT.Refnum = xT.Refnum
			INNER JOIN (
				SELECT DISTINCT rl.Refnum FROM dbo._RL rl
				WHERE rl.ListName IN (''' + @RefListBasis + ''')
				   OR rl.ListName IN (''' + @RefListCheck + ''')
				   OR rl.Refnum IN (''' + @RefListBasis + ''')
				   OR rl.Refnum IN (''' + @RefListCheck + ''')
				   
				   OR rl.Refnum LIKE ''' + @RefListBasis + '''
				   OR rl.Refnum LIKE ''' + @RefListCheck + '''
				   
				) rl ON rl.Refnum = xT.Refnum AND rl.Refnum = yT.Refnum
			WHERE xT.[' + @xField + '] IS NOT NULL AND yT.[' + @yField + '] IS NOT NULL
			ORDER BY xT.[' + @xField + '] ASC';
	
	END;
	
	SET @sql = N'INSERT INTO #Raw ([Refnum], [x], [y]) ' + @sql + ';';
	EXECUTE sp_executesql @sql;

	--------------------------------------------------------------------------------
	--	Determine [x] as Cumulative Percent [0, 100] When @xField = '1'
	IF @Use1 = 1 BEGIN
	
		DECLARE	@xSum	FLOAT = 0.0;
		
		DECLARE @p		FLOAT = 0.0;			-- Cumulative percent based on value, weight and total weight
		DECLARE @pM		FLOAT = 0.0;			-- Cumulative weighted midpoint (percent)
		
		------------------------------------------------------------------------------
		--	Update #Basis.[x]
		SET @p	= 0.0;
		SET @pM	= 0.0;
		
		SELECT	@xSum = COUNT(1)
		FROM	#Basis b;
		
		UPDATE	#Basis
		SET	  @p	  = @p + x / @xSum
			, @pM	  = @p - x / @xSum / 2.0
			, x		  = (@p - x / @xSum / 2.0) * 100.0
		FROM	#Basis b;
		
		------------------------------------------------------------------------------
		--	Update #Raw.[x] - Non-basis points
		SET @p	= 0.0;
		SET @pM	= 0.0;
		
		SELECT	@xSum = COUNT(1)
		FROM	#Raw r
			LEFT OUTER JOIN #Basis b ON b.Refnum = r.Refnum
		WHERE b.Refnum IS NULL;
		
		UPDATE	#Raw
		SET	  @p	= @p + r.x / @xSum
			, @pM	= @p - r.x / @xSum / 2.0
			, x		= (@p - r.x / @xSum / 2.0) * 100.0
		FROM	#Raw r
			LEFT OUTER JOIN #Basis b ON b.Refnum = r.Refnum
		WHERE b.Refnum IS NULL;
		
		------------------------------------------------------------------------------
		--	Update #Raw.[x] - Basis points
		SET @p	= 0.0;
		SET @pM	= 0.0;
		
		SELECT	@xSum = COUNT(1)
		FROM	#Raw r
			INNER JOIN #Basis b ON b.Refnum = r.Refnum;
			
		UPDATE	#Raw
		SET	  @p	= @p + r.x / @xSum
			, @pM	= @p - r.x / @xSum / 2.0
			, x		= (@p - r.x / @xSum / 2.0) * 100.0
		FROM	#Raw r
			INNER JOIN #Basis b ON b.Refnum = r.Refnum;
			
	END;
	
	--------------------------------------------------------------------------------
	--	Declare variables for coefficients
	DECLARE @b0			FLOAT;	-- First Coefficient or Term
	DECLARE @b1			FLOAT;	-- Second Coefficient or Term
	DECLARE	@StDev		FLOAT;	-- Standard Deviation
	
	DECLARE @SSE		FLOAT;	-- Sum of Squares due to Error (Error Sum of Squares)
	DECLARE @SSR		FLOAT;	-- Sum of Squares due to Regression (Regression Sum of Squares)
	DECLARE @SST		FLOAT;	-- Sum of Squares (Total)
	DECLARE @MSE		FLOAT;	-- Mean Square Error (Estimates Population Variance (σ²) or Sample Variance (s²)) 
	DECLARE @MSR		FLOAT;	-- Mean Square Regression
	
	DECLARE	@StDevB1	FLOAT;	-- T-Test Denominator
	
	DECLARE	@xMean		FLOAT;	-- Mean of linearlized x values
	DECLARE	@yMean		FLOAT;	-- Mean of Y for @b1e, Mean, Pearson
	
	DECLARE	@SumXiX		FLOAT;	-- Sum of xi - xbar (Σ(xi-AVG(x))
	DECLARE @VarB1		FLOAT;	-- @b1 Variance
	
	DECLARE @DFn		INT = 1;-- Determine the number of independet variables [0, 255]
	DECLARE @DFd		INT;	-- Degress of Freedom in the Denominator (Number of Records) 
	
	SELECT @DFd = COUNT(1) FROM #Basis;


	--------------------------------------------------------------------------------
	--	Mean: ŷ = avg(x)
	IF @fType = 'M' BEGIN
			
		SELECT	@yMean = AVG(b.y)
			  , @xMean = AVG(b.x)
			  , @StDev = STDEVP(b.y)
			  , @b0 = AVG(b.y)
			  , @b1 = 0.0
		FROM	#Basis b;
		
		SELECT	@SST = @StDev
			  , @SSR = @StDev
			  ,	@SSE = @StDev
			  , @MSE = @SSE / (COUNT(1) - 2.0)
			  , @SumXiX = SUM(POWER(b.x - @xMean, 2.0))
		FROM	#Basis b;
			
		------------------------------------------------------------------------------
		-- Update calculatable fields
		--	• Expected values
		--	• Z-Test based on regression
		--	• Relative Error Percentiles: ((y - ŷ) / y) ^ 2
		--	• Absolute Error Percentiles: (y - ŷ) ^ 2
		--	• Confidence Interval
		--	• Prediction Interval
		--	• Standardized Residuals

		UPDATE #Raw
		SET	yFcn = @yMean
		  , zFcn = c.zMean
		  , eRel = c.eMeanRel
		  , eAbs = c.eMeanAbs
		  , cInterval = @StDev * POWER(		 1.0/@DFd + POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , pInterval = @StDev * POWER(1.0 + 1.0/@DFd + POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , sResidual = @StDev * POWER(1.0 - 1.0/@DFd - POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		FROM (
			SELECT	r.Refnum
				  , (r.y - @yMean) / @StDev														[zMean]
				  , (SELECT COUNT(1) FROM #Raw t
					 WHERE	CASE WHEN t.y <> 0.0 THEN Power((t.y - @yMean) / t.y, 2.0) ELSE 1.0 END
						<=	CASE WHEN r.y <> 0.0 THEN Power((r.y - @yMean) / r.y, 2.0) ELSE 1.0 END
					)	 /	CAST(Count(1) OVER () AS FLOAT)										[eMeanRel]
				  , (SELECT COUNT(1) FROM #Raw t WHERE Power(t.y - @yMean, 2.0) <= Power(r.y - @yMean, 2.0)) / CAST(Count(1) OVER () AS FLOAT)
																								[eMeanAbs]
			FROM #Raw r
			) c
		WHERE c.Refnum = #Raw.Refnum;
		
	END;

	--------------------------------------------------------------------------------
	--	Linear: ŷ = a + bx
	--	• Intercept (a): b0
	--	• Slope (b): b1
	IF @fType = 'L' BEGIN
		
		SELECT	@b1 = CASE WHEN (COUNT(1) * SUM(POWER(b.x, 2.0)) - POWER(SUM(b.x), 2.0)) <> 0
					THEN
						(COUNT(1) * SUM(b.x * b.y) - (SUM(b.x) * SUM(b.y))) / (COUNT(1) * SUM(POWER(b.x, 2.0)) - POWER(SUM(b.x), 2.0))
					ELSE
						0.0
					END
			  ,	@b0 = AVG(b.y) - @b1 * AVG(b.x)
			  , @xMean = AVG(b.x)
			  , @yMean = AVG(b.y)
		FROM	#Basis b;
		
		SELECT 	@SSR = SUM(POWER(	   (@b0 + @b1 * b.x) - @yMean, 2.0))
			  , @SSE = SUM(POWER(b.y - (@b0 + @b1 * b.x)		 , 2.0))
			  , @MSE = @SSE / (COUNT(1) - 2.0)
			  , @SumXiX = SUM(POWER(b.x - @xMean, 2.0))
		FROM #Basis b;
			
		SET @StDev = POWER(@MSE, 0.5);
		SET @SST = @SSR + @SSE;
		
		------------------------------------------------------------------------------
		-- Update calculatable fields
		--	• Expected values
		--	• Z-Test based on regression
		--	• Relative Error Percentiles: ((y - ŷ) / y) ^ 2
		--	• Absolute Error Percentiles: (y - ŷ) ^ 2
		--	• Confidence Interval
		--	• Prediction Interval
		--	• Standardized Residuals

		UPDATE #Raw
		SET	yFcn = @b0 + @b1 * #Raw.x
		  , zFcn = c.zLine
		  , eRel = c.eLineRel
		  , eAbs = c.eLineAbs
		  , cInterval = @StDev * POWER(		 1.0/@DFd + POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , pInterval = @StDev * POWER(1.0 + 1.0/@DFd + POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , sResidual = @StDev * POWER(1.0 - 1.0/@DFd - POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		FROM (
			SELECT	r.Refnum
				  , CASE WHEN @b1 <> 0.0 THEN (r.y - (@b0 + @b1 * r.x)) / @StDev ELSE NULL END		[zLine]
				  ,	(SELECT COUNT(1) FROM #Raw t
					 WHERE	CASE WHEN t.y <> 0.0 THEN Power((t.y - (@b0 + @b1 * t.x)) / t.y, 2.0) ELSE 1.0 END
						<=	CASE WHEN r.y <> 0.0 THEN Power((r.y - (@b0 + @b1 * r.x)) / r.y, 2.0) ELSE 1.0 END
					)	 /	CAST(COUNT(1) OVER () AS FLOAT)											[eLineRel]
				  , (SELECT COUNT(1) FROM #Raw t WHERE Power(t.y - (@b0 + @b1 * t.x), 2.0) <= Power(r.y - (@b0 + @b1 * r.x), 2.0)) / CAST(COUNT(1) OVER () AS FLOAT)
																									[eLineAbs]
			FROM #Raw r
			) c
		WHERE c.Refnum = #Raw.Refnum;

	END;

	--------------------------------------------------------------------------------
	--	Exponential: ŷ = a * EXP(b*x)
	--	• Coefficient (a): b0
	--	• Coefficient (b): b1
	If @fType = 'E' BEGIN

		SELECT	@b1 = CASE WHEN (COUNT(1) * SUM(POWER(b.x, 2.0)) - POWER(SUM(b.x), 2.0)) <> 0
					THEN
						(COUNT(1) * SUM(b.x * LOG(b.y)) - (SUM(b.x) * SUM(LOG(b.y)))) / (COUNT(1) * SUM(POWER(b.x, 2.0)) - POWER(SUM(b.x), 2.0))
					ELSE
						0.0
					END
			  ,	@b0 = EXP(AVG(LOG(b.y)) - @b1 * AVG(b.x))
			  , @xMean = AVG(b.x)
			  , @yMean = AVG(b.y)
		FROM #Basis b WHERE b.y <> 0.0;
		
		SELECT	@SSR = SUM(POWER(	   EXP(LOG(@b0) + @b1 * b.x) - @yMean, 2.0))
			  , @SSE = SUM(POWER(b.y - EXP(LOG(@b0) + @b1 * b.x)		 , 2.0))
			  , @MSE = @SSE / (COUNT(1) - 2.0)
			  , @SumXiX = SUM(POWER(b.x - @xMean, 2.0))
		FROM #Basis b WHERE b.y <> 0.0;
						
		SET @StDev = POWER(@MSE, 0.5);	
		SET @SST = @SSR + @SSE;
		
		------------------------------------------------------------------------------
		-- Update calculatable fields
		--	• Expected values
		--	• Z-Test based on regression
		--	• Relative Error Percentiles: ((y - ŷ) / y) ^ 2
		--	• Absolute Error Percentiles: (y - ŷ) ^ 2
		--	• Confidence Interval
		--	• Prediction Interval
		--	• Standardized Residuals
		
		UPDATE #Raw
		SET	yFcn = EXP(LOG(@b0) + @b1 * #Raw.x)
		  , zFcn = c.zExpo
		  , eRel = c.eExpoRel
		  , eAbs = c.eExpoAbs
		  , cInterval = @StDev * POWER(		 1.0/@DFd + POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , pInterval = @StDev * POWER(1.0 + 1.0/@DFd + POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , sResidual = @StDev * POWER(1.0 - 1.0/@DFd - POWER(#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		FROM (
			SELECT	r.Refnum
				  , CASE WHEN r.y <> 0.0 THEN (r.y - EXP(LOG(@b0) + @b1 * r.x)) / @StDev ELSE NULL END
																								[zExpo]
				  , (SELECT COUNT(1) FROM #Raw t	
					 WHERE	CASE WHEN t.y <> 0.0 THEN Power((LOG(t.y) - (LOG(@b0) + @b1 * t.x)) / LOG(t.y), 2.0) ELSE 1.0 END
						<=	CASE WHEN r.y <> 0.0 THEN Power((LOG(r.y) - (LOG(@b0) + @b1 * r.x)) / LOG(r.y), 2.0) ELSE 1.0 END
					)	 /	CAST(Count(1) OVER () AS FLOAT)										[eExpoRel]
				  , (SELECT COUNT(1) FROM #Raw t
					 WHERE	CASE WHEN t.y <> 0.0 THEN Power(LOG(t.y) - (LOG(@b0) + @b1 * t.x), 2.0) ELSE 1.0 END
						<=	CASE WHEN r.y <> 0.0 THEN Power(LOG(r.y) - (LOG(@b0) + @b1 * r.x), 2.0) ELSE 1.0 END
					)	/	CAST(Count(1) OVER () AS FLOAT)										[eExpoAbs]
			FROM #Raw r
			) c
		WHERE c.Refnum = #Raw.Refnum;
		
	END;

	--------------------------------------------------------------------------------
	--	Power: ŷ = a * x ^ b
	--	• Coefficient (a): b0
	--	• Exponent (b): b1
	IF @fType = 'P' BEGIN

		SELECT	@b1 = CASE WHEN (COUNT(1) * SUM(POWER(LOG10(b.x), 2)) - POWER(SUM(LOG10(b.x)), 2)) <> 0
					THEN
						(COUNT(1) * SUM(LOG10(b.x) * LOG10(b.y)) - (SUM(LOG10(b.x)) * SUM(LOG10(b.y)))) / (COUNT(1) * SUM(POWER(LOG10(b.x), 2)) - POWER(SUM(LOG10(b.x)), 2))
					ELSE
						0.0
					END
			  , @b0 = POWER(10.0, (AVG(LOG10(b.y)) - @b1 * AVG(LOG10(b.x))))
			  , @xMean = AVG(LOG10(b.x))
			  , @yMean = AVG(b.y)
		FROM #Basis b WHERE b.x <> 0.0 AND b.y <> 0.0;
		
		SELECT  @SSR = SUM(POWER(	   POWER(10.0, LOG10(@b0) + @b1 * LOG10(b.x))	- @yMean, 2.0))
			  , @SSE = SUM(POWER(b.y - POWER(10.0, LOG10(@b0) + @b1 * LOG10(b.x))			, 2.0))
			  , @MSE = @SSE / (COUNT(1) - 2.0)
			  , @SumXiX = SUM(POWER(LOG10(b.x) - @xMean, 2.0))
		FROM #Basis b WHERE b.x <> 0.0 AND b.y <> 0.0;
			
		SET @StDev = POWER(@MSE, 0.5);	
		SET @SST = @SSR + @SSE;	

		------------------------------------------------------------------------------
		-- Update calculatable fields
		--	• Expected values
		--	• Z-Test based on regression
		--	• Relative Error Percentiles: ((y - ŷ) / y) ^ 2
		--	• Absolute Error Percentiles: (y - ŷ) ^ 2
		--	• Confidence Interval
		--	• Prediction Interval
		--	• Standardized Residuals

		UPDATE #Raw
		SET	yFcn = POWER(10.0, LOG10(@b0) + @b1 * LOG10(#Raw.x))
		  , zFcn = c.zPowr
		  , eRel = c.ePowrRel
		  , eAbs = c.ePowrAbs
		  , cInterval = @StDev * POWER(		 1.0/@DFd + POWER(LOG10(#Raw.x) - @xMean, 2.0) / @SumXiX, 0.5)
		  , pInterval = @StDev * POWER(1.0 + 1.0/@DFd + POWER(LOG10(#Raw.x) - @xMean, 2.0) / @SumXiX, 0.5)
		  , sResidual = @StDev * POWER(1.0 - 1.0/@DFd - POWER(LOG10(#Raw.x) - @xMean, 2.0) / @SumXiX, 0.5)
		FROM (
			SELECT	r.Refnum
				  , CASE WHEN r.x <> 0.0 AND r.y <> 0.0 THEN (r.y - POWER(10.0, LOG10(@b0) + @b1 * LOG10(r.x))) / @StDev ELSE NULL END
																								[zPowr]
				  , (SELECT	COUNT(1) FROM #Raw t
					 WHERE	CASE WHEN t.x <> 0.0 AND t.y <> 0.0 THEN POWER((LOG10(t.y) - (LOG10(@b0) + @b1 * LOG10(t.x))) / LOG10(t.y), 2.0) ELSE 1.0 END
						<=	CASE WHEN r.x <> 0.0 AND r.y <> 0.0 THEN POWER((LOG10(r.y) - (LOG10(@b0) + @b1 * LOG10(r.x))) / LOG10(r.y), 2.0) ELSE 1.0 END
					)	 /	CAST(Count(1) OVER () AS FLOAT)										[ePowrRel]
				  , (SELECT COUNT(1) FROM #Raw t
					 WHERE	CASE WHEN t.x <> 0.0 AND t.y <> 0.0 THEN POWER(LOG10(t.y) - (LOG10(@b0) + @b1 * LOG10(t.x)), 2.0) ELSE 1.0 END
						<=	CASE WHEN r.x <> 0.0 AND r.y <> 0.0 THEN POWER(LOG10(r.y) - (LOG10(@b0) + @b1 * LOG10(r.x)), 2.0) ELSE 1.0 END
					) / CAST(Count(1) OVER () AS FLOAT)											[ePowrAbs]																						
			FROM #Raw r
			) c
		WHERE c.Refnum = #Raw.Refnum AND #Raw.x <> 0.0;
		
	END;

	--------------------------------------------------------------------------------
	-- Saturation Growth Rate: ŷ = a * x / (b + x)
	--	• Coefficient (a): b0
	--	• Denominator (b): b1
	IF @fType = 'S' BEGIN
	
		SELECT	@b1 = CASE WHEN (COUNT(1) * SUM(POWER((1.0/b.x), 2.0)) - POWER(SUM((1.0/b.x)), 2.0)) <> 0
					THEN
						(COUNT(1) * SUM((1.0/b.x) * (1.0/b.y)) - (SUM((1.0/b.x)) * SUM((1.0/b.y)))) / (COUNT(1) * SUM(POWER((1.0/b.x), 2.0)) - POWER(SUM((1.0/b.x)), 2.0))
					ELSE
						0.0
					END
			  , @b0 = 1.0/((AVG(1.0/b.y) - @b1 * AVG(1.0/b.x)))
			  , @xMean = AVG(1.0/b.x)
			  , @yMean = AVG(b.y)
		FROM #Basis b WHERE b.x <> 0.0 AND b.y <> 0.0;
		
		SELECT	@SSR = SUM(POWER(	   (1.0/(1.0/@b0 + @b1 * (1.0/b.x))) - @yMean, 2.0))
			  , @SSE = SUM(POWER(b.y - (1.0/(1.0/@b0 + @b1 * (1.0/b.x)))		 , 2.0))
			  , @MSE = @SSE / (COUNT(1) - 2.0)
			  , @SumXiX = SUM(POWER(1.0 / b.x - @xMean, 2.0))
		FROM #Basis b WHERE b.x <> 0.0 AND b.y <> 0.0;
		
		SET @StDev = POWER(@MSE, 0.5);	
		SET @SST = @SSR + @SSE;	
		
		------------------------------------------------------------------------------
		-- Update calculatable fields
		--	• Expected values
		--	• Z-Test based on regression
		--	• Relative Error Percentiles: ((y - ŷ) / y) ^ 2
		--	• Absolute Error Percentiles: (y - ŷ) ^ 2
		--	• Confidence Interval
		--	• Prediction Interval
		--	• Standardized Residuals

		UPDATE #Raw
		SET	yFcn = 1.0/(1.0/@b0 + @b1 * (1.0/#Raw.x))
		  , zFcn = c.zGrth
		  , eRel = c.eGrthRel
		  , eAbs = c.eGrthAbs
		  , cInterval = @StDev * POWER(		 1.0/@DFd + POWER(1.0/#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , pInterval = @StDev * POWER(1.0 + 1.0/@DFd + POWER(1.0/#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		  , sResidual = @StDev * POWER(1.0 - 1.0/@DFd - POWER(1.0/#Raw.x - @xMean, 2.0) / @SumXiX, 0.5)
		FROM (
			SELECT	r.Refnum
				  , CASE WHEN r.x <> 0.0 AND r.y <> 0 THEN (r.y - 1.0/(1.0/@b0 + @b1 * (1.0/r.x))) / @StDev ELSE NULL END
																									[zGrth]
				  ,	(SELECT COUNT(1) FROM #Raw t
					 WHERE	CASE WHEN t.x <> 0.0 AND t.y <> 0.0 THEN POWER((t.y - 1.0/(1.0/@b0 + @b1 * (1.0/t.x))) / t.y, 2.0) ELSE 1.0 END
						<=	CASE WHEN r.x <> 0.0 AND r.y <> 0.0 THEN POWER((r.y - 1.0/(1.0/@b0 + @b1 * (1.0/r.x))) / r.y, 2.0) ELSE 1.0 END
					)	 /	CAST(COUNT(1) OVER () AS FLOAT)											[eGrthRel]
				  , (SELECT COUNT(1) FROM #Raw t
					 WHERE	CASE WHEN t.x <> 0.0 AND t.y <> 0.0 THEN POWER(t.y - 1.0/(1.0/@b0 + @b1 * (1.0/t.x)), 2.0) ELSE 1.0 END
						<=  CASE WHEN r.x <> 0.0 AND r.y <> 0.0 THEN POWER(r.y - 1.0/(1.0/@b0 + @b1 * (1.0/r.x)), 2.0) ELSE 1.0 END
					) / CAST(COUNT(1) OVER () AS FLOAT)
																									[eGrthAbs]
			FROM #Raw r WHERE r.x <> 0.0 AND r.y <> 0.0
			) c
		WHERE c.Refnum = #Raw.Refnum;

	END;

	--------------------------------------------------------------------------------
	--	Update for all methods

	DECLARE @COD	FLOAT;	-- Coefficient of Determination (r²)
	DECLARE	@COR	FLOAT;	-- Correlation Coefficient (r)
	DECLARE @tStat	FLOAT;
	DECLARE @fStat	FLOAT;
	DECLARE @tConf	FLOAT;
	DECLARE @fConf	FLOAT;
	
	SELECT	@StDevB1 = @StDev / STDEVP(b.x)
		  , @MSR = @SSR / CAST(@DFd AS FLOAT)
	FROM	#Basis b;
	
	SELECT	@VarB1 = @StDev / @SumXiX
		  , @COD = CASE WHEN @SST <> 0.0 THEN @SSR / @SST END
		  , @COR = CASE WHEN @b1 <> 0.0 AND @SST <> 0.0 THEN ABS(@b1) / @b1 * POWER(@SSR / @SST, 0.5) END
		  , @tStat = CASE WHEN @StDevB1 <> 0.0 AND @b1 > 0.0 THEN ABS(@b1 / @StDevB1) ELSE 1 END
		  , @fStat = CASE WHEN @MSE <> 0.0 THEN @MSR / @MSE END
		  --, @tConf = dbo.StudentCI(@DFd - 2.0, @tStat)
		  --, @fConf = dbo.FischerCI(@DFn, @DFd - 2.0, @fStat);
	
	-- Update all records in #Raw
	UPDATE	#Raw
	SET		zTile = t.zTile
		, ciMin = yFcn - @tStat * cInterval
		, ciMax = yFcn + @tStat * cInterval
		, piMin = yFcn - @tStat * pInterval
		, piMax = yFcn + @tStat * pInterval
	FROM (
		SELECT	r.Refnum, NTILE(@zTile) OVER(ORDER BY ABS(r.zFcn) ASC) [zTile]
		FROM #Raw r
		) t
	WHERE t.Refnum = #Raw.Refnum;
	
	-- Update Update #Basis records in #Raw
	UPDATE	#Raw
	SET		eResidual = r.y - r.yFcn
			, sResidual = (r.y - r.yFcn) / sResidual
	FROM	#Raw r
		INNER JOIN #Basis b ON b.Refnum = r.Refnum;
	
	-- Update #Raw records that are in @RefListCheck
	UPDATE	#Raw
	SET		yCheck = r.y
	FROM	#Raw r
		INNER JOIN (
			SELECT DISTINCT rl.Refnum
			FROM	dbo._RL rl
			WHERE	rl.ListName = @RefListCheck OR rl.Refnum = @RefListCheck
		) rl ON rl.Refnum COLLATE SQL_Latin1_General_CP1_CI_AI = r.Refnum COLLATE SQL_Latin1_General_CP1_CI_AI;

	-- 	Update #Raw records that do not exist in #Basis
	UPDATE	#Raw
	SET		sResidual = NULL
	FROM	#Raw r
		LEFT OUTER JOIN #Basis b ON b.Refnum = r.Refnum
	WHERE	b.Refnum IS NULL;

	--------------------------------------------------------------------------------
	-- Determine Pearson Product Moment Correlation Coefficeints for Confidence
	-- intervals [0.7, 1.0]

	DECLARE @pTile	FLOAT = 0.70;
	DECLARE @pMax	FLOAT = 1.00;
	DECLARE @pStep	FLOAT = 0.01;

	IF OBJECT_ID('tempdb..#Pearson') IS NOT NULL BEGIN DROP TABLE #Pearson; END;
	
	CREATE TABLE #Pearson(
		  pTile		FLOAT	NOT NULL PRIMARY KEY CLUSTERED	--
		, Pearson	FLOAT	NOT NULL						--
		, items		INT		NOT NULL						--
		);

	WHILE @pTile <= @pMax + @pStep BEGIN
	
		SELECT	@xMean = AVG(b.x) 
			  , @yMean = AVG(b.y) 
		FROM	#Basis b INNER JOIN #Raw r ON r.Refnum = b.Refnum
		WHERE	r.eAbs < @pTile AND r.eRel < @pTile;
		
		INSERT INTO #Pearson (pTile, Pearson, items)
		SELECT  @pTile
			  , CASE WHEN STDEVP(b.x) <> 0 AND STDEVP(b.y) <> 0
				THEN ISNULL(SUM((b.x - @xMean) * (b.y - @yMean)) / COUNT(1) / STDEVP(b.x) / STDEVP(b.y), -1)
				ELSE 0
				END
			  , COUNT(1)
		FROM	#Basis b INNER JOIN #Raw r ON r.Refnum = b.Refnum
		WHERE	r.eAbs < @pTile AND r.eRel < @pTile;
		
		IF (@pTile >= @pMax) BREAK ELSE SET @pTile = @pTile + @pStep;
		
	END;


	--------------------------------------------------------------------------------
	-- Regression Analysis for Normal Probability chart, calculate Z-Test statistic
	-- to identify discrepant observations in the normal probability chart.
	
	DECLARE @rMSE		FLOAT;	-- Mean of x values
	DECLARE @rStDev		FLOAT;	-- Mean of x values

	SELECT	@rMSE = SUM(POWER(r.sResidual - r.zFcn, 2.0)) / (COUNT(1) - 2.0)
	FROM #Raw r
	WHERE r.sResidual - r.zFcn < 0.35;

	SET @rStDev = POWER(@rMSE, 0.5);	

	UPDATE #Raw
	SET	zNormal = (sResidual - zFcn) / @rStDev;
	
	--------------------------------------------------------------------------------
	-- Output

	SELECT 
		  @xTable					[iTable]
		, @xField					[iField]
		, @yTable					[dTable]
		, @yField					[dField]
		, @fType					[Type]
		, @RefListBasis				[Basis]
		, @b0						[b0]
		, @b1						[b1]
		
		, @StDev					[StDev]		-- Estimated Standard Deviation (σ, s) POWER(@MSE, 0.5)
		, @StDevB1					[StDevB1]	-- Estimated Standard Deviation of b1 (Slope)
		
		, @COD						[COD]		-- Coefficient of Determination (r²)
		, @COR						[COR]		-- Correlation Coefficient (r)
		
		, @tStat					[tStat]		--									
		, @fStat					[fStat]		--
		
		, @tConf					[tConf]
		, @fConf					[fConf]
		
		, @SSE						[SSE]		-- Sum of Squares due to Error
		, @SSR						[SSR]		-- Sum of Squares due to Regression
		, @SST						[SST]		-- Sum of Squares (Total: SST = SSE + SSR)
		, @MSE						[MSE]		-- Mean Square Error (Estimates Population Variance (σ²) or Sample Variance (s²))
		, @MSR						[MSR]		-- Mean Square Regression
		
		, @DFn						[DFn]		-- Degress of Freedom in the Numerator (Number of Independent Variables)
		, @DFd						[DFd]		-- Degress of Freedom in the Denominator
		, CASE WHEN @SSR <> 0.0 THEN @SST / @SSR END
									[StdAwy]	-- Calculate the number of Standard Deviations to set the Min/Max validation range 
		, @xMean					[xMean]
		, @SumXiX					[SumXiX]
		;
		
	SELECT p.* FROM #Pearson p;


	SELECT pTable.*
	FROM (
		SELECT	r.Refnum
			, r.x
			
			, CASE WHEN r.Refnum IN (SELECT DISTINCT b.Refnum FROM #Basis b)
			  THEN AVG(r.y) OVER() END	[yMean]
			  
			, CASE WHEN r.Refnum IN (SELECT DISTINCT b.Refnum FROM #Basis b)
			  THEN r.y END				[basisAll]			-- [All Basis Data Points]
			
			, CASE WHEN r.Refnum IN (SELECT DISTINCT b.Refnum FROM #Basis b) AND r.eAbs <=  @Threshold AND r.eRel <=  @Threshold
			  THEN r.y END				[basisPlausible]	-- [Plausible Basis Data Points]
			
			, r.yFcn
			
			, r.zFcn
			, r.sResidual
			--, zNormal
			, CASE WHEN ABS(r.sResidual) <= @ResidThres THEN r.sResidual END	[ResidIn]	-- Residual points <= @ResidThres
			, CASE WHEN ABS(r.sResidual) >  @ResidThres THEN r.sResidual END	[ResidOut]	-- Residual points >  @ResidThres
			
			, CASE WHEN ABS(r.zNormal) <= @NormThres THEN r.sResidual END		[NormalIn]
			, CASE WHEN ABS(r.zNormal) >  @NormThres THEN r.sResidual END		[NormalOut]
			
			, r.ciMin
			, r.ciMax
			, r.piMin
			, r.piMax
			
			, CASE WHEN r.eAbs > @Threshold OR  r.eRel > @Threshold THEN r.yCheck END
										[Suspect]			-- [Suspect Check Data Point]
			, r.yCheck					[yQuart]
			, r.zTile
			
		FROM	#Raw r
		) AS sTable
		PIVOT (MAX(sTable.yQuart) FOR sTable.zTile IN (	
			[1],[2],[3],[4]
			)	
		) AS pTable
	ORDER BY [x];
	
	SELECT	r.Refnum
	FROM	#Raw r
	WHERE	r.eAbs > @Threshold OR r.eRel > @Threshold;

--	SELECT b.* FROM #Basis b ORDER BY b.y;
--	SELECT r.* FROM #Raw r ORDER BY r.zNormal;
--	SELECT p.* FROM #Pearson p;
	
	IF OBJECT_ID('tempdb..#Pearson')IS NOT NULL BEGIN DROP TABLE #Pearson;	END;
	IF OBJECT_ID('tempdb..#Raw')	IS NOT NULL BEGIN DROP TABLE #Raw;		END;
	IF OBJECT_ID('tempdb..#Basis')	IS NOT NULL BEGIN DROP TABLE #Basis;	END;

END




