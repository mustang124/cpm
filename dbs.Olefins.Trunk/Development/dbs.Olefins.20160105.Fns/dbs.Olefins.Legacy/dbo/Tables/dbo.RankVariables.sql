﻿CREATE TABLE [dbo].[RankVariables] (
    [RankVariableID] SMALLINT     IDENTITY (1, 1) NOT NULL,
    [Description]    VARCHAR (40) NOT NULL,
    [VariableID]     INT          NOT NULL,
    [Scenario]       CHAR (8)     CONSTRAINT [DF__RankVaria__Scena__297722B6] DEFAULT ('BASE') NOT NULL,
    [SortOrder]      CHAR (4)     CONSTRAINT [DF__RankVaria__SortO__2A6B46EF] DEFAULT ('ASC') NOT NULL,
    [Active]         CHAR (1)     CONSTRAINT [DF__RankVaria__Activ__2B5F6B28] DEFAULT ('Y') NOT NULL,
    [Criteria]       VARCHAR (50) NULL,
    CONSTRAINT [PK__RankVaria__RankV__2EDAF651] PRIMARY KEY CLUSTERED ([RankVariableID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [RankVariables_Desc]
    ON [dbo].[RankVariables]([Description] ASC);

