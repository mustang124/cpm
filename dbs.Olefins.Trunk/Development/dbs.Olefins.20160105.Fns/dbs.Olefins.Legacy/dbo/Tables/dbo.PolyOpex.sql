﻿CREATE TABLE [dbo].[PolyOpex] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [PlantID]     TINYINT        NOT NULL,
    [NonTAMaint]  REAL           NULL,
    [TA]          REAL           NULL,
    [OthNonVol]   REAL           NULL,
    [STNonVol]    REAL           NULL,
    [Catalysts]   REAL           NULL,
    [Chemicals]   REAL           NULL,
    [Additives]   REAL           NULL,
    [Royalties]   REAL           NULL,
    [Energy]      REAL           NULL,
    [Packaging]   REAL           NULL,
    [OthVol]      REAL           NULL,
    [STVol]       REAL           NULL,
    [TotCashOpEx] REAL           NULL,
    CONSTRAINT [PK_PolyOpex_1__10] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PlantID] ASC)
);

