﻿CREATE TABLE [dbo].[RankForce] (
    [RefListNo]  INT          NOT NULL,
    [BreakID]    INT          NOT NULL,
    [BreakValue] VARCHAR (12) NOT NULL,
    [NumTiles]   TINYINT      NOT NULL,
    CONSTRAINT [PK_RankForce_1] PRIMARY KEY CLUSTERED ([RefListNo] ASC, [BreakID] ASC, [BreakValue] ASC)
);

