﻿CREATE TABLE [dbo].[PracOnlineFact] (
    [Refnum]  [dbo].[Refnum] NOT NULL,
    [Tbl9051] REAL           NULL,
    [Tbl9061] REAL           NULL,
    [Tbl9071] REAL           NULL,
    CONSTRAINT [PK___15__14] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

