﻿CREATE TABLE [dbo].[RankBreaks] (
    [BreakID]     INT          NOT NULL,
    [Description] VARCHAR (30) NULL,
    [Active]      CHAR (1)     CONSTRAINT [DF__RankBreak__Activ__1EF99443] DEFAULT ('Y') NOT NULL,
    CONSTRAINT [PK_RankBreaks_1] PRIMARY KEY CLUSTERED ([BreakID] ASC),
    CONSTRAINT [UniqueBreakDesc] UNIQUE NONCLUSTERED ([Description] ASC)
);

