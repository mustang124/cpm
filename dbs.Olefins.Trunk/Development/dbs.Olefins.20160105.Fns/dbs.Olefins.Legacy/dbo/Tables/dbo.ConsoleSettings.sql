﻿CREATE TABLE [dbo].[ConsoleSettings] (
    [UserName]    VARCHAR (10)  NOT NULL,
    [Application] VARCHAR (255) NOT NULL,
    [Section]     VARCHAR (255) NOT NULL,
    [KeyName]     VARCHAR (255) NOT NULL,
    [KeyValue]    VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ConsoleSettings] PRIMARY KEY CLUSTERED ([UserName] ASC, [Application] ASC, [Section] ASC, [KeyName] ASC)
);

