﻿CREATE TABLE [dbo].[Replacement] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [ISBL]             REAL           NULL,
    [AdjISBL]          REAL           NULL,
    [Pyrogas]          REAL           NULL,
    [Boil]             REAL           NULL,
    [Offsites]         REAL           NULL,
    [SuplFeed]         REAL           NULL,
    [DualTrain]        REAL           NULL,
    [OwnerCost]        REAL           NULL,
    [USGC]             REAL           NULL,
    [LocAdj]           REAL           NULL,
    [TotReplVal]       REAL           NULL,
    [OffsiteRepValFac] REAL           NULL,
    CONSTRAINT [PK___Replacement] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

