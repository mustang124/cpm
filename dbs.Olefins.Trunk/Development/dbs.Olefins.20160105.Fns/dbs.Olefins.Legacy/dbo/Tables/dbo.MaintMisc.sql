﻿CREATE TABLE [dbo].[MaintMisc] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [CorrEmergMaint]   REAL           NULL,
    [CorrOthMaint]     REAL           NULL,
    [PrevCondAct]      REAL           NULL,
    [PrevAddMaint]     REAL           NULL,
    [PrevRoutine]      REAL           NULL,
    [PrefAddRoutine]   REAL           NULL,
    [Accuracy]         CHAR (1)       NULL,
    [FurnTubeLife]     REAL           NULL,
    [RespUnplan]       REAL           NULL,
    [RespPlan]         REAL           NULL,
    [Prevent]          REAL           NULL,
    [Predict]          REAL           NULL,
    [Training]         REAL           NULL,
    [FurnRetubeTime]   REAL           NULL,
    [FurnRetubeWho]    VARCHAR (25)   NULL,
    [MaintMatlPY]      REAL           NULL,
    [ContMaintLaborPY] REAL           NULL,
    [ContMaintMatlPY]  REAL           NULL,
    [MaintEquipRentPY] REAL           NULL,
    [OCCMaintST_PY]    REAL           NULL,
    [MPSMaintST_PY]    REAL           NULL,
    CONSTRAINT [PK___20__14] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

