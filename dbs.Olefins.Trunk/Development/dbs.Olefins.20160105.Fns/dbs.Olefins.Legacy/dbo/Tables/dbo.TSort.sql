﻿CREATE TABLE [dbo].[TSort] (
    [Refnum]              [dbo].[Refnum] NOT NULL,
    [Co]                  CHAR (40)      NULL,
    [Loc]                 CHAR (40)      NULL,
    [CoLoc]               VARCHAR (60)   NULL,
    [CoName]              VARCHAR (100)  NULL,
    [PlantName]           VARCHAR (60)   NULL,
    [PlantLoc]            VARCHAR (40)   NULL,
    [CoordName]           VARCHAR (40)   NULL,
    [CoordTitle]          VARCHAR (75)   NULL,
    [POBox]               VARCHAR (35)   NULL,
    [Street]              VARCHAR (60)   NULL,
    [City]                VARCHAR (40)   NULL,
    [State]               VARCHAR (25)   NULL,
    [Country]             VARCHAR (30)   NULL,
    [Zip]                 VARCHAR (20)   NULL,
    [Telephone]           VARCHAR (30)   NULL,
    [Fax]                 VARCHAR (30)   NULL,
    [WWW]                 VARCHAR (65)   NULL,
    [PricingContact]      VARCHAR (40)   NULL,
    [PricingContactEmail] VARCHAR (65)   NULL,
    [DCContact]           VARCHAR (40)   NULL,
    [DCContactEmail]      VARCHAR (65)   NULL,
    [UOM]                 CHAR (1)       NULL,
    [PlantCountry]        CHAR (20)      NULL,
    [Region]              VARCHAR (10)   NULL,
    [CapPeerGrp]          TINYINT        NULL,
    [TechPeerGrp]         TINYINT        NULL,
    [FeedClass]           TINYINT        NULL,
    [FeedSubClass]        CHAR (2)       NULL,
    [EDCPeerGrp]          TINYINT        NULL,
    [LocFactor]           REAL           NULL,
    [InflFactor]          REAL           NULL,
    [CompanyID]           CHAR (25)      NULL,
    [ContactCode]         CHAR (25)      NULL,
    [StudyYear]           SMALLINT       NULL,
    [RefNumber]           CHAR (4)       NULL,
    [PresLabel]           VARCHAR (3)    NULL,
    [Energy]              TINYINT        NULL,
    [Polymer]             TINYINT        NULL,
    [CogenPeerGrp]        VARCHAR (10)   NULL,
    [SharedPeerGrp]       VARCHAR (20)   NULL,
    [Password]            VARCHAR (20)   NULL,
    [RefDirectory]        VARCHAR (100)  NULL,
    [Consultant]          CHAR (5)       NULL,
    [Comments]            CHAR (255)     NULL,
    CONSTRAINT [PK_Clients_1__14] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO
Create    trigger TSortCoLoc on dbo.TSort for insert, update 
AS
IF UPDATE(Co) OR UPDATE(Loc)
	UPDATE TSort
	SET CoLoc = rtrim(Co) + ' - ' + rtrim(Loc)
	WHERE Refnum IN (SELECT Refnum FROM inserted)
