﻿CREATE FUNCTION [audit].[ParseString]
(
	@List			NVARCHAR(MAX),
	@Delimiter		NCHAR(1)	= N'|'
)
RETURNS @Items TABLE
(
	[Id]			INT				NOT	NULL	IDENTITY (1, 1),
	[Item]			NVARCHAR(256)	NOT	NULL
)
WITH SCHEMABINDING
AS
BEGIN

	SET @List = LTRIM(RTRIM(@List)) + @Delimiter;

	DECLARE	@Pos	INT = CHARINDEX(@Delimiter, @List, 1);
	DECLARE	@Itm	NVARCHAR(256);

	WHILE (@Pos > 0)
	BEGIN

		SET @Itm = LTRIM(RTRIM(LEFT(@List, @Pos - 1)));

		IF (@Itm <> N'') INSERT INTO @Items(Item) VALUES(@Itm);

		SET @List = RIGHT(@List, LEN(@List) - @Pos)
		SET @Pos = CHARINDEX(@Delimiter, @List, 1)

	END

	RETURN;

END;