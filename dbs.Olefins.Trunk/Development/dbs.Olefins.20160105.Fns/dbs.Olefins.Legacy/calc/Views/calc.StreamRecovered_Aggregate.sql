﻿CREATE VIEW [calc].[StreamRecovered_Aggregate]
WITH SCHEMABINDING
AS
SELECT
	c.[MethodologyId],
	c.[SubmissionId],
	b.[StreamId],
	SUM(c.[_Recovered_Dur_kMT])		[Recovered_Dur_kMT],
	SUM(c.[_Recovered_Ann_kMT])		[Recovered_Ann_kMT],
	COUNT_BIG(*)					[Items]
FROM [calc].[StreamRecovered]		c
INNER JOIN [dim].[Stream_Bridge]	b
	ON	b.[DescendantId] = c.[StreamId]
GROUP BY
	c.[MethodologyId],
	c.[SubmissionId],
	b.[StreamId];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_StreamRecovered_Aggregate]
    ON [calc].[StreamRecovered_Aggregate]([MethodologyId] DESC, [SubmissionId] DESC, [StreamId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_StreamRecovered_Aggregate]
    ON [calc].[StreamRecovered_Aggregate]([MethodologyId] DESC, [SubmissionId] DESC, [StreamId] ASC)
    INCLUDE([Recovered_Dur_kMT], [Recovered_Ann_kMT]);

