﻿CREATE PROCEDURE [calc].[Insert_Standard_kEdc_Supplemental]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE	@StandardId			INT	= dim.Return_StandardId('kEdc');
	DECLARE	@ProcessUnitId		INT	= dim.Return_ProcessUnitId('Supp');
	DECLARE	@FactorId			INT	= dim.Return_FactorId('Supp');

	INSERT INTO [calc].[Standards]([MethodologyId], [SubmissionId], [StandardId], [ProcessUnitId], [StandardValue])
	SELECT
		cap.[MethodologyId],
		cap.[SubmissionId],
		f.[StandardId],
		@ProcessUnitId,
		f.[Coefficient] * cap.[_SupplementalInfer_MTSD] / 1000.0
	FROM	[calc].[Capacity]							cap
	INNER JOIN	[calc].[FeedClass]						fdc
		ON	fdc.[MethodologyId]	= cap.[MethodologyId]
		AND	fdc.[SubmissionId]	= cap.[SubmissionId]
	INNER JOIN	[ante].[MapFactorFeedClass]				map
		ON	map.[MethodologyId]	= fdc.[MethodologyId]
		AND	map.[FeedClassId]	= fdc.[FeedClassId]
	INNER JOIN	[ante].[Factors]						f
		ON	f.[MethodologyId]	= map.[MethodologyId]
		AND	f.[FactorId]		= map.[FactorId]
		AND	f.[StandardId]		= @StandardId
	INNER JOIN	[dim].[Factor_Bridge]					b
		ON	b.[MethodologyId]	= cap.[MethodologyId]
		AND	b.[DescendantId]	= f.[FactorId]
		AND	b.[FactorId]		= @FactorId
	WHERE	cap.[_SupplementalInfer_MTSD] >= 0.0
		AND	cap.[MethodologyId]	= @MethodologyId
		AND	cap.[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;