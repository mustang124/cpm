﻿CREATE PROCEDURE [calc].[Update_Audit]
(
	@MethodologyId			INT,
	@SubmissionId			INT,

	@TimeEnd				DATETIMEOFFSET,
	@Error_Count			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	UPDATE [calc].[Audit]
	SET	[TimeEnd]			= @TimeEnd,
		[Error_Count]		= @Error_Count
	WHERE	[MethodologyId]	= @MethodologyId
		AND	[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId))
					+ (', @TimeEnd:'		+ CONVERT(VARCHAR, @TimeEnd))
					+ (', @Error_Count:'	+ CONVERT(VARCHAR, @Error_Count));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;