﻿
CREATE FUNCTION [calc].[MaxValue]
(
	@Param1		FLOAT,
	@Param2		FLOAT
)
RETURNS FLOAT
AS
BEGIN

	RETURN CASE WHEN COALESCE(@Param1, 0.0) > COALESCE(@Param2, 0.0) THEN @Param1 ELSE @Param2 END;

END