﻿CREATE TABLE [calc].[StandardEnergy_PyrolysisBtu] (
    [MethodologyId]            INT                NOT NULL,
    [SubmissionId]             INT                NOT NULL,
    [Energy_kBtu]              FLOAT (53)         NOT NULL,
    [HvcYieldDivisor_kLbDay]   FLOAT (53)         NOT NULL,
    [StandardEnergy_kBtuLbDay] FLOAT (53)         NOT NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (128)     CONSTRAINT [DF_StandardEnergy_PyrolysisBtu_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]             ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StandardEnergy_PyrolysisBtu] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC),
    CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_Energy_kBtu_MinIncl_0.0] CHECK ([Energy_kBtu]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_HvcYieldDivisor_kLbDay_MinIncl_0.0] CHECK ([HvcYieldDivisor_kLbDay]>=(0.0)),
    CONSTRAINT [CR_StandardEnergy_PyrolysisBtu_StandardEnergy_kBtuLbDay_MinIncl_0.0] CHECK ([StandardEnergy_kBtuLbDay]>=(0.0)),
    CONSTRAINT [FK_StandardEnergy_PyrolysisBtu_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StandardEnergy_PyrolysisBtu_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [calc].[t_StandardEnergy_PyrolysisBtu_u]
	ON [calc].[StandardEnergy_PyrolysisBtu]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StandardEnergy_PyrolysisBtu]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StandardEnergy_PyrolysisBtu].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StandardEnergy_PyrolysisBtu].[SubmissionId]		= INSERTED.[SubmissionId];

END;