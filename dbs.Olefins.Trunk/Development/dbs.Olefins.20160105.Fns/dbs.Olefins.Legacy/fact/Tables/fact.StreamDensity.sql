﻿CREATE TABLE [fact].[StreamDensity] (
    [SubmissionId]   INT                NOT NULL,
    [StreamNumber]   INT                NOT NULL,
    [Density_SG]     FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_StreamDensity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_StreamDensity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_StreamDensity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_StreamDensity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamDensity] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC),
    CONSTRAINT [CR_StreamDensity_Density_SG_MinIncl_0.0] CHECK ([Density_SG]>=(0.0)),
    CONSTRAINT [FK_StreamDensity_StreamQuantity] FOREIGN KEY ([SubmissionId], [StreamNumber]) REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber]),
    CONSTRAINT [FK_StreamDensity_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_StreamDensity_u]
	ON [fact].[StreamDensity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamDensity]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamDensity].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[fact].[StreamDensity].[StreamNumber]	= INSERTED.[StreamNumber];

END;