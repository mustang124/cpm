﻿CREATE PROCEDURE [ante].[Merge_Factors_EiiIntercepts]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[Factors_EiiIntercepts] AS Target
	USING
	(
		VALUES
			(@MethodologyId, 0.8631, 6.4880)
	)
	AS Source([MethodologyId], [HvcYield_Intercept], [Energy_Intercept])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	WHEN MATCHED THEN UPDATE SET
		Target.[HvcYield_Intercept]		= Source.[HvcYield_Intercept],
		Target.[Energy_Intercept]		= Source.[Energy_Intercept]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [HvcYield_Intercept], [Energy_Intercept])
		VALUES([MethodologyId], [HvcYield_Intercept], [Energy_Intercept])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;