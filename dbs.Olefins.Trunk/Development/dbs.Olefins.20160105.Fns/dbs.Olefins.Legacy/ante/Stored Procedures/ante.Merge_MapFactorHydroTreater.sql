﻿CREATE PROCEDURE [ante].[Merge_MapFactorHydroTreater]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapFactorHydroTreater] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_HydroTreaterTypeId('B'),  dim.Return_FactorId('TowerPyroGasB')),
			(@MethodologyId, dim.Return_HydroTreaterTypeId('DS'), dim.Return_FactorId('TowerPyroGasDS')),
			(@MethodologyId, dim.Return_HydroTreaterTypeId('SS'), dim.Return_FactorId('TowerPyroGasSS'))
	)
	AS Source([MethodologyId], [HydroTreaterTypeId], [FactorId])
	ON	Target.[MethodologyId]			= Source.[MethodologyId]
	AND	Target.[HydroTreaterTypeId]		= Source.[HydroTreaterTypeId]
	WHEN MATCHED THEN UPDATE SET
		Target.[FactorId]	= Source.[FactorId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [HydroTreaterTypeId], [FactorId])
		VALUES([MethodologyId], [HydroTreaterTypeId], [FactorId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;