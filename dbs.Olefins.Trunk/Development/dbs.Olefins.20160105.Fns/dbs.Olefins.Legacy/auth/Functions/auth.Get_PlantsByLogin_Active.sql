﻿CREATE FUNCTION [auth].[Get_PlantsByLogin_Active]
(
	@LoginId		INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		pbl.[JoinId],
		pbl.[PlantId],
		pbl.[LoginId],
		pbl.[PlantName],
		pbl.[LoginTag]
	FROM [auth].[PlantsByLogin_Active]	pbl WITH (NOEXPAND)
	WHERE	pbl.[LoginId]	= @LoginId
);