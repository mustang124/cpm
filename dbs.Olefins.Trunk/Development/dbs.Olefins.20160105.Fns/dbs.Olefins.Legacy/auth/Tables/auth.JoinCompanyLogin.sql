﻿CREATE TABLE [auth].[JoinCompanyLogin] (
    [JoinId]         INT                IDENTITY (1, 1) NOT NULL,
    [CompanyId]      INT                NOT NULL,
    [LoginId]        INT                NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_JoinCompanyLogin_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_JoinCompanyLogin_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_JoinCompanyLogin_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_JoinCompanyLogin_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_JoinCompanyLogin_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_JoinCompanyLogin] PRIMARY KEY CLUSTERED ([JoinId] ASC),
    CONSTRAINT [FK_JoinCompanyLogin_Companies] FOREIGN KEY ([CompanyId]) REFERENCES [auth].[Companies] ([CompanyId]),
    CONSTRAINT [FK_JoinCompanyLogin_Logins] FOREIGN KEY ([LoginId]) REFERENCES [auth].[Logins] ([LoginId]),
    CONSTRAINT [UK_JoinCompanyLogin_CompanyLogin] UNIQUE NONCLUSTERED ([CompanyId] ASC, [LoginId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinCompanyLogin_Active]
    ON [auth].[JoinCompanyLogin]([CompanyId] ASC, [LoginId] ASC) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_JoinCompanyLogin_u]
	ON [auth].[JoinCompanyLogin]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinCompanyLogin]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinCompanyLogin].[JoinId]	= INSERTED.[JoinId];

END;