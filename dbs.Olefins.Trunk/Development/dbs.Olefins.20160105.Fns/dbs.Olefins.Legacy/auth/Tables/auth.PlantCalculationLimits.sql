﻿CREATE TABLE [auth].[PlantCalculationLimits] (
    [PlantId]              INT                NOT NULL,
    [CalcsPerPeriod_Count] INT                CONSTRAINT [DF_PlantCalculationLimits_CalcsPerPeriod_Count] DEFAULT ((4)) NOT NULL,
    [PeriodLen_Days]       INT                CONSTRAINT [DF_PlantCalculationLimits_PeriodLen_Days] DEFAULT ((90)) NOT NULL,
    [Active]               BIT                CONSTRAINT [DF_PlantCalculationLimits_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_PlantCalculationLimits_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (128)     CONSTRAINT [DF_PlantCalculationLimits_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (128)     CONSTRAINT [DF_PlantCalculationLimits_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (128)     CONSTRAINT [DF_PlantCalculationLimits_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_PlantCalculationLimits] PRIMARY KEY CLUSTERED ([PlantId] ASC),
    CONSTRAINT [CR_PlantCalculationLimits_CalcsPerPeriod_Count_MinIncl_0] CHECK ([CalcsPerPeriod_Count]>=(0)),
    CONSTRAINT [CR_PlantCalculationLimits_PeriodLen_Days_MinIncl_0] CHECK ([PeriodLen_Days]>=(0)),
    CONSTRAINT [FK_PlantCalculationLimits_Plants] FOREIGN KEY ([PlantId]) REFERENCES [auth].[Plants] ([PlantId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_PlantCalculationLimits_Active]
    ON [auth].[PlantCalculationLimits]([PlantId] ASC)
    INCLUDE([CalcsPerPeriod_Count], [PeriodLen_Days]) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_PlantCalculationLimits_u]
	ON [auth].[PlantCalculationLimits]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[PlantCalculationLimits]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[PlantCalculationLimits].[PlantId]	= INSERTED.[PlantId];

END;