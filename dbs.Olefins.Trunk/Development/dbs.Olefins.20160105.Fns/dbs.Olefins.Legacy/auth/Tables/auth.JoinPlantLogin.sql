﻿CREATE TABLE [auth].[JoinPlantLogin] (
    [JoinId]         INT                IDENTITY (1, 1) NOT NULL,
    [PlantId]        INT                NOT NULL,
    [LoginId]        INT                NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_JoinPlantLogin_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_JoinPlantLogin_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_JoinPlantLogin_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_JoinPlantLogin_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_JoinPlantLogin_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_JoinPlantLogin] PRIMARY KEY CLUSTERED ([JoinId] ASC),
    CONSTRAINT [FK_JoinPlantLogin_Logins] FOREIGN KEY ([LoginId]) REFERENCES [auth].[Logins] ([LoginId]),
    CONSTRAINT [FK_JoinPlantLogin_Plants] FOREIGN KEY ([PlantId]) REFERENCES [auth].[Plants] ([PlantId]),
    CONSTRAINT [UK_JoinPlantLogin_PlantLogin] UNIQUE NONCLUSTERED ([PlantId] ASC, [LoginId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_JoinPlantLogin_Active]
    ON [auth].[JoinPlantLogin]([PlantId] ASC, [LoginId] ASC) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_JoinPlantLogin_u]
	ON [auth].[JoinPlantLogin]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[JoinPlantLogin]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[JoinPlantLogin].[JoinId]		= INSERTED.[JoinId];

END;