﻿CREATE PROCEDURE [web].[Get_LoginsByCompanyRole_Active]
(
	@CompanyId			INT,
	@SecurityLevelID	INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @RoleId	INT = [auth].[Return_RoleId](@SecurityLevelID, NULL);

	SELECT
		lbcr.[JoinId],
		lbcr.[CompanyId],
		lbcr.[LoginId]		[UserId],
		lbcr.[CompanyName],
		lbcr.[LoginTag]		[UserName],
		lbcr.[NameFirst]	[FirstName],
		lbcr.[NameLast]		[LastName],
		lbcr.[eMail],
		lbcr.[_NameComma]	[LastFirst],
		lbcr.[_NameFull]	[NameFull],
		lbcr.[RoleId],
		@SecurityLevelID	[SecurityLevelID]
	FROM [auth].[Get_LoginsByCompanyRole_Active](@CompanyId, @RoleId)	lbcr
	ORDER BY
		lbcr.[NameLast]		ASC,
		lbcr.[NameFirst]	ASC,
		lbcr.[eMail]		ASC;

END;