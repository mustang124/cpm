﻿CREATE TABLE [stage].[SubmissionComments] (
    [SubmissionId]      INT                NOT NULL,
    [SubmissionComment] NVARCHAR (MAX)     NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_SubmissionComments_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_SubmissionComments_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_SubmissionComments_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_SubmissionComments_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_SubmissionComments] PRIMARY KEY CLUSTERED ([SubmissionId] DESC),
    CONSTRAINT [CL_SubmissionComments_SumbissionComment] CHECK ([SubmissionComment]<>''),
    CONSTRAINT [FK_SubmissionComments_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [stage].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [stage].[t_SubmissionComments_u]
	ON [stage].[SubmissionComments]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [stage].[SubmissionComments]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[stage].[SubmissionComments].[SubmissionId]		= INSERTED.[SubmissionId]

END;