﻿CREATE PROCEDURE [Console].[GetContactEmails] 
	@RefNum dbo.Refnum
AS
BEGIN
	SELECT 'Company' as [EmailType], FirstName + ' ' + LastName as [Name], cc.Email,cc.Password  
	FROM dbo.CoContactInfo cc INNER JOIN TSort t on cc.ContactCode = t.ContactCode
	Where t.Refnum = dbo.FormatRefNum(@RefNum,0)

	UNION

	SELECT 'Plant' as [EmailType], CoordName [Name], www, null 
	FROM TSort WHERE Refnum = @RefNum

	UNION
	
	SELECT 'Pricing' as [EmailType], PricingContact [Name], PricingContactEmail, null 
	FROM TSort WHERE Refnum = @RefNum
	
	UNION 
	
	SELECT 'DC' as [EmailType], DCContact [Name], DCContactEmail, null 
	FROM TSort WHERE Refnum = @RefNum
END
