﻿CREATE TABLE [ante].[PricingPeersStreamCountry] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [CountryId]         CHAR (3)           NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [OverRide_RegionId] VARCHAR (5)        NOT NULL,
    [AccountId]         VARCHAR (42)       NOT NULL,
    [StreamId]          VARCHAR (42)       NULL,
    [Adj_Proximity_Cur] REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPeersStreamCountry_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_PricingPeersStreamCountry_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_PricingPeersStreamCountry_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_PricingPeersStreamCountry_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [FK_PricingPeersStreamCountry_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_PricingPeersStreamCountry_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPeersStreamCountry_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingPeersStreamCountry_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingPeersStreamCountry_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingPeersStreamCountry_Region_LookUp] FOREIGN KEY ([OverRide_RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingPeersStreamCountry_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [UK_PricingPeersStreamCountry] UNIQUE CLUSTERED ([FactorSetId] ASC, [CountryId] ASC, [CurrencyRpt] ASC, [OverRide_RegionId] ASC, [AccountId] ASC, [StreamId] ASC, [CalDateKey] ASC)
);


GO

CREATE TRIGGER [ante].[t_PricingPeersStreamCountry_u]
	ON [ante].[PricingPeersStreamCountry]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingPeersStreamCountry]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingPeersStreamCountry].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[PricingPeersStreamCountry].CountryId		= INSERTED.CountryId
		AND	[ante].[PricingPeersStreamCountry].CalDateKey		= INSERTED.CalDateKey
		AND	[ante].[PricingPeersStreamCountry].StreamId			= INSERTED.StreamId
		AND	[ante].[PricingPeersStreamCountry].CurrencyRpt		= INSERTED.CurrencyRpt
		AND	[ante].[PricingPeersStreamCountry].AccountId		= INSERTED.AccountId;

END;