﻿CREATE TABLE [ante].[PricingScenarioPlant] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [ScenarioId]     VARCHAR (42)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [RegionId]       VARCHAR (5)        NULL,
    [CountryId]      CHAR (3)           NULL,
    [LocationId]     VARCHAR (42)       NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PricingScenarioPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PricingScenarioPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PricingScenarioPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PricingScenarioPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingScenarioPlant] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ScenarioId] ASC, [Refnum] ASC),
    CONSTRAINT [FK_PricingScenarioPlant_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingScenarioPlant_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingScenarioPlant_LocationId] FOREIGN KEY ([LocationId]) REFERENCES [dim].[PricingLocationsLu] ([LocationId]),
    CONSTRAINT [FK_PricingScenarioPlant_PricingScenarioConfig] FOREIGN KEY ([FactorSetId], [ScenarioId]) REFERENCES [ante].[PricingScenarioConfig] ([FactorSetId], [ScenarioId]),
    CONSTRAINT [FK_PricingScenarioPlant_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId])
);


GO

CREATE TRIGGER [ante].[t_PricingScenarioPlant_u]
	ON [ante].[PricingScenarioPlant]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingScenarioPlant]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingScenarioPlant].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[PricingScenarioPlant].ScenarioId		= INSERTED.ScenarioId
		AND	[ante].[PricingScenarioPlant].Refnum			= INSERTED.Refnum

END;