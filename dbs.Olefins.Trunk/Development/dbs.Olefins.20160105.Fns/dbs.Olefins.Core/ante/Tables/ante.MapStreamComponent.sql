﻿CREATE TABLE [ante].[MapStreamComponent] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapStreamComponent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MapStreamComponent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MapStreamComponent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MapStreamComponent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MapStreamComponent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC, [ComponentId] ASC),
    CONSTRAINT [FK_MapStreamComponent_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_MapStreamComponent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MapStreamComponent_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_MapStreamComponent_u]
	ON [ante].[MapStreamComponent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapStreamComponent]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[MapStreamComponent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[MapStreamComponent].StreamId		= INSERTED.StreamId
		AND	[ante].[MapStreamComponent].ComponentId	= INSERTED.ComponentId;

END;
