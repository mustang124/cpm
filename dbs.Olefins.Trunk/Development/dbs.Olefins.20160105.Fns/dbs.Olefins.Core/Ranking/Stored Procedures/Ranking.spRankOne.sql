﻿
CREATE PROC [Ranking].[spRankOne] @ListId varchar(42), @RankBreak sysname, @BreakValue varchar(12), @RankVariable sysname, @ForceTiles tinyint,
	@NumTiles tinyint OUTPUT, @DataCount int OUTPUT, @Top2 real OUTPUT, @Tile1Break real OUTPUT, @Tile2Break real OUTPUT, @Tile3Break real OUTPUT, @Btm2 real OUTPUT
AS
SET NOCOUNT ON

DECLARE @Refnums dbo.RefnumList
INSERT @Refnums(Refnum) SELECT Refnum FROM Ranking.GetRefnumList(@ListId, @RankBreak, @BreakValue)

SET NOCOUNT OFF

EXEC Ranking.RankRefnumList @Refnums, @RankVariable, @ForceTiles, @NumTiles OUTPUT, @DataCount OUTPUT, @Top2 OUTPUT, @Tile1Break OUTPUT, @Tile2Break OUTPUT, @Tile3Break OUTPUT, @Btm2 OUTPUT
