﻿CREATE FUNCTION [Ranking].[GetForceTiles](@ListId varchar(42), @RankBreak sysname, @BreakValue varchar(12))
RETURNS tinyint
AS
BEGIN
	DECLARE @NumTiles tinyint
	
	SELECT @NumTiles = NumTiles FROM RankForce
	WHERE ListId = @ListId AND RankBreak = @RankBreak AND BreakValue = @BreakValue
	
	RETURN @NumTiles
END
