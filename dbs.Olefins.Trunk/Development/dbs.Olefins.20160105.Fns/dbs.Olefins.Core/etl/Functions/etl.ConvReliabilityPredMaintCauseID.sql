﻿CREATE FUNCTION [etl].[ConvReliabilityPredMaintCauseId]
(
	@PredMaintCauseID	VARCHAR(8)
)
RETURNS NVARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(42) =
	CASE RTRIM(LTRIM(@PredMaintCauseID))
		WHEN 'FreqVib'	THEN 'Vibration'
		WHEN 'FreqComp'	THEN 'Compressor'
		WHEN 'FreqTurb'	THEN 'Turbine'
		WHEN 'FreqStat'	THEN 'Stastical'
	END

	RETURN @ResultVar;
	
END