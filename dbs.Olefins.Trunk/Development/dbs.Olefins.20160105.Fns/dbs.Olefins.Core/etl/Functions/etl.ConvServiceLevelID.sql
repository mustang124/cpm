﻿CREATE FUNCTION [etl].[ConvServiceLevelId]
(
	@ServiceLevelId	NVARCHAR(8)
)
RETURNS NVARCHAR(14)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(14) =
	CASE UPPER(LEFT(@ServiceLevelId, 1))
		WHEN 'O'	THEN 'Outage'
		WHEN 'S'	THEN 'SlowDown'
		WHEN 'D'	THEN 'DownTime'
	END
		
	RETURN @ResultVar;
	
END