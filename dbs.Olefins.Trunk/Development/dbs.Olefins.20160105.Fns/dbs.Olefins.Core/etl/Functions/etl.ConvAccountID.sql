﻿
CREATE FUNCTION etl.ConvAccountID
(
	@AccountId	VARCHAR(42)
)
RETURNS VARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	VARCHAR(42) = 
	CASE @AccountId
		WHEN 'Equip'			THEN 'MaintEquip'
		WHEN 'ContMaintLabor'	THEN 'MaintContractLabor'
		WHEN 'ContMaintMatl'	THEN 'MaintContractMatl'
		WHEN 'ContTechSvc'		THEN 'ContractTechSvc'
		WHEN 'OthCont'			THEN 'ContractServOth'
		
		WHEN 'OthVol'			THEN 'OtherVol'
		WHEN 'OthNonVol'		THEN 'OtherNonVol'
		
		WHEN 'SteamExports'		THEN 'ExportSteam'
		WHEN 'PowerExports'		THEN 'ExportElectric'
		
		WHEN 'PurFG'			THEN 'PurFuelGas'
		WHEN 'PPCFuelGas'		THEN 'PPFCFuelGas'
		WHEN 'PPCFuelOth'		THEN 'PPFCOther'
		
		-- Table 6-4
		WHEN 'RetubeCost'		THEN 'MaintRetube'
		WHEN 'RetubeCostMatl'	THEN 'MaintRetubeMatl'
		WHEN 'RetubeCostLabor'	THEN 'MaintRetubeLabor'
		WHEN 'OtherMajorCost'	THEN 'MaintMajor'
		
		-- Table 10
		
		WHEN 'MaintMatlPY'		THEN 'MaintMatl'
		WHEN 'ContMaintLaborPY'	THEN 'MaintContractLabor'
		WHEN 'ContMaintMatlPY'	THEN 'MaintContractMatl'
		WHEN 'MaintEquipRentPY'	THEN 'MaintEquip'
		
		WHEN 'CorrEmergMaint'	THEN 'MaintCorrEmergancy'
		WHEN 'CorrOthMaint'		THEN 'MaintCorrProcess'
		WHEN 'PrevCondAct'		THEN 'MaintPrevCond'
		WHEN 'PrevAddMaint'		THEN 'MaintPrevCondAdd'
		WHEN 'PrevRoutine'		THEN 'MaintPrevSys'
		WHEN 'PrefAddRoutine'	THEN 'MaintPrevSysAdd'
		
		-- Table 12-1 (Polymner)
		WHEN 'NonTAMaint'		THEN 'Maint'
		WHEN 'TA'				THEN 'TACurrExp'
		WHEN 'Additives'		THEN 'ChemAdd'
		WHEN 'Energy'			THEN 'PurchasedEnergy'
		WHEN 'Packaging'		THEN 'PackagingMatlSvcs'
		
		ELSE @AccountId
		
		END;
	
	RETURN @ResultVar;
	
END