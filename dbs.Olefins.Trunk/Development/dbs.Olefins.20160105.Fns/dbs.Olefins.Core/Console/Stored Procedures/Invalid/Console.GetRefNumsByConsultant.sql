﻿CREATE PROCEDURE [Console].[GetRefNumsByConsultant]
@Consultant nvarchar(3),
@Study varchar(3),
@StudyYear nvarchar(4)

AS
BEGIN

	SELECT distinct SmallRefnum as refnum, t.Coloc FROM dbo.TSort t join fact.TSort f on t.Refnum = f.Refnum  WHERE Consultant=@Consultant AND t.StudyYear = @StudyYear  and f.CompanyID <> 'Solomon'

END