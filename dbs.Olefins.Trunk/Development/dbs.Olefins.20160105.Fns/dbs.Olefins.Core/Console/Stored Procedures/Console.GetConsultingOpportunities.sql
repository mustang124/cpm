﻿CREATE PROCEDURE [Console].[GetConsultingOpportunities]

	@Refnum varchar(18),
	@NoteType varchar(20)

AS
BEGIN

	select [Note] from Val.Notes  where Refnum = @Refnum and NoteType = @NoteType

END
