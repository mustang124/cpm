﻿CREATE PROCEDURE [Console].[IssueVCheck]
	@Refnum varchar(18),
	@IssueId varchar(20)
AS
BEGIN
DECLARE @Ret int
Select @Ret = Completed from Val.Checklist where Refnum='20' + @Refnum and IssueId = @IssueId
IF @Ret > 'Y'
	Return 1
ELSE
	return 0
END
