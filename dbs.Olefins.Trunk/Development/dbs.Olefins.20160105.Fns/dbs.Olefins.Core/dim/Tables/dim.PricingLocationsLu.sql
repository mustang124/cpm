﻿CREATE TABLE [dim].[PricingLocationsLu] (
    [LocationId]     VARCHAR (42)        NOT NULL,
    [LocationName]   NVARCHAR (84)       NOT NULL,
    [LocationDetail] NVARCHAR (256)      NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_PricingLocationsLu_Operator] DEFAULT ('+') NOT NULL,
    [Parent]         VARCHAR (42)        NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_PricingLocationsLu_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)      CONSTRAINT [DF_PricingLocationsLu_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)      CONSTRAINT [DF_PricingLocationsLu_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)      CONSTRAINT [DF_PricingLocationsLu_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingLocationsLu] PRIMARY KEY CLUSTERED ([LocationId] ASC),
    CONSTRAINT [CL_PricingLocationsLu_LocationDetail] CHECK (len([LocationDetail])>(0)),
    CONSTRAINT [CL_PricingLocationsLu_LocationId] CHECK (len([LocationId])>(0)),
    CONSTRAINT [CL_PricingLocationsLu_LocationName] CHECK (len([LocationName])>(0)),
    CONSTRAINT [CR_PricingLocationsLu_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_PricingLocationsLu_Parents] FOREIGN KEY ([Parent]) REFERENCES [dim].[PricingLocationsLu] ([LocationId]),
    CONSTRAINT [UK_PricingLocationsLu_LocationDetail] UNIQUE NONCLUSTERED ([LocationDetail] ASC),
    CONSTRAINT [UK_PricingLocationsLu_LocationName] UNIQUE NONCLUSTERED ([LocationName] ASC)
);


GO

CREATE TRIGGER [dim].[t_PricingLocationsLu_u]
	ON [dim].[PricingLocationsLu]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[PricingLocationsLu]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[PricingLocationsLu].[LocationId]		= INSERTED.[LocationId];
		
END;