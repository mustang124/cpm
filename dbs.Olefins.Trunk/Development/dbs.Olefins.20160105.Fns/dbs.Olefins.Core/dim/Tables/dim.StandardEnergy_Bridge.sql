﻿CREATE TABLE [dim].[StandardEnergy_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [StandardEnergyId]   VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_StandardEnergy_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_StandardEnergy_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_StandardEnergy_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_StandardEnergy_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_StandardEnergy_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_StandardEnergy_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StandardEnergyId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_StandardEnergy_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_StandardEnergy_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergy_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergy_Bridge_LookUp_StandardEnergy] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergy_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [StandardEnergyId]) REFERENCES [dim].[StandardEnergy_Parent] ([FactorSetId], [StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergy_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[StandardEnergy_Parent] ([FactorSetId], [StandardEnergyId])
);


GO

CREATE TRIGGER [dim].[t_StandardEnergy_Bridge_u]
ON [dim].[StandardEnergy_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[StandardEnergy_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[StandardEnergy_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[StandardEnergy_Bridge].[StandardEnergyId]	= INSERTED.[StandardEnergyId]
		AND	[dim].[StandardEnergy_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;