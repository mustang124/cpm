﻿CREATE TABLE [dim].[FacilityCoating_LookUp] (
    [CoatingId]      VARCHAR (42)       NOT NULL,
    [CoatingName]    NVARCHAR (84)      NOT NULL,
    [CoatingDetail]  NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilityCoating_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FacilityCoating_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FacilityCoating_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FacilityCoating_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilityCoating_LookUp] PRIMARY KEY CLUSTERED ([CoatingId] ASC),
    CONSTRAINT [CL_FacilityCoating_LookUp_CoatingDetail] CHECK ([CoatingDetail]<>''),
    CONSTRAINT [CL_FacilityCoating_LookUp_CoatingId] CHECK ([CoatingId]<>''),
    CONSTRAINT [CL_FacilityCoating_LookUp_CoatingName] CHECK ([CoatingName]<>''),
    CONSTRAINT [UK_FacilityCoating_LookUp_CoatingDetail] UNIQUE NONCLUSTERED ([CoatingDetail] ASC),
    CONSTRAINT [UK_FacilityCoating_LookUp_CoatingName] UNIQUE NONCLUSTERED ([CoatingName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FacilityCoating_LookUp_u]
	ON [dim].[FacilityCoating_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FacilityCoating_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FacilityCoating_LookUp].[CoatingId]		= INSERTED.[CoatingId];
		
END;
