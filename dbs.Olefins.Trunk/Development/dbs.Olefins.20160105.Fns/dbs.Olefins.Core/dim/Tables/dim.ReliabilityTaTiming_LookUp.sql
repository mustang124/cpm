﻿CREATE TABLE [dim].[ReliabilityTaTiming_LookUp] (
    [TimingId]       CHAR (1)           NOT NULL,
    [TimingName]     NVARCHAR (84)      NOT NULL,
    [TimingDetail]   NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityTaTiming_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityTaTiming_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityTaTiming_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityTaTiming_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReliabilityTaTiming_LookUp] PRIMARY KEY CLUSTERED ([TimingId] ASC),
    CONSTRAINT [CL_ReliabilityTaTiming_LookUp_TimingDetail] CHECK ([TimingDetail]<>''),
    CONSTRAINT [CL_ReliabilityTaTiming_LookUp_TimingId] CHECK ([TimingId]<>''),
    CONSTRAINT [CL_ReliabilityTaTiming_LookUp_TimingName] CHECK ([TimingName]<>''),
    CONSTRAINT [UK_ReliabilityTaTiming_LookUp_TimingDetail] UNIQUE NONCLUSTERED ([TimingDetail] ASC),
    CONSTRAINT [UK_ReliabilityTaTiming_LookUp_TimingName] UNIQUE NONCLUSTERED ([TimingName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReliabilityTaTiming_LookUp_u]
	ON [dim].[ReliabilityTaTiming_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReliabilityTaTiming_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityTaTiming_LookUp].[TimingId]		= INSERTED.[TimingId];
		
END;