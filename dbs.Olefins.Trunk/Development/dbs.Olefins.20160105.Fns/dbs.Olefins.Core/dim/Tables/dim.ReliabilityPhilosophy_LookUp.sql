﻿CREATE TABLE [dim].[ReliabilityPhilosophy_LookUp] (
    [PhilosophyId]     CHAR (1)           NOT NULL,
    [PhilosophyName]   NVARCHAR (84)      NOT NULL,
    [PhilosophyDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityPhilosophy_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPhilosophy_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPhilosophy_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPhilosophy_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReliabilityPhilosophy_LookUp] PRIMARY KEY CLUSTERED ([PhilosophyId] ASC),
    CONSTRAINT [CL_ReliabilityPhilosophy_LookUp_PhilosophyDetail] CHECK ([PhilosophyDetail]<>''),
    CONSTRAINT [CL_ReliabilityPhilosophy_LookUp_PhilosophyId] CHECK ([PhilosophyId]<>''),
    CONSTRAINT [CL_ReliabilityPhilosophy_LookUp_PhilosophyName] CHECK ([PhilosophyName]<>''),
    CONSTRAINT [UK_ReliabilityPhilosophy_LookUp_PhilosophyDetail] UNIQUE NONCLUSTERED ([PhilosophyDetail] ASC),
    CONSTRAINT [UK_ReliabilityPhilosophy_LookUp_PhilosophyName] UNIQUE NONCLUSTERED ([PhilosophyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReliabilityPhilosophy_LookUp_u]
	ON [dim].[ReliabilityPhilosophy_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReliabilityPhilosophy_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityPhilosophy_LookUp].[PhilosophyId]		= INSERTED.[PhilosophyId];
		
END;
