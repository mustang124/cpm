﻿CREATE FUNCTION [stat].[DescriptiveHistogram]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Histogram

	@Population			stat.ObservationsRandomVarResults	READONLY,						--	Data points to examine
	@zWidth				FLOAT				= 0.5											--	Histogram bucket width
)
RETURNS @ReturnTable TABLE 
(
	[zTestLower]		FLOAT				NOT NULL,										--	Minimum Z-Score of bucket
	[zTestUpper]		FLOAT				NOT NULL,										--	Maximum Z-Score of bucket
	[_zRange]			AS CAST('[' + CAST(ROUND([zTestLower], 2) AS VARCHAR(5)) + ', ' + CAST(ROUND([zTestUpper], 2) AS VARCHAR(5)) +']' AS VARCHAR(14)),

	[LimitLower]		FLOAT				NOT	NULL,										--	Minimum value of bucket
	[LimitUpper]		FLOAT				NOT	NULL,										--	Maximum value of bucket
	[_LimitRange]		AS '[' + CAST(ROUND([LimitLower], 2) AS VARCHAR(32)) + ', ' + CAST(ROUND([LimitUpper], 2) AS VARCHAR(32)) +']',

	[xCount]			SMALLINT			NOT	NULL	CHECK([xCount]	>= 0),				--	Item Count
	[_xHistogram]		AS REPLICATE(N'█', [xCount])										--	Histogram bars
						PERSISTED			NOT	NULL,

	[xMin]				FLOAT					NULL,										--	Minimum
	[xMax]				FLOAT					NULL,										--	Maximum
	[_xRange]			AS CAST([xMax] - [xMin]			AS FLOAT)							--	Range
						PERSISTED					,
	[xMean]				FLOAT					NULL,										--	Mean								http://en.wikipedia.org/wiki/Mean

	--[Observation]		FLOAT					NULL,

	--[_xHistoRange]		AS	CASE WHEN [Observation] IS NOT NULL
	--						THEN	REPLICATE(N'▒', [xCount])
	--						ELSE	REPLICATE(N'█', [xCount])
	--						END,

	--[_xHistoObs]		AS	CASE WHEN [Observation] IS NOT NULL
	--						THEN	REPLICATE(N'█',				FLOOR(([Observation] - [LimitLower]) / ([LimitUpper] - [LimitLower]) * [xCount])) + N'▒' + 
	--								REPLICATE(N'█', [xCount] -	FLOOR(([Observation] - [LimitLower]) / ([LimitUpper] - [LimitLower]) * [xCount]) - 1)
	--						ELSE	REPLICATE(N'█', [xCount])
	--						END,

	CHECK([zTestLower] <= [zTestUpper]),
	CHECK([LimitLower] <= [LimitUpper]),

	CHECK([xMin] <= [xMax]),
	CHECK([xMin] BETWEEN [LimitLower] AND [LimitUpper]),
	CHECK([xMax] BETWEEN [LimitLower] AND [LimitUpper]),
	CHECK([xMean] BETWEEN [xMin] AND [xMax]),

	PRIMARY KEY CLUSTERED(zTestLower ASC, zTestUpper ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @xMean		FLOAT(53);
	DECLARE @xStDevP	FLOAT(53);
	DECLARE @zMin		FLOAT(53);
	DECLARE @zMax		FLOAT(53);

	SELECT
		@xMean		= AVG(o.x),
		@xStDevP	= STDEVP(o.x),
		@zMin		= MIN(o.zScore),
		@zMax		= MAX(o.zScore)
	FROM @Population o
	WHERE o.Basis = 1;

	DECLARE @zTestLower	FLOAT;
	DECLARE @zTestUpper	FLOAT;
	DECLARE @LimitLower	FLOAT;
	DECLARE @LimitUpper	FLOAT;

	SET @zTestLower = FLOOR(@zMin - @zWidth) - @zWidth / 2.0;

	WHILE @zTestLower <= CEILING(@zMax + @zWidth / 2.0) 
	BEGIN
	
		SET @zTestUpper = @zTestLower + @zWidth;
		SET @LimitLower = @xMean + @zTestLower * @xStDevP
		SET @LimitUpper = @xMean + (@zTestLower + @zWidth) * @xStDevP;

		INSERT INTO @ReturnTable(zTestLower, zTestUpper, LimitLower, LimitUpper,
			xCount, xMin, xMax, xMean)
		SELECT
			@zTestLower,
			@zTestUpper,
			@LimitLower,
			@LimitUpper,
			COUNT(o.x),
			MIN(o.x),
			MAX(o.x),
			AVG(o.x)
		FROM @Population o
		WHERE	o.Basis = 1
			AND	o.x BETWEEN @LimitLower AND	@LimitUpper
			
		SET @zTestLower = @zTestLower + @zWidth;
		
	END;
	
	RETURN;

END;
