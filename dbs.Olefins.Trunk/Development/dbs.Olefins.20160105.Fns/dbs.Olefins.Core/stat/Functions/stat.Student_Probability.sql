﻿CREATE FUNCTION [stat].[Student_Probability]
(@DegreesOfFreedom FLOAT (53), @TStatistic FLOAT (53), @Sides TINYINT)
RETURNS FLOAT (53)
AS
 EXTERNAL NAME [Sa.Meta.Numerics].[UserDefinedFunctions].[Student_Probability]

