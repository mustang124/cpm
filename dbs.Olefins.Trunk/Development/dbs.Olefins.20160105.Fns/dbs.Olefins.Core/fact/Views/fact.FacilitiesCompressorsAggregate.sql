﻿CREATE VIEW fact.FacilitiesCompressorsAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	b.FacilityId,
	SUM(c.Age_Years * c.Power_BHP) / SUM(c.Power_BHP)	[Age_Years],
	SUM(c.Power_BHP)									[Power_BHP]
FROM dim.Facility_Bridge				b
INNER JOIN fact.FacilitiesCompressors	c
	ON	c.FacilityId = b.DescendantId
GROUP BY
	b.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	b.FacilityId
HAVING
	SUM(c.Power_BHP) <> 0.0;