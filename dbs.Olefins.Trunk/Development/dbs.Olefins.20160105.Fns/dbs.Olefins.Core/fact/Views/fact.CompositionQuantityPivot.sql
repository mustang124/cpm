﻿CREATE VIEW fact.CompositionQuantityPivot
WITH SCHEMABINDING
AS
SELECT
	t.Refnum,
	t.StreamId,
	t.StreamDescription,

	t.H2,
	t.CH4,
	t.C2H2,
	t.C2H4,
	t.C2H6,

	t.C3H6,
	t.C3H8,
		
	t.C4H6,
	t.C4H8,
	t.C4H10,
		
	t.C6H6,
	t.C7H16,
	t.C8H18,

	t.NBUTA,
	t.IBUTA,
	t.IB,
	t.B1,
	t.NC5,
	t.IC5,
	t.NC6,
	t.C6ISO,
	t.CO_CO2,
	t.S,

	t.P,
	t.I,
	t.A,
	t.N,
	t.O,

	t.PyroGasoline,
	t.PyroFuelOil,
	t.Inerts,

	t.Other
FROM(
	SELECT
		c.Refnum,
		c.StreamId,
		c.StreamDescription,
		c.ComponentId,
		c.Component_WtPcnt
	FROM fact.CompositionQuantity				c
	WHERE c.Component_WtPcnt IS NOT NULL
		AND c.ComponentId IN (
		'H2',
		'CH4',
		'C2H2', 'C2H4', 'C2H6',
		'C3H6', 'C3H8',
		'C4H6', 'C4H8', 'C4H10',
		'C6H6',
		'C7H16',
		'C8H18',
		'NBUTA', 'IBUTA', 'IB', 'B1', 'NC5', 'IC5', 'NC6', 'C6ISO', 'CO_CO2', 'S',
		'P', 'I', 'A', 'N', 'O',
		'PyroGasoline', 'PyroFuelOil', 'Inerts', 'Other')
	) u
	PIVOT (
	MAX(u.Component_WtPcnt) FOR u.ComponentId IN (
		H2,
		CH4,
		C2H2,
		C2H4,
		C2H6,

		C3H6,
		C3H8,
		
		C4H6,
		C4H8,
		C4H10,
		
		C6H6,
		C7H16,
		C8H18,

		NBUTA,
		IBUTA,
		IB,
		B1,
		NC5,
		IC5,
		NC6,
		C6ISO,
		CO_CO2,
		S,

		P,
		I,
		A,
		N,
		O,

		PyroGasoline,
		PyroFuelOil,
		Inerts,

		Other
		)
	) t;