﻿CREATE VIEW fact.QuantityWeightAggregate
WITH SCHEMABINDING
AS
SELECT
	t.Refnum,
	t.CalDateKey,
	t.StreamId,
	t.StreamDescription,
	t.ComponentId,
	
	CASE WHEN t.ComponentId			NOT	IN ('P', 'I', 'A', 'N', 'O') THEN t.Composition_kMT END
												[Composition_kMT],

	CASE WHEN t.ComponentId			NOT	IN ('P', 'I', 'A', 'N', 'O') AND t.StreamId <> 'StreamLoss' THEN t.Composition_kMT END 
		/ SUM(CASE WHEN t.ComponentId	NOT IN ('P', 'I', 'A', 'N', 'O') AND t.StreamId <> 'StreamLoss' THEN t.Composition_kMT END) OVER(PARTITION BY
			  t.Refnum
			, t.CalDateKey
			, t.StreamId
			) * 100.0							[Component_WtPcnt],
			
	CASE WHEN t.ComponentId				IN ('P', 'I', 'A', 'N', 'O') THEN t.Composition_kMT END
												[CompositionPIANO_kMT],

	CASE WHEN t.ComponentId				IN ('P', 'I', 'A', 'N', 'O') AND t.StreamId <> 'StreamLoss' THEN t.Composition_kMT END
		/ SUM(CASE WHEN t.ComponentId		IN ('P', 'I', 'A', 'N', 'O') AND t.StreamId <> 'StreamLoss' THEN t.Composition_kMT END) OVER(PARTITION BY
			  t.Refnum
			, t.CalDateKey
			, t.StreamId
			) * 100.0							[CompositionPIANO_WtPcnt]

FROM (	
	SELECT
		b.FactorSetId,
		q.Refnum,
		MAX(q.CalDateKey)	[CalDateKey],
		b.StreamId,
		CASE WHEN b.StreamId = q.StreamId THEN q.StreamDescription ELSE b.StreamId END	[StreamDescription],
		c.ComponentId,
		SUM(CASE b.DescendantOperator
			WHEN '+' THEN + q.Tot_kMT * c.Component_WtPcnt
			WHEN '-' THEN - q.Tot_kMT * c.Component_WtPcnt
			END) / 100.0								[Composition_kMT]
	FROM dim.Stream_Bridge						b
	INNER JOIN fact.QuantityPivot				q
		ON	q.StreamId = b.DescendantId
	INNER JOIN fact.CompositionQuantity			c
		ON	c.Refnum = q.Refnum
		AND c.StreamId = q.StreamId
		AND	c.StreamDescription = q.StreamDescription
	WHERE b.StreamId NOT IN ('NoRecycle', 'StreamLoss')
	GROUP BY
		b.FactorSetId,
		q.Refnum,
		b.StreamId,
		CASE WHEN b.StreamId = q.StreamId THEN q.StreamDescription ELSE b.StreamId END,
		c.ComponentId
	) t;