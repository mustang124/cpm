﻿CREATE VIEW fact.MetaOpExAggregate
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	p.CurrencyRpt,
	p.AccountId,
	p.Q1,
	p.Q2,
	p.Q3,
	p.Q4,
	p.Tot_Amount_Cur
FROM (
	SELECT
		b.FactorSetId,
		o.Refnum,
		COALESCE('Q' + CONVERT(CHAR(1), c.CalQtr), 'Tot_Amount_Cur')	[Qtr],
		o.CurrencyRpt,
		b.AccountId,
		SUM(CASE b.DescendantOperator
			WHEN '+' THEN + o.Amount_Cur
			WHEN '-' THEN - o.Amount_Cur
			END)			[Amount_Cur]
	FROM dim.Account_Bridge			b
	INNER JOIN fact.MetaOpEx		o
		ON	o.AccountId = b.DescendantId
	INNER JOIN dim.Calendar_LookUp	c
		ON	c.CalDateKey = o.CalDateKey
	GROUP BY
		b.FactorSetId,
		o.Refnum,
		ROLLUP(c.CalQtr),
		o.CurrencyRpt,
		b.AccountId
	) u
	PIVOT (MAX([Amount_Cur]) FOR [Qtr] IN (
		[Q1], [Q2], [Q3], [Q4], [Tot_Amount_Cur]
		)
	)p;