﻿CREATE VIEW [fact].[QuantitySupplementalRecycled]
WITH SCHEMABINDING
AS
SELECT
	q.Refnum,
	r.CalDateKey,
	m.StreamID_Supp,
	m.StreamID_SuppRec,
	m.ComponentId,
	m.StreamId,
	m.RecycleId,
	r.Recycled_WtPcnt,
	SUM(q.Quantity_kMT)								[Supplemental_kMT],
	SUM(q.Quantity_kMT) * r.Recycled_WtPcnt / 100.0	[SuppRecycled_kMT]
FROM fact.Quantity						q
INNER JOIN (VALUES
	('DilEthane',	'SuppToRecEthane',	'C2H6',		'EthRec', 1),
	('ROGEthane',	'SuppToRecEthane',	'C2H6',		'EthRec', 1),
	('DilPropane',	'SuppToRecPropane',	'C3H8',		'ProRec', 2),
	('ROGPropane',	'SuppToRecPropane',	'C3H8',		'ProRec', 2),
	('DilButane',	'SuppToRecButane',	'C4H10',	'ButRec', 4),
	('ROGC4Plus',	'SuppToRecButane',	'C4H10',	'ButRec', 4)
	) AS m(StreamID_Supp, StreamID_SuppRec, ComponentId, StreamId, RecycleId)
	ON	m.StreamID_Supp = q.StreamId
INNER JOIN fact.QuantitySuppRecycled	r
	ON	r.Refnum = q.Refnum
	AND	r.StreamId = m.StreamID_SuppRec
	AND r.ComponentId = m.ComponentId
WHERE	q.Quantity_kMT > 0.0
GROUP BY
	q.Refnum,
	r.CalDateKey,
	m.StreamID_Supp,
	m.StreamID_SuppRec,
	m.ComponentId,
	m.StreamId,
	m.RecycleId,
	r.Recycled_WtPcnt;