﻿CREATE VIEW fact.PolyQuantityAggregate
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	p.CalDateKey,
	p.StreamId,
	p.T1			[T1_kMT],
	p.T2			[T2_kMT],
	p.T3			[T3_kMT],
	p.T4			[T4_kMT],
	p.T5			[T5_kMT],
	p.T6			[T6_kMT]
FROM (
	SELECT
		b.FactorSetId,
		p.Refnum,
		p.CalDateKey,
		a.AssetIDSec,
		b.StreamId,
		CASE b.DescendantOperator
		WHEN '+' THEN + p.Quantity_kMT
		WHEN '-' THEN - p.Quantity_kMT
		END					[Quantity_kMT]
	FROM dim.Stream_Bridge			b
	INNER JOIN fact.PolyQuantity	p
		ON	p.StreamId = b.DescendantId
	INNER JOIN cons.Assets			a
		ON	a.AssetId = p.AssetId
	WHERE p.Refnum = '2011PCH118' and b.FactorSetId = '2011'
	) u
	PIVOT(
		SUM(u.Quantity_kMT)
		FOR AssetIDSec IN (
		T1, T2, T3, T4, T5, T6
		)
	) p;