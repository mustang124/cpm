﻿CREATE TABLE [fact].[PolyFacilities] (
    [Refnum]              VARCHAR (25)       NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [AssetId]             VARCHAR (30)       NOT NULL,
    [PolyName]            NVARCHAR (84)      NOT NULL,
    [PolyCity]            NVARCHAR (84)      NULL,
    [PolyState]           NVARCHAR (84)      NULL,
	[PolyCountryId]		  VARCHAR(4)		 NULL,
    [Capacity_kMT]        REAL               NULL,
    [Stream_MTd]          REAL               NULL,
    [ReactorTrains_Count] INT                NOT NULL,
    [StartUp_Year]        SMALLINT           NULL,
    [PolymerFamilyId]     VARCHAR (6)        NULL,
    [PolymerTechId]       VARCHAR (42)       NULL,
    [PrimeResin_Pcnt]     REAL               NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_PolyFacilities_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_PolyFacilities_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_PolyFacilities_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_PolyFacilities_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PolyFacilities] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AssetId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_PolyFacilities_PolyCity] CHECK ([PolyCity]<>''),
    CONSTRAINT [CL_PolyFacilities_PolyName] CHECK ([PolyName]<>''),
    CONSTRAINT [CL_PolyFacilities_PolyState] CHECK ([PolyState]<>''),
    CONSTRAINT [CR_PolyFacilities_CapacitykMT] CHECK ([Capacity_kMT]>=(0.0)),
    CONSTRAINT [CR_PolyFacilities_PrimeResin_Pcnt] CHECK ([PrimeResin_Pcnt]>=(0.0) AND [PrimeResin_Pcnt]<=(100.0)),
    CONSTRAINT [CR_PolyFacilities_ReactorTrainsCnt] CHECK ([ReactorTrains_Count]>=(0)),
    CONSTRAINT [CR_PolyFacilities_StartUp_Year] CHECK ([StartUp_Year]>=(1950) AND len([StartUp_Year])=(4)),
    CONSTRAINT [CR_PolyFacilities_StreamMTd] CHECK ([Stream_MTd]>=(0.0)),
    CONSTRAINT [FK_PolyFacilities_Assets] FOREIGN KEY ([AssetId]) REFERENCES [cons].[Assets] ([AssetId]),
    CONSTRAINT [FK_PolyFacilities_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PolyFacilities_PolymerFamily_LookUp] FOREIGN KEY ([PolymerFamilyId]) REFERENCES [dim].[PolymerFamily_LookUp] ([PolymerFamilyId]),
    CONSTRAINT [FK_PolyFacilities_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_PolyFacilities_u]
	ON [fact].[PolyFacilities]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[PolyFacilities]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[PolyFacilities].Refnum		= INSERTED.Refnum
		AND [fact].[PolyFacilities].AssetId		= INSERTED.AssetId
		AND [fact].[PolyFacilities].CalDateKey	= INSERTED.CalDateKey;

END;