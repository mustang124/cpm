﻿CREATE TABLE [fact].[ReliabilityTA] (
    [Refnum]                           VARCHAR (25)       NOT NULL,
    [CalDateKey]                       INT                NOT NULL,
    [TrainId]                          INT                NOT NULL,
    [SchedId]                          CHAR (1)           NOT NULL,
    [Interval_Mnths]                   REAL               NOT NULL,
    [DownTime_Hrs]                     REAL               NOT NULL,
    [TurnAround_Date]                  DATE               NULL,
    [CurrencyRpt]                      VARCHAR (4)        NULL,
    [TurnAroundCost_MCur]              REAL               NULL,
    [TurnAround_ManHrs]                REAL               NULL,
    [StreamId]                         VARCHAR (42)       NULL,
    [Capacity_Pcnt]                    REAL               NULL,
	[MiniTA_Bit]						BIT					NULL,
    [_TurnAroundInStudyYear_Bit]       AS                 (CONVERT([bit],case when [TurnAround_Date] IS NOT NULL then case when datepart(year,[TurnAround_Date])=datepart(year,CONVERT([date],CONVERT([varchar](8),[CalDateKey],(0)),(112))) then (1) else (0) end  end,(0))) PERSISTED,
    [_Interval_Years]                  AS                 (CONVERT([real],[calc].[MaxValue]([Interval_Mnths]/(12.0),isnull(datediff(day,[TurnAround_Date],CONVERT([date],CONVERT([varchar](8),[CalDateKey],(0)),(112)))/(365.25),(0))),(1))) PERSISTED,
    [_DownTime_Days]                   AS                 (CONVERT([real],[DownTime_Hrs]/(24.0),(0))) PERSISTED,
    [_Ann_DownTime_Hrs]                AS                 (CONVERT([real],[DownTime_Hrs]/[calc].[MaxValue]([Interval_Mnths]/(12.0),isnull(datediff(day,[TurnAround_Date],CONVERT([date],CONVERT([varchar](8),[CalDateKey],(0)),(112)))/(365.25),(0))),(1))) PERSISTED,
    [_Ann_TurnAroundCost_kCur]         AS                 (CONVERT([real],([TurnAroundCost_MCur]*(1000.0))/[calc].[MaxValue]([Interval_Mnths]/(12.0),isnull(datediff(day,[TurnAround_Date],CONVERT([date],CONVERT([varchar](8),[CalDateKey],(0)),(112)))/(365.25),(0))),(1))) PERSISTED,
    [_Ann_TurnAround_ManHrs]           AS                 (CONVERT([real],[TurnAround_ManHrs]/[calc].[MaxValue]([Interval_Mnths]/(12.0),isnull(datediff(day,[TurnAround_Date],CONVERT([date],CONVERT([varchar](8),[CalDateKey],(0)),(112)))/(365.25),(0))),(1))) PERSISTED,
    [_CostPerManHour_Cur]             AS                 (CONVERT([real],case when [TurnAround_ManHrs]<>(0.0) then ([TurnAroundCost_MCur]/[TurnAround_ManHrs])*(1000000.0)  end,(1))) PERSISTED,
    [_Ann_TurnAroundLoss_Days]         AS                 (CONVERT([real],case when [Interval_Mnths]<>(0.0) then (([Capacity_Pcnt]/(100.0))*([DownTime_Hrs]/(24.0)))/([Interval_Mnths]/(12.0))  end,(1))) PERSISTED,
    [_Ann_TurnAroundLoss_Pcnt]         AS                 (CONVERT([real],(case when [Interval_Mnths]<>(0.0) then (([Capacity_Pcnt]/(100.0))*([DownTime_Hrs]/(24.0)))/([Interval_Mnths]/(12.0))  end/(365.25))*(100.0),(1))) PERSISTED,
    [_Ann_FullPlantEquivalentDT_HrsYr] AS                 (CONVERT([real],(([DownTime_Hrs]/CONVERT([real],[calc].[MaxValue]([Interval_Mnths]/(12.0),isnull(datediff(day,[TurnAround_Date],CONVERT([date],CONVERT([varchar](8),[CalDateKey],(0)),(112)))/(365.25),(0))),(1)))*[Capacity_Pcnt])/(100.0),(0))) PERSISTED,
    [tsModified]                       DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityTA_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]                   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityTA_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]                   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityTA_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]                    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityTA_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityTA] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TrainId] ASC, [SchedId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ReliabilityTA_Capacity_Pcnt] CHECK ([Capacity_Pcnt]>=(0.0) AND [Capacity_Pcnt]<=(100.0)),
    CONSTRAINT [CR_ReliabilityTA_DownTime_Hrs] CHECK ([DownTime_Hrs]>=(0.0)),
    CONSTRAINT [CR_ReliabilityTA_Interval_Mnths] CHECK ([Interval_Mnths]>=(0.0)),
    CONSTRAINT [CR_ReliabilityTA_TrainId] CHECK ([TrainId]>(0)),
    CONSTRAINT [CR_ReliabilityTA_TurnAround_Date] CHECK (datepart(year,[TurnAround_Date])>=(1965)),
    CONSTRAINT [CR_ReliabilityTA_TurnAround_ManHrs] CHECK ([TurnAround_ManHrs]>=(0.0)),
    CONSTRAINT [CR_ReliabilityTA_TurnAroundCost_MCur] CHECK ([TurnAroundCost_MCur]>=(0.0)),
    CONSTRAINT [FK_ReliabilityTA_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityTA_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_ReliabilityTA_Schedule_LookUp] FOREIGN KEY ([SchedId]) REFERENCES [dim].[Schedule_LookUp] ([ScheduleId]),
    CONSTRAINT [FK_ReliabilityTA_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReliabilityTA_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityTA_u]
	ON [fact].[ReliabilityTA]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityTA]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityTA].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityTA].TrainId	= INSERTED.TrainId
		AND [fact].[ReliabilityTA].SchedId	= INSERTED.SchedId
		AND [fact].[ReliabilityTA].CalDateKey	= INSERTED.CalDateKey;

END;