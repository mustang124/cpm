﻿CREATE TABLE [fact].[OpEx] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_OpEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_OpEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_OpEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_OpEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_OpEx] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_OpEx_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_OpEx_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_OpEx_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_OpEx_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_OpEx_u]
	ON [fact].[OpEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[OpEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[OpEx].Refnum		= INSERTED.Refnum
		AND [fact].[OpEx].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[OpEx].AccountId		= INSERTED.AccountId
		AND [fact].[OpEx].CalDateKey	= INSERTED.CalDateKey;

END;