﻿CREATE TABLE [fact].[CapacityGrowth] (
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
	[StudyYear]			SMALLINT			NOT NULL,
    [StreamId]             VARCHAR (42)       NOT NULL,
    [Prod_kMT]             REAL               NULL,
    [Capacity_kMT]         REAL               NULL,
    [_Utilization_Pcnt]    AS                 (case when [Capacity_kMT]<>(0.0) then ([Prod_kMT]/[Capacity_kMT])*(100.0)  end) PERSISTED,
    [_StudyYearDifference] AS                 CONVERT(SMALLINT, ([StudyYear] - CONVERT(SMALLINT, LEFT(CONVERT(VARCHAR(8),[CalDateKey]), 4)))) PERSISTED NOT NULL,
    [_FutureValues_Bit]    AS                 CONVERT(BIT, (CASE WHEN [StudyYear] < CONVERT(SMALLINT, LEFT(CONVERT(VARCHAR(8),[CalDateKey]), 4)) THEN 1 ELSE 0 END)) PERSISTED NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityGrowth_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_CapacityGrowth_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_CapacityGrowth_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_CapacityGrowth_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_CapacityGrowth] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_CapacityGrowth_Capacity_kMTt] CHECK ([Capacity_kMT]>=(0.0)),
    CONSTRAINT [CR_CapacityGrowth_Prod_kMT] CHECK ([Prod_kMT]>=(0.0)),
    CONSTRAINT [FK_CapacityGrowth_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_CapacityGrowth_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_CapacityGrowth_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_CapacityGrowth_u]
	ON [fact].[CapacityGrowth]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[CapacityGrowth]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[CapacityGrowth].Refnum		= INSERTED.Refnum
		AND [fact].[CapacityGrowth].StreamId	= INSERTED.StreamId
		AND [fact].[CapacityGrowth].CalDateKey	= INSERTED.CalDateKey;

END;
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = 'Flag identifies estimated future values using Refnum and CalDateKey YYYY segments.', @level0type = N'SCHEMA', @level0name = N'fact', @level1type = N'TABLE', @level1name = N'CapacityGrowth', @level2type = N'COLUMN', @level2name = N'_FutureValues_Bit';

