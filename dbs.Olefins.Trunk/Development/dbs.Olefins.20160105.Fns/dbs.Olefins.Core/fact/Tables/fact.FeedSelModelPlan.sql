﻿CREATE TABLE [fact].[FeedSelModelPlan] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [PlanId]          VARCHAR (42)       NOT NULL,
    [Daily_Bit]       BIT                NULL,
    [BiWeekly_Bit]    BIT                NULL,
    [Weekly_Bit]      BIT                NULL,
    [BiMonthly_Bit]   BIT                NULL,
    [Monthly_Bit]     BIT                NULL,
    [LessMonthly_Bit] BIT                NULL,
    [_Monitored_Bit]  AS                 (CONVERT([bit],((((isnull([Daily_Bit],(0))|isnull([BiWeekly_Bit],(0)))|isnull([Weekly_Bit],(0)))|isnull([BiMonthly_Bit],(0)))|isnull([Monthly_Bit],(0)))|isnull([LessMonthly_Bit],(0)),(0))) PERSISTED,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_FeedSelModelPlan_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelPlan_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelPlan_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_FeedSelModelPlan_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FeedSelModelPlan] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PlanId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_FeedSelModelPlan_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FeedSelModelPlan_FeedSelPlan_LookUp] FOREIGN KEY ([PlanId]) REFERENCES [dim].[FeedSelPlan_LookUp] ([PlanId]),
    CONSTRAINT [FK_FeedSelModelPlan_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FeedSelModelPlan_u]
	ON [fact].[FeedSelModelPlan]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FeedSelModelPlan]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FeedSelModelPlan].Refnum		= INSERTED.Refnum
		AND [fact].[FeedSelModelPlan].PlanId		= INSERTED.PlanId
		AND [fact].[FeedSelModelPlan].CalDateKey	= INSERTED.CalDateKey;

END;