﻿CREATE TABLE [fact].[ReliabilityOppLoss] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [OppLossId]         VARCHAR (42)       NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [DownTimeLoss_MT]   REAL               NULL,
    [SlowDownLoss_MT]   REAL               NULL,
    [_TotLoss_MT]       AS                 (CONVERT([real],isnull([DownTimeLoss_MT],(0.0))+isnull([SlowDownLoss_MT],(0.0)),(1))) PERSISTED NOT NULL,
    [_DownTimeLoss_kMT] AS                 (CONVERT([real],[DownTimeLoss_MT]/(1000.0),(1))) PERSISTED,
    [_SlowDownLoss_kMT] AS                 (CONVERT([real],[SlowDownLoss_MT]/(1000.0),(1))) PERSISTED,
    [_TotLoss_kMT]      AS                 (CONVERT([real],(isnull([DownTimeLoss_MT],(0.0))+isnull([SlowDownLoss_MT],(0.0)))/(1000.0),(1))) PERSISTED NOT NULL,
    [CurrentYear]       AS                 (case when left([Refnum],(4))=left([CalDateKey],(4)) then (1) else (0) end) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityOppLoss_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOppLoss_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityOppLoss] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC, [OppLossId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReliabilityOppLoss_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityOppLoss_ReliabilityOppLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOppLoss_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReliabilityOppLoss_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);
GO
CREATE NONCLUSTERED INDEX [IX_ReliabilityOppLoss_Calc]
    ON [fact].[ReliabilityOppLoss]([OppLossId] ASC, [StreamId] ASC)
    INCLUDE([Refnum], [CalDateKey], [_TotLoss_kMT]);

GO
CREATE NONCLUSTERED INDEX [IX_ReliabilityOppLoss_ReliabilityProductLost]
    ON [fact].[ReliabilityOppLoss]([OppLossId] ASC, [StreamId] ASC)
    INCLUDE([Refnum], [CalDateKey], [SlowDownLoss_MT]);

GO
CREATE TRIGGER [fact].[t_ReliabilityOppLoss_u]
	ON [fact].[ReliabilityOppLoss]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityOppLoss]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityOppLoss].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityOppLoss].StreamId	= INSERTED.StreamId
		AND [fact].[ReliabilityOppLoss].OppLossId	= INSERTED.OppLossId
		AND [fact].[ReliabilityOppLoss].CalDateKey	= INSERTED.CalDateKey;

END;