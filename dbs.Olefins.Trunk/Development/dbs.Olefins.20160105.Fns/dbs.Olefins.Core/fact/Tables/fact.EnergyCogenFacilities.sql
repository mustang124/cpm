﻿CREATE TABLE [fact].[EnergyCogenFacilities] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [FacilityId]      VARCHAR (42)       NOT NULL,
    [Unit_Count]      TINYINT            NULL,
    [Capacity_MW]     REAL               NULL,
    [Efficiency_Pcnt] REAL               NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyCogenFacilities_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenFacilities_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenFacilities_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_EnergyCogenFacilities_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyCogenFacilities] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyCogenFacilities_Capacity_MW] CHECK ([Capacity_MW]>=(0.0)),
    CONSTRAINT [CR_EnergyCogenFacilities_Efficiency_Pcnt] CHECK ([Efficiency_Pcnt]>=(0.0)),
    CONSTRAINT [CR_EnergyCogenFacilities_Unit_Count] CHECK ([Unit_Count]>=(0.0)),
    CONSTRAINT [FK_EnergyCogenFacilities_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyCogenFacilities_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_EnergyCogenFacilities_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_EnergyCogenFacilities_u]
	ON [fact].[EnergyCogenFacilities]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[EnergyCogenFacilities]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[EnergyCogenFacilities].Refnum		= INSERTED.Refnum
		AND [fact].[EnergyCogenFacilities].FacilityId	= INSERTED.FacilityId
		AND [fact].[EnergyCogenFacilities].CalDateKey	= INSERTED.CalDateKey;

END;