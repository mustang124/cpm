﻿CREATE TABLE [fact].[FacilitiesCompressors] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [FacilityId]     VARCHAR (42)       NOT NULL,
    [Age_Years]      REAL               NULL,
    [Power_BHP]      REAL               NULL,
    [CoatingId]      VARCHAR (42)       NULL,
    [DriverId]       VARCHAR (42)       NULL,
    [Stages_Count]   INT                NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesCompressors_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FacilitiesCompressors_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FacilitiesCompressors_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FacilitiesCompressors_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FacilitiesCompressors] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [Cr_FacilitiesCompressors_Age_Years] CHECK ([Age_Years]>=(0.0)),
    CONSTRAINT [Cr_FacilitiesCompressors_Power_BHP] CHECK ([Power_BHP]>=(0.0)),
    CONSTRAINT [Cr_FacilitiesCompressors_Stages_Count] CHECK ([Stages_Count]>=(0.0)),
    CONSTRAINT [FK_FacilitiesCompressors_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FacilitiesCompressors_Facilities] FOREIGN KEY ([Refnum], [FacilityId], [CalDateKey]) REFERENCES [fact].[Facilities] ([Refnum], [FacilityId], [CalDateKey]),
    CONSTRAINT [FK_FacilitiesCompressors_FacilitiesCoating_LookUp] FOREIGN KEY ([CoatingId]) REFERENCES [dim].[FacilityCoating_LookUp] ([CoatingId]),
    CONSTRAINT [FK_FacilitiesCompressors_FacilitiesDriver_LookUp] FOREIGN KEY ([DriverId]) REFERENCES [dim].[FacilityDriver_LookUp] ([DriverId]),
    CONSTRAINT [FK_FacilitiesCompressors_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesCompressors_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FacilitiesCompressors_u]
	ON [fact].[FacilitiesCompressors]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesCompressors]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FacilitiesCompressors].Refnum		= INSERTED.Refnum
		AND [fact].[FacilitiesCompressors].FacilityId	= INSERTED.FacilityId
		AND [fact].[FacilitiesCompressors].CalDateKey	= INSERTED.CalDateKey;

END;