﻿CREATE TABLE [fact].[GenPlantEnergy] (
    [Refnum]                   VARCHAR (25)       NOT NULL,
    [CalDateKey]               INT                NOT NULL,
    [CoolingWater_MTd]         REAL               NULL,
    [CoolingWaterSupplyTemp_C] REAL               NULL,
    [CoolingWaterReturnTemp_C] REAL               NULL,
    [FinFanHeatAbsorb_kBTU]    REAL               NULL,
    [CompGasEfficiency_Pcnt]   REAL               NULL,
    [CalcTypeId]               VARCHAR (7)        NULL,
    [SteamExtratRate_kMTd]     REAL               NULL,
    [SteamCondenseRate_kMTd]   REAL               NULL,
    [SurfaceCondVacuum_InH20]  REAL               NULL,
    [ExchangerHeatDuty_BTU]    REAL               NULL,
    [QuenchHeatMatl_Pcnt]      REAL               NULL,
    [QuenchDilution_Pcnt]      REAL               NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantEnergy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnergy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnergy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_GenPlantEnergy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantEnergy] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantEnergy_CompGasEfficiency_Pcnt] CHECK ([CompGasEfficiency_Pcnt]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_CoolingWater_MTd] CHECK ([CoolingWater_MTd]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_CoolingWaterReturnTemp_C] CHECK ([CoolingWaterReturnTemp_C]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_CoolingWaterSupplyTemp_C] CHECK ([CoolingWaterSupplyTemp_C]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_ExchangerHeatDuty_BTU] CHECK ([ExchangerHeatDuty_BTU]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_FinFanHeatAbsorb_kBTU] CHECK ([FinFanHeatAbsorb_kBTU]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_QuenchDilution_Pcnt] CHECK ([QuenchDilution_Pcnt]>=(0.0) AND [QuenchDilution_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantEnergy_QuenchHeatMatl_Pcnt] CHECK ([QuenchHeatMatl_Pcnt]>=(0.0) AND [QuenchHeatMatl_Pcnt]<=(100.0)),
    CONSTRAINT [CR_GenPlantEnergy_SteamCondenseRate_kMTd] CHECK ([SteamCondenseRate_kMTd]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_SteamExtratRate_kMTd] CHECK ([SteamExtratRate_kMTd]>=(0.0)),
    CONSTRAINT [CR_GenPlantEnergy_SurfaceCondVacuum_InH20] CHECK ([SurfaceCondVacuum_InH20]>=(0.0)),
    CONSTRAINT [FK_GenPlantEnergy_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantEnergy_CompGasEfficiencyDetail] FOREIGN KEY ([CalcTypeId]) REFERENCES [dim].[FacilityCompGasCalc_LookUp] ([CalcTypeId]),
    CONSTRAINT [FK_GenPlantEnergy_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantEnergy_u]
	ON [fact].[GenPlantEnergy]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[GenPlantEnergy]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantEnergy].Refnum		= INSERTED.Refnum
		AND [fact].[GenPlantEnergy].CalDateKey	= INSERTED.CalDateKey;

END;