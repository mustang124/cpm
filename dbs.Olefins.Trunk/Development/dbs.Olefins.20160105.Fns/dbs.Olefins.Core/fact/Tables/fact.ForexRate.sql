﻿CREATE TABLE [fact].[ForexRate] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CurrencyFrom]   VARCHAR (4)        NULL,
    [CurrencyTo]     VARCHAR (4)        NOT NULL,
    [ForexRate]      FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ForexRate_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ForexRate_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ForexRate_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ForexRate_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ForexRate] PRIMARY KEY CLUSTERED ([Refnum] ASC),
    CONSTRAINT [CK_ForexRate_ForexRate_Min_0] CHECK ([ForexRate]>=(0.0)),
    CONSTRAINT [FK_ForexRate_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ForexRate_Currency_LookUp_From] FOREIGN KEY ([CurrencyFrom]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_ForexRate_Currency_LookUp_To] FOREIGN KEY ([CurrencyTo]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_ForexRate_TSortSolomon] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ForexRate_u]
	ON [fact].[ForexRate]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ForexRate]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ForexRate].Refnum		= INSERTED.Refnum;

END;