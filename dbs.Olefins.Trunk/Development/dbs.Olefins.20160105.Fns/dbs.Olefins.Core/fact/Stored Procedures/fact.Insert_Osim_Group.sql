﻿CREATE PROCEDURE [fact].[Insert_Osim_Group]
(
	@GroupId	VARCHAR(25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [fact].[TSortClient]([Refnum], [CalDateKey], [PlantAssetName], [PlantCompanyName], [UomId], [CurrencyFcn], [CurrencyRpt])
		SELECT
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[AssetId],
			fpl.[CompanyId],
			fpl.[UomId],
			fpl.[CurrencyFcn],
			fpl.[CurrencyRpt]
		FROM @fpl				 fpl
		WHERE	fpl.[CalQtr]				= 4;

		INSERT INTO [fact].[Facilities]
		(
			[Refnum],
			[CalDateKey],
			[FacilityId],
			[Unit_Count]
		)
		SELECT
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			f.[FacilityId],
			CEILING(AVG(f.[Unit_Count]))
		FROM @fpl												fpl
		INNER JOIN [reports].[GroupPlants]						gp
			ON gp.[GroupId]				= fpl.[AssetId]
		INNER JOIN [fact].[Facilities]						f
			ON	f.[Refnum]				= gp.[Refnum]
			AND	f.[FacilityId]			= 'TowerPyroGasHT'
		WHERE	fpl.[CalQtr]			= 4
		GROUP BY
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			f.[FacilityId];

		INSERT INTO [fact].[Quantity]([Refnum], [CalDateKey], [StreamId], [StreamDescription], [Quantity_kMT])
		SELECT
			t.[GroupId],
			t.[Plant_QtrDateKey],
			t.[DescendantId],
			[StreamDescription] = CASE
			WHEN t.[DescendantId] = 'FeedLtOther'	THEN 'Other Mixed Light'
			WHEN t.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
			WHEN t.[DescendantId] = 'ProdOther'		AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
			WHEN t.[DescendantId] = 'SuppOther'		THEN 'Other Supplemental'
			ELSE RTRIM(LTRIM(t.[DescendantId]))
			END,
			[Quantity_kMT] = SUM(q.[Quantity_kMT]) / t.[PlantCount]
		FROM (
			SELECT
				[GroupId] = fpl.[Refnum],
				gp.[Refnum],
				fpl.[Plant_QtrDateKey],
				ls.[DescendantId],
				[PlantCount] = COUNT(gp.[Refnum]) OVER(PARTITION BY fpl.[Refnum], fpl.[Plant_QtrDateKey], ls.[DescendantId])
			FROM @fpl								fpl
			INNER JOIN [reports].[GroupPlants]		gp
				ON gp.[GroupId]			= fpl.[AssetId]
			INNER JOIN [dim].[Stream_Bridge]		ls
				ON	ls.[FactorSetId]	= fpl.[FactorSetId]
				AND	ls.[StreamId]		= 'FeedProdLoss'
			) t
		LEFT OUTER JOIN [fact].[Quantity]			q
			ON	q.[Refnum]			= t.[Refnum]
			AND	q.[StreamId]		= t.[DescendantId]
			AND	q.[CalDateKey]		= t.[Plant_QtrDateKey]
		GROUP BY
			t.[GroupId],
			t.[Plant_QtrDateKey],
			t.[DescendantId],
			CASE
			WHEN t.[DescendantId] = 'FeedLtOther'	THEN 'Other Mixed Light'
			WHEN t.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
			WHEN t.[DescendantId] = 'ProdOther'		AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
			WHEN t.[DescendantId] = 'SuppOther'		THEN 'Other Supplemental'
			ELSE RTRIM(LTRIM(t.[DescendantId]))
			END,
			t.[PlantCount]
		HAVING	SUM(q.[Quantity_kMT]) IS NOT NULL;

		INSERT INTO [fact].[CompositionQuantity]([Refnum], [CalDateKey], [StreamId], [StreamDescription], [ComponentId], [Component_WtPcnt])
		SELECT
			t.[GroupId],
			[CalDateKey] = t.[Plant_AnnDateKey],
			t.[StreamId],
			t.[StreamDescription],
			c.[ComponentId],

			[Component_WtPcnt] = SUM(c.[Component_WtPcnt] * t.[Quantity_kMT]) / t.[TotQuantity_kMT]
		FROM (
			SELECT
				[GroupId] = fpl.[Refnum],
				fpl.[Plant_AnnDateKey],
				fpl.[FactorSetId],
				[StreamId] = ls.[DescendantId],
				q.[Refnum],
				[StreamDescription] = CASE
					WHEN ls.[DescendantId] = 'FeedLtOther'	THEN 'Other Mixed Light'
					WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
					WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
					WHEN ls.[DescendantId] = 'SuppOther'	THEN 'Other Supplemental'
					ELSE ls.[DescendantId]
					END,
				[Quantity_kMT] = SUM(q.[Quantity_kMT]),
				[TotQuantity_kMT] = SUM(SUM(q.[Quantity_kMT])) OVER(PARTITION BY fpl.[Refnum], fpl.[Plant_AnnDateKey], ls.[DescendantId])
			FROM @fpl									fpl
			INNER JOIN [reports].[GroupPlants]			gp
				ON gp.[GroupId]				= fpl.[AssetId]
			INNER JOIN [dim].[Stream_Bridge]			ls
				ON	ls.[FactorSetId]		= fpl.[FactorSetId]
				AND	ls.[StreamId]			= 'FeedProdLoss'
			INNER JOIN [fact].[Quantity]				q
				ON	q.[Refnum]				= gp.[Refnum]
				AND	q.[StreamId]			= ls.[DescendantId]
				AND	q.[CalDateKey]			= fpl.[Plant_QtrDateKey]
			GROUP BY
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey],
				fpl.[FactorSetId],
				ls.[DescendantId],
				q.[Refnum],
				q.[StreamId],
				CASE
					WHEN ls.[DescendantId] = 'FeedLtOther'	THEN 'Other Mixed Light'
					WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
					WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '	+ LEFT(RIGHT(RTRIM(LTRIM(q.[StreamDescription])), 2), 1) + ')'
					WHEN ls.[DescendantId] = 'SuppOther'	THEN 'Other Supplemental'
					ELSE ls.[DescendantId]
					END
			HAVING SUM(q.[Quantity_kMT]) <> 0.0
			) t
		INNER JOIN [dim].[Component_Bridge]				lc
			ON	lc.[FactorSetId]		= t.[FactorSetId]
			AND	lc.[ComponentId]		= 'Tot'
		INNER JOIN [fact].[CompositionQuantity]			c
			ON	c.[Refnum]				= t.[Refnum]
			AND	c.[StreamId]			= t.[StreamId]
			AND	CASE
					WHEN c.[StreamId] = 'FeedLtOther'	THEN 'Other Mixed Light'
					WHEN c.[StreamId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(c.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(RTRIM(LTRIM(c.[StreamDescription])), 2), 1) + ')'
					WHEN c.[StreamId] = 'ProdOther'		AND ISNUMERIC(LEFT(RIGHT(RTRIM(LTRIM(c.[StreamDescription])), 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '	+ LEFT(RIGHT(RTRIM(LTRIM(c.[StreamDescription])), 2), 1) + ')'
					WHEN c.[StreamId] = 'SuppOther'		THEN 'Other Supplemental'
					ELSE c.[StreamId]
					END	= t.[StreamDescription]
			AND	c.[CalDateKey]			= t.[Plant_AnnDateKey]
			AND	c.[ComponentId]			= lc.[DescendantId]
		GROUP BY
			t.[GroupId],
			t.[Plant_AnnDateKey],
			t.[StreamId],
			t.[StreamDescription],
			c.[ComponentId],
			t.[TotQuantity_kMT];

		INSERT INTO [fact].[FeedStockCrackingParameters](
			[Refnum],
			[CalDateKey],
			[StreamId],
			[StreamDescription],
			[CoilOutletPressure_Psia],
			[CoilOutletTemp_C],
			[CoilInletPressure_Psia],
			[CoilInletTemp_C],
			[RadiantWallTemp_C],
			[SteamHydrocarbon_Ratio],
			[EthyleneYield_WtPcnt],
			[PropyleneEthylene_Ratio],
			[PropyleneMethane_Ratio],
			[FeedConv_WtPcnt],
			[SulfurContent_ppm],
			[Density_SG],
			[FurnConstruction_Year],
			[ResidenceTime_s],
			[FlowRate_KgHr],
			[Recycle_Bit]
		)
		SELECT
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId],
			CASE
			WHEN ls.[DescendantId] = 'FeedLtOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Light'
			WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '  + LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			ELSE ls.[DescendantId]
			END	[StreamDescription],

			SUM(sqa.[Quantity_kMT] * a.[CoilOutletPressure_Psia])	/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[CoilOutletTemp_C])			/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[CoilInletPressure_Psia])	/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[CoilInletTemp_C])			/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[RadiantWallTemp_C])			/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[SteamHydrocarbon_Ratio])	/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[EthyleneYield_WtPcnt])		/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[PropyleneEthylene_Ratio])	/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[PropyleneMethane_Ratio])	/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[FeedConv_WtPcnt])			/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[SulfurContent_ppm])			/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[Density_SG])				/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[FurnConstruction_Year])		/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[ResidenceTime_s])			/ SUM(sqa.[Quantity_kMT]),
			SUM(sqa.[Quantity_kMT] * a.[FlowRate_KgHr])				/ SUM(sqa.[Quantity_kMT]),

			a.[Recycle_Bit]

		FROM @fpl												fpl
		INNER JOIN [reports].[GroupPlants]						gp
			ON gp.[GroupId]				= fpl.[AssetId]
		INNER JOIN [dim].[Stream_Bridge]						ls
			ON	ls.[FactorSetId]		= fpl.[FactorSetId]
			AND	ls.[StreamId]			= 'FreshPyroFeed'
		INNER JOIN [fact].[StreamQuantityAggregate]				sqa	WITH (NOEXPAND)
			ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
			AND	sqa.[Refnum]			= gp.[Refnum]
			AND	sqa.[StreamId]			= ls.[DescendantId]
		INNEr JOIN [fact].[FeedStockCrackingParameters]			a
			ON	a.[Refnum]				= gp.[Refnum]
			AND	a.[StreamId]			= sqa.[StreamId]
			AND	a.[StreamDescription]	= COALESCE(sqa.[StreamDescription], a.[StreamDescription])
		WHERE	fpl.[CalQtr]			= 4
		GROUP BY
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId],
			CASE
			WHEN ls.[DescendantId] = 'FeedLtOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Light'
			WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '  + LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			ELSE ls.[DescendantId]
			END,
			a.[Recycle_Bit];

		INSERT INTO [fact].[FeedStockDistillation](
			[Refnum],
			[CalDateKey],
			[StreamId],
			[StreamDescription],
			[DistillationId],
			[Temp_C]
		)
		SELECT
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId],
			CASE
			WHEN ls.[DescendantId] = 'FeedLtOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Light'
			WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '  + LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			ELSE ls.[DescendantId]
			END	[StreamDescription],
			a.[DistillationId],
			SUM(sqa.[Quantity_kMT] * a.[Temp_C])	/ SUM(sqa.[Quantity_kMT])

		FROM @fpl												fpl
		INNER JOIN [reports].[GroupPlants]						gp
			ON gp.[GroupId]				= fpl.[AssetId]
		INNER JOIN [dim].[Stream_Bridge]						ls
			ON	ls.[FactorSetId]		= fpl.[FactorSetId]
			AND	ls.[StreamId]			= 'Liquid'
		INNER JOIN [fact].[StreamQuantityAggregate]				sqa	WITH (NOEXPAND)
			ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
			AND	sqa.[Refnum]			= gp.[Refnum]
			AND	sqa.[StreamId]			= ls.[DescendantId]
		INNER JOIN [fact].[FeedStockDistillation]				a
			ON	a.[Refnum]				= gp.[Refnum]
			AND	a.[StreamId]			= sqa.[StreamId]
			AND	a.[StreamDescription]	= COALESCE(sqa.[StreamDescription], a.[StreamDescription])
		WHERE	fpl.[CalQtr]			= 4
		GROUP BY
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId],
			CASE
			WHEN ls.[DescendantId] = 'FeedLtOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Light'
			WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '  + LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			ELSE ls.[DescendantId]
			END,
			a.[DistillationId];

		INSERT INTO [fact].[QuantityValue]
		(
			[Refnum],
			[CalDateKey],
			[StreamId],
			[StreamDescription],
			[CurrencyRpt],
			[Amount_Cur]
		)
		SELECT
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId],
			CASE
			WHEN ls.[DescendantId] = 'FeedLtOther'	THEN 'Other Mixed Light'
			WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '  + LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'SuppOther'	THEN 'Other Supplemental'
			ELSE ls.[DescendantId]
			END	[StreamDescription],
			fpl.[CurrencyRpt],

			SUM(sqa.[Quantity_kMT] * v.[Amount_Cur])	/ SUM(sqa.[Quantity_kMT])

		FROM @fpl												fpl
		INNER JOIN [reports].[GroupPlants]						gp
			ON gp.[GroupId]				= fpl.[AssetId]
		INNER JOIN [dim].[Stream_Bridge]						ls
			ON	ls.[FactorSetId]		= fpl.[FactorSetId]
			AND	ls.[StreamId]			= 'FeedProdLoss'
		INNER JOIN [fact].[StreamQuantityAggregate]				sqa	WITH (NOEXPAND)
			ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
			AND	sqa.[Refnum]			= gp.[Refnum]
			AND	sqa.[StreamId]			= ls.[DescendantId]
		INNER JOIN [fact].[QuantityValue]						v
			ON	v.[Refnum]				= gp.[Refnum]
			AND	v.[StreamId]			= sqa.[StreamId]
			AND	v.[StreamDescription]	= COALESCE(sqa.[StreamDescription], v.[StreamDescription])
			AND	v.[CurrencyRpt]	= fpl.[CurrencyRpt]
		WHERE	fpl.[CalQtr]			= 4
		GROUP BY
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId],
			CASE
			WHEN ls.[DescendantId] = 'FeedLtOther'	THEN 'Other Mixed Light'
			WHEN ls.[DescendantId] = 'FeedLiqOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Liquid (Other Feed '	+ LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'ProdOther'	AND ISNUMERIC(LEFT(RIGHT(sqa.[StreamDescription], 2), 1)) = 1 THEN 'Other Mixed Products (Other Prod '  + LEFT(RIGHT(sqa.[StreamDescription], 2), 1) + ')'
			WHEN ls.[DescendantId] = 'SuppOther'	THEN 'Other Supplemental'
			ELSE ls.[DescendantId]
			END,
			fpl.[CurrencyRpt];

		INSERT INTO [fact].[EnergyLHValue]
		(
			[Refnum],
			[CalDateKey],
			[AccountId],
			[LHValue_MBTU]
		)
		SELECT
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId],
			SUM(sqa.[Quantity_kMT] * e.[LHValue_MBTU])	/ SUM(sqa.[Quantity_kMT])
		FROM @fpl												fpl
		INNER JOIN [reports].[GroupPlants]						gp
			ON gp.[GroupId]				= fpl.[AssetId]
		INNER JOIN [dim].[Account_Bridge]						ls
			ON	ls.[FactorSetId]		= fpl.[FactorSetId]
			AND	ls.[AccountId]			= 'PPFC'
		INNER JOIN [fact].[EnergyLHValue]						e
			ON	e.[Refnum]				= gp.[Refnum]
			AND	e.[AccountId]			= ls.[DescendantId]
		INNER JOIN [fact].[StreamQuantityAggregate]				sqa	WITH (NOEXPAND)
			ON	sqa.[FactorSetId]		= fpl.[FactorSetId]
			AND	sqa.[Refnum]			= gp.[Refnum]
			AND	sqa.[StreamId]			= 'PPFC'
		WHERE	fpl.[CalQtr]			= 4
		GROUP BY
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			ls.[DescendantId];

	END TRY
	BEGIN CATCH

		SET @GroupId = 'G:' + @GroupId;
		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
