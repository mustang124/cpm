﻿CREATE PROCEDURE [fact].[Insert_GenPlantCommIntegration]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantCommIntegration(Refnum, CalDateKey, StreamId, Integration_Pcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.StreamId
			, u.Pcnt * 100.0
		FROM(
			SELECT 
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, AffGasPcnt	[FreshPyroFeed]
				, AffEtyPcnt	[Ethylene]
				, AffPryPcnt	[Propylene]
				, AffOthPcnt	[Prod]
			FROM stgFact.Prac p
			INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
			WHERE t.Refnum = @sRefnum
			) t
			UNPIVOT (
			Pcnt FOR StreamId IN(
				  FreshPyroFeed
				, Ethylene
				, Propylene
				, Prod
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;