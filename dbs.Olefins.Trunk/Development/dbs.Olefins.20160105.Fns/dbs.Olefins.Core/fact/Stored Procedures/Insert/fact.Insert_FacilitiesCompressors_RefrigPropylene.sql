﻿CREATE PROCEDURE [fact].[Insert_FacilitiesCompressors_RefrigPropylene]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesCompressors(Refnum, CalDateKey, FacilityId
			, Age_Years, Power_BHP, CoatingId, DriverId)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'CompRefrigPropylene'
			, f.PropCompAge
			, f.PropCompBHP
			, etl.ConvFacilityCoatingID(f.PropCompCoat)		[CoatingId]
			, etl.ConvFacilityDriverID(f.PropCompDriver)	[DriverId]
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		LEFT OUTER JOIN fact.Facilities x ON x.Refnum = @Refnum
			AND	x.FacilityId = 'CompRefrigPropylene'
		WHERE t.Refnum = @sRefnum
			AND	x.FacilityId IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;