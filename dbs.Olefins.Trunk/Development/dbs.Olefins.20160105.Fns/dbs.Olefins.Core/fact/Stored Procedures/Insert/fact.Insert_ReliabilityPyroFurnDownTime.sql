﻿CREATE PROCEDURE [fact].[Insert_ReliabilityPyroFurnDownTime]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityPyroFurnDownTime(Refnum, CalDateKey, FurnId, OppLossId, DownTime_Days)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.FurnId
			, etl.ConvDownTime(u.OppLossId)
			, u.DownTimeDays
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum]
				, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
				, f.FurnNo													[FurnId]
				, f.DTDecoke
				, f.DTTLEClean
				, f.DTMinor
				, f.DTMajor
				, f.DTStandby
				, CONVERT(REAL, 365)										[PeriodDays]
				, f.TADownDays
				, f.OthDownDays
			FROM  stgFact.FurnRely f
			INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
			WHERE t.Refnum = @sRefnum
			) p
			UNPIVOT(
			DownTimeDays FOR OppLossId IN(
				  DTDecoke
				, DTTLEClean
				, DTMinor
				, DTMajor
				, DTStandby
				, PeriodDays
				, TADownDays
				, OthDownDays
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;