﻿CREATE PROCEDURE [fact].[Insert_ReliabilityDTPhilosophy]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[ReliabilityDTPhilosophy]
		(
			[Refnum],
			[CalDateKey],
			[PhilosophyId]
		)
		SELECT
				[Refnum]		= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
				[CalDateKey]	= [etl].[ConvDateKey]([t].[StudyYear]),
			[p].[CompPhil]
		FROM
			[stgFact].[PracRely]	[p]
		INNER JOIN
			[stgFact].[TSort]		[t]
				ON [t].[Refnum]		= [p].[Refnum]
		WHERE	[p].[CompPhil]		IS NOT NULL
			AND	[t].[Refnum]		= @sRefnum

	END TRY
	BEGIN CATCH

		EXECUTE [dbo].[usp_LogError] 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;