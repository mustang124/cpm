﻿CREATE PROCEDURE [fact].[Insert_GenPlantEnvironment]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantEnvironment(Refnum, CalDateKey, Emissions_MTd, WasteHazMat_MTd, NOx_LbMBtu, WasteWater_MTd)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, e.Emis
			, e.Waste
			, e.NOx
			, e.Water
		FROM stgFact.Environ e
		INNER JOIN stgFact.TSort t ON t.Refnum = e.Refnum
		WHERE	(e.Emis IS NOT NULL
			OR	e.Waste IS NOT NULL
			OR	e.NOx IS NOT NULL
			OR	e.Water IS NOT NULL)
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;