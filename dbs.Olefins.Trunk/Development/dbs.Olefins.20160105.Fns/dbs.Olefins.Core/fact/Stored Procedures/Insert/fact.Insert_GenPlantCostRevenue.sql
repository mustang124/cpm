﻿CREATE PROCEDURE [fact].[Insert_GenPlantCostRevenue]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantCostsRevenue(Refnum, CalDateKey, AccountId, StreamId, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.StreamId [AccountId]
			, u.StreamId
			, 'USD'
			, u.Amount_Cur
		FROM(
			SELECT 
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, p.TotFDCost	[FreshPyroFeed]
				, p.TotProdRev	[Prod]
			FROM stgFact.Prac p
			INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
			WHERE t.Refnum = @sRefnum
			) t
			UNPIVOT (
			Amount_Cur FOR StreamId IN(
				  FreshPyroFeed
				, Prod
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;