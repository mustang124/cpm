﻿CREATE PROCEDURE [fact].[Insert_ReliabilityOppLoss_Availability]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss_Availability (Previous)';
		PRINT @ProcedureDesc;

		INSERT INTO [fact].[ReliabilityOppLoss_Availability]([Refnum], [CalDateKey], [OppLossId], [DownTimeLoss_Pcnt], [SlowDownLoss_Pcnt])
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear - 1)				[CalDateKey],
			pla.CauseId,
			pla.PrevDTLoss * 100.0,
			pla.PrevSDLoss * 100.0
		FROM	stgFact.TSort							t
		INNER JOIN stgFact.ProdLoss_Availability		pla
			ON	pla.Refnum = t.Refnum
		WHERE	t.Refnum = @sRefnum
			AND	(pla.PrevDTLoss > 0.0
			OR	pla.PrevSDLoss > 0.0);

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss_Availability (Current)';
		PRINT @ProcedureDesc;

		INSERT INTO [fact].[ReliabilityOppLoss_Availability]([Refnum], [CalDateKey], [OppLossId], [DownTimeLoss_Pcnt], [SlowDownLoss_Pcnt])
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear)					[CalDateKey],
			pla.CauseId,
			pla.DTLoss * 100.0,
			pla.SDLoss * 100.0
		FROM	stgFact.TSort							t
		INNER JOIN stgFact.ProdLoss_Availability		pla
			ON	pla.Refnum = t.Refnum
		WHERE	t.Refnum = @sRefnum
			AND	(pla.DTLoss > 0.0
			OR	pla.SDLoss > 0.0);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;