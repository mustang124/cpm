﻿CREATE PROCEDURE [fact].[Insert_GenPlantEnergy]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.GenPlantEnergy(
			Refnum,
			CalDateKey,
			CoolingWater_MTd,
			CoolingWaterSupplyTemp_C,
			CoolingWaterReturnTemp_C,
			FinFanHeatAbsorb_kBTU,
			CompGasEfficiency_Pcnt,
			CalcTypeId,
			SteamExtratRate_kMTd,
			SteamCondenseRate_kMTd,
			SurfaceCondVacuum_InH20,
			ExchangerHeatDuty_BTU,
			QuenchHeatMatl_Pcnt,
			QuenchDilution_Pcnt)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum],
			etl.ConvDateKey(t.StudyYear)							[CalDateKey],
			p.CW,
			p.CWSupplyTemp,
			p.CWReturnTemp,
			p.HeatFF,
			p.ProcGasCompr * 100.0,
			etl.ConvFacilitiesCompGasCalcLu(p.ComprEffDesc),
			p.ExtractSteam,
			p.CondSteam,
			p.SurfCondVac,
			p.DutyPPHE,
			p.QuenchOilHM * 100.0,
			CASE WHEN p.QuenchOilDS > 1.0 THEN p.QuenchOilDS ELSE p.QuenchOilDS * 100.0 END
		FROM stgFact.Prac p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;