﻿CREATE PROCEDURE [fact].[Insert_ReliabilityCompEvents]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[ReliabilityCompEvents]
		(
			[Refnum],
			[CalDateKey],
			[EventNo],
			[CompressorId],
			[ServiceLevelId],
			[CompComponentId],
			[EventCauseId],
			[EventOccur_Year],
			[Duration_Hours]
		)
		SELECT
				[Refnum]			= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
				[CalDateKey]		= [etl].[ConvDateKey]([t].[StudyYear]),
			[c].[EventNo],
			[f].[FacilityId],
			[s].[ServiceLevelId],
			[x].[CompComponentId],
			[y].[EventCauseId],
				[EventOccur_Year]	= [c].[EventYear],
				[Duration_Hours]	= [c].[Duration]
		FROM
			[stgFact].[TSort]				[t]
		INNER JOIN
			[stgFact].[Compressors]			[c]
				ON	[c].[Refnum]			= [t].[Refnum]
		INNER JOIN
			[dim].[ReliabilityServiceLevel_LookUp]	[s]
				ON	[s].[ServiceLevelId]	= [c].[EventType]
		LEFT OUTER JOIN(VALUES
			('CG', 'CompGas'),
			('C3', 'CompRefrigPropylene'),
			('C2', 'CompRefrigEthylene'),
			('C1', 'CompRefrigMethane'),
			('C2/C3', 'CompRefrigOlefins'),
			('Other', 'CompOther')
			) [f]([Compressor], [FacilityId])
				ON	[f].[Compressor]	= [c].[Compressor]
		LEFT OUTER JOIN(VALUES
			('COMPR', 'Comp'),
			('', 'Interruption'),
			('CW', 'InterruptionCooling'),
			('ELEC', 'InterruptionElectric'),
			('FG', 'InterruptionFuelGas'),
			('STEAM', 'InterruptionSteam'),
			('EM', 'MotorElectric'),
			('HX', 'Exchanger'),
			('GB', 'GearBox'),
			('INSTR', 'Instr'),
			('LUBE', 'Lube'),
			('OTHER', 'CompOther'),
			('COMP', 'PrimaryComponent'),
			('', 'System'),
			('SCS', 'SystemSpeed'),
			('AUX', 'SystemAux'),
			('', 'Turbine'),
			('GT', 'TurbineGas'),
			('ST', 'TurbineSteam'),
			('SEAL', ''),
			('INST', '')
			) [x]([Component], [CompComponentId])
				ON	[x].[Component]	= [c].[Component]
		LEFT OUTER JOIN(VALUES
			('1', 'CompressorFouling'),
			('2', 'CompressorBearings'),
			('3', 'CompressorSeals'),
			('4', 'CompressorImpeller'),
			('5', 'PistonSeal'),
			('6', 'BearingThrust'),
			('7', 'CasingLeak'),
			('8', 'OverspeedTrip'),
			('9', 'TurbineBlade'),
			('10', 'TurbineBearing'),
			('11', 'ValveGoverner'),
			('12', 'ValveTT'),
			('13', 'SystemSpeed'),
			('14', 'SystemSurge'),
			('15', 'TurbinePower'),
			('16', 'GearBoxShaft'),
			('17', 'GearBoxBearing'),

			('19', 'GearBoxCoupling'),
			('20', 'GlandSeal'),
			('21', 'LubeSeal'),
			('22', 'SteamCondenser'),
			('23', 'Coolers'),
			('24', 'TurbineDisk'),
			('25', 'RV'),
			('26', 'ProcessPiping'),
			('27', 'FalseTripVibration'),
			('28', 'FalseTripOther'),
			('29', 'MotorElectricFault'),
			('30', 'MotorElectricStarter'),
			('31', 'MotorElectricBearing'),
			('32', 'ProcessUpset'),
			('33', 'OtherCause'),

			('', 'Turbine'),
			('', 'Compressor'),
			('', 'FalseTrip'),
			('', 'GearBox'),
			('', 'GearBoxGears'),
			('', 'MotorElectric'),
			('', 'PrimaryCause'),
			('', 'System'),
			('', 'Valve')

			) [y]([Cause], [EventCauseId])
				ON	[y].[Cause]	= [c].[Cause]

		WHERE	[f].[FacilityId]	IS NOT NULL
			AND	[c].[Duration]		IS NOT NULL
			AND	[t].[Refnum]		= @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;