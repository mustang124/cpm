﻿CREATE PROCEDURE [fact].[Insert_ReliabilityPyroFurn]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityPyroFurn(Refnum, CalDateKey, FurnId, StreamId
			, FeedQty_kMT, FeedCap_MTd
			, FuelTypeId, FuelCons_kMT
			, StackOxygen_Pcnt, ArchDraft_H20, StackTemp_C, CrossTemp_C, Retubed_Year, Retubed_Pcnt, RetubeInterval_Mnths)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum]
			, etl.ConvDateKey(t.StudyYear)								[CalDateKey]
			, f.FurnNo
			, l.StreamId
			, f.FeedQtyKMTA
			, f.FeedCapMTD
		
			, etl.ConvStreamID(f.FuelType)
			, f.FuelCons
		
			, f.StackOxy
			, f.ArchDraft
			, f.StackTemp
			, f.CrossTemp
		
			, CASE WHEN LEN(f.YearRetubed) = 4 THEN f.YearRetubed END
			, f.PcntRetubed * 100.0
			, f.RetubeInterval
		FROM stgFact.FurnRely f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		INNER JOIN dim.Stream_LookUp l ON l.StreamId  = etl.ConvStreamID(f.FurnFeed)
		WHERE ISNUMERIC(f.FurnFeed) = 0 AND f.FurnFeed <> '1+4'
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;