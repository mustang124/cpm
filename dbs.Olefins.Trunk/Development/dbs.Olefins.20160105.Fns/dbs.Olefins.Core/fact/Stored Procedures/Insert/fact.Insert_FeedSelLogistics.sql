﻿CREATE PROCEDURE [fact].[Insert_FeedSelLogistics]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSelLogistics(Refnum, CalDateKey, StreamId, FacilityId, Delivery_Pcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.StreamId
			, u.FacilityId
			, u.Pcnt
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, etl.ConvStreamID(CASE s.FeedProdId WHEN 'GasOil' THEN 'LiqHeavy' ELSE s.FeedProdId END)
																		[StreamId]
				, s.PipelinePct * 100.0									[Pipeline]
				, s.TruckPct	* 100.0									[RailTruck]
				, s.BargePct	* 100.0									[Barge]
				, s.ShipPct		* 100.0									[Ship]
			FROM stgFact.FeedSelections s
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = s.Refnum
				AND	t.Refnum = @sRefnum
			) p
			UNPIVOT(
			Pcnt FOR FacilityId IN(
				  [Pipeline]
				, [RailTruck]
				, [Barge]
				, [Ship]
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;