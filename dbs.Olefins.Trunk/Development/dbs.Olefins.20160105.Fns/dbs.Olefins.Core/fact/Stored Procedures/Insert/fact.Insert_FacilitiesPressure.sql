﻿CREATE PROCEDURE [fact].[Insert_FacilitiesPressure]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

	INSERT INTO fact.FacilitiesPressure(Refnum, CalDateKey, FacilityId, Pressure_PSIg, Rate_kLbHr)
	SELECT
		  ISNULL(u0.Refnum, u1.Refnum)								[Refnum]
		
		, ISNULL(u0.CalDateKey, u1.CalDateKey)						[CalDateKey]
		
		, etl.ConvFacilityId(
			ISNULL(LEFT(u0.FacilityId, LEN(u0.FacilityId) - 4)
			, LEFT(u1.FacilityId, LEN(u1.FacilityId) - 4))
			)														[FacilityId]
		
		, u0.PSIG
		, u1.kLbHr

	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			
			, f.DemethTowerPSIG
			, f.DeethTowerPSIG
			, f.EthylFracPSIG
			, f.DepropTowerPSIG
			, f.PropylFracPSIG
			, f.HPBoilPSIG
			, f.LPBoilPSIG
			, f.SteamHeatPSIG
			
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE t.Refnum = @sRefnum
		) p
		UNPIVOT (
			PSIG FOR FacilityId IN(
				  DemethTowerPSIG
				, DeethTowerPSIG
				, EthylFracPSIG
				, DepropTowerPSIG
				, PropylFracPSIG
				, HPBoilPSIG
				, LPBoilPSIG
				, SteamHeatPSIG
				)
			) u0
	FULL OUTER JOIN(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			
			, HPBoilRate
			, LPBoilRate
			, SteamHeatRate
			
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE t.Refnum = @sRefnum
		) p
		UNPIVOT (
			kLbHr FOR FacilityId IN(
				  HPBoilRate
				, LPBoilRate
				, SteamHeatRate
				)
			) u1
		ON	u1.Refnum = u0.Refnum
		AND	LEFT(u1.FacilityId, LEN(u1.FacilityId) - 4) = LEFT(u0.FacilityId, LEN(u0.FacilityId) - 4)
	INNER JOIN fact.Facilities f
		ON	f.Refnum		= ISNULL(u0.Refnum, u1.Refnum)
		AND f.CalDateKey	= ISNULL(u0.CalDateKey, u1.CalDateKey)
		AND f.FacilityId	= etl.ConvFacilityId(
			ISNULL(LEFT(u0.FacilityId, LEN(u0.FacilityId) - 4)
			, LEFT(u1.FacilityId, LEN(u1.FacilityId) - 4)));

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;