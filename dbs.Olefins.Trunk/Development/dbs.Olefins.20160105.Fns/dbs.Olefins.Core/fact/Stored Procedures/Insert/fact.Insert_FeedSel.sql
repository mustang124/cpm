﻿CREATE PROCEDURE [fact].[Insert_FeedSel]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FeedSel(
			Refnum, CalDateKey,
			MinParaffin_Pcnt, MaxVaporPresFRS_RVP,
			YieldPattern_Count, YieldPatternLP_Count, YieldGroup_Count,
			YPS_Lummus, YPS_SPYRO, YPS_SelfDev, YPS_Other, YPS_OtherName, YPS_RPT,
			LP_Aspen,
			LP_DeltaBase_No, LP_DeltaBase_NotSure, LP_DeltaBase_RPT, LP_DeltaBase_Yes,
			LP_Haverly, LP_Honeywell,
			LP_MixInt_No, LP_MixInt_NotSure, LP_MixInt_RPT, LP_MixInt_Yes,
			LP_Online_No, LP_Online_NotSure, LP_Online_RPT, LP_Online_Yes,
			LP_Other, LP_OtherName,
			LP_Recur_No, LP_Recur_NotSure, LP_Recur_RPT, LP_Recur_Yes,
			LP_RPT, LP_SelfDev,
			LP_RowCount,
			BeyondCrack_None, BeyondCrack_C4, BeyondCrack_C5, BeyondCrack_Pygas, BeyondCrack_RPT, BeyondCrack_BTX, BeyondCrack_OthChem, BeyondCrack_OthRefine, BeyondCrack_Oth, BeyondCrack_OtherName,
			DecisionPlant_Bit, DecisionCorp_Bit,
			PlanTime_Days, EffFeedChg_Levels, FeedPurchase_Levels
			)
		SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
	
			, r.MinParaffin		  * 100.0
			, r.MaxVaporPresFRS
	
			, r.YieldPatternCnt
			, r.YieldPatternLPCnt
			, r.YieldGroupCnt
	
			, r.YPS_Lummus
			, r.YPS_SPYRO
			, r.YPS_SelfDev
			, r.YPS_Other
			, r.YPS_OtherDesc
			, r.YPS_RPT
	
			, r.LP_Aspen

			, r.LP_DeltaBase_No
			, r.LP_DeltaBase_NotSure
			, r.LP_DeltaBase_RPT
			, r.LP_DeltaBase_Yes

			, r.LP_Haverly
			, r.LP_Honeywell

			, r.LP_MixInt_No
			, r.LP_MixInt_NotSure
			, r.LP_MixInt_RPT
			, r.LP_MixInt_Yes

			, r.LP_Online_No
			, r.LP_Online_NotSure
			, r.LP_Online_RPT
			, r.LP_Online_Yes

			, r.LP_Other
			, r.LP_OtherDesc

			, r.LP_Recur_No
			, r.LP_Recur_NotSure
			, r.LP_Recur_RPT
			, r.LP_Recur_Yes

			, r.LP_RPT
			, r.LP_SelfDev
	
			, r.LP_RowCnt
	
			, r.BeyondCrack_None
			, r.BeyondCrack_C4
			, r.BeyondCrack_C5
			, r.BeyondCrack_Pygas
			, r.BeyondCrack_RPT
			, r.BeyondCrack_BTX
			, r.BeyondCrack_OthChem
			, r.BeyondCrack_OthRefine
			, r.BeyondCrack_Oth
			, r.BeyondCrack_OthDesc
	
			, etl.ConvBit(r.Plant_Decision)		[Plant_Decision]
			, etl.ConvBit(r.Corp_Decision)		[Corp_Decision]
	
			, r.PlanTimeDays
			, r.EffFeedChg
			, r.FeedPurchase
	
		FROM  stgFact.FeedResources r
		INNER JOIN stgFact.TSort t
			ON t.Refnum = r.Refnum
		WHERE	t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;