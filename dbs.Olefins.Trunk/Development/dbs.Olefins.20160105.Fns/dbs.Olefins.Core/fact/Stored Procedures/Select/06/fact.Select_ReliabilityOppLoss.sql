﻿CREATE PROCEDURE [fact].[Select_ReliabilityOppLoss]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-1
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[OppLossId],
		[f].[StreamId],
		[f].[DownTimeLoss_MT],
		[f].[SlowDownLoss_MT],
			[TotLoss_MT]		= [f].[_TotLoss_MT],
			[DownTimeLoss_kMT]	= [f].[_DownTimeLoss_kMT],
			[SlowDownLoss_kMT]	= [f].[_SlowDownLoss_kMT],
			[TotLoss_kMT]		= [f].[_TotLoss_kMT],
		[f].[CurrentYear],
		[n].[OppLossName]
	FROM
		[fact].[ReliabilityOppLoss]				[f]
	LEFT OUTER JOIN
		[fact].[ReliabilityOppLossNameOther]	[n]
			ON	[n].[Refnum]		= [f].[Refnum]
			AND	[n].[CalDateKey]	= [f].[CalDateKey]
			AND	[n].[StreamId]		= [f].[StreamId]
			AND	[n].[OppLossId]		= [f].[OppLossId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;