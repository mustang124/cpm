﻿CREATE PROCEDURE [fact].[Select_EnergyLHValue]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 7
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[p].[CurrencyRpt],

			[Amount_Met]		= [s].[Amount_kMT],
			[Amount_Imp]		= [$(DbGlobal)].[dbo].[UnitsConv]([s].[Amount_kMT], 'KMT', 'MLB'),

			[Pressure_Met]		= [s].[Pressure_Barg],
			[Pressure_Imp]		= [$(DbGlobal)].[dbo].[UnitsConv]([s].[Pressure_Barg], 'BARG', 'PSIG'),

			[Temp_Met]			= [s].[Temp_C],
			[Temp_Imp]			= [$(DbGlobal)].[dbo].[UnitsConv]([s].[Temp_C], 'TC', 'TF'),

			[LHValue_Met]		= [f].[_LHValue_GJ],
			[LHValue_Imp]		= [f].[LHValue_MBTU],

			[Amount_Cur_Met]	= [p].[Amount_Cur] * 0.94786,
			[Amount_Cur_Imp]	= [p].[Amount_Cur],

		[c].[ComponentId],
		[c].[Component_WtPcnt]
	FROM
		[fact].[EnergyLHValue]		[f]
	LEFT OUTER JOIN
		[fact].[EnergyPrice]		[p]
			ON	[p].[Refnum]		= [f].[Refnum]
			AND	[p].[CalDateKey]	= [f].[CalDateKey]
			AND	[p].[AccountId]		= [f].[AccountId]
	LEFT OUTER JOIN
		[fact].[EnergyComposition]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[CalDateKey]
			AND	[c].[AccountId]		= [f].[AccountId]
	LEFT OUTER JOIN
		[fact].[EnergySteam]		[s]
			ON	[s].[Refnum]		= [f].[Refnum]
			AND	[s].[CalDateKey]	= [f].[CalDateKey]
			AND	[s].[AccountId]		= [f].[AccountId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;