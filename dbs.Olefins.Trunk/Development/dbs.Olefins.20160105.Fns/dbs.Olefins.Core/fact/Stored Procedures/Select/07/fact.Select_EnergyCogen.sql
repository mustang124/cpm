﻿CREATE PROCEDURE [fact].[Select_EnergyCogen]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 7
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[HasCogen_Bit],
		[HasCogen_YN]	= CASE WHEN [f].[HasCogen_Bit] = 1 THEN 'Y' ELSE 'N' END,
		[f].[Thermal_Pcnt]
	FROM
		[fact].[EnergyCogen]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;