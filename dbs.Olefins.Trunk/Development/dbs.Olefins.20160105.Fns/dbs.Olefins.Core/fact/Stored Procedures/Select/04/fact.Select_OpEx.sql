﻿CREATE PROCEDURE [fact].[Select_OpEx]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 4
	DECLARE @FactorSetID	VARCHAR(12) = LEFT(@Refnum, 4);

	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[n].[AccountName],
		[f].[CurrencyRpt],
			[Amount_Cur]	= CASE WHEN ([f].[AccountId] = 'ChemOthProc') THEN [f].[Amount_Cur] + COALESCE([c].[Amount_Cur], 0.0) ELSE [f].[Amount_Cur] END
	FROM
		[fact].[OpExAggregate]			[f]

	LEFT OUTER JOIN
		[fact].[OpExNameOther]			[n]
			ON	[n].[Refnum]			= [f].[Refnum]
			AND	[n].[AccountId]			= [f].[AccountId]

	LEFT OUTER JOIN
		[fact].[OpEx]					[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[AccountId]			= 'ChemCorrection'

	WHERE	[f].[Refnum]				= @Refnum
		AND	[f].[FactorSetId]			= @FactorSetID
		AND	LEFT([f].[CalDateKey], 4)	= @FactorSetID
		AND	[f].[AccountId]				<> 'ChemCorrection';

END;