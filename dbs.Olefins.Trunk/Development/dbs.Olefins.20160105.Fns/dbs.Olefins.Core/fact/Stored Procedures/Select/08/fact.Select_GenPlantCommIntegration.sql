﻿CREATE PROCEDURE [fact].[Select_GenPlantCommIntegration]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[Integration_Pcnt]
	FROM
		[fact].[GenPlantCommIntegration]	[f]
	WHERE
		[f].[Refnum]		= @Refnum

END;