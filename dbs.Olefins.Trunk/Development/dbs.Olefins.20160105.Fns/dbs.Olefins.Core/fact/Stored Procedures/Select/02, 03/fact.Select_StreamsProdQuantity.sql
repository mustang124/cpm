﻿CREATE PROCEDURE [fact].[Select_StreamsProdQuantity]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		--	Table 2A-1, Table 2A-2, Table 2B, Table 2C, Table 3
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[StreamIdInt]		= CASE WHEN [f].[StreamId] IN ('FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										[f].[StreamId] + SUBSTRING([f].[StreamDescription], LEN([f].[StreamDescription]) - 1, 1)
									ELSE
										[f].[StreamId]
									END,
		[f].[StreamDescription],
			[StreamDesc]		= CASE WHEN [f].[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										LEFT([f].[StreamDescription], LEN([f].[StreamDescription]) - CHARINDEX('(', REVERSE([f].[StreamDescription])))
									END,
		[f].[Q1_kMT],
		[f].[Q2_kMT],
		[f].[Q3_kMT],
		[f].[Q4_kMT],

		--	Table 2C
		[r].[Recovered_WtPcnt],

		[Amount_Cur]	= COALESCE([v].[Amount_Cur], [q].[Amount_Cur])

	FROM
		[fact].[QuantityPivot]					[f]
	INNER JOIN
		[dim].[Stream_Bridge]					[b]
			ON	[b].[DescendantId]		= [f].[StreamId]
			AND	[b].[FactorSetId]		= '2013'
			AND	[b].[StreamId]			= 'ProdLoss'
	LEFT OUTER JOIN
		[fact].[QuantitySuppRecovery]			[r]
			ON	[r].[Refnum]			= [f].[Refnum]
			AND	[r].[CalDateKey]		= [f].[CalDateKey]
			AND	[r].[StreamId]			= [f].[StreamId]
			AND	[r].[StreamDescription]	= [f].[StreamDescription]
	LEFT OUTER JOIN
		[fact].[QuantityValue]					[v]
			ON	[v].[Refnum]			= [f].[Refnum]
			AND	[v].[CalDateKey]		= [f].[CalDateKey]
			AND	[v].[StreamId]			= [f].[StreamId]
			AND	[v].[StreamDescription]	= [f].[StreamDescription]
	CROSS APPLY(
		SELECT
			[Amount_Cur] = AVG([q].[Amount_Cur])
		FROM
			[super].[QuantityValue]					[q]
		WHERE	[q].[Refnum]			= [f].[Refnum]
			AND	[q].[StreamId]			= [f].[StreamId]
			AND	[q].[StreamDescription]	= [f].[StreamDescription]
		) [q]
	WHERE
		[f].[Refnum]	= @Refnum;

END;