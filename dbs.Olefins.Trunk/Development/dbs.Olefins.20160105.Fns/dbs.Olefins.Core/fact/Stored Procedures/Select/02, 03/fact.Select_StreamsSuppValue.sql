﻿CREATE PROCEDURE [fact].[Select_StreamsSuppValue]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
			[StreamIdInt]		= CASE WHEN [f].[StreamId] IN ('FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										[f].[StreamId] + SUBSTRING([f].[StreamDescription], LEN([f].[StreamDescription]) - 1, 1)
									ELSE
										[f].[StreamId]
									END,
		[f].[StreamDescription],
			[StreamDesc]		= CASE WHEN [f].[StreamId] IN ('FeedLtOther', 'FeedLiqOther', 'SuppOther', 'ProdOther')
									THEN
										LEFT([f].[StreamDescription], LEN([f].[StreamDescription]) - CHARINDEX('(', REVERSE([f].[StreamDescription])))
									END,

		[Amount_Cur]	= COALESCE([F].[Amount_Cur], [q].[Amount_Cur])

	FROM
		[fact].[QuantityValue]					[f]
	CROSS APPLY(
		SELECT
			[Amount_Cur] = AVG([q].[Amount_Cur])
		FROM
			[super].[QuantityValue]					[q]
		WHERE	[q].[Refnum]			= [f].[Refnum]
			AND	[q].[StreamId]			= [f].[StreamId]
			AND	[q].[StreamDescription]	= [f].[StreamDescription]
		) [q]
	WHERE
		[f].[Refnum]	= @Refnum;

END;