﻿CREATE PROCEDURE [fact].[Select_PolyOpEx]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 12-1 (2)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AssetId],
			[TrainId]		= RIGHT([f].[AssetId], 2),
		[f].[AccountId],
		[f].[CurrencyRpt],
		[f].[Amount_Cur]
	FROM
		[fact].[PolyOpEx]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;