﻿CREATE PROCEDURE [fact].[Select_Maint]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 10-1 (1)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[FacilityId],
		[f].[CurrencyRpt],
		[f].[MaintMaterial_Cur],
		[f].[MaintLabor_Cur],
		[Maint_Cur]		= [f].[_Maint_Cur],

		[f].[TaMaterial_Cur],
		[f].[TaLabor_Cur],
		[TA_Cur]		= [f].[_TA_Cur],

		[Material_Cur]	= [f].[_Material_Cur],
		[Labor_Cur]		= [f].[_Labor_Cur],
		[Tot_Cur]		= [f].[_Tot_Cur]
	FROM
		[fact].[Maint]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;