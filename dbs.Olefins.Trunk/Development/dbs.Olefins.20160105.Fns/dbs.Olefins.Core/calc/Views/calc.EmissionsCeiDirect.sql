﻿CREATE VIEW [calc].[EmissionsCeiDirect]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	p.CalDateKey,
	p.EmissionsCarbon_MTCO2e	[PlantEmissions_MTCO2e],
	s.EmissionsCarbon_MTCO2e	[StdDirectEmissions_MTCO2e],
	p.EmissionsCarbon_MTCO2e / s.EmissionsCarbon_MTCO2e * 100.0	[CeiDirect]
FROM calc.EmissionsCarbonPlantAggregate			p
INNER JOIN calc.EmissionsCarbonDirectAggregate	s
	ON	s.FactorSetId	= p.FactorSetId
	AND	s.Refnum		= p.Refnum
	AND	s.CalDateKey	= p.CalDateKey
WHERE	p.EmissionsId	= 'DirectEmissions'
	AND	s.EmissionsId	= 'TotalEmissions';
