﻿CREATE VIEW calc.CashProductionCostSensitivity
WITH SCHEMABINDING
AS
SELECT
	m.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	m.CurrencyRpt,
	m.MarginAnalysis,
	SUM(m.Amount_Cur)			[Amount_Cur],
	SUM(m.TaAdj_Amount_Cur)		[TaAdj_Amount_Cur]
FROM calc.MarginSensitivity						m
WHERE	m.MarginId IN ('TotCashOpEx', 'PlantFeed')
GROUP BY
	m.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	m.CurrencyRpt,
	m.MarginAnalysis