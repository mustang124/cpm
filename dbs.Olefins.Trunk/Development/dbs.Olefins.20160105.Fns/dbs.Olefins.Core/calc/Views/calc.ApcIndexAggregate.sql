﻿CREATE VIEW calc.ApcIndexAggregate
WITH SCHEMABINDING
AS
SELECT
	a.FactorSetId,
	a.Refnum,
	a.CalDateKey,
	b.ApcId,
	CASE WHEN b.ApcId = b.DescendantId THEN a.OnLine_Pcnt END		[OnLine_Pcnt],
	CASE WHEN b.ApcId = b.DescendantId THEN a.Mpc_Int END			[Mpc_Int],
	CASE WHEN b.ApcId = b.DescendantId THEN a.Sep_Int END			[Sep_Int],
	CASE WHEN b.ApcId = b.DescendantId THEN a.ControllerTypes END	[ControllerTypes],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + a.[Mpc_Value]
		WHEN '-' THEN - a.[Mpc_Value]
		ELSE 0.0
		END)							[Mpc_Value],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + a.[Sep_Value]
		WHEN '-' THEN - a.[Sep_Value]
		ELSE 0.0
		END)							[Sep_Value],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + a.[Apc_Index]
		WHEN '-' THEN - a.[Apc_Index]
		ELSE 0.0
		END)							[Apc_Index],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + a.[ApcOnLine_Index]
		WHEN '-' THEN - a.[ApcOnLine_Index]
		ELSE 0.0
		END)							[ApcOnLine_Index],
	COUNT_BIG(*)						[IndexItems]
FROM  dim.Apc_Bridge				b
INNER JOIN calc.ApcIndex			a
	ON	a.FactorSetId = b.FactorSetId
	AND	a.ApcId = b.DescendantId
GROUP BY
	a.FactorSetId,
	a.Refnum,
	a.CalDateKey,
	b.ApcId,
	CASE WHEN b.ApcId = b.DescendantId THEN a.OnLine_Pcnt END,
	CASE WHEN b.ApcId = b.DescendantId THEN a.Mpc_Int END,
	CASE WHEN b.ApcId = b.DescendantId THEN a.Sep_Int END,
	CASE WHEN b.ApcId = b.DescendantId THEN a.ControllerTypes END;
