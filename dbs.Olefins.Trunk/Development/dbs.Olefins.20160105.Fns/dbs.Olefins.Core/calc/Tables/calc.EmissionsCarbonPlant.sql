﻿CREATE TABLE [calc].[EmissionsCarbonPlant] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [EmissionsId]            VARCHAR (42)       NOT NULL,
    [Quantity_kMT]           REAL               NULL,
    [Quantity_MBtu]          REAL               NULL,
    [CefGwp]                 REAL               NOT NULL,
    [EmissionsCarbon_MTCO2e] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsCarbonPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_EmissionsCarbonPlant] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [EmissionsId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_EmissionsCarbonPlant_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_EmissionsCarbonPlant_Emissions_LookUp] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_calc_EmissionsCarbonPlant_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_EmissionsCarbonPlant_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_EmissionsCarbonPlant_u
	ON  calc.EmissionsCarbonPlant
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.EmissionsCarbonPlant
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.EmissionsCarbonPlant.FactorSetId	= INSERTED.FactorSetId
		AND	calc.EmissionsCarbonPlant.Refnum		= INSERTED.Refnum
		AND calc.EmissionsCarbonPlant.CalDateKey	= INSERTED.CalDateKey
		AND calc.EmissionsCarbonPlant.EmissionsId	= INSERTED.EmissionsId;
	
END