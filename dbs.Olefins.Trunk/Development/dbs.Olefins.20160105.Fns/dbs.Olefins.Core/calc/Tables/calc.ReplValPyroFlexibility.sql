﻿CREATE TABLE [calc].[ReplValPyroFlexibility] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [Actual_ReplValue]  REAL               NOT NULL,
    [Design_ReplValue]  REAL               NOT NULL,
    [Demon_ReplValue]   REAL               NOT NULL,
    [Base_ISBL]         REAL               NOT NULL,
    [Design_ISBL]       REAL               NOT NULL,
    [Demon_ISBL]        REAL               NOT NULL,
    [MaxDesign_ISBL]    REAL               NOT NULL,
    [MaxDemon_ISBL]     REAL               NOT NULL,
    [Capability_ISBL]   REAL               NOT NULL,
    [FlexAdjustment]    REAL               NOT NULL,
    [StartUpCost]       REAL               NOT NULL,
    [SuppRecovery_Pcnt] REAL               NOT NULL,
    [_Recovery_ISBL]    AS                 (CONVERT([real],([Base_ISBL]*[SuppRecovery_Pcnt])/(100.0),(0))) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_ReplValPyroFlexibility_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroFlexibility_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroFlexibility_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_ReplValPyroFlexibility_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ReplValPyroFlexibility] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ReplValPyroFlexibility_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ReplValPyroFlexibility_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ReplValPyroFlexibility_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ReplValPyroFlexibility_u
	ON  calc.ReplValPyroFlexibility
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ReplValPyroFlexibility
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ReplValPyroFlexibility.FactorSetId	= INSERTED.FactorSetId
		AND calc.ReplValPyroFlexibility.Refnum		= INSERTED.Refnum
		AND calc.ReplValPyroFlexibility.CalDateKey	= INSERTED.CalDateKey;
			
END