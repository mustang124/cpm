﻿CREATE TABLE [calc].[MaintExpenseLaborMatl] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MaintExpenseLaborMatl_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MaintExpenseLaborMatl_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MaintExpenseLaborMatl_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MaintExpenseLaborMatl_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MaintExpenseLaborMatl] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MaintExpenseLaborMatl_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_MaintExpenseLaborMatl_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MaintExpenseLaborMatl_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_MaintExpenseLaborMatl_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MaintExpenseLaborMatl_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_MaintExpenseLaborMatl_u
	ON  calc.MaintExpenseLaborMatl
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.MaintExpenseLaborMatl
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MaintExpenseLaborMatl.FactorSetId	= INSERTED.FactorSetId
		AND calc.MaintExpenseLaborMatl.Refnum		= INSERTED.Refnum
		AND calc.MaintExpenseLaborMatl.CalDateKey	= INSERTED.CalDateKey
		AND calc.MaintExpenseLaborMatl.CurrencyRpt	= INSERTED.CurrencyRpt
		AND calc.MaintExpenseLaborMatl.AccountId	= INSERTED.AccountId;
				
END;