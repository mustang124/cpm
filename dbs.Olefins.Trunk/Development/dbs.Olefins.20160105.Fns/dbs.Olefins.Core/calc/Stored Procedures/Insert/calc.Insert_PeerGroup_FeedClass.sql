﻿CREATE PROCEDURE [calc].[Insert_PeerGroup_FeedClass]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @FeedSlate TABLE
		(
			[FactorSetId]				VARCHAR (12)		NOT NULL,
			[Refnum]					VARCHAR (25)		NOT NULL,
			[CalDateKey]				INT					NOT NULL,
			[FeedClass]					TINYINT				NOT	NULL,
			[BasisSlate]				BIT					NOT	NULL,
			[BasisDensity]				BIT					NOT	NULL,

			[_FeedClassSlate]			AS CASE WHEN [BasisSlate]	= 1 THEN [FeedClass] END,
			[_FeedClassDensity]			AS CASE WHEN [BasisDensity]	= 1 THEN [FeedClass] END

			PRIMARY KEY CLUSTERED([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [FeedClass] ASC)
		);

		DECLARE @StreamMap TABLE
		(
			[StreamId]			VARCHAR (42)	NOT	NULL,
			[ComponentId]		VARCHAR (42)	NOT	NULL,
			PRIMARY KEY CLUSTERED([StreamId] ASC, [ComponentId] ASC)
		);

		INSERT INTO @StreamMap(StreamId, ComponentId)
		VALUES
			('Ethane', 'C2H6'),
			('Propane', 'C3H8'),
			('Butane', 'C4H10'),
			('LiqLight', 'LiqLight'),
			('Diesel', 'LiqHeavy'),
			('GasOilHv', 'LiqHeavy');

		INSERT INTO @FeedSlate(FactorSetId, Refnum, CalDateKey, FeedClass, BasisSlate, BasisDensity)
		SELECT
			p.FactorSetId,
			p.Refnum,
			p.Plant_QtrDateKey,
			3					[FeedClass],
			CASE
			WHEN p.FactorSet_AnnDateKey <= 20091231 THEN
				CASE WHEN p.C2H6 >= 10.0 AND p.C3H8 > 0.0 AND p.LiqLight > 10.0 AND p.LiqHeavy > 0.0 THEN 1 ELSE 0 END

			WHEN p.FactorSet_AnnDateKey >= 20110101 THEN
				CASE WHEN p.C2H6 >= 10.0 AND p.C3H8 > 0.0 AND p.LiqLight > 10.0 AND p.LiqHeavy > 5.0 THEN 1 ELSE 0 END

			END					[BasisSlate],
			CASE
			WHEN p.FactorSet_AnnDateKey <= 20091231 THEN
				CASE WHEN pg3.PeerGroup IS NOT NULL AND p.LiqHeavy > 0.0						THEN 1 ELSE 0 END

			WHEN p.FactorSet_AnnDateKey >= 20110101 THEN
				CASE WHEN pg3.PeerGroup IS NOT NULL AND p.LiqHeavy > 5.0 AND p.C2H6 > 10.0	THEN 1 ELSE 0 END

			END					[BasisDensity]
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey,
				fpl.FactorSet_AnnDateKey,
				rve.ComponentId,
				calc.MaxValue(ff.Capability_Pcnt, ISNULL(rve.Component_WtPcnt, 0.0))	[Comparison_Pcnt]
			FROM @fpl									fpl
/**/		INNER JOIN fact.GenPlantFeedFlex			ff
				ON	ff.Refnum		= fpl.Refnum
				AND	ff.CalDateKey	= fpl.Plant_QtrDateKey
			INNER JOIN @StreamMap						sm
				ON	sm.StreamId		= ff.StreamId
			INNER JOIN inter.ReplValEncEstimate			rve
				ON	rve.FactorSetId = fpl.FactorSetId
				AND	rve.Refnum		= fpl.Refnum
				AND	rve.CalDateKey	= fpl.Plant_QtrDateKey
				AND	rve.ComponentId	= sm.ComponentId
			WHERE fpl.CalQtr = 4
			) u
			PIVOT (
				MAX(u.Comparison_Pcnt) FOR u.ComponentId IN (
					C2H6,
					C3H8,
					C4H10,
					LiqLight,
					LiqHeavy
				)
			) p
		INNER JOIN fact.QuantityFeedAttributes			qfa
			ON	qfa.FactorSetId = p.FactorSetId
			AND	qfa.Refnum = p.Refnum
		LEFT OUTER JOIN ante.PeerGroupFeedClass			pg3
			ON	pg3.FactorSetId = p.FactorSetId
			AND	pg3.[LimitLower_SG] <= qfa.Density_SG AND qfa.Density_SG < pg3.[LimitUpper_SG]
			AND	pg3.PeerGroup = 3;

		INSERT INTO @FeedSlate(FactorSetId, Refnum, CalDateKey, FeedClass, BasisSlate, BasisDensity)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			1,

			CASE WHEN fpl.[FactorSet_AnnDateKey] < 20130000
			THEN CASE WHEN rve.Component_WtPcnt > 85.0 AND COALESCE(fs3.BasisSlate, 0) = 0 THEN 1 ELSE 0 END
			ELSE CASE WHEN rve.Component_WtPcnt > 84.0 AND COALESCE(fs3.BasisSlate, 0) = 0 THEN 1 ELSE 0 END
			END			[BasisSlate],

			CASE WHEN pg1.PeerGroup IS NOT NULL THEN 1 ELSE 0 END								[BasisDensity]
		FROM @fpl										fpl
		INNER JOIN inter.ReplValEncEstimate				rve
			ON	rve.FactorSetId = fpl.FactorSetId
			AND	rve.Refnum = fpl.Refnum
			AND	rve.CalDateKey = fpl.Plant_QtrDateKey
			AND	rve.ComponentId = 'C2H6'
		LEFT OUTER JOIN @FeedSlate						fs3
			ON	fs3.FactorSetId = fpl.FactorSetId
			AND	fs3.Refnum = fpl.Refnum
			AND	fs3.CalDateKey = fpl.Plant_QtrDateKey
			AND	fs3.FeedClass = 3
		INNER JOIN fact.QuantityFeedAttributes			qfa
			ON	qfa.FactorSetId = fpl.FactorSetId
			AND	qfa.Refnum = fpl.Refnum
		LEFT OUTER JOIN ante.PeerGroupFeedClass			pg1
			ON	pg1.FactorSetId = fpl.FactorSetId
			AND	pg1.[LimitLower_SG] <= qfa.Density_SG AND qfa.Density_SG < pg1.[LimitUpper_SG]
			AND	pg1.PeerGroup = 1
		WHERE fpl.CalQtr = 4;

		INSERT INTO @FeedSlate(FactorSetId, Refnum, CalDateKey, FeedClass, BasisSlate, BasisDensity)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			2,
			CASE 
			WHEN  fpl.FactorSet_AnnDateKey <= 20091231	THEN
				CASE WHEN SUM(rve.Component_WtPcnt) > 75.0 AND COALESCE(fs1.BasisSlate, 0) = 0 THEN 1 ELSE 0 END
			WHEN  fpl.FactorSet_AnnDateKey >= 20110101	THEN
				CASE WHEN SUM(rve.Component_WtPcnt) > 50.0 AND COALESCE(fs1.BasisSlate, 0) = 0 THEN 1 ELSE 0 END
			END					[BasisSlate],

			CASE
			WHEN  fpl.FactorSet_AnnDateKey <= 20091231	THEN
				CASE WHEN pg2.PeerGroup IS NOT NULL										THEN 1 ELSE 0 END
			WHEN  fpl.FactorSet_AnnDateKey >= 20110101	THEN
				CASE WHEN pg2.PeerGroup IS NOT NULL OR SUM(rve.Component_WtPcnt) > 50.0	THEN 1 ELSE 0 END
			END					[BasisDensity]

		FROM @fpl										fpl
		INNER JOIN inter.ReplValEncEstimate				rve
			ON	rve.FactorSetId = fpl.FactorSetId
			AND	rve.Refnum = fpl.Refnum
			AND	rve.CalDateKey = fpl.Plant_QtrDateKey
			AND	rve.ComponentId IN ('C2H6', 'C3H8', 'C4H10')
		LEFT OUTER JOIN @FeedSlate						fs1
			ON	fs1.FactorSetId = fpl.FactorSetId
			AND	fs1.Refnum = fpl.Refnum
			AND	fs1.CalDateKey = fpl.Plant_QtrDateKey
			AND	fs1.FeedClass = 1
		INNER JOIN fact.QuantityFeedAttributes			qfa
			ON	qfa.FactorSetId = fpl.FactorSetId
			AND	qfa.Refnum = fpl.Refnum
		LEFT OUTER JOIN ante.PeerGroupFeedClass			pg2
			ON	pg2.FactorSetId = fpl.FactorSetId
			AND	pg2.[LimitLower_SG] <= qfa.Density_SG AND qfa.Density_SG < pg2.[LimitUpper_SG]
			AND	pg2.PeerGroup = 2
		WHERE fpl.CalQtr = 4
		GROUP BY
			fpl.FactorSetId,
			fpl.FactorSet_AnnDateKey,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fs1.BasisSlate,
			pg2.PeerGroup;

		INSERT INTO @FeedSlate(FactorSetId, Refnum, CalDateKey, FeedClass, BasisSlate, BasisDensity)
		SELECT
			p.FactorSetId,
			p.Refnum,
			p.Plant_QtrDateKey,
			4,
			CASE WHEN ISNULL(p.LiqLight, 0.0) + ISNULL(p.LiqHeavy, 0.0) > 85.0 AND ISNULL(p.LiqHeavy, 0.0) < 25.0 AND ROUND(qfa.Density_SG, 3) <= 0.7 AND COALESCE(fs3.BasisSlate, 0) = 0 THEN 1 ELSE 0 END
								[BasisSlate],
			CASE 
			WHEN  p.FactorSet_AnnDateKey <= 20091231	THEN
				CASE WHEN COALESCE(fs3.BasisDensity, 0) = 0 AND pg4.PeerGroup IS NOT NULL										THEN 1 ELSE 0 END
			WHEN  p.FactorSet_AnnDateKey >= 20110101	THEN
				CASE WHEN COALESCE(fs3.BasisDensity, 0) = 0 AND pg4.PeerGroup IS NOT NULL AND COALESCE(fs2.BasisDensity, 0) = 0	THEN 1 ELSE 0 END
			END					[BasisDensity]
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey,
				fpl.FactorSet_AnnDateKey,
				rve.ComponentId,
				rve.Component_WtPcnt
			FROM @fpl									fpl
			INNER JOIN inter.ReplValEncEstimate			rve
				ON	rve.FactorSetId = fpl.FactorSetId
				AND	rve.Refnum = fpl.Refnum
				AND	rve.CalDateKey = fpl.Plant_QtrDateKey
				AND rve.ComponentId IN ('LiqLight', 'LiqHeavy')
			WHERE	fpl.CalQtr = 4
			) u
			PIVOT (
			 SUM(u.Component_WtPcnt) FOR u.ComponentId IN (
				LiqLight,
				LiqHeavy
				)
			) p
		INNER JOIN fact.QuantityFeedAttributes			qfa
			ON	qfa.FactorSetId		= p.FactorSetId
			AND	qfa.Refnum			= p.Refnum
		LEFT OUTER JOIN @FeedSlate						fs3
			ON	fs3.FactorSetId		= p.FactorSetId
			AND	fs3.Refnum			= p.Refnum
			AND	fs3.CalDateKey		= p.Plant_QtrDateKey
			AND	fs3.FeedClass		= 3
		LEFT OUTER JOIN @FeedSlate						fs2
			ON	fs2.FactorSetId		= p.FactorSetId
			AND	fs2.Refnum			= p.Refnum
			AND	fs2.CalDateKey		= p.Plant_QtrDateKey
			AND	fs2.FeedClass		= 2
		LEFT OUTER JOIN ante.PeerGroupFeedClass			pg4
			ON	pg4.FactorSetId		= p.FactorSetId
			AND	pg4.[LimitLower_SG] <= qfa.Density_SG AND qfa.Density_SG < pg4.[LimitUpper_SG]
			AND	pg4.PeerGroup		= 4;

		INSERT INTO @FeedSlate(FactorSetId, Refnum, CalDateKey, FeedClass, BasisSlate, BasisDensity)
		SELECT
			p.FactorSetId,
			p.Refnum,
			p.Plant_QtrDateKey,
			5,
			CASE WHEN ISNULL(p.LiqLight, 0.0) + ISNULL(p.LiqHeavy, 0.0) > 75.0 AND ((ISNULL(p.LiqHeavy, 0.0) >= 25.0 OR ROUND(qfa.Density_SG, 3) > 0.7) AND COALESCE(fs3.BasisSlate, 0) = 0) THEN 1 ELSE 0 END
							[BasisSlate],
			CASE WHEN COALESCE(fs3.BasisDensity, 0) = 0 AND pg5.PeerGroup IS NOT NULL THEN 1 ELSE 0 END
							[BasisDensity]
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_QtrDateKey,
				fpl.FactorSet_AnnDateKey,
				rve.ComponentId,
				rve.Component_WtPcnt
			FROM @fpl									fpl
			INNER JOIN inter.ReplValEncEstimate			rve
				ON	rve.FactorSetId = fpl.FactorSetId
				AND	rve.Refnum = fpl.Refnum
				AND	rve.CalDateKey = fpl.Plant_QtrDateKey
				AND rve.ComponentId IN ('LiqLight', 'LiqHeavy')
			WHERE	fpl.CalQtr = 4
			) u
			PIVOT (
			 SUM(u.Component_WtPcnt) FOR u.ComponentId IN (
				LiqLight,
				LiqHeavy
				)
			) p
		INNER JOIN fact.QuantityFeedAttributes			qfa
			ON	qfa.FactorSetId		= p.FactorSetId
			AND	qfa.Refnum			= p.Refnum
		LEFT OUTER JOIN @FeedSlate						fs3
			ON	fs3.FactorSetId		= p.FactorSetId
			AND	fs3.Refnum			= p.Refnum
			AND	fs3.CalDateKey		= p.Plant_QtrDateKey
			AND	fs3.FeedClass		= 3
		LEFT OUTER JOIN ante.PeerGroupFeedClass			pg5
			ON	pg5.FactorSetId		= p.FactorSetId
			AND	pg5.[LimitLower_SG]	<= qfa.Density_SG AND qfa.Density_SG < pg5.[LimitUpper_SG]
			AND	pg5.PeerGroup		= 5;

		INSERT INTO calc.[PeerGroupFeedClass](FactorSetId, Refnum, CalDateKey, PeerGroup, StreamId, PlantDensity_SG)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,

			[PeerGroup]	= calc.MinValue(4, CASE WHEN SUM(fs.[_FeedClassSlate]) > 0 THEN calc.MaxValue(MAX(fs.[_FeedClassSlate]), ISNULL(fs4.[_FeedClassDensity], 0)) ELSE MAX(fd.[_FeedClassDensity]) END),
	
			[StreamId]	= CASE WHEN
				calc.MinValue(4, CASE WHEN SUM(fs.[_FeedClassSlate]) > 0 THEN calc.MaxValue(MAX(fs.[_FeedClassSlate]), ISNULL(fs4.[_FeedClassDensity], 0)) ELSE MAX(fd.[_FeedClassDensity]) END) = 4
			THEN
				CASE WHEN sc.Light / sc.FreshPyroFeed > 0.1 AND (ISNULL(sc.LiqHeavy, 0.0) +  ISNULL(sc.FeedLiqOther, 0.0)) = 0.0
				THEN
					'LiqLight'
				ELSE
					CASE WHEN (ISNULL(sc.LiqHeavy, 0.0) +  ISNULL(sc.FeedLiqOther, 0.0)) / sc.FreshPyroFeed > 0.15
					THEN 'LiqHeavy'
					ELSE 'Naphtha'
					END
				END
			END,
			qfa.Density_SG
		FROM @fpl											fpl
		LEFT OUTER JOIN @FeedSlate							fs
			ON	fs.FactorSetId = fpl.FactorSetId
			AND	fs.Refnum = fpl.Refnum
			AND	fs.CalDateKey = fpl.Plant_QtrDateKey
			AND	fs.BasisSlate = 1
		LEFT OUTER JOIN @FeedSlate							fd
			ON	fd.FactorSetId = fpl.FactorSetId
			AND	fd.Refnum = fpl.Refnum
			AND	fd.CalDateKey = fpl.Plant_QtrDateKey
			AND	fd.BasisDensity = 1
			AND	fd.FeedClass <= CASE
								WHEN fpl.FactorSet_AnnDateKey <= 20091231 THEN 5
								WHEN fpl.FactorSet_AnnDateKey >= 20110101 THEN 4
								END
		LEFT OUTER JOIN @FeedSlate							fs4
			ON	fs4.FactorSetId = fpl.FactorSetId
			AND	fs4.Refnum = fpl.Refnum
			AND	fs4.CalDateKey = fpl.Plant_QtrDateKey
			AND	fs4.BasisDensity = 1
			AND	fs4.FeedClass = CASE
								WHEN fpl.FactorSet_AnnDateKey <= 20091231 THEN 5
								WHEN fpl.FactorSet_AnnDateKey >= 20110101 THEN 4
								END

		INNER JOIN fact.QuantityFeedAttributes				qfa
			ON	qfa.FactorSetId = fpl.FactorSetId
			AND	qfa.Refnum = fpl.Refnum
		LEFT OUTER JOIN ante.[PeerGroupFeedClass]			dm
			ON	dm.FactorSetId = fpl.FactorSetId
			AND	dm.LimitLower_SG <= qfa.Density_SG
			AND qfa.Density_SG < LimitUpper_SG
		LEFT OUTER JOIN fact.GenPlantFeedFlex				ff
			ON	ff.Refnum = fpl.Refnum
			AND	ff.CalDateKey = fpl.Plant_QtrDateKey
			AND	ff.Capability_Pcnt > 0.0
			AND	ff.StreamId IN ('Diesel', 'GasOilHv')
		LEFT OUTER JOIN inter.ReplValEncEstimate			rve
			ON	rve.FactorSetId = fpl.FactorSetId
			AND	rve.Refnum = fpl.Refnum
			AND	rve.CalDateKey = fpl.Plant_QtrDateKey
			AND	rve.Component_WtPcnt > 0.0
			AND	rve.ComponentId = 'LiqHeavy'
		LEFT OUTER JOIN(
			SELECT
				q.FactorSetId,
				q.Refnum,
				q.StreamId,
				q.[Quantity_kMT]
			FROM fact.StreamQuantityAggregate		q	WITH (NOEXPAND)
			WHERE	q.StreamId IN ('Light', 'FreshPyroFeed', 'LiqHeavy', 'FeedLiqOther')
			) p
			PIVOT (
				SUM(Quantity_kMT) FOR StreamId IN (
					FreshPyroFeed,
					Light,
					LiqHeavy,
					FeedLiqOther
				)
			)												sc
			ON	sc.FactorSetId = fpl.FactorSetId
			AND	sc.Refnum = fpl.Refnum
		WHERE	fpl.CalQtr = 4
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fs4.[_FeedClassDensity],
			rve.Component_WtPcnt,
			sc.Light,
			sc.LiqHeavy,
			sc.FeedLiqOther,
			sc.FreshPyroFeed,
			qfa.Density_SG;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
