﻿CREATE PROCEDURE [calc].[Insert_Divisors_PyroFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Divisors(FactorSetId, Refnum, CalDateKey, DivisorId, DivisorField, DivisorValue, DivisorMultiplier)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			d.StreamId,
			'FeedPyrolysis_kMT',
			d.FeedPyrolysis_kMT,
			0.001
		FROM @fpl												tsq
		INNER JOIN calc.DivisorsFeed							d
			ON	d.FactorSetId = tsq.FactorSetId
			AND	d.Refnum = tsq.Refnum
			AND	d.CalDateKey = tsq.Plant_QtrDateKey
		WHERE	d.FeedPlant_kMT <> 0.0
			AND d.StreamId = 'PlantFeed'
			AND	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;