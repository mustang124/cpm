﻿CREATE PROCEDURE [calc].[Insert_ReplacementValue_Electricity]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplacementValue(FactorSetId, Refnum, CalDateKey, CurrencyRpt, ReplacementValueId, ReplacementValue)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			i.CurrencyRpt,
			f.FacilityId,
			CASE calc.MinValue(3, f.Unit_Count) + 1
				WHEN 1 THEN 1.0
				WHEN 2 THEN 1.0
				WHEN 3 THEN 1.15
				WHEN 4 THEN 1.2
			END * 65.0 * POWER(m.ElecGen_MW / 100.0, 0.6) * 1.17 * 1.35 * i._InflationFactor
		FROM @fpl									fpl
		INNER JOIN ante.InflationFactor				i
			--ON	i.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, i.FactorSetId) = fpl.DataYear
			AND	i.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN	fact.Facilities					f
			ON	f.Refnum = fpl.Refnum
			AND	f.CalDateKey = fpl.Plant_QtrDateKey
			AND f.FacilityId = 'ElecGenDist'
		INNER JOIN fact.FacilitiesMisc				m
			ON	m.Refnum = fpl.Refnum
			AND	m.CalDateKey = fpl.Plant_QtrDateKey
			AND	m.ElecGen_MW > 0.0
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;