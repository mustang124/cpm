﻿CREATE PROCEDURE [calc].[Insert_MeiLosses]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.MeiLosses(FactorSetId, Refnum, CalDateKey, CurrencyRpt, Stream_Value_Cur, Quantity_kMT, Component_kMT, Losses_Index, Losses_PcntRv)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			fpl.CurrencyRpt,
			SUM(fpl.Stream_Value_Cur)	[Stream_Value_Cur],
			SUM(fpl.Quantity_kMT)		[Quantity_kMT],
			SUM(r.Component_kMT)		[Component_kMT],
			SUM(r.Component_kMT) * SUM(fpl.Stream_Value_Cur) / SUM(fpl.Quantity_kMT)									[Losses_Index],
			SUM(r.Component_kMT) * SUM(fpl.Stream_Value_Cur) / SUM(fpl.Quantity_kMT) / rv.ReplacementLocation / 10.0	[Losses_PcntRv]
		FROM (
			SELECT
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey,
				fpl.Plant_QtrDateKey,
				fpl.CurrencyRpt,
				SUM(pp._Stream_Value_Cur)		[Stream_Value_Cur],
				SUM(pp.Quantity_kMT)			[Quantity_kMT]
			FROM @fpl									fpl
			INNER JOIN calc.PricingPlantStream			pp
				ON	pp.FactorSetId = fpl.FactorSetId
				AND	pp.Refnum = fpl.Refnum
				AND	pp.CalDateKey = fpl.Plant_QtrDateKey
				--	Task 141: Implement Tri-Tables in calculations
				--AND	pp.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('PlantFeed', 1, NULL) d)
				AND	pp.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'PlantFeed')
			GROUP BY
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey,
				fpl.Plant_QtrDateKey,
				fpl.CurrencyRpt
			) fpl
		INNER JOIN calc.CompositionStream			r
			ON	r.FactorSetId = fpl.FactorSetId
			AND	r.Refnum = fpl.Refnum
			AND	r.CalDateKey = fpl.Plant_QtrDateKey
			AND	r.StreamId = 'LossFlareVent'
		INNER JOIN calc.ReplacementValueAggregate	rv
			ON	rv.FactorSetId = fpl.FactorSetId
			AND	rv.Refnum = fpl.Refnum
			AND	rv.CalDateKey = fpl.Plant_AnnDateKey
			AND	rv.CurrencyRpt = fpl.CurrencyRpt
			AND	rv.ReplacementValueId = 'UscgReplacement'
			AND rv.ReplacementLocation IS NOT NULL
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			fpl.CurrencyRpt,
			rv.ReplacementLocation;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END