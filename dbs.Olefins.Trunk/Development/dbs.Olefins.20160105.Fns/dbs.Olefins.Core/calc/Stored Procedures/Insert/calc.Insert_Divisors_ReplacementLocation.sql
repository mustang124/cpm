﻿CREATE PROCEDURE [calc].[Insert_Divisors_ReplacementLocation]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Divisors(FactorSetId, Refnum, CalDateKey, DivisorId, DivisorField, DivisorValue, DivisorMultiplier)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			d.ReplacementValueId,
			'ReplacementLocation',
			d.ReplacementLocation,
			1.0
		FROM @fpl										tsq
		INNER JOIN calc.ReplacementValueAggregate		d
			ON	d.FactorSetId = tsq.FactorSetId
			AND	d.Refnum = tsq.Refnum
			AND	d.CalDateKey = tsq.Plant_QtrDateKey
			AND	d.CurrencyRpt = tsq.CurrencyRpt
		WHERE	d.ReplacementLocation <> 0.0
			AND	d.ReplacementValueId = 'UscgReplacement'
			AND	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;