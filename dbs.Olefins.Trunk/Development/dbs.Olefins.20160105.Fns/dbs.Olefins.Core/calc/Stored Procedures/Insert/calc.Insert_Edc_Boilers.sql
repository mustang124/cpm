﻿CREATE PROCEDURE [calc].[Insert_Edc_Boilers]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey]	[CalDateKey],
			k.[EdcId],
			k.[EdcId],
			SUM(f.[Rate_kLbHr]) * k.[Value] / 1000.0
		FROM @fpl										fpl
		INNER JOIN [fact].[FacilitiesPressure]			f
			ON	f.[Refnum]			= fpl.[Refnum]
			AND	f.[CalDateKey]		= fpl.[Plant_QtrDateKey]
			AND	f.[FacilityId]		IN (SELECT d.DescendantId FROM dim.Facility_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.FacilityId = 'BoilFiredSteam')
			AND	f.[Rate_kLbHr]		IS NOT NULL
		INNER JOIN [ante].[EdcCoefficients]				k
			ON	k.[FactorSetId]		= fpl.[FactorSetId]
			AND	k.[EdcId]			= 'BoilFiredSteam'
		WHERE	fpl.[CalQtr]		= 4
			AND	fpl.[FactorSet_QtrDateKey]	> 20130000
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			k.[EdcId],
			k.[Value]
		HAVING	SUM(f.[Rate_kLbHr])	IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;