﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonPlant_Steam]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY
	
		INSERT INTO calc.EmissionsCarbonPlant(FactorSetId, Refnum, CalDateKey, EmissionsId, Quantity_MBtu, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			ef.EmissionsId,
			e.LHValue_MBTU,
			ef.CEF_MTCO2_MBtu,
			e.LHValue_MBTU * ef.CEF_MTCO2_MBtu
		FROM @fpl												fpl
		INNER JOIN ante.EmissionsFactorCarbon					ef
			ON	ef.FactorSetId = fpl.FactorSetId
			AND	ef.CEF_MTCO2_MBtu IS NOT NULL
		INNER JOIN fact.EnergyLHValue							e
			ON	e.Refnum = fpl.Refnum
			AND	e.CalDateKey = fpl.Plant_QtrDateKey
			AND	e.AccountId = ef.EmissionsId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;