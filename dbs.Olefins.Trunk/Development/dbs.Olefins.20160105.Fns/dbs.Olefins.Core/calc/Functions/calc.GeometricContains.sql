﻿CREATE FUNCTION [calc].[GeometricContains]
(
	@GeometricNumber	INT, 
	@PartialAggregate	INT
)
RETURNS BIT
WITH SCHEMABINDING
AS
BEGIN
	
	DECLARE @rtn BIT;
	DECLARE @r INT = 2;

	IF (@GeometricNumber > 0)
		IF (@PartialAggregate % (@r * @GeometricNumber)) >= @GeometricNumber SET @rtn = 1 ELSE SET @rtn = 0;
	ELSE
		IF ((@GeometricNumber = 0) AND (@PartialAggregate = 0))
			SET @rtn = 1;
		ELSE
			SET @rtn = 0;

	RETURN @rtn;

END;