﻿







CREATE view [dbo].[PlantStatus] as
Select t.Refnum, CoLoc, t.Consultant
, OSIMUpload = MAX(CASE WHEN MessageId = 5 THEN MessageTime END) 
, PYPSUpload = MAX(CASE WHEN MessageId = 8 THEN MessageTime END) 
, SPSLUpload = MAX(CASE WHEN MessageId = 9 THEN MessageTime END) 
, Calcs = MAX(CASE WHEN MessageId = 4 THEN MessageTime END) 
, MakeTables = MAX(CASE WHEN MessageId = 11 THEN MessageTime END) 
, reports = MAX(CASE WHEN MessageId = 12 THEN MessageTime END) 
, PTs = MAX(CASE WHEN MessageId = 18 THEN MessageTime END) 
, CTs = MAX(CASE WHEN MessageId = 10 THEN MessageTime END) 
, SumCalc = MAX(CASE WHEN MessageId = 17 THEN MessageTime END) 
From MessageLog m JOIn dbo.TSort t on t.Refnum=m.Refnum
Group by t.Refnum, CoLoc, t.Consultant







