﻿


CREATE              PROC [dbo].[spCapUtil](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @RVAvg real, @EDCAvg_k real, @UEDCAvg_k real, @UEDCAvgTAAdj_k real, @HvcProd_kMT real
SELECT 
  @RVAvg		= RV_MUS
, @EDCAvg_k		= kEdc
, @UEDCAvg_k	= kUEdc
, @UEDCAvgTAAdj_k	= TaAdj_kUEDC
, @HvcProd_kMT	= HvcProd_kMT
FROM dbo.Divisors d WHERE d.Refnum = @Refnum

DECLARE @EthyleneCapYE_kMT real, @PropyleneCapYE_kMT real, @OlefinsCapYE_kMT real, @FreshPyroFeedCapYE_kMT real
SELECT
  @EthyleneCapYE_kMT			= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.Capacity_kMT ELSE 0 END)
, @PropyleneCapYE_kMT			= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.Capacity_kMT ELSE 0 END)
, @OlefinsCapYE_kMT				= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.Capacity_kMT ELSE 0 END)
, @FreshPyroFeedCapYE_kMT		= -SUM(CASE WHEN cu.StreamId='FreshPyroFeed' THEN cu.Capacity_kMT ELSE 0 END)
From calc.CapacityPlant cu WHERE cu.Refnum = @Refnum and cu.FactorSetId = @FactorSetId

DECLARE @EthyleneProd_kMT real,@PropyleneProd_kMT real,@OlefinsProd_kMT real
DECLARE @EthyleneCapAvg_kMT real, @PropyleneCapAvg_kMT real, @OlefinsCapAvg_kMT real, @PlantFeedCapAvg_kMT real, 
	@EthyleneCpbyAvg_kMT real, @PropyleneCpbyAvg_kMT real, @OlefinsCpbyAvg_kMT real,
	@EthyleneCapUtil_pcnt real,@PropyleneCapUtil_pcnt real,@OlefinsCapUtil_pcnt real,@PlantFeedCapUtil_pcnt real,
	@EthyleneCapUtilTAAdj_pcnt real,@PropyleneCapUtilTAAdj_pcnt real,@OlefinsCapUtilTAAdj_pcnt real,@PlantFeedCapUtilTAAdj_pcnt real,
	@EthyleneCpbyUtil_pcnt real,@PropyleneCpbyUtil_pcnt real,@OlefinsCpbyUtil_pcnt real,
	@EthyleneCpbyUtilTAAdj_pcnt real,@PropyleneCpbyUtilTAAdj_pcnt real,@OlefinsCpbyUtilTAAdj_pcnt real
SELECT
  @EthyleneProd_kMT				= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.ProductionActual_kMT ELSE 0 END)
, @PropyleneProd_kMT			= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.ProductionActual_kMT ELSE 0 END)
, @OlefinsProd_kMT				= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.ProductionActual_kMT ELSE 0 END)

, @EthyleneCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.CapacityAvg_kMT ELSE 0 END)
, @PropyleneCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.CapacityAvg_kMT ELSE 0 END)
, @OlefinsCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.CapacityAvg_kMT ELSE 0 END)
, @PlantFeedCapAvg_kMT			= SUM(CASE WHEN cu.StreamId='PlantFeed' THEN cu.CapacityAvg_kMT ELSE 0 END)

, @EthyleneCpbyAvg_kMT			= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu.StreamAvg_kMT ELSE 0 END)
, @PropyleneCpbyAvg_kMT			= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu.StreamAvg_kMT ELSE 0 END)
, @OlefinsCpbyAvg_kMT			= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu.StreamAvg_kMT ELSE 0 END)

, @EthyleneCapUtil_pcnt			= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu._UtilizationNominal_Pcnt ELSE 0 END)
, @PropyleneCapUtil_pcnt		= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu._UtilizationNominal_Pcnt ELSE 0 END)
, @OlefinsCapUtil_pcnt			= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu._UtilizationNominal_Pcnt ELSE 0 END)
, @PlantFeedCapUtil_pcnt		= SUM(CASE WHEN cu.StreamId='PlantFeed' THEN cu._UtilizationNominal_Pcnt ELSE 0 END)

, @EthyleneCapUtilTAAdj_pcnt	= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu._TaAdj_UtilizationNominal_Pcnt ELSE 0 END)
, @PropyleneCapUtilTAAdj_pcnt	= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu._TaAdj_UtilizationNominal_Pcnt ELSE 0 END)
, @OlefinsCapUtilTAAdj_pcnt		= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu._TaAdj_UtilizationNominal_Pcnt ELSE 0 END)
, @PlantFeedCapUtilTAAdj_pcnt	= SUM(CASE WHEN cu.StreamId='PlantFeed' THEN cu._TaAdj_UtilizationNominal_Pcnt ELSE 0 END)

, @EthyleneCpbyUtil_pcnt		= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu._UtilizationStream_Pcnt ELSE 0 END)
, @PropyleneCpbyUtil_pcnt		= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu._UtilizationStream_Pcnt ELSE 0 END)
, @OlefinsCpbyUtil_pcnt			= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu._UtilizationStream_Pcnt ELSE 0 END)

, @EthyleneCpbyUtilTAAdj_pcnt	= SUM(CASE WHEN cu.StreamId='Ethylene' THEN cu._TaAdj_UtilizationStream_Pcnt ELSE 0 END)
, @PropyleneCpbyUtilTAAdj_pcnt	= SUM(CASE WHEN cu.StreamId='Propylene' THEN cu._TaAdj_UtilizationStream_Pcnt ELSE 0 END)
, @OlefinsCpbyUtilTAAdj_pcnt	= SUM(CASE WHEN cu.StreamId='ProdOlefins' THEN cu._TaAdj_UtilizationStream_Pcnt ELSE 0 END)

From calc.CapacityUtilization cu WHERE cu.Refnum=@Refnum and cu.SchedId='A' and cu.FactorSetId = @FactorSetId

DECLARE @EthyleneCapUtilYE_pcnt real, @PropyleneCapUtilYE_pcnt real, @OlefinsCapUtilYE_pcnt real
SELECT
  @EthyleneCapUtilYE_pcnt			= CASE WHEN @EthyleneCapYE_kMT > 0 THEN @EthyleneCapUtil_pcnt * @EthyleneCapAvg_kMT / @EthyleneCapYE_kMT ELSE 0 END
, @PropyleneCapUtilYE_pcnt			= CASE WHEN @PropyleneCapYE_kMT > 0 THEN @PropyleneCapUtil_pcnt * @PropyleneCapAvg_kMT / @PropyleneCapYE_kMT ELSE 0 END
, @OlefinsCapUtilYE_pcnt			= CASE WHEN @OlefinsCapYE_kMT > 0 THEN @OlefinsCapUtil_pcnt * @OlefinsCapAvg_kMT / @OlefinsCapYE_kMT ELSE 0 END

DECLARE @RecycleCapAvg_kMT real, @ServiceFactor real, @EthyleneMaxProd_MTd real, @PropyleneMaxProd_MTd real, @OlefinsMaxProd_MTd real
SELECT
  @RecycleCapAvg_kMT	= SUM(CASE WHEN c.StreamId='Recycle' THEN c.Capacity_kMT ELSE 0 END)
, @ServiceFactor		= SUM(CASE WHEN c.StreamId='Ethylene' THEN c._ServiceFactor_Ann ELSE 0 END) * 100
, @EthyleneMaxProd_MTd	= SUM(CASE WHEN c.StreamId='Ethylene' THEN c.Record_MTd ELSE 0 END)
, @PropyleneMaxProd_MTd	= SUM(CASE WHEN c.StreamId='Propylene' THEN c.Record_MTd ELSE 0 END)
From fact.Capacity c WHERE c.Refnum=@Refnum

SELECT @OlefinsMaxProd_MTd	= ISNULL(@EthyleneMaxProd_MTd,0) + ISNULL(@PropyleneMaxProd_MTd,0)

DECLARE @CapLoss_Pcnt real
SELECT
	@CapLoss_Pcnt = CapLoss_Pcnt
FROM fact.CapacityAttributes c WHERE c.Refnum=@Refnum

DELETE FROM dbo.CapUtil WHERE Refnum = @Refnum
Insert into dbo.CapUtil (Refnum
, EDCAvg_k
, UEDCAvg_k
, UEDCAvgTAAdj_k
, EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT
, PlantFeedCapAvg_kMT
, RecycleCapAvg_kMT
, EthyleneCapYE_kMT
, PropyleneCapYE_kMT
, OlefinsCapYE_kMT
, FreshPyroFeedCapYE_kMT

, EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT
, EthyleneProd_kMT
, PropyleneProd_kMT
, OlefinsProd_kMT
, HvcProd_kMT
, EthyleneMaxProd_MTd
, PropyleneMaxProd_MTd
, OlefinsMaxProd_MTd
, ServiceFactor
, EthyleneCapUtil_pcnt
, PropyleneCapUtil_pcnt
, OlefinsCapUtil_pcnt
, PlantFeedCapUtil_pcnt
, EthyleneCapUtilTAAdj_pcnt
, PropyleneCapUtilTAAdj_pcnt
, OlefinsCapUtilTAAdj_pcnt
, PlantFeedCapUtilTAAdj_pcnt
, EthyleneCapUtilYE_pcnt
, PropyleneCapUtilYE_pcnt
, OlefinsCapUtilYE_pcnt
, EthyleneCpbyUtil_pcnt
, PropyleneCpbyUtil_pcnt
, OlefinsCpbyUtil_pcnt
, EthyleneCpbyUtilTAAdj_pcnt
, PropyleneCpbyUtilTAAdj_pcnt
, OlefinsCpbyUtilTAAdj_pcnt
, CapLoss_Pcnt
, HVCPerEDC
, RVPerEDC
, RVPerHVC
, FurnFeedToEthyleneRatio
, FurnFeedToProdOlefinsRatio)

SELECT Refnum = @Refnum
, EDCAvg_k = @EDCAvg_k
, UEDCAvg_k = @UEDCAvg_k
, UEDCAvgAdj_k = @UEDCAvgTAAdj_k
, EthyleneCapAvg_kMT = @EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT = @PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT = @OlefinsCapAvg_kMT
, PlantFeedCapAvg_kMT = @PlantFeedCapAvg_kMT
, RecycleCapAvg_kMT = @RecycleCapAvg_kMT
, EthyleneCapYE_kMT = @EthyleneCapYE_kMT
, PropyleneCapYE_kMT = @PropyleneCapYE_kMT
, OlefinsCapYE_kMT = @OlefinsCapYE_kMT
, FreshPyroFeedCapYE_kMT = @FreshPyroFeedCapYE_kMT
, EthyleneCpbyAvg_kMT = @EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT = @PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT = @OlefinsCpbyAvg_kMT
, EthyleneProd_kMT = @EthyleneProd_kMT
, PropyleneProd_kMT = @PropyleneProd_kMT
, OlefinsProd_kMT = @OlefinsProd_kMT
, HvcProd_kMT = @HvcProd_kMT
, EthyleneMaxProd_MTd = @EthyleneMaxProd_MTd
, PropyleneMaxProd_MTd = @PropyleneMaxProd_MTd
, OlefinsMaxProd_MTd = @OlefinsMaxProd_MTd
, ServiceFactor = @ServiceFactor
, EthyleneCapUtil_pcnt = @EthyleneCapUtil_pcnt
, PropyleneCapUtil_pcnt = @PropyleneCapUtil_pcnt
, OlefinsCapUtil_pcnt = @OlefinsCapUtil_pcnt
, PlantFeedCapUtil_pcnt = @PlantFeedCapUtil_pcnt
, EthyleneCapUtilTAAdj_pcnt = @EthyleneCapUtilTAAdj_pcnt
, PropyleneCapUtilTAAdj_pcnt = @PropyleneCapUtilTAAdj_pcnt
, OlefinsCapUtilTAAdj_pcnt = @OlefinsCapUtilTAAdj_pcnt
, PlantFeedCapUtilTAAdj_pcnt = @PlantFeedCapUtilTAAdj_pcnt
, EthyleneCapUtilYE_pcnt = @EthyleneCapUtilYE_pcnt
, PropyleneCapUtilYE_pcnt = @PropyleneCapUtilYE_pcnt
, OlefinsCapUtilYE_pcnt = @OlefinsCapUtilYE_pcnt
, EthyleneCpbyUtil_pcnt = @EthyleneCpbyUtil_pcnt
, PropyleneCpbyUtil_pcnt = @PropyleneCpbyUtil_pcnt
, OlefinsCpbyUtil_pcnt = @OlefinsCpbyUtil_pcnt
, EthyleneCpbyUtilTAAdj_pcnt = @EthyleneCpbyUtilTAAdj_pcnt
, PropyleneCpbyUtilTAAdj_pcnt = @PropyleneCpbyUtilTAAdj_pcnt
, OlefinsCpbyUtilTAAdj_pcnt = @OlefinsCpbyUtilTAAdj_pcnt
, CapLoss_Pcnt = @CapLoss_Pcnt
, HVCPerEDC						= CASE WHEN @EDCAvg_k > 0 THEN @HvcProd_kMT / @EDCAvg_k ELSE 0 END
, RVPerEDC						= CASE WHEN @EDCAvg_k > 0 THEN @RVAvg / @EDCAvg_k ELSE 0 END
, RVPerHVC						= CASE WHEN @HvcProd_kMT > 0 THEN @RVAvg / @HvcProd_kMT ELSE 0 END
, FurnFeedToEthyleneRatio		= CASE WHEN @EthyleneCapAvg_kMT > 0 THEN @PlantFeedCapAvg_kMT / @EthyleneCapAvg_kMT ELSE 0 END
, FurnFeedToProdOlefinsRatio	= CASE WHEN @OlefinsCapAvg_kMT > 0 THEN @PlantFeedCapAvg_kMT / @OlefinsCapAvg_kMT ELSE 0 END

SET NOCOUNT OFF

















