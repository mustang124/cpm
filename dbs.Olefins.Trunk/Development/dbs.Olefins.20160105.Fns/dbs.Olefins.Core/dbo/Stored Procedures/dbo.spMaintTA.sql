﻿





CREATE              PROC [dbo].[spMaintTA](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON
DECLARE @Currency varchar(5)
SELECT  @Currency = 'USD'


DECLARE @RV_MUS real, @kEdc real, @kUEdc real
SELECT 
  @RV_MUS			= RV_MUS
, @kEdc				= kEdc
FROM dbo.Divisors d WHERE d.Refnum = @Refnum

DECLARE @TATimingRegInsp real, @TATimingDiscInsp real, @TATimingDeterioration real, @TATimingLowDemand real, @TATimingRegular real, @TATimingCptl real
		, @TAPrepHrs real, @TAStartupHrs real, @TAMini real
Select
  @TATimingRegInsp			= CASE WHEN TimingFactorId = 'A' THEN 100.0 ELSE 0 END
, @TATimingDiscInsp			= CASE WHEN TimingFactorId = 'B' THEN 100.0 ELSE 0 END
, @TATimingDeterioration	= CASE WHEN TimingFactorId = 'C' THEN 100.0 ELSE 0 END
, @TATimingLowDemand		= CASE WHEN TimingFactorId = 'D' THEN 100.0 ELSE 0 END
, @TATimingRegular			= CASE WHEN TimingFactorId = 'E' THEN 100.0 ELSE 0 END
, @TATimingCptl				= CASE WHEN TimingFactorId = 'F' THEN 100.0 ELSE 0 END
, @TAPrepHrs				= TaPrep_Hrs
, @TAStartupHrs				= TaStartUp_Hrs
, @TAMini					= ISNULL(MiniTurnAround_Bit,0) * 100.0
FROM fact.Reliability r WHERE r.Refnum=@Refnum

DECLARE @CritPathCompGas real, @CritPathCompRefrig real, @CritPathPyroFurnTle real, @CritPathExchanger real, @CritPathVessel real, @CritPathFurn real, @CritPathUtilities real, @CritPathCritPathCapProj real, @CritPathCritPathOther real
SELECT
  @CritPathCompGas			= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'CompGas' THEN 100.0 ELSE 0.0 END)
, @CritPathCompRefrig		= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'CompRefrig' THEN 100.0 ELSE 0.0 END)
, @CritPathPyroFurnTle		= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'PyroFurnTle' THEN 100.0 ELSE 0.0 END)
, @CritPathExchanger		= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'Exchanger' THEN 100.0 ELSE 0.0 END)
, @CritPathVessel			= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'Vessel' THEN 100.0 ELSE 0.0 END)
, @CritPathFurn				= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'PyroFurn' THEN 100.0 ELSE 0.0 END)
, @CritPathUtilities		= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'Utilities' THEN 100.0 ELSE 0.0 END)
, @CritPathCritPathCapProj	= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'CritPathCapProj' THEN 100.0 ELSE 0.0 END)
, @CritPathCritPathOther	= MAX(CASE WHEN CritPath_Bit = 1 AND FacilityId = 'CritPathOther' THEN 100.0 ELSE 0.0 END)
FROM fact.ReliabilityCritPath r WHERE r.Refnum=@Refnum
GROUP BY r.Refnum

DECLARE @TAPlanIntMonth real, @TAPlanDTHrs real, @TAPlanLossPcnt real, @TAIntMonth real, @TADTHrs real, @TALossPcnt real, @TACostMUS real, @TACostPcntRV real, @TACostPerEDC real, @TAWHrs real
		, @TotTACostMUS real
SELECT 
  @TAPlanIntMonth		= TAPlanInt_Month
, @TAPlanDTHrs 			= TAPlanDT_Hrs
, @TAPlanLossPcnt 		= TAPlanLoss_Pcnt
, @TAIntMonth 			= TAInt_Month
, @TADTHrs 				= TADT_Hrs
, @TALossPcnt 			= TA_LossPcnt
, @TACostMUS 			= TACost_MUS
, @TotTACostMUS 		= TACost_MUS
, @TAWHrs 				= TAWHrs
FROM dbo.ReliabilityTACalc r WHERE r.Refnum = @Refnum and r.Currency = @Currency

DECLARE @TAAdjFactor real
SELECT 
  @TAAdjFactor = _TaAdj_Production_Ratio
FROM calc.TaAdjRatio t 
where t.Refnum = @Refnum and t.FactorSetId=@FactorSetId

DECLARE @TAMechHrs real, @TATotDTHrs real
select TOP 1 @TATotDTHrs = r.DownTime_Hrs
from fact.ReliabilityTA r
where Refnum = @Refnum And SchedId = 'A'
order by TurnAround_Date desc

SELECT @TAMechHrs = @TATotDTHrs - @TAPrepHrs
SELECT @TAMechHrs = CASE WHEN @TAMechHrs <= 0 THEN 0 ELSE @TAMechHrs END


DECLARE @TaAdj_Feed_Ratio	REAL;
DECLARE @TaAdj_Prod_Ratio	REAL;

SELECT
	@TaAdj_Feed_Ratio	= r.[_TaAdj_Feed_Ratio],
	@TaAdj_Prod_Ratio	= r.[_TaAdj_Production_Ratio]
FROM [calc].[TaAdjRatio]	r
WHERE	r.[FactorSetId]	= @FactorSetId
	AND	r.[Refnum]		= @Refnum;
	
DELETE FROM dbo.MaintTA WHERE Refnum = @Refnum
INSERT into dbo.MaintTA (Refnum
, Currency
, TAPlanInt_Month
, TAPlanDT_Hrs
, TAPlanDT_Days
, TAPlan_LossPcnt
, TAInt_Month
, TADT_Hrs
, TADT_Days
, TA_LossPcnt
, TACost_MUS
, TACost_PcntRV
, TACost_PerEDC
, TAWHrs
, TAAdjFactor
, TATimingRegInsp
, TATimingDiscInsp
, TATimingDeterioration
, TATimingLowDemand
, TATimingRegular
, TATimingCptl
, CritPathCompGas
, CritPathCompRefrig
, CritPathPyroFurnTle
, CritPathExchanger
, CritPathVessel
, CritPathFurn
, CritPathUtilities
, CritPathCritPathCapProj
, CritPathCritPathOther
, TAPrepHrs
, TAMechHrs
, TAStartupHrs
, TAMini
, TaAdj_Feed_Ratio
, TaAdj_Prod_Ratio
)

SELECT Refnum = @Refnum
, Currency = @Currency
, TAPlanIntMonth= @TAPlanIntMonth
, TAPlanDTHrs= @TAPlanDTHrs
, TAPlanDTDays= @TAPlanDTHrs / 24.0
, TAPlanLossPcnt= @TAPlanLossPcnt
, TAIntMonth= @TAIntMonth
, TADTHrs= @TADTHrs
, TADTDays= @TADTHrs / 24.0
, TALossPcnt= @TALossPcnt
, TACostMUS= @TACostMUS
, TACostPcntRV= CASE WHEN @RV_MUS>0 THEN @TotTACostMUS / @RV_MUS * 100.0 END
, TACostPerEDC= CASE WHEN @kEdc>0 THEN @TotTACostMUS / @kEdc * 1000.0 END
, TAWHrs= @TAWHrs
, TAAdjFactor = @TAAdjFactor
, TATimingRegInsp= @TATimingRegInsp
, TATimingDiscInsp= @TATimingDiscInsp
, TATimingDeterioration= @TATimingDeterioration
, TATimingLowDemand= @TATimingLowDemand
, TATimingRegular= @TATimingRegular
, TATimingCptl= @TATimingCptl
, CritPathCompGas= @CritPathCompGas
, CritPathCompRefrig= @CritPathCompRefrig
, CritPathPyroFurnTle= @CritPathPyroFurnTle
, CritPathExchanger= @CritPathExchanger
, CritPathVessel= @CritPathVessel
, CritPathFurn= @CritPathFurn
, CritPathUtilities= @CritPathUtilities
, CritPathCritPathCapProj= @CritPathCritPathCapProj
, CritPathCritPathOther= @CritPathCritPathOther
, TAPrepHrs= @TAPrepHrs
, TAMechHrs = @TAMechHrs
, TAStartupHrs= @TAStartupHrs
, TAMini= @TAMini
, TaAdj_Feed_Ratio = @TaAdj_Feed_Ratio
, TaAdj_Prod_Ratio = @TaAdj_Prod_Ratio;

SET NOCOUNT OFF
