﻿CREATE TABLE [sim].[QueueErrors]
(
	[QueueErrorsId]		INT					NOT	NULL	IDENTITY(1, 1),
	[QueueId]			INT					NOT	NULL	CONSTRAINT [FK_QueueErrors_QueueAdd]			REFERENCES [sim].[QueueAdd]([QueueId]),

	[ErrorMessage]		VARCHAR(MAX)		NOT	NULL	CONSTRAINT [CL_QueueErrors_ErrorMessage]		CHECK([ErrorMessage] <> ''),

	[tsModified]		DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_QueueErrors_tsModified]			DEFAULT (SYSDATETIMEOFFSET()),
	[tsModifiedHost]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueErrors_tsModifiedHost]		DEFAULT (HOST_NAME()),
	[tsModifiedUser]	NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueErrors_tsModifiedUser]		DEFAULT (SUSER_SNAME()),
	[tsModifiedApp]		NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_QueueErrors_tsModifiedApp]		DEFAULT (APP_NAME()),

	CONSTRAINT [PK_QueueErrors] PRIMARY KEY NONCLUSTERED([QueueErrorsId] ASC)
);