﻿CREATE FUNCTION sim.Distillation
(
	@FactorSetId			VARCHAR(12) = NULL,
	@Refnum					VARCHAR(25) = NULL
)
RETURNS @ReturnTable TABLE
(
	Refnum					VARCHAR(18)			NULL,
	CalDateKey				INT					NULL,
	StreamId				VARCHAR(42)			NULL,
	StreamDescription		NVARCHAR(256)		NULL,
	Density_SG				REAL				NULL,
	D000					REAL				NULL,
	D005					REAL				NULL,
	D010					REAL				NULL,
	D030					REAL				NULL,
	D050					REAL				NULL,
	D070					REAL				NULL,
	D090					REAL				NULL,
	D095					REAL				NULL,
	D100					REAL				NULL
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @tblRegDistTemps TABLE(
		FactorSetId				VARCHAR(12)			NOT NULL CHECK(LEN(FactorSetId) > 0),
		Refnum					VARCHAR(18)			NOT	NULL CHECK(LEN(Refnum) > 0),
		CalDateKey				INT					NOT	NULL,
		StreamId				VARCHAR(42)			NOT	NULL CHECK(LEN(StreamId) > 0),
		StreamDescription		NVARCHAR(256)		NOT	NULL CHECK(LEN(StreamDescription) > 0),
		Density_SG				REAL					NULL,
		DistillationId			VARCHAR(4)			NOT NULL CHECK(LEN(DistillationId) > 0),
		SortKey					TINYINT					NULL,
		RegTemp_C				REAL					NULL,
		RegTempiFact			REAL					NULL,
		RegTempSlope			REAL					NULL,
		RegTempInter			REAL					NULL,
		RegTempMin_C			REAL					NULL,
		RegTempMax_C			REAL					NULL,
		ClientTemp_C			REAL					NULL,
		InterpTempA_C			REAL					NULL,
		InterpTempB_C			REAL					NULL,
		InterpTemp_C			REAL					NULL,
		Temp_C					REAL					NULL,
		PRIMARY KEY CLUSTERED(FactorSetId DESC, Refnum DESC, CalDateKey DESC, StreamId ASC, StreamDescription ASC, DistillationId ASC)
		);
		
	--INSERT @tblRegDistTemps (FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, SG, 
	--	DistillationId, RegTempiFact, RegTempSlope, RegTempInter, RegTempMin_C, RegTempMax_C, RegTemp_C)
	--SELECT m.FactorSetId, p.Refnum, p.CalDateKey, p.StreamId, p.StreamDescription, p.Density_SG AS SG, 
	--	m.DistillationId, m.iFact, m.Slope, m.Inter, m.Temp_Min_C, m.Temp_Max_C,
	--	RegTemp_C = m.Slope * p.Density_SG + m.Inter
	--FROM fact.FeedStockCrackingParameters p
	--INNER JOIN ante.[SimDistillationNormalization] m ON ISNULL(@FactorSetId, m.FactorSetId) = m.FactorSetId
	--  AND ((m.StreamId = p.StreamId AND p.StreamId NOT LIKE 'FeedLiqOther') 
	--  OR (p.StreamId LIKE 'FeedLiqOther' AND m.StreamId = CASE WHEN ISNULL(p.Density_SG, 0.0) < 0.75 THEN 'NaphthaLT' ELSE 'HeavyGasoil' END))
	--WHERE p.Refnum = ISNULL(@Refnum, p.Refnum);

	INSERT @tblRegDistTemps (FactorSetId, Refnum, CalDateKey, StreamId, StreamDescription, Density_SG, 
		DistillationId, RegTempiFact, RegTempSlope, RegTempInter, RegTempMin_C, RegTempMax_C, RegTemp_C)
	SELECT m.FactorSetId, p.Refnum, p.CalDateKey, p.StreamId, p.StreamDescription, p.Density_SG, 
		m.DistillationId, m.iFact, m.Slope, m.Inter, m.Temp_Min_C, m.Temp_Max_C,
		RegTemp_C = m.Slope * p.Density_SG + m.Inter
	FROM fact.FeedStockCrackingParameters p
	INNER JOIN ante.[SimNormalizationDistillation] m
		ON ((m.StreamId = p.StreamId AND p.StreamId NOT LIKE 'FeedLiqOther') 
		OR (p.StreamId LIKE 'FeedLiqOther' AND m.StreamId = CASE WHEN ISNULL(p.Density_SG, 0.0) < 0.75 THEN 'NaphthaLT' ELSE 'HeavyGasoil' END))
	WHERE p.Refnum = ISNULL(@Refnum, p.Refnum)
		AND m.FactorSetId = ISNULL(@FactorSetId, m.FactorSetId);
	
	UPDATE tmp
	SET ClientTemp_C = (SELECT j.Temp_C FROM fact.FeedStockDistillation j WHERE j.Refnum = tmp.Refnum AND j.CalDateKey = tmp.CalDateKey 
				AND j.StreamId = tmp.StreamId AND j.StreamDescription = tmp.StreamDescription AND j.DistillationId = tmp.DistillationId),
		SortKey = (SELECT l.SortKey FROM dim.FeedStockDistillationLu l WHERE /* l.DistMethodId = 'D86' AND */ l.DistillationId = tmp.DistillationId)
	FROM @tblRegDistTemps tmp

	UPDATE tmp
	SET InterpTempA_C = (SELECT CASE WHEN ISNULL(ClientTemp_C,0) > 0 THEN ClientTemp_C ELSE RegTemp_C END FROM @tblRegDistTemps a WHERE a.Refnum = tmp.Refnum AND a.CalDateKey = tmp.CalDateKey AND a.FactorSetId = tmp.FactorSetId AND a.StreamId = tmp.StreamId AND a.StreamDescription = tmp.StreamDescription AND a.SortKey = tmp.SortKey - 1)
	  , InterpTempB_C = (SELECT CASE WHEN ISNULL(ClientTemp_C,0) > 0 THEN ClientTemp_C ELSE RegTemp_C END FROM @tblRegDistTemps b WHERE b.Refnum = tmp.Refnum AND b.CalDateKey = tmp.CalDateKey AND b.FactorSetId = tmp.FactorSetId AND b.StreamId = tmp.StreamId AND b.StreamDescription = tmp.StreamDescription AND b.SortKey = tmp.SortKey + 1)
	FROM @tblRegDistTemps tmp

	UPDATE @tblRegDistTemps
	SET InterpTemp_C = RegTempiFact*InterpTempA_C+(1-RegTempiFact)*InterpTempB_C
	WHERE DistillationId NOT IN ('D000','D100')

	UPDATE tmp
	SET InterpTemp_C = ISNULL(a.ClientTemp_C,0) + 5
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps a ON a.Refnum = tmp.Refnum AND a.CalDateKey = tmp.CalDateKey AND a.FactorSetId = tmp.FactorSetId AND a.StreamId = tmp.StreamId AND a.StreamDescription = tmp.StreamDescription AND a.SortKey = tmp.SortKey - 1
	WHERE tmp.DistillationId NOT IN ('D000','D100') AND tmp.InterpTemp_C < ISNULL(a.ClientTemp_C,0) + 5 

	UPDATE @tblRegDistTemps
	SET InterpTemp_C = CASE WHEN RegTemp_C > RegTempMin_C THEN RegTemp_C ELSE RegTempMin_C END
	WHERE DistillationId = 'D000'

	UPDATE tmp
	SET InterpTemp_C = ISNULL(a.InterpTemp_C,0) - 5
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps a ON a.Refnum = tmp.Refnum AND a.CalDateKey = tmp.CalDateKey AND a.FactorSetId = tmp.FactorSetId AND a.StreamId = tmp.StreamId AND a.StreamDescription = tmp.StreamDescription AND a.SortKey = tmp.SortKey + 1
	WHERE tmp.DistillationId = 'D000' AND tmp.InterpTemp_C > ISNULL(a.InterpTemp_C,0) - 5 

	UPDATE @tblRegDistTemps
	SET InterpTemp_C = CASE WHEN RegTemp_C < RegTempMax_C THEN RegTemp_C ELSE RegTempMax_C END
	WHERE DistillationId = 'D100'

	UPDATE tmp
	SET InterpTemp_C = ISNULL(a.InterpTemp_C,0) + 5
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps a ON a.Refnum = tmp.Refnum AND a.CalDateKey = tmp.CalDateKey AND a.FactorSetId = tmp.FactorSetId AND a.StreamId = tmp.StreamId AND a.StreamDescription = tmp.StreamDescription AND a.SortKey = tmp.SortKey - 1
	WHERE tmp.DistillationId = 'D100' AND tmp.InterpTemp_C < ISNULL(a.InterpTemp_C,0) + 5 

	UPDATE tmp
	SET InterpTemp_C = (SELECT MAX(ClientTemp_C) + 5 FROM @tblRegDistTemps a WHERE a.Refnum = tmp.Refnum AND a.CalDateKey = tmp.CalDateKey AND a.FactorSetId = tmp.FactorSetId AND a.StreamId = tmp.StreamId AND a.StreamDescription = tmp.StreamDescription)
	FROM @tblRegDistTemps tmp 
	WHERE tmp.DistillationId = 'D100' AND tmp.InterpTemp_C < (SELECT MAX(ClientTemp_C) + 5 FROM @tblRegDistTemps a WHERE a.Refnum = tmp.Refnum AND a.CalDateKey = tmp.CalDateKey AND a.FactorSetId = tmp.FactorSetId AND a.StreamId = tmp.StreamId AND a.StreamDescription = tmp.StreamDescription)

	UPDATE @tblRegDistTemps
	SET Temp_C = CASE WHEN ISNULL(Density_SG,0) = 0 THEN 0 WHEN ClientTemp_C > 0 THEN ClientTemp_C ELSE InterpTemp_C END

	UPDATE tmp
	SET Temp_C = p.Temp_C + 0.1
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps p ON p.Refnum = tmp.Refnum AND p.CalDateKey = tmp.CalDateKey  AND p.FactorSetId = tmp.FactorSetId AND p.StreamId = tmp.StreamId AND p.StreamDescription = tmp.StreamDescription
		AND (tmp.DistillationId = 'D010' AND p.DistillationId = 'D000') 
	WHERE tmp.Temp_C <= p.Temp_C + 0.1 AND tmp.StreamId <> 'OtherFeedLiq'

	UPDATE tmp
	SET Temp_C = p.Temp_C + 0.1
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps p ON p.Refnum = tmp.Refnum AND p.CalDateKey = tmp.CalDateKey  AND p.FactorSetId = tmp.FactorSetId AND p.StreamId = tmp.StreamId AND p.StreamDescription = tmp.StreamDescription
		AND (tmp.DistillationId = 'D030' AND p.DistillationId = 'D010') 
	WHERE tmp.Temp_C <= p.Temp_C + 0.1 AND tmp.StreamId <> 'OtherFeedLiq'

	UPDATE tmp
	SET Temp_C = p.Temp_C + 0.1
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps p ON p.Refnum = tmp.Refnum AND p.CalDateKey = tmp.CalDateKey  AND p.FactorSetId = tmp.FactorSetId AND p.StreamId = tmp.StreamId AND p.StreamDescription = tmp.StreamDescription
		AND (tmp.DistillationId = 'D050' AND p.DistillationId = 'D030') 
	WHERE tmp.Temp_C <= p.Temp_C + 0.1 AND tmp.StreamId <> 'OtherFeedLiq'

	UPDATE tmp
	SET Temp_C = p.Temp_C + 0.1
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps p ON p.Refnum = tmp.Refnum AND p.CalDateKey = tmp.CalDateKey  AND p.FactorSetId = tmp.FactorSetId AND p.StreamId = tmp.StreamId AND p.StreamDescription = tmp.StreamDescription
		AND (tmp.DistillationId = 'D070' AND p.DistillationId = 'D050') 
	WHERE tmp.Temp_C <= p.Temp_C + 0.1 AND tmp.StreamId <> 'OtherFeedLiq'

	UPDATE tmp
	SET Temp_C = p.Temp_C + 0.1
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps p ON p.Refnum = tmp.Refnum AND p.CalDateKey = tmp.CalDateKey  AND p.FactorSetId = tmp.FactorSetId AND p.StreamId = tmp.StreamId AND p.StreamDescription = tmp.StreamDescription
		AND (tmp.DistillationId = 'D090' AND p.DistillationId = 'D070') 
	WHERE tmp.Temp_C <= p.Temp_C + 0.1 AND tmp.StreamId <> 'OtherFeedLiq'

	UPDATE tmp
	SET Temp_C = p.Temp_C + 0.1
	FROM @tblRegDistTemps tmp INNER JOIN @tblRegDistTemps p ON p.Refnum = tmp.Refnum AND p.CalDateKey = tmp.CalDateKey  AND p.FactorSetId = tmp.FactorSetId AND p.StreamId = tmp.StreamId AND p.StreamDescription = tmp.StreamDescription
		AND (tmp.DistillationId = 'D100' AND p.DistillationId = 'D090') 
	WHERE tmp.Temp_C <= p.Temp_C + 0.1 AND tmp.StreamId <> 'OtherFeedLiq'
	
	INSERT INTO @ReturnTable(Refnum, CalDateKey, StreamId, StreamDescription, Density_SG, D000, D005, D010, D030, D050, D070, D090, D095, D100)
	SELECT p.Refnum, p.CalDateKey, p.StreamId, p.StreamDescription, p.Density_SG, p.D000, p.D005, p.D010, p.D030, p.D050, p.D070, p.D090, p.D095, p.D100
	FROM (
		SELECT t.Refnum, t.CalDateKey, t.StreamId, t.StreamDescription, t.Density_SG, t.DistillationId, t.Temp_C
		FROM @tblRegDistTemps t
		) u
		PIVOT(
		MAX(Temp_C) FOR DistillationId IN (
			D000,
			D005,
			D010,
			D030,
			D050,
			D070,
			D090,
			D095,
			D100
			)
		) p;

	RETURN;
	
END;