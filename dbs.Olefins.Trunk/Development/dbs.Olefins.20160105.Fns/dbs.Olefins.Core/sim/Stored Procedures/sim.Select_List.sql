﻿CREATE PROCEDURE [sim].[Select_List]
AS
BEGIN

	SELECT
		[RowsRemaining] = CONVERT(VARCHAR(5), COUNT(1) OVER() + 1 - ROW_NUMBER() OVER(ORDER BY [sa].[Refnum])),
		[sa].[Refnum],
		[FactorSetId]	= CONVERT(CHAR(4), [sa].[_StudyYear])
	FROM
		[cons].[SubscriptionsAssets]	[sa]
	WHERE	[sa].[CompanyId] <> 'Solomon'
		AND	[sa].[Refnum] LIKE '%PCH%'
		AND	[sa].[Refnum] = '2013PCH003'
	ORDER BY
		[sa].[Refnum] ASC;

	RETURN 0;

END;