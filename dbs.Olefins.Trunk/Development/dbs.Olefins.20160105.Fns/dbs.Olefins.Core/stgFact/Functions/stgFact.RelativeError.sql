﻿CREATE FUNCTION [stgFact].[RelativeError]
(
	@Numerator		FLOAT,
	@Denominator	FLOAT
)
RETURNS FLOAT
AS
BEGIN
	
	DECLARE @Rtn FLOAT;
	
	IF (@Denominator <> 0.0)
		SET @Rtn = ABS(@Denominator - @Numerator) / @Denominator;
	ELSE
		SET @Rtn = 0.0;

	RETURN @Rtn;

END