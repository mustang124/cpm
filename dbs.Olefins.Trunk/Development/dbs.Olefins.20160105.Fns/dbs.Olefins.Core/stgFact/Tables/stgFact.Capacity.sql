﻿CREATE TABLE [stgFact].[Capacity] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [EthylCapKMTA]     REAL               NULL,
    [EthylCapMTD]      REAL               NULL,
    [EthylMaxCapMTD]   REAL               NULL,
    [PropylCapKMTA]    REAL               NULL,
    [OlefinsCapMTD]    REAL               NULL,
    [OlefinsMaxCapMTD] REAL               NULL,
    [FurnaceCapKMTA]   REAL               NULL,
    [RecycleCapKMTA]   REAL               NULL,
    [SvcFactor]        REAL               NULL,
    [ExpansionPcnt]    REAL               NULL,
    [ExpansionDate]    DATETIME           NULL,
    [CapAllow]         CHAR (1)           NULL,
    [CapLossPcnt]      REAL               NULL,
    [PlantStartUpDate] SMALLINT           NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Capacity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_Capacity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_Capacity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_Capacity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Capacity] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Capacity_u]
	ON [stgFact].[Capacity]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Capacity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Capacity].Refnum		= INSERTED.Refnum;

END;