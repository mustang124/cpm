﻿CREATE PROCEDURE [stgFact].[ExcelMappingDeleteByWorkbookName]
	
	@WorkbookName  NVARCHAR(50)
	
AS
	UPDATE [stgFact].ExcelMapping
	SET Active = 0
	WHERE ExcelMapping.WorkbookName = @WorkbookName

RETURN 0
