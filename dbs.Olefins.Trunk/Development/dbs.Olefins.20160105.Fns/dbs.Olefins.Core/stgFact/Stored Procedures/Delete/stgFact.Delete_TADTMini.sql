﻿CREATE PROCEDURE [stgFact].[Delete_TADTMini]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[TADTMini]
	WHERE [Refnum] = @Refnum;

END;