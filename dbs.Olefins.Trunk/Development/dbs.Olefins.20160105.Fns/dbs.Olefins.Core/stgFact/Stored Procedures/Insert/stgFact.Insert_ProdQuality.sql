﻿CREATE PROCEDURE [stgFact].[Insert_ProdQuality]
(
	@Refnum        VARCHAR (25),
	@FeedProdId    VARCHAR (20),

	@WtPcnt        REAL         = NULL,
	@MolePcnt      REAL         = NULL,
	@H2Value       REAL         = NULL,
	@HeatValue     REAL         = NULL,
	@H2Content     REAL         = NULL,
	@OthProd1Value REAL         = NULL,
	@OthProd2Value REAL         = NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO [stgFact].[ProdQuality]([Refnum], [FeedProdId], [WtPcnt], [MolePcnt], [H2Value], [HeatValue], [H2Content], [OthProd1Value], [OthProd2Value])
	VALUES(@Refnum, @FeedProdId, @WtPcnt, @MolePcnt, @H2Value, @HeatValue, @H2Content, @OthProd1Value, @OthProd2Value);

END;