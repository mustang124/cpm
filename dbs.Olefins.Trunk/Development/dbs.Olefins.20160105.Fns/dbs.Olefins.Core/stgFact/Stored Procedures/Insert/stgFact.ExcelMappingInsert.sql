﻿CREATE PROCEDURE [stgFact].[ExcelMappingInsert]
	
			@StudyYear int = 0,
			@WorkbookName nvarchar(50),
			@WorksheetName nvarchar(32),
			@DataStartCol nvarchar(2),
			@DataEndCol nvarchar(2),
			@DataStartRow int,
			@DataEndRow int,
			@TableRefCol nvarchar(2),
			@ColFldAndValue nvarchar(2),
			@ReadType nvarchar(15),
			@Active bit

AS
	INSERT INTO [stgFact].[ExcelMapping]
        (
		    [StudyYear],
			[WorkbookName], 
			[WorksheetName], 
			[DataStartCol], 
			[DataEndCol],
			[DataStartRow],
			[DataEndRow],
			[TableRefCol],
			[ColFldAndValue],
			[ReadType],
			[Active]
		)
     VALUES
	 (
           @StudyYear
           ,@WorkbookName
           ,@WorksheetName
           ,@DataStartCol
           ,@DataEndCol
           ,@DataStartRow
		   ,@DataEndRow
           ,@TableRefCol
           ,@ColFldAndValue
           ,@ReadType
           ,@Active
	 )


