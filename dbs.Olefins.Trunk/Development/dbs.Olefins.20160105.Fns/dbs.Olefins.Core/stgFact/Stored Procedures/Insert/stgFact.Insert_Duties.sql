﻿CREATE PROCEDURE [stgFact].[Insert_Duties]
(
	@Refnum			VARCHAR (25),
	@FeedProdId		VARCHAR (30),

	@ImpTx			REAL		= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Duties]([Refnum], [FeedProdId], [ImpTx])
	VALUES(@Refnum, @FeedProdId, @ImpTx)

END;