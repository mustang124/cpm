﻿CREATE PROCEDURE [stgFact].[Insert_PracFurnInfo]
(
	@Refnum     VARCHAR (25),
	@FeedProdId VARCHAR (20),

	@Run        REAL         = NULL,
	@Inj        VARCHAR (5)  = NULL,
	@Pretreat   CHAR (1)     = NULL,
	@AntiFoul   CHAR (1)     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PracFurnInfo]([Refnum], [FeedProdId], [Run], [Inj], [Pretreat], [AntiFoul])
	VALUES(@Refnum, @FeedProdId, @Run, @Inj, @Pretreat, @AntiFoul);

END;