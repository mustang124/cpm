﻿CREATE PROCEDURE [stgFact].[Insert_FeedFlex]
(
	@Refnum     VARCHAR (25),
	@FeedProdId VARCHAR (20),

	@PlantCap   REAL         = NULL,
	@ActCap     REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[FeedFlex]([Refnum], [FeedProdId], [PlantCap], [ActCap])
	VALUES(@Refnum, @FeedProdId, @PlantCap, @ActCap);

END;