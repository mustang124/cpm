﻿CREATE PROC [reports].[CalcGroup](@GroupId varchar(25))
AS

SET NOCOUNT ON 

-- Determine what sites are in the group
IF EXISTS (SELECT * FROM fact.TSort WHERE Refnum = @GroupId)
BEGIN
	DELETE FROM reports.GroupPlants WHERE GroupId = @GroupId AND Refnum <> @GroupId
	
	IF NOT EXISTS (SELECT * FROM reports.GroupPlants WHERE GroupId = @GroupId AND Refnum = @GroupId)
		INSERT reports.GroupPlants (GroupId, Refnum)
		VALUES (@GroupId, @GroupId)
END
ELSE BEGIN
	DELETE FROM reports.GroupPlants WHERE GroupId = @GroupId
	
	IF EXISTS (SELECT * FROM reports.ReportGroups WHERE GroupId = @GroupId AND TileRankVariable IS NOT NULL)
		INSERT reports.GroupPlants(GroupId, Refnum)
		SELECT ReportGroups.GroupId, r.Refnum
		FROM reports.ReportGroups ReportGroups INNER JOIN Ranking.RankSpecsLu lu
			ON lu.RankVariable = ReportGroups.TileRankVariable
			AND lu.RankBreak = ReportGroups.TileRankBreak	
			AND lu.BreakValue = ReportGroups.TileBreakValue	
			AND lu.ListId = ReportGroups.ListId
		INNER JOIN Ranking.Rank r ON r.RankSpecsId = lu.RankSpecsId AND r.Tile = ReportGroups.TileTile
		WHERE ReportGroups.GroupId = @GroupId
	ELSE IF EXISTS (SELECT * FROM reports.ReportGroups WHERE GroupId = @GroupId AND TileRankVariable IS NULL)
	BEGIN
		DECLARE @Where varchar(4000)
		SELECT	@Where = 'rl.ListId =  ''' + rg.ListId + ''''
			+ CASE WHEN rg.UserGroup IS NOT NULL THEN ' AND (rl.UserGroup=''' + rg.UserGroup + ''')' ELSE '' END
			+ CASE WHEN rg.StudyYear IS NOT NULL THEN ' AND (TSort.StudyYear = ' + CAST(rg.StudyYear AS varchar(4)) + ')' ELSE '' END
			+ CASE WHEN rg.CountryId IS NOT NULL THEN ' AND (TSort.CountryId = ''' + rg.CountryId + ''')' ELSE '' END
			+ CASE WHEN rg.CapPeerGrp IS NOT NULL THEN ' AND (GENSUM.CapGroup = ' + CAST(rg.CapPeerGrp AS varchar(3)) + ')' ELSE '' END
			+ CASE WHEN rg.TechPeerGrp IS NOT NULL THEN ' AND (GENSUM.TechClass = ' + CAST(rg.TechPeerGrp AS varchar(3)) + ')' ELSE '' END
			+ CASE WHEN rg.FeedClass IS NOT NULL THEN ' AND (GENSUM.FeedClass = ' + CAST(rg.FeedClass AS varchar(3)) + ')' ELSE '' END
			+ CASE WHEN rg.FeedSubClass IS NOT NULL THEN ' AND (GENSUM.FeedSubClass = ''' + CAST(rg.FeedSubClass AS varchar(10)) + ''')' ELSE '' END
			+ CASE WHEN rg.CogenPeerGrp IS NOT NULL THEN ' AND (GENSUM.CoGenGroup = ''' + rg.CogenPeerGrp + ''')' ELSE '' END
			+ CASE WHEN rg.EDCPeerGrp IS NOT NULL THEN ' AND (GENSUM.EDCGroup = ' + CAST(rg.EDCPeerGrp AS varchar(3)) + ')' ELSE '' END
			+ CASE WHEN rg.RegionId IS NOT NULL THEN ' AND (GENSUM.Region = ''' + rg.RegionId + ''')' ELSE '' END
			+ CASE WHEN ISNULL(rg.SpecialCriteria, '') <> '' THEN ' AND (' + SpecialCriteria + ')' ELSE '' END
		FROM reports.ReportGroups rg
		WHERE rg.GroupId = @GroupId AND rg.TileRankVariable IS NULL


		EXEC ('INSERT reports.GroupPlants (GroupId, Refnum)
		SELECT DISTINCT ''' + @GroupId + ''', rl.Refnum
		FROM dbo._RL rl INNER JOIN dbo.TSort TSort ON TSort.Refnum = rl.Refnum
		INNER JOIN dbo.GENSUM GENSUM ON GENSUM.Refnum = TSort.Refnum
		WHERE ' + @Where)

	END
END

/*DELETE FROM output tables*/

/* Calls to procedures or queries to calculate aggregate data*/

/* Loading a table with the refnums for the group is helpful if doing calcs in queries below */
/* It is unnecessary if you are calling stored procedures as they will all need to do this */
/*
DECLARE @Plants TABLE (
	GroupId varchar(25) NOT NULL,
	Refnum varchar(18) NOT NULL)
INSERT @Plants (GroupId, Refnum)
SELECT g.GroupId, g.Refnum
FROM reports.GroupPlants g 
WHERE g.GroupId = @GroupId
*/

