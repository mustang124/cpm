﻿CREATE PROCEDURE [cons].[Insert_SubscriptionsAssets]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [cons].[SubscriptionsAssets]
		(
			[CompanyId],
			[CalDateKey],
			[StudyId],
			[AssetId],
			[SubscriberAssetName],
			[SubscriberAssetDetail]
		)
		SELECT
			[CompanyId]				= [etl].[ConvCompanyId]([t].[CompanyId], [t].[CoName], [t].[Co]),
			[CalDateKey]			= [etl].[ConvDateKey]([t].[StudyYear]),
			[StudyId]				= @StudyId,
			[AssetId]				= [etl].[ConvAssetPCH]([t].[Refnum]),
			[SubscriberAssetName]	= [t].[Loc],
			[SubscriberAssetDetail]	= [t].[Loc]
		FROM
			[stgFact].[TSort]				[t]
		LEFT OUTER JOIN
			[cons].[SubscriptionsAssets]	[s]
				ON	CHARINDEX(RTRIM(LTRIM([t].[Refnum])), RTRIM(LTRIM([s].[Refnum])), 1) = 3
		WHERE	[s].[Refnum]	IS NULL
			AND	[t].[Refnum]	= @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;