﻿CREATE VIEW [inter].[YIRProductAggregate]
WITH SCHEMABINDING
AS
SELECT
	s.FactorSetId,
	s.Refnum,
	s.CalDateKey,
	d.ComponentId,
	SUM(
		CASE d.DescendantOperator
		WHEN '+' THEN + s.Component_kMT
		WHEN '-' THEN - s.Component_kMT
		END)								[Component_kMT],
	SUM(
		CASE d.DescendantOperator
		WHEN '+' THEN + s.Component_WtPcnt
		WHEN '-' THEN - s.Component_WtPcnt
		END)								[Component_WtPcnt]
FROM dim.Component_Bridge				d
INNER JOIN inter.YIRProduct				s
	ON	s.FactorSetId = d.FactorSetId
	AND	s.ComponentId = d.DescendantId
GROUP BY
	s.FactorSetId,
	s.Refnum,
	s.CalDateKey,
	d.ComponentId,
	d.Hierarchy;