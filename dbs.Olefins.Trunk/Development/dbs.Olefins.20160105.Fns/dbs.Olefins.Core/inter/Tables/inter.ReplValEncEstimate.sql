﻿CREATE TABLE [inter].[ReplValEncEstimate] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [Refnum]                VARCHAR (25)       NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [ComponentId]           VARCHAR (42)       NOT NULL,
    [Component_kMT]         REAL               NULL,
    [Component_WtPcnt]      REAL               NULL,
    [YieldEst_RoughWt]      REAL               NOT NULL,
    [Est_C2Yield_EstWtPcnt] REAL               NULL,
    [Act_C2Yield_EstWtPcnt] REAL               NULL,
    [Escalated_ENC_Cur]     REAL               NOT NULL,
    [EthyleneCapBasis]      REAL               NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_ReplValEncEstimate_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_ReplValEncEstimate_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_ReplValEncEstimate_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_ReplValEncEstimate_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReplValEncEstimate] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ReplValEncEstimate_ActYieldEstimate_Pcnt] CHECK ([Act_C2Yield_EstWtPcnt]>=(0.0) AND [Act_C2Yield_EstWtPcnt]<=(100.0)),
    CONSTRAINT [CR_ReplValEncEstimate_Component_kMT] CHECK ([Component_kMT]>=(0.0)),
    CONSTRAINT [CR_ReplValEncEstimate_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_ReplValEncEstimate_EscalatedENC] CHECK ([Escalated_ENC_Cur]>=(0.0)),
    CONSTRAINT [CR_ReplValEncEstimate_Est_C2YieldEstWtPcnt] CHECK ([Est_C2Yield_EstWtPcnt]>=(0.0) AND [Est_C2Yield_EstWtPcnt]<=(100.0)),
    CONSTRAINT [CR_ReplValEncEstimate_EstYieldEstimate_Pcnt] CHECK ([YieldEst_RoughWt]>=(0.0) AND [YieldEst_RoughWt]<=(100.0)),
    CONSTRAINT [CR_ReplValEncEstimate_EthyleneCapBasis] CHECK ([EthyleneCapBasis]>=(0.0)),
    CONSTRAINT [FK_ReplValEncEstimate_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReplValEncEstimate_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_ReplValEncEstimate_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_ReplValEncEstimate_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReplValEncEstimate_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [inter].[t_ReplValEncEstimate_u]
	ON inter.[ReplValEncEstimate]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.[ReplValEncEstimate]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.[ReplValEncEstimate].FactorSetId	= INSERTED.FactorSetId
		AND inter.[ReplValEncEstimate].Refnum		= INSERTED.Refnum
		AND inter.[ReplValEncEstimate].CalDateKey	= INSERTED.CalDateKey
		AND inter.[ReplValEncEstimate].CurrencyRpt	= INSERTED.CurrencyRpt
		AND inter.[ReplValEncEstimate].ComponentId	= INSERTED.ComponentId;

END;