﻿CREATE TABLE [inter].[PricingStreamCountry] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [PricingMethodId]       VARCHAR (42)       NOT NULL,
    [CountryId]             CHAR (3)           NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [Pricing_Cur]           REAL               NOT NULL,
    [MatlBal_Cur]           REAL               NOT NULL,
    [Report_Cur]            REAL               NOT NULL,
    [Supersede_Pricing_Cur] REAL               NULL,
    [Supersede_MatlBal_Cur] REAL               NULL,
    [Supersede_Report_Cur]  REAL               NULL,
    [_Pricing_Amount_Cur]   AS                 (CONVERT([real],coalesce([Supersede_Pricing_Cur],[Pricing_Cur]),(1))) PERSISTED NOT NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],coalesce([Supersede_MatlBal_Cur],[MatlBal_Cur]),(1))) PERSISTED NOT NULL,
    [_Report_Amount_Cur]    AS                 (CONVERT([real],coalesce([Supersede_Report_Cur],[Report_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingStreamCountry_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingStreamCountry_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingStreamCountry_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingStreamCountry_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingStreamCountry] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PricingMethodId] ASC, [CurrencyRpt] ASC, [CountryId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingStreamCountry_MatlBal_Cur] CHECK ([MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamCountry_Pricing_Cur] CHECK ([Pricing_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamCountry_Report_Cur] CHECK ([Report_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamCountry_Supersede_MatlBal_Cur] CHECK ([Supersede_MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamCountry_Supersede_Pricing_Cur] CHECK ([Supersede_Pricing_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamCountry_Supersede_Report_Cur] CHECK ([Supersede_Report_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingStreamCountry_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingStreamCountry_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingStreamCountry_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingStreamCountry_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingStreamCountry_PricingMethod] FOREIGN KEY ([PricingMethodId]) REFERENCES [dim].[PricingMethodLu] ([MethodId]),
    CONSTRAINT [FK_PricingStreamCountry_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO
CREATE NONCLUSTERED INDEX [IX_PricingStreamCountry_PlantPricing]
    ON [inter].[PricingStreamCountry]([StreamId] ASC)
    INCLUDE([FactorSetId], [PricingMethodId], [CountryId], [CalDateKey], [CurrencyRpt], [Pricing_Cur], [Supersede_Pricing_Cur]);


GO
CREATE NONCLUSTERED INDEX [IX_PricingStreamCountry_PlantPricing_Propane]
    ON [inter].[PricingStreamCountry]([StreamId] ASC)
    INCLUDE([FactorSetId], [CountryId], [CalDateKey], [CurrencyRpt], [MatlBal_Cur], [Supersede_MatlBal_Cur]);


GO

CREATE TRIGGER [inter].t_PricingStreamCountry_u
	ON [inter].PricingStreamCountry
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [inter].PricingStreamCountry
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].PricingStreamCountry.FactorSetId	= INSERTED.FactorSetId
		AND [inter].PricingStreamCountry.CountryId		= INSERTED.CountryId
		AND [inter].PricingStreamCountry.CalDateKey		= INSERTED.CalDateKey
		AND [inter].PricingStreamCountry.StreamId		= INSERTED.StreamId
		AND [inter].PricingStreamCountry.CurrencyRpt	= INSERTED.CurrencyRpt;

END;