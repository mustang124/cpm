﻿CREATE ROLE [Admin Support]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Admin Support', @membername = N'AZ';


GO
EXECUTE sp_addrolemember @rolename = N'Admin Support', @membername = N'PED';

