﻿CREATE TABLE [dbo].[Method_LU] (
    [MethodNo] TINYINT   NOT NULL,
    [Name]     CHAR (11) NULL,
    CONSTRAINT [PK_Methods] PRIMARY KEY CLUSTERED ([MethodNo] ASC)
);

