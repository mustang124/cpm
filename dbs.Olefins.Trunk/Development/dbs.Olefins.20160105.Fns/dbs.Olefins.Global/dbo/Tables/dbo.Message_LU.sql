﻿CREATE TABLE [dbo].[Message_LU] (
    [MessageID]   INT           IDENTITY (1, 1) NOT NULL,
    [Severity]    CHAR (1)      NULL,
    [MessageText] VARCHAR (255) NULL,
    [SysAdmin]    BIT           CONSTRAINT [DF_Message_LU_SysAdmin_7__13] DEFAULT (1) NOT NULL,
    [Consultant]  BIT           CONSTRAINT [DF_Message_LU_Consultant_5__13] DEFAULT (0) NOT NULL,
    [Pricing]     BIT           CONSTRAINT [DF_Message_LU_Pricing_6__13] DEFAULT (0) NOT NULL,
    [Audience4]   BIT           CONSTRAINT [DF_Message_LU_Audience4_1__13] DEFAULT (0) NOT NULL,
    [Audience5]   BIT           CONSTRAINT [DF_Message_LU_Audience5_2__13] DEFAULT (0) NOT NULL,
    [Audience6]   BIT           CONSTRAINT [DF_Message_LU_Audience6_3__13] DEFAULT (0) NOT NULL,
    [Audience7]   BIT           CONSTRAINT [DF_Message_LU_Audience7_4__13] DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_Message_LU_8__13] PRIMARY KEY CLUSTERED ([MessageID] ASC)
);

