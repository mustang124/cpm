﻿






CREATE              PROC [reports].[spFacilities](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DECLARE @PostProcess char(1)
SELECT @PostProcess = PostProcess from reports.ReportGroups where GroupId = @GroupId
SELECT @PostProcess = ISNULL(@PostProcess, 'N')

DECLARE @ElecGenCnt int, @NHYTCnt int
SELECT @ElecGencnt = SUM(CASE WHEN ISNULL(ElecGen_MW,0) > 0 THEN 1 ELSE 0 END)
, @NHYTCnt = SUM(CASE WHEN ISNULL(NHYTFeedProcessed_kMT,0) > 0 THEN 1 ELSE 0 END)
FROM @MyGroup r JOIN dbo.Facilities f on f.Refnum=r.Refnum

DELETE FROM reports.Facilities WHERE GroupId = @GroupId
Insert into reports.Facilities (GroupId
, FracFeedCnt
, PyroFurnCnt
, TowerQuenchCnt
, QWaterSysCnt
, TowerGasCnt
, CompGasCnt
, TowerCausticCnt
, DryerProcessCnt
, MethSysCnt
, TowerDemethanizerCnt
, TowerDeethanizerCnt
, AcetylCnt
, TowerEthyleneCnt
, TowerDepropanizerCnt
, TowerPropyleneCnt
, TowerDebutanizerCnt
, CompRefrigPropyleneCnt
, CompRefrigEthyleneCnt
, CompRefrigMethaneCnt
, BoilFiredSteamCnt
, ElecGenDistCnt
, SteamSuperHeatCnt
, TowerCoolCnt
, HydroPurCnt
, EthyLiqCnt
, TowerPyroGasHTCnt
, RGRCnt
, VesselOthCnt
, FacilitiesCnt
, SingleTrain_pcnt
, DualTrain_pcnt
, ThreeTrain_pcnt
, EDCk_FracFeed
, EDCk_FeedStockMix
, EDCk_PyroFurn
, EDCk_Comp
, EDCk_SuppTot
, EDCk_Utly
, EDCk_HydroPur
, EDCk_TowerPyroGasHT
, EDCk
, PyroFurnSpare_Pcnt
, CGCompPower_BHP
, CGCompStages_Count
, CGCompAge_Years
, PRCompPower_BHP
, PRCompAge_Years
, ERCompPower_BHP
, ERCompAge_Years
, MRCompPower_BHP
, MRCompAge_Years
, BoilHPPressure_PSIg
, BoilHPRate_kLbHr
, BoilLPPressure_PSIg
, BoilLPRate_kLbHr
, ElecGen_MW
, NHYTFeedRate_kBsd
, NHYTFeedProcessed_kMT
, NHYTFeedPressure_PSIG
, CompGasHPToRefrigHPRatio
, RV_ISBL
, RVAdj_FeedFlex
, RVAdj_FeedPrep
, RVAdj_Boilers
, RVAdj_OtherOffSite
, RVAdj_FeedSuppl
, RVAdj_DualTrain
, RVAdj_Owner
, RV_USGC
, RV_LocFact
, RV
, IntegrationFeed_Pcnt
, IntegrationEthylene_Pcnt
, IntegrationPropylene_Pcnt
, IntegrationOther_Pcnt
, LogisticsFeed_USDperMT
, LogisticsEthylene_USDperMT
, LogisticsPropylene_USDperMT
, EthyleneCarrier
, ExtractButdiene
, ExtractAromatics
, MTBE
, Isobutylene
, IsobutyleneHighPurity
, Butene1
, Isoprene
, CycloPentadiene
, OtherC5
, PolyPlantShare
, OlefinsDerivatives
, IntegrationRefinery
, Startup_Year
, PlantDensity_SG 
, OSIMPrep_Hrs
)

SELECT GroupId = @GroupId
, FracFeedCnt = [$(DbGlobal)].dbo.WtAvg(FracFeedCnt, 1.0)
, PyroFurnCnt = [$(DbGlobal)].dbo.WtAvg(PyroFurnCnt, 1.0)
, TowerQuenchCnt = [$(DbGlobal)].dbo.WtAvg(TowerQuenchCnt, 1.0)
, QWaterSysCnt = [$(DbGlobal)].dbo.WtAvg(QWaterSysCnt, 1.0)
, TowerGasCnt = [$(DbGlobal)].dbo.WtAvg(TowerGasCnt, 1.0)
, CompGasCnt = [$(DbGlobal)].dbo.WtAvg(CompGasCnt, 1.0)
, TowerCausticCnt = [$(DbGlobal)].dbo.WtAvg(TowerCausticCnt, 1.0)
, DryerProcessCnt = [$(DbGlobal)].dbo.WtAvg(DryerProcessCnt, 1.0)
, MethSysCnt = [$(DbGlobal)].dbo.WtAvg(MethSysCnt, 1.0)
, TowerDemethanizerCnt = [$(DbGlobal)].dbo.WtAvg(TowerDemethanizerCnt, 1.0)
, TowerDeethanizerCnt = [$(DbGlobal)].dbo.WtAvg(TowerDeethanizerCnt, 1.0)
, AcetylCnt = [$(DbGlobal)].dbo.WtAvg(AcetylCnt, 1.0)
, TowerEthyleneCnt = [$(DbGlobal)].dbo.WtAvg(TowerEthyleneCnt, 1.0)
, TowerDepropanizerCnt = [$(DbGlobal)].dbo.WtAvg(TowerDepropanizerCnt, 1.0)
, TowerPropyleneCnt = [$(DbGlobal)].dbo.WtAvg(TowerPropyleneCnt, 1.0)
, TowerDebutanizerCnt = [$(DbGlobal)].dbo.WtAvg(TowerDebutanizerCnt, 1.0)
, CompRefrigPropyleneCnt = [$(DbGlobal)].dbo.WtAvg(CompRefrigPropyleneCnt, 1.0)
, CompRefrigEthyleneCnt = [$(DbGlobal)].dbo.WtAvg(CompRefrigEthyleneCnt, 1.0)
, CompRefrigMethaneCnt = [$(DbGlobal)].dbo.WtAvg(CompRefrigMethaneCnt, 1.0)
, BoilFiredSteamCnt = [$(DbGlobal)].dbo.WtAvg(BoilFiredSteamCnt, 1.0)
, ElecGenDistCnt = [$(DbGlobal)].dbo.WtAvg(ElecGenDistCnt, 1.0)
, SteamSuperHeatCnt = [$(DbGlobal)].dbo.WtAvg(SteamSuperHeatCnt, 1.0)
, TowerCoolCnt = [$(DbGlobal)].dbo.WtAvg(TowerCoolCnt, 1.0)
, HydroPurCnt = [$(DbGlobal)].dbo.WtAvg(HydroPurCnt, 1.0)
, EthyLiqCnt = [$(DbGlobal)].dbo.WtAvg(EthyLiqCnt, 1.0)
, TowerPyroGasHTCnt = [$(DbGlobal)].dbo.WtAvg(TowerPyroGasHTCnt, 1.0)
, RGRCnt = [$(DbGlobal)].dbo.WtAvg(RGRCnt, 1.0)
, VesselOthCnt = [$(DbGlobal)].dbo.WtAvg(VesselOthCnt, 1.0)
, FacilitiesCnt = [$(DbGlobal)].dbo.WtAvg(FacilitiesCnt, 1.0)
, SingleTrain_pcnt = [$(DbGlobal)].dbo.WtAvg(SingleTrain_pcnt, 1.0)
, DualTrain_pcnt = [$(DbGlobal)].dbo.WtAvg(DualTrain_pcnt, 1.0)
, ThreeTrain_pcnt = [$(DbGlobal)].dbo.WtAvg(ThreeTrain_pcnt, 1.0)
, EDCk_FracFeed = [$(DbGlobal)].dbo.WtAvg(EDCk_FracFeed, 1.0)
, EDCk_FeedStockMix = [$(DbGlobal)].dbo.WtAvg(EDCk_FeedStockMix, 1.0)
, EDCk_PyroFurn = [$(DbGlobal)].dbo.WtAvg(EDCk_PyroFurn, 1.0)
, EDCk_Comp = [$(DbGlobal)].dbo.WtAvg(EDCk_Comp, 1.0)
, EDCk_SuppTot = [$(DbGlobal)].dbo.WtAvg(EDCk_SuppTot, 1.0)
, EDCk_Utly = [$(DbGlobal)].dbo.WtAvg(EDCk_Utly, 1.0)
, EDCk_HydroPur = [$(DbGlobal)].dbo.WtAvg(EDCk_HydroPur, 1.0)
, EDCk_TowerPyroGasHT = [$(DbGlobal)].dbo.WtAvg(EDCk_TowerPyroGasHT, 1.0)
, EDCk = [$(DbGlobal)].dbo.WtAvg(EDCk, 1.0)
, PyroFurnSpare_Pcnt = [$(DbGlobal)].dbo.WtAvg(PyroFurnSpare_Pcnt, 1.0)
, CGCompPower_BHP = [$(DbGlobal)].dbo.WtAvgNZ(CGCompPower_BHP, 1.0)
, CGCompStages_Count = [$(DbGlobal)].dbo.WtAvgNZ(CGCompStages_Count, CGCompPower_BHP)
, CGCompAge_Years = [$(DbGlobal)].dbo.WtAvgNZ(CGCompAge_Years, CGCompPower_BHP)
, PRCompPower_BHP = [$(DbGlobal)].dbo.WtAvgNZ(PRCompPower_BHP, 1.0)
, PRCompAge_Years = [$(DbGlobal)].dbo.WtAvgNZ(PRCompAge_Years, PRCompPower_BHP)
, ERCompPower_BHP = [$(DbGlobal)].dbo.WtAvgNZ(ERCompPower_BHP, 1.0)
, ERCompAge_Years = [$(DbGlobal)].dbo.WtAvgNZ(ERCompAge_Years, ERCompPower_BHP)
, MRCompPower_BHP = [$(DbGlobal)].dbo.WtAvgNZ(MRCompPower_BHP, 1.0)
, MRCompAge_Years = [$(DbGlobal)].dbo.WtAvgNZ(MRCompAge_Years, MRCompPower_BHP)
, BoilHPPressure_PSIg = [$(DbGlobal)].dbo.WtAvgNZ(BoilHPPressure_PSIg, BoilHPRate_kLbHr)
, BoilHPRate_kLbHr = [$(DbGlobal)].dbo.WtAvgNZ(BoilHPRate_kLbHr, 1.0)
, BoilLPPressure_PSIg = [$(DbGlobal)].dbo.WtAvgNZ(BoilLPPressure_PSIg, BoilLPRate_kLbHr)
, BoilLPRate_kLbHr = [$(DbGlobal)].dbo.WtAvgNZ(BoilLPRate_kLbHr, 1.0)
, ElecGen_MW = CASE WHEN (@PostProcess = 'Y' and @ElecGenCnt > 3) OR (@PostProcess = 'N' and @ElecGenCnt >0) THEN [$(DbGlobal)].dbo.WtAvgNZ(ElecGen_MW, 1.0) END
, NHYTFeedRate_kBsd = CASE WHEN (@PostProcess = 'Y' and @NHYTCnt > 3) OR (@PostProcess = 'N' and @NHYTCnt >0) THEN [$(DbGlobal)].dbo.WtAvgNZ(NHYTFeedRate_kBsd, 1.0) END
, NHYTFeedProcessed_kMT = CASE WHEN (@PostProcess = 'Y' and @NHYTCnt > 3) OR (@PostProcess = 'N' and @NHYTCnt >0) THEN [$(DbGlobal)].dbo.WtAvgNZ(NHYTFeedProcessed_kMT, 1.0) END
, NHYTFeedPressure_PSIG = CASE WHEN (@PostProcess = 'Y' and @NHYTCnt > 3) OR (@PostProcess = 'N' and @NHYTCnt >0) THEN [$(DbGlobal)].dbo.WtAvgNZ(NHYTFeedPressure_PSIG, 1.0) END
, CompGasHPToRefrigHPRatio = [$(DbGlobal)].dbo.WtAvgNN(CASE WHEN (ISNULL(PRCompPower_BHP,0) +ISNULL(ERCompPower_BHP,0) + ISNULL(MRCompPower_BHP,0) ) > 0 THEN CGCompPower_BHP / (ISNULL(PRCompPower_BHP,0) +ISNULL(ERCompPower_BHP,0) + ISNULL(MRCompPower_BHP,0) ) END, 1.0)
, RV_ISBL = [$(DbGlobal)].dbo.WtAvg(RV_ISBL, 1.0)
, RVAdj_FeedFlex = [$(DbGlobal)].dbo.WtAvg(RVAdj_FeedFlex, 1.0)
, RVAdj_FeedPrep = [$(DbGlobal)].dbo.WtAvg(RVAdj_FeedPrep, 1.0)
, RVAdj_Boilers = [$(DbGlobal)].dbo.WtAvg(RVAdj_Boilers, 1.0)
, RVAdj_OtherOffSite = [$(DbGlobal)].dbo.WtAvg(RVAdj_OtherOffSite, 1.0)
, RVAdj_FeedSuppl = [$(DbGlobal)].dbo.WtAvg(RVAdj_FeedSuppl, 1.0)
, RVAdj_DualTrain = [$(DbGlobal)].dbo.WtAvg(RVAdj_DualTrain, 1.0)
, RVAdj_Owner = [$(DbGlobal)].dbo.WtAvg(RVAdj_Owner, 1.0)
, RV_USGC = [$(DbGlobal)].dbo.WtAvg(RV_USGC, 1.0)
, RV_LocFact = [$(DbGlobal)].dbo.WtAvg(RV_LocFact, 1.0)
, RV = [$(DbGlobal)].dbo.WtAvg(RV, 1.0)
, IntegrationFeed_Pcnt = [$(DbGlobal)].dbo.WtAvg(IntegrationFeed_Pcnt, d.FreshPyroFeed_kMT)
, IntegrationEthylene_Pcnt = [$(DbGlobal)].dbo.WtAvg(IntegrationEthylene_Pcnt, d.EthyleneProd_kMT)
, IntegrationPropylene_Pcnt = [$(DbGlobal)].dbo.WtAvg(IntegrationPropylene_Pcnt, d.OlefinsProd_kMT)
, IntegrationOther_Pcnt = [$(DbGlobal)].dbo.WtAvg(IntegrationOther_Pcnt, d.HvcProd_kMT)
, LogisticsFeed_USDperMT = [$(DbGlobal)].dbo.WtAvg(LogisticsFeed_USDperMT, d.FreshPyroFeed_kMT)
, LogisticsEthylene_USDperMT = [$(DbGlobal)].dbo.WtAvg(LogisticsEthylene_USDperMT, d.EthyleneProd_kMT)
, LogisticsPropylene_USDperMT = [$(DbGlobal)].dbo.WtAvg(LogisticsPropylene_USDperMT, d.PropyleneProd_kMT)
, EthyleneCarrier = [$(DbGlobal)].dbo.WtAvg(EthyleneCarrier, 1.0)
, ExtractButdiene = [$(DbGlobal)].dbo.WtAvg(ExtractButdiene, 1.0)
, ExtractAromatics = [$(DbGlobal)].dbo.WtAvg(ExtractAromatics, 1.0)
, MTBE = [$(DbGlobal)].dbo.WtAvg(MTBE, 1.0)
, Isobutylene = [$(DbGlobal)].dbo.WtAvg(Isobutylene, 1.0)
, IsobutyleneHighPurity = [$(DbGlobal)].dbo.WtAvg(IsobutyleneHighPurity, 1.0)
, Butene1 = [$(DbGlobal)].dbo.WtAvg(Butene1, 1.0)
, Isoprene = [$(DbGlobal)].dbo.WtAvg(Isoprene, 1.0)
, CycloPentadiene = [$(DbGlobal)].dbo.WtAvg(CycloPentadiene, 1.0)
, OtherC5 = [$(DbGlobal)].dbo.WtAvg(OtherC5, 1.0)
, PolyPlantShare = [$(DbGlobal)].dbo.WtAvg(PolyPlantShare, 1.0)
, OlefinsDerivatives = [$(DbGlobal)].dbo.WtAvg(OlefinsDerivatives, 1.0)
, IntegrationRefinery = [$(DbGlobal)].dbo.WtAvg(IntegrationRefinery, 1.0)
, Startup_Year = [$(DbGlobal)].dbo.WtAvg(Startup_Year, 1.0)
, PlantDensity_SG  = [$(DbGlobal)].dbo.WtAvg(PlantDensity_SG, d.FreshPyroFeed_kMT)
, OSIMPrep_Hrs = [$(DbGlobal)].dbo.WtAvg(OSIMPrep_Hrs, 1.0)
FROM @MyGroup r JOIN dbo.Facilities f on f.Refnum=r.Refnum
JOIN dbo.Divisors d on d.Refnum=r.Refnum


SET NOCOUNT OFF

















