﻿





CREATE              PROC [reports].[spInventory](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DECLARE @PostProcess char(1)
SELECT @PostProcess = PostProcess from reports.ReportGroups where GroupId = @GroupId
SELECT @PostProcess = ISNULL(@PostProcess, 'N')

DELETE FROM reports.Inventory WHERE GroupId = @GroupId
Insert into reports.Inventory (GroupId
,StoreFeedAbove_kMT
,StoreFeedUnder_kMT
,StoreInter_kMT
,StoreProdAbove_kMT
,StoreProdUnder_kMT
,Store_kMT
,InvenFeedAbove_kMT
,InvenFeedUnder_kMT
,InvenInter_kMT
,InvenProdAbove_kMT
,InvenProdUnder_kMT
,Inven_kMT
,InvenFeedAbove_Pcnt
,InvenFeedUnder_Pcnt
,InvenInter_Pcnt
,InvenProdAbove_Pcnt
,InvenProdUnder_Pcnt
,Inven_Pcnt
,InvenByTypeFeedAbove_Pcnt
,InvenByTypeFeedUnder_Pcnt
,InvenByTypeInter_Pcnt
,InvenByTypeProdAbove_Pcnt
,InvenByTypeProdUnder_Pcnt
,InvenByType_Pcnt
,InvenPerDaysCap)

SELECT GroupId = @GroupId
,StoreFeedAbove_kMT = [$(DbGlobal)].dbo.WtAvg(StoreFeedAbove_kMT, 1) 
,StoreFeedUnder_kMT = [$(DbGlobal)].dbo.WtAvg(StoreFeedUnder_kMT, 1) 
,StoreInter_kMT		= [$(DbGlobal)].dbo.WtAvg(StoreInter_kMT, 1) 
,StoreProdAbove_kMT = [$(DbGlobal)].dbo.WtAvg(StoreProdAbove_kMT, 1) 
,StoreProdUnder_kMT = [$(DbGlobal)].dbo.WtAvg(StoreProdUnder_kMT, 1) 
,Store_kMT			= [$(DbGlobal)].dbo.WtAvg(Store_kMT, 1)
,InvenFeedAbove_kMT = [$(DbGlobal)].dbo.WtAvg(InvenFeedAbove_kMT, 1) 
,InvenFeedUnder_kMT = [$(DbGlobal)].dbo.WtAvg(InvenFeedUnder_kMT, 1) 
,InvenInter_kMT		= [$(DbGlobal)].dbo.WtAvg(InvenInter_kMT, 1) 
,InvenProdAbove_kMT = [$(DbGlobal)].dbo.WtAvg(InvenProdAbove_kMT, 1) 
,InvenProdUnder_kMT = [$(DbGlobal)].dbo.WtAvg(InvenProdUnder_kMT, 1) 
,Inven_kMT			= [$(DbGlobal)].dbo.WtAvg(Inven_kMT, 1)
,InvenFeedAbove_Pcnt	= ISNULL([$(DbGlobal)].dbo.WtAvg(InvenFeedAbove_Pcnt,	StoreFeedAbove_kMT),0) 
,InvenFeedUnder_Pcnt	= ISNULL([$(DbGlobal)].dbo.WtAvg(InvenFeedUnder_Pcnt,	StoreFeedUnder_kMT),0) 
,InvenInter_Pcnt		= ISNULL([$(DbGlobal)].dbo.WtAvg(InvenInter_Pcnt,		StoreInter_kMT),0) 
,InvenProdAbove_Pcnt	= ISNULL([$(DbGlobal)].dbo.WtAvg(InvenProdAbove_Pcnt,	StoreProdAbove_kMT),0) 
,InvenProdUnder_Pcnt	= ISNULL([$(DbGlobal)].dbo.WtAvg(InvenProdUnder_Pcnt,	StoreProdUnder_kMT),0) 
,Inven_Pcnt				= [$(DbGlobal)].dbo.WtAvg(Inven_Pcnt,			Store_kMT)
,InvenByTypeFeedAbove_Pcnt	= [$(DbGlobal)].dbo.WtAvg(InvenByTypeFeedAbove_Pcnt,	TotInven) 
,InvenByTypeFeedUnder_Pcnt	= [$(DbGlobal)].dbo.WtAvg(InvenByTypeFeedUnder_Pcnt,	TotInven) 
,InvenByTypeInter_Pcnt		= [$(DbGlobal)].dbo.WtAvg(InvenByTypeInter_Pcnt,		TotInven) 
,InvenByTypeProdAbove_Pcnt	= [$(DbGlobal)].dbo.WtAvg(InvenByTypeProdAbove_Pcnt,	TotInven) 
,InvenByTypeProdUnder_Pcnt	= [$(DbGlobal)].dbo.WtAvg(InvenByTypeProdUnder_Pcnt,	TotInven) 
,InvenByType_Pcnt			= [$(DbGlobal)].dbo.WtAvg(InvenByType_Pcnt,			TotInven)
,InvenPerDaysCap			= [$(DbGlobal)].dbo.WtAvg(InvenPerDaysCap,			FreshPyroFeed)
FROM @MyGroup r JOIN dbo.Inventory i on i.Refnum=r.Refnum



--,StoreFeedAbove_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedAboveCnt) <4 THEN NULL ELSE [$(DbGlobal)].dbo.WtAvg(StoreFeedAbove_kMT, 1) END
--,StoreFeedUnder_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedUnderCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(StoreFeedUnder_kMT, 1) END
--,StoreInter_kMT		= CASE WHEN @PostProcess = 'Y' and SUM(StoreInterCnt) <4	 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(StoreInter_kMT, 1) END
--,StoreProdAbove_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreProdAboveCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(StoreProdAbove_kMT, 1) END
--,StoreProdUnder_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreProdUnderCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(StoreProdUnder_kMT, 1) END
--,Store_kMT			= [$(DbGlobal)].dbo.WtAvg(Store_kMT, 1)
--,InvenFeedAbove_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedAboveCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenFeedAbove_kMT, 1) END
--,InvenFeedUnder_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedUnderCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenFeedUnder_kMT, 1) END
--,InvenInter_kMT		= CASE WHEN @PostProcess = 'Y' and SUM(StoreInterCnt) <4	 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenInter_kMT, 1) END
--,InvenProdAbove_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreProdAboveCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenProdAbove_kMT, 1) END
--,InvenProdUnder_kMT = CASE WHEN @PostProcess = 'Y' and SUM(StoreProdUnderCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenProdUnder_kMT, 1) END
--,Inven_kMT			= [$(DbGlobal)].dbo.WtAvg(Inven_kMT, 1)
--,InvenFeedAbove_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedAboveCnt) <4 THEN NULL ELSE  ISNULL([$(DbGlobal)].dbo.WtAvg(InvenFeedAbove_Pcnt,	StoreFeedAbove_kMT),0) END
--,InvenFeedUnder_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedUnderCnt) <4 THEN NULL ELSE  ISNULL([$(DbGlobal)].dbo.WtAvg(InvenFeedUnder_Pcnt,	StoreFeedUnder_kMT),0) END
--,InvenInter_Pcnt		= CASE WHEN @PostProcess = 'Y' and SUM(StoreInterCnt) <4	 THEN NULL ELSE  ISNULL([$(DbGlobal)].dbo.WtAvg(InvenInter_Pcnt,		StoreInter_kMT),0) END
--,InvenProdAbove_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreProdAboveCnt) <4 THEN NULL ELSE  ISNULL([$(DbGlobal)].dbo.WtAvg(InvenProdAbove_Pcnt,	StoreProdAbove_kMT),0) END
--,InvenProdUnder_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreProdUnderCnt) <4 THEN NULL ELSE  ISNULL([$(DbGlobal)].dbo.WtAvg(InvenProdUnder_Pcnt,	StoreProdUnder_kMT),0) END
--,Inven_Pcnt				= [$(DbGlobal)].dbo.WtAvg(Inven_Pcnt,			Store_kMT)
--,InvenByTypeFeedAbove_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedAboveCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenByTypeFeedAbove_Pcnt,	TotInven) END
--,InvenByTypeFeedUnder_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreFeedUnderCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenByTypeFeedUnder_Pcnt,	TotInven) END
--,InvenByTypeInter_Pcnt		= CASE WHEN @PostProcess = 'Y' and SUM(StoreInterCnt) <4	 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenByTypeInter_Pcnt,		TotInven) END
--,InvenByTypeProdAbove_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreProdAboveCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenByTypeProdAbove_Pcnt,	TotInven) END
--,InvenByTypeProdUnder_Pcnt	= CASE WHEN @PostProcess = 'Y' and SUM(StoreProdUnderCnt) <4 THEN NULL ELSE  [$(DbGlobal)].dbo.WtAvg(InvenByTypeProdUnder_Pcnt,	TotInven) END
SET NOCOUNT OFF
















