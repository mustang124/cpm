﻿CREATE View gap.TargetYieldMargins as
select a.GroupId
, TargetGroupId = y.GroupId
, DataType
, ProdLoss
, FreshPyroFeed
, FeedSupp
, PlantFeed
, Recycle
, _FreshRec
, _ProdPyro
, ProdSupp
, _GMPyro
, _GMSupp
, _GM
from gap.YieldMargins y JOIN gap.Peers a on a.TargetId=y.GroupId
JOIN TSort t on t.Refnum=a.GroupId and t.CountryId = y.DataType
Where  a.PeerNumber = 0