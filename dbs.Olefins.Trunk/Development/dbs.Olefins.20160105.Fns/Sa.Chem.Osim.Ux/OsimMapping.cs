﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

using Sa.Chem.Upload.Manager;

namespace Sa.Chem
{
    public partial class OsimMapping : Form
    {

        private int currentStudyYear = 0;
        private string connectionString = string.Empty;
        
        public OsimMapping()
        {
            InitializeComponent();
        }

        public OsimMapping(int studyYear)
        {

            InitializeComponent();

            this.Text = "OSIM Mapping for " + Convert.ToString(studyYear);
            connectionString = Convert.ToString(ConfigurationManager.AppSettings["DatabaseConnection"]);
            currentStudyYear = studyYear;
            LoadGrid(studyYear, true, connectionString);
            pnlEditAdd.Visible = false;
            this.Height = 480;

        }

        private void LoadGrid(int studyYear, bool active, string connectionString)
        {
            Sa.Chem.Upload.Manager.Service service = new Sa.Chem.Upload.Manager.Service();

            try
            {
                List<ExcelMapping> excelMappingList = service.SelectMappingByStudyYear(studyYear, true, connectionString);
                dgMapping.Width = 850;
                dgMapping.DataSource = excelMappingList;
                dgMapping.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SetEditInsertControls(false);
            LoadGrid(currentStudyYear, true, connectionString);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            SetEditInsertControls(false);
          
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            LoadGrid(currentStudyYear, true, connectionString);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SetEditInsertControls(true);
            
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            SetEditInsertControls(true);
        }

        private void SetEditInsertControls(bool show)
        {
            
           
            bool opp = false;

            if (show) 
            {
                opp = false;
                this.Height = 620;
            } else 
            {
                opp = true;
                this.Height = 480;
            }

            pnlEditAdd.Visible = show;
            btnDelete.Visible = opp;
            btnInsert.Visible = opp;
            btnUpdate.Visible = opp;
        }

       
    }
}
