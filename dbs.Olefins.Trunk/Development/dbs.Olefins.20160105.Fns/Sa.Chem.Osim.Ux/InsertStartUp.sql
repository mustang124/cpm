﻿--Use this script to add records to create a new study
Use Olefins
DECLARE	@CalDateKey		INT			= 20171231;
DECLARE	@StudyId		VARCHAR(4)	= 'PCH';
DECLARE	@StudyYear		INT			= 2017;
DECLARE	@FactorSetId	VARCHAR(4)	= CONVERT(CHAR(4), @StudyYear);

--SET NOCOUNT ON;

DECLARE @Subscriptions TABLE
(
	[Refnum]				VARCHAR(12)			NULL	CHECK([Refnum] <> ''),
	[CompanyNameReport]		VARCHAR(42)		NOT	NULL	CHECK([CompanyNameReport] <> ''),
	[AssetNameReport]		VARCHAR(128)	NOT	NULL	CHECK([AssetNameReport] <> ''),
	[AssetIDPri]			VARCHAR(30)		NOT	NULL	CHECK([AssetIDPri] <> ''),
	[CompanyId]				VARCHAR(24)		NOT	NULL	CHECK([CompanyId] <> ''),

	[Coordinator]			VARCHAR(48)			NULL	CHECK([Coordinator] <> ''),
	[EMail]					VARCHAR(254)		NULL	CHECK([EMail] <> ''),
	[CompanyPassWord]		VARCHAR(128)		NULL	CHECK([CompanyPassWord] <> ''),

	[CountryId]				VARCHAR(4)			NULL	CHECK([CountryId] <> ''),
	[Co]					AS [CompanyNameReport],
	[Loc]					AS [AssetNameReport],

	PRIMARY KEY CLUSTERED ([AssetIDPri] ASC),
	UNIQUE NONCLUSTERED ([CompanyId] ASC, [Loc] ASC)
);

INSERT INTO @Subscriptions
(
	[Refnum],
	[CompanyNameReport],
	[AssetNameReport],
	[AssetIDPri],
	[CompanyId],
	[Coordinator],
	[EMail],
	[CompanyPassWord],
	[CountryId]
)
SELECT 
	[t].[Refnum],
	[t].[CompanyNameReport],
	[t].[AssetNameReport],
	[t].[AssetIDPri],
	[t].[CompanyId],
	[t].[Coordinator],
	[t].[EMail],
	[t].[CompanyPassWord],
		[CountryId]	= COALESCE([a].[CountryId], [t].[CountryId])
FROM (VALUES

--('2017PCH011', 'EXXONMOBIL', 'BAYTOWN BOP', '011','EXXONMOBIL','Colby Jones','colby.f.jones@exxonmobil.com','EMCC2015XOM','NULL'),
--('2017PCH152', 'SINOPEC','MAOMING','152','SINOPEC','Liu ranbing','liurb.edri@sinopec.com','yxjx2015','NULL'),
--('2017PCH175','HANWHA TOTAL','DAESAN','175','HANWHA TOTAL','In-Yeob Lee','inyeob.lee@hanwha-total.com','topooda100','NULL'),
--('2017PCH214','SIBUR','TOMSK','214','SIBUR','Alexander Kapustin','alexander.kapustin@honeywell.com','Sibur_2015','NULL'),
--('2017PCH238','FPCC','MAILIAO OL2','238','FPCC','Chun-Lung Lee','chun-lunglee@fpcc.com.tw','N000014092','NULL'),
--('2017PCH997','Solomon','SINGAPORE','997','DOW','Yes','NULL','','NULL'),
--('2017PCH998','Solomon','SINGAPORE','998','NULL','NULL','NULL','','NULL'),
--('2017PCH999','Solomon','SINGAPORE','999','Solomon','Dwane Wilson','dwane.wilson@solomononline.com','','NULL')
 ('2017PCH135', 'HALDIA PETROCHEMICALS LIMITED',	'HALDIA',	'135',	'HALDIA PETROCHEMICALS','NULL',	'NULL',	'NULL',	'IND'0

--also note --   '([t].[Refnum] NOT IN'  -below


SELECT * FROM DIM.company_Lookup

) [t](
	[Refnum],
	[CompanyNameReport],
	[AssetNameReport],
	[AssetIDPri],
	[CompanyId],
	[Coordinator],
	[EMail],
	[CompanyPassWord],
	[CountryId]
)
LEFT OUTER JOIN
	[cons].[Assets]		[a]
		ON	[t].[AssetIDPri]	= [a].[AssetIdPri]
		AND	[a].[AssetIDSec]	IS NULL
WHERE
	([t].[Refnum] NOT IN
	(
		'2013PCH234',
		'2013PCH029P',
		'2013PCH209A',
		'2013PCH223',
		'2013PCH208',
		'2013PCH121',
		'2013PCH041',
		'2013PCH176R',
		'2013PCH109R',
		'2013PCH134R',
		'2013PCH186R',
		'2013PCH201R',
		'2013PCH026',
		'2013PCH043',
		'2013PCH212',
		'2013PCH074'
	)
	OR [t].[Refnum] IS NULL);
--===================================================
INSERT INTO [cons].[Assets]
(
	[AssetIDPri],
	[AssetName],
	[AssetDetail],
	[CountryID]
)
SELECT
	[s].[AssetIDPri],
		[AssetName]		= COALESCE([s].[AssetNameReport], [s].[Loc]),
		[AssetDetail]	= COALESCE([s].[AssetNameReport], [s].[Loc]),
	[s].[CountryId]
FROM
	@Subscriptions		[s]
LEFT OUTER JOIN
	[cons].[Assets]		[a]
		ON	[a].[AssetIDPri]	= [s].[AssetIDPri]
WHERE
	[a].[AssetIDPri]	IS NULL;
--===================================================
INSERT INTO [dim].[Company_LookUp]
(
	[CompanyId],
	[CompanyName],
	[CompanyDetail]
)
SELECT DISTINCT
	[s].[CompanyId],
		[CompanyName]	= COALESCE([s].[Co], [s].[CompanyId]),
		[CompanyDetail]	= COALESCE([s].[Co], [s].[CompanyId])
FROM
	@Subscriptions				[s]
LEFT OUTER JOIN
	[dim].[Company_LookUp]		[c]
		ON	[c].[CompanyId]	= [s].[CompanyId]
WHERE
	[c].[CompanyId]	IS NULL;
--===================================================
MERGE [cons].[SubscriptionsCompanies]	AS [t]
USING
(
	SELECT DISTINCT
		[s].[CompanyId],
			[CalDateKey]				= @CalDateKey,
			[StudyID]					= @StudyId,
			[SubscriberCompanyName]		= [s].[Co],
			[SubscriberCompanyDetail]	= [s].[Co],
			[CompanyNameReport]			= [s].[Co],
		[s].[CompanyPassWord]
	FROM
		@Subscriptions				[s]
)										AS [s]
ON	(	[t].[CompanyID]		= [s].[CompanyID]
	AND	[t].[CalDateKey]	= [s].[CalDateKey]
	AND	[t].[StudyID]		= [s].[StudyID])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[SubscriberCompanyName],
	[SubscriberCompanyDetail],
	[CompanyNameReport],
	[PassWord_PlainText]
)
VALUES
(
	[s].[CompanyId],
	[s].[CalDateKey],
	[s].[StudyId],
	[s].[SubscriberCompanyName],
	[s].[SubscriberCompanyDetail],
	[s].[CompanyNameReport],
	[s].[CompanyPassWord]
)
WHEN MATCHED AND
	(	[t].[SubscriberCompanyName]		<> [s].[SubscriberCompanyName]
	OR	[t].[SubscriberCompanyDetail]	<> [s].[SubscriberCompanyDetail]
	OR	[t].[CompanyNameReport]			<> [s].[CompanyNameReport]
	OR	[t].[PassWord_PlainText]		<> [s].[CompanyPassWord]
	OR	[t].[SubscriberCompanyName]		IS NULL
	OR	[t].[SubscriberCompanyDetail]	IS NULL
	OR	[t].[CompanyNameReport]			IS NULL
	OR	[t].[PassWord_PlainText]		IS NULL) THEN
UPDATE
SET
	[t].[SubscriberCompanyName]		= [s].[SubscriberCompanyName],
	[t].[SubscriberCompanyDetail]	= [s].[SubscriberCompanyDetail],
	[t].[CompanyNameReport]			= [s].[CompanyNameReport],
	[t].[PassWord_PlainText]		= [s].[CompanyPassWord];
--===================================================
MERGE [cons].[SubscriptionsAssets]		AS [t]
USING
(
	SELECT
		[s].[CompanyId],
			[CalDateKey]				= @CalDateKey,
			[StudyID]					= @StudyId,
		[s].[AssetIDPri],
			[SubscriberAssetName]		= [s].[Loc],
			[SubscriberAssetDetail]		= [s].[Loc],
		[s].[Loc]
	FROM
		@Subscriptions				[s]
)										AS [s]
ON	(	[t].[CompanyId]		= [s].[CompanyId]
	AND	[t].[CalDateKey]	= [s].[CalDateKey]
	AND	[t].[StudyID]		= [s].[StudyID]
	AND	[t].[AssetID]		= [s].[AssetIDPri])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[AssetID],
	[SubscriberAssetName],
	[SubscriberAssetDetail],
	[AssetNameReport]
)
VALUES
(
	[s].[CompanyID],
	[s].[CalDateKey],
	[s].[StudyID],
	[s].[AssetIDPri],
	[s].[SubscriberAssetName],
	[s].[SubscriberAssetDetail],
	[s].[Loc]
)
WHEN MATCHED AND 
	(	[t].[CompanyId]		<> [s].[CompanyId]
	OR	[t].[CalDateKey]	<> [s].[CalDateKey]
	OR	[t].[StudyID]		<> [s].[StudyID]
	OR	[t].[AssetID]		<> [s].[AssetIDPri]
	OR	[t].[CompanyId]		IS NULL
	OR	[t].[CalDateKey]	IS NULL
	OR	[t].[StudyID]		IS NULL
	OR	[t].[AssetID]		IS NULL) THEN
UPDATE
SET
	[t].[CompanyId]		= [s].[CompanyId],
	[t].[CalDateKey]	= [s].[CalDateKey],
	[t].[StudyID]		= [s].[StudyID],
	[t].[AssetID]		= [s].[AssetIDPri];
--===================================================
MERGE [cons].[TSortSolomon]				AS [t]
USING
(
	SELECT
			[Refnum]		= CONVERT(CHAR(4), @StudyYear) + @StudyId + [s].[AssetIDPri],
			[CalDateKey]	= @CalDateKey,
			[FactorSetId]	= @FactorSetId,
		[s].[CompanyId]
	FROM
		@Subscriptions	[s]
)											AS [s]
ON	([t].[Refnum]	= [s].[Refnum])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[Refnum],
	[CalDateKey],
	[FactorSetId],
	[ContactCode]
)
VALUES
(
	[s].[Refnum],
	[s].[CalDateKey],
	[s].[FactorSetId],
	[s].[CompanyId]
)
WHEN MATCHED AND 
	(	[t].[CalDateKey]	<> [s].[CalDateKey]
	OR	[t].[FactorSetId]	<> [s].[FactorSetId]
	OR	[t].[ContactCode]	<> [s].[CompanyId]
	OR	[t].[CalDateKey]	IS NULL
	OR	[t].[FactorSetId]	IS NULL
	OR	[t].[ContactCode]	IS NULL) THEN
UPDATE
SET
	[t].[CalDateKey]	= [s].[CalDateKey],
	[t].[FactorSetId]	= [s].[FactorSetId],
	[t].[ContactCode]	= [s].[CompanyId];
--===================================================
MERGE [dbo].[CoContactInfo]				AS [t]
USING
(
	SELECT DISTINCT
		[s].[CompanyId],
			[StudyYear]		= @StudyYear,
			[ContactType]	= 'COORD',
			[FirstName]		= LEFT([s].[Coordinator], CHARINDEX(' ', [s].[Coordinator]) - 1),
			[LastName]		= RIGHT([s].[Coordinator], LEN([s].[Coordinator]) - CHARINDEX(' ', [s].[Coordinator])),
		[s].[EMail]
	FROM
		@Subscriptions			[s]
)										AS [s]
ON	(	[t].[ContactCode]	= [s].[CompanyId]
	AND	[t].[StudyYear]		= [s].[StudyYear]
	AND	[t].[ContactType]	= [s].[ContactType])
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	[ContactCode],
	[StudyYear],
	[ContactType],
	[FirstName],
	[LastName],
	[Email]
)
VALUES
(
	[s].[CompanyId],
	[s].[StudyYear],
	[s].[ContactType],
	[s].[FirstName],
	[s].[LastName],
	[s].[Email]
)
WHEN MATCHED AND
	(	[t].[FirstName]	<> [s].[FirstName]
	OR	[t].[LastName]	<> [s].[LastName]
	OR	[t].[Email]		<> [s].[Email]
	OR	[t].[FirstName]	IS NULL
	OR	[t].[LastName]	IS NULL
	OR	[t].[Email]		IS NULL) THEN
UPDATE
SET
	[FirstName]	= [s].[FirstName],
	[LastName]	= [s].[LastName],
	[Email]		= [s].[Email];
--===================================================
DECLARE @CheckList TABLE
(
	[IssueID]			CHAR(8)				NOT	NULL	CHECK([IssueID] <> ''),
	[IssueTitle]		CHAR(50)			NOT	NULL	CHECK([IssueTitle] <> ''),
	[IssueText]			TEXT				NOT	NULL,
	PRIMARY KEY CLUSTERED ([IssueID] ASC),
	UNIQUE NONCLUSTERED([IssueTitle] ASC)
);

INSERT INTO @CheckList
(
	[IssueID],
	[IssueTitle],
	[IssueText]
)
SELECT
	[t].[IssueID],
	[t].[IssueTitle],
	[t].[IssueText]
FROM (VALUES
('A1', 'OSIM received (JAB)', 'Check when OSIM is received, acknowledged, dragged to Consol (Triggers OSIM is ready to unpack)'),
('A2', 'DV Consultant assigned (JAB)', 'Check when DV Con assignment is recorded in Console for plant.'),
('A3', 'OSIM unpacked (Prog)', 'Check when OSIM is unpacked, uploaded, calced (Triggers OSIM is ready for PYPS/SPSL & DV)'),
('G1', 'EDC/EII reviewed (Cons)', 'Check when EDC/EII/*EI have been reviewed.'),
('G2', 'Indicator Trends (Cons)', 'Trends have been sent for comment.'),
('S1', 'SPSL is ready to run (Cons)', 'Check when the data required for SPSL is complete and you are ready for JSJ to run SPSL.'),
('S2', 'SPSL is ready to be uploaded (JSJ)', 'Check after a sucessful SPSL run and you wish to have SPSL uploaded and CALCS initiated.'),
('V1', 'First stage IDR complete (Cons)', 'Check after first stage IDR is complete'),
('V2', 'IDR Polishing completed (Cons)', 'Check when polishing and ranging are completed'),
('V3', 'FFinal IDR completed (Cons-2/PM)', 'Check when final IDR is completed')
) [t] ( [IssueID], [IssueTitle], [IssueText])
--------------------------------------------------------------------------------------------
MERGE [val].[Checklist]					AS [t]
USING
(
	SELECT
		[Refnum]		= CONVERT(CHAR(4), @StudyYear) + @StudyId + [s].[AssetIDPri],
	[c].[IssueID],
	[c].[IssueTitle],
	[c].[IssueText],
		[PostedBy]		= '-',
		[PostedTime]	= SYSDATETIME(),
		[Completed]		= 'N',
		[SetBy]			= NULL,
		[SetTime]		= NULL

	FROM
		@Subscriptions		[s]
	CROSS JOIN
		@CheckList			[c]
)										AS [s]
ON	(	[t].[Refnum]	= [s].[Refnum]
	AND	[t].[IssueID]	= [s].[IssueID])
WHEN NOT MATCHED BY TARGET THEN
INSERT 
(
	[Refnum],
	[IssueID],
	[IssueTitle],
	[IssueText],
	[PostedBy],
	[PostedTime],
	[Completed],
	[SetBy],
	[SetTime]	
)
VALUES
(
	[s].[Refnum],
	[s].[IssueID],
	[s].[IssueTitle],
	[s].[IssueText],
	[s].[PostedBy],
	[s].[PostedTime],
	[s].[Completed],
	[s].[SetBy],
	[s].[SetTime]
)
WHEN MATCHED AND ([t].[IssueTitle] <> [s].[IssueTitle]) THEN
UPDATE
SET
	[IssueTitle]	= [s].[IssueTitle],
	[IssueText]		= [s].[IssueText];

--For testing
--SELECT 
--	[Subscriptions]	= (SELECT COUNT(1) FROM @Subscriptions),
--	[Tsort]			= (SELECT COUNT(1) FROM [OlefinsDev20151102].[cons].[TSort] [t] WHERE [t].[StudyYear] = @StudyYear);
