USE [Olefins]

DECLARE	@CalDateKey		INT			= 20151231;
DECLARE	@StudyId		VARCHAR(4)	= 'PCH';
DECLARE	@StudyYear		INT			= 2015;
DECLARE	@FactorSetId	VARCHAR(4)	= CONVERT(CHAR(4), @StudyYear);

--SET NOCOUNT ON;

DECLARE @Subscriptions TABLE
(
	[Refnum]				VARCHAR(12)			NULL	CHECK([Refnum] <> ''),
	[CompanyNameReport]		VARCHAR(42)		NOT	NULL	CHECK([CompanyNameReport] <> ''),
	[AssetNameReport]		VARCHAR(128)	NOT	NULL	CHECK([AssetNameReport] <> ''),
	[AssetIDPri]			VARCHAR(30)		NOT	NULL	CHECK([AssetIDPri] <> ''),
	[CompanyId]				VARCHAR(24)		NOT	NULL	CHECK([CompanyId] <> ''),

	[Coordinator]			VARCHAR(48)			NULL	CHECK([Coordinator] <> ''),
	[EMail]					VARCHAR(254)		NULL	CHECK([EMail] <> ''),
	[CompanyPassWord]		VARCHAR(128)		NULL	CHECK([CompanyPassWord] <> ''),

	[CountryId]				VARCHAR(4)			NULL	CHECK([CountryId] <> ''),
	[Co]					AS [CompanyNameReport],
	[Loc]					AS [AssetNameReport],

	PRIMARY KEY CLUSTERED ([AssetIDPri] ASC),
	UNIQUE NONCLUSTERED ([CompanyId] ASC, [Loc] ASC)
);

INSERT INTO @Subscriptions
(
	[Refnum],
	[CompanyNameReport],
	[AssetNameReport],
	[AssetIDPri],
	[CompanyId],
	[Coordinator],
	[EMail],
	[CompanyPassWord],
	[CountryId]
)
SELECT 
	[t].[Refnum],
	[t].[CompanyNameReport],
	[t].[AssetNameReport],
	[t].[AssetIDPri],
	[t].[CompanyId],
	[t].[Coordinator],
	[t].[EMail],
	[t].[CompanyPassWord],
		[CountryId]	= COALESCE([a].[CountryId], [t].[CountryId])
FROM (VALUES
--Sample Proforma from prior study:
--('2013PCH029', 'FLINT HILLS RESOURCES', 'PORT ARTHUR', '029', 'FlintHillsResources', NULL, NULL, NULL, NULL),
--('2013PCH029P', 'FLINT HILLS RESOURCES', 'PORT ARTHUR', '029', 'FlintHillsResources', NULL, NULL, NULL, NULL),
--What Jon sent me:
--('2015PCH029P', 'FLINT HILLS RESOURCES', 'PORT ARTHUR - 2016 Pro Forma', '29P', 'FlintHillsResources', 'Alex Hardt', 'alexander.hardt@fhr.com', 'FHR2015OLEFIN', NULL)
--WHat I will try:
--('2015PCH029P', 'FLINT HILLS RESOURCES', 'PORT ARTHUR - 2016 Pro Forma', '029', 'FlintHillsResources', 'Alex Hardt', 'alexander.hardt@fhr.com', 'FHR2015OLEFIN', NULL)
--('2015PCH243H', 'CP CHEM', 'Cedar Bayou 2 1Q Peer Proforma', '243', 'CPChem', 'Kristen Brown', 'svatekm@cpchem.com', 'CPC15SOLOM', 'USA')
('2015PCH209A','FREP','Fujian 2016','209','FREP','JAB','liheping@fjrep.com','OlefinFREP','CHN')
) [t](
	[Refnum],
	[CompanyNameReport],
	[AssetNameReport],
	[AssetIDPri],
	[CompanyId],
	[Coordinator],
	[EMail],
	[CompanyPassWord],
	[CountryId]
)
LEFT OUTER JOIN
	[cons].[Assets]		[a]
		ON	[t].[AssetIDPri]	= [a].[AssetIdPri]
		AND	[a].[AssetIDSec]	IS NULL
WHERE
	([t].[Refnum] NOT IN
	(
		
		'2013PCH074'
	)
	OR [t].[Refnum] IS NULL);

--===================================================
/*
INSERT INTO [cons].[Assets]
(
	[AssetIDPri],
	[AssetName],
	[AssetDetail],
	[CountryID]
)

SELECT
	[s].[AssetIDPri],
		[AssetName]		= COALESCE([s].[AssetNameReport], [s].[Loc]),
		[AssetDetail]	= COALESCE([s].[AssetNameReport], [s].[Loc]),
	[s].[CountryId]
FROM
	@Subscriptions		[s]
LEFT OUTER JOIN
	[cons].[Assets]		[a]
		ON	[a].[AssetIDPri]	= [s].[AssetIDPri]
WHERE
	[a].[AssetIDPri]	IS NULL;

select * from [cons].[Assets] where assetid  like '%24%'
--there are no %P ids in cons.assets, and since 029 already there, don't insert into cons.assets.
--===================================================

INSERT INTO [dim].[Company_LookUp]
(
	[CompanyId],
	[CompanyName],
	[CompanyDetail]
)

SELECT DISTINCT
	[s].[CompanyId],
		[CompanyName]	= COALESCE([s].[Co], [s].[CompanyId]),
		[CompanyDetail]	= COALESCE([s].[Co], [s].[CompanyId])
FROM
	@Subscriptions				[s]
LEFT OUTER JOIN
	[dim].[Company_LookUp]		[c]
		ON	[c].[CompanyId]	= [s].[CompanyId]
WHERE
	[c].[CompanyId]	IS NULL;
select * from [dim].[Company_LookUp] where companyid like 'CPChem';  --from what consultant sent me
--already exists so don't do insert here
--===================================================
--MERGE [cons].[SubscriptionsCompanies]	AS [t]
select * from [cons].[SubscriptionsCompanies]	where companyid like 'CPChem';
--already exists and don't see any 29P here, so don't do this Merge.
*/
--===================================================
--MERGE [cons].[SubscriptionsAssets]		AS [t]
-- this MERGE didn't work. will do manually below.

select * from [cons].SubscriptionsAssets	where companyid like 'FREP';
--there are some "p" here so need to do this insert.

select 'Do the insert into  [cons].[SubscriptionsAssets]   below';
insert into  [cons].[SubscriptionsAssets] (
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[AssetID],
	[ScenarioID],
	[ScenarioName],
	[SubscriberAssetName],
	[SubscriberAssetDetail],                                      
	[Active],
	[AssetNameReport]
 ) values('FREP','20151231', 'PCH','209','A','PROFORMA', 'FUJIAN 2016�','FUJIAN 2016�',1,'FUJIAN 2016�');


--here is a select to see what is now in there (use as a check):
select * from [cons].[SubscriptionsAssets]	 [t] where [t].[CompanyId]		= 'FREP'	AND	[t].[CalDateKey]	= 20151231 AND	[t].[StudyID]		= 'PCH' AND	[t].[AssetID]		= '209'
--=============================
--MERGE [cons].[TSortSolomon]				AS [t]

--there is a P needed here too so need insert here too.
insert into [cons].[TSortSolomon]
(refnum,caldatekey,consultant,factorsetid,RefDirectory,contactcode,active,region)
values('2015PCH209A','20151231','JAB',2015,'K:\Study\Olefins\2015\Plants\15PCH209A\','FREP',1,'ASIA');
----NEED TO ADD AN INSERT TO Fact.TsortClient'
INSERT INTO FACT.TSORTCLIENT (Refnum,	CalDateKey,	PlantAssetName,	PlantCompanyName, UomID, CurrencyFcn,CurrencyRpt)
Values('2015PCH209A','20151231','FUJIAN 2016�','FREP',	'Metric',	'CNY',	'USD')

--here is a select to see what is now in there (use as a check):
select * from [cons].[TSortSolomon]	where refnum like '2015PCH209%';
--===================================================
--MERGE [dbo].[CoContactInfo]				AS [t]
select * from [dbo].[CoContactInfo]	where contactcode = 'FREP'
--record already there so skipping this MERGE.
--======================
--MERGE [val].[Checklist]	
--the P refnums also get checklist.
select 'Also need to get benchmarking participant P added in MFiles.';

select 'Also need to run this DECLARE -@CheckList TABLE-  and -MERGE [val].[Checklist]-  below'




DECLARE @CheckList TABLE
(
	[IssueID]			CHAR(8)				NOT	NULL	CHECK([IssueID] <> ''),
	[IssueTitle]		CHAR(50)			NOT	NULL	CHECK([IssueTitle] <> ''),
	[IssueText]			TEXT				NOT	NULL,
	PRIMARY KEY CLUSTERED ([IssueID] ASC),
	UNIQUE NONCLUSTERED([IssueTitle] ASC)
);

INSERT INTO @CheckList
(
	[IssueID],
	[IssueTitle],
	[IssueText]
)
SELECT
	[t].[IssueID],
	[t].[IssueTitle],
	[t].[IssueText]
FROM (VALUES
('A1', 'OSIM received (JSJ)', 'Check when OSIM is received, acknowledged, dragged to Consol (Triggers OSIM is ready to unpack)'),
('A2', 'OSIM unpacked (Prog)', 'Check when OSIM is unpacked, uploaded, calced (Triggers OSIM is ready for PYPS/SPSL & DV)'),
('A3', 'DV Consultant assigned (JSJ)', 'Check when DV Con assignment is recorded in Console for plant.'),
('C1', 'Scrubber has been run (DV)', 'Check after you have run Scrubber.'),
('G1', 'EDC/EII reviewed (DV)', 'Check when EDC/EII/' + '*EI have been reviewed.'),
('G2', 'Indicator Trends (DV)', 'Trends have been sent for comment.'),
('P1', 'PYPS is ready to run (DV)', 'Check when the data required for PYPS is complete and you are ready for JSJ to run PYPS.'),
('P2', 'PYPS is ready to be uploaded (JSJ)', 'Check after a sucessful PYPS run and you wish to have PYPS uploaded and CALCS initiated.'),
('S1', 'SPSL is ready to run (DV)', 'Check when the data required for SPSL is complete and you are ready for JSJ to run SPSL.'),
('S2', 'SPSL is ready to be uploaded (JSJ)', 'Check after a sucessful SPSL run and you wish to have SPSL uploaded and CALCS initiated.'),
('V1', 'First stage validation complete (DV)', 'Check after first stage validation is complete'),
('V2', 'Validation Polishing completed (DV)', 'Check when polishing and ranging are completed'),
('V3', 'Final validation completed (DV-2/PM)', 'Check when final validation is completed')
) [t] ( [IssueID], [IssueTitle], [IssueText])

MERGE [val].[Checklist]					AS [t]
USING
(
	SELECT
		[Refnum]		= '2015PCH209A', -- CONVERT(CHAR(4), @StudyYear) + @StudyId + [s].[AssetIDPri],
	[c].[IssueID],
	[c].[IssueTitle],
	[c].[IssueText],
		[PostedBy]		= '-',
		[PostedTime]	= SYSDATETIME(),
		[Completed]		= 'N',
		[SetBy]			= NULL,
		[SetTime]		= NULL

	FROM
		@Subscriptions		[s]
	CROSS JOIN
		@CheckList			[c]
)										AS [s]
ON	(	[t].[Refnum]	= [s].[Refnum]
	AND	[t].[IssueID]	= [s].[IssueID])
WHEN NOT MATCHED BY TARGET THEN
INSERT 
(
	[Refnum],
	[IssueID],
	[IssueTitle],
	[IssueText],
	[PostedBy],
	[PostedTime],
	[Completed],
	[SetBy],
	[SetTime]	
)
VALUES
(
	[s].[Refnum],
	[s].[IssueID],
	[s].[IssueTitle],
	[s].[IssueText],
	[s].[PostedBy],
	[s].[PostedTime],
	[s].[Completed],
	[s].[SetBy],
	[s].[SetTime]
)
WHEN MATCHED AND ([t].[IssueTitle] <> [s].[IssueTitle]) THEN
UPDATE
SET
	[IssueTitle]	= [s].[IssueTitle],
	[IssueText]		= [s].[IssueText];
--For testing
--SELECT 
--	[Subscriptions]	= (SELECT COUNT(1) FROM @Subscriptions),
--	[Tsort]			= (SELECT COUNT(1) FROM [OlefinsDev20151102].[cons].[TSort] [t] WHERE [t].[StudyYear] = @StudyYear);

--here is a select to see what is now in there (use as a check):
select * from [val].[Checklist]	 where refnum = '2015PCH209A';
