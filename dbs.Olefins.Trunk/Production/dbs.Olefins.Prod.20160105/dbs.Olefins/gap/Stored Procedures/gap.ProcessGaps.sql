﻿CREATE PROCEDURE [gap].[ProcessGaps]
(
	@GroupId		VARCHAR(25),
	@TargetId		VARCHAR(25),
	@DatabaseName	NVARCHAR(128)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	DECLARE cPeer CURSOR FOR 		
	SELECT gp.[GroupId], gp.[TargetId], gp.[DatabaseName]
	FROM [gap].[Peers]	gp
	WHERE	gp.[GroupId]		= COALESCE(@GroupId, gp.[GroupId])
		AND	gp.[TargetId]		= COALESCE(@TargetId, gp.[TargetId])
		AND	gp.[DatabaseName]	= COALESCE(@DatabaseName, gp.[DatabaseName]);

	OPEN cPeer;

	FETCH NEXT FROM cPeer INTO @GroupId, @TargetId, @DatabaseName;

	WHILE (@@FETCH_STATUS = 0)		
	BEGIN

		PRINT 'Processing: ' + @GroupId + '::' + @TargetId;

		EXECUTE [gap].[Delete_AnalysisResults] @GroupId, @TargetId;
		EXECUTE [gap].[Insert_AnalysisResults] @GroupId, @TargetId, @DatabaseName;

		FETCH NEXT FROM cPeer INTO @GroupId, @TargetId, @DatabaseName;

	END;

	CLOSE cPeer;
	DEALLOCATE cPeer;

END;