-------------------------------------------------------------------------------
-- Fields (Tables and Views)
DECLARE @TableName		NVARCHAR(MAX);
DECLARE @FieldName		NVARCHAR(MAX);
DECLARE @Description	NVARCHAR(MAX);

SET @TableName = '';	--	<-- Enter table name to find tables that contain this string 
SET @FieldName = 'UserGroup';	--	<-- Enter field name to find fields that contain this string
SET @Description = '';	--	<-- Enter description name to find descriptions that contain this string

SELECT [SchemaName], [TableName], [FieldName], [Description], [DataType], [Length]
FROM (
	SELECT ALL
		s.name AS [SchemaName], t.name AS [TableName], f.name AS [FieldName], e.value AS [Description], y.name AS [DataType], y.max_length AS [Length]
	FROM sys.schemas s
		INNER JOIN sys.tables t ON
				t.[schema_id] = s.[schema_id]
		INNER JOIN sys.columns f ON
				f.[object_id] = t.[object_id]
		INNER JOIN sys.types y ON
				y.[system_type_id] = f.[system_type_id]
			AND y.[user_type_id] = f.[user_type_id] 
		LEFT OUTER JOIN sys.extended_properties e ON
				e.[major_id] = f.[object_id]
			AND e.[minor_id] = f.[column_id]
			AND e.name = 'MS_Description'
	UNION SELECT ALL
		s.name AS [SchemaName], v.name AS [TableName], f.name AS [FieldName], NULL AS [Description], NULL AS [DataType], NULL AS [Length]
	FROM sys.schemas s
		INNER JOIN sys.views v ON
				v.[schema_id] = s.[schema_id]
		INNER JOIN sys.columns f ON
				f.[object_id] = v.[object_id]
	) tbl
WHERE UPPER([TableName]) LIKE UPPER('%' + @TableName + '%') AND UPPER([FieldName]) LIKE UPPER('%' + @FieldName + '%')
	AND (UPPER(CAST([Description] AS NVARCHAR(MAX))) LIKE UPPER('%' + @Description + '%') OR [Description] IS Null)
	
ORDER BY [SchemaName] ASC, [TableName] ASC, [FieldName] ASC;

-------------------------------------------------------------------------------
-- Assemblies
--SELECT * FROM sys.assemblies

--SELECT * FROM sysobjects
--WHERE id IN(
--	SELECT id FROM syscomments WHERE [text] LIKE '%PersCalc%'
--	);
