﻿DECLARE	@ListID				VARCHAR(25)	= '13PCH'
DECLARE	@FactorSetID		VARCHAR(4)	= '2013';
DECLARE	@DataYear			SMALLINT	= 2013;

DECLARE @CalDateKey		INT			= @DataYear * 10000 + 1231
DECLARE @CurrencyFcn	VARCHAR(4)	= 'USD';
DECLARE @CurrencyRpt	VARCHAR(4)	= 'USD';

--EXECUTE [fact].[Insert_Fact_ListId] @ListID, @DataYear, @FactorSetID, @DataYear;

--	EXECUTE [fact].[Delete_OSIM] @ListId;

	--INSERT INTO [fact].[TSortClient]
	--(
	--	[Refnum],
	--	[CalDateKey],
	--	[PlantAssetName],
	--	[PlantCompanyName],
	--	[UomID],
	--	[CurrencyFcn],
	--	[CurrencyRpt]
	--)
	--SELECT
	--	[Refnum]			= [l].[ListID],
	--	[CalDateKey]		= @CalDateKey,
	--	[PlantAssetName]	= [l].[ListName],
	--	[PlantCompanyName]	= 'Solomon',
	--	[UomID]				= 'US',
	--	[CurrencyFcn]		= @CurrencyFcn,
	--	[CurrencyRpt]		= @CurrencyRpt
	--FROM
	--	[cons].[RefListLu]	[l]
	--WHERE
	--	[l].[ListID]	= @ListID

	DECLARE @fpl	[calc].[FoundationPlantList];

	INSERT INTO @fpl
	(
		[FactorSetID],
		[FactorSetName],
		[FactorSet_AnnDateKey],
		[FactorSet_QtrDateKey],
		[FactorSet_QtrDate],
		[Refnum],
		[Plant_AnnDateKey],
		[Plant_QtrDateKey],
		[Plant_QtrDate],
		[CompanyID],
		[AssetID],
		[AssetName],
		[SubscriberCompanyName],
		[PlantCompanyName],
		[SubscriberAssetName],
		[PlantAssetName],
		[CountryID],
		[StateName],
		[EconRegionID],
		[UomID],
		[CurrencyFcn],
		[CurrencyRpt],
		[AssetPassWord_PlainText],
		[StudyYear],
		[DataYear],
		[Consultant],
		[StudyYearDifference],
		[CalQtr]
	)
	SELECT
		fpl.[FactorSetId],
		fpl.[FactorSetName],
		fpl.[FactorSet_AnnDateKey],
		fpl.[FactorSet_QtrDateKey],
		fpl.[FactorSet_QtrDate],
		fpl.[Refnum],
		fpl.[Plant_AnnDateKey],
		fpl.[Plant_QtrDateKey],
		fpl.[Plant_QtrDate],
		fpl.[CompanyId],
		fpl.[AssetIdPri],
		fpl.[AssetName],
		fpl.[SubscriberCompanyName],
		fpl.[PlantCompanyName],
		fpl.[SubscriberAssetName],
		fpl.[PlantAssetName],
		fpl.[CountryId],
		fpl.[StateName],
		fpl.[EconRegionID],
		fpl.[UomId],
		fpl.[CurrencyFcn],
		fpl.[CurrencyRpt],
		fpl.[AssetPassWord_PlainText],
		fpl.[StudyYear],
		fpl.[DataYear],
		fpl.[Consultant],
		fpl.[StudyYearDifference],
		fpl.[CalQtr]
	FROM [calc].[FoundationPlantListSource]						fpl
	WHERE	[fpl].[Refnum] IN (SELECT [l].[Refnum] FROM [cons].[RefList] [l] WHERE [l].[ListId] = @ListID)
		AND	[fpl].[FactorSetId] = @FactorSetId;


	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[l].[StreamId],
			[StreamDescription]	= MAX([c].[StreamDescription]),
			[Quantity_kMT]		= AVG(COALESCE([c].[Quantity_kMT], 0.0))
	FROM
		@fpl							[f]
	CROSS APPLY
		[dim].[Stream_LookUp]			[l]
	LEFT OUTER JOIN
		[fact].[Quantity]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
			AND	[c].[StreamId]			= [l].[StreamId]
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[l].[StreamId]
	HAVING
		AVG(COALESCE([c].[Quantity_kMT], 0.0))	<> 0.0;



ALTER PROCEDURE [fact].[Insert_Fact_ListId]
(
	@ListId				VARCHAR(25),
	@StudyYear			SMALLINT,
	@FactorSetId		VARCHAR(4),
	@DataYear			SMALLINT
)
AS
BEGIN

SET NOCOUNT ON;

	DECLARE @CalDateKey		INT			= @DataYear * 10000 + 1231
	DECLARE @CurrencyFcn	VARCHAR(4)	= 'USD';
	DECLARE @CurrencyRpt	VARCHAR(4)	= 'USD';

	EXECUTE [fact].[Delete_OSIM] @ListId;

	INSERT INTO [fact].[TSortClient]
	(
		[Refnum],
		[CalDateKey],
		[PlantAssetName],
		[PlantCompanyName],
		[UomId],
		[CurrencyFcn],
		[CurrencyRpt]
	)
	SELECT
		[Refnum]			= [l].[ListId],
		[CalDateKey]		= @CalDateKey,
		[PlantAssetName]	= [l].[ListName],
		[PlantCompanyName]	= 'Solomon',
		[UomId]				= 'US',
		[CurrencyFcn]		= @CurrencyFcn,
		[CurrencyRpt]		= @CurrencyRpt
	FROM
		[cons].[RefListLu]	[l]
	WHERE
		[l].[ListId]	= @ListId

	DECLARE @fpl	[calc].[FoundationPlantList];

	INSERT INTO @fpl
	(
		[FactorSetId],
		[FactorSetName],
		[FactorSet_AnnDateKey],
		[FactorSet_QtrDateKey],
		[FactorSet_QtrDate],
		[Refnum],
		[Plant_AnnDateKey],
		[Plant_QtrDateKey],
		[Plant_QtrDate],
		[CompanyId],
		[AssetId],
		[AssetName],
		[SubscriberCompanyName],
		[PlantCompanyName],
		[SubscriberAssetName],
		[PlantAssetName],
		[CountryId],
		[StateName],
		[EconRegionId],
		[UomId],
		[CurrencyFcn],
		[CurrencyRpt],
		[AssetPassWord_PlainText],
		[StudyYear],
		[DataYear],
		[Consultant],
		[StudyYearDifference],
		[CalQtr]
	)
	SELECT
		fpl.[FactorSetId],
		fpl.[FactorSetName],
		fpl.[FactorSet_AnnDateKey],
		fpl.[FactorSet_QtrDateKey],
		fpl.[FactorSet_QtrDate],
		fpl.[Refnum],
		fpl.[Plant_AnnDateKey],
		fpl.[Plant_QtrDateKey],
		fpl.[Plant_QtrDate],
		fpl.[CompanyId],
		fpl.[AssetIdPri],
		fpl.[AssetName],
		fpl.[SubscriberCompanyName],
		fpl.[PlantCompanyName],
		fpl.[SubscriberAssetName],
		fpl.[PlantAssetName],
		fpl.[CountryId],
		fpl.[StateName],
		fpl.[EconRegionId],
		fpl.[UomId],
		fpl.[CurrencyFcn],
		fpl.[CurrencyRpt],
		fpl.[AssetPassWord_PlainText],
		fpl.[StudyYear],
		fpl.[DataYear],
		fpl.[Consultant],
		fpl.[StudyYearDifference],
		fpl.[CalQtr]
	FROM [calc].[FoundationPlantListSource]						fpl
	WHERE	[fpl].[Refnum] IN (SELECT [l].[Refnum] FROM [cons].[RefList] [l] WHERE [l].[ListId] = @ListId)
		AND	[fpl].[FactorSetId] = ISNULL(@FactorSetId, [fpl].[FactorSetId])

	INSERT INTO [fact].[Capacity]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[Capacity_kMT],
		[Stream_MTd],
		[Record_MTd]
	)
	SELECT
			[Refnum]		= @ListId,
			[CalDateKey]	= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
			[Capacity_kMT]	= AVG([c].[Capacity_kMT]),
			[Stream_MTd]	= AVG([c].[Stream_MTd]),
			[Record_MTd]	= AVG([c].[Record_MTd])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[Capacity]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId];

	INSERT INTO [fact].[CapacityAttributes]
	(
		[Refnum],
		[CalDateKey],
		[Expansion_Pcnt],
		[Expansion_Date],
		[CapAllow_Bit],
		[CapLoss_Pcnt],
		[StartUp_Year]
	)
	SELECT
		[c].[Refnum],
		[c].[CalDateKey],
			[Expansion_Pcnt]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[Expansion_Pcnt], [w].[Capacity_kMT]),
			[Expansion_Date]	= DATEADD(DAY, SUM([c].[DateDifference] * [w].[Capacity_kMT]) / SUM([w].[Capacity_kMT]), MIN([c].[Expansion_Date])),
			[CapAllow_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[CapAllow_Bit]))),
			[CapLoss_Pcnt]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[CapLoss_Pcnt], [w].[Capacity_kMT]),
			[StartUp_Year]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[StartUp_Year], [w].[Capacity_kMT])
	FROM(
		SELECT
				[Refnum]			= @ListId,
				[CalDateKey]		= [f].[FactorSet_QtrDateKey],
			[c].[Expansion_Pcnt],
			[c].[Expansion_Date],
				[DateDifference]	= DATEDIFF(DAY, MIN([c].[Expansion_Date]) OVER(), [c].[Expansion_Date]),
			[c].[CapAllow_Bit],
			[c].[CapLoss_Pcnt],
			[c].[StartUp_Year]
		FROM
			@fpl							[f]
		INNER JOIN
			[fact].[CapacityAttributes]		[c]
				ON	[c].[Refnum]			= [f].[Refnum]
				AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
		WHERE
			[f].[CalQtr]	= 4
		) [c]
	INNER JOIN
		[fact].[Capacity]				[w]
			ON	[w].[Refnum]			= [c].[Refnum]
			AND	[w].[CalDateKey]		= [c].[CalDateKey]
			AND	[w].[StreamId]			= 'FreshPyroFeed'
	GROUP BY
		[c].[Refnum],
		[c].[CalDateKey];

	INSERT INTO [fact].[CapacityGrowth]
	(
		[Refnum],
		[CalDateKey],
		[StudyYear],
		[StreamId],
		[Prod_kMT],
		[Capacity_kMT]
	)
	SELECT
			[Refnum]		= @ListId,
		[c].[CalDateKey],
			[StudyYear]		= @StudyYear,
		[c].[StreamId],
			[Prod_kMT]		= [GlobalDB].[dbo].[WtAvgNN]([c].[Prod_kMT], [w].[Capacity_kMT]),
			[Capacity_kMT]	= [GlobalDB].[dbo].[WtAvgNN]([c].[Capacity_kMT], [w].[Capacity_kMT])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[CapacityGrowth]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
	INNER JOIN
		[fact].[Capacity]				[w]
			ON	[w].[Refnum]			= [c].[Refnum]
			AND	[w].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
			AND	[w].[StreamId]			= 'FreshPyroFeed'
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[c].[CalDateKey],
		[c].[StreamId];

	INSERT INTO [fact].[Facilities]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[Unit_Count]
	)
	SELECT
			[Refnum]		= @ListId,
			[CalDateKey]	= [f].[FactorSet_QtrDateKey],
			[FacilityId]	= [c].[FacilityId],
			[Unit_Count]	= MAX([c].[Unit_Count])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[Facilities]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[FacilitiesCompressors]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[Age_Years],
		[Power_BHP],
		[Stages_Count],
		[DriverId],
		[CoatingId]
	)
	SELECT
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[Age_Years],
		[Power_BHP],
		[Stages_Count],
		[DriverId]		= (SELECT TOP 1
								[c].[DriverId]
							FROM
								@fpl							[f]
							INNER JOIN
								[fact].[FacilitiesCompressors]	[c]
									ON	[c].[Refnum]			= [f].[Refnum]
									AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
							WHERE	[c].[DriverId] IS NOT NULL
								AND	[f].[CalQtr]	= 4
							GROUP BY
								[c].[DriverId]
							ORDER BY
								COUNT(1) DESC),
		[CoatingId]		= (SELECT TOP 1
								[c].[CoatingId]
							FROM
								@fpl							[f]
							INNER JOIN
								[fact].[FacilitiesCompressors]	[c]
									ON	[c].[Refnum]			= [f].[Refnum]
									AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
							WHERE	[c].[CoatingId] IS NOT NULL
								AND	[f].[CalQtr]	= 4
							GROUP BY
								[c].[CoatingId]
							ORDER BY
								COUNT(1) DESC)
	FROM (
		SELECT
				[Refnum]		= @ListId,
				[CalDateKey]	= [f].[FactorSet_QtrDateKey],
				[FacilityId]	= [c].[FacilityId],
				[Age_Years]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Age_Years], [c].[Power_BHP]),
				[Power_BHP]		= AVG([c].[Power_BHP]),
				[Stages_Count]	= ROUND([GlobalDB].[dbo].[WtAvgNZ]([c].[Stages_Count], [c].[Power_BHP]), 0)
		FROM
			@fpl							[f]
		INNER JOIN
			[fact].[FacilitiesCompressors]	[c]
				ON	[c].[Refnum]			= [f].[Refnum]
				AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
		WHERE
			[f].[CalQtr]	= 4
		GROUP BY
			[f].[FactorSet_QtrDateKey],
			[c].[FacilityId]
		) [t];

	INSERT INTO [fact].[FacilitiesCoolingWater]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[FanHorsepower_bhp],
		[PumpHorsepower_bhp],
		[FlowRate_TonneDay]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[FacilityId]			= [c].[FacilityId],
			[FanHorsepower_bhp]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[FanHorsepower_bhp], 1.0),
			[PumpHorsepower_bhp]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[PumpHorsepower_bhp], 1.0),
			[FlowRate_TonneDay]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[FlowRate_TonneDay], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FacilitiesCoolingWater]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[FacilitiesFeedFrac]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[StreamId],
		[FeedRate_kBsd],
		[FeedProcessed_kMT],
		[FeedDensity_SG]
	)
	SELECT
			[Refnum]		= @ListId,
			[CalDateKey]	= [f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
			[StreamId]		= (SELECT TOP 1
									[c].[StreamId]
								FROM
									@fpl							[f]
								INNER JOIN
									[fact].[FacilitiesFeedFrac]	[c]
										ON	[c].[Refnum]			= [f].[Refnum]
										AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
								WHERE	[c].[StreamId] IS NOT NULL
									AND	[f].[CalQtr]	= 4
								GROUP BY
									[c].[StreamId]
								ORDER BY
									COUNT(1) DESC),

			[FeedRate_kBsd]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedRate_kBsd], 1.0),
			[FeedProcessed_kMT]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedProcessed_kMT], 1.0),
			[FeedDensity_SG]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedDensity_SG], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FacilitiesFeedFrac]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[FacilitiesHydroTreat]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[HTTypeId],
		[FeedRate_kBsd],
		[FeedPressure_PSIg],
		[FeedProcessed_kMT],
		[FeedDensity_SG]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
			[HTTypeId]			= (SELECT TOP 1
										[c].[HTTypeId]
									FROM
										@fpl							[f]
									INNER JOIN
										[fact].[FacilitiesHydroTreat]	[c]
											ON	[c].[Refnum]			= [f].[Refnum]
											AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
									WHERE	[c].[HTTypeId] IS NOT NULL
										AND	[f].[CalQtr]	= 4
									GROUP BY
										[c].[HTTypeId]
									ORDER BY
										COUNT(1) DESC),
			[FeedRate_kBsd]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedRate_kBsd], 1.0),
			[FeedPressure_PSIg]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedPressure_PSIg], 1.0),
			[FeedProcessed_kMT]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedProcessed_kMT], 1.0),
			[FeedDensity_SG]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedDensity_SG], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FacilitiesHydroTreat]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[FacilitiesMisc]
	(
		[Refnum],
		[CalDateKey],
		[PyroFurnSpare_Pcnt],
		[ElecGen_MW],
		[StreamId],
		[StreamShip_Pcnt],
		[AcetylLocId]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[PyroFurnSpare_Pcnt]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[PyroFurnSpare_Pcnt], 1.0),
			[ElecGen_MW]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[ElecGen_MW], 1.0),
			[StreamId]				= (SELECT TOP 1
											[c].[StreamId]
										FROM
											@fpl							[f]
										INNER JOIN
											[fact].[FacilitiesMisc]			[c]
												ON	[c].[Refnum]			= [f].[Refnum]
												AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
										WHERE	[c].[StreamId] IS NOT NULL
											AND	[f].[CalQtr]	= 4
										GROUP BY
											[c].[StreamId]
										ORDER BY
											COUNT(1) DESC),
			[StreamShip_Pcnt]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[StreamShip_Pcnt], 1.0),
			[AcetylLocId]			= (SELECT TOP 1
											[c].[AcetylLocId]
										FROM
											@fpl							[f]
										INNER JOIN
											[fact].[FacilitiesMisc]			[c]
												ON	[c].[Refnum]			= [f].[Refnum]
												AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
										WHERE	[c].[AcetylLocId] IS NOT NULL
											AND	[f].[CalQtr]	= 4
										GROUP BY
											[c].[AcetylLocId]
										ORDER BY
											COUNT(1) DESC)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FacilitiesMisc]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[FacilitiesPressure]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[Pressure_PSIg],
		[Rate_kLbHr]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
			[Pressure_PSIg]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Pressure_PSIg], 1.0),
			[Rate_kLbHr]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Rate_kLbHr], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FacilitiesPressure]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[Apc]
	(
		[Refnum],
		[CalDateKey],
		[Loop_Count],
		[ClosedLoopAPC_Pcnt],
		[ApcMonitorId], 
		[ApcAge_Years], 
		[OffLineLinearPrograms_Bit], 
		[OffLineNonLinearSimulators_Bit], 
		[OffLineNonLinearInput_Bit],
		[OnLineClosedLoop_Bit],
		[OffLineComparison_Days],
		[OnLineComparison_Days],
		[PriceUpdateFreq_Days],
		[ClosedLoopModelCokingPred_Bit],
		[OptimizationOffLine_Mnths],
		[OptimizationPlanFreq_Mnths],
		[OptimizationOnLineOper_Mnths],
		[OnLineInput_Count],
		[OnLineVariable_Count],
		[OnLineOperation_Pcnt],
		[OnLinePointCycles_Count],
		[PyroFurnModeledIndependent_Bit],
		[OnLineAutoModeSwitch_Bit],
		[DynamicResponse_Count],
		[StepTestSimple_Days],
		[StepTestComplex_Days],
		[StepTestUpdateFreq_Mnths],
		[AdaptiveAlgorithm_Count],
		[EmbeddedLP_Count],
		[MaxVariablesManipulated_Count],
		[MaxVariablesConstraint_Count],
		[MaxVariablesControlled_Count]
	)
	SELECT
			[Refnum]							= @ListId,
			[CalDateKey]						= [f].[FactorSet_QtrDateKey],

			[Loop_Count]						= [GlobalDB].[dbo].[WtAvgNZ]([c].[Loop_Count], 1.0),
			[ClosedLoopAPC_Pcnt]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[ClosedLoopAPC_Pcnt], 1.0),
			[ApcMonitorId]						= (SELECT TOP 1
														[c].[ApcMonitorId]
													FROM
														@fpl							[f]
													INNER JOIN
														[fact].[Apc]			[c]
															ON	[c].[Refnum]			= [f].[Refnum]
															AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
													WHERE	[c].[ApcMonitorId] IS NOT NULL
														AND	[f].[CalQtr]	= 4
													GROUP BY
														[c].[ApcMonitorId]
													ORDER BY
														COUNT(1) DESC),
			[ApcAge_Years]						= [GlobalDB].[dbo].[WtAvgNZ]([c].[ApcAge_Years], 1.0),

			[OffLineLinearPrograms_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[OffLineLinearPrograms_Bit]))),
			[OffLineNonLinearSimulators_Bit]	= CONVERT(BIT, MAX(CONVERT(INT, [c].[OffLineNonLinearSimulators_Bit]))),
			[OffLineNonLinearInput_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[OffLineNonLinearInput_Bit]))),
			[OnLineClosedLoop_Bit]				= CONVERT(BIT, MAX(CONVERT(INT, [c].[OnLineClosedLoop_Bit]))),

			[OffLineComparison_Days]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[OffLineComparison_Days], 1.0),
			[OnLineComparison_Days]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[OnLineComparison_Days], 1.0),
			[PriceUpdateFreq_Days]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[PriceUpdateFreq_Days], 1.0),
		
			[ClosedLoopModelCokingPred_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[ClosedLoopModelCokingPred_Bit]))),

			[OptimizationOffLine_Mnths]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[OptimizationOffLine_Mnths], 1.0),
			[OptimizationPlanFreq_Mnths]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[OptimizationPlanFreq_Mnths], 1.0),
			[OptimizationOnLineOper_Mnths]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[OptimizationOnLineOper_Mnths], 1.0),
			[OnLineInput_Count]					= [GlobalDB].[dbo].[WtAvgNZ]([c].[OnLineInput_Count], 1.0),
			[OnLineVariable_Count]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[OnLineVariable_Count], 1.0),
			[OnLineOperation_Pcnt]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[OnLineOperation_Pcnt], 1.0),
			[OnLinePointCycles_Count]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[OnLinePointCycles_Count], 1.0),

			[PyroFurnModeledIndependent_Bit]	= CONVERT(BIT, MAX(CONVERT(INT, [c].[PyroFurnModeledIndependent_Bit]))),
			[OnLineAutoModeSwitch_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[OnLineAutoModeSwitch_Bit]))),

			[DynamicResponse_Count]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[DynamicResponse_Count], 1.0),
			[StepTestSimple_Days]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[StepTestSimple_Days], 1.0),
			[StepTestComplex_Days]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[StepTestComplex_Days], 1.0),
			[StepTestUpdateFreq_Mnths]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[StepTestUpdateFreq_Mnths], 1.0),
			[AdaptiveAlgorithm_Count]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[AdaptiveAlgorithm_Count], 1.0),
			[EmbeddedLP_Count]					= [GlobalDB].[dbo].[WtAvgNZ]([c].[EmbeddedLP_Count], 1.0),
			[MaxVariablesManipulated_Count]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[MaxVariablesManipulated_Count], 1.0),
			[MaxVariablesConstraint_Count]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[MaxVariablesConstraint_Count], 1.0),
			[MaxVariablesControlled_Count]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[MaxVariablesControlled_Count], 1.0)

	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[Apc]					[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[ApcControllers]
	(
		[Refnum],
		[CalDateKey],
		[ApcId],
		[Controller_Count]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[ApcId],
		[Controller_Count]		= ROUND([GlobalDB].[dbo].[WtAvgNZ]([c].[Controller_Count], 1.0), 1)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ApcControllers]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[ApcId];

	INSERT INTO [fact].[ApcExistance]
	(
		[Refnum],
		[CalDateKey],
		[ApcId],
		[Mpc_Bit],
		[Sep_Bit]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[ApcId],
			[Mpc_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[Mpc_Bit]))),
			[Sep_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[Sep_Bit])))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ApcExistance]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[ApcId];

	INSERT INTO [fact].[ApcOnLinePcnt]
	(
		[Refnum],
		[CalDateKey],
		[ApcId],
		[OnLine_Pcnt]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[ApcId],
			[OnLine_Pcnt]		= AVG([c].[OnLine_Pcnt])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ApcOnLinePcnt]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[ApcId];

	INSERT INTO [fact].[Quantity]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[Quantity_kMT]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
			[Quantity_kMT]		= SUM([c].[Quantity_kMT]) / CONVERT(REAL, (SELECT COUNT(1) FROM @fpl [f] WHERE [f].[CalQtr] = 4))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[Quantity]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription];

	--INSERT INTO [fact].[Quantity]
	--(
	--	[Refnum],
	--	[CalDateKey],
	--	[StreamId],
	--	[StreamDescription],
	--	[Quantity_kMT]
	--)
	--SELECT
	--		[Refnum]			= @ListId,
	--		[CalDateKey]		= [f].[FactorSet_QtrDateKey],
	--	[l].[StreamId],
	--		[StreamDescription]	= MAX([c].[StreamDescription]),
	--		[Quantity_kMT]		= AVG(COALESCE([c].[Quantity_kMT], 0.0))
	--FROM
	--	@fpl							[f]
	--CROSS APPLY
	--	[dim].[Stream_LookUp]			[l]
	--LEFT OUTER JOIN
	--	[fact].[Quantity]				[c]
	--		ON	[c].[Refnum]			= [f].[Refnum]
	--		AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	--		AND	[c].[StreamId]			= [l].[StreamId]
	--GROUP BY
	--	[f].[FactorSet_QtrDateKey],
	--	[l].[StreamId]
	--HAVING
	--	AVG(COALESCE([c].[Quantity_kMT], 0.0))	<> 0.0;

	INSERT INTO [fact].[CompositionQuantity]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[ComponentId],
		[Component_WtPcnt]
	)
	SELECT
		[t].[Refnum],
		[t].[CalDateKey],
		[t].[StreamId],
		[t].[StreamDescription],
		[t].[ComponentId],
			[Component_WtPcnt]		= [t].[Component_kMT] / SUM([t].[Component_kMT]) OVER(PARTITION BY [t].[Refnum], [t].[CalDateKey], [t].[StreamId], [t].[StreamDescription]) * 100.0
	FROM (
		SELECT
				[Refnum]			= @ListId,
				[CalDateKey]		= [f].[FactorSet_QtrDateKey],
			[c].[StreamId],
			[c].[StreamDescription],
			[c].[ComponentId],
				[Component_kMT]		= SUM([c].[Component_WtPcnt] * ABS([q].[Quantity_kMT]) / 100.0)
		FROM
			@fpl								[f]
		INNER JOIN
			[fact].[CompositionQuantity]		[c]
				ON	[c].[Refnum]			= [f].[Refnum]
				AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
		INNER JOIN
			[fact].[StreamQuantityAggregate]	[q]	WITH(NOEXPAND)
				ON	[q].[Refnum]			= [f].[Refnum]
				AND	[q].[FactorSetId]		= [f].[FactorSetId]
				AND	[q].[StreamId]			= [c].[StreamId]
				AND	([q].[StreamDescription]	= [c].[StreamDescription] OR [q].[StreamDescription] IS NULL)
		WHERE
			[f].[CalQtr]	= 4
		GROUP BY
			[f].[FactorSet_QtrDateKey],
			[c].[StreamId],
			[c].[StreamDescription],
			[c].[ComponentId]
		) [t];

	INSERT INTO [fact].[QuantityLHValue]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[LHValue_MBtuLb]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
			[LHValue_MBtuLb]		= AVG([c].[LHValue_MBtuLb])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[QuantityLHValue]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription];

	INSERT INTO [fact].[QuantitySuppRecovery]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[Recovered_WtPcnt]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
			[Recovered_WtPcnt]		= AVG([c].[Recovered_WtPcnt])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[QuantitySuppRecovery]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription];

	INSERT INTO [fact].[QuantitySuppRecycled]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[ComponentId],
		[Recycled_WtPcnt]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[ComponentId],
			[Recycled_WtPcnt]		= AVG([c].[Recycled_WtPcnt])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[QuantitySuppRecycled]		[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[ComponentId];

	INSERT INTO [fact].[QuantityValue]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
		[c].[CurrencyRpt],
			[Amount_Cur]			= AVG([c].[Amount_Cur])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[QuantityValue]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[EnergyCogen]
	(
		[Refnum],
		[CalDateKey],
		[HasCogen_Bit],
		[Thermal_Pcnt]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
			[HasCogen_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[HasCogen_Bit]))),
			[Thermal_Pcnt]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Thermal_Pcnt], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergyCogen]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]		= 4
		AND	[c].[Thermal_Pcnt]	<> 0.0
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[EnergyCogenFacilities]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[Unit_Count],
		[Capacity_MW],
		[Efficiency_Pcnt]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
			[FacilityId]		= (SELECT TOP 1
										[c].[FacilityId]
									FROM
										@fpl							[f]
									INNER JOIN
										[fact].[EnergyCogenFacilities]	[c]
											ON	[c].[Refnum]			= [f].[Refnum]
											AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
									WHERE	[f].[CalQtr]	= 4
									GROUP BY
										[c].[FacilityId]
									ORDER BY
										COUNT(1) DESC),
			[Unit_Count]		= MAX([c].[Unit_Count]),
			[Capacity_MW]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Capacity_MW], 1.0),
			[Efficiency_Pcnt]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[Efficiency_Pcnt], [c].[Capacity_MW])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergyCogenFacilities]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[EnergyCogenPcnt]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[FacilityId],
		[CogenEnergy_Pcnt]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[FacilityId],
			[CogenEnergy_Pcnt]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[CogenEnergy_Pcnt], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergyCogenPcnt]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]			= 4
		AND	[c].[CogenEnergy_Pcnt]	> 0.0
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[FacilityId];

	INSERT INTO [fact].[EnergyComposition]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[ComponentId],
		[Component_WtPcnt]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[ComponentId],
			[Component_WtPcnt]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[Component_WtPcnt], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergyComposition]		[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]			= 4
		AND	[c].[Component_WtPcnt]	> 0.0
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[ComponentId];

	INSERT INTO [fact].[EnergyElec]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[CurrencyRpt],
		[Amount_MWh],
		[Rate_BTUkWh],
		[Price_Cur]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt],
			[Amount_MWh]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Amount_MWh], 1.0),
			[Rate_BTUkWh]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Rate_BTUkWh], 1.0),
			[Price_Cur]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[Price_Cur], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergyElec]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]	= 4
		AND	[c].[CurrencyRpt]		= @CurrencyRpt
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[EnergyLHValue]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[LHValue_MBTU]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
			[LHValue_MBTU]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[LHValue_MBTU], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergyLHValue]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]		= 4
		AND	[c].[LHValue_MBTU]	> 0.0
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId];

	INSERT INTO [fact].[EnergyPrice]
	(
		[Refnum],
		[CalDateKey],
		[CurrencyRpt],
		[AccountId],
		[Amount_Cur]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[CurrencyRpt],
		[c].[AccountId],
			[Amount_Cur]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Amount_Cur], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergyPrice]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]	= 4
		AND	[c].[CurrencyRpt]		= @CurrencyRpt
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[CurrencyRpt],
		[c].[AccountId];

	INSERT INTO [fact].[EnergySteam]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[Amount_kMT],
		[Pressure_Barg],
		[Temp_C]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
			[Amount_kMT]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Amount_kMT], 1.0),
			[Pressure_Barg]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Pressure_Barg], 1.0),
			[Temp_C]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[Temp_C], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[EnergySteam]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId];

	INSERT INTO [fact].[FeedSel]
	(
		[Refnum],
		[CalDateKey],
		[MinParaffin_Pcnt],
		[MaxVaporPresFRS_RVP],
		[YieldPattern_Count],
		[YieldPatternLP_Count],
		[YieldGroup_Count],

		[YPS_Lummus],
		[YPS_SPYRO],
		[YPS_SelfDev],
		[YPS_Other],
		--[YPS_OtherName],
		[YPS_RPT],

		[LP_Aspen],
		[LP_DeltaBase_No],
		[LP_DeltaBase_NotSure],
		[LP_DeltaBase_RPT],
		[LP_DeltaBase_Yes],
		[LP_Haverly],
		[LP_Honeywell],

		[LP_MixInt_No],
		[LP_MixInt_NotSure],
		[LP_MixInt_RPT],
		[LP_MixInt_Yes],

		[LP_Online_No],
		[LP_Online_NotSure],
		[LP_Online_RPT],
		[LP_Online_Yes],

		[LP_Other],
		--[LP_OtherName],

		[LP_Recur_No],
		[LP_Recur_NotSure],
		[LP_Recur_RPT],
		[LP_Recur_Yes],

		[LP_RPT],
		[LP_SelfDev],
		[LP_RowCount],

		[BeyondCrack_None],
		[BeyondCrack_C4],
		[BeyondCrack_C5],
		[BeyondCrack_Pygas],
		[BeyondCrack_RPT],
		[BeyondCrack_BTX],
		[BeyondCrack_OthChem],
		[BeyondCrack_OthRefine],
		[BeyondCrack_Oth],
		--[BeyondCrack_OtherName],

		[DecisionPlant_Bit],
		[DecisionCorp_Bit],
		[PlanTime_Days],
		[EffFeedChg_Levels],
		[FeedPurchase_Levels]
	)

	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[MinParaffin_Pcnt]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[MinParaffin_Pcnt], 1.0),
			[MaxVaporPresFRS_RVP]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[MaxVaporPresFRS_RVP], 1.0),
			[YieldPattern_Count]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[YieldPattern_Count], 1.0),
			[YieldPatternLP_Count]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[YieldPatternLP_Count], 1.0),
			[YieldGroup_Count]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[YieldGroup_Count], 1.0),

			[YPS_Lummus]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[YPS_Lummus]))),
			[YPS_SPYRO]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[YPS_SPYRO]))),
			[YPS_SelfDev]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[YPS_SelfDev]))),
			[YPS_Other]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[YPS_Other]))),
			[YPS_RPT]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[YPS_RPT]))),

			[LP_Aspen]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Aspen]))),

			[LP_DeltaBase_No]		= MAX([c].[LP_DeltaBase_No]),
			[LP_DeltaBase_NotSure]	= MAX([c].[LP_DeltaBase_NotSure]),
			[LP_DeltaBase_RPT]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_DeltaBase_RPT]))),
			[LP_DeltaBase_Yes]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_DeltaBase_Yes]))),

			[LP_Haverly]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Haverly]))),
			[LP_Honeywell]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Honeywell]))),

			[LP_MixInt_No]			= MAX([c].[LP_MixInt_No]),
			[LP_MixInt_NotSure]		= MAX([c].[LP_MixInt_NotSure]),
			[LP_MixInt_RPT]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_MixInt_RPT]))),
			[LP_MixInt_Yes]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_MixInt_Yes]))),

			[LP_Online_No]			= MAX([c].[LP_Online_No]),
			[LP_Online_NotSure]		= MAX([c].[LP_Online_NotSure]),
			[LP_Online_RPT]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Online_RPT]))),
			[LP_Online_Yes]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Online_Yes]))),

			[LP_Other]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Other]))),

			[LP_Recur_No]			= MAX([c].[LP_Recur_No]),
			[LP_Recur_NotSure]		= MAX([c].[LP_Recur_NotSure]),
			[LP_Recur_RPT]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Recur_RPT]))),
			[LP_Recur_Yes]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_Recur_Yes]))),

			[LP_RPT]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_RPT]))),
			[LP_SelfDev]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LP_SelfDev]))),

			[LP_RowCount]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[LP_RowCount], 1.0),

			[BeyondCrack_None]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_None]))),
			[BeyondCrack_C4]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_C4]))),
			[BeyondCrack_C5]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_C5]))),
			[BeyondCrack_Pygas]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_Pygas]))),
			[BeyondCrack_RPT]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_RPT]))),
			[BeyondCrack_BTX]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_BTX]))),
			[BeyondCrack_OthChem]	= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_OthChem]))),
			[BeyondCrack_OthRefine]	= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_OthRefine]))),
			[BeyondCrack_Oth]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BeyondCrack_Oth]))),

			[DecisionPlant_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[DecisionPlant_Bit]))),
			[DecisionCorp_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[DecisionCorp_Bit]))),

			[PlanTime_Days]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[PlanTime_Days], 1.0),
			[EffFeedChg_Levels]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[EffFeedChg_Levels], 1.0),
			[FeedPurchase_Levels]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedPurchase_Levels], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FeedSel]					[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[FeedSelLogistics]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[FacilityId],
		[Delivery_Pcnt]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[FacilityId],
			[Delivery_Pcnt]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Delivery_Pcnt], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FeedSelLogistics]					[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE	[f].[CalQtr]		= 4
		AND	[c].[Delivery_Pcnt]	> 0.0
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[FacilityId];

	INSERT INTO [fact].[FeedSelModelPenalty]
	(
		[Refnum],
		[CalDateKey],
		[PenaltyId],
		[Penalty_Bit]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[PenaltyId],
			[Penalty_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Penalty_Bit])))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FeedSelModelPenalty]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[PenaltyId];

	INSERT INTO [fact].[FeedSelModelPlan]
	(
		[Refnum],
		[CalDateKey],
		[PlanId],
		[Daily_Bit],
		[BiWeekly_Bit],
		[Weekly_Bit],
		[BiMonthly_Bit],
		[Monthly_Bit],
		[LessMonthly_Bit]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[PlanId],
			[Daily_Bit]			= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Daily_Bit]))),
			[BiWeekly_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BiWeekly_Bit]))),
			[Weekly_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Weekly_Bit]))),
			[BiMonthly_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[BiMonthly_Bit]))),
			[Monthly_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Monthly_Bit]))),
			[LessMonthly_Bit]	= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[LessMonthly_Bit])))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FeedSelModelPlan]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[PlanId];

	INSERT INTO [fact].[FeedSelModels]
	(
		[Refnum],
		[CalDateKey],
		[ModelId],
		[Implemented_Bit],
		[InCompany_Bit],
		[LocationId]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[ModelId],
			[Implemented_Bit]	= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Implemented_Bit]))),
			[InCompany_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[InCompany_Bit]))),
			[LocationId]		= (SELECT TOP 1
										[c].[LocationId]
									FROM
										@fpl							[f]
									INNER JOIN
										[fact].[FeedSelModels]	[c]
											ON	[c].[Refnum]			= [f].[Refnum]
											AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
									WHERE	[f].[CalQtr]		= 4
										AND	[c].[LocationId]	 IS NOT NULL
									GROUP BY
										[c].[LocationId]
									ORDER BY
										COUNT(1) DESC)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FeedSelModels]			[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[ModelId];

	INSERT INTO [fact].[FeedSelNonDiscretionary]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[Affiliated_Pcnt],
		[NonAffiliated_Pcnt]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
			[Affiliated_Pcnt]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Affiliated_Pcnt], 1.0),
			[NonAffiliated_Pcnt]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[NonAffiliated_Pcnt], 1.0)
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[FeedSelNonDiscretionary]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId];

	INSERT INTO [fact].[FeedSelPersonnel]
	(
		[Refnum],
		[CalDateKey],
		[PersId],
		[Plant_FTE],
		[Corporate_FTE]
	)
	SELECT
			[Refnum]			= @ListId,
			[CalDateKey]		= [f].[FactorSet_QtrDateKey],
		[c].[PersId],
			[Plant_FTE]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[Plant_FTE], 1.0),
			[Corporate_FTE]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[Corporate_FTE], 1.0)
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[FeedSelPersonnel]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[PersId];

	INSERT INTO [fact].[FeedSelPractices]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[Mix_Bit],
		[Blend_Bit],
		[Fractionation_Bit],
		[HydroTreat_Bit],
		[Purification_Bit],
		[MinConsidDensity_SG],
		[MaxConsidDensity_SG],
		[MinActualDensity_SG],
		[MaxActualDensity_SG]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[StreamId]				= (SELECT TOP 1
											[c].[StreamId]
										FROM
											@fpl							[f]
										INNER JOIN
											[fact].[FeedSelPractices]	[c]
												ON	[c].[Refnum]			= [f].[Refnum]
												AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
										WHERE	[f].[CalQtr]	= 4
										GROUP BY
											[c].[StreamId]
										ORDER BY
											COUNT(1) DESC),

			[Mix_Bit]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Mix_Bit]))),
			[Blend_Bit]				= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Blend_Bit]))),
			[Fractionation_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Fractionation_Bit]))),
			[HydroTreat_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[HydroTreat_Bit]))),
			[Purification_Bit]		= CONVERT(BIT, MAX(CONVERT(TINYINT, [c].[Purification_Bit]))),

			[MinConsidDensity_SG]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[MinConsidDensity_SG], 1.0),
			[MaxConsidDensity_SG]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[MaxConsidDensity_SG], 1.0),
			[MinActualDensity_SG]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[MinActualDensity_SG], 1.0),
			[MaxActualDensity_SG]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[MaxActualDensity_SG], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FeedSelPractices]				[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[FeedStockCrackingParameters]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],

		[CoilOutletPressure_Psia],
		[CoilOutletTemp_C],
		[CoilInletPressure_Psia],
		[CoilInletTemp_C],
		[RadiantWallTemp_C],
		[SteamHydrocarbon_Ratio],
		[EthyleneYield_WtPcnt],
		[PropyleneEthylene_Ratio],
		[PropyleneMethane_Ratio],
		[FeedConv_WtPcnt],

		[DistMethodId],

		[SulfurContent_ppm],
		[Density_SG],
		[FurnConstruction_Year],
		[ResidenceTime_s],
		[FlowRate_KgHr],
		[Recycle_Bit]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],

			[CoilOutletPressure_Psia]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[CoilOutletPressure_Psia], 1.0),
			[CoilOutletTemp_C]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[CoilOutletTemp_C], 1.0),
			[CoilInletPressure_Psia]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[CoilInletPressure_Psia], 1.0),
			[CoilInletTemp_C]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[CoilInletTemp_C], 1.0),
			[RadiantWallTemp_C]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[RadiantWallTemp_C], 1.0),
			[SteamHydrocarbon_Ratio]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[SteamHydrocarbon_Ratio], 1.0),
			[EthyleneYield_WtPcnt]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[EthyleneYield_WtPcnt], 1.0),
			[PropyleneEthylene_Ratio]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[PropyleneEthylene_Ratio], 1.0),
			[PropyleneMethane_Ratio]	= [GlobalDB].[dbo].[WtAvgNZ]([c].[PropyleneMethane_Ratio], 1.0),
			[FeedConv_WtPcnt]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[FeedConv_WtPcnt], 1.0),

			[DistMethodId]				= (SELECT TOP 1
												[c].[DistMethodId]
											FROM
												@fpl									[f]
											INNER JOIN
												[fact].[FeedStockCrackingParameters]	[c]
													ON	[c].[Refnum]			= [f].[Refnum]
													AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
											WHERE	[c].[StreamId] IS NOT NULL
												AND	[f].[CalQtr]	= 4
											GROUP BY
												[c].[DistMethodId]
											ORDER BY
												COUNT(1) DESC),

			[SulfurContent_ppm]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[SulfurContent_ppm], 1.0),
			[Density_SG]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[Density_SG], 1.0),
			[FurnConstruction_Year]		= [GlobalDB].[dbo].[WtAvgNZ]([c].[FurnConstruction_Year], 1.0),
			[ResidenceTime_s]			= [GlobalDB].[dbo].[WtAvgNZ]([c].[ResidenceTime_s], 1.0),
			[FlowRate_KgHr]				= [GlobalDB].[dbo].[WtAvgNZ]([c].[FlowRate_KgHr], 1.0),
		[c].[Recycle_Bit]
	FROM
		@fpl									[f]
	INNER JOIN
		[fact].[FeedStockCrackingParameters]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
		[c].[Recycle_Bit];

	INSERT INTO [fact].[FeedStockDistillation]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[DistillationId],

		[Temp_C]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
		[c].[DistillationId],
			[Temp_C]					= [GlobalDB].[dbo].[WtAvgNZ]([c].[Temp_C], 1.0)
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[FeedStockDistillation]	[c]
			ON	[c].[Refnum]			= [f].[Refnum]
			AND	[c].[CalDateKey]		= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]	= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
		[c].[DistillationId];

	INSERT INTO [fact].[ForexRate]
	(
		[Refnum],
		[CalDateKey],
		[CurrencyFrom],
		[CurrencyTo],
		[ForexRate]
	)
	SELECT
		[Refnum]		= @ListId,
		[CalDateKey]	= @CalDateKey,
		[CurrencyFrom]	= @CurrencyRpt,
		[CurrencyTo]	= @CurrencyRpt,
		[ForexRate]		= 1.0;

	INSERT INTO [fact].[GenPlant]
	(
		[Refnum],
		[CalDateKey],
		[EthyleneCarrier_Bit],
		[ExtractButadiene_Bit],
		[ExtractAromatics_Bit],
		[MTBE_Bit],
		[Isobutylene_Bit],
		[IsobutyleneHighPurity_Bit],
		[Butene1_Bit],
		[Isoprene_Bit],
		[CycloPentadiene_Bit],
		[OtherC5_Bit],
		[PolyPlantShare_Bit],
		[OlefinsDerivatives_Bit],
		[IntegrationRefinery_Bit],
		[CapCreepAnn_Pcnt],
		[LocationFactor],
		[OSIMPrep_Hrs]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
			[EthyleneCarrier_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[EthyleneCarrier_Bit]))),
			[ExtractButadiene_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[ExtractButadiene_Bit]))),
			[ExtractAromatics_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[ExtractAromatics_Bit]))),
			[MTBE_Bit]					= CONVERT(BIT, MAX(CONVERT(INT, [c].[MTBE_Bit]))),
			[Isobutylene_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[Isobutylene_Bit]))),
			[IsobutyleneHighPurity_Bit]	= CONVERT(BIT, MAX(CONVERT(INT, [c].[IsobutyleneHighPurity_Bit]))),
			[Butene1_Bit]				= CONVERT(BIT, MAX(CONVERT(INT, [c].[Butene1_Bit]))),
			[Isoprene_Bit]				= CONVERT(BIT, MAX(CONVERT(INT, [c].[Isoprene_Bit]))),
			[CycloPentadiene_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[CycloPentadiene_Bit]))),
			[OtherC5_Bit]				= CONVERT(BIT, MAX(CONVERT(INT, [c].[OtherC5_Bit]))),
			[PolyPlantShare_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[PolyPlantShare_Bit]))),
			[OlefinsDerivatives_Bit]	= CONVERT(BIT, MAX(CONVERT(INT, [c].[OlefinsDerivatives_Bit]))),
			[IntegrationRefinery_Bit]	= CONVERT(BIT, MAX(CONVERT(INT, [c].[IntegrationRefinery_Bit]))),
			[CapCreepAnn_Pcnt]			= AVG([CapCreepAnn_Pcnt]),
			[LocationFactor]			= AVG([LocationFactor]),
			[OSIMPrep_Hrs]				= AVG([OSIMPrep_Hrs])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[GenPlant]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[GenPlantCapEx]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[CurrencyRpt],
		[Amount_Cur],
		[Budget_Cur]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt],
			[Amount_Cur]			= AVG([c].[Amount_Cur]),
			[Budget_Cur]			= AVG([c].[Budget_Cur])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[GenPlantCapEx]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[GenPlantCommIntegration]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[Integration_Pcnt]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
			[Integration_Pcnt]			= AVG([c].[Integration_Pcnt])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantCommIntegration]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId];

	INSERT INTO [fact].[GenPlantCostsRevenue]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[StreamId],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId],
		[c].[CurrencyRpt],
			[Amount_Cur]				= AVG([c].[Amount_Cur])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantCostsRevenue]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[GenPlantDuties]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[StreamId],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId],
		[c].[CurrencyRpt],
			[Amount_Cur]				= AVG([c].[Amount_Cur])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantDuties]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[GenPlantEnergy]
	(
		[Refnum],
		[CalDateKey],
		[CoolingWater_MTd],
		[CoolingWaterSupplyTemp_C],
		[CoolingWaterReturnTemp_C],
		[FinFanHeatAbsorb_kBTU],
		[CompGasEfficiency_Pcnt],
		[CalcTypeId],
		[SteamExtratRate_kMTd],
		[SteamCondenseRate_kMTd],
		[SurfaceCondVacuum_InH20],
		[ExchangerHeatDuty_BTU],
		[QuenchHeatMatl_Pcnt],
		[QuenchDilution_Pcnt]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
			[CoolingWater_MTd]			= AVG([c].[CoolingWater_MTd]),
			[CoolingWaterSupplyTemp_C]	= AVG([c].[CoolingWaterSupplyTemp_C]),
			[CoolingWaterReturnTemp_C]	= AVG([c].[CoolingWaterReturnTemp_C]),
			[FinFanHeatAbsorb_kBTU]		= AVG([c].[FinFanHeatAbsorb_kBTU]),
			[CompGasEfficiency_Pcnt]	= AVG([c].[CompGasEfficiency_Pcnt]),
			[CalcTypeId]				= MAX([c].[CalcTypeId]),
			[SteamExtratRate_kMTd]		= AVG([c].[SteamExtratRate_kMTd]),
			[SteamCondenseRate_kMTd]	= AVG([c].[SteamCondenseRate_kMTd]),
			[SurfaceCondVacuum_InH20]	= AVG([c].[SurfaceCondVacuum_InH20]),
			[ExchangerHeatDuty_BTU]		= AVG([c].[ExchangerHeatDuty_BTU]),
			[QuenchHeatMatl_Pcnt]		= AVG([c].[QuenchHeatMatl_Pcnt]),
			[QuenchDilution_Pcnt]		= AVG([c].[QuenchDilution_Pcnt])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantEnergy]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[GenPlantEnergyEfficiency]
	(
		[Refnum],
		[CalDateKey],
		[PyroFurnInletTemp_C],
		[PyroFurnPreheat_Pcnt],
		[PyroFurnPrimaryTLE_Pcnt],
		[PyroFurnTLESteam_PSIa],
		[PyroFurnSecondaryTLE_Pcnt],
		[DriverGT_Bit],
		[PyroFurnFlueGasTemp_C],
		[PyroFurnFlueGasO2_Pcnt],
		[TLEOutletTemp_C],
		[EnergyDeterioration_Pcnt],
		[IBSLSteamRequirement_Pcnt]
	)
	SELECT
			[Refnum]					= @ListId,
			[CalDateKey]				= [f].[FactorSet_QtrDateKey],
			[PyroFurnInletTemp_C]		= AVG([c].[PyroFurnInletTemp_C]),
			[PyroFurnPreheat_Pcnt]		= AVG([c].[PyroFurnPreheat_Pcnt]),
			[PyroFurnPrimaryTLE_Pcnt]	= AVG([c].[PyroFurnPrimaryTLE_Pcnt]),
			[PyroFurnTLESteam_PSIa]		= AVG([c].[PyroFurnTLESteam_PSIa]),
			[PyroFurnSecondaryTLE_Pcnt]	= AVG([c].[PyroFurnSecondaryTLE_Pcnt]),
			[DriverGT_Bit]				= CONVERT(BIT, MAX(CONVERT(INT, [c].[DriverGT_Bit]))),
			[PyroFurnFlueGasTemp_C]		= AVG([c].[PyroFurnFlueGasTemp_C]),
			[PyroFurnFlueGasO2_Pcnt]	= AVG([c].[PyroFurnFlueGasO2_Pcnt]),
			[TLEOutletTemp_C]			= AVG([c].[TLEOutletTemp_C]),
			[EnergyDeterioration_Pcnt]	= AVG([c].[EnergyDeterioration_Pcnt]),
			[IBSLSteamRequirement_Pcnt]	= AVG([c].[IBSLSteamRequirement_Pcnt])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantEnergyEfficiency]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[GenPlantEnvironment]
	(
		[Refnum],
		[CalDateKey],
		[Emissions_MTd],
		[WasteHazMat_MTd],
		[NOx_LbMBtu],
		[WasteWater_MTd]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[Emissions_MTd]			= AVG([c].[Emissions_MTd]),
			[WasteHazMat_MTd]		= AVG([c].[WasteHazMat_MTd]),
			[NOx_LbMBtu]			= AVG([c].[NOx_LbMBtu]),
			[WasteWater_MTd]		= AVG([c].[WasteWater_MTd])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantEnvironment]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[GenPlantExpansion]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[StreamId],
		[CapacityAdded_kMT]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId],
			[CapacityAdded_kMT]		= AVG([c].[CapacityAdded_kMT])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantExpansion]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId];

	INSERT INTO [fact].[GenPlantFeedFlex]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[Capability_Pcnt],
		[Demonstrate_Pcnt]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
			[Capability_Pcnt]		= AVG([c].[Capability_Pcnt]),
			[Demonstrate_Pcnt]		= AVG([c].[Demonstrate_Pcnt])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantFeedFlex]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId];

	INSERT INTO [fact].[GenPlantInventory]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[StorageCapacity_kMT],
		[StorageLevel_Pcnt]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
			[StorageCapacity_kMT]	= AVG([c].[StorageCapacity_kMT]),
			[StorageLevel_Pcnt]		= AVG([c].[StorageLevel_Pcnt])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantInventory]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[GenPlantLogistics]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[StreamId],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]				= @ListId,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId],
		[c].[CurrencyRpt],
			[Amount_Cur]			= AVG([c].[Amount_Cur])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[GenPlantLogistics]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[StreamId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[Maint]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[CurrencyRpt],
		[MaintMaterial_Cur],
		[MaintLabor_Cur],
		[TaMaterial_Cur],
		[TaLabor_Cur]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
		[c].[CurrencyRpt],
			[MaintMaterial_Cur]		= AVG([c].[MaintMaterial_Cur]),
			[MaintLabor_Cur]		= AVG([c].[MaintLabor_Cur]),
			[TaMaterial_Cur]		= AVG([c].[TaMaterial_Cur]),
			[TaLabor_Cur]			= AVG([c].[TaLabor_Cur])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[Maint]						[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[MaintAccuracy]
	(
		[Refnum],
		[CalDateKey],
		[AccuracyId]
	)
	SELECT TOP 1
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccuracyId]
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[MaintAccuracy]						[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccuracyId]
	ORDER BY COUNT(1) DESC;

	INSERT INTO [fact].[MaintExpense]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[CompMaintHours_Pcnt]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
			[CompMaintHours_Pcnt]	= AVG([c].[CompMaintHours_Pcnt])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[MaintExpense]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId];

	INSERT INTO [fact].[MetaCapEx]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt],
			[Amount_Cur]		= AVG([c].[Amount_Cur])
	FROM
		@fpl								[f]
	INNER JOIN
		[fact].[MetaCapEx]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[MetaEnergy]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[LHValue_MBTU]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
			[Amount_Cur]			= AVG([c].[LHValue_MBTU])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[MetaEnergy]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId];

	INSERT INTO [fact].[MetaMisc]
	(
		[Refnum],
		[CalDateKey],
		[StartUp_Year]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[StartUp_Year]			= CEILING(AVG([c].[StartUp_Year]))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[MetaMisc]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[MetaOpEx]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt],
			[Amount_Cur]			= AVG([c].[Amount_Cur])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[MetaOpEx]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[MetaQuantity]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[Quantity_kMT]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
			[Quantity_kMT]			= AVG([c].[Quantity_kMT])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[MetaQuantity]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId];

	INSERT INTO [fact].[OpEx]
	(
		[Refnum],
		[CalDateKey],
		[AccountId],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt],
			[Amount_Cur]			= AVG([c].[Amount_Cur])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[OpEx]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[AccountId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[Pers]
	(
		[Refnum],
		[CalDateKey],
		[PersId],
		[Personnel_Count],
		[StraightTime_Hrs],
		[OverTime_Hrs],
		[Contract_Hrs]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[PersId],
			[Personnel_Count]		= AVG([c].[Personnel_Count]),
			[StraightTime_Hrs]		= AVG([c].[StraightTime_Hrs]),
			[OverTime_Hrs]			= AVG([c].[OverTime_Hrs]),
			[Contract_Hrs]			= AVG([c].[Contract_Hrs])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[Pers]				[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[PersId];

	INSERT INTO [fact].[PersAbsence]
	(
		[Refnum],
		[CalDateKey],
		[PersId],
		[Absence_Pcnt]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[PersId],
			[Absence_Pcnt]			= AVG([c].[Absence_Pcnt])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[PersAbsence]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[PersId];


	-- Polymer is Excluded


	INSERT INTO [fact].[Reliability]
	(
		[Refnum],
		[CalDateKey],
		[TimingFactorId],
		[TaPrep_Hrs],
		[TaStartUp_Hrs],
		[MiniTurnAround_Bit],
		[FurnRetubeWorkId],
		[FurnTubeLife_Mnths],
		[FurnRetube_Days]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[TimingFactorId]		= MAX([c].[TimingFactorId]),
			[FurnRetube_Days]		= AVG([c].[FurnRetube_Days]),
			[TaPrep_Hrs]			= AVG([c].[TaPrep_Hrs]),
			[MiniTurnAround_Bit]	= CONVERT(BIT, MAX(CONVERT(INT, [c].[MiniTurnAround_Bit]))),
			[FurnRetubeWorkId]		= MAX([c].[FurnRetubeWorkId]),
			[FurnTubeLife_Mnths]	= AVG([c].[FurnTubeLife_Mnths]),
			[FurnRetube_Days]		= AVG([c].[FurnRetube_Days])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[Reliability]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[ReliabilityCompEvents]
	(
		[Refnum],
		[CalDateKey],
		[EventNo],
		[CompressorId],
		[ServiceLevelId],
		[CompComponentId],
		[EventCauseId],
		[EventOccur_Year],
		[Duration_Hours]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[EventNo],
			[CompressorId]			= MAX([c].[CompressorId]),
			[ServiceLevelId]		= MAX([c].[ServiceLevelId]),
			[CompComponentId]		= MAX([c].[CompComponentId]),
			[EventCauseId]			= MAX([c].[EventCauseId]),
			[EventOccur_Year]		= FLOOR(AVG([c].[EventOccur_Year])),
			[Duration_Hours]		= AVG([c].[Duration_Hours])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityCompEvents]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[EventNo];

	INSERT INTO [fact].[ReliabilityCompOperation]
	(
		[Refnum],
		[CalDateKey],
		[Operation_Years]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[Operation_Years]		= AVG([c].[Operation_Years])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityCompOperation]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[ReliabilityCritPath]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[CritPath_Bit]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
			[CritPath_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[CritPath_Bit])))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityCritPath]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[ReliabilityDTOverhaulFrequency]
	(
		[Refnum],
		[CalDateKey],
		[FacilityId],
		[Frequency_Years]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[FacilityId],
			[Frequency_Years]		= CEILING(AVG([c].[Frequency_Years]))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityDTOverhaulFrequency]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FacilityId];

	INSERT INTO [fact].[ReliabilityDTPhilosophy]
	(
		[Refnum],
		[CalDateKey],
		[PhilosophyId]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[PhilosophyId]			= MIN([c].[PhilosophyId])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityDTPhilosophy]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

	INSERT INTO [fact].[ReliabilityDTPredMaint]
	(
		[Refnum],
		[CalDateKey],
		[CauseId],
		[Frequency_Days]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[CauseId],
			[Frequency_Days]		= AVG([c].[Frequency_Days])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityDTPredMaint]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[CauseId];

	INSERT INTO [fact].[ReliabilityOppLoss]
	(
		[Refnum],
		[CalDateKey],
		[OppLossId],
		[StreamId],
		[DownTimeLoss_MT],
		[SlowDownLoss_MT]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[OppLossId],
		[c].[StreamId],
			[DownTimeLoss_MT]		= AVG([c].[DownTimeLoss_MT]),
			[SlowDownLoss_MT]		= AVG([c].[SlowDownLoss_MT])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityOppLoss]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[OppLossId],
		[c].[StreamId];

	INSERT INTO [fact].[ReliabilityOppLoss_Availability]
	(
		[Refnum],
		[CalDateKey],
		[OppLossId],
		[DownTimeLoss_Pcnt],
		[SlowDownLoss_Pcnt]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[OppLossId],
			[DownTimeLoss_Pcnt]		= AVG([c].[DownTimeLoss_Pcnt]),
			[SlowDownLoss_Pcnt]		= AVG([c].[SlowDownLoss_Pcnt])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityOppLoss_Availability]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[OppLossId];

	INSERT INTO [fact].[ReliabilityPyroFurn]
	(
		[Refnum],
		[CalDateKey],
		[FurnId],
		[StreamId],
		[FeedQty_kMT],
		[FeedCap_MTd],
		[FuelTypeId],
		[FuelCons_kMT],
		[StackOxygen_Pcnt],
		[ArchDraft_H20],
		[StackTemp_C],
		[CrossTemp_C],
		[Retubed_Year],
		[RetubeInterval_Mnths],
		[Retubed_Pcnt]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[FurnId],
			[StreamId]				= MAX([c].[StreamId]),
			[FeedQty_kMT]			= AVG([c].[FeedQty_kMT]),
			[FeedCap_MTd]			= AVG([c].[FeedCap_MTd]),
			[FuelTypeId]			= MAX([c].[FuelTypeId]),
			[FuelCons_kMT]			= AVG([c].[FuelCons_kMT]),
			[StackOxygen_Pcnt]		= AVG([c].[StackOxygen_Pcnt]),
			[ArchDraft_H20]			= AVG([c].[ArchDraft_H20]),
			[StackTemp_C]			= AVG([c].[StackTemp_C]),
			[CrossTemp_C]			= AVG([c].[CrossTemp_C]),
			[Retubed_Year]			= FLOOR(AVG([c].[Retubed_Year])),
			[RetubeInterval_Mnths]	= AVG([c].[RetubeInterval_Mnths]),
			[Retubed_Pcnt]			= AVG([c].[Retubed_Pcnt])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityPyroFurn]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FurnId];

	INSERT INTO [fact].[ReliabilityPyroFurnDownTime]
	(
		[Refnum],
		[CalDateKey],
		[FurnId],
		[OppLossId],
		[DownTime_Days]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[FurnId],
		[c].[OppLossId],
			[DownTime_Days]			= AVG([c].[DownTime_Days])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityPyroFurnDownTime]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FurnId],
		[c].[OppLossId];

	INSERT INTO [fact].[ReliabilityPyroFurnFeed]
	(
		[Refnum],
		[CalDateKey],
		[StreamId],
		[RunLength_Days],
		[Inj],
		[PreTreat_Bit],
		[AntiFoul_Bit]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[StreamId],
			[RunLength_Days]		= AVG([c].[RunLength_Days]),
			[Inj]					= MAX([c].[Inj]),
			[PreTreat_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[PreTreat_Bit]))),
			[AntiFoul_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[AntiFoul_Bit])))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityPyroFurnFeed]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[StreamId];

	INSERT INTO [fact].[ReliabilityPyroFurnOpEx]
	(
		[Refnum],
		[CalDateKey],
		[FurnId],
		[AccountId],
		[CurrencyRpt],
		[Amount_Cur]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[FurnId],
		[c].[AccountId],
		[c].[CurrencyRpt],
			[Amount_Cur]			= AVG([c].[Amount_Cur])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityPyroFurnOpEx]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[FurnId],
		[c].[AccountId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[ReliabilityPyroFurnPlantLimit]
	(
		[Refnum],
		[CalDateKey],
		[OppLossId],
		[PlantFurnLimit_Pcnt]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[OppLossId],
			[PlantFurnLimit_Pcnt]	= AVG([c].[PlantFurnLimit_Pcnt])
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityPyroFurnPlantLimit]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[OppLossId];

	INSERT INTO [fact].[ReliabilityTA]
	(
		[Refnum],
		[CalDateKey],
		[TrainId],
		[SchedId],
		[Interval_Mnths],
		[DownTime_Hrs],
		[TurnAround_Date],
		[CurrencyRpt],
		[TurnAroundCost_MCur],
		[TurnAround_ManHrs],
		[StreamId],
		[Capacity_Pcnt],
		[MiniTA_Bit]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[TrainId],
		[c].[SchedId],
			[Interval_Mnths]		= AVG([c].[Interval_Mnths]),
			[DownTime_Hrs]			= AVG([c].[DownTime_Hrs]),
			[TurnAround_Date]		= DATEADD(DAY, DATEDIFF(DAY, MIN([c].[TurnAround_Date]), MAX([c].[TurnAround_Date])), MIN([c].[TurnAround_Date])),
		[c].[CurrencyRpt],
			[TurnAroundCost_MCur]	= AVG([c].[TurnAroundCost_MCur]),
			[TurnAround_ManHrs]		= AVG([c].[TurnAround_ManHrs]),
			[StreamId]				= MAX([c].[StreamId]),
			Capacity_Pcnt			= AVG([c].Capacity_Pcnt),
			[MiniTA_Bit]			= CONVERT(BIT, MAX(CONVERT(INT, [c].[MiniTA_Bit])))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityTA]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[TrainId],
		[c].[SchedId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[ReliabilityTAMini]
	(
		[Refnum],
		[CalDateKey],
		[TrainId],
		[CurrencyRpt],
		[DownTime_Hrs],
		[TurnAroundCost_MCur],
		[TurnAround_ManHrs],
		[TurnAround_Date]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
		[c].[TrainId],
		[c].[CurrencyRpt],
			[DownTime_Hrs]			= AVG([c].[DownTime_Hrs]),
			[TurnAroundCost_MCur]	= AVG([c].[TurnAroundCost_MCur]),
			[TurnAround_ManHrs]		= AVG([c].[TurnAround_ManHrs]),
			[TurnAround_Date]		= DATEADD(DAY, DATEDIFF(DAY, MIN([c].[TurnAround_Date]), MAX([c].[TurnAround_Date])), MIN([c].[TurnAround_Date]))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ReliabilityTAMini]	[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey],
		[c].[TrainId],
		[c].[CurrencyRpt];

	INSERT INTO [fact].[ROGEntryPoint]
	(
		[Refnum],
		[CalDateKey],
		[CompGasInlet_Bit],
		[CompGasDischarge_Bit],
		[C2Recovery_Bit],
		[C3Recovery_Bit]
	)
	SELECT
			[Refnum]				= @ListID,
			[CalDateKey]			= [f].[FactorSet_QtrDateKey],
			[CompGasInlet_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[CompGasInlet_Bit]))),
			[CompGasDischarge_Bit]	= CONVERT(BIT, MAX(CONVERT(INT, [c].[CompGasDischarge_Bit]))),
			[C2Recovery_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[C2Recovery_Bit]))),
			[C3Recovery_Bit]		= CONVERT(BIT, MAX(CONVERT(INT, [c].[C3Recovery_Bit])))
	FROM
		@fpl							[f]
	INNER JOIN
		[fact].[ROGEntryPoint]			[c]
			ON	[c].[Refnum]		= [f].[Refnum]
			AND	[c].[CalDateKey]	= [f].[FactorSet_QtrDateKey]
	WHERE
		[f].[CalQtr]				= 4
	GROUP BY
		[f].[FactorSet_QtrDateKey];

END;


EXECUTE [fact].[Insert_Fact_ListId] '13PCH', 2013, '2013', 2013;
EXECUTE [fact].[Insert_Fact_ListId] '11PCH', 2011, '2011', 2011;

