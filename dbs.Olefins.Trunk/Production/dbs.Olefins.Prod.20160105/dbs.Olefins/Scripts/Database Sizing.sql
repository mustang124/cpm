--SELECT
--	[p].[Schema],
--	[p].[FN],
--	[p].[IF],
--	[p].[TF],
--	[p].[U],
--	[p].[V],
--	[p].[P]
--FROM (
--	SELECT
--		[Schema] = [s].[name],
--		[o].[type]
--	FROM
--		sys.schemas		[s]
--	INNER JOIN
--		sys.objects		[o]
--			ON	[o].[schema_id] = [s].[schema_id]
--			AND	[o].[type] IN ('FN', 'IF', 'TF', 'U', 'V', 'P')
--	) [o]
--	PIVOT
--	(
--		COUNT([o].[type]) FOR [o].[type] IN ([FN], [IF], [TF], [U], [V], [P])
--	) [p]

SELECT
	[Schema]			= COALESCE([s].[name], 'TOTAL: ' + CONVERT(CHAR, COUNT(DISTINCT [s].[name]))),

	[Tables]			= COUNT(CASE WHEN [o].[type] = 'U'	THEN 1 END),
	[Views]				= COUNT(CASE WHEN [o].[type] = 'V'	THEN 1 END),
	[Procedures]		= COUNT(CASE WHEN [o].[type] = 'P'	THEN 1 END),

	[ScalarFunctions]	= COUNT(CASE WHEN [o].[type] = 'FN'	THEN 1 END),
	[InlineFunctions]	= COUNT(CASE WHEN [o].[type] = 'IF'	THEN 1 END),
	[TableFunctions]	= COUNT(CASE WHEN [o].[type] = 'TF'	THEN 1 END),

	[Rows]				= SUM(CASE WHEN [o].[type] = 'U'	THEN [p].[rows] END)
FROM
	sys.schemas		[s]
INNER JOIN
	sys.objects		[o]
		ON	[o].[schema_id] = [s].[schema_id]
		AND	[o].[type] IN ('FN', 'IF', 'TF', 'U', 'V', 'P')
LEFT OUTER JOIN
	sys.partitions	[p]
		ON	[p].[object_id]	= [o].[object_id]
		AND	[p].[index_id]	<= 1
		AND	[o].[type]		= 'U'
GROUP BY
	ROLLUP([s].[name]);


SELECT
	[Schema]		= [s].[name],
	[Table]			= [t].[name],
	[Partitions]	= COUNT([p].[rows]),
	[RowCount]		= SUM([p].[rows])
FROM
	sys.schemas		[s]
INNER JOIN
	sys.tables		[t]
		ON	[t].[schema_id]	= [s].[schema_id]
INNER JOIN
	sys.partitions	[p]
		ON	[p].[object_id]	= [t].[object_id]
		AND	[p].[index_id]	<= 1
WHERE
	[s].[name] = 'sim'
GROUP BY
	[s].[name],
	[t].[name]
ORDER BY
	SUM([p].[rows])	DESC;