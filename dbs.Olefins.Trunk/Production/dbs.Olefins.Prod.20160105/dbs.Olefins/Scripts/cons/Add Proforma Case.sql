﻿DECLARE @RefnumBase		VARCHAR(10)	= '2013PCH209';
DECLARE @ScenarioId		VARCHAR(1)	= 'A';
DECLARE @ScenarioName	VARCHAR(84)	= 'PROFORMA';
DECLARE @FactorSetId	VARCHAR(4)	= LEFT(@RefnumBase, 4);

DECLARE @RefnumPro		VARCHAR(11)	= @RefnumBase + @ScenarioId;

INSERT INTO [cons].[SubscriptionsAssets]
(
	[CompanyID],
	[CalDateKey],
	[StudyID],
	[AssetID],
	[ScenarioID],
	[ScenarioName],
	[SubscriberAssetName],
	[SubscriberAssetDetail]
)
SELECT
	[b].[CompanyID],
	[b].[CalDateKey],
	[b].[StudyID],
	[b].[AssetID],
	@ScenarioId,
	@ScenarioName,
	[b].[SubscriberAssetName]		+ ' ' + @ScenarioName,
	[b].[SubscriberAssetDetail]	+ ' ' + @ScenarioName
FROM
	[cons].[SubscriptionsAssets] [b]
LEFT OUTER JOIN
	[cons].[SubscriptionsAssets] [dc]	--	dc = Duplicate Check
		ON	[dc].[Refnum]	= @RefnumPro
WHERE	[b].[Refnum]	= @RefnumBase
	AND	[dc].[Refnum]	IS NULL;

INSERT INTO [cons].[TSortSolomon]
(
	[Refnum],
	[CalDateKey],
	[Consultant],
	[FactorSetId],
	[RefDirectory],
	[Comments],
	[ContactCode],
	[Active]
)
SELECT
	[b].[Refnum],
	[b].[CalDateKey],
	[t].[Consultant],
	[t].[FactorSetId],
	[t].[RefDirectory],
	[t].[Comments],
	[t].[ContactCode],
	1
FROM
	[cons].[SubscriptionsAssets] [b]
LEFT OUTER JOIN
	[cons].[TSortSolomon]		 [t]
		ON	[t].[Refnum]	= @RefnumBase
LEFT OUTER JOIN
	[cons].[TSortSolomon]		 [dc]
		ON	[dc].[Refnum]	= @RefnumPro
WHERE	[b].[Refnum]		= @RefnumPro
	AND	[dc].[Refnum]	IS NULL;

INSERT INTO [val].[Checklist]
(
	[Refnum],
	[IssueID],
	[IssueTitle],
	[IssueText],
	[PostedBy],
	[PostedTime],
	[Completed],
	[SetBy],
	[SetTime]	
)
SELECT
		[Refnum]		= @RefnumPro,
	[c].[IssueID],
	[c].[IssueTitle],
	[c].[IssueText],
		[PostedBy]		= '-',
		[PostedTime]	= SYSDATETIME(),
		[Completed]		= 'N',
		[SetBy]			= NULL,
		[SetTime]		= NULL
FROM
	[val].[Checklist]	[c]
WHERE
	[c].[Refnum] = '2013PCH003';