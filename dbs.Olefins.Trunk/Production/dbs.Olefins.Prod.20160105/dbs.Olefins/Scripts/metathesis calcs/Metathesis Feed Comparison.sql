﻿--EXECUTE [dbo].[spCalcs] '2013PCH033';
--EXECUTE [dbo].[spCalcs] '2013PCH144';
--EXECUTE [dbo].[spCalcs] '2013PCH157';
--EXECUTE [dbo].[spCalcs] '2013PCH195';
--EXECUTE [dbo].[spCalcs] '2013PCH198';

DECLARE @Refnum			VARCHAR(25)	= '2013PCH195';
DECLARE @sRefnum		VARCHAR(25) = RIGHT(@Refnum, 8);
DECLARE @StudyId		VARCHAR(4)	= 'PCH';
DECLARE @FactorSetId	VARCHAR(4)	= LEFT(@Refnum, 4);

	SELECT
		[StreamName] = REPLICATE(' ', POWER([p].[Hierarchy].GetLevel() - 1, 2)) + [p].[StreamId],
		[p].[StreamId],
		[psa].[StreamDescription],
		[psa].[Quantity_kMT],
		[psa].[Calc_Amount_Cur],
		[psa].[Avg_Calc_Amount_Cur],
		[psa].[Stream_Value_Cur],
		[psa].[Avg_Stream_Value_Cur]
	FROM [dim].[Stream_Meta_Parent]						[p]
	INNER JOIN [calc].[PricingPlantStreamMetaAggregate]	[psa]
		ON	[psa].[FactorSetId]	= [p].[FactorSetId]
		AND	[psa].[StreamId]	= [p].[StreamId]
		AND	[psa].[Refnum]		= @Refnum
		AND	[psa].[FactorSetId]	= [p].[FactorSetId]
	WHERE	[p].[FactorSetId]	= @FactorSetId
	ORDER BY [p].[Hierarchy] ASC;




SELECT
	[q].[StreamName],
	--[q].[StreamDescription],
	
	[q].[StdQty_kMT],
	[q].[MetaQty_kMT],
	'-'[-],
	[p].[StdQty_kMT],
	[p].[MetaQty_kMT],
	[p].[StdAmt_Cur],
	[p].[StdAvgAmt_Cur],
	[p].[MetaAmt_Cur],
	[p].[MetaAvgAmt_Cur]
FROM 
(
	SELECT
		[m].[StreamId],
		[m].[StreamName],
		[m].[StreamDescription],
		[StdQty_kMT]	= [a].[Quantity_kMT],
		[StdItems]		= [a].[IndexItems],
		[MetaQty_kMT]	= [m].[Quantity_kMT],
		[MetaItems]		= [m].[IndexItems],
		[m].[MetaHierarchy],
		[a].[StdHierarchy]
	FROM
		(
		SELECT
			[StreamName] = REPLICATE(' ', POWER([p].[Hierarchy].GetLevel() - 1, 2)) + [p].[StreamId],
			[sqa].[StreamDescription],
			[p].[StreamId],
			[MetaHierarchy] = [p].[Hierarchy],
			[sqa].[Quantity_kMT],
			[sqa].[IndexItems]
		FROM [dim].[Stream_Meta_Parent]					[p]
		INNER JOIN [fact].[StreamQuantityMetaAggregate]	[sqa]	WITH (NOEXPAND)
			ON	[sqa].[FactorSetId]	= [p].[FactorSetId]
			AND	[sqa].[StreamId]	= [p].[StreamId]
			AND	[sqa].[Refnum]		= @Refnum
			AND	[sqa].[FactorSetId]	= [p].[FactorSetId]
		WHERE	[p].[FactorSetId]	= @FactorSetId
		) [m]
	FULL OUTER JOIN
		(
		SELECT
			[StreamName] = REPLICATE(' ', POWER([p].[Hierarchy].GetLevel() - 1, 2)) + [p].[StreamId],
			[sqa].[StreamDescription],
			[p].[StreamId],
			[StdHierarchy] = [p].[Hierarchy],
			[sqa].[Quantity_kMT],
			[sqa].[IndexItems]
		FROM [dim].[Stream_Parent]						[p]
		INNER JOIN [fact].[StreamQuantityAggregate]		[sqa]	WITH (NOEXPAND)
			ON	[sqa].[FactorSetId]	= [p].[FactorSetId]
			AND	[sqa].[StreamId]	= [p].[StreamId]
			AND	[sqa].[Refnum]		= @Refnum
			AND	[sqa].[FactorSetId]	= [p].[FactorSetId]
		WHERE	[p].[FactorSetId]	= @FactorSetId
			AND [p].[StreamId]	NOT IN (SELECT [d].[DescendantId] FROM [dim].[Stream_Bridge] [d] WHERE [d].[StreamId] = 'MetaStream' AND [d].[FactorSetId] = @FactorSetId)
		) [a]
		ON	[m].[StreamId]			= [a].[StreamId]
		AND	COALESCE([m].[StreamDescription], [m].[StreamId])	= COALESCE([a].[StreamDescription], [a].[StreamId])
) [q]
LEFT OUTER JOIN
(
	SELECT
		[m].[StreamId],
		[m].[StreamName],
		[m].[StreamDescription],
		[StdQty_kMT]		= [a].[Quantity_kMT],
		[StdAmt_Cur]		= [a].[Calc_Amount_Cur],
		[StdAvgAmt_Cur]		= [a].[Avg_Calc_Amount_Cur],
		[StdHierarchy]		= [a].[Hierarchy],
		[MetaQty_kMT]		= [m].[Quantity_kMT],
		[MetaAmt_Cur]		= [m].[Calc_Amount_Cur],
		[MetaAvgAmt_Cur]	= [m].[Avg_Calc_Amount_Cur],
		[MetaHierarchy]		= [m].[Hierarchy]
	FROM
		(
		SELECT
			[StreamName] = REPLICATE(' ', POWER([p].[Hierarchy].GetLevel() - 1, 2)) + [p].[StreamId],
			[psa].[StreamDescription],
			[p].[StreamId],
			[p].[Hierarchy],
			[psa].[Quantity_kMT],
			[Calc_Amount_Cur] = [psa].[Calc_Amount_Cur] / 10.0,
			[psa].[Avg_Calc_Amount_Cur]
		FROM [dim].[Stream_Meta_Parent]						[p]
		INNER JOIN [calc].[PricingPlantStreamMetaAggregate]	[psa]
			ON	[psa].[FactorSetId]	= [p].[FactorSetId]
			AND	[psa].[StreamId]	= [p].[StreamId]
			AND	[psa].[Refnum]		= @Refnum
			AND	[psa].[FactorSetId]	= [p].[FactorSetId]
		WHERE	[p].[FactorSetId]	= @FactorSetId
		) [m]
	FULL OUTER JOIN
		(
		SELECT
			[StreamName] = REPLICATE(' ', POWER([p].[Hierarchy].GetLevel() - 1, 2)) + [p].[StreamId],
			[psa].[StreamDescription],
			[p].[StreamId],
			[p].[Hierarchy],
			[psa].[Quantity_kMT],
			[Calc_Amount_Cur] = [psa].[Calc_Amount_Cur] / 10.0,
			[psa].[Avg_Calc_Amount_Cur]
		FROM [dim].[Stream_Parent]						[p]
		INNER JOIN [calc].[PricingPlantStreamAggregate]	[psa]
			ON	[psa].[FactorSetId]	= [p].[FactorSetId]
			AND	[psa].[StreamId]	= [p].[StreamId]
			AND	[psa].[Refnum]		= @Refnum
			AND	[psa].[FactorSetId]	= [p].[FactorSetId]
		WHERE	[p].[FactorSetId]	= @FactorSetId
			AND [p].[StreamId]	NOT IN (SELECT [d].[DescendantId] FROM [dim].[Stream_Bridge] [d] WHERE [d].[StreamId] = 'MetaStream' AND [d].[FactorSetId] = @FactorSetId)
		) [a]
		ON	[m].[StreamId]			= [a].[StreamId]
		AND	COALESCE([m].[StreamDescription], [m].[StreamId])	= COALESCE([a].[StreamDescription], [a].[StreamId])
) [p]
ON [p].[StreamId] = [q].[StreamId]
AND	COALESCE([p].[StreamDescription], [p].[StreamId])	= COALESCE([q].[StreamDescription], [q].[StreamId])
ORDER BY 
	[q].[MetaHierarchy],
	[q].[StdHierarchy],
	[p].[MetaHierarchy],
	[p].[StdHierarchy];

