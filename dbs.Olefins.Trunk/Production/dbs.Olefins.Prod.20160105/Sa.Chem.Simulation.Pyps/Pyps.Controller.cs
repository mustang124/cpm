﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.ComponentModel;

using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sa.Chem
{
	public sealed partial class Pyps : Simulation
	{
		public sealed class Controller : AController
		{
			#region Single thread

			public override void RunQueue()
			{
				string rowsRemaining;

				string cnString = Sa.Database.dbConnectionString("DBS5", "Olefins", "rrh", "rrh#4279");
				string procedure = "[sim].[Select_List]";

				string factorSetId;
				string refnum;

				using (SqlConnection cn = new SqlConnection(cnString))
				{
					using (SqlCommand cmd = new SqlCommand(procedure, cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Connection.Open();

						using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							while (rdr.Read())
							{
								factorSetId = rdr.GetString(rdr.GetOrdinal("FactorSetId"));
								refnum = rdr.GetString(rdr.GetOrdinal("Refnum"));

								rowsRemaining = rdr.GetString(rdr.GetOrdinal("RowsRemaining"));

								Console.WriteLine(rowsRemaining + "\t" + refnum);

								RunPlant(new Asset(factorSetId, refnum));
							}
						}
						cmd.Connection.Close();
					}
				}
			}

			public override void RunPlant(Asset asset)
			{
				string path = Guid.NewGuid().ToString();

				using (Pyps.Engine e = new Pyps.Engine("Engine.Pyps", path, true))
				{
					LoopFeeds(asset, e);
				}
			}

			private void LoopFeeds(Asset asset, Pyps.Engine e)
			{
				string cString = Sa.Database.dbConnectionString("DBS5", "Olefins", "rrh", "rrh#4279");
				string procedure = "[sim].[Select_SimulationInputPyps]";

				string factorSetId;
				string refnum;
				string opCondId;
				string streamId;
				string streamDescription;
				int recycleId;
				int f127;
				float f130;
				float f131;
				float f132;

				IOutputWriter oWtr = new MdlOutDatabaseWriter();

				using (SqlConnection cn = new SqlConnection(cString))
				{
					using (SqlCommand cmd = new SqlCommand(procedure, cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@FactorSetId", System.Data.SqlDbType.VarChar, 12).Value = asset.factorSetId;
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = asset.refnum;

						cn.Open();

						using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
						{
							while (rdr.Read())
							{
								factorSetId = rdr.GetString(rdr.GetOrdinal("FactorSetId"));
								refnum = rdr.GetString(rdr.GetOrdinal("Refnum"));
								streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));
								streamDescription = rdr.GetString(rdr.GetOrdinal("StreamDescription"));
								opCondId = rdr.GetString(rdr.GetOrdinal("OpCondId"));
								recycleId = rdr.GetInt32(rdr.GetOrdinal("RecycleId"));
								f127 = rdr.GetInt32(rdr.GetOrdinal("127"));
								f130 = rdr.GetFloat(rdr.GetOrdinal("130"));
								f131 = rdr.GetFloat(rdr.GetOrdinal("131"));
								f132 = rdr.GetFloat(rdr.GetOrdinal("132"));

								Feed feed = new Feed(factorSetId, refnum, opCondId, streamId, streamDescription, recycleId, f127, f130, f131, f132);
								IInputReader iRdr = new SolInpDatabaseReader(rdr);

								e.SimulateFeed(feed, iRdr, oWtr);
							}
						}
						cmd.Connection.Close();
					}
				}
			}

			[Obsolete("For Testing purposes", false)]
			public void RunFeed()
			{
				//string path = System.IO.Directory.GetCurrentDirectory() + "\\Development\\FormattedString.txt";
				//string path = System.IO.Directory.GetCurrentDirectory() + "\\Development\\SOLINP.txt";
				string path = System.IO.Directory.GetCurrentDirectory() + "\\Development\\2013PCH053.LPG.txt";

				string factorSetId = "2013";
				string refnum = "2013PCH053";
				string opCondId = "OSOP";
				string streamId = "LPG";

				Feed feed = new Feed(factorSetId, refnum, opCondId, streamId, streamId, 0, 0, 0, 0, 0);

				IInputReader iRdr = new SolInpReader(path);
				//IOutputWriter oWtr = new xMdlOutWriter();
				IOutputWriter oWtr = new MdlOutDatabaseWriter();

				using (Pyps.Engine e = new Pyps.Engine("Engine.Pyps", "OneFile", false))
				{
					e.SimulateFeed(feed, iRdr, oWtr);
				}
			}

			private abstract class InputReader : IInputReader
			{
				protected internal InputItems inputItems = new InputItems();

				public bool Verified()
				{
					return (this.inputItems.Count == Constants.ItemsSolInp);
				}

				public abstract SimInput Read();
			}

			private sealed class SolInpDatabaseReader : InputReader
			{
				private readonly SqlDataReader Rdr;

				internal SolInpDatabaseReader(SqlDataReader readerRow)
				{
					this.Rdr = readerRow;
				}

				public override SimInput Read()
				{
					string col;
					string val;

					int ord;

					for (int idx = 1; idx <= Constants.ItemsSolInp; idx++)
					{
						col = idx.ToString(NumberFormatInfo.InvariantInfo);

						if (this.Rdr.FieldExists(col))
						{
							ord = this.Rdr.GetOrdinal(col);
							val = this.GetValue(this.Rdr, ord);

							val = (!string.IsNullOrEmpty(val)) ? val : "0";

							base.inputItems.Add(new InputItem(col, float.Parse(val)));
						}
						else
						{
							base.inputItems.Add(new InputItem(col, 0f));
						}
					}
					return new SolInput(base.inputItems);
				}

				private string GetValue(SqlDataReader rdr, int ord)
				{
					switch (rdr.GetDataTypeName(ord))
					{
						case "float":
							return rdr.GetSqlDouble(ord).ToString();
						case "int":
							return rdr.GetSqlInt32(ord).ToString();
						case "real":
							return rdr.GetSqlSingle(ord).ToString();
						case "decimal":
							return rdr.GetSqlDecimal(ord).ToString();
						default:
							return rdr.GetSqlValue(ord).ToString();
					}
				}
			}

			private sealed class SolInpReader : InputReader
			{
				private readonly string SolInpPath;

				internal SolInpReader(string pathInput)
				{
					this.SolInpPath = pathInput;
				}

				public override SimInput Read()
				{
					string[] delimiter = new string[] { " " };

					string solInp = System.IO.File.ReadAllText(this.SolInpPath, Encoding.ASCII);

					List<string> lst = new List<string>(solInp.Split(delimiter, System.StringSplitOptions.RemoveEmptyEntries));

					string col;
					float val;

					foreach (string s in lst)
					{
						col = lst.IndexOf(s).ToString();
						val = float.Parse(s);

						base.inputItems.Add(new InputItem(col, val));
					}
					return new SolInput(base.inputItems);
				}
			}

			private sealed class MdlOutDatabaseWriter : IOutputWriter
			{
				public void Write(Feed feed, SimResults simResults)
				{
					WriteEnergy(feed, simResults.energy);
					WriteYield(feed, simResults.yield);
				}

				private void WriteEnergy(Feed feed, Energy energy)
				{
					if (!float.IsNaN(energy.amount))
					{
						Database.InsertEnergy(feed.factorSetId, feed.refnum, Pyps.modelId, feed.opCondId, feed.streamId, feed.streamDescription, feed.recycleId, "", energy.amount);
					}
				}

				private void WriteYield(Feed feed, Yield yield)
				{
					string cString = Sa.Database.dbConnectionString("DBS5", "Olefins", "rrh", "rrh#4279");

					using (SqlConnection cn = new SqlConnection(cString))
					{
						foreach (Component comp in yield)
						{
							if (comp.wtPcnt > 0f)
							{
								Database.InsertComposition(cn, feed.factorSetId, feed.refnum, Pyps.modelId, feed.opCondId, feed.streamId, feed.streamDescription, feed.recycleId, "", comp.id, comp.wtPcnt);
							}
						}
					}
				}
			}

			private sealed class xMdlOutWriter : IOutputWriter
			{
				public void Write(Feed feed, SimResults simResults)
				{
				}
			}

			#endregion
		}

		private sealed class SolInput : SimInput, ILiquidProperties
		{
			private const float val = 1f;

			public SolInput(InputItems inputItems)
				: base(inputItems)
			{
			}

			public bool IsLiquid
			{
				get
				{
					if (base.InputItems[Constants.IndexNapInN].FValue == val) return true;
					if (base.InputItems[Constants.IndexNapInG].FValue == val) return true;
					if (base.InputItems[Constants.IndexNapIn1].FValue == val) return true;
					if (base.InputItems[Constants.IndexNapIn2].FValue == val) return true;

					return false;
				}
			}

			public int IndexIbp
			{
				get
				{
					if (base.InputItems[Constants.IndexNapInN].FValue == val) return Constants.IndexIbpInN;
					if (base.InputItems[Constants.IndexNapInG].FValue == val) return Constants.IndexIbpInG;
					if (base.InputItems[Constants.IndexNapIn1].FValue == val) return Constants.IndexIbpIn1;
					if (base.InputItems[Constants.IndexNapIn2].FValue == val) return Constants.IndexIbpIn2;

					return 0;
				}
			}

			public int IndexDensity
			{
				get
				{
					if (base.InputItems[Constants.IndexNapInN].FValue == val) return Constants.IndexSgInN;
					if (base.InputItems[Constants.IndexNapInG].FValue == val) return Constants.IndexSgInG;
					if (base.InputItems[Constants.IndexNapIn1].FValue == val) return Constants.IndexSgIn1;
					if (base.InputItems[Constants.IndexNapIn2].FValue == val) return Constants.IndexSgIn2;

					return 0;
				}
			}

			public float InitialBoilingPoint
			{
				get
				{
					if (this.IndexIbp > 0)
					{
						return base.InputItems[this.IndexIbp].FValue;
					}
					return 0;
				}
			}

			public float Density
			{
				get
				{
					if (this.IndexDensity > 0)
					{
						return base.InputItems[this.IndexDensity].FValue;
					}
					return 0;
				}
			}
		}
	}

	public sealed class PypsParallel
	{
		private readonly ListBox lbParallel;
		private readonly Label lblParallel;

		public PypsParallel(ListBox lbParallel, Label lblParallel)
		{
			this.lbParallel = lbParallel;
			this.lblParallel = lblParallel;
		}

		private Assets assets = new Assets();

		public void AddToList(ListBox lb)
		{
			string cnString = Sa.Database.dbConnectionString("DBS5", "Olefins", "rrh", "rrh#4279");
			string procedure = "[sim].[Select_List]";

			string factorSetId;
			string refnum;

			using (SqlConnection cn = new SqlConnection(cnString))
			{
				using (SqlCommand cmd = new SqlCommand(procedure, cn))
				{
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Connection.Open();

					using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
					{
						while (rdr.Read())
						{
							factorSetId = rdr.GetString(rdr.GetOrdinal("FactorSetId"));
							refnum = rdr.GetString(rdr.GetOrdinal("Refnum"));

							lb.Items.Add(refnum);
							assets.Add(new Asset(factorSetId, refnum));
						}
					}
					cmd.Connection.Close();
				}
			}
		}

		public void ParallelList()
		{
			Task.Factory.StartNew(() =>
				Parallel.ForEach(assets, new ParallelOptions { MaxDegreeOfParallelism = 30 },
					asset =>
					{
						lbParallel.Parent.BeginInvoke((Action)(() =>
						{
							lbParallel.Items.Add(asset.refnum);
							lblParallel.Text = lbParallel.Items.Count.ToString();
						}));

						RunPlantParallel(asset);

						lbParallel.Parent.BeginInvoke((Action)(() =>
						{
							lbParallel.Items.Remove(asset.refnum);
							lblParallel.Text = lbParallel.Items.Count.ToString();
						}));
					}
				)
			);
		}

		private void RunPlantParallel(Asset asset)
		{
			Console.WriteLine(assets.IndexOf(asset) + "\t" + asset.refnum);

			string path = Guid.NewGuid().ToString();

			Pyps.Controller c = new Pyps.Controller();
			c.RunPlant(asset);
		}
	}

	//public sealed class PypsMT : SimulationMT
	//{
	//	public class Controller
	//	{
	//		public void RunQueue()
	//		{
	//			PypsThreadManager ptm = new PypsThreadManager(1);

	//			ptm.AddToQueue();
	//			ptm.ProcessQueue();
	//		}
	//	}

	//	public class PypsThreadManager : ThreadManager
	//	{
	//		public string Test;

	//		public PypsThreadManager(int maxThreadCount)
	//			: base(maxThreadCount)
	//		{
	//		}

	//		public override void OnDoWork(EngineWorker engineWorker, DoWorkEventArgs e)
	//		{
	//			Plant plant = (Plant)e.Argument;
	//			Asset asset = new Asset(plant.factorSetId, plant.refnum);
	//			Pyps.Controller c = new Pyps.Controller();
	//			c.RunPlant(asset);
	//		}

	//		public override void OnProgressChanged(EngineWorker engineWorker, ProgressChangedEventArgs e)
	//		{
	//			Console.WriteLine(DateTime.Now);
	//		}

	//		public void OnRunWorkerCompleted(EngineWorker engineWorker, RunWorkerCompletedEventArgs e)
	//		{
	//			base.OnRunWorkerCompleted(engineWorker, e);
	//		}

	//		public override void AddToQueue()
	//		{
	//			string cnString = Sa.Database.dbConnectionString("DBS5", "Olefins", "rrh", "rrh#4279");
	//			string procedure = "[sim].[Select_List]";

	//			Asset asset = new Asset();

	//			using (SqlConnection cn = new SqlConnection(cnString))
	//			{
	//				using (SqlCommand cmd = new SqlCommand(procedure, cn))
	//				{
	//					cmd.CommandType = CommandType.StoredProcedure;

	//					cmd.Connection.Open();

	//					using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
	//					{
	//						while (rdr.Read())
	//						{
	//							asset.factorSetId = rdr.GetString(rdr.GetOrdinal("FactorSetId"));
	//							asset.refnum = rdr.GetString(rdr.GetOrdinal("Refnum"));
								
	//							Console.WriteLine("\t" + asset.refnum);

	//							this.engineQueue.Enqueue(new Plant(0, asset.factorSetId, asset.refnum, false));
	//						}
	//					}
	//					cmd.Connection.Close();
	//				}
	//			}
	//		}
	//	}
	//}
}