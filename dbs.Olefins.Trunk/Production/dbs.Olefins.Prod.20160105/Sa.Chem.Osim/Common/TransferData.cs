﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	static class TransferData
	{
		internal interface IDownload
		{
			string StoredProcedure { get; }
			string LookUpColumn { get; }
			Dictionary<string, RangeReference> Columns { get; }
			Dictionary<string, RangeReference> Items { get; }
		}

		internal interface IDownloadIntersection
		{
			string StoredProcedure { get; }
			string ValueColumn { get; }
			string RowLookUpColumn { get; }
			string ColLookUpColumn { get; }
			Dictionary<string, RangeReference> Rows { get; }
			Dictionary<string, RangeReference> Cols { get; }
		}

		internal interface IUpload
		{
			string StoredProcedure { get; }
			Dictionary<string, RangeReference> Parameters { get; }
		}

		internal interface IUploadMultiple
		{
			string StoredProcedure { get; }
			Dictionary<string, RangeReference> Items { get; }
			Dictionary<string, RangeReference> Parameters { get; }
		}

		[SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		internal static void Delete(SqlConnection cn, string StoredProcedure, string sRefnum)
		{
			using (SqlCommand cmd = new SqlCommand(StoredProcedure, cn))
			{
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = sRefnum;

				cmd.ExecuteNonQuery();
			}
		}

		[SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		internal static void Upload(SqlConnection cn, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IUpload upl)
		{
			using (SqlCommand cmd = new SqlCommand(upl.StoredProcedure, cn))
			{
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

				foreach (KeyValuePair<string, RangeReference> parameter in upl.Parameters)
				{
					cmd.Parameters.Add(parameter.Key, parameter.Value.sqlType).Value = XL.GetCellValue(wkb, tabNamePrefix, parameter.Value);
				}

				cmd.ExecuteNonQuery();
			}
		}

		[SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		internal static void Upload(SqlConnection cn, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IUploadMultiple upl)
		{
			string pfx;
			string sfx;
			int row;
			int col;
			SqlDbType typ;

			foreach (KeyValuePair<string, RangeReference> item in upl.Items)
			{
				using (SqlCommand cmd = new SqlCommand(upl.StoredProcedure, cn))
				{
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

					foreach (KeyValuePair<string, RangeReference> parameter in upl.Parameters)
					{
						pfx = (!string.IsNullOrEmpty(parameter.Value.sheetPrefix)) ? parameter.Value.sheetPrefix : item.Value.sheetPrefix;
						sfx = (!string.IsNullOrEmpty(parameter.Value.sheetSuffix)) ? parameter.Value.sheetSuffix : item.Value.sheetSuffix;
						row = (parameter.Value.row != 0) ? parameter.Value.row : item.Value.row;
						col = (parameter.Value.col != 0) ? parameter.Value.col : item.Value.col;
						typ = (parameter.Value.sqlType != SqlDbType.Variant) ? parameter.Value.sqlType : item.Value.sqlType;

						cmd.Parameters.Add(parameter.Key, typ).Value = XL.GetCellValue(wkb, tabNamePrefix, new RangeReference(pfx, sfx, row, col, typ));
					}

					cmd.ExecuteNonQuery();
				}
			}
		}


		internal static void Download(SqlConnection cn, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownload pop, bool forceIn)
		{
			Download(cn, string.Empty, refnum, wkb, tabNamePrefix, pop, double.NaN, forceIn);
		}

		internal static void Download(SqlConnection cn, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownload pop, double forex, bool forceIn)
		{
			Download(cn, string.Empty, refnum, wkb, tabNamePrefix, pop, forex, forceIn);
		}

		internal static void Download(SqlConnection cn, string factorSetId, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownload pop, bool forceIn)
		{
			Download(cn, factorSetId, refnum, wkb, tabNamePrefix, pop, double.NaN, forceIn);
		}

		[SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		internal static void Download(SqlConnection cn, string factorSetId, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownload pop, double forex, bool forceIn)
		{
			string sht;
			int row;
			int col;
			SqlDbType typ;
			double div;
			bool frc;

			using (SqlCommand cmd = new SqlCommand(pop.StoredProcedure, cn))
			{
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

				if (factorSetId != string.Empty)
				{
					cmd.Parameters.Add("@FactorSetId", SqlDbType.VarChar, 12).Value = factorSetId;
				}

				using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
				{
					RangeReference r = new RangeReference();

					while (rdr.Read())
					{
						if (pop.Items.Count == 0 || pop.Items.TryGetValue(rdr.GetString(rdr.GetOrdinal(pop.LookUpColumn)), out r))
						{
							foreach (KeyValuePair<string, RangeReference> entry in pop.Columns)
							{
								sht = (r != null && !string.IsNullOrEmpty(r.sheetSuffix)) ? r.sheetSuffix : entry.Value.sheetSuffix;
								row = (r != null && r.row != 0) ? r.row : entry.Value.row;
								col = (r != null && r.col != 0) ? r.col : entry.Value.col;
								typ = (r != null && r.sqlType != SqlDbType.Variant) ? r.sqlType : entry.Value.sqlType;

								if (double.IsNaN(forex))
								{
									div = (r != null && !double.IsNaN(r.divisor) && r.divisor != 0.0) ? r.divisor : entry.Value.divisor;
								}
								else
								{
									div = forex;
								}

								frc = (r.forceIn || entry.Value.forceIn || forceIn);

								XL.SetCellValue(rdr, entry.Key, wkb, tabNamePrefix, new RangeReference(sht, row, col, typ, div, frc));
							}
						}
					}
				}
			}
		}


		internal static void Download(SqlConnection cn, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownloadIntersection pop, bool forceIn)
		{
			Download(cn, string.Empty, refnum, wkb, tabNamePrefix, pop, double.NaN, forceIn);
		}

		internal static void Download(SqlConnection cn, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownloadIntersection pop, double forex, bool forceIn)
		{
			Download(cn, string.Empty, refnum, wkb, tabNamePrefix, pop, forex, forceIn);
		}

		internal static void Download(SqlConnection cn, string factorSetId, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownloadIntersection pop, bool forceIn)
		{
			Download(cn, factorSetId, refnum, wkb, tabNamePrefix, pop, double.NaN, forceIn);
		}

		[SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
		internal static void Download(SqlConnection cn, string factorSetId, string refnum, Excel.Workbook wkb, string tabNamePrefix, TransferData.IDownloadIntersection pop, double forex, bool forceIn)
		{
			string sht;
			int row;
			int col;
			SqlDbType typ;
			double div;
			bool frc;

			using (SqlCommand cmd = new SqlCommand(pop.StoredProcedure, cn))
			{
				cmd.CommandType = CommandType.StoredProcedure;

				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

				if (factorSetId != string.Empty)
				{
					cmd.Parameters.Add("@FactorSetId", SqlDbType.VarChar, 12).Value = factorSetId;
				}

				using (SqlDataReader rdr = cmd.ExecuteReader(CommandBehavior.SingleResult))
				{
					RangeReference r = new RangeReference();
					RangeReference c = new RangeReference();

					while (rdr.Read())
					{
						if (pop.Rows.TryGetValue(rdr.GetString(rdr.GetOrdinal(pop.RowLookUpColumn)), out r))
						{
							if (pop.Cols.TryGetValue(rdr.GetString(rdr.GetOrdinal(pop.ColLookUpColumn)), out c))
							{
								sht = (!string.IsNullOrEmpty(r.sheetSuffix)) ? r.sheetSuffix : c.sheetSuffix;
								row = (r.row != 0) ? r.row : c.row;
								col = (r.col != 0) ? r.col : c.col;
								typ = (r.sqlType != SqlDbType.Variant) ? r.sqlType : c.sqlType;

								if (double.IsNaN(forex))
								{
									div = (!double.IsNaN(r.divisor) && r.divisor != 0.0) ? r.divisor : (!double.IsNaN(c.divisor) && c.divisor != 0.0) ? c.divisor : 0.0;
								}
								else
								{
									div = forex;
								}

								frc = (r.forceIn || c.forceIn || forceIn);

								XL.SetCellValue(rdr, pop.ValueColumn, wkb, tabNamePrefix, new RangeReference(sht, row, col, typ, div, frc));
							}
						}
					}
				}
			}
		}
	}
}