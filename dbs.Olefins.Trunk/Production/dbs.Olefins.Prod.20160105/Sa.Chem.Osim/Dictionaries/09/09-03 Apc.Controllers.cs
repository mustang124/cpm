﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Apc
		{
			internal class Controllers
			{
				private static RangeReference ApcPyroFurnIndivid = new RangeReference(Tabs.T09_03, 44, 9, SqlDbType.Int);
				private static RangeReference ApcFurnFeed = new RangeReference(Tabs.T09_03, 45, 9, SqlDbType.Int);
				private static RangeReference ApcTowerQuench = new RangeReference(Tabs.T09_03, 46, 9, SqlDbType.Int);
				private static RangeReference ApcTowerComp = new RangeReference(Tabs.T09_03, 47, 9, SqlDbType.Int);
				private static RangeReference ApcTowerOther = new RangeReference(Tabs.T09_03, 48, 9, SqlDbType.Int);
				private static RangeReference ApcTowerDepropDebut = new RangeReference(Tabs.T09_03, 49, 9, SqlDbType.Int);
				private static RangeReference ApcTowerPropylene = new RangeReference(Tabs.T09_03, 50, 9, SqlDbType.Int);
				private static RangeReference ApcSteamHeat = new RangeReference(Tabs.T09_03, 51, 9, SqlDbType.Int);
				private static RangeReference ApcOtherOnSite = new RangeReference(Tabs.T09_03, 52, 9, SqlDbType.Int);

				private static RangeReference Controller_Count = new RangeReference(Tabs.T09_03, 0, 9, SqlDbType.Int);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ApcControllers]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "ApcId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Controller_Count", Controllers.Controller_Count);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("ApcPyroFurnIndivid", Controllers.ApcPyroFurnIndivid);
							d.Add("ApcFurnFeed", Controllers.ApcFurnFeed);
							d.Add("ApcTowerQuench", Controllers.ApcTowerQuench);
							d.Add("ApcTowerComp", Controllers.ApcTowerComp);
							d.Add("ApcOtherOnSite", Controllers.ApcOtherOnSite);
							d.Add("ApcTowerDepropDebut", Controllers.ApcTowerDepropDebut);
							d.Add("ApcTowerPropylene", Controllers.ApcTowerPropylene);
							d.Add("ApcSteamHeat", Controllers.ApcSteamHeat);
							d.Add("ApcTowerOther", Controllers.ApcTowerOther);

							return d;
						}
					}
				}
			}
		}
	}
}