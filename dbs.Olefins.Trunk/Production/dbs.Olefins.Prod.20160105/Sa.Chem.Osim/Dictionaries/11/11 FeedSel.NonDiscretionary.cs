﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class FeedSelection
		{
			internal partial class NonDiscretionary
			{
				private static RangeReference Affiliated_Pcnt = new RangeReference(Tabs.T11, 43, 11, SqlDbType.Float, 100.0);
				private static RangeReference NonAffiliated_Pcnt = new RangeReference(Tabs.T11, 46, 11, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_FeedSelNonDiscretionary]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Affiliated_Pcnt", NonDiscretionary.Affiliated_Pcnt);
							d.Add("NonAffiliated_Pcnt", NonDiscretionary.NonAffiliated_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}