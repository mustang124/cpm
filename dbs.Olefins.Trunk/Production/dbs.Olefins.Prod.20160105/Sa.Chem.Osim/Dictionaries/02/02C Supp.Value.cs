﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal partial class Supp
			{
				internal class Value
				{
					private static RangeReference SuppOther = new RangeReference(Tabs.T02_C, 75, 3, SqlDbType.Float);
					private static RangeReference Amount_Cur = new RangeReference(string.Empty, 0, 0, SqlDbType.Float);

					internal class Download : TransferData.IDownload
					{
						public string StoredProcedure
						{
							get
							{
								return "[fact].[Select_StreamsSuppValue]";
							}
						}

						public string LookUpColumn
						{
							get
							{
								return new Streams().LookUpColumn;
							}
						}

						public Dictionary<string, RangeReference> Columns
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								d.Add("Amount_Cur", Value.Amount_Cur);

								return d;
							}
						}

						public Dictionary<string, RangeReference> Items
						{
							get
							{
								Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

								//	Supplemental
								d.Add("SuppOther", Value.SuppOther);

								return d;
							}
						}
					}
				}
			}
		}
	}
}