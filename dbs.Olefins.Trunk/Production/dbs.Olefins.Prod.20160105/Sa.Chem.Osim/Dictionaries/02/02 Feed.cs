﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Streams
		{
			internal class Feed
			{
				internal static RangeReference StreamDesc = new RangeReference(string.Empty, 4, 0, SqlDbType.VarChar);
				internal static RangeReference FeedProdId = new RangeReference(Tabs.Upload, string.Empty, 4, 0, SqlDbType.VarChar);

				internal static RangeReference CoilOutletPressure_Psia = new RangeReference(string.Empty, 28, 0, SqlDbType.Float);
				internal static RangeReference SteamHydrocarbon_Ratio = new RangeReference(string.Empty, 29, 0, SqlDbType.Float);
				internal static RangeReference CoilOutletTemp_C = new RangeReference(string.Empty, 30, 0, SqlDbType.Float);

				internal static RangeReference EthyleneYield_WtPcnt = new RangeReference(string.Empty, 32, 0, SqlDbType.Float, 100.0);
				internal static RangeReference PropyleneEthylene_Ratio = new RangeReference(string.Empty, 33, 0, SqlDbType.Float);
				internal static RangeReference FeedConv_WtPcnt = new RangeReference(string.Empty, 34, 0, SqlDbType.Float, 100.0);
				internal static RangeReference PropyleneMethane_Ratio = new RangeReference(string.Empty, 35, 0, SqlDbType.Float);

				internal static RangeReference FurnConstruction_Year = new RangeReference(string.Empty, 44, 0, SqlDbType.Float);
				internal static RangeReference ResidenceTime_s = new RangeReference(string.Empty, 46, 0, SqlDbType.Float);
				internal static RangeReference Amount_Cur = new RangeReference(string.Empty, 47, 0, SqlDbType.Float);

				private static RangeReference Q1_kMT = new RangeReference(string.Empty, 37, 0, SqlDbType.Float);
				private static RangeReference Q2_kMT = new RangeReference(string.Empty, 38, 0, SqlDbType.Float);
				private static RangeReference Q3_kMT = new RangeReference(string.Empty, 39, 0, SqlDbType.Float);
				private static RangeReference Q4_kMT = new RangeReference(string.Empty, 40, 0, SqlDbType.Float);
				private static RangeReference QT_kMT = new RangeReference(string.Empty, 41, 0, SqlDbType.Float);

				internal static Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						//	Light
						d.Add("Ethane", Streams.Ethane);
						d.Add("EPMix", Streams.EPMix);
						d.Add("Propane", Streams.Propane);
						d.Add("LPG", Streams.LPG);
						d.Add("Butane", Streams.Butane);
						d.Add("FeedLtOther", Streams.FeedLtOther);

						//	Liqud
						d.Add("NaphthaLt", Streams.NaphthaLt);
						d.Add("NaphthaFr", Streams.NaphthaFr);
						d.Add("NaphthaHv", Streams.NaphthaHv);
						d.Add("Raffinate", Streams.Raffinate);
						d.Add("HeavyNGL", Streams.HeavyNGL);
						d.Add("Condensate", Streams.Condensate);
						d.Add("Diesel", Streams.Diesel);
						d.Add("GasOilHv", Streams.GasOilHv);
						d.Add("GasOilHt", Streams.GasOilHt);
						d.Add("FeedLiqOther1", Streams.FeedLiqOther1);
						d.Add("FeedLiqOther2", Streams.FeedLiqOther2);
						d.Add("FeedLiqOther3", Streams.FeedLiqOther3);

						//	Recycle
						d.Add("EthRec", Streams.EthRec);
						d.Add("ProRec", Streams.ProRec);
						d.Add("ButRec", Streams.ButRec);

						return d;
					}
				}

				public class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_StreamsFeed]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return new Streams().LookUpColumn;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("StreamDesc", Feed.StreamDesc);

							d.Add("CoilOutletPressure_Psia", Feed.CoilOutletPressure_Psia);
							d.Add("SteamHydrocarbon_Ratio", Feed.SteamHydrocarbon_Ratio);
							d.Add("CoilOutletTemp_C", Feed.CoilOutletTemp_C);

							d.Add("EthyleneYield_WtPcnt", Feed.EthyleneYield_WtPcnt);
							d.Add("PropyleneEthylene_Ratio", Feed.PropyleneEthylene_Ratio);
							d.Add("FeedConv_WtPcnt", Feed.FeedConv_WtPcnt);
							d.Add("PropyleneMethane_Ratio", Feed.PropyleneMethane_Ratio);

							d.Add("Q1_kMT", Feed.Q1_kMT);
							d.Add("Q2_kMT", Feed.Q2_kMT);
							d.Add("Q3_kMT", Feed.Q3_kMT);
							d.Add("Q4_kMT", Feed.Q4_kMT);

							d.Add("FurnConstruction_Year", Feed.FurnConstruction_Year);
							d.Add("ResidenceTime_s", Feed.ResidenceTime_s);

							d.Add("Amount_Cur", Feed.Amount_Cur);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Feed.Items;
						}
					}
				}

				public class Upload : TransferData.IUploadMultiple
				{
					public string StoredProcedure
					{
						get
						{
							return "[stgFact].[Insert_Quantity]";
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return Feed.Items;
						}
					}

					public Dictionary<string, RangeReference> Parameters
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("@FeedProdID", Feed.FeedProdId);

							d.Add("@Q1Feed", Feed.Q1_kMT);
							d.Add("@Q2Feed", Feed.Q2_kMT);
							d.Add("@Q3Feed", Feed.Q3_kMT);
							d.Add("@Q4Feed", Feed.Q4_kMT);
							d.Add("@AnnFeedProd", Feed.QT_kMT);

							d.Add("@MiscFeed", Feed.StreamDesc);

							return d;
						}
					}
				}
			}
		}
	}
}