﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Capacity
		{
			internal static RangeReference Expansion_Pcnt = new RangeReference(Tabs.T01_01, 19, 9, SqlDbType.Float, 100.0);
			internal static RangeReference Expansion_Date = new RangeReference(Tabs.T01_01, 20, 9, SqlDbType.Date);
			internal static RangeReference CapacityLoss_YN = new RangeReference(Tabs.T01_01, 23, 9, SqlDbType.VarChar);
			internal static RangeReference CapacityLoss_Pcnt = new RangeReference(Tabs.T01_01, 27, 9, SqlDbType.Float, 100.0);
			internal static RangeReference PlantStart_Date = new RangeReference(Tabs.T01_01, 29, 9, SqlDbType.SmallInt);

			internal class Upload : TransferData.IUpload
			{
				private static RangeReference Ethylene_Capacity = new RangeReference(Tabs.T01_01, 10, 6, SqlDbType.Float);
				private static RangeReference Ethylene_CapacitySd = new RangeReference(Tabs.T01_01, 10, 8, SqlDbType.Float);
				private static RangeReference Ethylene_Record = new RangeReference(Tabs.T01_01, 10, 9, SqlDbType.Float);

				private static RangeReference Propylene_Capacity = new RangeReference(Tabs.T01_01, 11, 6, SqlDbType.Float);

				private static RangeReference Olefins_Capacity = new RangeReference(Tabs.T01_01, 12, 6, SqlDbType.Float);
				private static RangeReference Olefins_CapacitySd = new RangeReference(Tabs.T01_01, 12, 8, SqlDbType.Float);
				private static RangeReference Olefins_Record = new RangeReference(Tabs.T01_01, 12, 9, SqlDbType.Float);

				private static RangeReference Feed_Capacity = new RangeReference(Tabs.T01_01, 13, 6, SqlDbType.Float);
				private static RangeReference Recycle_Capacity = new RangeReference(Tabs.T01_01, 14, 6, SqlDbType.Float);

				private static RangeReference ServiceFactor = new RangeReference(Tabs.T01_01, 16, 9, SqlDbType.Float);

				public string StoredProcedure
				{
					get
					{
						return "[stgFact].[Insert_Capacity]";
					}
				}

				public Dictionary<string, RangeReference> Parameters
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("@EthylCapKMTA", Ethylene_Capacity);
						d.Add("@EthylCapMTD", Ethylene_CapacitySd);
						d.Add("@EthylMaxCapMTD", Ethylene_Record);

						d.Add("@PropylCapKMTA", Propylene_Capacity);

						d.Add("@OlefinsCapMTD", Olefins_Capacity);
						d.Add("@OlefinsMaxCapMTD", Olefins_Record);

						d.Add("@FurnaceCapKMTA", Feed_Capacity);
						d.Add("@RecycleCapKMTA", Recycle_Capacity);

						d.Add("@SvcFactor", ServiceFactor);

						d.Add("@ExpansionPcnt", Capacity.Expansion_Pcnt);
						d.Add("@ExpansionDate", Capacity.Expansion_Date);
						d.Add("@CapAllow", Capacity.CapacityLoss_YN);
						d.Add("@CapLossPcnt", Capacity.CapacityLoss_Pcnt);
						d.Add("@PlantStartupDate", Capacity.PlantStart_Date);

						return d;
					}
				}
			}
		}
	}
}