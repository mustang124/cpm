﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Metathesis
		{
			internal partial class OpEx
			{
				private static RangeReference MetaNonVol = new RangeReference(Tabs.T13, 46, 0, SqlDbType.Float);
				private static RangeReference MetaSTVol = new RangeReference(Tabs.T13, 47, 0, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MetaOpEx]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "AccountID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							return Metathesis.Values;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("MetaNonVol", OpEx.MetaNonVol);
							d.Add("MetaSTVol", OpEx.MetaSTVol);

							return d;
						}
					}
				}
			}
		}
	}
}