﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Metathesis
		{
			internal partial class Misc
			{
				private static RangeReference StartUp_Year = new RangeReference(Tabs.T13, 61, 8, SqlDbType.Int);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_MetaMisc]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}


					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("StartUp_Year", Misc.StartUp_Year);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}