﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Polymer
		{
			internal partial class QuantityDescription
			{
				private static RangeReference StreamName = new RangeReference(Tabs.T12_02, 0, 3, SqlDbType.VarChar);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_PolyQuantityDesc]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "StreamID";
						}
					}


					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("StreamName", QuantityDescription.StreamName);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Quantity.Download().Rows;
						}
					}
				}
			}
		}
	}
}