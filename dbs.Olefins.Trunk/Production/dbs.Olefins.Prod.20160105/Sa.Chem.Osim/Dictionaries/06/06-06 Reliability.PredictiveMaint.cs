﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class PredictiveMaint
			{
				private static RangeReference Frequency_Days = new RangeReference(Tabs.T06_06, 0, 6, SqlDbType.Float);

				private static RangeReference Vibration = new RangeReference(Tabs.T06_06, 33, 6, SqlDbType.Float);
				private static RangeReference Compressor = new RangeReference(Tabs.T06_06, 34, 6, SqlDbType.Float);
				private static RangeReference Turbine = new RangeReference(Tabs.T06_06, 35, 6, SqlDbType.Float);
				private static RangeReference Stastical = new RangeReference(Tabs.T06_06, 36, 6, SqlDbType.Float);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityDTPredictiveMaint]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "CauseID";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Frequency_Days", PredictiveMaint.Frequency_Days);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("Vibration", PredictiveMaint.Vibration);
							d.Add("Compressor", PredictiveMaint.Compressor);
							d.Add("Turbine", PredictiveMaint.Turbine);
							d.Add("Stastical", PredictiveMaint.Stastical);

							return d;
						}
					}
				}
			}
		}
	}
}