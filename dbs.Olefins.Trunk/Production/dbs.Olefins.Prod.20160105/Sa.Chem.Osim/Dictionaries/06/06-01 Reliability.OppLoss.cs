﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class Reliability
		{
			internal class LostProduction
			{
				private static RangeReference OppLossName = new RangeReference(Tabs.T06_01, 0, 2, SqlDbType.VarChar);

				private static RangeReference TurnAround = new RangeReference(Tabs.T06_01, 10, 0, SqlDbType.Float);

				private static RangeReference OperError = new RangeReference(Tabs.T06_01, 12, 0, SqlDbType.Float);
				private static RangeReference IntFeedInterrupt = new RangeReference(Tabs.T06_01, 13, 0, SqlDbType.Float);

				private static RangeReference FFPCause1 = new RangeReference(Tabs.T06_01, 15, 0, SqlDbType.Float);
				private static RangeReference FFPCause2 = new RangeReference(Tabs.T06_01, 16, 0, SqlDbType.Float);
				private static RangeReference FFPCause3 = new RangeReference(Tabs.T06_01, 17, 0, SqlDbType.Float);
				private static RangeReference FFPCause4 = new RangeReference(Tabs.T06_01, 18, 0, SqlDbType.Float);

				private static RangeReference Transition = new RangeReference(Tabs.T06_01, 19, 0, SqlDbType.Float);
				private static RangeReference AcetyleneConv = new RangeReference(Tabs.T06_01, 20, 0, SqlDbType.Float);

				private static RangeReference ProcessOther1 = new RangeReference(Tabs.T06_01, 23, 0, SqlDbType.Float);
				private static RangeReference ProcessOther2 = new RangeReference(Tabs.T06_01, 24, 0, SqlDbType.Float);
				private static RangeReference ProcessOther3 = new RangeReference(Tabs.T06_01, 25, 0, SqlDbType.Float);
				private static RangeReference ProcessOther4 = new RangeReference(Tabs.T06_01, 26, 0, SqlDbType.Float);

				private static RangeReference FurnaceProcess = new RangeReference(Tabs.T06_01, 27, 0, SqlDbType.Float);

				private static RangeReference Compressor = new RangeReference(Tabs.T06_01, 29, 0, SqlDbType.Float);
				private static RangeReference OtherRotating = new RangeReference(Tabs.T06_01, 30, 0, SqlDbType.Float);
				private static RangeReference FurnaceMech = new RangeReference(Tabs.T06_01, 31, 0, SqlDbType.Float);
				private static RangeReference Corrosion = new RangeReference(Tabs.T06_01, 33, 0, SqlDbType.Float);
				private static RangeReference ElecDist = new RangeReference(Tabs.T06_01, 34, 0, SqlDbType.Float);
				private static RangeReference ControlSys = new RangeReference(Tabs.T06_01, 35, 0, SqlDbType.Float);
				private static RangeReference OtherFailure = new RangeReference(Tabs.T06_01, 36, 0, SqlDbType.Float);

				private static RangeReference ExtElecFailure = new RangeReference(Tabs.T06_01, 38, 0, SqlDbType.Float);
				private static RangeReference ExtOthUtilFailure = new RangeReference(Tabs.T06_01, 39, 0, SqlDbType.Float);
				private static RangeReference IntElecFailure = new RangeReference(Tabs.T06_01, 40, 0, SqlDbType.Float);
				private static RangeReference IntSteamFailure = new RangeReference(Tabs.T06_01, 41, 0, SqlDbType.Float);
				private static RangeReference IntOthUtilFailure = new RangeReference(Tabs.T06_01, 42, 0, SqlDbType.Float);

				private static RangeReference Demand = new RangeReference(Tabs.T06_01, 44, 0, SqlDbType.Float);
				private static RangeReference ExtFeedInterrupt = new RangeReference(Tabs.T06_01, 45, 0, SqlDbType.Float);
				private static RangeReference CapProject = new RangeReference(Tabs.T06_01, 47, 0, SqlDbType.Float);
				private static RangeReference Strikes = new RangeReference(Tabs.T06_01, 48, 0, SqlDbType.Float);
				private static RangeReference FeedMix = new RangeReference(Tabs.T06_01, 49, 0, SqlDbType.Float);
				private static RangeReference Severity = new RangeReference(Tabs.T06_01, 50, 0, SqlDbType.Float);

				private static RangeReference OtherNonOp1 = new RangeReference(Tabs.T06_01, 52, 0, SqlDbType.Float);
				private static RangeReference OtherNonOp2 = new RangeReference(Tabs.T06_01, 53, 0, SqlDbType.Float);
				private static RangeReference OtherNonOp3 = new RangeReference(Tabs.T06_01, 54, 0, SqlDbType.Float);
				private static RangeReference OtherNonOp4 = new RangeReference(Tabs.T06_01, 55, 0, SqlDbType.Float);


				private static RangeReference Category = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 12, SqlDbType.VarChar);
				private static RangeReference CauseID = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 13, SqlDbType.VarChar);
				private static RangeReference Description = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 14, SqlDbType.VarChar);

				private static RangeReference PrevTotLoss = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 15, SqlDbType.Float);
				private static RangeReference TotLoss = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 16, SqlDbType.Float);
				private static RangeReference AnnDTLoss = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 17, SqlDbType.Float);
				private static RangeReference AnnSDLoss = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 18, SqlDbType.Float);
				private static RangeReference AnnTotLoss = new RangeReference(Tabs.Upload, Tabs.T06_01, 0, 19, SqlDbType.Float);

				private static Dictionary<string, RangeReference> Items
				{
					get
					{
						Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

						d.Add("TurnAround", LostProduction.TurnAround);

						d.Add("OperError", LostProduction.OperError);
						d.Add("IntFeedInterrupt", LostProduction.IntFeedInterrupt);

						d.Add("FFPCause1", LostProduction.FFPCause1);
						d.Add("FFPCause2", LostProduction.FFPCause2);
						d.Add("FFPCause3", LostProduction.FFPCause3);
						d.Add("FFPCause4", LostProduction.FFPCause4);

						d.Add("Transition", LostProduction.Transition);
						d.Add("AcetyleneConv", LostProduction.AcetyleneConv);

						d.Add("ProcessOther1", LostProduction.ProcessOther1);
						d.Add("ProcessOther2", LostProduction.ProcessOther2);
						d.Add("ProcessOther3", LostProduction.ProcessOther3);
						d.Add("ProcessOther4", LostProduction.ProcessOther4);

						d.Add("FurnaceProcess", LostProduction.FurnaceProcess);

						d.Add("Compressor", LostProduction.Compressor);
						d.Add("OtherRotating", LostProduction.OtherRotating);
						d.Add("FurnaceMech", LostProduction.FurnaceMech);
						d.Add("Corrosion", LostProduction.Corrosion);
						d.Add("ElecDist", LostProduction.ElecDist);
						d.Add("ControlSys", LostProduction.ControlSys);
						d.Add("OtherFailure", LostProduction.OtherFailure);

						d.Add("ExtElecFailure", LostProduction.ExtElecFailure);
						d.Add("ExtOthUtilFailure", LostProduction.ExtOthUtilFailure);
						d.Add("IntElecFailure", LostProduction.IntElecFailure);
						d.Add("IntSteamFailure", LostProduction.IntSteamFailure);
						d.Add("IntOthUtilFailure", LostProduction.IntOthUtilFailure);

						d.Add("Demand", LostProduction.Demand);
						d.Add("ExtFeedInterrupt", LostProduction.ExtFeedInterrupt);
						d.Add("CapProject", LostProduction.CapProject);
						d.Add("Strikes", LostProduction.Strikes);
						d.Add("FeedMix", LostProduction.FeedMix);
						d.Add("Severity", LostProduction.Severity);

						d.Add("OtherNonOp1", LostProduction.OtherNonOp1);
						d.Add("OtherNonOp2", LostProduction.OtherNonOp2);
						d.Add("OtherNonOp3", LostProduction.OtherNonOp3);
						d.Add("OtherNonOp4", LostProduction.OtherNonOp4);

						return d;
					}
				}

				internal class History : TransferData.IDownload
				{
					internal static RangeReference downTimeHistory = new RangeReference(Tabs.T06_01, 0, 6, SqlDbType.Float);
					internal static RangeReference slowDownHistory = new RangeReference(Tabs.T06_01, 0, 7, SqlDbType.Float);

					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityOppLoss_History]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "OppLossId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("OppLossName", LostProduction.OppLossName);
							d.Add("DownTimeLoss_MT", History.downTimeHistory);
							d.Add("SlowDownLoss_MT", History.slowDownHistory);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return LostProduction.Items;
						}
					}
				}

				internal class Current : TransferData.IDownload
				{
					internal static RangeReference downTimeCurrent = new RangeReference(Tabs.T06_01, 0, 8, SqlDbType.Float);
					internal static RangeReference slowDownCurrent = new RangeReference(Tabs.T06_01, 0, 9, SqlDbType.Float);

					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_ReliabilityOppLoss_Current]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return "OppLossId";
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("OppLossName", LostProduction.OppLossName);
							d.Add("DownTimeLoss_MT", Current.downTimeCurrent);
							d.Add("SlowDownLoss_MT", Current.slowDownCurrent);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return LostProduction.Items;
						}
					}
				}

				internal class Upload : TransferData.IUploadMultiple
				{
					public string StoredProcedure
					{
						get
						{
							return "[stgFact].[Insert_ProdLoss]";
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return LostProduction.Items;
						}
					}

					public Dictionary<string, RangeReference> Parameters
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("@Category", Category);
							d.Add("@CauseID", CauseID);
							d.Add("@Description", Description);

							d.Add("@PrevDTLoss", History.downTimeHistory);
							d.Add("@PrevSDLoss", History.slowDownHistory);
							d.Add("@DTLoss", Current.downTimeCurrent);
							d.Add("@SDLoss", Current.slowDownCurrent);

							d.Add("@PrevTotLoss", PrevTotLoss);
							d.Add("@TotLoss", TotLoss);
							d.Add("@AnnDTLoss", AnnDTLoss);
							d.Add("@AnnSDLoss", AnnSDLoss);
							d.Add("@AnnTotLoss", AnnTotLoss);

							return d;
						}
					}
				}
			}
		}
	}
}