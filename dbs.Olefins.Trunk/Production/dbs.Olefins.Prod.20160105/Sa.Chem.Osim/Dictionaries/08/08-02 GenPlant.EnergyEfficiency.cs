﻿using System.Collections.Generic;
using System.Data;

namespace Sa.Chem
{
	public partial class Osim
	{
		internal partial class GenPlant
		{
			internal class EnergyEfficiency
			{
				private static RangeReference PyroFurnInletTemp_C = new RangeReference(Tabs.T08_02, 27, 8, SqlDbType.Float);
				private static RangeReference PyroFurnTLESteam_PSIa = new RangeReference(Tabs.T08_02, 34, 8, SqlDbType.Float);
				private static RangeReference DriverGT_YN = new RangeReference(Tabs.T08_02, 38, 8, SqlDbType.VarChar);
				private static RangeReference PyroFurnFlueGasTemp_C = new RangeReference(Tabs.T08_02, 39, 8, SqlDbType.Float);
				private static RangeReference TLEOutletTemp_C = new RangeReference(Tabs.T08_02, 43, 8, SqlDbType.Float);

				private static RangeReference PyroFurnPreheat_Pcnt = new RangeReference(Tabs.T08_02, 29, 8, SqlDbType.Float, 100.0);
				private static RangeReference PyroFurnPrimaryTLE_Pcnt = new RangeReference(Tabs.T08_02, 32, 8, SqlDbType.Float, 100.0);
				private static RangeReference PyroFurnSecondaryTLE_Pcnt = new RangeReference(Tabs.T08_02, 37, 8, SqlDbType.Float, 100.0);
				private static RangeReference PyroFurnFlueGasO2_Pcnt = new RangeReference(Tabs.T08_02, 40, 8, SqlDbType.Float, 100.0);
				private static RangeReference EnergyDeterioration_Pcnt = new RangeReference(Tabs.T08_02, 47, 8, SqlDbType.Float, 100.0);
				private static RangeReference IBSLSteamRequirement_Pcnt = new RangeReference(Tabs.T08_02, 52, 8, SqlDbType.Float, 100.0);

				internal class Download : TransferData.IDownload
				{
					public string StoredProcedure
					{
						get
						{
							return "[fact].[Select_GenPlantEnergyEfficiency]";
						}
					}

					public string LookUpColumn
					{
						get
						{
							return string.Empty;
						}
					}

					public Dictionary<string, RangeReference> Columns
					{
						get
						{
							Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

							d.Add("PyroFurnInletTemp_C", EnergyEfficiency.PyroFurnInletTemp_C);
							d.Add("PyroFurnTLESteam_PSIa", EnergyEfficiency.PyroFurnTLESteam_PSIa);
							d.Add("DriverGT_YN", EnergyEfficiency.DriverGT_YN);
							d.Add("PyroFurnFlueGasTemp_C", EnergyEfficiency.PyroFurnFlueGasTemp_C);
							d.Add("TLEOutletTemp_C", EnergyEfficiency.TLEOutletTemp_C);

							d.Add("PyroFurnPreheat_Pcnt", EnergyEfficiency.PyroFurnPreheat_Pcnt);
							d.Add("PyroFurnPrimaryTLE_Pcnt", EnergyEfficiency.PyroFurnPrimaryTLE_Pcnt);
							d.Add("PyroFurnSecondaryTLE_Pcnt", EnergyEfficiency.PyroFurnSecondaryTLE_Pcnt);
							d.Add("PyroFurnFlueGasO2_Pcnt", EnergyEfficiency.PyroFurnFlueGasO2_Pcnt);
							d.Add("EnergyDeterioration_Pcnt", EnergyEfficiency.EnergyDeterioration_Pcnt);
							d.Add("IBSLSteamRequirement_Pcnt", EnergyEfficiency.IBSLSteamRequirement_Pcnt);

							return d;
						}
					}

					public Dictionary<string, RangeReference> Items
					{
						get
						{
							return new Dictionary<string, RangeReference>();
						}
					}
				}
			}
		}
	}
}