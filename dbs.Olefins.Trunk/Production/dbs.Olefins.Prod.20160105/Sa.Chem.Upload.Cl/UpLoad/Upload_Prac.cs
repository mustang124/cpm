﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPrac(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			//@OthC5             CHAR (1)     = NULL
			//@AffChmPcnt        REAL         = NULL
			//@SteamReq          REAL         = NULL

			string cT08_01 = (includeSpace) ? "Table 8-1" : "Table8-1";
			string cT08_02 = (includeSpace) ? "Table 8-2" : "Table8-2";
			string cT08_03 = (includeSpace) ? "Table 8-3" : "Table8-3";
			string cT08_04 = (includeSpace) ? "Table 8-4" : "Table8-4";
			string cT11_01 = (includeSpace) ? "Table 11" : "Table11";

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Prac]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						#region Non-Discretionary Feedstocks

						wks = wkb.Worksheets[cT11_01];

						c = 11;

						//@AffGasPcntNonDisc REAL         = NULL
						r = 43;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AffGasPcntNonDisc", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@NonAffGasPcnt     REAL         = NULL
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@NonAffGasPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Logistics Costs and Duties

						wks = wkb.Worksheets[cT08_01];

						c = 8;

						//@FeedLogCost       REAL         = NULL
						r = 29;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FeedLogCost", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@EtyLogCost        REAL         = NULL
						r = 32;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EtyLogCost", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PryLogCost        REAL         = NULL
						r = 35;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PryLogCost", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Commercial Integration

						wks = wkb.Worksheets[cT08_01];

						c = 8;

						//@AffGasPcnt        REAL         = NULL
						r = 51;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AffGasPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }
						//@AffEtyPcnt        REAL         = NULL
						r = 53;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AffEtyPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@AffPryPcnt        REAL         = NULL
						r = 54;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AffPryPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@AffOthPcnt        REAL         = NULL
						r = 55;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AffOthPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TotFDCost         REAL         = NULL
						r = 58;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotFDCost", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TotProdRev        REAL         = NULL
						r = 62;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotProdRev", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Plant Integration

						wks = wkb.Worksheets[cT08_02];

						c = 9;

						//@EtyCarrier        CHAR (1)     = NULL
						r = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EtyCarrier", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@ButaExtract       CHAR (1)     = NULL
						r = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ButaExtract", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@AromExtract       CHAR (1)     = NULL
						r = 9;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@AromExtract", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@MTBE              CHAR (1)     = NULL
						r = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MTBE", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@IsoMTBE           CHAR (1)     = NULL
						r = 11;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@IsoMTBE", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@IsoHighPurity     CHAR (1)     = NULL
						r = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@IsoHighPurity", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Butene1           CHAR (1)     = NULL
						r = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Butene1", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Isoprene          CHAR (1)     = NULL
						r = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Isoprene", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@Cyclopent         CHAR (1)     = NULL
						r = 15;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Cyclopent", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						#endregion

						#region Energy Efficiency and Capacity Creep

						wks = wkb.Worksheets[cT08_02];

						c = 8;

						//@InletTemp         REAL         = NULL
						r = 27;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@InletTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FurnPreheatPcnt   REAL         = NULL
						r = 29;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnPreheatPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@FurnOutputPcnt    REAL         = NULL
						r = 32;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnOutputPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@StmPress          REAL         = NULL
						r = 34;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@StmPress", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TLE2Pcnt          REAL         = NULL
						r = 37;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TLE2Pcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@TurbDriver        CHAR (1)     = NULL
						r = 38;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@TurbDriver", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@FurnTemp          REAL         = NULL
						r = 39;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OxyContent        REAL         = NULL
						r = 40;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OxyContent", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OutletTemp        REAL         = NULL
						r = 43;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OutletTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Deterioration     REAL         = NULL
						r = 47;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Deterioration", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@SteamPcnt         REAL         = NULL
						r = 52;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@SteamPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CapCreep          REAL         = NULL
						r = 58;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CapCreep", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						wks = wkb.Worksheets[cT08_03];

						//@LocFactor         REAL         = NULL
						c = 8;
						r = 54;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LocFactor", SqlDbType.Float).Value = ReturnFloat(rng); }

						#region Energy

						wks = wkb.Worksheets[cT08_04];

						c = 5;

						//@PrepHrs           REAL         = NULL
						r = 10;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PrepHrs", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CW                REAL         = NULL
						r = 36;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CW", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CWSupplyTemp      REAL         = NULL
						r = 39;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CWSupplyTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CWReturnTemp      REAL         = NULL
						r = 40;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CWReturnTemp", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@HeatFF            REAL         = NULL
						r = 43;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@HeatFF", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ProcGasCompr      REAL         = NULL
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ProcGasCompr", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ComprEffDesc      VARCHAR (40) = NULL
						r = 49;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ComprEffDesc", SqlDbType.VarChar, 40).Value = ReturnString(rng, 40); }

						//@ExtractSteam      REAL         = NULL
						r = 52;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ExtractSteam", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@CondSteam         REAL         = NULL
						r = 53;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@CondSteam", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@SurfCondVac       REAL         = NULL
						if (StudyYear <= 2007)
						{
							r = 55;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@SurfCondVac", SqlDbType.Float).Value = ReturnFloat(rng); }
						}

						//@DutyPPHE          REAL         = NULL
						if (StudyYear >= 2009) { r = 58; } else { r = 60; }
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@DutyPPHE", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@QuenchOilHM       REAL         = NULL
						if (StudyYear >= 2009) { r = 61; } else { r = 63; }
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@QuenchOilHM", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@QuenchOilDS       REAL         = NULL
						if (StudyYear >= 2009) { r = 64; } else { r = 66; }
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@QuenchOilDS", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPrac", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Prac]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}