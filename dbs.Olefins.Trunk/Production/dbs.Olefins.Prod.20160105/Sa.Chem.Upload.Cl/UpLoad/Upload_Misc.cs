﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadMisc(Excel.Workbook wkb, string Refnum, uint StudyYear, bool includeSpace)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Misc]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						#region Other Light Feed (Table2A-1)

						string cT02_A1 = (includeSpace) ? "Table 2A-1" : "Table2A-1";
						wks = wkb.Worksheets[cT02_A1];

						//@LtFeedPriceBasis      REAL         = NULL
						c = 8;
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LtFeedPriceBasis", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Supplemental Feeds (Table2C)

						string cT02_C = (includeSpace) ? "Table 2C" : "Table2C";
						wks = wkb.Worksheets[cT02_C];

						c = 3;

						//@EthPcntTot            REAL         = NULL
						r = 44;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthPcntTot", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ProPcntTot            REAL         = NULL
						r = 45;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ProPcntTot", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ButPcntTot            REAL         = NULL
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ButPcntTot", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ROG_CGCI              CHAR (1)     = NULL
						r = 53;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_CGCI", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@ROG_CGCD              CHAR (1)     = NULL
						r = 54;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_CGCD", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@ROG_C2RSF             CHAR (1)     = NULL
						r = 55;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_C2RSF", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@ROG_C3RSF             CHAR (1)     = NULL
						r = 56;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_C3RSF", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						#endregion

						#region Personnel (Table5A and Table5B)

						string cT05_A = (includeSpace) ? "Table 5A" : "Table5A";
						wks = wkb.Worksheets[cT05_A];

						//@OCCAbsence            REAL         = NULL
						c = 6;
						r = 42;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OCCAbsence", SqlDbType.Float).Value = ReturnFloat(rng); }

						string cT05_B = (includeSpace) ? "Table 5B" : "Table5B";
						wks = wkb.Worksheets[cT05_B];

						//@MPSAbsence            REAL         = NULL
						c = 5;
						r = 44;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MPSAbsence", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Furnace Limited (Table6-4)

						string cT06_04 = (includeSpace) ? "Table 6-4" : "Table6-4";
						wks = wkb.Worksheets[cT06_04];

						//@PctFurnRelyLimit      REAL         = NULL
						c = 3;
						r = 47;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PctFurnRelyLimit", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Other Liquid Feeds (Table2A-2)

						string cT02_A2 = (includeSpace) ? "Table 2A-2" : "Table2A-2";
						wks = wkb.Worksheets[cT02_A2];

						r = 4;

						//@OthLiqFeedName1       VARCHAR (25) = NULL
						c = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedName1", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						//@OthLiqFeedName2       VARCHAR (25) = NULL
						c = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedName2", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						//@OthLiqFeedName3       VARCHAR (25) = NULL
						c = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedName3", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						r = 47;

						//@OthLiqFeedPriceBasis1 REAL         = NULL
						c = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedPriceBasis1", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthLiqFeedPriceBasis2 REAL         = NULL
						c = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedPriceBasis2", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthLiqFeedPriceBasis3 REAL         = NULL
						c = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedPriceBasis3", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Invalid

						if (StudyYear <= 1989)
						{
							#region Refinery Off Gas

							#endregion

							//@SplFeedEnergy         REAL         = NULL
							r = 0;
							c = 0;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@SplFeedEnergy", SqlDbType.Float).Value = ReturnFloat(rng); }

							//@TotCompOutage         REAL         = NULL
							r = 0;
							c = 0;
							rng = wks.Cells[r, c];
							if (RangeHasValue(rng)) { cmd.Parameters.Add("@TotCompOutage", SqlDbType.Float).Value = ReturnFloat(rng); }
						}

						#endregion

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMisc", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Misc]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		public void UpLoadMisc_SEEC(Excel.Workbook wkb, string Refnum, uint StudyYear)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			try
			{
				using (SqlConnection cn = new SqlConnection(Chem.Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Misc]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						#region Other Light Feed (Table2A-1)

						string cT02_A1 = "Table2A-1";
						wks = wkb.Worksheets[cT02_A1];

						//@LtFeedPriceBasis      REAL         = NULL
						c = 8;
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@LtFeedPriceBasis", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						#region Supplemental Feeds (Table2C)

						string cT02_C = "Table2C";
						wks = wkb.Worksheets[cT02_C];

						c = 3;

						//@EthPcntTot            REAL         = NULL
						r = 44;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthPcntTot", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ProPcntTot            REAL         = NULL
						r = 45;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ProPcntTot", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ButPcntTot            REAL         = NULL
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ButPcntTot", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@ROG_CGCI              CHAR (1)     = NULL
						r = 53;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_CGCI", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@ROG_CGCD              CHAR (1)     = NULL
						r = 54;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_CGCD", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@ROG_C2RSF             CHAR (1)     = NULL
						r = 55;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_C2RSF", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						//@ROG_C3RSF             CHAR (1)     = NULL
						r = 56;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@ROG_C3RSF", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

						#endregion

						#region Other Liquid Feeds (Table2A-2)

						string cT02_A2 = "Table2A-2";
						wks = wkb.Worksheets[cT02_A2];

						r = 4;

						//@OthLiqFeedName1       VARCHAR (25) = NULL
						c = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedName1", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						//@OthLiqFeedName2       VARCHAR (25) = NULL
						c = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedName2", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						//@OthLiqFeedName3       VARCHAR (25) = NULL
						c = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedName3", SqlDbType.VarChar, 25).Value = ReturnString(rng, 25); }

						r = 47;

						//@OthLiqFeedPriceBasis1 REAL         = NULL
						c = 12;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedPriceBasis1", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthLiqFeedPriceBasis2 REAL         = NULL
						c = 13;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedPriceBasis2", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@OthLiqFeedPriceBasis3 REAL         = NULL
						c = 14;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@OthLiqFeedPriceBasis3", SqlDbType.Float).Value = ReturnFloat(rng); }

						#endregion

						cmd.ExecuteNonQuery();
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMisc", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Misc]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}
	}
}