﻿CREATE TABLE [dbo].[Inventory_LU] (
    [TankType]    CHAR (2)     NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK___4__14] PRIMARY KEY CLUSTERED ([TankType] ASC)
);

