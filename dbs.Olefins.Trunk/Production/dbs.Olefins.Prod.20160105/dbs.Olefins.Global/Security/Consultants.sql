﻿CREATE ROLE [Consultants]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CJM';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CLC';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'DBB';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JGY';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JSJ';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'RBJ';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'REB';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'FSW';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JPH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'SSW';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'WDW';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'MGV';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JAB';

