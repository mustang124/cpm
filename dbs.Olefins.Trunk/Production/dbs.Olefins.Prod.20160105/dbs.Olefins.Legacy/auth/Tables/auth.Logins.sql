﻿CREATE TABLE [auth].[Logins] (
    [LoginId]        INT                IDENTITY (1, 1) NOT NULL,
    [LoginTag]       NVARCHAR (254)     NOT NULL,
    [Active]         BIT                CONSTRAINT [DF_Logins_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Logins_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Logins_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Logins_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Logins_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_LoginIds] PRIMARY KEY CLUSTERED ([LoginId] ASC),
    CONSTRAINT [CL_Logins_UserTag] CHECK ([LoginTag]<>''),
    CONSTRAINT [UK_Logins_UserTag] UNIQUE NONCLUSTERED ([LoginTag] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Logins_Active]
    ON [auth].[Logins]([LoginId] ASC)
    INCLUDE([LoginTag]) WHERE ([Active]=(1));


GO

CREATE TRIGGER [auth].[t_LoginIds_u]
	ON [auth].[Logins]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [auth].[Logins]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[auth].[Logins].[LoginId]		= INSERTED.[LoginId];

END;