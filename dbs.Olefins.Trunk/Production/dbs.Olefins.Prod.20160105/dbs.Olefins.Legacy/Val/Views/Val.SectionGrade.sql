﻿CREATE View [Val].[SectionGrade] as
Select g.Refnum, g.CoLoc, g.Version, w.Section, Score = SUM(Score)
, Grade = SUM(Score) * s.Slope + s.Intercept
, WtFactor = AVG(s.WtFactor)
, NumReds = Sum(NumReds)
, NumBlues = Sum(NumBlues)
, NumTeals = Sum(NumTeals)
, NumGreens = Sum(NumGreens)
, PercentRed = CASE WHEN Sum(NumReds+NumBlues+NumTeals+NumGreens) > 0 THEN Sum(NumReds)/Sum(NumReds+NumBlues+NumTeals+NumGreens) * 100 ELSE 0 END
, VersionTime = MIN(VersionTime)
from Val.DCGrade g JOIN Val.Weighting w on w.Category=g.Category and g.RefineryType = w.RefineryType
join Val.Versions v on v.Refnum=g.Refnum and v.Version=g.Version
Join Val.Sections s on s.Section=w.Section and s.RefineryType = g.RefineryType and s.StudyYear = g.StudyYear
Group by g.Refnum, g.CoLoc, g.Version, w.Section, s.Slope, s.Intercept
