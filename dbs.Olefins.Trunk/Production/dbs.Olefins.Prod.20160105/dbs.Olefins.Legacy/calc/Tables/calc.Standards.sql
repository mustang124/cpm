﻿CREATE TABLE [calc].[Standards] (
    [MethodologyId]  INT                NOT NULL,
    [SubmissionId]   INT                NOT NULL,
    [StandardId]     INT                NOT NULL,
    [ProcessUnitId]  INT                NOT NULL,
    [StandardValue]  FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Standards_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Standards_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Standards_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Standards_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Standards] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [StandardId] ASC, [ProcessUnitId] ASC),
    CONSTRAINT [FK_Standards_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Standards_ProcessUnit] FOREIGN KEY ([ProcessUnitId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId]),
    CONSTRAINT [FK_Standards_Standard_LookUp] FOREIGN KEY ([StandardId]) REFERENCES [dim].[Standard_LookUp] ([StandardId]),
    CONSTRAINT [FK_Standards_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Standards_Aggregate]
    ON [calc].[Standards]([MethodologyId] ASC, [StandardId] ASC, [ProcessUnitId] ASC)
    INCLUDE([SubmissionId], [StandardValue]);


GO

CREATE TRIGGER [calc].[t_Standards_u]
	ON [calc].[Standards]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[Standards]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[Standards].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[Standards].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[calc].[Standards].[StandardId]		= INSERTED.[StandardId]
		AND	[calc].[Standards].[ProcessUnitId]	= INSERTED.[ProcessUnitId];

END;