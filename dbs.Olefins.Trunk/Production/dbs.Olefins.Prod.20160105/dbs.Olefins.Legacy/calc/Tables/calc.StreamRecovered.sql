﻿CREATE TABLE [calc].[StreamRecovered] (
    [MethodologyId]      INT                NOT NULL,
    [SubmissionId]       INT                NOT NULL,
    [StreamNumber]       INT                NOT NULL,
    [StreamId]           INT                NOT NULL,
    [Quantity_Dur_kMT]   FLOAT (53)         NOT NULL,
    [Quantity_Ann_kMT]   FLOAT (53)         NOT NULL,
    [Recovered_WtPcnt]   FLOAT (53)         NOT NULL,
    [_Recovered_Dur_kMT] AS                 (CONVERT([float],([Quantity_Dur_kMT]*[Recovered_WtPcnt])/(100.0),0)) PERSISTED NOT NULL,
    [_Recovered_Ann_kMT] AS                 (CONVERT([float],([Quantity_Ann_kMT]*[Recovered_WtPcnt])/(100.0),0)) PERSISTED NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_StreamRecovered_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamRecovered] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [StreamNumber] ASC),
    CONSTRAINT [CR_StreamRecovered_Quantity_Ann_kMT_MinIncl_0.0] CHECK ([Quantity_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamRecovered_Quantity_Dur_kMT_MinIncl_0.0] CHECK ([Quantity_Dur_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MaxIncl_100.0] CHECK ([Recovered_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MinIncl_0.0] CHECK ([Recovered_WtPcnt]>=(0.0)),
    CONSTRAINT [FK_StreamRecovered_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_StreamRecovered_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_StreamRecovered_StreamQuantity] FOREIGN KEY ([SubmissionId], [StreamNumber]) REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber]),
    CONSTRAINT [FK_StreamRecovered_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO
CREATE NONCLUSTERED INDEX [IX_StreamRecovered_StreamId]
    ON [calc].[StreamRecovered]([StreamId] ASC)
    INCLUDE([MethodologyId], [SubmissionId], [_Recovered_Dur_kMT], [_Recovered_Ann_kMT]);


GO

CREATE TRIGGER [calc].[t_StreamRecovered_u]
	ON [calc].[StreamRecovered]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[StreamRecovered]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[StreamRecovered].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[StreamRecovered].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[calc].[StreamRecovered].[StreamNumber]		= INSERTED.[StreamNumber];

END;