﻿CREATE VIEW [calc].[ContainedProduct_CompositeHVC]
WITH SCHEMABINDING
AS
SELECT
	c.[MethodologyId],
	c.[SubmissionId],
	c.[ComponentId],
	SUM(c.[Component_Dur_kMT])		[ContainedProduct_Dur_kMT],
	SUM(c.[Component_Ann_kMT])		[ContainedProduct_Ann_kMT],
	COUNT_BIG(*)					[Items]
FROM [calc].[StreamComposition]			c
INNER JOIN [dim].[Component_Bridge]				k
	ON	k.[MethodologyId]	= c.[MethodologyId]
	AND	k.[DescendantId]	= c.[ComponentId]
	AND	k.[ComponentId]		= 3
INNER JOIN [dim].[Stream_Bridge]				s
	ON	s.[MethodologyId]	= c.[MethodologyId]
	AND	s.[DescendantId]	= c.[StreamId]	
	AND	s.[StreamId]		IN (85, 102, 113)
WHERE c.[StreamNumber] >= 5000 AND c.[StreamNumber] <= 5999
GROUP BY
	c.[MethodologyId],
	c.[SubmissionId],
	c.[ComponentId]
GO
CREATE UNIQUE CLUSTERED INDEX [UX_ContainedProduct_CompositeHVC]
    ON [calc].[ContainedProduct_CompositeHVC]([MethodologyId] DESC, [SubmissionId] DESC, [ComponentId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ContainedProduct_CompositeHVC]
    ON [calc].[ContainedProduct_CompositeHVC]([MethodologyId] DESC, [SubmissionId] DESC, [ComponentId] ASC)
    INCLUDE([ContainedProduct_Dur_kMT], [ContainedProduct_Ann_kMT]);

