﻿CREATE PROCEDURE [calc].[Delete_StandardEnergy_Supplemental]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DELETE FROM [calc].[StandardEnergy_Supplemental]
	WHERE	[MethodologyId]	= @MethodologyId
		AND	[SubmissionId]	= @SubmissionId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;