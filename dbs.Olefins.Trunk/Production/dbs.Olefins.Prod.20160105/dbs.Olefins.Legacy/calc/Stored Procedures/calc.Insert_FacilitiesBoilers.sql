﻿CREATE PROCEDURE [calc].[Insert_FacilitiesBoilers]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @FacilityId		INT = [dim].[Return_FacilityId]('BoilFiredSteam');

	INSERT INTO [calc].[FacilitiesBoilers]([MethodologyId], [SubmissionId], [FacilityId], [Rate_kLbHr])
	SELECT
		@MethodologyId,
		f.[SubmissionId],
		f.[FacilityId],
		f.[Rate_kLbHr]
	FROM [fact].[FacilitiesBoilers_FiredSteam]	f WITH (NOEXPAND)
	WHERE	f.[SubmissionId]	= @SubmissionId
		AND	f.[FacilityId]		= @FacilityId;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;