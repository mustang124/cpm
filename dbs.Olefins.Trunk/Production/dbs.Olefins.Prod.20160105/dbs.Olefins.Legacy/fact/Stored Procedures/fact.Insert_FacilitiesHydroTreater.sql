﻿CREATE PROCEDURE [fact].[Insert_FacilitiesHydroTreater]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[FacilitiesHydroTreater]([SubmissionId], [FacilityId], [Quantity_kBSD], [HydroTreaterTypeId], [Pressure_PSIg], [Processed_kMT], [Processed_Pcnt], [Density_SG])
	SELECT
		h.[SubmissionId],
		h.[FacilityId],
		h.[Quantity_kBSD],
		h.[HydroTreaterTypeId],
		h.[Pressure_PSIg],
		h.[Processed_kMT],
		h.[Processed_Pcnt],
		h.[Density_SG]
	FROM [stage].[FacilitiesHydroTreater]		h
	INNER JOIN [stage].[Facilities]				f
		ON	f.[SubmissionId]	= h.[SubmissionId]
		AND	f.[FacilityId]		= h.[FacilityId]
		AND	f.[Unit_Count]		> 0
	WHERE	h.[SubmissionId]	= @SubmissionId
		AND	h.[Quantity_kBSD]	> 0.0;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;