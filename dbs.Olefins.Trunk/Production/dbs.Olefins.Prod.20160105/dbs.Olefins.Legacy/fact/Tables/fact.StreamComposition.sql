﻿CREATE TABLE [fact].[StreamComposition] (
    [SubmissionId]     INT                NOT NULL,
    [StreamNumber]     INT                NOT NULL,
    [ComponentId]      INT                NOT NULL,
    [Component_WtPcnt] FLOAT (53)         NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_StreamComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)     CONSTRAINT [DF_StreamComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamComposition] PRIMARY KEY CLUSTERED ([SubmissionId] DESC, [StreamNumber] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MinIncl_0.0] CHECK ([Component_WtPcnt]>=(0.0)),
    CONSTRAINT [CR_StreamComposition_Component_WtPcnt_MsxIncl_100.0] CHECK ([Component_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_StreamComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_StreamComposition_StreamQuantity] FOREIGN KEY ([SubmissionId], [StreamNumber]) REFERENCES [fact].[StreamQuantity] ([SubmissionId], [StreamNumber]),
    CONSTRAINT [FK_StreamComposition_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_StreamComposition_u]
	ON [fact].[StreamComposition]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[StreamComposition]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[StreamComposition].[SubmissionId]	= INSERTED.[SubmissionId]
		AND	[fact].[StreamComposition].[StreamNumber]	= INSERTED.[StreamNumber]
		AND	[fact].[StreamComposition].[ComponentId]	= INSERTED.[ComponentId];

END;