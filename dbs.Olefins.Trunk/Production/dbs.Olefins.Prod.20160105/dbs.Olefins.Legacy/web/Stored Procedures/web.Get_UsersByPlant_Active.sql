﻿CREATE PROCEDURE [web].[Get_UsersByPlant_Active]
(
	@PlantId		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		lbp.[JoinId],
		lbp.[PlantId],
		lbp.[LoginId],
		lbp.[PlantName],
		lbp.[LoginTag]		[UserName],
		lbp.[NameFirst]		[FirstName],
		lbp.[NameLast]		[LastName],
		lbp.[eMail],
		lbp.[_NameComma]	[LastFirst],
		lbp.[_NameFull]		[NameFull]
	FROM [auth].[Get_LoginsByPlant_Active](@PlantId)		lbp
	ORDER BY
		lbp.[NameLast]	ASC,
		lbp.[NameFirst]	ASC,
		lbp.[eMail]		ASC;

END;