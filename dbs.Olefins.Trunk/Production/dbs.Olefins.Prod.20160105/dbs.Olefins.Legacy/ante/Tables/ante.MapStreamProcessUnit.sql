﻿CREATE TABLE [ante].[MapStreamProcessUnit] (
    [MethodologyId]  INT                NOT NULL,
    [StreamId]       INT                NOT NULL,
    [ProcessUnitId]  INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapStreamProcessUnit_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_MapStreamProcessUnit_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_MapStreamProcessUnit_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_MapStreamProcessUnit_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_MapStreamProcessUnit] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC),
    CONSTRAINT [FK_MapStreamProcessUnit_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_MapStreamProcessUnit_ProcessUnit] FOREIGN KEY ([ProcessUnitId]) REFERENCES [dim].[ProcessUnit_LookUp] ([ProcessUnitId]),
    CONSTRAINT [FK_MapStreamProcessUnit_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_MapStreamProcessUnit_u]
	ON [ante].[MapStreamProcessUnit]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapStreamProcessUnit]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[MapStreamProcessUnit].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[MapStreamProcessUnit].[StreamId]		= INSERTED.[StreamId];

END;