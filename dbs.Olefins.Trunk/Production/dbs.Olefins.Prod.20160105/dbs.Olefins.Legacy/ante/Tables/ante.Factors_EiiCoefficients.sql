﻿CREATE TABLE [ante].[Factors_EiiCoefficients] (
    [MethodologyId]        INT                NOT NULL,
    [StreamId]             INT                NOT NULL,
    [HvcYield_Coefficient] FLOAT (53)         NULL,
    [Energy_Coefficient]   FLOAT (53)         NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_Factors_EiiCoefficients_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (128)     CONSTRAINT [DF_Factors_EiiCoefficients_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (128)     CONSTRAINT [DF_Factors_EiiCoefficients_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (128)     CONSTRAINT [DF_Factors_EiiCoefficients_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Factors_EiiCoefficients] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC),
    CONSTRAINT [CR_Factors_EiiCoefficients_Energy_Coefficient_MaxIncl_0.0] CHECK ([Energy_Coefficient]<=(0.0)),
    CONSTRAINT [CR_Factors_EiiCoefficients_HvcYield_Coefficient_MaxIncl_0.0] CHECK ([HvcYield_Coefficient]<=(0.0)),
    CONSTRAINT [FK_Factors_EiiCoefficients_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Factors_EiiCoefficients_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_Factors_EiiCoefficients_u]
	ON [ante].[Factors_EiiCoefficients]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[Factors_EiiCoefficients]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[Factors_EiiCoefficients].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[Factors_EiiCoefficients].[StreamId]			= INSERTED.[StreamId];

END;