﻿CREATE PROCEDURE [ante].[Merge_MapFactorStreams]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapFactorStream] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_StreamId('PropyleneCG'), dim.Return_FactorId('PropyleneAdjCG')),
			(@MethodologyId, dim.Return_StreamId('PropyleneRG'), dim.Return_FactorId('PropyleneAdjRG'))
	)
	AS Source([MethodologyId], [StreamId], [FactorId])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[FactorId]		= Source.[FactorId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [FactorId])
		VALUES([MethodologyId], [StreamId], [FactorId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;