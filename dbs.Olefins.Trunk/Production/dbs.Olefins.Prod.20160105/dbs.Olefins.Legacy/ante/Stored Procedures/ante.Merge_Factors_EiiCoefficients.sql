﻿CREATE PROCEDURE [ante].[Merge_Factors_EiiCoefficients]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[Factors_EiiCoefficients] AS Target
	USING
	(
		VALUES
			(@MethodologyId, [dim].[Return_StreamId]('Ethane'),		NULL,		NULL),
			(@MethodologyId, [dim].[Return_StreamId]('Propane'),	-0.1937,	-1.743),
			(@MethodologyId, [dim].[Return_StreamId]('Butane'),		-0.2886,	-2.312),
			(@MethodologyId, [dim].[Return_StreamId]('Condensate'),	-0.3252,	-2.489),
			(@MethodologyId, [dim].[Return_StreamId]('HeavyNGL'),	-0.3252,	-2.489),
			(@MethodologyId, [dim].[Return_StreamId]('NaphthaLt'),	-0.2586,	-2.296),
			(@MethodologyId, [dim].[Return_StreamId]('Raffinate'),	-0.2586,	-2.296),
			(@MethodologyId, [dim].[Return_StreamId]('NaphthaFr'),	-0.2888,	-2.435),
			(@MethodologyId, [dim].[Return_StreamId]('NaphthaHv'),	-0.3252,	-2.53),
			(@MethodologyId, [dim].[Return_StreamId]('Diesel'),		-0.3401,	-2.696),
			(@MethodologyId, [dim].[Return_StreamId]('GasOilHv'),	-0.4199,	-2.914),
			(@MethodologyId, [dim].[Return_StreamId]('GasOilHt'),	-0.3191,	-2.518),
			(@MethodologyId, [dim].[Return_StreamId]('Methane'),	-1.1,		NULL)
	)
	AS Source([MethodologyId], [StreamId], [HvcYield_Coefficient], [Energy_Coefficient])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	WHEN MATCHED THEN UPDATE SET
		Target.[HvcYield_Coefficient]		= Source.[HvcYield_Coefficient],
		Target.[Energy_Coefficient]		= Source.[Energy_Coefficient]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [HvcYield_Coefficient], [Energy_Coefficient])
		VALUES([MethodologyId], [StreamId], [HvcYield_Coefficient], [Energy_Coefficient])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;