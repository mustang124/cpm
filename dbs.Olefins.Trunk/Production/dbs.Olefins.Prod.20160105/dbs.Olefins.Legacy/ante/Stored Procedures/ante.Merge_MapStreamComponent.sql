﻿CREATE PROCEDURE [ante].[Merge_MapStreamComponent]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapStreamComponent] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_StreamId('ProdOlefins'),	dim.Return_ComponentId('ProdOlefins'),	  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('Ethylene'),		dim.Return_ComponentId('C2H4'),			  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('Propylene'),		dim.Return_ComponentId('C3H6'),			  0.0,   0.0),

			-------------------------------------------------------------------------------------------------------

			(@MethodologyId, dim.Return_StreamId('Ethane'),			dim.Return_ComponentId('C2H6'),			  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('Propane'),		dim.Return_ComponentId('C3H8'),			  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('Butane'),			dim.Return_ComponentId('C4H10'),		  0.0,   0.0),

			-------------------------------------------------------------------------------------------------------
			(@MethodologyId, dim.Return_StreamId('DilMoGas'),		dim.Return_ComponentId('Other'),		  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('SuppGasOil'),		dim.Return_ComponentId('PyroFuelOil'),	  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('SuppWashOil'),	dim.Return_ComponentId('PyroGasOil'),	  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('SuppNitrogen'),	dim.Return_ComponentId('Inerts'),		  0.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('SuppOther'),		dim.Return_ComponentId('Inerts'),		  0.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('Loss'),			dim.Return_ComponentId('Loss'),			  0.0,   0.0),

			-------------------------------------------------------------------------------------------------------

			(@MethodologyId, dim.Return_StreamId('Acetylene'),		dim.Return_ComponentId('C2H2'),			100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('Butadiene'),		dim.Return_ComponentId('C4H6'),			100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('IsoButylene'),	dim.Return_ComponentId('C4H8'),			100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('C4Oth'),			dim.Return_ComponentId('C4H10'),		100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('Benzene'),		dim.Return_ComponentId('C6H6'),			100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('PyroGasoline'),	dim.Return_ComponentId('Other'),		100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('PyroGasOil'),		dim.Return_ComponentId('PyroGasOil'),	100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('PyroFuelOil'),	dim.Return_ComponentId('PyroFuelOil'),	100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('AcidGas'),		dim.Return_ComponentId('Inerts'),		100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('LossFlareVent'),	dim.Return_ComponentId('Loss'),			100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('LossMeasure'),	dim.Return_ComponentId('Loss'),			100.0,   0.0),
			(@MethodologyId, dim.Return_StreamId('LossOther'),		dim.Return_ComponentId('Loss'),			100.0,   0.0),
			
			-------------------------------------------------------------------------------------------------------
			
			(@MethodologyId, dim.Return_StreamId('ConcBenzene'),	dim.Return_ComponentId('C6H6'),			100.0,   0.0),	-- 100.0
			(@MethodologyId, dim.Return_StreamId('ConcButadiene'),	dim.Return_ComponentId('C4H6'),			100.0,   0.0),	-- 100.0
			(@MethodologyId, dim.Return_StreamId('ConcEthylene'),	dim.Return_ComponentId('C2H4'),			100.0,   0.0),	-- 100.0
			(@MethodologyId, dim.Return_StreamId('ConcPropylene'),	dim.Return_ComponentId('C3H6'),			100.0,   0.0),	-- 100.0

			(@MethodologyId, dim.Return_StreamId('RogC4Plus'),		dim.Return_ComponentId('C4H10'),		100.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('RogEthane'),		dim.Return_ComponentId('C2H6'),			100.0,   0.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('RogEthylene'),	dim.Return_ComponentId('C2H4'),			100.0,   0.0),	-- 100.0
			(@MethodologyId, dim.Return_StreamId('RogHydrogen'),	dim.Return_ComponentId('H2'),			100.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('RogInerts'),		dim.Return_ComponentId('Inerts'),		100.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('RogMethane'),		dim.Return_ComponentId('CH4'),			100.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('RogPropane'),		dim.Return_ComponentId('C3H8'),			100.0,   0.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('RogPropylene'),	dim.Return_ComponentId('C3H6'),			100.0,   0.0),	-- 100.0

			(@MethodologyId, dim.Return_StreamId('DilBenzene'),		dim.Return_ComponentId('C6H6'),			100.0,   0.0),	-- 100.0
			(@MethodologyId, dim.Return_StreamId('DilPropylene'),	dim.Return_ComponentId('C3H6'),			100.0,   0.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('DilButadiene'),	dim.Return_ComponentId('C4H6'),			100.0,   0.0),	-- 100.0
			(@MethodologyId, dim.Return_StreamId('DilButane'),		dim.Return_ComponentId('C4H10'),		100.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('DilButylene'),	dim.Return_ComponentId('C4H8'),			100.0,   0.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('DilEthane'),		dim.Return_ComponentId('C2H6'),			100.0,   0.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('DilEthylene'),	dim.Return_ComponentId('C2H4'),			100.0,   0.0),	-- 100.0
			(@MethodologyId, dim.Return_StreamId('DilHydrogen'),	dim.Return_ComponentId('H2'),			100.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('DilMethane'),		dim.Return_ComponentId('CH4'),			100.0, 100.0),	--   0.0
			(@MethodologyId, dim.Return_StreamId('DilPropane'),		dim.Return_ComponentId('C3H8'),			100.0,   0.0)	-- 100.0
	)
	AS Source([MethodologyId], [StreamId], [ComponentId], [Component_WtPcnt], [Recovered_WtPcnt])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	AND	Target.[ComponentId]		= Source.[ComponentId]
	WHEN MATCHED THEN UPDATE SET
		Target.[Component_WtPcnt]	= Source.[Component_WtPcnt],
		Target.[Recovered_WtPcnt]	= Source.[Recovered_WtPcnt]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [ComponentId], [Component_WtPcnt], [Recovered_WtPcnt])
		VALUES([MethodologyId], [StreamId], [ComponentId], [Component_WtPcnt], [Recovered_WtPcnt])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;