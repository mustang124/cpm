﻿CREATE PROCEDURE [ante].[Merge_MapFactorFeedClass]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[MapFactorFeedClass] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_FeedClassId('1'), dim.Return_FactorId('Ethane')),
		(@MethodologyId, dim.Return_FeedClassId('2'), dim.Return_FactorId('LPG')),
		(@MethodologyId, dim.Return_FeedClassId('3'), dim.Return_FactorId('Liquid')),
		(@MethodologyId, dim.Return_FeedClassId('4'), dim.Return_FactorId('Liquid')),

		(@MethodologyId, dim.Return_FeedClassId('1'), dim.Return_FactorId('SuppEthane')),
		(@MethodologyId, dim.Return_FeedClassId('2'), dim.Return_FactorId('SuppLPG')),
		(@MethodologyId, dim.Return_FeedClassId('3'), dim.Return_FactorId('SuppLiquid')),
		(@MethodologyId, dim.Return_FeedClassId('4'), dim.Return_FactorId('SuppLiquid')),

		(@MethodologyId, dim.Return_FeedClassId('1'), dim.Return_FactorId('CompEthane')),
		(@MethodologyId, dim.Return_FeedClassId('2'), dim.Return_FactorId('CompLPG')),
		(@MethodologyId, dim.Return_FeedClassId('3'), dim.Return_FactorId('CompLiquid')),
		(@MethodologyId, dim.Return_FeedClassId('4'), dim.Return_FactorId('CompLiquid'))
	)
	AS Source([MethodologyId], [FeedClassId], [FactorId])
	ON	Target.[MethodologyId]		= Source.[MethodologyId]
	AND	Target.[FeedClassId]		= Source.[FeedClassId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [FeedClassId], [FactorId])
		VALUES([MethodologyId], [FeedClassId], [FactorId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;