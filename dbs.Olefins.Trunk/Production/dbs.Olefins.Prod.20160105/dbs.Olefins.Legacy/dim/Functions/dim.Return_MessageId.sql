﻿CREATE FUNCTION [dim].[Return_MessageId]
(
	@MessageTag		VARCHAR(42)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Id	INT;

	SELECT @Id = l.[MessageId]
	FROM [dim].[Message_LookUp]	l
	WHERE l.[MessageTag] = @MessageTag;

	RETURN @Id;

END;