﻿CREATE TABLE [dim].[Component_LookUp] (
    [ComponentId]     INT                IDENTITY (1, 1) NOT NULL,
    [ComponentTag]    VARCHAR (42)       NOT NULL,
    [ComponentName]   VARCHAR (84)       NOT NULL,
    [ComponentDetail] VARCHAR (256)      NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_Component_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (128)     CONSTRAINT [DF_Component_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (128)     CONSTRAINT [DF_Component_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (128)     CONSTRAINT [DF_Component_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Component_LookUp] PRIMARY KEY CLUSTERED ([ComponentId] ASC),
    CONSTRAINT [CL_Component_LookUp_ComponentsDetail] CHECK ([ComponentDetail]<>''),
    CONSTRAINT [CL_Component_LookUp_ComponentsName] CHECK ([ComponentName]<>''),
    CONSTRAINT [CL_Component_LookUp_ComponentsTag] CHECK ([ComponentTag]<>''),
    CONSTRAINT [UK_Component_LookUp_ComponentsDetail] UNIQUE NONCLUSTERED ([ComponentDetail] ASC),
    CONSTRAINT [UK_Component_LookUp_ComponentsName] UNIQUE NONCLUSTERED ([ComponentName] ASC),
    CONSTRAINT [UK_Component_LookUp_ComponentsTag] UNIQUE NONCLUSTERED ([ComponentTag] ASC)
);


GO

CREATE TRIGGER [dim].[t_Component_LookUp_u]
	ON [dim].[Component_LookUp]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Component_LookUp]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Component_LookUp].[ComponentId]		= INSERTED.[ComponentId];

END;