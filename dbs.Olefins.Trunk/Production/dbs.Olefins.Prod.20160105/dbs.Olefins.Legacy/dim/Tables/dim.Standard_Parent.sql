﻿CREATE TABLE [dim].[Standard_Parent] (
    [MethodologyId]  INT                 NOT NULL,
    [StandardId]     INT                 NOT NULL,
    [ParentId]       INT                 NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_Standard_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_Standard_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_Standard_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_Standard_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_Standard_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Standard_Parent] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StandardId] ASC),
    CONSTRAINT [FK_Standard_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[Standard_LookUp] ([StandardId]),
    CONSTRAINT [FK_Standard_Parent_LookUp_Standard] FOREIGN KEY ([StandardId]) REFERENCES [dim].[Standard_LookUp] ([StandardId]),
    CONSTRAINT [FK_Standard_Parent_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Standard_Parent_Operator] FOREIGN KEY ([Operator]) REFERENCES [dim].[Operator] ([Operator]),
    CONSTRAINT [FK_Standard_Parent_Parent] FOREIGN KEY ([MethodologyId], [StandardId]) REFERENCES [dim].[Standard_Parent] ([MethodologyId], [StandardId])
);


GO

CREATE TRIGGER [dim].[t_Standard_Parent_u]
	ON [dim].[Standard_Parent]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Standard_Parent]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Standard_Parent].[MethodologyId]		= INSERTED.[MethodologyId]
		AND	[dim].[Standard_Parent].[StandardId]		= INSERTED.[StandardId];

END;