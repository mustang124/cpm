﻿CREATE TABLE [dbo].[QualityWt] (
    [Refnum]                  [dbo].[Refnum] NOT NULL,
    [HydrogenH2]              REAL           NULL,
    [MethaneFG]               REAL           NULL,
    [EthyleneEthPG]           REAL           NULL,
    [EthylenePGProp]          REAL           NULL,
    [EthyleneCGProp]          REAL           NULL,
    [PropaneC3]               REAL           NULL,
    [Loss_FuelEthyl_PcntProd] REAL           NULL,
    [Loss_RecEthyl_PcntProd]  REAL           NULL,
    [EthyleneFG]              REAL           NULL,
    [EthyleneRec]             REAL           NULL,
    CONSTRAINT [PK___QualityWt] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

