﻿CREATE TABLE [dbo].[Maint01] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [ProjectID]      VARCHAR (15)   NOT NULL,
    [RoutMaintMatl]  REAL           NULL,
    [RoutMaintLabor] REAL           NULL,
    [RoutMaintTot]   REAL           NULL,
    [TAMatl]         REAL           NULL,
    [TALabor]        REAL           NULL,
    [TATot]          REAL           NULL,
    CONSTRAINT [PK_Maint01] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ProjectID] ASC)
);

