﻿CREATE TABLE [dbo].[PracMPC] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [Tbl9020]    TINYINT        NULL,
    [Tbl9021]    TINYINT        NULL,
    [Tbl9022]    TINYINT        NULL,
    [Tbl9023]    TINYINT        NULL,
    [Tbl9024]    TINYINT        NULL,
    [Tbl9025]    TINYINT        NULL,
    [Tbl9026]    TINYINT        NULL,
    [Tbl9027]    TINYINT        NULL,
    [Tbl9028]    TINYINT        NULL,
    [Tbl9029]    TINYINT        NULL,
    [Tbl9030]    REAL           NULL,
    [Tbl9031]    REAL           NULL,
    [Tbl9032]    REAL           NULL,
    [Tbl9033]    TINYINT        NULL,
    [Tbl9034]    TINYINT        NULL,
    [Tbl9035]    SMALLINT       NULL,
    [Tbl9036]    TINYINT        NULL,
    [Tbl9037]    SMALLINT       NULL,
    [Tot9020_28] AS             ([Tbl9020] + [Tbl9021] + [Tbl9022] + [Tbl9023] + [Tbl9024] + [Tbl9025] + [Tbl9026] + [Tbl9027] + [Tbl9028]),
    CONSTRAINT [PK___13__14] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

