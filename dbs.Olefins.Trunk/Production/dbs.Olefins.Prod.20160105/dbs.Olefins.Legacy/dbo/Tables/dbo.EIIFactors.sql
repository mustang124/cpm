﻿CREATE TABLE [dbo].[EIIFactors] (
    [FactorSet]     SMALLINT NOT NULL,
    [FracFeedKBSD]  REAL     NOT NULL,
    [HydroCryoCnt]  REAL     NOT NULL,
    [HydroPSACnt]   REAL     NOT NULL,
    [HydroMembCnt]  REAL     NOT NULL,
    [PGasHydroKBSD] REAL     NOT NULL,
    CONSTRAINT [PK_EIIFactors] PRIMARY KEY CLUSTERED ([FactorSet] ASC)
);

