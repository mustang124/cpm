﻿CREATE TABLE [dbo].[RankQueue] (
    [RefListNo]      INT NOT NULL,
    [BreakID]        INT CONSTRAINT [DF_RankQueue_BreakID_1__13] DEFAULT (0) NOT NULL,
    [RankVariableID] INT NOT NULL,
    CONSTRAINT [PK_RankQueue_3__13] PRIMARY KEY CLUSTERED ([RefListNo] ASC, [BreakID] ASC, [RankVariableID] ASC)
);

