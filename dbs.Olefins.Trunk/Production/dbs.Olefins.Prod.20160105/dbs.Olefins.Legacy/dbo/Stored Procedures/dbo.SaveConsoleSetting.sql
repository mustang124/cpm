﻿

CREATE PROCEDURE [dbo].[SaveConsoleSetting] (@UserName varchar(10), @Application varchar(255), @Section varchar(255), @KeyName varchar(255), @KeyValue varchar(255))
AS
BEGIN
	SET NOCOUNT ON
	IF EXISTS (SELECT * FROM dbo.ConsoleSettings WHERE UserName = @UserName AND Application = @Application AND Section = @Section AND KeyName = @KeyName)
		UPDATE dbo.ConsoleSettings
		SET KeyValue = @KeyValue
		WHERE UserName = @UserName AND Application = @Application AND Section = @Section AND KeyName = @KeyName
	ELSE
		INSERT dbo.ConsoleSettings(UserName, Application, Section, KeyName, KeyValue)
		VALUES (@UserName, @Application, @Section, @KeyName, @KeyValue)
END

