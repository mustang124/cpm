﻿CREATE PROCEDURE [dbo].[Insert_StreamDensity]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@Density_SG				FLOAT
)
AS
BEGIN

	IF(@Density_SG >= 0.0)
	EXECUTE [stage].[Insert_StreamDensity] @SubmissionId, @StreamNumber, @Density_SG;

END;
