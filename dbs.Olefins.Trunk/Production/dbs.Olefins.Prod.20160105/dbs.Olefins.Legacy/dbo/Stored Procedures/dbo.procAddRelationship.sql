﻿
/****** Object:  Stored Procedure dbo.procAddRelationship    Script Date: 4/18/2003 4:32:53 PM ******/

/****** Object:  Stored Procedure dbo.procAddRelationship    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.procAddRelationship    Script Date: 04/13/2000 8:32:49 AM ******/
CREATE PROC procAddRelationship @ObjA varchar(30), @ObjB varchar(30), @ColumnA varchar(30), @ColumnB varchar(30) AS
DECLARE @ObjIDA integer, @ObjIDB integer, @TempObj integer, @TempColumn varchar(30)
EXEC procGetObjectID @ObjA, @ObjIDA OUTPUT
EXEC procGetObjectID @ObjB, @ObjIDB OUTPUT
IF @ObjIDA > @ObjIDB
BEGIN
	SELECT @TempObj = @ObjIDB, @TempColumn = @ColumnB
	SELECT @ObjIDB = @ObjIDA, @ColumnB = @ColumnA
	SELECT @ObjIDA = @TempObj, @ColumnA = @TempColumn
END
INSERT INTO DD_Relationships
SELECT @ObjIDA, @ObjIDB, a.ColumnID, b.ColumnID
FROM DD_Columns a, DD_Columns B
WHERE a.ObjectID = @ObjIDA AND a.ColumnName = @ColumnA
AND b.ObjectID = @ObjIDB AND b.ColumnName = @ColumnB


