﻿





/****** Object:  Stored Procedure dbo.CalcALL    Script Date: 6/10/2008 12:53:13 PM ******/

CREATE   PROC [dbo].[CalcALL] (@Refnum Refnum) AS

-- Run all OLEFIN Calcs

EXEC CalcFurnRely @Refnum
EXEC CalcUnitOPEX @Refnum

EXEC dbo.CalcEDC @Refnum, 2007
EXEC dbo.CalcEII @Refnum, 2007

EXEC dbo.CalcEDC @Refnum, 2009
EXEC dbo.CalcEII @Refnum, 2009

EXEC [dbo].[RefreshNewFactors] @Refnum	-- RRH 20131015: commented for late participants; new factors are still being developed
EXEC CalcUpdateEDCFields @Refnum







