﻿
CREATE  PROC spOutputCheckTableID (@Study varchar(5), @StudyYear smallint, @OutputTab varchar(25), @Workbook varchar(400), @Worksheet varchar(50), @RowIDColumn smallint, @ColumnIDRow smallint, @TableID int OUTPUT)
AS
SET NOCOUNT ON
IF EXISTS (SELECT * FROM dbo.OutputCheckTableIDs WHERE Study = @Study AND StudyYear = @StudyYear AND OutputTab = @OutputTab)
BEGIN
	SELECT @TableID = TableID FROM dbo.OutputCheckTableIDs 
	WHERE Study = @Study AND StudyYear = @StudyYear AND OutputTab = @OutputTab

	UPDATE dbo.OutputCheckTableIDs
	SET Workbook = @Workbook, Worksheet = @Worksheet, RowIDColumn = @RowIDColumn, ColumnIDRow = @ColumnIDRow
	WHERE TableID = @TableID
END
ELSE BEGIN
	INSERT dbo.OutputCheckTableIDs (Study, StudyYear, OutputTab, Workbook, Worksheet, RowIDColumn, ColumnIDRow)
	VALUES (@Study, @StudyYear, @OutputTab, @Workbook, @Worksheet, @RowIDColumn, @ColumnIDRow)
	SELECT @TableID = SCOPE_IDENTITY( )
END


