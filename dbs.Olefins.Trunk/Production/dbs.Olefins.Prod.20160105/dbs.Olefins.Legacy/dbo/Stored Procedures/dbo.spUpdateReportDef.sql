﻿


CREATE  PROCEDURE spUpdateReportDef
	@ReportID DD_ID = -2 OUTPUT, @ReportName varchar(30),
	@Description varchar(100) = NULL, @SQL varchar(255) = NULL,
	@Scenario Scenario = NULL, @Currency varchar(5) = NULL
AS
IF EXISTS (SELECT * FROM ReportDef 
	   WHERE ReportName = @ReportName AND ReportID <> @ReportID)
	RETURN -101
IF @ReportID = -2
BEGIN
	INSERT INTO ReportDef (ReportName, Description, ReportDefSQL, ReportDefScenario, ReportDefCurrency)
	VALUES (@ReportName, @Description, @SQL, @Scenario, @Currency)
	SELECT @ReportID = ReportID FROM ReportDef
	WHERE ReportName = @ReportName
END
ELSE
BEGIN
	UPDATE ReportDef
	SET ReportName = @ReportName, Description = @Description,
		ReportDefSQL = @SQL, ReportDefScenario = @Scenario,
		ReportDefCurrency = @Currency
	WHERE ReportID = @ReportID
END


