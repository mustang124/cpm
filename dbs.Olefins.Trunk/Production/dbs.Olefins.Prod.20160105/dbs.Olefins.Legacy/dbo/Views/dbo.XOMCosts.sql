﻿
/* 

This view is specifially used for XOM supplemental work.  
See JSJ for details!

Update DBB/JSJ/CH 29Sep2008
*/
CREATE VIEW [dbo].[XOMCosts]
AS
SELECT     t.Refnum, b.ByProdBasis, t.CoName, t.PlantLoc, t.Region, t.PlantCountry, t.CapPeerGrp, t.TechPeerGrp, t.FeedClass, t.StudyYear, b.DivFactor, 
                      f.RMC / b.DivFactor AS GrossFeedstockCost,
                      - (b.TotVal * 1000 / b.DivFactor) AS CoProduct,
                      (f.RMC - b.TotVal * 1000) / b.DivFactor AS NetFeedCost, 
                      o.PurElec / b.DivFactor AS PurElec, 
                      o.PurSteam / b.DivFactor AS PurSteam, 
                      o.PurFG / b.DivFactor AS PurFG, 
                      o.PurLiquid / b.DivFactor AS PurLiquid, 
                      f.PPCFGCost / b.DivFactor AS PPCFuelGas, 
                      f.PPCOthFuelCost / b.DivFactor AS OthPPC, 
                      o.SteamExports / b.DivFactor AS SteamExports, 
                      o.PowerExports / b.DivFactor AS PowerExports, 
                      (ISNULL(o.PurElec, 0) + ISNULL(o.PurSteam, 0) + ISNULL(o.PurFG, 0) + ISNULL(o.PurLiquid, 0) + ISNULL(f.PPCFGCost, 0) + ISNULL(f.PPCOthFuelCost, 0) + ISNULL(o.SteamExports, 0) + ISNULL(o.PowerExports, 0)) / b.DivFactor AS NetEnergy, 
                      o.Chemicals / b.DivFactor AS Chemicals, 
                      o.Catalysts / b.DivFactor AS Catalysts, 
                      o.PurOth / b.DivFactor AS OthUtil, 
                      (ISNULL(o.OthVol, 0) + ISNULL(o.Royalties, 0)) / b.DivFactor AS OthVol, 
                      (ISNULL(o.Chemicals, 0) + ISNULL(o.Catalysts, 0) + ISNULL(o.PurOth, 0) + ISNULL(o.OthVol, 0) + ISNULL(o.Royalties, 0)) / b.DivFactor AS STOthVar, 
                      (f.RMC - b.TotVal * 1000 + ISNULL(o.PurElec, 0) + ISNULL(o.PurSteam, 0) + ISNULL(o.PurFG, 0) + ISNULL(o.PurLiquid, 0) + ISNULL(f.PPCFGCost, 0) + ISNULL(f.PPCOthFuelCost, 0) + ISNULL(o.SteamExports, 0) + ISNULL(o.PowerExports, 0) + ISNULL(o.Chemicals, 0) + ISNULL(o.Catalysts, 0) + ISNULL(o.PurOth, 0) + ISNULL(o.OthVol, 0) + ISNULL(o.Royalties, 0)) / b.DivFactor AS STVol, 
                      f.FixExpAmt / b.DivFactor AS STOthFix, 
                      f.AnnTAExp / b.DivFactor AS AnnTACost, 
                      o.InvenCarry / b.DivFactor AS HCInventory, 
                      o.GANonPers / b.DivFactor AS GAPers, 
                      (ISNULL(f.FixExpAmt, 0) + ISNULL(f.AnnTAExp, 0) + ISNULL(o.InvenCarry, 0) + ISNULL(o.GANonPers, 0))/ b.DivFactor AS STFix, 
                      (f.RMC - b.TotVal * 1000 + ISNULL(o.PurElec, 0) + ISNULL(o.PurSteam, 0) + ISNULL(o.PurFG, 0) + ISNULL(o.PurLiquid, 0) 
						+ ISNULL(f.PPCFGCost, 0) + ISNULL(f.PPCOthFuelCost, 0) + ISNULL(o.SteamExports, 0) + ISNULL(o.PowerExports, 0) + ISNULL(o.Chemicals, 0) 
						+ ISNULL(o.Catalysts, 0) + ISNULL(o.PurOth, 0) + ISNULL(o.OthVol, 0) + ISNULL(o.Royalties, 0) + ISNULL(f.FixExpAmt, 0) + ISNULL(f.AnnTAExp, 0) 
						+ ISNULL(o.InvenCarry, 0) + ISNULL(o.GANonPers, 0)) / b.DivFactor AS TotEthCashCost, 
					  o.Depreciation / b.DivFactor AS Depr, 
                      o.Interest / b.DivFactor AS Interest, 
                      (f.RMC - b.TotVal * 1000 + ISNULL(o.PurElec, 0) + ISNULL(o.PurSteam, 0) + ISNULL(o.PurFG, 0) 
						+ ISNULL(o.PurLiquid, 0) + ISNULL(f.PPCFGCost, 0) + ISNULL(f.PPCOthFuelCost, 0) + ISNULL(o.SteamExports, 0) + ISNULL(o.PowerExports, 0) 
						+ ISNULL(o.Chemicals, 0) + ISNULL(o.Catalysts, 0) + ISNULL(o.PurOth, 0) + ISNULL(o.OthVol, 0) + ISNULL(o.Royalties, 0) + ISNULL(f.FixExpAmt, 0) 
						+ ISNULL(f.AnnTAExp, 0) + ISNULL(o.InvenCarry, 0) + ISNULL(o.GANonPers, 0) + ISNULL(o.Depreciation, 0) + ISNULL(o.Interest, 0)) / b.DivFactor AS TotEthProdCost, 
					  m.TotMaintCost, 
					  ISNULL(f.FixExpAmt, 0) / b.DivFactor - m.RoutMaintCost AS STOthFixLessRoutMaint, 
                      ISNULL(x.FurnaceRoutMaintTot, 0) / b.DivFactor AS FurnaceMaint, 
                      m.RoutMaintCost - ISNULL(x.FurnaceRoutMaintTot, 0) / b.DivFactor AS RoutMaintLessFurnaceCost, 
                      ISNULL(f.FixExpAmt, 0) / b.DivFactor AS STNonVol,
                      ISNULL(ta.Adj_Exp_Retube,0) / b.DivFactor AS AnnRetube
FROM         dbo.FinancialTot AS f INNER JOIN
                      dbo.Production AS p ON p.Refnum = f.Refnum INNER JOIN
                      dbo.TSort AS t ON t.Refnum = p.Refnum INNER JOIN
                      dbo.Opex AS o ON o.Refnum = t.Refnum INNER JOIN
                      dbo.XOMMaintCosts AS m ON m.Refnum = t.Refnum INNER JOIN
                      dbo.ByProd AS b ON b.Refnum = t.Refnum AND m.ByProdBasis = b.ByProdBasis INNER JOIN
                      dbo.Replacement AS r ON f.Refnum = r.Refnum LEFT OUTER JOIN
                      dbo.XOMFurnaceCosts AS x ON f.Refnum = x.Refnum
                      INNER JOIN dbo.TAAdj ta ON ta.Refnum = f.Refnum 


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "f"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 197
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 235
               Bottom = 114
               Right = 418
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "o"
            Begin Extent = 
               Top = 114
               Left = 249
               Bottom = 222
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "m"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 222
               Left = 236
               Bottom = 330
               Right = 387
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "r"
            Begin Extent = 
               Top = 330
               Left = 38
               Bottom = 438
               Right = 201
            End
            DisplayFlags = 280
            TopColumn = 0
         End
 ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'XOMCosts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'        Begin Table = "x"
            Begin Extent = 
               Top = 330
               Left = 239
               Bottom = 408
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'XOMCosts';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'XOMCosts';

