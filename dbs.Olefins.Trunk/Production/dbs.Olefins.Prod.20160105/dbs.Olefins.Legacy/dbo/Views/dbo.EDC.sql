﻿








CREATE View [dbo].[EDC] as
SELECT Refnum,  kEDC, EDC,
 TAAdjkUEDC, kUEDC,
  FracFeed, FeedStock, Furnace,
   Compression, Supplemental, Utilities,
    HydrogenPurification, PGasHydrotreater,
     LightFeed, NaphthatFeed, HeavyFeed,
      DiSuppl, RefSuppl, ConcSuppl,
       FracFeedKBSD, HPBoilRate,
        LPBoilRate, ElecGenMW,
         HydroCryoCnt, HydroPSACnt,
          HydroMembCnt, PGasHydroKBSD,
           GasCompBHP, MethCompBHP,
            EthyCompBHP, PropCompBHP,
             FurnaceCapKMTA, SpareFurnaceCapKMTA, Ethane,
              Propane, Butane, LPG, OthLtFeed,
               HeavyGasoil, HTGasoil, OthLiqFeed1,
                OthLiqFeed2, OthLiqFeed3, HeavyNaphtha,
                 LtNaphtha, FRNaphtha, Condensate,
                  HeavyNGL, Diesel, ConcEthylene,
                   DiEthylene, RefEthylene, 
                   ConcPropylene, DiPropylene,
                   RefPropylene,
nfFresh,
nfSupp,
nfAuxiliary,
nfReduction,
nfUtilities,
tsCalculated
FROM EDCVersionCalcs 
WHERE FactorSet = '2013' and ModelVersion = '2013'
