﻿
/****** Object:  View dbo.QuantityTotCalc    Script Date: 4/18/2003 4:32:50 PM ******/

/****** Object:  View dbo.QuantityTotCalc    Script Date: 12/28/2001 7:34:24 AM ******/
CREATE VIEW QuantityTotCalc AS 



SELECT Refnum, Type, SUM(AnnFeedProd) as AnnFeedProd
FROM Quantity q, FeedProd_LU lu
WHERE q.FeedProdID = lu.FeedProdID
GROUP BY Refnum, Type


