﻿






/* 

This view is specifially used for XOM supplemental work.  
See JSJ for details!

Update DBB/JSJ/CH 29Sep2008
Update for new calcs - DBB 15Aug14

*/
CREATE VIEW [Cust].[XOMCosts]
AS
Select Refnum = LEFT(x.StudyYear,2) + RTRIM(x.Refnum)
, ByProdBasis
, CoName
, PlantLoc
, x.Region
, PlantCountry
, CapPeerGrp
, TechPeerGrp
, FeedClass
, x.StudyYear
, DivFactor
, EDC = 1.0
, GrossFeedstockCost
, CoProduct
, NetFeedCost
, PurElec
, PurSteam
, PurFG
, PurLiquid
, PPCFuelGas
, OthPPC
, SteamExports
, PowerExports
, NetEnergy
, Chemicals
, Catalysts
, OthUtil
, OthVol
, STOthVar
, STVol
, STOthFix
, AnnTACost
, HCInventory
, GAPers
, STFix
, TotEthCashCost
, Depr
, Interest
, TotEthProdCost
, TotMaintCost
, STOthFixLessRoutMaint
, FurnaceMaint
, RoutMaintLessFurnaceCost
, STNonVol
, AnnRetube
from [$(DbOlefinsLegacy)].dbo.XOMCosts x where x.StudyYear < 2007

UNION

SELECT g.Refnum
, DataType = CASE o.DataType WHEN 'ETHDiv_MT' THEN 'C2' WHEN 'OLEDiv_MT' THEN 'OLE' WHEN 'HVC_MT' THEN 'HVC' ELSE o.DataType END
, g.Company, g.Location, g.Region, g.Country, g.CapGroup, TechClass, FeedClass, g.StudyYear
, DivFactor = o.DivisorValue
, EDCGroup = g.kEdc
, GrossFeedstockCost = ProdLoss --m.RawMaterialCost + m.GrossProductValue
, CoProduct =  - m.GrossProductValue
, NetFeedCost = m.RawMaterialCost
, PurElec = o.PurElec
, PurSteam = o.PurSteam
, PurFG = o.PurFuelGas
, PurLiquid = o.PurLiquid + o.PurSolid
, PPCFuelGas = o.PPFCFuelGas
, OthPPC = o.PPFCOther
, SteamExports = o.ExportSteam
, PowerExports = o.ExportElectric
, NetEnergy = o.EnergyOpex
, Chemicals = o.Chemicals
, Catalysts = o.Catalysts
, OthUtil = o.PurOth
, OthVol = ISNULL(o.OtherVol,0) + ISNULL(o.Royalties,0)
, STOthVar = ISNULL(o.Chemicals, 0) + ISNULL(o.Catalysts, 0) + ISNULL(o.PurOth, 0) + ISNULL(o.OtherVol, 0) + ISNULL(o.Royalties, 0)
, STVol = m.RawMaterialCost +  ISNULL(o.PurElec, 0) + ISNULL(o.PurSteam, 0) + ISNULL(o.PurFuelGas, 0) + ISNULL(o.PurLiquid, 0) + ISNULL(o.PPFCFuelGas, 0) + ISNULL(o.PPFCOther, 0) + ISNULL(o.ExportSteam, 0) + ISNULL(o.ExportElectric, 0) + ISNULL(o.Chemicals, 0) + ISNULL(o.Catalysts, 0) + ISNULL(o.PurOth, 0) + ISNULL(o.OtherVol, 0) + ISNULL(o.Royalties, 0)
, STOthFix = o.STNonVol  -- I don't know what this is...
, AnnTACost = o.TAAccrual
, HCInventory = o.InvenCarry
, GANonPers = o.GANonPers
, STFix = ISNULL(o.STNonVol, 0) + ISNULL(o.TAAccrual, 0) + ISNULL(o.InvenCarry, 0) + ISNULL(o.GANonPers, 0)
, TotEthCashCost = m.RawMaterialCost + o.TotCashOpEx
, Depr = o.Depreciation
, Interest = o.Interest
, TotEthProdCost =  m.RawMaterialCost + o.TotCashOpEx + ISNULL(o.Depreciation, 0) + ISNULL(o.Interest, 0)
, TotMaintCost = mc.TotMaintCost
, STOthFixLessRoutMaint = o.STNonVol - mc.RoutMaintCost
, FurnaceMaint = x.FurnaceRoutMaintTot / o.DivisorValue
, RoutMaintLessFurnaceCost = mc.RoutMaintCost -  x.FurnaceRoutMaintTot / o.DivisorValue
, STNonVol = o.STNonVol
, AnnRetube = o.MaintRetube
FROM dbo.OpEx  o join GENSUM G ON G.Refnum=o.Refnum and o.DataType in ('ETHDiv_MT','OLEDiv_MT','HVC_MT','EDC','UEDC','OLECpby_MT')
JOIN dbo.Financial f on f.Refnum=o.Refnum 
JOIN Cust.XOMMaintCosts AS mc ON mc.Refnum = o.Refnum and mc.ByProdBasis = CASE o.DataType WHEN 'ETHDiv_MT' THEN 'C2' WHEN 'OLEDiv_MT' THEN 'OLE' WHEN 'HVC_MT' THEN 'HVC' ELSE o.DataType END
JOIN Cust.XOMFurnaceCosts AS x ON f.Refnum = x.Refnum
JOIN (select * 
	from dbo.MarginCalc 
	WHERE ((DataType = 'ETHDiv_MT' and MarginAnalysis = 'Ethylene') 
	or (DataType = 'OLEDiv_MT' and MarginAnalysis = 'ProdOlefins')
	or (DataType = 'OLECpby_MT' and MarginAnalysis = 'ProdOlefins')
	or (DataType = 'HVC_MT' and MarginAnalysis = 'ProdHVC')
	or (DataType = 'EDC' and MarginAnalysis = 'None')
	or (DataType = 'UEDC' and MarginAnalysis = 'None')
	)) m on m.Refnum=o.Refnum and m.DataType = o.DataType
WHERE g.StudyYear >= 2007

--select * from Cust.XOMCosts





