﻿


/*

		This view is created for the LT rends for XOM custom follow-up
		
		DBB 14Aug14

*/
CREATE view [Cust].[XOMFurnaceCosts] as
Select Refnum = LEFT(T.StudyYear,2) + RTRIM(t.Refnum), FurnaceRoutMaintTot = sum(RoutMaintTot) 
from [$(DbOlefinsLegacy)].dbo.Maint m JOIN [$(DbOlefinsLegacy)].dbo.TSort t on t.Refnum=m.Refnum
where ProjectID in ('Tubes','MinFurn','OthMajFurn') and t.StudyYear < 2007
group by t.Refnum, T.StudyYear

UNION

Select Refnum = LEFT(T.StudyYear,2) + RTRIM(t.Refnum), FurnaceRoutMaintTot = sum(RoutMaintTot) 
from [$(DbOlefinsLegacy)].dbo.Maint01 m JOIN [$(DbOlefinsLegacy)].dbo.TSort t on t.Refnum=m.Refnum
where ProjectID in ('Tubes','MinFurn','OthMajFurn')  and t.StudyYear < 2007
group by t.Refnum, T.StudyYear

Union

Select m.Refnum, 
FurnaceRoutMaintTot = ISNULL(AnnFurnReTube,0) + ISNULL(PyroFurnMinOth,0) + ISNULL(PyroFurnMajOth,0)
from [$(DatabaseName)].dbo.MaintCost m JOIN [$(DatabaseName)].dbo.TSort t on t.Refnum=m.Refnum
Where m.DataType = 'RoutTot'
 and t.StudyYear >= 2007
 
 --select * from Cust.XOMFurnaceCosts where Refnum = '2011PCH115'














