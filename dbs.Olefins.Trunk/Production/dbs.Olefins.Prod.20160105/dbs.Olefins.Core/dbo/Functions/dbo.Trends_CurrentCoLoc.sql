﻿CREATE FUNCTION [dbo].[Trends_CurrentCoLoc]
(
	@ListName	VARCHAR(84),
	@UserGroup	VARCHAR(42)
)
RETURNS TABLE
AS RETURN
SELECT DISTINCT
	[t].[Refnum],
	[m].[CoLoc],
	[m].[AssetId],
	[t].[StudyYear],
	[r].[ListName],
	[r].[UserGroup]
FROM
	[dbo].[TSort]		[t]
INNER JOIN
	[dbo].[_RL]			[r]
		ON	[r].[Refnum]	= [t].[Refnum]
INNER JOIN
	(
	SELECT DISTINCT
		[t].[AssetId],
			[StudyYear]	= MAX([t].[StudyYear])
	FROM
		[dbo].[TSort]		[t]
	INNER JOIN
		[dbo].[_RL]			[r]
			ON	[r].[Refnum]	= [t].[Refnum]
	WHERE
		[r].[ListName]		= @ListName
		AND	[r].[UserGroup]	LIKE @UserGroup + '__'
	GROUP BY
		[t].[AssetId]
	)	[x]([AssetId], [StudyYear])
	ON	[x].[AssetId]	= [t].[AssetId]
INNER JOIN
	[dbo].[TSort]		[m]
		ON	[m].[AssetId]	= [x].[AssetId]
		AND	[m].[StudyYear]	= [x].[StudyYear]
WHERE
	[r].[ListName]		= @ListName
	AND	[r].[UserGroup]	LIKE @UserGroup + '__';