﻿CREATE TABLE [dbo].[CoContactInfo] (
    [ContactCode]    CHAR (25)     NOT NULL,
    [StudyYear]      SMALLINT      NOT NULL,
    [ContactType]    CHAR (5)      NOT NULL,
    [FirstName]      VARCHAR (25)  NULL,
    [LastName]       VARCHAR (25)  NULL,
    [JobTitle]       VARCHAR (75)  NULL,
    [PersonalTitle]  VARCHAR (5)   NULL,
    [Phone]          VARCHAR (40)  NULL,
    [PhoneSecondary] VARCHAR (40)  NULL,
    [Fax]            VARCHAR (40)  NULL,
    [Email]          VARCHAR (255) NULL,
    [StrAddr1]       VARCHAR (75)  NULL,
    [StrAddr2]       VARCHAR (50)  NULL,
    [StrAdd3]        VARCHAR (50)  NULL,
    [StrCity]        VARCHAR (30)  NULL,
    [StrState]       VARCHAR (30)  NULL,
    [StrZip]         VARCHAR (30)  NULL,
    [StrCountry]     VARCHAR (30)  NULL,
    [MailAddr1]      VARCHAR (75)  NULL,
    [MailAddr2]      VARCHAR (50)  NULL,
    [MailAddr3]      VARCHAR (50)  NULL,
    [MailCity]       VARCHAR (30)  NULL,
    [MailState]      VARCHAR (30)  NULL,
    [MailZip]        VARCHAR (30)  NULL,
    [MailCountry]    VARCHAR (30)  NULL,
    [AltFirstName]   VARCHAR (20)  NULL,
    [AltLastName]    VARCHAR (25)  NULL,
    [AltEmail]       VARCHAR (255) NULL,
    [CCAlt]          CHAR (1)      NULL,
    [SendMethod]     CHAR (3)      NULL,
    [Comment]        VARCHAR (400) NULL,
    [Password]       VARCHAR (25)  NULL
);
GO

ALTER TABLE [dbo].[CoContactInfo]
ADD CONSTRAINT [PK_CoContactInfo] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [ContactCode] ASC, [ContactType] aSC)
GO