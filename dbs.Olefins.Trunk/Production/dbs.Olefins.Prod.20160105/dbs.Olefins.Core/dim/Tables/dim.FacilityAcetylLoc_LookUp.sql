﻿CREATE TABLE [dim].[FacilityAcetylLoc_LookUp] (
    [AcetylLocId]     CHAR (1)           NOT NULL,
    [AcetylLocName]   NVARCHAR (84)      NOT NULL,
    [AcetylLocDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_FacilityAcetylLoc_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_FacilityAcetylLoc_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_FacilityAcetylLoc_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_FacilityAcetylLoc_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilityAcetylLoc_LookUp] PRIMARY KEY CLUSTERED ([AcetylLocId] ASC),
    CONSTRAINT [CL_FacilityAcetylLoc_LookUp_AcetylLocDetail] CHECK ([AcetylLocDetail]<>''),
    CONSTRAINT [CL_FacilityAcetylLoc_LookUp_AcetylLocId] CHECK ([AcetylLocId]<>''),
    CONSTRAINT [CL_FacilityAcetylLoc_LookUp_AcetylLocName] CHECK ([AcetylLocName]<>''),
    CONSTRAINT [UK_FacilityAcetylLoc_LookUp_AcetylLocDetail] UNIQUE NONCLUSTERED ([AcetylLocDetail] ASC),
    CONSTRAINT [UK_FacilityAcetylLoc_LookUp_AcetylLocName] UNIQUE NONCLUSTERED ([AcetylLocName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FacilityAcetylLoc_LookUp_u]
	ON [dim].[FacilityAcetylLoc_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FacilityAcetylLoc_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FacilityAcetylLoc_LookUp].[AcetylLocId]		= INSERTED.[AcetylLocId];
		
END;
