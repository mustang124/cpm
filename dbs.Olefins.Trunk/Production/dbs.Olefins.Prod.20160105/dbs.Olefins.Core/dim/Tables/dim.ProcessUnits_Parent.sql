﻿CREATE TABLE [dim].[ProcessUnits_Parent] (
    [FactorSetId]    VARCHAR (12)        NOT NULL,
    [UnitId]         VARCHAR (42)        NOT NULL,
    [ParentId]       VARCHAR (42)        NOT NULL,
    [Operator]       CHAR (1)            CONSTRAINT [DF_ProcessUnits_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]        INT                 NOT NULL,
    [Hierarchy]      [sys].[hierarchyid] NOT NULL,
    [tsModified]     DATETIMEOFFSET (7)  CONSTRAINT [DF_ProcessUnits_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)      CONSTRAINT [DF_ProcessUnits_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)      CONSTRAINT [DF_ProcessUnits_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)      CONSTRAINT [DF_ProcessUnits_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION          NOT NULL,
    CONSTRAINT [PK_ProcessUnits_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [UnitId] ASC),
    CONSTRAINT [CR_ProcessUnits_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_ProcessUnits_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ProcessUnits_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId]),
    CONSTRAINT [FK_ProcessUnits_Parent_LookUp_ProcessUnits] FOREIGN KEY ([UnitId]) REFERENCES [dim].[ProcessUnits_LookUp] ([UnitId]),
    CONSTRAINT [FK_ProcessUnits_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[ProcessUnits_Parent] ([FactorSetId], [UnitId])
);


GO

CREATE TRIGGER [dim].[t_ProcessUnits_Parent_u]
ON [dim].[ProcessUnits_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[ProcessUnits_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ProcessUnits_Parent].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[ProcessUnits_Parent].[UnitId]		= INSERTED.[UnitId];

END;
