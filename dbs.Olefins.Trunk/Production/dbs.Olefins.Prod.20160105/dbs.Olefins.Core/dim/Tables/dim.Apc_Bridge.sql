﻿CREATE TABLE [dim].[Apc_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [ApcId]              VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Apc_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Apc_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Apc_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Apc_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Apc_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Apc_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ApcId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Apc_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Apc_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_Apc_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Apc_Bridge_LookUp_Apc] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_Apc_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [ApcId]) REFERENCES [dim].[Apc_Parent] ([FactorSetId], [ApcId]),
    CONSTRAINT [FK_Apc_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Apc_Parent] ([FactorSetId], [ApcId])
);


GO

CREATE TRIGGER [dim].[t_Apc_Bridge_u]
ON [dim].[Apc_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Apc_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Apc_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Apc_Bridge].[ApcId]			= INSERTED.[ApcId]
		AND	[dim].[Apc_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;