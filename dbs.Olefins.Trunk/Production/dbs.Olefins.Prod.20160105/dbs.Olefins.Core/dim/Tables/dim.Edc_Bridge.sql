﻿CREATE TABLE [dim].[Edc_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [EdcId]              VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Edc_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Edc_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Edc_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Edc_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Edc_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Edc_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EdcId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Edc_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Edc_Bridge__FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Edc_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_Edc_Bridge_LookUp_Edc] FOREIGN KEY ([EdcId]) REFERENCES [dim].[Edc_LookUp] ([EdcId]),
    CONSTRAINT [FK_Edc_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [EdcId]) REFERENCES [dim].[Edc_Parent] ([FactorSetId], [EdcId]),
    CONSTRAINT [FK_Edc_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Edc_Parent] ([FactorSetId], [EdcId])
);


GO

CREATE TRIGGER [dim].[t_Edc_Bridge_u]
ON [dim].[Edc_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Edc_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Edc_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Edc_Bridge].[EdcId]			= INSERTED.[EdcId]
		AND	[dim].[Edc_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;