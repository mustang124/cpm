﻿CREATE TABLE [dim].[Accuracy_LookUp] (
    [AccuracyId]     CHAR (1)           NOT NULL,
    [AccuracyName]   NVARCHAR (84)      NOT NULL,
    [AccuracyDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Accuracy_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Accuracy_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Accuracy_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Accuracy_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Accuracy_LookUp] PRIMARY KEY CLUSTERED ([AccuracyId] ASC),
    CONSTRAINT [CL_Accuracy_LookUp_AccuracyDetail] CHECK ([AccuracyDetail]<>''),
    CONSTRAINT [CL_Accuracy_LookUp_AccuracyId] CHECK ([AccuracyId]<>''),
    CONSTRAINT [CL_Accuracy_LookUp_AccuracyName] CHECK ([AccuracyName]<>''),
    CONSTRAINT [UK_Accuracy_LookUp_AccuracyDetail] UNIQUE NONCLUSTERED ([AccuracyDetail] ASC),
    CONSTRAINT [UK_Accuracy_LookUp_AccuracyName] UNIQUE NONCLUSTERED ([AccuracyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Accuracy_LookUp_u]
	ON [dim].[Accuracy_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Accuracy_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Accuracy_LookUp].[AccuracyId]		= INSERTED.[AccuracyId];
		
END;
