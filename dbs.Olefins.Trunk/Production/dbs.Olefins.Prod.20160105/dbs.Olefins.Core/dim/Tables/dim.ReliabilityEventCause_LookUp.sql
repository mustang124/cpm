﻿CREATE TABLE [dim].[ReliabilityEventCause_LookUp] (
    [EventCauseId]     VARCHAR (42)       NOT NULL,
    [EventCauseName]   NVARCHAR (84)      NOT NULL,
    [EventCauseDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityEventCause_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityEventCause_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityEventCause_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_ReliabilityEventCause_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReliabilityEventCause_LookUp] PRIMARY KEY CLUSTERED ([EventCauseId] ASC),
    CONSTRAINT [CL_ReliabilityEventCause_LookUp_EventCauseDetail] CHECK ([EventCauseDetail]<>''),
    CONSTRAINT [CL_ReliabilityEventCause_LookUp_EventCauseId] CHECK ([EventCauseId]<>''),
    CONSTRAINT [CL_ReliabilityEventCause_LookUp_EventCauseName] CHECK ([EventCauseName]<>''),
    CONSTRAINT [UK_ReliabilityEventCause_LookUp_EventCauseDetail] UNIQUE NONCLUSTERED ([EventCauseDetail] ASC),
    CONSTRAINT [UK_ReliabilityEventCause_LookUp_EventCauseName] UNIQUE NONCLUSTERED ([EventCauseName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReliabilityEventCause_LookUp_u]
	ON [dim].[ReliabilityEventCause_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReliabilityEventCause_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityEventCause_LookUp].[EventCauseId]		= INSERTED.[EventCauseId];
		
END;
