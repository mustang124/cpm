﻿CREATE PROCEDURE [Console].[GetMessageLog]
    @MessageType int,
	@Refnum varchar(18)

AS
BEGIN

	SELECT * FROM dbo.MessageLog
	WHERE (Refnum = @Refnum OR Refnum = dbo.FormatRefNum(@Refnum, 0))
	AND MessageId = @MessageType 
	Order by MessageTime Desc
	
END
