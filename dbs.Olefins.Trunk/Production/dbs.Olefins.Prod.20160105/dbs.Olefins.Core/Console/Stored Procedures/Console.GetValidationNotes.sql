﻿CREATE PROCEDURE [Console].[GetValidationNotes]
	@Refnum varchar(18),
	@NoteType varchar(20)
AS
BEGIN

	SELECT * From Val.Notes Where Refnum = @Refnum and NoteType = @NoteType order by Updated desc

END
