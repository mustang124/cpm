﻿CREATE TABLE [super].[Composition] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [H2]             REAL               NULL,
    [CH4]            REAL               NULL,
    [C2H2]           REAL               NULL,
    [C2H6]           REAL               NULL,
    [C2H4]           REAL               NULL,
    [C3H6]           REAL               NULL,
    [C3H8]           REAL               NULL,
    [BUTAD]          REAL               NULL,
    [C4S]            REAL               NULL,
    [C4H10]          REAL               NULL,
    [BZ]             REAL               NULL,
    [PYGAS]          REAL               NULL,
    [PYOIL]          REAL               NULL,
    [INERTS]         REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_super_Composition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_super_Composition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_super_Composition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_super_Composition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_super_Composition] PRIMARY KEY CLUSTERED ([Refnum] ASC, [StreamId] ASC),
    CONSTRAINT [CK_super_Composition_BUTADS] CHECK ([BUTAD]>=(0.0)),
    CONSTRAINT [CK_super_Composition_BZ] CHECK ([BZ]>=(0.0)),
    CONSTRAINT [CK_super_Composition_C2H2] CHECK ([C2H2]>=(0.0)),
    CONSTRAINT [CK_super_Composition_C2H4] CHECK ([C2H4]>=(0.0)),
    CONSTRAINT [CK_super_Composition_C2H6] CHECK ([C2H6]>=(0.0)),
    CONSTRAINT [CK_super_Composition_C3H6] CHECK ([C3H6]>=(0.0)),
    CONSTRAINT [CK_super_Composition_C3H8] CHECK ([C3H8]>=(0.0)),
    CONSTRAINT [CK_super_Composition_C4H10] CHECK ([C4H10]>=(0.0)),
    CONSTRAINT [CK_super_Composition_C4S] CHECK ([C4S]>=(0.0)),
    CONSTRAINT [CK_super_Composition_CH4] CHECK ([CH4]>=(0.0)),
    CONSTRAINT [CK_super_Composition_H2] CHECK ([H2]>=(0.0)),
    CONSTRAINT [CK_super_Composition_INERTS] CHECK ([INERTS]>=(0.0)),
    CONSTRAINT [CK_super_Composition_PYGAS] CHECK ([PYGAS]>=(0.0)),
    CONSTRAINT [CK_super_Composition_PYOIL] CHECK ([PYOIL]>=(0.0)),
    CONSTRAINT [CL_super_Composition_FeedProdId] CHECK ([StreamId]<>''),
    CONSTRAINT [CL_super_Composition_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [FK_super_composition_StreamId] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [super].[t_Composition_u]
	ON [super].[Composition]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [super].[Composition]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[super].[Composition].Refnum			= INSERTED.Refnum
		AND	[super].[Composition].[StreamId]		= INSERTED.[StreamId];

END;