﻿CREATE TYPE [stat].[DescriptiveGiniResults] AS TABLE (
    [FactorSetId] VARCHAR (12) NOT NULL,
    [Refnum]      VARCHAR (25) NOT NULL,
    [x]           FLOAT (53)   NOT NULL,
    [w]           FLOAT (53)   NULL,
    [xLorenz]     FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [yLorenz]     FLOAT (53)   DEFAULT ((0)) NOT NULL,
    [_dLorenz]    AS           (CONVERT([float],[xLorenz]-[yLorenz],0)) PERSISTED NOT NULL,
    [Gini]        FLOAT (53)   NULL,
    PRIMARY KEY CLUSTERED ([x] ASC, [FactorSetId] ASC, [Refnum] ASC),
    CHECK ([FactorSetId]<>''),
    CHECK ([Refnum]<>''),
    CHECK (round([xLorenz],(5))>=(0.0) AND round([xLorenz],(5))<=(100.0)),
    CHECK (round([yLorenz],(5))>=(0.0) AND round([yLorenz],(5))<=(100.0)),
    CHECK ([Gini]>=(0.0) AND [Gini]<=(100.0)));

