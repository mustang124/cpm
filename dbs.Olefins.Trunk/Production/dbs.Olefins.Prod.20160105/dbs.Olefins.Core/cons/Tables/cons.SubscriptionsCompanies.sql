﻿CREATE TABLE [cons].[SubscriptionsCompanies] (
    [CompanyId]               VARCHAR (42)       NOT NULL,
    [CalDateKey]              INT                NOT NULL,
    [_StudyYear]            AS                 (CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0))) PERSISTED NOT NULL,
    [StudyId]                 VARCHAR (4)        NOT NULL,
    [SubscriberCompanyName]   NVARCHAR (84)      NULL,
    [SubscriberCompanyDetail] NVARCHAR (256)     NULL,
    [PassWord_PlainText]      NVARCHAR (42)      NULL,
    [CompanyNameReport]       VARCHAR (84)       NULL,
    [dtSent]                  DATETIMEOFFSET (2) NULL,
    [dtReceived]              DATETIMEOFFSET (2) NULL,
    [Active]                  BIT                CONSTRAINT [DF_SubscriptionsCompanies_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]              DATETIMEOFFSET (7) CONSTRAINT [DF_SubscriptionsCompanies_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]          NVARCHAR (168)     CONSTRAINT [DF_SubscriptionsCompanies_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]          NVARCHAR (168)     CONSTRAINT [DF_SubscriptionsCompanies_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]           NVARCHAR (168)     CONSTRAINT [DF_SubscriptionsCompanies_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SubscriptionsCompanies] PRIMARY KEY CLUSTERED ([StudyId] ASC, [CalDateKey] ASC, [CompanyId] ASC),
    CONSTRAINT [CL_SubscriptionsCompanies_PassWord_PlainText] CHECK ([PassWord_PlainText]<>''),
    CONSTRAINT [CL_SubscriptionsCompanies_SubscriberCoDetail] CHECK ([SubscriberCompanyDetail]<>''),
    CONSTRAINT [CL_SubscriptionsCompanies_SubscriberCoName] CHECK ([SubscriberCompanyName]<>''),
    CONSTRAINT [FK_SubscriptionsCompanies_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_SubscriptionsCompanies_Company_LookUp] FOREIGN KEY ([CompanyId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_SubscriptionsCompanies_StudiesStudyPeriods] FOREIGN KEY ([CalDateKey], [StudyId]) REFERENCES [cons].[StudiesStudyPeriods] ([CalDateKey], [StudyId]),
    CONSTRAINT [FK_SubscriptionsCompanies_Study_LookUp] FOREIGN KEY ([StudyId]) REFERENCES [dim].[Study_LookUp] ([StudyId]),
    CONSTRAINT [FK_SubscriptionsCompanies_StudyPeriods_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[StudyPeriods_LookUp] ([CalDateKey])
);


GO

CREATE TRIGGER [cons].[t_SubscriptionsCompanies_u]
	ON [cons].[SubscriptionsCompanies]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[SubscriptionsCompanies]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[SubscriptionsCompanies].StudyId		= INSERTED.StudyId
		AND	[cons].[SubscriptionsCompanies].CalDateKey	= INSERTED.CalDateKey
		AND	[cons].[SubscriptionsCompanies].CompanyId	= INSERTED.CompanyId;

END;