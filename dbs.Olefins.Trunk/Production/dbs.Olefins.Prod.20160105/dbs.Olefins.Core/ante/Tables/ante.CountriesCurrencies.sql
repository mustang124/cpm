﻿CREATE TABLE [ante].[CountriesCurrencies] (
    [CountryId]      CHAR (3)           NOT NULL,
    [CurrencyId]     VARCHAR (4)        NOT NULL,
    [SelectionUse]   BIT                CONSTRAINT [DF_CountriesCurrencies_InflationUse] DEFAULT ((1)) NOT NULL,
    [InflationUse]   BIT                CONSTRAINT [DF_CountriesCurrencies_SelectionUse] DEFAULT ((0)) NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_CountriesCurrencies_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_CountriesCurrencies_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_CountriesCurrencies_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_CountriesCurrencies_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_CountriesCurrencies] PRIMARY KEY CLUSTERED ([CountryId] ASC, [CurrencyId] ASC),
    CONSTRAINT [FK_CountriesCurrencies_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_CountriesCurrencies_Currency_LookUp] FOREIGN KEY ([CurrencyId]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId])
);


GO

CREATE TRIGGER [ante].[t_CountriesCurrencies_u]
	ON [ante].[CountriesCurrencies]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [ante].[CountriesCurrencies]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[CountriesCurrencies].[CountryId]	= INSERTED.[CountryId]
		AND	[ante].[CountriesCurrencies].[CurrencyId]	= INSERTED.[CurrencyId];
		
END;
