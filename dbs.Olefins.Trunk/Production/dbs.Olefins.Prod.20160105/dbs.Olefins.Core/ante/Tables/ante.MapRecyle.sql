﻿CREATE TABLE [ante].[MapRecyle] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [RecycleId]      VARCHAR (42)       NOT NULL,
    [StreamNiceness] VARCHAR (42)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MapRecyle_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MapRecyle_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MapRecyle_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MapRecyle_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MapRecycle] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC),
    CONSTRAINT [FK_MapRecyle_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_MapRecyle_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MapRecyle_Niceness_Stream_LookUp] FOREIGN KEY ([StreamNiceness]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_MapRecyle_Recycle_Stream_LookUp] FOREIGN KEY ([RecycleId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_MapRecyle_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_MapRecyle_u]
	ON [ante].[MapRecyle]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[MapRecyle]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[MapRecyle].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[MapRecyle].[StreamId]		= INSERTED.[StreamId];

END;
