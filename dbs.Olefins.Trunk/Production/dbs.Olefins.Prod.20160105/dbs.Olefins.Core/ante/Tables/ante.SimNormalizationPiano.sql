﻿CREATE TABLE [ante].[SimNormalizationPiano] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [StreamId]         VARCHAR (42)       NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Component_WtPcnt] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_SimNormalizationPiano_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_SimNormalizationPiano_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_SimNormalizationPiano_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_SimNormalizationPiano_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SimNormalizationPiano] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_SimNormalizationPiano_Component_WtPcnt] CHECK ([Component_WtPcnt]>=(0.0) AND [Component_WtPcnt]<=(100.0)),
    CONSTRAINT [FK_SimNormalizationPiano_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_SimNormalizationPiano_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_SimNormalizationPiano_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_SimNormalizationPiano_u]
	ON [ante].[SimNormalizationPiano]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[SimNormalizationPiano]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[SimNormalizationPiano].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[SimNormalizationPiano].StreamId			= INSERTED.StreamId
		AND	[ante].[SimNormalizationPiano].ComponentId		= INSERTED.ComponentId;

END;