﻿CREATE PROCEDURE [stgFact].[Insert_MetaFeedProd]
(
	@Refnum      VARCHAR (25),
	@FeedProdId  VARCHAR (20),

	@Qtr1        REAL         = NULL,
	@Qtr2        REAL         = NULL,
	@Qtr3        REAL         = NULL,
	@Qtr4        REAL         = NULL,
	@AnnFeedProd REAL         = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[MetaFeedProd]([Refnum], [FeedProdId], [Qtr1], [Qtr2], [Qtr3], [Qtr4], [AnnFeedProd])
	VALUES(@Refnum, @FeedProdId, @Qtr1, @Qtr2, @Qtr3, @Qtr4, @AnnFeedProd);

END;
