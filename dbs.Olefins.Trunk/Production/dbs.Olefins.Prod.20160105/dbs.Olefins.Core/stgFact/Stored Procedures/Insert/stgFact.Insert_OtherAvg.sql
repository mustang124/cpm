﻿CREATE PROCEDURE [stgFact].[Insert_OtherAvg]
(
	@Refnum			VARCHAR(25),
	@FeedProdId		VARCHAR(20),

	@Q1Avg			REAL		= NULL,
	@Q2Avg			REAL		= NULL,
	@Q3Avg			REAL		= NULL,
	@Q4Avg			REAL		= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[OtherAvg]([Refnum], [FeedProdId], [Q1Avg], [Q2Avg], [Q3Avg], [Q4Avg])
	VALUES(@Refnum, @FeedProdId, @Q1Avg, @Q2Avg, @Q3Avg, @Q4Avg);

END;
