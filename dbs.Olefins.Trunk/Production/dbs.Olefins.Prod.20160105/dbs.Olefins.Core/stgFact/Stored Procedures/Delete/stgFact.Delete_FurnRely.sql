﻿CREATE PROCEDURE [stgFact].[Delete_FurnRely]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[FurnRely]
	WHERE [Refnum] = @Refnum;

END;