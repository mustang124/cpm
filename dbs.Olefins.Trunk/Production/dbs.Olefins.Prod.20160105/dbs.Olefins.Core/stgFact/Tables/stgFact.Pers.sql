﻿CREATE TABLE [stgFact].[Pers] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [PersId]         VARCHAR (15)       NOT NULL,
    [SortKey]        INT                NOT NULL,
    [SectionId]      VARCHAR (4)        NULL,
    [NumPers]        REAL               NULL,
    [STH]            REAL               NULL,
    [OvtHours]       REAL               NULL,
    [Contract]       REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Pers_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Pers_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Pers_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Pers_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Pers] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PersId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Pers_u]
	ON [stgFact].[Pers]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Pers]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Pers].Refnum		= INSERTED.Refnum
		AND	[stgFact].[Pers].PersId		= INSERTED.PersId;

END;