﻿CREATE TABLE [stgFact].[MetaFeedProd] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdId]     VARCHAR (20)       NOT NULL,
    [Qtr1]           REAL               NULL,
    [Qtr2]           REAL               NULL,
    [Qtr3]           REAL               NULL,
    [Qtr4]           REAL               NULL,
    [AnnFeedProd]    REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_MetaFeedProd_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaFeedProd_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaFeedProd_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaFeedProd_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_MetaFeedProd] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_MetaFeedProd_u]
	ON [stgFact].[MetaFeedProd]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[MetaFeedProd]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[MetaFeedProd].Refnum			= INSERTED.Refnum
		AND	[stgFact].[MetaFeedProd].FeedProdId		= INSERTED.FeedProdId;

END;