﻿CREATE TABLE [stgFact].[OtherAvg] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdId]     VARCHAR (20)       NOT NULL,
    [Q1Avg]          REAL               NULL,
    [Q2Avg]          REAL               NULL,
    [Q3Avg]          REAL               NULL,
    [Q4Avg]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_OtherAvg_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_OtherAvg_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_OtherAvg_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_OtherAvg_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_OtherAvg] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_OtherAvg_u]
	ON [stgFact].[OtherAvg]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[OtherAvg]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[OtherAvg].Refnum			= INSERTED.Refnum
		AND	[stgFact].[OtherAvg].FeedProdId		= INSERTED.FeedProdId;

END;