﻿CREATE PROCEDURE [inter].[Insert_CompositionCalc]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [inter].[CompositionCalc](
			FactorSetId,
			Refnum,
			CalDateKey,
			SimModelId,
			OpCondId,
			StreamId,
			StreamDescription,
			ComponentId,
			Component_WtPcnt
			)
		SELECT
			u.FactorSetId,
			u.Refnum,
			u.Plant_QtrDateKey,
			'Plant',
			'OSOP',
			u.StreamId,
			u.StreamDescription,
			u.ComponentId,
			u.Component_WtPcnt
		FROM (
			SELECT
				p.FactorSetId,
				p.Refnum,
				p.Plant_QtrDateKey,
				p.StreamId,
				p.StreamDescription,

				CASE [StreamId]
					WHEN 'Hydrogen'			THEN	p.H2
					WHEN 'Methane'			THEN	(100.0 - p.CH4)* 0.777778
				END	[H2],

				CASE [StreamId]
					WHEN 'Hydrogen'			THEN	100.0 - p.H2
					WHEN 'Methane'			THEN	p.CH4
					WHEN 'EthylenePG'		THEN	(100.0 - p.C2H4) * 0.25
					WHEN 'EthyleneCG'		THEN	(100.0 - p.C2H4) * 0.05
				END	[CH4],

				CASE [StreamId]
					WHEN 'EthylenePG'		THEN	p.C2H4
					WHEN 'EthyleneCG'		THEN	p.C2H4
					WHEN 'PropyleneCG'		THEN	[calc].[MinValue]((100.0 - p.C3H6)* 0.05, 3.0)
					WHEN 'PropyleneRG'		THEN	[calc].[MinValue]((100.0 - p.C3H6)* 0.05, 3.0)
				END	[C2H4],

				CASE [StreamId]
					WHEN 'Methane'			THEN	(100.0 - p.CH4) * 0.222222
					WHEN 'EthylenePG'		THEN	(100.0 - p.C2H4) * 0.50
					WHEN 'EthyleneCG'		THEN	(100.0 - p.C2H4) * 0.80
					WHEN 'PropylenePG'		THEN	(100.0 - p.C3H6) * 0.25
					WHEN 'PropyleneCG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.1, 2.0)
					WHEN 'PropyleneRG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.1, 2.0)
				END	[C2H6],

				CASE [StreamId]
					WHEN 'PropyleneCG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.05, 0.5)
					WHEN 'PropyleneRG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.05, 0.5)
				END	[C3H4],

				CASE [StreamId]
					WHEN 'EthyleneCG'		THEN	(100. - p.C2H4) * 0.05
					WHEN 'PropylenePG'		THEN	p.C3H6
					WHEN 'PropyleneCG'		THEN	p.C3H6
					WHEN 'PropyleneRG'		THEN	p.C3H6
					WHEN 'PropaneC3Resid'	THEN	(100.0 - p.C3H8) * 15.0 / 16.0
				END	[C3H6],

				CASE [StreamId]
					WHEN 'EthylenePG'		THEN	(100.0 - p.C2H4) * 0.25
					WHEN 'EthyleneCG'		THEN	(100.0 - p.C2H4) * 0.1
					WHEN 'PropylenePG'		THEN	(100.0 - p.C3H6) * 0.5
					WHEN 'PropyleneCG'		THEN	100.0 - ((((((p.C3H6+[calc].[MinValue]((100.0 - p.C3H6) * 0.050, 3.0)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.100, 2.0)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.050, 0.5)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.015, 0.2)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.050, 1.0)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.100, 2.0))
					WHEN 'PropyleneRG'		THEN	100.0 - ((((((p.C3H6+[calc].[MinValue]((100.0 - p.C3H6) * 0.050, 3.0)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.100, 2.0)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.050, 0.5)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.015, 0.2)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.050, 1.0)) + [calc].[MinValue]((100.0 - p.C3H6) * 0.100, 2.0))
					WHEN 'PropaneC3Resid'	THEN	p.C3H8
				END	[C3H8],

				CASE [StreamId]
					WHEN 'PropyleneCG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.015, 0.2)
					WHEN 'PropyleneRG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.015, 0.2)
				END	[C4H6],

 				CASE [StreamId]
					WHEN 'PropyleneCG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.05, 1.0)
					WHEN 'PropyleneRG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.05, 1.0)
				END	[C4H8],

				CASE [StreamId]
					WHEN 'PropylenePG'		THEN	(100.0 - p.C3H6) * 0.25
					WHEN 'PropyleneCG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.1, 2.0)
					WHEN 'PropyleneRG'		THEN	[calc].[MinValue]((100.0 - p.C3H6) * 0.1, 2.0)
					WHEN 'PropaneC3Resid'	THEN	(100.0 - p.C3H8) / 16.0
				END	[C4H10]

			FROM (
				SELECT
					tsq.FactorSetId,
					tsq.Refnum,
					tsq.Plant_QtrDateKey,
					q.StreamId,
					q.StreamDescription,
					p.ComponentId,
					ISNULL(c.Component_WtPcnt, p.Component_WtPcnt)	[Component_WtPcnt]
				FROM	@fpl									tsq
				INNER JOIN fact.Quantity						q
					ON	q.Refnum = tsq.Refnum
					AND	q.CalDateKey = tsq.Plant_QtrDateKey
				INNER JOIN ante.Composition						p
					ON	p.FactorSetId = tsq.FactorSetId
					AND	p.CalDateKey = tsq.FactorSet_QtrDateKey
					AND	p.StreamId = q.StreamId
					AND p.Component_Expansion = 1
					AND p.ComponentId IN ('H2', 'CH4', 'C2H4', 'C3H6', 'C3H8')
				LEFT OUTER JOIN fact.CompositionQuantity		c
					ON	c.Refnum = tsq.Refnum
					AND	c.CalDateKey = tsq.Plant_QtrDateKey
					AND	c.StreamId = p.StreamId
					AND	c.ComponentId = p.ComponentId
					AND c.ComponentId IN ('H2', 'CH4', 'C2H4', 'C3H6', 'C3H8')
				) u
				PIVOT(
				MAX(u.Component_WtPcnt) FOR u.ComponentId IN(
					H2,
					CH4,
					C2H4,
					C3H6,
					C3H8
					)
				) p
			) p
			UNPIVOT (
			Component_WtPcnt FOR ComponentId IN (
				H2,
				CH4,
				C2H4,
				C2H6,
				C3H4,
				C3H6,
				C3H8,
				C4H6,
				C4H8,
				C4H10
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;