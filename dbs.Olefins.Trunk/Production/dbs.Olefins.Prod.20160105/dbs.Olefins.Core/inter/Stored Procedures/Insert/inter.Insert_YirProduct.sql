﻿CREATE PROCEDURE [inter].[Insert_YirProduct]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @InterProduct TABLE
		(
			FactorSetId				VARCHAR(12)			NOT	NULL	CHECK(FactorSetId <> ''),
			Refnum					VARCHAR(25)			NOT NULL	CHECK(Refnum <> ''),
			CalDateKey				INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			StreamId				VARCHAR(42)			NOT	NULL	CHECK(StreamId <> ''),
			ComponentId				VARCHAR(42)			NOT	NULL	CHECK(ComponentId <> ''),
			
			Component_kMT			REAL					NULL
			);

		INSERT INTO @InterProduct(FactorSetId, Refnum, CalDateKey, StreamId, ComponentId, Component_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			c.StreamId,
			'CH4',
			 - c.Component_kMT
		FROM @fpl												tsq
		INNER JOIN calc.[CompositionStream]						c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_QtrDateKey
			AND	c.StreamId		= 'ROGInerts'
			AND	c.ComponentId	= 'Inerts'
			AND c.Component_kMT > 0.0
		WHERE	tsq.FactorSet_AnnDateKey	< 20130000;

		INSERT INTO @InterProduct(FactorSetId, Refnum, CalDateKey, StreamId, ComponentId, Component_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			CASE WHEN c.StreamId = 'ProdOther' THEN LEFT(c.StreamDescription, 42) ELSE c.StreamId END,
			c.ComponentId,
			c.Component_kMT
		FROM @fpl												tsq
		INNER JOIN calc.[CompositionStream]						c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_QtrDateKey
			AND c.Component_kMT	> 0.0
		INNER JOIN dim.Stream_Bridge							b
			ON	b.FactorSetId	= tsq.FactorSetId
			AND	b.DescendantId	= c.StreamId
			AND	b.StreamId		IN ('ProdLoss', 'ROGInerts')
		WHERE	NOT(c.StreamId = 'PPFC' AND c.ComponentId IN ('Other', 'Loss', 'Inerts', 'C2H6', 'C3H8', 'C4H10'))
			AND	tsq.FactorSet_AnnDateKey	< 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.Insert_YirProduct (FactorSet > 20130000, StreamId <> PPFC)';
		PRINT @ProcedureDesc;

		INSERT INTO @InterProduct(FactorSetId, Refnum, CalDateKey, StreamId, ComponentId, Component_kMT)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_AnnDateKey,
			CASE WHEN c.StreamId = 'ProdOther' THEN LEFT(c.StreamDescription, 42) ELSE c.StreamId END,
			c.ComponentId,
			c.Component_kMT
		FROM @fpl										tsq
		INNER JOIN calc.[CompositionStream]				c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_QtrDateKey
			AND	c.Component_kMT > 0.0
		INNER JOIN dim.Stream_Bridge					b
			ON	b.FactorSetId	= tsq.FactorSetId
			AND	b.DescendantId	= c.StreamId
			AND	b.StreamId		= 'ProdLoss'
			AND	b.DescendantId	<> 'PPFC'
		WHERE	tsq.FactorSet_AnnDateKey > 20130000;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.Insert_YirProduct (PPFCEnergy > 20130000)';
		PRINT @ProcedureDesc;

		DECLARE @PPFCEnergy TABLE
		(
			FactorSetId				VARCHAR(12)			NOT	NULL	CHECK(FactorSetId <> ''),
			Refnum					VARCHAR(25)			NOT NULL	CHECK(Refnum <> ''),
			CalDateKey				INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			StreamId				VARCHAR(42)			NOT	NULL	CHECK(StreamId <> ''),
			PPFC_kMT				REAL				NOT	NULL,
			Energy_kMT				REAL				NOT	NULL,
			[_PPFCLessEnergy_kMT]	AS CONVERT(REAL, [PPFC_kMT] - [Energy_kMT]),

			PRIMARY KEY CLUSTERED (FactorSetId ASC, Refnum ASC, StreamId ASC, CalDateKey ASC)
		);

		INSERT INTO @PPFCEnergy (FactorSetId, Refnum, CalDateKey, StreamId, PPFC_kMT, Energy_kMT)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			'PPFC',
			ABS(sqa.Quantity_kMT)		[PPFC_kMT],
			SUM(e.LHValue_MBTU / nhv.NHValue_MBtuLb) / 2.2046
		FROM @fpl						fpl
		INNER JOIN fact.StreamQuantityAggregate		sqa WITH (NOEXPAND)
			ON	sqa.FactorSetId		= fpl.FactorSetId
			AND	sqa.Refnum			= fpl.Refnum
			AND	sqa.StreamId		= 'PPFC'
		INNER JOIN fact.EnergyLHValue	e
			ON	e.Refnum			= fpl.Refnum
			AND	e.CalDateKey		= fpl.Plant_QtrDateKey
			AND	e.AccountId	IN (SELECT d.DescendantId FROM dim.Account_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.AccountId = 'PPFCLiquid')
		INNER JOIN ante.PricingNetHeatingValueStream	nhv
			ON	nhv.FactorSetId		= fpl.FactorSetId
			AND	nhv.StreamId		= e.AccountId
		WHERE	fpl.CalQtr			= 4
			AND	fpl.FactorSet_AnnDateKey	> 20130000
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			sqa.Quantity_kMT;

		DECLARE @PPFCLessOther TABLE (
			FactorSetId					VARCHAR(12)			NOT	NULL	CHECK(FactorSetId <> ''),
			Refnum						VARCHAR(25)			NOT NULL	CHECK(Refnum <> ''),
			CalDateKey					INT					NOT	NULL	CHECK(CalDateKey > 19000101 AND CalDateKey < 99991231),
			StreamId					VARCHAR(42)			NOT	NULL	CHECK(StreamId <> ''),
			ComponentId					VARCHAR(42)			NOT	NULL	CHECK(ComponentId <> ''),
			
			Component_kMT				REAL				NOT	NULL,
			PPFCLessOther_kMT			REAL				NOT NULL,
			PPFCLessEnergy_kMT			REAL					NULL,

			_ComponentDistributed_kMT	AS CONVERT(REAL, Component_kMT / PPFCLessOther_kMT * COALESCE(PPFCLessEnergy_kMT, PPFCLessOther_kMT), 1)
			);

		INSERT INTO @PPFCLessOther(FactorSetId, Refnum, CalDateKey, StreamId, ComponentId, Component_kMT, PPFCLessOther_kMT, PPFCLessEnergy_kMT)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			c.StreamId,
			c.ComponentId,
			SUM(c.Component_kMT),
			SUM(SUM(c.Component_kMT)) OVER(PARTITION BY fpl.FactorSetId, fpl.Refnum, fpl.Plant_AnnDateKey, c.StreamId),
			e._PPFCLessEnergy_kMT
		FROM @fpl									fpl
		INNER JOIN calc.[CompositionStream]			c
			ON	c.FactorSetId	= fpl.FactorSetId
			AND	c.Refnum		= fpl.Refnum
			AND	c.CalDateKey	= fpl.Plant_QtrDateKey
			AND	c.StreamId		= 'PPFC'
			AND	c.ComponentId	IN ('H2', 'CH4', 'C2H4', 'Inerts')
		LEFT OUTER JOIN @PPFCEnergy					e
			ON	e.FactorSetId	= fpl.FactorSetId
			AND	e.Refnum		= fpl.Refnum
			AND	e.CalDateKey	= fpl.Plant_AnnDateKey
			AND	e.StreamId		= c.StreamId
		WHERE	fpl.FactorSet_AnnDateKey > 20130000
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			c.StreamId,
			c.ComponentId,
			e._PPFCLessEnergy_kMT;

		INSERT INTO @InterProduct(FactorSetId, Refnum, CalDateKey, StreamId, ComponentId, Component_kMT)
		SELECT
			p.FactorSetId,
			p.Refnum,
			p.CalDateKey,
			p.StreamId,
			p.ComponentId,
			p._ComponentDistributed_kMT
		FROM @PPFCLessOther p

		INSERT INTO @InterProduct(FactorSetId, Refnum, CalDateKey, StreamId, ComponentId, Component_kMT)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			'PPFC',
			nhv.ComponentId,
			e.LHValue_MBTU / nhv.NHValue_MBtuLb / 2.2046
		FROM @fpl						fpl
		INNER JOIN fact.EnergyLHValue	e
			ON	e.Refnum			= fpl.Refnum
			AND	e.CalDateKey		= fpl.Plant_QtrDateKey
			AND	e.AccountId	IN (SELECT d.DescendantId FROM dim.Account_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.AccountId = 'PPFCLiquid')
		INNER JOIN ante.PricingNetHeatingValueStream	nhv
			ON	nhv.FactorSetId		= fpl.FactorSetId
			AND	nhv.StreamId		= e.AccountId
		WHERE	fpl.CalQtr			= 4
			AND	fpl.FactorSet_AnnDateKey	> 20130000

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO inter.Insert_YirProduct';
		PRINT @ProcedureDesc;

		INSERT INTO inter.YIRProduct(FactorSetId, Refnum, CalDateKey, ComponentId, Component_kMT, Component_WtPcnt)
		SELECT
			t.FactorSetId,
			t.Refnum,
			t.CalDateKey,
			t.ComponentId,
			t.Component_kMT,
			t.Component_kMT / SUM(t.Component_kMT) OVER(PARTITION BY t.FactorSetId, t.Refnum, t.CalDateKey) * 100.0
		FROM (
			SELECT
				p.FactorSetId,
				p.Refnum,
				MAX(p.CalDateKey)		[CalDateKey],
				p.ComponentId,
				SUM(p.Component_kMT)	[Component_kMT]
			FROM @InterProduct p
			WHERE p.Component_kMT IS NOT NULL
			GROUP BY
				p.FactorSetId,
				p.Refnum,
				p.ComponentId
			HAVING SUM(p.Component_kMT) IS NOT NULL
			) t;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;