﻿CREATE FUNCTION etl.ConvStreamID
(
	@StreamId	NVARCHAR(42)
)
RETURNS NVARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar NVARCHAR(42) =
	CASE @StreamId
		
		WHEN 'OthC4'					THEN 'C4Oth'
		WHEN 'OthLoss'					THEN 'LossOther'
		
		WHEN 'OthPyGas'					THEN 'PyroGasoline'
		WHEN 'PyFuelOil'				THEN 'PyroFuelOil'
		WHEN 'PyGasoil'					THEN 'PyroGasOil'
		
		WHEN 'OthSpl'					THEN 'SuppOther'
		WHEN 'WashOil'					THEN 'SuppWashOil'
		WHEN 'GasOil'					THEN 'SuppGasOil'
		WHEN 'Nitrogen'					THEN 'SuppNitrogen'
				
		WHEN 'FlareLoss'				THEN 'LossFlareVent'
		WHEN 'MeasureLoss'				THEN 'LossMeasure'
		
		WHEN 'Acetylene'				THEN 'Acetylene'
		WHEN 'Hydrogen'					THEN 'Hydrogen'
		WHEN 'Butane'					THEN 'Butane'
		WHEN 'Ethane'					THEN 'Ethane'
		WHEN 'EPMix'					THEN 'EPMix'
		WHEN 'LPG'						THEN 'LPG'
		
		WHEN 'ButRec'					THEN 'ButRec'
		WHEN 'EthRec'					THEN 'EthRec'
		WHEN 'ProRec'					THEN 'ProRec'
				
		WHEN 'LtNaphtha'				THEN 'NaphthaLt'
		WHEN 'FRNaphtha'				THEN 'NaphthaFr'
		WHEN 'HeavyNaphtha'				THEN 'NaphthaHv'
		WHEN 'HTGasoil'					THEN 'GasOilHt'
		WHEN 'HeavyGasoil'				THEN 'GasOilHv'
		WHEN 'Diesel'					THEN 'Diesel'
		WHEN 'Condensate'				THEN 'Condensate'
		WHEN 'Raffinate'				THEN 'Raffinate'
		WHEN 'HeavyNGL'					THEN 'HeavyNGL'
	
		WHEN 'PPCFuel'					THEN 'PPFC'
		
		WHEN 'Butadiene'				THEN 'Butadiene'
		
		WHEN 'Ethylene'					THEN 'EthylenePG'
		WHEN 'EthyleneCG'				THEN 'EthyleneCG'
		WHEN 'Propylene'				THEN 'PropylenePG'
		WHEN 'PropyleneCG'				THEN 'PropyleneCG'
		WHEN 'PropyleneRG'				THEN 'PropyleneRG'
		
		WHEN 'PropaneC3'				THEN 'PropaneC3Resid'
		WHEN 'PropaneResidual'			THEN 'PropaneC3Resid'
		
		WHEN 'DiBenzene'				THEN 'DilBenzene'
		WHEN 'DiButadiene'				THEN 'DilButadiene'
		WHEN 'DiButane'					THEN 'DilButane'
		WHEN 'DiButylenes'				THEN 'DilButylene'
		WHEN 'DiEthane'					THEN 'DilEthane'
		WHEN 'DiEthylene'				THEN 'DilEthylene'
		WHEN 'DiHydrogen'				THEN 'DilHydrogen'
		WHEN 'DiMethane'				THEN 'DilMethane'
		WHEN 'DiPropane'				THEN 'DilPropane'
		WHEN 'DiPropylene'				THEN 'DilPropylene'
		WHEN 'DiMogas'					THEN 'DilMogas'
		
		WHEN 'RefC4Plus'				THEN 'ROGC4Plus'
		WHEN 'RefEthane'				THEN 'ROGEthane'
		WHEN 'RefEthylene'				THEN 'ROGEthylene'
		WHEN 'RefHydrogen'				THEN 'ROGHydrogen'
		WHEN 'RefInerts'				THEN 'ROGInerts'
		WHEN 'RefMethane'				THEN 'ROGMethane'
		WHEN 'RefPropane'				THEN 'ROGPropane'
		WHEN 'RefPropylene'				THEN 'ROGPropylene'
		
		WHEN 'ConcBenzene'				THEN 'ConcBenzene'
		WHEN 'ConcButadiene'			THEN 'ConcButadiene'
		WHEN 'ConcEthylene'				THEN 'ConcEthylene'
		WHEN 'ConcPropylene'			THEN 'ConcPropylene'

		WHEN 'OthLtFeed'				THEN 'FeedLtOther'
		WHEN 'OthLiqFeed1'				THEN 'FeedLiqOther'
		WHEN 'OthLiqFeed2'				THEN 'FeedLiqOther'
		WHEN 'OthLiqFeed3'				THEN 'FeedLiqOther'
		WHEN 'OthProd1'					THEN 'ProdOther'
		WHEN 'OthProd2'					THEN 'ProdOther'
		WHEN 'FuelGasSales'				THEN 'Methane'
		WHEN 'Propane'					THEN 'Propane'
		WHEN 'ProdOther1'				THEN 'ProdOther'
		WHEN 'ProdOther2'				THEN 'ProdOther'
		
		WHEN 'AcidGas'					THEN 'AcidGas'
		WHEN 'Benzene'					THEN 'Benzene'
		WHEN 'Isobutylene'				THEN 'Isobutylene'
		
		-- Table 6-4
		WHEN 'LiqHeavy'					THEN 'LiqHeavy'
		WHEN 'DSL'						THEN 'Diesel'
		WHEN 'Ethane/ Propane mix'		THEN 'EPMix'
		WHEN 'GASOIL <390'				THEN 'EpLT390'
		WHEN 'GasOil >390'				THEN 'EpGT390'
		WHEN 'GASOIL<390'				THEN 'EpLT390'
		WHEN 'GASOIL<391'				THEN 'EpLT390'
		WHEN 'GASOIL>390'				THEN 'EpGT390'
		WHEN 'NAP'						THEN 'Naphtha'
		WHEN 'NAP+C2/C3'				THEN 'Naphtha'
		WHEN 'NAP/SSOT'					THEN 'Naphtha'
		WHEN 'NAPHTA'					THEN 'Naphtha'
		WHEN 'Other Light'				THEN 'Light'
		WHEN 'Other Liquid'				THEN 'Liquid'
		WHEN 'OTHERLIGHT'				THEN 'Light'
		WHEN 'OTHERLIQUID'				THEN 'Liquid'
		WHEN 'SEV.HYD.GO'				THEN 'GasOilHt'
		
		--WHEN 'FG'						THEN 'PyroFuelOil'
		--WHEN 'FO'						THEN 'PyroGasOil'

		--WHEN 'FG'						THEN 'FuelGas'
		--WHEN 'FO'						THEN 'FuelOil'
		
		-- Table 8-1 (Import Duties)
		WHEN 'HEAVY FEEDS'				THEN 'LiqHeavy'
		WHEN 'HEAVY NATURAL GAS LIQUID'	THEN 'HeavyNGL'
		WHEN 'KEROSENE'					THEN 'EpLT390'
		WHEN 'Other - please describe'	THEN 'FeedOther'
		WHEN 'OTHER – PLEASE DESCRIBE'	THEN 'FeedOther'
		WHEN 'FeedOther'				THEN 'FeedOther'
		
		-- Table 8-3 (Import Duties)
		WHEN 'NRC'						THEN 'LiqLight'
		WHEN 'ProLPG'					THEN 'Propane'
		WHEN 'Other'					THEN 'FeedOther'
		
		-- Table 11 (Feedstock Practices)
		WHEN 'LPGs'						THEN 'LPG'
		WHEN 'Naphtha'					THEN 'Naphtha'
		
		-- Table 13 (Metathesis)
		WHEN 'Ethy_OC'					THEN 'MetaOCEthylene'
		WHEN 'Butanes_OC'				THEN 'MetaOCButanes'
		WHEN 'Butene1_OC'				THEN 'MetaOCButene1'
		WHEN 'Butene2_OC'				THEN 'MetaOCButene2'
		WHEN 'Oth_OC'					THEN 'MetaOCOther'
		
		WHEN 'Ethy_Purch'				THEN 'MetaPurchEthylene'
		WHEN 'Butanes_Purch'			THEN 'MetaPurchButane'
		WHEN 'Butene1_Purch'			THEN 'MetaPurchButene1'
		WHEN 'Butene2_Purch'			THEN 'MetaPurchButene2'
		WHEN 'Oth_Purch'				THEN 'MetaPurchOther'
		
		WHEN 'PGP'						THEN 'MetaProdPropylenePG'
		WHEN 'UnreactLP'				THEN 'MetaProdUnReactLightPurge'
		WHEN 'UnreatButanes'			THEN 'MetaProdUnReactButane'
		WHEN 'C5'						THEN 'MetaProdC5'
		
		-- Pricing
		--WHEN 'ATMO_GASOIL'				THEN NULL
		--WHEN 'BUTYLENE_ALKY'			THEN NULL
		--WHEN 'ISOBUTANE'				THEN NULL
		--WHEN 'MIXEDBUTANES'				THEN NULL
		--WHEN 'NAPHTHA_GENERIC'			THEN NULL
		--WHEN 'REFORM_NAPHTHA'			THEN NULL
		--WHEN 'NATURAL_GAS'				THEN 'DilMethane'
		--WHEN 'OCTANE'					THEN NULL
		--WHEN 'PROPYLENE_ALKY'			THEN NULL
		--WHEN 'PROPYLENE_REFY'			THEN NULL
		--WHEN 'PYROGASOLINE_HY'			THEN NULL
		--WHEN 'PYROGASOLINE_UT'			THEN NULL
		--WHEN 'PREMIUM_MOGAS'			THEN NULL
		--WHEN 'REGULAR_MOGAS'			THEN NULL
		--WHEN 'RES_HI_S'					THEN NULL
		--WHEN 'RES_LO_S'					THEN NULL
		--WHEN 'C4Oth'					THEN 'C4Oth'
		
		--WHEN 'PyroGasOil'				THEN 'PyroGasOil'
		--WHEN 'PyroFuelOil'				THEN 'PyroFuelOil'
		--WHEN 'DilPropane'				THEN 'DilPropane'
		--WHEN 'DilButane'				THEN 'DilButane'
		--WHEN 'SuppWashOil'				THEN 'SuppWashOil'
		--WHEN 'SuppGasOil'				THEN 'SuppGasOil'
		--WHEN 'PyroGasoilHt'				THEN 'PyroGasoilHt'
		--WHEN 'DilButylene'				THEN 'DilButylene'
		--WHEN 'PyroGasoilUt'				THEN 'PyroGasoilUt'
		--WHEN 'PricingAdj'				THEN 'PricingAdj'
		--WHEN 'DilEthane'				THEN 'DilEthane'
		--WHEN 'EthylenePG'				THEN 'EthylenePG'
		--WHEN 'PropylenePG'				THEN 'PropylenePG'
		--WHEN 'NaphthaHv'				THEN 'NaphthaHv'
		--WHEN 'NaphthaLt'				THEN 'NaphthaLt'
		--WHEN 'GasOilHv'					THEN 'GasOilHv'
		--WHEN 'GasOilHt'					THEN 'GasOilHt'
		--WHEN 'PPFCEthane'				THEN 'PPFCEthane'
		--WHEN 'PPFCPropane'				THEN 'PPFCPropane'
		--WHEN 'PPFCButane'				THEN 'PPFCButane'
		--WHEN 'PPFCPyroNaphtha'			THEN 'PPFCPyroNaphtha'
		--WHEN 'PPFCPyroGasOil'			THEN 'PPFCPyroGasOil'
		--WHEN 'PPFCPyroFuelOil'			THEN 'PPFCPyroFuelOil'

		ELSE @StreamId

	END;
	
	RETURN @ResultVar;

END