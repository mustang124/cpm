﻿
CREATE FUNCTION etl.ConvStreamIDPoly
(
	@StreamId	NVARCHAR(42)
)
RETURNS NVARCHAR(42)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar NVARCHAR(42) =
	CASE @StreamId
		WHEN 'TotalRawMatl'			THEN 'PolyFeed'
		WHEN 'MonomerConsumed'		THEN 'PolyMonomerConsumed'
		WHEN 'Hydrogen'				THEN 'PolyHydrogen'
		WHEN 'Ethylene_Plant'		THEN 'PolyAPEthylene'
		WHEN 'Ethylene_Other'		THEN 'PolyPurchEthylene'
		WHEN 'Propylene_Plant'		THEN 'PolyAPPropylene'
		WHEN 'Propylene_Other'		THEN 'PolyPurchPropylene'
		WHEN 'Butene'				THEN 'PolyButene'
		WHEN 'Hexene'				THEN 'PolyHexene'
		WHEN 'Octene'				THEN 'PolyOctene'
		
		WHEN 'OthMono1'				THEN 'PolyMonomer1'
		WHEN 'OthMono2'				THEN 'PolyMonomer2'
		WHEN 'OthMono3'				THEN 'PolyMonomer3'
		WHEN 'Additives'			THEN 'PolyAdditives'
		WHEN 'Catalysts'			THEN 'PolyCatalysts'
		WHEN 'OtherFeed'			THEN 'PolyOther'
		WHEN 'TotProducts'			THEN 'PolyProd'
		
		WHEN 'LDPE_Liner'			THEN 'LDPELiner'
		WHEN 'LDPE_Clarity'			THEN 'LDPEClarity'
		WHEN 'LDPE_Blow'			THEN 'LDPEBlowMold'
		WHEN 'LDPE_GP'				THEN 'LDPEGPInjMold'
		WHEN 'LDPE_LidResin'		THEN 'LDPEInjLidResin'
		WHEN 'LDPE_EVA'				THEN 'LDPEEVA'
		WHEN 'LDPE_Other'			THEN 'LDPEOther'
		WHEN 'LDPE_Offspec'			THEN 'LDPEOff'
		
		WHEN 'LLDPE_ButeneLiner'	THEN 'LLDPEBCLinerfilm'
		WHEN 'LLDPE_ButeneMold'		THEN 'LLDPEBCMold'
		WHEN 'LLDPE_HAOLiner'		THEN 'LLDPEHAOLinerFilm'
		WHEN 'LLDPE_HAOMeltIndex'	THEN 'LLDPEHAOMIFilm'
		WHEN 'LLDPE_HAOInject'		THEN 'LLDPEHAOInjMold'
		WHEN 'LLDPE_HAOLidResin'	THEN 'LLDPEHAOLidResin'
		WHEN 'LLDPE_HAORoto'		THEN 'LLDPEHAORotoMold'
		WHEN 'LLDPE_Other'			THEN 'LLDPEOther'
		WHEN 'LLDPE_Offspec'		THEN 'LLDPEOff'
		
		WHEN 'HDPE_Homopoly'		THEN 'HDPEBlowMoldHomoPoly'
		WHEN 'HDPE_Copoly'			THEN 'HDPEBlowMoldCoPoly'
		WHEN 'HDPE_Film'			THEN 'HDPEFilm'
		WHEN 'HDPE_GP'				THEN 'HDPEGPInjMold'
		WHEN 'HDPE_Pipe'			THEN 'HDPEPipe'
		WHEN 'HDPE_Drum'			THEN 'HDPEDrum'
		WHEN 'HDPE_Roto'			THEN 'HDPERotoMold'
		WHEN 'HDPE_Other'			THEN 'HDPEOther'
		WHEN 'HDPE_Offspec'			THEN 'HDPEOff'
		
		WHEN 'POLY_Homo'			THEN 'PolyProHomoPoly'
		WHEN 'POLY_Copoly'			THEN 'PolyProFilm'
		WHEN 'POLY_CopolyBlow'		THEN 'PolyProBlowMold'
		WHEN 'POLY_CopolyInject'	THEN 'PolyProInj'
		WHEN 'POLY_CopolyBlock'		THEN 'PolyProBlockCoPoly'
		WHEN 'POLY_AtacticPP'		THEN 'PolyProAtacticPolyPro'
		WHEN 'POLY_Other'			THEN 'PolyProOther'
		WHEN 'POLY_Offspec'			THEN 'PolyProOff'
		
		WHEN 'ByProdOth1'			THEN 'PolyByClient1'
		WHEN 'ByProdOth2'			THEN 'PolyByClient2'
		WHEN 'ByProdOth3'			THEN 'PolyByClient3'
		WHEN 'ByProdOth4'			THEN 'PolyByClient4'
		WHEN 'ByProdOth5'			THEN 'PolyByClient5'
		WHEN 'ReactorProd'			THEN 'PolyProdReactor'
		
		END;
	
	RETURN @ResultVar;

END