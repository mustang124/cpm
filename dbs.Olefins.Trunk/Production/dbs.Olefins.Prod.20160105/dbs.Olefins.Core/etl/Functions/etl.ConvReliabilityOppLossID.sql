﻿
CREATE FUNCTION etl.ConvReliabilityOppLossId
(
	@ReliabilityOppLossId	NVARCHAR(24)
)
RETURNS NVARCHAR(24)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(24) =
	CASE @ReliabilityOppLossId
		WHEN 'Cause1'			THEN 'FFPCause1'
		WHEN 'Cause2'			THEN 'FFPCause2'
		WHEN 'Cause3'			THEN 'FFPCause3'
		WHEN 'Cause4'			THEN 'FFPCause4'
		WHEN 'Cause5'			THEN 'FFPCause5'
		
		WHEN 'OtherProcess'	THEN 'ProcessOther'
		WHEN 'OtherProcess1'	THEN 'ProcessOther1'
		WHEN 'OtherProcess2'	THEN 'ProcessOther2'
		WHEN 'OtherProcess3'	THEN 'ProcessOther3'
		WHEN 'OtherProcess4'	THEN 'ProcessOther4'
		WHEN 'OtherProcess5'	THEN 'ProcessOther5'
		
		WHEN 'PlantTA'			THEN 'TurnAround'
		ELSE @ReliabilityOppLossId
	END;
	
	RETURN @ResultVar;
	
END;