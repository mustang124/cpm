﻿CREATE TABLE [gap].[YieldMargins] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [DataType]       VARCHAR (4)        NOT NULL,
    [GroupId]        VARCHAR (25)       NOT NULL,
    [ProdLoss]       REAL               NOT NULL,
    [FreshPyroFeed]  REAL               NOT NULL,
    [FeedSupp]       REAL               NOT NULL,
    [PlantFeed]      REAL               NOT NULL,
    [Recycle]        REAL               NOT NULL,
    [_FreshRec]      AS                 ([FreshPyroFeed]+[Recycle]) PERSISTED NOT NULL,
    [_ProdPyro]      AS                 ([ProdLoss]-[ProdSupp]) PERSISTED NOT NULL,
    [ProdSupp]       REAL               NOT NULL,
    [_GMPyro]        AS                 ((([ProdLoss]-[ProdSupp])-[FreshPyroFeed])-[Recycle]) PERSISTED NOT NULL,
    [_GMSupp]        AS                 (([ProdSupp]-[FeedSupp])+[Recycle]) PERSISTED NOT NULL,
    [_GM]            AS                 ([ProdLoss]-[PlantFeed]) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_YieldMargins_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_YieldMargins_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_YieldMargins_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_YieldMargins_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_YieldMargins] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [DataType] ASC, [GroupId] ASC),
    CONSTRAINT [CK_YieldMargins_DataType_StringLength] CHECK ([DataType]<>''),
    CONSTRAINT [CK_YieldMargins_GroupId_StringLength] CHECK ([GroupId]<>'')
);


GO

CREATE TRIGGER [gap].[t_YieldMargins_u]
    ON [gap].[YieldMargins]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [gap].[YieldMargins]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[gap].[YieldMargins].[FactorSetId]	= INSERTED.[FactorSetId]
		AND [gap].[YieldMargins].[DataType]	= INSERTED.[DataType]
		AND [gap].[YieldMargins].[GroupId]	= INSERTED.[GroupId];

END;