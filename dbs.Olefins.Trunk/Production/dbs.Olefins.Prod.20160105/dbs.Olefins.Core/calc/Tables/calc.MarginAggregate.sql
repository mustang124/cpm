﻿CREATE TABLE [calc].[MarginAggregate] (
    [FactorSetId]      VARCHAR (12)        NOT NULL,
    [Refnum]           VARCHAR (25)        NOT NULL,
    [CalDateKey]       INT                 NOT NULL,
    [CurrencyRpt]      VARCHAR (4)         NOT NULL,
    [MarginAnalysis]   VARCHAR (42)        NOT NULL,
    [MarginId]         VARCHAR (42)        NOT NULL,
    [Hierarchy]        [sys].[hierarchyid] NOT NULL,
    [Stream_Cur]       REAL                NULL,
    [OpEx_Cur]         REAL                NULL,
    [Amount_Cur]       REAL                NOT NULL,
    [TaAdj_Stream_Cur] REAL                NULL,
    [TaAdj_OpEx_Cur]   REAL                NULL,
    [TaAdj_Amount_Cur] REAL                NOT NULL,
    [tsModified]       DATETIMEOFFSET (7)  CONSTRAINT [DF_MarginAggregate_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)      CONSTRAINT [DF_MarginAggregate_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)      CONSTRAINT [DF_MarginAggregate_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)      CONSTRAINT [DF_MarginAggregate_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_MarginAggregate] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MarginAnalysis] ASC, [MarginId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_MarginAggregate_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_MarginAggregate_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_MarginAggregate_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_MarginAggregate_Margin_LookUp] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_calc_MarginAggregate_Stream_LookUp] FOREIGN KEY ([MarginAnalysis]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_MarginAggregate_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [calc].[t_MarginAggregate_u]
	ON [calc].[MarginAggregate]
	AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.MarginAggregate
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MarginAggregate.FactorSetId	= INSERTED.FactorSetId
		AND	calc.MarginAggregate.Refnum			= INSERTED.Refnum
		AND calc.MarginAggregate.CalDateKey		= INSERTED.CalDateKey
		AND calc.MarginAggregate.CurrencyRpt	= INSERTED.CurrencyRpt
		AND calc.MarginAggregate.MarginAnalysis	= INSERTED.MarginAnalysis
		AND calc.MarginAggregate.[MarginId]		= INSERTED.[MarginId];

END