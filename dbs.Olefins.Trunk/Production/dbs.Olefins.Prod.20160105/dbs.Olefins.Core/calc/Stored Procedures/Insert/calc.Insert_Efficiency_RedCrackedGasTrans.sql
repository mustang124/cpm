﻿CREATE PROCEDURE [calc].[Insert_Efficiency_RedCrackedGasTrans]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Efficiency([FactorSetId], [Refnum], [CalDateKey], [EfficiencyId], [UnitId], [EfficiencyStandard])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			wCdu.[EfficiencyId],
			'RedCrackedGasTrans'	[UnitId],
			(eComp.[Coefficient] - eFeed.[Coefficient])
			* POWER(cp.[Stream_MTd], eComp.[Exponent])
			* fpl.[Component_kMT] / cUtil.[_UtilizationStream_Pcnt]
			* wCdu.[Coefficient] / 10.0
		FROM (
			SELECT
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey],
				SUM(c.[Component_kMT])	[Component_kMT]
			FROM @fpl								fpl
			INNER JOIN [calc].[CompositionStream]	c
				ON	c.[FactorSetId]	= fpl.[FactorSetId]
				AND	c.[Refnum]		= fpl.[Refnum]
				AND	c.[CalDateKey]	= fpl.[Plant_QtrDateKey]
				AND	c.[StreamId]	= 'ProdOther'
				AND	c.[ComponentId]	IN ('C2H4', 'C3H6')
			GROUP BY
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey]
			)							fpl
		INNER JOIN ante.UnitEfficiencyWWCdu					wCdu
			ON	wCdu.[FactorSetId]	= fpl.[FactorSetId]
		INNER JOIN [calc].[PeerGroupFeedClass]			fc
			ON	fc.[FactorSetId]		= fpl.[FactorSetId]
			AND	fc.[Refnum]				= fpl.[Refnum]
			AND	fc.[CalDateKey]			= fpl.[Plant_AnnDateKey]
		LEFT OUTER JOIN [super].[PeerGroupFeedClass]	sf
			ON	sf.[FactorSetId]	= fpl.[FactorSetId]
			AND	sf.[Refnum]			= fpl.[Refnum]
			AND	sf.[CalDateKey]		= fpl.[Plant_AnnDateKey]
		INNER JOIN [ante].[MapFeedClass]					map
			ON	map.[FactorSetId]	= fpl.[FactorSetId]
			AND	map.[PeerGroup]		= COALESCE(sf.[PeerGroup], fc.[PeerGroup])

		INNER JOIN ante.UnitEfficiencyFactors			eComp
			ON	eComp.[FactorSetId]		= fpl.[FactorSetId]
			AND	eComp.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	eComp.[UnitId]			= map.[CompUnitId]

		INNER JOIN ante.UnitEfficiencyFactors			eFeed
			ON	eFeed.[FactorSetId]		= fpl.[FactorSetId]
			AND	eFeed.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	eFeed.[UnitId]			= map.[UnitId]

		INNER JOIN [calc].[CapacityPlant]				cp
			ON	cp.[FactorSetId]	= fpl.[FactorSetId]
			AND	cp.[Refnum]			= fpl.[Refnum]
			AND	cp.[CalDateKey]		= fpl.[Plant_AnnDateKey]
			AND	cp.[StreamId]		= 'ProdOlefins'
		INNER JOIN [calc].[CapacityUtilization]			cUtil
			ON	cUtil.[FactorSetId]	= fpl.[FactorSetId]
			AND	cUtil.[Refnum]		= fpl.[Refnum]
			AND	cUtil.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	cUtil.[SchedId]		= 'P'
			AND	cUtil.[StreamId]	= 'ProdOlefins';
		
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;