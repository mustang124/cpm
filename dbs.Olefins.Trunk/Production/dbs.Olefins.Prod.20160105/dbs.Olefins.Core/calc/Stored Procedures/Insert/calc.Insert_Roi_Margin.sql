﻿CREATE PROCEDURE [calc].[Insert_Roi_Margin]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Roi(FactorSetId, Refnum, CalDateKey, CurrencyRpt, MarginAnalysis, MarginId, Roi, TaAdj_Roi, Loc_Roi, TaAdj_Loc_Roi)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			m.MarginAnalysis,
			m.[MarginId],
			m.Amount_Cur		/ rv.ReplacementValue		* 100.0		[Roi],
			m.TaAdj_Amount_Cur	/ rv.ReplacementValue		* 100.0		[TaAdj_Roi],
			m.Amount_Cur		/ rv.ReplacementLocation	* 100.0		[Loc_Roi],
			m.TaAdj_Amount_Cur	/ rv.ReplacementLocation	* 100.0		[TaAdj_Loc_Roi]
		FROM @fpl									fpl
		INNER JOIN calc.MarginAggregate				m
			ON	m.FactorSetId = fpl.FactorSetId
			AND	m.Refnum = fpl.Refnum
			AND	m.CalDateKey = fpl.Plant_QtrDateKey
			AND	m.CurrencyRpt = fpl.CurrencyRpt
			AND	m.MarginAnalysis = 'None'
			AND	m.[MarginId] IN ('MarginGross', 'MarginNetCash', 'PreTaxIncome')
		INNER JOIN calc.ReplacementValueAggregate	rv
			ON	rv.FactorSetId = fpl.FactorSetId
			AND	rv.Refnum = fpl.Refnum
			AND	rv.CalDateKey = fpl.Plant_QtrDateKey
			AND	rv.CurrencyRpt = fpl.CurrencyRpt
			AND	rv.ReplacementValueId = 'UscgReplacement'
			AND	rv.ReplacementLocation IS NOT NULL
		ORDER BY m.Amount_Cur;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END