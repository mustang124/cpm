﻿CREATE PROCEDURE [calc].[Insert_MaintExpenseMpsSwb]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[MaintExpenseMpsSwb]([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [AccountId], [Amount_Cur])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[CurrencyRpt],
			'MpsSwb',
			mpa.[Maint_Pcnt] * (mcaa.[MaintAvg_Cur] - SUM(melm.[Amount_Cur])) / 100.0
		FROM @fpl										fpl
		INNER JOIN [calc].[MaintCostAvgAggregate]		mcaa	WITH (NOEXPAND)
			ON	mcaa.[FactorSetId]	= fpl.[FactorSetId]
			AND	mcaa.[Refnum]		= fpl.[Refnum]
			AND	mcaa.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	mcaa.[AccountId]	= 'Maint'
		INNER JOIN [calc].[MaintExpenseLaborMatl]		melm
			ON	melm.[FactorSetId]	= fpl.[FactorSetId]
			AND	melm.[Refnum]		= fpl.[Refnum]
			AND	melm.[CalDateKey]	= fpl.[Plant_QtrDateKey]
		INNER JOIN [calc].[MaintPersAverage]			mpa
			ON	mpa.[FactorSetId]	= fpl.[FactorSetId]
			AND	mpa.[Refnum]		= fpl.[Refnum]
			AND	mpa.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	mpa.[PersId]		= 'MpsMaint'
		WHERE	fpl.[CalQtr]		= 4
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[CurrencyRpt],
			mcaa.[MaintAvg_Cur],
			mpa.[Maint_Pcnt];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;