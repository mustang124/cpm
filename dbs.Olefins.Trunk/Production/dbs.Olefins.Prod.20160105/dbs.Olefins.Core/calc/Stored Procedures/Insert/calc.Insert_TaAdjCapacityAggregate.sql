﻿CREATE PROCEDURE calc.Insert_TaAdjCapacityAggregate
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.TaAdjCapacityAggregate(
			FactorSetId,
			Refnum,
			CalDateKey,
			SchedId,
			StreamId,
			CurrencyRpt,

			Interval_Mnths,
			Interval_Years,
			DownTime_Hrs,

			Ann_TurnAroundCost_kCur,
			Ann_TurnAround_ManHrs,
			Ann_CostPerManHour_kCur,
			Ann_TurnAroundLoss_Days,
			Ann_TurnAroundLoss_Pcnt,
			Ann_FullPlantEquivalentDT_HrsYr,
			TaAdjExRateAnn_TurnAroundCost_kCur
			)
		SELECT
			r.FactorSetId,
			r.Refnum,
			r.CalDateKey,
			r.SchedId,
			r.StreamId,
			r.CurrencyRpt,

			SUM(r.Interval_Mnths	* r.CapacityCalc_Pcnt) / 100.0		[Interval_Mnths],
			SUM(r.Interval_Years	* r.CapacityCalc_Pcnt) / 100.0		[Interval_Years],
			SUM(r.DownTime_Hrs		* r.CapacityCalc_Pcnt) / 100.0		[DownTime_Hrs],

			SUM(r.Ann_TurnAroundCost_kCur)								[Ann_TurnAroundCost_kCur],
			SUM(r.Ann_TurnAround_ManHrs)								[Ann_TurnAround_ManHrs],
			CASE WHEN SUM(r.Ann_TurnAround_ManHrs) <> 0.0 THEN
				SUM(r.Ann_TurnAroundCost_kCur)
					/ SUM(r.Ann_TurnAround_ManHrs) * 1000.0	
					END													[Ann_CostPerManHour_kCur],
			
			SUM(r._Ann_TurnAroundLoss_Days)								[Ann_TurnAroundLoss_Days],
			SUM(r._Ann_TurnAroundLoss_Pcnt)								[Ann_TurnAroundLoss_Pcnt],
			SUM(r._Ann_FullPlantEquivalentDT_HrsYr)						[Ann_FullPlantEquivalentDT_HrsYr],
			SUM(r._TaAdjExRateAnn_TurnAroundCost_kCur)					[TaAdjExRateAnn_TurnAroundCost_kCur]
		FROM @fpl							tsq
		INNER JOIN calc.TaAdjCapacity		r
			ON	r.FactorSetId = tsq.FactorSetId
			AND	r.Refnum = tsq.Refnum
			AND	r.CalDateKey = tsq.Plant_QtrDateKey
		GROUP BY
			r.FactorSetId,
			r.Refnum,
			r.CalDateKey,
			r.SchedId,
			r.StreamId,
			r.CurrencyRpt
		HAVING	(SUM(r.Interval_Mnths	* r.CapacityCalc_Pcnt) / 100.0	IS NOT NULL
			OR	SUM(r.Interval_Years	* r.CapacityCalc_Pcnt) / 100.0	IS NOT NULL
			OR	SUM(r.DownTime_Hrs		* r.CapacityCalc_Pcnt) / 100.0	IS NOT NULL
			OR	SUM(r.Ann_TurnAroundCost_kCur)							IS NOT NULL
			OR	SUM(r.Ann_TurnAround_ManHrs)							IS NOT NULL
			OR	SUM(r.Ann_TurnAroundCost_kCur)
				/ SUM(r.Ann_TurnAround_ManHrs) * 1000.0					IS NOT NULL);

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;