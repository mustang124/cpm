﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_FracFeed]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO calc.StandardEnergy(FactorSetId, Refnum, CalDateKey, SimModelId, StandardEnergyId, StandardEnergyDetail, StandardEnergy)
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			m.ModelId,
			k.StandardEnergyId,
			k.StandardEnergyId,
			k.Value * f.FeedRate_kBsd
		FROM @fpl												tsq
		INNER JOIN fact.FacilitiesFeedFrac						f
			ON	f.Refnum = tsq.Refnum
			AND	f.CalDateKey = tsq.Plant_QtrDateKey
		INNER JOIN ante.StandardEnergyCoefficients				k
			ON	k.FactorSetId = tsq.FactorSetId
			AND k.StandardEnergyId = f.FacilityId
		INNER JOIN dim.SimModel_LookUp							m
			ON	m.ModelId	<>	'Plant'
		WHERE	f.FeedRate_kBsd <> 0.0
			AND	tsq.FactorSet_AnnDateKey	<= 20130000;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;