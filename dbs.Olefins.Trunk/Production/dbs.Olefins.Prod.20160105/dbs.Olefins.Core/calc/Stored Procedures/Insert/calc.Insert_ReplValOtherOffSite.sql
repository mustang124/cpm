﻿CREATE PROCEDURE [calc].[Insert_ReplValOtherOffSite]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplValOtherOffSite(FactorSetId, Refnum, CalDateKey, CurrencyRpt, OtherOffSite_IBSL)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,

			(rfx.Capability_ISBL + ISNULL(sfa._SuppFeedAdjustment, 0.0)) * 
			CASE
				WHEN rpc.PyroEthyleneCap_kMT < 200.0	THEN 0.45
				WHEN rpc.PyroEthyleneCap_kMT > 600.0	THEN 0.35
				ELSE										0.5 - rpc.PyroEthyleneCap_kMT * 0.00025
				END * calc.MinValue(1.1, calc.MaxValue(0.7, fcc.Replacement_Wt + 0.20)) + ISNULL(ofs.Value, 0.0)

		FROM @fpl										fpl
		INNER JOIN	calc.ReplValPyroFlexibility			rfx
			ON	rfx.FactorSetId = fpl.FactorSetId
			AND	rfx.Refnum = fpl.Refnum
			AND	rfx.CalDateKey = fpl.Plant_QtrDateKey
		INNER JOIN inter.ReplValPyroCapacity			rpc
			ON	rpc.FactorSetId = fpl.FactorSetId
			AND	rpc.Refnum = fpl.Refnum
			AND	rpc.CalDateKey = fpl.Plant_QtrDateKey
			AND	rpc.CurrencyRpt = fpl.CurrencyRpt
		INNER JOIN calc.FacilitiesCountComplexity		fcc
			ON	fcc.FactorSetId = fpl.FactorSetId
			AND	fcc.Refnum = fpl.Refnum
			AND	fcc.CalDateKey = fpl.Plant_QtrDateKey
			AND	fcc.FacilityId = 'TotUnitCnt'
		LEFT OUTER JOIN calc.ReplValSuppFeedAdjustment	sfa
			ON	sfa.FactorSetId = fpl.FactorSetId
			AND	sfa.Refnum = fpl.Refnum
			AND	sfa.CalDateKey = fpl.Plant_QtrDateKey
			AND	sfa.CurrencyRpt = fpl.CurrencyRpt
		LEFT OUTER JOIN super.ReplValOtherOffSite		ofs
			ON	ofs.Refnum = fpl.Refnum
			AND	ofs.CalDateKey = fpl.Plant_QtrDateKey
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;