﻿CREATE VIEW [calc].[PersMaintTaAggregate]
WITH SCHEMABINDING
AS
--	Task 141: Implement Tri-Tables in calculations
--SELECT
--	f.FactorSetId,
--	f.Refnum,
--	f.CalDateKey,
--	l.PersIdSec,
--	SUM(f._InflAdjAnn_Tot_Hrs)	[InflAdjAnn_Tot_Hrs]
--FROM calc.PersMaintTa	f
--INNER JOIN dim.PersLu	l
--	ON	l.PersId = f.PersId
--GROUP BY
--	f.FactorSetId,
--	f.Refnum,
--	f.CalDateKey,
--	l.PersIdSec
SELECT
	f.FactorSetId,
	f.Refnum,
	f.CalDateKey,
	l.PersIdSec,
	SUM(f._InflAdjAnn_Tot_Hrs)	[InflAdjAnn_Tot_Hrs]
FROM calc.PersMaintTa	f
INNER JOIN dim.Pers_LookUp	l
	ON	l.PersId = f.PersId
GROUP BY
	f.FactorSetId,
	f.Refnum,
	f.CalDateKey,
	l.PersIdSec;
