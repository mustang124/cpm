﻿CREATE VIEW calc.EdcAggregate
WITH SCHEMABINDING
AS
SELECT
	e.FactorSetId,
	e.Refnum,
	e.CalDateKey,
	b.EdcId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + e.kEdc
		WHEN '-' THEN - e.kEdc
		END)											[kEdc],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + e.kEdc
		WHEN '-' THEN - e.kEdc
		END) * u._UtilizationStream_Pcnt / 100.0		[kUEdc],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + e.kEdc
		WHEN '-' THEN - e.kEdc
		END) * u._TaAdj_UtilizationStream_Pcnt / 100.0	[TaAdj_kUEDC]
FROM calc.Edc									e
INNER JOIN dim.Edc_Bridge						b
	ON	b.FactorSetId = e.FactorSetId
	AND	b.DescendantId = e.EdcId
LEFT OUTER JOIN calc.CapacityUtilization		u
	ON	u.Refnum = e.Refnum
	AND	u.CalDateKey = e.CalDateKey
	AND	u.FactorSetId = e.FactorSetId
	AND u.SchedId = 'P'
	AND	u.StreamId = 'ProdOlefins'
WHERE e.kEdc <> 0.0
GROUP BY
	e.FactorSetId,
	e.Refnum,
	e.CalDateKey,
	b.EdcId,
	u._UtilizationStream_Pcnt,
	u._TaAdj_UtilizationStream_Pcnt;