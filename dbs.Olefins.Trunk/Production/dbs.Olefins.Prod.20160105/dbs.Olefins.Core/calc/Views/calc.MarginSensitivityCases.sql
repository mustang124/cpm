﻿CREATE VIEW [calc].[MarginSensitivityCases]
WITH SCHEMABINDING
AS
SELECT
	ms.[FactorSetId],
	ms.[Refnum],
	ms.[CalDateKey],
	ms.[CurrencyRpt],
	ms.[MarginAnalysis],
	CASE ms.[MarginId]
		WHEN 'MarginGross'	THEN 'MarginNetCash'
		WHEN 'PlantFeed'	THEN 'ProductionCost'
		WHEN 'NonCash'		THEN 'TotRefExp'
		END																	[MarginId],
	ms.[Amount_Cur]						+ ca.[Amount_Cur]					[Amount_Cur],
	ms.[Amount_SensEDC]					+ ca.[Amount_SensEDC]				[Amount_SensEDC],
	ms.[Amount_SensProd_MT]				+ ca.[Amount_SensProd_MT]			[Amount_SensProd_MT],
	ms.[Amount_SensProd_Cents]			+ ca.[Amount_SensProd_Cents]		[Amount_SensProd_Cents],
	ms.[TaAdj_Amount_Cur]				+ ca.[TaAdj_Amount_Cur]				[TaAdj_Amount_Cur],
	ms.[TAAdj_Amount_SensEDC]			+ ca.[TAAdj_Amount_SensEDC]			[TAAdj_Amount_SensEDC],
	ms.[TaAdj_Amount_SensProd_MT]		+ ca.[TaAdj_Amount_SensProd_MT]		[TaAdj_Amount_SensProd_MT],
	ms.[TAAdj_Amount_SensProd_Cents]	+ ca.[TAAdj_Amount_SensProd_Cents]	[TAAdj_Amount_SensProd_Cents]
FROM [calc].[MarginSensitivity]						ms
INNER JOIN [calc].[MarginSensitivityCashAggregate]	ca
	ON	ca.[FactorSetId]	= ms.[FactorSetId]
	AND	ca.[Refnum]			= ms.[Refnum]
	AND	ca.[CalDateKey]		= ms.[CalDateKey]
	AND	ca.[CurrencyRpt]	= ms.[CurrencyRpt]
	AND	ca.[MarginAnalysis]	= ms.[MarginAnalysis]
WHERE	ms.[MarginId] IN ('MarginGross', 'PlantFeed', 'NonCash');
