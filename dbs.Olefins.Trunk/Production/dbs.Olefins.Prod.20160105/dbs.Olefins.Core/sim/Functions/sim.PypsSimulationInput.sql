﻿CREATE FUNCTION [sim].[PypsSimulationInput]
(
	@FactorSetId	VARCHAR(12),
	@Refnum			VARCHAR(25)
)
RETURNS TABLE
AS RETURN
(
SELECT DISTINCT
	[FactorSetId] = @FactorSetId,
	[q].[Refnum],
	[q].[StreamId],
	[q].[StreamDescription],
	[p].[OpCondId],

	[ConversionID2]	=		COALESCE([c].[MaxComponent_Id],		[f].[FeedConvId],		0),

	[1]		= COALESCE([c].[1], 0),
	[2]		= COALESCE([c].[2], 0),
	[3]		= COALESCE([c].[3], 0),
	[4]		= COALESCE([c].[4], 0),
	[5]		= COALESCE([c].[5], 0),
	[6]		= COALESCE([c].[6], 0),
	[7]		= COALESCE([c].[7], 0),
	[8]		= COALESCE([c].[8], 0),
	[9]		= COALESCE([c].[9], 0),
	[10]	= COALESCE([c].[10], 0),
	[15]	= COALESCE([c].[15], 0),
	[16]	= COALESCE([c].[16], 0),

	[RecycleId]		= COALESCE([r].[RecycleId], 0),
	[PypsRecycleId] = CASE COALESCE([r].[RecycleId], 0)
				WHEN 0	THEN 0
				WHEN 1	THEN 1
				WHEN 2	THEN 2
				ELSE 2
			END,

	[f].[FeedType],
	[l].[HasPiano],

	[PypsFeedType] = [sim].[PypsFeedTypePiano](
		[f].[FeedType],
		COALESCE([l].[HasPiano], 0)),

	[PypsLiquidType] = [sim].[PypsLiquidType](
		COALESCE([l].[HasPiano], 0),
		COALESCE([p].[Density_SG], [k].[Density_SG], [d].[Density_SG], [f].[Density_SG], 0.0),
		COALESCE([d].[D100], [d].[D095]),
		[q].[StreamId]),

	[D000]	= COALESCE([d].[D000], [d].[D005], 0),
	[D010]	= COALESCE([d].[D010], 0),
	[D030]	= COALESCE([d].[D030], 0),
	[D050]	= COALESCE([d].[D050], 0),
	[D070]	= COALESCE([d].[D070], 0),
	[D090]	= COALESCE([d].[D090], 0),
	[D100]	= COALESCE([d].[D100], [d].[D095], 0),

	[P]		= COALESCE([l].[P],	[f].[P] * 100.0, 0),
	[I]		= COALESCE([l].[I],	[f].[I] * 100.0, 0),
	[A]		= COALESCE([l].[A],	[f].[A] * 100.0, 0),
	[N]		= COALESCE([l].[N],	[f].[N] * 100.0, 0),
	[O]		= COALESCE([l].[O],	[f].[O] * 100.0, 0),

	[ParaffinINRatio]	= COALESCE([l].[ParaffinINRatio], 0),

	[H2]	= COALESCE([l].[H2], 0),

	[CoilOutletPressure_kgcm2]	= ROUND(COALESCE([p].[CoilOutletPressure_kgcm2],
											[$(DbGlobal)].[dbo].[UnitsConv]([k].[CoilOutletPressure_Psia], 'PSIA', 'KGM2A'),	[f].[CoilOutletPressure_kgcm2], 0), 3),
	[CoilOutletTemp_C]			= COALESCE([p].[CoilOutletTemp_C],			[k].[CoilOutletTemp_C],													0),

	[CoilInletPressure_kgcm2]	= ROUND(COALESCE([p].[CoilInletPressure_kgcm2],
											[$(DbGlobal)].[dbo].[UnitsConv]([k].[CoilInletPressure_Psia], 'PSIA', 'KGM2A'),											0), 3),
	[CoilInletTemp_C]			= COALESCE([p].[CoilInletTemp_C],			[k].[CoilInletTemp_C],													0),

	[RadiantWallTemp_C]			= COALESCE([p].[RadiantWallTemp_C],			[k].[RadiantWallTemp_C],												0),
	[EthyleneYield_WtPcnt]		= COALESCE([p].[EthyleneYield_WtPcnt],		[k].[EthyleneYield_WtPcnt],												0),
	[PropyleneEthylene_Ratio]	= COALESCE([p].[PropyleneEthylene_Ratio],	[k].[PropyleneEthylene_Ratio],											0),
	[PropyleneMethane_Ratio]	= COALESCE([p].[PropyleneMethane_Ratio],	[k].[PropyleneMethane_Ratio],			[f].[PropyleneMethane_Ratio],	0),
	[FeedConv_WtPcnt]			= COALESCE([p].[FeedConv_WtPcnt],			[k].[FeedConv_WtPcnt],					[f].[FeedConv_Pcnt],			0) / 100.0,

	[SeverityConversion]		= COALESCE([p].[SeverityConversion],																				0),
	[SeverityValue]				= COALESCE([p].[SeverityValue],																						0),

	[SteamHydrocarbon_Ratio]	= COALESCE([p].[SteamHydrocarbon_Ratio],	[k].[SteamHydrocarbon_Ratio],			[f].[SteamHydrocarbon_Ratio],	0),
	[Density_SG]				= COALESCE([p].[Density_SG],				[k].[Density_SG],	[d].[Density_SG],	[f].[Density_SG],				0),
	[DistMethodId]				= COALESCE([p].[DistMethodId],																						0),
	[FurnConstruction_Year]		= COALESCE([p].[FurnConstruction_Year],		[k].[FurnConstruction_Year],											0),
	[ResidenceTime_s]			= COALESCE([p].[ResidenceTime_s],			[k].[ResidenceTime_s],													0),
	[FlowRate_KgHr]				= COALESCE([p].[FlowRate_KgHr],				[k].[FlowRate_KgHr],													0)
	
FROM
	[fact].[Quantity]								[q]

INNER JOIN
	[ante].[SimCrackingParameters]					[p]
		ON	[p].[FactorSetId]		= @FactorSetId

INNER JOIN
	[ante].[SimFurnaceConfigPyps]					[f]
		ON	[f].[FactorSetId]		= @FactorSetId
		AND	[f].[StreamId]			= [q].[StreamId]

LEFT OUTER JOIN
	[fact].[FeedStockCrackingParameters]			[k]
		ON	[k].[Refnum]			= [q].[Refnum]
		AND	[k].[StreamId]			= [q].[StreamId]
		AND	[k].[StreamDescription]	= [q].[StreamDescription]

LEFT OUTER JOIN
	[fact].[PlantRecycles](@FactorSetId, @Refnum)	[r]
		ON	[r].[FactorSetId]		= @FactorSetId
		AND	[r].[Refnum]			= @Refnum

LEFT OUTER JOIN
	[fact].[CompositionLiquid]						[l]
		ON	[l].[Refnum]			= [q].[Refnum]
		AND	[l].[StreamId]			= [q].[StreamId]
		AND	[l].[StreamDescription]	= [q].[StreamDescription]

LEFT OUTER JOIN
	[sim].[PypsComposition](@Refnum)				[c]
		ON	[c].[StreamId]			= [q].[StreamId]
		AND	[c].[StreamDescription]	= [q].[StreamDescription]

LEFT OUTER JOIN
	[sim].[Distillation](@FactorSetId, @Refnum)		[d]
		ON	[d].[StreamId]			= [q].[StreamId]
		AND	[d].[StreamDescription]	= [q].[StreamDescription]

WHERE	[q].[Refnum]	= @Refnum
	AND	([c].[Refnum] IS NOT NULL OR [l].[Refnum] IS NOT NULL OR [d].[Refnum] IS NOT NULL)
);