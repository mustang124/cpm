﻿CREATE FUNCTION [sim].[PypsComposition]
(
	@Refnum	VARCHAR(25) = NULL
)
RETURNS @ReturnTable TABLE
(
	[FactorSetId]			VARCHAR(12)			NOT	NULL,
	[Refnum]				VARCHAR(25)			NOT	NULL,
	[CalDateKey]			INT					NOT	NULL,
	[StreamId]				VARCHAR(42)			NOT	NULL,
	[StreamDescription]		NVARCHAR(256)		NOT	NULL,
	[MaxComponent_Id]		INT					NOT	NULL,
	[MaxComponent_WtPcnt]	FLOAT				NOT	NULL,

	[1]						FLOAT					NULL	CHECK([1]	>= 0.0	AND [1]		<= 100.0),
	[2]						FLOAT					NULL	CHECK([2]	>= 0.0	AND [2]		<= 100.0),
	[3]						FLOAT					NULL	CHECK([3]	>= 0.0	AND [3]		<= 100.0),
	[4]						FLOAT					NULL	CHECK([4]	>= 0.0	AND [4]		<= 100.0),
	[5]						FLOAT					NULL	CHECK([5]	>= 0.0	AND [5]		<= 100.0),
	[6]						FLOAT					NULL	CHECK([6]	>= 0.0	AND [6]		<= 100.0),
	[7]						FLOAT					NULL	CHECK([7]	>= 0.0	AND [7]		<= 100.0),
	[8]						FLOAT					NULL	CHECK([8]	>= 0.0	AND [8]		<= 100.0),
	[9]						FLOAT					NULL	CHECK([9]	>= 0.0	AND [9]		<= 100.0),
	[10]					FLOAT					NULL	CHECK([10]	>= 0.0	AND [10]	<= 100.0),
	[15]					FLOAT					NULL	CHECK([15]	>= 0.0	AND [15]	<= 100.0),
	[16]					FLOAT					NULL	CHECK([16]	>= 0.0	AND [16]	<= 100.0),
	PRIMARY KEY CLUSTERED([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [StreamId] ASC, [StreamDescription] ASC)
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @Round INT = 3;

	DECLARE @Composition TABLE
	(
		[FactorSetId]			VARCHAR(12)			NOT	NULL,
		[Refnum]				VARCHAR(25)			NOT	NULL,
		[CalDateKey]			INT					NOT	NULL,
		[StreamId]				VARCHAR(42)			NOT	NULL,
		[StreamDescription]		NVARCHAR(256)		NOT	NULL,
		[ComponentId]			VARCHAR (42)		NOT	NULL,
		[Component_WtPcnt]		FLOAT				NOT	NULL,
		[PypsFieldNumber_Std]	INT					NOT	NULL,
		PRIMARY KEY CLUSTERED([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [StreamId] ASC, [StreamDescription] ASC, [ComponentId] ASC)
	);

	INSERT INTO @Composition
	(
		[FactorSetId],
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[ComponentId],
		[Component_WtPcnt],
		[PypsFieldNumber_Std]
	)
	SELECT
		[b].[FactorSetId],
		[c].[Refnum],
		[c].[CalDateKey],
		[c].[StreamId],
		[c].[StreamDescription],
		[c].[ComponentId],
		ROUND([c].[Component_WtPcnt] / 100.0, @Round),
		[x].[PypsFieldNumber_Std]
	FROM
		[dim].[Stream_Bridge]			[b]
	INNER JOIN
		[fact].[CompositionQuantity]	[c]
			ON	[c].[StreamId]	= [b].[DescendantId]
	INNER JOIN (VALUES
		('CH4',		10),	--	Methane		(10)
		('C2H6',	 1),	--	Ethane		( 1)
		('C2H4',	16),	--	Ethylene	(16)
		('C3H8',	 2),	--	Propane		( 2)
		('C3H6',	 7),	--	Propylene	( 7)
		('NBUTA',	 3),	--	n. Butane	( 3)
		('IBUTA',	 4),	--	i. Butane	( 4)
		('IB',		 9),	--	i-Butene	( 9) / 1-Butene		( 8)
		('B1',		 8),	--	1-Butene	( 8)
		('C4H6',	 9),	--	i-Butene	( 9) / 1-Butene		( 8)
		('NC5',		 5),	--	n. Pentane	( 5)
		('IC5',		 6),	--	i. Pentane	( 6) / n. Pentane	( 5)
		('NC6',		15),	--	Hexanes		(15) / NULL
		('C6ISO',	15),	--	Hexanes		(15) / NULL
		('C7H16',	15),	--	Hexanes		(15) / n. Pentane	( 5)
		('C8H18',	15),	--	Hexanes		(15) / NULL
		('H2',		10)		--	Methane		(10)
		) [x]([ComponentId], [PypsFieldNumber_Std])
			ON	[x].[ComponentId] = [c].[ComponentId]
	WHERE	[b].[FactorSetId]	= (SELECT TOP 1 [b].[FactorSetId] FROM [dim].[Stream_Bridge] [b] ORDER BY [b].[FactorSetId] DESC)
		AND	[b].[StreamId]		IN ('Light', 'Recycle')
		AND	[c].[Refnum]		= @Refnum;

	INSERT INTO @ReturnTable
	(
		[FactorSetId],
		[Refnum],
		[CalDateKey],
		[StreamId],
		[StreamDescription],
		[MaxComponent_Id],
		[MaxComponent_WtPcnt],
		[1],
		[2],
		[3],
		[4],
		[5],
		[6],
		[7],
		[8],
		[9],
		[10],
		[15],
		[16]
	)
	SELECT
		[p].[FactorSetId],
		[p].[Refnum],
		[p].[CalDateKey],
		[p].[StreamId],
		[p].[StreamDescription],
		[p].[MaxComponent_Id],
		[p].[MaxComponent_WtPcnt],
		[p].[1],
		[p].[2],
		[p].[3],
		[p].[4],
		[p].[5],
		[p].[6],
		[p].[7],
		[p].[8],
		[p].[9],
		[p].[10],
		[p].[15],
		[p].[16]
	FROM (
		SELECT
			[t].[FactorSetId],
			[t].[Refnum],
			[t].[CalDateKey],
			[t].[StreamId],
			[t].[StreamDescription],
			[t].[Component_WtPcnt],
			[t].[PypsFieldNumber],
			[MaxComponent_Id]		= MAX(CASE WHEN [t].[IsMaxComponent] = 1 THEN [t].[PypsFieldNumber] END) OVER(PARTITION BY [t].[FactorSetId], [t].[Refnum], [t].[CalDateKey], [t].[StreamId], [t].[StreamDescription]),
			[MaxComponent_WtPcnt]	= MAX(CASE WHEN [t].[IsMaxComponent] = 1 THEN [t].[Component_WtPcnt] END) OVER(PARTITION BY [t].[FactorSetId], [t].[Refnum], [t].[CalDateKey], [t].[StreamId], [t].[StreamDescription])
		FROM (
			SELECT
				[c].[FactorSetId],
				[c].[Refnum],
				[c].[CalDateKey],
				[c].[StreamId],
				[c].[StreamDescription],
				[c].[ComponentId],
				[c].[Component_WtPcnt],

				--[c].[PypsFieldNumber_Std],
				--[x].[PypsFieldNumber_Super],
				--[y].[PypsFieldNumber_Ethane],
				--[z].[PypsFieldNumber_IsoButylene],
				--[Items] = SUM(CASE WHEN COALESCE([x].[PypsFieldNumber_Super], [y].[PypsFieldNumber_Ethane], [c].[PypsFieldNumber_Std]) = 1
				--					AND	[c].[StreamId] IN ('LPG', 'Butane', 'ButRec')
				--					THEN [c].[Component_WtPcnt] ELSE 0.0 END)
				--					OVER(PARTITION BY [c].[StreamId], [c].[StreamDescription]),

				[IsMaxComponent] = CASE
					WHEN [c].[Component_WtPcnt] = MAX([c].[Component_WtPcnt]) OVER(PARTITION BY [c].[FactorSetId], [c].[Refnum], [c].[CalDateKey], [c].[StreamId], [c].[StreamDescription])
					THEN 1
					END,

				[PypsFieldNumber] = CASE WHEN [c].[StreamId] IN ('LPG', 'Butane', 'ButRec')
					THEN
						CASE WHEN 
							SUM(CASE WHEN COALESCE([x].[PypsFieldNumber_Super], [y].[PypsFieldNumber_Ethane], [c].[PypsFieldNumber_Std]) = 1
									AND	[c].[StreamId] IN ('LPG', 'Butane', 'ButRec')
									THEN [c].[Component_WtPcnt] ELSE 0.0 END)
									OVER(PARTITION BY [c].[StreamId], [c].[StreamDescription]) = 0
							OR [p].[Items]	> 9
						THEN
							COALESCE([lpg].[PypsFieldNumber_Lpg], [z].[PypsFieldNumber_IsoButylene], [y].[PypsFieldNumber_Ethane], [x].[PypsFieldNumber_Super], [c].[PypsFieldNumber_Std])
						ELSE
							COALESCE([lpg].[PypsFieldNumber_Lpg], [y].[PypsFieldNumber_Ethane], [x].[PypsFieldNumber_Super], [c].[PypsFieldNumber_Std])
						END
					ELSE
						COALESCE([lpg].[PypsFieldNumber_Lpg], [y].[PypsFieldNumber_Ethane], [x].[PypsFieldNumber_Super], [c].[PypsFieldNumber_Std])
					END

			FROM
				@Composition		[c]

			CROSS APPLY (
				SELECT
					[Items] = COUNT(DISTINCT [i].[PypsFieldNumber_Std])
				FROM
					@Composition	[i]
				WHERE	[i].[FactorSetId]		= [c].[FactorSetId]
					AND	[i].[Refnum]			= [c].[Refnum]
					AND	[i].[CalDateKey]		= [c].[CalDateKey]
					AND	[i].[StreamId]			= [c].[StreamId]
					AND	[i].[StreamDescription]	= [c].[StreamDescription]
				) [p]

			--	Move Components
			LEFT OUTER JOIN (VALUES
				('IB',		 8),	--	i-Butene	( 9) / 1-Butene		( 8)
				('C4H6',	 8),	--	i-Butene	( 9) / 1-Butene		( 8)

				('IC5',		 5),	--	i. Pentane	( 6) / n. Pentane	( 5)
				('NC6',		 0),	--	Hexanes		(15) / NULL
				('C6ISO',	 0),	--	Hexanes		(15) / NULL
				('C7H16',	 5),	--	Hexanes		(15) / n. Pentane	( 5)
				('C8H18',	 0)		--	Hexanes		(15) / NULL
				) [x]([ComponentId], [PypsFieldNumber_Super])
					ON	[x].[ComponentId]	= [c].[ComponentId]
					AND	[c].[StreamId]		IN ('Ethane', 'EPMix', 'Propane', 'FeedLtOther', 'EthRec', 'ProRec')
					AND	[p].[Items]			> 9

			--	Move Component to Ethane
			LEFT OUTER JOIN (VALUES
				('CH4',		 1),
				('H2',		 1),

				('NC6',		 0),
				('C6ISO',	 0),
				('C7H16',	 0),
				('C8H18',	 0),
				('C2H4',	 1)
				) [y]([ComponentId], [PypsFieldNumber_Ethane])
					ON	[y].[ComponentId]	= [c].[ComponentId]
					AND	[c].[StreamId]		IN ('LPG', 'Butane', 'ButRec')
					AND	[p].[Items]			> 9
	
			--	LPG - Move C3H8 to N-butane, if C2H$ < 0.05
			LEFT OUTER JOIN (VALUES
				('C3H8',	 3)
				) [lpg]([ComponentId], [PypsFieldNumber_Lpg])
					ON	[lpg].[ComponentId]		= [c].[ComponentId]
					AND	[c].[StreamId]			= 'LPG'
					AND	[c].[Component_WtPcnt]	< 0.05

			--	Move Component to IsoButylene
			LEFT OUTER JOIN (VALUES
				('IB',		 8),
				('C4H6',	 8)
				) [z]([ComponentId], [PypsFieldNumber_IsoButylene])
					ON	[z].[ComponentId]	= [c].[ComponentId]
					AND	[c].[StreamId]		IN ('LPG', 'Butane', 'ButRec')
			) [t]
		) [u]
		PIVOT(
			SUM([u].[Component_WtPcnt]) FOR [u].[PypsFieldNumber] IN (
				[1], [2], [3], [4], [5], [6], [7], [8], [9], [10],
				[15], [16]
				)
		) [p];

	RETURN;

END;
