﻿CREATE TABLE [sim].[Supersede] (
    [DataSetId]         VARCHAR (6)        NOT NULL,
    [SimModelId]        VARCHAR (12)       NOT NULL,
    [FieldId]           VARCHAR (12)       NOT NULL,
    [Value]             REAL               NOT NULL,
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [OpCondId]          VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [Active]            BIT                CONSTRAINT [DF_Supersede_Active] DEFAULT ((0)) NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Supersede_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_Supersede_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_Supersede_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_Supersede_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [CL_Supersede_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_Supersede_DataSetId] CHECK ([DataSetId]='Output' OR [DataSetId]='Input'),
    CONSTRAINT [FK_Supersede_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Supersede_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Supersede_SimFieldDefinitions_LookUp] FOREIGN KEY ([DataSetId], [SimModelId], [FieldId]) REFERENCES [dim].[SimFieldDefinitions_LookUp] ([DataSetId], [ModelId], [FieldId]),
    CONSTRAINT [FK_Supersede_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_Supersede_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_Supersede_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [UK_Supersede] UNIQUE NONCLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [DataSetId] ASC, [SimModelId] ASC, [FieldId] ASC, [StreamId] ASC, [StreamDescription] ASC, [OpCondId] ASC, [CalDateKey] ASC)
);


GO

CREATE TRIGGER [sim].[t_Supersede_u]
	ON  [sim].[Supersede]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [sim].[Supersede]
	SET	tsModified			= SYSDATETIMEOFFSET(),
		tsModifiedHost		= HOST_NAME(),
		tsModifiedUser		= SUSER_SNAME(),
		tsModifiedApp		= APP_NAME()
	FROM INSERTED
	WHERE	[sim].[Supersede].DataSetId			= INSERTED.DataSetId
		AND [sim].[Supersede].SimModelId		= INSERTED.SimModelId
		AND [sim].[Supersede].FieldId			= INSERTED.FieldId;

END