﻿CREATE TABLE [sim].[SpslIn] (
    [QueueId]           BIGINT             NOT NULL,
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [SimModelId]        VARCHAR (12)       NOT NULL,
    [OpCondId]          VARCHAR (12)       NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [RecycleId]         INT                NOT NULL,
    [DataSetId]         VARCHAR (6)        CONSTRAINT [DF_SpslIn_DataSetId] DEFAULT ('Input') NOT NULL,
    [FieldId]           VARCHAR (12)       NOT NULL,
    [Value]             REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_SpslIn_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_SpslIn_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_SpslIn_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_SpslIn_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SpslIn] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [RecycleId] ASC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC, [StreamDescription] ASC, [FieldId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_SpslIn_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_SpslIn_DataSetId] CHECK ([DataSetId]='Input'),
    CONSTRAINT [CR_SpslIn_Value] CHECK ([Value]>=(0.0)),
    CONSTRAINT [FK_SpslIn_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_SpslIn_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_SpslIn_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_SpslIn_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_SpslIn_SimQueue] FOREIGN KEY ([QueueId]) REFERENCES [q].[Simulation] ([QueueId]),
    CONSTRAINT [FK_SpslIn_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [sim].[t_SpslIn_u]
	ON [sim].[SpslIn]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [sim].[SpslIn]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[sim].[SpslIn].FactorSetId			= INSERTED.FactorSetId
		AND	[sim].[SpslIn].Refnum				= INSERTED.Refnum
		AND	[sim].[SpslIn].CalDateKey			= INSERTED.CalDateKey
		AND	[sim].[SpslIn].SimModelId			= INSERTED.SimModelId
		AND	[sim].[SpslIn].OpCondId				= INSERTED.OpCondId
		AND	[sim].[SpslIn].StreamId				= INSERTED.StreamId
		AND	[sim].[SpslIn].StreamDescription	= INSERTED.StreamDescription
		AND	[sim].[SpslIn].RecycleId			= INSERTED.RecycleId
		AND	[sim].[SpslIn].FieldId				= INSERTED.FieldId;

END;