﻿CREATE PROCEDURE [sim].[Insert_Temp_EnergyConsumptionStream]
(
	@FactorSetId		VARCHAR(12),
	@Refnum				VARCHAR(25),
	@ModelId			VARCHAR(12),
	@OpCondId			VARCHAR(12),
	@StreamId			VARCHAR(42),
	@StreamDescription	VARCHAR(256),
	@RecycleId			INT,
	@ErrorId			VARCHAR(12),
	@Energy_kCalKg		REAL
)
AS
BEGIN

	INSERT INTO [sim].[Temp_EnergyConsumptionStream]
	(
		[FactorSetId],
		[Refnum],
		[ModelId],
		[OpCondId],
		[StreamId],
		[StreamDescription],
		[RecycleId],
		[ErrorId],
		[Energy_kCalKg]
	)
	VALUES
	(
		@FactorSetId,
		@Refnum,
		@ModelId,
		@OpCondId,
		@StreamId,
		@StreamDescription,
		@RecycleId,
		@ErrorId,
		@Energy_kCalKg
	);

	RETURN 0;

END;