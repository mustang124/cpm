﻿CREATE VIEW [fact].[InflationTagging]
WITH SCHEMABINDING
AS
SELECT
	[f].[Refnum],
	[f].[StudyYear],
	[r].[ForexRate],

		[PreviousRefnum]		= [h].[Refnum],
		[PreviousStudyYear]		= [h].[StudyYear],
		[PreviousForexRate]		= [h].[ForexRate],

	[f].[CurrencyFcn],
	[f].[CurrencyRpt],
	[f].[CountryId],
		[OpExCountryName]		= [x].[CountryName]

FROM
	[fact].[TSort]				[f]
LEFT OUTER JOIN
	(VALUES
		('ARG', 'ARGENTINA'),
		('AUS', 'AUSTRALIA'),
		('AUT', 'AUSTRIA'),
		('BEL', 'BELGIUM'),
		('BRA', 'BRAZIL'),
		('CAN', 'CANADA'),
		('CHL', 'CHILE'),
		('COL', 'COLOMBIA'),
		('CZE', 'CZECH REP'),
		('FIN', 'FINLAND'),
		('FRA', 'FRANCE'),
		('DEU', 'GERMANY'),
		('HUN', 'HUNGARY'),
		('IND', 'INDIA'),
		('ISR', 'ISRAEL'),
		('ITA', 'ITALY'),
		('IDN', 'INDONESIA'),
		('JPN', 'JAPAN'),
		('KOR', 'SOUTH KOREA'),
		('KWT', 'KUWAIT'),
		('MYS', 'MALAYSIA'),
		('MEX', 'MEXICO'),
		('NLD', 'NETHERLANDS'),
		('NOR', 'NORWAY'),
		('POL', 'POLAND'),
		('PRT', 'PORTUGAL'),
		('CHN', 'PRC CHINA'),
		('QAT', 'QATAR'),
		('ROU', 'ROMANIA'),
		('RUS', 'RUSSIA'),
		('SAU', 'SAUDI ARABIA'),
		('SGP', 'SINGAPORE'),
		('SVK', 'SLOVAKIA'),
		('ZAF', 'SOUTH AFRICA'),
		('ESP', 'SPAIN'),
		('SWE', 'SWEDEN'),
		('TWN', 'TAIWAN'),
		('THA', 'THAILAND'),
		('TUR', 'TURKEY'),
		('ARE', 'UAE'),
		('GBR', 'UNITED KINGDOM'),
		('USA', 'USA')
		) 	[x]([CountryId], [CountryName])
	ON	[x].[CountryId]	= [f].[CountryId]
LEFT OUTER JOIN
	[fact].[ForexRate]		[r]
		ON	[r].[Refnum]	= [f].[Refnum]
OUTER APPLY (
	SELECT TOP 1
		[s].[Refnum],
		[s].[StudyYear],
		[x].[ForexRate]
	FROM
		[fact].[TSort]		[s]
	LEFT OUTER JOIN
		[fact].[ForexRate]	[x]
			ON	[x].[Refnum]	= [s].[Refnum]
	WHERE
			[s].[AssetId]	=	[f].[AssetId]
		AND	[s].[Refnum]	<>	[f].[Refnum]
		AND	[s].[StudyYear]	<	[f].[StudyYear]
	ORDER BY
		[s].[StudyYear]		DESC
		) [h];
GO