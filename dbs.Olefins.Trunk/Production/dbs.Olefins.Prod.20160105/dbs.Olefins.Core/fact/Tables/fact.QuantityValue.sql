﻿CREATE TABLE [fact].[QuantityValue] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [Amount_Cur]        REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_QuantityValue_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_QuantityValue_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_QuantityValue_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_QuantityValue_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_QuantityValue] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_QuantityValue_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [CR_QuantityValue_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_QuantityValue_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_QuantityValue_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_QuantityValue_Quantity] FOREIGN KEY ([Refnum], [StreamId], [StreamDescription], [CalDateKey]) REFERENCES [fact].[Quantity] ([Refnum], [StreamId], [StreamDescription], [CalDateKey]),
    CONSTRAINT [FK_QuantityValue_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_QuantityValue_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_QuantityValue_u]
	ON [fact].[QuantityValue]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[QuantityValue]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[QuantityValue].Refnum				= INSERTED.Refnum
		AND [fact].[QuantityValue].CurrencyRpt		= INSERTED.CurrencyRpt
		AND [fact].[QuantityValue].StreamId			= INSERTED.StreamId
		AND [fact].[QuantityValue].StreamDescription	= INSERTED.StreamDescription
		AND [fact].[QuantityValue].CalDateKey			= INSERTED.CalDateKey;

END;