﻿CREATE TABLE [fact].[GenPlantLogistics] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [StreamId]       VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_GenPlantLogistics_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_GenPlantLogistics_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_GenPlantLogistics_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_GenPlantLogistics_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_GenPlantLogistics] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_GenPlantLogistics_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_GenPlantLogistics_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_GenPlantLogistics_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_GenPlantLogistics_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_GenPlantLogistics_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_GenPlantLogistics_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_GenPlantLogistics_u]
	ON [fact].[GenPlantLogistics]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[GenPlantLogistics]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[GenPlantLogistics].Refnum			= INSERTED.Refnum
		AND [fact].[GenPlantLogistics].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[GenPlantLogistics].AccountId		= INSERTED.AccountId
		AND [fact].[GenPlantLogistics].StreamId		= INSERTED.StreamId
		AND [fact].[GenPlantLogistics].CalDateKey		= INSERTED.CalDateKey;

END;