﻿CREATE TABLE [fact].[ApcOnLinePcnt] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [ApcId]          VARCHAR (42)       NOT NULL,
    [OnLine_Pcnt]    REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ApcOnLinePcnt_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ApcOnLinePcnt_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ApcOnLinePcnt_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ApcOnLinePcnt_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ApcOnLinePcnt] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ApcId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ApcOnLinePcnt_OnLine_Pcnt] CHECK ([OnLine_Pcnt]>=(0.0) AND [OnLine_Pcnt]<=(100.0)),
    CONSTRAINT [FK_ApcOnLinePcnt_Apc_LookUp] FOREIGN KEY ([ApcId]) REFERENCES [dim].[Apc_LookUp] ([ApcId]),
    CONSTRAINT [FK_ApcOnLinePcnt_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ApcOnLinePcnt_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ApcOnLinePcnt_u]
	ON [fact].[ApcOnLinePcnt]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ApcOnLinePcnt]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ApcOnLinePcnt].Refnum		= INSERTED.Refnum
		AND [fact].[ApcOnLinePcnt].ApcId		= INSERTED.ApcId
		AND [fact].[ApcOnLinePcnt].CalDateKey	= INSERTED.CalDateKey;

END;