﻿CREATE TABLE [fact].[ReliabilityPyroFurnDownTime] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [FurnId]         INT                NOT NULL,
    [OppLossId]      VARCHAR (42)       NOT NULL,
    [DownTime_Days]  REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityPyroFurnDownTime_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnDownTime_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnDownTime_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurnDownTime_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityPyroFurnDownTime] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FurnId] ASC, [OppLossId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ReliabilityPyroFurnDownTime_DownTime_Days] CHECK ([DownTime_Days]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurnDownTime_FurnId] CHECK ([FurnId]>=(0)),
    CONSTRAINT [FK_ReliabilityPyroFurnDownTime_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityPyroFurnDownTime_ReliabilityOppLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityPyroFurnDownTime_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityPyroFurnDownTime_u]
	ON [fact].[ReliabilityPyroFurnDownTime]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityPyroFurnDownTime]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityPyroFurnDownTime].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityPyroFurnDownTime].FurnId		= INSERTED.FurnId
		AND [fact].[ReliabilityPyroFurnDownTime].OppLossId	= INSERTED.OppLossId
		AND [fact].[ReliabilityPyroFurnDownTime].CalDateKey	= INSERTED.CalDateKey;

END;