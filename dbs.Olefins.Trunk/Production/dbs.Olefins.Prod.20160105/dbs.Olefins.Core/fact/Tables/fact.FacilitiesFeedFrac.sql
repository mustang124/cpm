﻿CREATE TABLE [fact].[FacilitiesFeedFrac] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [FacilityId]        VARCHAR (42)       NOT NULL,
    [StreamId]          VARCHAR (42)       NULL,
    [FeedRate_kBsd]     REAL               NULL,
    [FeedProcessed_kMT] REAL               NULL,
    [FeedDensity_SG]    REAL               NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesFeedFrac_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_FacilitiesFeedFrac_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_FacilitiesFeedFrac_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_FacilitiesFeedFrac_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_FacilitiesFeedFrac] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_FacilitiesFeedFrac_FeedDensity_SG] CHECK ([FeedDensity_SG]>=(0.0)),
    CONSTRAINT [CR_FacilitiesFeedFrac_FeedProcessed_kMT] CHECK ([FeedProcessed_kMT]>=(0.0)),
    CONSTRAINT [CR_FacilitiesFeedFrac_FeedRate_kBsd] CHECK ([FeedRate_kBsd]>=(0.0)),
    CONSTRAINT [FK_FacilitiesFeedFrac_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_FacilitiesFeedFrac_Facilities] FOREIGN KEY ([Refnum], [FacilityId], [CalDateKey]) REFERENCES [fact].[Facilities] ([Refnum], [FacilityId], [CalDateKey]),
    CONSTRAINT [FK_FacilitiesFeedFrac_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_FacilitiesFeedFrac_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_FacilitiesFeedFrac_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_FacilitiesFeedFrac_u]
	ON [fact].[FacilitiesFeedFrac]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesFeedFrac]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[FacilitiesFeedFrac].Refnum		= INSERTED.Refnum
		AND [fact].[FacilitiesFeedFrac].FacilityId	= INSERTED.FacilityId
		AND [fact].[FacilitiesFeedFrac].CalDateKey	= INSERTED.CalDateKey;

END;