﻿CREATE TABLE [fact].[MetaMisc] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [StartUp_Year]   SMALLINT           NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MetaMisc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MetaMisc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MetaMisc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MetaMisc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MetaMisc] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_MetaMisc_StartUp_Year] CHECK ([StartUp_Year]>=(1950) AND len([StartUp_Year])=(4)),
    CONSTRAINT [FK_MetaMisc_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MetaMisc_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_MetaMisc_u]
	ON [fact].[MetaMisc]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[MetaMisc]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[MetaMisc].Refnum		= INSERTED.Refnum
		AND [fact].[MetaMisc].CalDateKey	= INSERTED.CalDateKey;

END;