﻿CREATE PROCEDURE [fact].[Select_CapacityAttributes]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 1-1
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[Expansion_Pcnt],
		[f].[Expansion_Date],
		[f].[CapAllow_Bit],
		[CapAllow_YN] = CASE WHEN [f].[CapAllow_Bit] = 1 THEN 'Y' ELSE 'N' END,
		[f].[CapLoss_Pcnt],
		[f].[StartUp_Year]
	FROM
		[fact].[CapacityAttributes]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;