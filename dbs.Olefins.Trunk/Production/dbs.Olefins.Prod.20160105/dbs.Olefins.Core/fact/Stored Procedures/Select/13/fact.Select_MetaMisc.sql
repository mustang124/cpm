﻿CREATE PROCEDURE [fact].[Select_MetaMisc]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 13 (4)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StartUp_Year]
	FROM
		[fact].[MetaMisc]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;