﻿CREATE PROCEDURE [fact].[Select_GenPlantExpansion]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 8-3 (3)
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[AccountId],
		[f].[StreamId],
		[f].[CapacityAdded_kMT]
	FROM
		[fact].[GenPlantExpansion]	[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;