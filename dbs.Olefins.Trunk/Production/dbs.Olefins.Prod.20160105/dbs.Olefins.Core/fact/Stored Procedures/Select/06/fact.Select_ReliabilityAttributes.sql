﻿CREATE PROCEDURE [fact].[Select_ReliabilityAttributes]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-2, Table 6-3, Table 6-4
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],

		--	Table 6-2
		[f].[TimingFactorId],
		[f].[TaPrep_Hrs],
		[f].[TaStartUp_Hrs],
		
		--	Table 6-3, Table 6-4
		[f].[MiniTurnAround_Bit],
			[MiniTurnAround_YN]		= CASE WHEN [f].[MiniTurnAround_Bit] = 1 THEN 'Yes' ELSE 'No' END,
		[f].[FurnRetubeWorkId],
		[x].[XlsFurnRetubeWork],
		[f].[FurnTubeLife_Mnths],
		[f].[FurnRetube_Days],

		--	Table 6-4
		[r].[PlantFurnLimit_Pcnt],

		[o].[Operation_Years],
		[p].[PhilosophyId]
	FROM
		[fact].[Reliability]					[f]
	LEFT OUTER JOIN
		[fact].[ReliabilityPyroFurnPlantLimit]	[r]
			ON	[r].[Refnum]		= [f].[Refnum]
			AND	[r].[CalDateKey]	= [f].[CalDateKey]
	LEFT OUTER JOIN
		[fact].[ReliabilityCompOperation]		[o]
			ON	[o].[Refnum]		= [f].[Refnum]
			AND	[o].[CalDateKey]	= [f].[CalDateKey]
	LEFT OUTER JOIN
		[fact].[ReliabilityDTPhilosophy]		[p]
			ON	[p].[Refnum]		= [f].[Refnum]
	LEFT OUTER JOIN (
		VALUES
			('Company', 'Company Employees'),
			('General', 'General Contractors'),
			('Speciality', 'Speciality Contractors')
			) [x]([FurnRetubeWorkId], [XlsFurnRetubeWork])
			ON [x].[FurnRetubeWorkId]	 = [f].[FurnRetubeWorkId]
	WHERE
		[f].[Refnum]	= @Refnum;

END;