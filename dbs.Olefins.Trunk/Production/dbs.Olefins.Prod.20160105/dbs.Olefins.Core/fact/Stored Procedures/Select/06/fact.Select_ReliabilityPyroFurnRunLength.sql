﻿CREATE PROCEDURE [fact].[Select_ReliabilityPyroFurn_RunLength]
(
	@Refnum			VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	--	Table 6-3
	SELECT
		[f].[Refnum],
		[f].[CalDateKey],
		[f].[StreamId],
		[f].[RunLength_Days],
		[f].[Inj],
		[f].[PreTreat_Bit],
		[f].[AntiFoul_Bit],
		[AverageDecokesPerYear]	= [f].[_AverageDecokesPerYear]
	FROM
		[fact].[ReliabilityPyroFurnFeed]			[f]
	WHERE
		[f].[Refnum]	= @Refnum;

END;