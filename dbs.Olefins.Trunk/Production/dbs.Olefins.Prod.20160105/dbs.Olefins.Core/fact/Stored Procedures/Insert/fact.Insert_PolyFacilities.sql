﻿CREATE PROCEDURE [fact].[Insert_PolyFacilities]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.PolyFacilities
		(
			[Refnum],
			[CalDateKey],
			[AssetId],
		
			[PolyName],
			[PolyCity],
			[PolyState],
			[PolyCountryId],
		
			[Capacity_kMT],
		
			[Stream_MTd],
			[ReactorTrains_Count],
			[StartUp_Year],
			
			[PolymerFamilyId],
			[PolymerTechId],
			[PrimeResin_Pcnt]
			)
		SELECT
				[Refnum]			= [etl].[ConvRefnum]([t].[Refnum], t.[StudyYear], @StudyId),
				[CalDateKey]		= [etl].[ConvDateKey]([t].[StudyYear]),
				[AssetId]			= [etl].[ConvAssetPCH]([p].[Refnum]) + '-T' + CONVERT(VARCHAR(2), [p].[PlantId]),

			[p].[Name],
			[p].[City],
			[p].[State],
			[l].[CountryId],
			[p].[CapKMTA],
			[p].[CapMTD],
			[p].[ReactorTrainCnt],
				[StartUp_Year]		= CASE WHEN [p].[ReactorStartup] > 1900 THEN [p].[ReactorStartup] END,
				[PolymerFamilyId]	= [etl].[ConvPolymerFamily]([p].[Product]),
				[PolymerTechId]		= [etl].[ConvPolymerTech]([p].[Technology]),
				[PrimeResin_Pcnt]	= CASE WHEN [p].[ReactorResinPct] > 100.0 THEN 100.0 ELSE [p].[ReactorResinPct] END
		FROM
			[stgFact].[PolyFacilities]	[p]
		INNER JOIN
			[stgFact].[TSort]			[t]
				ON	[t].[Refnum] = [p].[Refnum]
		LEFT OUTER JOIN
			[dim].[Country_LookUp]		[l]
				ON	[l].[CountryName]	= [p].[Country]
		WHERE	[p].[Name] <> '0'
			AND [t].[Refnum]		= @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;