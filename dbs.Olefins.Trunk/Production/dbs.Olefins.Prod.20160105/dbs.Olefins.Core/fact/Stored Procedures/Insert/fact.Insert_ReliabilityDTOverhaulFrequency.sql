﻿CREATE PROCEDURE [fact].[Insert_ReliabilityDTOverhaulFrequency]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO [fact].[ReliabilityDTOverhaulFrequency]
		(
			[Refnum],
			[CalDateKey],
			[FacilityId],
			[Frequency_Years]
		)
		SELECT
			[t].[Refnum],
			[t].[CalDateKey],
			[t].[FacilityId],
			[t].[Frequency_Years]
		FROM (
			SELECT
				[Refnum]				= [etl].[ConvRefnum]([t].[Refnum], [t].[StudyYear], @StudyId),
				[CalDateKey]			= [etl].[ConvDateKey]([t].[StudyYear]),
				[CompRefrigEthylene]	= [p].[RTC2Comp],
				[CompTurbEthylene]		= [p].[RTC2Turb],
				[CompRefrigPropylene]	= [p].[RTC3Comp],
				[CompTurbPropylene]		= [p].[RTC3Turb],
				[CompGas]				= [p].[RTCGComp],
				[CompTurbGas]			= [p].[RTCGTurb]
			FROM
				[stgFact].[TSort]		[t]
			INNER JOIN
				[stgFact].[PracRely]	[p]
					ON	[p].[Refnum]	= [t].[Refnum]
			WHERE
				[t].[Refnum]			= @sRefnum
			) [p]
			UNPIVOT (
				[Frequency_Years] FOR [FacilityId] IN
				(
					[CompRefrigEthylene],
					[CompTurbEthylene],
					[CompRefrigPropylene],
					[CompTurbPropylene],
					[CompGas],
					[CompTurbGas]
				)
			) [t]
		WHERE
			[t].[Frequency_Years]	IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE [dbo].[usp_LogError] 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;