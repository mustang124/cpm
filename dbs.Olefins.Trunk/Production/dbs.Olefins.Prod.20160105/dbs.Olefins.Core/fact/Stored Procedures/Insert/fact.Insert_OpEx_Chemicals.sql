﻿CREATE PROCEDURE [fact].[Insert_OpEx_Chemicals]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.OpEx(Refnum, CalDateKey, AccountId, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvAccountID(u.AccountId)
			, 'USD'
			, u.Amount_Cur
		FROM(
			SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum],
				etl.ConvDateKey(t.StudyYear)							[CalDateKey],
				
				o.ChemBFWCW,
				o.ChemWW,
				o.ChemCaustic,
				o.ChemPretreatment,
				o.ChemAntiFoul,
				o.ChemOthProc,
				o.ChemAdd,
				o.ChemOth,
				o.Chemicals
					- ISNULL(o.ChemBFWCW, 0.0)
					- ISNULL(o.ChemWW, 0.0)
					- ISNULL(o.ChemCaustic, 0.0)
					- ISNULL(o.ChemPretreatment, 0.0)
					- ISNULL(o.ChemAntiFoul, 0.0)
					- ISNULL(o.ChemOthProc, 0.0)
					- ISNULL(o.ChemAdd, 0.0)
					- ISNULL(o.ChemOth, 0.0)			[ChemCorrection]

			FROM stgFact.OpEx o
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = o.Refnum
				AND	t.StudyYear >= 2001
			WHERE t.Refnum = @sRefnum
			) p
			UNPIVOT (
			Amount_Cur FOR AccountId IN(
				ChemBFWCW,
				ChemWW,
				ChemCaustic,
				ChemPretreatment,
				ChemAntiFoul,
				ChemOthProc,
				ChemAdd,
				ChemOth,
				ChemCorrection
				)
			) u
		WHERE u.Amount_Cur <> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;