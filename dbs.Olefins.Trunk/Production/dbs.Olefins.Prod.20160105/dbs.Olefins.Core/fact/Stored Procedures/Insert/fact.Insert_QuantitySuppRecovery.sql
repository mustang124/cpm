﻿CREATE PROCEDURE [fact].[Insert_QuantitySuppRecovery]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.QuantitySuppRecovery(Refnum, CalDateKey, StreamId, StreamDescription, Recovered_WtPcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
		
			, u.StreamId
			, ISNULL(u.StreamDescription, u.StreamId)					[StreamDescription]
		
			, u.RecPcnt	* 100.0											[RecoveredPcnt]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)		[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, etl.ConvStreamID(q.FeedProdId)						[StreamId]
				, etl.ConvStreamDescription(q.FeedProdId, NULL, q.MiscProd1, q.MiscProd2, q.MiscFeed) [StreamDescription]
				, CASE WHEN q.RecPcnt = 100 THEN 1 ELSE q.RecPcnt END	[RecPcnt]
			FROM stgFact.Quantity q
			INNER JOIN stgFact.TSort t		ON	t.Refnum = q.Refnum
			WHERE (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
				AND q.RecPcnt IS NOT NULL
				AND t.Refnum = @sRefnum
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;