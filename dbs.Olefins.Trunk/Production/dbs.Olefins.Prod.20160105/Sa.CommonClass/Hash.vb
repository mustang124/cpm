﻿Imports Microsoft.VisualBasic.FileIO

Public Module Hash

    ''' <summary>
    ''' Stream reader interface
    ''' </summary>
    ''' <remarks></remarks>
    Friend Interface iStreamReader

        ''' <summary>
        ''' Function to read and store the stream
        ''' </summary>
        ''' <returns>System.IO.Stream</returns>
        ''' <remarks></remarks>
        Function Read() As System.IO.Stream

        ''' <summary>
        ''' Input Placeholder 
        ''' </summary>
        ''' <value>String representing</value>
        ''' <remarks></remarks>
        WriteOnly Property Item As String

    End Interface

    ''' <summary>
    ''' Byte writer interface
    ''' </summary>
    ''' <remarks></remarks>
    Friend Interface iByteWriter

        ''' <summary>
        ''' Routine to store the results of a Byte() stream
        ''' </summary>
        ''' <param name="b"></param>
        ''' <remarks></remarks>
        Sub Store(ByVal b As Byte())

        ''' <summary>
        ''' Output Placeholder
        ''' </summary>
        ''' <value></value>
        ''' <remarks></remarks>
        WriteOnly Property Item As Byte()

    End Interface

    Friend Enum Algorithm As System.Int32

        SHA512 = 512
        SHA384 = 384
        SHA256 = 256
        SHA1 = 1
        RIPEMD160 = 160
        MD5 = 5

    End Enum

    ''' <summary>
    ''' Reads a string for use as System.IO.Stream.
    ''' </summary>
    ''' <remarks></remarks>
    Friend Class ReaderString
        Implements iStreamReader

        ''' <summary>
        ''' Placeholder for the Item property.
        ''' </summary>
        ''' <remarks></remarks>
        Private _Item As String

        ''' <summary>
        ''' Force using a string to be converted into IO.Stream.
        ''' </summary>
        ''' <param name="ItemString">String to be read into IO.Stream.</param>
        ''' <remarks></remarks>
        Friend Sub New(ByVal ItemString As String)
            Me.Item = ItemString
        End Sub

        ''' <summary>
        ''' Sets _Item to be used during Read.
        ''' </summary>
        ''' <value>String</value>
        ''' <remarks></remarks>
        Private WriteOnly Property Item As String Implements iStreamReader.Item
            Set(ByVal value As String)
                Me._Item = value
            End Set
        End Property

        ''' <summary>
        ''' Read _Item into memory
        ''' </summary>
        ''' <returns>System.IO.Stream</returns>
        ''' <remarks></remarks>
        Private Function Read() As System.IO.Stream Implements iStreamReader.Read

            Dim b As Byte() = System.Text.Encoding.UTF8.GetBytes(Me._Item)
            Dim s As New System.IO.MemoryStream(b)

            Return s

        End Function

    End Class

    ''' <summary>
    ''' Reads a file for use as System.IO.Stream.
    ''' </summary>
    ''' <remarks></remarks>
    Friend Class ReaderFile
        Implements iStreamReader

        ''' <summary>
        ''' Placeholder for the Item property.
        ''' </summary>
        ''' <remarks></remarks>
        Private ItemString As String

        ''' <summary>
        ''' Force setting the path to the file.
        ''' </summary>
        ''' <param name="FilePath">String to be read into IO.Stream.</param>
        ''' <remarks></remarks>
        Friend Sub New(ByVal FilePath As String)
            Me.Item = FilePath
        End Sub

        ''' <summary>
        ''' Sets _Item to be used during Read.
        ''' </summary>
        ''' <value>String</value>
        ''' <remarks></remarks>
        Private WriteOnly Property Item As String Implements iStreamReader.Item
            Set(value As String)
                Me.ItemString = value
            End Set
        End Property

        ''' <summary>
        ''' Reads the file, set in New and stored in _Item, into a System.IO.Stream
        ''' </summary>
        ''' <returns>System.IO.Stream</returns>
        ''' <remarks></remarks>
        Private Function Read() As System.IO.Stream Implements iStreamReader.Read

            Dim s As System.IO.FileStream = Nothing

            If System.IO.File.Exists(ItemString) Then
                s = System.IO.File.OpenRead(ItemString)
            End If

            Return s

        End Function

    End Class

    ''' <summary>
    ''' Reads a string for use as System.IO.Stream.
    ''' </summary>
    ''' <remarks></remarks>
    Friend Class ReaderSalt
        Implements iStreamReader

        ''' <summary>
        ''' Placeholder for the Item property.
        ''' </summary>
        ''' <remarks></remarks>
        Private ItemString As String

        ''' <summary>
        ''' Force using a string to be converted into IO.Stream.
        ''' </summary>
        ''' <param name="uName">String to be read into IO.Stream.</param>
        ''' <param name="pWord">String to be read into IO.Stream.</param>
        ''' <remarks></remarks>
        Friend Sub New(ByVal uName As String, ByVal pWord As String, ByVal Salt As String)
            Me.Item = Salt + uName + pWord
        End Sub

        ''' <summary>
        ''' Sets _Item to be used during Read.
        ''' </summary>
        ''' <value>String</value>
        ''' <remarks></remarks>
        Private WriteOnly Property Item As String Implements iStreamReader.Item
            Set(ByVal value As String)
                Me.ItemString = value
            End Set
        End Property

        ''' <summary>
        ''' Read _Item into memory
        ''' </summary>
        ''' <returns>System.IO.Stream</returns>
        ''' <remarks></remarks>
        Private Function Read() As System.IO.Stream Implements iStreamReader.Read

            Dim b As Byte() = System.Text.Encoding.UTF8.GetBytes(Me.ItemString)
            Dim s As New System.IO.MemoryStream(b)

            Return s

        End Function

    End Class

    ''' <summary>
    ''' Writes Byte array to a string
    ''' </summary>
    ''' <remarks></remarks>
    Friend Class WriterByte
        Implements iByteWriter

        ''' <summary>
        ''' Placeholder for the Item Porperty.
        ''' </summary>
        ''' <remarks></remarks>
        Private ItemByte As Byte()

        ''' <summary>
        ''' Saves the Byte array to Item
        ''' </summary>
        ''' <param name="b"></param>
        ''' <remarks></remarks>
        Private Sub Store(ByVal b As Byte()) Implements iByteWriter.Store
            Me.Item = b
        End Sub

        ''' <summary>
        ''' Sets _Item to be used during Write.
        ''' </summary>
        ''' <value>Byte()</value>
        ''' <remarks></remarks>
        Private WriteOnly Property Item As Byte() Implements iByteWriter.Item
            Set(ByVal value As Byte())
                Me.ItemByte = value
            End Set
        End Property

        ''' <summary>
        ''' Outputs the Byte() to System.String.
        ''' </summary>
        ''' <returns>System.String</returns>
        ''' <remarks></remarks>
        Friend Function Write() As System.String

            Dim s As String = Nothing

            If Not ItemByte Is Nothing Then

                s = System.BitConverter.ToString(ItemByte)
                s = s.Replace("-", String.Empty)

            End If

            Return s

        End Function

    End Class

    ''' <summary>
    ''' Calculates a Hash from iStreamReader and stores the results in iByteWriter
    ''' </summary>
    ''' <param name="iReader">Stream Reader Class; implements iStreamReader.</param>
    ''' <param name="iWriter">Byte Writer Class; implements iByteWriter.</param>
    ''' <param name="Algorithm">Type of algorithm to perform.</param>
    ''' <remarks>
    ''' Sa.FileOps.HashAlgorithm() to return the type of algorithm.
    ''' </remarks>
    Friend Sub HashService(ByVal iReader As iStreamReader, ByVal iWriter As iByteWriter, ByVal Algorithm As Sa.Hash.Algorithm)

        Dim s As System.IO.Stream = Nothing
        Dim b As Byte() = Nothing

        Dim a As System.Security.Cryptography.HashAlgorithm = Sa.Hash.HashAlgorithm(Algorithm)

        s = iReader.Read()

        If Not s Is Nothing Then

            b = a.ComputeHash(s)

            s.Close()
            s.Dispose()

        End If

        iWriter.Store(b)

    End Sub

    ''' <summary>
    ''' Selectes the Hash Algorithm class for the Hash Type Name
    ''' </summary>
    ''' <param name="HashType">Hash algorithm name</param>
    ''' <returns>HashAlgorithm</returns>
    ''' <remarks>
    ''' <para>
    ''' <a href="http://msdn.microsoft.com/en-us/library/system.security.cryptography.hashalgorithm.aspx">
    ''' HashAlgorithm Class: http://msdn.microsoft.com/en-us/library/system.security.cryptography.hashalgorithm.aspx </a>
    ''' </para>
    ''' </remarks>
    Friend Function HashAlgorithm(ByVal HashType As Sa.Hash.Algorithm) As System.Security.Cryptography.HashAlgorithm

        Dim h As System.Security.Cryptography.HashAlgorithm = Nothing

        Select Case HashType
            Case Sa.Hash.Algorithm.SHA512
                ' 512 Bits
                h = New System.Security.Cryptography.SHA512Managed

            Case Sa.Hash.Algorithm.SHA384
                ' 348 Bits
                h = New System.Security.Cryptography.SHA384Managed

            Case Sa.Hash.Algorithm.SHA256
                ' 256 Bits
                h = New System.Security.Cryptography.SHA256Managed

            Case Sa.Hash.Algorithm.SHA1
                ' 160 Bits
                h = New System.Security.Cryptography.SHA1Managed

            Case Sa.Hash.Algorithm.RIPEMD160
                ' 160 Bits
                h = New System.Security.Cryptography.RIPEMD160Managed

            Case Sa.Hash.Algorithm.MD5
                ' 128 Bits
                h = New System.Security.Cryptography.MD5CryptoServiceProvider

        End Select

        Return h

    End Function

    ' http://crackstation.net/hashing-security.html
    <Obsolete()>
    Friend Function GetRandomSalt() As String

        Dim rng As New System.Security.Cryptography.RNGCryptoServiceProvider

        Dim salt(32) As Byte

        rng.GetBytes(salt)

        GetRandomSalt = BytesToHex(salt)

    End Function

    Private Function BytesToHex(ByVal toConvert As Byte()) As String

        Dim sb As New System.Text.StringBuilder(toConvert.Length * 2)
        For Each b As Byte In toConvert

            sb.Append(b.ToString("X2"))

        Next b

        Return sb.ToString()

    End Function

 End Module
