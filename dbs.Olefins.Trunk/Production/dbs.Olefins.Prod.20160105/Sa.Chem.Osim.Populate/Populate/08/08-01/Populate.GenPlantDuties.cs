﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void GenPlantDuties(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			const int c = 8;

			string streamId;

			Dictionary<string, int> genDuties = GenPlantDutiesRowDictionary();

			try
			{
				wks = wkb.Worksheets[tabNamePrefix + "8-1"];

				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_GenPlantDuties]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								streamId = rdr.GetString(rdr.GetOrdinal("StreamID"));

								if (genDuties.TryGetValue(streamId, out r))
								{
									AddRangeValues(wks, r, c, rdr, "Amount_Cur");
								}
							}
						}
					}
					cn.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "GenPlantDuties", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_GenPlantDuties]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, int> GenPlantDutiesRowDictionary()
		{
			Dictionary<string, int> d = new Dictionary<string, int>();

			d.Add("Naphtha", 39);
			d.Add("LiqHeavy", 40);
			d.Add("FeedOther", 41);
			d.Add("EthylenePG", 42);
			d.Add("PropylenePG", 43);
			d.Add("Butadiene", 44);
			d.Add("Benzene", 45);

			//d.Add("LPG", 0);
			//d.Add("Butane", 0);
			//d.Add("Propane", 0);

			return d;
		}
	}
}