﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Sa.Chem.Osim
{
	public partial class Populate
	{
		public void PolyQuantity(Excel.Workbook wkb, string tabNamePrefix, string refnum, string factorSetId)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			int r = 0;
			int c = 0;

			string traindId;
			string streamId;
			RangeReference rr;

			Dictionary<string, int> polyCol = PolyTrainColDictionary();
			Dictionary<string, RangeReference> polyProd = PolyProdDictionary();

			try
			{
				using (SqlConnection cn = new SqlConnection(Common.cnString()))
				{
					cn.Open();

					using (SqlCommand cmd = new SqlCommand("[fact].[Select_PolyQuantity]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = refnum;

						using (SqlDataReader rdr = cmd.ExecuteReader())
						{
							while (rdr.Read())
							{
								traindId = rdr.GetString(rdr.GetOrdinal("AssetId"));
								traindId = traindId.Substring(traindId.Length - 2);

								if (polyCol.TryGetValue(traindId, out c))
								{
									c = c + 2;

									streamId = rdr.GetString(rdr.GetOrdinal("StreamId"));

									if (polyProd.TryGetValue(streamId, out rr))
									{
										wks = wkb.Worksheets[tabNamePrefix + rr.sheet];
										r = rr.row;
										AddRangeValues(wks, r, c, rdr, "Quantity_kMT");

										c = 3;
										AddRangeValues(wks, r, c, rdr, "StreamName");
									}
								}
							}
						}
					}
					cn.Close();
				};
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "PolyQuantity", refnum, wkb, wks, rng, (UInt32)r, (UInt32)c, "[fact].[Select_PolyQuantity]", ex);
			}
			finally
			{
				rng = null;
				wks = null;
			}
		}

		private Dictionary<string, RangeReference> PolyProdDictionary()
		{
			Dictionary<string, RangeReference> d = new Dictionary<string, RangeReference>();

			d.Add("PolyHydrogen", new RangeReference(T12_02, 7, 0));
			d.Add("PolyAPEthylene", new RangeReference(T12_02, 8, 0));
			d.Add("PolyPurchEthylene", new RangeReference(T12_02, 9, 0));
			d.Add("PolyAPPropylene", new RangeReference(T12_02, 10, 0));
			d.Add("PolyPurchPropylene", new RangeReference(T12_02, 11, 0));
			d.Add("PolyButene", new RangeReference(T12_02, 12, 0));
			d.Add("PolyHexene", new RangeReference(T12_02, 13, 0));
			d.Add("PolyOctene", new RangeReference(T12_02, 14, 0));
			d.Add("PolyMonomer1", new RangeReference(T12_02, 16, 0));
			d.Add("PolyMonomer2", new RangeReference(T12_02, 17, 0));
			d.Add("PolyMonomer3", new RangeReference(T12_02, 18, 0));
			d.Add("PolyAdditives", new RangeReference(T12_02, 19, 0));
			d.Add("PolyCatalysts", new RangeReference(T12_02, 20, 0));
			d.Add("PolyOther", new RangeReference(T12_02, 21, 0));

			d.Add("LDPELiner", new RangeReference(T12_02, 28, 0));
			d.Add("LDPEClarity", new RangeReference(T12_02, 29, 0));
			d.Add("LDPEBlowMold", new RangeReference(T12_02, 30, 0));
			d.Add("LDPEGPInjMold", new RangeReference(T12_02, 31, 0));
			d.Add("LDPEInjLidResin", new RangeReference(T12_02, 32, 0));
			d.Add("LDPEEVA", new RangeReference(T12_02, 33, 0));
			d.Add("LDPEOther", new RangeReference(T12_02, 34, 0));
			d.Add("LDPEOff", new RangeReference(T12_02, 35, 0));

			d.Add("LLDPEBCLinerfilm", new RangeReference(T12_02, 37, 0));
			d.Add("LLDPEBCMold", new RangeReference(T12_02, 38, 0));
			d.Add("LLDPEHAOLinerFilm", new RangeReference(T12_02, 39, 0));
			d.Add("LLDPEHAOMIFilm", new RangeReference(T12_02, 40, 0));
			d.Add("LLDPEHAOInjMold", new RangeReference(T12_02, 41, 0));
			d.Add("LLDPEHAOLidResin", new RangeReference(T12_02, 42, 0));
			d.Add("LLDPEHAORotoMold", new RangeReference(T12_02, 43, 0));
			d.Add("LLDPEOther", new RangeReference(T12_02, 44, 0));
			d.Add("LLDPEOff", new RangeReference(T12_02, 45, 0));

			d.Add("HDPEBlowMoldHomoPoly", new RangeReference(T12_02, 47, 0));
			d.Add("HDPEBlowMoldCoPoly", new RangeReference(T12_02, 48, 0));
			d.Add("HDPEFilm", new RangeReference(T12_02, 49, 0));
			d.Add("HDPEGPInjMold", new RangeReference(T12_02, 50, 0));
			d.Add("HDPEPipe", new RangeReference(T12_02, 51, 0));
			d.Add("HDPEDrum", new RangeReference(T12_02, 52, 0));
			d.Add("HDPERotoMold", new RangeReference(T12_02, 53, 0));
			d.Add("HDPEOther", new RangeReference(T12_02, 54, 0));
			d.Add("HDPEOff", new RangeReference(T12_02, 55, 0));

			d.Add("PolyProHomoPoly", new RangeReference(T12_02,57, 0));
			d.Add("PolyProFilm", new RangeReference(T12_02, 58, 0));
			d.Add("PolyProBlowMold", new RangeReference(T12_02, 59, 0));
			d.Add("PolyProInj", new RangeReference(T12_02, 60, 0));
			d.Add("PolyProBlockCoPoly", new RangeReference(T12_02, 61, 0));
			d.Add("PolyProAtacticPolyPro", new RangeReference(T12_02, 62, 0));
			d.Add("PolyProOther", new RangeReference(T12_02, 63, 0));
			d.Add("PolyProOff", new RangeReference(T12_02, 64, 0));

			d.Add("PolyByClient1", new RangeReference(T12_02, 66, 0));
			d.Add("PolyByClient2", new RangeReference(T12_02, 67, 0));
			d.Add("PolyByClient3", new RangeReference(T12_02, 68, 0));
			d.Add("PolyByClient4", new RangeReference(T12_02, 69, 0));
			d.Add("PolyByClient5", new RangeReference(T12_02, 70, 0));

			d.Add("PolyProdReactor", new RangeReference(T12_02, 76, 0));

			return d;
		}
	}
}