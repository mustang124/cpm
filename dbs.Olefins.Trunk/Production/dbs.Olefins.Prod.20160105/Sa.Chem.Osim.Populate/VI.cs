﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa.Chem.Osim
{
	public class VI
	{
		private const string template = "\\\\Dallas2\\data\\Data\\STUDY\\SEEC\\Template\\OSIM_YYYY_SEEC_NNN_NoCode.xls";

		public void MakeVI(string path)
		{
			Excel.Application xla = Common.NewExcelApplication(false);
			Excel.Workbook wkbSource = Common.OpenWorkbook_ReadOnly(xla, path);
			Excel.Workbook wkbTarget = Common.OpenWorkbook_ReadOnly(xla, template);

			Excel.Worksheet wksTarget;

			try
			{
				wkbTarget.Worksheets["PlantName"].Range["C40"].Value2 = wkbSource.Worksheets["PlantName"].Range["C40"].Value2;
			}
			catch
			{
			}

			foreach (Excel.Worksheet wksSource in wkbSource.Worksheets)
			{
				try
				{
					wksTarget = wkbTarget.Worksheets[wksSource.Name];

					wksSource.Unprotect("saipassword");
					wksTarget.Unprotect("saipassword");

					foreach (Excel.Range rngSource in wksSource.UsedRange.SpecialCells(Excel.XlCellType.xlCellTypeConstants))
					{
						if (!rngSource.Locked)
						{
							wksTarget.Range[rngSource.AddressLocal].Value2 = rngSource.Value2;
						}
					}

					wksTarget.Protect("saipassword");
				}
				catch
				{
				}

				xla.Calculate();
			}

			string pathSaveAs = wkbSource.FullName.Replace(".xls", "_VI.xls");

			wkbTarget.SaveAs(pathSaveAs);

			if (wkbTarget != null)
			{
				wkbTarget.Close(false);
				System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wkbTarget);
				wkbTarget = null;
			}

			if (wkbSource != null)
			{
				wkbSource.Close(false);
				System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wkbSource);
				wkbSource = null;
			}

			if (xla != null)
			{
				xla.Quit();
				System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xla);
				xla = null;
			}

			System.GC.WaitForPendingFinalizers();
			System.GC.Collect();
			System.GC.WaitForFullGCComplete();
		}
	}
}