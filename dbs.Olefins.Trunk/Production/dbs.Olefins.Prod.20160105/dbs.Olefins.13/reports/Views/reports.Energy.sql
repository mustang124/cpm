﻿









CREATE VIEW [reports].[Energy]
AS
SELECT re.GroupId, Currency, Scenario, DataType
, EII
, EII_StdEnergy
, EEI_SPSL
, EEI_SPSL_StdEnergy
, EEI_PYPS
, EEI_PYPS_StdEnergy
, PurFuelNatural
, PurSteamSHP
, PurSteamHP
, PurSteamIP
, PurSteamLP
, PurElec
, PurOther
, PurchasedEnergy
, PPFCFuelGas
, PPFCEthane
, PPFCPropane
, PPFCOther
, PPFC
, TotConsumption
, ExportSteamSHP
, ExportSteamHP
, ExportSteamIP
, ExportSteamLP
, ExportElectric
, ExportCredits
, NetEnergyCons
, EIIStdPerUEDC
, PurFuelNatural_CostPer = CASE WHEN PurFuelNatural_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PurFuelNatural_CostPer END
, PurSteamSHP_CostPer = CASE WHEN PurSteamSHP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PurSteamSHP_CostPer END
, PurSteamHP_CostPer = CASE WHEN PurSteamHP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PurSteamHP_CostPer END
, PurSteamIP_CostPer = CASE WHEN PurSteamIP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PurSteamIP_CostPer END
, PurSteamLP_CostPer = CASE WHEN PurSteamLP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PurSteamLP_CostPer END
, PurElec_CostPerkWh = CASE WHEN PurElec_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PurElec_CostPerkWh END
, PurOther_CostPer = CASE WHEN PurOther_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PurOther_CostPer END
, PurTot_CostPer
, PPFCFuelGas_CostPer = CASE WHEN PPFCFuelGas_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PPFCFuelGas_CostPer END
, PPFCOther_CostPer = CASE WHEN PPFCOther_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN PPFCOther_CostPer END
, PPFC_CostPer
, TotConsumption_CostPer
, ExportSteamSHP_CostPer = CASE WHEN ExportSteamSHP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN ExportSteamSHP_CostPer END
, ExportSteamHP_CostPer = CASE WHEN ExportSteamHP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN ExportSteamHP_CostPer END
, ExportSteamIP_CostPer = CASE WHEN ExportSteamIP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN ExportSteamIP_CostPer END
, ExportSteamLP_CostPer = CASE WHEN ExportSteamLP_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN ExportSteamLP_CostPer END
, ExportElectric_CostPerkWh = CASE WHEN ExportElectric_cnt >= 3 OR ISNULL(rg.PostProcess, 'N') <> 'Y' THEN ExportElectric_CostPerkWh END
, ExportTot_CostPer
, NetEnergyCons_CostPer
, PyroFurnInletTemp_C
, PyroFurnInletTemp_F
, PyroFurnPreheat_Pcnt
, PyroFurnFlueGasTemp_C
, PyroFurnFlueGasTemp_F
, PyroFurnFlueGasO2_Pcnt
, PyroFurnPrimaryTLE_Pcnt
, PyroFurnSecondaryTLE_Pcnt
, PyroFurnTLESteam_PSIa
, TLEOutletTemp_C
, TLEOutletTemp_F
, CoolingWater_MTPerHVCMT
, CoolingWaterDelta_C
, CoolingWaterDelta_F
, CoolingWaterEnergy_BtuLbHVC
, CoolingWaterEnergy_GJMTHVC
, FinFanEnergy_BtuLb
, FinFanEnergy_GJMT
, CompGasEfficiency_Pcnt
, EffCalcCompany_Pcnt
, EffCalcAPI_Pcnt
, SteamExtraction_MtMtHVC
, SteamCondensing_MtMtHVC
, ProcessedHeatExchanged_BTULbHVC
, ProcessedHeatExchanged_GJMtHVC
, QuenchHeatMatl_Pcnt
, QuenchDilution_Pcnt
, PurFuelNatural_cnt
, PurEthane_cnt
, PurPropane_cnt
, PurResidual_cnt
, PurOther_cnt
, PurSteamSHP_cnt
, PurSteamHP_cnt
, PurSteamIP_cnt
, PurSteamLP_cnt
, PurElec_cnt
, PPFCFuelGas_cnt
, PPFCOther_cnt
, ExportSteamSHP_cnt
, ExportSteamHP_cnt
, ExportSteamIP_cnt
, ExportSteamLP_cnt
, ExportElectric_cnt
, EnergyDeteriation_Pcnt
, IBSLSteamRequirement_Pcnt
, DivisorValue
, DivisorAvgValue
, tsModified
  FROM reports.EnergyRaw re LEFT JOIN reports.ReportGroups rg ON rg.GroupId = re.GroupId
  









