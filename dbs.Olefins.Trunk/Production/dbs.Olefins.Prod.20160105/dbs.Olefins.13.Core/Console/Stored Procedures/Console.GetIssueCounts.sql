﻿CREATE PROCEDURE [Console].[GetIssueCounts]
	@RefNum varchar(18),
	@Mode varchar(1)

AS
BEGIN

	SELECT COUNT(IssueID) as Issues FROM Val.Checklist 
	WHERE Refnum = dbo.FormatRefNum('20' + @RefNum,0)
	AND completed = @Mode or @Mode='A'

END
