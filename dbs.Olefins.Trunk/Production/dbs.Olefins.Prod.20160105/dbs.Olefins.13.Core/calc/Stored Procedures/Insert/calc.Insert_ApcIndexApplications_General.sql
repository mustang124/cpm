﻿CREATE PROCEDURE [calc].[Insert_ApcIndexApplications_General]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ApcIndexApplications]([FactorSetId], [Refnum], [CalDateKey], [ApcId], [AbsenceCorrectionFactor], [OnLine_Pcnt], [Mpc_Int], [Sep_Int], [Mpc_Value], [Sep_Value])
		SELECT
			tsq.FactorSetId,
			tsq.Refnum,
			tsq.Plant_QtrDateKey,
			w.[ApcId],
			c._AbsenceCorrectionFactor,
			COALESCE(o.OnLine_Pcnt, 0.0)	[OnLine_Pcnt],
			CASE WHEN k.Controller_Count >= 1 THEN 1 ELSE 0 END	[Mpc_Int],
			0								[Sep_Int],
			w.[Mpc_Value],
			0.0								[Sep_Value]
		FROM @fpl												tsq
		INNER JOIN calc.ApcAbsenceCorrection					c
			ON	c.FactorSetId	= tsq.FactorSetId
			AND	c.Refnum		= tsq.Refnum
			AND	c.CalDateKey	= tsq.Plant_AnnDateKey
		INNER JOIN ante.ApcWeighting							w
			ON	w.FactorSetId	= tsq.FactorSetId
			AND	w.ApcId			IN ('ApcFurnFeed', 'ApcTowerComp', 'ApcTowerOther', 'ApcTowerPropylene')
		LEFT OUTER JOIN fact.ApcControllers						k
			ON	k.Refnum		= tsq.Refnum
			AND	k.CalDateKey	= tsq.Plant_QtrDateKey
			AND	k.ApcId			= w.ApcId
		LEFT OUTER JOIN fact.ApcOnLinePcnt						o
			ON	o.Refnum		= tsq.Refnum
			AND	o.CalDateKey	= tsq.Plant_QtrDateKey
			AND	o.ApcId			= 'ProdRecovery'
		WHERE	tsq.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;