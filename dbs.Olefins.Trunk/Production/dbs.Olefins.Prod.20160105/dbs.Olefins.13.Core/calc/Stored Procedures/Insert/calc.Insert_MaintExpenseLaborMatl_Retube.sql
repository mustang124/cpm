﻿CREATE PROCEDURE [calc].[Insert_MaintExpenseLaborMatl_Retube]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[MaintExpenseLaborMatl]([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [AccountId], [Amount_Cur])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			fpl.[CurrencyRpt],
			'MaintRetubeLabor',
			f.[InflAdjAnn_MaintRetubeLabor_Cur]
		FROM @fpl								fpl
		INNER JOIN [calc].[FurnacesAggregate]	f
			ON	f.[FactorSetId]	= fpl.[FactorSetId]
			AND	f.[Refnum]		= fpl.[Refnum]
			AND	f.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	f.[InflAdjAnn_MaintRetubeLabor_Cur]	IS NOT NULL
		INNER JOIN [fact].[PersAggregate]		p	WITH (NOEXPAND)
			ON	p.[FactorSetId]	= fpl.[FactorSetId]
			AND	p.[Refnum]		= fpl.[Refnum]
			AND	p.[CalDateKey]	= fpl.[Plant_QtrDateKey]
			AND	p.[PersId]		= 'OccMaintPlant'
			AND(p.[Company_Hrs]	IS NULL
			OR	p.[Company_Hrs] = 0.0)
		WHERE	fpl.[CalQtr] = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;