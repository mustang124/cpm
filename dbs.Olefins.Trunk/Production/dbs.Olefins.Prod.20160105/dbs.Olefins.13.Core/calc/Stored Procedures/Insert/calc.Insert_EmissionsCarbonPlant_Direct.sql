﻿CREATE PROCEDURE [calc].[Insert_EmissionsCarbonPlant_Direct]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY
	
		INSERT INTO calc.EmissionsCarbonPlant(FactorSetId, Refnum, CalDateKey, EmissionsId, Quantity_MBtu, CefGwp, EmissionsCarbon_MTCO2e)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			ef.EmissionsId,
			lhv.LHValue_MBtu,
			ef.CEF_MTCO2_MBtu,
			lhv.LHValue_MBtu * ef.CEF_MTCO2_MBtu
		FROM @fpl									fpl
		INNER JOIN ante.EmissionsFactorCarbon		ef
			ON	ef.FactorSetId = fpl.FactorSetId
			AND	ef.CEF_MTCO2_MBtu IS NOT NULL
		INNER JOIN fact.EnergyLHValue				lhv
			ON	lhv.Refnum = fpl.Refnum
			AND	lhv.CalDateKey = fpl.Plant_QtrDateKey
			AND	'CO2' + lhv.AccountId = ef.EmissionsId
			AND	lhv.AccountId IN (SELECT d.DescendantId FROM dim.Account_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.AccountId IN ('PPFC', 'PurchasedFuel'))
		WHERE fpl.CalQtr = 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;