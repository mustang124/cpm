﻿CREATE PROCEDURE [calc].[Insert_Efficiency_HydroPur]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @HydroPur	VARCHAR(42);

		SELECT TOP 1
			@HydroPur = f.[FacilityId]
		FROM @fpl							fpl
		INNER JOIN [dim].[Facility_Bridge]	b
			ON	b.[FactorSetId]	= fpl.[FactorSetId]
			AND	b.[FacilityId]	= 'HydroPur'
		INNER JOIN [fact].[Facilities]		f
			ON	f.[Refnum]		= fpl.[Refnum]
			AND	f.[FacilityId]	= b.[DescendantId]
			AND	f.Unit_Count	> 0
		WHERE fpl.[CalQtr] = 4
			AND	fpl.[FactorSet_QtrDateKey]	> 20130000
		ORDER BY b.[Hierarchy] ASC;

		INSERT INTO calc.Efficiency([FactorSetId], [Refnum], [CalDateKey], [EfficiencyId], [UnitId], [EfficiencyStandard])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			wCdu.[EfficiencyId],
			uef.[UnitId],

			uef.[Coefficient]
			* POWER(
				[calc].[MaxValue](
					[calc].[MinValue](
						uef.[ValueMax],
						(423.3 + 49.33 * 3.0) * 1.1 * ABS(sqa.[Quantity_kMT]) * comp.[Component_WtPcnt] / cUtil.[_UtilizationStream_Pcnt] / 365.0 * 1000.0
							/ CONVERT(REAL, fac.[Unit_Count])
						),
					uef.[ValueMin]
					),
				uef.[Exponent])
			* (423.3 + 49.33 * 3.0) * 1.1 * ABS(sqa.[Quantity_kMT]) * comp.[Component_WtPcnt] / cUtil.[_UtilizationStream_Pcnt] / 365.0 * 1000.0
			* wCdu.[Coefficient]
			/ 1000.0
		FROM @fpl										fpl
		INNER JOIN ante.UnitEfficiencyWWCdu				wCdu
			ON	wCdu.[FactorSetId]	= fpl.[FactorSetId]
		INNER JOIN [fact].[FacilitiesCount]				fac
			ON	fac.[FactorSetId]	= fpl.[FactorSetId]
			AND	fac.[Refnum]		= fpl.[Refnum]
			AND	fac.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	fac.[FacilityId]	= 'HydroPur'
			AND	fac.[Unit_Count]	> 0
		INNER JOIN ante.UnitEfficiencyFactors			uef
			ON	uef.[FactorSetId]	= fpl.[FactorSetId]
			AND	uef.[EfficiencyId]	= wCdu.[EfficiencyId]
			AND	uef.[UnitId]		= @HydroPur
		INNER JOIN [calc].[CapacityUtilization]			cUtil
			ON	cUtil.[FactorSetId]	= fpl.[FactorSetId]
			AND	cUtil.[Refnum]		= fpl.[Refnum]
			AND	cUtil.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	cUtil.[SchedId]		= 'P'
			AND	cUtil.[StreamId]	= 'ProdOlefins'
		INNER JOIN [calc].[CompositionStream]			comp
			ON	comp.[FactorSetId]	= fpl.[FactorSetId]
			AND	comp.[Refnum]		= fpl.[Refnum]
			AND	comp.[CalDateKey]	= fpl.[Plant_AnnDateKey]
			AND	comp.[StreamId]		= 'Hydrogen'
			AND	comp.[ComponentId]	= 'H2'
		INNER JOIN fact.[StreamQuantityAggregate]		sqa		WITH (NOEXPAND)
			ON	sqa.[FactorSetId]	= fpl.[FactorSetId]
			AND	sqa.[Refnum]		= fpl.[Refnum]
			AND	sqa.[StreamId]		= 'Hydrogen'
		WHERE	fpl.CalQtr			= 4
			AND fac.[Unit_Count]	> 0
			AND comp.[Component_WtPcnt] > 40.0;
		
	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;