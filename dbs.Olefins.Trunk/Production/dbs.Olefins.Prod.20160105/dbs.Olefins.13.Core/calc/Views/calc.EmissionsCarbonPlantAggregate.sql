﻿CREATE VIEW [calc].[EmissionsCarbonPlantAggregate]
WITH SCHEMABINDING
AS
SELECT
	ecp.FactorSetId,
	ecp.Refnum,
	ecp.CalDateKey,
	b.EmissionsId,
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecp.Quantity_kMT
		WHEN '-' THEN -	ecp.Quantity_kMT
		END)				[Quantity_kMT],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecp.Quantity_MBtu
		WHEN '-' THEN -	ecp.Quantity_MBtu
		END)				[Quantity_MBtu],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecp.EmissionsCarbon_MTCO2e
		WHEN '-' THEN -	ecp.EmissionsCarbon_MTCO2e
		END)/
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ISNULL(ecp.Quantity_MBtu, ecp.Quantity_kMT)
		WHEN '-' THEN -	ISNULL(ecp.Quantity_MBtu, ecp.Quantity_kMT)
		END)				[CefGwp],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN	ecp.EmissionsCarbon_MTCO2e
		WHEN '-' THEN -	ecp.EmissionsCarbon_MTCO2e
		END)				[EmissionsCarbon_MTCO2e]

FROM calc.EmissionsCarbonPlant				ecp
INNER JOIN dim.EmissionsPlant_Bridge		b
	ON	b.FactorSetId	= ecp.FactorSetId
	AND	b.DescendantId	= ecp.EmissionsId
GROUP BY
	ecp.FactorSetId,
	ecp.Refnum,
	ecp.CalDateKey,
	b.EmissionsId;