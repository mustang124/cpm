﻿CREATE VIEW [calc].[CapacityProjected]
WITH SCHEMABINDING
AS
SELECT
	c.FactorSetId,
	c.Refnum,
	c.CalDateKey,
	c.StreamId,
	c.Capacity_kMT							[CurrentCapacity_kMT],
	g.CalDateKey							[Projected_CalDateKey],
	g.Capacity_kMT							[Projected_Capacity_kMT],
	g.Capacity_kMT / c.Capacity_kMT * 100.0	[Projected_CapIndex_Pcnt],
	g.StudyYearDifference
FROM calc.CapacityGrowthAggregate			c
INNER JOIN calc.CapacityGrowthAggregate			g
	ON	g.FactorSetId = c.FactorSetId
	AND	g.Refnum = c.Refnum
	AND	g.StreamId = c.StreamId
	AND	g.StudyYearDifference = -3
WHERE c.StudyYearDifference = 0
	AND c.Capacity_kMT <> 0.0