﻿CREATE VIEW [calc].[MaintCostAvgAggregate]
WITH SCHEMABINDING
AS
SELECT
	a.[FactorSetId],
	a.[Refnum],
	a.[CalDateKey],
	b.[AccountId],
	a.[CurrencyRpt],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + a.[MaintPrev_Cur]
		WHEN '-' THEN - a.[MaintPrev_Cur]
		ELSE 0.0
		END)							[MaintPrev_Cur],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + a.[MaintCurr_Cur]
		WHEN '-' THEN - a.[MaintCurr_Cur]
		ELSE 0.0
		END)							[MaintCurr_Cur],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + a.[_MaintAvg_Cur]
		WHEN '-' THEN - a.[_MaintAvg_Cur]
		ELSE 0.0
		END)							[MaintAvg_Cur],
	COUNT_BIG(*)						[IndexItems]
FROM dim.AccountMaint_Bridge		b
INNER JOIN calc.MaintCostAverage	a
	ON	a.[FactorSetId]		= b.[FactorSetId]
	AND	a.[AccountId]		= b.[DescendantId]
GROUP BY
	a.[FactorSetId],
	a.[Refnum],
	a.[CalDateKey],
	b.[AccountId],
	a.[CurrencyRpt];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_MaintCostAvgAggregate]
    ON [calc].[MaintCostAvgAggregate]([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC);

