﻿CREATE VIEW [calc].[CompositionYieldPlantAggregate]
WITH SCHEMABINDING
AS
SELECT
	s.FactorSetId,
	s.Refnum,
	MAX(s.CalDateKey)	[CalDateKey],
	s.SimModelId,
	s.OpCondId,
	b.ComponentId,
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + s.Component_kMT
		WHEN '-' THEN - s.Component_kMT
		END)								[Component_kMT],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + s.Component_WtPcnt
		WHEN '-' THEN - s.Component_WtPcnt
		END)								[Component_WtPcnt],

	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + s.Component_Extrap_kMT
		WHEN '-' THEN - s.Component_Extrap_kMT
		END)								[Component_Extrap_kMT],

	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + s.Component_Extrap_WtPcnt
		WHEN '-' THEN - s.Component_Extrap_WtPcnt
		END)								[Component_Extrap_WtPcnt],

	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + s.Component_Supp_kMT
		WHEN '-' THEN - s.Component_Supp_kMT
		END)								[Component_Supp_kMT]
FROM calc.CompositionYieldPlant		s
INNER JOIN dim.Component_Bridge		b
	ON	b.FactorSetId = s.FactorSetId
	AND	b.DescendantId = s.ComponentId
GROUP BY
	s.FactorSetId,
	s.Refnum,
	s.SimModelId,
	s.OpCondId,
	b.ComponentId
HAVING 
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + s.Component_kMT
		WHEN '-' THEN - s.Component_kMT
		END) IS NOT NULL;