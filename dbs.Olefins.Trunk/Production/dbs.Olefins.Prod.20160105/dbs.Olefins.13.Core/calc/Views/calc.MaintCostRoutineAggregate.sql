﻿CREATE VIEW [calc].[MaintCostRoutineAggregate]
WITH SCHEMABINDING
AS
SELECT
	m.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	b.FacilityId,
	m.CurrencyRpt,
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(m.[MaintMaterial_Cur], 0.0)
		WHEN '-' THEN - ISNULL(m.[MaintMaterial_Cur], 0.0)
		ELSE 0.0
		END)							[MaintMaterial_Cur],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(m.[MaintLabor_Cur], 0.0)
		WHEN '-' THEN - ISNULL(m.[MaintLabor_Cur], 0.0)
		ELSE 0.0
		END)							[MaintLabor_Cur],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(m.[_Maint_Cur], 0.0)
		WHEN '-' THEN - ISNULL(m.[_Maint_Cur], 0.0)
		ELSE 0.0
		END)							[Maint_Cur],
	COUNT_BIG(*)						[IndexItems]
FROM dim.Facility_Bridge			b
INNER JOIN calc.MaintCostRoutine	m
	ON	m.FactorSetId	= b.FactorSetId
	AND	m.FacilityId	= b.DescendantId
GROUP BY
	m.FactorSetId,
	m.Refnum,
	m.CalDateKey,
	b.FacilityId,
	m.CurrencyRpt;
GO
CREATE UNIQUE CLUSTERED INDEX [UX_MaintCostRoutineAggregate]
    ON [calc].[MaintCostRoutineAggregate]([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [FacilityId] ASC, [CalDateKey] ASC);

