﻿CREATE TABLE [calc].[PricingPlantStream] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [ScenarioId]             VARCHAR (42)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [CurrencyRpt]            VARCHAR (4)        NOT NULL,
    [AccountId]              VARCHAR (42)       NULL,
    [StreamId]               VARCHAR (42)       NOT NULL,
    [StreamDescription]      NVARCHAR (256)     NOT NULL,
    [Quantity_kMT]           REAL               NOT NULL,
    [Client_Amount_Cur]      REAL               NULL,
    [Region_Amount_Cur]      REAL               NULL,
    [Country_Amount_Cur]     REAL               NULL,
    [Location_Amount_Cur]    REAL               NULL,
    [Composition_Amount_Cur] REAL               NULL,
    [Energy_Amount_Cur]      REAL               NULL,
    [Adj_CompPurity_Cur]     REAL               NULL,
    [Adj_NonStdStream_Coeff] REAL               NULL,
    [Adj_Logistics_Cur]      REAL               NULL,
    [Supersede_Amount_Cur]   REAL               NULL,
    [_Calc_Amount_Cur]       AS                 (CONVERT([real],isnull([Supersede_Amount_Cur],isnull([Adj_NonStdStream_Coeff],(1.0))*(isnull([Adj_Logistics_Cur],(0.0))+coalesce([Energy_Amount_Cur],[Composition_Amount_Cur]+isnull([Adj_CompPurity_Cur],(0.0)),[Location_Amount_Cur],[Country_Amount_Cur],[Region_Amount_Cur],[Client_Amount_Cur]))),(1))) PERSISTED NOT NULL,
    [_Stream_Value_Cur]      AS                 (CONVERT([real],[Quantity_kMT]*isnull([Supersede_Amount_Cur],isnull([Adj_NonStdStream_Coeff],(1.0))*(isnull([Adj_Logistics_Cur],(0.0))+coalesce([Energy_Amount_Cur],[Composition_Amount_Cur]+isnull([Adj_CompPurity_Cur],(0.0)),[Location_Amount_Cur],[Country_Amount_Cur],[Region_Amount_Cur],[Client_Amount_Cur]))),(1))) PERSISTED NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPlantStream_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_PricingPlantStream_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_PricingPlantStream_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_PricingPlantStream_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_PricingPlantStream] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ScenarioId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [StreamId] ASC, [StreamDescription] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingPlantStream_Client_Amount_Cur] CHECK ([Client_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantStream_Composition_Amount_Cur] CHECK ([Composition_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantStream_Country_Amount_Cur] CHECK ([Country_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantStream_Location_Amount_Cur] CHECK ([Location_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantStream_Region_Amount_Cur] CHECK ([Region_Amount_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPlantStream_Supersede_Amount_Cur] CHECK ([Supersede_Amount_Cur]>=(0.0)),
    CONSTRAINT [CV_PricingPlantStream_Amount_Cur] CHECK ([Composition_Amount_Cur] IS NOT NULL OR [Location_Amount_Cur] IS NOT NULL OR [Region_Amount_Cur] IS NOT NULL OR [Country_Amount_Cur] IS NOT NULL OR [Client_Amount_Cur] IS NOT NULL OR [Energy_Amount_Cur] IS NOT NULL OR [Supersede_Amount_Cur] IS NOT NULL),
    CONSTRAINT [FK_calc_PricingPlantStream_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_calc_PricingPlantStream_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_PricingPlantStream_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_calc_PricingPlantStream_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_PricingPlantStream_Scenarios] FOREIGN KEY ([FactorSetId], [ScenarioId]) REFERENCES [ante].[PricingScenarioConfig] ([FactorSetId], [ScenarioId]),
    CONSTRAINT [FK_calc_PricingPlantStream_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_PricingPlantStream_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_PricingPlantStream_Margin]
    ON [calc].[PricingPlantStream]([Refnum] ASC)
    INCLUDE([FactorSetId], [CalDateKey], [CurrencyRpt], [StreamId], [_Stream_Value_Cur]);


GO
CREATE TRIGGER calc.t_PricingPlantStream_u
	ON  calc.[PricingPlantStream]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.[PricingPlantStream]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.[PricingPlantStream].FactorSetId			= INSERTED.FactorSetId
		AND calc.[PricingPlantStream].ScenarioId			= INSERTED.ScenarioId
		AND calc.[PricingPlantStream].Refnum				= INSERTED.Refnum
		AND calc.[PricingPlantStream].CalDateKey			= INSERTED.CalDateKey
		AND calc.[PricingPlantStream].CurrencyRpt			= INSERTED.CurrencyRpt
		AND calc.[PricingPlantStream].StreamId			= INSERTED.StreamId
		AND calc.[PricingPlantStream].StreamDescription	= INSERTED.StreamDescription;
		
	
	
END;