﻿CREATE TABLE [calc].[CapacityPlant] (
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [Study_Date]        DATE               NOT NULL,
    [Expansion_Date]    DATE               NULL,
    [Expansion_Pcnt]    REAL               NULL,
    [Stream_MTd]        REAL               NULL,
    [_StreamPrev_MTd]   AS                 (CONVERT([real],([Stream_MTd]/((100.0)+[Expansion_Pcnt]))*(100.0),(1))) PERSISTED,
    [_StreamAvg_MTd]    AS                 ((CONVERT([real],datediff(day,case when dateadd(year,(-1),[Study_Date])>isnull([Expansion_Date],CONVERT([date],'19000101',(112))) then dateadd(year,(-1),[Study_Date]) else isnull([Expansion_Date],CONVERT([date],'19000101',(112))) end,[Study_Date]),(1))/(365.0))*([Stream_MTd]-([Stream_MTd]/((100.0)+isnull([Expansion_Pcnt],(0.0))))*(100.0))+([Stream_MTd]/((100.0)+isnull([Expansion_Pcnt],(0.0))))*(100.0)) PERSISTED,
    [Stream_kMT]        REAL               NULL,
    [_StreamPrev_kMT]   AS                 (CONVERT([real],(([Stream_MTd]*(0.365))/((100.0)+[Expansion_Pcnt]))*(100.0),(1))) PERSISTED,
    [_StreamAvg_kMT]    AS                 ((CONVERT([real],datediff(day,case when dateadd(year,(-1),[Study_Date])>isnull([Expansion_Date],CONVERT([date],'19000101',(112))) then dateadd(year,(-1),[Study_Date]) else isnull([Expansion_Date],CONVERT([date],'19000101',(112))) end,[Study_Date]),(1))/(365.0))*([Stream_kMT]-(([Stream_MTd]*(0.365))/((100.0)+isnull([Expansion_Pcnt],(0.0))))*(100.0))+([Stream_kMT]/((100.0)+isnull([Expansion_Pcnt],(0.0))))*(100.0)) PERSISTED,
    [Capacity_kMT]      REAL               NULL,
    [_CapacityPrev_kMT] AS                 (CONVERT([real],([Capacity_kMT]/((100.0)+[Expansion_Pcnt]))*(100.0),(1))) PERSISTED,
    [_CapacityAvg_kMT]  AS                 ((CONVERT([real],datediff(day,case when dateadd(year,(-1),[Study_Date])>isnull([Expansion_Date],CONVERT([date],'19000101',(112))) then dateadd(year,(-1),[Study_Date]) else isnull([Expansion_Date],CONVERT([date],'19000101',(112))) end,[Study_Date]),(1))/(365.0))*([Capacity_kMT]-([Capacity_kMT]/((100.0)+isnull([Expansion_Pcnt],(0.0))))*(100.0))+([Capacity_kMT]/((100.0)+isnull([Expansion_Pcnt],(0.0))))*(100.0)) PERSISTED,
    [_BegCapacity_Days] AS                 (((365)-datediff(day,[Expansion_Date],[Study_Date]))-(1)),
    [_EndCapacity_Days] AS                 (isnull(datediff(day,[Expansion_Date],[Study_Date])+(1),(365))),
    [_StreamWtAvg_kMT]  AS                 ((isnull(datediff(day,[Expansion_Date],[Study_Date])+(1),(365))*[Stream_MTd]+(((365)-isnull(datediff(day,[Expansion_Date],[Study_Date])-(1),(365)))*([Stream_MTd]/((100.0)+[Expansion_Pcnt])))*(100.0))/(365.0)),
    [_Ann_Stream_kMT]   AS                 ((isnull(datediff(day,[Expansion_Date],[Study_Date])+(1),(365))*[Stream_MTd]+(((365)-isnull(datediff(day,[Expansion_Date],[Study_Date])-(1),(365)))*([Stream_MTd]/((100.0)+[Expansion_Pcnt])))*(100.0))/(1000.0)),
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_CapacityPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_CapacityPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_CapacityPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_CapacityPlant] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_CapacityPlant_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_CapacityPlant_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_CapacityPlant_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_CapacityPlant_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_CapacityPlant_ReliabilityIndicator]
    ON [calc].[CapacityPlant]([StreamId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [_StreamAvg_kMT], [_StreamAvg_MTd]);


GO
CREATE TRIGGER calc.t_CapacityPlant_u
	ON  calc.CapacityPlant
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.CapacityPlant
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.CapacityPlant.FactorSetId	= INSERTED.FactorSetId
		AND calc.CapacityPlant.Refnum		= INSERTED.Refnum
		AND calc.CapacityPlant.CalDateKey	= INSERTED.CalDateKey
		AND calc.CapacityPlant.StreamId		= INSERTED.StreamId;
			
END