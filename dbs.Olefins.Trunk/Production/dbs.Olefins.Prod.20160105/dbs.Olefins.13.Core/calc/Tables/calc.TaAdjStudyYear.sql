﻿CREATE TABLE [calc].[TaAdjStudyYear] (
    [FactorSetId]               VARCHAR (12)       NOT NULL,
    [Refnum]                    VARCHAR (25)       NOT NULL,
    [CalDateKey]                INT                NOT NULL,
    [TurnAroundInStudyYear_Bit] BIT                NOT NULL,
    [tsModified]                DATETIMEOFFSET (7) CONSTRAINT [DF_TaAdjStudyYear_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]            NVARCHAR (168)     CONSTRAINT [DF_TaAdjStudyYear_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]            NVARCHAR (168)     CONSTRAINT [DF_TaAdjStudyYear_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]             NVARCHAR (168)     CONSTRAINT [DF_TaAdjStudyYear_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_TaAdjStudyYear] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_TaAdjStudyYear_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_TaAdjStudyYear_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_TaAdjStudyYear_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_TaAdjStudyYear_u
	ON  calc.TaAdjStudyYear
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.TaAdjStudyYear
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.TaAdjStudyYear.FactorSetId		= INSERTED.FactorSetId
		AND calc.TaAdjStudyYear.Refnum			= INSERTED.Refnum
		AND calc.TaAdjStudyYear.CalDateKey		= INSERTED.CalDateKey;
			
END