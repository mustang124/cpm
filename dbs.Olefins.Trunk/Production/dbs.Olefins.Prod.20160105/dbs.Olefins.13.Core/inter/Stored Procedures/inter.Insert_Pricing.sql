﻿CREATE PROCEDURE [inter].[Insert_Pricing]
(
	@FactorSetId			VARCHAR(12) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		EXECUTE [inter].[Insert_PricingStaging]			@FactorSetId;
		EXECUTE [inter].[Insert_PricingStream]			@FactorSetId;
		EXECUTE [inter].[Insert_PricingComposition]		@FactorSetId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, 'N/A';

		RETURN ERROR_NUMBER();

	END CATCH;

END;