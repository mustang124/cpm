﻿CREATE PROCEDURE [inter].[Insert_ReplValEncEstimate]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO inter.[ReplValEncEstimate](FactorSetId, Refnum, CalDateKey, CurrencyRpt, ComponentId,
			Component_kMT, Component_WtPcnt, [YieldEst_RoughWt],
			Est_C2Yield_EstWtPcnt, Act_C2Yield_EstWtPcnt,
			Escalated_ENC_Cur, EthyleneCapBasis)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			enc.CurrencyRpt,
			enc.ComponentId,

			SUM(c.Component_kMT)								[Component_kMT],

			SUM(c.Component_kMT)
				/ SUM(SUM(c.Component_kMT)) OVER(PARTITION BY fpl.FactorSetId, fpl.Refnum, fpl.Plant_AnnDateKey) * 100.0
																[Component_WtPcnt],

			yp.YieldEstimate_Pcnt,

			SUM(c.Component_kMT)
				/ SUM(SUM(c.Component_kMT)) OVER(PARTITION BY fpl.FactorSetId, fpl.Refnum, fpl.Plant_AnnDateKey) * yp.YieldEstimate_Pcnt
																[Est_C2Yield_EstWtPcnt],

			SUM(c.Component_kMT * yp.YieldEstimate_Pcnt)
				/ SUM(SUM(c.Component_kMT * yp.YieldEstimate_Pcnt)) OVER(PARTITION BY fpl.FactorSetId, fpl.Refnum, fpl.Plant_AnnDateKey) * 100.0
																[Act_C2Yield_EstWtPcnt],

			enc.Escalated_ENC_Cur,
			SUM(c.Component_kMT * yp.YieldEstimate_Pcnt)
				/ SUM(SUM(c.Component_kMT * yp.YieldEstimate_Pcnt)) OVER(PARTITION BY fpl.FactorSetId, fpl.Refnum, fpl.Plant_AnnDateKey) * enc.Escalated_ENC_Cur
																[EthyleneCapBasis]

		FROM @fpl										fpl

		INNER JOIN ante.EngrConstEstimate				enc
			--ON	enc.FactorSetId = fpl.FactorSetId
			ON	CONVERT(SMALLINT, enc.FactorSetId) = fpl.DataYear
			AND	enc.CurrencyRpt	= 'USD'

		INNER JOIN ante.C2YieldPercent					yp
			ON	yp.FactorSetId = fpl.FactorSetId
			AND	yp.ComponentId = enc.ComponentId

		LEFT OUTER JOIN (
			SELECT ALL
				c.FactorSetId,
				c.Refnum,
				c.CalDateKey,
				CASE
					WHEN c.ComponentId IN ('CH4', 'C2H6', 'C2H4', 'CO_CO2', 'H', 'S')	THEN 'C2H6'
					WHEN c.ComponentId IN ('C3H8', 'C3H6')								THEN 'C3H8'
					ELSE																	 'C4H10'
				END									[ComponentId],
				c.Component_kMT						[Component_kMT]
			FROM calc.CompositionStream						c
			WHERE	c.SimModelId = 'Plant'
				AND	c.OpCondId = 'PlantFeed'
				--	Task 141: Implement Tri-Tables in calculations
				--AND	c.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('Light', 1, '') d)
				AND	c.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = c.FactorSetId AND d.StreamId = 'Light')
				AND	c.Component_kMT > 0.0

			UNION ALL
			SELECT ALL
				fpl.FactorSetId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey,
				CASE
					--	Task 141: Implement Tri-Tables in calculations
					--WHEN q.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('LiqLight', 1, '') d)	THEN  'LiqLight'
					--WHEN q.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('LiqHeavy', 1, '') d)	THEN  'LiqHeavy'
					WHEN q.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'LiqLight')	THEN  'LiqLight'
					WHEN q.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'LiqHeavy')	THEN  'LiqHeavy'
					WHEN cp.Density_SG >= 0.78 THEN 'LiqHeavy'
					ELSE 'LiqLight'
				END
													[ComponentId],
				q.Quantity_kMT						[Component_kMT]
			FROM @fpl										fpl
			INNER JOIN fact.Quantity						q
				ON	q.Refnum = fpl.Refnum
				AND	q.CalDateKey = fpl.Plant_QtrDateKey
				--	Task 141: Implement Tri-Tables in calculations
				--AND	q.StreamId IN (SELECT d.DescendantId FROM dim.StreamLuDescendants('Liquid', 1, '') d)
				AND	q.StreamId IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fpl.FactorSetId AND d.StreamId = 'Liquid')
			INNER JOIN fact.FeedStockCrackingParameters		cp
				ON	cp.Refnum = fpl.Refnum
				AND	cp.CalDateKey = fpl.Plant_AnnDateKey
				AND	cp.StreamId = q.StreamId
				AND	cp.StreamDescription = q.StreamDescription

			UNION ALL
			SELECT ALL
				c.FactorSetId,
				c.Refnum,
				c.CalDateKey,
				c.ComponentId,
				c._Recycled_kMT						[Component_kMT]
			FROM calc.QuantityRecycled					c
			WHERE c._Recycled_kMT > 0.0

			) c
			ON	c.FactorSetId = fpl.FactorSetId
			AND	c.Refnum = fpl.Refnum
			AND	c.CalDateKey = fpl.Plant_QtrDateKey
			AND	c.ComponentId = enc.ComponentId

		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_AnnDateKey,
			enc.CurrencyRpt,
			enc.ComponentId,
			enc.Escalated_ENC_Cur,
			yp.YieldEstimate_Pcnt

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;