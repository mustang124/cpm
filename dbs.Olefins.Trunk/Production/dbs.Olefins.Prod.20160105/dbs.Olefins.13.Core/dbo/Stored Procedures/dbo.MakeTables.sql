﻿CREATE PROCEDURE [dbo].[MakeTables] (@Refnum VARCHAR(25) = NULL, @FactorSetId VARCHAR(12) = '' )
AS
BEGIN

	SET NOCOUNT ON;

	SET @Refnum = UPPER(@Refnum);

	IF @FactorSetId = '' SELECT @FactorSetId = [t].[FactorSetId] FROM [dbo].[TSort] [t] WHERE [t].[Refnum] = @Refnum;

	EXECUTE [dbo].[Delete_GapEnergySeparation]		@Refnum;
	EXECUTE [dbo].[Delete_GapReliabilityOppLoss]	@Refnum;
	EXECUTE [dbo].[Delete_GapProductPurity]			@Refnum;
	EXECUTE [dbo].[Delete_MetaMarginCalc]			@Refnum;
	EXECUTE [dbo].[Delete_MetaYield]				@Refnum;

	EXEC dbo.spTrendRefnums			@Refnum;
	EXEC dbo.spDivisors				@Refnum, @FactorSetId;
	EXEC dbo.spFactors				@Refnum, @FactorSetId;
	EXEC dbo.spEnergyCalc			@Refnum, @FactorSetId;
	EXEC dbo.spReliabilityCalc		@Refnum, @FactorSetId;
	EXEC dbo.spMarginCalc			@Refnum, @FactorSetId;
	EXEC dbo.spPyroYieldComposition	@Refnum, @FactorSetId;
	EXEC dbo.spMaintCost			@Refnum, @FactorSetId;

	EXEC dbo.spCapUtil				@Refnum, @FactorSetId;
	EXEC dbo.spCapital				@Refnum, @FactorSetId;
	EXEC dbo.spPyrolysis			@Refnum, @FactorSetId;
	EXEC dbo.spYield				@Refnum, @FactorSetId;
	EXEC dbo.spYieldOther			@Refnum, @FactorSetId;
	EXEC dbo.spFacilities			@Refnum, @FactorSetId;
	EXEC dbo.spEnvironment			@Refnum, @FactorSetId;		-- must be after spEnergyCalc
	EXEC dbo.spEnergy				@Refnum, @FactorSetId;
	EXEC dbo.spFinancial			@Refnum, @FactorSetId;
	EXEC dbo.spInventory			@Refnum, @FactorSetId;
	EXEC dbo.spMaint				@Refnum, @FactorSetId;
	EXEC dbo.spMaintTA				@Refnum, @FactorSetId;
	EXEC dbo.spOpEx					@Refnum, @FactorSetId;
	EXEC dbo.spPersonnel			@Refnum, @FactorSetId;
	EXEC dbo.spPersCost				@Refnum, @FactorSetId;
	EXEC dbo.spReliability			@Refnum, @FactorSetId;
	EXEC dbo.spFeedStocks			@Refnum, @FactorSetId;

	EXECUTE [dbo].[Insert_MetaYield_kMT]			@Refnum, @FactorSetId;
	EXECUTE [dbo].[Insert_MetaYield_MLb]			@Refnum, @FactorSetId;
	EXECUTE [dbo].[Insert_MetaYield_CurPerMT]		@Refnum, @FactorSetId;
	EXECUTE [dbo].[Insert_MetaMarginCalc]			@Refnum, @FactorSetId;

	EXECUTE [dbo].[Insert_GapProductPurity]			@Refnum, @FactorSetId;
	EXECUTE [dbo].[Insert_GapReliabilityOppLoss]	@Refnum, @FactorSetId;
	EXECUTE [dbo].[Insert_GapEnergySeparation]		@Refnum, @FactorSetId;

	EXEC dbo.spGENSUM				@Refnum, @FactorSetId	 -- must be after all tables are built
	EXEC dbo.spInsertMessage		@Refnum, 11, 'MakeTables'

END;