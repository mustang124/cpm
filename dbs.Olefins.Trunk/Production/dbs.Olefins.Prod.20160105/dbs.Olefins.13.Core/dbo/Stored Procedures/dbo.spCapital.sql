﻿






CREATE              PROC [dbo].[spCapital](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON
Declare @Currency varchar(4) = 'USD'

DECLARE @RVAvg real, @EDCAvg_k real
SELECT 
  @RVAvg		= RV_MUS
, @EDCAvg_k		= kEdc 
FROM dbo.Divisors d 
WHERE d.Refnum = @Refnum 

--
--	Only include those that have expansions, either onsite or exist
--
DECLARE @EthyleneCapAddNew_kMT real, @EthyleneCapAddExist_kMT real, @EthyleneCapAddedCnt real
Select 
  @EthyleneCapAddedCnt = COUNT(distinct g.Refnum)
, @EthyleneCapAddNew_kMT = SUM(CASE WHEN g.AccountId = 'OnSite' then CapacityAdded_kMT ELSE 0 END)
, @EthyleneCapAddExist_kMT = SUM(CASE WHEN g.AccountId = 'Exist' then CapacityAdded_kMT ELSE 0 END)
FROM fact.GenPlantExpansion g 
WHERE g.Refnum=@Refnum
GROUP by g.Refnum
HAVING SUM(CASE WHEN g.AccountId IN ('OnSite', 'Exist') then CapacityAdded_kMT ELSE 0 END) > 0

DECLARE @CptlPlantCnt int, @ProjPlantCnt int
DECLARE @CptlOnsite_MUSD real, @CptlOffsite_MUSD real, @CptlExist_MUSD real, @CptlEnergyCons_MUSD real, @CptlRegEnvir_MUSD real, @CptlSafety_MUSD real, @CptlComputer_MUSD real, @CptlMaintCap_MUSD real, @CptlTot_MUSD real
DECLARE @ProjCptlOnsite_MUSD real, @ProjCptlOffsite_MUSD real, @ProjCptlExist_MUSD real, @ProjCptlEnergyCons_MUSD real, @ProjCptlRegEnvir_MUSD real, @ProjCptlSafety_MUSD real, @ProjCptlComputer_MUSD real, @ProjCptlMaintCap_MUSD real, @ProjCptlTot_MUSD real

SELECT 
  @CptlPlantCnt			= COUNT(Distinct g.Refnum)
, @CptlOnsite_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'OnSite' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlOffsite_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'OffSite' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlExist_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'Exist' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlEnergyCons_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'EnergyCons' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlRegEnvir_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'RegEnvir' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlSafety_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'Safety' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlComputer_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'Computer' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlMaintCap_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'MaintCap' THEN g.Amount_Cur ELSE 0 END), 0)
, @CptlTot_MUSD			= ISNULL(SUM(CASE WHEN g.AccountId = 'TotRefCapEx' THEN g.Amount_Cur ELSE 0 END), 0)
FROM fact.GenPlantCapExAggregate g 
WHERE g.Refnum = @Refnum and g.CurrencyRpt = @Currency and g.FactorSetId = @FactorSetId
AND (SELECT Amount_Cur FROM fact.GenPlantCapExAggregate WHERE Refnum = @Refnum and AccountId = 'TotRefCapEx' and FactorSetId = @FactorSetId and CurrencyRpt = @Currency) > 0

SELECT 
  @ProjPlantCnt			= COUNT(Distinct g.Refnum)
, @ProjCptlOnsite_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'OnSite' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlOffsite_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'OffSite' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlExist_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'Exist' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlEnergyCons_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'EnergyCons' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlRegEnvir_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'RegEnvir' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlSafety_MUSD		= ISNULL(SUM(CASE WHEN g.AccountId = 'Safety' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlComputer_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'Computer' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlMaintCap_MUSD	= ISNULL(SUM(CASE WHEN g.AccountId = 'MaintCap' THEN g.Budget_Cur ELSE 0 END), 0)
, @ProjCptlTot_MUSD			= ISNULL(SUM(CASE WHEN g.AccountId = 'TotRefCapEx' THEN g.Budget_Cur ELSE 0 END), 0)
FROM fact.GenPlantCapExAggregate g 
WHERE g.Refnum = @Refnum and g.CurrencyRpt = @Currency and g.FactorSetId = @FactorSetId
AND (SELECT Budget_Cur FROM fact.GenPlantCapExAggregate WHERE Refnum = @Refnum and AccountId = 'TotRefCapEx' and FactorSetId = @FactorSetId and CurrencyRpt = @Currency) > 0

DELETE FROM dbo.Capital WHERE Refnum = @Refnum
Insert into dbo.Capital (Refnum
, Currency
, CptlPlantCnt
, CptlOnsite_MUSD
, CptlOffsite_MUSD
, CptlExist_MUSD
, CptlEnergyCons_MUSD
, CptlRegEnvir_MUSD
, CptlSafety_MUSD
, CptlComputer_MUSD
, CptlMaintCap_MUSD
, CptlTot_MUSD
, CptlTot_PcntRV
, CptlTotPerEDC
, EthyleneCapAddedCnt
, EthyleneCapAddNew_kMT
, EthyleneCapAddExist_kMT
, EthyleneCapAddTot_kMT
, ProjPlantCnt
, ProjCptlOnsite_MUSD
, ProjCptlOffsite_MUSD
, ProjCptlExist_MUSD
, ProjCptlEnergyCons_MUSD
, ProjCptlRegEnvir_MUSD
, ProjCptlSafety_MUSD
, ProjCptlComputer_MUSD
, ProjCptlMaintCap_MUSD
, ProjCptlTot_MUSD
, ProjCptlTot_PcntRV
, ProjCptlTotPerEDC
, RV_MUS
, kEdc)

SELECT GroupId = @Refnum
, Currency = @Currency
, CptlPlantCnt = @CptlPlantCnt
, CptlOnsite_MUSD = @CptlOnsite_MUSD
, CptlOffsite_MUSD = @CptlOffsite_MUSD
, CptlExist_MUSD = @CptlExist_MUSD
, CptlEnergyCons_MUSD = @CptlEnergyCons_MUSD
, CptlRegEnvir_MUSD = @CptlRegEnvir_MUSD
, CptlSafety_MUSD = @CptlSafety_MUSD
, CptlComputer_MUSD = @CptlComputer_MUSD
, CptlMaintCap_MUSD = @CptlMaintCap_MUSD
, CptlTot_MUSD = @CptlTot_MUSD
, CptlTot_PcntRV = CASE WHEN @RVAvg>0 THEN @CptlTot_MUSD / @RVAvg * 100.0 ELSE 0 END
, CptlTotPerEDC = CASE WHEN @EDCAvg_k>0 THEN @CptlTot_MUSD / @EDCAvg_k * 1000.0 ELSE 0 END
, EthyleneCapAddedCnt = @EthyleneCapAddedCnt
, EthyleneCapAddNew_kMT = ISNULL(@EthyleneCapAddNew_kMT,0)
, EthyleneCapAddExist_kMT = ISNULL(@EthyleneCapAddExist_kMT,0)
, EthyleneCapAddTot_kMT = ISNULL(@EthyleneCapAddNew_kMT,0) + ISNULL(@EthyleneCapAddExist_kMT, 0)
, ProjPlantCnt = @ProjPlantCnt
, ProjCptlOnsite_MUSD = @ProjCptlOnsite_MUSD
, ProjCptlOffsite_MUSD = @ProjCptlOffsite_MUSD
, ProjCptlExist_MUSD = @ProjCptlExist_MUSD
, ProjCptlEnergyCons_MUSD = @ProjCptlEnergyCons_MUSD
, ProjCptlRegEnvir_MUSD = @ProjCptlRegEnvir_MUSD
, ProjCptlSafety_MUSD = @ProjCptlSafety_MUSD
, ProjCptlComputer_MUSD = @ProjCptlComputer_MUSD
, ProjCptlMaintCap_MUSD = @ProjCptlMaintCap_MUSD
, ProjCptlTot_MUSD = @ProjCptlTot_MUSD
, ProjCptlTot_PcntRV = CASE WHEN @RVAvg>0 THEN @ProjCptlTot_MUSD / @RVAvg * 100.0 ELSE 0 END
, ProjCptlTotPerEDC = CASE WHEN @EDCAvg_k>0 THEN @ProjCptlTot_MUSD / @EDCAvg_k * 1000.0 ELSE 0 END
, RV_MUS = @RVAvg
, kEdc = @EDCAvg_k


SET NOCOUNT OFF

















