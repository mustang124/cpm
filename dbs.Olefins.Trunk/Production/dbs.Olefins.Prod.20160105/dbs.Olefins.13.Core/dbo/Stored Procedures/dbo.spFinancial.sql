﻿


CREATE              PROC [dbo].[spFinancial](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @ProformaOpExPerUEDC real,@ProformaOpExPerEthyleneMT real,@ProformaNCMPerEthyleneMT real,@ProformaProdCostPerEthyleneMT real,@ProformaOpExPerHVChemMT real,
	@ProformaNCMPerHVChemMT real,@ProformaProdCostPerHVChemMT real,@ProformaROI real
SELECT
  @ProformaOpExPerUEDC			= -SUM(CASE WHEN m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensEDC ELSE 0 END) 
, @ProformaOpExPerEthyleneMT	= -SUM(CASE WHEN m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensProd_MT ELSE 0 END)
, @ProformaOpExPerHVChemMT		= - SUM(CASE WHEN m.MarginAnalysis = 'ProdHVC' THEN m.Amount_SensProd_MT ELSE 0 END)
FROM calc.MarginSensitivityCashAggregate m
where m.FactorSetId = @FactorSetId and m.Refnum=@Refnum

SELECT
  @ProformaNCMPerEthyleneMT		= SUM(CASE WHEN m.MarginId = 'MarginNetCash' and m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensProd_MT ELSE 0 END)
, @ProformaProdCostPerEthyleneMT= - SUM(CASE WHEN m.MarginId = 'ProductionCost' and m.MarginAnalysis = 'Ethylene'  THEN m.Amount_SensProd_MT ELSE 0 END) 
, @ProformaNCMPerHVChemMT		= SUM(CASE WHEN m.MarginId = 'MarginNetCash' and m.MarginAnalysis = 'ProdHVC' THEN m.Amount_SensProd_MT ELSE 0 END)
, @ProformaProdCostPerHVChemMT	= - SUM(CASE WHEN m.MarginId = 'ProductionCost' and m.MarginAnalysis = 'ProdHVC' THEN m.Amount_SensProd_MT ELSE 0 END)
, @ProformaROI					= CASE WHEN d.RV_MUS > 0 THEN
									SUM(CASE WHEN m.MarginId = 'MarginNetCash' and m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensProd_MT  ELSE 0 END) * d.EthyleneCapAvg_kMT / d.RV_MUS * 100 / 1000
									END
FROM calc.MarginSensitivityCases m INNER JOIN dbo.Divisors d on d.Refnum=m.Refnum
where m.FactorSetId = @FactorSetId and m.Refnum=@Refnum
GROUP BY d.Refnum, d.EthyleneCapAvg_kMT , d.RV_MUS
----SELECT
----  @ProformaOpExPerUEDC			= -SUM(CASE WHEN m.MarginId = 'TotCashOpEx' and m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensEDC ELSE 0 END) 
----, @ProformaOpExPerEthyleneMT	= -SUM(CASE WHEN m.MarginId = 'TotCashOpEx' and m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensProd_MT ELSE 0 END)
----, @ProformaNCMPerEthyleneMT		= SUM(CASE WHEN m.MarginId = 'MarginNetCash' and m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensProd_MT ELSE 0 END)
----, @ProformaProdCostPerEthyleneMT= - SUM(CASE WHEN m.MarginId = 'TotCashOpEx' and m.MarginAnalysis = 'Ethylene'  THEN m.Amount_SensProd_MT ELSE 0 END) 
----			- SUM(CASE WHEN m.MarginId = 'PlantFeed' and m.MarginAnalysis = 'Ethylene' THEN m.Amount_SensProd_MT ELSE 0 END)
----, @ProformaOpExPerHVChemMT		= - SUM(CASE WHEN m.MarginId = 'TotCashOpEx' and m.MarginAnalysis = 'ProdHVC' THEN m.Amount_SensProd_MT ELSE 0 END)
----, @ProformaNCMPerHVChemMT		= SUM(CASE WHEN m.MarginId = 'MarginNetCash' and m.MarginAnalysis = 'ProdHVC' THEN m.Amount_SensProd_MT ELSE 0 END)
----, @ProformaProdCostPerHVChemMT	= - SUM(CASE WHEN m.MarginId = 'TotCashOpEx' and m.MarginAnalysis = 'ProdHVC' THEN m.Amount_SensProd_MT ELSE 0 END)
----			- SUM(CASE WHEN m.MarginId = 'PlantFeed' and m.MarginAnalysis = 'ProdHVC'  THEN m.Amount_SensProd_MT ELSE 0 END)
----, @ProformaROI					= CASE WHEN SUM(CASE WHEN m.MarginId = 'TotCashOpEx' and m.MarginAnalysis = 'ProdHVC' THEN d.RV_MUS  ELSE 0 END) > 0 THEN
----									SUM(CASE WHEN m.MarginId = 'MarginNetCash' and m.MarginAnalysis = 'ProdHVC' THEN m.Amount_SensProd_MT * d.HvcProd_kMT  ELSE 0 END) / 
----										SUM(CASE WHEN m.MarginId = 'TotCashOpEx' and m.MarginAnalysis = 'ProdHVC' THEN d.RV_MUS  ELSE 0 END) * 100 / 1000.0
----									END
----FROM calc.MarginSensitivity m INNER JOIN dbo.Divisors d on d.Refnum=m.Refnum
----where m.FactorSetId = @FactorSetId and m.Refnum=@Refnum

DELETE FROM dbo.Financial WHERE Refnum = @Refnum
Insert into dbo.Financial (Refnum
, Currency
, NCMPerEthyleneMT
, NCMPerOlefinsMT
, NCMPerHVChemMT
, EthyleneProdCostPerUEDC
, OlefinsProdCostPerUEDC
, HVChemProdCostPerUEDC
, EthyleneProdCostPerMT
, OlefinsProdCostPerMT
, HVChemProdCostPerMT
, GPVPerUEDC
, RMCPerUEDC
, OpExPerUEDC
, GPVPerRVUsgc
, RMCPerRVUsgc
, GMPerRVUsgc
, OpExPerRVUsgc
, NCMPerRVUsgc
, NMPerRVUsgc
, GPVPerRV
, RMCPerRV
, GMPerRV
, OpExPerRV
, NCMperRV
, ROI
, NCMperRVTAAdj
, NMPerRV
, ProformaOpExPerUEDC
, ProformaOpExPerEthyleneMT
, ProformaNCMPerEthyleneMT
, ProformaProdCostPerEthyleneMT
, ProformaOpExPerHVChemMT
, ProformaNCMPerHVChemMT
, ProformaProdCostPerHVChemMT
, ProformaROI
)

SELECT Refnum = @Refnum
, Currency = mm.Currency
, NCMPerEthyleneMT			= SUM(CASE WHEN mm.DataType = 'ETHProd_MT' AND mm.MarginAnalysis = 'Ethylene' THEN mm.NetCashMargin ELSE 0 END)
, NCMPerOlefinsMT			= SUM(CASE WHEN mm.DataType = 'OLEProd_MT' AND mm.MarginAnalysis = 'ProdOlefins' THEN mm.NetCashMargin ELSE 0 END)
, NCMPerHVChemMT			= SUM(CASE WHEN mm.DataType = 'HVC_MT' AND mm.MarginAnalysis = 'ProdHVC' THEN mm.NetCashMargin ELSE 0 END)
, EthyleneProdCostPerUEDC	= SUM(CASE WHEN mm.DataType = 'UEDC' AND mm.MarginAnalysis = 'Ethylene' THEN mm.ProductionCost ELSE 0 END)
, OlefinsProdCostPerUEDC	= SUM(CASE WHEN mm.DataType = 'UEDC' AND mm.MarginAnalysis = 'ProdOlefins' THEN mm.ProductionCost ELSE 0 END)
, HVChemProdCostPerUEDC		= SUM(CASE WHEN mm.DataType = 'UEDC' AND mm.MarginAnalysis = 'ProdHVC' THEN mm.ProductionCost ELSE 0 END)
, EthyleneProdCostPerMT		= SUM(CASE WHEN mm.DataType = 'ETHProd_MT' AND mm.MarginAnalysis = 'Ethylene' THEN mm.ProductionCost ELSE 0 END)
, OlefinsProdCostPerMT		= SUM(CASE WHEN mm.DataType = 'OLEProd_MT' AND mm.MarginAnalysis = 'ProdOlefins' THEN mm.ProductionCost ELSE 0 END)
, HVChemProdCostPerMT		= SUM(CASE WHEN mm.DataType = 'HVC_MT' AND mm.MarginAnalysis = 'ProdHVC' THEN mm.ProductionCost ELSE 0 END)
, GPVPerUEDC				= SUM(CASE WHEN mm.DataType = 'UEDC' AND mm.MarginAnalysis = 'None' THEN mm.GrossProductValue ELSE 0 END)
, RMCPerUEDC				= SUM(CASE WHEN mm.DataType = 'UEDC' AND mm.MarginAnalysis = 'None' THEN mm.RawMaterialCost ELSE 0 END)
, OpExPerUEDC				= SUM(CASE WHEN mm.DataType = 'UEDC' AND mm.MarginAnalysis = 'None' THEN mm.TotCashOpEx ELSE 0 END)
, GPVPerRVUsgc				= SUM(CASE WHEN mm.DataType = 'RVUSGC' AND mm.MarginAnalysis = 'None' THEN mm.GrossProductValue ELSE 0 END)
, RMCPerRVUsgc				= SUM(CASE WHEN mm.DataType = 'RVUSGC' AND mm.MarginAnalysis = 'None' THEN mm.RawMaterialCost ELSE 0 END)
, GMPerRVUsgc				= SUM(CASE WHEN mm.DataType = 'RVUSGC' AND mm.MarginAnalysis = 'None' THEN mm.GrossMargin ELSE 0 END)
, OpExPerRVUsgc				= SUM(CASE WHEN mm.DataType = 'RVUSGC' AND mm.MarginAnalysis = 'None' THEN mm.TotCashOpEx ELSE 0 END)
, NCMPerRVUsgc				= SUM(CASE WHEN mm.DataType = 'RVUSGC' AND mm.MarginAnalysis = 'None' THEN mm.NetCashMargin ELSE 0 END)
, NMPerRVUsgc				= SUM(CASE WHEN mm.DataType = 'RVUSGC' AND mm.MarginAnalysis = 'None' THEN mm.NetMargin ELSE 0 END)
, GPVPerRV					= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.GrossProductValue ELSE 0 END)
, RMCPerRV					= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.RawMaterialCost ELSE 0 END)
, GMPerRV					= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.GrossMargin ELSE 0 END)
, OpExPerRV					= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.TotCashOpEx ELSE 0 END)
, NCMperRV					= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.NetCashMargin ELSE 0 END)
, ROI						= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.NetCashMargin ELSE 0 END)
, NCMperRVTAAdj				= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.NetCashMarginTAAdj ELSE 0 END)
, NMPerRV					= SUM(CASE WHEN mm.DataType = 'RV' AND mm.MarginAnalysis = 'None' THEN mm.NetMargin ELSE 0 END)
, ProformaOpExPerUEDC			= @ProformaOpExPerUEDC
, ProformaOpExPerEthyleneMT		= @ProformaOpExPerEthyleneMT
, ProformaNCMPerEthyleneMT		= @ProformaNCMPerEthyleneMT
, ProformaProdCostPerEthyleneMT = @ProformaProdCostPerEthyleneMT
, ProformaOpExPerHVChemMT		= @ProformaOpExPerHVChemMT
, ProformaNCMPerHVChemMT		= @ProformaNCMPerHVChemMT
, ProformaProdCostPerHVChemMT   = @ProformaProdCostPerHVChemMT
, ProformaROI					= @ProformaROI
FROM dbo.MarginCalc mm 
WHERE mm.Refnum=@Refnum
GROUP BY mm.Currency

SET NOCOUNT OFF

















