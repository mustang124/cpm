﻿CREATE VIEW [dbo].[DivisorsUnPivot]
AS
SELECT Refnum, unp.Variable, DataType, DivisorValue = Value*Conversion
FROM (SELECT * FROM dbo.Divisors) g
UNPIVOT (Value FOR Variable IN ([kEdc] ,[kUEdc],[EthyleneDiv_kMT],[PropyleneDiv_kMT],[OlefinsDiv_kMT], [EthyleneProd_kMT],[PropyleneProd_kMT],[OlefinsProd_kMT],OlefinsCpbyAvg_kMT,[HvcProd_kMT],[TotalPlantInput_kMT],[PlantFeed_kMT],[FreshPyroFeed_kMT]
      ,[RV_MUS],[RVUSGC_MUS], EIIStd, NEIStd, MEIStd, PEIStd, mPEIStd, nmPEIStd)) unp
JOIN (Values ('kEdc',				'EDC',			1.0)
			,('kUEdc',				'UEDC',			1.0)
			,('HvcProd_kMT',		'HVC_MT',		1.0)
			,('HvcProd_kMT',		'HVC_LB',	[$(DbGlobal)].dbo.UnitsConv(1.0, 'MT', 'kLB'))
			,('EthyleneDiv_kMT',	'ETHDiv_MT',	1.0) 
			,('PropyleneDiv_kMT',	'PRPDiv_MT',	1.0) 
			,('OlefinsDiv_kMT',		'OLEDiv_MT',	1.0) 
			,('EthyleneProd_kMT',	'ETHProd_MT',	1.0) 
			,('PropyleneProd_kMT',	'PRPProd_MT',	1.0) 
			,('OlefinsProd_kMT',	'OLEProd_MT',	1.0) 
			,('OlefinsCpbyAvg_kMT',	'OLECpby_MT',	1.0)
			,('RV_MUS',				'RV',			1.0) 
			,('RVUSGC_MUS',			'RVUSGC',		1.0) 
			,('EIIStd',				'EII',			1.0) 
			,('NEIStd',				'NEI',			1.0) 
			,('MEIStd',				'MEI',			1.0) 
			,('PEIStd',				'PEI',			1.0) 
			,('mPEIStd',			'mPEI',			1.0) 
			,('nmPEIStd',			'npPEI',		1.0) 
			)DT(Variable, DataType, Conversion) on dt.Variable = unp.Variable
UNION
SELECT DISTINCT Refnum, 'None', 'ADJ', 1.0 FROM dbo.Divisors

--select * from DIVISORS