﻿CREATE TABLE [dim].[StandardEnergy_Parent] (
    [FactorSetId]      VARCHAR (12)        NOT NULL,
    [StandardEnergyId] VARCHAR (42)        NOT NULL,
    [ParentId]         VARCHAR (42)        NOT NULL,
    [Operator]         CHAR (1)            CONSTRAINT [DF_StandardEnergy_Parent_Operator] DEFAULT ('+') NOT NULL,
    [SortKey]          INT                 NOT NULL,
    [Hierarchy]        [sys].[hierarchyid] NOT NULL,
    [tsModified]       DATETIMEOFFSET (7)  CONSTRAINT [DF_StandardEnergy_Parent_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (128)      CONSTRAINT [DF_StandardEnergy_Parent_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (128)      CONSTRAINT [DF_StandardEnergy_Parent_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (128)      CONSTRAINT [DF_StandardEnergy_Parent_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]     ROWVERSION          NOT NULL,
    CONSTRAINT [PK_StandardEnergy_Parent] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StandardEnergyId] ASC),
    CONSTRAINT [CR_StandardEnergy_Parent_Operator] CHECK ([Operator]='~' OR [Operator]='-' OR [Operator]='+' OR [Operator]='/' OR [Operator]='*'),
    CONSTRAINT [FK_StandardEnergy_Parent_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergy_Parent_LookUp_Parent] FOREIGN KEY ([ParentId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergy_Parent_LookUp_StandardEnergy] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId]),
    CONSTRAINT [FK_StandardEnergy_Parent_Parent] FOREIGN KEY ([FactorSetId], [ParentId]) REFERENCES [dim].[StandardEnergy_Parent] ([FactorSetId], [StandardEnergyId])
);


GO

CREATE TRIGGER [dim].[t_StandardEnergy_Parent_u]
ON [dim].[StandardEnergy_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[StandardEnergy_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[StandardEnergy_Parent].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[StandardEnergy_Parent].[StandardEnergyId]	= INSERTED.[StandardEnergyId];

END;