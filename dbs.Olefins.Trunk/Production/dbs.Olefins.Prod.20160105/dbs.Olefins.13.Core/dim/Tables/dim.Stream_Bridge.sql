﻿CREATE TABLE [dim].[Stream_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [StreamId]           VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Stream_Bridge_DescendantOperator] DEFAULT ('+') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Stream_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Stream_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Stream_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Stream_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Stream_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StreamId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Stream_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Stream_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Stream_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Stream_Bridge_LookUp_Stream] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Stream_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [StreamId]) REFERENCES [dim].[Stream_Parent] ([FactorSetId], [StreamId]),
    CONSTRAINT [FK_Stream_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Stream_Parent] ([FactorSetId], [StreamId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Stream_Bridge]
    ON [dim].[Stream_Bridge]([StreamId] ASC)
    INCLUDE([FactorSetId], [DescendantId], [DescendantOperator]);


GO

CREATE TRIGGER [dim].[t_Stream_Bridge_u]
ON [dim].[Stream_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Stream_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Stream_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Stream_Bridge].[StreamId]	= INSERTED.[StreamId]
		AND	[dim].[Stream_Bridge].[DescendantId]		= INSERTED.[DescendantId];

END;