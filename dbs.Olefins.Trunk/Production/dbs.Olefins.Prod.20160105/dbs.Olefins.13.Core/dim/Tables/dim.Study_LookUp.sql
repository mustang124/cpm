﻿CREATE TABLE [dim].[Study_LookUp] (
    [StudyId]        VARCHAR (4)        NOT NULL,
    [StudyName]      NVARCHAR (84)      NOT NULL,
    [StudyDetail]    NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Study_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Study_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Study_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Study_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Study_LookUp] PRIMARY KEY CLUSTERED ([StudyId] ASC),
    CONSTRAINT [CL_Study_LookUp_StudyDetail] CHECK ([StudyDetail]<>''),
    CONSTRAINT [CL_Study_LookUp_StudyID] CHECK ([StudyId]<>''),
    CONSTRAINT [CL_Study_LookUp_StudyName] CHECK ([StudyName]<>''),
    CONSTRAINT [UK_Study_LookUp_StudyDetail] UNIQUE NONCLUSTERED ([StudyDetail] ASC),
    CONSTRAINT [UK_Study_LookUp_StudyName] UNIQUE NONCLUSTERED ([StudyName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Study_LookUp_u]
	ON [dim].[Study_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Study_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Study_LookUp].[StudyId]		= INSERTED.[StudyId];
		
END;
