﻿CREATE TABLE [dim].[Facility_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [FacilityId]         VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Facility_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Facility_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Facility_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Facility_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Facility_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Facility_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [FacilityId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Facility_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Facility_Bridge_DescendantID] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_Facility_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Facility_Bridge_LookUp_Facility] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_Facility_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [FacilityId]) REFERENCES [dim].[Facility_Parent] ([FactorSetId], [FacilityId]),
    CONSTRAINT [FK_Facility_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Facility_Parent] ([FactorSetId], [FacilityId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Facility_Bridge]
    ON [dim].[Facility_Bridge]([FactorSetId] ASC, [DescendantId] ASC)
    INCLUDE([FacilityId]);


GO
CREATE NONCLUSTERED INDEX [IX_Facility_Bridge_B]
    ON [dim].[Facility_Bridge]([FacilityId] ASC)
    INCLUDE([FactorSetId], [DescendantId], [DescendantOperator]);


GO

CREATE TRIGGER [dim].[t_Facility_Bridge_u]
ON [dim].[Facility_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Facility_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Facility_Bridge].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[dim].[Facility_Bridge].[FacilityId]		= INSERTED.[FacilityId]
		AND	[dim].[Facility_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;