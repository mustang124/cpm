﻿CREATE TABLE [dim].[ProcessUnits_LookUp] (
    [UnitId]         VARCHAR (42)       NOT NULL,
    [UnitName]       NVARCHAR (84)      NOT NULL,
    [UnitDetail]     NVARCHAR (348)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ProcessUnits_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ProcessUnits_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ProcessUnits_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ProcessUnits_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ProcessUnits_LookUp] PRIMARY KEY CLUSTERED ([UnitId] ASC),
    CHECK ([UnitDetail]<>''),
    CHECK ([UnitId]<>''),
    CHECK ([UnitName]<>''),
    UNIQUE NONCLUSTERED ([UnitDetail] ASC),
    UNIQUE NONCLUSTERED ([UnitName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ProcessUnits_LookUp_u]
	ON [dim].[ProcessUnits_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ProcessUnits_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ProcessUnits_LookUp].[UnitId]	= INSERTED.[UnitId];
		
END;
