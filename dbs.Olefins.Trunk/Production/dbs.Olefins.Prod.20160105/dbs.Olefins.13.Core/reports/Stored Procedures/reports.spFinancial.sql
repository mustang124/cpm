﻿


CREATE              PROC [reports].[spFinancial](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint, @Currency as varchar(5)
SELECT @Currency = 'USD'
DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DELETE FROM reports.Financial WHERE GroupId = @GroupId
Insert into reports.Financial (GroupId
, Currency
, NCMPerEthyleneMT
, NCMPerOlefinsMT
, NCMPerHVChemMT
, EthyleneProdCostPerUEDC
, OlefinsProdCostPerUEDC
, HVChemProdCostPerUEDC
, EthyleneProdCostPerMT
, OlefinsProdCostPerMT
, HVChemProdCostPerMT
, GPVPerUEDC
, RMCPerUEDC
, OpExPerUEDC
, GPVPerRVUsgc
, RMCPerRVUsgc
, GMPerRVUsgc
, OpExPerRVUsgc
, NCMPerRVUsgc
, NMPerRVUsgc
, GPVPerRV
, RMCPerRV
, GMPerRV
, OpExPerRV
, NCMperRV
, ROI
, NCMperRVTAAdj
, NMPerRV
, ProformaOpExPerUEDC
, ProformaOpExPerEthyleneMT
, ProformaNCMPerEthyleneMT
, ProformaProdCostPerEthyleneMT
, ProformaOpExPerHVChemMT
, ProformaNCMPerHVChemMT
, ProformaProdCostPerHVChemMT
, ProformaROI
)

SELECT GroupId = @GroupId
, Curency = f.Currency
, NCMPerEthyleneMT = [$(DbGlobal)].dbo.WtAvg(NCMPerEthyleneMT, d.EthyleneDiv_kMT)
, NCMPerOlefinsMT = [$(DbGlobal)].dbo.WtAvg(NCMPerOlefinsMT, d.OlefinsDiv_kMT)
, NCMPerHVChemMT = [$(DbGlobal)].dbo.WtAvg(NCMPerHVChemMT, d.HvcProd_kMT)
, EthyleneProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(EthyleneProdCostPerUEDC, d.kUEdc)
, OlefinsProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(OlefinsProdCostPerUEDC, d.kUEdc)
, HVChemProdCostPerUEDC = [$(DbGlobal)].dbo.WtAvg(HVChemProdCostPerUEDC, d.kUEdc)
, EthyleneProdCostPerMT = [$(DbGlobal)].dbo.WtAvg(EthyleneProdCostPerMT, d.EthyleneDiv_kMT)
, OlefinsProdCostPerMT = [$(DbGlobal)].dbo.WtAvg(OlefinsProdCostPerMT, d.OlefinsDiv_kMT)
, HVChemProdCostPerMT = [$(DbGlobal)].dbo.WtAvg(HVChemProdCostPerMT, d.HvcProd_kMT)
, GPVPerUEDC = [$(DbGlobal)].dbo.WtAvg(GPVPerUEDC, d.kUEdc)
, RMCPerUEDC = [$(DbGlobal)].dbo.WtAvg(RMCPerUEDC, d.kUEdc)
, OpExPerUEDC = [$(DbGlobal)].dbo.WtAvg(OpExPerUEDC, d.kUEdc)
, GPVPerRVUsgc = [$(DbGlobal)].dbo.WtAvg(GPVPerRVUsgc, d.RVUSGC_MUS)
, RMCPerRVUsgc = [$(DbGlobal)].dbo.WtAvg(RMCPerRVUsgc, d.RVUSGC_MUS)
, GMPerRVUsgc = [$(DbGlobal)].dbo.WtAvg(GMPerRVUsgc, d.RVUSGC_MUS)
, OpExPerRVUsgc = [$(DbGlobal)].dbo.WtAvg(OpExPerRVUsgc, d.RVUSGC_MUS)
, NCMPerRVUsgc = [$(DbGlobal)].dbo.WtAvg(NCMPerRVUsgc, d.RVUSGC_MUS)
, NMPerRVUsgc = [$(DbGlobal)].dbo.WtAvg(NMPerRVUsgc, d.RVUSGC_MUS)
, GPVPerRV = [$(DbGlobal)].dbo.WtAvg(GPVPerRV, d.RV_MUS)
, RMCPerRV = [$(DbGlobal)].dbo.WtAvg(RMCPerRV, d.RV_MUS)
, GMPerRV = [$(DbGlobal)].dbo.WtAvg(GMPerRV, d.RV_MUS)
, OpExPerRV = [$(DbGlobal)].dbo.WtAvg(OpExPerRV, d.RV_MUS)
, NCMperRV = [$(DbGlobal)].dbo.WtAvg(NCMperRV, d.RV_MUS)
, ROI = [$(DbGlobal)].dbo.WtAvg(ROI, d.RV_MUS)
, NCMperRVTAAdj = [$(DbGlobal)].dbo.WtAvg(NCMperRVTAAdj, d.RV_MUS)
, NMPerRV = [$(DbGlobal)].dbo.WtAvg(NMPerRV, d.RV_MUS)
, ProformaOpExPerUEDC = [$(DbGlobal)].dbo.WtAvg(ProformaOpExPerUEDC, d.kUEdc)
, ProformaOpExPerEthyleneMT = [$(DbGlobal)].dbo.WtAvg(ProformaOpExPerEthyleneMT, d.EthyleneDiv_kMT)
, ProformaNCMPerEthyleneMT = [$(DbGlobal)].dbo.WtAvg(ProformaNCMPerEthyleneMT, d.EthyleneDiv_kMT)
, ProformaProdCostPerEthyleneMT = [$(DbGlobal)].dbo.WtAvg(ProformaProdCostPerEthyleneMT, d.EthyleneDiv_kMT)
, ProformaOpExPerHVChemMT = [$(DbGlobal)].dbo.WtAvg(ProformaOpExPerHVChemMT, d.HvcProd_kMT)
, ProformaNCMPerHVChemMT = [$(DbGlobal)].dbo.WtAvg(ProformaNCMPerHVChemMT, d.HvcProd_kMT)
, ProformaProdCostPerHVChemMT = [$(DbGlobal)].dbo.WtAvg(ProformaProdCostPerHVChemMT, d.HvcProd_kMT)
, ProformaROI = [$(DbGlobal)].dbo.WtAvg(ProformaROI, d.RV_MUS)

FROM @MyGroup r JOIN dbo.Financial f on r.Refnum=f.Refnum
JOIN dbo.Divisors d on d.Refnum=r.Refnum
GROUP BY f.Currency

SET NOCOUNT OFF

















