﻿CREATE TABLE [reports].[Personnel] (
    [GroupId]             VARCHAR (25)  NOT NULL,
    [DataType]            VARCHAR (10)  NOT NULL,
    [DivisorValue]        REAL          NULL,
    [OccProcProc]         REAL          NULL,
    [OccProcUtil]         REAL          NULL,
    [OccProcOffSite]      REAL          NULL,
    [OccProcTruckRail]    REAL          NULL,
    [OccProcMarine]       REAL          NULL,
    [OccProcRelief]       REAL          NULL,
    [OccProcTraining]     REAL          NULL,
    [OccProcOther]        REAL          NULL,
    [OccProc]             REAL          NULL,
    [OccMaintRoutine]     REAL          NULL,
    [OccMaintTaAnnRetube] REAL          NULL,
    [OccMaintTaMaint]     REAL          NULL,
    [OccMaintInsp]        REAL          NULL,
    [OccMaintShop]        REAL          NULL,
    [OccMaintPlan]        REAL          NULL,
    [OccMaintWarehouse]   REAL          NULL,
    [OccMaintTraining]    REAL          NULL,
    [OccMaint]            REAL          NULL,
    [OccAdminLab]         REAL          NULL,
    [OccAdminAcct]        REAL          NULL,
    [OccAdminPurchase]    REAL          NULL,
    [OccAdminSecurity]    REAL          NULL,
    [OccAdminClerical]    REAL          NULL,
    [OccAdminOther]       REAL          NULL,
    [OccAdmin]            REAL          NULL,
    [OccPlantSite]        REAL          NULL,
    [MpsProc]             REAL          NULL,
    [MpsMaintRoutine]     REAL          NULL,
    [MpsMaintTaAnnRetube] REAL          NULL,
    [MpsMaintTaMaint]     REAL          NULL,
    [MpsMaintShop]        REAL          NULL,
    [MpsMaintPlan]        REAL          NULL,
    [MpsMaintWarehouse]   REAL          NULL,
    [MpsMaint]            REAL          NULL,
    [MpsAdminLab]         REAL          NULL,
    [MpsTechProc]         REAL          NULL,
    [MpsTechEcon]         REAL          NULL,
    [MpsTechRely]         REAL          NULL,
    [MpsTechInsp]         REAL          NULL,
    [MpsTechAPC]          REAL          NULL,
    [MpsTechEnviron]      REAL          NULL,
    [MpsTechOther]        REAL          NULL,
    [MpsTech]             REAL          NULL,
    [MpsAdminAcct]        REAL          NULL,
    [MpsAdminMIS]         REAL          NULL,
    [MpsAdminPurchase]    REAL          NULL,
    [MpsAdminSecurity]    REAL          NULL,
    [MpsAdminOther]       REAL          NULL,
    [MpsAdmin]            REAL          NULL,
    [MpsPlantSite]        REAL          NULL,
    [MpsAdminGenPurchase] REAL          NULL,
    [MpsAdminGenOther]    REAL          NULL,
    [MpsSubTotal]         REAL          NULL,
    [PlantTotal]          REAL          NULL,
    [OccAbsence_Pcnt]     REAL          NULL,
    [MpsAbsence_Pcnt]     REAL          NULL,
    [MaintRoutine]        REAL          NULL,
    [MaintTaAnnRetube]    REAL          NULL,
    [MaintTaMaint]        REAL          NULL,
    [MaintInsp]           REAL          NULL,
    [MaintShop]           REAL          NULL,
    [MaintPlan]           REAL          NULL,
    [MaintWarehouse]      REAL          NULL,
    [MaintTraining]       REAL          NULL,
    [Maint]               REAL          NULL,
    [NonMaint]            REAL          NULL,
    [nmOccPlantSite]      REAL          NULL,
    [nmMpsTech]           REAL          NULL,
    [nmMpsSubTotal]       REAL          NULL,
    [nmMpsPlantSite]      REAL          NULL,
    [tsModified]          SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Personnel] PRIMARY KEY CLUSTERED ([GroupId] ASC, [DataType] ASC) WITH (FILLFACTOR = 70)
);

