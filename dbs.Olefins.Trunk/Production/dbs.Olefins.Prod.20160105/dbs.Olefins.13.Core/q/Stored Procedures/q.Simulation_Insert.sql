﻿CREATE PROCEDURE q.Simulation_Insert(
	  @Refnum				VARCHAR(25)
	, @FactorSetId			VARCHAR(12)
	, @SimModelId			VARCHAR(12)
	)
AS
	INSERT INTO q.Simulation(Refnum, FactorSetId, SimModelId)
	VALUES(@Refnum, @FactorSetId, @SimModelId);
