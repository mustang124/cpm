﻿CREATE TABLE [ante].[StandardEnergyExponents] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [StandardEnergyId] VARCHAR (42)       NOT NULL,
    [Value]            REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_StandardEnergyExponents_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyExponents_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyExponents_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_StandardEnergyExponents_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_StandardEnergyExponents] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [StandardEnergyId] ASC),
    CONSTRAINT [FK_StandardEnergyExponents_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_StandardEnergyExponents_StandardEnergy_LookUp] FOREIGN KEY ([StandardEnergyId]) REFERENCES [dim].[StandardEnergy_LookUp] ([StandardEnergyId])
);


GO

CREATE TRIGGER [ante].[t_CalcStandardEnergyExponents_u]
	ON [ante].[StandardEnergyExponents]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[StandardEnergyExponents]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[StandardEnergyExponents].FactorSetId		= INSERTED.FactorSetId
		AND	[ante].[StandardEnergyExponents].StandardEnergyId	= INSERTED.StandardEnergyId;

END;