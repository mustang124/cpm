﻿
CREATE FUNCTION etl.ConvDownTime
(
	@FurnRetubeWorkID	NVARCHAR(24)
)
RETURNS NVARCHAR(24)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(24) =
	CASE @FurnRetubeWorkID
		WHEN 'DTDecoke'			THEN 'MaintDecoke'
		WHEN 'DTTLEClean'		THEN 'MaintTLE'
		WHEN 'DTMinor'			THEN 'MaintMinor'
		WHEN 'DTMajor'			THEN 'MaintMajor'
		WHEN 'STDT'				THEN 'Maint'
		WHEN 'DTStandby'		THEN 'StandByDT'
		WHEN 'TotTimeOff'		THEN 'PyroFurnDT'
		WHEN 'TADownDays'		THEN 'TurnAround'
		WHEN 'OthDownDays'		THEN 'PlantOutages'
		WHEN 'OnStreamDays'		THEN 'LostProdOpp'
		ELSE @FurnRetubeWorkID
	END;
	
	RETURN @ResultVar;
	
END;