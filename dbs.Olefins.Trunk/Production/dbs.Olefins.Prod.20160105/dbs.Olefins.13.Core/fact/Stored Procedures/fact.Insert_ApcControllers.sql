﻿CREATE PROCEDURE [fact].[Insert_ApcControllers]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ApcControllers(Refnum, CalDateKey, ApcId, Controller_Count)
		SELECT
			u.Refnum,
			u.CalDateKey,
			etl.ConvApcID(u.ApcId),
			u.Items
		FROM(
			SELECT
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum],
				etl.ConvDateKey(t.StudyYear)								[CalDateKey],
				m.Tbl9020,
				m.Tbl9021,
				m.Tbl9022,
				m.Tbl9023,
				m.Tbl9024,
				m.Tbl9025,
				m.Tbl9026,
				m.Tbl9027,
				m.Tbl9028
			FROM stgFact.PracMPC m
			INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
			WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT(
			Items FOR ApcId IN(
				Tbl9020,
				Tbl9021,
				Tbl9022,
				Tbl9023,
				Tbl9024,
				Tbl9025,
				Tbl9026,
				Tbl9027,
				Tbl9028
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;