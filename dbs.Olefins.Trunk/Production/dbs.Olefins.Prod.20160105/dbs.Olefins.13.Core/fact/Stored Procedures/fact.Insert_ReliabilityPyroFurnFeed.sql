﻿CREATE PROCEDURE [fact].[Insert_ReliabilityPyroFurnFeed]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityPyroFurnFeed(Refnum, CalDateKey, StreamId
				, RunLength_Days, Inj, PreTreat_Bit, AntiFoul_Bit)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')					[Refnum]
			, etl.ConvDateKey(t.StudyYear)									[CalDateKey]
			, etl.ConvStreamID(
				CASE p.FeedProdID
				WHEN 'GasOil' THEN 'LiqHeavy'
				WHEN 'FRNaphtha' THEN 'Naphtha'
				ELSE p.FeedProdID END
				)
																			[StreamId]
			, p.Run
			, UPPER(p.Inj)													[Inj]
			, etl.ConvBit(p.Pretreat)										[PreTreat]
			, etl.ConvBit(p.AntiFoul)										[AntiFoul]
		FROM stgFact.PracFurnInfo p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE p.Run IS NOT NULL
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;