﻿CREATE VIEW fact.GenPlantInventoryAggregate
WITH SCHEMABINDING
AS
SELECT
	b.FactorSetId,
	i.Refnum,
	i.CalDateKey,
	b.FacilityId,
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + i.StorageCapacity_kMT
		WHEN '-' THEN - i.StorageCapacity_kMT
		END
		)						[StorageCapacity_kMT],
	CASE WHEN
		SUM(
			CASE b.DescendantOperator
			WHEN '+' THEN + i.StorageCapacity_kMT
			WHEN '-' THEN - i.StorageCapacity_kMT
			END
		)	> 0.0 THEN
		SUM(
			CASE b.DescendantOperator
			WHEN '+' THEN + i.StorageCapacity_kMT * i.StorageLevel_Pcnt
			WHEN '-' THEN - i.StorageCapacity_kMT * i.StorageLevel_Pcnt
			END
			)
		/
		SUM(
			CASE b.DescendantOperator
			WHEN '+' THEN + i.StorageCapacity_kMT
			WHEN '-' THEN - i.StorageCapacity_kMT
			END
		)
		END						[StorageLevel_Pcnt],
	SUM(
		CASE b.DescendantOperator
		WHEN '+' THEN + ISNULL(i.StorageCapacity_kMT, 0.0) * ISNULL(i.StorageLevel_Pcnt, 0.0)
		WHEN '-' THEN - ISNULL(i.StorageCapacity_kMT, 0.0) * ISNULL(i.StorageLevel_Pcnt, 0.0)
		END
		) / 100.0				[YearEndInventory_kMT]

FROM dim.Facility_Bridge			b
INNER JOIN fact.GenPlantInventory	i
	ON	i.FacilityId = b.DescendantId
GROUP BY
	b.FactorSetId,
	i.Refnum,
	i.CalDateKey,
	b.FacilityId;