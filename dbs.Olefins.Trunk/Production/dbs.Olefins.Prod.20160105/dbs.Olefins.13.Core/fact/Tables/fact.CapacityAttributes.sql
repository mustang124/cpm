﻿CREATE TABLE [fact].[CapacityAttributes] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [Expansion_Pcnt] REAL               NULL,
    [Expansion_Date] DATE               NULL,
    [CapAllow_Bit]   BIT                NOT NULL,
    [CapLoss_Pcnt]   REAL               NULL,
    [StartUp_Year]   SMALLINT           NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_CapacityAttributes_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_CapacityAttributes_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_CapacityAttributes_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_CapacityAttributes_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_CapacityAttributes] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_CapacityAttributes_CapLoss_Pcnt] CHECK ([CapLoss_Pcnt]>=(0.0) AND [CapLoss_Pcnt]<=(100.0)),
    CONSTRAINT [CR_CapacityAttributes_Expansion_Pcnt] CHECK ([Expansion_Pcnt]>=(0.0) AND [Expansion_Pcnt]<=(200.0)),
    CONSTRAINT [CR_CapacityAttributes_StartUp_Year] CHECK ([StartUp_Year]>=(1950) AND len([StartUp_Year])=(4)),
    CONSTRAINT [FK_CapacityAttributes_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_CapacityAttributes_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_CapacityAttributes_u]
	ON [fact].[CapacityAttributes]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[CapacityAttributes]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[CapacityAttributes].Refnum		= INSERTED.Refnum
		AND [fact].[CapacityAttributes].CalDateKey	= INSERTED.CalDateKey;

END;