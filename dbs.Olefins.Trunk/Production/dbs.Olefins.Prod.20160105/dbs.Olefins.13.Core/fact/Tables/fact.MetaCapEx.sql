﻿CREATE TABLE [fact].[MetaCapEx] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [AccountId]      VARCHAR (42)       NOT NULL,
    [CurrencyRpt]    VARCHAR (4)        NOT NULL,
    [Amount_Cur]     REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_MetaCapEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_MetaCapEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_MetaCapEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_MetaCapEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MetaCapEx] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_MetaCapEx_Amount_Cur] CHECK ([Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_MetaCapEx_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_MetaCapEx_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MetaCapEx_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_MetaCapEx_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_MetaCapEx_u]
	ON [fact].[MetaCapEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[MetaCapEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[MetaCapEx].Refnum			= INSERTED.Refnum
		AND [fact].[MetaCapEx].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[MetaCapEx].AccountId		= INSERTED.AccountId
		AND [fact].[MetaCapEx].CalDateKey		= INSERTED.CalDateKey;

END;