﻿CREATE PROCEDURE sim.SpslIn_Insert(
	@QueueID			BIGINT,
	@FactorSetId		VARCHAR(12),
	@Refnum				VARCHAR(25),
	@CalDateKey			INT,
	@SimModelId			VARCHAR(12),
	@OpCondId			VARCHAR(12),
	@StreamId			VARCHAR(42),
	@StreamDescription	NVARCHAR(256),
	@RecycleId			INT,
	@DataSetId			VARCHAR(6),
	@FieldId			VARCHAR(12),
	@Value				REAL
	)
AS
	INSERT INTO sim.SpslIn(QueueID, FactorSetId, SimModelId, Refnum, CalDateKey, StreamId, StreamDescription, OpCondId, RecycleId, DataSetId, FieldId, Value)
	VALUES(@QueueID, @FactorSetId, @SimModelId, @Refnum, @CalDateKey, @StreamId, @StreamDescription, @OpCondId, @RecycleId, @DataSetId, @FieldId, @Value)
	;