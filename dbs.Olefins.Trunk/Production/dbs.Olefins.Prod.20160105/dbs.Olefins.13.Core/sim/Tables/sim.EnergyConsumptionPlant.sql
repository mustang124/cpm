﻿CREATE TABLE [sim].[EnergyConsumptionPlant] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [SimModelId]      VARCHAR (12)       NOT NULL,
    [OpCondId]        VARCHAR (12)       NOT NULL,
    [RecycleId]       INT                NOT NULL,
    [Energy_kCalC2H4] REAL               NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_EnergyConsumptionPlant_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_EnergyConsumptionPlant_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_EnergyConsumptionPlant_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_EnergyConsumptionPlant_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EnergyConsumptionPlant] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [OpCondId] ASC, [RecycleId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_EnergyConsumptionPlant_Energy_kCalC2H4] CHECK ([Energy_kCalC2H4]>=(0.0)),
    CONSTRAINT [FK_EnergyConsumptionPlant_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_EnergyConsumptionPlant_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_EnergyConsumptionPlant_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_EnergyConsumptionPlant_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId])
);


GO

CREATE TRIGGER [sim].[t_EnergyConsumptionPlant_u]
	ON [sim].[EnergyConsumptionPlant]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [sim].[EnergyConsumptionPlant]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[sim].[EnergyConsumptionPlant].FactorSetId		= INSERTED.FactorSetId
		AND	[sim].[EnergyConsumptionPlant].Refnum			= INSERTED.Refnum
		AND	[sim].[EnergyConsumptionPlant].CalDateKey		= INSERTED.CalDateKey
		AND	[sim].[EnergyConsumptionPlant].SimModelId		= INSERTED.SimModelId
		AND	[sim].[EnergyConsumptionPlant].OpCondId			= INSERTED.OpCondId
		AND	[sim].[EnergyConsumptionPlant].RecycleId		= INSERTED.RecycleId;

END;