﻿
CREATE FUNCTION sim.PypsLiquidType
(
	@Piano		BIT,
	@SG			REAL,
	@D100		REAL,
	@StreamId	VARCHAR(42)
)
RETURNS TINYINT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @PypsLiquidType TINYINT = NULL;

	SET @PypsLiquidType =
	CASE @StreamId
	WHEN 'Raffinate'	THEN 21
	WHEN 'HeavyNGL'		THEN 23
	WHEN 'Condensate'	THEN 23
	
	WHEN 'Diesel'		THEN 20 --18
	WHEN 'HTGasoil'		THEN 20 --18
	WHEN 'HeavyGasoil'	THEN 20
	ELSE
		CASE @Piano
		WHEN 1 THEN
		
			CASE
			WHEN @SG >= 0.00 AND @SG < 0.70	THEN 23			-- HNGL/PNA/Condensate
			WHEN @SG >= 0.70 AND @SG < 0.77	THEN 19			-- Gasoline
			WHEN @SG >= 0.77 AND @SG < 1.00	THEN 21			-- Raffinate
			END
			
		WHEN 0 THEN
		
			CASE
			WHEN @SG >= 0.00 AND @SG < 0.77	THEN 24			-- NGL
			
			WHEN @SG >= 0.77 AND @SG < 0.83	THEN 
				CASE WHEN @D100 < 268 THEN 22 ELSE 20 END	-- Kerosene (22)
				
			WHEN @SG >= 0.83 AND @SG < 0.86	THEN 20
				--CASE WHEN @D100 < 390 THEN 18 ELSE 20 END	-- Atmospheric Gas Oil (18)
			
			WHEN @SG >= 0.86 AND @SG < 1.00	THEN 20			-- Vacuum/Hydrotreated Gas Oil
			END

		END
	END;
	
	RETURN @PypsLiquidType;
	
END;