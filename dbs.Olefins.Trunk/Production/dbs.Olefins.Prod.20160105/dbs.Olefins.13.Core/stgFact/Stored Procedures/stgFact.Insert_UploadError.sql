﻿CREATE PROCEDURE [stgFact].[Insert_UploadError]
(
	@FileType			VARCHAR (20),
	@Refnum				VARCHAR (25),

	@FilePath			VARCHAR (512),

	@Sheet				VARCHAR (128)	= NULL,
	@Range				VARCHAR (128)	= NULL,
	@CellText			NVARCHAR (128)	= NULL,

	@ProcedureName		VARCHAR (128)	= NULL,
	@TableName			VARCHAR (128)	= NULL,
	@ParameterName		VARCHAR (128)	= NULL,

	@ErrorNotes			VARCHAR (MAX)	= NULL,
	@SystemErrorMessage	VARCHAR (MAX)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[UploadErrors]([FileType], [Refnum], [FilePath], [Sheet], [Range], [CellText], [ProcedureName], [TableName], [ParameterName], [ErrorNotes], [SystemErrorMessage])
	VALUES(@FileType, @Refnum, @FilePath, @Sheet, @Range, @CellText, @ProcedureName, @TableName, @ParameterName, @ErrorNotes, @SystemErrorMessage);

END;