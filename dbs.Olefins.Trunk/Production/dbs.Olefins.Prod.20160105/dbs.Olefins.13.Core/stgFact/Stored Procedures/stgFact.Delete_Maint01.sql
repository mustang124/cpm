﻿CREATE PROCEDURE [stgFact].[Delete_Maint01]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Maint01]
	WHERE [Refnum] = @Refnum;

END;