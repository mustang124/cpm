﻿CREATE PROCEDURE [stgFact].[Delete_CapGrowth]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[CapGrowth]
	WHERE [Refnum] = @Refnum;

END;