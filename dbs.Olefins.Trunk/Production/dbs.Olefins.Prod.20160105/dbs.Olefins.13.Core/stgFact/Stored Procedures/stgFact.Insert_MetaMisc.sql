﻿CREATE PROCEDURE [stgFact].[Insert_MetaMisc]
(
	@Refnum        VARCHAR (25),

	@CapitalCost   REAL     = NULL,
	@UnitStartUpYr REAL     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[MetaMisc]([Refnum], [CapitalCost], [UnitStartUpYr])
	VALUES(@Refnum, @CapitalCost, @UnitStartUpYr);

END;
