﻿CREATE PROCEDURE [stgFact].[Insert_TADT]
(
	@Refnum         VARCHAR (25),
	@TrainId        TINYINT,

	@PlanInterval   REAL     = NULL,
	@PlanDT         REAL     = NULL,
	@ActInterval    REAL     = NULL,
	@ActDT          REAL     = NULL,
	@TACost         REAL     = NULL,
	@TAManHrs       REAL     = NULL,
	@TaDate         DATETIME = NULL,
	@PctTotEthylCap REAL     = NULL,
	@TAMini         VARCHAR (3) = NULL
)
AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO [stgFact].[TADT]([Refnum], [TrainId], [PlanInterval], [PlanDT], [ActInterval], [ActDT], [TACost], [TAManHrs], [TaDate], [PctTotEthylCap], [TAMini])
	VALUES(@Refnum, @TrainId, @PlanInterval, @PlanDT, @ActInterval, @ActDT, @TACost, @TAManHrs, @TaDate, @PctTotEthylCap, @TAMini);

END