﻿CREATE PROCEDURE [stgFact].[Insert_PolyOpEx]
(
	@Refnum      VARCHAR (25),
	@PlantId     TINYINT,

	@NonTAMaint  REAL     = NULL,
	@TA          REAL     = NULL,
	@OthNonVol   REAL     = NULL,
	@STNonVol    REAL     = NULL,
	@Catalysts   REAL     = NULL,
	@Chemicals   REAL     = NULL,
	@Additives   REAL     = NULL,
	@Royalties   REAL     = NULL,
	@Energy      REAL     = NULL,
	@Packaging   REAL     = NULL,
	@OthVol      REAL     = NULL,
	@STVol       REAL     = NULL,
	@TotCashOpEx REAL     = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[PolyOpEx]([Refnum], [PlantId], [NonTAMaint], [TA], [OthNonVol], [STNonVol], [Catalysts], [Chemicals], [Additives], [Royalties], [Energy], [Packaging], [OthVol], [STVol], [TotCashOpEx])
	VALUES(@Refnum, @PlantId, @NonTAMaint, @TA, @OthNonVol, @STNonVol, @Catalysts, @Chemicals, @Additives, @Royalties, @Energy, @Packaging, @OthVol, @STVol, @TotCashOpEx);

END;