﻿CREATE PROCEDURE [stgFact].[Delete_MetaMisc]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[MetaMisc]
	WHERE [Refnum] = @Refnum;

END;