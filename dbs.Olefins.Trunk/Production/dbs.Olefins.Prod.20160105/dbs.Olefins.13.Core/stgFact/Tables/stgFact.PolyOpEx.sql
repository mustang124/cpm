﻿CREATE TABLE [stgFact].[PolyOpEx] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [PlantId]        TINYINT            NOT NULL,
    [NonTAMaint]     REAL               NULL,
    [TA]             REAL               NULL,
    [OthNonVol]      REAL               NULL,
    [STNonVol]       REAL               NULL,
    [Catalysts]      REAL               NULL,
    [Chemicals]      REAL               NULL,
    [Additives]      REAL               NULL,
    [Royalties]      REAL               NULL,
    [Energy]         REAL               NULL,
    [Packaging]      REAL               NULL,
    [OthVol]         REAL               NULL,
    [STVol]          REAL               NULL,
    [TotCashOpEx]    REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_PolyOpEx_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyOpEx_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyOpEx_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_PolyOpEx_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_PolyOpEx] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PlantId] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_PolyOpEx_u]
	ON [stgFact].[PolyOpEx]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[PolyOpEx]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[PolyOpEx].Refnum		= INSERTED.Refnum
		AND	[stgFact].[PolyOpEx].PlantId		= INSERTED.PlantId;

END;