﻿CREATE TABLE [stgFact].[Composition] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdID]     VARCHAR (20)       NOT NULL,
    [H2]             REAL               NULL,
    [CH4]            REAL               NULL,
    [C2H2]           REAL               NULL,
    [C2H6]           REAL               NULL,
    [C2H4]           REAL               NULL,
    [C3H6]           REAL               NULL,
    [C3H8]           REAL               NULL,
    [BUTAD]          REAL               NULL,
    [C4S]            REAL               NULL,
    [C4H10]          REAL               NULL,
    [BZ]             REAL               NULL,
    [PYGAS]          REAL               NULL,
    [PYOIL]          REAL               NULL,
    [INERT]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_Composition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Composition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_Composition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_Composition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_Composition] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_Composition_u]
	ON [stgFact].[Composition]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[Composition]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[Composition].Refnum			= INSERTED.Refnum
		AND	[stgFact].[Composition].FeedProdID		= INSERTED.FeedProdID;

END;