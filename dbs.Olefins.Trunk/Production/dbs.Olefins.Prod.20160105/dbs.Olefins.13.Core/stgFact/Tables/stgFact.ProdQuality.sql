﻿CREATE TABLE [stgFact].[ProdQuality] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [FeedProdID]     VARCHAR (20)       NOT NULL,
    [WtPcnt]         REAL               NULL,
    [MolePcnt]       REAL               NULL,
    [H2Value]        REAL               NULL,
    [HeatValue]      REAL               NULL,
    [H2Content]      REAL               NULL,
    [OthProd1Value]  REAL               NULL,
    [OthProd2Value]  REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_ProdQuality_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdQuality_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdQuality_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_ProdQuality_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_ProdQuality] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FeedProdID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_ProdQuality_u]
	ON [stgFact].[ProdQuality]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[ProdQuality]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[ProdQuality].Refnum		= INSERTED.Refnum
		AND	[stgFact].[ProdQuality].FeedProdID	= INSERTED.FeedProdID;

END;