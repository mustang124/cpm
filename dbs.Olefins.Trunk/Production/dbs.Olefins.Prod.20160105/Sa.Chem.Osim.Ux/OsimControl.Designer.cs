﻿namespace Sa.Chem
{
	partial class OsimControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnUpload = new System.Windows.Forms.Button();
			this.JabTestList = new System.Windows.Forms.Button();
			this.btnOsimStudyCreation = new System.Windows.Forms.Button();
			this.btnCreateOneDist = new System.Windows.Forms.Button();
			this.lblCurrent = new System.Windows.Forms.Label();
			this.lblHistory = new System.Windows.Forms.Label();
			this.txtCurrent = new System.Windows.Forms.TextBox();
			this.txtHistory = new System.Windows.Forms.TextBox();
			this.btnCreateOneDcs = new System.Windows.Forms.Button();
			this.lblOsimStudyCreation = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnUpload
			// 
			this.btnUpload.Location = new System.Drawing.Point(42, 12);
			this.btnUpload.Name = "btnUpload";
			this.btnUpload.Size = new System.Drawing.Size(184, 23);
			this.btnUpload.TabIndex = 2;
			this.btnUpload.Text = "Upload OSIM File";
			this.btnUpload.UseVisualStyleBackColor = true;
			this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
			// 
			// JabTestList
			// 
			this.JabTestList.Location = new System.Drawing.Point(42, 220);
			this.JabTestList.Name = "JabTestList";
			this.JabTestList.Size = new System.Drawing.Size(184, 23);
			this.JabTestList.TabIndex = 3;
			this.JabTestList.Text = "Jon Bowen Test List";
			this.JabTestList.UseVisualStyleBackColor = true;
			this.JabTestList.Click += new System.EventHandler(this.btnCreateTestDist_Click);
			// 
			// btnOsimStudyCreation
			// 
			this.btnOsimStudyCreation.Location = new System.Drawing.Point(42, 50);
			this.btnOsimStudyCreation.Name = "btnOsimStudyCreation";
			this.btnOsimStudyCreation.Size = new System.Drawing.Size(184, 23);
			this.btnOsimStudyCreation.TabIndex = 4;
			this.btnOsimStudyCreation.Text = "OSIM Study Creation";
			this.btnOsimStudyCreation.UseVisualStyleBackColor = true;
			this.btnOsimStudyCreation.Click += new System.EventHandler(this.btnCreateAllDist_Click);
			// 
			// btnCreateOneDist
			// 
			this.btnCreateOneDist.Location = new System.Drawing.Point(42, 161);
			this.btnCreateOneDist.Name = "btnCreateOneDist";
			this.btnCreateOneDist.Size = new System.Drawing.Size(184, 23);
			this.btnCreateOneDist.TabIndex = 5;
			this.btnCreateOneDist.Text = "Create One Osim (Distribution)";
			this.btnCreateOneDist.UseVisualStyleBackColor = true;
			this.btnCreateOneDist.Click += new System.EventHandler(this.btnCreateOneDist_Click);
			// 
			// lblCurrent
			// 
			this.lblCurrent.AutoSize = true;
			this.lblCurrent.Location = new System.Drawing.Point(39, 115);
			this.lblCurrent.Name = "lblCurrent";
			this.lblCurrent.Size = new System.Drawing.Size(41, 13);
			this.lblCurrent.TabIndex = 6;
			this.lblCurrent.Text = "Current";
			// 
			// lblHistory
			// 
			this.lblHistory.AutoSize = true;
			this.lblHistory.Location = new System.Drawing.Point(39, 138);
			this.lblHistory.Name = "lblHistory";
			this.lblHistory.Size = new System.Drawing.Size(39, 13);
			this.lblHistory.TabIndex = 7;
			this.lblHistory.Text = "History";
			// 
			// txtCurrent
			// 
			this.txtCurrent.Location = new System.Drawing.Point(102, 112);
			this.txtCurrent.Name = "txtCurrent";
			this.txtCurrent.Size = new System.Drawing.Size(124, 20);
			this.txtCurrent.TabIndex = 8;
			// 
			// txtHistory
			// 
			this.txtHistory.Location = new System.Drawing.Point(102, 135);
			this.txtHistory.Name = "txtHistory";
			this.txtHistory.Size = new System.Drawing.Size(124, 20);
			this.txtHistory.TabIndex = 9;
			// 
			// btnCreateOneDcs
			// 
			this.btnCreateOneDcs.Location = new System.Drawing.Point(42, 191);
			this.btnCreateOneDcs.Name = "btnCreateOneDcs";
			this.btnCreateOneDcs.Size = new System.Drawing.Size(184, 23);
			this.btnCreateOneDcs.TabIndex = 10;
			this.btnCreateOneDcs.Text = "Create One Osim (DCS)";
			this.btnCreateOneDcs.UseVisualStyleBackColor = true;
			this.btnCreateOneDcs.Click += new System.EventHandler(this.btnCreateOneDcs_Click);
			// 
			// lblOsimStudyCreation
			// 
			this.lblOsimStudyCreation.AutoSize = true;
			this.lblOsimStudyCreation.Location = new System.Drawing.Point(232, 55);
			this.lblOsimStudyCreation.Name = "lblOsimStudyCreation";
			this.lblOsimStudyCreation.Size = new System.Drawing.Size(118, 13);
			this.lblOsimStudyCreation.TabIndex = 11;
			this.lblOsimStudyCreation.Text = "[fact].[Select_Refnums]";
			// 
			// OsimControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(377, 262);
			this.Controls.Add(this.lblOsimStudyCreation);
			this.Controls.Add(this.btnCreateOneDcs);
			this.Controls.Add(this.txtHistory);
			this.Controls.Add(this.txtCurrent);
			this.Controls.Add(this.lblHistory);
			this.Controls.Add(this.lblCurrent);
			this.Controls.Add(this.btnCreateOneDist);
			this.Controls.Add(this.btnOsimStudyCreation);
			this.Controls.Add(this.JabTestList);
			this.Controls.Add(this.btnUpload);
			this.Name = "OsimControl";
			this.Text = "OsimControl";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnUpload;
		private System.Windows.Forms.Button JabTestList;
		private System.Windows.Forms.Button btnOsimStudyCreation;
		private System.Windows.Forms.Button btnCreateOneDist;
		private System.Windows.Forms.Label lblCurrent;
		private System.Windows.Forms.Label lblHistory;
		private System.Windows.Forms.TextBox txtCurrent;
		private System.Windows.Forms.TextBox txtHistory;
		private System.Windows.Forms.Button btnCreateOneDcs;
		private System.Windows.Forms.Label lblOsimStudyCreation;
	}
}