﻿CREATE TABLE [dim].[ReliabilityServiceLevel_LookUp] (
    [ServiceLevelId]     VARCHAR (14)       NOT NULL,
    [ServiceLevelName]   NVARCHAR (84)      NOT NULL,
    [ServiceLevelDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityServiceLevel_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (168)     CONSTRAINT [DF_ReliabilityServiceLevel_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (168)     CONSTRAINT [DF_ReliabilityServiceLevel_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (168)     CONSTRAINT [DF_ReliabilityServiceLevel_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_ReliabilityServiceLevel_LookUp] PRIMARY KEY CLUSTERED ([ServiceLevelId] ASC),
    CONSTRAINT [CL_ReliabilityServiceLevel_LookUp_ServiceLevelDetail] CHECK ([ServiceLevelDetail]<>''),
    CONSTRAINT [CL_ReliabilityServiceLevel_LookUp_ServiceLevelId] CHECK ([ServiceLevelId]<>''),
    CONSTRAINT [CL_ReliabilityServiceLevel_LookUp_ServiceLevelName] CHECK ([ServiceLevelName]<>''),
    CONSTRAINT [UK_ReliabilityServiceLevel_LookUp_ServiceLevelDetail] UNIQUE NONCLUSTERED ([ServiceLevelDetail] ASC),
    CONSTRAINT [UK_ReliabilityServiceLevel_LookUp_ServiceLevelName] UNIQUE NONCLUSTERED ([ServiceLevelName] ASC)
);


GO

CREATE TRIGGER [dim].[t_ReliabilityServiceLevel_LookUp_u]
	ON [dim].[ReliabilityServiceLevel_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[ReliabilityServiceLevel_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[ReliabilityServiceLevel_LookUp].[ServiceLevelId]		= INSERTED.[ServiceLevelId];
		
END;
