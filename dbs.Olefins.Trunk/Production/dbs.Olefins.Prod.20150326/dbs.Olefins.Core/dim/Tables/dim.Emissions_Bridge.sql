﻿CREATE TABLE [dim].[Emissions_Bridge] (
    [FactorSetId]        VARCHAR (12)        NOT NULL,
    [EmissionsId]        VARCHAR (42)        NOT NULL,
    [SortKey]            INT                 NOT NULL,
    [Hierarchy]          [sys].[hierarchyid] NOT NULL,
    [DescendantId]       VARCHAR (42)        NOT NULL,
    [DescendantOperator] CHAR (1)            CONSTRAINT [DF_Emissions_Bridge_DescendantOperator] DEFAULT ('~') NOT NULL,
    [tsModified]         DATETIMEOFFSET (7)  CONSTRAINT [DF_Emissions_Bridge_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)      CONSTRAINT [DF_Emissions_Bridge_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)      CONSTRAINT [DF_Emissions_Bridge_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)      CONSTRAINT [DF_Emissions_Bridge_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION          NOT NULL,
    CONSTRAINT [PK_Emissions_Bridge] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [EmissionsId] ASC, [DescendantId] ASC),
    CONSTRAINT [CR_Emissions_Bridge_DescendantOperator] CHECK ([DescendantOperator]='~' OR [DescendantOperator]='-' OR [DescendantOperator]='+' OR [DescendantOperator]='/' OR [DescendantOperator]='*'),
    CONSTRAINT [FK_Emissions_Bridge_DescendantId] FOREIGN KEY ([DescendantId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_Emissions_Bridge_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_Emissions_Bridge_LookUp_Emissions] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_Emissions_Bridge_Parent_Ancestor] FOREIGN KEY ([FactorSetId], [EmissionsId]) REFERENCES [dim].[Emissions_Parent] ([FactorSetId], [EmissionsId]),
    CONSTRAINT [FK_Emissions_Bridge_Parent_Descendant] FOREIGN KEY ([FactorSetId], [DescendantId]) REFERENCES [dim].[Emissions_Parent] ([FactorSetId], [EmissionsId])
);


GO

CREATE TRIGGER [dim].[t_Emissions_Bridge_u]
ON [dim].[Emissions_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Emissions_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Emissions_Bridge].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[dim].[Emissions_Bridge].[EmissionsId]	= INSERTED.[EmissionsId]
		AND	[dim].[Emissions_Bridge].[DescendantId]	= INSERTED.[DescendantId];

END;