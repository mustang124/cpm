﻿CREATE  PROCEDURE [stgFact].[Insert_Compressors]
(
	@Refnum			VARCHAR (25),
	@EventNo		TINYINT,

	@Compressor		VARCHAR (5)		= NULL,
	@EventType		CHAR (8)		= NULL,
	@Duration		REAL			= NULL,
	@Component		CHAR (5)		= NULL,
	@Cause			TINYINT			= NULL,
	@EventYear		INT				= NULL,
	@Comments		VARCHAR (MAX)	= NULL,
	@CompLife		TINYINT			= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Compressors]([Refnum], [EventNo], [Compressor], [EventType], [Duration], [Component], [Cause], [EventYear], [Comments], [CompLife])
	VALUES(@Refnum, @EventNo, @Compressor, @EventType, @Duration, @Component, @Cause, @EventYear, @Comments, @CompLife);

END;
