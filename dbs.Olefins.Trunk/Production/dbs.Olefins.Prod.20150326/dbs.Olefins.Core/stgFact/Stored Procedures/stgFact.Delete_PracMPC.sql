﻿CREATE PROCEDURE [stgFact].[Delete_PracMPC]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[PracMPC]
	WHERE [Refnum] = @Refnum;

END;