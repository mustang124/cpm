﻿CREATE TABLE [Console].[ValidationFiles] (
    [StudyYear]      SMALLINT           NOT NULL,
    [Study]          VARCHAR (3)        NOT NULL,
    [Description]    NVARCHAR (100)     NOT NULL,
    [Link]           NVARCHAR (100)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ValidationFiles_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ValidationFiles_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ValidationFiles_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ValidationFiles_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ValidationFiles] PRIMARY KEY CLUSTERED ([Study] ASC, [StudyYear] DESC, [Description] ASC),
    CONSTRAINT [CL_ValidationFiles_Description] CHECK ([Description]<>''),
    CONSTRAINT [CL_ValidationFiles_Link] CHECK ([Link]<>''),
    CONSTRAINT [CL_ValidationFiles_Study] CHECK ([Study]<>'')
);


GO

CREATE TRIGGER [Console].[t_ValidationFiles_u]
	ON  [Console].[ValidationFiles]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [Console].[ValidationFiles]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[Console].[ValidationFiles].[Study]			= INSERTED.[Study]
		AND	[Console].[ValidationFiles].[StudyYear]		= INSERTED.[StudyYear]
		AND	[Console].[ValidationFiles].[Description]	= INSERTED.[Description];

END;