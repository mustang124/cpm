﻿CREATE PROCEDURE [Console].[GetConsultants]
	@StudyYear smallint,
	@Study char(3),
	@RefNum varchar(18)

AS
BEGIN

SELECT Consultant FROM [TSort] 
WHERE Consultant is not null and StudyYear =  @StudyYear and SmallRefNum =  @RefNum  
ORDER BY SmallRefNum ASC
END
