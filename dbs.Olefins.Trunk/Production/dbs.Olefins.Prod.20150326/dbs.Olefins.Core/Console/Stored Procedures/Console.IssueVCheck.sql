﻿CREATE PROCEDURE [Console].[IssueVCheck]
	@RefNum varchar(18),
	@IssueID varchar(20)
AS
BEGIN
DECLARE @Ret int
Select @Ret = Completed from Val.Checklist where Refnum='20' + @RefNum and IssueID = @IssueID
IF @Ret > 'Y'
	Return 1
ELSE
	return 0
END
