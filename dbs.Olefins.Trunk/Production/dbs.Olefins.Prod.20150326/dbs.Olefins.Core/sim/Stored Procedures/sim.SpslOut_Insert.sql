﻿CREATE PROCEDURE sim.SpslOut_Insert
(
	@QueueId			BIGINT,
	@FactorSetId		VARCHAR(12),
	@Refnum				VARCHAR(25),
	@CalDateKey			INT,
	@SimModelId			VARCHAR(12),
	@OpCondId			VARCHAR(12),
	@StreamId			VARCHAR(42),
	@StreamDescription	NVARCHAR(256),
	@RecycleId			INT,
	@DataSetId			VARCHAR(6),
	@FieldId			VARCHAR(12),
	@Value				REAL
	)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO sim.SpslOut(QueueId, FactorSetId, SimModelId, Refnum, CalDateKey, StreamId, StreamDescription, OpCondId, RecycleId, DataSetId, FieldId, Value)
	VALUES(@QueueId, @FactorSetId, @SimModelId, @Refnum, @CalDateKey, @StreamId, @StreamDescription, @OpCondId, @RecycleId, @DataSetId, @FieldId, @Value)
	;

END;