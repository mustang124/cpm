﻿CREATE TABLE [ante].[PricingScenarioConfig] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [ScenarioId]      VARCHAR (42)       NOT NULL,
    [ConfigName]      NVARCHAR (84)      NOT NULL,
    [ConfigDetail]    NVARCHAR (256)     NOT NULL,
    [PricingMethodId] VARCHAR (42)       NOT NULL,
    [RegionId]        VARCHAR (5)        NULL,
    [CountryId]       CHAR (3)           NULL,
    [LocationID]      VARCHAR (42)       NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_PricingScenarioConfig_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_PricingScenarioConfig_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_PricingScenarioConfig_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_PricingScenarioConfig_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingScenarioConfig] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ScenarioId] ASC),
    CONSTRAINT [CL_PricingScenarioConfig_ConfigDetail] CHECK ([ConfigDetail]<>''),
    CONSTRAINT [CL_PricingScenarioConfig_ConfigName] CHECK ([ConfigName]<>''),
    CONSTRAINT [CL_PricingScenarioConfig_ScenarioID] CHECK ([ScenarioId]<>''),
    CONSTRAINT [FactorSet_LookUp] CHECK ([ConfigName]<>''),
    CONSTRAINT [FK_PricingScenarioConfig_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingScenarioConfig_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingScenarioConfig_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [dim].[PricingLocationsLu] ([LocationID]),
    CONSTRAINT [FK_PricingScenarioConfig_PricingMethodID] FOREIGN KEY ([PricingMethodId]) REFERENCES [dim].[PricingMethodLu] ([MethodId]),
    CONSTRAINT [FK_PricingScenarioConfig_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId])
);


GO

CREATE TRIGGER [ante].[t_PricingScenarioConfig_u]
	ON [ante].[PricingScenarioConfig]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingScenarioConfig]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingScenarioConfig].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[PricingScenarioConfig].ScenarioId		= INSERTED.ScenarioId;

END;