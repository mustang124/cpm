﻿CREATE TABLE [ante].[ApiGhgCoefficient] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [Coefficient]    REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_ApiGhgCoefficient_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_ApiGhgCoefficient_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_ApiGhgCoefficient_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_ApiGhgCoefficient_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ApiGhgCoefficient] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_ApiGhgCoefficient_Coefficient] CHECK ([Coefficient]>=(0.0)),
    CONSTRAINT [FK_ApiGhgCoefficient_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_ApiGhgCoefficient_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_ApiGhgCoefficient]
	ON [ante].[ApiGhgCoefficient]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ApiGhgCoefficient]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[ApiGhgCoefficient].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[ApiGhgCoefficient].ComponentId	= INSERTED.ComponentId;

END;