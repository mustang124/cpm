﻿CREATE TABLE [ante].[SimCrackingParameters] (
    [FactorSetId]              VARCHAR (12)       NOT NULL,
    [OpCondId]                 VARCHAR (12)       NOT NULL,
    [StreamId]                 VARCHAR (42)       NULL,
    [CoilOutletPressure_kgcm2] REAL               NULL,
    [CoilInletPressure_kgcm2]  REAL               NULL,
    [CoilOutletTemp_C]         REAL               NULL,
    [CoilInletTemp_C]          REAL               NULL,
    [RadiantWallTemp_C]        REAL               NULL,
    [EthyleneYield_WtPcnt]     REAL               NULL,
    [PropyleneEthylene_Ratio]  REAL               NULL,
    [PropyleneMethane_Ratio]   REAL               NULL,
    [FeedConv_WtPcnt]          REAL               NULL,
    [SeverityConversion]       REAL               NULL,
    [SeverityValue]            REAL               NULL,
    [SteamHydrocarbon_Ratio]   REAL               NULL,
    [Density_SG]               REAL               NULL,
    [DistMethodID]             VARCHAR (8)        NULL,
    [FurnConstruction_Year]    REAL               NULL,
    [ResidenceTime_s]          REAL               NULL,
    [FlowRate_KgHr]            REAL               NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_SimCrackingParameters_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_SimCrackingParameters_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_SimCrackingParameters_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_SimCrackingParameters_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [CR_SimCrackingParameters_CoilInletPressure_kgcm2] CHECK ([CoilInletPressure_kgcm2]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_CoilInletTemp_C] CHECK ([CoilInletTemp_C]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_CoilOutletPressure_kgcm2] CHECK ([CoilOutletPressure_kgcm2]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_CoilOutletTemp_C] CHECK ([CoilOutletTemp_C]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_Density_SG] CHECK ([Density_SG]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_EthyleneYield_WtPcnt] CHECK ([EthyleneYield_WtPcnt]>=(0.0) AND [EthyleneYield_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_SimCrackingParameters_FeedConv_WtPcnt] CHECK ([FeedConv_WtPcnt]>=(0.0) AND [FeedConv_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_SimCrackingParameters_FlowRate_KgHr] CHECK ([FlowRate_KgHr]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_FurnConstruction_Year] CHECK ([FurnConstruction_Year]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_PropyleneEthylene_Ratio] CHECK ([PropyleneEthylene_Ratio]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_PropyleneMethane_Ratio] CHECK ([PropyleneMethane_Ratio]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_RadiantWallTemp_C] CHECK ([RadiantWallTemp_C]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_ResidenceTime_s] CHECK ([ResidenceTime_s]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_SeverityConversion] CHECK ([SeverityConversion]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_SeverityValue] CHECK ([SeverityValue]>=(0.0)),
    CONSTRAINT [CR_SimCrackingParameters_SteamHydrocarbon_Ratio] CHECK ([SteamHydrocarbon_Ratio]>=(0.0)),
    CONSTRAINT [FK_SimCrackingParameters_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_SimCrackingParameters_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_SimCrackingParameters_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [UK_SimCrackingParameters] UNIQUE CLUSTERED ([FactorSetId] ASC, [OpCondId] ASC, [StreamId] ASC)
);


GO

CREATE TRIGGER [ante].[t_SimCrackingParameters_u]
	ON [ante].[SimCrackingParameters]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[SimCrackingParameters]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[SimCrackingParameters].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[SimCrackingParameters].OpCondId			= INSERTED.OpCondId
		AND	[ante].[SimCrackingParameters].StreamId			= INSERTED.StreamId;

END;