﻿CREATE TABLE [ante].[PeerGroupEii] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [PeerGroup]      TINYINT            NOT NULL,
    [LimitLower_Eii] REAL               NOT NULL,
    [LimitUpper_Eii] REAL               NOT NULL,
    [_LimitRange]    AS                 (((('['+CONVERT([varchar](32),round([LimitLower_Eii],(2)),0))+', ')+CONVERT([varchar](32),round([LimitUpper_Eii],(2)),0))+')') PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupEii_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupEii_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupEii_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupEii_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PeerGroupEii] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PeerGroup] ASC),
    CONSTRAINT [CV_PeerGroupEii_Limit] CHECK ([LimitLower_Eii]<[LimitUpper_Eii]),
    CONSTRAINT [FK_PeerGroupEii_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_PeerGroupEii_u]
	ON [ante].[PeerGroupEii]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PeerGroupEii]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PeerGroupEii].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[PeerGroupEii].PeerGroup		= INSERTED.PeerGroup;

END;