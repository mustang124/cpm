﻿CREATE TABLE [ante].[ApiGhgEmissions] (
    [FactorSetId]      VARCHAR (12)       NOT NULL,
    [CountryId]        CHAR (3)           NOT NULL,
    [ComponentId]      VARCHAR (42)       NOT NULL,
    [Emissions_MTMWHr] REAL               NOT NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_ApiGhgEmissions_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_ApiGhgEmissions_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_ApiGhgEmissions_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_ApiGhgEmissions_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ApiGhgEmissions] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CountryId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_ApiGhgEmissions_Emissions_MTMWHr] CHECK ([Emissions_MTMWHr]>=(0.0)),
    CONSTRAINT [FK_ApiGhgEmissions_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_ApiGhgEmissions_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_ApiGhgEmissions_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_ApiGhgEmissions_u]
	ON [ante].[ApiGhgEmissions]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[ApiGhgEmissions]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[ApiGhgEmissions].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[ante].[ApiGhgEmissions].CountryId		= INSERTED.CountryId
		AND	[ante].[ApiGhgEmissions].ComponentId	= INSERTED.ComponentId;

END;