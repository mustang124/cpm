﻿CREATE TABLE [ante].[EmissionsFactorElectricity] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CountryId]      CHAR (3)           NOT NULL,
    [EmissionsId]    VARCHAR (42)       NOT NULL,
    [CEF_MTCO2_MBtu] REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsFactorElectricity_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorElectricity_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorElectricity_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_EmissionsFactorElectricity_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_EmissionsFactorElectricity] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CountryId] ASC, [EmissionsId] ASC),
    CONSTRAINT [FK_EmissionsFactorElectricity_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_EmissionsFactorElectricity_Emissions_LookUp] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_EmissionsFactorElectricity_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_EmissionsFactorElectricity_u]
	ON [ante].[EmissionsFactorElectricity]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[EmissionsFactorElectricity]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[EmissionsFactorElectricity].[FactorSetId]		= INSERTED.[FactorSetId]
		AND	[ante].[EmissionsFactorElectricity].CountryId		= INSERTED.CountryId
		AND	[ante].[EmissionsFactorElectricity].EmissionsId		= INSERTED.EmissionsId;

END;