﻿CREATE TABLE [etl].[MapCompanies] (
    [ReturnCompanyId] VARCHAR (42)       NOT NULL,
    [CompanyId]       VARCHAR (25)       NULL,
    [CoName]          VARCHAR (100)      NULL,
    [Co]              VARCHAR (40)       NULL,
    [ContactCode]     VARCHAR (25)       NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_MapCompanies_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (128)     CONSTRAINT [DF_MapCompanies_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (128)     CONSTRAINT [DF_MapCompanies_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (128)     CONSTRAINT [DF_MapCompanies_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]    ROWVERSION         NOT NULL,
    CONSTRAINT [CL_MapCompanies_Co] CHECK ([Co]<>''),
    CONSTRAINT [CL_MapCompanies_CompanyID] CHECK ([CompanyId]<>''),
    CONSTRAINT [CL_MapCompanies_CoName] CHECK ([CoName]<>''),
    CONSTRAINT [CL_MapCompanies_ContactCode] CHECK ([ContactCode]<>''),
    CONSTRAINT [FK_MapCompanies_Companies] FOREIGN KEY ([ReturnCompanyId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [UK_MapCompanies] UNIQUE CLUSTERED ([ReturnCompanyId] ASC, [CompanyId] ASC, [Co] ASC, [CoName] ASC, [ContactCode] ASC)
);


GO

CREATE TRIGGER [etl].[t_MapCompanies_u]
ON [etl].[MapCompanies]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [etl].[MapCompanies]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[etl].[MapCompanies].[CompanyId]		= INSERTED.[CompanyId]
		AND	[etl].[MapCompanies].[CoName]			= INSERTED.[CoName]
		AND	[etl].[MapCompanies].[Co]				= INSERTED.[Co]
		AND	[etl].[MapCompanies].[ContactCode]		= INSERTED.[ContactCode];

END;
