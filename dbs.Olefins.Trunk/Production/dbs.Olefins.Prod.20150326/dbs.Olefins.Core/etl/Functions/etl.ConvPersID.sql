﻿
CREATE FUNCTION etl.ConvPersId
(
	@PersId	NVARCHAR(34)
)
RETURNS NVARCHAR(34)
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT 
AS
BEGIN

	DECLARE @ResultVar	NVARCHAR(34) =
	CASE RTRIM(LTRIM(@PersId))

		--WHEN 'Tot'						THEN 'PlantTotal'

		WHEN 'OCC'						THEN 'OccSubTotal'
		WHEN 'OP'						THEN 'OccProc'
		WHEN 'OCCOpProc'				THEN 'OccProcProc'
		WHEN 'OCCOpUtil'				THEN 'OccProcUtil'
		WHEN 'OCCOpOffsite'				THEN 'OccProcOffSite'
		WHEN 'OCCOpRacks'				THEN 'OccProcTruckRail'
		WHEN 'OCCOpMarine'				THEN 'OccProcMarine'
		WHEN 'OCCOpRelief'				THEN 'OccProcRelief'
		WHEN 'OCCOpTrng'				THEN 'OccProcTraining'
		WHEN 'OCCOpOth'					THEN 'OccProcOther'
		
		WHEN 'OM'						THEN 'OccMaint'
		--WHEN ''						THEN 'OccMaintPlant'
		WHEN 'OCCMaintRtn'				THEN 'OccMaintRoutine'
		WHEN 'OCCMaintShop'				THEN 'OccMaintShop'
		WHEN 'OCCMaintPlan'				THEN 'OccMaintPlan'
		WHEN 'OCCMaintWhs'				THEN 'OccMaintWarehouse'
		WHEN 'OCCMaintInsp'				THEN 'OccMaintInsp'
		WHEN 'OCCMaintTrng'				THEN 'OccMaintTraining'
		WHEN 'OCCMaintTA'				THEN 'OccMaintTaMaint'
		WHEN 'OCCAnnRetube'				THEN 'OccMaintTaAnnRetube'
		
		WHEN 'OA'						THEN 'OccAdmin'
		WHEN 'OCCAdmLab'				THEN 'OccAdminLab'
		WHEN 'OCCAdmAcct'				THEN 'OccAdminAcct'
		WHEN 'OCCAdmPurch'				THEN 'OccAdminPurchase'
		WHEN 'OCCAdmSecurity'			THEN 'OccAdminSecurity'
		WHEN 'OCCAdmClerical'			THEN 'OccAdminClerical'
		WHEN 'OCCAdmOth'				THEN 'OccAdminOther'

		WHEN 'Mps'						THEN 'MpsSubTotal'
		WHEN 'MP'						THEN 'MpsProc'
		WHEN 'MpsProcess'				THEN 'MpsProcProc'

		WHEN 'MM'						THEN 'MpsMaint'
		--WHEN ''						THEN 'MpsMaintPlant'
		WHEN 'MpsMaintRtn'				THEN 'MpsMaintRoutine'
		WHEN 'MpsMaintShop'				THEN 'MpsMaintShop'
		WHEN 'MpsMaintPlan'				THEN 'MpsMaintPlan'
		WHEN 'MpsMaintWhs'				THEN 'MpsMaintWarehouse'
		WHEN 'MpsMaintTA'				THEN 'MpsMaintTaMaint'
		WHEN 'MpsAnnRetube'				THEN 'MpsMaintTaAnnRetube'
		
		WHEN 'MT'						THEN 'MpsTech'
		WHEN 'MpsTechProc'				THEN 'MpsTechProc'
		WHEN 'MpsTechEcon'				THEN 'MpsTechEcon'
		WHEN 'MpsTechRely'				THEN 'MpsTechRely'
		WHEN 'MpsTechInsp'				THEN 'MpsTechInsp'
		WHEN 'MpsTechAPC'				THEN 'MpsTechAPC'
		WHEN 'MpsTechEnvir'				THEN 'MpsTechEnviron'
		WHEN 'MpsTechOth'				THEN 'MpsTechOther'
		
		WHEN 'MA'						THEN 'MpsAdmin'
		WHEN 'ML'						THEN 'MpsAdmin'
		WHEN 'MpsLab'					THEN 'MpsAdminLab'
		WHEN 'MpsAdmAcct'				THEN 'MpsAdminAcct'
		WHEN 'MpsAdmMIS'				THEN 'MpsAdminMIS'
		WHEN 'MpsAdmPurch'				THEN 'MpsAdminPurchase'
		WHEN 'MpsAdmSecurity'			THEN 'MpsAdminSecurity'
		WHEN 'MpsAdmOth'				THEN 'MpsAdminOther'
		
		WHEN 'MG'						THEN 'MpsGeneral'
		WHEN 'MpsFeedStaff'				THEN 'MpsGenPurchase'
		WHEN 'MpsGA'					THEN 'MpsGenOther'

		WHEN 'BEA'						THEN 'BeaSubTotal'
		WHEN 'BEAProdPlan'				THEN 'BeaProdPlan'
		WHEN 'BEAProdPlanExperienced'	THEN 'BeaProdPlanExperienced'
		WHEN 'BEATrader'				THEN 'BeaTrader'
		WHEN 'BEADataAnalyst'			THEN 'BeaAnalyst'
		WHEN 'BEALogistics'				THEN 'BeaLogistics'
	END;
	
	RETURN @ResultVar;
	
END;