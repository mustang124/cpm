﻿CREATE TABLE [fact].[ReliabilityPyroFurn] (
    [Refnum]                VARCHAR (25)       NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [FurnID]                INT                NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [FeedQty_kMT]           REAL               NULL,
    [FeedCap_MTd]           REAL               NULL,
    [FuelTypeID]            VARCHAR (42)       NULL,
    [FuelCons_kMT]          REAL               NULL,
    [StackOxygen_Pcnt]      REAL               NULL,
    [ArchDraft_H20]         REAL               NULL,
    [StackTemp_C]           REAL               NULL,
    [CrossTemp_C]           REAL               NULL,
    [Retubed_Year]          SMALLINT           NULL,
    [RetubeInterval_Mnths]  REAL               NULL,
    [_RetubeInterval_Years] AS                 (CONVERT([real],[RetubeInterval_Mnths]/(12.0),(1))) PERSISTED,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityPyroFurn_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurn_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurn_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_ReliabilityPyroFurn_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityPyroFurn] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FurnID] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_ReliabilityPyroFurn_ArchDraftH20] CHECK ([ArchDraft_H20]<=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_CrossTemp_C] CHECK ([CrossTemp_C]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_FeedCapMTd] CHECK ([FeedCap_MTd]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_FeedQtyKMT] CHECK ([FeedQty_kMT]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_FuelConsKMT] CHECK ([FuelCons_kMT]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_FurnID] CHECK ([FurnID]>=(0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_Retubed_Year] CHECK ([Retubed_Year]>=(0.0) AND len([Retubed_Year])=(4)),
    CONSTRAINT [CR_ReliabilityPyroFurn_RetubeIntervalMnths] CHECK ([RetubeInterval_Mnths]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_StackOxygen] CHECK ([StackOxygen_Pcnt]>=(0.0) AND [StackOxygen_Pcnt]<=(100.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_StackTemp] CHECK ([StackTemp_C]>=(0.0)),
    CONSTRAINT [CR_ReliabilityPyroFurn_StackTemp_C] CHECK ([StackTemp_C]>=(0.0)),
    CONSTRAINT [FK_ReliabilityPyroFurn_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityPyroFurn_FuelType_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReliabilityPyroFurn_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_ReliabilityPyroFurn_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityPyroFurn_u]
	ON [fact].[ReliabilityPyroFurn]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [fact].[ReliabilityPyroFurn]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityPyroFurn].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityPyroFurn].FurnID		= INSERTED.FurnID
		AND [fact].[ReliabilityPyroFurn].CalDateKey	= INSERTED.CalDateKey;

END;