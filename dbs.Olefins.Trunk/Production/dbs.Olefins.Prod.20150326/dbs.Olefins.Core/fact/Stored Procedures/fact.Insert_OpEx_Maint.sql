﻿CREATE PROCEDURE [fact].[Insert_OpEx_Maint]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.OpEx(Refnum, CalDateKey, AccountId, CurrencyRpt, Amount_Cur)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvAccountID(u.AccountId) [AccoutnID]
			, 'USD'
			, u.Amount_RPT
		FROM (
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum]
				, etl.ConvDateKey(t.StudyYear - 1)							[CalDateKey]
				, m.MaintMatlPY
				, m.ContMaintLaborPY
				, m.ContMaintMatlPY
				, m.MaintEquipRentPY
			FROM stgFact.MaintMisc m
			INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
			WHERE t.Refnum = @sRefnum
			) p
			UNPIVOT(
			Amount_RPT FOR AccountId IN(
				  p.MaintMatlPY
				, p.ContMaintLaborPY
				, p.ContMaintMatlPY
				, p.MaintEquipRentPY
				)
			)u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;