﻿CREATE PROCEDURE [fact].[Insert_Energy]
(
	@Refnum		VARCHAR(18)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyLHValue';

	INSERT INTO fact.EnergyLHValue(Refnum, CalDateKey, AccountId, LHValue_MBTU)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvEnergyID(e.EnergyType)						[AccountId]
		, e.MBTU
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t ON t.Refnum = e.Refnum
	WHERE e.MBTU <> 0.0
		AND e.EnergyType NOT IN ('ElecPower', 'ExpElecPower')
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyPrice';

	INSERT INTO fact.EnergyPrice(Refnum, CalDateKey, AccountId, CurrencyRpt, Amount_Cur)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvEnergyID(e.EnergyType)						[AccountId]
		, 'USD'
		, e.Cost
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t ON t.Refnum = e.Refnum
	WHERE e.Cost <> 0.0
	AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyComposition';

	INSERT INTO fact.EnergyComposition(Refnum, CalDateKey, AccountId, ComponentId, Component_WtPcnt)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvEnergyID(e.EnergyType)						[AccountId]
		, 'CH4'													[ComponentId]
		, e.Methane_WtPcnt * 100.0								[WtPcnt]
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t ON t.Refnum = e.Refnum
	WHERE e.Methane_WtPcnt <> 0.0
	AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergySteam';

	INSERT INTO fact.EnergySteam(Refnum, CalDateKey, AccountId, Amount_kMT, Pressure_Barg, Temp_C)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvEnergyID(e.EnergyType)						[AccountId]
		, CASE WHEN t.UOM = 2 AND t.StudyYear IN (2007, 2009)
			THEN	[$(DbGlobal)].dbo.UnitsConv(ABS(e.Amount), 'MLB', 'KMT')
			ELSE	ABS(e.Amount)
			END
		, [$(DbGlobal)].dbo.UnitsConv(e.PSIG, 'PSIG', 'BARG')
		, CASE WHEN t.UOM = 2 AND t.StudyYear IN (2007, 2009) AND etl.ConvEnergyID(e.EnergyType) IN ('PurSteamHP', 'PurSteamIP', 'PurSteamSHP')
			THEN	[$(DbGlobal)].dbo.UnitsConv(e.Temperature, 'TF', 'TC')
			ELSE	e.Temperature
			END
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
	--INNER JOIN dim.AccountLu d		ON	d.AccountId = etl.ConvEnergyID(RTRIM(LTRIM(e.EnergyType)))
	--INNER JOIN dim.AccountLu a		ON	d.Hierarchy.IsDescendantOf(a.Hierarchy) = 1
	--								AND	a.AccountId IN ('ExportSteam', 'PurSteam')
	WHERE	e.Amount <> 0.0
		AND	etl.ConvEnergyID(RTRIM(LTRIM(e.EnergyType))) IN (SELECT b.DescendantId FROM dim.Account_Bridge b WHERE b.AccountId IN ('ExportSteam', 'PurSteam'))
	AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyElec'

	INSERT INTO fact.EnergyElec(Refnum, CalDateKey, AccountId, Amount_MWh, Rate_BTUkWh, CurrencyRpt, Price_Cur)
	SELECT
		etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
		etl.ConvDateKey(t.StudyYear)						[CalDateKey],
		etl.ConvEnergyID(e.EnergyType)						[AccountId],
		e.Amount											[Amount],
		COALESCE(e.MBTU / e.Amount, 9.09)					[Rate],
		'USD',
		e.Cost
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
	WHERE	e.EnergyType IN ('ElecPower', 'ExpElecPower')
		AND	e.Amount > 0.0
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyCogen';

	INSERT INTO fact.EnergyCogen(Refnum, CalDateKey, HasCogen_Bit, Thermal_Pcnt)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvBit(e.YN)										[HasCogen]
		, e.ThermalPct * 100.0									[ThermalPcnt]
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
	WHERE (e.YN IS NOT NULL OR e.ThermalPct <> 0.0)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyCogenFacilities';

	INSERT INTO fact.EnergyCogenFacilities(Refnum, CalDateKey, FacilityId, Unit_Count, Capacity_MW, Efficiency_Pcnt)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvFacilityId(e.EnergyType)						[FacilityId]
		, e.UnitCnt
		, e.Amount
		, e.Efficiency * 100.0
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
	--INNER JOIN dim.FacilitiesLu d	ON	d.FacilityId =  etl.ConvFacilityId(e.EnergyType)
	--INNER JOIN dim.FacilitiesLu a	ON	d.Hierarchy.IsDescendantOf(a.Hierarchy) = 1
	--								AND	a.FacilityId = 'Cogen'
	WHERE (e.UnitCnt <> 0 OR e.Amount <> 0.0 OR e.Efficiency <> 0.0)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		AND	etl.ConvFacilityId(e.EnergyType) IN (SELECT b.DescendantId FROM dim.Facility_Bridge b WHERE b.FacilityId = 'Cogen');

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyCogenFacilitiesNameOther';

	INSERT INTO fact.EnergyCogenFacilitiesNameOther(Refnum, CalDateKey, FacilityId, FacilityName)
	SELECT
		  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
		, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
		, etl.ConvFacilityId(e.EnergyType)						[FacilityId]
		, e.OthDesc
	FROM stgFact.EnergyQnty e
	INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
	WHERE e.EnergyType = 'Other'
		AND	e.OthDesc IS NOT NULL
		AND (e.UnitCnt <> 0 OR e.Amount <> 0.0 OR e.Efficiency <> 0.0)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	PRINT NCHAR(9) + 'INSERT INTO fact.EnergyCogenPcnt';

	INSERT INTO fact.EnergyCogenPcnt(Refnum, CalDateKey, FacilityId, AccountId, CogenEnergy_Pcnt)
	SELECT
		  u.Refnum
		, u.CalDateKey
		, u.FacilityId
		, u.AccountId
		, u.Pcnt * 100.0	[Pcnt]
	FROM(
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, e.EnergyType											[FacilityId]
			, e.ElecPowerPct										[PurElec]
			, e.PurchaseSteamPct									[PurSteam]
		FROM stgFact.EnergyQnty e
		INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
		WHERE e.EnergyType = 'Cogen'
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
		) u
		UNPIVOT(
		Pcnt FOR AccountId IN (
			  [PurElec]
			, [PurSteam]
			)
		) u;

END;