﻿CREATE PROCEDURE [fact].[Insert_ReliabilityOppLoss]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (FFPCauseBalance - Current)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear)					[CalDateKey],
			CASE WHEN a.CauseID IS NULL
				THEN 'FFPCauses'
				ELSE 'FFPCauseBalance'
				END											[OppLossId],	
			'Ethylene'										[StreamId],
			a.DTLoss - SUM(l.DTLoss),
			a.SDLoss - SUM(l.SDLoss)
		FROM stgFact.ProdLoss								l
		INNER JOIN stgFact.TSort							t
			ON	t.Refnum = l.Refnum
		LEFT OUTER JOIN stgFact.ProdLoss					a
			ON	a.Refnum	= l.Refnum
			AND	a.Category	= 'PRO'
			AND	a.CauseID	= 'Fouling'
		WHERE	t.Refnum = @sRefnum
			AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'FFPCauses' AND d.DescendantId <> d.OppLossId)
		GROUP BY
			t.Refnum,
			t.StudyYear,
			a.CauseID,
			a.DTLoss,
			a.SDLoss
		HAVING ((a.DTLoss - SUM(l.DTLoss) <> 0.0) OR (a.SDLoss - SUM(l.SDLoss) <> 0.0));

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (FFPCauseBalance - Previous)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear - 1)				[CalDateKey],
			CASE WHEN a.CauseID IS NULL
				THEN 'FFPCauses'
				ELSE 'FFPCauseBalance'
				END											[OppLossId],	
			'Ethylene'										[StreamId],
			a.PrevDTLoss - SUM(l.PrevDTLoss),
			a.PrevSDLoss - SUM(l.PrevSDLoss)
		FROM stgFact.ProdLoss								l
		INNER JOIN stgFact.TSort							t
			ON	t.Refnum = l.Refnum
		LEFT OUTER JOIN stgFact.ProdLoss					a
			ON	a.Refnum = l.Refnum
			AND	a.Category	= 'PRO'
			AND	a.CauseID	= 'Fouling'
		WHERE	t.Refnum = @sRefnum
			AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'FFPCauses' AND d.DescendantId <> d.OppLossId)
		GROUP BY
			t.Refnum,
			t.StudyYear,
			a.CauseID,
			a.PrevDTLoss,
			a.PrevSDLoss
		HAVING ((a.PrevDTLoss - SUM(l.PrevDTLoss) <> 0.0) OR (a.PrevSDLoss - SUM(l.PrevSDLoss) <> 0.0));

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (ProcessOtherBalance - Current)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear)					[CalDateKey],
			CASE WHEN a.CauseID IS NULL
				THEN 'ProcessOther'
				ELSE 'ProcessOtherBalance'
				END											[OppLossId],	
			'Ethylene'										[StreamId],
			a.DTLoss - SUM(l.DTLoss),
			a.SDLoss - SUM(l.SDLoss)
		FROM stgFact.ProdLoss								l
		INNER JOIN stgFact.TSort							t
			ON	t.Refnum = l.Refnum
		LEFT OUTER JOIN stgFact.ProdLoss					a
			ON	a.Refnum	= l.Refnum
			AND	a.Category	= 'PRO'
			AND	a.CauseID	= 'OtherProcess'
		WHERE	t.Refnum = @sRefnum
			AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'ProcessOther' AND d.DescendantId <> d.OppLossId)
		GROUP BY
			t.Refnum,
			t.StudyYear,
			a.CauseID,
			a.DTLoss,
			a.SDLoss
		HAVING ((a.DTLoss - SUM(l.DTLoss) <> 0.0) OR (a.SDLoss - SUM(l.SDLoss) <> 0.0));

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (ProcessOtherBalance - Previous)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear - 1)				[CalDateKey],
			CASE WHEN a.CauseID IS NULL
				THEN 'ProcessOther'
				ELSE 'ProcessOtherBalance'
				END											[OppLossId],	
			'Ethylene'										[StreamId],
			a.PrevDTLoss - SUM(l.PrevDTLoss),
			a.PrevSDLoss - SUM(l.PrevSDLoss)
		FROM stgFact.ProdLoss								l
		INNER JOIN stgFact.TSort							t
			ON	t.Refnum = l.Refnum
		LEFT OUTER JOIN stgFact.ProdLoss					a
			ON	a.Refnum = l.Refnum
			AND	a.Category	= 'PRO'
			AND	a.CauseID	= 'OtherProcess'
		WHERE	t.Refnum = @sRefnum
			AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'ProcessOther' AND d.DescendantId <> d.OppLossId)
		GROUP BY
			t.Refnum,
			t.StudyYear,
			a.CauseID,
			a.PrevDTLoss,
			a.PrevSDLoss
		HAVING ((a.PrevDTLoss - SUM(l.PrevDTLoss) <> 0.0) OR (a.PrevSDLoss - SUM(l.PrevSDLoss) <> 0.0));

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (OtherNonOpBalance - Current)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum],
			etl.ConvDateKey(t.StudyYear)							[CalDateKey],
			etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId],
			'Ethylene'												[StreamId],
			ABS(p.DTLoss)											[DownTimeLossMT],
			ABS(p.SDLoss)											[SlowDownLossMT]
		FROM stgFact.ProdLoss p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	p.CauseID	= 'OtherNonOp'
			AND	(p.DTLoss <> 0.0 OR p.SDLoss <> 0.0)
			AND t.Refnum = @sRefnum
			AND	etl.ConvDateKey(t.StudyYear) < 20130000;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear)					[CalDateKey],
			CASE WHEN a.CauseID IS NULL
				THEN 'OtherNonOp'
				ELSE 'OtherNonOpBalance'
				END											[OppLossId],	
			'Ethylene'										[StreamId],
			a.DTLoss - SUM(l.DTLoss),
			a.SDLoss - SUM(l.SDLoss)
		FROM stgFact.ProdLoss								l
		INNER JOIN stgFact.TSort							t
			ON	t.Refnum = l.Refnum
		LEFT OUTER JOIN stgFact.ProdLoss					a
			ON	a.Refnum	= l.Refnum
			AND	a.Category	= 'NOP'
			AND	a.CauseID	= 'OtherNonOp'
		WHERE	t.Refnum = @sRefnum
			AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'OtherNonOp' AND d.DescendantId <> d.OppLossId)
			AND	etl.ConvDateKey(t.StudyYear) > 20130000
		GROUP BY
			t.Refnum,
			t.StudyYear,
			a.CauseID,
			a.DTLoss,
			a.SDLoss
		HAVING ((a.DTLoss - SUM(l.DTLoss) <> 0.0) OR (a.SDLoss - SUM(l.SDLoss) <> 0.0));

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (OtherNonOpBalance - Previous)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear - 1)				[CalDateKey],
			etl.ConvReliabilityOppLossId(p.CauseID)			[ReliabilityOppLossId],
			'Ethylene'										[StreamId],
			ABS(p.PrevDTLoss)								[DownTimeLossMT],
			ABS(p.PrevSDLoss)								[SlowDownLossMT]
		FROM stgFact.ProdLoss p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	p.CauseID	= 'OtherNonOp'
			AND	(p.PrevDTLoss <> 0.0 OR p.PrevSDLoss <> 0.0)
			AND t.Refnum = @sRefnum
			AND	etl.ConvDateKey(t.StudyYear) < 20130000;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear - 1)				[CalDateKey],
			CASE WHEN a.CauseID IS NULL
				THEN 'OtherNonOp'
				ELSE 'OtherNonOpBalance'
				END											[OppLossId],	
			'Ethylene'										[StreamId],
			a.PrevDTLoss - SUM(l.PrevDTLoss),
			a.PrevSDLoss - SUM(l.PrevSDLoss)
		FROM stgFact.ProdLoss								l
		INNER JOIN stgFact.TSort							t
			ON	t.Refnum = l.Refnum
		LEFT OUTER JOIN stgFact.ProdLoss					a
			ON	a.Refnum = l.Refnum
			AND	a.Category	= 'NOP'
			AND	a.CauseID	= 'OtherNonOp'
		WHERE	t.Refnum = @sRefnum
			AND etl.ConvReliabilityOppLossId(l.CauseID) IN (SELECT d.DescendantId FROM dim.ReliabilityOppLoss_Bridge d WHERE d.OppLossId = 'OtherNonOp' AND d.DescendantId <> d.OppLossId)
			AND	etl.ConvDateKey(t.StudyYear) > 20130000
		GROUP BY
			t.Refnum,
			t.StudyYear,
			a.CauseID,
			a.PrevDTLoss,
			a.PrevSDLoss
		HAVING ((a.PrevDTLoss - SUM(l.PrevDTLoss) <> 0.0) OR (a.PrevSDLoss - SUM(l.PrevSDLoss) <> 0.0));

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (Current)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum],
			etl.ConvDateKey(t.StudyYear)							[CalDateKey],
			etl.ConvReliabilityOppLossId(p.CauseID)					[ReliabilityOppLossId],
			'Ethylene'												[StreamId],
			ABS(p.DTLoss)											[DownTimeLossMT],
			ABS(p.SDLoss)											[SlowDownLossMT]
		FROM stgFact.ProdLoss p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	p.CauseID	NOT IN ('Fouling', 'OtherProcess', 'OtherNonOp')
			AND	(p.DTLoss <> 0.0 OR p.SDLoss <> 0.0)
			AND t.Refnum = @sRefnum;

		SET @ProcedureDesc = NCHAR(9) + 'INSERT INTO fact.ReliabilityOppLoss (Previous)';
		PRINT @ProcedureDesc;

		INSERT INTO fact.ReliabilityOppLoss(Refnum, CalDateKey, OppLossId, StreamId, DownTimeLoss_MT, SlowDownLoss_MT)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)	[Refnum],
			etl.ConvDateKey(t.StudyYear - 1)				[CalDateKey],
			etl.ConvReliabilityOppLossId(p.CauseID)			[ReliabilityOppLossId],
			'Ethylene'										[StreamId],
			ABS(p.PrevDTLoss)								[DownTimeLossMT],
			ABS(p.PrevSDLoss)								[SlowDownLossMT]
		FROM stgFact.ProdLoss p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE	p.CauseID	NOT IN ('Fouling', 'OtherProcess', 'OtherNonOp')
			AND	(p.PrevDTLoss <> 0.0 OR p.PrevSDLoss <> 0.0)
			AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;