﻿CREATE PROCEDURE [fact].[Insert_CompositionQuantity_OtherProd]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.FeedProdID)							[StreamId]
			, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
			, etl.ConvComponentID(u.ComponentId)						[ComponentId]
			, u.WtPcnt * 100.0											[WtPcnt]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				
				, c.FeedProdID
			
				, CASE q.FeedProdID
					WHEN 'OthSpl'		THEN q.MiscFeed
					WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
					WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
					END													[StreamDescription]
			
				, c.H2			[H2]			-- Hydrogen
				, c.CH4			[CH4]			-- Methane
			
				, c.C2H2		[C2H2]			-- Acetylene
				, c.C2H4		[C2H4]			-- Ethylene
				, c.C2H6		[C2H6]			-- Ethane
			
				, c.C3H6		[C3H6]			-- Propylene
				, c.C3H8		[C3H8]			-- Propane
			
				, c.BUTAD		[C4H6]			-- Butadiene
				, c.C4S			[C4H8]			-- Butene
				, c.C4H10		[C4H10]			-- Butane
				, c.BZ			[C6H6]			-- Benzene
				--, c.PYGAS		[C9_400F]		-- PyroGasOil	/* Component discreptancy in calcs!YIRCalc	*/
				--, c.PYOIL		[PyroFuelOil]

				, c.PYGAS		[PyroGasoline]
				, c.PYOIL		[PyroFuelOil]

				, c.INERT		[Inerts]
			
			FROM stgFact.Composition c
			INNER JOIN stgFact.TSort t
				ON t.Refnum = c.Refnum
			INNER JOIN stgFact.Quantity q
				ON q.Refnum = c.Refnum
				AND q.FeedProdID = c.FeedProdID
				AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
			WHERE	(c.H2		> 0.0
				OR	c.CH4		> 0.0
				OR	c.C2H2		> 0.0
				OR	c.C2H4		> 0.0
				OR	c.C2H6		> 0.0
				OR	c.C3H6		> 0.0
				OR	c.C3H8		> 0.0
				OR	c.BUTAD		> 0.0
				OR	c.C4S		> 0.0
				OR	c.C4H10		> 0.0
				OR	c.BZ		> 0.0
				OR	c.PYGAS		> 0.0
				OR	c.PYOIL		> 0.0
				OR	c.INERT		> 0.0)
				AND t.Refnum = @sRefnum
			) p
			UNPIVOT(
			WtPcnt FOR ComponentId IN (
				H2,
				CH4,
				C2H2,
				C2H4,
				C2H6,
				C3H6,
				C3H8,
				C4H6,
				C4H8,
				C4H10,
				C6H6,
				PyroGasoline,
				-- PyroGasOil, /* Component discreptancy in calcs!YIRCalc	*/
				PyroFuelOil,
				Inerts
				)
			) u
		WHERE u.WtPcnt > 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;