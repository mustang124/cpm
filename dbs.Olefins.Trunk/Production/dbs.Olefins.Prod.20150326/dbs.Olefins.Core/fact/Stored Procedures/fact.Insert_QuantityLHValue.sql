﻿CREATE PROCEDURE [fact].[Insert_QuantityLHValue]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.QuantityLHValue(Refnum, CalDateKey, StreamId, StreamDescription, LHValue_MBtuLb)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.FeedProdID)							[StreamId]
			, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
			, u.HeatValue
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				
				, p.FeedProdID
			
				, CASE q.FeedProdID
					WHEN 'OthSpl'		THEN q.MiscFeed
					WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
					WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
					END													[StreamDescription]
				
				, p.HeatValue
			FROM stgFact.ProdQuality p
			INNER JOIN stgFact.Quantity q
				ON	q.Refnum = p.Refnum
				AND q.FeedProdID = p.FeedProdID
				AND (ISNULL(q.AnnFeedProd, 0.0) + ISNULL(q.Q1Feed, 0.0) + ISNULL(q.Q2Feed, 0.0) + ISNULL(q.Q3Feed, 0.0) + ISNULL(q.Q4Feed, 0.0)) <> 0.0
			INNER JOIN stgFact.TSort t		ON t.Refnum = p.Refnum
			WHERE p.HeatValue > 0.0
			AND t.Refnum = @sRefnum
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;