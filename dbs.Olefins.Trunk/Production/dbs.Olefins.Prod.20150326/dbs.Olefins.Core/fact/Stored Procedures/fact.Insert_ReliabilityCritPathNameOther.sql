﻿CREATE PROCEDURE [fact].[Insert_ReliabilityCritPathNameOther]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ReliabilityCritPathNameOther(Refnum, CalDateKey, FacilityId, FacilityName)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'CritPathOther'										[FacilityId]
			, p.OthCPMDesc											[FacilityName]
		FROM stgFact.PracTA p
		INNER JOIN stgFact.TSort t ON t.Refnum = p.Refnum
		WHERE p.OthCPMDesc IS NOT NULL AND p.Other IS NOT NULL
		AND t.Refnum = @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;