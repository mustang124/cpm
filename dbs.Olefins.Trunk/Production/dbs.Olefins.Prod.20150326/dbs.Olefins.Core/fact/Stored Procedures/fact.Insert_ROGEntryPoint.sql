﻿CREATE PROCEDURE [fact].[Insert_ROGEntryPoint]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.ROGEntryPoint(Refnum, CalDateKey, CompGasInlet_Bit, CompGasDischarge_Bit, C2Recovery_Bit, C3Recovery_Bit)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvBit(m.ROG_CGCI)
			, etl.ConvBit(m.ROG_CGCD)
			, etl.ConvBit(m.ROG_C2RSF)
			, etl.ConvBit(m.ROG_C3RSF)	
		FROM stgFact.Misc m
		INNER JOIN stgFact.TSort t ON	t.Refnum = m.Refnum
		WHERE	(m.ROG_CGCI IS NOT NULL
			OR	m.ROG_CGCD IS NOT NULL
			OR	m.ROG_C2RSF IS NOT NULL
			OR	m.ROG_C3RSF IS NOT NULL)
			AND t.Refnum = @sRefnum

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;