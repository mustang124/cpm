﻿CREATE PROCEDURE [fact].[Insert_CompositionQuantity_Sulfur]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.FeedProdID)							[StreamId]
			, ISNULL(u.StreamDescription, u.FeedProdID)					[StreamDescription]
			, etl.ConvComponentID(u.ComponentId)						[ComponentId]
			, u.Component_WtPcnt										[WtPcnt]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, q.FeedProdID
				, CASE q.FeedProdID
					--WHEN 'OthSpl'		THEN q.MiscFeed
					--WHEN 'OthProd1'		THEN q.MiscProd1 + ' (Other Prod 1)'
					--WHEN 'OthProd2'		THEN q.MiscProd2 + ' (Other Prod 2)'
				
					WHEN 'OthLiqFeed1'	THEN q.OthLiqFeedDESC + ' (Other Feed 1)'
					WHEN 'OthLiqFeed2'	THEN q.OthLiqFeedDESC + ' (Other Feed 2)'
					WHEN 'OthLiqFeed3'	THEN q.OthLiqFeedDESC + ' (Other Feed 3)'
					WHEN 'OthLtFeed'	THEN q.OthLiqFeedDESC
					ELSE q.FeedProdID
					END												[StreamDescription]
			
			, 'S'													[ComponentId]
			, q.SulfurPPM / 10000.0									[Component_WtPcnt]
			
			FROM stgFact.FeedQuality q
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = q.Refnum
			INNER JOIN stgFact.Quantity z
				ON	z.Refnum = q.Refnum
				AND	z.FeedProdID = q.FeedProdID
				AND (ISNULL(z.AnnFeedProd, 0.0) + ISNULL(z.Q1Feed, 0.0) + ISNULL(z.Q2Feed, 0.0) + ISNULL(z.Q3Feed, 0.0) + ISNULL(z.Q4Feed, 0.0)) <> 0.0
				AND q.SulfurPPM > 0.0
				AND t.Refnum = @sRefnum
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;