﻿CREATE PROCEDURE [fact].[Insert_TsortContact_DC]
(
	@Refnum		VARCHAR(25),
	@sRefnum	VARCHAR(25),
	@StudyId	VARCHAR(4)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.TSortContact([Refnum], [ContactTypeId], [NameFull], [NameTitle],
			[AddressPOBox], [AddressStreet], [AddressCity], [AddressState], [AddressCountry], [AddressCountryId], [AddressPostalCode],
			[NumberVoice], [NumberFax], [NumberMobile], [eMail])
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, @StudyId)				[Refnum],
			'Coord',
			t.CoordName,
			CASE WHEN t.CoordTitle	<> '' THEN t.CoordTitle		ELSE NULL END,

			CASE WHEN t.POBox		<> '' THEN t.POBox			ELSE NULL END,
			CASE WHEN t.Street		<> '' THEN t.Street			ELSE NULL END,
			CASE WHEN t.City		<> '' THEN t.City			ELSE NULL END,
			CASE WHEN t.[State]		<> '' THEN t.[State]		ELSE NULL END,
			CASE WHEN t.Country		<> '' THEN t.Country		ELSE NULL END,
			etl.ConvCountryID(t.Refnum, t.Country),
			CASE WHEN t.Zip			<> '' THEN t.Zip			ELSE NULL END,

			CASE WHEN t.Telephone	<> '' THEN t.Telephone		ELSE NULL END,
			CASE WHEN t.Fax			<> '' THEN t.Fax			ELSE NULL END,
			NULL,
			CASE WHEN t.WWW			<> '' THEN t.WWW			ELSE NULL END
		FROM stgFact.TSort				t
		INNER JOIN fact.TSortClient		c
			ON	c.Refnum		= @Refnum
		LEFT OUTER JOIN fact.TSortContact	x
			ON	x.Refnum		= c.Refnum
			AND	x.ContactTypeId = 'Coord'
		WHERE	t.CoordName		IS NOT NULL
			AND	t.CoordName		<> ''
			AND	x.Refnum		IS NULL
			AND t.Refnum		= @sRefnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;