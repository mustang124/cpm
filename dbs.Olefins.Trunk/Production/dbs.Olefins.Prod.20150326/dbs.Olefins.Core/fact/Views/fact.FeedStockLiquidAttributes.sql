﻿CREATE VIEW fact.FeedStockLiquidAttributes
WITH SCHEMABINDING
AS
SELECT
	y.FactorSetId,
	a.Refnum,
	a.CalDateKey,
	a.StreamId,
	a.StreamDescription,

	/* J110 */	
	a.Density_SG,
	
	/* J111 */
	d.C5C6_Fraction,

	/* J115 */	
	CASE WHEN a.Density_SG = 0.0
	THEN 0.648
	ELSE CASE WHEN a.Density_SG < 0.648 THEN a.Density_SG ELSE 0.648 END
	END												[C5C6FractionDensity_SG],
	
	/* J116 */
	CASE WHEN d.C5C6_Fraction = 1.0
	THEN
		CASE WHEN a.Density_SG = 0.0
		THEN 0.648
		ELSE CASE WHEN a.Density_SG < 0.648 THEN a.Density_SG ELSE 0.648 END
		END
	ELSE
		(a.Density_SG - d.C5C6_Fraction * CASE WHEN a.Density_SG < 0.648 THEN a.Density_SG ELSE 0.648 END) / (1.0 - d.C5C6_Fraction)
	END												[HeavyFractionDensity_SG],
				
	/* J117 */
	CASE WHEN a.Density_SG = 0.0
	THEN 1.0
	ELSE 
		CASE WHEN a.Density_SG = 0.0
		THEN 0.648
		ELSE CASE WHEN a.Density_SG < 0.648 THEN a.Density_SG ELSE 0.648 END
		END * d.C5C6_Fraction / a.Density_SG
	END												[C5C6Weight_Fraction],
				
	/* J118 */
	1.0 - CASE WHEN a.Density_SG = 0.0
	THEN 1.0
	ELSE 
		CASE WHEN a.Density_SG = 0.0
		THEN 0.648
		ELSE CASE WHEN a.Density_SG < 0.648 THEN a.Density_SG ELSE 0.648 END
		END * d.C5C6_Fraction / a.Density_SG
	END												[HeavyWeight_Fraction],

	/* J119 */
	q.Tot_kMT										[Quantity_kMT],

	/* J120 */
	CONVERT(BIT, CASE WHEN a.StreamId = 'OtherFeedLiq'
		THEN
			CASE WHEN a.Density_SG < 0.75
			THEN 1
			ELSE 0
			END
		ELSE
			CASE WHEN b.DescendantId IS NOT NULL
			THEN 1
			ELSE 0
			END
		END)										[LightFeed_Bit]
FROM fact.FeedStockCrackingParameters				a
INNER JOIN fact.FeedStockDistillationPivot			d
	ON	d.Refnum = a.Refnum
	AND	d.CalDateKey = a.CalDateKey
	AND	d.StreamId = a.StreamId
	AND	d.StreamDescription = a.StreamDescription
INNER JOIN fact.QuantityPivot						q
	ON	q.Refnum = a.Refnum
	AND	q.CalDateKey = a.CalDateKey
	AND	q.StreamId = a.StreamId
	AND	q.StreamDescription = a.StreamDescription
INNER JOIN fact.TSortClient							t
	ON	t.Refnum = a.Refnum
INNER JOIN ante.FactorSetsInStudyYear				y
	ON	 y._StudyYear = t._DataYear
LEFT OUTER JOIN dim.Stream_Bridge					b
	ON	b.FactorSetId = y.FactorSetId
	AND	b.DescendantId = a.StreamId
	AND	b.StreamId = 'LiqLight';
