﻿CREATE VIEW fact.MetaQuantityAggregate
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.Refnum,
	p.StreamId,
	p.Q1,
	p.Q2,
	p.Q3,
	p.Q4,
	p.Tot_Quantity
FROM (
	SELECT
		b.FactorSetId,
		q.Refnum,
		COALESCE('Q' + CONVERT(CHAR(1), c.CalQtr), 'Tot_Quantity')	[Qtr],
		b.StreamId,
		SUM(CASE b.DescendantOperator
			WHEN '+' THEN + q.Quantity_kMT
			WHEN '-' THEN - q.Quantity_kMT
			END)			[Quantity_kMT]
	FROM dim.Stream_Bridge			b
	INNER JOIN fact.MetaQuantity	q
		ON	q.StreamId = b.DescendantId
	INNER JOIN dim.Calendar_LookUp	c
		ON	c.CalDateKey = q.CalDateKey
	GROUP BY
		b.FactorSetId,
		q.Refnum,
		ROLLUP(c.CalQtr),
		b.StreamId
	) u
	PIVOT (MAX([Quantity_kMT]) FOR [Qtr] IN (
		[Q1], [Q2], [Q3], [Q4], [Tot_Quantity]
		)
	)p;