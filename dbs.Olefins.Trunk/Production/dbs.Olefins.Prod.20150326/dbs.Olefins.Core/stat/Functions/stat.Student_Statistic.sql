﻿CREATE FUNCTION [stat].[Student_Statistic]
(@DegreesOfFreedom FLOAT (53), @Probability FLOAT (53), @Sides TINYINT)
RETURNS FLOAT (53)
AS
 EXTERNAL NAME [Sa.Meta.Numerics].[UserDefinedFunctions].[Student_Statistic]

