﻿Create View dbo.FeedSelections AS
Select t.Refnum,	FeedProdID,	Mix,	Blend,	Fractionation,	Hydrotreat,	Purification,	
MinSGEst,	MaxSGEst,	MinSGAct,	MaxSGAct,	
PipelinePct,	TruckPct,	BargePct,	ShipPct
from stgFact.FeedSelections s JOIN dbo.TSort t on t.SmallRefnum=s.Refnum
