﻿CREATE PROCEDURE [dbo].[Insert_MetaYield_CurPerMT]
(
	@Refnum			VARCHAR(25),
	@FactorSetId	VARCHAR(12)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE	@CurrencyId		VARCHAR(5) = 'USD';
		DECLARE	@DataType		VARCHAR(8) = '$PerMT';
		DECLARE	@ScenarioId		VARCHAR(5) = 'Basis';

		INSERT INTO [dbo].[MetaYield]
		(
			[Refnum],
			[DataType],
			[Ethane],
			[EPMix],
			[Propane],
			[LPG],
			[Butane],
			[FeedLtOther],
			[NaphthaLt],
			[NaphthaFr],
			[NaphthaHv],
			[Condensate],
			[HeavyNGL],
			[Raffinate],
			[Diesel],
			[GasOilHv],
			[GasOilHt],
			[FeedLiqOther],
			[FreshPyroFeed],
			[ConcEthylene],
			[ConcPropylene],
			[ConcButadiene],
			[ConcBenzene],
			[ROGHydrogen],
			[ROGMethane],
			[ROGEthane],
			[ROGEthylene],
			[ROGPropylene],
			[ROGPropane],
			[ROGC4Plus],
			[ROGInerts],
			[DilHydrogen],
			[DilMethane],
			[DilEthane],
			[DilEthylene],
			[DilPropane],
			[DilPropylene],
			[DilButane],
			[DilButylene],
			[DilButadiene],
			[DilBenzene],
			[DilMogas],
			[SuppWashOil],
			[SuppGasOil],
			[SuppOther],
			[SuppTot],
			[PlantFeed],
			[Hydrogen],
			[Methane],
			[Acetylene],
			[EthylenePG],
			[EthyleneCG],
			[PropylenePG],
			[PropyleneCG],
			[PropyleneRG],
			[PropaneC3Resid],
			[Butadiene],
			[IsoButylene],
			[C4Oth],
			[Benzene],
			[PyroGasoline],
			[PyroGasOil],
			[PyroFuelOil],
			[AcidGas],
			[PPFC],
			[ProdOther],
			[Prod],
			[LossFlareVent],
			[LossOther],
			[LossMeasure],
			[Loss],
			[ProdLoss],
			[FeedProdLoss],
			[FeedLiqOther1],
			[FeedLiqOther2],
			[FeedLiqOther3],
			[ProdOther1],
			[ProdOther2]
		)
		SELECT
			[ppsma].[Refnum],
			@DataType,

			[Ethane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Ethane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[EPMix]				= SUM(CASE WHEN [ppsma].[StreamId] = 'EPMix' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Propane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Propane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[LPG]				= SUM(CASE WHEN [ppsma].[StreamId] = 'LPG' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Butane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Butane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[FeedLtOther]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLtOther' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[NaphthaLt]			= SUM(CASE WHEN [ppsma].[StreamId] = 'NaphthaLt' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[NaphthaFr]			= SUM(CASE WHEN [ppsma].[StreamId] = 'NaphthaFr' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[NaphthaHv]			= SUM(CASE WHEN [ppsma].[StreamId] = 'NaphthaHv' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Condensate]		= SUM(CASE WHEN [ppsma].[StreamId] = 'Condensate' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[HeavyNGL]			= SUM(CASE WHEN [ppsma].[StreamId] = 'HeavyNGL' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Raffinate]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Raffinate' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Diesel]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Diesel' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[GasOilHv]			= SUM(CASE WHEN [ppsma].[StreamId] = 'GasOilHv' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[GasOilHt]			= SUM(CASE WHEN [ppsma].[StreamId] = 'GasOilHt' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[FeedLiqOther]		= [$(DbGlobal)].[dbo].[WtAvg]([ppsma].[Avg_Calc_Amount_Cur], CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' AND [ppsma].[Quantity_kMT] <> 0 THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[FreshPyroFeed]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FreshPyroFeed' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ConcEthylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcEthylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ConcPropylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcPropylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ConcButadiene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcButadiene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ConcBenzene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ConcBenzene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGHydrogen]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGHydrogen' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGMethane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGMethane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGEthane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGEthane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGEthylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGEthylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGPropylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGPropylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGPropane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGPropane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGC4Plus]			= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGC4Plus' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ROGInerts]			= SUM(CASE WHEN [ppsma].[StreamId] = 'ROGInerts' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilHydrogen]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilHydrogen' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilMethane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilMethane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilEthane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'DilEthane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilEthylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilEthylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilPropane]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilPropane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilPropylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilPropylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilButane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'DilButane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilButylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilButylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilButadiene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilButadiene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilBenzene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'DilBenzene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[DilMogas]			= SUM(CASE WHEN [ppsma].[StreamId] = 'DilMogas' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[SuppWashOil]		= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppWashOil' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[SuppGasOil]		= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppGasOil' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[SuppOther]			= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppOther' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[SuppTot]			= SUM(CASE WHEN [ppsma].[StreamId] = 'SuppTot' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PlantFeed]			= SUM(CASE WHEN [ppsma].[StreamId] = 'PlantFeed' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),

			[Hydrogen]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Hydrogen' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Methane]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Methane' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Acetylene]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Acetylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[EthylenePG]		= SUM(CASE WHEN [ppsma].[StreamId] = 'EthylenePG' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[EthyleneCG]		= SUM(CASE WHEN [ppsma].[StreamId] = 'EthyleneCG' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PropylenePG]		= SUM(CASE WHEN [ppsma].[StreamId] = 'PropylenePG' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PropyleneCG]		= SUM(CASE WHEN [ppsma].[StreamId] = 'PropyleneCG' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PropyleneRG]		= SUM(CASE WHEN [ppsma].[StreamId] = 'PropyleneRG' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PropaneC3Resid]	= SUM(CASE WHEN [ppsma].[StreamId] = 'PropaneC3Resid' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Butadiene]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Butadiene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[IsoButylene]		= SUM(CASE WHEN [ppsma].[StreamId] = 'IsoButylene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[C4Oth]				= SUM(CASE WHEN [ppsma].[StreamId] = 'C4Oth' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Benzene]			= SUM(CASE WHEN [ppsma].[StreamId] = 'Benzene' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PyroGasoline]		= SUM(CASE WHEN [ppsma].[StreamId] = 'PyroGasoline' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PyroGasOil]		= SUM(CASE WHEN [ppsma].[StreamId] = 'PyroGasOil' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PyroFuelOil]		= SUM(CASE WHEN [ppsma].[StreamId] = 'PyroFuelOil' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[AcidGas]			= SUM(CASE WHEN [ppsma].[StreamId] = 'AcidGas' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[PPFC]				= SUM(CASE WHEN [ppsma].[StreamId] = 'PPFC' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ProdOther]			= [$(DbGlobal)].[dbo].[WtAvg]([ppsma].[Avg_Calc_Amount_Cur], CASE WHEN [ppsma].[StreamId] = 'ProdOther' AND [ppsma].[Quantity_kMT] <> 0.0 THEN [ppsma].[Quantity_kMT] ELSE 0.0 END),
			[Prod]				= SUM(CASE WHEN [ppsma].[StreamId] = 'Prod' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[LossFlareVent]		= SUM(CASE WHEN [ppsma].[StreamId] = 'LossFlareVent' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[LossOther]			= SUM(CASE WHEN [ppsma].[StreamId] = 'LossOther' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[LossMeasure]		= SUM(CASE WHEN [ppsma].[StreamId] = 'LossMeasure' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[Loss]				= SUM(CASE WHEN [ppsma].[StreamId] = 'Loss' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ProdLoss]			= SUM(CASE WHEN [ppsma].[StreamId] = 'ProdLoss' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[FeedProdLoss]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FeedProdLoss' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[FeedLiqOther1]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' AND [ppsma].[StreamDescription] LIKE '%(Other Feed 1)%' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[FeedLiqOther2]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' AND [ppsma].[StreamDescription] LIKE '%(Other Feed 2)%' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[FeedLiqOther3]		= SUM(CASE WHEN [ppsma].[StreamId] = 'FeedLiqOther' AND [ppsma].[StreamDescription] LIKE '%(Other Feed 3)%' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ProdOther1]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ProdOther' AND [ppsma].[StreamDescription] LIKE '%(Other Prod 1)%' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END),
			[ProdOther2]		= SUM(CASE WHEN [ppsma].[StreamId] = 'ProdOther' AND [ppsma].[StreamDescription] LIKE '%(Other Prod 2)%' THEN [ppsma].[Avg_Calc_Amount_Cur] ELSE 0.0 END)
		FROM
			[calc].[PricingPlantStreamMetaAggregate] [ppsma]
		WHERE	[ppsma].[FactorSetId]	= @FactorSetId
			AND	[ppsma].[Refnum]		= @Refnum
			AND	[ppsma].[ScenarioId]	= @ScenarioId
			AND	[ppsma].[CurrencyRpt]	= @CurrencyId
		GROUP BY
			[ppsma].[Refnum];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;