﻿CREATE TABLE [dbo].[Queue] (
    [Refnum]      VARCHAR (25)  NOT NULL,
    [Program]     VARCHAR (20)  NOT NULL,
    [Consultant]  VARCHAR (5)   NOT NULL,
    [FilePath]    VARCHAR (255) NULL,
    [Options]     VARCHAR (80)  NULL,
    [Priority]    SMALLINT      NULL,
    [LastAttempt] SMALLDATETIME NULL,
    [NumErrors]   INT           NULL,
    [tsQueued]    SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Queue] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Program] ASC) WITH (FILLFACTOR = 70)
);

