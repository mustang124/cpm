﻿CREATE TABLE [dbo].[Environment] (
    [Refnum]                VARCHAR (25)  NOT NULL,
    [Emissions_MT]          REAL          NULL,
    [Emissions_PcntInput]   REAL          NULL,
    [Emissions_MTPerEDC]    REAL          NULL,
    [WasteHazMat_MT]        REAL          NULL,
    [WasteHazMat_PcntInput] REAL          NULL,
    [WasteHazMat_MTPerEDC]  REAL          NULL,
    [NOx_LbPerMBTU]         REAL          NULL,
    [NOX_MTperGJ]           REAL          NULL,
    [TotPyroFuel_MBTU]      REAL          NULL,
    [WasteWater_MTd]        REAL          NULL,
    [WasteWater_PerInput]   REAL          NULL,
    [WasteWater_MTPerEDC]   REAL          NULL,
    [EstDirectCO2E_kMT]     REAL          NULL,
    [EstTotCO2E_kMT]        REAL          NULL,
    [CeiDirect]             REAL          NULL,
    [CeiTotal]              REAL          NULL,
    [StdDirectCO2E_kMT]     REAL          NULL,
    [StdTotCO2E_kMT]        REAL          NULL,
    [tsModified]            SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Environment] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 70)
);

