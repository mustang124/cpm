﻿CREATE TABLE [inter].[PricingStreamLocation] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [PricingMethodId]       VARCHAR (42)       NOT NULL,
    [LocationID]            VARCHAR (42)       NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [Pricing_Cur]           REAL               NOT NULL,
    [MatlBal_Cur]           REAL               NOT NULL,
    [Report_Cur]            REAL               NOT NULL,
    [Supersede_Pricing_Cur] REAL               NULL,
    [Supersede_MatlBal_Cur] REAL               NULL,
    [Supersede_Report_Cur]  REAL               NULL,
    [_Pricing_Amount_Cur]   AS                 (CONVERT([real],isnull([Supersede_Pricing_Cur],[Pricing_Cur]),(1))) PERSISTED NOT NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],isnull([Supersede_MatlBal_Cur],[MatlBal_Cur]),(1))) PERSISTED NOT NULL,
    [_Report_Amount_Cur]    AS                 (CONVERT([real],isnull([Supersede_Report_Cur],[Report_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingStreamLocation_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingStreamLocation_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingStreamLocation_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingStreamLocation_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingStreamLocation] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PricingMethodId] ASC, [CurrencyRpt] ASC, [LocationID] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingStreamLocation_MatlBal_Cur] CHECK ([MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamLocation_Pricing_Cur] CHECK ([Pricing_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamLocation_Report_Cur] CHECK ([Report_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamLocation_Supersede_MatlBal_Cur] CHECK ([Supersede_MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamLocation_Supersede_Pricing_Cur] CHECK ([Supersede_Pricing_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamLocation_Supersede_Report_Cur] CHECK ([Supersede_Report_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingStreamLocation_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingStreamLocation_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingStreamLocation_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingStreamLocation_PricingLocations] FOREIGN KEY ([LocationID]) REFERENCES [dim].[PricingLocationsLu] ([LocationID]),
    CONSTRAINT [FK_PricingStreamLocation_PricingMethod] FOREIGN KEY ([PricingMethodId]) REFERENCES [dim].[PricingMethodLu] ([MethodId]),
    CONSTRAINT [FK_PricingStreamLocation_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [inter].t_PricingStreamLocation_u
	ON [inter].PricingStreamLocation
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [inter].PricingStreamLocation
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].PricingStreamLocation.FactorSetId		= INSERTED.FactorSetId
		AND [inter].PricingStreamLocation.LocationID		= INSERTED.LocationID
		AND [inter].PricingStreamLocation.CalDateKey		= INSERTED.CalDateKey
		AND [inter].PricingStreamLocation.StreamId			= INSERTED.StreamId
		AND [inter].PricingStreamLocation.CurrencyRpt		= INSERTED.CurrencyRpt;

END;