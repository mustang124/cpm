﻿CREATE TABLE [inter].[PricingPrimaryCompositionRegion] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [RegionId]              VARCHAR (5)        NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [ComponentId]           VARCHAR (42)       NOT NULL,
    [Raw_Amount_Cur]        REAL               NOT NULL,
    [MatlBal_Supersede_Cur] REAL               NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],isnull([MatlBal_Supersede_Cur],[Raw_Amount_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPrimaryCompositionRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryCompositionRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryCompositionRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingPrimaryCompositionRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingPrimaryCompositionRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingPrimaryCompositionRegion_MatlBal_Supersede_Cur] CHECK ([MatlBal_Supersede_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingPrimaryCompositionRegion_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingPrimaryCompositionRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPrimaryCompositionRegion_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingPrimaryCompositionRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingPrimaryCompositionRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingPrimaryCompositionRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingPrimaryCompositionRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingPrimaryCompositionRegion_u
	ON inter.PricingPrimaryCompositionRegion
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingPrimaryCompositionRegion
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingPrimaryCompositionRegion.FactorSetId		= INSERTED.FactorSetId
		AND inter.PricingPrimaryCompositionRegion.RegionId			= INSERTED.RegionId
		AND inter.PricingPrimaryCompositionRegion.CalDateKey		= INSERTED.CalDateKey
		AND inter.PricingPrimaryCompositionRegion.CurrencyRpt		= INSERTED.CurrencyRpt
		AND inter.PricingPrimaryCompositionRegion.StreamId			= INSERTED.StreamId
		AND inter.PricingPrimaryCompositionRegion.ComponentId		= INSERTED.ComponentId;

END;