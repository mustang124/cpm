﻿CREATE TABLE [inter].[PricingStreamRegion] (
    [FactorSetId]           VARCHAR (12)       NOT NULL,
    [PricingMethodId]       VARCHAR (42)       NOT NULL,
    [RegionId]              VARCHAR (5)        NOT NULL,
    [CalDateKey]            INT                NOT NULL,
    [CurrencyRpt]           VARCHAR (4)        NOT NULL,
    [StreamId]              VARCHAR (42)       NOT NULL,
    [Pricing_Cur]           REAL               NOT NULL,
    [MatlBal_Cur]           REAL               NOT NULL,
    [Report_Cur]            REAL               NOT NULL,
    [Supersede_Pricing_Cur] REAL               NULL,
    [Supersede_MatlBal_Cur] REAL               NULL,
    [Supersede_Report_Cur]  REAL               NULL,
    [_Pricing_Amount_Cur]   AS                 (CONVERT([real],isnull([Supersede_Pricing_Cur],[Pricing_Cur]),(1))) PERSISTED NOT NULL,
    [_MatlBal_Amount_Cur]   AS                 (CONVERT([real],isnull([Supersede_MatlBal_Cur],[MatlBal_Cur]),(1))) PERSISTED NOT NULL,
    [_Report_Amount_Cur]    AS                 (CONVERT([real],isnull([Supersede_Report_Cur],[Report_Cur]),(1))) PERSISTED NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_PricingStreamRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_PricingStreamRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_PricingStreamRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_PricingStreamRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingStreamRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PricingMethodId] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingStreamRegion_MatlBal_Cur] CHECK ([MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamRegion_Pricing_Cur] CHECK ([Pricing_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamRegion_Report_Cur] CHECK ([Report_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamRegion_Supersede_MatlBal_Cur] CHECK ([Supersede_MatlBal_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamRegion_Supersede_Pricing_Cur] CHECK ([Supersede_Pricing_Cur]>=(0.0)),
    CONSTRAINT [CR_PricingStreamRegion_Supersede_Report_Cur] CHECK ([Supersede_Report_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingStreamRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingStreamRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingStreamRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingStreamRegion_PricingMethod] FOREIGN KEY ([PricingMethodId]) REFERENCES [dim].[PricingMethodLu] ([MethodId]),
    CONSTRAINT [FK_PricingStreamRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingStreamRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [inter].t_PricingStreamRegion_u
	ON [inter].[PricingStreamRegion]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [inter].[PricingStreamRegion]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].[PricingStreamRegion].FactorSetId		= INSERTED.FactorSetId
		AND [inter].[PricingStreamRegion].RegionId			= INSERTED.RegionId
		AND [inter].[PricingStreamRegion].CalDateKey			= INSERTED.CalDateKey
		AND [inter].[PricingStreamRegion].StreamId			= INSERTED.StreamId
		AND [inter].[PricingStreamRegion].CurrencyRpt		= INSERTED.CurrencyRpt;

END;