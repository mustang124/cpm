﻿CREATE TABLE [inter].[PricingPPFCFuelGas] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [ScenarioId]             VARCHAR (42)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [CountryId]              CHAR (3)           NOT NULL,
    [AnnDateKey]             INT                NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [CurrencyRpt]            VARCHAR (4)        NOT NULL,
    [AccountId]              VARCHAR (42)       NOT NULL,
    [Quantity_kMT]           REAL               NOT NULL,
    [LHValue_MBTU]           REAL               NOT NULL,
    [Composition_Amount_Cur] REAL               NOT NULL,
    [_MatlBal_Amount_Cur]    AS                 (CONVERT([real],[Quantity_kMT]*[Composition_Amount_Cur],(1))) PERSISTED NOT NULL,
    [Supersede_Cur]          REAL               NULL,
    [_Supersede_Amount_Cur]  AS                 (CONVERT([real],[Quantity_kMT]*[Supersede_Cur],(1))) PERSISTED,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_PricingPPFCFuelGas_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_PricingPPFCFuelGas_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_PricingPPFCFuelGas_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_PricingPPFCFuelGas_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingPPFCFuelGas] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ScenarioId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [AccountId] ASC, [CalDateKey] ASC),
    CHECK ([_MatlBal_Amount_Cur]>=(0.0)),
    CHECK ([_Supersede_Amount_Cur]>=(0.0)),
    CHECK ([Composition_Amount_Cur]>=(0.0)),
    CHECK ([LHValue_MBTU]>=(0.0)),
    CHECK ([Quantity_kMT]>=(0.0)),
    CHECK ([ScenarioId]<>''),
    CHECK ([Supersede_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingPPFCFuelGas_Account_LookUp] FOREIGN KEY ([AccountId]) REFERENCES [dim].[Account_LookUp] ([AccountId]),
    CONSTRAINT [FK_PricingPPFCFuelGas_AnnCalendar_LookUp] FOREIGN KEY ([AnnDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPPFCFuelGas_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingPPFCFuelGas_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId]),
    CONSTRAINT [FK_PricingPPFCFuelGas_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingPPFCFuelGas_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingPPFCFuelGas_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER [inter].[t_PricingPPFCFuelGas_u]
	ON  [inter].[PricingPPFCFuelGas]
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [inter].[PricingPPFCFuelGas]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[inter].[PricingPPFCFuelGas].[FactorSetId]	= INSERTED.[FactorSetId]
		AND [inter].[PricingPPFCFuelGas].[ScenarioId]	= INSERTED.[ScenarioId]
		AND [inter].[PricingPPFCFuelGas].[Refnum]		= INSERTED.[Refnum]
		AND [inter].[PricingPPFCFuelGas].[CurrencyRpt]	= INSERTED.[CurrencyRpt]
		AND [inter].[PricingPPFCFuelGas].[AccountId]	= INSERTED.[AccountId]
		AND [inter].[PricingPPFCFuelGas].[CalDateKey]	= INSERTED.[CalDateKey];
				
END;