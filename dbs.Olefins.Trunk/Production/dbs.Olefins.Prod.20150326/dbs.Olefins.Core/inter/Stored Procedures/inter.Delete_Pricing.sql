﻿CREATE PROCEDURE [inter].[Delete_Pricing]
(
	@FactorSetId			VARCHAR(12) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingBasisCompositionRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingBasisCompositionRegion		WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingBasisStreamRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingBasisStreamRegion			WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingCompositionCountry';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingCompositionCountry			WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingCompositionLocation';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingCompositionLocation		WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingCompositionRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingCompositionRegion			WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingPrimaryCompositionCountry';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingPrimaryCompositionCountry	WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingPrimaryCompositionRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingPrimaryCompositionRegion	WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingPrimaryStreamCountry';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingPrimaryStreamCountry		WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingPrimaryStreamRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingPrimaryStreamRegion		WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingSecondaryStreamCountry';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingSecondaryStreamCountry		WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingSecondaryStreamRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingSecondaryStreamRegion		WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingStagingCompositionRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingStagingCompositionRegion	WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingStagingStreamRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingStagingStreamRegion		WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingStreamCountry';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingStreamCountry				WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingStreamLocation';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingStreamLocation				WHERE FactorSetId = @FactorSetId;

	SET @ProcedureDesc = NCHAR(9) + N'DELETE FROM inter.PricingStreamRegion';
	PRINT @ProcedureDesc;

	DELETE FROM inter.PricingStreamRegion				WHERE FactorSetId = @FactorSetId;

END;