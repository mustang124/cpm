﻿CREATE PROCEDURE [calc].[Insert_Eei]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.Eei(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId,
			Simulation_MBTU, Separation_MBTU,
			ComponentId, Production_kMT, Simulation_kMT, Supplemental_kMT,
			Actual_MBTU_kMT)
		SELECT
			e.FactorSetId,
			e.Refnum,
			e.CalDateKey,
			e.SimModelId,
			e.OpCondId,
			e.RecycleId,
	
			e.Energy_kCalC2H4 * y.Component_WtPcnt * f.FeedPyrolysis_kMT * 2.2046	/ 100000.0		[ModelEnergy_MBTU],
			esa.EnergySeparation_MBTU												/ 1000000.0		[SeparationEnergy_MBTU],

			yComp.ComponentId,
			p.Production_kMT,
			yComp.Component_Extrap_kMT,
			yComp.Component_Supp_kMT,

			l.LHValue_MBTU / p.Production_kMT / 2.2046												[ActualEnergy_MBTU_kMT]
 
		FROM @fpl												tsq
		INNER JOIN sim.EnergyConsumptionPlant					e
			ON	e.FactorSetId			= tsq.FactorSetId
			AND	e.Refnum				= tsq.Refnum
			AND	e.CalDateKey			= tsq.Plant_QtrDateKey
		INNER JOIN sim.YieldCompositionPlant					y
			ON	y.FactorSetId			= tsq.FactorSetId
			AND	y.Refnum				= tsq.Refnum
			AND y.CalDateKey			= tsq.Plant_QtrDateKey
			AND	y.SimModelId			= e.SimModelId
			AND y.OpCondId				= e.OpCondId
			AND y.RecycleId				= e.RecycleId
			AND y.ComponentId			= 'C2H4'
		INNER JOIN calc.DivisorsFeed							f
			ON	f.FactorSetId			= tsq.FactorSetId
			AND	f.Refnum				= tsq.Refnum
			AND	f.CalDateKey			= tsq.Plant_QtrDateKey
			AND	f.StreamId				= 'PlantFeed'
		LEFT OUTER JOIN [calc].[EnergySeparationAggregate]		esa WITH (NOEXPAND)
			ON	esa.FactorSetId			= tsq.FactorSetId
			AND	esa.Refnum				= tsq.Refnum
			AND	esa.CalDateKey			= tsq.Plant_QtrDateKey
			AND	esa.StandardEnergyId	= 'StandardEnergyTot'
		INNER JOIN  calc.CompositionYieldRatio					yComp
			ON	yComp.FactorSetId		= tsq.FactorSetId
			AND	yComp.Refnum			= tsq.Refnum
			AND	yComp.CalDateKey		= tsq.Plant_QtrDateKey
			AND	yComp.SimModelId		= e.SimModelId
			AND	yComp.OpCondId			= e.OpCondId
			AND	yComp.RecycleId			= e.RecycleId
			AND	yComp.SimModelId		<> 'Plant'
			AND	yComp.ComponentId		IN ('C2H4', 'ProdOlefins', 'ProdHVC')
		INNER JOIN fact.EnergyLHValueAggregate					l
			ON	l.FactorSetId			= tsq.FactorSetId
			AND	l.Refnum				= tsq.Refnum
			AND	l.CalDateKey			= tsq.Plant_QtrDateKey
			AND	l.AccountId				= 'TotRefExp'
		INNER JOIN calc.DivisorsProduction						p
			ON	p.FactorSetId			= e.FactorSetId
			AND	p.Refnum				= e.Refnum
			AND	p.CalDateKey			= e.CalDateKey
			AND	p.ComponentId			= yComp.ComponentId;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;