﻿CREATE PROCEDURE [calc].[Insert_ReliabilityOpLossAvailability_Other]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[ReliabilityOpLossAvailability]([FactorSetId], [Refnum], [CalDateKey], [OppLossId], [DownTimeLoss_MT], [SlowDownLoss_MT], [TotLoss_MT])
		SELECT
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			l.[OppLossId],

			(SUM(l.[DownTimeLoss_MT]) - SUM(l.[DownTimeLoss_MT] * a.[DownTimeLoss_Pcnt]) / 100.0)
				/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END		[DownTimeLoss_MT],
			(SUM(l.[SlowDownLoss_MT]) - SUM(l.[SlowDownLoss_MT] * a.[SlowDownLoss_Pcnt]) / 100.0)
				/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END		[SlowDownLoss_MT],

			(SUM(COALESCE(l.[DownTimeLoss_MT], 0.0) + COALESCE(l.[SlowDownLoss_MT], 0.0)) - SUM(COALESCE(l.[DownTimeLoss_MT] * a.[DownTimeLoss_Pcnt], 0.0) + COALESCE(l.[SlowDownLoss_MT] * a.[SlowDownLoss_Pcnt], 0.0)) / 100.0) 
				/ CASE WHEN p.[TotLoss_MT] > 0.0 THEN 2.0 ELSE 1.0 END		[TotLoss_MT]

		FROM	@fpl					fpl
		INNER JOIN	[fact].[ReliabilityOppLoss]				l
			ON	l.[Refnum]			= fpl.[Refnum]
		INNER JOIN [fact].[ReliabilityOppLoss_Availability] a
			ON	a.[Refnum]			= fpl.[Refnum]
			AND	a.[CalDateKey]		= l.[CalDateKey]
			AND	a.[OppLossId]		= l.[OppLossId]
			AND	a.[OppLossId]		IN ('Demand', 'ExtFeedInterrupt', 'CapProject')
		LEFT OUTER JOIN [fact].[ReliabilityOppLossAggregate]		p
			ON	p.[FactorSetId]		= fpl.[FactorSetId]
			AND	p.[Refnum]			= fpl.[Refnum]
			AND	p.[CalDateKey]		= fpl.[Plant_QtrDateKey] - 10000
			AND	p.[TotLoss_MT]		> 0.0
			AND p.[OppLossId]		= 'LostProdOpp'
		WHERE fpl.CalQtr		= 4
		GROUP BY
			fpl.[FactorSetId],
			fpl.[Refnum],
			fpl.[Plant_QtrDateKey],
			p.[TotLoss_MT],
			l.[OppLossId];

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;