﻿CREATE PROCEDURE [calc].[Insert_PricingPlantQuarters]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.PricingPlantStreamQuarters(FactorSetId, ScenarioId, Refnum, CalDateKey, CurrencyRpt, StreamId, StreamDescription, Q1, Q2, Q3, Q4, Total)
		SELECT
			p.FactorSetId,
			p.ScenarioId,
			p.Refnum,
			p.Plant_AnnDateKey,
			p.CurrencyRpt,
			p.StreamId,
			p.StreamDescription,
			COALESCE(p.Q1, 0.0),
			COALESCE(p.Q2, 0.0),
			COALESCE(p.Q3, 0.0),
			COALESCE(p.Q4, 0.0),
			COALESCE(p.Q1, 0.0) + COALESCE(p.Q2, 0.0) + COALESCE(p.Q3, 0.0) + COALESCE(p.Q4, 0.0) [Total]
		FROM(
			SELECT
				fpl.FactorSetId,
				s.ScenarioId,
				fpl.Refnum,
				fpl.Plant_AnnDateKey,
				fpl.CurrencyRpt,
				'Q' + CONVERT(CHAR(1), c.CalQtr)		[ValueQtr],
				d.StreamId,
				CASE WHEN d.StreamId IN ('FeedLiqOther', 'FeedLtOther', 'ProdOther') THEN p.StreamDescription END [StreamDescription],
				(CASE d.DescendantOperator
					WHEN '+' THEN + p._Stream_Value_Cur
					WHEN '-' THEN - p._Stream_Value_Cur
					END)							[Stream_Value_Cur]
			FROM @fpl									fpl
			INNER JOIN dim.Calendar_LookUp				c
				ON	c.CalDateKey = fpl.Plant_QtrDateKey
			INNER JOIN ante.PricingScenarioConfig		s
				ON	s.FactorSetId = fpl.FactorSetId
			INNER JOIN dim.Stream_Bridge				d
				ON	d.FactorSetId = fpl.FactorSetId
			LEFT OUTER JOIN calc.PricingPlantStream		p
				ON	p.FactorSetId = fpl.FactorSetId
				AND	p.Refnum = fpl.Refnum
				AND	p.CalDateKey = fpl.Plant_QtrDateKey
				AND	p.CurrencyRpt = fpl.CurrencyRpt
				AND	p.ScenarioId = s.ScenarioId
				AND	p.StreamId = d.DescendantId
			WHERE p._Stream_Value_Cur IS NOT NULL
			) u
			PIVOT (
				SUM(Stream_Value_Cur) FOR ValueQtr IN (
					Q1, Q2, Q3, Q4
					)
			) p
		WHERE 	Q1 IS NOT NULL
			OR	Q2 IS NOT NULL
			OR	Q3 IS NOT NULL
			OR	Q4 IS NOT NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END