﻿CREATE VIEW calc.MaterialBalance
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.ScenarioId,
	p.Refnum,
	MAX(p.CalDateKey)		[CalDateKey],
	p.CurrencyRpt,
	b.StreamId,
	CASE WHEN b.StreamId IN ('ProdOther', 'FeedLiqOther', 'FeedLtOther') THEN p.StreamDescription END	[StreamDescription],
			
	AVG(CASE b.DescendantOperator
		WHEN '+' THEN + p.Quantity_kMT
		WHEN '-' THEN - p.Quantity_kMT
		END)										[Avg_Quantity_kMT],
			
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p.Quantity_kMT
		WHEN '-' THEN - p.Quantity_kMT
		END)										[Tot_Quantity_kMT],
			
	AVG(CASE b.DescendantOperator
		WHEN '+' THEN + p._Calc_Amount_Cur
		WHEN '-' THEN - p._Calc_Amount_Cur
		END)										[Avg_Amount_Cur],
			
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p._Stream_Value_Cur
		WHEN '-' THEN - p._Stream_Value_Cur
		END)										[Tot_Stream_Value_Cur],
				
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p._Stream_Value_Cur
		WHEN '-' THEN - p._Stream_Value_Cur
		END) / 
	SUM(CASE b.DescendantOperator
		WHEN '+' THEN + p.Quantity_kMT
		WHEN '-' THEN - p.Quantity_kMT
		END)										[WtAvg_Stream_Value_Cur]
FROM calc.[PricingPlantStream]	p
--	Task 141: Implement Tri-Tables in calculations
--INNER JOIN	dim.StreamLuDescendants(NULL, 1, '')	d
--	ON	d.DescendantId = p.StreamId
INNER JOIN dim.Stream_Bridge	b
	ON	b.FactorSetId = p.FactorSetId
	AND	b.DescendantId = p.StreamId
GROUP BY
	p.FactorSetId,
	p.ScenarioId,
	p.Refnum,
	p.CurrencyRpt,
	b.StreamId,
	CASE WHEN b.StreamId IN ('ProdOther', 'FeedLiqOther', 'FeedLtOther') THEN p.StreamDescription END
HAVING SUM(p.Quantity_kMT) <> 0.0;