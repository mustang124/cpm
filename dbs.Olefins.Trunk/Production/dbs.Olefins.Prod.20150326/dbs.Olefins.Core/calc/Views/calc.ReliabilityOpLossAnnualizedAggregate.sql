﻿CREATE VIEW [calc].[ReliabilityOpLossAnnualizedAggregate]
WITH SCHEMABINDING
AS
SELECT
	a.[FactorSetId],
	a.[Refnum],
	a.[CalDateKey],
	b.[OppLossId],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(a.[DownTimeLoss_MT], 0.0)
		WHEN '-' THEN - ISNULL(a.[DownTimeLoss_MT], 0.0)
		ELSE 0.0
		END)								[DownTimeLoss_MT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(a.[SlowDownLoss_MT], 0.0)
		WHEN '-' THEN - ISNULL(a.[SlowDownLoss_MT], 0.0)
		ELSE 0.0
		END)								[SlowDownLoss_MT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(a.[TotLoss_MT], 0.0)
		WHEN '-' THEN - ISNULL(a.[TotLoss_MT], 0.0)
		ELSE 0.0
		END)								[TotLoss_MT],

	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(a.[DownTimeLoss_kMT], 0.0)
		WHEN '-' THEN - ISNULL(a.[DownTimeLoss_kMT], 0.0)
		ELSE 0.0
		END)								[DownTimeLoss_kMT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(a.[SlowDownLoss_kMT], 0.0)
		WHEN '-' THEN - ISNULL(a.[SlowDownLoss_kMT], 0.0)
		ELSE 0.0
		END)								[SlowDownLoss_kMT],
	SUM(
		CASE b.[DescendantOperator]
		WHEN '+' THEN + ISNULL(a.[TotLoss_kMT], 0.0)
		WHEN '-' THEN - ISNULL(a.[TotLoss_kMT], 0.0)
		ELSE 0.0
		END)								[TotLoss_kMT],
	COUNT_BIG(*)							[IndexItems]
FROM  [dim].[ReliabilityOppLoss_Bridge]				b
INNER JOIN [calc].[ReliabilityOpLossAnnualized]		a
	ON	a.[FactorSetId]	= b.[FactorSetId]
	AND	a.[OppLossId]	= b.[DescendantId]
GROUP BY
	a.[FactorSetId],
	a.[Refnum],
	a.[CalDateKey],
	b.[OppLossId];
GO
CREATE UNIQUE CLUSTERED INDEX [UX_ReliabilityOpLossAnnualizedAggregate]
    ON [calc].[ReliabilityOpLossAnnualizedAggregate]([FactorSetId] ASC, [Refnum] ASC, [OppLossId] ASC, [CalDateKey] ASC);

