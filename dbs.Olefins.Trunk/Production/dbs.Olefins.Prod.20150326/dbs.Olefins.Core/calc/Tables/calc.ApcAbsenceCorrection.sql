﻿CREATE TABLE [calc].[ApcAbsenceCorrection] (
    [FactorSetId]              VARCHAR (12)       NOT NULL,
    [Refnum]                   VARCHAR (25)       NOT NULL,
    [CalDateKey]               INT                NOT NULL,
    [TowerDebutanizer]         INT                NULL,
    [TowerDepropanizer]        INT                NULL,
    [TowerPropylene]           INT                NULL,
    [_AbsenceCorrectionFactor] AS                 (CONVERT([real],((1.0)-case when (isnull([TowerDebutanizer],(0.0))+isnull([TowerDepropanizer],(0.0)))>(0.0) then (0.0) else (0.01) end)-case when isnull([TowerPropylene],(0.0))>(0.0) then (0.0) else (0.12) end,(1))) PERSISTED NOT NULL,
    [tsModified]               DATETIMEOFFSET (7) CONSTRAINT [DF_ApcAbsenceCorrection_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]           NVARCHAR (168)     CONSTRAINT [DF_ApcAbsenceCorrection_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]           NVARCHAR (168)     CONSTRAINT [DF_ApcAbsenceCorrection_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]            NVARCHAR (168)     CONSTRAINT [DF_ApcAbsenceCorrection_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_ApcAbsenceCorrection] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_ApcAbsenceCorrection_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_ApcAbsenceCorrection_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_ApcAbsenceCorrection_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_ApcAbsenceCorrection_u
	ON  calc.ApcAbsenceCorrection
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ApcAbsenceCorrection
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ApcAbsenceCorrection.FactorSetId	= INSERTED.FactorSetId
		AND calc.ApcAbsenceCorrection.Refnum		= INSERTED.Refnum
		AND calc.ApcAbsenceCorrection.CalDateKey	= INSERTED.CalDateKey;
				
END;