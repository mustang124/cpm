﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.CSharp;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public class Simulation
	{
		public void UpLoad(string Refnum, string PathFile, string SimModelId)
		{
			string sRefnum = Common.GetRefnumSmall(Refnum);

			Delete(SimModelId, sRefnum);
			Insert(PathFile, SimModelId, sRefnum);
		}

		void Delete(string SimModelId, string Refnum)
		{
			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();

			System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("[stgFact].[Delete_SimulationData]", cn);
			
			cmd.CommandType = System.Data.CommandType.StoredProcedure;

			cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 12).SqlValue = Refnum;
			cmd.Parameters.Add("@SimModelId", SqlDbType.VarChar, 12).SqlValue = SimModelId;

			cmd.ExecuteNonQuery();
			
			cn.Close();
		}

		void Insert(string PathFile, string SimModelId, string Refnum)
		{
			Excel.Application xla = Common.NewExcelApplication();
			Excel.Workbook wkb = Common.OpenWorkbook_ReadOnly(xla, PathFile);
			Excel.Worksheet wks = (Excel.Worksheet)wkb.Worksheets[1];
		
			Insert_Streams(wks, SimModelId, Refnum);
			Insert_Plant(wks, SimModelId, Refnum);

			if (SimModelId == "PYPS")
			{
				Insert_SpecificPyps(wks, Refnum);
			}

			Common.CloseExcel(ref xla, ref wkb, ref wks);
		}

		const int colComponentId  = 1;
		const int rowEnergy = 28;
		const int rowComponentBeg = 8;
		const int rowComponentEnd = 26;

		const int colStreamBeg = 7;
		const int colStreamEnd = 69;
		const int rowStreamId = 7;

		void Insert_Streams(Excel.Worksheet wks, string SimModelId, string Refnum)
		{
			Excel.Range rng = null;

			string OpCondId = string.Empty;
			string StreamId = string.Empty;
			string ComponentId = string.Empty;

			double SimEnergy_kCalkg;
			double Component_WtPcnt;

			System.Data.SqlClient.SqlCommand cmd = null;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());
			cn.Open();

			for (int c = colStreamBeg; c <= colStreamEnd; c++)
			{
				try
				{
					rng = wks.Cells[rowEnergy, c];
					SimEnergy_kCalkg = (double)rng.Value2;
					
					if (SimEnergy_kCalkg > 0.0)
					{
						OpCondId = ConvertColumnToOpCondId(c);

						rng = wks.Cells[rowStreamId, c];
						StreamId = ConvertFeedProdId(rng);

						cmd = new System.Data.SqlClient.SqlCommand("[stgFact].[Insert_EnergyConsumptionStream]", cn);
						cmd.CommandType = System.Data.CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
						cmd.Parameters.Add("@SimModelId", SqlDbType.VarChar, 12).Value = SimModelId;
						cmd.Parameters.Add("@OpCondId", SqlDbType.VarChar, 12).Value = OpCondId;
						cmd.Parameters.Add("@StreamId", SqlDbType.VarChar, 42).Value = StreamId;
						cmd.Parameters.Add("@SimEnergy_kCalkg", SqlDbType.Real).Value = SimEnergy_kCalkg;

						cmd.ExecuteNonQuery();

						for (int r = rowComponentBeg; r <= rowComponentEnd; r++)
						{
							try
							{
								rng = wks.Cells[r, c];
								Component_WtPcnt = (double)rng.Value2;

								if (Component_WtPcnt > 0.0)
								{
									ComponentId = ConvertComponentId(wks.Cells[r, Simulation.colComponentId].Value2);

									cmd = new System.Data.SqlClient.SqlCommand("[stgFact].[Insert_YieldCompositionStream]", cn);
									cmd.CommandType = System.Data.CommandType.StoredProcedure;

									cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
									cmd.Parameters.Add("@SimModelId", SqlDbType.VarChar, 12).Value = SimModelId;
									cmd.Parameters.Add("@OpCondId", SqlDbType.VarChar, 12).Value = OpCondId;
									cmd.Parameters.Add("@StreamId", SqlDbType.VarChar, 42).Value = StreamId;
									cmd.Parameters.Add("@ComponentId", SqlDbType.VarChar, 42).Value = ComponentId;
									cmd.Parameters.Add("@Component_WtPcnt", SqlDbType.Real).Value = Component_WtPcnt;

									cmd.ExecuteNonQuery();
								}
							}
							catch (Exception ex)
							{
								ErrorHandler.Insert_UpLoadError(SimModelId, "Insert_Streams", Refnum, wks.Parent, wks, rng, 0, 0, cmd.CommandText, ex);
							}
						}
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError(SimModelId, "Insert_Streams", Refnum, wks.Parent, wks, rng, 0, 0, cmd.CommandText, ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
		}

		void Insert_Plant(Excel.Worksheet wks, string SimModelId, string Refnum)
		{
			Excel.Range rng = null;

			const int colOpCondBeg = 2;
			const int colOpCondEnd = 4;

			string OpCondId = string.Empty;
			string ComponentId = string.Empty;

			double SimEnergy_kCalkg;
			double Component_WtPcnt;

			System.Data.SqlClient.SqlCommand cmd = null;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());
			cn.Open();

			for (int c = colOpCondBeg; c <= colOpCondEnd; c++)
			{
				try
				{
					rng = wks.Cells[rowEnergy, c];
					SimEnergy_kCalkg = (double)rng.Value2;

					if (SimEnergy_kCalkg > 0.0)
					{
						OpCondId = ConvertColumnToOpCondId(c);

						cmd = new System.Data.SqlClient.SqlCommand("[stgFact].[Insert_EnergyConsumptionPlant]", cn);
						cmd.CommandType = System.Data.CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
						cmd.Parameters.Add("@SimModelId", SqlDbType.VarChar, 12).Value = SimModelId;
						cmd.Parameters.Add("@OpCondId", SqlDbType.VarChar, 12).Value = OpCondId;
						cmd.Parameters.Add("@SimEnergy_kCalkg", SqlDbType.Real).Value = SimEnergy_kCalkg;

						cmd.ExecuteNonQuery();

						for (int r = rowComponentBeg; r <= rowComponentEnd; r++)
						{
							try
							{
								rng = wks.Cells[r, c];
								Component_WtPcnt = (double)rng.Value2;

								if (Component_WtPcnt > 0.0)
								{
									ComponentId = ConvertComponentId(wks.Cells[r, Simulation.colComponentId].Value2);

									cmd = new System.Data.SqlClient.SqlCommand("[stgFact].[Insert_YieldCompositionPlant]", cn);
									cmd.CommandType = System.Data.CommandType.StoredProcedure;

									cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
									cmd.Parameters.Add("@SimModelId", SqlDbType.VarChar, 12).Value = SimModelId;
									cmd.Parameters.Add("@OpCondId", SqlDbType.VarChar, 12).Value = OpCondId;
									cmd.Parameters.Add("@ComponentId", SqlDbType.VarChar, 42).Value = ComponentId;
									cmd.Parameters.Add("@Component_WtPcnt", SqlDbType.Real).Value = Component_WtPcnt;

									cmd.ExecuteNonQuery();
								}
							}
							catch (Exception ex)
							{
								ErrorHandler.Insert_UpLoadError(SimModelId, "Insert_Plant", Refnum, wks.Parent, wks, rng, 0, 0, cmd.CommandText, ex);
							}
						}
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError(SimModelId, "Insert_Plant", Refnum, wks.Parent, wks, rng, 0, 0, cmd.CommandText, ex);
				}
			}
			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
		}

		void Insert_SpecificPyps(Excel.Worksheet wks, string Refnum)
		{
			Excel.Range rng = null;

			const int rowSeverityBasis = 52;
			const int colStreamUniqueEnd = 27;
			const int rowNonStdFeedType = 41;

			string StreamId = string.Empty;
			string SevBasis;

			System.Data.SqlClient.SqlCommand cmd = null;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());
			cn.Open();

			for (int c = colStreamBeg; c <= colStreamUniqueEnd; c++)
			{
				try
				{
					rng = wks.Cells[rowSeverityBasis, c];
					SevBasis = Convert.ToString(rng.Value2);

					if (SevBasis != string.Empty && SevBasis != null)
					{
						StreamId = (string)wks.Cells[rowStreamId, c].Value2;

						cmd = new System.Data.SqlClient.SqlCommand("[stgFact].[Insert_SimulationSeverity_Pyps]", cn);
						cmd.CommandType = System.Data.CommandType.StoredProcedure;

						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
						cmd.Parameters.Add("@StreamId", SqlDbType.VarChar, 42).Value = StreamId;

						cmd.Parameters.Add("@SeverityBasis", SqlDbType.VarChar, 12).Value = SevBasis;

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("PYPS", "Insert_SpecificPyps", Refnum, wks.Parent, wks, rng, 0, 0, cmd.CommandText, ex);
				}
			}

			try
			{
				rng = wks.Cells[36, 1];
				string AdjButane = (string)rng.Value2;

				rng = wks.Cells[37, 1];
				string ErrorStream = (string)rng.Value2;

				int? FeedId_Raffinate = null;
				int? FeedId_Heavy = null;
				int? FeedId_Condensate = null;
				int? FeedId_Atmospheric = null;
				int? FeedId_Vacuum = null;
				int? FeedId_Hydrotreated = null;

				int? FeedId_OthLiq1 = null;
				int? FeedId_OthLiq2 = null;
				int? FeedId_OthLiq3 = null;

				//foo.BarEvent += (s, e) => { if (e.Value == true) DoSomething(); };
				int col;

				col = 22;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_Raffinate = (int)rng.Value2; };

				col = 23;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_Heavy = (int)rng.Value2; };

				col = 24;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_Condensate = (int)rng.Value2; };

				col = 25;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_Atmospheric = (int)rng.Value2; };

				col = 26;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_Vacuum = (int)rng.Value2; };

				col = 27;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_Hydrotreated = (int)rng.Value2; };

				col = 16;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_OthLiq1 = (int)rng.Value2; };

				col = 17;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_OthLiq2 = (int)rng.Value2; };

				col = 18;
				rng = wks.Cells[rowNonStdFeedType, col];
				if ((string)rng.Text != ".") { FeedId_OthLiq3 = (int)rng.Value2; };


				cmd = new System.Data.SqlClient.SqlCommand("[stgFact].[Insert_SimulationParameters_Pyps]", cn);
				cmd.CommandType = System.Data.CommandType.StoredProcedure;

				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;
				cmd.Parameters.Add("@AdjButane", SqlDbType.VarChar, 50).Value = AdjButane;
				cmd.Parameters.Add("@ErrorStream", SqlDbType.VarChar, 14).Value = ErrorStream;

				cmd.Parameters.Add("@FeedId_Raffinate", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_Heavy", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_Condensate", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_Atmospheric", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_Vacuum", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_Hydrotreated", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_OthLiq1", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_OthLiq2", SqlDbType.Int);
				cmd.Parameters.Add("@FeedId_OthLiq3", SqlDbType.Int);

				cmd.Parameters["@FeedId_Raffinate"].Value = FeedId_Raffinate;
				cmd.Parameters["@FeedId_Heavy"].Value = FeedId_Heavy;
				cmd.Parameters["@FeedId_Condensate"].Value = FeedId_Condensate;
				cmd.Parameters["@FeedId_Atmospheric"].Value = FeedId_Atmospheric;
				cmd.Parameters["@FeedId_Vacuum"].Value = FeedId_Vacuum;
				cmd.Parameters["@FeedId_Hydrotreated"].Value = FeedId_Hydrotreated;
				cmd.Parameters["@FeedId_OthLiq1"].Value = FeedId_OthLiq1;
				cmd.Parameters["@FeedId_OthLiq2"].Value = FeedId_OthLiq2;
				cmd.Parameters["@FeedId_OthLiq3"].Value = FeedId_OthLiq3;

				cmd.ExecuteNonQuery();

			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("PYPS", "Insert_SpecificPyps", Refnum, wks.Parent, wks, rng, 0, 0, cmd.CommandText, ex);
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
		}

		static string ConvertComponentId(string ComponentName)
		{
			string ComponentId = string.Empty;

			switch (ComponentName)
			{
				case "HYDROGEN": 
					ComponentId = "H2";
					break;
				case "METHANE":
					ComponentId = "CH4";
					break;
				case "ACETYLENE": 
					ComponentId = "C2H2";
					break;
				case "ETHYLENE": 
					ComponentId = "C2H4";
					break;
				case "ETHANE": 
					ComponentId = "C2H6";
					break;
				case "MA + PD": 
					ComponentId = "C3H4";
					break;
				case "PROPYLENE":
					ComponentId = "C3H6";
					break;
				case "PROPANE": 
					ComponentId = "C3H8";
					break;
				case "BUTADIENE":
					ComponentId = "C4H6";
					break;
				case "BUTENE": 
					ComponentId = "C4H8";
					break;
				case "BUTANE": 
					ComponentId = "C4H10";
					break;
				case "C5-S":
					ComponentId = "C5S";
					break;
				case "C6-C8 NA": 
					ComponentId = "C6C8NA";
					break;
				case "BENZENE": 
					ComponentId = "C6H6";
					break;
				case "TOLUENE": 
					ComponentId = "C7H8";
					break;
				case "XY + ETB":
					ComponentId = "C8H10";
					break;
				case "STYRENE": 
					ComponentId = "C8H8";
					break;
				case "C9-400F":
					ComponentId = "PyroGasOil";
					break;
				case "FUEL OIL": 
					ComponentId = "PyroFuelOil";
					break;
			}
			return ComponentId;
		}

		static string ConvertColumnToOpCondId(int c)
		{
			string OpCondId = string.Empty;

			if ((c >= 7 && c <= 27) || c == 2 || c == 70) { OpCondId = "OS25"; }
			if ((c >= 28 && c <= 48) || c == 3 || c == 71) { OpCondId = "MS25"; }
			if ((c >= 49 && c <= 69) || c == 4 || c == 72) { OpCondId = "OSOP"; }

			return OpCondId;
		}

		string ConvertFeedProdId(Excel.Range rng)
		{
			string FeedProdID = Convert.ToString(rng.Value).Trim();

			switch (FeedProdID)
			{
				case "ETHANE": return "Ethane";
				case "PROPANE": return "Propane";
				case "LPG": return "LPG";
				case "BUTANE": return "Butane";
				case "LT_GAS": return "EPMix";

				case "ETH_RCY": return "EthRec";
				case "PRO_RCY": return "ProRec";
				case "BUT_RCY": return "ButRec";

				case "OthLight": return "OthLtFeed";

				case "OthLiq1": return "OthLiqFeed1";
				case "OthLiq2": return "OthLiqFeed2";
				case "OthLiq3": return "OthLiqFeed3";

				case "NAPHTHA": return "FRNaphtha";
				case "LT_NAPH": return "HeavyNaphtha";
				case "HV_NAPH": return "LtNaphtha";

				case "RAFFIN": return "Raffinate";
				case "HV_GAS": return "HeavyNGL";
				case "FIELD": return "Condensate";

				case "AT_GAS": return "Diesel";
				case "VAC_GAS": return "HeavyGasoil";
				case "HYDRO": return "HTGasoil";

				default: return ReturnString(rng, 20);
			}
		}

		string ReturnString(Excel.Range rng, UInt32 FieldLength)
		{
			string t = Convert.ToString(rng.Value).Trim();
			return t.Substring(0, Math.Min(t.Length, (int)FieldLength));
		}
	}
}
