﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadEnviron(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			const UInt32 c = 5;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table8-4"];

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Environ]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum		CHAR (9)
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				//@Emis   REAL     = NULL
				r = 19;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Emis", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Waste  REAL     = NULL
				r = 24;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Waste", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@NOx    REAL     = NULL
				r = 28;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@NOx", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@Water  REAL     = NULL
				r = 32;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@Water", SqlDbType.Float).Value = ReturnFloat(rng); }

				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCapitalExp", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Environ]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
