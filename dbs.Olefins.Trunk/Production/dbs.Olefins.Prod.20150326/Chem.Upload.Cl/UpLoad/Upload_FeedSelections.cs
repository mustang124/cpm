﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadFeedSelections(Excel.Workbook wkb, string Refnum, uint StudyYear)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 y = 0;
			if (StudyYear >= 2009) { y = 2; }

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table11"];

			object[] itm = new object[4]
			{
				new object[3] { "LPG", 8, 5 },
				new object[3] { "Condensate", 9, 6 },
				new object[3] { "Naphtha", 10, 7 },
				new object[3] { "GasOil", 11, 8 }
			};

			foreach (object[] s in itm)
			{
				try
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_FeedSelections]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum			CHAR (9),
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@FeedProdID			VARCHAR (20)
					cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 20).Value = (string)s[0];

					#region Table11-a

					c = Convert.ToUInt32(s[1]);

					//@Mix					CHAR (1)		= NULL
					r = 51;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Mix", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Blend				CHAR (1)		= NULL
					r = 53;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Blend", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Fractionation		CHAR (1)		= NULL
					r = 54;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Fractionation", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Hydrotreat			CHAR (1)		= NULL
					r = 55;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Hydrotreat", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Purification			CHAR (1)		= NULL
					r = 57;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Purification", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					if ((string)s[0] != "LPG")
					{
						//@MinSGEst				REAL			= NULL
						r = 60 + y;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MinSGEst", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MaxSGEst				REAL			= NULL
						r = 61 + y;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MaxSGEst", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MinSGAct				REAL			= NULL
						r = 62 + y;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MinSGAct", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MaxSGAct				REAL			= NULL
						r = 63 + y;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MaxSGAct", SqlDbType.Float).Value = ReturnFloat(rng); }

					}

					#endregion

					#region Table11-b

					c = Convert.ToUInt32(s[2]);

					//@PipelinePct			REAL			= NULL
					r = 75 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@PipelinePct", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@TruckPct				REAL			= NULL
					r = 76 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@TruckPct", SqlDbType.Float).Value = ReturnFloat(rng); }
					if (RangeHasValue(rng))

						//@BargePct				REAL			= NULL
						r = 77 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@BargePct", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@ShipPct				REAL			= NULL
					r = 78 + y;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@ShipPct", SqlDbType.Float).Value = ReturnFloat(rng); }

					#endregion

					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadFeedSelections", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_FeedSelections]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
