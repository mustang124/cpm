﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadEnergyQnty(Excel.Workbook wkb, string Refnum, uint StudyYear)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			UInt32 rBeg = 0;
			UInt32 rEnd = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["xTable7"];

			#region Purchased and Imported Energy

			rBeg = 7;
			rEnd = 14;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@EnergyType       CHAR (12)
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = ConvertEnergyId(rng, r); }

						//@Methane_WtPcnt   REAL          = NULL
						c = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Methane_WtPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MBTU             REAL          = NULL
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MBTU", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Cost             REAL          = NULL
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Cost", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();

					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
				}
			}

			#endregion

			#region Steam: Purchased

			rBeg = 16;
			rEnd = 19;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@EnergyType       CHAR (12)
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = ConvertEnergyId(rng, r); }

						//@Amount           REAL          = NULL
						c = 4;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Amount", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PSIG             REAL          = NULL
						c = 5;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Temperature      REAL          = NULL
						c = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Temperature", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MBTU             REAL          = NULL
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MBTU", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Cost             REAL          = NULL
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Cost", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
				}
			}

				#endregion

			#region Steam Purchased Fuel, Methane Content

			try
			{
				r = 21;
				c = 6;

				rng = wks.Cells[r, c];
				if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum			CHAR (9),
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@EnergyType       CHAR (12)
					cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = "PurchSteam";

					//@Methane_WtPcnt   REAL          = NULL
					cmd.Parameters.Add("@Methane_WtPcnt", SqlDbType.Float).Value = ReturnFloat(rng);

					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
			}

			#endregion

			#region Electricity: Consumed

			try
			{
				r = 24;
				c = 4;

				rng = wks.Cells[r, c];
				if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum			CHAR (9),
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@EnergyType       CHAR (12)
					cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = "ElecPower";

					//@Amount           REAL          = NULL
					c = 4;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Amount", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@PSIG             REAL          = NULL
					c = 5;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng) && StudyYear >= 2011) { cmd.Parameters.Add("@PSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@MBTU             REAL          = NULL
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@MBTU", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@Cost             REAL          = NULL
					c = 8;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Cost", SqlDbType.Float).Value = ReturnFloat(rng); }

					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
			}

			#endregion

			#region Electricity: Export

			try
			{
				r = 45;
				c = 4;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum			CHAR (9),
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@EnergyType       CHAR (12)
					cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = "ExpElecPower";

					//@Amount           REAL          = NULL
					c = 4;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Amount", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@PSIG             REAL          = NULL
					c = 5;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@PSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@MBTU             REAL          = NULL
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@MBTU", SqlDbType.Float).Value = ReturnFloat(rng); }

					//@Cost             REAL          = NULL
					c = 8;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Cost", SqlDbType.Float).Value = ReturnFloat(rng); }

					cmd.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
			}

			#endregion

			#region PPFC

			rBeg = 27;
			rEnd = 33;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@EnergyType       CHAR (12)
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = ConvertEnergyId(rng, r); }

						//@MBTU             REAL          = NULL
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MBTU", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
				}
			}

				#endregion

			#region Steam: Exported

			rBeg = 40;
			rEnd = 43;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					c = 7;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@EnergyType       CHAR (12)
						c = 2;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = ConvertEnergyId(rng, r); }

						//@Amount           REAL          = NULL
						c = 4;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Amount", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@PSIG             REAL          = NULL
						c = 5;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@PSIG", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Temperature      REAL          = NULL
						//Done: EnergyQnty.Temperature ETL is poorly designed
						c = 6;
						if (StudyYear <= 2009) { rng = wkb.Worksheets["Table7"].Cells[r, c]; } else { rng = wks.Cells[r, c]; }
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Temperature", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@MBTU             REAL          = NULL
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@MBTU", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Cost             REAL          = NULL
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Cost", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
				}
			}

			#endregion

			#region Cogen

			try
			{
				SqlCommand cmdCogen = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
				cmdCogen.CommandType = CommandType.StoredProcedure;

				//@Refnum			CHAR (9),
				cmdCogen.Parameters.Add("@Refnum", SqlDbType.VarChar, 9).Value = Refnum;

				//@EnergyType       CHAR (12)
				cmdCogen.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = "Cogen";

				c = 8;

				//@YN               VARCHAR (3)   = NULL
				r = 53;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmdCogen.Parameters.Add("@YN", SqlDbType.VarChar, 3).Value = ReturnString(rng, 3); }

				//@PurchaseSteamPct REAL          = NULL
				r = 63;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng))
				{
					cmdCogen.Parameters.Add("@PurchaseSteamPct", SqlDbType.Float).Value = ReturnFloat(rng);
				}
				else
				{
					cmdCogen.Parameters.Add("@PurchaseSteamPct", SqlDbType.Float).Value = 0.0f;
				}

				//@ElecPowerPct     REAL          = NULL
				r = 65;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng))
				{
					cmdCogen.Parameters.Add("@ElecPowerPct", SqlDbType.Float).Value = ReturnFloat(rng);
				}
				else
				{
					cmdCogen.Parameters.Add("@ElecPowerPct", SqlDbType.Float).Value = 0.0f;
				}

				//@ThermalPct       REAL          = NULL
				r = 68;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng))
				{
					cmdCogen.Parameters.Add("@ThermalPct", SqlDbType.Float).Value = ReturnFloat(rng);
				}
				else
				{
					cmdCogen.Parameters.Add("@ThermalPct", SqlDbType.Float).Value = 0.0f;
				}

				cmdCogen.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
			}

			#endregion

			#region Turbines

			rBeg = 57;
			rEnd = 60;

			for (r = rBeg; r <= rEnd; r++)
			{
				try
				{
					c = 6;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng) && ReturnFloat(rng) != 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_EnergyQnty]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9),
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@EnergyType       CHAR (12)
						c = 3;
						rng = wks.Cells[r, c];
						cmd.Parameters.Add("@EnergyType", SqlDbType.VarChar, 12).Value = ConvertEnergyId(rng, r);

						//@UnitCnt          INT           = NULL
						c = 6;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@UnitCnt", SqlDbType.Int).Value = ReturnUShort(rng); }

						//@Amount           REAL          = NULL
						c = 7;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Amount", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Efficiency       REAL          = NULL
						c = 8;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Efficiency", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();

					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadEnergyQnty", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_EnergyQnty]", ex);
				}
			}

			#endregion

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
