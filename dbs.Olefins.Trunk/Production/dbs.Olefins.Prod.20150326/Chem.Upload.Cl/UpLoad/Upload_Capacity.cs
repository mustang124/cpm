﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadCapacity(Excel.Workbook wkb, string Refnum, uint StudyYear)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			try
			{
				cn.Open();
				wks = wkb.Worksheets["Table1-1"];

				SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Capacity]", cn);
				cmd.CommandType = CommandType.StoredProcedure;

				//@Refnum			CHAR (9),
				cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

				//@EthylCapKMTA			REAL			= NULL,
				r = 10;
				c = 6;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthylCapKMTA", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@EthylCapMTD			REAL			= NULL,
				r = 10;
				c = 8;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthylCapMTD", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@EthylMaxCapMTD		REAL			= NULL,
				r = 10;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@EthylMaxCapMTD", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PropylCapKMTA		REAL			= NULL,
				r = 11;
				c = 6;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PropylCapKMTA", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@OlefinsCapMTD		REAL			= NULL,
				r = 12;
				c = 8;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OlefinsCapMTD", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@OlefinsMaxCapMTD		REAL			= NULL,
				r = 12;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@OlefinsMaxCapMTD", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@FurnaceCapKMTA		REAL			= NULL,
				r = 13;
				c = 6;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@FurnaceCapKMTA", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@RecycleCapKMTA		REAL			= NULL,
				r = 14;
				c = 6;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@RecycleCapKMTA", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@SvcFactor			REAL			= NULL,
				r = 16;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@SvcFactor", SqlDbType.Float).Value = ReturnFloat(rng); }
				//if (RangeHasValue(rng) && StudyYear <= 2007) { cmd.Parameters.Add("@SvcFactor", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ExpansionPcnt		REAL			= NULL,
				r = 19;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ExpansionPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@ExpansionDate		DATETIME		= NULL,
				r = 20;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@ExpansionDate", SqlDbType.DateTime).Value = ReturnDateTime(rng); }

				//@CapAllow				CHAR (1)		= NULL,
				r = 23;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@CapAllow", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

				//@CapLossPcnt			REAL			= NULL,
				r = 27;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@CapLossPcnt", SqlDbType.Float).Value = ReturnFloat(rng); }

				//@PlantStartupDate		SMALLINT		= NULL
				r = 29;
				c = 9;
				rng = wks.Cells[r, c];
				if (RangeHasValue(rng)) { cmd.Parameters.Add("@PlantStartupDate", SqlDbType.SmallInt).Value = ReturnShort(rng); }

				cmd.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCapacity", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Capacity]", ex);
			}
			finally
			{
				if (cn.State != ConnectionState.Closed) { cn.Close(); }
				rng = null;
				wks = null;
			}
		}
	}
}
