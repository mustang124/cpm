﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadMetaOpEx(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rngNve = null;
			Excel.Range rngVol = null;

			UInt32 c = 0;

			const UInt32 rNve = 46;
			const UInt32 rVol = 47;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table13"];

			object[] itm = new object[5]
			{
				new object[2] { "Qtr1", 6 },
				new object[2] { "Qtr2", 7 },
				new object[2] { "Qtr3", 8 },
				new object[2] { "Qtr4", 9 },
				new object[2] { "Total", 10 }
			};

			foreach (object[] s in itm)
			{
				try
				{
					c = Convert.ToUInt32(s[1]);

					rngNve = wks.Cells[rNve, c];
					rngVol = wks.Cells[rVol, c];

					if ((RangeHasValue(rngNve) && ReturnFloat(rngNve) > 0.0f) || (RangeHasValue(rngVol) && ReturnFloat(rngVol) > 0.0f))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_MetaOpEx]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum				CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@Type   CHAR (10),
						cmd.Parameters.Add("@Type", SqlDbType.VarChar, 10).Value = Convert.ToString(s[0]);

						//@NVE    REAL      = NULL,
						if (RangeHasValue(rngNve)) { cmd.Parameters.Add("@NVE", SqlDbType.Float).Value = ReturnFloat(rngNve); }

						//@Vol    REAL      = NULL		
						if (RangeHasValue(rngVol)) { cmd.Parameters.Add("@Vol", SqlDbType.Float).Value = ReturnFloat(rngVol); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMetaOpEx", Refnum, wkb, wks, rngNve, rNve, c, "[stgFact].[Insert_MetaOpEx]", ex);
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadMetaOpEx", Refnum, wkb, wks, rngVol, rVol, c, "[stgFact].[Insert_MetaOpEx]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rngNve = null;
			rngVol = null;
			wks = null;
		}
	}
}
