﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadDuties(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			const UInt32 c = 8;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table8-1"];

			object[] itm = new object[7]
			{
				new object[2] { "FRNaphtha", 39 },
				new object[2] { "GasOil", 40 },
				new object[2] { "Other", 41 },
				new object[2] { "Ethylene", 42 },
				new object[2] { "Propylene", 43 },
				new object[2] { "Butadiene", 44 },
				new object[2] { "Benzene", 45 }
			};

			foreach (object[] s in itm)
			{
				try
				{
					r = Convert.ToUInt32(s[1]);
					rng = wks.Cells[r, c];

					if (RangeHasValue(rng) && ReturnFloat(rng) > 0.0f)
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_Duties]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum		CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@FeedProdID	VARCHAR (30)
						cmd.Parameters.Add("@FeedProdID", SqlDbType.VarChar, 30).Value = Convert.ToString(s[0]);

						//@ImpTx			REAL		= NULL
						cmd.Parameters.Add("@ImpTx", SqlDbType.Float).Value = ReturnFloat(rng);

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadDuties", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_Duties]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
