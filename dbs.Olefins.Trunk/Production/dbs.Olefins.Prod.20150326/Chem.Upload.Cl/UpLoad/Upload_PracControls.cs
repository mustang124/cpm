﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadPracControls(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			string sht = string.Empty;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());
			cn.Open();

			object[] itm = new object[2]
			{
				new object[2] { "MPC", 8 },
				new object[2] { "SEP", 10 }
			};

			foreach (object[] s in itm)
			{
				try
				{
					SqlCommand cmd = new SqlCommand("[stgFact].[Insert_PracControls]", cn);
					cmd.CommandType = CommandType.StoredProcedure;

					//@Refnum			CHAR (9),
					cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

					//@ContType			CHAR (3),
					cmd.Parameters.Add("@ContType", SqlDbType.VarChar, 3).Value = (string)s[0];

					c = Convert.ToUInt32(s[1]);

					#region Table9-4

					sht = "Table9-4";

					wks = wkb.Worksheets[sht];

					//@Tbl9038  CHAR (1) = NULL
					r = 42;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9038", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9039  CHAR (1) = NULL
					r = 44;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9039", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9040  CHAR (1) = NULL
					r = 47;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9040", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9041  CHAR (1) = NULL
					r = 49;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9041", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9042  CHAR (1) = NULL
					r = 53;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9042", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					#endregion

					#region Table9-5

					sht = "Table9-5";

					wks = wkb.Worksheets[sht];

					//@Tbl9043  CHAR (1) = NULL
					r = 11;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9043", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9044  CHAR (1) = NULL
					r = 14;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9044", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9045  CHAR (1) = NULL
					r = 16;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9045", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9046  CHAR (1) = NULL
					r = 19;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9046", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9047  CHAR (1) = NULL
					r = 22;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9047", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9048  CHAR (1) = NULL
					r = 24;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9048", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9049  CHAR (1) = NULL
					r = 26;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9049", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9050  CHAR (1) = NULL
					r = 28;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9050", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9052  CHAR (1) = NULL
					r = 38;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9052", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9053  CHAR (1) = NULL
					r = 40;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9053", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9054  CHAR (1) = NULL
					r = 44;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9054", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9055  CHAR (1) = NULL
					r = 46;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9055", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9056  CHAR (1) = NULL
					r = 49;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9056", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9057  CHAR (1) = NULL
					r = 53;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9057", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					#endregion

					#region Table9-6

					sht = "Table9-6";

					wks = wkb.Worksheets[sht];

					//@Tbl9058  CHAR (1) = NULL
					r = 10;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9058", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9059  CHAR (1) = NULL
					r = 13;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9059", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9060  CHAR (1) = NULL
					r = 15;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9060", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9062  CHAR (1) = NULL
					r = 27;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9062", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9063  CHAR (1) = NULL
					r = 29;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9063", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9064  CHAR (1) = NULL
					r = 32;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9064", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9065  CHAR (1) = NULL
					r = 34;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9065", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9066  CHAR (1) = NULL
					r = 36;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9066", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9067  CHAR (1) = NULL
					r = 38;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9067", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9068  CHAR (1) = NULL
					r = 41;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9068", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9069  CHAR (1) = NULL
					r = 44;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9069", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9070  CHAR (1) = NULL
					r = 47;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9070", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9071n CHAR (1) = NULL
					r = 49;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9071n", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9072n CHAR (1) = NULL
					r = 51;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9072n", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9073n CHAR (1) = NULL
					r = 53;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9073n", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9074n CHAR (1) = NULL
					r = 56;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9074n", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					//@Tbl9075n CHAR (1) = NULL
					r = 58;
					rng = wks.Cells[r, c];
					if (RangeHasValue(rng)) { cmd.Parameters.Add("@Tbl9075n", SqlDbType.VarChar, 1).Value = ReturnString(rng, 1); }

					#endregion

					cmd.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadPracControls", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_PracControls]", ex);
				}
			}


			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
