﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.SqlClient;

namespace Chem.UpLoad
{
	public partial class InputForm
	{
		public void UpLoadCapitalExp(Excel.Workbook wkb, string Refnum)
		{
			Excel.Worksheet wks = null;
			Excel.Range rng = null;

			UInt32 r = 0;
			UInt32 c = 0;

			SqlConnection cn = new SqlConnection(Chem.UpLoad.Common.cnString());

			cn.Open();
			wks = wkb.Worksheets["Table8-3"];

			object[] itm = new object[3]
			{
				new object[2] { "CUR", 7 },
				new object[2] { "FUT", 8 },
				new object[2] { "ADD", 9 }
			};

			foreach (object[] s in itm)
			{
				try
				{
					c = Convert.ToUInt32(s[1]);

					r = 47;
					rng = wks.Cells[r, c];
					if ((RangeHasValue(rng) && ReturnFloat(rng) > 0.0f))
					{
						SqlCommand cmd = new SqlCommand("[stgFact].[Insert_CapitalExp]", cn);
						cmd.CommandType = CommandType.StoredProcedure;

						//@Refnum			CHAR (9)
						cmd.Parameters.Add("@Refnum", SqlDbType.VarChar, 25).Value = Refnum;

						//@CptlCode			CHAR (3)
						cmd.Parameters.Add("@CptlCode", SqlDbType.VarChar, 3).Value = Convert.ToString(s[0]);

						//@Onsite			REAL	= NULL
						r = 39;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Onsite", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Offsite			REAL	= NULL
						r = 40;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Offsite", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Exist			REAL	= NULL
						r = 41;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Exist", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@EnergyCons		REAL	= NULL
						r = 42;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@EnergyCons", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Envir			REAL	= NULL
						r = 43;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Envir", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Safety			REAL	= NULL
						r = 44;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Safety", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Computer			REAL	= NULL
						r = 45;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Computer", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Maint			REAL	= NULL
						r = 46;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Maint", SqlDbType.Float).Value = ReturnFloat(rng); }

						//@Total			REAL	= NULL
						r = 47;
						rng = wks.Cells[r, c];
						if (RangeHasValue(rng)) { cmd.Parameters.Add("@Total", SqlDbType.Float).Value = ReturnFloat(rng); }

						cmd.ExecuteNonQuery();
					}
				}
				catch (Exception ex)
				{
					ErrorHandler.Insert_UpLoadError("OSIM", "UpLoadCapitalExp", Refnum, wkb, wks, rng, r, c, "[stgFact].[Insert_CapitalExp]", ex);
				}
			}

			if (cn.State != ConnectionState.Closed) { cn.Close(); }
			rng = null;
			wks = null;
		}
	}
}
