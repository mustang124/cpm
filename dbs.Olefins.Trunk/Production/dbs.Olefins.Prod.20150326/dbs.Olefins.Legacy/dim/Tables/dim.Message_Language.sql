﻿CREATE TABLE [dim].[Message_Language] (
    [LanguageId]        INT                CONSTRAINT [DF_Message_Language_MessageLanguage] DEFAULT ((1033)) NOT NULL,
    [MessageId]         INT                NOT NULL,
    [DisplayName]       NVARCHAR (256)     NULL,
    [DisplayDetail]     NVARCHAR (256)     NULL,
    [DisplaySection]    NVARCHAR (256)     NULL,
    [DisplayLocation]   NVARCHAR (256)     NULL,
    [DisplayError]      NVARCHAR (256)     NULL,
    [DisplayCorrection] NVARCHAR (256)     NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Message_Language_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_Message_Language_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_Message_Language_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_Message_Language_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Message_Language] PRIMARY KEY CLUSTERED ([LanguageId] ASC, [MessageId] ASC),
    CONSTRAINT [CL_Message_Language_MessageCorrection] CHECK ([DisplayCorrection]<>''),
    CONSTRAINT [CL_Message_Language_MessageDetail] CHECK ([DisplayDetail]<>''),
    CONSTRAINT [CL_Message_Language_MessageError] CHECK ([DisplayError]<>''),
    CONSTRAINT [CL_Message_Language_MessageLocation] CHECK ([DisplayLocation]<>''),
    CONSTRAINT [CL_Message_Language_MessageName] CHECK ([DisplayName]<>''),
    CONSTRAINT [CL_Message_Language_MessageSection] CHECK ([DisplaySection]<>''),
    CONSTRAINT [CV_Message] CHECK ([DisplayName] IS NOT NULL OR [DisplayDetail] IS NOT NULL OR [DisplaySection] IS NOT NULL OR [DisplayLocation] IS NOT NULL OR [DisplayError] IS NOT NULL OR [DisplayCorrection] IS NOT NULL),
    CONSTRAINT [FK_Message_Language_lcid] FOREIGN KEY ([LanguageId]) REFERENCES [dim].[SysLanguages_LookUp] ([lcid]),
    CONSTRAINT [FK_Message_Language_Message_LookUp] FOREIGN KEY ([MessageId]) REFERENCES [dim].[Message_LookUp] ([MessageId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Message_Language_LangId=1033]
    ON [dim].[Message_Language]([LanguageId] ASC, [MessageId] ASC)
    INCLUDE([DisplayName], [DisplayDetail], [DisplaySection], [DisplayLocation], [DisplayError], [DisplayCorrection]) WHERE ([LanguageId]=(1033));


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayName]
    ON [dim].[Message_Language]([DisplayName] ASC) WHERE ([DisplayName] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayDetail]
    ON [dim].[Message_Language]([DisplayDetail] ASC) WHERE ([DisplayDetail] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [UX_Message_Language_DisplaySection]
    ON [dim].[Message_Language]([DisplaySection] ASC) WHERE ([DisplaySection] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [UX_Message_Language_DisplayLocation]
    ON [dim].[Message_Language]([DisplayLocation] ASC) WHERE ([DisplayLocation] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayError]
    ON [dim].[Message_Language]([DisplayError] ASC) WHERE ([DisplayError] IS NOT NULL);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Message_Language_DisplayCorrection]
    ON [dim].[Message_Language]([DisplayCorrection] ASC) WHERE ([DisplayCorrection] IS NOT NULL);


GO

CREATE TRIGGER [dim].[t_Message_Language_u]
	ON [dim].[Message_Language]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [dim].[Message_Language]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[dim].[Message_Language].[MessageId]		= INSERTED.[MessageId]
		AND	[dim].[Message_Language].[LanguageId]		= INSERTED.[LanguageId];

END;