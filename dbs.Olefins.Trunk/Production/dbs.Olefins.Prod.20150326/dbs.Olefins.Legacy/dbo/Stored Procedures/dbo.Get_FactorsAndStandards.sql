﻿CREATE PROCEDURE [dbo].[Get_FactorsAndStandards]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

/*	-- The pivot and select statements can be created wiht the following code:
	SELECT DISTINCT
	'p.[' + s.[Report_Prefix] + p.[Report_Suffix] + '],',
	'[' + s.[Report_Prefix] + p.[Report_Suffix] + '],'
	FROM [ante].[ReportMap_ProcessUnit]	p
	CROSS JOIN [ante].[ReportMap_Standard] s
*/

	SET NOCOUNT ON;

	IF (@MethodologyId IS NULL)
	SET @MethodologyId = IDENT_CURRENT('[ante].[Methodology]');

	SELECT
		jps.[PlantId],
		c.[MethodologyId],
		c.[SubmissionId],
		c.[Ethylene]						[TotalCapacityECapability],
		c.[ProdOlefins]						[TotalCapacityEPCapability],

		p.[EIIStdEnergyTotal] * z.[_Duration_Days] / d.[HvcProdDivisor_kMT] / 2.2046
											[EIIStdEnergyEII1],

		p.[EIIStdEnergyElectricGeneration],
		p.[EIIStdEnergyFeedPrep],
		p.[EIIStdEnergyFiredSteamBoilers],
		p.[EIIStdEnergyHydroP],
		p.[EIIStdEnergyPGas],
		p.[EIIStdEnergyReductionCGas],
		p.[EIIStdEnergyReductionCGEthylene],
		p.[EIIStdEnergyReductionCGRGProp],
		p.[EIIStdEnergyStdConfigEandP],
		p.[EIIStdEnergySuppFeedsEandP],
		p.[EIIStdEnergyTotal],
		p.[EIIStdEnergyTotalProcessUnits],
		p.[MESElectricGeneration],
		p.[MESFeedPrep],
		p.[MESFiredSteamBoilers],
		p.[MESHydroP],
		p.[MESPGas],
		p.[MESReductionCGas],
		p.[MESReductionCGEthylene],
		p.[MESReductionCGRGProp],
		p.[MESStdConfigEandP],
		p.[MESSuppFeedsEandP],
		p.[MESTotal],
		p.[MESTotalProcessUnits],
		p.[NonEnergyCESElectricGeneration],
		p.[NonEnergyCESFeedPrep],
		p.[NonEnergyCESFiredSteamBoilers],
		p.[NonEnergyCESHydroP],
		p.[NonEnergyCESPGas],
		p.[NonEnergyCESReductionCGas],
		p.[NonEnergyCESReductionCGEthylene],
		p.[NonEnergyCESReductionCGRGProp],
		p.[NonEnergyCESStdConfigEandP],
		p.[NonEnergyCESSuppFeedsEandP],
		p.[NonEnergyCESTotal],
		p.[NonEnergyCESTotalProcessUnits],
		p.[PESMaintenanceElectricGeneration],
		p.[PESMaintenanceFeedPrep],
		p.[PESMaintenanceFiredSteamBoilers],
		p.[PESMaintenanceHydroP],
		p.[PESMaintenancePGas],
		p.[PESMaintenanceReductionCGas],
		p.[PESMaintenanceReductionCGEthylene],
		p.[PESMaintenanceReductionCGRGProp],
		p.[PESMaintenanceStdConfigEandP],
		p.[PESMaintenanceSuppFeedsEandP],
		p.[PESMaintenanceTotal],
		p.[PESMaintenanceTotalProcessUnits],
		p.[PESNonMaintenanceElectricGeneration],
		p.[PESNonMaintenanceFeedPrep],
		p.[PESNonMaintenanceFiredSteamBoilers],
		p.[PESNonMaintenanceHydroP],
		p.[PESNonMaintenancePGas],
		p.[PESNonMaintenanceReductionCGas],
		p.[PESNonMaintenanceReductionCGEthylene],
		p.[PESNonMaintenanceReductionCGRGProp],
		p.[PESNonMaintenanceStdConfigEandP],
		p.[PESNonMaintenanceSuppFeedsEandP],
		p.[PESNonMaintenanceTotal],
		p.[PESNonMaintenanceTotalProcessUnits],
		p.[PESTotalElectricGeneration],
		p.[PESTotalFeedPrep],
		p.[PESTotalFiredSteamBoilers],
		p.[PESTotalHydroP],
		p.[PESTotalPGas],
		p.[PESTotalReductionCGas],
		p.[PESTotalReductionCGEthylene],
		p.[PESTotalReductionCGRGProp],
		p.[PESTotalStdConfigEandP],
		p.[PESTotalSuppFeedsEandP],
		p.[PESTotalTotal],
		p.[PESTotalTotalProcessUnits],
		p.[TotalCapacityElectricGeneration],
		p.[TotalCapacityFeedPrep],
		p.[TotalCapacityFiredSteamBoilers],
		p.[TotalCapacityHydroP],
		p.[TotalCapacityPGas],
		p.[TotalCapacityReductionCGas],
		p.[TotalCapacityReductionCGEthylene],
		p.[TotalCapacityReductionCGRGProp],
		p.[TotalCapacityStdConfigEandP],
		p.[TotalCapacitySuppFeedsEandP],
		p.[TotalCapacityTotal],
		p.[TotalCapacityTotalProcessUnits],
		p.[TotalkEDCElectricGeneration],
		p.[TotalkEDCFeedPrep],
		p.[TotalkEDCFiredSteamBoilers],
		p.[TotalkEDCHydroP],
		p.[TotalkEDCPGas],
		p.[TotalkEDCReductionCGas],
		p.[TotalkEDCReductionCGEthylene],
		p.[TotalkEDCReductionCGRGProp],
		p.[TotalkEDCStdConfigEandP],
		p.[TotalkEDCSuppFeedsEandP],
		p.[TotalkEDCTotal],
		p.[TotalkEDCTotalProcessUnits]
	FROM (
		SELECT
			u.[MethodologyId],
			u.[SubmissionId],
			l.[StreamTag],
			u.[_Utilization_Pcnt]
		FROM [calc].[Utilization]				u
		INNER JOIN [dim].[Stream_LookUp]		l
			ON	l.[StreamId] = u.[StreamId]
		WHERE	l.StreamTag IN ('Ethylene', 'ProdOlefins')
			AND	u.[MethodologyId]	= @MethodologyId
			AND	u.[SubmissionId]	= @SubmissionId
		) u
		PIVOT(MAX(u.[_Utilization_Pcnt]) FOR u.[StreamTag] IN ([Ethylene], [ProdOlefins])) c
	INNER JOIN [fact].[Submissions]				z
		ON	z.[SubmissionId] = c.[SubmissionId]
	INNER JOIN [calc].[HvcProductionDivisor]	d
		ON	d.[MethodologyId]	= c.[MethodologyId]
		AND	d.[SubmissionId]	= c.[SubmissionId]

	INNER JOIN [auth].[JoinPlantSubmission]		jps
		ON	jps.[SubmissionId] = c.[SubmissionId]

	INNER JOIN(
		SELECT
			s.[MethodologyId],
			s.[SubmissionId],
			rms.[Report_Prefix] + rmp.[Report_Suffix]	[FieldName],
			s.[StandardValue]
		FROM [calc].[Standards_Aggregate]	s
		INNER JOIN [ante].[ReportMap_Standard]		rms
			ON	rms.[MethodologyId]	= s.[MethodologyId]
			AND	rms.[StandardId]	= s.[StandardId]
		INNER JOIN [ante].[ReportMap_ProcessUnit]	rmp
			ON	rmp.[MethodologyId]	= s.[MethodologyId]
			AND	rmp.[ProcessUnitId]	= s.[ProcessUnitId]
		WHERE	s.[MethodologyId]	= @MethodologyId
			AND	s.[SubmissionId]	= @SubmissionId
		) u
		PIVOT(
		MAX(u.[StandardValue]) FOR u.[FieldName] IN (
			[EIIStdEnergyElectricGeneration],
			[EIIStdEnergyFeedPrep],
			[EIIStdEnergyFiredSteamBoilers],
			[EIIStdEnergyHydroP],
			[EIIStdEnergyPGas],
			[EIIStdEnergyReductionCGas],
			[EIIStdEnergyReductionCGEthylene],
			[EIIStdEnergyReductionCGRGProp],
			[EIIStdEnergyStdConfigEandP],
			[EIIStdEnergySuppFeedsEandP],
			[EIIStdEnergyTotal],
			[EIIStdEnergyTotalProcessUnits],
			[MESElectricGeneration],
			[MESFeedPrep],
			[MESFiredSteamBoilers],
			[MESHydroP],
			[MESPGas],
			[MESReductionCGas],
			[MESReductionCGEthylene],
			[MESReductionCGRGProp],
			[MESStdConfigEandP],
			[MESSuppFeedsEandP],
			[MESTotal],
			[MESTotalProcessUnits],
			[NonEnergyCESElectricGeneration],
			[NonEnergyCESFeedPrep],
			[NonEnergyCESFiredSteamBoilers],
			[NonEnergyCESHydroP],
			[NonEnergyCESPGas],
			[NonEnergyCESReductionCGas],
			[NonEnergyCESReductionCGEthylene],
			[NonEnergyCESReductionCGRGProp],
			[NonEnergyCESStdConfigEandP],
			[NonEnergyCESSuppFeedsEandP],
			[NonEnergyCESTotal],
			[NonEnergyCESTotalProcessUnits],
			[PESMaintenanceElectricGeneration],
			[PESMaintenanceFeedPrep],
			[PESMaintenanceFiredSteamBoilers],
			[PESMaintenanceHydroP],
			[PESMaintenancePGas],
			[PESMaintenanceReductionCGas],
			[PESMaintenanceReductionCGEthylene],
			[PESMaintenanceReductionCGRGProp],
			[PESMaintenanceStdConfigEandP],
			[PESMaintenanceSuppFeedsEandP],
			[PESMaintenanceTotal],
			[PESMaintenanceTotalProcessUnits],
			[PESNonMaintenanceElectricGeneration],
			[PESNonMaintenanceFeedPrep],
			[PESNonMaintenanceFiredSteamBoilers],
			[PESNonMaintenanceHydroP],
			[PESNonMaintenancePGas],
			[PESNonMaintenanceReductionCGas],
			[PESNonMaintenanceReductionCGEthylene],
			[PESNonMaintenanceReductionCGRGProp],
			[PESNonMaintenanceStdConfigEandP],
			[PESNonMaintenanceSuppFeedsEandP],
			[PESNonMaintenanceTotal],
			[PESNonMaintenanceTotalProcessUnits],
			[PESTotalElectricGeneration],
			[PESTotalFeedPrep],
			[PESTotalFiredSteamBoilers],
			[PESTotalHydroP],
			[PESTotalPGas],
			[PESTotalReductionCGas],
			[PESTotalReductionCGEthylene],
			[PESTotalReductionCGRGProp],
			[PESTotalStdConfigEandP],
			[PESTotalSuppFeedsEandP],
			[PESTotalTotal],
			[PESTotalTotalProcessUnits],
			[TotalCapacityElectricGeneration],
			[TotalCapacityFeedPrep],
			[TotalCapacityFiredSteamBoilers],
			[TotalCapacityHydroP],
			[TotalCapacityPGas],
			[TotalCapacityReductionCGas],
			[TotalCapacityReductionCGEthylene],
			[TotalCapacityReductionCGRGProp],
			[TotalCapacityStdConfigEandP],
			[TotalCapacitySuppFeedsEandP],
			[TotalCapacityTotal],
			[TotalCapacityTotalProcessUnits],
			[TotalkEDCElectricGeneration],
			[TotalkEDCFeedPrep],
			[TotalkEDCFiredSteamBoilers],
			[TotalkEDCHydroP],
			[TotalkEDCPGas],
			[TotalkEDCReductionCGas],
			[TotalkEDCReductionCGEthylene],
			[TotalkEDCReductionCGRGProp],
			[TotalkEDCStdConfigEandP],
			[TotalkEDCSuppFeedsEandP],
			[TotalkEDCTotal],
			[TotalkEDCTotalProcessUnits]
			)
		) p
		ON	p.[MethodologyId]	= c.[MethodologyId]
		AND	p.[SubmissionId]	= c.[SubmissionId];

END;
