﻿CREATE PROCEDURE [dbo].[Insert_StreamComposition]
(
	@SubmissionId			INT,
	@StreamNumber			INT,
	@ComponentId			INT,

	@Component_WtPcnt		FLOAT
)
AS
BEGIN

	IF(@Component_WtPcnt >= 0.0)
	EXECUTE [stage].[Insert_StreamComposition] @SubmissionId, @StreamNumber, @ComponentId, @Component_WtPcnt;

END;
