﻿
/****** Object:  Stored Procedure dbo.spUpdateRankSpecs    Script Date: 4/18/2003 4:32:53 PM ******/

/****** Object:  Stored Procedure dbo.spUpdateRankSpecs    Script Date: 12/28/2001 7:34:25 AM ******/
/****** Object:  Stored Procedure dbo.spUpdateRankSpecs    Script Date: 04/13/2000 8:32:50 AM ******/
CREATE PROCEDURE spUpdateRankSpecs 
	@RefListNo integer,
	@BreakID integer, 
	@RankVariableID integer
AS
UPDATE RankSpecs_LU
SET LastTime = GetDate()
WHERE RefListNo = @RefListNo AND BreakID = @BreakID 
AND RankVariableID = @RankVariableID

UPDATE RankSpecs_LU
SET NumTiles = (SELECT MAX(Tile) 
	FROM Rank 
	WHERE RankSpecsID = RankSpecs_LU.RankSpecsID)
WHERE RefListNo = @RefListNo AND BreakID = @BreakID
AND RankVariableID = @RankVariableID 


UPDATE RankSpecs_LU
SET DataCount = (SELECT Count(Refnum) 
	FROM Rank 
	WHERE RankSpecsID = RankSpecs_LU.RankSpecsID)
WHERE RefListNo = @RefListNo AND BreakID = @BreakID
AND RankVariableID = @RankVariableID

