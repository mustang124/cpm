﻿CREATE PROCEDURE [dbo].[RefreshNewFactors_StudyYear]
(
	@StudyYearBeg		INT	= NULL,
	@StudyYearEnd		INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	ALTER TABLE [fact].[Audit]	NOCHECK CONSTRAINT [FK_Audit_Submissions];

	IF(@StudyYearBeg IS NULL)
	SELECT @StudyYearBeg = MIN(t.StudyYear)
	FROM [$(DatabaseName)].[dbo].[TSort]	t;

	IF(@StudyYearEnd IS NULL)
	SELECT @StudyYearEnd = MAX(t.StudyYear)
	FROM [$(DatabaseName)].[dbo].[TSort]	t;
	
	PRINT CONVERT(VARCHAR, @StudyYearBeg) + ' - ' + CONVERT(VARCHAR, @StudyYearEnd);

	DELETE FROM [dbo].[EDCVersionCalcs] WHERE [FactorSet] = 2013;
	DELETE FROM [dbo].[EIIVersionCalcs] WHERE [FactorSet] = 2013;

	BEGIN	--	Delete from calculation engine

	DELETE FROM calc.Audit
	FROM calc.Audit						d
	INNER JOIN [fact].[Submissions]		z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]			t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.Standards
	FROM calc.Standards					d
	INNER JOIN [fact].[Submissions]		z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]			t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StandardEnergy_PyrolysisBtu
	FROM calc.StandardEnergy_PyrolysisBtu		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StandardEnergy_PyrolysisHvc
	FROM calc.StandardEnergy_PyrolysisHvc		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StandardEnergy_Reduction
	FROM calc.StandardEnergy_Reduction		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StandardEnergy_HydroPur
	FROM calc.StandardEnergy_HydroPur		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StandardEnergy_HydroTreater
	FROM calc.StandardEnergy_HydroTreater		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StandardEnergy_Fractionator
	FROM calc.StandardEnergy_Fractionator		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StandardEnergy_Supplemental
	FROM calc.StandardEnergy_Supplemental		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.ContainedLightFeed
	FROM calc.ContainedLightFeed				d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.FeedClass
	FROM calc.FeedClass							d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.Capacity_CrackedGasTransfers
	FROM calc.Capacity_CrackedGasTransfers		d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.FacilitiesHydroTreater
	FROM calc.FacilitiesHydroTreater			d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.FacilitiesFractionator
	FROM calc.FacilitiesFractionator			d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.FacilitiesBoilers
	FROM calc.FacilitiesBoilers					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.FacilitiesElecGeneration
	FROM calc.FacilitiesElecGeneration			d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.SupplementalRecovered
	FROM calc.SupplementalRecovered				d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.HvcProductionDivisor
	FROM calc.HvcProductionDivisor				d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.HydrogenPurification
	FROM calc.HydrogenPurification				d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.Capacity
	FROM calc.Capacity							d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.Utilization
	FROM calc.Utilization						d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StreamRecycled
	FROM calc.StreamRecycled					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StreamRecovered
	FROM calc.StreamRecovered					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM calc.StreamComposition
	FROM calc.StreamComposition					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	-------------------------------------------------------

	DELETE FROM fact.Audit
	FROM fact.Audit								d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.Capacity
	FROM fact.Capacity							d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.FacilitiesBoilers
	FROM fact.FacilitiesBoilers					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.FacilitiesElecGeneration
	FROM fact.FacilitiesElecGeneration			d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.FacilitiesFractionator
	FROM fact.FacilitiesFractionator			d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.FacilitiesHydroTreater
	FROM fact.FacilitiesHydroTreater			d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.FacilitiesTrains
	FROM fact.FacilitiesTrains					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.Facilities
	FROM fact.Facilities						d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.StreamDescription
	FROM fact.StreamDescription					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.StreamComposition
	FROM fact.StreamComposition					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.StreamCompositionMol
	FROM fact.StreamCompositionMol				d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.StreamDensity
	FROM fact.StreamDensity						d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.StreamRecovered
	FROM fact.StreamRecovered					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.StreamRecycled
	FROM fact.StreamRecycled					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.StreamQuantity
	FROM fact.StreamQuantity					d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.SubmissionComments
	FROM fact.SubmissionComments				d
	INNER JOIN [fact].[Submissions]				z
		ON	z.SubmissionId = d.[SubmissionId]
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = z.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	DELETE FROM fact.Submissions
	FROM fact.Submissions						d
	INNER JOIN [dbo].[TSort]					t
		ON	LTRIM(RTRIM(t.[Refnum])) = d.[SubmissionName]
	WHERE	t.[StudyYear] >= @StudyYearBeg
		AND	t.[StudyYear] <= @StudyYearEnd;

	-------------------------------------------------------

	DELETE FROM stage.StreamDescription;
	DELETE FROM stage.StreamComposition;
	DELETE FROM stage.StreamCompositionMol;
	DELETE FROM stage.StreamDensity;
	DELETE FROM stage.StreamRecovered;
	DELETE FROM stage.StreamRecycled;
	DELETE FROM stage.StreamQuantity;
	DELETE FROM stage.FacilitiesTrains;
	DELETE FROM stage.FacilitiesHydroTreater;
	DELETE FROM stage.FacilitiesFractionator;
	DELETE FROM stage.FacilitiesElecGeneration;
	DELETE FROM stage.FacilitiesBoilers;
	DELETE FROM stage.Facilities;
	DELETE FROM stage.Capacity;

	-------------------------------------------------------

	DELETE FROM stage.SubmissionComments;
	DELETE FROM auth.JoinPlantSubmission;
	DELETE FROM stage.Submissions;

	-------------------------------------------------------

	DELETE FROM auth.JoinCompanyPlant;
	DELETE FROM auth.Plants;
	DELETE FROM auth.Companies;

	DELETE FROM audit.LogError;

	END;

	---------------------------------------------------------------------------------------------------

	DECLARE @CompanyName	VARCHAR(20) = 'Solomon'
	DECLARE @PlantName		VARCHAR(20) = 'Solomon Plant'

	DECLARE	@CompanyId		INT;
	DECLARE	@PlantId		INT =1;
	DECLARE	@JoinId			INT;

	EXECUTE	@CompanyId		= [auth].[Insert_Company] @CompanyName, @CompanyName;
	EXECUTE	@PlantId		= [auth].[Insert_Plant] @PlantName;
	EXECUTE	@JoinId			= [auth].[Insert_JoinCompanyPlant] @CompanyId, @PlantId;

	---------------------------------------------------------------------------------------------------

	/*	Submission Id					*/
	INSERT INTO [stage].[Submissions]([SubmissionName], [DateBeg], [DateEnd])
	SELECT
		t.[Refnum],
		CONVERT(DATE, '01/01/' + CONVERT(CHAR(4), t.[StudyYear]))	[DateBeg],
		CONVERT(DATE, '12/31/' + CONVERT(CHAR(4), t.[StudyYear]))	[DateEnd]
	FROM [$(DatabaseName)].[dbo].[TSort]	t
	WHERE	t.StudyYear >= @StudyYearBeg
		AND	t.StudyYear	<= @StudyYearEnd;

	INSERT INTO [auth].[JoinPlantSubmission]([PlantId], [SubmissionId])
	SELECT
		@PlantId,
		s.[SubmissionId]
	FROM [stage].[Submissions] s;

	/*	Submission Comments				*/
	INSERT INTO [stage].[SubmissionComments]([SubmissionId], [SubmissionComment])
	SELECT
		z.[SubmissionId],
		'Olefins New Factor Calculation + (' + RTRIM(LTRIM(t.[Refnum])) + ')'
	FROM [$(DatabaseName)].[dbo].[TSort]	t
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = t.[Refnum];

	/*	Capacity						*/
	INSERT INTO [stage].[Capacity]([SubmissionId], [StreamId], [StreamDay_MTSD])
	SELECT
		z.[SubmissionId],
		[dim].[Return_StreamId](u.[StreamTag]),
		u.[StreamDay_MTSD]
	FROM (
		SELECT
			c.[Refnum],
			c.[EthylCapMTD]							[Ethylene],
			c.[OlefinsCapMTD] - c.[EthylCapMTD]		[Propylene]
		FROM [$(DatabaseName)].[dbo].[Capacity]	c
		) p
		UNPIVOT (
			[StreamDay_MTSD] FOR [StreamTag] IN (
				p.[Ethylene],
				p.[Propylene]
			)
		) u
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = u.[Refnum]
	WHERE u.[StreamDay_MTSD] > 0.0;

	-------------------------------------------------------------------------------

	/*	Number of Trains				*/
	INSERT INTO [stage].[FacilitiesTrains]([SubmissionId], [Train_Count])
	SELECT
		z.[SubmissionId],
		COUNT(t.[PlanInterval])
	FROM [$(DatabaseName)].[dbo].[TADT]	t
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = t.[Refnum]
	WHERE t.[PlanInterval] > 0.0
	GROUP BY
		z.[SubmissionId];

	/*	Facilities						*/
	INSERT INTO [stage].[Facilities]([SubmissionId], [FacilityId], [Unit_Count])
	SELECT
		z.[SubmissionId],
		[dim].[Return_FacilityId](u.[FacilityTag]),
		u.[Unit_Count]
	FROM (
		SELECT
			f.[Refnum],
			f.[FracFeedCnt]		[FracFeed],
			f.[HPBoilCnt]		[BoilHP],
			f.[LPBoilCnt]		[BoilLP],
			f.[ElecGenCnt]		[ElecGen],
			f.[HydroCryoCnt]	[HydroPurCryogenic],
			f.[HydroPSACnt]		[HydroPurPSA],
			f.[HydroMembCnt]	[HydroPurMembrane],
			f.[PGasHydroCnt]	[TowerPyroGasHT]
		FROM [$(DatabaseName)].[dbo].[Facilities]	f
		) p
		UNPIVOT (
			[Unit_Count] FOR [FacilityTag] IN (
				p.[FracFeed],
				p.[BoilHP],
				p.[BoilLP],
				p.[ElecGen],
				p.[HydroPurCryogenic],
				p.[HydroPurPSA],
				p.[HydroPurMembrane],
				p.[TowerPyroGasHT]
			)
		) u
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = u.[Refnum]
	WHERE u.[Unit_Count] > 0;

	/*	FacilitiesBoilers				*/
	INSERT INTO [stage].[FacilitiesBoilers]([SubmissionId], [FacilityId], [Pressure_PSIg], [Rate_kLbHr])
	SELECT
		z.[SubmissionId],
		[dim].[Return_FacilityId]('BoilHP'),
		f.[HPBoilPSIG],
		f.[HPBoilRate]
	FROM [$(DatabaseName)].[dbo].[Facilities]	f
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = f.[Refnum]
	WHERE f.[HPBoilRate]	> 0.0;

	INSERT INTO [stage].[FacilitiesBoilers]([SubmissionId], [FacilityId], [Pressure_PSIg], [Rate_kLbHr])
	SELECT
		z.[SubmissionId],
		[dim].[Return_FacilityId]('BoilLP'),
		f.[LPBoilPSIG],
		f.[LPBoilRate]
	FROM [$(DatabaseName)].[dbo].[Facilities]	f
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = f.[Refnum]
	WHERE f.[LPBoilRate]	> 0.0;

	/*	FacilitiesElecGeneration		*/
	INSERT INTO [stage].[FacilitiesElecGeneration]([SubmissionId], [FacilityId], [Capacity_MW])
	SELECT
		z.[SubmissionId],
		[dim].[Return_FacilityId]('ElecGen'),
		f.[ElecGenMW]
	FROM [$(DatabaseName)].[dbo].[Facilities]	f
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = f.[Refnum]
	WHERE f.[ElecGenMW]	> 0.0;

	/*	FacilitiesFractionator			*/
	INSERT INTO [stage].[FacilitiesFractionator]([SubmissionId], [FacilityId], [Quantity_kBSD], [StreamId], [Throughput_kMT], [Density_SG])
	SELECT
		z.[SubmissionId],
		[dim].[Return_FacilityId]('FracFeed'),
		f.[FracFeedKBSD],
		[dim].[Return_StreamId]
		(CASE f.[FracFeed]
			WHEN 'E/P'				THEN 'EPMix'
			WHEN 'EP'				THEN 'EPMix'
			WHEN 'ETHANE/PROPANE'	THEN 'EPMix'
			WHEN 'E/P MIX'			THEN 'EPMix'
			WHEN 'C2/C3'			THEN 'EPMix'
			WHEN 'LPG'				THEN 'LPG'
			WHEN 'Condensate'		THEN 'Condensate'
			WHEN 'NAPHTHA'			THEN 'Naphtha'
			WHEN 'Y-GRADE'			THEN 'LiqHeavy'
		END)			[StreamTag],
		f.[FracFeedProcMT],
		f.[SG_FracFeedProc]
	FROM [$(DatabaseName)].[dbo].[Facilities]	f
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = f.[Refnum]
	WHERE	f.[FracFeedKBSD] >= 0.0
		AND	f.[FracFeed] <> '0'
		AND	f.[FracFeedProcMT] >= 0.0
		AND	f.[SG_FracFeedProc] >= 0.0;

	/*	FacilitiesHydroTreater			*/
	INSERT INTO [stage].[FacilitiesHydroTreater]([SubmissionId], [FacilityId], [Quantity_kBSD], [HydroTreaterTypeId], [Processed_Pcnt], [Density_SG])
	SELECT
		z.[SubmissionId],
		[dim].[Return_FacilityId]('TowerPyroGasHT'),
		t.[Quantity_kBSD],
		[dim].[Return_HydroTreaterTypeId](t.[HydroTreaterTypeTag]),
		CASE WHEN t.[Processed_kMT] > t.[Product_kMT]
		THEN 1.0
		ELSE t.[Processed_kMT] / t.[Product_kMT]
		END * 100.0	[Processed_Pcnt],
		t.[Density_SG]
	FROM (
		SELECT
			f.[Refnum],
			f.[PGasHydroKBSD]		[Quantity_kBSD],
			CASE f.[PyrGasHydroType]
				WHEN 'Selective Saturation'	THEN 'SS'
				WHEN 'Both'					THEN 'B'
				END		[HydroTreaterTypeTag],
			f.[SG_NonBnzPGH]		[Density_SG],
			f.[PyrGasHydroMT]		[Processed_kMT],
			SUM(COALESCE(q.[Q1Feed], 0.0) +
				COALESCE(q.[Q2Feed], 0.0) +
				COALESCE(q.[Q3Feed], 0.0) +
				COALESCE(q.[Q4Feed], 0.0)) [Product_kMT]
		FROM [$(DatabaseName)].[dbo].[Facilities]			f
		LEFT OUTER JOIN [$(DatabaseName)].[dbo].[Quantity]	q
			ON	q.[Refnum] = f.[Refnum]
			AND	q.[FeedProdID] IN ('Benzene', 'OthPyGas')
		WHERE	f.[PGasHydroCnt] >= 1
			AND	f.[PGasHydroKBSD] >= 0.0
			AND	f.[PyrGasHydroType] IN ('Selective Saturation', 'Both')
			AND	f.[SG_NonBnzPGH] >= 0.0
		GROUP BY
			f.[Refnum],
			f.[PGasHydroKBSD],
			f.[PyrGasHydroType],
			f.[SG_NonBnzPGH],
			f.[PyrGasHydroMT]
		) t
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = t.[Refnum];

	--	Olefins study requests tons processed.

	-------------------------------------------------------------------------------
	/*	Mapping FeedProdID to StreamId	*/
	DECLARE @MapStreams	TABLE
	(
		[FeedProdID]		VARCHAR(25),
		[StreamTag]			VARCHAR(25),
		[StreamNumber]		INT
		PRIMARY KEY CLUSTERED([FeedProdID] ASC)
	)

	INSERT INTO @MapStreams([FeedProdID], [StreamTag], [StreamNumber])
	SELECT ms.[FeedProdID], ms.[StreamTag], ms.[StreamNumber]
	FROM (VALUES
		('Ethane',			'Ethane',			1001),
		('EPMix',			'EPMix',			1002),
		('Propane',			'Propane',			1003),
		('LPG',				'LPG',				1004),
		('Butane',			'Butane',			1005),
		('OthLtFeed',		'FeedLtOther',		1006),

		('LtNaphtha',		'NaphthaLt',		2001),
		('FRNaphtha',		'NaphthaFr',		2002),
		('HeavyNaphtha',	'NaphthaHv',		2003),
		('Raffinate',		'Raffinate',		2004),
		('HeavyNGL',		'HeavyNGL',			2005),
		('Condensate',		'Condensate',		2006),
		('Diesel',			'Diesel',			2007),
		('HeavyGasoil',		'GasOilHv',			2008),
		('HTGasoil',		'GasOilHt',			2009),
		('OthLiqFeed1',		'FeedLiqOther',		2010),
		('OthLiqFeed2',		'FeedLiqOther',		2011),
		('OthLiqFeed3',		'FeedLiqOther',		2012),

		('EthRec',			'EthRec',			3001),
		('ProRec',			'ProRec',			3002),
		('ButRec',			'ButRec',			3003),

		('ConcEthylene',	'ConcEthylene',		4001),
		('ConcPropylene',	'ConcPropylene',	4002),
		('ConcButadiene',	'ConcButadiene',	4003),
		('ConcBenzene',		'ConcBenzene',		4004),

		('RefHydrogen',		'RogHydrogen',		4005),
		('RefMethane',		'RogMethane',		4006),
		('RefEthane',		'RogEthane',		4007),
		('RefEthylene',		'RogEthylene',		4008),
		('RefPropane',		'RogPropane',		4009),
		('RefPropylene',	'RogPropylene',		4010),
		('RefC4Plus',		'RogC4Plus',		4011),
		('RefInerts',		'RogInerts',		4012),

		('DiHydrogen',		'DilHydrogen',		4013),
		('DiMethane',		'DilMethane',		4014),
		('DiEthane',		'DilEthane',		4015),
		('DiEthylene',		'DilEthylene',		4016),
		('DiPropane',		'DilPropane',		4017),
		('DiPropylene',		'DilPropylene',		4018),
		('DiButane',		'DilButane',		4019),
		('DiButylenes',		'DilButylene',		4020),
		('DiButadiene',		'DilButadiene',		4021),
		('DiBenzene',		'DilBenzene',		4022),
		('DiMogas',			'DilMogas',			4023),

		('WashOil',			'SuppWashOil',		4024),
		('GasOil',			'SuppGasOil',		4025),
		('OthSpl',			'SuppOther',		4026),

		('Hydrogen',		'Hydrogen',			5001),
		('FuelGasSales',	'Methane',			5002),
		('Acetylene',		'Acetylene',		5003),
		('Ethylene',		'EthylenePG',		5004),
		('EthyleneCG',		'EthyleneCG',		5005),
		('Propylene',		'PropylenePG',		5006),
		('PropyleneCG',		'PropyleneCG',		5007),
		('PropyleneRG',		'PropyleneRG',		5008),
		('PropaneC3Resid',	'PropaneC3Resid',	5009),
		('Butadiene',		'Butadiene',		5010),
		('Isobutylene',		'Isobutylene',		5011),
		('OthC4',			'C4Oth',			5012),
		('Benzene',			'Benzene',			5013),
		('OthPyGas',		'PyroGasoline',		5014),
		('PyGasoil',		'PyroGasOil',		5015),
		('PyFuelOil',		'PyroFuelOil',		5016),
		('AcidGas',			'AcidGas',			5017),
		('PPCFuel',			'PPFC',				5018),
		('OthProd1',		'ProdOther',		5019),
		('OthProd2',		'ProdOther',		5020),
		('FlareLoss',		'LossFlareVent',	5022),
		('OthLoss',			'LossOther',		5023),
		('MeasureLoss',		'LossMeasure',		5024)
		) [ms]([FeedProdID], [StreamTag], [StreamNumber]);

	DECLARE @MapComponent	TABLE
	(
		[FeedProdID]		VARCHAR(25),
		[ComponentTag]		VARCHAR(25),
		PRIMARY KEY CLUSTERED([FeedProdID] ASC)
	);

	INSERT INTO @MapComponent([FeedProdID], [ComponentTag])
	VALUES
		('Hydrogen',		'H2'),
		('FuelGasSales',	'CH4'),
		('Ethylene',		'C2H4'),
		('EthyleneCG',		'C2H4'),
		('Propylene',		'C3H6'),
		('PropyleneCG',		'C3H6'),
		('PropyleneRG',		'C3H6'),
		('PropaneC3Resid',	'C3H8'),
		('PPCFuel_CH4',		'CH4'),
		('PPCFuel_ETH',		'C2H4'),
		('PPCFuel_H2',		'H2'),
		('PPCFuel_Other',	'C2H6');

	/*	StreamQuantity					*/
	INSERT INTO [stage].[StreamQuantity]([SubmissionId], [StreamNumber], [StreamId], [Quantity_kMT])
	SELECT
		z.[SubmissionId],
		m.[StreamNumber],
		[dim].[Return_StreamId](m.[StreamTag]),
		COALESCE(q.[Q1Feed], 0.0) +
		COALESCE(q.[Q2Feed], 0.0) +
		COALESCE(q.[Q3Feed], 0.0) +
		COALESCE(q.[Q4Feed], 0.0)
	FROM [$(DatabaseName)].[dbo].[Quantity]	q
	INNER JOIN @MapStreams	m
		ON	m.[FeedProdID] = q.[FeedProdID]
	INNER JOIN [stage].[Submissions] z
		ON	z.[SubmissionName] = q.[Refnum]
	WHERE	COALESCE(q.[Q1Feed], 0.0) +
			COALESCE(q.[Q2Feed], 0.0) +
			COALESCE(q.[Q3Feed], 0.0) +
			COALESCE(q.[Q4Feed], 0.0)	> 0.0;

	/*	StreamAttributes				*/
	INSERT INTO [stage].[StreamDescription]([SubmissionId], [StreamNumber], [StreamDescription])
	SELECT
		z.[SubmissionId],
		m.[StreamNumber],
		COALESCE(q.[MiscFeed], q.[OthProdDesc], q.[MiscProd1], q.[MiscProd2], d.[OthLiqFeedDESC])
	FROM [$(DatabaseName)].[dbo].[Quantity]					q
	INNER JOIN [stage].[Submissions]				z
		ON	z.[SubmissionName] = q.[Refnum]
	LEFT OUTER JOIN [$(DatabaseName)].[dbo].[FeedQuality]	d
		ON	d.[Refnum] = q.[Refnum]
		AND	d.[FeedProdID] = q.[FeedProdID]
	INNER JOIN @MapStreams	m
		ON	m.[FeedProdID] = q.[FeedProdID]
	WHERE	COALESCE(q.[MiscFeed], q.[OthProdDesc], q.[MiscProd1], q.[MiscProd2], d.[OthLiqFeedDESC]) IS NOT NULL
		AND COALESCE(q.[MiscFeed], q.[OthProdDesc], q.[MiscProd1], q.[MiscProd2], d.[OthLiqFeedDESC]) <> '0';

	/*	StreamComposition				*/
	INSERT INTO [stage].[StreamComposition]([SubmissionId], [StreamNumber], [ComponentId], [Component_WtPcnt])
	SELECT
		z.[SubmissionId],
		m.[StreamNumber],
		[dim].[Return_ComponentId](u.[ComponentTag]),
		u.[Component_WtPcnt] * 100.0
	FROM (
		SELECT
			q.[Refnum],
			q.[FeedProdID],
			q.[Methane]			[CH4],
			q.[Ethane]			[C2H6],
			q.[Ethylene]		[C2H4],
			q.[Propane]			[C3H8],
			q.[Propylene]		[C3H6],
			q.[nButane]			[NBUTA],
			q.[iButane]			[IBUTA],
			q.[Isobutylene]		[B2],
			q.[Butene1]			[B1],
			q.[Butadiene]		[C4H6],
			q.[nPentane]		[NC5],
			q.[iPentane]		[IC5],
			q.[nHexane]			[NC6],
			q.[iHexane]			[C6ISO],
			q.[Septane]			[C7H16],
			q.[Octane]			[C8H18],
			q.[CO2]				[CO_CO2],
			q.[Hydrogen]		[H2],
			q.[Sulfur]			[S],
			q.[NParaffins]		[P],
			q.[IsoParaffins]	[I],
			q.[Naphthenes]		[N],
			q.[Olefins]			[O],
			q.[Aromatics]		[A]
		FROM  [$(DatabaseName)].[dbo].[FeedQuality]	q
		) p
		UNPIVOT (
			[Component_WtPcnt] FOR [ComponentTag] IN (
				p.[CH4],
				p.[C2H6],
				p.[C2H4],
				p.[C3H8],
				p.[C3H6],
				p.[NBUTA],
				p.[IBUTA],
				p.[B2],
				p.[B1],
				p.[C4H6],
				p.[NC5],
				p.[IC5],
				p.[NC6],
				p.[C6ISO],
				p.[C7H16],
				p.[C8H18],
				p.[CO_CO2],
				p.[H2],
				p.[S],
				p.[P],
				p.[I],
				p.[N],
				p.[O],
				p.[A]
			)
		) u
	INNER JOIN [stage].[Submissions]				z
		ON	z.[SubmissionName] = u.[Refnum]
	INNER JOIN @MapStreams m
		ON	m.[FeedProdID] = u.[FeedProdID]
	WHERE	u.[Component_WtPcnt] >	0.0
		AND	u.[Component_WtPcnt] <= 1.0;

	INSERT INTO [stage].[StreamComposition]([SubmissionId], [StreamNumber], [ComponentId], [Component_WtPcnt])
	SELECT
		z.[SubmissionId],
		s.[StreamNumber],
		[dim].[Return_ComponentId](c.[ComponentTag]),
		q.[WtPcnt] * 100.0
	FROM [$(DatabaseName)].[dbo].[ProdQuality]			q
	INNER JOIN [stage].[Submissions]			z
		ON	z.[SubmissionName] = q.[Refnum]
	INNER JOIN @MapStreams						s
		ON	s.[FeedProdID]	= q.[FeedProdID]
	INNER JOIN @MapComponent					c
		ON	c.[FeedProdID]	= q.[FeedProdID]
	WHERE 	q.[WtPcnt]		>  0.0
		AND q.[WtPcnt]		<= 1.0
		AND	q.[FeedProdID]	<> 'PPCFuel';

	/*	StreamCompositionMol			*/
	INSERT INTO [stage].[StreamCompositionMol]([SubmissionId], [StreamNumber], [ComponentId], [Component_MolPcnt])
	SELECT
		z.[SubmissionId],
		s.[StreamNumber],
		[dim].[Return_ComponentId](c.[ComponentTag]),
		q.[MolePcnt] * 100.0
	FROM [$(DatabaseName)].[dbo].[ProdQuality]			q
	INNER JOIN [stage].[Submissions]			z
		ON	z.[SubmissionName] = q.[Refnum]
	INNER JOIN @MapStreams						s
		ON	s.[FeedProdID]	= q.[FeedProdID]
	INNER JOIN @MapComponent					c
		ON	c.[FeedProdID]	= q.[FeedProdID]
	WHERE 	q.[MolePcnt]	>  0.0
		AND q.[MolePcnt]	<= 1.0;

	/*	StreamDensity					*/
	INSERT INTO [stage].[StreamDensity]([SubmissionId], [StreamNumber], [Density_SG])
	SELECT
		z.[SubmissionId],
		s.[StreamNumber],
		q.[SG]
	FROM [$(DatabaseName)].[dbo].[FeedQuality]			q
	INNER JOIN [stage].[Submissions]			z
		ON	z.[SubmissionName] = q.[Refnum]
	INNER JOIN @MapStreams						s
		ON	s.[FeedProdID]	= q.[FeedProdID]
	WHERE	q.[SG] > 0.0;

	/*	StreamRecovered					*/
	INSERT INTO [stage].[StreamRecovered]([SubmissionId], [StreamNumber], [Recovered_WtPcnt])
	SELECT
		z.[SubmissionId],
		s.[StreamNumber],
		q.[RecPcnt] * 100.0
	FROM [$(DatabaseName)].[dbo].[Quantity]				q
	INNER JOIN [stage].[Submissions]			z
		ON	z.[SubmissionName] = q.[Refnum]
	INNER JOIN @MapStreams						s
		ON	s.[FeedProdID]	= q.[FeedProdID]
	WHERE	q.[RecPcnt]		>  0.0
		AND	q.[RecPcnt]		<= 1.0;

	/*	StreamRecycled				*/
	INSERT INTO [stage].[StreamRecycled]([SubmissionId], [ComponentId], [Recycled_WtPcnt])
	SELECT
		z.[SubmissionId],
		[dim].[Return_ComponentId](u.[ComponentTag]),
		u.[Recycled_WtPcnt] * 100.0
	FROM (
		SELECT
			m.[Refnum],
			m.[EthPcntTot]	[C2H6],
			m.[ProPcntTot]	[C3H8],
			m.[ButPcntTot]	[C4H10]
		FROM [$(DatabaseName)].[dbo].[Misc]	m
		) p
		UNPIVOT (
			[Recycled_WtPcnt] FOR [ComponentTag] IN (
				p.[C2H6],
				p.[C3H8],
				p.[C4H10]
			)
		) u
	INNER JOIN [stage].[Submissions]			z
		ON	z.[SubmissionName] = u.[Refnum]
	WHERE	u.[Recycled_WtPcnt] >  0.0
		AND	u.[Recycled_WtPcnt] <= 1.0;

	-------------------------------------------------------------------------------

	DECLARE @SubmissionId	INT;
	DECLARE @Method			INT = ante.Return_MethodologyId('2013');
	DECLARE @Error_Count	INT = 0;

	DECLARE Cur CURSOR FAST_FORWARD FOR
	SELECT
		s.[SubmissionId]
	FROM [stage].[Submissions]	s;

	OPEN Cur;

	FETCH NEXT FROM Cur 
	INTO @SubmissionId;

	WHILE @@FETCH_STATUS = 0
	BEGIN

		PRINT @SubmissionId;
		SET	@Error_Count = 0;
		IF (@Error_Count >= 0)	EXECUTE @Error_Count = [fact].[Insert_DataSet]			 @SubmissionId;
		IF (@Error_Count >= 0)	EXECUTE @Error_Count = [calc].[Insert_DataSet]	@Method, @SubmissionId;

		FETCH NEXT FROM Cur 
		INTO @SubmissionId;

	END;

	CLOSE Cur;
	DEALLOCATE Cur;

	-------------------------------------------------------------------------------

	INSERT INTO [dbo].[EDCVersionCalcs]([Refnum], [FactorSet], [ModelVersion], [kEDC], [EDC],
		[TAAdjkUEDC], [kUEDC], [FracFeed], [FeedStock], [Furnace], [Compression], [Supplemental], [Utilities], [HydrogenPurification], [PGasHydrotreater], [LightFeed], [NaphthatFeed], [HeavyFeed], [DiSuppl], [RefSuppl], [ConcSuppl], [FracFeedKBSD], [HPBoilRate], [LPBoilRate], [ElecGenMW], [HydroCryoCnt], [HydroPSACnt], [HydroMembCnt], [PGasHydroKBSD], [GasCompBHP], [MethCompBHP], [EthyCompBHP], [PropCompBHP], [FurnaceCapKMTA], [SpareFurnaceCapKMTA], [Ethane], [Propane], [Butane], [LPG], [OthLtFeed], [HeavyGasoil], [HTGasoil], [OthLiqFeed1], [OthLiqFeed2], [OthLiqFeed3], [HeavyNaphtha], [LtNaphtha], [FRNaphtha], [Condensate], [HeavyNGL], [Diesel], [ConcEthylene], [DiEthylene], [RefEthylene], [ConcPropylene], [DiPropylene], [RefPropylene],
		[nfFresh], [nfSupp], [nfAuxiliary], [nfReduction], [nfUtilities],
		[tsCalculated])
	SELECT
		z.[SubmissionName],
		2013,
		2013,
		s.[Total],
		s.[Total] * 1000.0,
		s.[Total] * u.[TAAdjC2C3Util] [TAAdjkUEDC],
		s.[Total] * u.[UnAdjC2C3Util] [kUEDC],
		0.0 [FracFeed],
		0.0 [FeedStock],
		0.0 [Furnace],
		0.0 [Compression],
		0.0 [Supplemental],
		0.0 [Utilities],
		0.0 [HydrogenPurification],
		0.0 [PGasHydrotreater],
		0.0 [LightFeed],
		0.0 [NaphthatFeed],
		0.0 [HeavyFeed],
		0.0 [DiSuppl],
		0.0 [RefSuppl],
		0.0 [ConcSuppl],
		0.0 [FracFeedKBSD],
		0.0 [HPBoilRate],
		0.0 [LPBoilRate],
		0.0 [ElecGenMW],
		0.0 [HydroCryoCnt],
		0.0 [HydroPSACnt],
		0.0 [HydroMembCnt],
		0.0 [PGasHydroKBSD],
		0.0 [GasCompBHP],
		0.0 [MethCompBHP],
		0.0 [EthyCompBHP],
		0.0 [PropCompBHP],
		0.0 [FurnaceCapKMTA],
		0.0 [SpareFurnaceCapKMTA],
		0.0 [Ethane],
		0.0 [Propane],
		0.0 [Butane],
		0.0 [LPG],
		0.0 [OthLtFeed],
		0.0 [HeavyGasoil],
		0.0 [HTGasoil],
		0.0 [OthLiqFeed1],
		0.0 [OthLiqFeed2],
		0.0 [OthLiqFeed3],
		0.0 [HeavyNaphtha],
		0.0 [LtNaphtha],
		0.0 [FRNaphtha],
		0.0 [Condensate],
		0.0 [HeavyNGL],
		0.0 [Diesel],
		0.0 [ConcEthylene],
		0.0 [DiEthylene],
		0.0 [RefEthylene],
		0.0 [ConcPropylene],
		0.0 [DiPropylene],
		0.0 [RefPropylene],

		COALESCE(s.[Fresh], 0.0)		[nfFresh],
		COALESCE(s.[Supp], 0.0)			[nfSupp],
		COALESCE(s.[Auxiliary], 0.0)	[nfAuxiliary],
		COALESCE(s.[Reduction], 0.0)	[nfReduction],
		COALESCE(s.[Utilities], 0.0)	[nfUtilities],

		SYSDATETIME()
	FROM [calc].[Standards_Pivot_ProcessUnit]	s
	INNER JOIN [fact].[Submissions]				z
		ON	z.[SubmissionId] = s.[SubmissionId]
	INNER JOIN [dim].[Standard_LookUp]			l
		ON	l.[StandardId] = s.[StandardId]
	INNER JOIN [dbo].[CapUtil]					u
		ON	RTRIM(LTRIM(u.[Refnum])) = z.[SubmissionName]
	WHERE l.[StandardTag] = 'kEdc';

	INSERT INTO [dbo].[EIIVersionCalcs]([Refnum], [FactorSet], [ModelVersion], [EII], [StdEnergy],
		[PyrolysisFeed], [Compression], [Supplemental], [Auxiliary], [LightFeed], [NaphthatFeed], [HeavyFeed],
		[DiSuppl], [RefSuppl], [ConcSuppl], [OtherSuppl],
		[FracFeedKBSD], [HydroCryoCnt], [HydroPSACnt], [HydroMembCnt], [PGasHydroKBSD],
		[GasCompBHP], [MethCompBHP], [EthyCompBHP], [PropCompBHP],
		[Purity], [Ethane], [Propane], [Butane], [LPG], [OthLtFeed], [LtNaphtha], [Raffinate], [HeavyNGL], [Condensate], [FRNaphtha], [HeavyNaphtha], [Diesel], [HeavyGasoil], [HTGasoil], [OthLiqFeed1], [OthLiqFeed2], [OthLiqFeed3], [DiHydrogen], [DiMethane], [DiEthane], [DiEthylene], [DiPropylene], [DiPropane], [DiButane], [DiButylenes], [DiBenzene], [DiButadiene], [DiMoGas], [RefHydrogen], [RefMethane], [RefEthane], [RefEthylene], [RefPropylene], [RefPropane], [RefC4Plus], [RefInerts], [ConcEthylene], [ConcPropylene], [ConcBenzene], [ConcButadiene], [GasOil], [WashOil], [OthSpl],
		[nfFresh], [nfSupp], [nfAuxiliary], [nfReduction], [nfUtilities],
		[tsCalculated])
	SELECT DISTINCT
		z.[SubmissionName],
		2013,
		2013,
		e.[NetEnergyConsumed] / (s.[Total] * 365.0) * 100.0 [EII],
		s.[Total] * 365.0,

		0.0 [PyrolysisFeed],
		0.0 [Compression],
		0.0 [Supplemental],
		0.0 [Auxiliary],
		0.0 [LightFeed],
		0.0 [NaphthatFeed],
		0.0 [HeavyFeed],

		0.0 [DiSuppl],
		0.0 [RefSuppl],
		0.0 [ConcSuppl],
		0.0 [OtherSuppl],

		0.0 [FracFeedKBSD],
		0.0 [HydroCryoCnt],
		0.0 [HydroPSACnt],
		0.0 [HydroMembCnt],
		0.0 [PGasHydroKBSD],

		0.0 [GasCompBHP],
		0.0 [MethCompBHP],
		0.0 [EthyCompBHP],
		0.0 [PropCompBHP],

		0.0 [Purity],
		0.0 [Ethane],
		0.0 [Propane],
		0.0 [Butane],
		0.0 [LPG],
		0.0 [OthLtFeed],
		0.0 [LtNaphtha],
		0.0 [Raffinate],
		0.0 [HeavyNGL],
		0.0 [Condensate],
		0.0 [FRNaphtha],
		0.0 [HeavyNaphtha],
		0.0 [Diesel],
		0.0 [HeavyGasoil],
		0.0 [HTGasoil],
		0.0 [OthLiqFeed1],
		0.0 [OthLiqFeed2],
		0.0 [OthLiqFeed3],
		0.0 [DiHydrogen],
		0.0 [DiMethane],
		0.0 [DiEthane],
		0.0 [DiEthylene],
		0.0 [DiPropylene],
		0.0 [DiPropane],
		0.0 [DiButane],
		0.0 [DiButylenes],
		0.0 [DiBenzene],
		0.0 [DiButadiene],
		0.0 [DiMoGas],
		0.0 [RefHydrogen],
		0.0 [RefMethane],
		0.0 [RefEthane],
		0.0 [RefEthylene],
		0.0 [RefPropylene],
		0.0 [RefPropane],
		0.0 [RefC4Plus],
		0.0 [RefInerts],
		0.0 [ConcEthylene],
		0.0 [ConcPropylene],
		0.0 [ConcBenzene],
		0.0 [ConcButadiene],
		0.0 [GasOil],
		0.0 [WashOil],
		0.0 [OthSpl],

		COALESCE(s.[Fresh], 0.0)	 * 365.0	[nfFresh],
		COALESCE(s.[Supp], 0.0)		 * 365.0	[nfSupp],
		COALESCE(s.[Auxiliary], 0.0) * 365.0	[nfAuxiliary],
		COALESCE(s.[Reduction], 0.0) * 365.0	[nfReduction],
		COALESCE(s.[Utilities], 0.0) * 365.0	[nfUtilities],

		SYSDATETIME()
	FROM [calc].[Standards_Pivot_ProcessUnit]	s
	INNER JOIN [fact].[Submissions]				z
		ON	z.[SubmissionId] = s.[SubmissionId]
	INNER JOIN [dim].[Standard_LookUp]			l
		ON	l.[StandardId] = s.[StandardId]
	INNER JOIN [dbo].[EEI]						e
		ON	RTRIM(LTRIM(e.[Refnum])) = z.[SubmissionName]
		AND	e.Model = 'PYPS'
	WHERE l.[StandardTag] = 'StdEnergy';

END;
