﻿




CREATE  PROC [dbo].[CalcUpdateEDCFields](@Refnum Refnum) as
--


--Declare @Refnum as char(9)

--select @Refnum = '07PCH012'

----Update per EDC or uEDC fields in Calc Tables that have been uploaded.
DECLARE @PYPS CHAR(5)
DECLARE @SPSL CHAR(5)

SELECT @PYPS = 'pyps'
SELECT @spsl = 'spsl'

Declare @EDC as real
Declare @kEDC as real
Declare @kUEDC as real
Declare @TaAdjkUEDC as real

Select @EDC = EDC, @kEDC = kEDC, @kUEDC = kUEDC, @TAAdjkUEDC = TAAdjkUEDC from EDC Where Refnum = @Refnum


---EEI.BTUuEDC
UPDATE EEI 
	SET BTUuEDC = NetEnergyConsumed/1000/@kUEDC
	WHERE Refnum = @Refnum
		

---UnitOpex.ProformaUEDCCash
---UnitOpex.TAAdjUEDCCash
---UnitOpex.TotUEDCExp
---UnitOpex.UEDCCash
---UnitOpex.UEDCNEOpex

--June 18, 2010 changed NEOpex from UEDC, to EDC...prd
update UnitOpex
	set TAAdjUEDCCash = (Select ((VarExpAmt*AdjFactor)+FixExpAmt+AnnTAExp) 
		From FinancialTot JOIN TAAdj on Financialtot.Refnum = taadj.Refnum Where FinancialTot.Refnum = @Refnum)/@TaAdjkUEDC,
		TotUEDCExp = (Select TotOpex From FinancialTot FT Where Refnum = @Refnum)/@kUEDC,
		UEDCCash = (Select TotCash From FinancialTot FT Where Refnum = @Refnum)/@kUEDC,
		EDCNEOpex = (Select NEOpex From FinancialTot FT Where Refnum = @Refnum)/@kEDC,
		ProformaUEDCCash = (Select (VarExpAmt/UnadjC2Util)+FixExpAmt+AnnTAExp 
		From FinancialTot JOIN CapUtil on FinancialTot.Refnum = CapUtil.Refnum Where FinancialTot.Refnum = @Refnum)/@kEDC
	Where UnitOpex.Refnum = @Refnum
	
	
	



	
---	MaintInd.MaintCostEDC
--- MaintInd.NewMaintEffEDC

UPdate MaintInd
set MaintCostEDC = TotMaintCostAvg /@kEDC,
	NewMaintEffEDC = (TotMaintCostAvg+((UnreliabOppCost+FlareCost)*10*(select TotReplVal FROM Replacement Where Refnum = @Refnum)))/@kEDC 		
	Where Refnum = @Refnum


-- Productivity.EDCTEP
-- Productivity.EDCTotWHr
UPdate Productivity
set EDCTEP = (select SUM(TotEqP) from  PersSTCalc where (Refnum = @Refnum and SectionID not in ('Maint', 'NonMaint')) group by Refnum)/@kEDC*1000,
	EDCTotWHr = (select  SUM(TotWHr) from  PersSTCalc where (Refnum = @Refnum and SectionID not in ('Maint', 'NonMaint')) group by Refnum)/@EDC*100 		
	Where Refnum = @Refnum

--ROI.MfgC2C3UEDC
--ROI.MfgC2UEDC
--ROI.MfgHVCUEDC
--ROI.TAAdjMfgC2C3UEDC
--ROI.TAAdjMfgC2UEDC
--ROI.TAAdjMfgHVCUEDC
--ROI.MfgProformaC2UEDC
--ROI.MfgProformaHVCUEDC


	update ROI
	set MfgC2C3UEDC = (Select RMC-AnnTotalBPC+TotCash From FinQtr Join FinancialTot on FinQtr.Refnum = FinancialTot.Refnum
		Where FeedProdID = 'ByProdOlefin' and FinQtr.Refnum = @Refnum) /@kUEDC,
	MfgHVCUEDC = (Select RMC-AnnTotalBPC+TotCash From FinQtr Join FinancialTot on FinQtr.Refnum = FinancialTot.Refnum
		Where FeedProdID = 'ByProdHVChem' and FinQtr.Refnum = @Refnum) /@kUEDC,
	MfgC2UEDC = (Select RMC-AnnTotalBPC+TotCash From FinQtr Join FinancialTot on FinQtr.Refnum = FinancialTot.Refnum
		Where FeedProdID = 'ByProdEthylene' and FinQtr.Refnum = @Refnum) /@kUEDC,	
	TAAdjMfgC2C3UEDC = (Select ((RMC-AnnTotalBPC)*AdjFactor)+ ((VarExpAmt*AdjFactor)+FixExpAmt+AnnTAExp)From FinQtr Join FinancialTot ON 
		FinQtr.Refnum = FinancialTot.Refnum	JOIN TAAdj ON FinQtr.Refnum = TAAdj.Refnum 
		Where FeedProdID = 'ByProdOlefin' and FinQtr.Refnum = @Refnum) /@TAAdjkUEDC,
	TAAdjMfgC2UEDC = (Select ((RMC-AnnTotalBPC)*AdjFactor)+ ((VarExpAmt*AdjFactor)+FixExpAmt+AnnTAExp)From FinQtr Join FinancialTot ON 
		FinQtr.Refnum = FinancialTot.Refnum	JOIN TAAdj ON FinQtr.Refnum = TAAdj.Refnum
		Where FeedProdID = 'ByProdEthylene' and FinQtr.Refnum = @Refnum )/@TAAdjkUEDC,
	TAAdjMfgHVCUEDC = (Select ((RMC-AnnTotalBPC)*AdjFactor)+ ((VarExpAmt*AdjFactor)+FixExpAmt+AnnTAExp)From FinQtr Join FinancialTot ON 
		FinQtr.Refnum = FinancialTot.Refnum	JOIN TAAdj ON FinQtr.Refnum = TAAdj.Refnum
		Where FeedProdID = 'ByProdHVChem' and FinQtr.Refnum = @Refnum )/@TAAdjkUEDC,
	MfgProformaC2EDC = (select ((RMC-AnnTotalBPC)/UnadjC2Util)+ ((VarExpAmt/UnadjC2Util)+FixExpAmt+AnnTAExp)From FinQtr Join FinancialTot ON 
		FinQtr.Refnum = FinancialTot.Refnum	JOIN CapUtil ON FinQtr.Refnum = CapUtil.Refnum
		Where FeedProdID = 'ByProdEthylene' and FinQtr.Refnum = @Refnum )/@kEDC,
	MfgProformaHVCEDC = (select ((RMC-AnnTotalBPC)/UnadjC2Util)+ ((VarExpAmt/UnadjC2Util)+FixExpAmt+AnnTAExp) From FinQtr Join FinancialTot ON 
		FinQtr.Refnum = FinancialTot.Refnum	JOIN CapUtil ON FinQtr.Refnum = CapUtil.Refnum
	Where FeedProdID = 'ByProdHVChem' and FinQtr.Refnum = @Refnum )/@kEDC
	
	Where ROI.Refnum = @Refnum
	
	

		



