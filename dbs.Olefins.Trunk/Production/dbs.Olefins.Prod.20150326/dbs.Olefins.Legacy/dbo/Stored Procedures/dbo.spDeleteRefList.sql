﻿
/****** Object:  Stored Procedure dbo.spDeleteRefList    Script Date: 4/18/2003 4:32:54 PM ******/

CREATE PROCEDURE spDeleteRefList @ListName RefListName 
AS
DECLARE @Access smallint
EXEC @Access = spCheckListOwner @ListName
IF @Access > 0
BEGIN
	DELETE FROM RefList
	WHERE RefListNo = (SELECT RefListNo
				FROM RefList_LU
				WHERE ListName = @ListName)
	DELETE FROM RefList_LU
	WHERE ListName = @ListName
END
ELSE
	RETURN @Access


