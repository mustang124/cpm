﻿CREATE PROCEDURE [dbo].[Update_StreamQuantity]
(
	@SubmissionId			INT,
	@StreamNumber			INT,

	@StreamTypeId			INT		= NULL,
	@Quantity_kMT			FLOAT	= NULL
)
AS
BEGIN

	IF EXISTS (SELECT 1 FROM [stage].[StreamQuantity] t WHERE t.[SubmissionId] = @SubmissionId AND t.[StreamNumber] = @StreamNumber)
	BEGIN
		SET @Quantity_kMT = COALESCE(@Quantity_kMT, 0.0);
		EXECUTE [stage].[Update_StreamQuantity] @SubmissionId, @StreamNumber, @StreamTypeId, @Quantity_kMT;
	END;
	ELSE
	BEGIN
		IF(@Quantity_kMT >= 0.0)
		EXECUTE [stage].[Insert_StreamQuantity] @SubmissionId, @StreamNumber, @StreamTypeId, @Quantity_kMT;
	END;

END;
