﻿


CREATE  PROCEDURE spUpdateReportSets
	@ReportSetID DD_ID = -2 OUT, @ReportSetName varchar(30), 
	@Description varchar(100) = NULL, @Owner varchar(5) = NULL, 
	@JobNo varchar(10) = NULL, 
	@FileName varchar(255), @Replace bit = 0
AS
DECLARE @Result integer, @ErrMsg varchar(255)
IF EXISTS (SELECT * FROM ReportSets WHERE ReportSetName = @ReportSetName) 
	BEGIN
		IF NOT EXISTS (SELECT * FROM ReportSets 
				WHERE ReportSetName = @ReportSetName
				AND ReportSetID = @ReportSetID)
		BEGIN
			IF @Replace = 0
			BEGIN
				SELECT @ErrMsg = @ReportSetName + ' already exists.'
				RAISERROR(@ErrMsg, 11, -1)
			END
			ELSE BEGIN
				EXECUTE @Result = spCheckReportSetOwner NULL, @ReportSetName, NULL
				IF @Result >= 0
				BEGIN
					UPDATE ReportSets
					SET Description = @Description, Owner = @Owner,
					    JobNo = @JobNo, FileName = @FileName, CreateDate = Default
					WHERE ReportSetName = @ReportSetName
					SELECT @ReportSetID = ReportSetID FROM ReportSets
					WHERE ReportSetName = @ReportSetName
				END
				ELSE BEGIN
					SELECT @ErrMsg = 'Insufficient rights to replace '+@ReportSetName
					RAISERROR(@ErrMsg, 11, -1)
				END
			END
		END
		ELSE BEGIN
			EXECUTE @Result = spCheckReportSetOwner NULL, @ReportSetName, NULL
			IF @Result >= 0
				UPDATE ReportSets
				SET Description = @Description, Owner = @Owner,
				    JobNo = @JobNo, FileName = @FileName, CreateDate = Default
				WHERE ReportSetID = @ReportSetID
			ELSE BEGIN
				SELECT @ErrMsg = 'Insufficient rights to modify ' + @ReportSetName
				RAISERROR(@ErrMsg, 11, -1)
			END
		END
	END
ELSE BEGIN
	IF EXISTS (SELECT * FROM ReportSets WHERE ReportSetID = @ReportSetID)
	BEGIN
		EXECUTE @Result = spCheckReportSetOwner @ReportSetID, NULL, NULL
		IF @Result >= 0
			UPDATE ReportSets
			SET ReportSetName = @ReportSetName, Description = @Description,
			    Owner = @Owner, JobNo = @JobNo, 
			    FileName = @FileName, CreateDate = Default
			WHERE ReportSetID = @ReportSetID
		ELSE BEGIN
			SELECT @ErrMsg = 'Insufficient rights to modify ReportSet '+
					CONVERT(varchar(20), @ReportSetID)
			RAISERROR(@ErrMsg, 11, -1)
		END
	END
	ELSE BEGIN
		INSERT INTO ReportSets (ReportSetName, Description, Owner, 
					JobNo, FileName, CreateDate)
		VALUES (@ReportSetName, @Description, @Owner, @JobNo, @FileName, DEFAULT)
		SELECT @ReportSetID = ReportSetID 
		FROM ReportSets 
		WHERE ReportSetName = @ReportSetName
	END
END

