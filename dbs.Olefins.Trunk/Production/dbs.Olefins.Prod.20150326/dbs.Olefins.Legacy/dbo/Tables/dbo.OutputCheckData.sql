﻿CREATE TABLE [dbo].[OutputCheckData] (
    [TableID]    INT           NOT NULL,
    [RowTag]     VARCHAR (50)  NOT NULL,
    [ColTag]     VARCHAR (50)  NOT NULL,
    [RowNo]      INT           NOT NULL,
    [ColNo]      SMALLINT      NOT NULL,
    [CellValue]  REAL          NULL,
    [CellText]   VARCHAR (255) NULL,
    [UploadTime] SMALLDATETIME CONSTRAINT [DF_OutputCheckData_UploadTime] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OutputCheckData] PRIMARY KEY CLUSTERED ([TableID] ASC, [RowTag] ASC, [ColTag] ASC),
    CONSTRAINT [FK_OutputCheckData_OutputCheckTableIDs] FOREIGN KEY ([TableID]) REFERENCES [dbo].[OutputCheckTableIDs] ([TableID]) ON DELETE CASCADE ON UPDATE CASCADE
);

