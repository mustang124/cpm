﻿CREATE TABLE [dbo].[CalcQueue] (
    [ListName]     VARCHAR (30)  NOT NULL,
    [Calculate]    BIT           CONSTRAINT [DF_CalcQueue_Calculate_2__22] DEFAULT (1) NOT NULL,
    [Compare]      BIT           CONSTRAINT [DF_CalcQueue_Compare_4__22] DEFAULT (1) NOT NULL,
    [Copy]         BIT           CONSTRAINT [DF_CalcQueue_Copy_5__22] DEFAULT (1) NOT NULL,
    [ClientTables] BIT           CONSTRAINT [DF_CalcQueue_ClientTables3__22] DEFAULT (1) NOT NULL,
    [CalcSummary]  BIT           CONSTRAINT [DF_CalcQueue_CalcSummary_1__22] DEFAULT (1) NOT NULL,
    [MessageLog]   BIT           CONSTRAINT [DF_CalcQueue_MessageLog_7__22] DEFAULT (1) NOT NULL,
    [EmailNotify]  BIT           CONSTRAINT [DF_CalcQueue_EmailNotify_6__22] DEFAULT (1) NOT NULL,
    [EMailMessage] VARCHAR (255) NULL,
    CONSTRAINT [PK_CalcQueue_8__22] PRIMARY KEY NONCLUSTERED ([ListName] ASC)
);

