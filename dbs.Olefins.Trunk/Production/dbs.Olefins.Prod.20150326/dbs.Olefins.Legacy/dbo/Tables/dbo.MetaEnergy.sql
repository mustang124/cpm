﻿CREATE TABLE [dbo].[MetaEnergy] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [EnergyType] CHAR (12)      NOT NULL,
    [Qtr1]       REAL           NULL,
    [Qtr2]       REAL           NULL,
    [Qtr3]       REAL           NULL,
    [Qtr4]       REAL           NULL,
    [Total]      REAL           NULL,
    CONSTRAINT [PK_MetaEnergy] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EnergyType] ASC)
);

