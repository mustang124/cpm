﻿CREATE TABLE [dbo].[RefList_LU] (
    [RefListNo]   INT           IDENTITY (1, 1) NOT NULL,
    [ListName]    VARCHAR (30)  NOT NULL,
    [CreateDate]  SMALLDATETIME CONSTRAINT [DF_RefineryLi_CreateDate14__13] DEFAULT (getdate()) NOT NULL,
    [Owner]       VARCHAR (5)   NULL,
    [JobNo]       VARCHAR (10)  NULL,
    [Description] VARCHAR (100) NULL,
    CONSTRAINT [PK_RefList_LU] PRIMARY KEY CLUSTERED ([RefListNo] ASC),
    CONSTRAINT [UniqueRefListName] UNIQUE NONCLUSTERED ([ListName] ASC)
);


GO

CREATE TRIGGER [dbo].[RefListLUModify] ON [dbo].[RefList_LU] 
FOR UPDATE,DELETE 
AS
DECLARE @ListName varchar(30), @UName varchar(30), @UGroup varchar(30)
SELECT @UName = User_Name()

IF NOT EXISTS (SELECT * FROM deleted
	WHERE Owner <> @UName AND Owner IS NOT NULL)
OR
	@UName = 'dbo'
OR
	EXISTS (SELECT * FROM sys.sysusers u INNER JOIN sys.sysmembers m ON m.memberuid = u.uid INNER JOIN sys.sysusers g ON g.uid = m.groupuid 
				WHERE u.name = @UName AND g.name in ('developer', 'Datagrp', 'db_owner'))
	
	DELETE FROM RefList
	WHERE RefListNo IN
		(SELECT DISTINCT RefListNo FROM deleted
		WHERE RefListNo NOT IN
			(SELECT DISTINCT RefListNo FROM inserted))
ELSE
BEGIN
	SELECT @ListName = ListName FROM deleted
	WHERE Owner <> @UName AND Owner IS NOT NULL
 
	RAISERROR ('Insufficient rights to modify the list %s.  Transaction rolled back.',
		16, -1, @ListName)
	ROLLBACK TRANSACTION
END