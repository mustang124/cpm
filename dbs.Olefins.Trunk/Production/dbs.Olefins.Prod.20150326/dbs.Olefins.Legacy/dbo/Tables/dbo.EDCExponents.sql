﻿CREATE TABLE [dbo].[EDCExponents] (
    [FactorSet]           SMALLINT NOT NULL,
    [GasCompBHP]          REAL     NOT NULL,
    [MethCompBHP]         REAL     NOT NULL,
    [EthyCompBHP]         REAL     NOT NULL,
    [PropCompBHP]         REAL     NOT NULL,
    [FurnaceCapKMTA]      REAL     NOT NULL,
    [SpareFurnaceCapKMTA] REAL     NOT NULL,
    CONSTRAINT [PK_EDCExponents] PRIMARY KEY CLUSTERED ([FactorSet] ASC)
);

