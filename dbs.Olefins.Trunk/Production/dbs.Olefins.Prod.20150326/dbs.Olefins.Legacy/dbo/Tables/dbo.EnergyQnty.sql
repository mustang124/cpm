﻿CREATE TABLE [dbo].[EnergyQnty] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [EnergyType]       CHAR (12)      NOT NULL,
    [Amount]           REAL           NULL,
    [PSIG]             REAL           NULL,
    [MBTU]             REAL           NULL,
    [Cost]             REAL           NULL,
    [YN]               VARCHAR (3)    NULL,
    [UnitCnt]          INT            NULL,
    [Efficiency]       REAL           NULL,
    [PurchaseSteamPct] REAL           NULL,
    [ElecPowerPct]     REAL           NULL,
    [ThermalPct]       REAL           NULL,
    [OthDesc]          VARCHAR (100)  NULL,
    [Temperature]      REAL           NULL,
    [Methane_WtPcnt]   REAL           NULL,
    CONSTRAINT [PK___2__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EnergyType] ASC)
);

