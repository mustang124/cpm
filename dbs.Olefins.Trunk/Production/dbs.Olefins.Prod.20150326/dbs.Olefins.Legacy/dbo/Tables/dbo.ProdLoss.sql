﻿CREATE TABLE [dbo].[ProdLoss] (
    [Refnum]      [dbo].[Refnum] NOT NULL,
    [Category]    CHAR (3)       NOT NULL,
    [CauseID]     VARCHAR (20)   NOT NULL,
    [Description] VARCHAR (250)  NULL,
    [PrevDTLoss]  REAL           NULL,
    [PrevSDLoss]  REAL           NULL,
    [PrevTotLoss] REAL           NULL,
    [DTLoss]      REAL           NULL,
    [SDLoss]      REAL           NULL,
    [TotLoss]     REAL           NULL,
    [AnnDTLoss]   REAL           NULL,
    [AnnSDLoss]   REAL           NULL,
    [AnnTotLoss]  REAL           NULL,
    CONSTRAINT [PK___3__19] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Category] ASC, [CauseID] ASC)
);


GO
/****** Object:  Trigger dbo.UpdateProdLoss    Script Date: 4/18/2003 4:32:55 PM ******/

/****** Object:  Trigger dbo.UpdateProdLoss    Script Date: 12/28/2001 7:34:25 AM ******/
CREATE TRIGGER UpdateProdLoss ON dbo.ProdLoss 
FOR INSERT,UPDATE
AS

UPDATE ProdLoss
SET TotLoss = ISNULL(i.DTLoss, 0) + ISNULL(i.SDLoss, 0)
FROM ProdLoss INNER JOIN inserted i 
ON ProdLoss.Refnum = i.Refnum 
AND ProdLoss.Category = i.Category
AND ProdLoss.CauseID = i.CauseID
