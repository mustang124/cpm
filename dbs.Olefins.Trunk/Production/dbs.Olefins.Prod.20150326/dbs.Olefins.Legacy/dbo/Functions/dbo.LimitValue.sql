﻿CREATE FUNCTION [dbo].[LimitValue]

(

      @MinVal           REAL,

      @MaxVal           REAL,

      @ActVal           REAL

)

RETURNS REAL

WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT

AS 

BEGIN

 

      RETURN

      CASE

            WHEN @ActVal <= @MinVal THEN @MinVal

            WHEN @ActVal >= @MaxVal THEN @MaxVal

            ELSE @ActVal

      END;

 

END;

