﻿




/****** Object:  View dbo.PersSTCalc    Script Date: 4/18/2003 4:32:50 PM ******/
/****** Object:  View dbo.PersSTCalc    Script Date: 12/28/2001 7:34:24 AM ******/
CREATE VIEW [dbo].[PersSTCalc] AS 

SELECT Refnum, SectionID, SUM(CompEqP) AS CompEqP, SUM(ContEqP) AS ContEqP, SUM(TotEqP) AS TotEqP, 
SUM(CompWHr) AS CompWHr, SUM(ContWHr) AS ContWHr, SUM(TotWHr) AS TotWHr
FROM PersCalc
GROUP BY Refnum, SectionID
UNION
SELECT Refnum, 'Maint', SUM(CompEqP) AS CompEqP, SUM(ContEqP) AS ContEqP, SUM(TotEqP) AS TotEqP, 
SUM(CompWHr) AS CompWHr, SUM(ContWHr) AS ContWHr, SUM(TotWHr) AS TotWHr
FROM PersCalc
WHERE (SectionID in ('OM', 'MM') or PersID in ('MPSTechRely', 'MPSTechInsp'))
GROUP BY Refnum
UNION
SELECT Refnum, 'NonMaint', SUM(CompEqP) AS CompEqP, SUM(ContEqP) AS ContEqP, SUM(TotEqP) AS TotEqP, 
SUM(CompWHr) AS CompWHr, SUM(ContWHr) AS ContWHr, SUM(TotWHr) AS TotWHr
FROM PersCalc
WHERE (SectionID NOT in ('OM', 'MM') and PersID NOT in ('MPSTechRely', 'MPSTechInsp'))
GROUP BY Refnum


