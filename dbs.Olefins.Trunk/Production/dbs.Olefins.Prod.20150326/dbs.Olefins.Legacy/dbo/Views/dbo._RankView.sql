﻿

/****** Object:  View dbo._RankView    Script Date: 4/18/2003 4:32:51 PM ******/

/****** Object:  View dbo._RankView    Script Date: 12/28/2001 7:34:24 AM ******/
/****** Object:  View dbo._RankView    Script Date: 04/13/2000 7:55:23 AM ******/
CREATE  VIEW _RankView AS
SELECT r.Refnum, ts.CoLoc, Variable = v.Description, r.Value,
r.Rank, s.DataCount, r.Percentile, r.Tile, s.NumTiles, 
l.ListName, BreakCondition = b.Description, s.BreakValue,
s.RankSpecsID, s.BreakID, s.RankVariableID, LastTime /*, v.VariableID, v.Scenario, s.RefListNo*/
FROM Rank r, RankSpecs_LU s, RankBreaks b, RankVariables v,
	RefList_LU l, TSort ts
WHERE r.RankSpecsID = s.RankSpecsID
and s.BreakID = b.BreakID 
and s.RankVariableID = v.RankVariableID
and s.RefListNo = l.RefListNo
and r.Refnum = ts.Refnum


