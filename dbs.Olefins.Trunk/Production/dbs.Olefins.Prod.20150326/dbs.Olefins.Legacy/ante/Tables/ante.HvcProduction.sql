﻿CREATE TABLE [ante].[HvcProduction] (
    [MethodologyId]     INT                NOT NULL,
    [StreamId]          INT                NOT NULL,
    [ComponentId]       INT                NOT NULL,
    [LowerLimit_WtPcnt] FLOAT (53)         NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_HvcProduction_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_HvcProduction_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_HvcProduction_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_HvcProduction_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_HvcProduction] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [StreamId] ASC, [ComponentId] ASC),
    CONSTRAINT [CR_HvcProduction_LowerLimit_WtPcnt_MaxIncl_100.0] CHECK ([LowerLimit_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_HvcProduction_LowerLimit_WtPcnt_MinIncl_0.0] CHECK ([LowerLimit_WtPcnt]>=(0.0)),
    CONSTRAINT [FK_HvcProduction_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_HvcProduction_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_HvcProduction_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [ante].[t_HvcProduction_u]
	ON [ante].[HvcProduction]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[HvcProduction]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[ante].[HvcProduction].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[ante].[HvcProduction].[StreamId]		= INSERTED.[StreamId]
		AND	[ante].[HvcProduction].[ComponentId]		= INSERTED.[ComponentId];

END;