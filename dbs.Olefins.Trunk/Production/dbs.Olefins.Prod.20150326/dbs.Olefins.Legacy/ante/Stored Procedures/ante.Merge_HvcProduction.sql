﻿CREATE PROCEDURE [ante].[Merge_HvcProduction]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[HvcProduction] AS Target
	USING
	(
		VALUES
			(@MethodologyId, dim.Return_StreamId('EthylenePG'),		dim.Return_ComponentId('C2H4'),		 0.0),
			(@MethodologyId, dim.Return_StreamId('EthyleneCG'),		dim.Return_ComponentId('C2H4'),		 0.0),
			(@MethodologyId, dim.Return_StreamId('PropylenePG'),	dim.Return_ComponentId('C3H6'),		 0.0),
			(@MethodologyId, dim.Return_StreamId('PropyleneCG'),	dim.Return_ComponentId('C3H6'),		 0.0),
			(@MethodologyId, dim.Return_StreamId('PropyleneRG'),	dim.Return_ComponentId('C3H6'),		 0.0),
			(@MethodologyId, dim.Return_StreamId('Hydrogen'),		dim.Return_ComponentId('H2'),		 0.0),

			(@MethodologyId, dim.Return_StreamId('Acetylene'),		dim.Return_ComponentId('AcetMAPD'),	 0.0),	-- Use Default 100%, no 
			(@MethodologyId, dim.Return_StreamId('Benzene'),		dim.Return_ComponentId('C3H6'),		 0.0),	-- Use Default 100%, no 
			(@MethodologyId, dim.Return_StreamId('Butadiene'),		dim.Return_ComponentId('C3H6'),		 0.0),	-- Use Default 100%, no 

			(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('H2'),		85.0),
			(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('AcetMAPD'),	25.0),
			(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C2H4'),		25.0),
			(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C3H6'),		25.0),
			(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C4H6'),		32.0),
			(@MethodologyId, dim.Return_StreamId('ProdOther'),		dim.Return_ComponentId('C6H6'),		30.0)
	)
	AS Source([MethodologyId], [StreamId], [ComponentId], [LowerLimit_WtPcnt])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StreamId]		= Source.[StreamId]
	AND	Target.[ComponentId]		= Source.[ComponentId]
	WHEN MATCHED THEN UPDATE SET
		Target.[LowerLimit_WtPcnt]	= Source.[LowerLimit_WtPcnt]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StreamId], [ComponentId], [LowerLimit_WtPcnt])
		VALUES([MethodologyId], [StreamId], [ComponentId], [LowerLimit_WtPcnt])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;