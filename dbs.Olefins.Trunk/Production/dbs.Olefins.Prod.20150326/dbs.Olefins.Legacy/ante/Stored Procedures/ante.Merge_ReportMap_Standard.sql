﻿CREATE PROCEDURE [ante].[Merge_ReportMap_Standard]
AS
BEGIN

	SET NOCOUNT ON;
	PRINT 'Executing [' + OBJECT_SCHEMA_NAME(@@ProcId) + '].[' + OBJECT_NAME(@@ProcId) + ']...';

	DECLARE @MethodologyId INT = [ante].[Return_MethodologyId]('2013');

	MERGE INTO [ante].[ReportMap_Standard] AS Target
	USING
	(
		VALUES
		(@MethodologyId, dim.Return_StandardId('kEdc'),			'TotalkEDC'),
		(@MethodologyId, dim.Return_StandardId('StdEnergy'),	'EIIStdEnergy'),
		(@MethodologyId, dim.Return_StandardId('PersMaint'),	'PESMaintenance'),
		(@MethodologyId, dim.Return_StandardId('PersNonMaint'),	'PESNonMaintenance'),
		(@MethodologyId, dim.Return_StandardId('Pers'),			'PESTotal'),
		(@MethodologyId, dim.Return_StandardId('Mes'),			'MES'),
		(@MethodologyId, dim.Return_StandardId('NonEnergy'),	'NonEnergyCES')
	)
	AS Source([MethodologyId], [StandardId], [Report_Prefix])
	ON	Target.[MethodologyId]	= Source.[MethodologyId]
	AND	Target.[StandardId]		= Source.[StandardId]
	WHEN MATCHED THEN UPDATE SET
		Target.[Report_Prefix]	= Source.[Report_Prefix]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MethodologyId], [StandardId], [Report_Prefix])
		VALUES([MethodologyId], [StandardId], [Report_Prefix])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;

END;