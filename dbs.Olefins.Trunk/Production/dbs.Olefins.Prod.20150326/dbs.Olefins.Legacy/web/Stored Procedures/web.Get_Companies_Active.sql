﻿CREATE PROCEDURE [web].[Get_Companies_Active]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		c.[CompanyId],
		c.[CompanyName]
	FROM [auth].[Get_Companies_Active]()	c
	ORDER BY c.CompanyName ASC;

END;