﻿CREATE TABLE [audit].[Log_Calc] (
    [LogChanges_Bit]    BIT                CONSTRAINT [DF_Log_Calc_LogChanges] DEFAULT ((1)) NOT NULL,
    [TruncateData_Days] INT                CONSTRAINT [DF_Log_Calc_TruncateData] DEFAULT ((-1)) NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Log_Calc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_Log_Calc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_Log_Calc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_Log_Calc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Log_Calc] PRIMARY KEY CLUSTERED ([LogChanges_Bit] DESC)
);


GO

CREATE TRIGGER [audit].[Log_Calc_LogChanges]
ON [audit].[Log_Calc]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [audit].[Log_Calc]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE [audit].[Log_Calc].[LogChanges_Bit]	= INSERTED.[LogChanges_Bit];

END;
