﻿CREATE FUNCTION [fact].[Get_FacilitiesFractionator]
(
	@SubmissionId			INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		s.[SubmissionId],
		s.[FacilityId],
		s.[Quantity_kBSD],
		s.[StreamId],
		s.[Throughput_kMT],
		s.[Density_SG]
	FROM [fact].[FacilitiesFractionator]		s
	WHERE	s.[SubmissionId] = @SubmissionId
);