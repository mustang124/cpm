﻿CREATE PROCEDURE [fact].[Insert_StreamDescription]
(
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO [fact].[StreamDescription]([SubmissionId], [StreamNumber], [StreamDescription])
	SELECT
		a.[SubmissionId],
		a.[StreamNumber],
		a.[StreamDescription]
	FROM [stage].[StreamDescription]		a
	INNER JOIN [stage].[StreamQuantity]		q
		ON	q.[SubmissionId]		= a.[SubmissionId]
		AND	q.[StreamNumber]		= a.[StreamNumber]
		AND	q.[Quantity_kMT]		> 0
	WHERE	a.[SubmissionId]		= @SubmissionId
		AND a.[StreamDescription]	IS NOT NULL;

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;