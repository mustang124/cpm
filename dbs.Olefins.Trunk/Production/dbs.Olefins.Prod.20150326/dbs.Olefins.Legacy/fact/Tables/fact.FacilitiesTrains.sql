﻿CREATE TABLE [fact].[FacilitiesTrains] (
    [SubmissionId]   INT                NOT NULL,
    [Train_Count]    INT                NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Trains_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_Trains_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_Trains_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_Trains_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Trains] PRIMARY KEY CLUSTERED ([SubmissionId] DESC),
    CONSTRAINT [CR_Trains_Train_Count_MinIncl_0] CHECK ([Train_Count]>=(0)),
    CONSTRAINT [FK_Trains_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO

CREATE TRIGGER [fact].[t_Trains_u]
	ON [fact].[FacilitiesTrains]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[FacilitiesTrains]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[fact].[FacilitiesTrains].[SubmissionId]	= INSERTED.[SubmissionId];

END;