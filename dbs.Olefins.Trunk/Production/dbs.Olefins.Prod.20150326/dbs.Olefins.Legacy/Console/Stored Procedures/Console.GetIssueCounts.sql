﻿CREATE PROCEDURE [Console].[GetIssueCounts]
	@RefNum dbo.Refnum,
	@Mode varchar(1)

AS
BEGIN

	SELECT COUNT(IssueID) as Issues FROM ValStat 
	WHERE Refnum = dbo.FormatRefNum(@RefNum,0)
	AND completed = @Mode or @Mode='A'

END


