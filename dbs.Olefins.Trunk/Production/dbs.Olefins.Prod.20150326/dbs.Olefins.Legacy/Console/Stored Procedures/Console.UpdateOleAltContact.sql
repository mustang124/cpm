﻿CREATE PROCEDURE [Console].[UpdateOleAltContact] 
@StudyYear nvarchar(4),
	@RefNum dbo.Refnum,
	@AltFirstName nvarchar(80),
	@AltLastName nvarchar(80),
	@AltEmail nvarchar(80)
AS

BEGIN

DECLARE @ContactCode nvarchar(20)

	SELECT @ContactCode = ContactCode from TSort where Refnum=@Refnum

	IF EXISTS(SELECT ContactCode from CoContactInfo where ContactCode = @ContactCode)

		UPDATE C 
		SET
				c.AltFirstName=@AltFirstName ,
				c.AltLastName=@AltLastName ,
				c.AltEmail =@AltEmail
				
				
			 FROM CoContactInfo c join tsort t on t.ContactCode = c.contactcode
				WHERE t.Refnum=@RefNum and c.StudyYear=@StudyYear
				
	ELSE
	
		INSERT INTO CoContactInfo (ContactCode, StudyYear, ContactType, AltFirstName,AltLastName,AltEmail, CCAlt)
		VALUES (@ContactCode, @StudyYear, 'Alt', @AltFirstName,@AltLastName, @AltEmail, 'Y')
END
