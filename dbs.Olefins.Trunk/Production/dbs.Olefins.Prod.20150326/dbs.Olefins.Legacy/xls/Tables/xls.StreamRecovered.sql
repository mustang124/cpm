﻿CREATE TABLE [xls].[StreamRecovered] (
    [Refnum]             VARCHAR (12)       NOT NULL,
    [StreamNumber]       INT                NOT NULL,
    [StreamId]           INT                NOT NULL,
    [Quantity_Dur_kMT]   FLOAT (53)         NOT NULL,
    [Quantity_Ann_kMT]   FLOAT (53)         NOT NULL,
    [Recovered_WtPcnt]   FLOAT (53)         NOT NULL,
    [_Recovered_Dur_kMT] AS                 (CONVERT([float],([Quantity_Dur_kMT]*[Recovered_WtPcnt])/(100.0),0)) PERSISTED NOT NULL,
    [_Recovered_Ann_kMT] AS                 (CONVERT([float],([Quantity_Ann_kMT]*[Recovered_WtPcnt])/(100.0),0)) PERSISTED NOT NULL,
    [tsModified]         DATETIMEOFFSET (7) CONSTRAINT [DF_StreamRecovered_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]     NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]     NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]      NVARCHAR (128)     CONSTRAINT [DF_StreamRecovered_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]       ROWVERSION         NOT NULL,
    CONSTRAINT [PK_StreamRecovered] PRIMARY KEY CLUSTERED ([Refnum] DESC, [StreamNumber] ASC),
    CONSTRAINT [CL_StreamRecovered_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_StreamRecovered_Quantity_Ann_kMT_MinIncl_0.0] CHECK ([Quantity_Ann_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamRecovered_Quantity_Dur_kMT_MinIncl_0.0] CHECK ([Quantity_Dur_kMT]>=(0.0)),
    CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MaxIncl_100.0] CHECK ([Recovered_WtPcnt]<=(100.0)),
    CONSTRAINT [CR_StreamRecovered_Recovered_WtPcnt_MinIncl_0.0] CHECK ([Recovered_WtPcnt]>=(0.0)),
    CONSTRAINT [FK_StreamRecovered_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [xls].[t_StreamRecovered_u]
	ON [xls].[StreamRecovered]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[StreamRecovered]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[StreamRecovered].[Refnum]		= INSERTED.[Refnum]
		AND	[xls].[StreamRecovered].[StreamNumber]	= INSERTED.[StreamNumber];

END;