﻿CREATE TABLE [xls].[FacilitiesHydroTreater] (
    [Refnum]         VARCHAR (12)       NOT NULL,
    [FacilityId]     INT                NOT NULL,
    [Quantity_BSD]   FLOAT (53)         NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilitiesHydroTreater_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (128)     CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (128)     CONSTRAINT [DF_FacilitiesHydroTreater_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilitiesHydroTreater] PRIMARY KEY CLUSTERED ([Refnum] DESC),
    CONSTRAINT [CL_FacilitiesHydroTreater_Refnum] CHECK ([Refnum]<>''),
    CONSTRAINT [CR_FacilitiesHydroTreater_Quantity_BSD_MinIncl_0.0] CHECK ([Quantity_BSD]>=(0.0)),
    CONSTRAINT [FK_FacilitiesHydroTreater_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId])
);


GO

CREATE TRIGGER [xls].[t_FacilitiesHydroTreater_u]
	ON [xls].[FacilitiesHydroTreater]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [xls].[FacilitiesHydroTreater]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[xls].[FacilitiesHydroTreater].[Refnum]	= INSERTED.[Refnum];

END;