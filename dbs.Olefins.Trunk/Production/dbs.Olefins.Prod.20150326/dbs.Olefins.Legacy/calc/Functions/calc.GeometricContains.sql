﻿CREATE FUNCTION [calc].[GeometricContains]
(
	@Value	INT, 
	@Total	INT
)
RETURNS BIT
WITH SCHEMABINDING
AS
BEGIN
	
	DECLARE @Return BIT;
	DECLARE @Base INT = 2;

	IF (@Value > 0)
		IF (@Total % (@Base * @Value)) >= @Value SET @Return = 1 ELSE SET @Return = 0;
	ELSE
		IF ((@Value = 0) AND (@Total = 0))
			SET @Return = 1;
		ELSE
			SET @Return = 0;

	RETURN @Return;

END;