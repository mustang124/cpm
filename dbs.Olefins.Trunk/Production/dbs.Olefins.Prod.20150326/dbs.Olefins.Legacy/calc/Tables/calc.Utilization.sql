﻿CREATE TABLE [calc].[Utilization] (
    [MethodologyId]     INT                NOT NULL,
    [SubmissionId]      INT                NOT NULL,
    [StreamId]          INT                NOT NULL,
    [ComponentId]       INT                NOT NULL,
    [Duration_Days]     FLOAT (53)         NOT NULL,
    [StreamDay_MTSD]    FLOAT (53)         NOT NULL,
    [Component_kMT]     FLOAT (53)         NOT NULL,
    [_Utilization_Pcnt] AS                 (CONVERT([float],(([Component_kMT]/[StreamDay_MTSD])/[Duration_Days])*(100000.0),0)) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Utilization_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (128)     CONSTRAINT [DF_Utilization_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (128)     CONSTRAINT [DF_Utilization_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (128)     CONSTRAINT [DF_Utilization_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]      ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Utilization] PRIMARY KEY CLUSTERED ([MethodologyId] DESC, [SubmissionId] DESC, [ComponentId] ASC),
    CONSTRAINT [CR_Utilization_Component_kMT_MinIncl_0.0] CHECK ([Component_kMT]>=(0.0)),
    CONSTRAINT [CR_Utilization_Duration_Days_MinIncl_0.0] CHECK ([Duration_Days]>=(0.0)),
    CONSTRAINT [CR_Utilization_StreamDay_MTSD_MinIncl_0.0] CHECK ([StreamDay_MTSD]>=(0.0)),
    CONSTRAINT [CR_Utilization_Utilization_Pcnt_MinIncl_0.0] CHECK ([_Utilization_Pcnt]>=(0.0)),
    CONSTRAINT [FK_Utilization_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_Utilization_Methodology] FOREIGN KEY ([MethodologyId]) REFERENCES [ante].[Methodology] ([MethodologyId]),
    CONSTRAINT [FK_Utilization_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_Utilization_Submissions] FOREIGN KEY ([SubmissionId]) REFERENCES [fact].[Submissions] ([SubmissionId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Utilization]
    ON [calc].[Utilization]([MethodologyId] DESC, [SubmissionId] DESC, [StreamId] ASC);


GO

CREATE TRIGGER [calc].[t_Utilization_u]
	ON [calc].[Utilization]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [calc].[Utilization]
	SET	[tsModified]		= sysdatetimeoffset(),
		[tsModifiedHost]	= host_name(),
		[tsModifiedUser]	= suser_sname(),
		[tsModifiedApp]		= app_name()
	FROM INSERTED
	WHERE	[calc].[Utilization].[MethodologyId]	= INSERTED.[MethodologyId]
		AND	[calc].[Utilization].[SubmissionId]		= INSERTED.[SubmissionId]
		AND	[calc].[Utilization].[ComponentId]		= INSERTED.[ComponentId];

END;