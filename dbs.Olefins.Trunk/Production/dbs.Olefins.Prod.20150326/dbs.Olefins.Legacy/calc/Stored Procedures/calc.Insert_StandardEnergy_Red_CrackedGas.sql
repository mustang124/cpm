﻿CREATE PROCEDURE [calc].[Insert_StandardEnergy_Red_CrackedGas]
(
	@MethodologyId			INT,
	@SubmissionId			INT
)
AS
BEGIN

SET NOCOUNT ON;

BEGIN TRY

	DECLARE @OlefinMinLimit_WtPcnt	FLOAT = 20.0;

	DECLARE @ProdOther				INT = [dim].[Return_StreamId]('ProdOther');
	DECLARE @C2H4					INT = [dim].[Return_ComponentId]('C2H4');
	DECLARE @C3H6					INT = [dim].[Return_ComponentId]('C3H6');
	
	DECLARE @C2H2					INT = [dim].[Return_ComponentId]('C2H2');
	DECLARE @PyroGasOil				INT = [dim].[Return_ComponentId]('PyroGasOil');
	DECLARE @PyroFuelOil			INT = [dim].[Return_ComponentId]('PyroFuelOil');
	DECLARE @Inerts					INT = [dim].[Return_ComponentId]('Inerts');

	INSERT INTO [calc].[StandardEnergy_Reduction]([MethodologyId], [SubmissionId], [ProcessUnitId], [StreamId], [Quantity_kMT], [Quantity_bbl], [StandardEnergy_MBtu], [StandardEnergy_MBtuDay])
	SELECT
		c.[MethodologyId],
		c.[SubmissionId],
		m.[ProcessUnitId],
		c.[StreamId],
		SUM(c.[Component_Ann_kMT]),
		SUM(c.[Component_Ann_kMT]) * 2.2046,
		SUM(c.[Component_Ann_kMT]  * 2.2046 * e.[StandardEnergy_BtuLb]),
		SUM(c.[Component_Ann_kMT]  * 2.2046 * e.[StandardEnergy_BtuLb]) / s.[_Duration_Days]
	FROM [calc].[StreamComposition]					c
	INNER JOIN [ante].[ComponentEnergy]				e
		ON	e.[MethodologyId]	= c.[MethodologyId]
		AND	e.[ComponentId]		= c.[ComponentId]
	INNER JOIN [fact].[Submissions]					s
		ON	s.[SubmissionId]	= c.[SubmissionId]
	INNER JOIN [ante].[MapStreamProcessUnit]		m
		ON	m.[MethodologyId]	= c.[MethodologyId]
		AND	m.[StreamId]		= c.[StreamId]
	INNER JOIN (
		SELECT
			c.[MethodologyId],
			c.[SubmissionId],
			c.[StreamNumber]
		FROM [calc].[StreamComposition]		c
		WHERE	c.[MethodologyId] = @MethodologyId
			AND	c.[SubmissionId] = @SubmissionId
			AND	c.[StreamId] = @ProdOther
			AND c.[ComponentId] IN (@C2H4, @C3H6)
		GROUP BY
			c.[MethodologyId],
			c.[SubmissionId],
			c.[StreamNumber]
		HAVING SUM(c.[Component_WtPcnt]) > @OlefinMinLimit_WtPcnt
		) lim
		ON	lim.[MethodologyId]	= c.[MethodologyId] 
		AND lim.[SubmissionId]	= c.[SubmissionId] 
		AND lim.[StreamNumber]	= c.[StreamNumber] 
	WHERE	c.[MethodologyId]	= @MethodologyId
		AND	c.[SubmissionId]	= @SubmissionId
		AND	c.[StreamId]		= @ProdOther
		AND	c.[ComponentId]		NOT IN (@C2H2, @PyroGasOil, @PyroFuelOil, @Inerts)
		AND	s.[_Duration_Days]	<> 0.0
	GROUP BY
		c.[MethodologyId],
		c.[SubmissionId],
		m.[ProcessUnitId],
		c.[StreamId],
		s.[_Duration_Days];

END TRY
BEGIN CATCH

	DECLARE @XACT_STATE		SMALLINT		= XACT_STATE();
	DECLARE @Parameters		VARCHAR(4000)	=
						('@MethodologyId:'	+ CONVERT(VARCHAR, @MethodologyId))
					+ (', @SubmissionId:'	+ CONVERT(VARCHAR, @SubmissionId));

	EXECUTE [audit].[Insert_LogError] @@PROCID, @Parameters, @XACT_STATE;

	RETURN - ERROR_NUMBER();

END CATCH;

END;