﻿CREATE FUNCTION [auth].[Get_SubmissionsByPlant_Active]
(
	@PlantId		INT
)
RETURNS TABLE
AS
RETURN
(
	SELECT
		sbp.[JoinId],
		sbp.[PlantId],
		sbp.[SubmissionId],
		sbp.[PlantName],
		sbp.[SubmissionName],
		[SubmissionName] + N' (' + CONVERT(NVARCHAR, [DateBeg], 106) + N' - ' + CONVERT(NVARCHAR, [DateEnd], 106) + ')' [SubmissionNameFull],
		sbp.[TimeEnd]
	FROM [auth].[SubmissionsByPlant_Active]	sbp WITH (NOEXPAND)
	WHERE	sbp.[PlantId]	= @PlantId
);