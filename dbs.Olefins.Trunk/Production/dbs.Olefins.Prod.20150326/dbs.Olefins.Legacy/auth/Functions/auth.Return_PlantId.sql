﻿CREATE FUNCTION [auth].[Return_PlantId]
(
	@PlantName	NVARCHAR(128)
)
RETURNS INT
AS
BEGIN

	DECLARE @Id	INT;

	SELECT
		@Id = p.[PlantId]
	FROM [auth].[Plants]	p
	WHERE	p.[PlantName] = @PlantName;

	RETURN @Id;

END;