﻿


CREATE VIEW [reports].[LTRanks] AS
Select
  GroupId = r.GroupId
, StudyYear = LT.StudyYear
, SourceDB = LT.SourceDB
, NumPlants = COUNT(*)
, HvcProd_kMT			= [$(DbGlobal)].dbo.WtAvg(lt.HvcProd_kMT, 1.0)
, MCI_Pcnt				= [$(DbGlobal)].dbo.WtAvg(lt.MCI_Pcnt, lt.HvcProd_kMT)
, RI_Pcnt				= [$(DbGlobal)].dbo.WtAvg(lt.RI_Pcnt, lt.HvcProd_kMT)
FROM reports.GroupPlants r JOIN dbo.LTRanks lt on lt.Refnum=r.Refnum
GROUP by r.GroupId, LT.StudyYear, LT.SourceDB

UNION

Select
  GroupId = lt.Refnum
 , StudyYear = LT.StudyYear
 , LT.SourceDB
 , NumPlants = 1
, HvcProd_kMT
, MCI_Pcnt
, RI_Pcnt
FROM dbo.LTRanks lt


