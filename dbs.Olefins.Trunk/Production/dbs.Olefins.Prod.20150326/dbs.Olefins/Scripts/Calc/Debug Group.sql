﻿USE Olefins13
GO

SET NOCOUNT ON;

DECLARE @GroupId		VARCHAR(25) = '13PCHc:BASFP09';
DECLARE @FactorSetID	VARCHAR(4)	= '2013';
DECLARE @Refnum			VARCHAR(25) = @GroupId;

DECLARE @ProcedureDesc	NVARCHAR(4000);

DECLARE @fpl	[calc].[FoundationPlantList];

		INSERT INTO @fpl
		(
			[FactorSetID],
			[FactorSetName],
			[FactorSet_AnnDateKey],
			[FactorSet_QtrDateKey],
			[FactorSet_QtrDate],
			[Refnum],
			[Plant_AnnDateKey],
			[Plant_QtrDateKey],
			[Plant_QtrDate],
			[CompanyID],
			[AssetID],
			[AssetName],
			[SubscriberCompanyName],
			[PlantCompanyName],
			[SubscriberAssetName],
			[PlantAssetName],
			[CountryID],
			[StateName],
			[EconRegionID],
			[UomID],
			[CurrencyFcn],
			[CurrencyRpt],
			[AssetPassWord_PlainText],
			[StudyYear],
			[DataYear],
			[Consultant],
			[StudyYearDifference],
			[CalQtr]
		)
		SELECT
			fpl.[FactorSetId],
			fpl.[FactorSetName],
			fpl.[FactorSet_AnnDateKey],
			fpl.[FactorSet_QtrDateKey],
			fpl.[FactorSet_QtrDate],
			fpl.[Refnum],
			fpl.[Plant_AnnDateKey],
			fpl.[Plant_QtrDateKey],
			fpl.[Plant_QtrDate],
			fpl.[CompanyId],
			fpl.[AssetIdPri],
			fpl.[AssetName],
			fpl.[SubscriberCompanyName],
			fpl.[PlantCompanyName],
			fpl.[SubscriberAssetName],
			fpl.[PlantAssetName],
			fpl.[CountryId],
			fpl.[StateName],
			fpl.[EconRegionID],
			fpl.[UomId],
			fpl.[CurrencyFcn],
			fpl.[CurrencyRpt],
			fpl.[AssetPassWord_PlainText],
			fpl.[StudyYear],
			fpl.[DataYear],
			fpl.[Consultant],
			fpl.[StudyYearDifference],
			fpl.[CalQtr]
		FROM [calc].[FoundationPlantListSource]						fpl
		WHERE	CHARINDEX(@GroupId, fpl.Refnum) >= 1
			AND	fpl.FactorSetId = ISNULL(@FactorSetId, fpl.FactorSetId);

