SELECT * FROM dim.Account_LookUp;
SELECT * FROM dim.Account_Parent;
SELECT * FROM dim.Account_Bridge;

SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.AccountId,
	l.AccountId,
	l.AccountName,
	l.AccountDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Account_Parent	p
INNER JOIN dim.Account_LookUp	l
	ON	l.AccountId = p.AccountId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;

SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.AccountId,
	l.AccountId,
	l.AccountName,
	l.AccountDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.AccountMaint_Parent	p
INNER JOIN dim.Account_LookUp	l
	ON	l.AccountId = p.AccountId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.ApcId,
	l.ApcId,
	l.ApcName,
	l.ApcDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Apc_Parent p
INNER JOIN dim.Apc_LookUp	l
	ON	l.ApcId = p.ApcId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.ComponentId,
	l.ComponentId,
	l.ComponentName,
	l.ComponentDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Component_Parent p
INNER JOIN dim.Component_LookUp	l
	ON	l.ComponentId = p.ComponentId
WHERE p.FactorSetId = '2011'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.EdcId,
	l.EdcId,
	l.EdcName,
	l.EdcDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Edc_Parent p
INNER JOIN dim.Edc_LookUp	l
	ON	l.EdcId = p.EdcId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.EmissionsId,
	l.EmissionsId,
	l.EmissionsName,
	l.EmissionsDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Emissions_Parent	p
INNER JOIN dim.Emissions_LookUp	l
	ON	l.EmissionsId = p.EmissionsId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.FacilityId,
	l.FacilityId,
	l.FacilityName,
	l.FacilityDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Facility_Parent p
INNER JOIN dim.Facility_LookUp	l
	ON	l.FacilityId = p.FacilityId
WHERE p.FactorSetId = '2011'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.[Hierarchy].[GetLevel]() - 0, 2)) + p.[GapId],
	l.[GapId],
	l.[GapName],
	l.[GapDetail],
	p.[Hierarchy].[GetLevel](),
	p.[Hierarchy].[ToString](),
	p.[Operator]
FROM [dim].[Gap_Parent] p
INNER JOIN [dim].[Gap_LookUp]	l
	ON	l.[GapId] = p.[GapId]
WHERE p.FactorSetId = '2013'
ORDER BY p.[Hierarchy];


SELECT * FROM dim.Gap_LookUp l



SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.MarginId,
	--l.MarginId,
	l.MarginName,
	l.MarginDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Margin_Basis_Parent	p
INNER JOIN dim.Margin_LookUp	l
	ON	l.MarginId = p.MarginId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.OppLossId,
	l.OppLossId,
	l.OppLossName,
	l.OppLossDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.ReliabilityOppLoss_Parent			p
INNER JOIN dim.ReliabilityOppLoss_LookUp	l
	ON	l.OppLossId = p.OppLossId
WHERE p.FactorSetId = '2011'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.OppLossId,
	l.OppLossId,
	l.OppLossName,
	l.OppLossDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.ReliabilityOppLoss_Availability_Parent			p
INNER JOIN dim.ReliabilityOppLoss_LookUp	l
	ON	l.OppLossId = p.OppLossId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.PersId,
	l.PersId,
	l.PersName,
	l.PersDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Pers_Parent p
INNER JOIN dim.Pers_LookUp	l
	ON	l.PersId = p.PersId
WHERE p.FactorSetId = '2011'
ORDER BY p.Hierarchy;


SELECT * FROM dim.Pers_LookUp;
SELECT * FROM dim.Pers_Parent p WHERE p.FactorSetId = '2013' ORDER BY p.PersId;
SELECT * FROM dim.Pers_Bridge p WHERE p.FactorSetId = '2013' ORDER BY p.PersId;;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.StandardEnergyId,
	l.StandardEnergyId,
	l.StandardEnergyName,
	l.StandardEnergyDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.StandardEnergy_Parent p
INNER JOIN dim.StandardEnergy_LookUp	l
	ON	l.StandardEnergyId = p.StandardEnergyId
WHERE p.FactorSetId = '2013'
ORDER BY p.Hierarchy;


SELECT
	REPLICATE(' ', POWER(p.Hierarchy.GetLevel() - 1, 2)) + p.StreamId,
	l.StreamId,
	l.StreamName,
	l.StreamDetail,
	p.Hierarchy.GetLevel(),
	p.Hierarchy.ToString(),
	p.Operator
FROM dim.Stream_Parent p
INNER JOIN dim.Stream_LookUp	l
	ON	l.StreamId = p.StreamId
WHERE p.FactorSetId = '2011'
ORDER BY p.Hierarchy;

----------------------------------------------------------------------------


SELECT * FROM dim.Countries ORDER BY CountryNameEn;


SELECT
	l.ComponentID,
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.ComponentID,
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.ComponentName,
	l.Operator,
	l.Hierarchy.ToString()
FROM dim.ComponentLu l
ORDER BY l.Hierarchy;

SELECT 
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.OppLossID,
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.OppLossName,
	l.Operator,
	l.Hierarchy.ToString()
FROM dim.ReliabilityOppLossLu l
ORDER BY l.Hierarchy;

SELECT 
	REPLICATE(CHAR(9), POWER(d.DescendantHierarchy.GetLevel()- 1, 2)) + d.DescendantID,
	d.Operator,
	d.DescendantHierarchy.ToString()
FROM dim.StreamLuDescendants('PPFC', 1, '') d
ORDER BY d.DescendantHierarchy 

SELECT 
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.ApcID,
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.ApcName,
	l.Operator,
	l.Hierarchy.ToString()
FROM dim.ApcLu l
ORDER BY l.Hierarchy


-- View items that appear in related lookup.
SELECT
	REPLICATE(CHAR(9), POWER(f.Hierarchy.GetLevel(), 2)) + f.FacilityName			[Facility],
	f.Hierarchy.ToString()														[Facility],
	REPLICATE(CHAR(9), POWER(e.Hierarchy.GetLevel(), 2)) + e.StandardEnergyName	[Standard Energy],
	e.Hierarchy.ToString()														[Standard Energy],
	e.Hierarchy.GetLevel() - f.Hierarchy.GetLevel()
FROM dim.FacilitiesLu					f
LEFT OUTER JOIN dim.StandardEnergyLu	e
	ON	e.StandardEnergyID = f.FacilityID
ORDER BY  f.Hierarchy ASC;

-- Validate Hierarchy in related lookup
SELECT
	e.StandardEnergyID,
	REPLICATE(CHAR(9), POWER(e.Hierarchy.GetLevel(), 2)) + e.StandardEnergyName	[Standard Energy],
	--e.Hierarchy.ToString()														[Standard Energy],
	REPLICATE(CHAR(9), POWER(f.Hierarchy.GetLevel(), 2)) + f.FacilityName			[Facility],
	--f.Hierarchy.ToString()														[Facility],
	f.Hierarchy.GetLevel() - e.Hierarchy.GetLevel()								[Facility],
	REPLICATE(CHAR(9), POWER(s.Hierarchy.GetLevel(), 2)) + s.StreamName			[Stream],
	--s.Hierarchy.ToString()														[Stream],
	s.Hierarchy.GetLevel() - e.Hierarchy.GetLevel()								[Stream]
FROM dim.StandardEnergyLu				e
LEFT OUTER JOIN dim.FacilitiesLu		f
	ON	f.FacilityID = e.StandardEnergyID
LEFT OUTER JOIN dim.StreamLu			s
	ON	s.StreamID = e.StandardEnergyID
ORDER BY  e.Hierarchy ASC;


SELECT
	e.StandardEnergyID,
	REPLICATE(CHAR(9), POWER(e.Hierarchy.GetLevel(), 2)) + e.StandardEnergyName	[Standard Energy]
FROM dim.StandardEnergyLu				e
ORDER BY  e.Hierarchy ASC;

SELECT
	e.EdcID,
	REPLICATE(CHAR(9), POWER(e.Hierarchy.GetLevel(), 2)) + e.EdcName	[EDC]
FROM dim.EdcLu				e
ORDER BY  e.Hierarchy ASC;

SELECT
	p.PersID,
	REPLICATE(CHAR(9), POWER(p.Hierarchy.GetLevel(), 2)) + p.PersName
FROM dim.PersLu				p
ORDER BY p.Hierarchy ASC;


SELECT
	l.ReplacementValueID,
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.ReplacementValueID,
	REPLICATE(CHAR(9), POWER(l.Hierarchy.GetLevel(), 2)) + l.ReplacementValueName,
	l.Operator,
	l.Hierarchy.ToString()
FROM dim.ReplacementValueLu l
ORDER BY l.Hierarchy;


SELECT
	f.MarginID,
	REPLICATE(CHAR(9), POWER(f.Hierarchy.GetLevel(), 2)) + f.MarginID			[ID],
	f.Hierarchy.GetLevel()														[Level],
	f.Hierarchy.ToString()														[Hierarchy]
FROM dim.Margin					f
ORDER BY  f.Hierarchy ASC;

SELECT
	l.EmissionsID,
	REPLICATE(' ', POWER(l.Hierarchy.GetLevel() - 1, 2)) + l.EmissionsID,
	l.Hierarchy.GetLevel(),
	l.Operator,
	l.Hierarchy.ToString()
FROM dim.EmissionsLu l
ORDER BY l.Hierarchy
