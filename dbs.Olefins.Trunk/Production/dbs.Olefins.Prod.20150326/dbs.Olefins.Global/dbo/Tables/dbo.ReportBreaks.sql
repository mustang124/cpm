﻿CREATE TABLE [dbo].[ReportBreaks] (
    [BreakName]  VARCHAR (30)  NOT NULL,
    [ObjectName] [sysname]     NOT NULL,
    [ColumnName] [sysname]     NOT NULL,
    [KeyFields]  VARCHAR (255) NULL,
    CONSTRAINT [PK_ReportBreaks_1__21] PRIMARY KEY CLUSTERED ([BreakName] ASC) WITH (FILLFACTOR = 90)
);

