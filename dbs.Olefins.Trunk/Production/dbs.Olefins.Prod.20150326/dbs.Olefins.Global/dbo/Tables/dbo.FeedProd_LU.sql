﻿CREATE TABLE [dbo].[FeedProd_LU] (
    [FeedProdID] VARCHAR (20) NOT NULL,
    [IDDesc]     VARCHAR (50) NULL,
    [Type]       CHAR (4)     NULL,
    [Category]   CHAR (6)     NULL,
    [C2Prod]     CHAR (6)     NULL,
    [OleProd]    CHAR (6)     NULL,
    [HVCProd]    CHAR (6)     NULL,
    [FeedType]   VARCHAR (15) NULL,
    CONSTRAINT [PK___5__10] PRIMARY KEY CLUSTERED ([FeedProdID] ASC)
);

