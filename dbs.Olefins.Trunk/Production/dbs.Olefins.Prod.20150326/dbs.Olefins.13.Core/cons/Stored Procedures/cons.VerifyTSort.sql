﻿CREATE PROCEDURE [cons].[VerifyTSort]
(
	@Refnum	VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		DECLARE @sRefnum	VARCHAR(9);
		SET @sRefnum = RIGHT(@Refnum, 8);

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO cons.Assets (cons.VerifyTSort)';
		PRINT @ProcedureDesc;

		DECLARE @a TABLE(
			[AssetId]					AS					([AssetIdPri]+isnull('-'+[AssetIdSec],'')) PERSISTED NOT NULL,
			[AssetIdPri]				VARCHAR (4)			NOT	NULL,
			[AssetIdSec]				VARCHAR (4)				NULL,
			[AssetName]					NVARCHAR (84)		NOT	NULL,
			[AssetDetail]				NVARCHAR (192)		NOT	NULL,
			[CountryId]					CHAR (3)			NOT	NULL,
			[StateName]					NVARCHAR (42)			NULL,
			PRIMARY KEY CLUSTERED ([AssetId] ASC)
		);

		INSERT INTO @a(AssetIdPri, AssetName, AssetDetail, CountryId, StateName) 
		SELECT DISTINCT
			etl.ConvAssetPCH(t.Refnum)					[AssetId],
			RTRIM(LTRIM(ISNULL(t.Loc, ISNULL(t.PlantName, ISNULL(t.PlantLoc, N'————')))))	[AssetName],
			RTRIM(LTRIM(ISNULL(t.Loc, ISNULL(t.PlantName, ISNULL(t.PlantLoc, N'————')))))	[AssetDetail],
			etl.ConvCountryID(t.Refnum, COALESCE(t.PlantCountry, t.Country))	[CountryId],
			etl.ConvStateName(t.[State])				[StateName]
		FROM stgFact.TSort t
		WHERE	t.Refnum = @sRefnum

		INSERT INTO @a(AssetIdPri, AssetIdSec, AssetName, AssetDetail, CountryId)
		SELECT DISTINCT
			etl.ConvAssetPCH(f.Refnum)				[AssetIdPri],
			'T' + CONVERT(VARCHAR(2), f.PlantId)	[AssetIdSec],
			MIN(f.Name),
			MIN(f.Name + ISNULL(' (' + f.City, '') + CASE WHEN LEN(f.[State]) >= 1 THEN ISNULL(', ' + f.[State], '') ELSE '' END + CASE WHEN LEN(f.City) >= 2 THEN ')' ELSE '' END),
			MIN(etl.ConvCountryID(f.Refnum, ISNULL(f.Country, t.PlantCountry)))
		FROM stgFact.PolyFacilities		f
		INNER JOIN stgFact.TSort		t	
			ON t.Refnum = f.Refnum
		LEFT OUTER JOIN cons.Assets		a
			ON	a.AssetIdPri = etl.ConvAssetPCH(f.Refnum)
			AND	a.AssetIdSec = 'T' + CONVERT(VARCHAR(2), f.PlantId)
		WHERE	f.Name <> '0'
			AND	a.AssetId IS NULL
			AND	f.Refnum = @sRefnum
		GROUP BY 
			etl.ConvAssetPCH(f.Refnum),
			'T' + CONVERT(VARCHAR(2), f.PlantId);

		INSERT INTO cons.Assets(AssetIdPri, AssetIdSec, AssetName, AssetDetail, CountryId, StateName)
		SELECT a.AssetIdPri, a.AssetIdSec, a.AssetName, a.AssetDetail, a.CountryId, a.StateName
		FROM @a	a
		LEFT OUTER JOIN cons.Assets c
			ON	c.AssetId = a.AssetId
		WHERE c.AssetId IS NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO cons.AssetsParents (cons.VerifyTSort)';
		PRINT @ProcedureDesc;

		BEGIN TRY
			INSERT INTO cons.AssetsParents(CalDateKey, AssetId, Parent, Operator, Hierarchy)
			SELECT
				etl.ConvDateKey(t.StudyYear),
				a.AssetId,
				p.AssetId,
				'+',
				'/'
			FROM stgFact.TSort	t
			CROSS JOIN @a		a
			CROSS JOIN @a		p
			LEFT OUTER JOIN cons.AssetsParents ap
				ON	ap.CalDateKey = etl.ConvDateKey(t.StudyYear)
				AND	ap.AssetId = a.AssetId
				AND	ap.Parent IN (p.AssetId, '050', '023', '01B', '010')
			WHERE	t.Refnum = @sRefnum
				AND	p.AssetIdSec IS NULL
				AND ap.AssetId IS NULL;
		END TRY
		BEGIN CATCH
		END CATCH;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO cons.AssetsStudies (cons.VerifyTSort)';
		PRINT @ProcedureDesc;

		INSERT INTO cons.AssetsStudies([StudyId], [AssetId])
		SELECT 'PCH', a.AssetId
		FROM cons.Assets a
		LEFT OUTER JOIN cons.AssetsStudies s
			ON s.AssetId = a.AssetId
		WHERE s.AssetId IS NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO cons.SubscriptionsCompanies (cons.VerifyTSort)';
		PRINT @ProcedureDesc;

		INSERT INTO cons.SubscriptionsCompanies(CompanyId, CalDateKey, StudyId, SubscriberCompanyName, SubscriberCompanyDetail, PassWord_PlainText)
		SELECT
			k.CompanyId,
			etl.ConvDateKey(t.StudyYear),
			'PCH'												[StudyId],
			COALESCE(t.Co, t.CoName)							[CoName],
			CASE k.CompanyId 
				WHEN 'YNCC' THEN 'YNCC'
				WHEN 'Total' THEN 'Total'
				WHEN 'Solomon' THEN 'Solomon'
				WHEN 'SABIC' THEN 'SABIC'
				WHEN 'Sasol Pol' THEN 'SASOL POLYMERS'
				WHEN 'SECCO' THEN 'SECCO'
				ELSE COALESCE(t.CoName, t.Co)
				END												[CoDetail],
			ISNULL(c.[Password], t.[Password])					[pWord]
			FROM stgFact.TSort t
			INNER JOIN dim.Company_LookUp k
				ON	k.CompanyId = etl.ConvCompanyID(t.CompanyId, t.CoName, t.Co)
			LEFT OUTER JOIN stgFact.CoContactInfo c
				ON	c.ContactCode = t.ContactCode
				AND	c.StudyYear = t.StudyYear
				AND	c.[Password] IS NOT NULL
			LEFT OUTER JOIN cons.SubscriptionsCompanies sc
				ON sc.CompanyId = k.CompanyId
			WHERE t.Refnum = @sRefnum
				AND	sc.CompanyId IS NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO cons.SubscriptionsAssets (cons.VerifyTSort)';
		PRINT @ProcedureDesc;

		INSERT INTO cons.SubscriptionsAssets(
			CalDateKey,
			StudyId,
			AssetId,
			ScenarioId,
			ScenarioName,
			ScenarioDetail,
			CompanyId,
			SubscriberAssetName,
			SubscriberAssetDetail,
			PassWord_PlainText,
			Active
			)
		SELECT
			etl.ConvDateKey(t.StudyYear)						[StudyYear],
			'PCH'												[StudyId],
			etl.ConvAssetPCH(t.Refnum)							[AssetId],
			CASE RIGHT(RTRIM(t.Refnum), 1)
				WHEN 'U' THEN 'U'
				WHEN 'P' THEN 'P'
				ELSE 
					CASE t.Refnum
					WHEN '09PCH223A'	THEN 'A'
					ELSE NULL
					END
				END												[ScenarioId],
			CASE RIGHT(RTRIM(t.Refnum), 1)
				WHEN 'U' THEN 'Pro Forma U'
				WHEN 'P' THEN 'Pro Forma P'
				ELSE 
					CASE t.Refnum
					WHEN '09PCH223A'	THEN 'Pro Forma A'
					ELSE 'Basis'
					END
				END												[ScenarioName],
			NULL												[ScenarioDetail],
			etl.ConvCompanyID(t.CompanyId, t.CoName, t.Co)		[CompanyId],
			CASE WHEN LEN(t.PlantName) > 0 THEN ISNULL(t.PlantName, ISNULL(t.Loc, ISNULL(t.PlantLoc, N'————'))) ELSE N'————' END	[PlantName],
			CASE WHEN LEN(t.PlantName) > 0 THEN ISNULL(t.PlantName, ISNULL(t.Loc, ISNULL(t.PlantLoc, N'————'))) ELSE N'————' END	[PlantDetail],
			CASE WHEN LEN(t.[Password]) > 0 THEN t.[Password] END	[pWord],
			1
		FROM stgFact.TSort t
		LEFT OUTER JOIN cons.SubscriptionsAssets sa
			ON	sa.CalDateKey = etl.ConvDateKey(t.StudyYear)
			AND	sa.AssetId =  etl.ConvAssetPCH(t.Refnum)
		WHERE t.Refnum = @sRefnum
			AND	sa.AssetId IS NULL;

		SET @ProcedureDesc = NCHAR(9) + N'INSERT INTO cons.TSortSolomon (cons.VerifyTSort)';
		PRINT @ProcedureDesc;

		INSERT INTO cons.TSortSolomon(Refnum, CalDateKey, Consultant, Active)
		SELECT
			etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum],
			etl.ConvDateKey(t.StudyYear)							[CalDateKey],
			t.Consultant											[Consultant],
			1														[Active]
		FROM stgFact.TSort t
		LEFT OUTER JOIN cons.TSortSolomon	ct
			ON	ct.Refnum = etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')
		WHERE t.Refnum = @sRefnum
			--AND	etl.ConvCompanyID(t.CompanyId, t.CoName, t.Co) IS NOT NULL
			AND	ct.Refnum IS NULL;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
