﻿CREATE TABLE [cons].[SubscriptionsAssets] (
    [Refnum]                AS                 (CONVERT([varchar](25),case when charindex(':',[AssetId])>(0) then [AssetId] else ((isnull(left(CONVERT([char](8),[CalDateKey],(0)),(4)),N'————')+isnull([StudyId],N'————'))+isnull([AssetId],N'————'))+isnull([ScenarioId],'') end,0)) PERSISTED NOT NULL,
    [CompanyId]             VARCHAR (42)       NULL,
    [CalDateKey]            INT                NOT NULL,
    [_StudyYear]            AS                 (CONVERT([smallint],left(CONVERT([char](8),[CalDateKey],(0)),(4)),(0))) PERSISTED NOT NULL,
    [StudyId]               VARCHAR (4)        NOT NULL,
    [AssetId]               VARCHAR (30)       NOT NULL,
    [ScenarioId]            CHAR (1)           NULL,
    [ScenarioName]          NVARCHAR (84)      CONSTRAINT [DF_SubscriptionsAssets_ScenarioName] DEFAULT ('Basis') NOT NULL,
    [ScenarioDetail]        NVARCHAR (256)     NULL,
    [SubscriberAssetName]   NVARCHAR (84)      NOT NULL,
    [SubscriberAssetDetail] NVARCHAR (256)     NOT NULL,
    [PassWord_PlainText]    NVARCHAR (42)      NULL,
    [Active]                BIT                CONSTRAINT [DF_SubscriptionsAssets_Active] DEFAULT ((1)) NOT NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_SubscriptionsAssets_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_SubscriptionsAssets_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_SubscriptionsAssets_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_SubscriptionsAssets_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_SubscriptionsAssets] PRIMARY KEY CLUSTERED ([Refnum] ASC),
    CONSTRAINT [CL_SubscriptionsAssets_PassWord_PlainText] CHECK ([PassWord_PlainText]<>''),
    CONSTRAINT [CL_SubscriptionsAssets_PlantDetail] CHECK ([SubscriberAssetDetail]<>''),
    CONSTRAINT [CL_SubscriptionsAssets_PlantName] CHECK ([SubscriberAssetName]<>''),
    CONSTRAINT [CL_SubscriptionsAssets_ScenarioDetail] CHECK ([ScenarioDetail]<>''),
    CONSTRAINT [CL_SubscriptionsAssets_ScenarioID] CHECK ([ScenarioId]<>''),
    CONSTRAINT [CL_SubscriptionsAssets_ScenarioName] CHECK ([ScenarioName]<>''),
    CONSTRAINT [CR_SubscriptionsAssets_Scenario] CHECK ([ScenarioId] IS NULL AND [ScenarioName]='Basis' OR [ScenarioId] IS NOT NULL AND [ScenarioName]<>'Basis'),
    CONSTRAINT [FK_SubscriptionsAssets_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_SubscriptionsAssets_Company_LookUp] FOREIGN KEY ([CompanyId]) REFERENCES [dim].[Company_LookUp] ([CompanyId]),
    CONSTRAINT [FK_SubscriptionsAssets_StudiesStudyPeriods] FOREIGN KEY ([CalDateKey], [StudyId]) REFERENCES [cons].[StudiesStudyPeriods] ([CalDateKey], [StudyId]),
    CONSTRAINT [FK_SubscriptionsAssets_Study_LookUp] FOREIGN KEY ([StudyId]) REFERENCES [dim].[Study_LookUp] ([StudyId]),
    CONSTRAINT [FK_SubscriptionsAssets_StudyPeriods_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[StudyPeriods_LookUp] ([CalDateKey])
);


GO

CREATE TRIGGER [cons].[t_SubscriptionsAssets_u]
	ON [cons].[SubscriptionsAssets]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[SubscriptionsAssets]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[SubscriptionsAssets].Refnum		= INSERTED.Refnum;

END;