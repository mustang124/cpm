﻿CREATE TABLE [cons].[Assets] (
    [AssetId]        AS                 (ltrim(rtrim([AssetIdPri]))+case when left([AssetIdSec],(1))=':' then [AssetIdSec] else coalesce('-'+[AssetIdSec],'') end) PERSISTED NOT NULL,
    [AssetIdPri]     VARCHAR (25)       NOT NULL,
    [AssetIdSec]     VARCHAR (4)        NULL,
    [AssetName]      NVARCHAR (84)      NOT NULL,
    [AssetDetail]    NVARCHAR (192)     NOT NULL,
    [CountryId]      CHAR (3)           NOT NULL,
    [StateName]      NVARCHAR (42)      NULL,
    [Longitude_Deg]  DECIMAL (17, 14)   NULL,
    [Latitude_Deg]   DECIMAL (17, 14)   NULL,
    [_GeogLoc_WGS84] AS                 ([geography]::STPointFromText(((('POINT('+CONVERT([nvarchar](19),[Longitude_Deg],(0)))+' ')+CONVERT([nvarchar](19),[Latitude_Deg],(0)))+')',(4326))),
    [CalDateKey]     INT                NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Assets_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Assets_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Assets_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Assets_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Assets] PRIMARY KEY CLUSTERED ([AssetId] ASC),
    CONSTRAINT [CL_Assets_AssetDetail] CHECK ([AssetDetail]<>''),
    CONSTRAINT [CL_Assets_AssetIdPri] CHECK ([AssetIdPri]<>''),
    CONSTRAINT [CL_Assets_AssetIdSec] CHECK ([AssetIdSec]<>''),
    CONSTRAINT [CL_Assets_AssetName] CHECK ([AssetName]<>''),
    CONSTRAINT [CR_Assets_Latitude] CHECK ([Latitude_Deg]>=(-90.0) AND [Latitude_Deg]<=(90.0)),
    CONSTRAINT [CR_Assets_Longitude] CHECK ([Longitude_Deg]>=(-180.0) AND [Longitude_Deg]<=(180.0)),
    CONSTRAINT [FK_Assets_Country_LookUp] FOREIGN KEY ([CountryId]) REFERENCES [dim].[Country_LookUp] ([CountryId])
);


GO

CREATE TRIGGER [cons].[t_Assets_u]
	ON  [cons].[Assets]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [cons].[Assets]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[cons].[Assets].AssetId		= INSERTED.AssetId;

END;


--CREATE SPATIAL INDEX [SIX_Assets_Location] 
--    ON [cons].[Assets]
--	(_GeogLoc_WGS84)

--SELECT * FROM [cons].[Assets]