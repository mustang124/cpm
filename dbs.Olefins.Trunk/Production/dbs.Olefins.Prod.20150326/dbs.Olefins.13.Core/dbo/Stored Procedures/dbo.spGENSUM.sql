﻿


CREATE              PROC [dbo].[spGENSUM](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @Company varchar(40) , @Location varchar(40) , @CoLoc varchar(80) , @StudyYear int , @Country varchar(20) , @Region varchar(5) 
	, @CapGroup tinyint , @EDCGroup tinyint , @FeedClass tinyint , @TechClass tinyint , @FeedSubClass varchar(10)
	, @DaysInYear real
SELECT 	
  @StudyYear = t.StudyYear		
, @Country = t.Country
, @Company = RTRIM(t.Company)		
, @Location = RTRIM(t.Location)		
, @CoLoc = RTRIM(t.CoLoc)
, @Region = t.Region	
, @CapGroup = cap.PeerGroup		
, @EDCGroup = edc.PeerGroup		
, @FeedClass = fc.PeerGroup	
, @FeedSubClass = fc.StreamId	
, @TechClass = tech.PeerGroup	
, @DaysInYear = t.NumDays	
FROM dbo.TSort t LEFT JOIN calc.PeerGroupCapacity cap on t.Refnum=cap.Refnum
LEFT JOIN calc.PeerGroupEdc edc on edc.Refnum=t.Refnum and edc.FactorSetId=cap.FactorSetId		
LEFT JOIN calc.PeerGroupFeedClass fc on fc.Refnum=t.Refnum and fc.FactorSetId=cap.FactorSetId		
LEFT JOIN calc.PeerGroupTech tech on tech.Refnum=t.Refnum and tech.FactorSetId=cap.FactorSetId	
WHERE cap.FactorSetId = @FactorSetId and t.Refnum = @Refnum

DECLARE @CogenGroup varchar(10)
Select @CogenGroup = PeerGroup FROM calc.PeerGroupCoGen cog WHERE cog.Refnum=@Refnum and cog.FactorSetId = @FactorSetId		

DECLARE @kEdc real, @kUEdc real, @EthyleneDiv_kMT real, @PropyleneDiv_kMT real, @OlefinsDiv_kMT real
		, @EthyleneProd_kMT real, @PropyleneProd_kMT real, @OlefinsProd_kMT real, @HvcProd_kMT real, @TotalPlantInput_kMT real, @PlantFeed_kMT real
		, @FreshPyroFeed_kMT real, @RV_MUS real, @RVUSGC_MUS real, @TAAdj_kUEDC real
		, @EIIStd real, @NEIStd real, @MEIStd real, @PEIStd real, @mPEIStd real, @nmPEIStd real
SELECT
  @kEdc = kEdc
, @kUEdc = kUEdc
, @EthyleneDiv_kMT = EthyleneDiv_kMT
, @PropyleneDiv_kMT = PropyleneDiv_kMT
, @OlefinsDiv_kMT = OlefinsDiv_kMT
, @EthyleneProd_kMT = EthyleneProd_kMT
, @PropyleneProd_kMT = PropyleneProd_kMT
, @OlefinsProd_kMT = OlefinsProd_kMT
, @HvcProd_kMT = HvcProd_kMT
, @TotalPlantInput_kMT = TotalPlantInput_kMT
, @PlantFeed_kMT = PlantFeed_kMT
, @FreshPyroFeed_kMT = FreshPyroFeed_kMT
, @RV_MUS = RV_MUS
, @RVUSGC_MUS = RVUSGC_MUS
, @TAAdj_kUEDC = TAAdj_kUEDC
, @EIIStd = EIIStd
, @NEIStd = NEIStd
, @MEIStd = MEIStd
, @PEIStd = PEIStd
, @mPEIStd = mPEIStd
, @nmPEIStd = nmPEIStd
FROM dbo.Divisors d WHERE d.Refnum = @Refnum

DECLARE @LocFact real, @RVInflFact real
Select @LocFact = @RV_MUS / @RVUSGC_MUS WHERE @RVUSGC_MUS > 0
Select @RVInflFact = _InflationFactor from ante.InflationFactor where StudyYear = @StudyYear

DECLARE @EthyleneCapAvg_kMT real, @PropyleneCapAvg_kMT real, @OlefinsCapAvg_kMT real, @EthyleneCapYE_kMT real, @PropyleneCapYE_kMT real, @OlefinsCapYE_kMT real
, @EthyleneCpbyAvg_kMT real, @PropyleneCpbyAvg_kMT real, @OlefinsCpbyAvg_kMT real
, @EthyleneCapUtil_pcnt real, @OlefinsCapUtil_pcnt real, @EthyleneCapUtilTAAdj_pcnt real, @OlefinsCapUtilTAAdj_pcnt real
, @EthyleneCpbyUtil_pcnt real, @OlefinsCpbyUtil_pcnt real, @EthyleneCpbyUtilTAAdj_pcnt real, @OlefinsCpbyUtilTAAdj_pcnt real
SELECT
  @EthyleneCapAvg_kMT = EthyleneCapAvg_kMT
, @PropyleneCapAvg_kMT = PropyleneCapAvg_kMT
, @OlefinsCapAvg_kMT = OlefinsCapAvg_kMT
, @EthyleneCapYE_kMT = EthyleneCapYE_kMT
, @PropyleneCapYE_kMT = PropyleneCapYE_kMT
, @OlefinsCapYE_kMT = OlefinsCapYE_kMT
, @EthyleneCpbyAvg_kMT = EthyleneCpbyAvg_kMT
, @PropyleneCpbyAvg_kMT = PropyleneCpbyAvg_kMT
, @OlefinsCpbyAvg_kMT = OlefinsCpbyAvg_kMT

, @EthyleneCapUtil_pcnt = EthyleneCapUtil_pcnt
, @OlefinsCapUtil_pcnt = OlefinsCapUtil_pcnt
, @EthyleneCapUtilTAAdj_pcnt = EthyleneCapUtilTAAdj_pcnt
, @OlefinsCapUtilTAAdj_pcnt = OlefinsCapUtilTAAdj_pcnt
, @EthyleneCpbyUtil_pcnt = EthyleneCpbyUtil_pcnt
, @OlefinsCpbyUtil_pcnt = OlefinsCpbyUtil_pcnt
, @EthyleneCpbyUtilTAAdj_pcnt = EthyleneCpbyUtilTAAdj_pcnt
, @OlefinsCpbyUtilTAAdj_pcnt = OlefinsCpbyUtilTAAdj_pcnt
FROM dbo.CapUtil c where c.Refnum = @Refnum


DECLARE @NetEnergyCons_MBtuperUEDC real, @NetEnergyCons_BTUperHVCLb real,  @NetEnergyCons_GJperHVCMt real, @EII real, @EEI_SPSL real, @EEI_PYPS real
SELECT @NetEnergyCons_MBtuperUEDC = NetEnergyCons
FROM dbo.Energy e 
WHERE e.Refnum = @Refnum AND DataType = 'UEDC'

SELECT 
  @NetEnergyCons_BTUperHVCLb = NetEnergyCons
, @EII = EII
, @EEI_SPSL = EEI_SPSL
, @EEI_PYPS = EEI_PYPS
FROM dbo.Energy e 
WHERE e.Refnum = @Refnum AND DataType = 'BtuPerHVCLb'

SELECT @NetEnergyCons_GJperHVCMt = [$(DbGlobal)].dbo.UnitsConv(@NetEnergyCons_BTUperHVCLb, 'BTU/LB','GJ/MT')

DECLARE @TotPersEDC real, @TotPersHVCMt real, @PEI real, @mPEI real, @nmPEI real
SELECT
  @TotPersEDC = SUM(CASE WHEN DataType = '100EDC' THEN PlantTotal END)
, @TotPersHVCMt = SUM(CASE WHEN DataType = 'HVC_MT' THEN PlantTotal END)
, @PEI = SUM(CASE WHEN DataType = 'PEI' THEN PlantTotal END)
, @mPEI = SUM(CASE WHEN DataType = 'mPEI' THEN Maint END)
, @nmPEI = SUM(CASE WHEN DataType = 'nmPEI' THEN _NonMaint END)
FROM dbo.Personnel p 
WHERE p.Refnum = @Refnum and DataType in ('100EDC','HVC_MT', 'PEI','mPEI','nmPEI')

DECLARE @PersCost_CurrPerEDC real, @PersCost_CurrPerHVC_MT real
SELECT
  @PersCost_CurrPerEDC = PersCost_CurrPerEDC
, @PersCost_CurrPerHVC_MT = PersCost_CurrPerHVC_MT
FROM dbo.PersCost p WHERE p.Refnum=@Refnum

DECLARE @MaintIndexEDC real, @MaintIndexRV real, @RelInd real, @RMeiEDC real, @RMeiRV real, @MEI real
	, @MechAvail real, @OpAvail real, @PlantAvail real, @MechAvailSlow real, @OpAvailSlow real, @PlantAvailSlow real
SELECT
  @MaintIndexEDC = MaintIndexEDC
, @MaintIndexRV = MaintIndexRV
, @RelInd = RelInd
, @RMeiEDC = RMeiEDC
, @RMeiRV = RMeiRV
, @MEI = MEI
, @MechAvail = MechAvail
, @OpAvail = OpAvail
, @PlantAvail = PlantAvail
, @MechAvailSlow = MechAvailSlow
, @OpAvailSlow = OpAvailSlow
, @PlantAvailSlow = PlantAvailSlow
FROM dbo.Maint m WHERE m.Refnum = @Refnum

DECLARE @C2H4_OSOP real , @Chem_OSOP real
	, @C2H4YIR_MS25_SPSL real, @HVCProdYIR_MS25_SPSL real, @C2H4YIR_OSOP_SPSL real, @HVCProdYIR_OSOP_SPSL real, @C2H4YIR_OS25_SPSL real, @HVCProdYIR_OS25_SPSL real, @SeverityIndex_SPSL real
	, @C2H4YIR_MS25_PYPS real, @HVCProdYIR_MS25_PYPS real, @C2H4YIR_OSOP_PYPS real, @HVCProdYIR_OSOP_PYPS real, @C2H4YIR_OS25_PYPS real, @HVCProdYIR_OS25_PYPS real, @SeverityIndex_PYPS real
	, @ActPPV_PcntOS25_SPSL real, @ActPPV_PcntMS25_SPSL real, @ActPPV_PcntOS25_PYPS real, @ActPPV_PcntMS25_PYPS real
SELECT 
  @C2H4_OSOP				= SUM(CASE WHEN SimModelId = 'Plant' THEN C2H4_OSOP ELSE 0 END)
, @Chem_OSOP				= SUM(CASE WHEN SimModelId = 'Plant' THEN ISNULL(C2H4_OSOP,0) + ISNULL(C3H6_OSOP,0) + ISNULL(OthHVChem_OSOP,0) ELSE 0 END)
, @C2H4YIR_MS25_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN C2H4YIR_MS25 ELSE 0 END)
, @HVCProdYIR_MS25_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN ProdHVCYIR_MS25 ELSE 0 END)
, @C2H4YIR_OSOP_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN C2H4YIR_OSOP ELSE 0 END)
, @HVCProdYIR_OSOP_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN ProdHVCYIR_OSOP ELSE 0 END)
, @C2H4YIR_OS25_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN C2H4YIR_OS25 ELSE 0 END)
, @HVCProdYIR_OS25_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN ProdHVCYIR_OS25 ELSE 0 END)
, @SeverityIndex_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN SeverityIndex ELSE 0 END)
, @C2H4YIR_MS25_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN C2H4YIR_MS25 ELSE 0 END)
, @HVCProdYIR_MS25_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN ProdHVCYIR_MS25 ELSE 0 END)
, @C2H4YIR_OSOP_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN C2H4YIR_OSOP ELSE 0 END)
, @HVCProdYIR_OSOP_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN ProdHVCYIR_OSOP ELSE 0 END)
, @C2H4YIR_OS25_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN C2H4YIR_OS25 ELSE 0 END)
, @HVCProdYIR_OS25_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN ProdHVCYIR_OS25 ELSE 0 END)
, @SeverityIndex_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN SeverityIndex ELSE 0 END)
, @ActPPV_PcntOS25_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN ActPPV_PcntOS25 ELSE 0 END)
, @ActPPV_PcntMS25_SPSL		= SUM(CASE WHEN SimModelId = 'SPSL' THEN ActPPV_PcntMS25 ELSE 0 END)
, @ActPPV_PcntOS25_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN ActPPV_PcntOS25 ELSE 0 END)
, @ActPPV_PcntMS25_PYPS		= SUM(CASE WHEN SimModelId = 'PYPS' THEN ActPPV_PcntMS25 ELSE 0 END)
FROM dbo.Pyrolysis p WHERE p.Refnum=@Refnum and p.Currency='USD' and Scenario = 'BASIS' and DataType = 'WtPcnt'

DECLARE @PhysLoss_Pcnt real	
SELECT @PhysLoss_Pcnt = (ISNULL(y.LossFlareVent,0) + ISNULL(y.LossOther,0))/ProdLoss*100.0
FROM dbo.Yield y
WHERE y.Refnum = @Refnum and y.DataType = 'kMT'

DECLARE @NEOpExEDC real, @TotCashOpExUEDC real, @TotRefExpUEDC real, @NEI real, @NEOpExHVCLb real, @TotCashOpExHVCLb real, @TotRefExpHVCLb real, 
	@NEOpExHVCMt real, @TotCashOpExHVCMt real, @TotRefExpHVCMt real
SELECT 
  @NEOpExEDC			= SUM(CASE WHEN DataType = 'EDC' THEN NEOpEx ELSE 0 END)
, @NEI					= SUM(CASE WHEN DataType = 'NEI' THEN NEOpEx ELSE 0 END)
, @TotCashOpExUEDC		= SUM(CASE WHEN DataType = 'UEDC' THEN TotCashOpEx ELSE 0 END)
, @TotRefExpUEDC		= SUM(CASE WHEN DataType = 'UEDC' THEN TotRefExp ELSE 0 END)
, @NEOpExHVCLb			= SUM(CASE WHEN DataType = 'HVC_LB' THEN NEOpEx ELSE 0 END)
, @TotCashOpExHVCLb		= SUM(CASE WHEN DataType = 'HVC_LB' THEN TotCashOpEx ELSE 0 END)
, @TotRefExpHVCLb		= SUM(CASE WHEN DataType = 'HVC_LB' THEN TotRefExp ELSE 0 END)
, @NEOpExHVCMt			= SUM(CASE WHEN DataType = 'HVC_MT' THEN NEOpEx ELSE 0 END)
, @TotCashOpExHVCMt		= SUM(CASE WHEN DataType = 'HVC_MT' THEN TotCashOpEx ELSE 0 END)
, @TotRefExpHVCMt		= SUM(CASE WHEN DataType = 'HVC_MT' THEN TotRefExp ELSE 0 END)
FROM dbo.OpEx o
WHERE o.Refnum=@Refnum and o.Currency='USD'

DECLARE @EthyleneProdCostPerUEDC real, @OlefinsProdCostPerUEDC real, @HVCProdCostPerUEDC real, @EthyleneProdCostPerMT real, @OlefinsProdCostPerMT real, @HVCProdCostPerMT real
	, @EthyleneProdCentsPerLB real, @OlefinsProdCentsPerLB real, @HVCProdCentsPerLB real, @NCMPerEthyleneMT real, @NCMPerOlefinsMT real, @NCMPerHVCMT real
	, @NCMPerEthyleneLB real, @NCMPerOlefinsLB real, @NCMPerHVCLB real, @GMPerRV real, @ROI real, @ROITAAdj real
	, @GMPerRVUSGC real, @NCMPerRVUSGC real, @NCMPerRVUSGCTAAdj real, @ProformaOpExPerUEDC real
	, @ProformaOpExPerHVCMT real, @ProformaProdCostPerEthyleneMT real, @ProformaProdCostPerHVCMT real, @ProformaNCMPerEthyleneMT real, @ProformaNCMPerHVCMT real, @ProformaROI real
	, @ProformaOpExPerHVCLb real, @ProformaProdCostPerEthyleneLb real, @ProformaProdCostPerHVCLb real, @ProformaNCMPerEthyleneLb real, @ProformaNCMPerHVCLb real
	, @ProformaEthyleneProdCostPerUEDC real, @ProformaHVCProdCostPerUEDC real
SELECT 
  @EthyleneProdCostPerUEDC	= EthyleneProdCostPerUEDC
, @OlefinsProdCostPerUEDC	= OlefinsProdCostPerUEDC
, @HVCProdCostPerUEDC	= HVChemProdCostPerUEDC

, @EthyleneProdCostPerMT	= EthyleneProdCostPerMT
, @OlefinsProdCostPerMT		= OlefinsProdCostPerMT
, @HVCProdCostPerMT		= HVChemProdCostPerMT
, @EthyleneProdCentsPerLB	= [$(DbGlobal)].dbo.UnitsConv(EthyleneProdCostPerMT, '1/MT', '1/LB') * 100.0
, @OlefinsProdCentsPerLB	= [$(DbGlobal)].dbo.UnitsConv(OlefinsProdCostPerMT, '1/MT', '1/LB') * 100.0
, @HVCProdCentsPerLB		= [$(DbGlobal)].dbo.UnitsConv(HVChemProdCostPerMT, '1/MT', '1/LB') * 100.0

, @NCMPerEthyleneMT		= NCMPerEthyleneMT
, @NCMPerOlefinsMT		= NCMPerOlefinsMT
, @NCMPerHVCMT		= NCMPerHVChemMT
, @NCMPerEthyleneLB		= [$(DbGlobal)].dbo.UnitsConv(NCMPerEthyleneMT, '1/MT', '1/LB') * 100.0
, @NCMPerOlefinsLB		= [$(DbGlobal)].dbo.UnitsConv(NCMPerOlefinsMT, '1/MT', '1/LB') * 100.0
, @NCMPerHVCLB		= [$(DbGlobal)].dbo.UnitsConv(NCMPerHVChemMT, '1/MT', '1/LB') * 100.0

, @GMPerRV	= GMPerRV
, @ROI		= NCMperRV
, @ROITAAdj = NCMperRVTAAdj

, @GMPerRVUSGC			= GMPerRV * d.RV_MUS / d.RVUSGC_MUS
, @NCMPerRVUSGC			= NCMperRV * d.RV_MUS / d.RVUSGC_MUS
, @NCMPerRVUSGCTAAdj	= NCMperRVTAAdj * d.RV_MUS / d.RVUSGC_MUS

, @ProformaOpExPerUEDC = ProformaOpExPerUEDC
, @ProformaEthyleneProdCostPerUEDC	= ProformaProdCostPerEthyleneMT * d.EthyleneProd_kMT / d.kUEdc
, @ProformaHVCProdCostPerUEDC		= ProformaProdCostPerHVChemMT * d.HvcProd_kMT / d.kUEdc

, @ProformaOpExPerHVCMT			= ProformaOpExPerHVChemMT
, @ProformaProdCostPerEthyleneMT	= ProformaProdCostPerEthyleneMT
, @ProformaProdCostPerHVCMT		= ProformaProdCostPerHVChemMT
, @ProformaNCMPerEthyleneMT			= ProformaNCMPerEthyleneMT
, @ProformaNCMPerHVCMT			= ProformaNCMPerHVChemMT
, @ProformaROI						= ProformaROI

, @ProformaOpExPerHVCLb				= [$(DbGlobal)].dbo.UnitsConv(ProformaOpExPerHVChemMT, '1/MT', '1/LB') * 100.0
, @ProformaProdCostPerEthyleneLb	= [$(DbGlobal)].dbo.UnitsConv(ProformaProdCostPerEthyleneMT, '1/MT', '1/LB') * 100.0
, @ProformaProdCostPerHVCLb			= [$(DbGlobal)].dbo.UnitsConv(ProformaProdCostPerHVChemMT, '1/MT', '1/LB') * 100.0
, @ProformaNCMPerEthyleneLb			= [$(DbGlobal)].dbo.UnitsConv(ProformaNCMPerEthyleneMT, '1/MT', '1/LB') * 100.0
, @ProformaNCMPerHVCLb				= [$(DbGlobal)].dbo.UnitsConv(ProformaNCMPerHVChemMT, '1/MT', '1/LB') * 100.0
FROM dbo.Financial f JOIN dbo.Divisors d on d.Refnum=f.Refnum
WHERE f.Refnum = @Refnum and f.Currency = 'USD'

DECLARE @APCImplIndex real, @APCOnLineIndex real
SELECT
  @APCImplIndex = APCImplIndex
, @APCOnLineIndex = APCOnLineIndex
FROM dbo.APC a
WHERE a.Refnum=@Refnum

DELETE FROM dbo.GENSUM WHERE Refnum = @Refnum
Insert into dbo.GENSUM (Refnum
, StudyYear	
, Country
, Company		
, Location	
, CoLoc	
, Region		
, CapGroup	
, EDCGroup
, FeedClass	
, FeedSubClass
, TechClass
, CoGenGroup
, kEdc
, kUEdc
, EthyleneDiv_kMT
, PropyleneDiv_kMT
, OlefinsDiv_kMT
, EthyleneProd_kMT
, PropyleneProd_kMT
, OlefinsProd_kMT
, HvcProd_kMT
, TotalPlantInput_kMT
, PlantFeed_kMT
, FreshPyroFeed_kMT
, EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT
, EthyleneCapYE_kMT
, PropyleneCapYE_kMT
, OlefinsCapYE_kMT
, EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT
, RV_MUS
, RVUSGC_MUS
, LocFact
, RVInflFact
, TAAdj_kUEDC
, EthyleneCapUtil_pcnt
, OlefinsCapUtil_pcnt
, EthyleneCapUtilTAAdj_pcnt
, OlefinsCapUtilTAAdj_pcnt
, EthyleneCpbyUtil_pcnt
, OlefinsCpbyUtil_pcnt
, EthyleneCpbyUtilTAAdj_pcnt
, OlefinsCpbyUtilTAAdj_pcnt
, NetEnergyCons_MBtuperUEDC
, NetEnergyCons_BTUperHVCLb
, NetEnergyCons_GJperHVCMt
, EII
, EEI_SPSL
, EEI_PYPS
, TotPersEDC
, TotPersHVCMt
, PEI
, mPEI
, nmPEI
, PersCost_CurrPerEDC
, PersCost_CurrPerHVC_MT
, MaintIndexEDC
, MaintIndexRV
, RelInd
, MechAvail
, OpAvail
, PlantAvail
, MechAvailSlow
, OpAvailSlow
, PlantAvailSlow
, RMeiEDC
, RMeiRV
, MEI
, C2H4_OSOP
, Chem_OSOP
, C2H4YIR_MS25_SPSL
, HVCProdYIR_MS25_SPSL
, C2H4YIR_OSOP_SPSL
, HVCProdYIR_OSOP_SPSL
, C2H4YIR_OS25_SPSL
, HVCProdYIR_OS25_SPSL
, SeverityIndex_SPSL
, C2H4YIR_MS25_PYPS
, HVCProdYIR_MS25_PYPS
, C2H4YIR_OSOP_PYPS
, HVCProdYIR_OSOP_PYPS
, C2H4YIR_OS25_PYPS
, HVCProdYIR_OS25_PYPS
, SeverityIndex_PYPS
, ActPPV_PcntOS25_SPSL
, ActPPV_PcntMS25_SPSL
, ActPPV_PcntOS25_PYPS
, ActPPV_PcntMS25_PYPS
, PhysLoss_Pcnt
, NEOpExEDC
, NEI
, TotCashOpExUEDC
, TotRefExpUEDC
, NEOpExHVCLb
, TotCashOpExHVCLb
, TotRefExpHVCLb
, NEOpExHVCMt
, TotCashOpExHVCMt
, TotRefExpHVCMt
, EthyleneProdCostPerUEDC
, OlefinsProdCostPerUEDC
, HVCProdCostPerUEDC
, EthyleneProdCostPerMT
, OlefinsProdCostPerMT
, HVCProdCostPerMT
, EthyleneProdCentsPerLB
, OlefinsProdCentsPerLB
, HVCProdCentsPerLB
, NCMPerEthyleneMT
, NCMPerOlefinsMT
, NCMPerHVCMT
, NCMPerEthyleneLB
, NCMPerOlefinsLB
, NCMPerHVCLB
, GMPerRV
, ROI
, ROITAAdj
, GMPerRVUSGC
, NCMPerRVUSGC
, NCMPerRVUSGCTAAdj
, ProformaOpExPerUEDC
, ProformaEthyleneProdCostPerUEDC
, ProformaHVCProdCostPerUEDC
, ProformaOpExPerHVCMT
, ProformaProdCostPerEthyleneMT
, ProformaProdCostPerHVCMT
, ProformaNCMPerEthyleneMT
, ProformaNCMPerHVCMT
, ProformaROI
, ProformaOpExPerHVCLb
, ProformaProdCostPerEthyleneLb
, ProformaProdCostPerHVCLb
, ProformaNCMPerEthyleneLb
, ProformaNCMPerHVCLb
, APCImplIndex
, APCOnLineIndex
, EIIStd
, NEIStd
, MEIStd
, PEIStd
, mPEIStd
, nmPEIStd)

SELECT Refnum = @Refnum
, StudyYear	= @StudyYear
, Country = @Country
, Company = @Company
, Location= @Location
, CoLoc	= @CoLoc
, Region = @Region
, CapGroup = @CapGroup
, EDCGroup = @EDCGroup
, FeedClass	= @FeedClass
, FeedSubClass	= @FeedSubClass
, TechClass = @TechClass
, CogenGroup = @CogenGroup
, kEdc = @kEdc
, kUEdc = @kUEdc
, EthyleneDiv_kMT = @EthyleneDiv_kMT
, PropyleneDiv_kMT = @PropyleneDiv_kMT
, OlefinsDiv_kMT = @OlefinsDiv_kMT
, EthyleneProd_kMT = @EthyleneProd_kMT
, PropyleneProd_kMT = @PropyleneProd_kMT
, OlefinsProd_kMT = @OlefinsProd_kMT
, HvcProd_kMT = @HvcProd_kMT
, TotalPlantInput_kMT = @TotalPlantInput_kMT
, PlantFeed_kMT = @PlantFeed_kMT
, FreshPyroFeed_kMT = @FreshPyroFeed_kMT
, EthyleneCapAvg_kMT = @EthyleneCapAvg_kMT
, PropyleneCapAvg_kMT = @PropyleneCapAvg_kMT
, OlefinsCapAvg_kMT = @OlefinsCapAvg_kMT
, EthyleneCapYE_kMT = @EthyleneCapYE_kMT
, PropyleneCapYE_kMT = @PropyleneCapYE_kMT
, OlefinsCapYE_kMT = @OlefinsCapYE_kMT
, EthyleneCpbyAvg_kMT = @EthyleneCpbyAvg_kMT
, PropyleneCpbyAvg_kMT = @PropyleneCpbyAvg_kMT
, OlefinsCpbyAvg_kMT = @OlefinsCpbyAvg_kMT
, RV_MUS = @RV_MUS
, RVUSGC_MUS = @RVUSGC_MUS
, LocFact = @LocFact
, RVInflFact = @RVInflFact
, TAAdj_kUEDC = @TAAdj_kUEDC
, EthyleneCapUtil_pcnt = @EthyleneCapUtil_pcnt
, OlefinsCapUtil_pcnt = @OlefinsCapUtil_pcnt
, EthyleneCapUtilTAAdj_pcnt = @EthyleneCapUtilTAAdj_pcnt
, OlefinsCapUtilTAAdj_pcnt = @OlefinsCapUtilTAAdj_pcnt
, EthyleneCpbyUtil_pcnt = @EthyleneCpbyUtil_pcnt
, OlefinsCpbyUtil_pcnt = @OlefinsCpbyUtil_pcnt
, EthyleneCpbyUtilTAAdj_pcnt = @EthyleneCpbyUtilTAAdj_pcnt
, OlefinsCpbyUtilTAAdj_pcnt = @OlefinsCpbyUtilTAAdj_pcnt
, NetEnergyCons_MBtuperUEDC = @NetEnergyCons_MBtuperUEDC
, NetEnergyCons_BTUperHVCLb = @NetEnergyCons_BTUperHVCLb
, NetEnergyCons_GJperHVCMt =@NetEnergyCons_GJperHVCMt
, EII = @EII
, EEI_SPSL = @EEI_SPSL
, EEI_PYPS = @EEI_PYPS
, TotPersEDC = @TotPersEDC
, TotPersHVCMt = @TotPersHVCMt
, PEI = @PEI
, mPEI = @mPEI
, nmPEI = @nmPEI
, PersCost_CurrPerEDC = @PersCost_CurrPerEDC
, PersCost_CurrPerHVC_MT = @PersCost_CurrPerHVC_MT
, MaintIndexEDC = @MaintIndexEDC
, MaintIndexRV = @MaintIndexRV
, RelInd = @RelInd
, MechAvail = @MechAvail
, OpAvail = @OpAvail
, PlantAvail = @PlantAvail
, MechAvailSlow = @MechAvailSlow
, OpAvailSlow = @OpAvailSlow
, PlantAvailSlow = @PlantAvailSlow
, RMeiEDC = @RMeiEDC
, RMeiRV = @RMeiRV
, MEI = @MEI
, C2H4_OSOP = @C2H4_OSOP
, Chem_OSOP = @Chem_OSOP
, C2H4YIR_MS25_SPSL = @C2H4YIR_MS25_SPSL
, HVCProdYIR_MS25_SPSL = @HVCProdYIR_MS25_SPSL
, C2H4YIR_OSOP_SPSL = @C2H4YIR_OSOP_SPSL
, HVCProdYIR_OSOP_SPSL = @HVCProdYIR_OSOP_SPSL
, C2H4YIR_OS25_SPSL = @C2H4YIR_OS25_SPSL
, HVCProdYIR_OS25_SPSL = @HVCProdYIR_OS25_SPSL
, SeverityIndex_SPSL = @SeverityIndex_SPSL
, C2H4YIR_MS25_PYPS = @C2H4YIR_MS25_PYPS
, HVCProdYIR_MS25_PYPS = @HVCProdYIR_MS25_PYPS
, C2H4YIR_OSOP_PYPS = @C2H4YIR_OSOP_PYPS
, HVCProdYIR_OSOP_PYPS = @HVCProdYIR_OSOP_PYPS
, C2H4YIR_OS25_PYPS = @C2H4YIR_OS25_PYPS
, HVCProdYIR_OS25_PYPS = @HVCProdYIR_OS25_PYPS
, SeverityIndex_PYPS = @SeverityIndex_PYPS
, ActPPV_PcntOS25_SPSL = @ActPPV_PcntOS25_SPSL
, ActPPV_PcntMS25_SPSL = @ActPPV_PcntMS25_SPSL
, ActPPV_PcntOS25_PYPS = @ActPPV_PcntOS25_PYPS
, ActPPV_PcntMS25_PYPS = @ActPPV_PcntMS25_PYPS
, PhysLoss_Pcnt = @PhysLoss_Pcnt
, NEOpExEDC = @NEOpExEDC
, NEI = @NEI
, TotCashOpExUEDC = @TotCashOpExUEDC
, TotRefExpUEDC = @TotRefExpUEDC
, NEOpExHVCLb = @NEOpExHVCLb
, TotCashOpExHVCLb = @TotCashOpExHVCLb
, TotRefExpHVCLb = @TotRefExpHVCLb
, NEOpExHVCMt = @NEOpExHVCMt
, TotCashOpExHVCMt = @TotCashOpExHVCMt
, TotRefExpHVCMt = @TotRefExpHVCMt
, EthyleneProdCostPerUEDC = @EthyleneProdCostPerUEDC
, OlefinsProdCostPerUEDC = @OlefinsProdCostPerUEDC
, HVCProdCostPerUEDC = @HVCProdCostPerUEDC
, EthyleneProdCostPerMT = @EthyleneProdCostPerMT
, OlefinsProdCostPerMT = @OlefinsProdCostPerMT
, HVCProdCostPerMT = @HVCProdCostPerMT
, EthyleneProdCentsPerLB = @EthyleneProdCentsPerLB
, OlefinsProdCentsPerLB = @OlefinsProdCentsPerLB
, HVCProdCentsPerLB = @HVCProdCentsPerLB
, NCMPerEthyleneMT = @NCMPerEthyleneMT
, NCMPerOlefinsMT = @NCMPerOlefinsMT
, NCMPerHVCMT = @NCMPerHVCMT
, NCMPerEthyleneLB = @NCMPerEthyleneLB
, NCMPerOlefinsLB = @NCMPerOlefinsLB
, NCMPerHVCLB = @NCMPerHVCLB
, GMPerRV = @GMPerRV
, ROI = @ROI
, ROITAAdj = @ROITAAdj
, GMPerRVUSGC = @GMPerRVUSGC
, NCMPerRVUSGC = @NCMPerRVUSGC
, NCMPerRVUSGCTAAdj = @NCMPerRVUSGCTAAdj
, ProformaOpExPerUEDC = @ProformaOpExPerUEDC
, ProformaEthyleneProdCostPerUEDC = @ProformaEthyleneProdCostPerUEDC
, ProformaHVCProdCostPerUEDC = @ProformaHVCProdCostPerUEDC
, ProformaOpExPerHVCMT = @ProformaOpExPerHVCMT
, ProformaProdCostPerEthyleneMT = @ProformaProdCostPerEthyleneMT
, ProformaProdCostPerHVCMT = @ProformaProdCostPerHVCMT
, ProformaNCMPerEthyleneMT = @ProformaNCMPerEthyleneMT
, ProformaNCMPerHVCMT = @ProformaNCMPerHVCMT
, ProformaROI = @ProformaROI
, ProformaOpExPerHVCLb = @ProformaOpExPerHVCLb
, ProformaProdCostPerEthyleneLb = @ProformaProdCostPerEthyleneLb
, ProformaProdCostPerHVCLb = @ProformaProdCostPerHVCLb
, ProformaNCMPerEthyleneLb = @ProformaNCMPerEthyleneLb
, ProformaNCMPerHVCLb = @ProformaNCMPerHVCLb
, APCImplIndex = @APCImplIndex
, APCOnLineIndex = @APCOnLineIndex
, EIIStd = @EIIStd
, NEIStd = @NEIStd
, MEIStd = @MEIStd
, PEIStd = @PEIStd
, mPEIStd = @mPEIStd
, nmPEIStd = @nmPEIStd 

SET NOCOUNT OFF

















