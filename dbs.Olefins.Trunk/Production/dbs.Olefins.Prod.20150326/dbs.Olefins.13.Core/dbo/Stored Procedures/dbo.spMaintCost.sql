﻿






CREATE              PROC [dbo].[spMaintCost](@Refnum varchar(25), @FactorSetId varchar(12))
AS
SET NOCOUNT ON

DELETE FROM dbo.MaintCost WHERE Refnum = @Refnum
Insert into dbo.MaintCost (Refnum
, Currency
, DataType
, AnnFurnReTube
, PyroFurnMajOth
, PyroFurnMinOth
, PyroFurnTLE
, STFurn
, CompGas
, Pump
, Exchanger
, Vessel
, PipeOnSite
, InstrAna
, PCComputers
, ElecGen
, TowerCool
, BoilFiredSteam
, OtherOnSite
, TurnAroundServices
, STOnsite
, Store
, Flare
, Enviro
, OtherOffSite
, STOffSite
, TotDirect
, OverHead
, TotMaint
, CalDateKey)

SELECT pvt.Refnum,  Currency = pvt.CurrencyRpt, pvt.DataType
,AnnFurnReTube = CASE pvt.DataType	WHEN 'RoutLabor' THEN InflAdjAnn_MaintRetubeLabor_Cur
									WHEN 'RoutMatl' THEN InflAdjAnn_MaintRetubeMatl_Cur
									WHEN 'RoutTot' THEN InflAdjAnn_MaintRetube_Cur
									ELSE NULL END
,PyroFurnMajOth
,PyroFurnMinOth
,PyroFurnTLE
,STFurn			= ISNULL(CASE pvt.DataType	WHEN 'RoutLabor' THEN InflAdjAnn_MaintRetubeLabor_Cur
									WHEN 'RoutMatl' THEN InflAdjAnn_MaintRetubeMatl_Cur
									WHEN 'RoutTot' THEN InflAdjAnn_MaintRetube_Cur
									ELSE NULL END,0)
					+ ISNULL(PyroFurnMajOth,0) + ISNULL(PyroFurnMinOth,0) + ISNULL(PyroFurnTLE,0)
,CompGas,Pump,Exchanger,Vessel,PipeOnSite,InstrAna,PCComputers,ElecGen,TowerCool,BoilFiredSteam,OtherOnSite,TurnAroundServices
,STOnsite		= ISNULL(CASE pvt.DataType	WHEN 'RoutLabor' THEN InflAdjAnn_MaintRetubeLabor_Cur
									WHEN 'RoutMatl' THEN InflAdjAnn_MaintRetubeMatl_Cur
									WHEN 'RoutTot' THEN InflAdjAnn_MaintRetube_Cur
									ELSE NULL END,0)
					+ ISNULL(PyroFurnMajOth,0) + ISNULL(PyroFurnMinOth,0) + ISNULL(PyroFurnTLE,0)
					+ ISNULL(CompGas,0)+ ISNULL(Pump,0)+ ISNULL(Exchanger,0)+ ISNULL(Vessel,0)+ ISNULL(PipeOnSite,0)+ ISNULL(InstrAna,0)+ ISNULL(PCComputers,0)
					+ ISNULL(ElecGen,0)+ ISNULL(TowerCool,0)+ ISNULL(BoilFiredSteam,0)+ ISNULL(OtherOnSite,0) + ISNULL(TurnAroundServices,0)
,Store,Flare,Enviro,OtherOffSite
,STOffSite			= ISNULL(Store,0) + ISNULL(Flare,0) + ISNULL(Enviro,0) + ISNULL(OtherOffSite,0)
,TotDirect		= ISNULL(CASE pvt.DataType	WHEN 'RoutLabor' THEN InflAdjAnn_MaintRetubeLabor_Cur
									WHEN 'RoutMatl' THEN InflAdjAnn_MaintRetubeMatl_Cur
									WHEN 'RoutTot' THEN InflAdjAnn_MaintRetube_Cur
									ELSE NULL END,0)
					+ ISNULL(PyroFurnMajOth,0) + ISNULL(PyroFurnMinOth,0) + ISNULL(PyroFurnTLE,0)
					+ ISNULL(CompGas,0)+ ISNULL(Pump,0)+ ISNULL(Exchanger,0)+ ISNULL(Vessel,0)+ ISNULL(PipeOnSite,0)+ ISNULL(InstrAna,0)+ ISNULL(PCComputers,0)
					+ ISNULL(ElecGen,0)+ ISNULL(TowerCool,0)+ ISNULL(BoilFiredSteam,0)+ ISNULL(OtherOnSite,0) + ISNULL(TurnAroundServices,0)
					+ ISNULL(Store,0) + ISNULL(Flare,0) + ISNULL(Enviro,0) + ISNULL(OtherOffSite,0)
,OverHead
,TotMaint		= ISNULL(CASE pvt.DataType	WHEN 'RoutLabor' THEN InflAdjAnn_MaintRetubeLabor_Cur
									WHEN 'RoutMatl' THEN InflAdjAnn_MaintRetubeMatl_Cur
									WHEN 'RoutTot' THEN InflAdjAnn_MaintRetube_Cur
									ELSE NULL END,0)
					+ ISNULL(PyroFurnMajOth,0) + ISNULL(PyroFurnMinOth,0) + ISNULL(PyroFurnTLE,0)
					+ ISNULL(CompGas,0)+ ISNULL(Pump,0)+ ISNULL(Exchanger,0)+ ISNULL(Vessel,0)+ ISNULL(PipeOnSite,0)+ ISNULL(InstrAna,0)+ ISNULL(PCComputers,0)
					+ ISNULL(ElecGen,0)+ ISNULL(TowerCool,0)+ ISNULL(BoilFiredSteam,0)+ ISNULL(OtherOnSite,0) + ISNULL(TurnAroundServices,0)
					+ ISNULL(Store,0) + ISNULL(Flare,0) + ISNULL(Enviro,0) + ISNULL(OtherOffSite,0)
					+ ISNULL(OverHead,0)

,pvt.CalDateKey
FROM (
SELECT m.Refnum, m.CurrencyRpt, m.CalDateKey, m.FacilityId, p.DataType, p.Value
FROM fact.Maint m CROSS APPLY (
      VALUES ('RoutLabor', m.MaintLabor_Cur)
            , ('RoutMatl', m.MaintMaterial_Cur)
            , ('RoutTot', m._Maint_Cur)
            , ('TALabor', m.TaLabor_Cur)
            , ('TAMatl', m.TaMaterial_Cur)
            , ('TATot', m._TA_Cur)
      ) p(DataType, Value)) src
PIVOT (SUM(src.Value) FOR FacilityId IN (PyroFurnMajOth,PyroFurnMinOth,PyroFurnTLE,CompGas,Pump,Exchanger,Vessel,PipeOnSite,InstrAna,PCComputers,ElecGen,TowerCool,BoilFiredSteam,
	OtherOnSite,Store,Flare,Enviro,OtherOffSite,TotDirect,OverHead,TurnAroundServices)) pvt
	LEFT OUTER JOIN calc.FurnacesAggregate f on f.Refnum=pvt.Refnum and f.CurrencyRpt=pvt.CurrencyRpt and f.FactorSetId = @FactorSetId
WHERE  pvt.Refnum = @Refnum
SET NOCOUNT OFF

















