﻿--


CREATE              PROC [dbo].[spMaint](@Refnum varchar(18), @FactorSetId as varchar(5))
AS
SET NOCOUNT ON
DECLARE @Currency as varchar(5)
SELECT @Currency = 'USD'

DECLARE @RoutIndexEDC real, @TAIndexEDC real, @MaintIndexEDC real, @RoutIndexRV real, @TAIndexRV real, @MaintIndexRV real, @RoutMEI real, @TAMEI real, @MEI real
SELECT
  @RoutIndexEDC = m.MaintAvg_Cur / d.kEdc
, @TAIndexEDC  = m.InflAdjAnn_TurnAroundCost_kCur / d.kEdc
, @MaintIndexEDC  = m.Ann_MaintCostIndex_Cur / d.kEdc
, @RoutIndexRV  = m.MaintAvg_Cur / d.RV_MUS / 10.0
, @TAIndexRV  = m.InflAdjAnn_TurnAroundCost_kCur / d.RV_MUS / 10.0
, @MaintIndexRV  = m.Ann_MaintCostIndex_Cur / d.RV_MUS / 10.0
, @RoutMEI  = m.MaintAvg_Cur / d.MEIStd * 100.0
, @TAMEI  = m.InflAdjAnn_TurnAroundCost_kCur / d.MEIStd * 100.0
, @MEI  = m.Ann_MaintCostIndex_Cur / d.MEIStd * 100.0
FROM calc.MeiMaintenanceCostIndex m JOIN dbo.Divisors d on d.Refnum=m.Refnum
WHERE m.Refnum = @Refnum and d.RV_MUS>0 and d.kEdc>0 and m.FactorSetId = @FactorSetId and m.CurrencyRpt = @Currency

DECLARE	@MechAvail real, @OpAvail real, @PlantAvail real, @MechAvailSlow real, @OpAvailSlow real, @PlantAvailSlow real
	, @MechUnAvailTA real, @MechUnAvailRout real, @MechUnAvailRoutSlow real, @OthMechUnAvail real
Select
  @MechAvail = SUM(CASE WHEN OppLossId = 'TAEqF' THEN DtAvailability_Pcnt END)
, @OpAvail = SUM(CASE WHEN OppLossId = 'TAEqFPro' THEN DtAvailability_Pcnt END)
, @PlantAvail = SUM(CASE WHEN OppLossId = 'TAEqFProUtf' THEN DtAvailability_Pcnt END)
, @MechAvailSlow = SUM(CASE WHEN OppLossId = 'TAEqF' THEN TotAvailability_Pcnt END)
, @OpAvailSlow = SUM(CASE WHEN OppLossId = 'TAEqFPro' THEN TotAvailability_Pcnt END)
, @PlantAvailSlow = SUM(CASE WHEN OppLossId = 'TAEqFProUtf' THEN TotAvailability_Pcnt END)
, @MechUnAvailTA = 100 - SUM(CASE WHEN OppLossId = 'Turnaround' THEN DtAvailability_Pcnt END)
, @MechUnAvailRout = 100 - SUM(CASE WHEN OppLossId = 'Eqf' THEN DtAvailability_Pcnt END)
, @MechUnAvailRoutSlow = 100 - SUM(CASE WHEN OppLossId = 'Eqf' THEN TotAvailability_Pcnt END)
, @OthMechUnAvail = SUM(CASE WHEN OppLossId = 'NOC' THEN TotReliability_Ind END)
from calc.ReliabilityOpLossAvailabilityAggregate
where Refnum = @Refnum and FactorSetId = @FactorSetId
and OppLossId in ('Turnaround', 'TAEqF', 'TAEqFPRO','TAEqFPROUtf', 'EQF','NOC')

DECLARE @MaintOccSWBPerkEDC real, @MaintMpsSWBPerkEDC real, @MaintContractLaborPerkEDC real, @MaintTotMatlPerkEDC real, @MaintEquipPerkEDC real, @MaintRoutMaintPerkEDC real
	, @MaintContractMatlPerkEDC real, @MaintMatlPerkEDC real
	, @MaintOccSWBPerHVChem real, @MaintMpsSWBPerHVChem real, @MaintContractLaborPerHVChem real, @MaintTotMatlPerHVChem real
	, @MaintEquipPerHVChem real, @MaintRoutMaintPerHVChem real, @MaintContractMatlPerHVChem real, @MaintMatlPerHVChem real
	, @MaintOccSWBPerMEI real, @MaintMpsSWBPerMEI real, @MaintContractLaborPerMEI real, @MaintTotMatlPerMEI real
	, @MaintEquipPerMEI real, @MaintRoutMaintPerMEI real, @MaintContractMatlPerMEI real, @MaintMatlPerMEI real
	, @kEdc real, @ProdHVChem_kMT real
SELECT 
  @MaintOccSWBPerkEDC			= SUM(CASE WHEN m.AccountId='OccSWB' THEN m.Amount_Cur ELSE 0 END) / MIN(d.kEdc)
, @MaintMpsSWBPerkEDC			= SUM(CASE WHEN m.AccountId='MpsSWB' THEN m.Amount_Cur ELSE 0 END) / MIN(d.kEdc)
, @MaintContractLaborPerkEDC	= SUM(CASE WHEN m.AccountId in('MaintContractLabor', 'MaintRetubeLabor') THEN m.Amount_Cur ELSE 0 END) / MIN(d.kEdc)
, @MaintTotMatlPerkEDC			= SUM(CASE WHEN m.AccountId in ('MaintContractMatl', 'MaintMatl', 'MaintRetubeMatl') THEN m.Amount_Cur ELSE 0 END) / MIN(d.kEdc)
, @MaintEquipPerkEDC			= SUM(CASE WHEN m.AccountId='MaintEquip' THEN m.Amount_Cur ELSE 0 END) / MIN(d.kEdc)
, @MaintRoutMaintPerkEDC		= SUM(m.Amount_Cur) / MIN(d.kEdc)
, @MaintContractMatlPerkEDC		= SUM(CASE WHEN m.AccountId in ('MaintContractMatl', 'MaintRetubeMatl') THEN m.Amount_Cur ELSE 0 END) / MIN(d.kEdc)
, @MaintMatlPerkEDC				= SUM(CASE WHEN m.AccountId='MaintMatl' THEN m.Amount_Cur ELSE 0 END) / MIN(d.kEdc)
, @MaintOccSWBPerHVChem			= SUM(CASE WHEN m.AccountId='OccSWB' THEN m.Amount_Cur ELSE 0 END) / MIN(d.HvcProd_kMT)
, @MaintMpsSWBPerHVChem			= SUM(CASE WHEN m.AccountId='MpsSWB' THEN m.Amount_Cur ELSE 0 END) / MIN(d.HvcProd_kMT)
, @MaintContractLaborPerHVChem	= SUM(CASE WHEN m.AccountId in('MaintContractLabor','MaintRetubeLabor') THEN m.Amount_Cur ELSE 0 END) / MIN(d.HvcProd_kMT)
, @MaintTotMatlPerHVChem		= SUM(CASE WHEN m.AccountId in ('MaintContractMatl', 'MaintMatl', 'MaintRetubeMatl') THEN m.Amount_Cur ELSE 0 END) / MIN(d.HvcProd_kMT)
, @MaintEquipPerHVChem			= SUM(CASE WHEN m.AccountId='MaintEquip' THEN m.Amount_Cur ELSE 0 END) / MIN(d.HvcProd_kMT)
, @MaintRoutMaintPerHVChem		= SUM(m.Amount_Cur) / MIN(d.HvcProd_kMT)
, @MaintContractMatlPerHVChem	= SUM(CASE WHEN m.AccountId in ('MaintContractMatl','MaintRetubeMatl') THEN m.Amount_Cur ELSE 0 END) / MIN(d.HvcProd_kMT)
, @MaintMatlPerHVChem			= SUM(CASE WHEN m.AccountId='MaintMatl' THEN m.Amount_Cur ELSE 0 END) / MIN(d.HvcProd_kMT)
, @MaintOccSWBPerMEI			= SUM(CASE WHEN m.AccountId='OccSWB' THEN m.Amount_Cur ELSE 0 END) / MIN(d.MEIStd) * 100.0
, @MaintMpsSWBPerMEI			= SUM(CASE WHEN m.AccountId='MpsSWB' THEN m.Amount_Cur ELSE 0 END) / MIN(d.MEIStd) * 100.0
, @MaintContractLaborPerMEI		= SUM(CASE WHEN m.AccountId in('MaintContractLabor','MaintRetubeLabor') THEN m.Amount_Cur ELSE 0 END) / MIN(d.MEIStd) * 100.0
, @MaintTotMatlPerMEI			= SUM(CASE WHEN m.AccountId in ('MaintContractMatl', 'MaintMatl', 'MaintRetubeMatl') THEN m.Amount_Cur ELSE 0 END) / MIN(d.MEIStd) * 100.0
, @MaintEquipPerMEI				= SUM(CASE WHEN m.AccountId='MaintEquip' THEN m.Amount_Cur ELSE 0 END) / MIN(d.MEIStd) * 100.0
, @MaintRoutMaintPerMEI			= SUM(m.Amount_Cur) / MIN(d.MEIStd) * 100.0
, @MaintContractMatlPerMEI		= SUM(CASE WHEN m.AccountId in ('MaintContractMatl','MaintRetubeMatl') THEN m.Amount_Cur ELSE 0 END) / MIN(d.MEIStd) * 100.0
, @MaintMatlPerMEI				= SUM(CASE WHEN m.AccountId='MaintMatl' THEN m.Amount_Cur ELSE 0 END) / MIN(d.MEIStd) * 100.0
FROM calc.MaintExpenseCostCategory m JOIN dbo.Divisors d on d.Refnum=m.Refnum
WHERE m.Refnum = @Refnum and m.FactorSetId = @FactorSetId and m.CurrencyRpt = @Currency
GROUP BY m.Refnum
HAVING MIN(d.HvcProd_kMT)>0 and MIN(d.kEdc)>0


DECLARE @MaintCorrEmergencyPcnt real, @MainCorrProcessPcnt real, @MaintPrevCondPcnt real, @MaintPrevCondAddPcnt real, @MaintPrevSysPcnt real, @MaintPrevSysAddPcnt real
SELECT
  @MaintCorrEmergencyPcnt	= SUM(CASE WHEN m.AccountId='MaintCorrEmergancy' THEN CompMaintHours_Pcnt ELSE 0 END)
, @MainCorrProcessPcnt		= SUM(CASE WHEN m.AccountId='MaintCorrProcess' THEN CompMaintHours_Pcnt ELSE 0 END)
, @MaintPrevCondPcnt		= SUM(CASE WHEN m.AccountId='MaintPrevCond' THEN CompMaintHours_Pcnt ELSE 0 END)
, @MaintPrevCondAddPcnt		= SUM(CASE WHEN m.AccountId='MaintPrevCondAdd' THEN CompMaintHours_Pcnt ELSE 0 END)
, @MaintPrevSysPcnt			= SUM(CASE WHEN m.AccountId='MaintPrevSys' THEN CompMaintHours_Pcnt ELSE 0 END)
, @MaintPrevSysAddPcnt		= SUM(CASE WHEN m.AccountId='MaintPrevSysAdd' THEN CompMaintHours_Pcnt ELSE 0 END)
FROM fact.MaintExpense m 
WHERE m.Refnum=@Refnum

DECLARE @Maint real
SELECT @Maint = @MaintCorrEmergencyPcnt + @MainCorrProcessPcnt + @MaintPrevCondPcnt + @MaintPrevCondAddPcnt + @MaintPrevSysPcnt + @MaintPrevSysAddPcnt

IF @Maint = 0  BEGIN
	SELECT   @MaintCorrEmergencyPcnt	= NULL
	, @MainCorrProcessPcnt		= NULL
	, @MaintPrevCondPcnt		= NULL
	, @MaintPrevCondAddPcnt		= NULL
	, @MaintPrevSysPcnt			= NULL
	, @MaintPrevSysAddPcnt		= NULL
END

IF @Maint > 0  BEGIN
	SELECT   
	  @MaintCorrEmergencyPcnt	= @MaintCorrEmergencyPcnt / @Maint * 100.0
	, @MainCorrProcessPcnt		= @MainCorrProcessPcnt / @Maint * 100.0
	, @MaintPrevCondPcnt		= @MaintPrevCondPcnt / @Maint * 100.0
	, @MaintPrevCondAddPcnt		= @MaintPrevCondAddPcnt / @Maint * 100.0
	, @MaintPrevSysPcnt			= @MaintPrevSysPcnt / @Maint * 100.0
	, @MaintPrevSysAddPcnt		= @MaintPrevSysAddPcnt / @Maint * 100.0
END

DECLARE @FurnRunDaysEthane real, @FurnRunDaysPropane real, @FurnRunDaysNaphtha real, @FurnRunDaysLiqHeavy real
SELECT 
  @FurnRunDaysEthane	= SUM(CASE WHEN p.StreamId='Ethane' THEN p.RunLength_Days ELSE 0 END)
, @FurnRunDaysPropane	= SUM(CASE WHEN p.StreamId='Propane' THEN p.RunLength_Days ELSE 0 END)
, @FurnRunDaysNaphtha	= SUM(CASE WHEN p.StreamId='Naphtha' THEN p.RunLength_Days ELSE 0 END)
, @FurnRunDaysLiqHeavy	= SUM(CASE WHEN p.StreamId='LiqHeavy' THEN p.RunLength_Days ELSE 0 END)
FROM fact.ReliabilityPyroFurnFeed p 
WHERE p.Refnum=@Refnum

DECLARE @FurnTubeLifeYrs real, @FurnRetubeDays real
SELECT
  @FurnTubeLifeYrs 	= FurnTubeLife_Mnths/ 12.0
, @FurnRetubeDays	= FurnRetube_Days
FROM fact.Reliability m 
WHERE m.Refnum=@Refnum

DECLARE @RelInd real
SELECT
@RelInd = ReliabilityIndicator
FROM calc.ReliabilityIndicator ri WHERE ri.Refnum=@Refnum and ri.FactorSetId = @FactorSetId

DECLARE @RMeiEDC real, @RMeiRV real
SELECT
  @RMeiEDC = SUM(m.Indicator_Index)/MIN(d.kEdc)
, @RMeiRV = SUM(m.Indicator_Index)/MIN(d.RV_MUS)/10.0
FROM calc.MeiAggregate m JOIN dbo.Divisors d on m.Refnum=d.Refnum 
WHERE m.Refnum = @Refnum and m.FactorSetId=@FactorSetId and m.MeiId = 'MEI_Total' and m.CurrencyRpt = @Currency
GROUP BY m.Refnum
HAVING MIN(d.kEdc)>0 and MIN(d.RV_MUS) > 0

DELETE FROM dbo.Maint WHERE Refnum = @Refnum
Insert into dbo.Maint (Refnum
, Currency
, RoutIndexEDC
, TAIndexEDC
, MaintIndexEDC
, RoutIndexRV
, TAIndexRV
, MaintIndexRV
, RoutMEI
, TAMEI
, MEI
, RelInd
, MechAvail
, OpAvail
, PlantAvail
, MechAvailSlow
, OpAvailSlow
, PlantAvailSlow
, MechUnAvailTA
, MechUnAvailRout
, MechUnAvailRoutSlow
, OthMechUnAvail
, RMeiEDC
, RMeiRV
, MaintOccSWBPerkEDC
, MaintMpsSWBPerkEDC
, MaintContractLaborPerkEDC
, MaintTotMatlPerkEDC
, MaintEquipPerkEDC
, MaintRoutMaintPerkEDC
, MaintContractMatlPerkEDC
, MaintMatlPerkEDC
, MaintOccSWBPerHVChem
, MaintMpsSWBPerHVChem
, MaintContractLaborPerHVChem
, MaintTotMatlPerHVChem
, MaintEquipPerHVChem
, MaintRoutMaintPerHVChem
, MaintContractMatlPerHVChem
, MaintMatlPerHVChem
, MaintOccSWBPerMEI
, MaintMpsSWBPerMEI
, MaintContractLaborPerMEI
, MaintTotMatlPerMEI
, MaintEquipPerMEI
, MaintRoutMaintPerMEI
, MaintContractMatlPerMEI
, MaintMatlPerMEI
, MaintCorrEmergencyPcnt
, MainCorrProcessPcnt
, MaintPrevCondPcnt
, MaintPrevCondAddPcnt
, MaintPrevSysPcnt
, MaintPrevSysAddPcnt
, FurnRunDaysEthane
, FurnRunDaysPropane
, FurnRunDaysNaphtha
, FurnRunDaysLiqHeavy
, FurnTubeLifeYrs
, FurnRetubeDays
)
Select @Refnum
, @Currency 
, @RoutIndexEDC 
, @TAIndexEDC 
, @MaintIndexEDC 
, @RoutIndexRV 
, @TAIndexRV 
, @MaintIndexRV 
, @RoutMEI
, @TAMEI
, @MEI
, @RelInd 
, MechAvail = @MechAvail
, OpAvail = @OpAvail
, PlantAvail = @PlantAvail
, MechAvailSlow = @MechAvailSlow
, OpAvailSlow = @OpAvailSlow
, PlantAvailSlow = @PlantAvailSlow
, MechUnAvailTA = @MechUnAvailTA
, MechUnAvailRout = @MechUnAvailRout
, MechUnAvailRoutSlow = @MechUnAvailRoutSlow
, OthMechUnAvail = @OthMechUnAvail
, @RMeiEDC 
, @RMeiRV 
, @MaintOccSWBPerkEDC 
, @MaintMpsSWBPerkEDC 
, @MaintContractLaborPerkEDC 
, @MaintTotMatlPerkEDC 
, @MaintEquipPerkEDC 
, @MaintRoutMaintPerkEDC 
, @MaintContractMatlPerkEDC 
, @MaintMatlPerkEDC 
, @MaintOccSWBPerHVChem 
, @MaintMpsSWBPerHVChem 
, @MaintContractLaborPerHVChem 
, @MaintTotMatlPerHVChem 
, @MaintEquipPerHVChem 
, @MaintRoutMaintPerHVChem 
, @MaintContractMatlPerHVChem 
, @MaintMatlPerHVChem 
, @MaintOccSWBPerMEI
, @MaintMpsSWBPerMEI 
, @MaintContractLaborPerMEI
, @MaintTotMatlPerMEI
, @MaintEquipPerMEI 
, @MaintRoutMaintPerMEI
, @MaintContractMatlPerMEI
, @MaintMatlPerMEI
, @MaintCorrEmergencyPcnt 
, @MainCorrProcessPcnt 
, @MaintPrevCondPcnt 
, @MaintPrevCondAddPcnt 
, @MaintPrevSysPcnt 
, @MaintPrevSysAddPcnt 
, @FurnRunDaysEthane 
, @FurnRunDaysPropane 
, @FurnRunDaysNaphtha 
, @FurnRunDaysLiqHeavy 
, @FurnTubeLifeYrs 
, @FurnRetubeDays 


SET NOCOUNT OFF

















