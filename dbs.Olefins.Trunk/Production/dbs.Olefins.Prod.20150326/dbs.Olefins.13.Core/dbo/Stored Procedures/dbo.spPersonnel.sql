﻿

CREATE              PROC [dbo].[spPersonnel](@Refnum varchar(25), @FactorSetId as varchar(12))
AS
SET NOCOUNT ON

DECLARE @OccAbsence_Pcnt real, @MpsAbsence_Pcnt real
SELECT @OccAbsence_Pcnt = pa.Absence_Pcnt
FROM fact.PersAbsence pa WHERE pa.Refnum=@Refnum and pa.PersId = 'OccSubTotal'

SELECT @MpsAbsence_Pcnt = pa.Absence_Pcnt
FROM fact.PersAbsence pa WHERE pa.Refnum=@Refnum and pa.PersId = 'MpsSubTotal'

DELETE FROM dbo.Personnel WHERE Refnum = @Refnum
Insert into dbo.Personnel (Refnum
, DataType
, DivisorValue
, OccProcProc
, OccProcUtil
, OccProcOffSite
, OccProcTruckRail
, OccProcMarine
, OccProcRelief
, OccProcTraining
, OccProcOther
, OccProc
, OccMaintRoutine
, OccMaintTaAnnRetube
, OccMaintTaMaint
, OccMaintInsp
, OccMaintShop
, OccMaintPlan
, OccMaintWarehouse
, OccMaintTraining
, OccMaint
, OccAdminLab
, OccAdminAcct
, OccAdminPurchase
, OccAdminSecurity
, OccAdminClerical
, OccAdminOther
, OccAdmin
, OccPlantSite
, MpsProc
, MpsMaintRoutine
, MpsMaintTaAnnRetube
, MpsMaintTaMaint
, MpsMaintShop
, MpsMaintPlan
, MpsMaintWarehouse
, MpsMaint
, MpsAdminLab
, MpsTechProc
, MpsTechEcon
, MpsTechRely
, MpsTechInsp
, MpsTechAPC
, MpsTechEnviron
, MpsTechOther
, MpsTech
, MpsAdminAcct
, MpsAdminMIS
, MpsAdminPurchase
, MpsAdminSecurity
, MpsAdminOther
, MpsAdmin
, MpsPlantSite
, MpsAdminGenPurchase
, MpsAdminGenOther
, MpsSubTotal
, PlantTotal
, OccAbsence_Pcnt
, MpsAbsence_Pcnt
, MaintRoutine
, MaintTaAnnRetube
, MaintTaMaint
, MaintInsp
, MaintShop
, MaintPlan
, MaintWarehouse
, MaintTraining
, Maint
)

SELECT Refnum = @Refnum
, DataType					= dt.DataType
, DivisorValue				= d.DivisorValue
, OccProcProc = SUM(CASE WHEN p.PersId='OccProcProc' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcUtil = SUM(CASE WHEN p.PersId='OccProcUtil' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcOffSite = SUM(CASE WHEN p.PersId='OccProcOffSite' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcTruckRail = SUM(CASE WHEN p.PersId='OccProcTruckRail' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcMarine = SUM(CASE WHEN p.PersId='OccProcMarine' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcRelief = SUM(CASE WHEN p.PersId='OccProcRelief' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcTraining = SUM(CASE WHEN p.PersId='OccProcTraining' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcOther = SUM(CASE WHEN p.PersId='OccProcOther' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProc = SUM(CASE WHEN p.PersId='OccProc' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintRoutine = SUM(CASE WHEN p.PersId='OccMaintRoutine' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTaAnnRetube = SUM(CASE WHEN p.PersId='OccMaintTaAnnRetube' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTaMaint = SUM(CASE WHEN p.PersId='OccMaintTaMaint' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintInsp = SUM(CASE WHEN p.PersId='OccMaintInsp' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintShop = SUM(CASE WHEN p.PersId='OccMaintShop' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintPlan = SUM(CASE WHEN p.PersId='OccMaintPlan' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintWarehouse = SUM(CASE WHEN p.PersId='OccMaintWarehouse' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTraining = SUM(CASE WHEN p.PersId='OccMaintTraining' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaint = SUM(CASE WHEN p.PersId='OccMaint' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminLab = SUM(CASE WHEN p.PersId='OccAdminLab' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminAcct = SUM(CASE WHEN p.PersId='OccAdminAcct' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminPurchase = SUM(CASE WHEN p.PersId='OccAdminPurchase' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminSecurity = SUM(CASE WHEN p.PersId='OccAdminSecurity' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminClerical = SUM(CASE WHEN p.PersId='OccAdminClerical' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminOther = SUM(CASE WHEN p.PersId='OccAdminOther' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdmin = SUM(CASE WHEN p.PersId='OccAdmin' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccPlantSite = SUM(CASE WHEN p.PersId='OccPlantSite' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsProc = SUM(CASE WHEN p.PersId='MpsProc' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintRoutine = SUM(CASE WHEN p.PersId='MpsMaintRoutine' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintTaAnnRetube = SUM(CASE WHEN p.PersId='MpsMaintTaAnnRetube' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintTaMaint = SUM(CASE WHEN p.PersId='MpsMaintTaMaint' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintShop = SUM(CASE WHEN p.PersId='MpsMaintShop' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintPlan = SUM(CASE WHEN p.PersId='MpsMaintPlan' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintWarehouse = SUM(CASE WHEN p.PersId='MpsMaintWarehouse' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaint = SUM(CASE WHEN p.PersId='MpsMaint' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminLab = SUM(CASE WHEN p.PersId='MpsAdminLab' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechProc = SUM(CASE WHEN p.PersId='MpsTechProc' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechEcon = SUM(CASE WHEN p.PersId='MpsTechEcon' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechRely = SUM(CASE WHEN p.PersId='MpsTechRely' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechInsp = SUM(CASE WHEN p.PersId='MpsTechInsp' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechAPC = SUM(CASE WHEN p.PersId='MpsTechAPC' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechEnviron = SUM(CASE WHEN p.PersId='MpsTechEnviron' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechOther = SUM(CASE WHEN p.PersId='MpsTechOther' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTech = SUM(CASE WHEN p.PersId='MpsTech' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminAcct = SUM(CASE WHEN p.PersId='MpsAdminAcct' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminMIS = SUM(CASE WHEN p.PersId='MpsAdminMIS' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminPurchase = SUM(CASE WHEN p.PersId='MpsAdminPurchase' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminSecurity = SUM(CASE WHEN p.PersId='MpsAdminSecurity' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminOther = SUM(CASE WHEN p.PersId='MpsAdminOther' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdmin = SUM(CASE WHEN p.PersId='MpsAdmin' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsPlantSite = SUM(CASE WHEN p.PersId='MpsPlantSite' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminGenPurchase = SUM(CASE WHEN p.PersId='MpsGenPurchase' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminGenOther = SUM(CASE WHEN p.PersId='MpsGenOther' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsSubTotal = SUM(CASE WHEN p.PersId='MpsSubTotal' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, PlantTotal = SUM(CASE WHEN p.PersId='PlantTotal' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAbsence_Pcnt = @OccAbsence_Pcnt
, MpsAbsence_Pcnt = @MpsAbsence_Pcnt
, MaintRoutine = SUM(CASE WHEN p.PersId LIKE '___MaintRoutine' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTaAnnRetube = SUM(CASE WHEN p.PersId LIKE '___MaintTaAnnRetube' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTaMaint = SUM(CASE WHEN p.PersId LIKE '___MaintTaMaint' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintInsp = SUM(CASE WHEN p.PersId LIKE '___MaintInsp' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintShop = SUM(CASE WHEN p.PersId LIKE '___MaintShop' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintPlan = SUM(CASE WHEN p.PersId LIKE '___MaintPlan' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintWarehouse = SUM(CASE WHEN p.PersId LIKE '___MaintWarehouse' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTraining = SUM(CASE WHEN p.PersId LIKE '___MaintTraining' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, Maint = SUM(CASE WHEN p.PersId LIKE '___Maint' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
	+ SUM(CASE WHEN p.PersId='MpsTechRely' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
	+ SUM(CASE WHEN p.PersId='MpsTechInsp' THEN p.InflTaAdjAnn_Tot_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)

FROM calc.PersAggregateAdj p CROSS APPLY 
	(VALUES ('100EDC',	'kEDCTot',		'kEdc',				10.0), 
			('HVC_MT',	'ProdHVC',		'Production_kMT',	1.0),
			('EthDiv_MT','C2H4',		'Production_kMT',	1.0),
			('mPEI',	'PersMaint',	'UnitTot',			10.0),
			('nmPEI',	'PersNonMaint',	'UnitTot',			10.0),
			('PEI',		'PersTot',		'UnitTot',			10.0),
			('WHrs',	'None',			'None',				1.0)) dt(DataType, DivisorID, DivisorField, Mult)
JOIN calc.Divisors d on d.Refnum=p.Refnum and dt.DivisorID=d.DivisorID and dt.DivisorField=d.DivisorField and d.FactorSetId = p.FactorSetId
WHERE p.FactorSetId=@FactorSetId and p.Refnum=@Refnum
GROUP BY dt.DataType, d.DivisorValue, dt.Mult
UNION
SELECT Refnum = @Refnum
, DataType					= dt.DataType
, DivisorValue				= [$(DbGlobal)].dbo.WtAvg(d.DivisorValue,1)
, OccProcProc = SUM(CASE WHEN p.PersId='OccProcProc' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcUtil = SUM(CASE WHEN p.PersId='OccProcUtil' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcOffSite = SUM(CASE WHEN p.PersId='OccProcOffSite' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcTruckRail = SUM(CASE WHEN p.PersId='OccProcTruckRail' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcMarine = SUM(CASE WHEN p.PersId='OccProcMarine' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcRelief = SUM(CASE WHEN p.PersId='OccProcRelief' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcTraining = SUM(CASE WHEN p.PersId='OccProcTraining' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcOther = SUM(CASE WHEN p.PersId='OccProcOther' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProc = SUM(CASE WHEN p.PersId='OccProc' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintRoutine = SUM(CASE WHEN p.PersId='OccMaintRoutine' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTaAnnRetube = SUM(CASE WHEN p.PersId='OccMaintTaAnnRetube' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTaMaint = SUM(CASE WHEN p.PersId='OccMaintTaMaint' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintInsp = SUM(CASE WHEN p.PersId='OccMaintInsp' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintShop = SUM(CASE WHEN p.PersId='OccMaintShop' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintPlan = SUM(CASE WHEN p.PersId='OccMaintPlan' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintWarehouse = SUM(CASE WHEN p.PersId='OccMaintWarehouse' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTraining = SUM(CASE WHEN p.PersId='OccMaintTraining' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaint = SUM(CASE WHEN p.PersId='OccMaint' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminLab = SUM(CASE WHEN p.PersId='OccAdminLab' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminAcct = SUM(CASE WHEN p.PersId='OccAdminAcct' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminPurchase = SUM(CASE WHEN p.PersId='OccAdminPurchase' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminSecurity = SUM(CASE WHEN p.PersId='OccAdminSecurity' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminClerical = SUM(CASE WHEN p.PersId='OccAdminClerical' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminOther = SUM(CASE WHEN p.PersId='OccAdminOther' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdmin = SUM(CASE WHEN p.PersId='OccAdmin' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccPlantSite = SUM(CASE WHEN p.PersId='OccPlantSite' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsProc = SUM(CASE WHEN p.PersId='MpsProc' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintRoutine = SUM(CASE WHEN p.PersId='MpsMaintRoutine' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintTaAnnRetube = SUM(CASE WHEN p.PersId='MpsMaintTaAnnRetube' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintTaMaint = SUM(CASE WHEN p.PersId='MpsMaintTaMaint' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintShop = SUM(CASE WHEN p.PersId='MpsMaintShop' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintPlan = SUM(CASE WHEN p.PersId='MpsMaintPlan' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintWarehouse = SUM(CASE WHEN p.PersId='MpsMaintWarehouse' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaint = SUM(CASE WHEN p.PersId='MpsMaint' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminLab = SUM(CASE WHEN p.PersId='MpsAdminLab' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechProc = SUM(CASE WHEN p.PersId='MpsTechProc' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechEcon = SUM(CASE WHEN p.PersId='MpsTechEcon' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechRely = SUM(CASE WHEN p.PersId='MpsTechRely' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechInsp = SUM(CASE WHEN p.PersId='MpsTechInsp' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechAPC = SUM(CASE WHEN p.PersId='MpsTechAPC' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechEnviron = SUM(CASE WHEN p.PersId='MpsTechEnviron' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechOther = SUM(CASE WHEN p.PersId='MpsTechOther' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTech = SUM(CASE WHEN p.PersId='MpsTech' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminAcct = SUM(CASE WHEN p.PersId='MpsAdminAcct' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminMIS = SUM(CASE WHEN p.PersId='MpsAdminMIS' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminPurchase = SUM(CASE WHEN p.PersId='MpsAdminPurchase' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminSecurity = SUM(CASE WHEN p.PersId='MpsAdminSecurity' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminOther = SUM(CASE WHEN p.PersId='MpsAdminOther' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdmin = SUM(CASE WHEN p.PersId='MpsAdmin' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsPlantSite = SUM(CASE WHEN p.PersId='MpsPlantSite' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminGenPurchase = SUM(CASE WHEN p.PersId='MpsGenPurchase' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminGenOther = SUM(CASE WHEN p.PersId='MpsGenOther' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsSubTotal = SUM(CASE WHEN p.PersId='MpsSubTotal' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, PlantTotal = SUM(CASE WHEN p.PersId='PlantTotal' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAbsence_Pcnt = @OccAbsence_Pcnt
, MpsAbsence_Pcnt = @MpsAbsence_Pcnt
, MaintRoutine = SUM(CASE WHEN p.PersId LIKE '___MaintRoutine' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTaAnnRetube = SUM(CASE WHEN p.PersId LIKE '___MaintTaAnnRetube' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTaMaint = SUM(CASE WHEN p.PersId LIKE '___MaintTaMaint' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintInsp = SUM(CASE WHEN p.PersId LIKE '___MaintInsp' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintShop = SUM(CASE WHEN p.PersId LIKE '___MaintShop' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintPlan = SUM(CASE WHEN p.PersId LIKE '___MaintPlan' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintWarehouse = SUM(CASE WHEN p.PersId LIKE '___MaintWarehouse' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTraining = SUM(CASE WHEN p.PersId LIKE '___MaintTraining' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, Maint = SUM(CASE WHEN p.PersId LIKE '___Maint' THEN p.InflTaAdjAnn_Company_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
FROM calc.PersAggregateAdj p CROSS APPLY 
	(VALUES ('100EDCComp',	'kEDCTot',	'kEdc',				10.0), 
			('HVC_MTComp',	'ProdHVC',	'Production_kMT',	1.0),
			('WHrsComp',	'None',		'None',				1.0)) dt(DataType, DivisorID, DivisorField, Mult)
JOIN calc.Divisors d on d.Refnum=p.Refnum and dt.DivisorID=d.DivisorID and dt.DivisorField=d.DivisorField and d.FactorSetId = p.FactorSetId
WHERE p.FactorSetId=@FactorSetId and p.Refnum=@Refnum
GROUP BY dt.DataType, d.DivisorValue, dt.Mult
UNION
SELECT Refnum = @Refnum
, DataType					= dt.DataType
, DivisorValue				= [$(DbGlobal)].dbo.WtAvg(d.DivisorValue,1)
, OccProcProc = SUM(CASE WHEN p.PersId='OccProcProc' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcUtil = SUM(CASE WHEN p.PersId='OccProcUtil' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcOffSite = SUM(CASE WHEN p.PersId='OccProcOffSite' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcTruckRail = SUM(CASE WHEN p.PersId='OccProcTruckRail' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcMarine = SUM(CASE WHEN p.PersId='OccProcMarine' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcRelief = SUM(CASE WHEN p.PersId='OccProcRelief' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcTraining = SUM(CASE WHEN p.PersId='OccProcTraining' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProcOther = SUM(CASE WHEN p.PersId='OccProcOther' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccProc = SUM(CASE WHEN p.PersId='OccProc' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintRoutine = SUM(CASE WHEN p.PersId='OccMaintRoutine' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTaAnnRetube = SUM(CASE WHEN p.PersId='OccMaintTaAnnRetube' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTaMaint = SUM(CASE WHEN p.PersId='OccMaintTaMaint' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintInsp = SUM(CASE WHEN p.PersId='OccMaintInsp' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintShop = SUM(CASE WHEN p.PersId='OccMaintShop' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintPlan = SUM(CASE WHEN p.PersId='OccMaintPlan' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintWarehouse = SUM(CASE WHEN p.PersId='OccMaintWarehouse' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaintTraining = SUM(CASE WHEN p.PersId='OccMaintTraining' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccMaint = SUM(CASE WHEN p.PersId='OccMaint' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminLab = SUM(CASE WHEN p.PersId='OccAdminLab' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminAcct = SUM(CASE WHEN p.PersId='OccAdminAcct' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminPurchase = SUM(CASE WHEN p.PersId='OccAdminPurchase' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminSecurity = SUM(CASE WHEN p.PersId='OccAdminSecurity' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminClerical = SUM(CASE WHEN p.PersId='OccAdminClerical' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdminOther = SUM(CASE WHEN p.PersId='OccAdminOther' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAdmin = SUM(CASE WHEN p.PersId='OccAdmin' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccPlantSite = SUM(CASE WHEN p.PersId='OccPlantSite' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsProc = SUM(CASE WHEN p.PersId='MpsProc' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintRoutine = SUM(CASE WHEN p.PersId='MpsMaintRoutine' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintTaAnnRetube = SUM(CASE WHEN p.PersId='MpsMaintTaAnnRetube' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintTaMaint = SUM(CASE WHEN p.PersId='MpsMaintTaMaint' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintShop = SUM(CASE WHEN p.PersId='MpsMaintShop' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintPlan = SUM(CASE WHEN p.PersId='MpsMaintPlan' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaintWarehouse = SUM(CASE WHEN p.PersId='MpsMaintWarehouse' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsMaint = SUM(CASE WHEN p.PersId='MpsMaint' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminLab = SUM(CASE WHEN p.PersId='MpsAdminLab' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechProc = SUM(CASE WHEN p.PersId='MpsTechProc' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechEcon = SUM(CASE WHEN p.PersId='MpsTechEcon' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechRely = SUM(CASE WHEN p.PersId='MpsTechRely' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechInsp = SUM(CASE WHEN p.PersId='MpsTechInsp' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechAPC = SUM(CASE WHEN p.PersId='MpsTechAPC' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechEnviron = SUM(CASE WHEN p.PersId='MpsTechEnviron' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTechOther = SUM(CASE WHEN p.PersId='MpsTechOther' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsTech = SUM(CASE WHEN p.PersId='MpsTech' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminAcct = SUM(CASE WHEN p.PersId='MpsAdminAcct' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminMIS = SUM(CASE WHEN p.PersId='MpsAdminMIS' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminPurchase = SUM(CASE WHEN p.PersId='MpsAdminPurchase' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminSecurity = SUM(CASE WHEN p.PersId='MpsAdminSecurity' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminOther = SUM(CASE WHEN p.PersId='MpsAdminOther' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdmin = SUM(CASE WHEN p.PersId='MpsAdmin' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsPlantSite = SUM(CASE WHEN p.PersId='MpsPlantSite' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminGenPurchase = SUM(CASE WHEN p.PersId='MpsGenPurchase' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsAdminGenOther = SUM(CASE WHEN p.PersId='MpsGenOther' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MpsSubTotal = SUM(CASE WHEN p.PersId='MpsSubTotal' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, PlantTotal = SUM(CASE WHEN p.PersId='PlantTotal' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, OccAbsence_Pcnt = @OccAbsence_Pcnt
, MpsAbsence_Pcnt = @MpsAbsence_Pcnt
, MaintRoutine = SUM(CASE WHEN p.PersId LIKE '___MaintRoutine' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTaAnnRetube = SUM(CASE WHEN p.PersId LIKE '___MaintTaAnnRetube' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTaMaint = SUM(CASE WHEN p.PersId LIKE '___MaintTaMaint' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintInsp = SUM(CASE WHEN p.PersId LIKE '___MaintInsp' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintShop = SUM(CASE WHEN p.PersId LIKE '___MaintShop' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintPlan = SUM(CASE WHEN p.PersId LIKE '___MaintPlan' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintWarehouse = SUM(CASE WHEN p.PersId LIKE '___MaintWarehouse' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, MaintTraining = SUM(CASE WHEN p.PersId LIKE '___MaintTraining' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
, Maint = SUM(CASE WHEN p.PersId LIKE '___Maint' THEN p.InflTaAdjAnn_Contract_Hrs / d.DivisorValue / dt.Mult ELSE 0 END)
FROM calc.PersAggregateAdj p CROSS APPLY 
	(VALUES ('100EDCCont',	'kEDCTot',	'kEdc',				10.0), 
			('HVC_MTCont',	'ProdHVC',	'Production_kMT',	1.0),
			('WHrsCont',	'None',		'None',				1.0)) dt(DataType, DivisorID, DivisorField, Mult)
JOIN calc.Divisors d on d.Refnum=p.Refnum and dt.DivisorID=d.DivisorID and dt.DivisorField=d.DivisorField and d.FactorSetId = p.FactorSetId
WHERE p.FactorSetId=@FactorSetId and p.Refnum=@Refnum
GROUP BY dt.DataType, d.DivisorValue, dt.Mult

SET NOCOUNT OFF


















