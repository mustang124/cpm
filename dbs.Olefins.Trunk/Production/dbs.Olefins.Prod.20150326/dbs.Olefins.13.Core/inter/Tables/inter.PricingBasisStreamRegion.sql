﻿CREATE TABLE [inter].[PricingBasisStreamRegion] (
    [FactorSetId]         VARCHAR (12)       NOT NULL,
    [RegionId]            VARCHAR (5)        NOT NULL,
    [CalDateKey]          INT                NOT NULL,
    [CurrencyRpt]         VARCHAR (4)        NOT NULL,
    [StreamId]            VARCHAR (42)       NOT NULL,
    [Raw_Amount_Cur]      REAL               NOT NULL,
    [Adj_MatlBal_Cur]     REAL               NULL,
    [_MatlBal_Amount_Cur] AS                 (CONVERT([real],[Raw_Amount_Cur]+coalesce([Adj_MatlBal_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [_Report_Amount_Cur]  AS                 (CONVERT([real],[Raw_Amount_Cur],(1))) PERSISTED NOT NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_PricingBasisStreamRegion_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_PricingBasisStreamRegion_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_PricingBasisStreamRegion_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_PricingBasisStreamRegion_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingBasisStreamRegion] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [CurrencyRpt] ASC, [RegionId] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingBasisStreamRegion_Raw_Amount_Cur] CHECK ([Raw_Amount_Cur]>=(0.0)),
    CONSTRAINT [FK_PricingBasisStreamRegion_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingBasisStreamRegion_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_PricingBasisStreamRegion_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_PricingBasisStreamRegion_Region_LookUp] FOREIGN KEY ([RegionId]) REFERENCES [dim].[Region_LookUp] ([RegionId]),
    CONSTRAINT [FK_PricingBasisStreamRegion_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER inter.t_PricingBasisStreamRegion_u
	ON inter.PricingBasisStreamRegion
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.PricingBasisStreamRegion
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.PricingBasisStreamRegion.FactorSetId		= INSERTED.FactorSetId
		AND inter.PricingBasisStreamRegion.RegionId			= INSERTED.RegionId
		AND inter.PricingBasisStreamRegion.CalDateKey		= INSERTED.CalDateKey
		AND inter.PricingBasisStreamRegion.StreamId			= INSERTED.StreamId
		AND inter.PricingBasisStreamRegion.CurrencyRpt		= INSERTED.CurrencyRpt;

END;