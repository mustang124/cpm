﻿CREATE TABLE [inter].[DivisorsProduction] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Refnum]         VARCHAR (25)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [Production_kMT] REAL               NOT NULL,
    [Loss_kMT]       REAL               NULL,
    [_Total_kMT]     AS                 (CONVERT([real],[Production_kMT]+isnull([Loss_kMT],(0.0)),(1))) PERSISTED NOT NULL,
    [_Loss_Pcnt]     AS                 (CONVERT([real],([Loss_kMT]/[Production_kMT])*(100.0),(1))) PERSISTED,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_DivisorsProduction_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_DivisorsProduction_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_DivisorsProduction] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_DivisorsProduction_Loss_kMT] CHECK ([Loss_kMT]>=(0.0)),
    CONSTRAINT [CR_DivisorsProduction_Loss_Pcnt] CHECK ([_Loss_Pcnt]>=(0.0) AND [_Loss_Pcnt]<=(100.0)),
    CONSTRAINT [CR_DivisorsProduction_Production_kMT] CHECK ([Production_kMT]>=(0.0)),
    CONSTRAINT [CR_DivisorsProduction_Total_kMT] CHECK ([_Total_kMT]>=(0.0) AND [_Total_kMT]>=[Production_kMT]),
    CONSTRAINT [FK_DivisorsProduction_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_DivisorsProduction_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_DivisorsProduction_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_DivisorsProduction_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER inter.t_DivisorsProduction_u
	ON inter.DivisorsProduction
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE inter.DivisorsProduction
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	inter.DivisorsProduction.FactorSetId		= INSERTED.FactorSetId
		AND inter.DivisorsProduction.Refnum			= INSERTED.Refnum
		AND inter.DivisorsProduction.CalDateKey		= INSERTED.CalDateKey
		AND inter.DivisorsProduction.ComponentId		= INSERTED.ComponentId;

END;