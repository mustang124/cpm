﻿Create VIEW reports.Furnaces AS
Select
  GroupId = r.GroupId
, FurnAvail				= [$(DbGlobal)].dbo.WtAvg(f.FurnAvail, F.FeedCapMTD)
, AdjFurnOnstream		= [$(DbGlobal)].dbo.WtAvg(f.AdjFurnOnstream, F.FeedCapMTD)
, OperDays				= [$(DbGlobal)].dbo.WtAvg(f.OperDays, F.FeedCapMTD)
, ActCap				= [$(DbGlobal)].dbo.WtAvg(f.ActCap, F.FeedCapMTD)
, AdjFurnUtil			= [$(DbGlobal)].dbo.WtAvg(f.AdjFurnUtil, F.FeedCapMTD)
, FurnSlowD				= [$(DbGlobal)].dbo.WtAvg(f.FurnSlowD, F.FeedCapMTD)
FROM reports.GroupPlants r JOIN dbo.Furnaces f on f.Refnum=r.Refnum
GROUP by r.GroupId
