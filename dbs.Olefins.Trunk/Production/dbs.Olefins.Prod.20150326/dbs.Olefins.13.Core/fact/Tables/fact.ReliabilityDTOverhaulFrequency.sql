﻿CREATE TABLE [fact].[ReliabilityDTOverhaulFrequency] (
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [FacilityId]      VARCHAR (42)       NOT NULL,
    [Frequency_Years] REAL               NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityDTOverhaulFrequency_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTOverhaulFrequency_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTOverhaulFrequency_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityDTOverhaulFrequency_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityDTOverhaulFrequency] PRIMARY KEY CLUSTERED ([Refnum] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReliabilityDTOverhaulFrequency_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityDTOverhaulFrequency_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_ReliabilityDTOverhaulFrequency_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_ReliabilityDTOverhaulFrequency_u]
	ON [fact].[ReliabilityDTOverhaulFrequency]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[ReliabilityDTOverhaulFrequency]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[ReliabilityDTOverhaulFrequency].Refnum		= INSERTED.Refnum
		AND [fact].[ReliabilityDTOverhaulFrequency].FacilityId	= INSERTED.FacilityId
		AND [fact].[ReliabilityDTOverhaulFrequency].CalDateKey	= INSERTED.CalDateKey;

END;