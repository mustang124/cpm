﻿CREATE TABLE [fact].[Maint] (
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [FacilityId]        VARCHAR (42)       NOT NULL,
    [CurrencyRpt]       VARCHAR (4)        NOT NULL,
    [MaintMaterial_Cur] REAL               NULL,
    [MaintLabor_Cur]    REAL               NULL,
    [_Maint_Cur]        AS                 (CONVERT([real],isnull([MaintMaterial_Cur],(0.0))+isnull([MaintLabor_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [TaMaterial_Cur]    REAL               NULL,
    [TaLabor_Cur]       REAL               NULL,
    [_TA_Cur]           AS                 (CONVERT([real],isnull([TaMaterial_Cur],(0.0))+isnull([TaLabor_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [_Material_Cur]     AS                 (CONVERT([real],isnull([MaintMaterial_Cur],(0.0))+isnull([TaMaterial_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [_Labor_Cur]        AS                 (CONVERT([real],isnull([MaintLabor_Cur],(0.0))+isnull([TaLabor_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [_Tot_Cur]          AS                 (CONVERT([real],((isnull([MaintMaterial_Cur],(0.0))+isnull([MaintLabor_Cur],(0.0)))+isnull([TaMaterial_Cur],(0.0)))+isnull([TaLabor_Cur],(0.0)),(1))) PERSISTED NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_Maint_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_Maint_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_Maint_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_Maint_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_Maint] PRIMARY KEY CLUSTERED ([Refnum] ASC, [CurrencyRpt] ASC, [FacilityId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_Maint_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_Maint_Currency_LookUp] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_Maint_Facility_LookUp] FOREIGN KEY ([FacilityId]) REFERENCES [dim].[Facility_LookUp] ([FacilityId]),
    CONSTRAINT [FK_Maint_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [fact].[t_Maint_u]
	ON [fact].[Maint]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [fact].[Maint]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[fact].[Maint].Refnum		= INSERTED.Refnum
		AND [fact].[Maint].CurrencyRpt	= INSERTED.CurrencyRpt
		AND [fact].[Maint].FacilityId	= INSERTED.FacilityId
		AND [fact].[Maint].CalDateKey	= INSERTED.CalDateKey;

END;