﻿CREATE PROCEDURE [fact].[Insert_CompositionQuantity_ProdQuality]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		-- Table 3 (QualityWt)
		INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId, Component_WtPcnt)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, etl.ConvStreamID(q.FeedProdID)						[StreamId]
			, q.FeedProdID											[StreamDescription]
			
			, CASE q.FeedProdID
				WHEN 'Hydrogen'			THEN 'H2'
				WHEN 'FuelGasSales'		THEN 'CH4'
				END													[ComponentId]
			
			, CASE q.FeedProdID
				WHEN 'Hydrogen'			THEN calc.ConvMoleWeight('H2', q.MolePcnt, 'W')
				WHEN 'FuelGasSales'		THEN calc.ConvMoleWeight('CH4', q.MolePcnt, 'W')
				END													[WtPcnt]

		FROM stgFact.ProdQuality q
		INNER JOIN stgFact.TSort t
			ON	t.Refnum = q.Refnum
		INNER JOIN stgFact.Quantity v
			ON	v.Refnum = q.Refnum
			AND v.FeedProdID = q.FeedProdID
			AND (ISNULL(v.Q1Feed, 0.0) + ISNULL(v.Q2Feed, 0.0) + ISNULL(v.Q3Feed, 0.0) + ISNULL(v.Q4Feed, 0.0)) > 0.0
		WHERE q.MolePcnt > 0.0
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

		-- Table 3 (ProdQuality)
		INSERT INTO fact.CompositionQuantity(Refnum, CalDateKey, StreamId, StreamDescription, ComponentId,  Component_WtPcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvStreamID(u.StreamId)								[StreamId]
			, u.StreamId												[StreamDescription]
			, u.ComponentId											[ComponentId]
			, u.WtPcnt													[WtPcnt]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			
				, CASE q.FeedProdID
					WHEN 'PPCFuel_CH4'		THEN 'PPCFuel'
					WHEN 'PPCFuel_ETH'		THEN 'PPCFuel'
					--WHEN 'PPCFuel_H2'		THEN 'PPCFuel'
					WHEN 'PPCFuel_Inerts'	THEN 'PPCFuel'
					WHEN 'PPCFuel_Other'	THEN 'PPCFuel'
					ELSE q.FeedProdID
					END												[StreamId]
				
				, CASE q.FeedProdID
					WHEN 'Ethylene'			THEN 'C2H4'
					WHEN 'EthyleneCG'		THEN 'C2H4'
					WHEN 'PPCFuel'			THEN 'H2'
					WHEN 'PPCFuel_CH4'		THEN 'CH4'
					WHEN 'PPCFuel_ETH'		THEN 'C2H4'
					--WHEN 'PPCFuel_H2'		THEN 'H2'
					WHEN 'PPCFuel_Inerts'	THEN 'Inerts'
					WHEN 'PPCFuel_Other'	THEN 'Other'
					WHEN 'PropaneC3'		THEN 'C3H8'
					WHEN 'Propylene'		THEN 'C3H6'
					WHEN 'PropyleneCG'		THEN 'C3H6'
					WHEN 'PropyleneRG'		THEN 'C3H6'
					END												[ComponentId]
				
				, ISNULL(q.WtPcnt, q.H2Content) * 
					CASE WHEN ISNULL(q.WtPcnt, q.H2Content) <= 1.0
						THEN 100.0
						ELSE 1.0
						END											[WtPcnt]
			
			FROM stgFact.ProdQuality q
			INNER JOIN stgFact.TSort t
				ON	t.Refnum = q.Refnum
			INNER JOIN stgFact.Quantity v
				ON	v.Refnum = q.Refnum
				AND v.FeedProdID = CASE q.FeedProdID
									WHEN 'PPCFuel_CH4'		THEN 'PPCFuel'
									WHEN 'PPCFuel_ETH'		THEN 'PPCFuel'
									--WHEN 'PPCFuel_H2'		THEN 'PPCFuel'
									WHEN 'PPCFuel_Other'	THEN 'PPCFuel'
									WHEN 'PPCFuel_Inerts'	THEN 'PPCFuel'
									ELSE q.FeedProdID
									END	
				AND (ISNULL(v.AnnFeedProd, 0.0) + ISNULL(v.Q1Feed, 0.0) + ISNULL(v.Q2Feed, 0.0) + ISNULL(v.Q3Feed, 0.0) + ISNULL(v.Q4Feed, 0.0)) <> 0.0

			WHERE (q.WtPcnt > 0.0 OR q.H2Content > 0.0)
				AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			)u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;