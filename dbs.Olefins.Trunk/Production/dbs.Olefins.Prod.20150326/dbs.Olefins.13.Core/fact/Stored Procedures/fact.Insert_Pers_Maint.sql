﻿CREATE PROCEDURE [fact].[Insert_Pers_Maint]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.Pers(Refnum, CalDateKey, PersId, StraightTime_Hrs)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, etl.ConvPersID(u.PersId)
			, u.StraightTime_Hrs
		FROM (
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')				[Refnum]
				, etl.ConvDateKey(t.StudyYear - 1)							[CalDateKey]
				, m.OccMaintST_PY [OM]
				, m.MpsMaintST_PY [MM]
			FROM stgFact.MaintMisc m
			INNER JOIN stgFact.TSort t ON t.Refnum = m.Refnum
			WHERE etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) p
			UNPIVOT(
			StraightTime_Hrs FOR PersId IN(
				  p.OM
				, p.MM
				)
			)u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;