﻿CREATE PROCEDURE [fact].[Insert_FacilitiesFracFeed]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.FacilitiesFeedFrac(Refnum, CalDateKey, FacilityId, StreamId
				, FeedRate_kBsd, FeedProcessed_kMT, FeedDensity_SG)
		SELECT
			  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
			, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
			, 'FracFeed'											[FacilityId]
			, etl.ConvFacilityStreamID(f.FracFeed)					[StreamId]
			, f.FracFeedKBSD										[KBSD]
			, f.FracFeedProcMT										[kMT]
			, f.SG_FracFeedProc										[SG]
		FROM stgFact.Facilities f
		INNER JOIN stgFact.TSort t ON t.Refnum = f.Refnum
		WHERE (f.FracFeedKBSD <> 0.0
			OR f.FracFeedProcMT <> 0.0
			OR etl.ConvFacilityStreamID(f.FracFeed) IS NOT NULL)
		AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;