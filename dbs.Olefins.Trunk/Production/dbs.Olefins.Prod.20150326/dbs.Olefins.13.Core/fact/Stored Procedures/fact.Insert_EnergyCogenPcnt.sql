﻿CREATE PROCEDURE [fact].[Insert_EnergyCogenPcnt]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @Refnum + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;
	
	BEGIN TRY

		INSERT INTO fact.EnergyCogenPcnt(Refnum, CalDateKey, FacilityId, AccountId, CogenEnergy_Pcnt)
		SELECT
			  u.Refnum
			, u.CalDateKey
			, u.FacilityId
			, u.AccountId
			, u.Pcnt * 100.0	[Pcnt]
		FROM(
			SELECT
				  etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')			[Refnum]
				, etl.ConvDateKey(t.StudyYear)							[CalDateKey]
				, e.EnergyType											[FacilityId]
				, e.ElecPowerPct										[PurElec]
				, e.PurchaseSteamPct									[PurSteam]
			FROM stgFact.EnergyQnty e
			INNER JOIN stgFact.TSort t		ON	t.Refnum = e.Refnum
			WHERE e.EnergyType = 'Cogen'
			AND etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH') = @Refnum
			) u
			UNPIVOT(
			Pcnt FOR AccountId IN (
				  [PurElec]
				, [PurSteam]
				)
			) u;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;