﻿CREATE VIEW [ante].[EngrConstEstimate]
WITH SCHEMABINDING
AS
SELECT
	e.FactorSetId,
	e.CurrencyRpt,
	e.ComponentId,
	i.BaseYear,
	i.StudyYear,
	i._InflationFactor				[InflationFactor],
	e.ENC_Cur,
	e.ENC_Cur * i._InflationFactor	[Escalated_ENC_Cur]
FROM ante.EngrConstEstimateBase	e
INNER JOIN ante.InflationFactor	i
	ON	i.FactorSetId = e.FactorSetId
	AND	i.CurrencyRpt = e.CurrencyRpt
	AND	i.BaseYear = e.BaseYear;
GO
CREATE UNIQUE CLUSTERED INDEX [UX_EngrConstEstimate]
    ON [ante].[EngrConstEstimate]([FactorSetId] DESC, [CurrencyRpt] ASC, [ComponentId] ASC, [BaseYear] DESC);

