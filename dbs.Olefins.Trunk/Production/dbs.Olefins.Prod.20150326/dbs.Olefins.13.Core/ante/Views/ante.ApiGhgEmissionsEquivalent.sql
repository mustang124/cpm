﻿CREATE VIEW [ante].[ApiGhgEmissionsEquivalent]
WITH SCHEMABINDING
AS
SELECT
	e.FactorSetId,
	e.CountryId,
	SUM(c.Coefficient * e.Emissions_MTMWHr) * 12.0 / 44.0	[CO2Emissions_MTMWHr]
FROM [ante].[ApiGhgEmissions]				e
LEFT OUTER JOIN [ante].[ApiGhgCoefficient]	c
	ON	c.FactorSetId = e.FactorSetId
	AND	c.ComponentId = e.ComponentId
GROUP BY
	e.FactorSetId,
	e.CountryId;
