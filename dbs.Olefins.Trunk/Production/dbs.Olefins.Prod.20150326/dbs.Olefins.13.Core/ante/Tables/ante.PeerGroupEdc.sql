﻿CREATE TABLE [ante].[PeerGroupEdc] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [PeerGroup]      TINYINT            NOT NULL,
    [LimitLower_Edc] REAL               NOT NULL,
    [LimitUpper_Edc] REAL               NOT NULL,
    [_LimitRange]    AS                 (((('['+CONVERT([varchar](32),round([LimitLower_Edc],(2)),(0)))+', ')+CONVERT([varchar](32),round([LimitUpper_Edc],(2)),(0)))+')') PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PeerGroupEdc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupEdc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PeerGroupEdc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PeerGroupEdc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PeerGroupEdc] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [PeerGroup] ASC),
    CONSTRAINT [CV_PeerGroupEdc_Limit] CHECK ([LimitLower_Edc]<[LimitUpper_Edc]),
    CONSTRAINT [FK_PeerGroupEdc_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_PeerGroupEdc_u]
	ON [ante].[PeerGroupEdc]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PeerGroupEdc]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PeerGroupEdc].FactorSetId	= INSERTED.FactorSetId
		AND	[ante].[PeerGroupEdc].PeerGroup		= INSERTED.PeerGroup

END;