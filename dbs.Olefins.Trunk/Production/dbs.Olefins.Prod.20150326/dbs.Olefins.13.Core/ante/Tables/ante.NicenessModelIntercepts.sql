﻿CREATE TABLE [ante].[NicenessModelIntercepts] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [Yield]          REAL               NOT NULL,
    [Energy]         REAL               NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_NicenessModelIntercepts_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_NicenessModelIntercepts_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_NicenessModelIntercepts_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_NicenessModelIntercepts_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_NicenessModelIntercepts] PRIMARY KEY CLUSTERED ([FactorSetId] ASC),
    CONSTRAINT [FK_NicenessModelIntercepts_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_NicenessModelIntercepts_u]
	ON [ante].[NicenessModelIntercepts]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[NicenessModelIntercepts]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[NicenessModelIntercepts].FactorSetId		= INSERTED.FactorSetId;

END;
