﻿CREATE TABLE [ante].[PricingNetHeatingValueComposition] (
    [FactorSetId]    VARCHAR (12)       NOT NULL,
    [CalDateKey]     INT                NOT NULL,
    [ComponentId]    VARCHAR (42)       NOT NULL,
    [NHValue_MBtulb] REAL               NOT NULL,
    [_NHValue_GJkMT] AS                 (CONVERT([real],([NHValue_MBtulb]*(1.05505585))/(2204.62006305892),(1))) PERSISTED NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_PricingNetHeatingValueComposition_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_PricingNetHeatingValueComposition_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_PricingNetHeatingValueComposition_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_PricingNetHeatingValueComposition_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_PricingNetHeatingValueComposition] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CR_PricingNetHeatingValueComposition_LHValue_MBtulb] CHECK ([NHValue_MBtulb]>=(0.0)),
    CONSTRAINT [FK_PricingNetHeatingValueComposition_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_PricingNetHeatingValueComposition_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_PricingNetHeatingValueComposition_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId])
);


GO

CREATE TRIGGER [ante].[t_PricingNetHeatingValueComposition_u]
	ON [ante].[PricingNetHeatingValueComposition]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [ante].[PricingNetHeatingValueComposition]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[ante].[PricingNetHeatingValueComposition].FactorSetId		= INSERTED.FactorSetId
		AND	[ante].[PricingNetHeatingValueComposition].CalDateKey		= INSERTED.CalDateKey
		AND	[ante].[PricingNetHeatingValueComposition].ComponentId		= INSERTED.ComponentId;

END;