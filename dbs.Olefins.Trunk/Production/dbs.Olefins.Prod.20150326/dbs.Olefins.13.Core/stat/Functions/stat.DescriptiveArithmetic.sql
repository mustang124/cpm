﻿
CREATE FUNCTION [stat].[DescriptiveArithmetic]
(
	--	http://en.wikipedia.org/wiki/Portal:Statistics
	--	http://en.wikipedia.org/wiki/Summary_statistic
	--	http://en.wikipedia.org/wiki/Descriptive_statistics
	
	@Population			stat.PopulationRandomVar		READONLY,							--	Data points to examine
	@Confidence_Pcnt	FLOAT				= 90.0,											--	Expected confidence/prediction interval percent	[0, 100]
	@CapMin				FLOAT				= NULL,											--	Minimum reporting values to average
	@CapMax				FLOAT				= NULL											--	Maximum reporting values to average
)
RETURNS @ReturnTable TABLE
(
	[xCount]			SMALLINT			NOT	NULL	CHECK([xCount]	>= 1),				--	Item Count
	[xMin]				FLOAT				NOT	NULL,										--	Minimum
	[xMax]				FLOAT				NOT	NULL,										--	Maximum
	[_xRange]			AS CAST([xMax] - [xMin]			AS FLOAT)							--	Range
						PERSISTED			NOT	NULL,

	[xMean]				FLOAT				NOT	NULL,										--	Mean								http://en.wikipedia.org/wiki/Mean
	[xStDevP]			FLOAT				NOT	NULL	CHECK([xStDevP]	>= 0.0),			--	Standard Deviation			σ		http://en.wikipedia.org/wiki/Standard_deviation
	[xVarP]				FLOAT				NOT	NULL	CHECK([xVarP]	>= 0.0),			--	Variance					σ²		http://en.wikipedia.org/wiki/Variance

	[_ErrorStandard]	AS CAST([xStDevP] / SQRT([xCount]) AS FLOAT)						--	Standard Error						http://en.wikipedia.org/wiki/Z-test
						PERSISTED			NOT	NULL,
	[_xCoeffVarP]		AS CAST(CASE WHEN [xMean] <> 0.0 THEN [xStDevP]	/ [xMean] END AS FLOAT)	--  Coefficient of Variation		http://en.wikipedia.org/wiki/Coefficient_of_variation
						PERSISTED					,
	[_xIdxDispP]		AS CAST(CASE WHEN [xMean] <> 0.0 THEN [xVarP]	/ [xMean] END AS FLOAT)	--  Index of Dispersion				http://en.wikipedia.org/wiki/Variance-to-mean_ratio
						PERSISTED					,

	[xSum]				FLOAT				NOT	NULL,										--	Sum	of x					Σ(x)
	[xSumXiX]			FLOAT				NOT	NULL,										--	Sum of Error				Σ(xi-AVG(x))

	[wMean]				FLOAT					NULL,										--	Weighted Mean
	[xwDiv]				FLOAT					NULL,										--	SUM(x) / SUM(w)

	[xLimitLower]		FLOAT					NULL,										--	Minimum (Mean of smallest two items, >= @CapMin)
	[xLimitUpper]		FLOAT					NULL,										--	Minimum (Mean of largest two items, <= @CapMax)
	[_xLimitRange]		AS CAST([xLimitUpper] - [xLimitLower]	AS FLOAT)					--	Range
						PERSISTED					,

	[tTest]				FLOAT				NOT	NULL	CHECK([tTest]	>= 0.0),			--	Calcualted T Test
	[tConf]				FLOAT				NOT	NULL	CHECK([tConf]	>= 0.0),			--	Calculated Confidence
	[tAlpha]			FLOAT				NOT	NULL	CHECK([tAlpha]	>= 0.0),			--	Alpha' value						http://en.wikipedia.org/wiki/Student's_t-test
	[tStat]				FLOAT				NOT	NULL	CHECK([tStat]	>= 0.0),			--	Student T Statistic
	[_tHypo]			AS CAST(CASE WHEN [tStat] < [tTest] THEN 1 ELSE 0 END AS BIT)		--	Null hypothesis test 1 (Accept) or 0 (Reject)
						PERSISTED			NOT	NULL,

	[iConfidence]		FLOAT				NOT	NULL	CHECK([iConfidence]	>= 0.0),		--	Confidence Interval					http://en.wikipedia.org/wiki/Confidence_interval
	[iPrediction]		FLOAT				NOT	NULL	CHECK([iPrediction]	>= 0.0),		--	Prediction Interval					http://en.wikipedia.org/wiki/Prediction_interval
	[_iPredictionLower]	AS CAST([xMean] - [iPrediction] AS FLOAT)							--	Lower Prediction Limit
						PERSISTED			NOT	NULL,
	[_iConfidenceLower]	AS CAST([xMean] - [iConfidence] AS FLOAT)							--	Lower Confidence Limit
						PERSISTED			NOT	NULL,
	[_iConfidenceUpper]	AS CAST([xMean] + [iConfidence] AS FLOAT)							--	Upper Confidence Limit
						PERSISTED			NOT	NULL,
	[_iPredictionUpper]	AS CAST([xMean] + [iPrediction] AS FLOAT)							--	Upper Prediction Limit
						PERSISTED			NOT	NULL,

	[xKurtosis]			FLOAT					NULL,										--	Pearson's sample excess Kurtosis	http://en.wikipedia.org/wiki/Kurtosis
	[xSkewness]			FLOAT					NULL,										--	Skewness (negative leans right)		http://en.wikipedia.org/wiki/Skewness

	CHECK([xMin] <= [xMax]),
	CHECK([xMean] <= [xMax]),
	CHECK([xMin] <= [xLimitLower]),
	CHECK([xMax] >= [xLimitUpper]),
	CHECK([iConfidence] <= [iPrediction])
)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE	@xCount			FLOAT;
	DECLARE @xMin			FLOAT;
	DECLARE @xMax			FLOAT;
	DECLARE @xSum			FLOAT;
	DECLARE @xMean			FLOAT;
	DECLARE @xStDevP		FLOAT;
	DECLARE @xVarP			FLOAT;
	DECLARE @xSumXiX		FLOAT;

	DECLARE @wSum			FLOAT;
	DECLARE @wMean			FLOAT;
	DECLARE @xwDiv			FLOAT;

	DECLARE @xLimitLower	FLOAT;
	DECLARE @xLimitUpper	FLOAT;

	DECLARE @xKurtosis		FLOAT;
	DECLARE @xSkewness		FLOAT;

	SELECT
		@xCount		= COUNT(1),
		@xMin		= MIN(o.x),
		@xMax		= MAX(o.x),
		@xSum		= SUM(o.x),
		@xMean		= AVG(o.x),
		@xStDevP	= STDEVP(o.x),
		@xVarP		= VARP(o.x),
		@wSum		= SUM(o.w),
		@wMean		= CASE WHEN SUM(o.w) <> 0.0 THEN SUM(o.x * o.w) / SUM(o.w) END
	FROM @Population o
	WHERE o.Basis = 1;

	IF (@xCount = 0) RETURN;

	SELECT
		@xwDiv		= CASE WHEN @wSum <> 0.0 THEN @xSum / @wSum END,
		@xSumXiX	= SUM(SQUARE(o.x - @xMean)),
		@xKurtosis	= CASE WHEN SUM(SQUARE(o.x - @xMean)) <> 0.0 THEN
						(1.0 / @xCount) * SUM(POWER(o.x - @xMean, 4)) / SQUARE((1.0 / @xCount) * SUM(SQUARE(o.x - @xMean))) - 3.0
						END,
		@xSkewness	= CASE WHEN SUM(SQUARE(o.x - @xMean)) <> 0.0 AND (@xCount - 2.0) <> 0.0 THEN 
						(1.0 / @xCount) * SUM(POWER(o.x - @xMean, 3)) / POWER((1.0 / @xCount) * SUM(SQUARE(o.x - @xMean)), 3.0 / 2.0)
						* SQRT(@xCount * (@xCount - 1.0)) / (@xCount - 2.0)
						END
	FROM @Population o
	WHERE o.Basis = 1;

	SELECT
		@xLimitLower = AVG(t.x)
	FROM (
		SELECT TOP 2
			o.x
		FROM @Population o
		WHERE	o.x BETWEEN COALESCE(@CapMin, o.x) AND COALESCE(@CapMax, o.x)
			AND	o.Basis = 1
		ORDER BY o.x ASC
		) t

	SELECT
		@xLimitUpper = AVG(t.x)
	FROM (
		SELECT TOP 2
			o.x
		FROM @Population o
		WHERE	o.x BETWEEN COALESCE(@CapMin, o.x) AND COALESCE(@CapMax, o.x)
			AND	o.Basis = 1
		ORDER BY o.x DESC
		) t

	DECLARE @tTest			FLOAT = CASE WHEN @xStDevP <> 0.0	THEN ABS(@xMean) / @xStDevP								ELSE 0.0 END;	-- * SQRT(@xCount);
	DECLARE	@tConf			FLOAT = CASE WHEN @tTest <> 0.0		THEN stat.Student_Probability(@xCount - 1, @tTest, 2)	ELSE 0.0 END
	DECLARE @tAlpha			FLOAT = 50 + @Confidence_Pcnt / 2.0;	
	DECLARE @tStat			FLOAT = stat.Student_Statistic(@xCount - 1, @tAlpha, 2);

	DECLARE @iConfidence	FLOAT = @tStat * @xStDevP / SQRT(@xCount);
	DECLARE @iPrediction	FLOAT = @tStat * @xStDevP * SQRT(1.0 + 1.0/@xCount);

	INSERT INTO @ReturnTable(
		xCount,
		xMin, xMax, xMean, xStDevP, xVarP, xSum, xSumXiX,
		wMean,
		xwDiv,
		xLimitLower, xLimitUpper,
		tTest, tConf, tAlpha, tStat,
		iConfidence, iPrediction,
		xKurtosis, xSkewness)
	SELECT
		@xCount,

		@xMin,
		@xMax,
		@xMean,
		@xStDevP,
		@xVarP,
		@xSum,
		@xSumXiX,

		@wMean,
		@xwDiv,

		@xLimitLower,
		@xLimitUpper,

		@tTest,
		@tConf,
		@tAlpha,
		@tStat,

		@iConfidence,
		@iPrediction,

		@xKurtosis,
		@xSkewness
	
	RETURN;

END;