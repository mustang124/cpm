﻿CREATE TABLE [calc].[ReliabilityOpLossAvailability] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [OppLossId]       VARCHAR (42)       NOT NULL,
    [DownTimeLoss_MT] REAL               NULL,
    [SlowDownLoss_MT] REAL               NULL,
    [TotLoss_MT]      REAL               NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_ReliabilityOpLossAvailability_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOpLossAvailability_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOpLossAvailability_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_ReliabilityOpLossAvailability_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_ReliabilityOpLossAvailability] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [OppLossId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_ReliabilityOpLossAvailability_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_ReliabilityOpLossAvailability_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_ReliabilityOpLossAvailability_OpLoss_LookUp] FOREIGN KEY ([OppLossId]) REFERENCES [dim].[ReliabilityOppLoss_LookUp] ([OppLossId]),
    CONSTRAINT [FK_ReliabilityOpLossAvailability_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER calc.t_ReliabilityOpLossAvailability_u
	ON  calc.ReliabilityOpLossAvailability
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.ReliabilityOpLossAvailability
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.ReliabilityOpLossAvailability.FactorSetId	= INSERTED.FactorSetId
		AND calc.ReliabilityOpLossAvailability.Refnum			= INSERTED.Refnum
		AND calc.ReliabilityOpLossAvailability.CalDateKey		= INSERTED.CalDateKey
		AND calc.ReliabilityOpLossAvailability.OppLossId		= INSERTED.OppLossId;
	
END;