﻿CREATE TABLE [calc].[SimulationCompositionYieldImprovement] (
    [FactorSetId]          VARCHAR (12)       NOT NULL,
    [Refnum]               VARCHAR (25)       NOT NULL,
    [CalDateKey]           INT                NOT NULL,
    [SimModelId]           VARCHAR (12)       NOT NULL,
    [OpCondId]             VARCHAR (12)       NOT NULL,
    [RecycleId]            INT                NOT NULL,
    [ComponentId]          VARCHAR (42)       NOT NULL,
    [Simulation_kMT]       REAL               NULL,
    [Plant_kMT]            REAL               NULL,
    [_ProductionYield_kMT] AS                 (CONVERT([real],coalesce([Plant_kMT],[Simulation_kMT],(0.0)),(0))) PERSISTED NOT NULL,
    [tsModified]           DATETIMEOFFSET (7) CONSTRAINT [DF_SimulationCompositionYieldImprovement_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]       NVARCHAR (168)     CONSTRAINT [DF_SimulationCompositionYieldImprovement_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]       NVARCHAR (168)     CONSTRAINT [DF_SimulationCompositionYieldImprovement_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]        NVARCHAR (168)     CONSTRAINT [DF_SimulationCompositionYieldImprovement_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]         ROWVERSION         NOT NULL,
    CONSTRAINT [PK_calc_SimulationCompositionYieldImprovement] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CalDateKey] ASC, [SimModelId] ASC, [OpCondId] ASC, [RecycleId] ASC, [ComponentId] ASC),
    CONSTRAINT [CK_SimulationCompositionYieldImprovement_Plant_kMT] CHECK ([Plant_kMT]>=(0.0)),
    CONSTRAINT [CK_SimulationCompositionYieldImprovement_Simulation_kMT] CHECK ([Simulation_kMT]>=(0.0)),
    CONSTRAINT [FK_calc_SimulationCompositionYieldImprovement_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_SimulationCompositionYieldImprovement_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_SimulationCompositionYieldImprovement_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_SimulationCompositionYieldImprovement_OpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_calc_SimulationCompositionYieldImprovement_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_SimulationCompositionYieldImprovement_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO

CREATE TRIGGER [calc].[t_SimulationCompositionYieldImprovement_u]
	ON [calc].[SimulationCompositionYieldImprovement]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [calc].[SimulationCompositionYieldImprovement]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[calc].[SimulationCompositionYieldImprovement].[FactorSetId]	= INSERTED.[FactorSetId]
		AND	[calc].[SimulationCompositionYieldImprovement].[Refnum]			= INSERTED.[Refnum]
		AND	[calc].[SimulationCompositionYieldImprovement].[CalDateKey]		= INSERTED.[CalDateKey]
		AND	[calc].[SimulationCompositionYieldImprovement].[SimModelId]		= INSERTED.[SimModelId]
		AND	[calc].[SimulationCompositionYieldImprovement].[OpCondId]		= INSERTED.[OpCondId]
		AND	[calc].[SimulationCompositionYieldImprovement].[RecycleId]		= INSERTED.[RecycleId]
		AND	[calc].[SimulationCompositionYieldImprovement].[ComponentId]	= INSERTED.[ComponentId];
		
END;