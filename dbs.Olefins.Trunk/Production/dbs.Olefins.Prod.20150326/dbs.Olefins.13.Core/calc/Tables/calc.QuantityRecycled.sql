﻿CREATE TABLE [calc].[QuantityRecycled] (
    [FactorSetId]     VARCHAR (12)       NOT NULL,
    [Refnum]          VARCHAR (25)       NOT NULL,
    [CalDateKey]      INT                NOT NULL,
    [RecycleId]       INT                NOT NULL,
    [StreamId]        VARCHAR (42)       NOT NULL,
    [ComponentId]     VARCHAR (42)       NOT NULL,
    [Recycled_WtPcnt] REAL               NOT NULL,
    [Quantity_kMT]    REAL               NOT NULL,
    [_Recycled_kMT]   AS                 (CONVERT([real],([Recycled_WtPcnt]*[Quantity_kMT])/(100.0),(1))) PERSISTED NOT NULL,
    [tsModified]      DATETIMEOFFSET (7) CONSTRAINT [DF_QuantityRecycled_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]  NVARCHAR (168)     CONSTRAINT [DF_QuantityRecycled_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]  NVARCHAR (168)     CONSTRAINT [DF_QuantityRecycled_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]   NVARCHAR (168)     CONSTRAINT [DF_QuantityRecycled_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_QuantityRecycled] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [StreamId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_QuantityRecycled_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_QuantityRecycled_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_calc_QuantityRecycled_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_QuantityRecycled_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId]),
    CONSTRAINT [FK_calc_QuantityRecycled_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_QuantityRecycled_u
	ON  calc.QuantityRecycled
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.QuantityRecycled
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.QuantityRecycled.FactorSetId	= INSERTED.FactorSetId
		AND calc.QuantityRecycled.Refnum		= INSERTED.Refnum
		AND calc.QuantityRecycled.CalDateKey	= INSERTED.CalDateKey
		AND calc.QuantityRecycled.StreamId		= INSERTED.StreamId;
	
END