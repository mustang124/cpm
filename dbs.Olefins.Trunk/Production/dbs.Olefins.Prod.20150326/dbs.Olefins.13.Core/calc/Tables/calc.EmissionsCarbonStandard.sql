﻿CREATE TABLE [calc].[EmissionsCarbonStandard] (
    [FactorSetId]            VARCHAR (12)       NOT NULL,
    [Refnum]                 VARCHAR (25)       NOT NULL,
    [SimModelId]             VARCHAR (12)       NOT NULL,
    [CalDateKey]             INT                NOT NULL,
    [EmissionsId]            VARCHAR (42)       NOT NULL,
    [Quantity_kMT]           REAL               NULL,
    [Quantity_MBtu]          REAL               NULL,
    [CefGwp]                 REAL               NOT NULL,
    [EmissionsCarbon_MTCO2e] REAL               NOT NULL,
    [tsModified]             DATETIMEOFFSET (7) CONSTRAINT [DF_EmissionsCarbonStandard_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]         NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonStandard_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]         NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonStandard_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]          NVARCHAR (168)     CONSTRAINT [DF_EmissionsCarbonStandard_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_calc_EmissionsCarbonStandard] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [SimModelId] ASC, [EmissionsId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_calc_EmissionsCarbonStandard_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_calc_EmissionsCarbonStandard_Emissions_LookUp] FOREIGN KEY ([EmissionsId]) REFERENCES [dim].[Emissions_LookUp] ([EmissionsId]),
    CONSTRAINT [FK_calc_EmissionsCarbonStandard_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_calc_EmissionsCarbonStandard_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_calc_EmissionsCarbonStandard_TSortClient] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE TRIGGER calc.t_EmissionsCarbonStandard_u
	ON  calc.EmissionsCarbonStandard
	AFTER UPDATE
AS 
BEGIN

	SET NOCOUNT ON;
	
	UPDATE calc.EmissionsCarbonStandard
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.EmissionsCarbonStandard.FactorSetId	= INSERTED.FactorSetId
		AND	calc.EmissionsCarbonStandard.Refnum			= INSERTED.Refnum
		AND calc.EmissionsCarbonStandard.CalDateKey		= INSERTED.CalDateKey
		AND calc.EmissionsCarbonStandard.EmissionsId	= INSERTED.EmissionsId
		AND calc.EmissionsCarbonStandard.SimModelId		= INSERTED.SimModelId;
	
END