﻿CREATE TABLE [calc].[MarginMeta] (
    [FactorSetId]      VARCHAR (12)        NOT NULL,
    [Refnum]           VARCHAR (25)        NOT NULL,
    [CalDateKey]       INT                 NOT NULL,
    [CurrencyRpt]      VARCHAR (4)         NOT NULL,
    [MarginAnalysis]   VARCHAR (42)        NOT NULL,
    [MarginId]         VARCHAR (42)        NOT NULL,
    [Hierarchy]        [sys].[hierarchyid] NOT NULL,
    [Stream_Cur]       REAL                NULL,
    [OpEx_Cur]         REAL                NULL,
    [Amount_Cur]       REAL                NOT NULL,
    [TaAdj_Stream_Cur] REAL                NULL,
    [TaAdj_OpEx_Cur]   REAL                NULL,
    [TaAdj_Amount_Cur] REAL                NOT NULL,
    [tsModified]       DATETIMEOFFSET (7)  CONSTRAINT [DF_MarginMeta_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)      CONSTRAINT [DF_MarginMeta_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)      CONSTRAINT [DF_MarginMeta_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)      CONSTRAINT [DF_MarginMeta_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_MarginMeta] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [CurrencyRpt] ASC, [MarginAnalysis] ASC, [MarginId] ASC, [CalDateKey] ASC),
    CONSTRAINT [FK_MarginMeta_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_MarginMeta_Currency_LookUp_Rpt] FOREIGN KEY ([CurrencyRpt]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [FK_MarginMeta_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_MarginMeta_Margin_LookUp] FOREIGN KEY ([MarginId]) REFERENCES [dim].[Margin_LookUp] ([MarginId]),
    CONSTRAINT [FK_MarginMeta_TSort] FOREIGN KEY ([Refnum]) REFERENCES [fact].[TSortClient] ([Refnum])
);


GO
CREATE NONCLUSTERED INDEX [IX_MarginMeta_MarginSensitivity]
    ON [calc].[MarginMeta]([MarginAnalysis] ASC, [MarginId] ASC)
    INCLUDE([FactorSetId], [Refnum], [CalDateKey], [CurrencyRpt], [Hierarchy], [Amount_Cur], [TaAdj_Amount_Cur]);


GO

CREATE TRIGGER [calc].[t_MarginMeta_u]
	ON [calc].[MarginMeta]
	AFTER UPDATE 
AS 
BEGIN

    SET NOCOUNT ON;

	UPDATE calc.MarginMeta
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	calc.MarginMeta.FactorSetId		= INSERTED.FactorSetId
		AND	calc.MarginMeta.Refnum			= INSERTED.Refnum
		AND calc.MarginMeta.CalDateKey		= INSERTED.CalDateKey
		AND calc.MarginMeta.CurrencyRpt		= INSERTED.CurrencyRpt
		AND calc.MarginMeta.MarginAnalysis	= INSERTED.MarginAnalysis
		AND calc.MarginMeta.MarginId		= INSERTED.MarginId;

END;
