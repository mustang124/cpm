﻿CREATE FUNCTION [calc].[GeometricPrevious]
(
	@GeometricNumber	int, 
	@PartialAggregate	int
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @rtn	INT;
	DECLARE @r		INT = 2;

	IF calc.GeometricContains(@GeometricNumber, @PartialAggregate) = 1
		SET @rtn = @GeometricNumber;
	ELSE
	BEGIN

		DECLARE @k	INT = (Log(@GeometricNumber) / Log(@r));
		SET @GeometricNumber = POWER(@r, @k - 1);
		
		IF (@GeometricNumber <> 0)
			SET @rtn = calc.GeometricContains(@GeometricNumber, @PartialAggregate)
		ELSE
			SET @rtn = 0;

	END;
	
	RETURN @rtn;

END;