﻿CREATE PROCEDURE [calc].[Insert_ReplValPyroFlexibility]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.ReplValPyroFlexibility(FactorSetId, Refnum, CalDateKey,
			Actual_ReplValue, Design_ReplValue, Demon_ReplValue,
			Base_ISBL, Design_ISBL, Demon_ISBL,
			MaxDesign_ISBL, MaxDemon_ISBL, Capability_ISBL,
			FlexAdjustment, StartUpCost,
			SuppRecovery_Pcnt)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,

			SUM(e._Actual_ReplValue)	[Actual_ReplValue],
			SUM(e._Design_ReplValue)	[Design_ReplValue],
			SUM(e._Demon_ReplValue)		[Demon_ReplValue],

			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * SUM(e._Actual_ReplValue)		[Base_ISBL],
			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * SUM(e._Design_ReplValue)		[Design_ISBL],
			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * SUM(e._Demon_ReplValue)		[Demon_ISBL],

			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Design_ReplValue))	[MaxDesign_ISBL],
			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Demon_ReplValue))	[MaxDemon_ISBL],

			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * ([calc].[MinValue](
				calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Design_ReplValue)),
				calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Demon_ReplValue))))									[Capability_ISBL],

			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * ([calc].[MinValue](
				calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Design_ReplValue)),
				calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Demon_ReplValue))) - SUM(e._Actual_ReplValue))		[FlexAdjustment],

			POWER(p.PyroEthyleneCap_kMT / 200.0, 0.75) * [calc].[MinValue](
				calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Design_ReplValue)),
				calc.MaxValue(SUM(e._Actual_ReplValue), SUM(e._Demon_ReplValue))) * 0.25							[StartUpCost],

			40.0			[SuppRecovery_Pcnt]

		FROM @fpl											fpl
		INNER JOIN inter.ReplValPyroCapacityEstimate		e
			ON	e.FactorSetId = fpl.FactorSetId
			AND	e.Refnum = fpl.Refnum
			AND	e.CalDateKey = fpl.Plant_QtrDateKey
		INNER JOIN inter.ReplValPyroCapacity				p
			ON	p.FactorSetId = fpl.FactorSetId
			AND	p.Refnum = fpl.Refnum
			AND	p.CalDateKey = fpl.Plant_QtrDateKey
		WHERE	fpl.CalQtr = 4
		GROUP BY
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			p.PyroEthyleneCap_kMT;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;