﻿CREATE PROCEDURE [calc].[Insert_Edc_RedPropylene]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE	@Threshold_Pcnt		FLOAT = 90.0;

		INSERT INTO calc.Edc(FactorSetId, Refnum, CalDateKey, EdcId, EdcDetail, kEdc)
		SELECT
			adj.[FactorSetId],
			adj.[Refnum],
			adj.[CalDateKey],
			'Red' + adj.[StreamId],
			'Red' + adj.[StreamId],
			adj.[Adjusted_kMT] / u.[_UtilizationStream_Pcnt] * 100.0 * 10.24 / 365.0 * k.[Value]
		FROM (
			SELECT
				fpl.[FactorSetId],
				fpl.[Refnum],
				fpl.[Plant_AnnDateKey]	[CalDateKey],

				  CASE WHEN COALESCE(cg.[Component_WtPcnt], 0.0) <	@Threshold_Pcnt THEN COALESCE(cq.[Quantity_kMT], 0.0) ELSE 0.0 END
				+ CASE WHEN COALESCE(rg.[Component_WtPcnt], 0.0) <	@Threshold_Pcnt THEN COALESCE(rq.[Quantity_kMT], 0.0) ELSE 0.0 END	[PropyleneRG],

				( COALESCE(cq.[Quantity_kMT], 0.0) + COALESCE(rq.[Quantity_kMT], 0.0)
				- CASE WHEN cg.[Component_WtPcnt] <	@Threshold_Pcnt THEN cq.[Quantity_kMT] ELSE 0.0 END
				- CASE WHEN rg.[Component_WtPcnt] <	@Threshold_Pcnt THEN rq.[Quantity_kMT] ELSE 0.0 END) * 1.46							[PropyleneCG]

			FROM @fpl											fpl
			INNER JOIN [calc].[PeerGroupFeedClass]				fc
				ON	fc.[FactorSetId]	= fpl.[FactorSetId]
				AND	fc.[Refnum]			= fpl.[Refnum]

			LEFT OUTER JOIN [calc].[CompositionStream]			cg
				ON	cg.[FactorSetId]	= fpl.[FactorSetId]
				AND	cg.[Refnum]			= fpl.[Refnum]
				AND	cg.[CalDateKey]		= fpl.[Plant_QtrDateKey]
				AND	cg.[StreamId]		= 'PropyleneCG'
				AND	cg.[ComponentId]	= 'C3H6'
			LEFT OUTER JOIN [fact].[StreamQuantityAggregate]	cq	WITH (NOEXPAND)
				ON	cq.[FactorSetId]	= fpl.[FactorSetId]
				AND	cq.[Refnum]			= fpl.[Refnum]
				AND	cq.[StreamId]		= cg.[StreamId]

			LEFT OUTER JOIN [calc].[CompositionStream]			rg
				ON	rg.[FactorSetId]	= fpl.[FactorSetId]
				AND	rg.[Refnum]			= fpl.[Refnum]
				AND	rg.[CalDateKey]		= fpl.[Plant_QtrDateKey]
				AND	rg.[StreamId]		= 'PropyleneRG'
				AND	rg.[ComponentId]	= 'C3H6'
			LEFT OUTER JOIN [fact].[StreamQuantityAggregate]	rq	WITH (NOEXPAND)
				ON	rq.[FactorSetId]	= fpl.[FactorSetId]
				AND	rq.[Refnum]			= fpl.[Refnum]
				AND	rq.[StreamId]		= rg.[StreamId]

			WHERE	fpl.[CalQtr]	= 4
				AND	fpl.[FactorSet_QtrDateKey]	> 20130000
				AND	fc.[PeerGroup]	<> 1
			) t
			UNPIVOT (
				[Adjusted_kMT] FOR [StreamId] IN ([PropyleneRG], [PropyleneCG])
			) adj
		INNER JOIN [calc].[CapacityUtilization]		u
			ON	u.[FactorSetId]		= adj.[FactorSetId]
			AND	u.[Refnum]			= adj.[Refnum]
			AND	u.[CalDateKey]		= adj.[CalDateKey]
			AND	u.[SchedId]			= 'P'
			AND	u.[StreamId]		= 'ProdOlefins'
		INNER JOIN [ante].[EdcCoefficients]				k
			ON	k.[FactorSetId]		= adj.[FactorSetId]
			AND	k.[EdcId]			= 'Red' + adj.[StreamId]
		WHERE	adj.[Adjusted_kMT]	IS NOT NULL
			AND	u.[_UtilizationStream_Pcnt]	<> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;