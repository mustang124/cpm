﻿CREATE PROCEDURE [calc].[Insert_MaintCostRoutine_Furnace]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;
	
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO calc.MaintCostRoutine(FactorSetId, Refnum, CalDateKey, CurrencyRpt, FacilityId, MaintMaterial_Cur, MaintLabor_Cur)
		SELECT
			fpl.FactorSetId,
			fpl.Refnum,
			fpl.Plant_QtrDateKey,
			fpl.CurrencyRpt,
			'PyroFurnMajRad',
			f.InflAdjAnn_MaintRetubeMatl_Cur,
			f.InflAdjAnn_MaintRetubeLabor_Cur
		FROM @fpl				fpl
		INNER JOIN calc.FurnacesAggregate	f WITH (NOEXPAND)
			ON	f.FactorSetId	= fpl.FactorSetId
			AND	f.Refnum		= fpl.Refnum
			AND	f.CalDateKey	= fpl.Plant_QtrDateKey
			AND	f.CurrencyRpt	= fpl.CurrencyRpt
		WHERE	fpl.CalQtr		= 4
			AND f.InflAdjAnn_MaintRetube_Cur	<> 0.0;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;