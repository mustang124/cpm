﻿CREATE PROCEDURE [calc].[Insert_TaAdjStudyYear]
(
	@Refnum		VARCHAR (25),
	@fpl		[calc].[FoundationPlantList]	READONLY
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [calc].[TaAdjStudyYear]([FactorSetId], [Refnum], [CalDateKey], [TurnAroundInStudyYear_Bit])
		SELECT
			tsq.[FactorSetId],
			tsq.[Refnum],
			tsq.[Plant_QtrDateKey],
			CASE WHEN r.[TurnAroundInStudyYear_Count] >= 1
				AND	Occ.[_Tot_Hrs] > 0
				AND	Mps.[_Tot_Hrs] > 0
			THEN 1
			ELSE 0
			END
		FROM @fpl							tsq
		LEFT OUTER JOIN (
			SELECT
				r.[Refnum],
				r.[CalDateKey],
				COUNT(r.[_TurnAroundInStudyYear_Bit]) [TurnAroundInStudyYear_Count]
			FROM [fact].[ReliabilityTA]		r
			WHERE	r.[SchedId]		= 'A'
				AND	r.[_TurnAroundInStudyYear_Bit] = 1
			GROUP BY
				r.[Refnum],
				r.[CalDateKey]
			)						r
			ON	r.[Refnum]			= tsq.[Refnum]
			AND r.[CalDateKey]		= tsq.[Plant_QtrDateKey]
		LEFT OUTER JOIN fact.Pers	Occ
			ON	Occ.[Refnum]		= tsq.[Refnum]
			AND	Occ.[CalDateKey]	= tsq.[Plant_QtrDateKey]
			AND	Occ.[PersId]		= 'OccMaintTaMaint'
		LEFT OUTER JOIN fact.Pers	Mps
			ON	Mps.[Refnum]		= tsq.[Refnum]
			AND	Mps.[CalDateKey]	= tsq.[Plant_QtrDateKey]
			AND	Mps.[PersId]		= 'MpsMaintTaMaint'	
		WHERE	tsq.[CalQtr]		= 4;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;