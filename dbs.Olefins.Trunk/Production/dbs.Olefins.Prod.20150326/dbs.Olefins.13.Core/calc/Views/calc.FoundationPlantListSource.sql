﻿CREATE VIEW [calc].[FoundationPlantListSource]
WITH SCHEMABINDING
AS
SELECT
	fs.FactorSetId,
	fs.FactorSetName,
	fA.CalDateKey								[FactorSet_AnnDateKey],
	fQ.CalDateKey								[FactorSet_QtrDateKey],
	fQ.CalDate									[FactorSet_QtrDate],

	RTRIM(LTRIM(sa.Refnum))						[Refnum],
	pA.CalDateKey								[Plant_AnnDateKey],
	pQ.CalDateKey								[Plant_QtrDateKey],
	pQ.CalDate									[Plant_QtrDate],
	
	c.CompanyId,
	a.AssetId,
	a.AssetName,
	COALESCE(c.CompanyId + ' - ', '') + a.AssetName			[CoLoc],
	a.AssetIdPri,
	
	sc.SubscriberCompanyName,
	tc.PlantCompanyName,
	sa.SubscriberAssetName,
	tc.PlantAssetName,
	
	k.CountryId,
	a.StateName,

	cr.EconRegionID,
	
	COALESCE(tc.UomId, 'US')					[UomId],
	COALESCE(tc.CurrencyFcn, cc.CurrencyId)		[CurrencyFcn],
	COALESCE(tc.CurrencyRpt, 'USD')				[CurrencyRpt],
	
	sa.PassWord_PlainText						[AssetPassWord_PlainText],
	sa._StudyYear								[StudyYear],
	COALESCE(tc._DataYear,sa._StudyYear)		[DataYear],
	ts.Consultant,
	pA.CalYear - fA.CalYear						[StudyYearDifference],
	pq.CalQtr
FROM cons.SubscriptionsAssets					sa
INNER JOIN dim.Calendar_LookUp					pA
	ON	pA.CalDateKey		= sa.CalDateKey
INNER JOIN dim.Calendar_LookUp					pQ
	ON	pQ.CalYear			= pA.CalYear
INNER JOIN cons.Assets							a
	ON	a.AssetId			= sa.AssetId
LEFT OUTER JOIN dim.Company_LookUp				c
	ON	c.CompanyId			= sa.CompanyId

INNER JOIN ante.CountriesCurrencies				cc
	ON	cc.CountryId		= a.CountryId
	AND	cc.SelectionUse		= 1
	AND	cc.InflationUse		= 1

INNER JOIN ante.FactorSetsInStudyYear			sy
	ON	sy._StudyYear		= sa._StudyYear
	AND	sy.Calculate_Bit	= 1
INNER JOIN dim.FactorSet_LookUp					fs
	ON	fs.FactorSetId		= sy.FactorSetId
INNER JOIN dim.Calendar_LookUp					fA
	ON	fA.CalYear			= fs.FactorSetYear
	AND	fA.CalQtr			= pA.CalQtr
	AND	fA.MonthDay			= pA.MonthDay
	AND	fA.CalMonth			= pA.CalMonth
INNER JOIN dim.Calendar_LookUp					fQ
	ON	fQ.CalYear			= fA.CalYear
	AND	fq.CalQtr			= pQ.CalQtr
	AND	fQ.MonthDay			= pQ.MonthDay
	AND	fQ.CalMonth			= pQ.CalMonth

LEFT OUTER JOIN cons.SubscriptionsCompanies		sc
	ON	sc.CompanyId		= sa.CompanyId
	AND	sc.StudyId			= sa.StudyId
	AND	sc.CalDateKey		= sa.CalDateKey

INNER JOIN dim.Country_LookUp					k
	ON	k.CountryId			= a.CountryId
INNER JOIN ante.CountryEconRegion				cr
	ON	cr.FactorSetId		= sy.FactorSetId
	AND	cr.CountryId		= a.CountryId

INNER JOIN cons.TSortSolomon					ts
	ON	ts.Refnum			= sa.Refnum

LEFT OUTER JOIN fact.TSortClient				tc
	ON	tc.Refnum			= sa.Refnum;