﻿CREATE PROCEDURE [gap].[Insert_AnalysisResults2013]
(
	@FactorSetId			VARCHAR(12) = NULL,
	@GroupId				VARCHAR(25) = NULL,
	@TargetId				VARCHAR(25) = NULL
)
AS
BEGIN
	
	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + @GroupId + ' @ ' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	--BEGIN TRY

		BEGIN

		SET @ProcedureDesc = CHAR(9) + '@Disposition Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @RecycleValue		TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Ethane]			REAL	NULL,
			[DilEthane]			REAL	NULL,
			[ROGEthane]			REAL	NULL,
			[Propane]			REAL	NULL,
			[DilPropane]		REAL	NULL,
			[ROGPropane]		REAL	NULL,
			[Butane]			REAL	NULL,
			[DilButane]			REAL	NULL,
			[ROGC4Plus]			REAL	NULL,

			[EthRec]			AS CASE WHEN [Ethane] > 0.0 THEN [Ethane] ELSE CASE WHEN [DilEthane] > 0.0 THEN [DilEthane] ELSE CASE WHEN [ROGEthane] > 0.0 THEN [ROGEthane] ELSE 0.0 END END END
								PERSISTED NOT NULL,
			[ProRec]			AS CASE WHEN [Propane] > 0.0 THEN [Propane] ELSE CASE WHEN [DilPropane] > 0.0 THEN [DilPropane] ELSE CASE WHEN [ROGPropane] > 0.0 THEN [ROGPropane] ELSE 0.0 END END END
								PERSISTED NOT NULL,
			[ButRec]			AS CASE WHEN [Butane] > 0.0 THEN [Butane] ELSE CASE WHEN [DilButane] > 0.0 THEN [DilButane] ELSE CASE WHEN [ROGC4Plus] > 0.0 THEN [ROGC4Plus] ELSE 0.0 END END END
								PERSISTED NOT NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @RecycleValue([GroupId], [Ethane], [DilEthane], [ROGEthane], [Propane], [DilPropane], [ROGPropane], [Butane], [DilButane], [ROGC4Plus])
		SELECT
			[GroupID]	= @TargetId,

			[BenchEthane]		= CASE WHEN bV.Ethane		> 0.0 THEN bV.Ethane		ELSE gV.Ethane END,
			[BenchDilEthane]	= CASE WHEN bV.DilEthane	> 0.0 THEN bV.DilEthane		ELSE gV.DilEthane END,
			[BenchRogEthane]	= CASE WHEN bV.ROGEthane	> 0.0 THEN bV.ROGEthane		ELSE gV.ROGEthane END,

			[BenchPropane]		= CASE WHEN bV.Propane		> 0.0 THEN bV.Propane		ELSE gV.Propane END,
			[BenchDilPropane]	= CASE WHEN bV.DilPropane	> 0.0 THEN bV.DilPropane	ELSE gV.DilPropane END,
			[BenchRogPropane]	= CASE WHEN bV.ROGPropane	> 0.0 THEN bV.ROGPropane	ELSE gV.ROGPropane END,

			[BenchButane]		= CASE WHEN bV.Butane		> 0.0 THEN bV.Butane		ELSE gV.Butane END,
			[BenchDilButane]	= CASE WHEN bV.DilButane	> 0.0 THEN bV.DilButane		ELSE gV.DilButane END,
			[BenchRogButane]	= CASE WHEN bV.ROGC4Plus	> 0.0 THEN bV.ROGC4Plus		ELSE gV.ROGC4Plus END

		FROM			[$(DbOlefins)].[reports].[Yield]			gV
		LEFT OUTER JOIN	[$(DatabaseName)].[reports].[Yield]		bV
			ON	bV.[GroupID]	= @TargetId
			AND	bV.[DataType]	= '$PerMT'
		WHERE	gV.[GroupID]	= @GroupId
			AND	gV.[DataType]	= '$PerMT';

		INSERT INTO @RecycleValue([GroupId], [Ethane], [DilEthane], [ROGEthane], [Propane], [DilPropane], [ROGPropane], [Butane], [DilButane], [ROGC4Plus])
		SELECT
			gV.[GroupId],

			[GroupEthane]		= gV.Ethane,
			[GroupDilEthane]	= gV.DilEthane,
			[GroupRogEthane]	= gV.ROGEthane,

			[GroupPropane]		= gV.Propane,
			[GroupDilPropane]	= gV.DilPropane,
			[GroupRogPropane]	= gV.ROGPropane,

			[GroupButane]		= gV.Butane,
			[GroupDilButane]	= gV.DilButane,
			[GroupRogButane]	= gV.ROGC4Plus

		FROM	[$(DbOlefins)].[reports].[Yield]			gV
		WHERE	gV.[GroupID]	= @GroupId
			AND	gV.[DataType]	= '$PerMT';

		SET @ProcedureDesc = CHAR(9) + '@Disposition Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @Disposition		TABLE
		(
			[GroupId]	VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),

			[C14]		REAL		NOT	NULL,
			[C15]		REAL		NOT	NULL,
			[C16]		REAL		NOT	NULL,
			[C17]		REAL		NOT	NULL,
			[C18]		REAL		NOT	NULL,
			[C19]		REAL		NOT	NULL,
			[C20]		REAL		NOT	NULL,
			[C21]		REAL		NOT	NULL,
			[C22]		REAL		NOT	NULL,
			[C23]		REAL		NOT	NULL,
			[C24]		REAL		NOT	NULL,
			[C25]		REAL		NOT	NULL,
			[C26]		REAL		NOT	NULL,
			[C27]		REAL		NOT	NULL,
			[C28]		REAL		NOT	NULL,
			[C29]		REAL		NOT	NULL,
			[C30]		REAL		NOT	NULL,
			[C31]		REAL		NOT	NULL,
			[C32]		REAL		NOT	NULL,
			[C33]		REAL		NOT	NULL,
			[C34]		REAL		NOT	NULL,
			[C35]		REAL		NOT	NULL,
			[C192]		REAL		NOT	NULL,
			[C197]		REAL		NOT	NULL,
			[C201]		REAL		NOT	NULL,
			[C207]		REAL		NOT	NULL,
			[C211]		REAL		NOT	NULL,
			[C219]		REAL		NOT	NULL,
			[C227]		REAL		NOT	NULL,
			[C231]		REAL		NOT	NULL,
			[C236]		REAL		NOT	NULL,
			[C241]		REAL		NOT	NULL,
			[C246]		REAL		NOT	NULL,
			[C252]		REAL		NOT	NULL,
			[C257]		REAL		NOT	NULL,
			[C262]		REAL		NOT	NULL,
			[D193]		REAL		NOT	NULL,
			[D194]		REAL		NOT	NULL,
			[D195]		REAL		NOT	NULL,
			[D198]		REAL		NOT	NULL,
			[D199]		REAL		NOT	NULL,
			[D208]		REAL		NOT	NULL,
			[D220]		REAL		NOT	NULL,
			[D228]		REAL		NOT	NULL,
			[D242]		REAL		NOT	NULL,
			[D253]		REAL		NOT	NULL,
			[D258]		REAL		NOT	NULL,
			[D263]		REAL		NOT	NULL,
			[D202]		REAL		NOT	NULL,
			[D209]		REAL		NOT	NULL,
			[D212]		REAL		NOT	NULL,
			[D221]		REAL		NOT	NULL,
			[D229]		REAL		NOT	NULL,
			[D232]		REAL		NOT	NULL,
			[D237]		REAL		NOT	NULL,
			[D254]		REAL		NOT	NULL,
			[D259]		REAL		NOT	NULL,
			[D203]		REAL		NOT	NULL,
			[D213]		REAL		NOT	NULL,
			[D222]		REAL		NOT	NULL,
			[D233]		REAL		NOT	NULL,
			[D255]		REAL		NOT	NULL,
			[D260]		REAL		NOT	NULL,
			[D204]		REAL		NOT	NULL,
			[D214]		REAL		NOT	NULL,
			[D234]		REAL		NOT	NULL,
			[D238]		REAL		NOT	NULL,
			[D205]		REAL		NOT	NULL,
			[D215]		REAL		NOT	NULL,
			[D239]		REAL		NOT	NULL,
			[D243]		REAL		NOT	NULL,
			[D216]		REAL		NOT	NULL,
			[D223]		REAL		NOT	NULL,
			[D244]		REAL		NOT	NULL,
			[D247]		REAL		NOT	NULL,
			[D217]		REAL		NOT	NULL,
			[D224]		REAL		NOT	NULL,
			[D248]		REAL		NOT	NULL,
			[D225]		REAL		NOT	NULL,
			[D249]		REAL		NOT	NULL,
			[D250]		REAL		NOT	NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @Disposition([GroupId], 
			[C14], [C15], [C16], [C17], [C18], [C19], [C20], [C21], [C22], [C23], [C24], [C25], [C26], [C27], [C28], [C29], [C30], [C31], [C32], [C33], [C34], [C35],
			[C192], [C197], [C201], [C207], [C211], [C219], [C227], [C231], [C236], [C241], [C246], [C252], [C257], [C262],
			[D193], [D198], [D199], [D208], [D220], [D228], [D237], [D242], [D253], [D258], [D263],
			[D194], [D202], [D209], [D212], [D221], [D229], [D232], [D254], [D259],
			[D195], [D203], [D213], [D222], [D233], [D255], [D260],
			[D204], [D214], [D234], [D238],
			[D205], [D215], [D239], [D243],
			[D216], [D223], [D244], [D247],
			[D217], [D224], [D248],
			[D225], [D249],
			[D250]
		)
		SELECT
			i.[GroupID],

			i.[C14], i.[C15], i.[C16], i.[C17], i.[C18], i.[C19], i.[C20], i.[C21], i.[C22], i.[C23], i.[C24], i.[C25], i.[C26], i.[C27], i.[C28], i.[C29], i.[C30], i.[C31], i.[C32], i.[C33], i.[C34], i.[C35],
			i.[C192], i.[C197], i.[C201], i.[C207], i.[C211], i.[C219], i.[C227], i.[C231], i.[C236], i.[C241], i.[C246], i.[C252], i.[C257], i.[C262],
			i.[D193], i.[D198], i.[D199], i.[D208], i.[D220], i.[D228], i.[D237], i.[D242], i.[D253], i.[D258], i.[D263],
			i.[D194], i.[D202], i.[D209], i.[D212], i.[D221], i.[D229], i.[D232], i.[D254], i.[D259],
			i.[D195], i.[D203], i.[D213], i.[D222], i.[D233], i.[D255], i.[D260],
			i.[D204], i.[D214], i.[D234], i.[D238],
			i.[D205], i.[D215], i.[D239], i.[D243],
			i.[D216], i.[D223], i.[D244], i.[D247],
			i.[D217], i.[D224], i.[D248],
			i.[D225], i.[D249],

			--	[D250]	= C246-D247-D248-D249
			[D250]	= i.[C246] - i.[D247] - i.[D248] - i.[D249]

		FROM (
			SELECT
				h.[GroupID],

				h.[C14], h.[C15], h.[C16], h.[C17], h.[C18], h.[C19], h.[C20], h.[C21], h.[C22], h.[C23], h.[C24], h.[C25], h.[C26], h.[C27], h.[C28], h.[C29], h.[C30], h.[C31], h.[C32], h.[C33], h.[C34], h.[C35],
				h.[C192], h.[C197], h.[C201], h.[C207], h.[C211], h.[C219], h.[C227], h.[C231], h.[C236], h.[C241], h.[C246], h.[C252], h.[C257], h.[C262],
				h.[D193], h.[D198], h.[D199], h.[D208], h.[D220], h.[D228], h.[D237], h.[D242], h.[D253], h.[D258], h.[D263],
				h.[D194], h.[D202], h.[D209], h.[D212], h.[D221], h.[D229], h.[D232], h.[D254], h.[D259],
				h.[D195], h.[D203], h.[D213], h.[D222], h.[D233], h.[D255], h.[D260],
				h.[D204], h.[D214], h.[D234], h.[D238],
				h.[D205], h.[D215], h.[D239], h.[D243],
				h.[D216], h.[D223], h.[D244], h.[D247],
				h.[D217], h.[D224], h.[D248],

				--	[D225]	= C219-SUM(D220:D224)
				[D225]	= h.[C219] - h.[D220] - h.[D221] - h.[D222] - h.[D223] - h.[D224],

				--	[D249]	= MIN(C246-D247-D248,C28-D253-D259)
				[D249]	= [$(DbOlefins)].[calc].[MinValue](h.[C246] - h.[D247] - h.[D248], h.[C28] - h.[D253] - h.[D259])

			FROM (
				SELECT
					g.[GroupID],

					g.[C14], g.[C15], g.[C16], g.[C17], g.[C18], g.[C19], g.[C20], g.[C21], g.[C22], g.[C23], g.[C24], g.[C25], g.[C26], g.[C27], g.[C28], g.[C29], g.[C30], g.[C31], g.[C32], g.[C33], g.[C34], g.[C35],
					g.[C192], g.[C197], g.[C201], g.[C207], g.[C211], g.[C219], g.[C227], g.[C231], g.[C236], g.[C241], g.[C246], g.[C252], g.[C257], g.[C262],
					g.[D193], g.[D198], g.[D199], g.[D208], g.[D220], g.[D228], g.[D237], g.[D242], g.[D253], g.[D258], g.[D263],
					g.[D194], g.[D202], g.[D209], g.[D212], g.[D221], g.[D229], g.[D232], g.[D254], g.[D259],
					g.[D195], g.[D203], g.[D213], g.[D222], g.[D233], g.[D255], g.[D260],
					g.[D204], g.[D214], g.[D234], g.[D238],
					g.[D205], g.[D215], g.[D239], g.[D243],
					g.[D216], g.[D223], g.[D244], g.[D247],

					--	[D217]	= C211-SUM(D212:D216)
					[D217]	= g.[C211] - g.[D212] - g.[D213] - g.[D214] - g.[D215] - g.[D216],

					--	[D224]	= MIN(C219-D220-D221-D222-D223,C31-D194-D198-D204-D216)
					[D224]	= [$(DbOlefins)].[calc].[MinValue](g.[C219] - g.[D220] - g.[D221] - g.[D222] - g.[D223], g.[C31] - g.[D194] - g.[D198] - g.[D204] - g.[D216]),

					--	[D248]	= MIN(C246-D247,C26-D242)
					[D248]	= [$(DbOlefins)].[calc].[MinValue](g.[C246] - g.[D247], g.[C26] - g.[D242])

				FROM (
					SELECT
						f.[GroupID],

						f.[C14], f.[C15], f.[C16], f.[C17], f.[C18], f.[C19], f.[C20], f.[C21], f.[C22], f.[C23], f.[C24], f.[C25], f.[C26], f.[C27], f.[C28], f.[C29], f.[C30], f.[C31], f.[C32], f.[C33], f.[C34], f.[C35],
						f.[C192], f.[C197], f.[C201], f.[C207], f.[C211], f.[C219], f.[C227], f.[C231], f.[C236], f.[C241], f.[C246], f.[C252], f.[C257], f.[C262],
						f.[D193], f.[D198], f.[D199], f.[D208], f.[D220], f.[D228], f.[D237], f.[D242], f.[D253], f.[D258], f.[D263],
						f.[D194], f.[D202], f.[D209], f.[D212], f.[D221], f.[D229], f.[D232], f.[D254], f.[D259],
						f.[D195], f.[D203], f.[D213], f.[D222], f.[D233], f.[D255], f.[D260],
						f.[D204], f.[D214], f.[D234], f.[D238],
						f.[D205], f.[D215], f.[D239], f.[D243],

						--	[D216]	= MIN(C211-D212-D213-D214-D215,C31-D194-D198-D204)
						[D216]	= [$(DbOlefins)].[calc].[MinValue](f.[C211] - f.[D212] - f.[D213] - f.[D214 ] - f.[D215], f.[C31] - f.[D194] - f.[D198] - f.[D204]),

						--	[D223]	= MIN(C219-D220-D221-D222,C22-D215)
						[D223]	= [$(DbOlefins)].[calc].[MinValue](f.[C219] - f.[D220] - f.[D221] - f.[D222], f.[C22] - f.[D215]),

						--	[D244]	= C241-D242-D243
						[D244]	= f.[C241] - f.[D242] - f.[D243],

						--	[D247]	= MIN(C246,C27-D229-D234-D243)
						[D247]	= [$(DbOlefins)].[calc].[MinValue](f.[C246], f.[C27] - f.[D229] - f.[D234] - f.[D243])

					FROM (
						SELECT
							e.[GroupID],

							e.[C14], e.[C15], e.[C16], e.[C17], e.[C18], e.[C19], e.[C20], e.[C21], e.[C22], e.[C23], e.[C24], e.[C25], e.[C26], e.[C27], e.[C28], e.[C29], e.[C30], e.[C31], e.[C32], e.[C33], e.[C34], e.[C35],
							e.[C192], e.[C197], e.[C201], e.[C207], e.[C211], e.[C219], e.[C227], e.[C231], e.[C236], e.[C241], e.[C246], e.[C252], e.[C257], e.[C262],
							e.[D193], e.[D198], e.[D199], e.[D208], e.[D220], e.[D228], e.[D237], e.[D242], e.[D253], e.[D258], e.[D263],
							e.[D194], e.[D202], e.[D209], e.[D212], e.[D221], e.[D229], e.[D232], e.[D254], e.[D259],
							e.[D195], e.[D203], e.[D213], e.[D222], e.[D233], e.[D255], e.[D260],
							e.[D204], e.[D214], e.[D234], e.[D238],

							--	[D205]	= C201-SUM(D202:D204)
							[D205]	= e.[C201] - e.[D202] - e.[D203] - e.[D204],

							--	[D215]	= MIN(C211-D212-D213-D214,C22)
							[D215]	= [$(DbOlefins)].[calc].[MinValue](e.[C211] - e.[D212] - e.[D213] - e.[D214], e.[C22]),

							--	[D239]	= C236-D237-D238
							[D239]	= e.[C236] - e.[D237] - e.[D238],

							--	[D243]	= MIN(C241-D242,C27-D229-D234)
							[D243]	= [$(DbOlefins)].[calc].[MinValue](e.[C241] - e.[D242], e.[C27] - e.[D229] - e.[D234])

						FROM (
							SELECT
								d.[GroupID],

								d.[C14], d.[C15], d.[C16], d.[C17], d.[C18], d.[C19], d.[C20], d.[C21], d.[C22], d.[C23], d.[C24], d.[C25], d.[C26], d.[C27], d.[C28], d.[C29], d.[C30], d.[C31], d.[C32], d.[C33], d.[C34], d.[C35],
								d.[C192], d.[C197], d.[C201], d.[C207], d.[C211], d.[C219], d.[C227], d.[C231], d.[C236], d.[C241], d.[C246], d.[C252], d.[C257], d.[C262],
								d.[D193], d.[D198], d.[D199], d.[D208], d.[D220], d.[D228], d.[D237], d.[D242], d.[D253], d.[D258], d.[D263],
								d.[D194], d.[D202], d.[D209], d.[D212], d.[D221], d.[D229], d.[D232], d.[D254], d.[D259],
								d.[D195], d.[D203], d.[D213], d.[D222], d.[D233], d.[D255], d.[D260],
						
								--	[D204]	= IF(C31-D194-D198>=C201-D202-D203,C201-D202-D203,C31-D194-D198)
								[D204]	=	CASE WHEN d.[C31] - d.[D194] - d.[D198] >= d.[C201] - d.[D202] - d.[D203]
											THEN d.[C201] - d.[D202] - d.[D203]
											ELSE d.[C31] - d.[D194] - d.[D198]
											END,

								--	[D214]	= MIN(C211-D212-D213,D222*0.01)
								[D214]	= [$(DbOlefins)].[calc].[MinValue](d.[C211] - d.[D212] - d.[D213], d.[D222] * 0.01),

								--	[D234]	= C231-D232-D233
								[D234]	= d.[C231] - d.[D232] - d.[D233],

								--	[G238]	= MIN(C236-D237,C24-D233)
								[D238]	= [$(DbOlefins)].[calc].[MinValue](d.[C236] - d.[D237], d.[C24] - d.[D233])

							FROM (
								SELECT
									c.[GroupID],

									c.[C14], c.[C15], c.[C16], c.[C17], c.[C18], c.[C19], c.[C20], c.[C21], c.[C22], c.[C23], c.[C24], c.[C25], c.[C26], c.[C27], c.[C28], c.[C29], c.[C30], c.[C31], c.[C32], c.[C33], c.[C34], c.[C35],
									c.[C192], c.[C197], c.[C201], c.[C207], c.[C211], c.[C219], c.[C227], c.[C231], c.[C236], c.[C241], c.[C246], c.[C252], c.[C257], c.[C262],
									c.[D193], c.[D198], c.[D199], c.[D208], c.[D220], c.[D228], c.[D237], c.[D242], c.[D253], c.[D258], c.[D263],
									c.[D194], c.[D202], c.[D209], c.[D212], c.[D221], c.[D229], c.[D232], c.[D254], c.[D259],

									--	[D195]	= C192-D193-D194
									[D195]	=	c.[C192] - c.[D193] - c.[D194],

									--	[D203]	= IF(C201=0,0,D209*0.001)
									[D203] = CASE WHEN c.[C201] = 0.0 THEN 0.0 ELSE c.[D209] * 0.001 END,

									--	[D213]	= MIN(C211-D212,D221/L20*(1-L20))
									[D213]	= [$(DbOlefins)].[calc].[MinValue](c.[C211] - c.[D212], CASE WHEN c.[L20] <> 0.0 THEN c.[D221] / c.[L20] * (1.0 - c.[L20]) ELSE 0.0 END),

									--	[D222]	= MIN(C219-D220-D221,C19*0.99)
									[D222]	= [$(DbOlefins)].[calc].[MinValue](c.[C219] - c.[D220] - c.[D221], c.[C19] * 0.99),

									--	[D233]	= MIN(C231-D232,C24)
									[D233]	= [$(DbOlefins)].[calc].[MinValue](c.[C231] - c.[D232], c.[C24]),

									--	[D255]	= C252-D253-D254
									[D255]	= c.[C252] - c.[D253] - c.[D254],

									--	[D260]	= C257-D258-D259
									[D260]	= c.[C257] - c.[D258] - c.[D259]

								FROM (
									SELECT
										b.[GroupID],

										b.[C14], b.[C15], b.[C16], b.[C17], b.[C18], b.[C19], b.[C20], b.[C21], b.[C22], b.[C23], b.[C24], b.[C25], b.[C26], b.[C27], b.[C28], b.[C29], b.[C30], b.[C31], b.[C32], b.[C33], b.[C34], b.[C35],
										b.[C192], b.[C197], b.[C201], b.[C207], b.[C211], b.[C219], b.[C227], b.[C231], b.[C236], b.[C241], b.[C246], b.[C252], b.[C257], b.[C262],
										b.[D193], b.[D198], b.[D199], b.[D208], b.[D220], b.[D228], b.[D237], b.[D242], b.[D253], b.[D258], b.[D263],

										--	[D194]	= IF(C14>=C192,0,IF(C31>=C192+C197-D193,C192-D193,0))
										[D194]	=	CASE WHEN b.[C14] >= b.[C192]
													THEN 0.0
													ELSE
														CASE WHEN b.[C31] >= b.[C192] + b.[C197] - b.[D193]
														THEN b.[C192] - b.[D193]
														ELSE 0.0
														END
													END,

										--	[D202]	= IF(C201=0,0,D208/L18*(1-L18))
										[D202] = CASE WHEN b.[C201] = 0.0 OR b.[L18] = 0.0 THEN 0.0 ELSE b.[D208] / b.[L18] * (1.0 - b.[L18]) END,
								
										--	[D209]	= C207-D208
										[D209]	= b.[C207] - b.[D208],

										--	[D212]	= MIN(C211,D220/L21*(1-L21))
										[D212]	= [$(DbOlefins)].[calc].[MinValue](b.[C211], CASE WHEN b.[L21] <> 0.0 THEN b.[D220] / b.[L21] * (1.0 - b.[L21]) ELSE 0.0 END),

										--	[D221]	= MIN(C219-D220,C20*L20)
										[D221]	= [$(DbOlefins)].[calc].[MinValue](b.[C219] - b.[D220], b.[C20] * b.[L20]),

										--	[D229]	= C227-D228
										[D229]	= b.[C227] - b.[D228],

										--	[D232]	= MIN(C231,C25-D228)
										[D232]	= [$(DbOlefins)].[calc].[MinValue](b.[C231], b.[C25] - b.[D228]),

										--	[D254]	= MIN(C252-D253,C29-D258)
										[D254]	= [$(DbOlefins)].[calc].[MinValue](b.[C252] - b.[D253], b.[C29] - b.[D258]),

										--	[D259]	= MIN(C257-D258,C28-D253)
										[D259]	= [$(DbOlefins)].[calc].[MinValue](b.[C257] - b.[D258], b.[C28] - b.[D253]),

										--b.[L18],
										b.[L20]
										--b.[L21]

									FROM (
										SELECT
											a.[GroupID],

											a.[C14], a.[C15], a.[C16], a.[C17], a.[C18], a.[C19], a.[C20], a.[C21], a.[C22], a.[C23], a.[C24], a.[C25], a.[C26], a.[C27], a.[C28], a.[C29], a.[C30], a.[C31], a.[C32], a.[C33], a.[C34], a.[C35],
											a.[C192], a.[C197], a.[C201], a.[C207], a.[C211], a.[C219], a.[C227], a.[C231], a.[C236], a.[C241], a.[C246], a.[C252], a.[C257], a.[C262],

											--	[D193]	= IF(C14>=C192,C192,C14)
											[D193]	=	CASE WHEN a.[C14] >= a.[C192]
														THEN a.[C192]
														ELSE a.[C14]
														END,

											--	[D198]	= IF(C31>=C197,C197,C31)
											[D198]	=	CASE WHEN a.[C31] >= a.[C197]
														THEN a.[C197]
														ELSE a.[C31]
														END,

											--	[D199]	= IF(C31>=C197,0,C197-C31)
											[D199]	=	CASE WHEN a.[C31] >= a.[C197]
														THEN 0.0
														ELSE a.[C197] - a.[C31]
														END,

											--	[D208]	= MIN(C207,C18*L18)
											[D208]	= [$(DbOlefins)].[calc].[MinValue](a.[C207], a.[C18] * COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0) / 100.0),

											--	[D220]	= MIN(C219,C21*L21)
											[D220]	= [$(DbOlefins)].[calc].[MinValue](a.[C219], a.[C21] * COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0) / 100.0),

											--	[D228]	= MIN(C227,C25)
											[D228]	= [$(DbOlefins)].[calc].[MinValue](a.[C227], a.[C25]),

											--	[D237]	= MIN(C236,C24)
											[D237]	= [$(DbOlefins)].[calc].[MinValue](a.[C236], a.[C24]),

											--	[D242]	= MIN(C241,C26)
											[D242]	= [$(DbOlefins)].[calc].[MinValue](a.[C241], a.[C26]),

											--	[D253]	= MIN(C252,C28)
											[D253]	= [$(DbOlefins)].[calc].[MinValue](a.[C252], a.[C28]),

											--	[D258]	= MIN(C257,C29)
											[D258]	= [$(DbOlefins)].[calc].[MinValue](a.[C257], a.[C29]),

											--	[D263]	= F262
											[D263]	= a.[C262],

											[L18]	= COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0)	/ 100.0,
											[L20]	= COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0)	/ 100.0,
											[L21]	= COALESCE(bp.[EthyleneCG_WtPcnt], gp.[PropyleneRG_WtPcnt], 0.0)	/ 100.0

										FROM (
											SELECT
												y.[GroupID],
												[TargetID] = @TargetID,
												y.[DataType],
												[C14]	= y.[Hydrogen],
												[C15]	= y.[Methane],
												[C16]	= y.[Acetylene],
												[C17]	= y.[EthylenePG],
												[C18]	= y.[EthyleneCG],
												[C19]	= y.[PropylenePG],
												[C20]	= y.[PropyleneCG],
												[C21]	= y.[PropyleneRG],
												[C22]	= y.[PropaneC3Resid],
												[C23]	= y.[Butadiene],
												[C24]	= y.[IsoButylene],
												[C25]	= y.[C4Oth],
												[C26]	= y.[Benzene],
												[C27]	= y.[PyroGasoline],
												[C28]	= y.[PyroGasOil],
												[C29]	= y.[PyroFuelOil],
												[C30]	= y.[AcidGas],
												[C31]	= y.[PPFC],
												[C32]	= y.[ProdOther1],
												[C33]	= y.[ProdOther2],
												[C34]	= y.[LossFlareVent],
												[C35]	= y.[LossMeasure] + y.[LossOther],

												[C192]	= 						+ y.[DilHydrogen]	+ y.[ROGHydrogen],	
												[C197]	=						+ y.[DilMethane]	+ y.[ROGMethane],
												[C201]	=						+ y.[DilEthane]		+ y.[ROGEthane]		- y.[RecSuppEthane],
												[C207]	=	y.[ConcEthylene]	+ y.[DilEthylene]	+ y.[ROGEthylene],
												[C211]	=						+ y.[DilPropane]	+ y.[ROGPropane]	- y.[RecSuppPropane],
												[C219]	=	y.[ConcPropylene]	+ y.[DilPropylene]	+ y.[ROGPropylene],
												[C227]	=						+ y.[DilButane]		+ y.[ROGC4Plus]		- y.[RecSuppButane],
												[C231]	=						+ y.[DilButylene],
												[C236]	=	y.[ConcButadiene]	+ y.[DilButadiene],
												[C241]	=	y.[ConcBenzene]		+ y.[DilBenzene],
												[C246]	=						+ y.[DilMogas],		/*PyroGasoline*/
												[C252]	=	y.[SuppGasOil],
												[C257]	=	y.[SuppWashOil],
												[C262]	=	y.[SuppOther]		+ y.[ROGInerts]
											FROM	[$(DbOlefins)].[reports].[Yield]					y
											WHERE	y.[GroupID]		= @GroupId
												AND	y.[DataType]	= 'kMT'
											
											UNION ALL
											
											SELECT
												y.[GroupID],
												[TargetID] = @TargetID,
												y.[DataType],
												[C14]	= y.[Hydrogen],
												[C15]	= y.[Methane],
												[C16]	= y.[Acetylene],
												[C17]	= y.[EthylenePG],
												[C18]	= y.[EthyleneCG],
												[C19]	= y.[PropylenePG],
												[C20]	= y.[PropyleneCG],
												[C21]	= y.[PropyleneRG],
												[C22]	= y.[PropaneC3Resid],
												[C23]	= y.[Butadiene],
												[C24]	= y.[IsoButylene],
												[C25]	= y.[C4Oth],
												[C26]	= y.[Benzene],
												[C27]	= y.[PyroGasoline],
												[C28]	= y.[PyroGasOil],
												[C29]	= y.[PyroFuelOil],
												[C30]	= y.[AcidGas],
												[C31]	= y.[PPFC],
												[C32]	= y.[ProdOther1],
												[C33]	= y.[ProdOther2],
												[C34]	= y.[LossFlareVent],
												[C35]	= y.[LossMeasure] + y.[LossOther],

												[C192]	= 						+ y.[DilHydrogen]	+ y.[ROGHydrogen],	
												[C197]	=						+ y.[DilMethane]	+ y.[ROGMethane],
												[C201]	=						+ y.[DilEthane]		+ y.[ROGEthane]		- y.[RecSuppEthane],
												[C207]	=	y.[ConcEthylene]	+ y.[DilEthylene]	+ y.[ROGEthylene],
												[C211]	=						+ y.[DilPropane]	+ y.[ROGPropane]	- y.[RecSuppPropane],
												[C219]	=	y.[ConcPropylene]	+ y.[DilPropylene]	+ y.[ROGPropylene],
												[C227]	=						+ y.[DilButane]		+ y.[ROGC4Plus]		- y.[RecSuppButane],
												[C231]	=						+ y.[DilButylene],
												[C236]	=	y.[ConcButadiene]	+ y.[DilButadiene],
												[C241]	=	y.[ConcBenzene]		+ y.[DilBenzene],
												[C246]	=						+ y.[DilMogas],		/*PyroGasoline*/
												[C252]	=	y.[SuppGasOil],
												[C257]	=	y.[SuppWashOil],
												[C262]	=	y.[SuppOther]		+ y.[ROGInerts]
											FROM	[$(DatabaseName)].[reports].[Yield]					y
											WHERE	y.[GroupID]		= @TargetId
												AND	y.[DataType]	= 'kMT'
											) a		--	1
										LEFT OUTER JOIN	[$(DbOlefins)].[reports].[GapProductPurity]		gp
											ON	gp.[GroupId]	= a.[GroupID]
										LEFT OUTER JOIN	[$(DatabaseName)].[reports].[GapProductPurity]	bp
											ON	bp.[GroupId]	= a.[TargetID]
										) b			--	2
									) c				--	3
								) d					--	4
							) e						--	5
						) f							--	6
					) g								--	7
				) h									--	8
			) i;									--	9

		SET @ProcedureDesc = CHAR(9) + '@DispositionSupp Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @DispositionSupp	TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[StreamId]			VARCHAR(42)	NOT	NULL	CHECK([StreamId] <> ''),
			[Quantity_kMT]		REAL		NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [StreamId] ASC)
		);

		INSERT INTO @DispositionSupp([GroupId], [StreamId], [Quantity_kMT])
		SELECT
			p.[GroupId],
			p.[StreamId],
			p.[Quantity_kMT]
		FROM (
			SELECT
				d.[GroupId],
				[Hydrogen]			= d.[D193],
				[Methane]			= d.[D195] + d.[D199] + d.[D205] + d.[D217] + d.[D225],
				[Acetylene]			= CONVERT(REAL, 0.0),
				[EthylenePG]		= d.[D203] + d.[D209],
				[EthyleneCG]		= d.[D202] + d.[D208],
				[PropylenePG]		= d.[D214] + d.[D222],
				[PropyleneCG]		= d.[D213] + d.[D221],
				[PropyleneRG]		= d.[D212] + d.[D220],
				[PropaneC3Resid]	= d.[D215] + d.[D223],
				[Butadiene]			= d.[D237],
				[IsoButylene]		= d.[D233] + d.[D238],
				[C4Oth]				= d.[D228] + d.[D232] + d.[D239],
				[Benzene]			= d.[D242] + d.[D248],
				[PyroGasoline]		= d.[D229] + d.[D234] + d.[D243] + d.[D247],
				[PyroGasOil]		= d.[D244] + d.[D249] + d.[D253] + d.[D259],
				[PyroFuelOil]		= d.[D254] + d.[D258],
				[AcidGas]			= CONVERT(REAL, 0.0),
				[PPFC]				= d.[D194] + d.[D198] + d.[D204] + d.[D216] + d.[D224] + d.[D250] + d.[D255] + d.[D260] + d.[D263],
				[ProdOther1]		= CONVERT(REAL, 0.0),
				[ProdOther2]		= CONVERT(REAL, 0.0),
				[LossFlareVent]		= CONVERT(REAL, 0.0),
				[LossOther]			= CONVERT(REAL, 0.0)
			FROM @Disposition	d
			) u
			UNPIVOT (
				[Quantity_kMT] FOR [StreamId] IN
				(
				[Hydrogen],
				[Methane],
				[Acetylene],
				[EthylenePG],
				[EthyleneCG],
				[PropylenePG],
				[PropyleneCG],
				[PropyleneRG],
				[PropaneC3Resid],
				[Butadiene],
				[IsoButylene],
				[C4Oth],
				[Benzene],
				[PyroGasoline],
				[PyroGasOil],
				[PyroFuelOil],
				[AcidGas],
				[PPFC],
				[ProdOther1],
				[ProdOther2],
				[LossFlareVent],
				[LossOther]
				)
			) p;

		SET @ProcedureDesc = CHAR(9) + '@Production Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @Production			TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[StreamId]			VARCHAR(42)	NOT	NULL	CHECK([StreamId] <> ''),
			[Quantity_kMT]		REAL			NULL,
			[Amount_Cur]		REAL		NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [StreamId] ASC)
		);

		INSERT INTO @Production([GroupID], [StreamId], [Quantity_kMT], [Amount_Cur])
		SELECT
			d.[GroupID],
			d.[StreamId],
			d.[Quantity_kMT],
			d.[Amount_Cur]
		FROM (
			SELECT
				CASE y.[DataType]
					WHEN '$PerMT'	THEN 'Amount_Cur'
					WHEN 'kMT'		THEN 'Quantity_kMT'
					END	[DataType],
				y.[GroupID],
				y.[Hydrogen],
				y.[Methane],
				y.[Acetylene],
				y.[EthylenePG],
				y.[EthyleneCG],
				y.[PropylenePG],
				y.[PropyleneCG],
				y.[PropyleneRG],
				y.[PropaneC3Resid],
				y.[Butadiene],
				y.[IsoButylene],
				y.[C4Oth],
				y.[Benzene],
				y.[PyroGasoline],
				y.[PyroGasOil],
				y.[PyroFuelOil],
				y.[AcidGas],
				y.[PPFC],
				y.[ProdOther1],
				y.[ProdOther2],
				y.[LossFlareVent],
				[LossOther] = y.[LossOther] + y.[LossMeasure]
			FROM [$(DbOlefins)].[reports].[Yield]		y
			WHERE y.[GroupID] = @GroupId

			UNION ALL

			SELECT
				CASE y.[DataType]
					WHEN '$PerMT'	THEN 'Amount_Cur'
					WHEN 'kMT'		THEN 'Quantity_kMT'
					END	[DataType],
				y.[GroupID],
				y.[Hydrogen],
				y.[Methane],
				y.[Acetylene],
				y.[EthylenePG],
				y.[EthyleneCG],
				y.[PropylenePG],
				y.[PropyleneCG],
				y.[PropyleneRG],
				y.[PropaneC3Resid],
				y.[Butadiene],
				y.[IsoButylene],
				y.[C4Oth],
				y.[Benzene],
				y.[PyroGasoline],
				y.[PyroGasOil],
				y.[PyroFuelOil],
				y.[AcidGas],
				y.[PPFC],
				y.[ProdOther1],
				y.[ProdOther2],
				y.[LossFlareVent],
				[LossOther] = y.[LossOther] + y.[LossMeasure]
			FROM [$(DatabaseName)].[reports].[Yield]		y
			WHERE y.[GroupID] = @TargetId
			) p
			UNPIVOT (
				[Value]	FOR [StreamId] IN
				(
				[Hydrogen],
				[Methane],
				[Acetylene],
				[EthylenePG],
				[EthyleneCG],
				[PropylenePG],
				[PropyleneCG],
				[PropyleneRG],
				[PropaneC3Resid],
				[Butadiene],
				[IsoButylene],
				[C4Oth],
				[Benzene],
				[PyroGasoline],
				[PyroGasOil],
				[PyroFuelOil],
				[AcidGas],
				[PPFC],
				[ProdOther1],
				[ProdOther2],
				[LossFlareVent],
				[LossOther]
				)
			) u
			PIVOT (
				MAX(u.[Value]) FOR u.[DataType] IN
				(
					[Quantity_kMT],
					[Amount_Cur]
				)
			) d;

		SET @ProcedureDesc = CHAR(9) + '@ProdSupp Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @ProdSupp			TABLE
		(
			[GroupId]			VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[StreamId]			VARCHAR(42)	NOT	NULL	CHECK([StreamId] <> ''),
			[Production_kMT]	REAL		NOT	NULL,
			[Supplemental_kMT]	REAL		NOT	NULL,
			[ProdSupp_kMT]		REAL		NOT	NULL,

			[Amount_Cur]		REAL		NOT	NULL,

			[Production_Cur]	AS [Production_kMT]		* [Amount_Cur] / 1000.0
								PERSISTED	NOT	NULL,
			[Supplemental_Cur]	AS [Supplemental_kMT]	* [Amount_Cur] / 1000.0
								PERSISTED	NOT	NULL,
			[ProdSupp_Cur]		AS [ProdSupp_kMT]		* [Amount_Cur] / 1000.0
								PERSISTED	NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [StreamId] ASC)
		);

		INSERT INTO @ProdSupp([GroupId], [StreamId], [Production_kMT], [Supplemental_kMT], [ProdSupp_kMT], [Amount_Cur])
		SELECT
			d.[GroupId],
			d.[StreamId],
			[Production_kMT]	= p.[Quantity_kMT],
			[Supplemental_kMT]	= d.[Quantity_kMT] -
				CASE d.StreamId
					WHEN 'EthylenePG'	THEN dis.D203
					WHEN 'EthyleneCG'	THEN dis.D202
					WHEN 'PropylenePG'	THEN dis.D214
					WHEN 'PropyleneCG'	THEN dis.D213
					WHEN 'PropyleneRG'	THEN dis.D212
					ELSE 0.0
				END,
			[ProdSupp_kMT]		= d.[Quantity_kMT],
			p.[Amount_Cur]
		FROM @DispositionSupp			d
		INNER JOIN @Production			p
			ON	p.[GroupId]		= d.[GroupId]
			AND	p.[StreamId]	= d.[StreamId]
		INNER JOIN @Disposition			dis
			ON	dis.[GroupId]		= d.[GroupId];

		SET @ProcedureDesc = CHAR(9) + '@ProdSuppAggregate Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @ProdSuppAggregate	TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Production_kMT]		REAL		NOT	NULL,
			[AvgProduction_Cur]		AS 	[Production_Cur] / [Production_kMT] * 1000.0
									PERSISTED	NOT	NULL,
			[Production_Cur]		REAL		NOT	NULL,

			[Supplemental_kMT]		REAL		NOT	NULL,
			[AvgSupplemental_Cur]	AS 	CASE
										WHEN [Supplemental_kMT] <> 0.0
										THEN [Supplemental_Cur] / [Supplemental_kMT] * 1000.0
										ELSE 0.0
										END
									PERSISTED	NOT	NULL,
			[Supplemental_Cur]		REAL		NOT	NULL,

			[ProdPyro_kMT]			AS [Production_kMT] - [Supplemental_kMT]
									PERSISTED	NOT	NULL,
			[ProdPyro_Cur]			AS [Production_Cur] - [Supplemental_Cur]
									PERSISTED	NOT	NULL,
			[ProdPyro_KmtCur]		AS ([Production_Cur] - [Supplemental_Cur]) / ([Production_kMT] - [Supplemental_kMT]) * 1000.0
									PERSISTED	NOT	NULL,

			[ActProdPyro_Val]		AS [Production_Cur] / ([Production_kMT] - [Supplemental_kMT]) * 1000.0
									PERSISTED	NOT	NULL,

			[ProdHvc_kMT]			REAL		NOT	NULL,
			[ProdSupp_kMT]			REAL		NOT	NULL,
			[ProdSupp_Cur]			REAL		NOT	NULL,

			[ProductionPyro_kMT]	AS [Production_kMT] - [ProdSupp_kMT]
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @ProdSuppAggregate([GroupId], [Production_kMT], [Production_Cur], [Supplemental_kMT], [Supplemental_Cur], [ProdHvc_kMT], [ProdSupp_kMT], [ProdSupp_Cur])
		SELECT
			ps.[GroupId],
			[Production_kMT]	= SUM(ps.[Production_kMT]),
			[Production_Cur]	= SUM(ps.[Production_Cur]),

			[Supplemental_kMT]	= SUM(ps.[Supplemental_kMT]),
			[Supplemental_Cur]	= SUM(ps.[Supplemental_Cur]),

			[ProdHvc_kMT]		= SUM(CASE WHEN b.[DescendantId] IS NOT NULL THEN ps.[Supplemental_kMT]
				ELSE 0.0
				END),

			[ProdSupp_kMT]		= SUM(ps.[ProdSupp_kMT]),
			[ProdSupp_Cur]		= SUM(ps.[ProdSupp_Cur])
		FROM @ProdSupp										ps
		LEFT OUTER JOIN [$(DbOlefins)].[dim].[Stream_Bridge]		b
			ON	b.[FactorSetId]		= @FactorSetId
			AND	b.[StreamId]		= 'ProdHvc'
			AND	b.[DescendantId]	= ps.[StreamId]
		GROUP BY
			ps.[GroupId];	

		END;

		BEGIN

		SET @ProcedureDesc = CHAR(9) + '@CapUtil Declaration / Insert';
		PRINT @ProcedureDesc;

		DECLARE @CapUtil	TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL,

			[GroupProdCap_kMT]		REAL		NOT	NULL,
			[GroupSuppCap_kMT]		REAL		NOT	NULL,
			[GroupPyroCap_kMT]		AS			([GroupProdCap_kMT] - [GroupSuppCap_kMT])
									PERSISTED	NOT	NULL,
			[GroupCapability_Pcnt]	REAL		NOT	NULL,

			[GroupProdCpby_kMT]		AS			([GroupProdCap_kMT] / [GroupCapability_Pcnt] * 100.0)
									PERSISTED	NOT	NULL,
			[GroupPyroCpby_kMT]		AS			(([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0)
									PERSISTED	NOT	NULL,
			[GroupSuppCpby_kMT]		AS			([GroupSuppCap_kMT] / [GroupCapability_Pcnt] * 100.0)
									PERSISTED	NOT	NULL,

			[GroupPyroCpby_Pcnt]	AS			([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / ([GroupProdCap_kMT])
									PERSISTED	NOT	NULL,
			[GroupSuppCpby_Pcnt]	AS			([GroupSuppCap_kMT]) / ([GroupProdCap_kMT])
									PERSISTED	NOT	NULL,

			[GroupOlefinsCpby_kMT]	REAL		NOT	NULL,
			[GroupEthyleneCpby_kMT]	REAL		NOT	NULL,

			[TargetId]				VARCHAR(25)		NULL,
			[BenchProdCap_kMT]		REAL			NULL,
			[BenchPyroCap_kMT]		AS			([BenchProdCap_kMT] - [BenchSuppCap_kMT])
									PERSISTED,
			[BenchSuppCap_kMT]		REAL			NULL,
			[BenchCapability_Pcnt]	REAL			NULL,
			[BenchProdCpby_kMT]		AS			([BenchProdCap_kMT] / [BenchCapability_Pcnt] * 100.0)
									PERSISTED,
			[BenchPyroCpby_kMT]		AS			(([BenchProdCap_kMT] - [BenchSuppCap_kMT]) / [BenchCapability_Pcnt] * 100.0)
									PERSISTED,
			[BenchSuppCpby_kMT]		AS			([BenchSuppCap_kMT] / [BenchCapability_Pcnt] * 100.0)
									PERSISTED,

			[BenchPyroCpby_Pcnt]	AS			([BenchProdCap_kMT] - [BenchSuppCap_kMT]) / ([BenchProdCap_kMT])
									PERSISTED,
			[BenchSuppCpby_Pcnt]	AS			([BenchSuppCap_kMT]) / ([BenchProdCap_kMT])
									PERSISTED,

			[BenchOlefinsCpby_kMT]	REAL			NULL,
			[BenchEthyleneCpby_kMT]	REAL			NULL,

			[HvcProd_Ratio]			AS [GroupProdCap_kMT] / [GroupCapability_Pcnt] / [BenchProdCap_kMT] * [BenchCapability_Pcnt]
									PERSISTED,
			[Olefins_Ratio]			AS [GroupOlefinsCpby_kMT] / [BenchOlefinsCpby_kMT]
									PERSISTED,

			[DiffCapability_Pcnt]	AS ([GroupCapability_Pcnt] - [BenchCapability_Pcnt])
									PERSISTED,
			[EnergyCap_Ratio]		AS ([BenchCapability_Pcnt] - [GroupCapability_Pcnt]) * (([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt]) / ([BenchProdCap_kMT] - [BenchSuppCap_kMT])
									PERSISTED,

			[PyroCapUtil_Ratio]		AS ([GroupCapability_Pcnt] - [BenchCapability_Pcnt]) * 
									(([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0) -- [GroupPyroCpby_kMT]
									/ 
									([BenchProdCap_kMT] - [BenchSuppCap_kMT])    -- BenchPyroCap
									/ 100.0
									PERSISTED,

			[SuppCapUtil_Ratio]		AS CASE WHEN [BenchSuppCap_kMT] <> 0.0 THEN
									([GroupCapability_Pcnt] - [BenchCapability_Pcnt]) * 
									([GroupSuppCap_kMT] / [GroupCapability_Pcnt] * 100.0) -- [GroupSuppCpby_kMT] 
									/ 
									[BenchSuppCap_kMT] 
									/ 100.0
									ELSE
										0.0
									END
									PERSISTED,

			[OlefinsCpby_Ratio]		AS	([BenchOlefinsCpby_kMT] - [GroupOlefinsCpby_kMT]) / [BenchOlefinsCpby_kMT]
									PERSISTED,

			[PyroProd_Ratio]		AS	(
											(([GroupProdCap_kMT] - [GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0)
										-	(([BenchProdCap_kMT] - [BenchSuppCap_kMT]) / [BenchCapability_Pcnt] * 100.0)
										)
										/ 
										(
											([GroupProdCap_kMT] / [GroupCapability_Pcnt] * 100.0)
										-	([BenchProdCap_kMT] / [BenchCapability_Pcnt] * 100.0)
										)
									PERSISTED,

			[SuppProd_Ratio]		AS	(
											(([GroupSuppCap_kMT]) / [GroupCapability_Pcnt] * 100.0)
										-	(([BenchSuppCap_kMT]) / [BenchCapability_Pcnt] * 100.0)
										)
										/ 
										(
											([GroupProdCap_kMT] / [GroupCapability_Pcnt] * 100.0)
										-	([BenchProdCap_kMT] / [BenchCapability_Pcnt] * 100.0)
										)
									PERSISTED,

			PRIMARY KEY CLUSTERED ([GroupId] ASC)
		);

		INSERT INTO @CapUtil
		(
			[GroupId], [GroupProdCap_kMT], [GroupSuppCap_kMT], [GroupCapability_Pcnt], [GroupOlefinsCpby_kMT], [GroupEthyleneCpby_kMT],
			[TargetId], [BenchProdCap_kMT], [BenchSuppCap_kMT], [BenchCapability_Pcnt], [BenchOlefinsCpby_kMT], [BenchEthyleneCpby_kMT]
		)
		SELECT
			gc.[GroupID],
			gc.[HVCProd_kMT],
			COALESCE(gs.[ProdHvc_kMT], 0.0),
			gc.[OlefinsCpbyUtil_pcnt],
			gc.[OlefinsCpbyAvg_kMT],
			gc.[EthyleneCpbyAvg_kMT],

			bc.[GroupID],
			bc.[HVCProd_kMT],
			COALESCE(bs.[ProdHvc_kMT], 0.0),
			bc.[OlefinsCpbyUtil_pcnt],
			bc.[OlefinsCpbyAvg_kMT],
			bc.[EthyleneCpbyAvg_kMT]

		FROM		[$(DbOlefins)].[reports].[CapUtil]			gc
		INNER JOIN	[$(DbOlefins)].[reports].[Pyrolysis]			gp
			ON	gp.[GroupID]	=	gc.[GroupID]
			AND	gp.[DataType]	=	'SuppkMT'
			AND	gp.[Currency]	=	'USD'
			AND	gp.[Scenario]	=	'Basis'
			AND	gp.[SimModelID]	=	'Plant'
		LEFT OUTER JOIN @ProdSuppAggregate					gs
			ON	gs.[GroupId]	=	gc.[GroupID]

		LEFT OUTER JOIN [$(DatabaseName)].[reports].[CapUtil]		bc
			ON	bc.[GroupID]	=	@TargetId
			AND	bc.[GroupID]	<>	gc.[GroupID]
		LEFT OUTER JOIN @ProdSuppAggregate					bs
			ON	bs.[GroupId]	=	bc.[GroupID]

		WHERE	gc.[GroupID]	=	@GroupId;

		INSERT INTO @CapUtil
		(
			[GroupId], [GroupProdCap_kMT], [GroupSuppCap_kMT], [GroupCapability_Pcnt], [GroupOlefinsCpby_kMT], [GroupEthyleneCpby_kMT]
		)
		SELECT
			gc.[GroupID],
			gc.[HVCProd_kMT],
			COALESCE(gs.[ProdHvc_kMT], 0.0),
			gc.[OlefinsCpbyUtil_pcnt],
			gc.[OlefinsCpbyAvg_kMT],
			gc.[EthyleneCpbyAvg_kMT]

		FROM		[$(DatabaseName)].[reports].[CapUtil]			gc
		INNER JOIN	[$(DatabaseName)].[reports].[Pyrolysis]		gp
			ON	gp.[GroupID]	=	gc.[GroupID]
			AND	gp.[DataType]	=	'SuppkMT'
			AND	gp.[Currency]	=	'USD'
			AND	gp.[Scenario]	=	'Basis'
			AND	gp.[SimModelID]	=	'Plant'
		LEFT OUTER JOIN @ProdSuppAggregate					gs
			ON	gs.[GroupId]	=	gc.[GroupID]
		WHERE	gc.[GroupID]	=	@TargetId;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9		->	 934 (GAM!B34)	-> Pyrolysis Capacity (GAM!B13)(Insert Complete)';			--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F10		->	1046 (GAM!B46)	-> Supplemental Capacity (Insert Complete)';				--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F12:G12	->	1200			-> Energy Prices (Insert Complete)';						--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F15		->	1500			-> Energy Intensity (Insert Complete)';						--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F48:G49	->	4700			-> Energy Efficiency and Mix (Insert Complete)';
		PRINT @ProcedureDesc;

		BEGIN

		DECLARE @Energy	TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Currency]				VARCHAR(4)	NOT	NULL	CHECK([Currency] <> ''),
			[Scenario]				VARCHAR(24)	NOT	NULL	CHECK([Scenario] <> ''),
			[AccountId]				VARCHAR(24)	NOT	NULL	CHECK([AccountId] <> ''),
			[Amount_Cur]			REAL		NOT	NULL,
			[Energy_MBTU]			REAL		NOT	NULL,
			[Energy_MBtuHvc]		REAL		NOT	NULL,

			[Rate_CurMBtu]			AS CONVERT(REAL,
									CASE
									WHEN [Energy_MBTU] = 0.0
									THEN
										0.0
									ELSE
										[Amount_Cur] / [Energy_MBTU] * 1000.0
									END)
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([GroupId] ASC, [Currency] ASC, [AccountId] ASC)
		);

		INSERT INTO @Energy([GroupId], [Currency], [Scenario], [AccountId], [Amount_Cur], [Energy_MBTU], [Energy_MBtuHvc])
		SELECT
			o.[GroupID],
			o.[Currency],
			eAdj.[Scenario],
			cur.[AccountId],
			cur.[Amount_Cur],
			egy.[Energy_MBTU],
			hvc.[Energy_MBTU]
		FROM		[$(DbOlefins)].[reports].[OPEX]					o
		INNER JOIN	[$(DbOlefins)].[reports].[EnergyRaw]				eAdj
			ON	eAdj.[GroupID]		= o.[GroupID]
			AND	eAdj.[DataType]		= 'MBTU'

		INNER JOIN	[$(DbOlefins)].[reports].[EnergyRaw]				eHvc
			ON	eHvc.[GroupID]		= o.[GroupID]
			AND	eHvc.[DataType]		= 'kBtuPerHVCMt'
			AND	eHvc.[Scenario]		= eAdj.[Scenario]

		CROSS APPLY (VALUES
			('PurElec',	o.[PurElec]),
			('PurSteam', o.[PurSteam]),
			('PurFuelGas', o.[PurFuelGas] + o.[PurLiquid] + o.[PPFCFuelGas] + o.[PPFCOther]),
			('ExpSteam', o.[ExportSteam]),
			('ExpElectric', o.[ExportElectric])
			) cur([AccountId], [Amount_Cur])

		CROSS APPLY (VALUES
			('PurElec', eAdj.[PurElec]),
			('PurSteam', eAdj.[PurSteamSHP] + eAdj.[PurSteamHP] + eAdj.[PurSteamIP] + eAdj.[PurSteamLP]),
			('PurFuelGas',  eAdj.[PurFuelNatural] + eAdj.[PurOther] + eAdj.[PPFC]),
			('ExpSteam', eAdj.[ExportSteamSHP] + eAdj.[ExportSteamHP] + eAdj.[ExportSteamIP] + eAdj.[ExportSteamLP]),
			('ExpElectric', eAdj.[ExportElectric])
			) egy([EnergyId], [Energy_MBTU])

		CROSS APPLY (VALUES
			('PurElec', eHvc.[PurElec]),
			('PurSteam', eHvc.[PurSteamSHP] + eHvc.[PurSteamHP] + eHvc.[PurSteamIP] + eHvc.[PurSteamLP]),
			('PurFuelGas',  eHvc.[PurFuelNatural] + eHvc.[PurOther] + eHvc.[PPFC]),
			('ExpSteam', eHvc.[ExportSteamSHP] + eHvc.[ExportSteamHP] + eHvc.[ExportSteamIP] + eHvc.[ExportSteamLP]),
			('ExpElectric', eHvc.[ExportElectric])
			) hvc([EnergyId], [Energy_MBTU])

		WHERE	cur.[AccountId]	= egy.[EnergyId]
			AND	cur.[AccountId]	= hvc.[EnergyId]
			AND	o.[GroupID]		= @GroupId
			AND	o.[DataType]	= 'ADJ';

		INSERT INTO @Energy([GroupId], [Currency], [Scenario], [AccountId], [Amount_Cur], [Energy_MBTU], [Energy_MBtuHvc])
		SELECT
			o.[GroupID],
			o.[Currency],
			eAdj.[Scenario],
			cur.[AccountId],
			cur.[Amount_Cur],
			egy.[Energy_MBTU],
			hvc.[Energy_MBTU]
		FROM		[$(DatabaseName)].[reports].[OPEX]				o
		INNER JOIN	[$(DatabaseName)].[reports].[EnergyRaw]			eAdj
			ON	eAdj.[GroupID]		= o.[GroupID]
			AND	eAdj.[DataType]		= 'MBTU'

		INNER JOIN	[$(DatabaseName)].[reports].[EnergyRaw]			eHvc
			ON	eHvc.[GroupID]		= o.[GroupID]
			AND	eHvc.[DataType]		= 'kBtuPerHVCMt'
			AND	eHvc.[Scenario]		= eAdj.[Scenario]

		CROSS APPLY (VALUES
			('PurElec',	o.[PurElec]),
			('PurSteam', o.[PurSteam]),
			('PurFuelGas', o.[PurFuelGas] + o.[PurLiquid] + o.[PPFCFuelGas] + o.[PPFCOther]),
			('ExpSteam', o.[ExportSteam]),
			('ExpElectric', o.[ExportElectric])
			) cur([AccountId], [Amount_Cur])

		CROSS APPLY (VALUES
			('PurElec', eAdj.[PurElec]),
			('PurSteam', eAdj.[PurSteamSHP] + eAdj.[PurSteamHP] + eAdj.[PurSteamIP] + eAdj.[PurSteamLP]),
			('PurFuelGas',  eAdj.[PurFuelNatural] + eAdj.[PurOther] + eAdj.[PPFC]),
			('ExpSteam', eAdj.[ExportSteamSHP] + eAdj.[ExportSteamHP] + eAdj.[ExportSteamIP] + eAdj.[ExportSteamLP]),
			('ExpElectric', eAdj.[ExportElectric])
			) egy([EnergyId], [Energy_MBTU])

		CROSS APPLY (VALUES
			('PurElec', eHvc.[PurElec]),
			('PurSteam', eHvc.[PurSteamSHP] + eHvc.[PurSteamHP] + eHvc.[PurSteamIP] + eHvc.[PurSteamLP]),
			('PurFuelGas',  eHvc.[PurFuelNatural] + eHvc.[PurOther] + eHvc.[PPFC]),
			('ExpSteam', eHvc.[ExportSteamSHP] + eHvc.[ExportSteamHP] + eHvc.[ExportSteamIP] + eHvc.[ExportSteamLP]),
			('ExpElectric', eHvc.[ExportElectric])
			) hvc([EnergyId], [Energy_MBTU])

		WHERE	cur.[AccountId]	= egy.[EnergyId]
			AND	cur.[AccountId]	= hvc.[EnergyId]
			AND	o.[GroupID]		= @TargetId
			AND	o.[DataType]	= 'ADJ';

		DECLARE @EnergyAdj	TABLE
		(
			[DataType]				VARCHAR(4)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Currency]				VARCHAR(4)	NOT	NULL	CHECK([Currency] <> ''),
			[AccountId]				VARCHAR(24)	NOT	NULL	CHECK([AccountId] <> ''),
			[Amount_Cur]			REAL		NOT	NULL,
			[Energy_MBTU]			REAL		NOT	NULL,
			[Rate_CurMBtu]			REAL		NOT	NULL,

			[EEI_PYPS]				REAL		NOT	NULL,
			[NetEnergyCons_MBTU]	REAL		NOT	NULL,
			[StmEnergyCons_MBTU]	REAL		NOT	NULL,	/*	Stream Energy Consumption	*/

			[StreamAmount_Cur]		AS CONVERT(REAL, [Amount_Cur]	* [StmEnergyCons_MBTU] / [NetEnergyCons_MBTU])
									PERSISTED	NOT	NULL,
			[StreamEnergy_MBTU]		AS CONVERT(REAL, [Energy_MBTU]	* [StmEnergyCons_MBTU] / [NetEnergyCons_MBTU] / 1000.0)
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([DataType] ASC, [GroupId] ASC, [Currency] ASC, [AccountId] ASC)
		);

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Pyro',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			(en.[NetEnergyCons] - (en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0))
		FROM @Energy												ec
		INNER JOIN		[$(DbOlefins)].[reports].[EnergyRaw]				en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[$(DbOlefins)].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @GroupId;

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Supp',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0
		FROM @Energy												ec
		INNER JOIN		[$(DbOlefins)].[reports].[EnergyRaw]				en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[$(DbOlefins)].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @GroupId;

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Pyro',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			(en.[NetEnergyCons] - (en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0))
		FROM @Energy												ec
		INNER JOIN		[$(DatabaseName)].[reports].[EnergyRaw]			en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[$(DatabaseName)].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @TargetId;

		INSERT INTO @EnergyAdj([DataType], [GroupId], [Currency], [AccountId], [Amount_Cur], [Energy_MBTU], [Rate_CurMBtu], [EEI_PYPS], [NetEnergyCons_MBTU], [StmEnergyCons_MBTU])
		SELECT
			'Supp',
			ec.[GroupId],
			ec.[Currency],
			ec.[AccountId],
			ec.[Amount_Cur],
			ec.[Energy_MBTU],
			ec.[Rate_CurMBtu],
			en.[EEI_PYPS],
			en.[NetEnergyCons],
			en.[EEI_PYPS] * COALESCE(es.[SuppTot], 0.0) / 100.0
		FROM @Energy												ec
		INNER JOIN		[$(DatabaseName)].[reports].[EnergyRaw]			en
			ON	en.[GroupID]		= ec.[GroupID]
			AND	en.[DataType]		= 'MBTU'
		LEFT OUTER JOIN	[$(DatabaseName)].[reports].[GapEnergySeparation]	es
			ON	es.[GroupID]		= ec.[GroupID]
			AND	es.[DataType]		= 'MBTU'
		WHERE ec.GroupId			= @TargetId;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			t.[GroupId],
			t.[TargetId],
			t.[GroupProdCap_kMT],
			t.[BenchProdCap_kMT],
			[GapId] = CASE x.[GapId]
				WHEN 'EnergyCap'
				THEN t.[DataType] + '_' +  x.[GapId]
				ELSE x.[GapId]
				END,
			t.[Currency],
			t.[DataType],
			SUM(x.[GapAmount_Cur])
		FROM (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				cu.[GroupProdCap_kMT],
				cu.[BenchProdCap_kMT],
				gE.[AccountId],
				gE.[Currency],
				gE.[DataType],

				[GroupStreamAmount_Cur]	= gE.[StreamAmount_Cur],	--	Cost
				[BenchStreamAmount_Cur]	= bE.[StreamAmount_Cur],	--	Cost

				[GroupRate_CurMBtu]		= gE.[Rate_CurMBtu],		--	Price
				[BenchRate_CurMBtu]		= bE.[Rate_CurMBtu],		--	Price

				[EnergyCap] = 
					cu.[BenchCapability_Pcnt] / 100.0 *
					CASE gE.DataType
					WHEN 'Pyro'
					THEN (cu.[BenchPyroCpby_kMT] - cu.[GroupPyroCpby_kMT]) / cu.[BenchPyroCap_kMT]
					WHEN 'Supp'
					THEN
						CASE
						WHEN cu.[BenchSuppCap_kMT] = 0.0
						THEN 0.0
						ELSE (cu.[BenchSuppCpby_kMT] - cu.[GroupSuppCpby_kMT]) / cu.[BenchSuppCap_kMT]
						END
					END,

				[EnergyEff]	= (bE.[EEI_PYPS] / gE.[EEI_PYPS] - 1.0),
	
				[EnergyPrc]	=
					bE.[StreamEnergy_MBTU] *
					CASE gE.[DataType]
					WHEN 'Pyro'
					THEN (cu.[GroupPyroCap_kMT] / cu.[BenchPyroCap_kMT])
					WHEN 'Supp'
					THEN
						CASE WHEN  cu.[BenchSuppCap_kMT] = 0.0
						THEN 0.0
						ELSE (cu.[GroupSuppCap_kMT] / cu.[BenchSuppCap_kMT])
						END
					END,

				[EnergyMix]	=
					CASE ge.[DataType]
					WHEN 'Pyro'
					THEN (cu.[GroupPyroCap_kMT] / cu.[BenchPyroCap_kMT])
					WHEN 'Supp'
					THEN
						CASE WHEN cu.[BenchSuppCap_kMT] = 0.0
						THEN 0.0
						ELSE (cu.[GroupSuppCap_kMT] / cu.[BenchSuppCap_kMT])
						END
					END
					* be.[StmEnergyCons_MBTU]
					* (CASE WHEN bE.[StmEnergyCons_MBTU] = 0.0 THEN 0.0 ELSE bE.[StreamEnergy_MBTU] / bE.[StmEnergyCons_MBTU] END 
						- CASE WHEN gE.[StmEnergyCons_MBTU] = 0.0 THEN 0.0 ELSE gE.[StreamEnergy_MBTU] / gE.[StmEnergyCons_MBTU] END),

				[EnergyInt] =
					CASE WHEN gE.[StmEnergyCons_MBTU] = 0.0
					THEN 0.0
					ELSE (be.[StmEnergyCons_MBTU] / gE.[StmEnergyCons_MBTU])
					END
					*
					CASE gE.[DataType]
					WHEN 'Pyro'
					THEN (cu.[GroupPyroCap_kMT] / cu.[BenchPyroCap_kMT])
					WHEN 'Supp'
					THEN
						CASE WHEN cu.[BenchSuppCap_kMT] = 0.0
						THEN 0.0
						ELSE (cu.[GroupSuppCap_kMT] / cu.[BenchSuppCap_kMT])
						END
					END
					 - (bE.[EEI_PYPS] / gE.[EEI_PYPS])

			FROM @CapUtil			cu
			INNER JOIN @EnergyAdj	gE
				ON	gE.[GroupId]	= cu.[GroupId]
			INNER JOIN @EnergyAdj	bE
				ON	bE.[GroupId]	= cu.[TargetId]
				AND	bE.[DataType]	= gE.[DataType]
				AND	bE.[AccountId]	= gE.[AccountId]
				AND	bE.[Currency]	= gE.[Currency]
			WHERE	cu.[TargetId]	IS NOT NULL
			) t
		CROSS APPLY( VALUES
			('EnergyCap',	t.[EnergyCap] * t.[BenchStreamAmount_Cur] / 1000.0),
			('EnergyEff',	t.[EnergyEff] * t.[GroupStreamAmount_Cur] / 1000.0),
			('EnergyPrc',	t.[EnergyPrc] * (t.[BenchRate_CurMBtu] - t.[GroupRate_CurMBtu]) / 1000.0),
			('EnergyMix',	t.[EnergyMix] * t.[GroupRate_CurMBtu] / 1000.0),
			('EnergyInt',	t.[EnergyInt] * t.[GroupStreamAmount_Cur] / 1000.0)
			) x ([GapId], [GapAmount_Cur])
		GROUP BY
			t.[GroupId],
			t.[TargetId],
			t.[GroupProdCap_kMT],
			t.[BenchProdCap_kMT],
			x.[GapId],
			t.[Currency],
			t.[DataType]
		HAVING SUM(x.[GapAmount_Cur]) IS NOT NULL;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F52:G54	->	5100			-> Other Variable Costs (Insert Complete)'
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	 935 (GAM!B35)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	1047 (GAM!D47)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		BEGIN

		DECLARE @OVC	TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),

			[Prod_kMT]				REAL			NOT	NULL,
	
			[Stream_kMT]			REAL			NOT	NULL,
			[StreamCapUtil_Pcnt]	REAL			NOT	NULL,
			[StreamCapacity_kMT]	AS CONVERT(REAL, [Stream_kMT] / [StreamCapUtil_Pcnt] * 100.0)
									PERSISTED		NOT	NULL,

			[StreamChemicals_Cur]	REAL			NOT	NULL,
			[StreamCatalysts_Cur]	REAL			NOT	NULL,
			[StreamNeUtilCost_Cur]	REAL			NOT	NULL,
			[StreamOtherVol_Cur]	REAL			NOT	NULL,
	
			[Stream_Cur]			AS CONVERT(REAL, [StreamChemicals_Cur] + [StreamCatalysts_Cur] + [StreamNeUtilCost_Cur] + [StreamOtherVol_Cur])
									PERSISTED		NOT	NULL,
			PRIMARY KEY CLUSTERED([GroupId]	ASC, [Currency] ASC, [StreamId] ASC)
		);

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'FreshPyroFeed',
			h.[GroupProdCap_kMT],

			h.[GroupPyroCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! M5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! O5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! P5]
		FROM @CapUtil								h
		INNER JOIN [$(DbOlefins)].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @GroupId;

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'FreshPyroFeed',
			h.[GroupProdCap_kMT],

			h.[GroupPyroCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! M5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! N5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! O5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupPyroCap_kMT] / h.[GroupProdCap_kMT]	[GAM! P5]
		FROM @CapUtil								h
		INNER JOIN [$(DatabaseName)].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @TargetId;

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'SuppTot',
			h.[GroupProdCap_kMT],
			h.[GroupSuppCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! W5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Y5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Z5]
		FROM @CapUtil								h
		INNER JOIN [$(DbOlefins)].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @GroupId;

		INSERT INTO @OVC([GroupId], [Currency], [StreamId], [Prod_kMT], [Stream_kMT], [StreamCapUtil_Pcnt], [StreamChemicals_Cur], [StreamCatalysts_Cur], [StreamNeUtilCost_Cur], [StreamOtherVol_Cur])
		SELECT
			h.[GroupID],
			o.[Currency],
			'SuppTot',
			h.[GroupProdCap_kMT],
			h.[GroupSuppCap_kMT]										[GAM! L5],	--	NewOPEX!C61, GMSummary!D270
			h.[GroupCapability_Pcnt]									[GAM! W5],	--	NewOPEX!C85
			COALESCE(o.[Chemicals],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[Catalysts],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! X5],
			COALESCE(o.[PurOth],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Y5],
			COALESCE(o.[OtherVol],	0.0)	/ 1000.0 * h.[GroupSuppCap_kMT] / h.[GroupProdCap_kMT]	[GAM! Z5]
		FROM @CapUtil								h
		INNER JOIN [$(DatabaseName)].[reports].[OPEX]		o
			ON	o.[GroupID]		= h.[GroupID]
			AND	o.[DataType]	= 'ADJ'
			AND	o.[Currency]	= 'USD'
		WHERE h.[GroupID]		= @TargetId;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F52:G54	->	5100			-> Other Variable Costs (Insert Complete)'
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			u.[GroupId],
			u.[TargetId],
			u.[GroupProdCap_kMT],
			u.[BenchProdCap_kMT],
			u.[AccountId],
			u.[Currency],
			u.[StreamId],
			u.[Gap]
		FROM (
			SELECT
				g.[GroupId],
				[TargetId] = b.[GroupId],
				[GroupProdCap_kMT] = g.[Prod_kMT],
				[BenchProdCap_kMT] = b.[Prod_kMT],
				g.[Currency],
				g.[StreamId],
				g.[Prod_kMT],
				
				[Chemicals]		=
					(
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamChemicals_Cur]	/ b.[Stream_kMT]
					END
					-
					CASE
					WHEN g.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE g.[StreamChemicals_Cur]	/ g.[Stream_kMT]
					END
					)
					* g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0,
				
				[Catalysts]		=
					(
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamCatalysts_Cur]	/ b.[Stream_kMT]
					END
					-
					CASE
					WHEN g.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE g.[StreamCatalysts_Cur]	/ g.[Stream_kMT]
					END
					) * g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0,
				
				[NeUtilCost]	=
					(
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamNeUtilCost_Cur]	/ b.[Stream_kMT]
					END
					-
					CASE
					WHEN g.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE g.[StreamNeUtilCost_Cur]	/ g.[Stream_kMT]
					END
					)
					* g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0,
				[OtherVol]		=
					(
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamOtherVol_Cur]	/ b.[Stream_kMT]
					END
					-
					CASE
					WHEN g.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE g.[StreamOtherVol_Cur]	/ g.[Stream_kMT]
					END
					)
					* g.[StreamCapacity_kMT] * g.[StreamCapUtil_Pcnt] / 100.0
			FROM		@OVC		g
			INNER JOIN	@OVC		b
				ON	b.[StreamId]	= g.[StreamId]
				AND	b.[Currency]	= g.[Currency]
			WHERE	g.[GroupId]		= @GroupId
				AND	b.[GroupId]		= @TargetId
			) p
			UNPIVOT ([Gap] FOR [AccountId] IN (
				[Chemicals],
				[Catalysts],
				[NeUtilCost],
				[OtherVol]
				)
			) u;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	 935 (GAM!B35)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	1047 (GAM!D47)	-> Other Variable Costs (Insert Complete)'					--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			u.[GroupId],
			u.[TargetId],
			u.[GroupProdCap_kMT],
			u.[BenchProdCap_kMT],
			CASE u.[StreamId] 
				WHEN 'FreshPyroFeed'	THEN 'Pyro_'
				WHEN 'SuppTot'			THEN 'Supp_'
				END + u.[AccountId],
			u.[Currency],
			u.[StreamId],
			u.[Gap]
		FROM (
			SELECT
				g.[GroupId],
				[TargetId] = b.[GroupId],
				[GroupProdCap_kMT] = g.[Prod_kMT],
				[BenchProdCap_kMT] = b.[Prod_kMT],
				g.[Currency],
				g.[StreamId],
				g.[Prod_kMT],

				[Chemicals]		=
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamChemicals_Cur]	/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
					END,

				[Catalysts]		=
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamCatalysts_Cur]	/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
					END,

				[NeUtilCost]	=
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamNeUtilCost_Cur]	/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
					END,

				[OtherVol]		=
					CASE
					WHEN b.[Stream_kMT] = 0.0
					THEN 0.0
					ELSE b.[StreamOtherVol_Cur]		/ b.[Stream_kMT] * (b.[StreamCapacity_kMT] - g.[StreamCapacity_kMT]) * b.[StreamCapUtil_Pcnt] / 100.0
					END

			FROM		@OVC		g
			INNER JOIN	@OVC		b
				ON	b.[StreamId]	= g.[StreamId]
				AND	b.[Currency]	= g.[Currency]
			WHERE	g.[GroupId]		= @GroupId
				AND	b.[GroupId]		= @TargetId
			) p
			UNPIVOT ([Gap] FOR [AccountId] IN (
				[Chemicals],
				[Catalysts],
				[NeUtilCost],
				[OtherVol]
				)
			) u;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F19:G22	->	1800			-> Capability Utilization (Insert Complete)';
		PRINT @ProcedureDesc;

		BEGIN

		--	GapAnalysisMatrix!B32	-> @PyroCapUtil
		DECLARE @VarOpex			TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[PyroCapUtil_Energy]	REAL			NOT	NULL,
			[PyroCapUtil_Other]		REAL			NOT	NULL,
			[PyroCapUtil]			AS CONVERT(REAL, [PyroCapUtil_Energy] + [PyroCapUtil_Other])
									PERSISTED		NOT	NULL,
			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [StreamId] ASC, [Currency] ASC)
		);

		INSERT INTO @VarOpex([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamId], [Currency], [PyroCapUtil_Energy], [PyroCapUtil_Other])
		SELECT
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			eo.[StreamId],
			ea.[Currency],
			[GAM!B32] =
				CASE eo.[StreamId]
				WHEN 'FreshPyroFeed'	THEN   SUM(ea.[StreamAmount_Cur] * cu.[EnergyCap_Ratio])	/ 1000.0
				WHEN 'SuppTot'			THEN - SUM(ea.[StreamAmount_Cur] * cu.[SuppCapUtil_Ratio])	/ 1000.0
				END,
			[GAM!B33] =
				CASE eo.[StreamId]
				WHEN 'FreshPyroFeed'	THEN - eo.[Stream_Cur] * cu.[PyroCapUtil_Ratio]
				WHEN 'SuppTot'			THEN - eo.[Stream_Cur] * cu.[SuppCapUtil_Ratio]
				END
		FROM @CapUtil			cu
		INNER JOIN @OVC			eo
			ON	eo.[GroupId]	= cu.[TargetId]
		INNER JOIN @EnergyAdj	ea
			ON	ea.[GroupId]	= cu.[TargetId]
			AND	ea.[Currency]	= eo.[Currency]
		WHERE	ea.[DataType]	= CASE eo.[StreamId]
								WHEN 'FreshPyroFeed'	THEN 'Pyro'
								WHEN 'SuppTot'			THEN 'Supp'
								END
		GROUP BY
			cu.[GroupId],
			cu.[TargetId],
			eo.[StreamId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			cu.[PyroCapUtil_Ratio],
			cu.[SuppCapUtil_Ratio],
			eo.[Stream_Cur],
			ea.[Currency];

		--	GapAnalysisMatrix!B89:E92	-> @ReliabilityOppLoss (Only Percent Elements)
		DECLARE @ReliabilityOppLoss		TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[GroupEthyleneCpby_kMT]	REAL			NOT	NULL,
			[GroupPyroCpby_kMT]		REAL			NOT	NULL,

			[DtLoss_Pcnt]			REAL				NULL,
			[SdLoss_Pcnt]			REAL				NULL,
			[TotLoss_Pcnt]			REAL			NOT	NULL,
			[GapUtilization_Pcnt]	REAL			NOT	NULL,

			[DtLoss_Pcnt_Pcnt]		AS CONVERT(REAL, [DTLoss_Pcnt]	/ [GapUtilization_Pcnt])
									PERSISTED,
			[SdLoss_Pcnt_Pcnt]		AS CONVERT(REAL, [SdLoss_Pcnt]	/ [GapUtilization_Pcnt])
									PERSISTED,
			[TotLoss_Pcnt_Pcnt]		AS CONVERT(REAL, [TotLoss_Pcnt]	/ [GapUtilization_Pcnt])
									PERSISTED		NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC, [AccountId] ASC)
		);

		INSERT INTO @ReliabilityOppLoss([GroupId], [TargetId], [AccountId], [GroupEthyleneCpby_kMT], [GroupPyroCpby_kMT], [DTLoss_Pcnt], [SDLoss_Pcnt], [TotLoss_Pcnt], [GapUtilization_Pcnt])
		SELECT
			p.[GroupId],
			p.[TargetId],
			p.[AccountId],
			p.[GroupEthyleneCpby_kMT],
			p.[GroupPyroCpby_kMT],
			p.[DTLoss_Pcnt],
			p.[SDLoss_Pcnt],
			p.[TotLoss_Pcnt],
			p.[GapUtilization_Pcnt]
		FROM (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				cu.[GroupEthyleneCpby_kMT],
				cu.[GroupPyroCpby_kMT],
				[DataType]		= REPLACE(gRol.[DataType], '_MT', '_Pcnt'),

				[TurnAround]	= bRol.[TurnAround]	/ cu.[BenchEthyleneCpby_kMT] / 10.0 - COALESCE(gRol.[TurnAround], 0.0)	/ cu.[GroupEthyleneCpby_kMT] / 10.0,
				[UnPlanned]		= bRol.[UnPlanned]	/ cu.[BenchEthyleneCpby_kMT] / 10.0 - COALESCE(gRol.[UnPlanned], 0.0)	/ cu.[GroupEthyleneCpby_kMT] / 10.0,

				[GapUtilization_Pcnt] = cu.[GroupCapability_Pcnt] - cu.[BenchCapability_Pcnt]
			FROM @CapUtil												cu
			INNER JOIN [$(DbOlefins)].[reports].[GapReliabilityOppLoss]		gRol
				ON	gRol.[GroupID]	= cu.[GroupId]
				AND	gRol.[DataType]	IN ('DTLoss_MT', 'SDLoss_MT', 'TotLoss_MT')
			INNER JOIN [$(DatabaseName)].[reports].[GapReliabilityOppLoss]	bRol
				ON	bRol.[GroupID]	= cu.[TargetId]
				AND	bRol.[DataType]	= gRol.[DataType]
			WHERE	cu.[TargetId]	IS NOT NULL
			) t
			UNPIVOT (
				[Utilization_Pcnt] FOR [AccountId] IN
				(
					[TurnAround],
					[UnPlanned]
				)
			) u
			PIVOT (
				MAX([Utilization_Pcnt]) FOR [DataType] IN
				(
					[DTLoss_Pcnt],
					[SDLoss_Pcnt],
					[TotLoss_Pcnt]
				)
			) p;

		INSERT INTO @ReliabilityOppLoss([GroupId], [TargetId], [AccountId], [GroupEthyleneCpby_kMT], [GroupPyroCpby_kMT], [DTLoss_Pcnt], [TotLoss_Pcnt], [GapUtilization_Pcnt])
		SELECT
			rol.[GroupId],
			rol.[TargetId],
			'NOC',
			rol.[GroupEthyleneCpby_kMT],
			rol.[GroupPyroCpby_kMT],
			rol.[GapUtilization_Pcnt] - SUM(rol.[TotLoss_Pcnt]),
			rol.[GapUtilization_Pcnt] - SUM(rol.[TotLoss_Pcnt]),
			rol.[GapUtilization_Pcnt]
		FROM @ReliabilityOppLoss	rol
		GROUP BY
			rol.[GroupId],
			rol.[TargetId],
			rol.[GroupEthyleneCpby_kMT],
			rol.[GroupPyroCpby_kMT],
			rol.[GapUtilization_Pcnt];

		--	GapAnalysisMatrix!E89:E92	-> @ReliabilityOppLoss
		DECLARE @GapVarOpex				TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamID]				VARCHAR(48)		NOT	NULL	CHECK([StreamID] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[Gap_DtLoss]			REAL				NULL,
			[Gap_SdLoss]			REAL				NULL,
			[Gap_TotLoss]			REAL			NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC, [StreamID] ASC, [AccountId] ASC)
		);

		INSERT INTO @GapVarOpex([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamID], [Currency], [AccountId], [Gap_DtLoss], [Gap_SdLoss], [Gap_TotLoss])
		SELECT
			rol.[GroupId],
			rol.[TargetId],
			pcu.[GroupProdCap_kMT],
			pcu.[BenchProdCap_kMT],
			pcu.[StreamID],
			pcu.[Currency],
			rol.[AccountId],
			[Gap_DtLoss]	= pcu.[PyroCapUtil] * rol.[DtLoss_Pcnt_Pcnt],
			[Gap_SdLoss]	= pcu.[PyroCapUtil] * rol.[SdLoss_Pcnt_Pcnt],
			[Gap_TotLoss]	= pcu.[PyroCapUtil] * rol.[TotLoss_Pcnt_Pcnt]
		FROM @ReliabilityOppLoss	rol
		INNER JOIN @VarOpex			pcu
			ON	pcu.[GroupId]	= rol.[GroupId]
			AND	pcu.[TargetId]	= rol.[TargetId];

		--	GMSummary!{E,H,K}{156,161,166}	-> @YieldMargins
		DECLARE @TargetCountry	CHAR(3);
		DECLARE @StandardCalc	CHAR(4) = 'slmn';

		SELECT @TargetCountry = t.[CountryID]
		FROM [$(DbOlefins)].[fact].[TSort] t
		WHERE t.[Refnum] = @GroupId;

		DECLARE @YieldMargins			TABLE
		(
			[DataType]		VARCHAR(4)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]		VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),

			[ProdLoss]		REAL		NOT	NULL,
			[FreshPyroFeed]	REAL		NOT	NULL,
			[FeedSupp]		REAL		NOT	NULL,
			[PlantFeed]		REAL		NOT	NULL,
/**/		[Recycle]		REAL		NOT	NULL,

			[FreshRec]		AS CONVERT(REAL, [FreshPyroFeed] + [Recycle])
							PERSISTED	NOT	NULL,
			[ProdPyro]		AS CONVERT(REAL, [ProdLoss] - [ProdSupp])
							PERSISTED	NOT	NULL,

/**/		[ProdSupp]		REAL		NOT	NULL,

			[_GMPyro]		AS CONVERT(REAL, [ProdLoss] - [ProdSupp] - [FreshPyroFeed] - [Recycle])
							PERSISTED	NOT	NULL,

			[_GMSupp]		AS CONVERT(REAL, [ProdSupp] - [FeedSupp] + [Recycle])
							PERSISTED	NOT	NULL,

			[_GM]			AS CONVERT(REAL, [ProdLoss] - [PlantFeed])
							PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([DataType] ASC, [GroupId] ASC)
		);

		INSERT INTO @YieldMargins([DataType], [GroupId], [FreshPyroFeed], [FeedSupp], [PlantFeed], [ProdLoss], [ProdSupp], [Recycle])
		SELECT
			[DataType]	= @StandardCalc,
			p.[GroupId],
			p.[FreshPyroFeed],
			p.[SuppTot],
			p.[PlantFeed],
			p.[ProdLoss],
			COALESCE(s.[ProdSupp_Cur], 0.0),
			p.[EthRec] + p.[ProRec] + p.[ButRec]
		FROM (
			SELECT
				t.[GroupID],
				t.[StreamId],
				[Value_Cur]	= t.[Quantity_kMT] * t.[Amount_Cur] / 1000.0
			FROM (
				SELECT
					[DataType] =
						CASE y.[DataType]
						WHEN '$PerMT'	THEN 'Amount_Cur'
						WHEN 'kMT'		THEN 'Quantity_kMT'
						END,
					y.[GroupID],
					y.[FreshPyroFeed],
					y.[SuppTot],
					y.[PlantFeed],
					y.[Prod],
					y.[Loss],
					y.[ProdLoss],
					[EthRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppEthane]	ELSE r.[EthRec] END,
					[ProRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppPropane]	ELSE r.[ProRec] END,
					[ButRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppButane]	ELSE r.[ButRec] END
				FROM [$(DbOlefins)].[reports].[Yield]		y
				INNER JOIN @RecycleValue				r
					ON	r.[GroupId]	= y.[GroupId]
				WHERE	y.[GroupID]	= @GroupId
					AND	y.[DataType] IN ('kMT', '$PerMT')

				UNION ALL

				SELECT
					[DataType] =
						CASE y.[DataType]
						WHEN '$PerMT'	THEN 'Amount_Cur'
						WHEN 'kMT'		THEN 'Quantity_kMT'
						END,
					y.[GroupID],
					y.[FreshPyroFeed],
					y.[SuppTot],
					y.[PlantFeed],
					y.[Prod],
					y.[Loss],
					y.[ProdLoss],
					[EthRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppEthane]	ELSE r.[EthRec] END,
					[ProRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppPropane]	ELSE r.[ProRec] END,
					[ButRec] = CASE WHEN y.[DataType] = 'kMT' THEN y.[RecSuppButane]	ELSE r.[ButRec] END
				FROM [$(DatabaseName)].[reports].[Yield]		y
				INNER JOIN @RecycleValue				r
					ON	r.[GroupId]	= y.[GroupId]
				WHERE	y.[GroupID]	= @TargetId
					AND	y.[DataType] IN ('kMT', '$PerMT')
				) p
				UNPIVOT (
					[Value] FOR [StreamId] IN
					(
						[FreshPyroFeed],
						[SuppTot],
						[PlantFeed],
						[Prod],
						[Loss],
						[ProdLoss],
						[EthRec],
						[ProRec],
						[ButRec]
					)
				) a
				PIVOT (
					MAX(a.[Value]) FOR a.[DataType] IN
					(
						[Amount_Cur],
						[Quantity_kMT]
					)
				) t
			) v
			PIVOT (
				MAX(v.[Value_Cur]) FOR v.[StreamId] IN
				(
					[FreshPyroFeed],
					[SuppTot],
					[PlantFeed],
					[Prod],
					[Loss],
					[ProdLoss],
					[EthRec],
					[ProRec],
					[ButRec]
				)
			) p
		LEFT OUTER JOIN @ProdSuppAggregate	s
			ON	s.[GroupId] = p.[GroupID];

		DECLARE @Recycle	REAL;

		SELECT
			@Recycle = COALESCE(SUM(rYield.[Quantity_kMT] * x.[Sel_Calc_Amount_Cur]) / 1000.0, 0.0)
		FROM (
			SELECT
				[GroupId] = pps.[REfnum],
				pps.[StreamID],
				pps.[Avg_Calc_Amount_Cur]
			FROM [$(DatabaseName)].[calc].[PricingPlantStreamAggregate]			pps
			WHERE pps.[Refnum]			= @TargetId + ':' + @TargetCountry
				AND pps.[StreamID]		IN ('Ethane', 'DilEthane', 'ROGEthane', 'Propane', 'DilPropane', 'ROGPropane', 'Butane', 'DilButane', 'ROGC4Plus')
				AND	pps.[Quantity_kMT]	> 0.0
			) ppsa
			PIVOT (
				MAX(ppsa.[Avg_Calc_Amount_Cur]) FOR ppsa.[StreamID] IN (
					[Ethane], [DilEthane], [ROGEthane], [Propane], [DilPropane], [ROGPropane], [Butane], [DilButane], [ROGC4Plus]
				)
			) p
		CROSS APPLY( VALUES
			('RecSuppEthane',	COALESCE(p.[Ethane],	p.[DilEthane],	p.[ROGEthane])),
			('RecSuppPropane',	COALESCE(p.[Propane],	p.[DilPropane],	p.[ROGPropane])),
			('RecSuppButane',	COALESCE(p.[Butane],	p.[DilButane],	p.[ROGC4Plus]))
			) x([Rec_StreamID], [Sel_Calc_Amount_Cur])
		INNER JOIN (
			SELECT
				ty.[GroupID],
				ty.[RecSuppEthane],
				ty.[RecSuppPropane],
				ty.[RecSuppButane]
			FROM [$(DatabaseName)].[reports].[Yield] tY
			WHERE	tY.[GroupID]	= @TargetId
				AND tY.[DataType]	= 'kMT'
			) u
			UNPIVOT (
				[Quantity_kMT] FOR [Rec_StreamID] IN (
					[RecSuppEthane],
					[RecSuppPropane],
					[RecSuppButane]
				)
			) rYield
			ON	rYield.[Rec_StreamID]	= x.[Rec_StreamID]
			AND	rYield.[Quantity_kMT]	> 0.0;

		INSERT INTO @YieldMargins([DataType], [GroupId], [FreshPyroFeed], [FeedSupp], [PlantFeed], [ProdLoss], [ProdSupp], [Recycle])
		SELECT
			[DataType]	= @TargetCountry,
			[GroupId]	= @TargetId,
			p.[FreshPyroFeed],
			[SuppTot]	= COALESCE(p.[SuppTot], 0.0),
			p.[PlantFeed],
			p.[ProdLoss],
			[ProdSupp]	= COALESCE(s.[ProdSupp], 0.0),

			[Recycle]	= @Recycle
		FROM (
			SELECT
				[GroupId] = pps.[Refnum],
				pps.[StreamID],
				[Stream_Value_Cur] = pps.[Stream_Value_Cur] / 1000.0
			FROM [$(DatabaseName)].[calc].[PricingPlantStreamAggregate]	pps
			WHERE pps.[Refnum] = @TargetId + ':' + @TargetCountry
			AND	pps.[StreamID] IN ('FreshPyroFeed', 'SuppTot', 'PlantFeed', 'ProdLoss')
			) u
			PIVOT (MAX(u.[Stream_Value_Cur]) FOR u.[StreamID] IN(
				[FreshPyroFeed],
				[SuppTot],
				[PlantFeed],
				[ProdLoss]
				)
			) p
		LEFT OUTER JOIN (
			SELECT
				[GroupId]	= pps.[Refnum],
				[ProdSupp]	= SUM(d.[Quantity_kMT] * pps.[Avg_Calc_Amount_Cur]) / 1000.0
			FROM [$(DatabaseName)].[calc].[PricingPlantStreamAggregate]			pps
			INNER JOIN @DispositionSupp					d
				ON	d.[GroupId] + ':' + @TargetCountry	= pps.[Refnum]
				AND	d.[StreamId]		= pps.[StreamID]
				AND	d.[Quantity_kMT]	<> 0.0
			WHERE	pps.[Refnum] = @TargetId + ':' + @TargetCountry
			GROUP BY
				pps.[Refnum]
			) s
			ON	s.[GroupId]	= p.[GroupId];

		--	GapAnalysisMatrix!D83,D101	-> @GM
		DECLARE @GM	TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[PyroCapUtil]			REAL			NOT	NULL,
			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [StreamId] ASC, [Currency] ASC)
		);

		INSERT INTO @GM([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamId], [Currency], [PyroCapUtil])
		SELECT
			u.[GroupId],
			u.[TargetId],
			u.[GroupProdCap_kMT],
			u.[BenchProdCap_kMT],
			u.[StreamId],
			'USD',
			u.[PyroCapUtil]
		FROM (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				cu.[GroupProdCap_kMT],
				cu.[BenchProdCap_kMT],
				[FreshPyroFeed]	= ym._GMPyro * cu.PyroCapUtil_Ratio, 
				[SuppTot]		= ym._GMSupp * cu.SuppCapUtil_Ratio
			FROM @CapUtil				cu
			INNER JOIN @YieldMargins	ym
				ON	ym.[GroupId]		= cu.[TargetId]
				AND	ym.[DataType]		= @TargetCountry
			) t
		UNPIVOT (
			[PyroCapUtil] FOR [StreamId] IN (
				[FreshPyroFeed],
				[SuppTot]
			)
		) u;

		DECLARE @GapGM			TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL			NOT	NULL,
			[BenchProdCap_kMT]		REAL			NOT	NULL,
			[StreamID]				VARCHAR(48)		NOT	NULL	CHECK([StreamID] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[Gap_DtLoss]			REAL				NULL,
			[Gap_SdLoss]			REAL				NULL,
			[Gap_TotLoss]			REAL			NOT	NULL,
			PRIMARY KEY CLUSTERED ([GroupId] ASC, [TargetId] ASC, [StreamID] ASC, [AccountId] ASC)
		);

		--	GapAnalysisMatrix!E89:E100	-> @ReliabilityOppLoss
		INSERT INTO @GapGM([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamID], [Currency], [AccountId], [Gap_DtLoss], [Gap_SdLoss], [Gap_TotLoss])
		SELECT
			rol.[GroupId],
			rol.[TargetId],
			pcu.[GroupProdCap_kMT],
			pcu.[BenchProdCap_kMT],
			pcu.[StreamID],
			'USD',
			rol.[AccountId],
			[Gap_DtLoss]	= pcu.[PyroCapUtil] * rol.[DtLoss_Pcnt_Pcnt],
			[Gap_SdLoss]	= pcu.[PyroCapUtil] * rol.[SdLoss_Pcnt_Pcnt],
			[Gap_TotLoss]	= pcu.[PyroCapUtil] * rol.[TotLoss_Pcnt_Pcnt]
		FROM @ReliabilityOppLoss	rol
		INNER JOIN @GM				pcu
			ON	pcu.[GroupId]	= rol.[GroupId]
			AND	pcu.[TargetId]	= rol.[TargetId];

		--	GapAnalysisMatrix!F89:F92	->
		DECLARE @CapacilityUtilization	TABLE
		(
			[GroupId]				VARCHAR(25)		NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)		NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL				NULL,
			[BenchProdCap_kMT]		REAL				NULL,

			[StreamId]				VARCHAR(18)		NOT	NULL	CHECK([StreamId] <> ''),
			[AccountId]				VARCHAR(24)		NOT	NULL	CHECK([AccountId] <> ''),
			[Currency]				VARCHAR(4)		NOT	NULL	CHECK([Currency] <> ''),

			[GapTot_DtLoss]			REAL				NULL,
			[GapTot_SdLoss]			REAL				NULL,
			[GapTot_TotLoss]		REAL			NOT	NULL,

			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [StreamId] ASC, [AccountId] ASC, [Currency] ASC)
		);

		INSERT INTO @CapacilityUtilization([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [StreamId], [AccountId], [Currency], [GapTot_DtLoss], [GapTot_SdLoss], [GapTot_TotLoss])
		SELECT
			g.[GroupId],
			g.[TargetId],
			v.[GroupProdCap_kMT],
			v.[BenchProdCap_kMT],
			g.[StreamId],
			g.[AccountId],
			v.[Currency],
			[GapTot_DtLoss]		= COALESCE(g.[Gap_DtLoss], 0.0)		+ COALESCE(v.[Gap_DtLoss], 0.0),
			[GapTot_SdLoss]		= COALESCE(g.[Gap_SdLoss], 0.0)		+ COALESCE(v.[Gap_SdLoss], 0.0),
			[GapTot_TotLoss]	= COALESCE(g.[Gap_TotLoss], 0.0)	+ COALESCE(v.[Gap_TotLoss], 0.0)
		FROM @GapGM				g
		INNER JOIN @GapVarOpex	v
			ON	v.[GroupId]		= g.[GroupId]
			AND	v.[TargetId]	= g.[TargetId]
			AND	v.[StreamId]	= g.[StreamId]
			AND	v.[AccountId]	= g.[AccountId];

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			t.[GroupId],
			t.[TargetId],
			t.[GroupProdCap_kMT],
			t.[BenchProdCap_kMT],
			t.[AccountId] + '_' + t.[DataType],
			t.[Currency],
			t.[DataType],
			t.[GapAmount_Cur]
		FROM (
			SELECT
				cut.[GroupId],
				cut.[TargetId],
				cut.[GroupProdCap_kMT],
				cut.[BenchProdCap_kMT],
				cut.[AccountId],
				cut.[Currency],
				[DT] = SUM(cut.[GapTot_DtLoss]),
				[SD] = SUM(cut.[GapTot_SdLoss])
			FROM @CapacilityUtilization	cut
			GROUP BY
				cut.[GroupId],
				cut.[TargetId],
				cut.[GroupProdCap_kMT],
				cut.[BenchProdCap_kMT],
				cut.[AccountId],
				cut.[Currency]
			) u
			UNPIVOT (
				[GapAmount_Cur] FOR [DataType] IN (
					[DT],
					[SD]
				)
			) t;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F7:G7	->	 710 (GAM!D6)	-> Gross Margin (Insert Complete)';							--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			'GrossMargin',
			'USD',
			'GrossMargin',
			ym.[_GM]
		FROM @CapUtil					cu
		INNER JOIN @YieldMargins		ym
			ON	ym.[GroupId]	= cu.[TargetId]
			AND	ym.[DataType]	= @StandardCalc
		WHERE cu.[TargetId] IS NOT NULL;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F7:G7	->	 720 (GAM!D22)	-> Volume Related (Insert Complete)';						--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			'STVol',
			ox.[Currency],
			ox.[DataType],
			- ox.[STVol] / 1000.0
		FROM @CapUtil								cu
		INNER JOIN [$(DatabaseName)].[reports].[OPEX]		ox
			ON	ox.[GroupId]	= cu.[TargetId]
			AND	ox.[DataType]	= 'ADJ'
		WHERE cu.[TargetId] IS NOT NULL;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F7:G7	->	 730 (GAM!D52)	-> Cash OpEx (Insert Complete)';							--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			'CashOpEx',
			ox.[Currency],
			ox.[DataType],
			- (ox.[STNonVol] + ox.[TAAccrual]) / 1000.0
		FROM @CapUtil								cu
		INNER JOIN [$(DatabaseName)].[reports].[OPEX]		ox
			ON	ox.[GroupId]	= cu.[TargetId]
			AND	ox.[DataType]	= 'ADJ'
		WHERE cu.[TargetId] IS NOT NULL;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F11:G11	->	1100			-> Feedstock and Product Prices (Insert Complete)';			--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			'FeedProdPrices',
			'USD',
			'Total',
			cYm.[_GM] - bYm.[_GM]
		FROM @CapUtil						cu
		INNER JOIN @YieldMargins			bYm
			ON	bYm.[GroupId]	= cu.[TargetId]
			AND	bYm.[DataType]	= @StandardCalc
		INNER JOIN @YieldMargins			cYm
			ON	cYm.[GroupId]	= cu.[TargetId]
			AND	cYm.[DataType]	= @TargetCountry
		WHERE	cu.[TargetId]	IS NOT NULL;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F9:G9	->	 913 (GAM!B13)	-> Pyrolysis Capacity (Insert Complete)';					--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			'Pyro_Capacity',
			'USD',
			'Total',
			[Gam!B13] = bYm.[_GMPyro] / cu.[BenchPyroCap_kMT] * cu.[BenchCapability_Pcnt] / 100.0 * (cu.[GroupPyroCpby_kMT] - cu.[BenchPyroCpby_kMT])
		FROM @CapUtil						cu
		INNER JOIN @YieldMargins			bYm
			ON	bYm.[GroupId]	= cu.[TargetId]
			AND	bYm.[DataType]	= @TargetCountry
		WHERE	cu.[TargetId]	IS NOT NULL;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F10:G10	->	1017 (GAM!D17)	-> Supplemental	Capacity (Insert Complete)';				--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			'Supp_Capacity',
			'USD',
			'Total',
			[Gam!D17] =
				CASE
				WHEN cu.[BenchSuppCap_kMT] = 0.0
				THEN 0.0
				ELSE bYm.[_GMSupp] / cu.[BenchSuppCap_kMT] * cu.[BenchCapability_Pcnt] / 100.0 * (cu.[GroupSuppCpby_kMT] - cu.[BenchSuppCpby_kMT])
				END
		FROM @CapUtil						cu
		INNER JOIN @YieldMargins			bYm
			ON	bYm.[GroupId]	= cu.[TargetId]
			AND	bYm.[DataType]	= @TargetCountry
		WHERE	cu.[TargetId]	IS NOT NULL;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F14:G14	->	1400 (GAM!D15)	-> Supplemental Feedstock and Quality (Insert Complete)';	--	(Top)
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			'SuppFeedQuality',
			'USD',
			'Total',
			[Gap (F14)] = CASE WHEN gPsa.[ProdHvc_kMT] = 0.0 AND bPsa.[ProdHvc_kMT] = 0.0
				THEN gYm.[_GMSupp]
				ELSE (
						CASE WHEN gPsa.[ProdHvc_kMT] <> 0.0 THEN gYm.[_GMSupp] / gPsa.[ProdHvc_kMT] ELSE 0.0 END
					 -	CASE WHEN bPsa.[ProdHvc_kMT] <> 0.0 THEN tYm.[_GMSupp] / bPsa.[ProdHvc_kMT] ELSE 0.0 END
					 ) * gPsa.[ProdHvc_kMT]
				END
		FROM @CapUtil							cu
		INNER JOIN @ProdSuppAggregate			gPsa
			ON	gPsa.[GroupId]	= cu.[GroupId]
		INNER JOIN @YieldMargins				gYm
			ON	gYm.[GroupId]	= cu.[GroupId]
			AND gYm.[DataType]	= @StandardCalc
		LEFT OUTER JOIN @ProdSuppAggregate		bPsa
			ON	bPsa.[GroupId]	= cu.[TargetId]
		INNER JOIN @YieldMargins				tYm
			ON	tYm.[GroupId]	= cu.[TargetId]
			AND tYm.[DataType]	= @TargetCountry
		WHERE cu.[TargetId]	IS NOT NULL;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F13:G13	->	1300 (GAM!D84)	-> Pyrolysis Feedstock Mix and Quality (Insert Complete)';	--	(Top)
		PRINT @ProcedureDesc;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F25:G26	->	2400			-> Product Yields (Complete)';
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			p.[GroupId],
			p.[TargetId],
			p.[GroupProdCap_kMT],
			p.[BenchProdCap_kMT],
			p.[AccountId],
			'USD',
			'Total',
			p.[Amount_Cur]
		FROM (
			SELECT
				m.[GroupID],
				m.[TargetId],
				m.[GroupProdCap_kMT],
				m.[BenchProdCap_kMT],
				m.[Loss],
				m.[PyroYieldPerf],
				[PyroFeedMixQuality] = m.[PyroValAdded] - m.[Loss] - m.[PyroYieldPerf]
			FROM (
				SELECT
					cu.[GroupID],
					cu.[TargetId],
					cu.[GroupProdCap_kMT],
					cu.[BenchProdCap_kMT],
					[Loss]				= ((bYld.[Loss] / bYld.[ProdLoss]) - (gYld.[Loss] / gYld.[ProdLoss])) * (gYld.[ProdLoss] * gPrc.[ProdLoss] / 1000.0) / (1.0 - (bYld.[Loss] / bYld.[ProdLoss])),
			
					--	'gPsa.[ProductionPyro_kMT] / gPsa.[ProductionPyro_kMT]' is 1.0 - unsure why this is in the code
					--	Examine gap procedure for this...
					--[PyroYieldPerf]	= gYm.[ProdPyro] * (1.0 - gPsa.[ProductionPyro_kMT] / gPsa.[ProductionPyro_kMT] * bPyr.[ActPPV_PcntOS25] / gPyr.[ActPPV_PcntOS25])
					[PyroYieldPerf]	= gYm.[ProdPyro] * (1.0 -  bPyr.[ActPPV_PcntOS25] / gPyr.[ActPPV_PcntOS25])
										- ((bYld.[Loss] / bYld.[ProdLoss]) - (gYld.[Loss] / gYld.[ProdLoss])) * (gYld.[ProdLoss] * gPrc.[ProdLoss] / 1000.0) / (1.0 - (bYld.[Loss] / bYld.[ProdLoss])),
	
					[PyroValAdded]		= (gYm.[_GMPyro] / cu.[GroupPyroCap_kMT] - tYm.[_GMPyro] / cu.[BenchPyroCap_kMT]) * cu.[GroupPyroCpby_kMT] * cu.[GroupCapability_Pcnt] / 100.0

				FROM @CapUtil									cu
				INNER JOIN [$(DbOlefins)].[reports].[Yield]			gYld
					ON	gYld.[GroupID]		= cu.[GroupId]
					AND	gYld.[DataType]		= 'kMT'
				INNER JOIN [$(DbOlefins)].[reports].[Yield]			gPrc
					ON	gPrc.[GroupID]		= cu.[GroupId]
					AND	gPrc.[DataType]		= '$PerMT'
				INNER JOIN [$(DbOlefins)].[reports].[Pyrolysis]		gPyr
					ON	gPyr.[GroupID]		= cu.[GroupId]
					AND gPyr.[SimModelID]	= 'PYPS'
					AND	gPyr.[DataType]		= 'WtPcnt'
				INNER JOIN @ProdSuppAggregate					gPsa
					ON	gPsa.[GroupID]		= cu.[GroupId]
				INNER JOIN @YieldMargins						gYm
					ON	gYm.[GroupId]		= cu.[GroupId]
					AND gYm.[DataType]		= @StandardCalc

				INNER JOIN [$(DatabaseName)].[reports].[Yield]		bYld
					ON	bYld.[GroupID]		= cu.[TargetId]
					AND	bYld.[DataType]		= gYld.[DataType]
				INNER JOIN [$(DatabaseName)].[reports].[Pyrolysis]	bPyr
					ON	bPyr.[GroupID]		= cu.[TargetId]
					AND bPyr.[SimModelID]	= 'PYPS'
					AND	bPyr.[DataType]		= 'WtPcnt'
				INNER JOIN @YieldMargins						tYm
					ON	tYm.[GroupId]		= cu.[TargetId]
					AND tYm.[DataType]		= @TargetCountry
				) m
			) t
			UNPIVOT (
				[Amount_Cur] FOR [AccountId] IN (
					[Loss],
					[PyroYieldPerf],
					[PyroFeedMixQuality]
				)
			) p;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F29:G32	->	2800			-> Personnel Level and Compensation (Insert Complete)';
		PRINT @ProcedureDesc;

		BEGIN
		--	Contractor

		DECLARE @PersContract	TABLE
		(
			[DataType]				VARCHAR(12)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[IsGroup]				INT			NOT	NULL	CHECK([IsGroup] IN (1, 0)),
			[Currency]				VARCHAR(3)	NOT	NULL	CHECK([Currency] <> ''),
			[PersId]				VARCHAR(42)	NOT	NULL	CHECK([PersId] <> ''),

			[Occ_Hrs]				REAL		NOT	NULL,
			[Mps_Hrs]				REAL		NOT	NULL,
			[Tot_Hrs]				AS CONVERT(REAL, COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0))
									PERSISTED	NOT	NULL,

			[OccHrs_Pcnt]			AS CONVERT(REAL,
										CASE
										WHEN (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)) = 0.0
										THEN 0.0
										ELSE COALESCE([Occ_Hrs], 0.0) / (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)) * 100.0
										END)
									PERSISTED	NOT	NULL,

			[MpsHrs_Pcnt]			AS CONVERT(REAL,
										CASE
										WHEN (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)) = 0.0
										THEN 0.0
										ELSE COALESCE([Mps_Hrs], 0.0) / (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)) * 100.0
										END)
									PERSISTED	NOT	NULL,

			[OccSal_Cur]			AS CONVERT(REAL,
										CASE
										WHEN (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)) = 0.0
										THEN 0.0
										ELSE [TotSal_Cur] * COALESCE([Occ_Hrs], 0.0) / (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0))
										END)
									PERSISTED	NOT	NULL,

			[MpsSal_Cur]			AS CONVERT(REAL,
										CASE
										WHEN (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)) = 0.0
										THEN 0.0
										ELSE [TotSal_Cur] * COALESCE([Mps_Hrs], 0.0) / (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0))
										END)
									PERSISTED	NOT	NULL,

			[TotSal_Cur]			REAL		NOT	NULL,

			[Occ_Rate]				AS CONVERT(REAL,
										CASE
										WHEN [Occ_Hrs] = 0.0
										THEN 0.0
										ELSE
											([TotSal_Cur] * COALESCE([Occ_Hrs], 0.0) / (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)))
											/ [Occ_Hrs] * 1000.0
										END)
									PERSISTED	NOT	NULL,

			[Mps_Rate]				AS CONVERT(REAL,
										CASE
										WHEN [Mps_Hrs] = 0.0
										THEN 0.0
										ELSE
											([TotSal_Cur] * COALESCE([Mps_Hrs], 0.0) / (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)))
											/ [Mps_Hrs] * 1000.0
										END)
									PERSISTED	NOT	NULL,

			[Tot_Rate]				AS CONVERT(REAL,
										CASE
										WHEN COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0) = 0.0
										THEN 0.0
										ELSE
											[TotSal_Cur]
											/ (COALESCE([Occ_Hrs], 0.0) + COALESCE([Mps_Hrs], 0.0)) * 1000.0
										END)
									PERSISTED	NOT	NULL
						
			PRIMARY KEY CLUSTERED ([DataType] ASC, [GroupId] ASC, [PersId] ASC)
		);

		INSERT INTO @PersContract([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotSal_Cur], [Occ_Hrs], [Mps_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'Labor',
			gOpEx.[MaintContractLabor],
			[OccMaint (Q)]	= COALESCE(gPers.[OccMaint], 0.0) - COALESCE(gPers.[OccMaintTaMaint], 0.0),
			[MpsMaint (R)]	= COALESCE(gPers.[MpsMaint], 0.0) - COALESCE(gPers.[MpsMaintTaMaint], 0.0)
		FROM		[$(DbOlefins)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DbOlefins)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @GroupId
			AND	gPers.[DataType]	= 'WHrsCont';

		INSERT INTO @PersContract([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotSal_Cur], [Occ_Hrs], [Mps_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'Labor',
			gOpEx.[MaintContractLabor],
			[OccMaint (Q)]	= COALESCE(gPers.[OccMaint], 0.0) - COALESCE(gPers.[OccMaintTaMaint], 0.0),
			[MpsMaint (R)]	= COALESCE(gPers.[MpsMaint], 0.0) - COALESCE(gPers.[MpsMaintTaMaint], 0.0)
		FROM		[$(DatabaseName)].[reports].[Personnel]	gPers
		INNER JOIN	[$(DatabaseName)].[reports].[OPEX]		gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @TargetId
			AND	gPers.[DataType]	= 'WHrsCont';

		INSERT INTO @PersContract([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotSal_Cur], [Occ_Hrs], [Mps_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'Tech',
			gOpEx.[ContractTechSvc],
			[OccTech (T)]	= COALESCE(gPers.[OccAdminLab], 0.0),
			[MpsTech (U)]	= COALESCE(gPers.[MpsTech], 0.0) + COALESCE(gPers.[MpsAdminLab], 0.0)
		FROM		[$(DbOlefins)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DbOlefins)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @GroupId
			AND	gPers.[DataType]	= 'WHrsCont';

		INSERT INTO @PersContract([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotSal_Cur], [Occ_Hrs], [Mps_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'Tech',
			gOpEx.[ContractTechSvc],
			[OccTech (T)]	= COALESCE(gPers.[OccAdminLab], 0.0),
			[MpsTech (U)]	= COALESCE(gPers.[MpsTech], 0.0) + COALESCE(gPers.[MpsAdminLab], 0.0)
		FROM		[$(DatabaseName)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @TargetId
			AND	gPers.[DataType]	= 'WHrsCont';

		INSERT INTO @PersContract([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotSal_Cur], [Occ_Hrs], [Mps_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'Other',
			gOpEx.[ContractServOth],
			[OccOther (W)]	= COALESCE(gPers.[OccProc], 0.0) + COALESCE(gPErs.[OccAdmin], 0.0) - COALESCE(gPErs.[OccAdminLab], 0.0),
			[MpsOther (X)]	= COALESCE(gPers.[MpsProc], 0.0) + COALESCE(gPers.[MpsAdmin], 0.0)
		FROM		[$(DbOlefins)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DbOlefins)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @GroupId
			AND	gPers.[DataType]	= 'WHrsCont';

		INSERT INTO @PersContract([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotSal_Cur], [Occ_Hrs], [Mps_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'Other',
			gOpEx.[ContractServOth],
			[OccOther (W)]	= COALESCE(gPers.[OccProc], 0.0) + COALESCE(gPErs.[OccAdmin], 0.0) - COALESCE(gPErs.[OccAdminLab], 0.0),
			[MpsOther (X)]	= COALESCE(gPers.[MpsProc], 0.0) + COALESCE(gPers.[MpsAdmin], 0.0)
		FROM		[$(DatabaseName)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @TargetId
			AND	gPers.[DataType]	= 'WHrsCont';

		--	Company (Reported)

		DECLARE @PersCompany	TABLE
		(
			[DataType]				VARCHAR(12)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[IsGroup]				INT			NOT	NULL	CHECK([IsGroup] IN (1, 0)),
			[Currency]				VARCHAR(3)	NOT	NULL	CHECK([Currency] <> ''),
			[PersId]				VARCHAR(42)	NOT	NULL	CHECK([PersId] <> ''),

			[Maint_Hrs]				AS	CONVERT(REAL, [Total_Hrs] * [Maint_Pcnt] / 100.0)
									PERSISTED	NOT	NULL,
			[Other_Hrs]				AS	CONVERT(REAL, [Total_Hrs] * [Other_Pcnt] / 100.0)
									PERSISTED	NOT	NULL,
			[Total_Hrs]				REAL		NOT	NULL,

			[Maint_Pcnt]			REAL		NOT	NULL	DEFAULT (40.0),
			[Other_Pcnt]			REAL		NOT	NULL	DEFAULT (60.0),

			[MaintSal_Cur]			AS CONVERT(REAL, [TotalSal_Cur] * [Maint_Pcnt] / 100.0) -- + CASE WHEN [PersId] = 'OccSal' THEN 100.0 * [IsGroup] ELSE 0.0 END)
									PERSISTED	NOT	NULL,
			[OtherSal_Cur]			AS CONVERT(REAL, [TotalSal_Cur] * [Other_Pcnt] / 100.0) -- - CASE WHEN [PersId] = 'OccSal' THEN 100.0 * [IsGroup] ELSE 0.0 END)
									PERSISTED	NOT	NULL,
			[TotalSal_Cur]			REAL		NOT	NULL,

			[Maint_Rate]			AS CONVERT(REAL, ([TotalSal_Cur] * [Maint_Pcnt] / 100.0)) -- + CASE WHEN [PersId] = 'OccSal' THEN 100.0 * [IsGroup] ELSE 0.0 END))
										/ ([Total_Hrs] * [Maint_Pcnt] / 100.0) * 1000.0
									PERSISTED	NOT	NULL,
			[Other_Rate]			AS CONVERT(REAL, ([TotalSal_Cur] * [Other_Pcnt] / 100.0)) -- - CASE WHEN [PersId] = 'OccSal' THEN 100.0 * [IsGroup] ELSE 0.0 END))
										/ ([Total_Hrs] * [Other_Pcnt] / 100.0) * 1000.0
									PERSISTED	NOT	NULL,
			[Total_Rate]			AS [TotalSal_Cur] / [Total_Hrs] * 1000.0
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED ([DataType] ASC, [GroupId] ASC, [PersId] ASC)
		);

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'OccSal',
			gOpEx.[OccSal],
			gPers.[OccPlantSite] - COALESCE(gPers.[OccMaintTaMaint], 0.0)
		FROM		[$(DbOlefins)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DbOlefins)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @GroupId
			AND	gPers.[DataType]	= 'WHrsComp';

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'OccSal',
			gOpEx.[OccSal],
			gPers.[OccPlantSite] - COALESCE(gPers.[OccMaintTaMaint], 0.0)
		FROM		[$(DatabaseName)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @TargetId
			AND	gPers.[DataType]	= 'WHrsComp';

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'OccBen',
			gOpEx.[OccBen],
			gPers.[OccPlantSite] - COALESCE(gPers.[OccMaintTaMaint], 0.0)
		FROM		[$(DbOlefins)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DbOlefins)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @GroupId
			AND	gPers.[DataType]	= 'WHrsComp';

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'OccBen',
			gOpEx.[OccBen],
			gPers.[OccPlantSite] - COALESCE(gPers.[OccMaintTaMaint], 0.0)
		FROM		[$(DatabaseName)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @TargetId
			AND	gPers.[DataType]	= 'WHrsComp';

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'MpsSal',
			gOpEx.[MpsSal],
			gPers.[MpsSubTotal]		-  COALESCE(gPers.[MpsMaintTaMaint], 0.0)
		FROM		[$(DbOlefins)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DbOlefins)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @GroupId
			AND	gPers.[DataType]	= 'WHrsComp';

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'MpsSal',
			gOpEx.[MpsSal],
			gPers.[MpsSubTotal]		-  COALESCE(gPers.[MpsMaintTaMaint], 0.0)
		FROM		[$(DatabaseName)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @TargetId
			AND	gPers.[DataType]	= 'WHrsComp';

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'MpsBen',
			gOpEx.[MpsBen],
			gPers.[MpsSubTotal]		- COALESCE(gPers.[MpsMaintTaMaint], 0.0)
		FROM		[$(DbOlefins)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DbOlefins)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @GroupId
			AND	gPers.[DataType]	= 'WHrsComp';

		INSERT INTO @PersCompany([DataType], [GroupId], [IsGroup], [Currency], [PersId], [TotalSal_Cur], [Total_Hrs])
		SELECT
			gPers.[DataType],
			gPers.[GroupId],
			CASE WHEN gPers.[GroupId] = @GroupId THEN 1 ELSE 0 END,
			gOpEx.[Currency],
			'MpsBen',
			gOpEx.[MpsBen],
			gPers.[MpsSubTotal]		- COALESCE(gPers.[MpsMaintTaMaint], 0.0)
		FROM		[$(DatabaseName)].[reports].[Personnel]		gPers
		INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			gOpEx
			ON	gOpEx.[GroupID]		= gPers.[GroupId]
			AND	gOpEx.[Currency]	= 'USD'
			AND	gOpEx.[DataType]	= 'ADJ'
		WHERE	gPers.[GroupID]		= @TargetId
			AND	gPers.[DataType]	= 'WHrsComp';

		--	Contractor Utilization

		DECLARE @ContUtil	TABLE
		(
			[DataType]				VARCHAR(12)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[Currency]				VARCHAR(3)	NOT	NULL	CHECK([Currency] <> ''),
			[PersId]				VARCHAR(42)	NOT	NULL	CHECK([PersId] <> ''),
			[Comp_Hrs]				REAL		NOT	NULL,
			[ContId]				VARCHAR(42)	NOT	NULL	CHECK([ContId] <> ''),
			[Cont_Hrs]				REAL		NOT	NULL,
			[ContUtil_Pcnt]			AS CONVERT(REAL, [Cont_Hrs] / ([Cont_Hrs] + [Comp_Hrs]) * 100.0)
									PERSISTED	NOT	NULL,
			PRIMARY KEY CLUSTERED ([DataType] ASC, [GroupId] ASC, [Currency] ASC, [PersId] ASC, [ContId] ASC)
		);

		INSERT INTO @ContUtil([DataType], [GroupId], [Currency], [PersId], [Comp_Hrs], [ContId], [Cont_Hrs])
		SELECT
			comp.[DataType],
			comp.[GroupId],
			comp.[Currency],
			comp.[PersId],
			comp.[Maint_Hrs]		[Comp_Hrs],
			cont.[PersId]			[ContId],
			cont.[Occ_Hrs]			[Cont_Hrs]
		FROM @PersCompany			comp
		INNER JOIN @PersContract	cont
			ON	cont.[GroupId]	= comp.[GroupId]
			AND	cont.[Currency]	= comp.[Currency]
			AND	cont.[PersId]	= 'Labor'
		WHERE	comp.[PersId]	IN ('OccSal', 'OccBen');

		INSERT INTO @ContUtil([DataType], [GroupId], [Currency], [PersId], [Comp_Hrs], [ContId], [Cont_Hrs])
		SELECT
			comp.[DataType],
			comp.[GroupId],
			comp.[Currency],
			comp.[PersId],
			comp.[Other_Hrs]		[Comp_Hrs],
			'Other'					[ContId],
			SUM(cont.[Occ_Hrs])		[Cont_Hrs]
		FROM @PersCompany			comp
		INNER JOIN @PersContract	cont
			ON	cont.[GroupId]	= comp.[GroupId]
			AND	cont.[Currency]	= comp.[Currency]
			AND	cont.[PersId]	IN ('Other', 'Tech')
		WHERE	comp.[PersId]	IN ('OccSal', 'OccBen')
		GROUP BY
			comp.[DataType],
			comp.[GroupId],
			comp.[Currency],
			comp.[PersId],
			comp.[Other_Hrs];

		INSERT INTO @ContUtil([DataType], [GroupId], [Currency], [PersId], [Comp_Hrs], [ContId], [Cont_Hrs])
		SELECT
			comp.[DataType],
			comp.[GroupId],
			comp.[Currency],
			comp.[PersId],
			comp.[Maint_Hrs]		[Comp_Hrs],
			cont.[PersId]			[ContId],
			cont.[Mps_Hrs]			[Cont_Hrs]
		FROM @PersCompany			comp
		INNER JOIN @PersContract	cont
			ON	cont.[GroupId]	= comp.[GroupId]
			AND	cont.[Currency]	= comp.[Currency]
			AND	cont.[PersId]	= 'Labor'
		WHERE	comp.[PersId]	IN ('MpsSal', 'MpsBen');

		INSERT INTO @ContUtil([DataType], [GroupId], [Currency], [PersId], [Comp_Hrs], [ContId], [Cont_Hrs])
		SELECT
			comp.[DataType],
			comp.[GroupId],
			comp.[Currency],
			comp.[PersId],
			comp.[Other_Hrs]		[Comp_Hrs],
			'Other'					[ContId],
			SUM(cont.[Mps_Hrs])		[Cont_Hrs]
		FROM @PersCompany			comp
		INNER JOIN @PersContract	cont
			ON	cont.[GroupId]	= comp.[GroupId]
			AND	cont.[Currency]	= comp.[Currency]
			AND	cont.[PersId]	IN ('Other', 'Tech')
		WHERE	comp.[PersId]	IN ('MpsSal', 'MpsBen')
		GROUP BY
			comp.[DataType],
			comp.[GroupId],
			comp.[Currency],
			comp.[PersId],
			comp.[Other_Hrs];

		--	Man Hour Contract

		DECLARE @PersContPivot	TABLE
		(
			[DataType]	VARCHAR(12)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]	VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[IsGroup]	INT			NOT	NULL	CHECK([IsGroup] IN (1, 0)),
			[Currency]	VARCHAR(3)	NOT	NULL	CHECK([Currency] <> ''),
			[PersId]	VARCHAR(42)	NOT	NULL	CHECK([PersId] <> ''),
			[ContId]	CHAR(3)		NOT	NULL	CHECK([ContId] <> ''),

			[Hrs]		REAL		NOT	NULL,
			[Pcnt]		REAL		NOT	NULL,
			[Sal]		REAL		NOT	NULL,
			[Rate]		REAL		NOT	NULL,

			PRIMARY KEY CLUSTERED([DataType] ASC, [GroupId] ASC, [Currency] ASC, [PersId] ASC, [ContId] ASC)
		);

		INSERT INTO @PersContPivot([DataType], [GroupId], [IsGroup], [Currency], [PersId], [ContId], [Hrs], [Pcnt], [Sal], [Rate])
		SELECT
			p.DataType,
			p.GroupId,
			p.IsGroup,
			p.Currency,
			p.PersId,
			'Occ',
			p.Occ_Hrs,
			p.OccHrs_Pcnt,
			p.OccSal_Cur,
			p.Occ_Rate
		FROM @PersContract p; 

		INSERT INTO @PersContPivot([DataType], [GroupId], [IsGroup], [Currency], [PersId], [ContId], [Hrs], [Pcnt], [Sal], [Rate])
		SELECT
			p.DataType,
			p.GroupId,
			p.IsGroup,
			p.Currency,
			p.PersId,
			'Mps',
			p.Mps_Hrs,
			p.MpsHrs_Pcnt,
			p.MpsSal_Cur,
			p.Mps_Rate
		FROM @PersContract p; 

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F30,G30,F32,G32';
		PRINT @ProcedureDesc;

		--	Personnel Productivity Hours Gap

		DECLARE @PersProductivity_Hrs	TABLE
		(
			[FactorSetId]			VARCHAR(12)	NOT	NULL	CHECK([FactorSetId] <> ''),
			[DataType]				VARCHAR(12)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)	NOT NULL	CHECK([TargetId] <> ''),
			[Currency]				VARCHAR(3)	NOT	NULL	CHECK([Currency] <> ''),
			[PersId]				VARCHAR(6)	NOT	NULL	CHECK([PersId] <> ''),
			[ContId]				CHAR(6)		NOT	NULL	CHECK([ContId] <> ''),

			[GroupProdCap_kMT]		REAL		NOT	NULL,
			[BenchProdCap_kMT]		REAL		NOT	NULL,

			[GapPlantSize_Cur]		REAL		NOT	NULL,
			[GapEconOfScale_Cur]	REAL		NOT	NULL,

			[GapContUtil_Cur]		REAL		NOT	NULL,
			[GapTurnAround_Cur]		REAL		NOT	NULL,
			[GapOtherProd_Cur]		REAL		NOT	NULL,

			[GapPersAgg_Cur]		AS CONVERT(REAL, [GapContUtil_Cur] + [GapTurnAround_Cur] + [GapOtherProd_Cur])
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED([FactorSetId] ASC, [DataType] ASC, [GroupId] ASC, [TargetId] ASC, [Currency] ASC, [PersId] ASC, [ContId] ASC)
		);

		INSERT INTO @PersProductivity_Hrs([FactorSetId], [DataType], [GroupId], [TargetId], [Currency], [PersId], [ContId], [GroupProdCap_kMT], [BenchProdCap_kMT],
			[GapPlantSize_Cur], [GapEconOfScale_Cur], [GapContUtil_Cur], [GapTurnAround_Cur], [GapOtherProd_Cur])
		SELECT
			t.[FactorSetId],
			t.[DataType],
			t.[GroupId],
			t.[TargetId],
			t.[Currency],
			t.[PersId],
			t.[ContId],

			t.[GroupProdCap_kMT],
			t.[BenchProdCap_kMT],
			[GapPlantSize_Cur]		= t.[Bench_Rate] * t.[PlantSize_kHrs],
			[GapEconOfScale_Cur]	= t.[Bench_Rate] * t.[EconOfScale_kHrs],
			[GapContUtil_Cur]		= t.[Bench_Rate] * t.[ContUtil_kHrs],
			[GapTurnAround_Cur]		= t.[Bench_Rate] * t.[TurnAround_kHrs],
			[GapOtherProd_Cur]		= t.[Bench_Rate] * t.[OtherProd_kHrs]
		FROM (
			SELECT
				t.[FactorSetId],
				t.[DataType],
				t.[GroupId],
				t.[TargetId],
				t.[Currency],
				t.[PersId],
				t.[ContId],
				t.[GroupProdCap_kMT],
				t.[BenchProdCap_kMT],
				t.[Bench_Rate],
				[PlantSize_kHrs]	= (t.[Bench_SimpleRatio_Hrs] - t.[Bench_Hrs]) / 1000.0,
				[EconOfScale_kHrs]	= (t.[Bench_SimpleRatio_Hrs] - t.[Bench_Regression_Hrs]) / 1000.0,
				[ContUtil_kHrs]		= (t.[Bench_ContUtil_Pcnt] - t.[Group_ContUtil_Pcnt]) * t.[Bench_Regression_Hrs] / t.[Bench_ContUtil_Pcnt] / 1000.0,
				[TurnAround_kHrs]	= (t.[Bench_Regression_Hrs] / t.[Bench_ContUtil_Pcnt] * t.[Group_ContUtil_Pcnt] * (1.0 - t.[Bench_TaRatio] / t.[Group_TaRatio])) / 1000.0,
				[OtherProd_kHrs]	= (t.[Bench_Regression_Hrs] / t.[Bench_ContUtil_Pcnt] * t.[Group_ContUtil_Pcnt] * t.[Bench_TaRatio] / t.[Group_TaRatio] - t.[Group_Hrs]) / 1000.0
			FROM (
				SELECT
					eos.[FactorSetId],
					gp.[DataType],
					gp.[Currency],
					gp.[PersId],
					gp.[ContId],
					cu.[GroupProdCap_kMT],
					cu.[BenchProdCap_kMT],

					cu.[GroupId],
					[Group_Hrs]				= gp.[Hrs],
					[Group_ContUtil_Pcnt]	= gu.[ContUtil_Pcnt],
					[Group_TaRatio]			= CASE WHEN gp.[ContId] = 'Occ' THEN 0.67 * (gMta.[TaAdj_Prod_Ratio] - 1.0) + 1.0 ELSE 1 END,
					[Group_Rate]			= gp.[Rate],

					cu.[TargetId],
					[Bench_Hrs]				= bp.[Hrs],
					[Bench_ContUtil_Pcnt]	= bu.[ContUtil_Pcnt],
					[Bench_TaRatio]			= CASE WHEN gp.[ContId] = 'Occ' THEN 0.67 * (bMta.[TaAdj_Prod_Ratio] - 1.0) + 1.0 ELSE 1 END,
					[Bench_Rate]			= bp.[Rate],

					'-'[-],
					cu.[Olefins_Ratio],
					[Bench_SimpleRatio_Hrs]	= cu.[Olefins_Ratio] * bp.[Hrs],
					[Bench_Regression_Hrs]	= (eos.[Intercept] + bp.[Hrs] * POWER(cu.[Olefins_Ratio], eos.[Exponent]))

				FROM @CapUtil												cu
				INNER JOIN @PersContPivot									gp
					ON	gp.[GroupId]	= cu.[GroupId]
				INNER JOIN @ContUtil										gu
					ON	gu.[GroupId]	= cu.[GroupId]
					AND	gu.[Currency]	= gp.[Currency]
					AND	CASE gu.[ContId]
						WHEN 'Labor' THEN 'Occ'
						WHEN 'Other' THEN 'Mps'
						END				= gp.[ContId]
					AND((gu.[ContId] = 'Labor' AND gu.[PersId] = 'OccSal')
					OR  (gu.[ContId] = 'Other' AND gu.[PersId] = 'MpsSal'))
				INNER JOIN [$(DbOlefins)].[reports].[MaintTA]					gMta
					ON	gMta.[GroupID]	= cu.[GroupId]
				INNER JOIN @PersContPivot									bp
					ON	bp.[GroupId]	= cu.[TargetId]
					AND	bp.[Currency]	= gp.[Currency]
					AND	bp.[PersId]		= gp.[PersId]
					AND	bp.[ContId]		= gp.[ContId]
				INNER JOIN @ContUtil										bu
					ON	bu.[GroupId]	= cu.[TargetId]
					AND	bu.[Currency]	= gp.[Currency]
					AND	CASE bu.[ContId]
						WHEN 'Labor' THEN 'Occ'
						WHEN 'Other' THEN 'Mps'
						END				= gp.[ContId]
					AND((bu.[ContId] = 'Labor' AND bu.[PersId] = 'OccSal')
					OR  (bu.[ContId] = 'Other' AND bu.[PersId] = 'MpsSal'))
				INNER JOIN [$(DbOlefins)].[reports].[MaintTA]					bMta
					ON	bMta.[GroupID]	= cu.[GroupId]
				INNER JOIN [$(DbOlefins)].[ante].[GapEconomyOfScalePersonnel]	eos
					ON	eos.[PersId]		= gp.[PersId]
					AND	eos.[FactorSetId]	= @FactorSetId
				WHERE	cu.[TargetId]	IS NOT NULL
				) t
			) t;

		INSERT INTO @PersProductivity_Hrs([FactorSetId], [DataType], [GroupId], [TargetId], [Currency], [PersId], [ContId], [GroupProdCap_kMT], [BenchProdCap_kMT],
			[GapPlantSize_Cur], [GapEconOfScale_Cur], [GapContUtil_Cur], [GapTurnAround_Cur], [GapOtherProd_Cur])
		SELECT
			t.[FactorSetId],
			t.[DataType],
			t.[GroupId],
			t.[TargetId],
			t.[Currency],
			t.[PersId],
			t.[ContId],
			t.[GroupProdCap_kMT],
			t.[BenchProdCap_kMT],
			[GapPlantSize_Cur]		= t.[Bench_Total_Rate]	* t.[PlantSize_kHrs],
			[GapEconOfScale_Cur]	= t.[Bench_Total_Rate]	* t.[EconOfScale_kHrs],
			[GapContUtil_Cur]		= t.[Bench_Total_Rate]	* t.[ContUtil_kHrs],
			[GapTurnAround_Cur]		= t.[Bench_Total_Rate]	* t.[TurnAround_kHrs],
			[GapOtherProd_Cur]		= t.[Bench_Total_Rate]	* t.[OtherProd_kHrs]
		FROM (
			SELECT
				t.[FactorSetId],
				t.[DataType],
				t.[GroupId],
				t.[TargetId],
				t.[Currency],
				t.[PersId],
				t.[ContId],
				t.[GroupProdCap_kMT],
				t.[BenchProdCap_kMT],
				t.[Bench_Total_Rate],
				[PlantSize_kHrs]	= (t.[Bench_Hrs] - t.[Bench_SimpleRatio_Hrs]) / 1000.0,
				[EconOfScale_kHrs]	= (t.[Bench_SimpleRatio_Hrs] - t.[Bench_Regression_Hrs]) / 1000.0,
				[ContUtil_kHrs]		= (t.[Group_ContUtil_Pcnt] - t.[Bench_ContUtil_Pcnt]) * t.[Bench_Regression_Hrs] / (100.0 - t.[Bench_ContUtil_Pcnt]) / 1000.0,
				[TurnAround_kHrs]	= (t.[Bench_Regression_Hrs] / (100.0 - t.[Bench_ContUtil_Pcnt]) * (100.0 - t.[Group_ContUtil_Pcnt]) * (1.0 - t.[Bench_TaAdjRatio] / t.[Group_TaAdjRatio])),
				[OtherProd_kHrs]	= ((t.[Bench_Regression_Hrs] / (100.0 - t.[Bench_ContUtil_Pcnt]) * (100.0 - t.[Group_ContUtil_Pcnt]) * (t.[Bench_TaAdjRatio] / t.[Group_TaAdjRatio])) - t.[Group_Hrs]) / 1000.0
			FROM (
				SELECT
					eos.[FactorSetId],
					gp.[DataType],

					g.[Currency],
					g.[PersId],
					g.[ContId],
					cu.[GroupProdCap_kMT],
					cu.[BenchProdCap_kMT],

					cu.[GroupId],
					[Group_ContUtil_Pcnt]	= g.[ContUtil_Pcnt],
					[Group_Maint_Hrs]		= gp.[Maint_Hrs],
					[Group_Other_Hrs]		= gp.[Other_Hrs],
					[Group_Hrs]				= CASE WHEN g.[ContId] = 'Labor' THEN gp.[Maint_Hrs] ELSE gp.[Other_Hrs] END,
					[Group_Total_Rate]		= gp.[Total_Rate],
					[Group_TaAdjRatio]		= CASE WHEN g.[ContId] = 'Labor' AND gMta.[TaAdj_Prod_Ratio] <> 1.0 THEN 0.67 * (gMta.[TaAdj_Prod_Ratio] - 1.0) ELSE 1.0 END,
					cu.[GroupOlefinsCpby_kMT],

					cu.[TargetId],
					[Bench_ContUtil_Pcnt]	= b.[ContUtil_Pcnt],
					[Bench_Maint_Hrs]		= bp.[Maint_Hrs],
					[Bench_Other_Hrs]		= bp.[Other_Hrs],
					[Bench_Hrs]				= CASE WHEN g.[ContId] = 'Labor' THEN bp.[Maint_Hrs] ELSE bp.[Other_Hrs] END,
					[Bench_Total_Rate]		= bp.[Total_Rate],
					[Bench_TaAdjRatio]		= CASE WHEN g.[ContId] = 'Labor' AND bMta.[TaAdj_Prod_Ratio] <> 1.0 THEN 0.67 * (bMta.[TaAdj_Prod_Ratio] - 1.0) ELSE 1.0 END,
					cu.[BenchOlefinsCpby_kMT],

					cu.[Olefins_Ratio],
					[Bench_SimpleRatio_Hrs]	= cu.[Olefins_Ratio]	* CASE WHEN g.[ContId] = 'Labor' THEN bp.[Maint_Hrs] ELSE bp.[Other_Hrs] END,
					[Bench_Regression_Hrs]	= (eos.[Intercept]		+ CASE WHEN g.[ContId] = 'Labor' THEN bp.[Maint_Hrs] ELSE bp.[Other_Hrs] END * POWER(cu.[Olefins_Ratio], eos.[Exponent]))
				FROM @CapUtil												cu
				INNER JOIN @ContUtil										g
					ON	g.[GroupId]		= cu.[GroupId]
				INNER JOIN @PersCompany										gp
					ON	gp.[GroupId]	= cu.[GroupId]
					AND	gp.[Currency]	= g.[Currency]
					AND	gp.[PersId]		= g.[PersId]
				INNER JOIN @ContUtil										b
					ON	b.[GroupId]		= cu.[TargetId]
					AND	b.[DataType]	= g.[DataType]
					AND	b.[Currency]	= g.[Currency]
					AND	b.[PersId]		= g.[PersId]
					AND	b.[ContId]		= g.[ContId]
				INNER JOIN @PersCompany										bp
					ON	bp.[GroupId]	= cu.[TargetId]
					AND	bp.[Currency]	= g.[Currency]
					AND	bp.[PersId]		= g.[PersId]
				INNER JOIN [$(DbOlefins)].[ante].[GapEconomyOfScalePersonnel]	eos
					ON	eos.[PersId]		= g.[PersId]
					AND	eos.[FactorSetId]	= @FactorSetId
				INNER JOIN [$(DbOlefins)].[reports].[MaintTA]					gMta
					ON	gMta.[GroupID]	= cu.[GroupId]
				INNER JOIN [$(DbOlefins)].[reports].[MaintTA]					bMta
					ON	bMta.[GroupID]	= cu.[GroupId]
				WHERE	cu.[TargetId]	IS NOT NULL
				) t
			) t;

		/*	Also Includes Plant Size and Economy of Scale Calculations	*/
		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			g.[GroupId],
			g.[TargetId],
			g.[GroupProdCap_kMT],
			g.[BenchProdCap_kMT],
			'Pers' + CASE
				WHEN g.[PersId] IN ('MpsBen', 'MpsSal', 'OccBen', 'OccSal') THEN 'Company_Hours'
				WHEN g.[PersId] IN ('Labor', 'Other', 'Tech')				THEN 'Contractor_Hours'
				END							[AccountId],
			g.[Currency],
			'WHrs',
			[PersAgg]		= SUM(g.[GapPersAgg_Cur])		/ 1000.0 
		FROM @PersProductivity_Hrs g
		GROUP BY
			g.[FactorSetId],
			g.[GroupId],
			g.[TargetId],
			g.[GroupProdCap_kMT],
			g.[BenchProdCap_kMT],
			CASE
				WHEN g.[PersId] IN ('MpsBen', 'MpsSal', 'OccBen', 'OccSal') THEN 'Company_Hours'
				WHEN g.[PersId] IN ('Labor', 'Other', 'Tech')				THEN 'Contractor_Hours'
				END,
			g.[Currency],
			g.[DataType];

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F30,G30,F32,G32';
		PRINT @ProcedureDesc;

		-- Personnel Compensation Rate Gap

		DECLARE @PersCompensation_Rate	TABLE
		(
			[DataType]				VARCHAR(12)	NOT	NULL	CHECK([DataType] <> ''),
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)	NOT NULL	CHECK([TargetId] <> ''),
			[Currency]				VARCHAR(3)	NOT	NULL	CHECK([Currency] <> ''),
			[PersId]				VARCHAR(6)	NOT	NULL	CHECK([PersId] <> ''),

			[GroupProdCap_kMT]		REAL		NOT	NULL,
			[BenchProdCap_kMT]		REAL		NOT	NULL,

			[GapTotHours]			REAL		NOT	NULL,
			[GapOccHours]			REAL		NOT	NULL,
			[GapMpsHours]			REAL		NOT	NULL,

			[GapTotRate]			REAL		NOT	NULL,
			[GapOccRate]			REAL		NOT	NULL,
			[GapMpsRate]			REAL		NOT	NULL,

			PRIMARY KEY CLUSTERED([DataType] ASC, [GroupId] ASC, [TargetId] ASC, [Currency] ASC, [PersId] ASC)
		);

		--	Unit Rate Gaps (rows 17, 18)
		INSERT INTO @PersCompensation_Rate([DataType], [GroupId], [TargetId], [Currency], [PersId], [GroupProdCap_kMT], [BenchProdCap_kMT],
			[GapTotHours], [GapOccHours], [GapMpsHours], [GapTotRate], [GapOccRate], [GapMpsRate])
		SELECT
			g.[DataType],
			cu.[GroupId],
			cu.[TargetId],
			g.[Currency],
			g.[PersId],

			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],

			[GapTotHours] = (b.[Tot_Hrs] - g.[Tot_Hrs]) * b.[Tot_Rate] / 1000.0,
			[GapOccHours] = (b.[Occ_Hrs] - g.[Occ_Hrs]) * b.[Occ_Rate] / 1000.0,
			[GapMpsHours] = (b.[Mps_Hrs] - g.[Mps_Hrs]) * b.[Mps_Rate] / 1000.0,

			[GapTotRate] = (b.[Tot_Rate] - g.[Tot_Rate]) * g.[Tot_Hrs] / 1000.0,	--	[ContMaintLabor], [ContTechSrvice], [OtherContractor] => SUM() Hourly Contract Rates (*)
			[GapOccRate] = (b.[Occ_Rate] - g.[Occ_Rate]) * g.[Occ_Hrs] / 1000.0,
			[GapMpsRate] = (b.[Mps_Rate] - g.[Mps_Rate]) * g.[Mps_Hrs] / 1000.0
		FROM @CapUtil				cu
		INNER JOIN @PersContract	g
			ON	g.[GroupId]			= cu.[GroupId]
		INNER JOIN @PersContract	b
			ON	b.[GroupId]			= cu.[TargetId]
			AND	b.[DataType]		= g.[DataType]
			AND	b.[Currency]		= g.[Currency]
			AND	b.[PersId]			= g.[PersId]
		WHERE	cu.[TargetId]		IS NOT NULL;

		--	Unit Rate Gaps (rows 17, 18)
		INSERT INTO @PersCompensation_Rate([DataType], [GroupId], [TargetId], [Currency], [PersId], [GroupProdCap_kMT], [BenchProdCap_kMT],
			[GapTotHours], [GapOccHours], [GapMpsHours], [GapTotRate], [GapOccRate], [GapMpsRate])
		SELECT
			g.[DataType],
			cu.[GroupId],
			cu.[TargetId],
			g.[Currency],
			g.[PersId],

			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],

			[GapTotalHours] = (b.[Total_Hrs] - g.[Total_Hrs]) * b.[Total_Rate] / 1000.0,
			[GapMaintHours] = (b.[Maint_Hrs] - g.[Maint_Hrs]) * b.[Maint_Rate] / 1000.0,
			[GapOtherHours] = (b.[Other_Hrs] - g.[Other_Hrs]) * b.[Other_Rate] / 1000.0,

			[GapTotalRate] = (b.[Total_Rate] - g.[Total_Rate]) * g.[Total_Hrs] / 1000.0,
			[GapMaintRate] = (b.[Maint_Rate] - g.[Maint_Rate]) * g.[Maint_Hrs] / 1000.0,
			[GapOtherRate] = (b.[Other_Rate] - g.[Other_Rate]) * g.[Other_Hrs] / 1000.0
		FROM @CapUtil				cu
		INNER JOIN @PersCompany		g
			ON	g.[GroupId]			= cu.[GroupId]
		INNER JOIN @PersCompany		b
			ON	b.[GroupId]			= cu.[TargetId]
			AND	b.[DataType]		= g.[DataType]
			AND	b.[Currency]		= g.[Currency]
			AND	b.[PersId]			= g.[PersId]
		WHERE	cu.[TargetId]		IS NOT NULL;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			g.[GroupId],
			g.[TargetId],
			g.[GroupProdCap_kMT],
			g.[BenchProdCap_kMT],
			'Pers' + CASE
				WHEN g.[PersId] IN ('MpsBen', 'MpsSal', 'OccBen', 'OccSal') THEN 'Company_Rate'
				WHEN g.[PersId] IN ('Labor', 'Other', 'Tech')				THEN 'Contractor_Rate'
				END							[AccountId],
			g.[Currency],
			g.[DataType],
			SUM(g.[GapTotRate]) / 1000.0	[Gap]
		FROM @PersCompensation_Rate	g
		GROUP BY
			g.[DataType],
			g.[GroupId],
			g.[TargetId],
			g.[Currency],
			g.[GroupProdCap_kMT],
			g.[BenchProdCap_kMT],
			CASE
				WHEN g.[PersId] IN ('MpsBen', 'MpsSal', 'OccBen', 'OccSal') THEN 'Company_Rate'
				WHEN g.[PersId] IN ('Labor', 'Other', 'Tech')				THEN 'Contractor_Rate'
				END;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewOPEX!E7:E25			->	900, 1000 (GAM!B74:D75)	-> Plant Capacity (OpEx)	(Insert Complete)';		--	(Top)
		PRINT @ProcedureDesc;

		BEGIN

		DECLARE @OpExPlantSize	TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)	NOT NULL	CHECK([TargetId] <> ''),
			[CurrencyId]			VARCHAR(4)	NOT	NULL	CHECK([CurrencyId] <> ''),
			[OpExId]				VARCHAR(25)	NOT	NULL	CHECK([OpExId] <> ''),
			[LinAmount_Cur]			REAL		NOT	NULL,
			[EosAmount_Cur]			REAL		NOT	NULL,
			[TotAmount_Cur]			AS	CONVERT(REAL, [LinAmount_Cur] + [EosAmount_Cur])
									PERSISTED	NOT	NULL,
			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [OpExId] ASC, [CurrencyId] ASC)
		);

		--SET @ProcedureDesc = CHAR(9) + 'NewOPEX!E7:E11';
		INSERT INTO @OpExPlantSize([GroupId], [TargetId], [CurrencyId], [OpExId], [LinAmount_Cur], [EosAmount_Cur])
		SELECT
			cu.[GroupId],
			cu.[TargetId],
			bPers.[Currency],
			bPers.[PersId],
			[LinAmount_Cur] = bPers.[Total_Rate] * (bPers.[Total_Hrs] - bPers.[Total_Hrs] * cu.[GroupOlefinsCpby_kMT] / cu.[BenchOlefinsCpby_kMT]) / 1000.0,
			[EosAmount_Cur] = t.[GapEconOfScale_Cur]
		FROM @CapUtil				cu
		INNER JOIN @PersCompany		bPers
			ON	bPers.[GroupId]		= cu.[TargetId]
		INNER JOIN (
			SELECT
				p.[GroupId],
				p.[TargetId],
				p.[Currency],
				p.[PersId],
				[GapEconOfScale_Cur] = SUM(p.[GapEconOfScale_Cur])
			FROM @PersProductivity_Hrs p
			GROUP BY
				p.[GroupId],
				p.[TargetId],
				p.[Currency],
				p.[PersId]	
			) t
			ON	t.[GroupId]		= cu.[GroupId]
			AND t.[TargetId]	= cu.[TargetId]
			AND	t.[PersId]		= bPers.[PersId]
			AND	t.[Currency]	= bPers.[Currency]
		WHERE cu.[TargetId]	IS NOT NULL;

		--SET @ProcedureDesc = CHAR(9) + 'NewOPEX!E14,E17,E19';	-- Possible Contractor Tech Services Error
		INSERT INTO @OpExPlantSize([GroupId], [TargetId], [CurrencyId], [OpExId], [LinAmount_Cur], [EosAmount_Cur])
		SELECT
			cu.[GroupId],
			cu.[TargetId],
			bPers.[Currency],
			bPers.[PersId],
			[LinAmount_Cur] = bPers.[Tot_Rate] * (bPers.[Tot_Hrs] - bPers.[Tot_Hrs] * cu.[GroupOlefinsCpby_kMT] / cu.[BenchOlefinsCpby_kMT]) / 1000.0,
			[EosAmount_Cur] = t.[GapEconOfScale_Cur]
		FROM @CapUtil				cu
		INNER JOIN @PersContract	bPers
			ON	bPers.[GroupId]		= cu.[TargetId]
		INNER JOIN (
			SELECT
				p.[GroupId],
				p.[TargetId],
				p.[Currency],
				p.[PersId],
				[GapEconOfScale_Cur] = SUM(p.[GapEconOfScale_Cur])
			FROM @PersProductivity_Hrs p
			GROUP BY
				p.[GroupId],
				p.[TargetId],
				p.[Currency],
				p.[PersId]	
			) t
			ON	t.[GroupId]		= cu.[GroupId]
			AND t.[TargetId]	= cu.[TargetId]
			AND	t.[PersId]		= bPers.[PersId]
			AND	t.[Currency]	= bPers.[Currency]
		WHERE cu.[TargetId]	IS NOT NULL;

		--SET @ProcedureDesc = CHAR(9) + 'NewOPEX!E12,E13,E15,E16,E18,E20:E23,E25';
		INSERT INTO @OpExPlantSize([GroupId], [TargetId], [CurrencyId], [OpExId], [LinAmount_Cur], [EosAmount_Cur])
		SELECT
			u.[GroupId],
			u.[TargetId],
			u.[Currency],
			u.[OpExId],
			[LinAmount_Cur] = u.[Amount_Cur] * u.[OlefinsCpby_Ratio],
			[EosAmount_Cur] = u.[Amount_Cur] * (u.[Olefins_Ratio] - POWER(u.[HvcProd_Ratio], t.[Exponent]))
		FROM (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				o.[Currency],
				cu.[OlefinsCpby_Ratio],
				cu.[Olefins_Ratio],
				cu.[HvcProd_Ratio],
				o.[MaintRetube],
				o.[MaintMatl],
				o.[MaintContractMatl],
				o.[MaintEquip],
				o.[Envir],
				o.[NonMaintEquipRent],
				o.[Tax],
				o.[Insur],
				o.[OtherNonVol],
				o.[TAAccrual]
			FROM @CapUtil								cu
			INNER JOIN [$(DbOlefins)].[reports].[OPEX]		o
				ON	o.[GroupID]		= cu.[TargetId]
				AND	o.[DataType]	= 'ADJ'
			WHERE cu.[TargetId]	IS NOT NULL
			) t
			UNPIVOT (
				[Amount_Cur] FOR [OpExId] IN (
					[MaintRetube],
					[MaintMatl],
					[MaintContractMatl],
					[MaintEquip],
					[Envir],
					[NonMaintEquipRent],
					[Tax],
					[Insur],
					[OtherNonVol],
					[TAAccrual]
				)
			) u
		INNER JOIN [$(DbOlefins)].[ante].[GapEconomyOfScale] t
			ON	t.[AccountId]	= u.[OpExId]
			AND	t.[FactorSetId]	= @FactorSetId;

		DECLARE @OpExPlantCap		TABLE
		(
			[GroupId]				VARCHAR(25)	NOT NULL	CHECK([GroupId] <> ''),
			[TargetId]				VARCHAR(25)	NOT NULL	CHECK([TargetId] <> ''),
			[GroupProdCap_kMT]		REAL		NOT	NULL,
			[BenchProdCap_kMT]		REAL		NOT	NULL,

			[CurrencyId]			VARCHAR(4)	NOT	NULL	CHECK([CurrencyId] <> ''),

			[PyroProd_Ratio]		REAL		NOT	NULL,
			[SuppProd_Ratio]		REAL		NOT	NULL,

			[LinAmount_Cur]			REAL		NOT	NULL,
			[EosAmount_Cur]			REAL		NOT	NULL,
			[TotAmount_Cur]			AS	CONVERT(REAL, [LinAmount_Cur] + [EosAmount_Cur])
									PERSISTED	NOT	NULL,

			[PyroLinAmount_Cur]		AS	CONVERT(REAL, [PyroProd_Ratio] * [LinAmount_Cur])
									PERSISTED	NOT	NULL,
			[PyroEosAmount_Cur]		AS	CONVERT(REAL, [PyroProd_Ratio] * [EosAmount_Cur])
									PERSISTED	NOT	NULL,
			[PyroTotAmount_Cur]		AS	CONVERT(REAL, [PyroProd_Ratio] * ([LinAmount_Cur] + [EosAmount_Cur]))
									PERSISTED	NOT	NULL,

			[SuppLinAmount_Cur]		AS	CONVERT(REAL, [SuppProd_Ratio] * [LinAmount_Cur])
									PERSISTED	NOT	NULL,
			[SuppEosAmount_Cur]		AS	CONVERT(REAL, [SuppProd_Ratio] * [EosAmount_Cur])
									PERSISTED	NOT	NULL,
			[SuppTotAmount_Cur]		AS	CONVERT(REAL, [SuppProd_Ratio] * ([LinAmount_Cur] + [EosAmount_Cur]))
									PERSISTED	NOT	NULL,

			PRIMARY KEY CLUSTERED([GroupId] ASC, [TargetId] ASC, [CurrencyId] ASC)
		);

		INSERT INTO @OpExPlantCap([GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT],
			[CurrencyId], [PyroProd_Ratio], [SuppProd_Ratio], [LinAmount_Cur], [EosAmount_Cur])
		SELECT
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			ox.[CurrencyId],
			cu.[PyroProd_Ratio],
			cu.[SuppProd_Ratio],
			[Linear]	= SUM(ox.[LinAmount_Cur]) / 1000.0,
			[Eos]		= SUM(ox.[EosAmount_Cur]) / 1000.0
		FROM @CapUtil				cu
		INNER JOIN @OpexPlantSize	ox
			ON	ox.[GroupId]	= cu.[GroupId]
			AND	ox.[TargetId]	= cu.[TargetId]
		WHERE	cu.[TargetId]	IS NOT NULL
		GROUP BY
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			cu.[PyroProd_Ratio],
			cu.[SuppProd_Ratio],
			ox.[CurrencyId];

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			u.[GroupId],
			u.[TargetId],
			u.[GroupProdCap_kMT],
			u.[BenchProdCap_kMT],
			u.[GapId],
			u.[CurrencyId],
			[DataType] = 'Total',
			u.[GapAmount_Cut]
		FROM (
			SELECT
				opc.[GroupId],
				opc.[TargetId],
				opc.[GroupProdCap_kMT],
				opc.[BenchProdCap_kMT],
				opc.[CurrencyId],
				[Pyro_PlantLinear]  = opc.[PyroLinAmount_Cur],
				[Pyro_PlantEos]		= opc.[PyroEosAmount_Cur],
				[Supp_PlantLinear]	= opc.[SuppLinAmount_Cur],
				[Supp_PlantEos]		= opc.[SuppEosAmount_Cur]
			FROM @OpExPlantCap	opc
			) p
			UNPIVOT (
				[GapAmount_Cut] FOR [GapId] IN (
				[Pyro_PlantLinear],
				[Pyro_PlantEos],
				[Supp_PlantLinear],
				[Supp_PlantEos]
				)
			) u;

		END;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F35:G36	->	3400			-> Annualized Turnaround Costs	(Insert Complete)';
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			g.[GroupId],
			g.[TargetId],
			g.[GroupProdCap_kMT],
			g.[BenchProdCap_kMT],
			g.[AccountId],
			g.[Currency],
			'Total',
			g.[Gap]
		FROM (
			SELECT
				eos.[FactorSetId],
				h.[GroupId],
				h.[TargetId],
				gOpEx.[Currency],
				h.[GroupProdCap_kMT],
				h.[BenchProdCap_kMT],

				[TaCurrExp]		= CONVERT(REAL, (bMaint.[TACost_MUS] * POWER(h.Olefins_Ratio, eos.[Exponent]) - gMaint.[TACost_MUS]) / bMaint.[TAInt_Month] * 12.0),
				[TaInterval]	= gMaint.[TACost_MUS] * (12.0 / bMaint.[TAInt_Month] - 12.0 / gMaint.[TAInt_Month])

			FROM @CapUtil										h
			INNER JOIN [$(DbOlefins)].[ante].[GapEconomyOfScale]		eos
				ON	eos.[AccountId]		= 'STTaExp'
				AND	eos.[FactorSetId]	= @FactorSetId
			INNER JOIN [$(DbOlefins)].[reports].[OPEX]				gOpEx
				ON	gOpEx.[GroupID]		= h.[GroupId]
				AND	gOpEx.[Currency]	= 'USD'
				AND	gOpEx.[DataType]	= 'ADJ'
			INNER JOIN [$(DbOlefins)].[reports].[MaintTA]			gMaint
				ON	gMaint.[GroupID]	= h.[GroupId]
			INNER JOIN [$(DatabaseName)].[reports].[OPEX]				bOpEx
				ON	bOpEx.[GroupID]		= h.[TargetId]
				AND	bOpEx.[Currency]	= gOpEx.[Currency]
				AND	bOpEx.[DataType]	= gOpEx.[DataType]
			INNER JOIN [$(DatabaseName)].[reports].[MaintTA]			bMaint
				ON	bMaint.[GroupID]	= h.[TargetId]
			WHERE	h.[TargetId]		IS NOT NULL
			) t
		UNPIVOT ([Gap] FOR [AccountId] IN
			(
				[TaCurrExp],
				[TaInterval]
			)
			) g;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!F39:G45	->	3800			-> Other Fixed Cash Costs (Insert Complete)';
		PRINT @ProcedureDesc;

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur])
		SELECT
			@FactorSetId,
			h.[GroupID],
			h.[TargetId],
			h.[GroupProdCap_kMT],
			h.[BenchProdCap_kMT],
			e.[AccountId],
			d.[Currency],
			'Total',
			(b.[BenchAmount] * POWER(h.[HvcProd_Ratio], e.[Exponent]) - g.[GroupAmount]) / 1000.0	[Gap]
		FROM [$(DbOlefins)].[ante].[GapEconomyOfScale]				e
		CROSS JOIN (VALUES ('USD', 'ADJ'))			d ([Currency], [DataType])
		CROSS JOIN @CapUtil								h
		LEFT OUTER JOIN (
			SELECT
				r.[GroupID],
				r.[Currency],
				r.[DataType],
				r.[MaintRetube],
				r.[MaintMatl],
				r.[MaintContractMatl],
				r.[MaintEquip],
				r.[Envir],
				r.[NonMaintEquipRent],
				r.[Tax],
				r.[Insur],
				r.[OtherNonVol]
			FROM	[$(DbOlefins)].[reports].[OPEX]			r
			WHERE	r.[GroupID]		= @GroupId
				AND	r.[DataType]	= 'ADJ'
				AND	r.[Currency]	= 'USD'
			) p
			UNPIVOT ([GroupAmount] FOR [AccountId] IN (
				[MaintRetube],
				[MaintMatl],
				[MaintContractMatl],
				[MaintEquip],
				[Envir],
				[NonMaintEquipRent],
				[Tax],
				[Insur],
				[OtherNonVol]
				)
			) g
			ON	g.[GroupID]		= h.[GroupID]
			AND	g.[AccountId]	= e.[AccountId]
		LEFT OUTER JOIN (
			SELECT
				r.[GroupID],
				r.[Currency],
				r.[DataType],
				r.[MaintRetube],
				r.[MaintMatl],
				r.[MaintContractMatl],
				r.[MaintEquip],
				r.[Envir],
				r.[NonMaintEquipRent],
				r.[Tax],
				r.[Insur],
				r.[OtherNonVol]
			FROM	[$(DatabaseName)].[reports].[OPEX]			r
			WHERE	r.[GroupID]		= @TargetId
				AND	r.[DataType]	= 'ADJ'
				AND	r.[Currency]	= 'USD'
			) p
			UNPIVOT ([BenchAmount] FOR [AccountId] IN (
				[MaintRetube],
				[MaintMatl],
				[MaintContractMatl],
				[MaintEquip],
				[Envir],
				[NonMaintEquipRent],
				[Tax],
				[Insur],
				[OtherNonVol]
				)
			) b
			ON	b.[GroupID]		= h.[TargetId]
			AND	b.[AccountId]	= e.[AccountId]
		WHERE	h.[TargetId]	IS NOT NULL
			AND	e.[AccountId]	IN ('MaintMatl', 'MaintContractMatl', 'MaintEquip', 'MaintRetube', 'Envir', 'NonMaintEquipRent', 'Insur', 'Tax', 'OtherNonVol')
			AND	e.[FactorSetId]	= @FactorSetId;

		-------------------------------------------------------------------

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!G19:G22	->	1800 (DIV)		-> Capability Utilization (Update Complete)';
		PRINT @ProcedureDesc;

		UPDATE gar
		SET [DivAmount_Cur] = mlt.[DivAmount_Cur]
		FROM [$(DbOlefins)].[gap].[AnalysisResults] gar
		INNER JOIN (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				cu.[GroupProdCap_kMT],
				cu.[BenchProdCap_kMT],
				bOpEx.[Currency],
				cu.[GroupPyroCpby_Pcnt],
				cu.[GroupSuppCpby_Pcnt],
				[GAM!E76] = cu.[GroupPyroCpby_Pcnt] * 
					CASE
					WHEN [GroupSuppCpby_kMT] = 0.0
					THEN
						(COALESCE(bOpEx.[STNonVol], 0.0) + COALESCE(bOpEx.[TAAccrual], 0.0) - gPlantCap.[TotAmount_Cur] * 1000.0)
							*
							(1.0 / (cu.[GroupProdCpby_kMT] * cu.[BenchCapability_Pcnt] / 100.0) - 1.0 / cu.[GroupProdCap_kMT])
					ELSE
						(cu.[BenchPyroCpby_Pcnt] * (COALESCE(bOpEx.[STNonVol], 0.0) + COALESCE(bOpEx.[TAAccrual], 0.0)) - gPlantCap.[PyroTotAmount_Cur] * 1000.0)
							*
							(1.0 / (cu.[GroupPyroCpby_kMT] * cu.[BenchCapability_Pcnt] / 100.0) - 1.0 / cu.[GroupPyroCap_kMT])
					END,

				[GAM!F76] = cu.[GroupSuppCpby_Pcnt] * 
					CASE
					WHEN [GroupSuppCpby_kMT] = 0.0
					THEN
						0.0
					ELSE
						((cu.[BenchSuppCpby_Pcnt] * (COALESCE(bOpEx.[STNonVol], 0.0) + COALESCE(bOpEx.[TAAccrual], 0.0))) - gPlantCap.[SuppTotAmount_Cur] * 1000.0)
						*
						(1.0 / (cu.[GroupSuppCpby_kMT] * cu.[BenchCapability_Pcnt] / 100.0) - 1.0 / [GroupSuppCap_kMT])
					END

			FROM @CapUtil										cu
			INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			bOpEx
				ON	bOpEx.[GroupID]			= cu.[TargetId]
				AND	bOpEx.[DataType]		= 'ADJ'
				AND	bOpEx.[Currency]		= 'USD'
			INNER JOIN @OpExPlantCap							gPlantCap
				ON	gPlantCap.[GroupId]		= cu.[GroupId]
				AND	gPlantCap.[TargetId]	= cu.[TargetId]
				AND	gPlantCap.[CurrencyId]	= 'USD'
			) t
		INNER JOIN @ReliabilityOppLoss		rol
			ON	rol.[GroupId]	= t.[GroupId]
			AND	rol.[TargetId]	= t.[TargetId]
		CROSS APPLY (VALUES
			('DT', rol.[DtLoss_Pcnt_Pcnt] * (COALESCE(t.[GAM!E76], 0.0) + COALESCE(t.[GAM!F76], 0.0))),
			('SD', rol.[SdLoss_Pcnt_Pcnt] * (COALESCE(t.[GAM!E76], 0.0) + COALESCE(t.[GAM!F76], 0.0)))
			) mlt([DataType], [DivAmount_Cur])
			ON	t.[GroupId]								= gar.[GroupId]
			AND	t.[TargetId]							= gar.[TargetId]
			AND	rol.[AccountId] + '_' + mlt.[DataType]	= gar.[GapId]
			AND	t.[Currency]							= gar.[CurrencyId]
			AND	mlt.[DataType]							= gar.[DataType]
		WHERE	gar.GapId IN ('NOC_DT', 'NOC_SD', 'TurnAround_DT', 'TurnAround_SD', 'UnPlanned_DT', 'UnPlanned_SD', 'UnPlanned_SD')
			AND	gar.[FactorSetID] = @FactorSetId;

		SET @ProcedureDesc = CHAR(9) + 'NewGapAnalysis!G9:G10	->	900, 1000 (DIV)	-> Capacity (Update Complete)';
		PRINT @ProcedureDesc;

		UPDATE gar
		SET [DivAmount_Cur] = mlt.[DivAmount_Cur]
		FROM [$(DbOlefins)].[gap].[AnalysisResults] gar
		INNER JOIN (
			SELECT
				cu.[GroupId],
				cu.[TargetId],
				bOpEx.[Currency],

				[B107] = cu.[PyroProd_Ratio],
				[C107] = cu.[SuppProd_Ratio],

				[GAM!D107]	= (COALESCE(bOpEx.[STNonVol], 0.0) + COALESCE(bOpEx.[TAAccrual], 0.0)) / cu.[BenchProdCap_kMT]
							- (COALESCE(bOpEx.[STNonVol], 0.0) + COALESCE(bOpEx.[TAAccrual], 0.0) - COALESCE(gPlantCap.[TotAmount_Cur] * 1000.0, 0.0)) / cu.[GroupProdCpby_kMT] / cu.[BenchCapability_Pcnt] * 100.0,

				[PyroCpby_Pcnt]	= (cu.[GroupPyroCpby_Pcnt] - cu.[BenchPyroCpby_Pcnt]),	--	GAM!G18,G48, SFEC!H67 (Portions)	--	OK
				[SuppCpby_Pcnt]	= (cu.[GroupSuppCpby_Pcnt] - cu.[BenchSuppCpby_Pcnt]),	--	GAM!G18,G48, SFEC!H68 (Portions)	--	OK
				/*
				'-'[-],
				[GAM!E6]			=				   bYm.[_GMPyro]	/ cu.[BenchPyroCap_kMT] * 1000.0,
				[GAM!E9]			= (cYm.[_GMPyro] - bYm.[_GMPyro])	/ cu.[BenchPyroCap_kMT] * 1000.0,
				[GAM!E]				=  cYm.[_GMPyro] / cu.[BenchPyroCap_kMT] * 1000.0,

				'-'[-],
				[GAM!F6]			=				   bYm.[_GMSupp]	/ cu.[BenchSuppCap_kMT] * 1000.0,
				[GAM!F9]			= (cYm.[_GMSupp] - bYm.[_GMSupp])	/ cu.[BenchSuppCap_kMT] * 1000.0,
				[GAM!F]				=  cYm.[_GMSupp] / cu.[BenchSuppCap_kMT] * 1000.0,
				*/

				[GAM!G18 (Pyro)]	= cYm.[_GMPyro] / cu.[BenchPyroCap_kMT] * 1000.0,
				[GAM!G18 (Supp)]	=
					CASE
					WHEN cu.[BenchSuppCap_kMT] = 0.0
					THEN 0.0
					ELSE cYm.[_GMSupp] / cu.[BenchSuppCap_kMT] * 1000.0
					END,

				/*
				[SFEC!H42]	= cu.[BenchPyroCap_kMT],
				[SFEC!H43]	= cu.[BenchSuppCap_kMT],
				[SFEC!H44]	= cu.[BenchProdCap_kMT],

				[SFEC!H40]	= (bEr.[NetEnergyCons] - (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0)),


				[SFEC!H46]	= (bEr.[NetEnergyCons] - (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0)) / cu.[BenchPyroCap_kMT] / 2.2046,
				[SFEC!H47]	= (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0) / cu.[BenchSuppCap_kMT] / 2.2046,
				[SFEC!H48]	= bEr.[NetEnergyCons] / cu.[BenchProdCap_kMT] / 2.2046,

				[SFEC!H50]	= bOpEx.[STVol],
				[SFEC!H51]	= bOpEx.[EnergyOpex],
				[SFEC!H52]	= bOpEx.[STVol] - bOpEx.[EnergyOpex],

				[SFEC!H55]	= bOpEx.[EnergyOpex]					/ cu.[BenchProdCap_kMT],
				[SFEC!H56]	= (bOpEx.[STVol] - bOpEx.[EnergyOpex])	/ cu.[BenchProdCap_kMT],

				[SFEC!H58]	= ((bEr.[NetEnergyCons] - (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0)) * bOpEx.[EnergyOpex] / bEr.[NetEnergyCons])	/ cu.[BenchPyroCap_kMT],
				[SFEC!H59]	= (bOpEx.[STVol] - bOpEx.[EnergyOpex])																	/ cu.[BenchProdCap_kMT],
				[SFEC!H60]	= ((bEr.[NetEnergyCons] - (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0)) * bOpEx.[EnergyOpex] / bEr.[NetEnergyCons] + (bOpEx.[STVol] - bOpEx.[EnergyOpex])) / cu.[BenchPyroCap_kMT],

				[SFEC!H62]	= bOpEx.[EnergyOpex] * (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0) / cu.[BenchSuppCap_kMT] / bEr.[NetEnergyCons],
				[SFEC!H63]	= (bOpEx.[STVol] - bOpEx.[EnergyOpex]) / cu.[BenchProdCap_kMT],
				[SFEC!H64]	= (bOpEx.[EnergyOpex] * (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0) / cu.[BenchSuppCap_kMT] / bEr.[NetEnergyCons]) + (bOpEx.[STVol] - bOpEx.[EnergyOpex]) / cu.[BenchProdCap_kMT],

				[SFEC!H67 (Pyro)]	= (((bEr.[NetEnergyCons] - (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0)) * bOpEx.[EnergyOpex] / bEr.[NetEnergyCons] + (bOpEx.[STVol] - bOpEx.[EnergyOpex])) / cu.[BenchPyroCap_kMT]) * cu.[BenchPyroCap_kMT] / cu.[BenchPyroCap_kMT] * 1000.0,
				[SFEC!H68 (Supp)]	= ((bOpEx.[EnergyOpex] * (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0) / cu.[BenchSuppCap_kMT] / bEr.[NetEnergyCons]) + (bOpEx.[STVol] - bOpEx.[EnergyOpex]) / cu.[BenchProdCap_kMT]) * cu.[BenchSuppCap_kMT] / cu.[BenchSuppCap_kMT] * 1000.0,
				*/

				[GAM!E22 (Pyro)]	= - (((bEr.[NetEnergyCons] - (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0)) * bOpEx.[EnergyOpex] / bEr.[NetEnergyCons] + (bOpEx.[STVol] - bOpEx.[EnergyOpex])) / cu.[BenchPyroCap_kMT]),
				[GAM!F22 (Supp)]	= -
					CASE
					WHEN cu.[BenchSuppCap_kMT] = 0.0
					THEN
						0.0
					ELSE
						((bOpEx.[EnergyOpex] * (bEr.[SeparationEnergy_Supp_MBTU] * bEr.[EEI_PYPS] / 100.0) / cu.[BenchSuppCap_kMT] / bEr.[NetEnergyCons]) + (bOpEx.[STVol] - bOpEx.[EnergyOpex]) / cu.[BenchProdCap_kMT])
					END

			FROM @CapUtil										cu
			INNER JOIN	[$(DatabaseName)].[reports].[OPEX]			bOpEx
				ON	bOpEx.[GroupID]			= cu.[TargetId]
				AND	bOpEx.[DataType]		= 'ADJ'
				AND	bOpEx.[Currency]		= 'USD'
			INNER JOIN @OpExPlantCap							gPlantCap
				ON	gPlantCap.[GroupId]		= cu.[GroupId]
				AND	gPlantCap.[TargetId]	= cu.[TargetId]
				AND	gPlantCap.[CurrencyId]	= 'USD'
			INNER JOIN @YieldMargins							bYm
				ON	bYm.[GroupId]			= cu.[TargetId]
				AND	bYm.[DataType]			= @StandardCalc
			INNER JOIN @YieldMargins							cYm
				ON	cYm.[GroupId]			= cu.[TargetId]
				AND	cYm.[DataType]			= @TargetCountry
			INNER JOIN [$(DatabaseName)].[reports].[EnergyRaw]		bEr
				ON	bEr.[GroupID]			= cu.[TargetId]
				AND	bEr.[DataType]			= 'MBtu'
			WHERE	cu.[TargetId]			IS NOT NULL
			) t
			CROSS APPLY (VALUES
				('Pyro_Capacity',		COALESCE(t.[B107] * [GAM!D107], 0.0)),
				('Pyro_EnergyCap', 		0.0),
					('Pyro_Chemicals',		0.0),
					('Pyro_Catalysts',		0.0),
					('Pyro_NeUtilCost',		0.0),
					('Pyro_OtherVol',		0.0),
				('Pyro_PlantLinear',	0.0),
				('Pyro_PlantEos',		0.0),

				('Supp_Capacity',		COALESCE(t.[C107] * [GAM!D107], 0.0)
										+ COALESCE([PyroCpby_Pcnt] * [GAM!G18 (Pyro)], 0.0) + COALESCE([SuppCpby_Pcnt] * [GAM!G18 (Supp)], 0.0)
										+ COALESCE([PyroCpby_Pcnt] * [GAM!E22 (Pyro)], 0.0) + COALESCE([SuppCpby_Pcnt] * [GAM!F22 (Supp)], 0.0)
										),
				('Supp_EnergyCap', 		0.0),
					('Supp_Chemicals',		0.0),
					('Supp_Catalysts',		0.0),
					('Supp_NeUtilCost',		0.0),
					('Supp_OtherVol',		0.0),
				('Supp_PlantLinear',	0.0),
				('Supp_PlantEos',		0.0)
				) mlt([GapId], [DivAmount_Cur])
			ON	t.[GroupId]		= gar.[GroupId]
			AND	t.[TargetId]	= gar.[TargetId]
			AND	t.[Currency]	= gar.[CurrencyId]
			AND	mlt.[GapId]		= gar.[GapId]
		WHERE gar.[GapId] IN
			(
				'Pyro_Capacity', 'Pyro_EnergyCap', 'Pyro_Chemicals', 'Pyro_Catalysts', 'Pyro_NeUtilCost', 'Pyro_OtherVol', 'Pyro_PlantLinear', 'Pyro_PlantEos',
				'Supp_Capacity', 'Supp_EnergyCap', 'Supp_Chemicals', 'Supp_Catalysts', 'Supp_NeUtilCost', 'Supp_OtherVol', 'Supp_PlantLinear', 'Supp_PlantEos'
			)
			AND	gar.[FactorSetID] = @FactorSetId;

		-------------------------------------------------------------------
		--	Balance

		INSERT INTO [$(DbOlefins)].[gap].[AnalysisResults]([FactorSetId], [GroupId], [TargetId], [GroupProdCap_kMT], [BenchProdCap_kMT], [GapId], [CurrencyId], [DataType], [GapAmount_Cur], [DivAmount_Cur])
		SELECT
			@FactorSetId,
			cu.[GroupId],
			cu.[TargetId],
			cu.[GroupProdCap_kMT],
			cu.[BenchProdCap_kMT],
			x.[GapId],
			[CurrencyId]	= 'USD',
			[DataType]		= 'Balance',
			x.[GapAmount_Cur],
			x.[GapAmount_Div]
		FROM @CapUtil									cu
		INNER JOIN [$(DbOlefins)].[calc].[MarginAggregate]	ma
			ON	ma.[Refnum]			= cu.[GroupId]
			AND	ma.[MarginAnalysis]	= 'ProdHVC'
			AND	ma.[MarginID]		= 'MarginNetCash'
		INNER JOIN [$(DbOlefins)].[reports].[GENSUM]					g
			ON	g.[GroupID]			= cu.[GroupId]
		INNER JOIN [$(DbOlefins)].[gap].[AnalysisResultsAggregate]	gar
			ON	gar.[GroupId]		= cu.[GroupId]
			AND	gar.[TargetId]		= cu.[TargetId]
			AND	gar.[FactorSetID]	= ma.[FactorSetID]
			AND	gar.[GapId]			= 'ActualNetCashMargin'
		CROSS APPLY (VALUES
			('Pyro_Balance', cu.[GroupPyroCpby_Pcnt] * (ma.[Amount_Cur] - gar.[GapAmount_Cur]), cu.[GroupPyroCpby_Pcnt] * (g.[NCMPerHVCMT] - gar.[GapAmount_Div])),
			('Supp_Balance', cu.[GroupSuppCpby_Pcnt] * (ma.[Amount_Cur] - gar.[GapAmount_Cur]), cu.[GroupSuppCpby_Pcnt] * (g.[NCMPerHVCMT] - gar.[GapAmount_Div]))
			) x([GapId], [GapAmount_Cur], [GapAmount_Div])
		WHERE	ma.[FactorSetID]	= @FactorSetId;

	--END TRY
	--BEGIN CATCH
		
	--	SET @GroupId = 'G:' + @GroupId + '-T:' + @TargetId;
	--	EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

	--	RETURN ERROR_NUMBER();

	--END CATCH;

	--BEGIN TRY

		SET @ProcedureDesc = CHAR(9) + 'EXECUTE [$(DbOlefins)].[gap].[Delete_YieldMargin]';
		PRINT @ProcedureDesc;

		EXECUTE [$(DbOlefins)].[gap].[Delete_YieldMargin] @GroupId, @StandardCalc;
		EXECUTE [$(DbOlefins)].[gap].[Delete_YieldMargin] @TargetId, @StandardCalc;
		EXECUTE [$(DbOlefins)].[gap].[Delete_YieldMargin] @TargetId, @TargetCountry;

		INSERT INTO [$(DbOlefins)].[gap].[YieldMargins]([FactorSetId], [DataType], [GroupId], [FreshPyroFeed], [FeedSupp], [PlantFeed], [ProdLoss], [ProdSupp], [Recycle])
		SELECT
			@FactorSetId,
			ym.[DataType], ym.[GroupId], ym.[FreshPyroFeed], ym.[FeedSupp], ym.[PlantFeed], ym.[ProdLoss], ym.[ProdSupp], ym.[Recycle]
		FROM @YieldMargins ym;

	--END TRY
	--BEGIN CATCH
		
	--	SET @GroupId = 'G:' + @GroupId + '-T:' + @TargetId;
	--	EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @GroupId;

	--	RETURN ERROR_NUMBER();

	--END CATCH;

END;