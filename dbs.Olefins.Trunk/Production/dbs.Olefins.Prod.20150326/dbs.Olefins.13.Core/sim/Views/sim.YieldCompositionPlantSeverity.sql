﻿CREATE VIEW [sim].[YieldCompositionPlantSeverity]
WITH SCHEMABINDING
AS
SELECT
	p.FactorSetId,
	p.SimModelId,
	p.Refnum,
	p.CalDateKey,
	p.RecycleId,
	p.MS25,
	p.OS25,
	CONVERT(REAL, CASE WHEN p.MS25 <> 0.0
		THEN p.OS25 / p.MS25 * 100.0
		END, 1)									[YieldPlantSeverity]
FROM (
	SELECT
		c.FactorSetId,
		c.SimModelId,
		c.Refnum,
		c.CalDateKey,
		c.RecycleId,
		c.OpCondId,
		c.ComponentId,
		c.Component_WtPcnt
	FROM sim.[YieldCompositionPlant] c
	WHERE	c.OpCondId IN ('MS25', 'OS25')
		AND	c.ComponentId = 'C2H4'
	) u
	PIVOT(
	MAX(u.Component_WtPcnt) FOR u.OpCondId IN (
		[OS25],
		[MS25]
		)
	) p;