﻿CREATE PROCEDURE [sim].[Insert_SimulationData]
(
	@Refnum			VARCHAR(25)	= NULL,
	@SimModelId		VARCHAR(12)	= NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';
	PRINT @ProcedureDesc;

	SET NOCOUNT ON;

	BEGIN TRY

		SET @Refnum = RTRIM(LTRIM(@Refnum));

		IF( NOT EXISTS (SELECT TOP 1 1 FROM [sim].[YieldCompositionPlant] s		WHERE s.Refnum = @Refnum AND s.SimModelId = @SimModelId) 
		AND	NOT	EXISTS (SELECT TOP 1 1 FROM [sim].[YieldCompositionStream] s	WHERE s.Refnum = @Refnum AND s.SimModelId = @SimModelId)
		AND	NOT EXISTS (SELECT TOP 1 1 FROM [sim].[EnergyConsumptionPlant] s	WHERE s.Refnum = @Refnum AND s.SimModelId = @SimModelId)
		AND	NOT EXISTS (SELECT TOP 1 1 FROM [sim].[EnergyConsumptionStream] s	WHERE s.Refnum = @Refnum AND s.SimModelId = @SimModelId))
		BEGIN

			DECLARE @Multiplier	FLOAT = 0.0
			DECLARE @RecycleId	TINYINT = 0;
			DECLARE @sRefnum	VARCHAR(9) = RIGHT(@Refnum, LEN(@Refnum) - 2);

			SELECT
				@RecycleId = 
				SUM(CASE q.FeedProdID
					WHEN 'EthRec' THEN 1
					WHEN 'ProRec' THEN 2
					WHEN 'ButRec' THEN 4 
					ELSE 0
					END)
			FROM stgFact.Quantity	 q
			WHERE	q.FeedProdID IN ('EthRec', 'ProRec', 'ButRec')
				AND	q.AnnFeedProd > 0.0
				AND	q.Refnum = @sRefnum
			GROUP BY
				q.Refnum

			INSERT INTO sim.EnergyConsumptionStream(QueueID, FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, StreamDescription, RecycleId, ErrorID, SimEnergy_kCalkg)
			SELECT
				1,
				fsisy.FactorSetId,
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
				etl.ConvDateKey(t.StudyYear),
				ecs.SimModelId,
				ecs.OpCondId,
				etl.ConvStreamID(q.FeedProdID),
				etl.ConvStreamDescription(q.FeedProdID, f.OthLiqFeedDESC, q.MiscProd1, q.MiscProd2, q.MiscFeed),
				@RecycleId,
				0,
				ecs.SimEnergy_kCalkg
			FROM stgFact.EnergyConsumptionStream	ecs
			INNER JOIN stgFact.Quantity				q
				ON q.Refnum = ecs.Refnum
				AND	q.FeedProdID = ecs.StreamId
			LEFT OUTER JOIN stgFact.FeedQuality		f
				ON	f.Refnum = ecs.Refnum
				AND f.FeedProdID = q.FeedProdID
			INNER JOIN stgFact.TSort				t
				ON	t.Refnum = ecs.Refnum
				AND t.Refnum = @sRefnum
			INNER JOIN ante.FactorSetsInStudyYear	fsisy
				ON	 fsisy._StudyYear = t.StudyYear
			WHERE	ecs.SimModelId = @SimModelId;

			INSERT INTO sim.EnergyConsumptionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, Energy_kCalC2H4)
			SELECT
				fsisy.FactorSetId,
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
				etl.ConvDateKey(t.StudyYear),
				ecp.SimModelId,
				ecp.OpCondId,
				@RecycleId,
				ecp.SimEnergy_kCalkg
			FROM stgFact.EnergyConsumptionPlant		ecp
			INNER JOIN stgFact.TSort				t
				ON	t.Refnum = ecp.Refnum
				AND t.Refnum = @sRefnum
			INNER JOIN ante.FactorSetsInStudyYear	fsisy
				ON	 fsisy._StudyYear = t.StudyYear
			WHERE	ecp.SimModelId = @SimModelId;

			SELECT 
				@Multiplier = CASE WHEN MAX(ycs.Component_WtPcnt) <= 1.0 THEN 100.0 ELSE 1.0 END
			FROM stgFact.YieldCompositionStream		ycs
			INNER JOIN stgFact.TSort				t
				ON	t.Refnum = ycs.Refnum
				AND t.Refnum = @sRefnum
			WHERE	ycs.SimModelId = @SimModelId;

			INSERT INTO sim.YieldCompositionStream(QueueID, FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, StreamId, StreamDescription, RecycleId, ComponentId, Component_WtPcnt)
			SELECT
				1,
				fsisy.FactorSetId,
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
				etl.ConvDateKey(t.StudyYear),
				ycs.SimModelId,
				ycs.OpCondId,
				etl.ConvStreamID(q.FeedProdID),
				etl.ConvStreamDescription(q.FeedProdID, f.OthLiqFeedDESC, q.MiscProd1, q.MiscProd2, q.MiscFeed),
				@RecycleId,
				ycs.ComponentId,
				ycs.Component_WtPcnt * @Multiplier
			FROM stgFact.YieldCompositionStream		ycs
			INNER JOIN stgFact.Quantity				q
				ON q.Refnum = ycs.Refnum
				AND	q.FeedProdID = ycs.StreamId
			LEFT OUTER JOIN stgFact.FeedQuality		f
				ON	f.Refnum = ycs.Refnum
				AND f.FeedProdID = q.FeedProdID
			INNER JOIN stgFact.TSort				t
				ON	t.Refnum = ycs.Refnum
				AND t.Refnum = @sRefnum
			INNER JOIN ante.FactorSetsInStudyYear	fsisy
				ON	 fsisy._StudyYear = t.StudyYear
			WHERE	ycs.SimModelId = @SimModelId;

			SELECT 
				@Multiplier = CASE WHEN MAX(ycp.Component_WtPcnt) <= 1.0 THEN 100.0 ELSE 1.0 END
			FROM stgFact.YieldCompositionPlant		ycp
			INNER JOIN stgFact.TSort				t
				ON	t.Refnum = ycp.Refnum
				AND t.Refnum = @sRefnum
			WHERE	ycp.SimModelId = @SimModelId;

			INSERT INTO sim.YieldCompositionPlant(FactorSetId, Refnum, CalDateKey, SimModelId, OpCondId, RecycleId, ComponentId, Component_WtPcnt, Component_kMT)
			SELECT
				fsisy.FactorSetId,
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH')		[Refnum],
				etl.ConvDateKey(t.StudyYear),
				ycp.SimModelId,
				ycp.OpCondId,
				@RecycleId,
				ycp.ComponentId,
				ycp.Component_WtPcnt * @Multiplier,
				ycp.Component_WtPcnt * @Multiplier * SUM(q.AnnFeedProd) / 100.0
			FROM stgFact.YieldCompositionPlant		ycp
			INNER JOIN stgFact.Quantity				q
				ON	q.Refnum = ycp.Refnum
			INNER JOIN stgFact.TSort				t
				ON	t.Refnum = ycp.Refnum
				AND t.Refnum = @sRefnum
			INNER JOIN ante.FactorSetsInStudyYear	fsisy
				ON	 fsisy._StudyYear = t.StudyYear
			WHERE	etl.ConvStreamID(q.FeedProdID) IN (SELECT d.DescendantId FROM dim.Stream_Bridge d WHERE d.FactorSetId = fsisy.FactorSetId AND d.StreamId IN ('Light', 'Recycle', 'Liquid'))
				AND	ycp.SimModelId = @SimModelId
			GROUP BY
				fsisy.FactorSetId,
				etl.ConvRefnum(t.Refnum, t.StudyYear, 'PCH'),
				etl.ConvDateKey(t.StudyYear),
				ycp.SimModelId,
				ycp.OpCondId,
				ycp.ComponentId,
				ycp.Component_WtPcnt;
		
		END;

	END TRY
	BEGIN CATCH

		EXECUTE dbo.usp_LogError 0, @ProcedureDesc, @Refnum;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
