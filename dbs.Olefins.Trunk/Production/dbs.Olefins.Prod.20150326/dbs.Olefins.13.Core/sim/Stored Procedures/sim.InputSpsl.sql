﻿CREATE PROCEDURE sim.InputSpsl
(	
	@Refnum			VARCHAR(25),
	@FactorSetId	VARCHAR(12)
)
AS
BEGIN

	CREATE TABLE #CompositionModel
	(
		[Refnum]				VARCHAR (18)	NOT	NULL,
		[CalDateKey]			INT				NOT	NULL,
		[StreamId]				VARCHAR(42)		NOT	NULL,
		[StreamDescription]		NVARCHAR(256)	NOT	NULL,
		[FeedConvID]			VARCHAR(42)			NULL,
		[51]					REAL				NULL,
		[52]					REAL				NULL,
		[54]					REAL				NULL,
		[55]					REAL				NULL,
		[57]					REAL				NULL,
		[58]					REAL				NULL,
		[59]					REAL				NULL,
		[60]					REAL				NULL,
		[61]					REAL				NULL,
		[62]					REAL				NULL,
		[63]					REAL				NULL,
		[64]					REAL				NULL,
		[71]					REAL				NULL,
		[72]					REAL				NULL,
		[73]					REAL				NULL,
		[74]					REAL				NULL,
		[75]					REAL				NULL,
		[95]					REAL				NULL,
		[177]					REAL				NULL,
		[178]					REAL				NULL,
		[200]					REAL				NULL,
		PRIMARY KEY CLUSTERED([Refnum] DESC, [StreamId] ASC, [StreamDescription] ASC)
	)

	--INSERT INTO #CompositionModel(Refnum, CalDateKey, StreamId, StreamDescription, FeedConvID, [51], [52], [54], [55], [57], [58], [59], [60], [61], [62], [63], [64], [71], [72], [73], [74], [75], [95], [177], [178], [200])
	--SELECT
	--	p.Refnum,
	--	p.CalDateKey,
	--	p.StreamId,
	--	p.StreamDescription,
	--	p.FeedConvID,
	--	p.[51],
	--	p.[52],
	--	p.[54],
	--	p.[55],
	--	p.[57],
	--	p.[58],
	--	p.[59],
	--	p.[60],
	--	(ISNULL(p.[61], 0.0) + ISNULL(p.[63], 0.0)) / 2.0		[61],
	--	p.[62],
	--	(ISNULL(p.[61], 0.0) + ISNULL(p.[63], 0.0)) / 2.0		[63],
	--	p.[64],
	--	p.[71],
	--	p.[72],
	--	p.[73],
	--	p.[74],
	--	p.[75],
	--	p.[95],
	--	(ISNULL(p.[177], 0.0) + ISNULL(p.[178], 0.0)) / 2.0	[177],
	--	(ISNULL(p.[177], 0.0) + ISNULL(p.[178], 0.0)) / 2.0	[178],
	--	p.[250] * 100.0	 [200]		-- Need to convert component Number
	--FROM (
	--	SELECT
	--		c.Refnum,
	--		c.CalDateKey,
	--		c.StreamId,
	--		c.StreamDescription,
	--		l.SPSL + 50										[Spsl_Field],
	--		c.Component_WtPcnt	/ 100.0	[Component_WtPcnt],
	--		MAX(CASE WHEN c.SPSLComposition_Rank = 1 THEN l.SPSL END) OVER (PARTITION BY
	--			c.Refnum,
	--			c.CalDateKey,
	--			c.StreamId,
	--			c.StreamDescription
	--			)											[FeedConvID]
	--	--FROM dim.StreamLuDescendants('Light|Recycle|Liquid', 0, '|')	d
	--	FROM dim.Stream_Bridge									b
	--	INNER JOIN fact.CompositionQuantityAggregate			c
	--		ON	c.FactorSetId = b.FactorSetId
	--		AND	c.StreamId = b.DescendantId
	--		AND	c.ComponentId <> 'Tot'
	--	--INNER JOIN dim.Component_LookUp							l
	--	--	ON	l.ComponentId = c.ComponentId
	--	--	AND l.SPSL IS NOT NULL
	--	WHERE	c.Refnum = @Refnum
	--		AND	b.FactorSetId = @FactorSetId
	--		AND	b.StreamId IN ('Light', 'Recycle', 'Liquid')
	--	) u
	--	PIVOT(
	--	MAX(u.Component_WtPcnt) FOR u.Spsl_Field IN (
	--		[51], [52],		  [54], [55],		  [57], [58], [59], [60],
	--		[61], [62], [63], [64],
	--		[71], [72], [73], [74], [75],
	--		[95], [177], [178],
	--		[250]
	--		)
	--	) p;

	CREATE TABLE #DistallationModel
	(
		[Refnum]				VARCHAR (18)	NOT	NULL,
		[CalDateKey]			INT				NOT	NULL,
		[StreamId]				VARCHAR(42)		NOT	NULL,
		[StreamDescription]		NVARCHAR(256)	NOT	NULL,
		[Density_SG]			REAL				NULL,
		[D000]					REAL				NULL,
		[D005]					REAL				NULL,
		[D010]					REAL				NULL,
		[D030]					REAL				NULL,
		[D050]					REAL				NULL,
		[D070]					REAL				NULL,
		[D090]					REAL				NULL,
		[D095]					REAL				NULL,
		[D100]					REAL				NULL,
		PRIMARY KEY CLUSTERED([Refnum] DESC, [StreamId] ASC, [StreamDescription] ASC)
	)

	INSERT INTO #DistallationModel(Refnum, CalDateKey, StreamId, StreamDescription, Density_SG, D000, D005, D010, D030, D050, D070, D090, D095, D100)
	SELECT
		d.Refnum,
		d.CalDateKey,
		d.StreamId,
		d.StreamDescription,
		d.Density_SG,
		d.D000,
		d.D005,
		d.D010,
		d.D030,
		d.D050,
		d.D070,
		d.D090,
		d.D095,
		d.D100
	FROM sim.Distillation(@FactorSetId, @Refnum)	d;
	
	SELECT
		fConfig.FactorSetId,
		q.Refnum,
		q.CalDateKey,
		q.StreamId,
		q.StreamDescription,
		sKrak.OpCondId,

		0																	[RecycleType],
		0																	[RecycleId],
		CONVERT(BIT, ISNULL(s.Active, 0))									[Supersede_Bit],

		fConfig.FurnaceTypeId,

		ISNULL(s.[1], 1)													[1],	-- Calculation Mode
		ISNULL(s.[2], 99)													[2],	-- Error log file unit number
		ISNULL(s.[3], 60)													[3],	-- Integration steps along tubes
		ISNULL(s.[4], 1)													[4],	-- IGEOM Number (Reserved)
		ISNULL(s.[6], 0.00001)												[6],	-- Tolerance

		ISNULL(s.[9], 1) * COALESCE(s.[7], sKrak.CoilOutletPressure_kgcm2, [$(DbGlobal)].dbo.UnitsConv(pKrak.CoilOutletPressure_Psia, 'PSIA', 'KGM2A'), fConfig.CoilOutletPressure_kgcm2, fType.CoilOutletPressure_kgcm2)
																			[7],	-- Coil Outlet Pressure (kg/cm2)
		ISNULL(s.[8], sim.SpslConvergence(8, pKrak.FeedConv_WtPcnt, sComp.FeedConvID, pKrak.PropyleneEthylene_Ratio, pKrak.PropyleneMethane_Ratio, COALESCE(sKrak.CoilOutletTemp_C, pKrak.CoilOutletTemp_C, fConfig.CoilOutletTemp_C, fType.CoilOutletTemp_C)))
																			[8],	-- Severity Conversion Factor
		ISNULL(s.[9], 1)													[9],	-- Type of Pressure Convergence for  (If = 1 Then set Item 7 Else Set Item 13)
		ISNULL(s.[10], sim.SpslConvergence(10, pKrak.FeedConv_WtPcnt, sComp.FeedConvID, pKrak.PropyleneEthylene_Ratio, pKrak.PropyleneMethane_Ratio, COALESCE(sKrak.CoilOutletTemp_C, pKrak.CoilOutletTemp_C, fConfig.CoilOutletTemp_C, fType.CoilOutletTemp_C)))
																			[10],	-- Severity Conversion Factor
		COALESCE(s.[11], sKrak.FlowRate_KgHr, pKrak.FlowRate_KgHr, fConfig.FlowRate_KgHr, fType.FlowRate_KgHr)
																			[11],	-- Flow Rate

		COALESCE(s.[12], sKrak.SteamHydrocarbon_Ratio, pKrak.SteamHydrocarbon_Ratio, fConfig.SteamHydrocarbon_Ratio, fType.SteamHydrocarbon_Ratio)
																			[12],	-- SteamHydrocarbon_Ratio

		COALESCE(s.[13], sKrak.CoilInletPressure_kgcm2, [$(DbGlobal)].dbo.UnitsConv(pKrak.CoilInletPressure_Psia, 'PSIA', 'KGM2A'), fConfig.CoilInletPressure_kgcm2, fType.CoilInletPressure_kgcm2,
		COALESCE(s.[7], sKrak.CoilOutletPressure_kgcm2, [$(DbGlobal)].dbo.UnitsConv(pKrak.CoilOutletPressure_Psia, 'PSIA', 'KGM2A'), fConfig.CoilOutletPressure_kgcm2, fType.CoilOutletPressure_kgcm2) + 1.5)
																			[13],	-- Coil Inlet Pressure (kg/cm2)

		COALESCE(s.[14], sKrak.RadiantWallTemp_C, pKrak.RadiantWallTemp_C, fConfig.RadiantWallTemp_C, fType.RadiantWallTemp_C)
																			[14],	-- Default Radiant Wall Temperature
		COALESCE(s.[15], sKrak.CoilInletTemp_C, pKrak.CoilInletTemp_C, fConfig.CoilInletTemp_C, fType.CoilInletTemp_C)
																			[15],	-- Coil Inlet Temperature
		
		COALESCE(s.[51], sComp.[51])										[51],	-- Hydrogen				(H2)
		COALESCE(s.[52], sComp.[52])										[52],	-- Methane				(CH4)
		COALESCE(s.[54], sComp.[54])										[54],	-- Ethylene				(C2H4)
		COALESCE(s.[55], sComp.[55])										[55],	-- Ethane				(C2H6)
		COALESCE(s.[57], sComp.[57])										[57],	-- Propylene			(C3H6)
		COALESCE(s.[58], sComp.[58])										[58],	-- Propane				(C3H8)
		COALESCE(s.[59], sComp.[59])										[59],	-- N-Butane				(NBUTA)
		COALESCE(s.[60], sComp.[60])										[60],	-- Iso-Butane			(IBUTA)
		COALESCE(s.[61], sComp.[61])										[61],	-- Iso-Butene			(IB)
		COALESCE(s.[62], sComp.[62])										[62],	-- 1-Butene/N-Butene	(B1)
		COALESCE(s.[63], sComp.[63])										[63],	-- 2-Butene				(B2)
		COALESCE(s.[64], sComp.[64])										[64],	-- Butadiene			(BUTAD)
		COALESCE(s.[71], sComp.[71])										[71],	-- N-Pentane			(NC5)
		COALESCE(s.[72], sComp.[72])										[72],	-- Iso-Pentane			(IC5)
		COALESCE(s.[73], sComp.[73])										[73],	-- N-Hexane				(NC6)
		COALESCE(s.[74], sComp.[74])										[74],	-- N-Heptane			(C7H16)	--	20121203	NC7 
		COALESCE(s.[75], sComp.[75])										[75],	-- N-Octane				(C8H18)	--	20121203	NC8
		COALESCE(s.[95], sComp.[95])										[95],	-- Iso-Hexane			(C6ISO)
		COALESCE(s.[177], sComp.[177])										[177],	-- Carbon Monoxide		(CO)
		COALESCE(s.[178], sComp.[178])										[178],	-- Carbon Dioxide		(CO2)

		/*	79, 86, 160, 174 are used for Liquid C5	*/
		s.[79]																[79],	-- Cyclo-Pentanet		(C5H10)
		s.[86]																[86],	-- 1-Pentene			(C5H10)
		s.[160]																[160],	-- 2-Pentene			(C5H10)
		s.[174]																[174],	-- Methyl 1-Pentenes	(C6H12)	

		COALESCE(s.[187], sDist.D000)										[187],	-- Initial Boiling Point
		COALESCE(s.[188], sDist.D010)										[188],	-- ASTM 10% (°C) (GW)
		COALESCE(s.[189], sDist.D030)										[189],	-- ASTM 30% (°C) (GW)
		COALESCE(s.[190], sDist.D050)										[190],	-- ASTM 50% (°C) (GW)
		COALESCE(s.[191], sDist.D070)										[191],	-- ASTM 70% (°C) (GW)
		COALESCE(s.[192], sDist.D090)										[192],	-- ASTM 90% (°C) (GW)
		COALESCE(s.[193], sDist.D100)										[193],	-- Final Boiling Point

		COALESCE(s.[194], pPiano.P, sPiano.P)								[194],	-- Normal-Paraffins,
		COALESCE(s.[195], pPiano.I, sPiano.I)								[195],	-- Iso-Paraffins,
		COALESCE(s.[196], pPiano.N, sPiano.N)								[196],	-- Naphthenes,
		COALESCE(s.[197], pPiano.A, sPiano.A)								[197],	-- Aromatics,

		COALESCE(s.[198], sDist.Density_SG)									[198],	-- Density (Specific Gravity) (NGW)
		COALESCE(s.[199], -2.115 * sDist.Density_SG + 3.6538, fConfig.HydrogenCarbon_Ratio)
																			[199],	-- H to C ratio (molar basis) (GW)

		ISNULL(s.[200] / 10000.0, sComp.[200]) 								[200],	-- Sulfur Content (wt% basis) (GW)
		s.[202]																[202],	-- Total C4 (wt% basis) (N)
		ISNULL(s.[204], 1)													[204]	-- Volume (0) or weight(1) percent for the PINA specification (NGW)

	FROM fact.QuantityPivot										q
	INNER JOIN fact.FeedStockCrackingParameters					pKrak
		ON	pKrak.Refnum = q.Refnum
		AND pKrak.CalDateKey = q.CalDateKey
		AND	pKrak.StreamId = q.StreamId
		AND pKrak.StreamDescription = q.StreamDescription

	INNER JOIN ante.SimFurnaceConfigSpsl						fConfig
		ON	fConfig.StreamId = q.StreamId
	LEFT OUTER JOIN dim.SimFurnaceType_LookUp					fType
		ON	fType.FurnaceTypeId = fConfig.FurnaceTypeId

	INNER JOIN ante.SimCrackingParameters						sKrak
		ON	sKrak.FactorSetId = fConfig.FactorSetId
		AND	ISNULL(sKrak.StreamId, fConfig.StreamId) = fConfig.StreamId

	LEFT OUTER JOIN #CompositionModel							sComp
		ON	sComp.Refnum = q.Refnum
		AND	sComp.CalDateKey = q.CalDateKey
		AND	sComp.StreamId = q.StreamId
		AND	sComp.StreamDescription = q.StreamDescription

	LEFT OUTER JOIN #DistallationModel							sDist
		ON	sDist.Refnum = q.Refnum
		AND	sDist.CalDateKey = q.CalDateKey
		AND	sDist.StreamId = q.StreamId
		AND	sDist.StreamDescription = q.StreamDescription

	LEFT OUTER JOIN(
			SELECT
				p.FactorSetId,
				p.StreamId,
				p.ComponentId,
				(p.Component_WtPcnt +
				CASE p.ComponentId
					WHEN 'P' THEN 0.0
					WHEN 'I' THEN 0.0
					WHEN 'N' THEN 0.9
					WHEN 'A' THEN 0.1
					END * o.Component_WtPcnt)				[Component_WtPcnt]
			FROM ante.SimNormalizationPiano			p
			INNER JOIN ante.SimNormalizationPiano	o
				ON	o.FactorSetId = p.FactorSetId
				AND	o.StreamId = p.StreamId
				AND	o.ComponentId = 'O'
			WHERE p.ComponentId <> 'O'
			) p
			PIVOT(
			MAX(p.Component_WtPcnt) FOR p.ComponentId IN (P, I, N, A)
		)														sPiano
		ON	sPiano.FactorSetId = fConfig.FactorSetId
		AND	sPiano.StreamId = q.StreamId

	LEFT OUTER JOIN(
			SELECT
				p.Refnum,
				p.StreamId,
				p.ComponentId,
				p.Component_WtPcnt
			FROM fact.CompositionQuantity			p
			WHERE p.ComponentId <> 'O'
			) p
			PIVOT(
			MAX(p.Component_WtPcnt) FOR p.ComponentId IN (P, I, N, A)
		)														pPiano
		ON	pPiano.Refnum = q.Refnum
		AND	pPiano.StreamId = q.StreamId

	LEFT OUTER JOIN(
		SELECT
			s.FieldId,
			s.Value,
			s.Active,
			
			s.FactorSetId,
			s.OpCondId,
			
			s.Refnum,
			s.CalDateKey,
			s.StreamId,
			s.StreamDescription
		FROM sim.Supersede s
		WHERE	s.DataSetId = 'Input'
			AND	s.SimModelId = 'PYPS'
			AND	s.Active = 1
		) u
		PIVOT (
		MAX(u.Value) FOR u.FieldId IN (
			[1], [2], [3], [4], [5], [6],
			[7], [8], [9], [10],
			[11], [12], [13], [14], [15],
			[51], [52], [54], [55], [57], [58], [59], [60], [61], [62], [63], [64],
			[71], [72], [73], [74], [75], [79], [86], [95],
			[160], [174], [177], [178],
			
			[187], [188], [189], [190], [191], [192], [193],
			[194], [195], [196], [197],
			
			[198], [199], [200], [204],
			[202],
			[SevInd]
			)
		) s
		ON	ISNULL(s.Refnum, q.Refnum) = q.Refnum
		AND ISNULL(s.CalDateKey, q.CalDateKey) = q.CalDateKey
		AND	ISNULL(s.StreamId, q.StreamId) = q.StreamId
		AND	ISNULL(s.StreamDescription, q.StreamDescription) = q.StreamDescription
		AND	s.FactorSetId = fConfig.FactorSetId
		AND ISNULL(s.OpCondId, sKrak.OpCondId) = sKrak.OpCondId

	WHERE	q.Refnum = @Refnum
		AND	fConfig.FactorSetId = @FactorSetId

	DROP TABLE #CompositionModel;
	DROP TABLE #DistallationModel;

END