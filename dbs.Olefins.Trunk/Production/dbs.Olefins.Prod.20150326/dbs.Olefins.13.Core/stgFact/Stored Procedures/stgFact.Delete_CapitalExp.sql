﻿CREATE PROCEDURE [stgFact].[Delete_CapitalExp]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[CapitalExp]
	WHERE [Refnum] = @Refnum;

END;