﻿CREATE PROCEDURE [stgFact].[Delete_Misc]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Misc]
	WHERE [Refnum] = @Refnum;

END;