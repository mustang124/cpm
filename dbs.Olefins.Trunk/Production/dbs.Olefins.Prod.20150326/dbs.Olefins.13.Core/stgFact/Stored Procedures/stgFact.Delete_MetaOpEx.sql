﻿CREATE PROCEDURE [stgFact].[Delete_MetaOpEx]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[MetaOpEx]
	WHERE [Refnum] = @Refnum;

END;