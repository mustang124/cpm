﻿CREATE PROCEDURE [stgFact].[Insert_TSort]
(
	@Refnum					VARCHAR (25),

	@Co						VARCHAR (40)	= NULL,
	@Loc					VARCHAR (40)	= NULL,
	@CoLoc					VARCHAR (60)	= NULL,
	@CoName					VARCHAR (100)	= NULL,
	@PlantName				VARCHAR (60)	= NULL,
	@PlantLoc				VARCHAR (40)	= NULL,
	@CoordName				VARCHAR (40)	= NULL,
	@CoordTitle				VARCHAR (75)	= NULL,
	@POBox					VARCHAR (35)	= NULL,
	@Street					VARCHAR (60)	= NULL,
	@City					VARCHAR (40)	= NULL,
	@State					VARCHAR (25)	= NULL,
	@Country				VARCHAR (30)	= NULL,
	@Zip					VARCHAR (20)	= NULL,
	@Telephone				VARCHAR (30)	= NULL,
	@Fax					VARCHAR (30)	= NULL,
	@WWW					VARCHAR (65)	= NULL,
	@PricingContact			VARCHAR (40)	= NULL,
	@PricingContactEmail	VARCHAR (65)	= NULL,
	@DCContact				VARCHAR (40)	= NULL,
	@DCContactEmail			VARCHAR (65)	= NULL,
	@UOM					CHAR (1)		= NULL,
	@PlantCountry			VARCHAR (20)	= NULL,
	@Region					VARCHAR (10)	= NULL,
	@CapPeerGrp				TINYINT			= NULL,
	@TechPeerGrp			TINYINT			= NULL,
	@FeedClass				TINYINT			= NULL,
	@EDCPeerGrp				TINYINT			= NULL,
	@LocFactor				REAL			= NULL,
	@InflFactor				REAL			= NULL,
	@CompanyId				VARCHAR (25)	= NULL,
	@ContactCode			VARCHAR (25)	= NULL,
	@StudyYear				SMALLINT		= NULL,
	@RefNumber				VARCHAR (4)		= NULL,
	@PresLabel				VARCHAR (3)		= NULL,
	@Energy					TINYINT			= NULL,
	@Polymer				TINYINT			= NULL,
	@CogenPeerGrp			VARCHAR (10)	= NULL,
	@SharedPeerGrp			VARCHAR (20)	= NULL,
	@Password				VARCHAR (20)	= NULL,
	@RefDirectory			VARCHAR (100)	= NULL,
	@Consultant				VARCHAR (5)		= NULL,
	@Comments				VARCHAR (255)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	/*
	Reserved column names: State, Password
	*/

	INSERT INTO [stgFact].[TSort]([Refnum], [Co], [Loc], [CoLoc], [CoName], [PlantName], [PlantLoc], [CoordName], [CoordTitle], [POBox], [Street], [City], [State], [Country], [Zip], [Telephone], [Fax], [WWW], [PricingContact], [PricingContactEmail], [DCContact], [DCContactEmail], [UOM], [PlantCountry], [Region], [CapPeerGrp], [TechPeerGrp], [FeedClass], [EDCPeerGrp], [LocFactor], [InflFactor], [CompanyId], [ContactCode], [StudyYear], [RefNumber], [PresLabel], [Energy], [Polymer], [CogenPeerGrp], [SharedPeerGrp], [Password], [RefDirectory], [Consultant], [Comments])
	VALUES (@Refnum, @Co, @Loc, @CoLoc, @CoName, @PlantName, @PlantLoc, @CoordName, @CoordTitle, @POBox, @Street, @City, @State, @Country, @Zip, @Telephone, @Fax, @WWW, @PricingContact, @PricingContactEmail, @DCContact, @DCContactEmail, @UOM, @PlantCountry, @Region, @CapPeerGrp, @TechPeerGrp, @FeedClass, @EDCPeerGrp, @LocFactor, @InflFactor, @CompanyId, @ContactCode, @StudyYear, @RefNumber, @PresLabel, @Energy, @Polymer, @CogenPeerGrp, @SharedPeerGrp, @Password, @RefDirectory, @Consultant, @Comments);

END;