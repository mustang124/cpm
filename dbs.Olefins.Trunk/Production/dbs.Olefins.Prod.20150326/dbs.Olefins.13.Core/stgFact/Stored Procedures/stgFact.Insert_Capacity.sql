﻿CREATE PROCEDURE [stgFact].[Insert_Capacity]
(
	@Refnum					VARCHAR (25),

	@EthylCapKMTA			REAL			= NULL,
	@EthylCapMTD			REAL			= NULL,
	@EthylMaxCapMTD			REAL			= NULL,
	@PropylCapKMTA			REAL			= NULL,
	@OlefinsCapMTD			REAL			= NULL,
	@OlefinsMaxCapMTD		REAL			= NULL,
	@FurnaceCapKMTA			REAL			= NULL,
	@RecycleCapKMTA			REAL			= NULL,
	@SvcFactor				REAL			= NULL,
	@ExpansionPcnt			REAL			= NULL,
	@ExpansionDate			DATETIME		= NULL,
	@CapAllow				CHAR (1)		= NULL,
	@CapLossPcnt			REAL			= NULL,
	@PlantStartupDate		SMALLINT		= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[Capacity]([Refnum], [EthylCapKMTA], [EthylCapMTD], [EthylMaxCapMTD], [PropylCapKMTA], [OlefinsCapMTD], [OlefinsMaxCapMTD], [FurnaceCapKMTA], [RecycleCapKMTA], [SvcFactor], [ExpansionPcnt], [ExpansionDate], [CapAllow], [CapLossPcnt], [PlantStartupDate])
	VALUES(@Refnum, @EthylCapKMTA, @EthylCapMTD, @EthylMaxCapMTD, @PropylCapKMTA, @OlefinsCapMTD, @OlefinsMaxCapMTD, @FurnaceCapKMTA, @RecycleCapKMTA, @SvcFactor, @ExpansionPcnt, @ExpansionDate, @CapAllow, @CapLossPcnt, @PlantStartupDate);

END;