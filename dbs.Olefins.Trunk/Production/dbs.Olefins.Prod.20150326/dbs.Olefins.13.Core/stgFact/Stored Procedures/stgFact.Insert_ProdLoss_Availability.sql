﻿CREATE PROCEDURE [stgFact].[Insert_ProdLoss_Availability]
(
	@Refnum      VARCHAR (25),
	@Category    VARCHAR (3),
	@CauseID     VARCHAR (20),

	@Description VARCHAR (250) = NULL,
	@PrevDTLoss  REAL          = NULL,
	@PrevSDLoss  REAL          = NULL,
	@DTLoss      REAL          = NULL,
	@SDLoss      REAL          = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[ProdLoss_Availability]([Refnum], [Category], [CauseID], [Description], [PrevDTLoss], [PrevSDLoss], [DTLoss], [SDLoss])
	VALUES(@Refnum, @Category, @CauseID, @Description, @PrevDTLoss, @PrevSDLoss, @DTLoss, @SDLoss);

END;