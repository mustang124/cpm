﻿CREATE PROCEDURE [stgFact].[Delete_FeedSelections]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[FeedSelections]
	WHERE [Refnum] = @Refnum;

END;