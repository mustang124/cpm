﻿CREATE TABLE [stgFact].[TSort] (
    [Refnum]              VARCHAR (25)       NOT NULL,
    [Co]                  VARCHAR (40)       NULL,
    [Loc]                 VARCHAR (40)       NULL,
    [CoLoc]               VARCHAR (60)       NULL,
    [CoName]              VARCHAR (100)      NULL,
    [PlantName]           VARCHAR (60)       NULL,
    [PlantLoc]            VARCHAR (40)       NULL,
    [CoordName]           VARCHAR (40)       NULL,
    [CoordTitle]          VARCHAR (75)       NULL,
    [POBox]               VARCHAR (35)       NULL,
    [Street]              VARCHAR (60)       NULL,
    [City]                VARCHAR (40)       NULL,
    [State]               VARCHAR (25)       NULL,
    [Country]             VARCHAR (30)       NULL,
    [Zip]                 VARCHAR (20)       NULL,
    [Telephone]           VARCHAR (30)       NULL,
    [Fax]                 VARCHAR (30)       NULL,
    [WWW]                 VARCHAR (65)       NULL,
    [PricingContact]      VARCHAR (40)       NULL,
    [PricingContactEmail] VARCHAR (65)       NULL,
    [DCContact]           VARCHAR (40)       NULL,
    [DCContactEmail]      VARCHAR (65)       NULL,
    [UOM]                 CHAR (1)           NULL,
    [PlantCountry]        VARCHAR (20)       NULL,
    [Region]              VARCHAR (10)       NULL,
    [CapPeerGrp]          TINYINT            NULL,
    [TechPeerGrp]         TINYINT            NULL,
    [FeedClass]           TINYINT            NULL,
    [EDCPeerGrp]          TINYINT            NULL,
    [LocFactor]           REAL               NULL,
    [InflFactor]          REAL               NULL,
    [CompanyId]           VARCHAR (25)       NULL,
    [ContactCode]         VARCHAR (25)       NULL,
    [StudyYear]           SMALLINT           NULL,
    [RefNumber]           VARCHAR (4)        NULL,
    [PresLabel]           VARCHAR (3)        NULL,
    [Energy]              TINYINT            NULL,
    [Polymer]             TINYINT            NULL,
    [CogenPeerGrp]        VARCHAR (10)       NULL,
    [SharedPeerGrp]       VARCHAR (20)       NULL,
    [Password]            VARCHAR (20)       NULL,
    [RefDirectory]        VARCHAR (100)      NULL,
    [Consultant]          VARCHAR (5)        NULL,
    [Comments]            VARCHAR (255)      NULL,
    [tsModified]          DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_TSort_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]      NVARCHAR (168)     CONSTRAINT [DF_stgFact_TSort_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]      NVARCHAR (168)     CONSTRAINT [DF_stgFact_TSort_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]       NVARCHAR (168)     CONSTRAINT [DF_stgFact_TSort_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_TSort] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_stage_TSort_Pricing]
    ON [stgFact].[TSort]([PricingContact] ASC)
    INCLUDE([Refnum], [PricingContactEmail], [StudyYear]);


GO
CREATE NONCLUSTERED INDEX [IX_stage_TSort_DC]
    ON [stgFact].[TSort]([DCContact] ASC)
    INCLUDE([Refnum], [DCContactEmail], [StudyYear]);


GO
CREATE NONCLUSTERED INDEX [IX_stage_TSort_Coord]
    ON [stgFact].[TSort]([CoordName] ASC)
    INCLUDE([Refnum], [CoordTitle], [POBox], [Street], [City], [State], [Country], [Zip], [Telephone], [Fax], [WWW], [StudyYear]);


GO

CREATE TRIGGER [stgFact].[t_TSort_i]
	ON [stgFact].[TSort]
AFTER INSERT
AS BEGIN

	SET NOCOUNT ON;

	INSERT INTO [uploadAudit].[TSort]
	SELECT *
	FROM INSERTED;

END;

GO

CREATE TRIGGER [stgFact].[t_TSort_u]
	ON [stgFact].[TSort]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[TSort]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[TSort].Refnum		= INSERTED.Refnum;

END;