﻿CREATE TABLE [stgFact].[FeedPlan] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [PlanID]         VARCHAR (25)       NOT NULL,
    [Daily]          CHAR (1)           NULL,
    [BiWeekly]       CHAR (1)           NULL,
    [Weekly]         CHAR (1)           NULL,
    [BiMonthly]      CHAR (1)           NULL,
    [Monthly]        CHAR (1)           NULL,
    [LessMonthly]    CHAR (1)           NULL,
    [Rpt]            TINYINT            NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_FeedPlan_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedPlan_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedPlan_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_FeedPlan_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_FeedPlan] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PlanID] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_FeedPlan_u]
	ON [stgFact].[FeedPlan]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[FeedPlan]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[FeedPlan].Refnum		= INSERTED.Refnum
		AND	[stgFact].[FeedPlan].PlanID		= INSERTED.PlanID;

END;