﻿CREATE PROCEDURE [Console].[ExportContactsCRM]
	
AS
BEGIN

			
		SELECT	'' as Salutation,
	SUBSTRING(NameFull,1,CHARINDEX(' ',NameFull,1)) as [First Name],
	' ' as [Middle Name],
	SUBSTRING(NameFull,CHARINDEX(' ',NameFull,1),LEN(NameFull)) as [Last Name],
	[NameTitle] as Title,
	Company as [Parent Customer],
	[NumberVoice] as [Business Phone],
	'' as [Home Phone],
	[NumberMobile] as [Mobile Phone],
	[NumberFax] as [Fax],
	EMail as [E-mail],
	'' as Practice,
	[AddressPOBox] as [Address 1],
			[AddressStreet] as [Address 2] ,
			'' as [Address 3],
			'PRIMARY' as [Address Type],
			'' as [Name],
			[AddressCity] as [City],
			[AddressState] as [State],
			[AddressPostalCode] as [Zip],
			[AddressCountry] as [Country],
	'',0,0,'',0,1,'','','','','','','','','','','','','','','','','','','','','','',0,0,0,0,0,'','','','','',''	 
	FROM fact.TSortContact tc join dbo.TSort cl on tc.Refnum = cl.Refnum
	WHERE SUBSTRING(tc.RefNum,1,4) = '2013'
END
