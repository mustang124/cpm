﻿CREATE PROCEDURE [Console].[GetAnIssue] 

	@RefNum varchar(18),
	@IssueTitle nvarchar(50) = null,
	@IssueID nvarchar(50) = null

AS
BEGIN

Select * from Val.CheckList
Where Refnum = dbo.FormatRefNum('20' + @RefNum,0) And ((@IssueTitle IS NULL) OR (IssueTitle = @IssueTitle)) and 
((@IssueID IS NULL) OR (IssueID = @IssueID))

END
