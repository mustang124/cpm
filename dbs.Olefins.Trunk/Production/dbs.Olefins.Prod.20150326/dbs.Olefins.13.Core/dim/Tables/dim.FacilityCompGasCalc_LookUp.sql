﻿CREATE TABLE [dim].[FacilityCompGasCalc_LookUp] (
    [CalcTypeId]     VARCHAR (7)        NOT NULL,
    [CalcTypeName]   NVARCHAR (84)      NOT NULL,
    [CalcTypeDetail] NVARCHAR (256)     NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_FacilityCompGasCalc_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_FacilityCompGasCalc_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_FacilityCompGasCalc_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_FacilityCompGasCalc_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_FacilityCalcType_LookUp] PRIMARY KEY CLUSTERED ([CalcTypeId] ASC),
    CONSTRAINT [CL_FacilityCompGasCalc_LookUp_CalcTypeDetail] CHECK ([CalcTypeDetail]<>''),
    CONSTRAINT [CL_FacilityCompGasCalc_LookUp_CalcTypeID] CHECK ([CalcTypeId]<>''),
    CONSTRAINT [CL_FacilityCompGasCalc_LookUp_CalcTypeName] CHECK ([CalcTypeName]<>''),
    CONSTRAINT [UK_FacilityCompGasCalc_LookUp_CalcTypeDetail] UNIQUE NONCLUSTERED ([CalcTypeDetail] ASC),
    CONSTRAINT [UK_FacilityCompGasCalc_LookUp_CalcTypeName] UNIQUE NONCLUSTERED ([CalcTypeName] ASC)
);


GO

CREATE TRIGGER [dim].[t_FacilityCompGasCalc_LookUp_u]
	ON [dim].[FacilityCompGasCalc_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[FacilityCompGasCalc_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[FacilityCompGasCalc_LookUp].[CalcTypeId]		= INSERTED.[CalcTypeId];
		
END;