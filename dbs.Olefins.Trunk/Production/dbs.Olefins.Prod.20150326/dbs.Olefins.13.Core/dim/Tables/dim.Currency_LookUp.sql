﻿CREATE TABLE [dim].[Currency_LookUp] (
    [CurrencyId]            VARCHAR (4)        NOT NULL,
    [CurrencyName]          NVARCHAR (84)      NOT NULL,
    [CurrencyDetail]        NVARCHAR (256)     NOT NULL,
    [CurrencyGlyph]         NVARCHAR (3)       NULL,
    [SubUnit]               NVARCHAR (84)      NULL,
    [SubUnitGlyph]          NVARCHAR (3)       NULL,
    [MultiplierSubUnit]     REAL               CONSTRAINT [DF_Currency_LookUp_MultiplierSubUnit] DEFAULT ((100.0)) NULL,
    [MultiplierStored]      REAL               CONSTRAINT [DF_Currency_LookUp_MultiplierStored] DEFAULT ((1.0)) NULL,
    [MultiplierReported]    REAL               CONSTRAINT [DF_Currency_LookUp_MultiplierReported] DEFAULT ((1.0)) NULL,
    [LastUsed_Year]         INT                NULL,
    [ReplacementCurrencyID] VARCHAR (4)        NULL,
    [ISONumeric]            CHAR (3)           NULL,
    [tsModified]            DATETIMEOFFSET (7) CONSTRAINT [DF_Currency_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]        NVARCHAR (168)     CONSTRAINT [DF_Currency_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]        NVARCHAR (168)     CONSTRAINT [DF_Currency_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]         NVARCHAR (168)     CONSTRAINT [DF_Currency_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]          ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Currency_LookUp] PRIMARY KEY CLUSTERED ([CurrencyId] ASC),
    CONSTRAINT [CL_Currency_LookUp_CurrencyDetail] CHECK ([CurrencyDetail]<>''),
    CONSTRAINT [CL_Currency_LookUp_CurrencyGlyph] CHECK ([CurrencyGlyph]<>''),
    CONSTRAINT [CL_Currency_LookUp_CurrencyID] CHECK ([CurrencyId]<>''),
    CONSTRAINT [CL_Currency_LookUp_CurrencyName] CHECK ([CurrencyName]<>''),
    CONSTRAINT [CL_Currency_LookUp_ISONumeric] CHECK (len([ISONumeric])=(3)),
    CONSTRAINT [CL_Currency_LookUp_SubUnit] CHECK ([SubUnit]<>''),
    CONSTRAINT [CL_Currency_LookUp_SubUnitGlyph] CHECK ([SubUnitGlyph]<>''),
    CONSTRAINT [FK_Currency_LookUp_Replacement] FOREIGN KEY ([ReplacementCurrencyID]) REFERENCES [dim].[Currency_LookUp] ([CurrencyId]),
    CONSTRAINT [UK_Currency_LookUp_CurrencyDetail] UNIQUE NONCLUSTERED ([CurrencyDetail] ASC)
);


GO

CREATE TRIGGER [dim].[t_Currency_LookUp_u]
	ON [dim].[Currency_LookUp]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Currency_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Currency_LookUp].[CurrencyId]		= INSERTED.[CurrencyId];
		
END;
