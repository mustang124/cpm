﻿CREATE TABLE [dim].[Account_LookUp] (
    [AccountId]      VARCHAR (42)       NOT NULL,
    [AccountName]    NVARCHAR (84)      NOT NULL,
    [AccountDetail]  NVARCHAR (256)     NOT NULL,
    [AccountTypeId]  VARCHAR (12)       NOT NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_Account_LookUp_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_Account_LookUp_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_Account_LookUp_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_Account_LookUp_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    [tsModifiedRV]   ROWVERSION         NOT NULL,
    CONSTRAINT [PK_Account_LookUp] PRIMARY KEY CLUSTERED ([AccountId] ASC),
    CONSTRAINT [CL_Account_LookUp_AccountDetail] CHECK ([AccountDetail]<>''),
    CONSTRAINT [CL_Account_LookUp_AccountID] CHECK ([AccountId]<>''),
    CONSTRAINT [CL_Account_LookUp_AccountName] CHECK ([AccountName]<>''),
    CONSTRAINT [FK_Account_LookUp_AccountType] FOREIGN KEY ([AccountTypeId]) REFERENCES [dim].[AccountType_LookUp] ([AccountTypeId]),
    CONSTRAINT [UK_Account_LookUp_AccountDetail] UNIQUE NONCLUSTERED ([AccountDetail] ASC),
    CONSTRAINT [UK_Account_LookUp_AccountName] UNIQUE NONCLUSTERED ([AccountName] ASC)
);


GO

CREATE TRIGGER [dim].[t_Account_LookUp_u]
ON [dim].[Account_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE [dim].[Account_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM INSERTED
	WHERE	[dim].[Account_LookUp].[AccountId]		= INSERTED.[AccountId];
		
END;
