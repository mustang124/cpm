﻿
<System.Runtime.Remoting.Contexts.Synchronization()>
Friend NotInheritable Class BackgroundWorker
    Inherits Sa.Simulation.Threading.BackgroundWorker

    Private Shadows SimWorker As Sa.Simulation.Pyps.Engine

    Friend Sub New(ByVal SimWkr As Sa.Simulation.Pyps.Engine)
        MyBase.New(SimWkr)
    End Sub

End Class

<System.Runtime.Remoting.Contexts.Synchronization()>
Public NotInheritable Class Engine
    Inherits Sa.Simulation.Threading.Engine

    <System.ThreadStatic()> Private BGWs As New List(Of Sa.Simulation.Pyps.BackgroundWorker)

    Private Event Done()

    Protected Overrides Sub OnDoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs)

        Dim a As Sa.Simulation.Threading.Argument = DirectCast(e.Argument, Sa.Simulation.Threading.Argument)
        Dim b As Sa.Simulation.Pyps.BackgroundWorker = DirectCast(sender, Sa.Simulation.Pyps.BackgroundWorker)

        Dim s As New Sa.Simulation.Pyps.Simulation(b)

        s.StartSimulation(a)

    End Sub

    <System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)>
    Public Overrides Sub StopSimulations()

        Me.QueuedItems.Clear()

        For Each bgw As Sa.Simulation.Pyps.BackgroundWorker In BGWs
            bgw.CancelAsync()
            bgw.Dispose()
        Next

        BGWs.Clear()

        RaiseEvent Done()

    End Sub

    <System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)>
    Public Overrides Sub RunSimulation(
        ByVal lv As System.Windows.Forms.ListView,
        ByVal ThreadCount As System.Int32)

        MyBase.lv = lv

        If BGWs.Count < Math.Min(ThreadCount, Me.QueuedItems.Count) Then

            For i As System.Int32 = 0 To Math.Min(ThreadCount, Me.QueuedItems.Count) - 1 Step 1

                Dim a As Sa.Simulation.Threading.Argument = Me.QueuedItems.Dequeue

                BGWs.Add(New Sa.Simulation.Pyps.BackgroundWorker(Me))
                BGWs(i).RunWorkerAsync(a)

            Next i

        End If

    End Sub

End Class