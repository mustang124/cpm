﻿

CREATE FUNCTION [dbo].[ExchangeRate](@FromCurrency char(4), @ToCurrency char(4), @DataYear smallint, @InflateToYear smallint, @HomeCurrency char(4))
RETURNS real
BEGIN
	DECLARE @Value real
	IF @InflateToYear > 1900 -- inflate
	BEGIN
		IF @FromCurrency = @ToCurrency AND @FromCurrency = @HomeCurrency
        		SELECT @Value = dbo.InflationFactor(@FromCurrency, @DataYear, @InflateToYear)
     		ELSE BEGIN
			IF @FromCurrency = @HomeCurrency
        			SELECT @Value = dbo.ClosestCurrencyPerUSD(@ToCurrency, @InflateToYear)/dbo.ClosestCurrencyPerUSD(@HomeCurrency, @InflateToYear)*dbo.InflationFactor(@HomeCurrency, @DataYear, @InflateToYear)
			ELSE
        			SELECT @Value = dbo.ExchangeRate(@FromCurrency, @HomeCurrency, @DataYear, NULL, @HomeCurrency)*dbo.ExchangeRate(@HomeCurrency, @ToCurrency, @InflateToYear, NULL, @HomeCurrency)*dbo.InflationFactor(@HomeCurrency, @DataYear, @InflateToYear)
    		END
	END
    	ELSE BEGIN  -- no inflation
    		IF @FromCurrency = @ToCurrency
        		SELECT @Value = 1
     		ELSE
        		SELECT @Value = dbo.ClosestCurrencyPerUSD(@ToCurrency, @DataYear)/dbo.ClosestCurrencyPerUSD(@FromCurrency, @DataYear)
	END
	RETURN @Value
END


