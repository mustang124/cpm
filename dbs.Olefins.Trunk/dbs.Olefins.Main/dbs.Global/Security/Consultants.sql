﻿CREATE ROLE [Consultants]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'BLT';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CJM';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'CLC';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'DEJ';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'DGM';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JGY';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JIV';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JJC';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JRW';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JSJ';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'MRH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'PED';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'PowerConsole';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'REB';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'FSW';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'RGA';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'rlb';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'RWP';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'TWS';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'wlw';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'MDV';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JPH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'SSW';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'WHH';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JJ';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JRP';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'MGV';


GO
EXECUTE sp_addrolemember @rolename = N'Consultants', @membername = N'JAB';

