﻿CREATE TABLE [sim].[YieldCompositionStream] (
    [QueueID]           BIGINT             NOT NULL,
    [FactorSetId]       VARCHAR (12)       NOT NULL,
    [Refnum]            VARCHAR (25)       NOT NULL,
    [CalDateKey]        INT                NOT NULL,
    [SimModelId]        VARCHAR (12)       NOT NULL,
    [OpCondId]          VARCHAR (12)       NOT NULL,
    [StreamId]          VARCHAR (42)       NOT NULL,
    [StreamDescription] NVARCHAR (256)     NOT NULL,
    [RecycleId]         INT                NOT NULL,
    [ComponentId]       VARCHAR (42)       NOT NULL,
    [Component_WtPcnt]  REAL               NOT NULL,
    [tsModified]        DATETIMEOFFSET (7) CONSTRAINT [DF_YieldCompositionStream_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]    NVARCHAR (168)     CONSTRAINT [DF_YieldCompositionStream_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]    NVARCHAR (168)     CONSTRAINT [DF_YieldCompositionStream_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]     NVARCHAR (168)     CONSTRAINT [DF_YieldCompositionStream_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_YieldCompositionStream] PRIMARY KEY CLUSTERED ([FactorSetId] ASC, [Refnum] ASC, [RecycleId] ASC, [SimModelId] ASC, [OpCondId] ASC, [StreamId] ASC, [StreamDescription] ASC, [ComponentId] ASC, [CalDateKey] ASC),
    CONSTRAINT [CL_YieldCompositionStream_StreamDescription] CHECK ([StreamDescription]<>''),
    CONSTRAINT [FK_YieldCompositionStream_Calendar_LookUp] FOREIGN KEY ([CalDateKey]) REFERENCES [dim].[Calendar_LookUp] ([CalDateKey]),
    CONSTRAINT [FK_YieldCompositionStream_Component_LookUp] FOREIGN KEY ([ComponentId]) REFERENCES [dim].[Component_LookUp] ([ComponentId]),
    CONSTRAINT [FK_YieldCompositionStream_FactorSet_LookUp] FOREIGN KEY ([FactorSetId]) REFERENCES [dim].[FactorSet_LookUp] ([FactorSetId]),
    CONSTRAINT [FK_YieldCompositionStream_SimModel_LookUp] FOREIGN KEY ([SimModelId]) REFERENCES [dim].[SimModel_LookUp] ([ModelId]),
    CONSTRAINT [FK_YieldCompositionStream_SimOpCond_LookUp] FOREIGN KEY ([OpCondId]) REFERENCES [dim].[SimOpCond_LookUp] ([OpCondId]),
    CONSTRAINT [FK_YieldCompositionStream_SimQueue] FOREIGN KEY ([QueueID]) REFERENCES [q].[Simulation] ([QueueID]),
    CONSTRAINT [FK_YieldCompositionStream_Stream_LookUp] FOREIGN KEY ([StreamId]) REFERENCES [dim].[Stream_LookUp] ([StreamId])
);


GO

CREATE TRIGGER [sim].[t_YieldCompositionStream_u]
	ON [sim].[YieldCompositionStream]
	AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE sim.[YieldCompositionStream]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[sim].[YieldCompositionStream].FactorSetId			= INSERTED.FactorSetId
		AND	[sim].[YieldCompositionStream].Refnum				= INSERTED.Refnum
		AND	[sim].[YieldCompositionStream].CalDateKey			= INSERTED.CalDateKey
		AND	[sim].[YieldCompositionStream].SimModelId			= INSERTED.SimModelId
		AND	[sim].[YieldCompositionStream].OpCondId				= INSERTED.OpCondId
		AND	[sim].[YieldCompositionStream].StreamId				= INSERTED.StreamId
		AND	[sim].[YieldCompositionStream].StreamDescription	= INSERTED.StreamDescription
		AND	[sim].[YieldCompositionStream].RecycleId			= INSERTED.RecycleId
		AND	[sim].[YieldCompositionStream].ComponentId			= INSERTED.ComponentId;

END;