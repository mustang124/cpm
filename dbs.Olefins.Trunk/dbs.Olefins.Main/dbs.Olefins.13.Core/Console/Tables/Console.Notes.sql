﻿CREATE TABLE [Console].[Notes] (
    [Refnum]                  VARCHAR (18) NOT NULL,
    [ValidationNotes]         TEXT         NULL,
    [ConsultingOpportunities] TEXT         NULL,
    [ContinuingIssues]        TEXT         NULL,
    CONSTRAINT [PK_ValidationNotes] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 70)
);

