﻿CREATE PROCEDURE [Console].[UpdateContact] 
	@RefNum varchar(18),
	@ContactType nvarchar(20),
	@StudyYear nvarchar(4),
	@FirstName nvarchar(80),
	@LastName nvarchar(80),
	@Address1 nvarchar(80),
	@Address2 nvarchar(80),
	@Address3 nvarchar(80),
	@City nvarchar(40),
	@State nvarchar(20),
	@Zip nvarchar(20),
	@Country nvarchar(80),
	@Phone nvarchar(20),
	@Password nvarchar(20),
	@Email nvarchar(80),
	@StAddress1 as nvarchar(80),
	@StAddress2 as nvarchar(80),
	@StAddress3 as nvarchar(80),
	@StAddress4 as nvarchar(80),
	@StAddress5 as nvarchar(80),
	@StAddress6 as nvarchar(80),
	@STAddress7 as nvarchar(80),
	@Fax as nvarchar(20),
	@JobTitle as nvarchar(75)
AS

BEGIN
	DECLARE @ContactCode nvarchar(20)

	SELECT @ContactCode = ContactCode from TSort where SmallRefnum=@Refnum

	IF EXISTS(SELECT ContactCode from CoContactInfo where ContactType = @ContactType and ContactCode = @ContactCode and StudyYear=@StudyYear)
		
			UPDATE C 
			SET
					c.FirstName=@FirstName ,
					c.LastName=@LastName ,
					c.Email =@Email,
					c.Phone = @Phone,
					c.Password = @Password ,
					c.Fax = @Fax ,
					c.MailAddr1 = @Address1 ,
					c.MailAddr2 = @Address2 ,
					c.MailAddr3 = @Address3 ,
					c.MailCity = @City  ,
					c.MailState = @State ,
					c.MailZip = @Zip ,
					c.MailCountry = @Country ,
					c.StrAddr1 = @StAddress1,
					c.StrAddr2 = @StAddress2,
					c.StrAdd3 = @StAddress3,
					c.StrCity = @StAddress4,
					c.StrState = @StAddress5,
					c.StrZip = @StAddress6,
					c.JobTitle = @JobTitle,
					c.StrCountry = @StAddress7,
					c.ContactType = @ContactType
					
					
				 FROM CoContactInfo c join tsort t on t.ContactCode = c.contactcode
					WHERE t.SmallRefnum=@RefNum and c.StudyYear=@StudyYear and c.ContactType = @ContactType
					
	ELSE
	
				 INSERT INTO CoContactInfo (ContactCode, ContactType, StudyYear, FirstName, LastName,
				 JobTitle, Phone, Password, Fax, Email, StrAddr1, StrAddr2, StrAdd3,StrCity,StrState,StrZip,
				 StrCountry,MailAddr1, MailAddr2, MailAddr3, MailCity, MailState, MailZip, MailCountry,CCAlt)
				 Values(					
						@ContactCode,
						@ContactType,
						@StudyYear,
						@FirstName ,
						@LastName ,
						@JobTitle,
						@Phone,
						@Password,
						@Fax,
						@Email,
						@StAddress1,
						@StAddress2,
						@StAddress3,
						@StAddress4,
						@StAddress5,
						@StAddress6,
						@STAddress7,
						@Address1 ,
						@Address2 ,
						@Address3 ,
						@City  ,
						@State ,
						@Zip ,
						@Country,
						'N')
					
					
	
END
