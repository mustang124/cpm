﻿CREATE PROCEDURE [Console].[UpdateNote]

	@User varchar(10),
	@RefNum varchar(18),
	@NoteType varchar(20),
	@Note nvarchar(max)

AS
BEGIN
    if not exists(select * from Val.Notes where Refnum=@RefNum and notetype = @NoteType and Note = @Note)
	INSERT INTO val.Notes (RefNum, NoteType, Note, UpdatedBy, Updated) Values(@RefNum,@NoteType, @Note, @User, GETDATE())

END
