﻿CREATE PROCEDURE [Console].[GetValidationNotes]
	@RefNum varchar(18),
	@NoteType varchar(20)
AS
BEGIN

	SELECT * From Val.Notes Where Refnum = @RefNum and NoteType = @NoteType order by updated desc

END
