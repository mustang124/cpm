﻿CREATE proc [Console].[GetGradesByVersion]
@RefNum varchar(18),
@Version int
as
SELECT Version, ROUND(GRADE,0) AS Grade, NumReds, NumBlues, NumTeals, NumGreens FROM Val.RefineryGrade WHERE Refnum = @RefNum  ORDER BY version DESC, Grade DESC
