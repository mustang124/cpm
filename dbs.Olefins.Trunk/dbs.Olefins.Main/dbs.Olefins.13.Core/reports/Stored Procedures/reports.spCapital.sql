﻿






CREATE              PROC [reports].[spCapital](@GroupId varchar(25))
AS
SET NOCOUNT ON
DECLARE @RefCount smallint
Declare @Currency varchar(4) = 'USD'

DECLARE @MyGroup TABLE 
(
	Refnum varchar(25) NOT NULL
)
INSERT @MyGroup (Refnum) SELECT Refnum FROM reports.GroupPlants WHERE GroupId = @GroupId

DELETE FROM reports.Capital WHERE GroupId = @GroupId
Insert into reports.Capital (GroupId
, Currency
, CptlPlantCnt 
, CptlOnsite_MUSD
, CptlOffsite_MUSD
, CptlExist_MUSD
, CptlEnergyCons_MUSD
, CptlRegEnvir_MUSD
, CptlSafety_MUSD
, CptlComputer_MUSD
, CptlMaintCap_MUSD
, CptlTot_MUSD
, CptlTot_PcntRV
, CptlTotPerEDC
, EthyleneCapAddedCnt
, EthyleneCapAddNew_kMT
, EthyleneCapAddExist_kMT
, EthyleneCapAddTot_kMT
, ProjPlantCnt
, ProjCptlOnsite_MUSD
, ProjCptlOffsite_MUSD
, ProjCptlExist_MUSD
, ProjCptlEnergyCons_MUSD
, ProjCptlRegEnvir_MUSD
, ProjCptlSafety_MUSD
, ProjCptlComputer_MUSD
, ProjCptlMaintCap_MUSD
, ProjCptlTot_MUSD
, ProjCptlTot_PcntRV
, ProjCptlTotPerEDC)

SELECT GroupId = @GroupId
, Currency = @Currency
, CptlPlantCnt =		SUM(CptlPlantCnt)
, CptlOnsite_MUSD =		[$(DbGlobal)].dbo.WtAvg(CptlOnsite_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlOffsite_MUSD = 	[$(DbGlobal)].dbo.WtAvg(CptlOffsite_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlExist_MUSD = 		[$(DbGlobal)].dbo.WtAvg(CptlExist_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlEnergyCons_MUSD = [$(DbGlobal)].dbo.WtAvg(CptlEnergyCons_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlRegEnvir_MUSD = 	[$(DbGlobal)].dbo.WtAvg(CptlRegEnvir_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlSafety_MUSD = 	[$(DbGlobal)].dbo.WtAvg(CptlSafety_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlComputer_MUSD = 	[$(DbGlobal)].dbo.WtAvg(CptlComputer_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlMaintCap_MUSD = 	[$(DbGlobal)].dbo.WtAvg(CptlMaintCap_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlTot_MUSD = 		[$(DbGlobal)].dbo.WtAvg(CptlTot_MUSD, CASE WHEN CptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, CptlTot_PcntRV =		[$(DbGlobal)].dbo.WtAvg(CptlTot_PcntRV, CASE WHEN RV_MUS>0 AND CptlTot_MUSD > 0 THEN RV_MUS ELSE 0 END)
, CptlTotPerEDC =		[$(DbGlobal)].dbo.WtAvg(CptlTotPerEDC, CASE WHEN CptlTot_MUSD > 0 THEN kEdc ELSE 0 END)
, EthyleneCapAddedCnt =		SUM(EthyleneCapAddedCnt)
, EthyleneCapAddNew_kMT =	[$(DbGlobal)].dbo.WtAvg(EthyleneCapAddNew_kMT, 1)				-- CASE WHEN EthyleneCapAddedCnt > 0 THEN 1 ELSE 0 END)
, EthyleneCapAddExist_kMT = [$(DbGlobal)].dbo.WtAvg(EthyleneCapAddExist_kMT, 1)				-- CASE WHEN EthyleneCapAddedCnt > 0 THEN 1 ELSE 0 END)
, EthyleneCapAddTot_kMT =	[$(DbGlobal)].dbo.WtAvg(ISNULL(EthyleneCapAddNew_kMT,0) + ISNULL(EthyleneCapAddExist_kMT,0), 1)		-- CASE WHEN EthyleneCapAddedCnt > 0 THEN 1 ELSE 0 END)
, ProjPlantCnt =			SUM(ProjPlantCnt)
, ProjCptlOnsite_MUSD =		[$(DbGlobal)].dbo.WtAvg(ProjCptlOnsite_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlOffsite_MUSD =	[$(DbGlobal)].dbo.WtAvg(ProjCptlOffsite_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlExist_MUSD =		[$(DbGlobal)].dbo.WtAvg(ProjCptlExist_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlEnergyCons_MUSD = [$(DbGlobal)].dbo.WtAvg(ProjCptlEnergyCons_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlRegEnvir_MUSD =	[$(DbGlobal)].dbo.WtAvg(ProjCptlRegEnvir_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlSafety_MUSD =		[$(DbGlobal)].dbo.WtAvg(ProjCptlSafety_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlComputer_MUSD =	[$(DbGlobal)].dbo.WtAvg(ProjCptlComputer_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlMaintCap_MUSD =	[$(DbGlobal)].dbo.WtAvg(ProjCptlMaintCap_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlTot_MUSD =		[$(DbGlobal)].dbo.WtAvg(ProjCptlTot_MUSD, CASE WHEN ProjCptlTot_MUSD > 0 THEN 1 ELSE 0 END)
, ProjCptlTot_PcntRV =		[$(DbGlobal)].dbo.WtAvg(ProjCptlTot_PcntRV, CASE WHEN ProjCptlTot_MUSD > 0 THEN RV_MUS ELSE 0 END)
, ProjCptlTotPerEDC =		[$(DbGlobal)].dbo.WtAvg(ProjCptlTotPerEDC, CASE WHEN ProjCptlTot_MUSD > 0 THEN kEdc ELSE 0 END)
from @MyGroup r join dbo.Capital c on r.Refnum=c.Refnum

SET NOCOUNT OFF

















