﻿CREATE PROCEDURE [stgFact].[Delete_TotMaintCost]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[TotMaintCost]
	WHERE [Refnum] = @Refnum;

END;