﻿CREATE PROCEDURE [stgFact].[Insert_YieldCompositionStream]
(
	@Refnum					VARCHAR (25),
	@SimModelId				VARCHAR (12),
	@OpCondId				VARCHAR (12),
	@StreamId				VARCHAR (42),
	@ComponentId			VARCHAR (42),

	@Component_WtPcnt		REAL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [stgFact].[YieldCompositionStream]([Refnum], [SimModelId], [OpCondId], [StreamId], [ComponentId], [Component_WtPcnt])
	VALUES(@Refnum, @SimModelId, @OpCondId, @StreamId, @ComponentId, @Component_WtPcnt);

END;