﻿CREATE PROCEDURE [stgFact].[Delete_OpEx]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[OpEx]
	WHERE [Refnum] = @Refnum;

END;