﻿CREATE PROCEDURE [stgFact].[Delete_Facilities]
(
	@Refnum		VARCHAR(25)
)
AS
BEGIN

	SET NOCOUNT ON;

	DELETE FROM [stgFact].[Facilities]
	WHERE [Refnum] = @Refnum;

END;