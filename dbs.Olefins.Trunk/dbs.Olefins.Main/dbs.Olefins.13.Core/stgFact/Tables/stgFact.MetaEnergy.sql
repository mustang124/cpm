﻿CREATE TABLE [stgFact].[MetaEnergy] (
    [Refnum]         VARCHAR (25)       NOT NULL,
    [EnergyType]     VARCHAR (12)       NOT NULL,
    [Qtr1]           REAL               NULL,
    [Qtr2]           REAL               NULL,
    [Qtr3]           REAL               NULL,
    [Qtr4]           REAL               NULL,
    [Total]          REAL               NULL,
    [tsModified]     DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_MetaEnergy_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost] NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaEnergy_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser] NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaEnergy_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]  NVARCHAR (168)     CONSTRAINT [DF_stgFact_MetaEnergy_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_MetaEnergy] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EnergyType] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_MetaEnergy_u]
	ON [stgFact].[MetaEnergy]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[MetaEnergy]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[MetaEnergy].Refnum			= INSERTED.Refnum
		AND	[stgFact].[MetaEnergy].EnergyType		= INSERTED.EnergyType;

END;