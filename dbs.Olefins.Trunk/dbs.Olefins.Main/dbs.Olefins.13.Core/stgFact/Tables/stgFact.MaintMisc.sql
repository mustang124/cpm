﻿CREATE TABLE [stgFact].[MaintMisc] (
    [Refnum]           VARCHAR (25)       NOT NULL,
    [CorrEmergMaint]   REAL               NULL,
    [CorrOthMaint]     REAL               NULL,
    [PrevCondAct]      REAL               NULL,
    [PrevAddMaint]     REAL               NULL,
    [PrevRoutine]      REAL               NULL,
    [PrefAddRoutine]   REAL               NULL,
    [Accuracy]         CHAR (1)           NULL,
    [FurnTubeLife]     REAL               NULL,
    [RespUnplan]       REAL               NULL,
    [RespPlan]         REAL               NULL,
    [Prevent]          REAL               NULL,
    [Predict]          REAL               NULL,
    [Training]         REAL               NULL,
    [FurnRetubeTime]   REAL               NULL,
    [FurnRetubeWho]    VARCHAR (25)       NULL,
    [MaintMatlPY]      REAL               NULL,
    [ContMaintLaborPY] REAL               NULL,
    [ContMaintMatlPY]  REAL               NULL,
    [MaintEquipRentPY] REAL               NULL,
    [OccMaintST_PY]    REAL               NULL,
    [MpsMaintST_PY]    REAL               NULL,
    [tsModified]       DATETIMEOFFSET (7) CONSTRAINT [DF_stgFact_MaintMisc_tsModified] DEFAULT (sysdatetimeoffset()) NOT NULL,
    [tsModifiedHost]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_MaintMisc_tsModifiedHost] DEFAULT (host_name()) NOT NULL,
    [tsModifiedUser]   NVARCHAR (168)     CONSTRAINT [DF_stgFact_MaintMisc_tsModifiedUser] DEFAULT (suser_sname()) NOT NULL,
    [tsModifiedApp]    NVARCHAR (168)     CONSTRAINT [DF_stgFact_MaintMisc_tsModifiedApp] DEFAULT (app_name()) NOT NULL,
    CONSTRAINT [PK_stgFact_MaintMisc] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);


GO

CREATE TRIGGER [stgFact].[t_MaintMisc_u]
	ON [stgFact].[MaintMisc]
	AFTER UPDATE
AS BEGIN

	SET NOCOUNT ON;

	UPDATE [stgFact].[MaintMisc]
	SET	tsModified		= SYSDATETIMEOFFSET(),
		tsModifiedHost	= HOST_NAME(),
		tsModifiedUser	= SUSER_SNAME(),
		tsModifiedApp	= APP_NAME()
	FROM INSERTED
	WHERE	[stgFact].[MaintMisc].Refnum		= INSERTED.Refnum;

END;